<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
  <xsl:variable name="colNum" select="count(//colgroup/col)"/>
  <xsl:variable name="comName" select="/root/@comName"/>
  <root>
    <xsl:for-each select='//colgroup'>
      <colgroup>
      <xsl:for-each select='col'>
  	    <col style='{@style}'/>
      </xsl:for-each>
      </colgroup>
    </xsl:for-each>
    <xsl:for-each select='//maintitle | /root/top/title'>
      <tr height='35'><td border='none' colspan='{$colNum}' style='text-align:center;'><xsl:value-of select="."/></td></tr>
    </xsl:for-each>

    <xsl:for-each select='//subtitle'>
      <tr height='20'><td border='none' colspan='{$colNum}' style='text-align:center;'><xsl:value-of select="."/></td></tr>
    </xsl:for-each>

    <xsl:for-each select='/root/top/tr'>
      <xsl:call-template name="message">
        <xsl:with-param name="position" select="position()"/>
        <xsl:with-param name="colNum" select="$colNum"/>
        <xsl:with-param name="tdNum" select="count(td)"/>
      </xsl:call-template>
    </xsl:for-each>
    
    <xsl:for-each select='//thead/tr'>
      <tr height='25'>
        <xsl:for-each select='th'>
          <xsl:choose>
            <xsl:when test='@type = 2'/>
            <xsl:otherwise>
              <td colspan='{@colspan}' rowspan='{@rowspan}' style='text-align:center;{@style}'>
                <xsl:value-of select="."/>
              </td>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </tr>
    </xsl:for-each>

    <xsl:for-each select='//tbody/tr'>
      <tr height='22'>
        <xsl:for-each select='td'>
          <xsl:choose>
            <xsl:when test='@type = 2'/>
            <xsl:otherwise>
              <td colspan='{@colspan}' rowspan='{@rowspan}' style='{@style}' class='{@class}'>
                <xsl:value-of select="."/>
              </td>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </tr>
    </xsl:for-each>

    <xsl:for-each select='/root/bottom/tr'>
      <xsl:call-template name="message">
        <xsl:with-param name="position" select="position()"/>
        <xsl:with-param name="colNum" select="$colNum"/>
        <xsl:with-param name="tdNum" select="count(td)"/>
      </xsl:call-template>
    </xsl:for-each>
  </root>
  </xsl:template>
  
  <xsl:template name="message">
    <xsl:param name="position"/>
    <xsl:param name="colNum"/>
    <xsl:param name="tdNum"/>
    <tr height='22'>
      <xsl:for-each select="td">
        <td nowrap="nowrap" rowspan="{@rowspan}" border='none'>  
          <xsl:choose>
            <xsl:when test="position()=1">
              <xsl:attribute name='style'><xsl:value-of select='@style'/></xsl:attribute>
              <xsl:attribute name='colspan'><xsl:value-of select='$colNum - floor($colNum div $tdNum)*($tdNum - 1)'/></xsl:attribute>
            </xsl:when>
            <xsl:when test="position()=last()">
              <xsl:attribute name='style'>text-align:right;<xsl:value-of select='@style'/></xsl:attribute>
              <xsl:attribute name='colspan'><xsl:value-of select='floor($colNum div $tdNum)'/></xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name='style'><xsl:value-of select='@style'/></xsl:attribute>
              <xsl:attribute name='colspan'><xsl:value-of select='floor($colNum div $tdNum)'/></xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="contains(., '[ҳ��]/[��ҳ��]')">
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="." disable-output-escaping="yes"/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </xsl:for-each>
    </tr>
  </xsl:template>
</xsl:stylesheet>

