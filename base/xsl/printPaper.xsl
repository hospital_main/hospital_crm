<?xml version='1.0' encoding="GBK"?>
<!--xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl"-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/root">
    <xsl:variable name="choose" select="select"/>
    <select id="paper_Select">
      <xsl:for-each select="page">
        <option>
          <xsl:attribute name="value" >
            <xsl:value-of select="type"/>
          </xsl:attribute>
          <xsl:if test="type = $choose">
            <xsl:attribute name="selected"/>
          </xsl:if>
          <xsl:value-of select="type"/>
        </option>
      </xsl:for-each>
    </select>
  </xsl:template>
</xsl:stylesheet>


