<?xml version='1.0' encoding="GBK"?>
<!--xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl"-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <table width='100%' height='100%' style='border: inset 1px;background-color: #F6F6F6;'>
      <tr height="49%" width='100%'>
        <td valign="top" >
        <table width='100%'>
        <tr>
        <td class='checkTable'><b id="_checkLeftTitle_f6f5">银行对帐单</b></td>
        </tr>
        <tr>
        <td width='100%' >
        	<div  height='100%' id='_check1' style='overflow:auto;padding:0;SCROLLBAR-FACE-COLOR: #F2F2F2; SCROLLBAR-HIGHLIGHT-COLOR: #999999;SCROLLBAR-SHADOW-COLOR:#999999; SCROLLBAR-3DLIGHT-COLOR:#ffffff; SCROLLBAR-ARROW-COLOR:#999999; SCROLLBAR-TRACK-COLOR:#ffffff;SCROLLBAR-DARKSHADOW-COLOR: #ffffff;'>
          	<table1/>
          </div>
        </td>
        </tr>
        </table>
        </td>
      <td valign="top" >
      <table width='100%'>
      <tr>
        <td width='100%' class='checkTable'><b id="_checkRightTitle_f7f8">银行帐</b></td>
        </tr>
        <tr>
        <td width='100%' >
        	<div id='_check2' width='100%' height='100%' style='overflow:auto;padding:0;SCROLLBAR-FACE-COLOR: #F2F2F2; SCROLLBAR-HIGHLIGHT-COLOR: #999999;SCROLLBAR-SHADOW-COLOR:#999999; SCROLLBAR-3DLIGHT-COLOR:#ffffff; SCROLLBAR-ARROW-COLOR:#999999; SCROLLBAR-TRACK-COLOR:#ffffff;SCROLLBAR-DARKSHADOW-COLOR: #ffffff;'>
          	<table2/>
          </div>
        </td>
        </tr>
        </table>
        </td>
	    </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>


