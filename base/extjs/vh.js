Ext.menu.MMenu = Ext.extend(Ext.Container, {
    
    
    
    minWidth : 120,
    
    shadow : 'sides',
    
    subMenuAlign : 'tl-tr?',
    
    defaultAlign : 'tl-bl?',
    
    allowOtherMenus : false,
    
    ignoreParentClicks : false,
    
    enableScrolling : true,
    
    maxHeight : null,
    
    scrollIncrement : 24,
    
    showSeparator : true,
    
    defaultOffsets : [0, 0],

    
    plain : false,

    
    floating : true,


    
    zIndex: 15000,

    
    hidden : true,

    
    layout : 'menu',
    hideMode : 'offsets',    
    scrollerHeight : 8,
    autoLayout : true,       
    defaultType : 'menuitem',
    bufferResize : false,

    initComponent : function(){
        if(Ext.isArray(this.initialConfig)){
            Ext.apply(this, {items:this.initialConfig});
        }
        this.addEvents(
            
            'click',
            
            'mouseover',
            
            'mouseout',
            
            'itemclick'
        );
        //Ext.menu.MenuMgr.register(this);
        if(this.floating){
            Ext.EventManager.onWindowResize(this.hide, this);
        }else{
            if(this.initialConfig.hidden !== false){
                this.hidden = false;
            }
            this.internalDefaults = {hideOnClick: false};
        }
        Ext.menu.Menu.superclass.initComponent.call(this);
        if(this.autoLayout){
            var fn = this.doLayout.createDelegate(this, []);
            this.on({
                add: fn,
                remove: fn
            });
        }
    },

    
    getLayoutTarget : function() {
        return this.ul;
    },

    
    onRender : function(ct, position){
        if(!ct){
            ct = Ext.getBody();
        }

        var dh = {
            id: this.getId(),
            cls: 'x-menu ' + ((this.floating) ? 'x-menu-floating x-layer ' : '') + (this.cls || '') + (this.plain ? ' x-menu-plain' : '') + (this.showSeparator ? '' : ' x-menu-nosep'),
            style: this.style,
            cn: [
                {tag: 'a', cls: 'x-menu-focus', href: '#', onclick: 'return false;', tabIndex: '-1'},
                {tag: 'ul', cls: 'x-menu-list'}
            ]
        };
        if(this.floating){
            this.el = new Ext.Layer({
                shadow: this.shadow,
                dh: dh,
                constrain: false,
                parentEl: ct,
                zindex: this.zIndex
            });
        }else{
            this.el = ct.createChild(dh);
        }
        Ext.menu.Menu.superclass.onRender.call(this, ct, position);

        if(!this.keyNav){
            this.keyNav = new Ext.menu.MenuNav(this);
        }
        
        this.focusEl = this.el.child('a.x-menu-focus');
        this.ul = this.el.child('ul.x-menu-list');
        this.mon(this.ul, {
            scope: this,
            click: this.onClick,
            mouseover: this.onMouseOver,
            mouseout: this.onMouseOut
        });
        if(this.enableScrolling){
            this.mon(this.el, {
                scope: this,
                delegate: '.x-menu-scroller',
                click: this.onScroll,
                mouseover: this.deactivateActive
            });
        }
    },

    
    findTargetItem : function(e){
        var t = e.getTarget('.x-menu-list-item', this.ul, true);
        if(t && t.menuItemId){
            return this.items.get(t.menuItemId);
        }
    },

    
    onClick : function(e){
        var t = this.findTargetItem(e);
        if(t){
            if(t.isFormField){
                this.setActiveItem(t);
            }else if(t instanceof Ext.menu.BaseItem){
                if(t.menu && this.ignoreParentClicks){
                    t.expandMenu();
                    e.preventDefault();
                }else if(t.onClick){
                    t.onClick(e);
                    this.fireEvent('click', this, t, e);
                }
            }
        }
    },

    
    setActiveItem : function(item, autoExpand){
        if(item != this.activeItem){
            this.deactivateActive();
            if((this.activeItem = item).isFormField){
                item.focus();
            }else{
                item.activate(autoExpand);
            }
        }else if(autoExpand){
            item.expandMenu();
        }
    },

    deactivateActive : function(){
        var a = this.activeItem;
        if(a){
            if(a.isFormField){
                
                if(a.collapse){
                    a.collapse();
                }
            }else{
                a.deactivate();
            }
            delete this.activeItem;
        }
    },

    
    tryActivate : function(start, step){
        var items = this.items;
        for(var i = start, len = items.length; i >= 0 && i < len; i+= step){
            var item = items.get(i);
            if(item.isVisible() && !item.disabled && (item.canActivate || item.isFormField)){
                this.setActiveItem(item, false);
                return item;
            }
        }
        return false;
    },

    
    onMouseOver : function(e){
        var t = this.findTargetItem(e);
        if(t){
            if(t.canActivate && !t.disabled){
                this.setActiveItem(t, true);
            }
        }
        this.over = true;
        this.fireEvent('mouseover', this, e, t);
    },

    
    onMouseOut : function(e){
        var t = this.findTargetItem(e);
        if(t){
            if(t == this.activeItem && t.shouldDeactivate && t.shouldDeactivate(e)){
                this.activeItem.deactivate();
                delete this.activeItem;
            }
        }
        this.over = false;
        this.fireEvent('mouseout', this, e, t);
    },

    
    onScroll : function(e, t){
        if(e){
            e.stopEvent();
        }
        var ul = this.ul.dom, top = Ext.fly(t).is('.x-menu-scroller-top');
        ul.scrollTop += this.scrollIncrement * (top ? -1 : 1);
        if(top ? ul.scrollTop <= 0 : ul.scrollTop + this.activeMax >= ul.scrollHeight){
           this.onScrollerOut(null, t);
        }
    },

    
    onScrollerIn : function(e, t){
        var ul = this.ul.dom, top = Ext.fly(t).is('.x-menu-scroller-top');
        if(top ? ul.scrollTop > 0 : ul.scrollTop + this.activeMax < ul.scrollHeight){
            Ext.fly(t).addClass(['x-menu-item-active', 'x-menu-scroller-active']);
        }
    },

    
    onScrollerOut : function(e, t){
        Ext.fly(t).removeClass(['x-menu-item-active', 'x-menu-scroller-active']);
    },

    
    show : function(el, pos, parentMenu){
        if(this.floating){
            this.parentMenu = parentMenu;
            if(!this.el){
                this.render();
                this.doLayout(false, true);
            }
            this.showAt(this.el.getAlignToXY(el, pos || this.defaultAlign, this.defaultOffsets), parentMenu);
        }else{
            Ext.menu.Menu.superclass.show.call(this);
        }
    },

    
    showAt : function(xy, parentMenu){
        if(this.fireEvent('beforeshow', this) !== false){
            this.parentMenu = parentMenu;
            if(!this.el){
                this.render();
            }
            if(this.enableScrolling){
                
                this.el.setXY(xy);
                
                xy[1] = this.constrainScroll(xy[1]);
                xy = [this.el.adjustForConstraints(xy)[0], xy[1]];
            }else{
                
                xy = this.el.adjustForConstraints(xy);
            }
            this.el.setXY(xy);
            this.el.show();
            Ext.menu.Menu.superclass.onShow.call(this);
            if(Ext.isIE){
                
                this.fireEvent('autosize', this);
                if(!Ext.isIE8){
                    this.el.repaint();
                }
            }
            this.hidden = false;
            this.focus();
            this.fireEvent('show', this);
        }
    },

    constrainScroll : function(y){
        var max, full = this.ul.setHeight('auto').getHeight(),
            returnY = y, normalY, parentEl, scrollTop, viewHeight;
        if(this.floating){
            parentEl = Ext.fly(this.el.dom.parentNode);
            scrollTop = parentEl.getScroll().top;
            viewHeight = parentEl.getViewSize().height;
            
            
            normalY = y - scrollTop;
            max = this.maxHeight ? this.maxHeight : viewHeight - normalY;
            if(full > viewHeight) {
                max = viewHeight;
                
                returnY = y - normalY;
            } else if(max < full) {
                returnY = y - (full - max);
                max = full;
            }
        }else{
            max = this.getHeight();
        }
        
        if (this.maxHeight){
            max = Math.min(this.maxHeight, max);
        }
        if(full > max && max > 0){
            this.activeMax = max - this.scrollerHeight * 2 - this.el.getFrameWidth('tb') - Ext.num(this.el.shadowOffset, 0);
            this.ul.setHeight(this.activeMax);
            this.createScrollers();
            this.el.select('.x-menu-scroller').setDisplayed('');
        }else{
            this.ul.setHeight(full);
            this.el.select('.x-menu-scroller').setDisplayed('none');
        }
        this.ul.dom.scrollTop = 0;
        return returnY;
    },

    createScrollers : function(){
        if(!this.scroller){
            this.scroller = {
                pos: 0,
                top: this.el.insertFirst({
                    tag: 'div',
                    cls: 'x-menu-scroller x-menu-scroller-top',
                    html: '&#160;'
                }),
                bottom: this.el.createChild({
                    tag: 'div',
                    cls: 'x-menu-scroller x-menu-scroller-bottom',
                    html: '&#160;'
                })
            };
            this.scroller.top.hover(this.onScrollerIn, this.onScrollerOut, this);
            this.scroller.topRepeater = new Ext.util.ClickRepeater(this.scroller.top, {
                listeners: {
                    click: this.onScroll.createDelegate(this, [null, this.scroller.top], false)
                }
            });
            this.scroller.bottom.hover(this.onScrollerIn, this.onScrollerOut, this);
            this.scroller.bottomRepeater = new Ext.util.ClickRepeater(this.scroller.bottom, {
                listeners: {
                    click: this.onScroll.createDelegate(this, [null, this.scroller.bottom], false)
                }
            });
        }
    },

    onLayout : function(){
        if(this.isVisible()){
            if(this.enableScrolling){
                this.constrainScroll(this.el.getTop());
            }
            if(this.floating){
                this.el.sync();
            }
        }
    },

    focus : function(){
        if(!this.hidden){
            this.doFocus.defer(50, this);
        }
    },

    doFocus : function(){
        if(!this.hidden){
            this.focusEl.focus();
        }
    },

    
    hide : function(deep){
        if (!this.isDestroyed) {
            this.deepHide = deep;
            Ext.menu.Menu.superclass.hide.call(this);
            delete this.deepHide;
        }
    },

    
    onHide : function(){
        Ext.menu.Menu.superclass.onHide.call(this);
        this.deactivateActive();
        if(this.el && this.floating){
            this.el.hide();
        }
        var pm = this.parentMenu;
        if(this.deepHide === true && pm){
            if(pm.floating){
                pm.hide(true);
            }else{
                pm.deactivateActive();
            }
        }
    },

    
    lookupComponent : function(c){
         if(Ext.isString(c)){
            c = (c == 'separator' || c == '-') ? new Ext.menu.Separator() : new Ext.menu.TextItem(c);
             this.applyDefaults(c);
         }else{
            if(Ext.isObject(c)){
                c = this.getMenuItem(c);
            }else if(c.tagName || c.el){ 
                c = new Ext.BoxComponent({
                    el: c
                });
            }
         }
         return c;
    },

    applyDefaults : function(c) {
        if (!Ext.isString(c)) {
            c = Ext.menu.Menu.superclass.applyDefaults.call(this, c);
            var d = this.internalDefaults;
            if(d){
                if(c.events){
                    Ext.applyIf(c.initialConfig, d);
                    Ext.apply(c, d);
                }else{
                    Ext.applyIf(c, d);
                }
            }
        }
        return c;
    },

    
    getMenuItem : function(config) {
        if (!config.isXType) {
            if (!config.xtype && Ext.isBoolean(config.checked)) {
                return new Ext.menu.CheckItem(config);
            }
            return Ext.create(config, this.defaultType);
        }
        return config;
    },

    
    addSeparator : function() {
        return this.add(new Ext.menu.Separator());
    },

    
    addElement : function(el) {
        return this.add(new Ext.menu.BaseItem({
            el: el
        }));
    },

    
    addItem : function(item) {
        return this.add(item);
    },

    
    addMenuItem : function(config) {
        return this.add(this.getMenuItem(config));
    },

    
    addText : function(text){
        return this.add(new Ext.menu.TextItem(text));
    },

    
    onDestroy : function(){
        Ext.EventManager.removeResizeListener(this.hide, this);
        var pm = this.parentMenu;
        if(pm && pm.activeChild == this){
            delete pm.activeChild;
        }
        delete this.parentMenu;
        Ext.menu.Menu.superclass.onDestroy.call(this);
        Ext.menu.MenuMgr.unregister(this);
        if(this.keyNav) {
            this.keyNav.disable();
        }
        var s = this.scroller;
        if(s){
            Ext.destroy(s.topRepeater, s.bottomRepeater, s.top, s.bottom);
        }
        Ext.destroy(
            this.el,
            this.focusEl,
            this.ul
        );
    }
});

Ext.namespace("vh.form");
Ext.namespace("vh.data");
vh.form.ButtonPanel=Ext.extend(Ext.Panel,{
		defaultType:"button",
		height:40,
		width:"100%",
		layout:"hbox",
		initComponent:function(){
			var btns=this.buttons;
			var keys=[];
			var btnObjs=[];
			for(var i=0;i<btns.length;i++){
				var text=btns[i].text;
				if(btns[i].key){
					btns[i].text+="("+btns[i].key.toUpperCase()+")";
				}
				var btn=new Ext.Button(btns[i]);
				btnObjs.push(btn);
				var han=function(){};
				if(btns[i].handler){
					han=btns[i].handler;
				}
				if(btns[i].key){
					var k={key:btns[i].key,alt:true,scope:btn,fn:han};
					keys.push(k);
				}
			}
			new Ext.KeyMap(document,keys);
			this.buttons=btnObjs;
			vh.form.ButtonPanel.superclass.initComponent.call(this);
		}
	}
);
vh.data.Store=function(){
	this.items={};
	this._addSelect=function(item){
		if(item.dataType){}else{
			item.dataType="dict";
		}
		var it=this._createSelectItem(item.id);
		for(var k in item){
			it[k]=item[k];
		}
		this.items[item.id]=it;
		return it;
	}
	this._createSelectItem=function(id){
		return {
			onData:function(data){Ext.getCmp(id).store.loadData(data);},
			onAll:function(){}
		};	
	}
	this._requestListData=function(items){
		var param=[];
		for(var id in items){
			var data=[];
			if(typeof(items[id].dataParam)!="undefined"){
				data=items[id].dataParam;
			}
			var code="false";
			if(typeof(items[id].dataCode)!="undefined"){
				code=items[id].dataCode;
			}
			var name;
			if(items[id].dataType=="dict"){
				name=items[id].dataDict;
			}
			if(items[id].dataType=="sql"){
				name=items[id].dataSql;
			}
			var obj={id:items[id].id,name:name,code:code,type:items[id].dataType,data:data};
			param.push(obj);
		}
		var FORM=this;
		Ext.Ajax.request({
			method:"POST",
			url:"jhtcData.viewhigh?format=json",
			success:function(response){
				FORM._onListDataLoaded(response.responseText);
			},	
			failure:function(response){
				alert("����"+response.responseText);
			},
			params:{data:Ext.util.JSON.encode(param)}
		});
	}
	this._onListDataLoaded=function(str){
		if(str.indexOf("<error>")>0){
			str=str.substr(str.indexOf("<error>")+7,str.indexOf("</error>")-str.indexOf("<error>")-7);
			alert(str);
			return;
		}
		var data=Ext.util.JSON.decode(str);
		for(var i=0;i<data.length;i++){
			if(data[i].error){
				alert(data[i].error);
				return;	
			}
			if(this.items[data[i].id])
				this.items[data[i].id].onData(data[i].data);
		}
		for(var k in this.items){
			this.items[k].onAll();
		}
		this.items={};
		this._fun(this);
	}
	this.load=function(fun){
		if(fun){}else{
			fun=function(){};
		}
		this._fun=fun;
		this._requestListData(this.items);
	}
}
vh.form.postJson=function(action,data,fun){
	Ext.Ajax.request({
		method:"POST",
		url:action+".viewhigh",
		success:function(response){
			var str=response.responseText;
			if(str.indexOf("<error>")>0){
				str=str.substr(str.indexOf("<error>")+7,str.indexOf("</error>")-str.indexOf("<error>")-7);
				alert(str);
				return;
			}
			fun(Ext.util.JSON.decode(str));
		},	
		failure:function(response){
			alert("����"+response.responseText);
		},
		params:{data:Ext.util.JSON.encode(data)}
	});
}
vh.form.postToMDAction=function(action,main,detail,fun){
	var vDom2 = new ActiveXObject("Microsoft.XMLDOM");
	vDom2.async = false;
	vDom2.loadXML("<root><msg/><error/><record><total/></record><activeHead/><thead/><tbody></tbody></root>");
	var body=vDom2.getElementsByTagName("tbody")[0];
	var root=vDom2.documentElement;
	for(var i=0;i<main.length;i++){
		var name="0000000000"+i;
		root.setAttribute("_"+name.substr(name.length-6),main[i]);
	}
	
	for(var r=0;r<detail.length;r++){
		var tr=vDom2.createElement("tr");
		body.appendChild(tr);
		for(var c=0;c<detail[r].length;c++){
			var td=vDom2.createElement("td");
			var text=vDom2.createTextNode(detail[r][c]);
			td.appendChild(text);
			tr.appendChild(td);
		}
	}
	alert(vDom2.xml);
	
	var http=new ActiveXObject("Microsoft.XMLHTTP");
	http.open("POST", action+".viewhigh", false);
	http.send(vDom2.xml);
	var str=http.responseText;
	var isOk=true;
	var msg="";
	var pk="";
	if(str.indexOf("<error>")>0){
		isOk=false;
		msg=str.substring(str.search(/<error>/)+"<error>".length, str.search(/<\/error>/));
	}else{
		if(str.indexOf("<msg>")>0){
			msg=str.substring(str.search(/<msg>/)+"<msg>".length, str.search(/<\/msg>/));
		}
	}
	if(str.indexOf("<mdctn_main_pk>")>0){
		pk=str.substring(str.search(/<mdctn_main_pk>/)+"<mdctn_main_pk>".length, str.search(/<\/mdctn_main_pk>/));
	}
	fun(isOk,msg,pk);
}
vh.form.SelectS=function(initStore,item){
	var isPy=false;
	var comboItem=item;
	if(typeof(comboItem.dataPy)!="undefined"&&comboItem.dataPy==true){
		comboItem.dataParam.push("");
		isPy=true;
	}
	initStore._addSelect(comboItem);
	comboItem.store=new Ext.data.SimpleStore({fields:["code","value"],data:[]});
	comboItem.valueField="code";
	comboItem.displayField="value";
	comboItem.mode="local";
	comboItem.forceSelection=true;
	comboItem.triggerAction ='all';
	comboItem.editable=true;
	if(isPy==true){
		comboItem.editable=true;
		item=new Ext.form.ComboBox(item);
		item.on("keypress",function(obj,evt){
			comboItem.dataParam[comboItem.dataParam.length-1]=obj.getRawValue();
			this._vhRefresh(comboItem.dataParam);
		});
		item.vhStore=new vh.data.Store();
		item.vhRefresh=function(param){
			comboItem.dataParam=param;
			param.push("");
			this._vhRefresh(param);
		}
		item._vhRefresh=function(param){
			this.vhStore._addSelect(comboItem);
			this.vhStore.load();
		}
	}
	return item;	
}
vh.form.SelectPy=function(initStore,item){
	var fields=[];
	for(var i=0;i<item.columnModel.length;i++){
		fields.push(item.columnModel[i].dataIndex);	
	}
	if(item.listWidth){}else{
		item.listWidth="300";
	};
	if(item.listHeight){}else{
		item.listHeight="270";
	};
	var grid=new Ext.grid.GridPanel({
		id:item.id+"_grid",
		width:Number(item.listWidth),
		height:Number(item.listHeight),
		store:new Ext.data.SimpleStore({
			data:[],
			fields:fields
		}),
		colModel:new Ext.grid.ColumnModel({
			defaults: {
				width: 70
			},
			columns:item.columnModel
		}),
		sm: new Ext.grid.RowSelectionModel({singleSelect:true})
	});
	var selectMenu=new Ext.menu.MMenu({plain: true,
            showSeparator: false,
            items:[
            	 new Ext.menu.Adapter(grid, {hideOnClick: false})
            	]
        });
        
	var showFun=function(){
		//alert(1)
		if(this.menu==null){
			this.menu=selectMenu;
		}
		this.menu.show(this.el,"tl-bl?");
	}
	item.xtype="triggerfield";
	item.onSelect=item.dataSelect;
	item.onTriggerClick=showFun;
	item.enableKeyEvents = true;
	item.dataType="sql";
	item.dataParam.push("");
	var field=new Ext.form.TriggerField(item);
	var newItem={};
	for(var k in item)
		newItem[k]=item[k];
	newItem.id=item.id+"_grid";
	
	field.on("focus",showFun)
	field.on("blur",function(evt){
		this.menu.hide();
	});
	grid.on("rowclick",function(grid,rowIndex,e){
		selectMenu.hide();
		var sm=grid.getSelectionModel();
		if(sm.getCount()>0){
			newItem.dataSelect(sm.getSelected());
		}
	});
	grid.on("keydown",function(evt){
		var sm=grid.getSelectionModel();
		if(sm.getCount()==0){
			sm.selectFirstRow();	
		}
		if(evt.getCharCode()==evt.ENTER&&sm.getCount()>0){
			newItem.dataSelect(sm.getSelected());
			selectMenu.hide();
		}else if(evt.getCharCode()==evt.UP){
			sm.selectPrevious(false);
			field.focus();
		}else if(evt.getCharCode()==evt.DOWN){
			sm.selectNext(false);
			field.focus();
		}
	});
	
	field.on("keydown",function(obj,evt){
		var sm=grid.getSelectionModel();
		if(sm.getCount()==0){
			sm.selectFirstRow();	
		}
		if(evt.getCharCode()==evt.ENTER&&sm.getCount()>0){
			newItem.dataSelect(sm.getSelected());
			selectMenu.hide();
		}else if(evt.getCharCode()==evt.UP){
			sm.selectPrevious(false);
			field.focus();
		}else if(evt.getCharCode()==evt.DOWN){
			sm.selectNext(false);
			field.focus();
		}
	});
	field.on("keypress",function(obj,evt){
		newItem.dataParam[newItem.dataParam.length-1]=field.getRawValue();
		this._vhRefresh(newItem.dataParam);
	});
	field.vhStore=new vh.data.Store();
	field.vhRefresh=function(param){
		newItem.dataParam=param;
		param.push("");
		this._vhRefresh(param);
	}
	field._vhRefresh=function(param){
		this.vhStore._addSelect(newItem);
		this.vhStore.load();
	}
	
	return field;	
};
vh.form.FormPanel=Ext.extend(Ext.Panel,{
		formColumns:1,
		fieldHeight:40,
		autoFocus:true,
		initComponent:function(){
			if(typeof(this.initStore)=="undefined"){
				alert("Not provide initStore attribute!");
				return;
			}
			this.layout='table';
			this.padding=10;
			this.vhName2Id={};
			this.vhFirstId=null;
			this.vhFieldNames=[];
			if(this.layoutConfig){
				this.layoutConfig.columns=this.formColumns;
			}else{
				this.layoutConfig={columns:this.formColumns};
			}
			var comboItems=[];
			if(this.items){
				var newItems=[];
				var oldItems=this.items;
				for(var i=0;i<oldItems.length;i++){
					var rowSpan=1,colSpan=1;
					var empty=false;
					if(typeof(oldItems[i].xtype)!="undefined"){
						if(oldItems[i].xtype==""){
							empty=true;
							oldItems[i].name=jhtcGetNextId();
							oldItems[i].xtype="hidden";
						}
					}else{
						oldItems[i].xtype="textfield";
					}
					if(oldItems[i].name){}else{
						alert("Not provide name attribute!");
						return;
					}
					if(this.vhName2Id[oldItems[i].name]){
						alert("Field name '"+oldItems[i].name+"' is repeat");
						return;
					}
					if(oldItems[i].rowspan){
						rowSpan=oldItems[i].rowspan;
					}
					if(oldItems[i].colspan){
						colSpan=oldItems[i].colspan;
					}
					var src={id:jhtcGetNextId()};
					Ext.applyIf(oldItems[i],src);
					if(empty==false){
						this.vhName2Id[oldItems[i].name]=oldItems[i].id;
						if(this.vhFirstId==null)
							this.vhFirstId=oldItems[i].id;
					}
					if(oldItems[i].xtype=="combo"){
						oldItems[i]=vh.form.SelectS(this.initStore,oldItems[i]);
					}
					newItems.push({
						rowspan:rowSpan,
						colspan:colSpan,
						layout:"form",
						labelAlign:"right",
						border:false,
						frame:false,
						height:this.fieldHeight*rowSpan,
						items:[oldItems[i]]
					});
					this.vhFieldNames.push(oldItems[i].name);
				}
				this.items=newItems;
			}
			vh.form.FormPanel.superclass.initComponent.call(this);
		},
		initState:function(){
			vh.form.FormPanel.superclass.initState.call(this);
			if(this.vhFirstId!=null&&this.autoFocus==true){
				Ext.getCmp(this.vhFirstId).focus(true,10);
			}
		},
		getArrayData:function(data){
			if(data==null){
				data=[];	
			}
			for(var i=0;i<this.vhFieldNames.length;i++){
				data.push(Ext.getCmp(this.vhName2Id[this.vhFieldNames[i]]).getValue());	
			}
			return data;
		},
		setArrayData:function(data,start){
			for(var i=start;i<this.vhFieldNames.length;i++){
				Ext.getCmp(this.vhName2Id[this.vhFieldNames[i]]).setValue(data[i]);
			}	
		},
		getData:function(data){
			if(data==null){
				data={};
			}
			for(var k in this.vhName2Id){
				data[k]=Ext.getCmp(this.vhName2Id[k]).getValue();
			}
			return data;
		},
		setData:function(data){
			for(var k in data){
				if(this.vhName2Id[k]){
					Ext.getCmp(this.vhName2Id[k]).setValue(data[k]);
				}
			}
		}
	}
)

vh.form.GridPanel=Ext.extend(Ext.grid.EditorGridPanel,{
		columnModel:[],
		fillRows:16,
		initComponent:function(){
			this.clicksToEdit="1";
			if(typeof(this.initStore)=="undefined"){
				alert("Not provide initStore attribute!");
				return;
			}
			var names=[];
			var recordDef=[];
			for(var i=0;i<this.columnModel.length;i++){
				if(this.columnModel[i].editor){
					var isComp=false;
					var src={id:jhtcGetNextId()};
					Ext.applyIf(this.columnModel[i].editor,src);
					if(this.columnModel[i].editor.xtype=="combo"){
						this.columnModel[i].editor=vh.form.SelectS(this.initStore,this.columnModel[i].editor);
					}
					if(this.columnModel[i].editor.xtype=="py"){
						isComp=true;
						this.columnModel[i].editor=vh.form.SelectPy(this.initStore,this.columnModel[i].editor);
					}
					var field;
					if(isComp==false){
						field=Ext.create(this.columnModel[i].editor,this.columnModel[i].editor.xtype)
					}else
						field=this.columnModel[i].editor;
					this.columnModel[i].editor=new Ext.grid.GridEditor(field);
				}
				var r1={dataIndex:"n"+i,type:"string"};
				Ext.applyIf(this.columnModel[i],r1);
				recordDef.push({name:this.columnModel[i].dataIndex,type:this.columnModel[i].type});
				names.push(this.columnModel[i].dataIndex);
			}
			this.vhRecordDef=recordDef;
			this.vhRecord=new Ext.data.Record.create(recordDef);
			this.cm=new Ext.grid.ColumnModel(this.columnModel);
			this.store=new Ext.data.Store({
				proxy:new Ext.data.MemoryProxy([]),
				reader:new Ext.data.ArrayReader({},this.vhRecord)
			});
			this.store.load();
			vh.form.GridPanel.superclass.initComponent.call(this);
			this.on("afteredit",function(obj){
				if(obj.row>obj.grid.store.getTotalCount()-3){
					obj.grid.store.add(new obj.grid.vhRecord({}));
				}
			});
			var GRID=this;
			this.vhContextMenu=new Ext.menu.Menu({id:"aaaaa",items:[{text:"ɾ��",handler:function(){
				GRID.store.removeAt(GRID.vhSelectRow);
			}}]});
			this.on("rowcontextmenu",function(grid,rowIndex,evt){
				evt.preventDefault();
				grid.vhSelectRow=rowIndex;
				//grid.getSelectionModel().selectRow(rowIndex);
				grid.vhContextMenu.showAt(evt.getXY());
			});
		},
		getData:function(){
			var st=this.getStore();
			var data=[];
			var rs=st.getModifiedRecords();
			var rsdef=this.vhRecordDef;
			for(var i=0;i<rs.length;i++){
				var row=[];
				for(var j=0;j<rsdef.length;j++){
					row.push(rs[i].data[rsdef[j].name]);
				}
				data.push(row);
			}
			return data;
		},
		setData:function(data){
			var st=this.getStore();
			st.removeAll(true);
			var count=this.fillRows-data.length;
			if(count<=0){
				count=3;
			}
			var len=data.length;
			for(var row=0;row<count;row++){
				var r=[];
				for(var i=0;i<this.columnModel.length;i++)
					r.push("");
				data.push(r);
			}
			st.loadData(data);
			for(var i=0;i<len;i++){
				st.modified.push(st.getAt(i));
				st.getAt(i).markDirty();	
			}
		}
	}
);

Ext.onReady(function(){
	Ext.form.TriggerField.override({
	    afterRender : function(){
	        Ext.form.TriggerField.superclass.afterRender.call(this);
	        var y;
	        if(Ext.isIE && !this.hideTrigger && this.el.getY() != (y = this.trigger.getY())){
	            this.el.position();
	            this.el.setY(y);
	        }
	    }
	});
})