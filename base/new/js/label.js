//控钮创建 
global.createButton=function(page,id,parentObj,props){
		var obj=null; 
		id=global.generateCtrlId(id);
		var text=props["text"];
		if(props["accesskey"]){
			text+="(<span style=\"text-decoration:underline;\">"+props["accesskey"].toUpperCase()+"</span>)";
		}
		var oc=""; 
		if(props["onclick"])
			oc=" onclick=\""+props["onclick"]+"\" ";
		var htmlStr="<a id=\""+id+"\" type=\"button\" accesskey=\""+props["accesskey"]+"\" class=\""+global.css.buttonNormal+"\" onselectstart=\"return false\" "+oc+">"+text+"</a>";
		parentObj.innerHTML=htmlStr;
		obj=page.ctrl(id);
		global._initButtonMethod(page,obj);
		return obj;
	};
//控钮初始化
global._initButtonMethod=function(page,ctrl){
		ctrl.vhPage=page;
		ctrl.vhSetText=function(text){global_ButtonSetText(this,text);};
		ctrl.vhSetDisabled=function(dis){global_ButtonSetDisabled(this,dis);};
		ctrl.vhIsDisabled=function(){return global_ButtonIsDisabled(this);};
		ctrl.onmouseover=function(){global_ButtonOnMouseOver(this);};
	  ctrl.onmouseout=function(){global_ButtonOnMouseOut(this)};
	  ctrl.onmousedown=function(){global_ButtonOnMouseDown(this);};
	};
function global_ButtonSetText(ctrl,text){
		if(ctrl.accessKey!=""){
			text+="(<span style=\"text-decoration:underline;\">"+ctrl.accessKey+"</span>)";
		}
		ctrl.innerHTML=text;
	};
function global_ButtonSetDisabled(ctrl,dis){
		ctrl.disabled=dis;
		ctrl.className=(dis==true?global.css.buttonDisabled:global.css.buttonNormal);
	};
function global_ButtonIsDisabled(ctrl){
		return ctrl.className==global.css.buttonDisabled;
	};
function global_ButtonOnMouseOver(ctrl){
    if(ctrl.vhIsDisabled())
    	return;
    ctrl.className="buttonMouseOver";
};
function global_ButtonOnMouseOut(ctrl){
    if(ctrl.vhIsDisabled())
    	return;
    ctrl.className="buttonNormal";
};
function global_ButtonOnMouseDown(ctrl){
    if(ctrl.vhIsDisabled())
    	return;
    ctrl.className="buttonMouseDown";
};
//标签创建
global.createLabel=function(page,id,parentObj,props){
		var obj=null;
		id=global.generateCtrlId(id);
		var colon=":&nbsp;";
		if(props["colon"]){
			colon=props["colon"];
		}
		var htmlStr="<table id=\""+id+"\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td nowrap id=\""+id+"\" class=\""+global.css.labelNormal+"\">"+props["text"]+"</td><td>"+colon+"</td></tr></table>";
		parentObj.innerHTML=htmlStr;
		obj=page.ctrl(id);
		global._initLabelMethod(page,obj);
		return obj;
	}
//标签初始化
global._initLabelMethod=function(page,ctrl){
		ctrl.vhPage=page;
		ctrl.vhSetText=function(text){global_LabelSetText(this,text);};
		ctrl.vhGetText=function(){return global_LabelGetText(this)};
		ctrl.vhSetError=function(isError){global_LabelSetStatus(this,isError);};
		ctrl.vhIsError=function(){return global_LabelGetStatus(this);};
	};
function global_LabelSetText(ctrl,text){
		ctrl.vhPage.ctrl(ctrl.id+"TextTd").innerText=text;
	};
function global_LabelGetText(ctrl){
		return 	ctrl.vhPage.ctrl(ctrl.id+"TextTd").innerText;
	};
function global_LabelSetStatus(ctrl,isError){
		if(isError){
			ctrl.vhPage.ctrl(ctrl.id+"TextTd").className=global.css.labelError;
		}else{
			ctrl.vhPage.ctrl(ctrl.id+"TextTd").className=global.css.labelNormal;
		}
	};
function global_LabelGetStatus(ctrl){
		return ctrl.vhPage.ctrl(ctrl.id+"TextTd").className==global.css.labelError;
	};