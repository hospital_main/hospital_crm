global.createCombobox=function(page,id,parentObj,props){
		var obj=null;
		id=global.generateCtrlId(id);
		parentObj.innerHTML=global_BaseComboBoxCreateHtml("combobox",id,props,"images/selectBtn.png",true);
		obj=page.ctrl(id);
		global._initComboboxMethod(page,obj);
		return obj;
	};
global._initComboboxMethod=function(page,ctrl){
		global._initSelectMethod(page,ctrl);
	};