global.createPassword=function(page,id,parentObj,props){
		var obj=null;
		id=global.generateCtrlId(id);
		var htmlStr=global_BaseInputCreateHtml("password",id,props);
		parentObj.innerHTML=htmlStr;
		obj=page.ctrl(id);
		global._initPasswordMethod(page,obj);
		return obj;
	};
global._initPasswordMethod=function(page,ctrl){
		global_CtrlInitMethod(page,ctrl);
		ctrl.vhCheckValue=function(){return global_PasswordCheckValue(this);};
		ctrl.vhGetLimit=function(){return this._vhLimit;};
		ctrl.vhSetLimit=function(limit){this._vhLimit=limit;};
		ctrl.vhSetValue=function(initValue){this.value=initValue;};
		ctrl.vhGetValue=function(){return this.value;};
		ctrl.vhSetReadOnly=function(readOnly){global_PasswordSetReadonly(this,readOnly)};
	};
function global_PasswordSetReadonly(ctrl,readOnly){
		global_CtrlSetReadonly(ctrl,readOnly);
		ctrl.readOnly=readOnly;
	};
function global_PasswordCheckValue(ctrl){
		///implements
	};