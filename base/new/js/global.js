global._initArray=[];
global._pushInitFun=function(fun){
		global._initArray.push(fun);
	};
global._init=function(){
		for(var i=0;i<global._initArray.length;i++){
			global._initArray[i]();
		};
	};
global._initPageMethod=function(page){
		page.topPage=window;
		page.ctrl=function(id){return this.document.getElementById(id);};
		page.getParameter=function(key){
			if(this._vhParamMap){;}else{
				this._vhParamMap=global.decodeHrefParam(this.document.location.href);
			};
			if(this._vhParamMap[key]){
				return this._vhParamMap[key];
			};
			return "";
		};
	};
global.getStyleFilePath=function(file){
		var p=global.getPathPrefix()+"/base/new/style/"+global.getStyleName()+"/"+file;
		return p.replace(/\/\//g,"/");
	};
global.getFilePath=function(file){
		var p=global.getPathPrefix()+"/"+file;
		return p.replace(/\/\//g,"/");
	};
//取得环境变量(变量键)
__envionmentValue=new Object();
global.getEnvValue=function(n){
		if(__envionmentValue[n])
			return __envionmentValue[n];
		else
			return null;
	};
//设置环境变量(变量键,变量值)
global.setEnvValue=function(n,v){
		__envionmentValue[n]=v;
	};
//生成唯一ID
global.generateId=function(){
		var d=new Date();
		var s="";
		for(var i=0;i<6;i++)
			s+=parseInt(Math.random()*10000);
		return "guid"+d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds()+s;
	};
global.generateCtrlId=function(id){
		if(id){
			return id;
		}else{
			return global.generateId();
		}
	};
global.str2bool=function(v){
		if(v){
			if(v=="true"||v==true||v=="1"||v=="yes"){
				return true;
			}
		}
		return false;
	};
global.isNumber=function(v){
		return /^\-?([\d]*)\.?([\d]+)$/.test(v);
	}
global.genHtmlAttrStr=function(name,value){
		return " "+name+"=\""+value+"\" ";
	};
global.sendRequest=function(action,data,callBackObj,param){
		var cbo=callBackObj;
		var req=new ActiveXObject("Microsoft.XMLHTTP");
		if(param)
			req.open("POST",action+".viewhigh?"+param,true);
		else
			req.open("POST",action+".viewhigh",true);
		req.onreadystatechange=function(){
			if(req.readyState==4){
				if (req.status>=400){
					if(cbo.vhOnError){
						cbo.onError(req.statusText,req.responseText);
					}else{
						alert("请求失败：\n"+req.statusText+"\n"+req.responseText);
					}
					return;
              			};
				if(req.status!=200){
			  		return ;
			  	};
			  	cbo.vhOnDataLoaded(req.responseText,req.responseXML);
			};
		};
		req.send(data);
	};
global.encodeUrlParam=function(param){
		if(param==null){
			return "";	
		}
		var str="";
		for(var k in param){
			str+=k+"="+escape(param[k]);
		}
	};
global.decodeHrefParam=function(href){
		if(href.indexOf("?")>0){
			return global.decodeUrlParam(href.substr(href.indexOf("?")+1));
		}else{
			return {};
		};
	};
global.decodeUrlParam=function(str){
		var pstr=str.split("&"),ps,res={};
		for(var i=0;i<pstr.length;i++){
			ps=pstr[i].split("=");
			if(ps.length<2){
				continue;
			}
			res[ps[0]]=unescape(ps[1]);
		}
		return res;
	};
global.decodeXmlStr=function(s){
	s=s.replace(/&quot;/g,"\"");
	s=s.replace(/&amp;/g,"&");
	s=s.replace(/&gt;/g,">");
	s=s.replace(/&lt;/g,"<");
	return s;
}
global.encodeXmlStr=function(s){
	s=s.replace(/&/g,"&amp;");
	s=s.replace(/>/g,"&gt;");
	s=s.replace(/</g,"&lt;");
	return s;
}
//取得路径前缀
//global.getPathPrefix()
global.vhBuildTdData=function(value){
		return "<td>"+global.encodeXmlStr(value)+"</td>";
	};
global.vhBuildNamedData=function(name,value){
		return "<"+name+">"+global.encodeXmlStr(value)+"</"+name+">";
	};
global.vhBuildTrData=function(tdArray,tbodyArray){
		var str="<tr>";
		for(var i=0;i<valueArray.length;i++){
			str+=valueArray[i];
		}
		if(tbodyArray){
			if(tbodyArray.length>0){
				for(var i=0;i<tbodyArray.length;i++){
					str+=tbodyArray[i];
				}
			}
		}
		return str+="</tr>";
	};
global.vhBuildTbodyData=function(valueArray){
		var str="<tbody>";
		for(var i=0;i<valueArray.length;i++){
			str+=valueArray[i];
		}
		return str+="</tbody>";
	};
global.vhBuildTableData=function(valueArray){
		var str="<?xml version=\"1.0\" encoding=\"GBK\" ?><root>";
		for(var i=0;i<valueArray.length;i++){
			str+=valueArray[i];
		}
		return str+="</root>";
	};
global.vhBuildSubmitDataByObjs=function(){
		var str="";
		for(var i=0;arguments.length;i++){
			str+=arguments[i].vhBuildSubmitData();
		}
		return "<?xml version=\"1.0\" encoding=\"GBK\" ?><root>"+str+"</root>";
	};
/*
parseCallback(){
	thead/tr/td
	thead/tr/td
	tbody/tr
	tbody/tr/td
	tbody/tr/tbody/tr
	this.getMethod=function(){
		
	}
}

*/
global.vhParseStrData=function(str){
		var dom=new ActiveXObject("Microsoft.XMLDOM");
		dom.loadXML(str);
		global.vhParseXmlData(dom);
	};

//CTRL 基本方法初始化
global_CtrlInitMethod=function(page,ctrl){
		ctrl.vhPage=page;
		ctrl.vhSetFocus=function(){global_CtrlSetFocus(this,true);};
		ctrl.vhGetValue=function(){return this._vhValue;};
		ctrl.vhSetValue=function(value){this._vhValue=value;};
		ctrl.vhCheckValue=function(){return true;};
		ctrl.vhSetRequired=function(required){global_CtrlSetRequired(this,required);};
		ctrl.vhIsRequired=function(){return global.str2bool(this._vhRequired);};
		ctrl.vhSetReadOnly=function(readOnly){global_CtrlSetReadonly(this,readOnly);};
		ctrl.vhIsReadOnly=function(){return global.str2bool(this._vhReadOnly);};
		ctrl.vhGetWidth=function(){return this.style.offsetWidth;};
		ctrl.vhSetWidth=function(w){this.style.width;};
		ctrl.vhGetHeight=function(){return this.style.offsetHeight;};
		ctrl.vhSetHeight=function(h){};
		ctrl.vhIsSingleValue=function(){return true;};
		ctrl.vhBuildSubmitData=function(){};
		ctrl.vhParseLoadedData=function(obj){};
		
		ctrl._vhSetError=function(errMsg){global_CtrlSetError(this,errMsg);};
		ctrl._vhOnSetErrorStyle=function(clazz){this.className=clazz;};
		ctrl._vhOnSetFocusStyle=function(clazz){this.className=clazz;};
		ctrl._vhOnSetNormalStyle=function(clazz){this.className=clazz;};
		ctrl._vhIsHasFocus=function(){return this._vhHasFocus==global.str2bool(true)};
		ctrl._vhIsHasError=function(){return this._vhHasError==global.str2bool(true)};
		ctrl._vhOnLossFocus=function(){};
		ctrl._vhOnMoveNextBefore=function(){};
	};
function global_addClickDocListener(page,fun){
		page.document.attachEvent("onmouseup",fun);
	};
function global_removeClickDocListener(page,fun){
		page.document.detachEvent("onmouseup",fun);
	};
function global_CtrlSetError(ctrl,errMsg){
		if(ctrl._vhLabelId){
			ctrl.vhPage.ctrl(ctrl._vhLabelId).vhSetError(errMsg!=null);
		}
		if(ctrl._vhHasFocus=="true")
			ctrl._vhOnSetFocusStyle(ctrl._vhFocusClass);
		else if(errMsg!=null){
			ctrl._vhOnSetErrorStyle(ctrl._vhInvalidClass);
			ctrl._vhHasError=true;
		}else{
			ctrl._vhOnSetNormalStyle(ctrl._vhNormalClass);
			ctrl._vhHasError=false;
		}
	};
function global_CtrlSetFocus(ctrl,isFocus){
		ctrl._vhHasFocus=isFocus;
		if(isFocus==true){
			ctrl._vhOnSetFocusStyle(ctrl._vhFocusClass);
		}else{
			ctrl._vhOnSetNormalStyle(ctrl._vhNormalClass);
		}
	};
function global_CtrlSetReadonly(ctrl,readOnly){
		ctrl._vhReadOnly=readOnly;
		if(readOnly)
			ctrl._vhOnSetNormalStyle(ctrl._vhReadOnlyClass);
		else if(ctrl._vhHasError==true)
			ctrl._vhOnSetErrorStyle(ctrl._vhInvalidClass);
		else if(ctrl._vhRequired==true)
			ctrl._vhOnSetNormalStyle(ctrl._vhRequiredClass);
		else
			ctrl._vhOnSetNormalStyle(ctrl._vhNormalClass);
	};
function global_CtrlSetRequired(ctrl,req){
		ctrl._vhRequired=req;
		if(ctrl._vhHasFocus=="true")
			ctrl._vhOnSetFocusStyle(ctrl._vhFocusClass);
		else if(ctrl._vhHasError==true){
			ctrl._vhOnSetErrorStyle(ctrl._vhInvalidClass);
		}else if(req==true){
			ctrl._vhOnSetNormalStyle(ctrl._vhRequiredClass);
		}else
			ctrl._vhOnSetNormalStyle(ctrl._vhNormalClass);
	};

function global_CtrlGenerateCtrlWidth(props){
		if(props["width"])
			return props["width"];
		return "120";
	};
function global_CtrlGenerateInputCtrlCss(uiClass,props){
		return global_CtrlGenerateCssConf(uiClass,props,global.css.textNormal,global.css.textRequired,global.css.textReadonly,global.css.textInvalid,global.css.textFocus);
	};
function global_CtlGenerateSelectCtrlCss(uiClass,props){
		return global_CtrlGenerateCssConf(uiClass,props,global.css.selectNormal,global.css.selectRequired,global.css.selectReadonly,global.css.selectInvalid,global.css.selectFocus);
	};
function global_CtrlGenerateCssConf(uiClass,props,normal,required,readonly,invalid,focus){
		var css=normal;
		var app="";
		if(props["required"]){
			if(global.str2bool(props["required"])==true){
				css=required;
				app+=" _vhRequired=\"true\" "
			}else
				app+=" _vhRequired=\"false\" "
		}else
			app+=" _vhRequired=\"false\" ";
		if(props["readonly"]){
			if(global.str2bool(props["readOnly"])==true){
				css=readonly;
				app+=" _vhReadOnly=\"true\" ";
			}else
				app+=" _vhReadOnly=\"false\" ";
		}else
			app+=" _vhReadOnly=\"false\" ";
		return [css," class=\""+css+app+"\" _vhCtrlClass=\""+uiClass+"\" _vhNormalClass=\""+normal+"\" _vhRequiredClass=\""+required+"\" _vhReadOnlyClass=\""+readonly+"\" _vhInvalidClass=\""+invalid+"\" _vhFocusClass=\""+focus+"\" _vhHasFocus=\"false\" _vhHasError=\"false\" "];
	};
var global_CtrlGenerateZIndexSeq=100;
function global_CtrlGenerateZIndex(){
		return "z-index:"+(global_CtrlGenerateZIndexSeq++)+";";
	};
