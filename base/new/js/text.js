global.createText=function(page,id,parentObj,props){
		var obj=null;
		id=global.generateCtrlId(id);
		var htmlStr=global_BaseInputCreateHtml("text",id,props);
		parentObj.innerHTML=htmlStr;
		obj=page.ctrl(id);
		global._initTextMethod(page,obj);
		return obj;
	};
global._initTextMethod=function(page,ctrl){
		global_CtrlInitMethod(page,ctrl);
		ctrl.vhCheckValue=function(){return global_TextCheckValue(this);};
		ctrl.vhGetMask=function(){return this._vhMask;};
		ctrl.vhSetMask=function(mask){this._vhMask=mask;};
		ctrl.vhGetLimit=function(){return this._vhLimit;};
		ctrl.vhSetLimit=function(limit){this._vhLimit=limit;};
		ctrl.vhSetValue=function(initValue){this.value=initValue;};
		ctrl.vhGetValue=function(){return this.value;};
		ctrl.vhSetReadOnly=function(readOnly){global_TextSetReadonly(this,readOnly);};
		ctrl.vhBuildSubmitData=function(){return global.vhBuildTdData(this.value);};
	};
function global_TextSetReadonly(ctrl,readOnly){
		global_CtrlSetReadonly(ctrl,readOnly);
		ctrl.readOnly=readOnly;
	};
function global_TextCheckValue(ctrl){
		var vh_value=ctrl.vhGetValue();
		var vh_mask=ctrl.vhGetMask();
		
		var required=ctrl.vhIsRequired();
		
		var limit_value=ctrl.vhGetLimit();
		
		if((vh_value.length==0 || vh_value.trim()=="") && required){
			ctrl._vhSetError(ctrl.id+"不能为空值!");
			return false;
		}
		
		if(mask!=null && typeof(mask)!="undefined"){
			if(vh_value.search(vh_mask)==-1){
				ctrl._vhSetError("数据格式不正确!");
				return false;
			}
		}
		
		if(vh_value.length>parseInt(limit_value)){
			ctrl._vhSetError("文本输入过长,最多为"+limit_value+"个字!");
			return false;
		}
		return true;
	};
function global_BaseInputCreateHtml(uiClass,id,props,ext){
		var width=global_CtrlGenerateCtrlWidth(props);
		var clazz=global_CtrlGenerateInputCtrlCss(uiClass,props);
		var size="";
		if(props["limit"])
			size=" maxlength=\""+props["limit"]+"\" ";
		if(ext)
			size+=ext;
		var htmlStr="<input id=\""+id+"\" type=\"text\" "+clazz[1]+" width=\""+width+"\" "+size+"/>";
	};