global.msgBoxStyle={};
global.msgBoxStyle.OK=0;
global.msgBoxStyle.YESNO=1;
global.msgBoxStyle.YESNOCANCEL=2;
global.msgBoxStyle.INFORMATION=0;
global.msgBoxStyle.QUESTION=1;
global.msgBoxStyle.ERROR=2;
global.showMessageBox=function(page,title,msg,btnStyle,iconStyle,cb){
	if(btnStyle==null){
		btnStyle=0;
	}
	if(iconStyle==null){
		iconStyle=0;
	}
	if(global._msgBoxStyleicon==null){
		global._msgBoxStyleicon=[];
		global._msgBoxStyleicon[0]="<img src=\""+global.getStyleFilePath("images/iconInfo.gif")+"\">"
		global._msgBoxStyleicon[1]="<img src=\""+global.getStyleFilePath("images/iconQuestion.gif")+"\">"
		global._msgBoxStyleicon[2]="<img src=\""+global.getStyleFilePath("images/iconError.gif")+"\">"
	 };
	var id=global.generateCtrlId(null);
	var createDIV = page.document.createElement("<div id=\""+id+"\" style=\"position:absolute;top:0;left:0;width:100%;height:100%;align:center;"+global_CtrlGenerateZIndex()+"\"></div>");  
	htmlStr="<table height=\"100%\" width=\"100%\"><tr><td align=\"center\" valign=\"middle\"><div class=\"dialogFrame\"><table align=\"center\" width=\"100\" border=\"0\" cellspacing=\"2\">"
	htmlStr+="<tr><td colspan='2' class=\"dialogTitle\">title</td></tr><tr align=\"center\"> <td>&nbsp;"+global._msgBoxStyleicon[iconStyle]+"</td><td>"+msg+"</td></tr>";
	htmlStr+="<tr><td colspan='2' align=\"center\"><table id=\""+id+"Btns\"><tr>"+global._msgBoxStyleBtn[btnStyle].replace(/LCID/g,id)+"</tr></table></td></tr></table></div></td></tr></table>";
	createDIV.innerHTML=htmlStr;
	createDIV.vhcb=cb;
	page.document.body.appendChild(createDIV);
	var tds=page.ctrl(id+"Btns").getElementsByTagName("td");
	for(var i=0;i<tds.length;i++){
		var prop={};
		prop["text"]=tds[i].innerText;
		prop["onclick"]="global._delMsgBox(page,'"+id+"',"+i+")";
		global.createButton(page,null,tds[i],prop);
	}
};
global._delMsgBox=function(page,id,number){
	var obj=page.ctrl(id);
  obj.parentNode.removeChild(obj);
  obj.vhcb(number);
}
global._msgBoxStyleBtn=[];
global._msgBoxStyleBtn[0]="<td>确定</td>";
global._msgBoxStyleBtn[1]="<td>是</td><td>否</td>";
global._msgBoxStyleBtn[2]="<td>是</td><td>否</td><td>撤销</td>" ;
global._msgBoxStyleicon =null;