global.createTextArea=function(page,id,parentObj,props){
		var obj=null;
		id=global.generateCtrlId(id);
		var row="";
		if (props["rows"])
		row=props["rows"];
		var col="";
		if (props["cols"])
		col=props["cols"];
		parentObj.innerHTML="<textarea id=\""+id+"\" "+row+col+"></textarea>";
		obj=page.ctrl(id);
		global._initTextAreaMethod(page,obj);
		return obj;
	
	};
global._initTextAreaMethod=function(page,ctrl){
		global_CtrlInitMethod(page,ctrl);
		ctrl.vhCheckValue=function(){return global_TextAreaCheckValue(this);};
		ctrl.vhGetLimit=function(){return this._vhLimit;};
		ctrl.vhSetLimit=function(limit){this._vhLimit=limit;};
		ctrl.vhSetValue=function(initValue){this.value=initValue;};
		ctrl.vhGetValue=function(){return this.value;};
		ctrl.vhSetReadOnly=function(readOnly){global_TextAreaSetReadonly(this,readOnly);};
		ctrl.vhBuildSubmitData=function(){return global.vhBuildTdData(this.value);};
		
		};
function global_TextAreaSetReadonly(ctrl,readOnly){
		global_CtrlSetReadonly(ctrl,readOnly);
		ctrl.readOnly=readOnly;
	};



function global_TextAreaCheckValue(ctrl){
		var vh_value=ctrl.vhGetValue();
	
		
		var required=ctrl.vhIsRequired();
		
		var limit_value=ctrl.vhGetLimit();
		
	
	};
