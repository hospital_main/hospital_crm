global.createCheckbox=function(page,id,parentObj,props){
		var obj=null;
		id=global.generateCtrlId(id);
		var text=""
		if(props["text"])
			text=props["text"];
		var cli="";
	 	if(props["onclick"])
			cli=" onclick=\""+props["onclick"]+"\" ";
		parentObj.innerHTML="<table id=\""+id+"\"><tr><td><input type=\"checkbox\" id=\""+id+"_box_\" "+cli+"/></td><td nowrap=\"true\">"+text+"</td><tr/></table>";
		obj=page.ctrl(id);
		global._initCheckboxMethod(page,obj);
		return obj;
	};     
global._initCheckboxMethod=function(page,ctrl){
		global_CtrlInitMethod(page,ctrl);
		ctrl.vhGetValue=function(){return global_CheckboxGetValue();};
		ctrl.vhSetValue=function(value){global_CheckboxSetValue(this,value);};
		ctrl.vhSetReadOnly=function(readOnly){global_CheckboxSetReadonly(this,readOnly);};
	};
function global_CheckboxGetValue(ctrl){
		return ctrl.vhPage.ctrl(ctrl.id+"_box_").checked==true?1:0;
	};
function global_CheckboxSetValue(ctrl,value){
		ctrl.vhPage.ctrl(ctrl.id+"_box_").checked=global.str2bool(value);
	};
function global_CheckboxSetReadonly(ctrl,ro){
		global_CtrlSetReadonly(ctrl,ro);
		ctrl.vhPage.ctrl(ctrl.id+"_box_").disabled=ctrl._vhReadOnly;
	}