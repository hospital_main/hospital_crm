<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/root">
		<root>
				<xsl:copy-of select="top/node()[starts-with(name(),'maintitle')]"/>
				<xsl:copy-of select="top/subtitle"/>
				<xsl:copy-of select="colgroup"/>
				<xsl:copy-of select="thead"/>
		</root>
	</xsl:template>
</xsl:stylesheet>
