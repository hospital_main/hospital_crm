<?xml version="1.0" encoding="gb2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="gb2312"/>
	<xsl:decimal-format NaN=" "/>
	<xsl:variable name="colNum" select="count(//tbody/tr[1]/td)"/>
	<xsl:template match="/root">
		<xsl:for-each select="/root/pagegroup/page">
			<hr class="noprint" width="100%"/>
			<div style="border:0 groove gray;overflow:hidden;width:{/root/@pageWidth};height:{/root/@pageHeight};">
				<table width="{/root/@pageWidth}" height="{/root/@pageHeight}" border="0" cellpadding="0" cellspacing="0">
					<tr height="{/root/@topMargin}" valign="top">
						<td rowspan="3" width="{/root/@leftMargin}">��</td>
						<td/>
						<td rowspan="3">��</td>
					</tr>
					<tr valign="top">
						<td align="left">
							<table border="0" width="{/root/@pageWidth - /root/@leftMargin - /root/@rightMargin}" height="{/root/@pageHeight - /root/@topMargin - /root/@bottomMargin}">
								<tr valign="top">
									<td>
										<xsl:call-template name="top">
											<xsl:with-param name="page-num" select="concat(position(),'/',last())"/>
										</xsl:call-template>
									</td>
								</tr>
								<tr valign="top">
									<td>
										<xsl:call-template name="page">
											<xsl:with-param name="rowB" select="@rowB"/>
											<xsl:with-param name="rowE" select="@rowE"/>
											<xsl:with-param name="colB" select="@colB"/>
											<xsl:with-param name="colE" select="@colE"/>
										</xsl:call-template>
									</td>
								</tr>
								<tr height="100%">
									<td valign="top">
										<xsl:call-template name="bottom"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style="font-size:1">��</td>
					</tr>
				</table>
			</div>
			<xsl:if test="position()!=last()">
				<div style="PAGE-BREAK-AFTER:always;font-size:1">��</div>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="top">
		<xsl:param name="page-num"/>
		<xsl:if test="count(/root/top) > 0">
			<table width="100%">
				<xsl:for-each select="/root/top/title | /root/top/maintitle">
					<tr height="{/root/@titleHeight}">
						<td colspan="{$colNum}" class="prnTitle" style="{@style}">
							<xsl:value-of select="."/>
						</td>
					</tr>
				</xsl:for-each>
				<xsl:for-each select="/root/top/maintitle2">
					<tr height="{/root/@titleHeight}">
						<td colspan="{$colNum}" class="prnTitle2" style="{@style}">
							<xsl:value-of select="."/>
						</td>
					</tr>
				</xsl:for-each>
				<xsl:for-each select="/root/top/maintitle3">
					<tr height="{/root/@titleHeight}">
						<td colspan="{$colNum}" class="prnTitle3" style="{@style}">
							<xsl:value-of select="."/>
						</td>
					</tr>
				</xsl:for-each>
				<xsl:for-each select="/root/top/maintitle4">
					<tr height="{/root/@titleHeight}">
						<td colspan="{$colNum}" class="prnTitle4" style="{@style}">
							<xsl:value-of select="."/>
						</td>
					</tr>
				</xsl:for-each>
				<xsl:for-each select="/root/top/subtitle">
					<tr height="{/root/@subTitleHeight}">
						<td colspan="{$colNum}" class="prnSubTitle" style="{@style}">
							<xsl:value-of select="."/>
						</td>
					</tr>
				</xsl:for-each>
				<xsl:for-each select="/root/top/tr">
					<xsl:call-template name="message">
						<xsl:with-param name="position" select="position()"/>
						<xsl:with-param name="tdNum" select="count(td)"/>
						<xsl:with-param name="page-num" select="$page-num"/>
					</xsl:call-template>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template name="bottom">
		<br style="font-size:3px"/>
		<xsl:if test="count(/root/bottom) > 0">
			<table width="100%">
				<xsl:for-each select="/root/bottom/tr">
					<xsl:call-template name="message">
						<xsl:with-param name="position" select="position()"/>
						<xsl:with-param name="tdNum" select="count(td)"/>
					</xsl:call-template>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template name="message">
		<xsl:param name="position"/>
		<xsl:param name="tdNum"/>
		<xsl:param name="page-num"/>
		<tr style="{@style}">
			<xsl:for-each select="td">
				<td class="prnAddInfo" nowrap="nowrap" colspan="{@colspan}" rowspan="{@rowspan}">
					<xsl:if test="string-length(@width) = 0">
						<xsl:attribute name="width"><xsl:value-of select="100 div $tdNum"/>%</xsl:attribute>
					</xsl:if>
					<xsl:if test="@width != ''">
						<xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="position()=1">
							<xsl:attribute name="style"><xsl:value-of select="@style"/></xsl:attribute>
						</xsl:when>
						<xsl:when test="position()=last()">
							<xsl:attribute name="style">text-align:right;<xsl:value-of select="@style"/></xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="style"><xsl:value-of select="@style"/></xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test="contains(., '[ҳ��]/[��ҳ��]')">
							<xsl:value-of select="concat(substring-before(., '[ҳ��]/[��ҳ��]'), $page-num)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="." disable-output-escaping="yes"/>
						</xsl:otherwise>
					</xsl:choose>
				</td>
			</xsl:for-each>
		</tr>
	</xsl:template>
	<xsl:template name="page">
		<xsl:param name="rowB"/>
		<xsl:param name="rowE"/>
		<xsl:param name="colB"/>
		<xsl:param name="colE"/>
		<table class="printTable" bgColor="white" border="0" style="font-size:{13 * /root/@fontScale}">
			<colgroup>
				<xsl:for-each select="//colgroup/col">
					<xsl:if test="(position() &lt;= /root/@fixCol) or (position() &gt;= $colB and position() &lt;= $colE)">
						<col width="{@width}"/>
					</xsl:if>
				</xsl:for-each>
			</colgroup>
			<thead>
				<xsl:for-each select="//thead/tr">
					<tr class="mainHead" nowrap="true" height="{@height}" style="font-size:{15 * /root/@fontScale}">
						<xsl:for-each select="th">
							<xsl:if test="(position() &lt;= /root/@fixCol) or (position() &gt;= $colB and position() &lt;= $colE)">
								<th class="{@class}" rowspan="{@rowspan}" colspan="{@colspan}" style="{@style}">
									<xsl:value-of select="." disable-output-escaping="yes"/>
								</th>
							</xsl:if>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</thead>
			<tbody>
				<xsl:for-each select="//tbody/tr">
					<xsl:variable name="unitHeight" select="@height"/>
					<xsl:if test="position() &gt;= $rowB and position() &lt;= $rowE">
						<tr height="{$unitHeight * 4}">
							<xsl:for-each select="td/table">
								<xsl:if test="(position() &lt;= /root/@fixCol) or (position() &gt;= $colB and position() &lt;= $colE)">
									<td rowspan="{@rowspan}" colspan="{@colspan}" style="{@style}" class="{@class}">
										<xsl:attribute name="style">
                        overflow:hidden;
                        width:
                          <xsl:if test="(@colspan = 1 or @colspan = 0 or @colspan = '')"><xsl:call-template name="divWidth"><xsl:with-param name="indexTD" select="position()"/></xsl:call-template></xsl:if></xsl:attribute>
										<table>
											<xsl:for-each select="tr/td">
												<tr>
													<td align="center" height="{$unitHeight}">
														<xsl:choose>
															<xsl:when test="img">
																<img src="{img/@src}"/>
															</xsl:when>
															<xsl:when test="position() = 2">
																<xsl:if test="text() != ''">
																<xsl:attribute name="class">bg1</xsl:attribute>
																<xsl:attribute name="nowrap">nowrap</xsl:attribute>
																<xsl:value-of select="." disable-output-escaping="yes"/>
																</xsl:if>
															</xsl:when>
															<xsl:when test="position() = 3">
																<xsl:if test="../../tr[2]/td != ''">
																<xsl:attribute name="class">bg2</xsl:attribute>
																<xsl:value-of select="." disable-output-escaping="yes"/>
																<xsl:attribute name="nowrap">nowrap</xsl:attribute>
																</xsl:if>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="." disable-output-escaping="yes"/>
															</xsl:otherwise>
														</xsl:choose>
													</td>
												</tr>
											</xsl:for-each>
										</table>
									</td>
								</xsl:if>
							</xsl:for-each>
						</tr>
					</xsl:if>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
	<xsl:template name="cutStr">
		<xsl:param name="nodeTD"/>
		<xsl:param name="indexTD"/>
		<xsl:value-of select="substring($nodeTD, 1, floor(number(//colgroup/col[$indexTD]/@width) div 12 div number(/root/@fontScale) * number(/root/@colScale)))" disable-output-escaping="yes"/>
	</xsl:template>
	<xsl:template name="divWidth">
		<xsl:param name="indexTD"/>
		<xsl:value-of select="//colgroup/col[$indexTD]/@width"/>
	</xsl:template>
</xsl:stylesheet>
