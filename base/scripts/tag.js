var _tabpage_list=new Array();
var tableCtn;

function getValidStr(str) {
	str+="";
	if (str=="undefined" || str=="null")
		return "";
	else
		return str;
}

function compareText(str1, str2){
	str1=getValidStr(str1);
	str2=getValidStr(str2);
	if (str1==str2) return true;
	if (str1=="" || str2=="") return false;
	return (str1.toLowerCase()==str2.toLowerCase());
}

function initElements(element){
	if (compareText(element.getAttribute("attrib"), "tabpage")){
		_tabpage_list[_tabpage_list.length]=element;
	}
	else{
		if (!initElement(element)) return;
	}

	for (var i=0; i<element.children.length; i++){
		initElements(element.children[i]);
	}
}

function _tabpage_onclick(){
	_setActiveTab(event.srcElement.tab);
}

function _tabpage_onmouseover(){
	event.srcElement.style.textDecorationUnderline=true;
}

function _tabpage_onmouseout(){
	event.srcElement.style.textDecorationUnderline=false;
} 

function initTabPages(){
	for (var i=0; i<_tabpage_list.length; i++){
		initElement(_tabpage_list[i]);
	}
}

function initTag(obj,result){
 
  tableCtn = result;
	try{
		initElements(obj);
		initTabPages()
	}
	finally{
	}
}

function initElement(element){
	var initChildren=true;
	var _attrib=element.getAttribute("attrib");
	if (_attrib){
		switch (_attrib){
			case "tabpage":{
				if (!element.className) element.className=_attrib;
				initTabPage(element);
				initChildren=false;
				break;
			}
			default:{
				if (element.className &&_attrib) element.className=_attrib;
				break;
			}
		}

		element.window=window;
	}
	return initChildren;
}

function initTabPage(tabpage){
	var tabItems=tabpage.getAttribute("tabItems");
	if (!tabItems) return;
	var tabs=tabItems.split(";");

	for(var i=0; i<tabpage.tBodies[0].rows.length; i++){
		var row=tabpage.tBodies[0].rows[i];
		row.removeNode(true);
	}
	
	var row=tabpage.tBodies[0].insertRow();
	var cell=row.insertCell();
	cell.firstcell=true;
	cell.innerHTML="<img src=\""+"/base/themes/blue/images/tag/active_end_tab.gif\" width=7px height=19px>";

	var label, tabcode, index;
	for(i=0; i<tabs.length; i++){
		index=tabs[i].indexOf("=");
		if (index>=0){
			label=tabs[i].substr(0, index);
			tabcode=tabs[i].substr(index+1);
		}
		else{
			label=tabs[i];
			tabcode=tabs[i];
		}

		cell=row.insertCell();
		cell.tab_index=i;
		cell.tab_code=tabcode;
		

		btn=document.createElement("<button hideFocus=true style=\"border-style: none; font-size: 9pt; BACKGROUND-IMAGE='url(\/base\/themes\/blue\/images\/tag\/w.gif)'; cursor: hand\" ></button>");
		btn.value=label;
		btn.tabIndex=-1;
		btn.style.height=19;
		btn.style.backgroundColor="#ffffff";
		btn.onclick=_tabpage_onclick;
		btn.onmouseover=_tabpage_onmouseover;
		btn.onmouseout=_tabpage_onmouseout;
		btn.tab=cell;
		cell.appendChild(btn);

		cell=row.insertCell();
		if (i!=tabs.length-1){
			cell.innerHTML="<img src=\""+"/base/themes/blue/images/tag/tab.gif\">";
		}
		else{
			cell.lastcell=true;
			cell.innerHTML="<img src=\""+"/base/themes/blue/images/tag/end_tab.gif\">";
		}
		
		eval("if (typeof("+tabpage.id+"_"+tabcode+")!=\"undefined\"){ "+
			tabpage.id+"_"+tabcode+".style.visibility=\"hidden\";"+
			tabpage.id+"_"+tabcode+".style.position=\"absolute\";}");
		
	}
	cell=row.insertCell();
	cell.width="100%";

	setActiveTabIndex(tabpage, 0);
}

function setActiveTabIndex(table, index){
	for(var i=0; i<table.cells.length; i++){
		if (table.cells[i].tab_index==index){
			_setActiveTab(table.cells[i]);
			break;
		}
	}
}

function getRowByCell(cell){
	return cell.parentElement;
}

function getTableByCell(cell){
	var tbody=getRowByCell(cell).parentElement;
	if (tbody) return tbody.parentElement;
}

function getTableByRow(row){
	var tbody=row.parentElement;
	if (tbody) return tbody.parentElement;
}

function _setActiveTab(cell){//alert(cell.innerHTML);
	try{
	  if (tableCtn != null && (tableCtn.submitFlag == null || tableCtn.submitFlag == 0) && tableCtn.getRows()!= 0 && tableCtn.assemble()!='') {
      if(!confirm("是否放弃已修改的数据？"))
        return false;
    }
	
		var row=getRowByCell(cell);
		var tabpage=getTableByRow(row);
		var selectCell=tabpage.selectTab;

		if (selectCell==cell) return;
		var oldCode=(selectCell)?selectCell.tab_code:"";
		var newCode=cell.tab_code;

		if (selectCell){
			var prevCell=row.cells[selectCell.cellIndex-1];
			var nextCell=row.cells[selectCell.cellIndex+1];

			selectCell.firstChild.style.backgroundImage='url(/base/themes/blue/images/tag/w.gif)';
			
			selectCell.firstChild.style.backgroundImage='url(/base/themes/blue/images/tag/w.gif)';
			selectCell.firstChild.style.fontWeight="";

			if (prevCell.firstcell)
				prevCell.firstChild.src="/base/themes/blue/images/tag/start_tab.gif";
			else
				prevCell.firstChild.src="/base/themes/blue/images/tag/tab.gif";

			if (nextCell.lastcell)
				nextCell.firstChild.src="/base/themes/blue/images/tag/end_tab.gif";
			else
				nextCell.firstChild.src="/base/themes/blue/images/tag/tab.gif";
			eval("if (typeof("+tabpage.id+"_"+oldCode+")!=\"undefined\") "+tabpage.id+"_"+oldCode+".style.visibility=\"hidden\"");
		}

		var prevCell=row.cells[cell.cellIndex-1];
		var nextCell=row.cells[cell.cellIndex+1];

		cell.firstChild.style.backgroundImage='url(/base/themes/blue/images/tag/y.gif)';
		//cell.firstChild.style.color="white";
		cell.firstChild.style.backgroundImage='url(/base/themes/blue/images/tag/y.gif)';
		

		if (prevCell.firstcell)
			prevCell.firstChild.src="/base/themes/blue/images/tag/active_start_tab.gif";
		else
			prevCell.firstChild.src="/base/themes/blue/images/tag/active_tab1.gif";

		if (nextCell.lastcell)
			nextCell.firstChild.src="/base/themes/blue/images/tag/active_end_tab.gif";
		else
			nextCell.firstChild.src="/base/themes/blue/images/tag/active_tab2.gif";
		eval("if (typeof("+tabpage.id+"_"+newCode+")!=\"undefined\") "+tabpage.id+"_"+newCode+".style.visibility=\"\"");

		tabpage.selectTab=cell;
		tabpage.selectCode=cell.tab_code;
		tabpage.selectIndex=cell.tab_index;
		eval("tag_click(" + cell.tab_code + ")");
	}
	catch(e){
		
		processException(e);
	}
}
