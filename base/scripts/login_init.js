function login_init(){	
	var width = window.screen.width; // 屏幕分辨率的宽度
	var height = window.screen.height; // 屏幕分辨率的高度
	var availWidth = window.screen.availWidth; // 可用宽度
	var availHeight = window.screen.availHeight; // 去除掉任务栏的高度
	var toolHeight = 120; // 浏览器任务栏的高度，在80左右 ie9+ 为78 ie8- 为82
	var H = availHeight - toolHeight; // 浏览器页面用来显示内容的部分		
	var w = document.documentElement.clientWidth || document.body.clientWidth; // 浏览器能够显示页面的高度
	var h = document.documentElement.clientHeight || document.body.clientHeight; // 浏览器能够显示页面的高度	

	if(!document.getElementsByClassName) {
		document.getElementsByClassName = function (className, element) {
	    var children = (element || document).getElementsByTagName('*');
	    var elements = new Array();
	    for (var i = 0; i < children.length; i++) {
	        var child = children[i];
	        var classNames = child.className.split(' ');
	            for (var j = 0; j < classNames.length; j++) {
	                if (classNames[j] == className) {
	                    elements.push(child);
	                    break;
	                }
	            }
	        }
	        return elements;
	    };
	}
	
	/*
	 * 第一步：初始化容器container div
	 */
	var container = document.getElementsByClassName("container")[0];
	if(container){
		// 容器大小
		// container.style.width = availWidth * 0.98 + "px";
		// container.style.height = H + "px";
		// 随机更换背景
		var urls = new Array();
		urls[0] = "images/bg.png";
		//urls[1] = "images/bg2.jpg";
		//urls[2] = "images/bg3.png";
		var url = urls[Math.floor(Math.random() * urls.length)];		
		if("IE" == myBrowser()){
			container.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + url + "', sizingMethod='scale')";    	
		} else {
			container.style.background = "url(" + url + ") no-repeat";
			container.style.backgroundSize = "100% 100%";	
		}
	}
	// 第二步：初始化logo
	var logo = document.getElementsByClassName("logo")[0];	
	if(logo){
		//logo.style.background = "url(images/logo.png) no-repeat";
		//logo.style.width = width * 0.25 + "px";
		//logo.style.height = width * 0.25 * 0.2 + "px";
		if("IE" == myBrowser()){
			logo.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/logo.png', sizingMethod='scale')";    	
		} else {
			logo.style.background = "url(images/logo.png) no-repeat";
			logo.style.backgroundSize = "100% 100%";	
		}			
	}
	// 第三部：初始化main（登录div）
	var main = document.getElementsByClassName("main")[0];
	if(main){
		main.style.width = ((width * 0.18) > 250 ? (width * 0.18) : 250) + "px";
		main.style.height = height * 0.38 + "px";
		
		main.style.width = (parseInt(main.style.height) * 0.8 < 230 ? 230 : parseInt(main.style.height) * 0.8) + "px";
		main.style.position = "absolute";
		main.style.left = "50%";
		main.style.top = "50%";
		main.style.marginLeft = (-1 * parseInt(main.style.width) * 0.5) + "px";
		main.style.marginTop = (-1 * parseInt(main.style.height) * 0.5) + "px";
		
		// 初始化登录表格
		var table = main.getElementsByTagName("table")[0];
		if(table){
			
			// 初始化 包装 用户名、密码 的div层 的高度
			var inputs = document.getElementsByClassName("input");
			if(inputs && inputs.length){
				for(var i = 0 ; i < inputs.length ; i ++){
					inputs[i].style.height = height * 0.4 * 0.1 + "px";
				}
			}
			
			// 初始化 label 用户名、密码 
			var label_name = document.getElementsByClassName("label_name")[0];
			var label_pwd = document.getElementsByClassName("label_pwd")[0];
			
			if(label_name){
				label_name.style.lineHeight = height * 0.4 * 0.1 + "px";
				label_name.style.fontSize = "12px";
				//label_name.style.fontSize = ((height * 0.4 * 0.03) < 14 ? 14 : (height * 0.4 * 0.03)) + "px";	
			}
			if(label_pwd){
				label_pwd.style.lineHeight = height * 0.4 * 0.1 + "px";
				label_pwd.style.fontSize = "12px";
				//label_pwd.style.fontSize = ((height * 0.4 * 0.03) < 14 ? 14 : (height * 0.4 * 0.03)) + "px";
			}


			// 初始化 用户名和密码 输入框
			var title_span = document.getElementById("title");
			var username_input = document.getElementById("userName");
			var password_input = document.getElementById("userPwd");
			var login_btn = document.getElementById("login");
			var reset_btn = document.getElementById("reset");
			
			if(title_span){
				title_span.style.fontSize = "18px"
				//title_span.style.fontSize = ((height * 0.4 * 0.04) < 16 ? 16 : (height * 0.4 * 0.04)) + "px";
			}

			// 用户名输入框初始化（添加边框聚焦变化效果）
			if(username_input){
				username_input.style.height = height * 0.4 * 0.1 + "px";
				username_input.style.fontSize = "14px";
				//username_input.style.fontSize = ((height * 0.4 * 0.03) < 14 ? 14 : (height * 0.4 * 0.03)) + "px";
				username_input.style.lineHeight = height * 0.4 * 0.1 + "px";		
				username_input.onfocus = function(){
					this.parentNode.parentNode.style.border = "1px #000000 solid";					
				}
				username_input.onblur = function(){
					this.parentNode.parentNode.style.border = "1px #aeaeae solid";
				}
			}
			// 密码输入框初始化（添加边框聚焦变化效果）
			if(password_input){
				//password_input.style.border = "1px #aeaeae solid";	
				password_input.style.height = height * 0.4 * 0.1 + "px";
				password_input.style.fontSize = "14px";
				//password_input.style.fontSize = ((height * 0.4 * 0.03) < 14 ? 14 : (height * 0.4 * 0.03)) + "px";				
				password_input.style.lineHeight = height * 0.4 * 0.1 + "px";				
				password_input.onfocus = function(){
					this.parentNode.parentNode.style.border = "1px #000000 solid";			
				}
				password_input.onblur = function(){
					this.parentNode.parentNode.style.border = "1px #aeaeae solid";
				}
			}
			
			if(login_btn){
				login_btn.style.height = height * 0.4 * 0.1 + "px";
				login_btn.style.fontSize = "16px";
				//login_btn.style.fontSize = ((height * 0.4 * 0.04) < 16 ? 16 : (height * 0.4 * 0.04)) + "px";
				login_btn.style.lineHeight = height * 0.4 * 0.1 + "px";

			}

			if(reset_btn){
				reset_btn.style.height = height * 0.4 * 0.1 + "px";
				reset_btn.style.fontSize = ((height * 0.4 * 0.03) < 16 ? 16 : (height * 0.4 * 0.03)) + "px";
				reset_btn.style.lineHeight = height * 0.4 * 0.1 + "px";
			}

			// 初始化 遮罩 的宽度 以便能够遮挡 ie浏览器的 叉叉 和 眼睛图标
			
			var hovers = document.getElementsByClassName("hover");
			if(hovers && hovers.length){
				for(var i = 0 ; i < hovers.length ; i++){
					hovers[i].style.width = height * 0.4 * 0.1 + "px";
					hovers[i].style.zIndex = 10;
				}
			}

		}		
	}

	window.onresize = function(){
		if(main){
			//alert(main.style.width);
		}
	}
}

// 判断浏览器类型（不包括版本）
function myBrowser(){
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
    var isOpera = userAgent.indexOf("Opera") > -1;
    if (isOpera) {
        return "Opera"
    }; //判断是否Opera浏览器
    if (userAgent.indexOf("Firefox") > -1) {
        return "FF";
    } //判断是否Firefox浏览器
    if (userAgent.indexOf("Chrome") > -1){
  		return "Chrome";
 	}
    if (userAgent.indexOf("Safari") > -1) {
        return "Safari";
    } //判断是否Safari浏览器
    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
        return "IE";
    }; //判断是否IE浏览器
}

function showCode(){
	if(!document.getElementsByClassName) {
		document.getElementsByClassName = function (className, element) {
	    var children = (element || document).getElementsByTagName('*');
	    var elements = new Array();
	    for (var i = 0; i < children.length; i++) {
	        var child = children[i];
	        var classNames = child.className.split(' ');
	            for (var j = 0; j < classNames.length; j++) {
	                if (classNames[j] == className) {
	                    elements.push(child);
	                    break;
	                }
	            }
	        }
	        return elements;
	    };
	}
	var code = document.getElementsByClassName("qrcode")[0];
	if(code){
		code.style.display = "block";
	}
}

function hideCode(){
	if(!document.getElementsByClassName) {
		document.getElementsByClassName = function (className, element) {
	    var children = (element || document).getElementsByTagName('*');
	    var elements = new Array();
	    for (var i = 0; i < children.length; i++) {
	        var child = children[i];
	        var classNames = child.className.split(' ');
	            for (var j = 0; j < classNames.length; j++) {
	                if (classNames[j] == className) {
	                    elements.push(child);
	                    break;
	                }
	            }
	        }
	        return elements;
	    };
	}
	var code = document.getElementsByClassName("qrcode")[0];
	if(code){
		code.style.display = "none";
	}
}
