///Autor:Liu Zhikun Create Date:2007-12-12
var wageModulePrivateFunMap={
//function begin
"wageMonthListInit":function(obj){
		obj.para="<y>"+getAcctYear()+"</y>";
		obj.refresh();
	},
"wageDeptListInit":function(obj){
		obj.para="<c>"+getCompCode()+"</c>";
		obj.refresh();
	},
"wageSetMonthBound":function(obj){
		var p=getValuePairBySql("wage_get_cur_month_bound","");
		if(p!=null){
			obj.mindate=p[0];
			obj.maxdate=p[1];
		}
	},
"wageSetMonthListDefaultValue":function(obj){
		var p=getValuePairBySql("wage_get_cur_month_month","");
		if(p!=null){
			if(obj)
				obj.setValue(p[1]);
			return p[1];
		}else
			return "";
	},
"wageOpenSetFormulaDlgPara":function(pk){
		return [window.prefix+'hbos/wage/base/wage/set.html?load='+pk,'dialogWidth:650px;dialogHeight:500px;'];
	},
"wageCheckFormulaChar":function(obj){
		var n=obj.value;
		if(n.indexOf("+")>=0
			||n.indexOf("-")>=0
			||n.indexOf("*")>=0
			||n.indexOf("/")>=0
			||n.indexOf(")")>=0
			||n.indexOf("(")>=0
			||n.indexOf(".")>=0
			||n.indexOf("\"")>=0
		){
			alert(obj.htcLabel+"不能输入特殊字符!");
			return false;
		}
		return true;
	},
"wageEditBtnAutoDisabled":function(m){///某月是否已转账、是否已月结
		var p=getValuePairBySql("wage_editbnt_auto_disabled","<y>"+getAcctYear()+"</y><m>"+m+"</m>");
		var res=true;
		if(p==null)
			res=false;
		else
			res=true;
		if(arguments.length>1){
			for(var i=0;i<arguments.length;i++){
				arguments[i].disabled=res;
			}
		}
		return res;	
	},
"wageEditBtnAutoDisabled2":function(m){///是否已月结
		var p=getValuePairBySql("wage_editbnt_auto_disabled2","<y>"+getAcctYear()+"</y><m>"+m+"</m>");
		var res=true;
		if(p==null)
			res=false;
		else
			res=true;
		if(arguments.length>1){
			for(var i=1;i<arguments.length;i++){
				arguments[i].disabled=res;
			}
		}
		return res;	
	},
			
"wageEditBtnAutoDisabled3":function(y,m){///是否已月结
		var p=getValuePairBySql("wage_editbnt_auto_disabled2","<y>"+y+"</y><m>"+m+"</m>");
		var res=true;
		if(p==null)
			res=false;
		else
			res=true;
		if(arguments.length>1){
			for(var i=1;i<arguments.length;i++){
				arguments[i].disabled=res;
			}
		}
		return res;	
	},
"wageCheckDeptTreeValue":function(obj,text){
		if(obj.text.length>0){
			if(obj.text.substr(obj.text.length-1)=="　"){
				alert(text+" 只能选择末级科室!");
				return false;
			}
		}
		return true;
	},
"wageGetSelectSValue":function(obj){
		return obj.text.trim().replace("　","|||");
	}
//function end
};