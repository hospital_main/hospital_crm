function changeModule(obj){
			bmod=obj.options[obj.selectedIndex].bmod;
			var p=window.prefix;
			var mod=obj.value;
			if(bmod=="12"){
				if(p.indexOf("/vh")>0){
					h=p+'main2.jsp?'+mod;
				}else{
					h=p+'vh/main2.jsp?'+mod;
				}
			}else{
				if(p.indexOf("/vh")>0){
					h=p+'../main.jsp?'+mod;
				}else{
					h=p+'main.jsp?'+mod;
				}
			}
			_saveChangeModule(bmod=="12",mod);
			is_close_alert=0;
			_isDestroySession = false;
			window.location.href=h;
		}
function _saveChangeModule(isCbcs,mod){
		getValuePairBySql("sysSaveUserLastMode","<m>"+(isCbcs==true?"cbcs_":"")+mod+"</m>");
	}
function initModuleList(modbox,modId,dictName){
	srcTree=getDict(dictName);
	var nodes = srcTree.documentElement.getElementsByTagName("para");
	if(nodes.length==0)
		return ;
	var html="<select id='"+modId+"' onchange=\"changeModule(this)\">";
	var sel="";
	var groupBegin=null;
	for(var i=0;i<nodes.length;i++){
		if(window.module==nodes[i].getAttribute("code"))
			sel=" selected "
		else
			sel="";
		var c=nodes[i].getAttribute("code");
		var v=nodes[i].getAttribute("value");
		var bm=nodes[i].getAttribute("bmod");
		if(c==""){
			html+="<OPTGROUP LABEL='"+v+"'>";
			groupBegin=v;
		}else
			html+="<option "+sel+" value=\""+c+"\" bmod='"+bm+"' >"+v+"</option>"
		if(groupBegin!=null&&groupBegin!=bm){
			html+="</OPTGROUP>";
			groupBegin=null;
		}
	}
	if(groupBegin!=null){
		html+="</OPTGROUP>";
		groupBegin=null;
	}
	html+="</select>";
	modbox.innerHTML=html;
	var obj=document.getElementById(modId);
	window.moduleNum=obj.options[obj.selectedIndex].bmod;
}