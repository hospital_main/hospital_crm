function swapNode(node1,node2){    var parent=node1.parentNode;    var t1=node1.nextSibling;    var t2=node2.nextSibling;    if(t1) parent.insertBefore(node2,t1);    else parent.appendChild(node2);    if(t2) parent.insertBefore(node1,t2);    else parent.appendChild(node1);}
function delayShowDialog(dlg){
	window.setTimeout(dlg,200);
}
function initModule(){
	var strLocation = new String(window.location);
	//window.module = strLocation.replace(/^[^\?]*\?/g,'');
	window.module = strLocation.replace(/^[^\?]*\?/g,'').split('#')[0];
	if(window.module.substr(0,4)=="drug")
	{
	    var drug_l=window.module.split(";");
	    if(drug_l.length==3){
		    window.module=drug_l[0];
		    window._drugDept=drug_l[1];
		    window._drugDeptName=drug_l[2];
		}else
			delayShowDialog("chooseDrugHouse()");
	}else if(window.module.substr(0,4)=="budg")
	{
	    /*var drug_l=window.module.split(";");
	    if(drug_l.length==3){
		    window.module=drug_l[0];
		    window._budgCode=drug_l[1];
		    window._budgYear=drug_l[2];
	    }else
	    	delayShowDialog("chooseBudgYear()");*/
	}else
	{
	  window._drugDept=null;
	  window._drugDeptName=null;
	}
	/*if(window.module.substr(0,4)=="acct")
	{
	    var drug_l=window.module.split(";");
	    if(drug_l.length==4){
		    window.module=drug_l[0];
		    window._Acctyear=drug_l[1];
		    window._AcctyearBeginDate=drug_l[2];
		    window._AcctyearEndDate=drug_l[3];
	    }else
	    	delayShowDialog("chooseAcctYear()");
	}*/
	if(window.module=="")
		delayShowDialog("chooseModule()");

}
var _module = "";
var _menu_xml_setp=0;

function getMenu(module){
	var str,f;
	
	if(module.indexOf('cbcs_') == 0){
		cbcsObject = new ActiveXObject("Microsoft.XMLHTTP");
		cbcsObject.open("POST", "login.hbviewhigh", false);
		cbcsObject.send("<root><user><module>"+module.replace(/cbcs_/ig,'')+"</module></user></root>");
		str = cbcsObject.responseText
	}else{
		window.xmlhttp.post("login", "<user><module>"+module+"</module></user>");
	  str = window.xmlhttp._object.responseText
	}
	
	f = parseInt(str);
  if(f=="0"){
		alert("您没有正常登录或工作已超时,\n请您重新登录!");
		window.open('./login.htm');
		window.close();
		return;
	}
  if (f=='4') {
	  alert('软件狗没有设置此权限，请检查软件狗')
	  window.open('./login.htm');
	  window.close();
	  return;
	}
	var srcTree = new ActiveXObject("Microsoft.XMLDOM");
  srcTree.async=false;
  srcTree.loadXML(str);
  
  return srcTree.xml;
}
var curTab = 0;
var _module = "";
var _menu_xml_setp=0;
var ulCount = 0,liCount=0,groupCount = 31,menuHtml = '',newflag = false;
function tab_create(){
	if($('.pageTab').length == 6){
		alert('最多只能打开5个页签！');
		return false;
	}		
	$('.pageTab:last').after('<div class="pageTab"><a href="#" class="pageLabel">空白页</a><a href="#" class="pageOff"></a></div>');
	$('#pageContent').append("<td height='100%'><iframe src='blank.htm' width='100%' height='100%'></iframe></td>");
	setTabActive(($('.pageTab').length - 1))
	curTab = $('.pageTab').length - 1;
	$('#pageContent td').hide();
	$('#pageContent td:last').show();
	mainMainMainMainMainIFrame = $('#pageContent iframe:last').get(0)
	refreshTabs()
	return true;
}
//激活页签
function setTabActive(ii){
	$('.pageLabel').removeClass('pageLabel-active');
	$('.pageLabel:eq('+ii+')').addClass('pageLabel-active');
}
function refreshTabs(){
	$('.pageOff').click(
		function(){
			$('#pageContent td').hide();
			$('#pageContent td:eq('+$(".pageOff" ).index(this)+')').remove();
			$('#pageContent td:last').show();
			$(this).parent().remove();
			curTab = $('.pageTab').length - 1;
			setTabActive(curTab)
			mainMainMainMainMainIFrame = $('#pageContent iframe:eq('+curTab+')').get(0)
		}
	)
	
	//点击页签
	$('.pageTab').click(
		function(){
			curTab = $(".pageTab" ).index(this)
			
			setTabActive(curTab)
			$('#pageContent td').hide();
			$('#pageContent td:eq('+curTab+')').show();
			
			var moudle = $(this).attr('moudle')
			if(moudle == undefined) moudle = "";
			window.module = moudle;
			setEnvionmentValue("_module",moudle);
			setEnvionmentValue("_moduleNo",getModCode(window.module));
			
			mainMainMainMainMainIFrame = $('#pageContent iframe:eq('+curTab+')').get(0)
			
		}
	)
}

function goPageNew(url,menu_name,tab_name,moudle,openNew,firstClass,menu_node){
    //alert('url:'+url);
    //alert('menu_name:'+menu_name)
    //alert('moudle:'+moudle)
    //alert('openNew:'+openNew)
    //alert('firstClass:'+firstClass)
    //alert('menu_node:'+menu_node)
    //setWorkspaceTabLabel("fewffwe")
    if(url!='')
    {
        //cyz	2018/3/29	全景图跳转更改上面书签和下面页签
        setWorkspaceTabLabel(menu_name);
        tabTreeItemNames(tab_name,menu_name,moudle);

    }

    // if(openNew || curTab == 0){
    //     var createFlag = tab_create();
    //     if(!createFlag) return;
    // }

    //if(tab_create() == false) return;

    var fullName = '';
    if(menu_node){
        if($(menu_node).parents('.modList-li')[0]){
            fullName += $(menu_node).parents('.modList-li')[0].firstChild.nodeValue;
        }
        if($(menu_node).parents('.oneList')[0]){
            fullName += '-' + $(menu_node).parents('.oneList')[0].firstChild.firstChild.nodeValue;
        }
        if($(menu_node).parents('.twoList')[0]){
            fullName += '-' + $(menu_node).parents('.twoList')[0].firstChild.firstChild.nodeValue;
        }
        if($(menu_node).parents('.threeList')[0]){
            fullName += '-' + $(menu_node).parents('.threeList')[0].firstChild.firstChild.nodeValue;
        }
        if($(menu_node).parents('.fourList')[0]){
            fullName += '-' + $(menu_node).parents('.fourList')[0].firstChild.firstChild.nodeValue;
        }
        if($(menu_node).parents('.fiveList')[0]){
            fullName += '-' + $(menu_node).parents('.fiveList')[0].firstChild.firstChild.nodeValue;
        }
    }
    //alert("fullName====="+fullName)
    window.xmlhttp.post("globalRecordSysLog","<modal>"+fullName+"</modal><copy>"+getCopyCode()+"</copy><comp>"+getCompCode()+"</comp>","?isCheck=false");

    window.module = moudle;
    setEnvionmentValue("_module",moudle);
    setEnvionmentValue("_moduleNo",getModCode(window.module));
    // var reval = beforePage(menu_name,moudle,firstClass,url);
    // if(reval){
    //     menu_name = reval.menu_name
    //     url=reval.url
    // }

    // if(menu_name.length > 8){
    //     $('.pageLabel:eq('+curTab+')').text(menu_name.substr(0,8)+'...')
    // }else{
    //     $('.pageLabel:eq('+curTab+')').text(menu_name)
    // }
    // $('.pageLabel:eq('+curTab+')').attr('title',menu_name)
    // $('.pageTab:eq('+curTab+')').attr('moudle',moudle)

    if(moudle.indexOf('cbcs_') == 0){
        url = url.replace('&amp;','&')
        mainMainMainMainMainIFrame.src=window.prefix+'vh/'+url;
    }
    else
        mainMainMainMainMainIFrame.src=window.prefix+url;
    /*
    //记录历史信息
    window.xmlhttp.post("oesCommonMunuSetting","<moudle>"+moudle+"</moudle><menu_name>"+menu_name+"</menu_name><menu_path>"+url+"</menu_path><full_name>"+fullName+"</full_name>","?isCheck=false");
    var srcTree = new ActiveXObject("Microsoft.XMLDOM");
    srcTree.async=false;
    srcTree.load(xmlhttp._object.responseXML);
    var errNode = srcTree.selectSingleNode("/root/error");
    if(errNode.text){
        alert(errNode.text);
        return;
    }
    var msgNode = srcTree.selectSingleNode("/root/msg");
    if(msgNode.text){
    }
    */
}

function goPage(url,menu_name,moudle,openNew,firstClass,menu_node){
	//alert('url:'+url);
	//alert('menu_name:'+menu_name)
	//alert('moudle:'+moudle)
	//alert('openNew:'+openNew)
	//alert('firstClass:'+firstClass)
	//alert('menu_node:'+menu_node)
	//setWorkspaceTabLabel("fewffwe")
	if(url!='')
	{
		//cyz	2018/3/29	全景图跳转更改上面书签和下面页签
		setWorkspaceTabLabel(menu_name);
		tabTreeItemName(menu_name);
		
	}
	
	// if(openNew || curTab == 0){
	// 	var createFlag = tab_create();
	// 	if(!createFlag) return;
	// }
	//
	//if(tab_create() == false) return;
	
	var fullName = '';
	if(menu_node){
		if($(menu_node).parents('.modList-li')[0]){
			fullName += $(menu_node).parents('.modList-li')[0].firstChild.nodeValue;
		}
		if($(menu_node).parents('.oneList')[0]){
			fullName += '-' + $(menu_node).parents('.oneList')[0].firstChild.firstChild.nodeValue;
		}
		if($(menu_node).parents('.twoList')[0]){
			fullName += '-' + $(menu_node).parents('.twoList')[0].firstChild.firstChild.nodeValue;
		}
		if($(menu_node).parents('.threeList')[0]){
			fullName += '-' + $(menu_node).parents('.threeList')[0].firstChild.firstChild.nodeValue;
		}
		if($(menu_node).parents('.fourList')[0]){
			fullName += '-' + $(menu_node).parents('.fourList')[0].firstChild.firstChild.nodeValue;
		}
		if($(menu_node).parents('.fiveList')[0]){
			fullName += '-' + $(menu_node).parents('.fiveList')[0].firstChild.firstChild.nodeValue;
		}
	}
	//alert("fullName====="+fullName)
	window.xmlhttp.post("globalRecordSysLog","<modal>"+fullName+"</modal><copy>"+getCopyCode()+"</copy><comp>"+getCompCode()+"</comp>","?isCheck=false");
	
	window.module = moudle;
	setEnvionmentValue("_module",moudle);
	setEnvionmentValue("_moduleNo",getModCode(window.module));
	// var reval = beforePage(menu_name,moudle,firstClass,url);
	// if(reval){
	// 	menu_name = reval.menu_name
	// 	url=reval.url
	// }
	
	// if(menu_name.length > 8){
	// 	$('.pageLabel:eq('+curTab+')').text(menu_name.substr(0,8)+'...')
	// }else{
	// 	$('.pageLabel:eq('+curTab+')').text(menu_name)
	// }
	// $('.pageLabel:eq('+curTab+')').attr('title',menu_name)
	// $('.pageTab:eq('+curTab+')').attr('moudle',moudle)
	//
	if(moudle.indexOf('cbcs_') == 0){
		url = url.replace('&amp;','&')
		mainMainMainMainMainIFrame.src=window.prefix+'vh/'+url;
	}
	else
        mainMainMainMainMainIFrame.document.location.href=window.prefix+url;
	/*	
	//记录历史信息
	window.xmlhttp.post("oesCommonMunuSetting","<moudle>"+moudle+"</moudle><menu_name>"+menu_name+"</menu_name><menu_path>"+url+"</menu_path><full_name>"+fullName+"</full_name>","?isCheck=false");
	var srcTree = new ActiveXObject("Microsoft.XMLDOM");
	srcTree.async=false;
	srcTree.load(xmlhttp._object.responseXML);
	var errNode = srcTree.selectSingleNode("/root/error");
	if(errNode.text){
		alert(errNode.text);
		return;
	}
	var msgNode = srcTree.selectSingleNode("/root/msg");
	if(msgNode.text){
	}
	*/
}

	function setWorkspaceTabLabel(label){
			workspace_tab_labels[curr_workspace_index]=label;
		}
	
	//更改头标签显示名称
	function tabTreeItemName(paras){
		//得到当前打开窗口的个数
		var pageindex= document.getElementById("mainMainMainBottomTabTd").children[0].children[0].children.length;
		pageindex=pageindex-1;
		
		 var TagTreeItem=document.getElementById("mainMainMenuTabTd"+pageindex).children[0].children[0].children[0].children[0];
     //var TagTreeItemName=TagTreeItem.innerHTML;
     TagTreeItem.innerHTML=paras;
			
		}

//更改头标签显示名称
function tabTreeItemNames(paras,tab_name,moudle){
    //得到当前打开窗口的个数
    var pageindex= document.getElementById("mainMainMainBottomTabTd").children[0].children[0].children.length;
    pageindex=pageindex-1;

    var TagTreeItem=document.getElementById("mainMainMenuTabTd"+pageindex);
    //var TagTreeItemName=TagTreeItem.innerHTML;
    var pss=paras.split(";:;");
    var us;

    var html="";
    html+="<ul class=\"tabs\">";
    html+="  ";
    var url = '';
    for(var i=0;i<pss.length;i++){
    	if(pss[i]!=null&&pss[i]!=''){
            us=pss[i].split(";");

            if(us[0]==tab_name){
                html+="<li class=\"tabson\"><a><span>"+us[0]+"</span></a></li>";
                url=us[1];
            }else{
                selStr=" style='cursor:hand' moudle=\""+moudle+"\"  tabItemUrl=\""+us[1]+"\" tabItemIndex='"+i+"' ";
                html+="<li><a href=" + "..\/"  + us[1] +"><span "+selStr+">"+us[0]+"</span></a></li>";
            }
		}

    }
    html+="</ul>";
    TagTreeItem.innerHTML= html;
    parameter="../../../"+url;
    getCurrWorkspaceIFrame().location.replace(parameter);
    //document.getElementById("mainMainMainMainMainIFrame"+pageindex).document.href=parameter;

    // getTabShowTableDiv().innerHTML=html;/*
    // getTabShowTableDiv().style.display='block';*/
    var tds=TagTreeItem.getElementsByTagName("span");
    for(var i=0;i<tds.length;i++){
        if(tds[i].getAttribute("tabItemUrl")!=null){
            tds[i].style["cursor"]="hand";
            var selIdx=tds[i].getAttribute("tabItemIndex");
            tds[i].onclick=function(){

                // tabTreeItemNames(paras,this.getAttribute("tabItemIndex"));
                // moudle=this.getAttribute("moudle")
                // if(moudle.indexOf('cbcs_') == 0){
                //     urltd = this.getAttribute("tabItemUrl").replace('&amp;','&')
                //     mainMainMainMainMainIFrame.src=window.prefix+'vh/'+urltd;
                // }
                // else
                //     mainMainMainMainMainIFrame.src=window.prefix+ this.getAttribute("tabItemUrl");

                goPageNew( this.getAttribute("tabItemUrl") ,this.innerHTML,paras,moudle ,true);

            }
        }
    }
}

//
// //更改头标签显示名称
// function tabTreeItemNames(paras){
//     //得到当前打开窗口的个数
//     var pageindex= document.getElementById("mainMainMainBottomTabTd").children[0].children[0].children.length;
//     pageindex=pageindex-1;
//
//     var TagTreeItem=document.getElementById("mainMainMenuTabTd"+pageindex).children[0].children[0].children[0].children[0];
//     //var TagTreeItemName=TagTreeItem.innerHTML;
//     TagTreeItem.innerHTML=paras;
// "<ul class='tabs'><li class='tabson'><a><span>"+paras+"</span></a></li></ul>"
// }
	/*function showHideWorkspaceTab(index,show){
			var td;
			td=document.getElementById("mainMainMainMainMainIFrameTd"+index);
			td.style.display=show;
  		    td=document.getElementById("mainMainMenuTabTd"+index);
  	    	td.style.display=show;
			
		}*/
function start() {
	
	var modList = modeCodeToNameList;
	for(var o in modList){
	   if(window.module==modList[o])
	       setEnvionmentValue("moduleId",o);
	}
  var srcTree = new ActiveXObject("Microsoft.XMLDOM");
  srcTree.async=false;
	if(window.module.substr(window.module.length-1)=="#"){
		window.module=window.module.substr(0,window.module.length-1);
	}
	
	
  window.xmlhttp.post("login", "<user><module>"+window.module+"</module></user>");
  var str = window.xmlhttp._object.responseText
  
  var f = parseInt(str);
	if(f=="0"){
		alert("您没有正常登录或工作已超时,\n请您重新登录!");
		window.open('./login.htm');
		window.close();
		return;
	}
  if (f=='4') {
	  alert('软件狗没有设置此权限，请检查软件狗')
	  window.open('./login.htm');
	  window.close();
	  return;
	}
  srcTree.loadXML(str);



  // 预算模块，用于根据预算功能设置控制预算菜单的显示
  if(window.module=='budg'){
  	var p =  getValuePairBySql("budgGetInitPara","<comp>"+getCompCode()+"</comp><copy>"+getCopyCode()+"</copy><year>"+getAcctYear()+"</year>");
  	var pValue = p[0].split(';');
  	var pv1=pValue[0];
  	var flag1=pValue[1];
  	var flag2=pValue[2];
  	var flag3=pValue[3];
  	var flag4=pValue[4];
  	
  	if (pv1==1){

			var pages1 = srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='业务科室年度支出编制' and @exMode_2='5']");
		  var pages2 = srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='业务科室月份支出编制' and @exMode_2='6']");
		  var pages3 = srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='职能科室年度支出编制' and @exMode_2='3']");
		  var pages4 = srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='职能科室月份支出编制' and @exMode_2='4']");
		  if (flag1==0 && flag2==0){
		  		
		  }else if (flag1==1 && flag2==1){
		  	
		  	if(flag3 ==1 && flag4==1){
		  		
		  		swapNode(pages1[0],pages3[0])
		  		
		  		swapNode(pages2[0],pages4[0])
		  	}
		  	if(flag3 ==1 && flag4==0){
		  		swapNode(pages1[0],pages3[0])
		  		swapNode(pages2[0],pages3[0])
		  	}
		  	if(flag3 ==0 && flag4==1){
		  		swapNode(pages2[0],pages3[0])
		  		swapNode(pages2[0],pages4[0])
		  	}
		  }else if (flag1==0 && flag2==1){
		  	if(flag3 ==1 && flag4==1){
		  		swapNode(pages2[0],pages3[0])
		  		swapNode(pages2[0],pages4[0])
		  	}
		  	if(flag3 ==1 && flag4==0){
		  		swapNode(pages2[0],pages3[0])
		  	}
		  	if(flag3 ==0 && flag4==1){
		  		swapNode(pages2[0],pages4[0])
		  	}
		  }else if (flag1==1 && flag2==0){
		  	if(flag3 ==1 && flag4==1){
		  		swapNode(pages1[0],pages3[0])
		  		swapNode(pages1[0],pages4[0])
		  	}
		  	if(flag3 ==1 && flag4==0){
		  		swapNode(pages1[0],pages3[0])
		  	}
		  	if(flag3 ==0 && flag4==1){
		  		swapNode(pages1[0],pages4[0])
		  	}
		  }
		}
  	//srcTree.insertBefore(pages0[0],pages1[]);
  }
  //单机核算模块
  if(window.module=='dev'){
  	absModule = "dev";
	initDevParaData();
  }
  //人力资源模块
  if(window.module=='hr')
  {
	  window.xmlhttp.post("hr_empDocumentQueryInfo_userPerm","<user_id>"+getUserID()+"</user_id>","?isCheck=false");
	  var resXml = window.xmlhttp._object.responseXml;
	  if(resXml.getElementsByTagName("td")[0]&&resXml.getElementsByTagName("td")[0].firstChild!=null)
	  {
	  	  window.xmlhttp.post("hrWorkRemindMain_select","<a>"+getUserID()+"</a><b>auto</b>");
		  var resXml = window.xmlhttp._object.responseXml;
		  var data = resXml.getElementsByTagName('tbody')[0];
		  var trs = data.getElementsByTagName("tr");
		  var sumNum=0;
		  for (var i=0; i<trs.length; i++){
		  	sumNum=sumNum+parseInt(trs[i].getElementsByTagName("td")[1].firstChild.nodeValue);
		  }
		  //查询人员调动是否为0
		  window.xmlhttp.post("hrWorkRemind_personMoveNum","<user_id>"+getUserID()+"</user_id>","?isCheck=false");
	  	var resXml2 = window.xmlhttp._object.responseXml;
		  var data2 = resXml2.getElementsByTagName('tbody')[0];
		  var trs2 = data2.getElementsByTagName("tr");
		  var sumNum2 = 0;
		  sumNum2 = parseInt(trs2[0].getElementsByTagName("td")[0].firstChild.nodeValue);
		  
		  if(sumNum!=0 || sumNum2!=0)
		  {
		  	
		  	openIFDialog(window,'../../hbos/hr/workremind/remind/main_remind.html?load=<opmode>auto</opmode>','dialogWidth:900px;dialogHeight:600px');
		  }
	  }
  }
   if(getEnvionmentValue("_module")=='payctl' || getEnvionmentValue("_module")=='pact'){
  	init_mate_radix_point();
  	if (getEnvionmentValue("mateSysPara")==null || getEnvionmentValue("mateSysPara")=='undefined' ){
			var mateSysParaArray;
			var para='<comp_code>'+getCompCode()+'</comp_code><copy_code>'+getCopyCode()+'</copy_code>'
	  	mateSysParaArray = new Array();
	    window.xmlhttp.post("mate_sys_para_value_select", para, "?isCheck=false");
	    var srcTree1 = new ActiveXObject("Microsoft.XMLDOM");
	    srcTree1.async=false;
	    srcTree1.load(xmlhttp._object.responseXML);
	    var list1 = srcTree1.getElementsByTagName("tr");
	    for (var i=0; i<list1.length; i++) {
	      var list2 = list1.item(i).getElementsByTagName("td");
            var  nodeValue1 = ''
            if (list2.item(1).firstChild != null)
                nodeValue1 =list2.item(1).firstChild.nodeValue

            if (list2.item(0).firstChild != null)
	      mateSysParaArray[list2.item(0).firstChild.nodeValue]=nodeValue1
	   	}
			setEnvionmentValue("mateSysPara",mateSysParaArray);
		}
  }
  // Page load Excute Method<initPerfParaData>
  if(getEnvionmentValue("_module")=='perf'){
  	initPerfParaData();
  	initOprStratagem();
	if(typeof(showCompName)!="undefined")
		showCompName();
  }
  if(getEnvionmentValue("_module")=='bonus'){
  	initbonusParaData();
  }
  //初始化小数位数
  if(getEnvionmentValue("_module")=='mate'){
  	init_mate_radix_point();
 		init_is_link_budg();
  }
  
  if(getEnvionmentValue("_module")=='sys'){
  	
  	window.xmlhttp.post("getSysPara", '<a>1201</a>', "?isCheck=false");
    var sTree = new ActiveXObject("Microsoft.XMLDOM");
    sTree.async=false;
    sTree.load(xmlhttp._object.responseXML);
    
    if(sTree.getElementsByTagName('td')[0].firstChild.nodeValue=='否'){
    	pages=srcTree.getElementsByTagName("menu_node");
    	for(var i=0;i<pages.length;i++){
	    	var node =pages[i].getAttributeNode("name").value;
	     	if(node=="成本一体化数据交换"||node=="收入项目"||node=="收费类别"||node=="收费项目"||node=="成本项目"||node=="成本类型"){
	      	pages[i].parentNode.removeChild(pages[i]);
	      }
    	}
    }
  }
  
  if(getEnvionmentValue("_module")=='bankroll'){
  	/*
  	window.xmlhttp.post("getSysPara1", '<a>2200</a>', "?isCheck=false");
    var sTree = new ActiveXObject("Microsoft.XMLDOM");
    sTree.async=false;
    sTree.load(xmlhttp._object.responseXML);
    
    if(sTree.getElementsByTagName('td')[0].firstChild.nodeValue=='否'){
    	pages=srcTree.getElementsByTagName("menu_node");
    	for(var i=0;i<pages.length;i++){
	    	var node =pages[i].getAttributeNode("name").value;
	     	if(node=="资金计划(月)"||node=="资金监控(月)"||node=="资金计划(年)"||node=="资金监控(年)"){
	      	pages[i].parentNode.removeChild(pages[i]);
	      }
    	}
    }else if(sTree.getElementsByTagName('td')[0].firstChild.nodeValue=='是'){
    	window.xmlhttp.post("getSysPara1", '<a>2204</a>', "?isCheck=false");
	    var sTr = new ActiveXObject("Microsoft.XMLDOM");
	    sTr.async=false;
	    sTr.load(xmlhttp._object.responseXML);
	    if(sTr.getElementsByTagName('td')[0].firstChild.nodeValue=='年'){
	    	pages=srcTree.getElementsByTagName("menu_node");
	    	for(var i=0;i<pages.length;i++){
		    	var node =pages[i].getAttributeNode("name").value;
		     	if(node=="资金计划(月)"||node=="资金监控(月)"){
		      	pages[i].parentNode.removeChild(pages[i]);
		      }
	    	}
	    }else{
	    	pages=srcTree.getElementsByTagName("menu_node");
	    	for(var i=0;i<pages.length;i++){
		    	var node =pages[i].getAttributeNode("name").value;
		     	if(node=="资金计划(年)"||node=="资金监控(年)"){
		      	pages[i].parentNode.removeChild(pages[i]);
		      }
	    	}
	    }
    	
    }*/
  }
  
  //初始化小数位数
  if(getEnvionmentValue("_module")=='acct'){
  	pages=srcTree.getElementsByTagName("menu_node");
	  for(var i=0;i<pages.length;i++){
		   var node =pages[i].getAttributeNode("name").value;
		   if(node=="个人工资(网上查询)"){
		      pages[i].parentNode.removeChild(pages[i]);
		   }
	  }
	    	
 		if (getEnvionmentValue("mateSysPara")==null || getEnvionmentValue("mateSysPara")=='undefined' ){
			var mateSysParaArray;
			var para='<comp_code>'+getCompCode()+'</comp_code><copy_code>'+getCopyCode()+'</copy_code>'
	  	mateSysParaArray = new Array();
	    window.xmlhttp.post("mate_sys_para_value_select", para, "?isCheck=false");
	    var srcTree1 = new ActiveXObject("Microsoft.XMLDOM");
	    srcTree1.async=false;
	    srcTree1.load(xmlhttp._object.responseXML);
	    var list1 = srcTree1.getElementsByTagName("tr");
	    for (var i=0; i<list1.length; i++) {
	      var list2 = list1.item(i).getElementsByTagName("td");
            var  nodeValue1 = ''
            if (list2.item(1).firstChild != null)
                nodeValue1 =list2.item(1).firstChild.nodeValue
	      if (list2.item(0).firstChild != null)
	      mateSysParaArray[list2.item(0).firstChild.nodeValue]=nodeValue1
	   	}
			setEnvionmentValue("mateSysPara",mateSysParaArray);
		}
		//alert(getCopyCode());
		init_mate_radix_point();
 		init_is_link_budg();
  }
  //结束 fanxiaojing
  if(getEnvionmentValue("_module")=='equi'){
  	var edit_model;
    var d = new Date();
    var equi_month = d.getMonth()+1;
    if(equi_month<10){
    	equi_month = "0"+equi_month;
    }
    var equi_year =d.getFullYear();
    var array=getDict('equip_init','<year>'+equi_year+'</year><month>'+equi_month+'</month>')
    var vXml= new ActiveXObject("Microsoft.XMLDOM");
    vXml.async = false;
    vXml.loadXML(array.xml);
    if(vXml.getElementsByTagName("para").length>0){
      edit_model=vXml.getElementsByTagName("para")[0].getAttributeNode("code").value
	  }
	  {
	  var pages=srcTree.getElementsByTagName("menu_node");
    if (edit_model==1){
      pages=srcTree.getElementsByTagName("menu_node");
    	for(var i=0;i<pages.length;i++){
    	var node =pages[i].getAttributeNode("name").value;
     	if(node=="期初建帐"){
      	srcTree.getElementsByTagName("menu_node")[i].parentNode.removeChild(pages[i])
      	break;
      }
    }
	}
	}
  }
  if(getEnvionmentValue("_module")=='acct'){
  	
  	var compCode = getCompCode() ;
  	var copyCode = getCopyCode();
  	var acctYear = getAcctYear();
  	
  	var para_value;
  	var para_value2;
  	var para_value3;
  	var flag ;
  	var flag2;
		var hasEqui ;
		var p =  getValuePairBySql("acctGetInitPara","<comp>"+compCode+"</comp><copy>"+copyCode+"</copy>");
		var pValue = p[0].split(';');
		para_value = pValue[0];
		flag=pValue[1];
		hasEqui = pValue[2];
		para_value2 = pValue[3];
		para_value3 = pValue[4];
		flag2 = pValue[5];
		
		//新会计制度模块筛选
		var pCoCode =  getValuePairBySql("sysGetCoCode","<comp>"+compCode+"</comp><copy>"+copyCode+"</copy>");
		var coCode = pCoCode[0].split(';');
		
		//启用新会计制度年份
		var isUseNewFlag  =  getValuePairBySql("sys_vouch_new_flag","<comp>"+compCode+"</comp><copy>"+copyCode+"</copy><year>"+acctYear+"</year>");
     /* */var pagesOne=srcTree.getElementsByTagName("menu_node");
    	for(var i=0;i<pagesOne.length;i++){
     	   var checkMethodMenu = pagesOne[i].getAttribute("checkMethod");
		     if(pCoCode!=null&&coCode=="06"&&checkMethodMenu=="1")
		      {
		      	 
		         pagesOne[i].parentNode.removeChild(pagesOne[i]);
		      }
		      
		      else if((pCoCode!=null&&coCode!="06"))
		      {
		      	  if(isUseNewFlag!=null&&isUseNewFlag[0]=="1"&&checkMethodMenu=="1")
		      	  {
		      	  	  pagesOne[i].parentNode.removeChild(pagesOne[i]);
		      	  }
		      	  else if(isUseNewFlag!=null&&isUseNewFlag[0]!="1"&&checkMethodMenu=="2")
	      	  	{
	      	  			pagesOne[i].parentNode.removeChild(pagesOne[i]);
	      	  	}
		      }
   	
		  }
		
		if(hasEqui=="1"){//移除固定资产和无形资产菜单
			var pages = srcTree.selectNodes("/root/menu_node/menu_node[@name='固定资产']");
	  	for(var i=0;i<pages.length;i++){
	    	var node =pages[i].getAttributeNode("name").value;
	    	var ncode =pages[i].getAttribute("code");
	     	if(node=="固定资产"){
	      	srcTree.selectNodes("/root/menu_node/menu_node[@name='固定资产']")[i].parentNode.removeChild(pages[i])
	      	break;
	      }
	    }			
			var pages1 = srcTree.selectNodes("/root/menu_node/menu_node[@name='无形资产']");
	  	for(var i=0;i<pages1.length;i++){
	    	var node =pages1[i].getAttributeNode("name").value;
	    	var ncode =pages1[i].getAttribute("code");
	     	if(node=="无形资产"){
	      	srcTree.selectNodes("/root/menu_node/menu_node[@name='无形资产']")[i].parentNode.removeChild(pages1[i])
	      	break;
	      }
	    }
			var pages2 = srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='固定资产月报表']");
	  	for(var i=0;i<pages2.length;i++){
	    	var node =pages2[i].getAttributeNode("name").value;
	    	var ncode =pages2[i].getAttribute("code");
	     	if(node=="固定资产月报表"){
	      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='固定资产月报表']")[i].parentNode.removeChild(pages2[i])
	      	break;
	      }
	    }

	    
	    			
			var pages3 = srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='固定资产账簿']");
	  	for(var i=0;i<pages3.length;i++){
	    	var node =pages3[i].getAttributeNode("name").value;
	    	var ncode =pages3[i].getAttribute("code");
	     	if(node=="固定资产账簿"){
	      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='固定资产账簿']")[i].parentNode.removeChild(pages3[i])
	      	break;
	      }
	    }

			var pages4 = srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='无形资产月报表']");
	  	for(var i=0;i<pages4.length;i++){
	    	var node =pages4[i].getAttributeNode("name").value;
	    	var ncode =pages4[i].getAttribute("code");
	     	if(node=="无形资产月报表"){
	      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='无形资产月报表']")[i].parentNode.removeChild(pages4[i])
	      	break;
	      }
	    }
	    			
			var pages5 = srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='无形资产账簿']");
	  	for(var i=0;i<pages5.length;i++){
	    	var node =pages5[i].getAttributeNode("name").value;
	    	var ncode =pages5[i].getAttribute("code");
	     	if(node=="无形资产账簿"){
	      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='无形资产账簿']")[i].parentNode.removeChild(pages5[i])
	      	break;
	      }
	    }
			
		}else{
			var pages = srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='业务对账']");
			for(var i=0;i<pages.length;i++){
	    	var node =pages[i].getAttributeNode("name").value;
	    	var ncode =pages[i].getAttribute("code");
	     	if(node=="业务对账"){
	      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='业务对账']")[i].parentNode.removeChild(pages[i])
	      	break;
	      }
	    }		
		}
		if(para_value2 == '否'){
					var pages=srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='出纳签字']");
					for(var i=0;i<pages.length;i++){
				    	var node =pages[i].getAttributeNode("name").value;
				    	var ncode =pages[i].getAttribute("code");
				     	if(node=="出纳签字"){
				      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='出纳签字']")[i].parentNode.removeChild(pages[i]);
				      	break;
				      }
				      
	    		}
		}
		if(para_value3 == '1'){
			var pages=srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node/menu_node[@name='职工内部调动']");
			for(var i=0;i<pages.length;i++){
	    	var node =pages[i].getAttributeNode("name").value;
	    	var ncode =pages[i].getAttribute("code");
	     	if(node=="职工内部调动"){
	      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node/menu_node[@name='职工内部调动']")[i].parentNode.removeChild(pages[i]);
	      	var pages2=srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='职工内部调动']");
	      	if (flag2==0)
	      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='职工内部调动']")[0].parentNode.removeChild(pages2[0])
	      	break;
	      }
  		}
		}
		if (para_value == '往来部分核销'){
	  	var pages=srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='往来集中核销']");
	  	for(var i=0;i<pages.length;i++){
	    	var node =pages[i].getAttributeNode("name").value;
	    	var ncode =pages[i].getAttribute("code");
	     	if(node=="往来集中核销"){
	      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='往来集中核销']")[i].parentNode.removeChild(pages[i])
	      	var pages2=srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='往来核销']");
	      	var pages3=srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='往来部分核销']");

	      	if (flag==0)
	      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='往来核销']")[0].parentNode.removeChild(pages2[0])
	      	break;
	      }
	    }
	  }
	  
	  if (para_value == '往来集中核销'){
	  	var pages_log=srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='核销日志']");
	  	for(var i=0;i<pages_log.length;i++){
	    	var node =pages_log[i].getAttributeNode("name").value;
	    	var ncode =pages_log[i].getAttribute("code");
	      if(node=="核销日志" ){
	      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='核销日志']")[i].parentNode.removeChild(pages_log[i])
	      	break;
	      }
	    }
	  	var pages=srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='往来部分核销']");
	  	for(var i=0;i<pages.length;i++){
	    	var node =pages[i].getAttributeNode("name").value;
	    	var ncode =pages[i].getAttribute("code");
	      if(node=="往来部分核销" ){
	      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='往来部分核销']")[i].parentNode.removeChild(pages[i])
	      	var pages2=srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='往来核销']");
	      	var pages3=srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='往来集中核销']");

	      	if (flag ==0){	      		
	      	 srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='往来核销']")[0].parentNode.removeChild(pages2[0])
	      	}
	      	break;
	      }
	    }
	  }
	  
  }
  if(getEnvionmentValue("_module")=='mate'){
  	var compCode = getCompCode() ;
  	var copyCode = getCopyCode();
  	window.xmlhttp.post("mateGetInitPara","<comp>"+compCode+"</comp><copy>"+copyCode+"</copy>","?isCheck=false");
		var doc = window.xmlhttp._object.responseXML;
		var arr = doc.getElementsByTagName("td");
		var para_value;
		if(arr.length==1){
			para_value = arr[0].firstChild.xml
		}else{
			para_value = 1;
		}
	  var pages=srcTree.selectNodes("/root/menu_node/menu_node[@name='应付款管理']");
  	for(var i=0;i<pages.length;i++){
    	var node =pages[i].getAttributeNode("name").value;
    	var ncode =pages[i].getAttribute("code");
     	if(node=="应付款管理" && ncode=="yfkgl" && para_value==2){
      	srcTree.selectNodes("/root/menu_node/menu_node[@name='应付款管理']")[i].parentNode.removeChild(pages[i])
      	break;
      }
      if(node=="应付款管理" && ncode=="yfkglbd" && para_value==1){
      	srcTree.selectNodes("/root/menu_node/menu_node[@name='应付款管理']")[i].parentNode.removeChild(pages[i])
      	break;
      }
    }
    window.xmlhttp.post("mateGetInitPara2","<comp>"+compCode+"</comp><copy>"+copyCode+"</copy>","?isCheck=false");
		var doc2 = window.xmlhttp._object.responseXML;
		var arr2 = doc2.getElementsByTagName("td");
		var para_value2;
		if(arr2.length==1){
			para_value2 = arr2[0].firstChild.xml
		}else{
			para_value2 = 0;
		}
	  var pages2=srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='结存报表(按供应商)']");
	  for(var i=0;i<pages2.length;i++){
     	if(para_value2==0){
      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node[@name='结存报表(按供应商)']")[i].parentNode.removeChild(pages2[i])
      	break;
      }
    }
    window.xmlhttp.post("mateGetInitPara3","<comp>"+compCode+"</comp><copy>"+copyCode+"</copy>","?isCheck=false");
		var doc3 = window.xmlhttp._object.responseXML;
		var arr3 = doc3.getElementsByTagName("td");
		var para_value3;
		if(arr3.length==1){
			para_value3 = arr3[0].firstChild.xml
		}else{
			para_value3 = 0;
		}
	  var pages3=srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node/menu_node[@name='按科室级次(零售)']");
	  for(var i=0;i<pages3.length;i++){
     	if(para_value3==0){
      	srcTree.selectNodes("/root/menu_node/menu_node/menu_node/menu_node/menu_node[@name='按科室级次(零售)']")[i].parentNode.removeChild(pages3[i])
      	break;
      }
    }
  }
  //资产与合同管理模块连用的情况下禁用自身的合同管理
  if(getEnvionmentValue("_module")=='equi' || getEnvionmentValue("_module")=='imma'){

	  var compCode = getCompCode() ;
  		var copyCode = getCopyCode();
      var acctYear = getAcctYear();

      //新会计制度模块筛选
      var pCoCode =  getValuePairBySql("sysGetCoCode","<comp>"+compCode+"</comp><copy>"+copyCode+"</copy>");
      var coCode = pCoCode[0].split(';');
      var isUseNewFlag  =  getValuePairBySql("sys_vouch_new_flag","<comp>"+compCode+"</comp><copy>"+copyCode+"</copy><year>"+acctYear+"</year>");

  	var moduleCode=getEnvionmentValue("_moduleNo");
  	window.xmlhttp.post("pactCombined_moduleselect","<comp>"+compCode+"</comp><copy>"+copyCode+"</copy><mod_code>"+moduleCode+"</mod_code>","?isCheck=false");
		var doc = window.xmlhttp._object.responseXML;
		var arr = doc.getElementsByTagName("td");
		var is_combined;
		if(arr.length==1){
			is_combined = arr[0].firstChild.xml
		}else{
			is_combined = '0';
		}
	  var pages=srcTree.selectNodes("/root/menu_node/menu_node[@name='合同管理']");
  	for(var i=0;i<pages.length;i++){

    	var node =pages[i].getAttributeNode("name").value;
    	var ncode =pages[i].getAttribute("code");
     	if(node=="合同管理" && ncode=="htgl" && is_combined=='1'){
      	srcTree.selectNodes("/root/menu_node/menu_node[@name='合同管理']")[i].parentNode.removeChild(pages[i])
      	break;
      }
    }


      var pagesOne=srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='计提折旧']");
      for(var i=0;i<pagesOne.length;i++){
          var checkMethodMenu = pagesOne[i].getAttribute("checkMethod");
          if(pCoCode!=null&&coCode=="06"&&checkMethodMenu=="1")
          {

              pagesOne[i].parentNode.removeChild(pagesOne[i]);
          }

          else if((pCoCode!=null&&coCode!="06"))
          {
              if(isUseNewFlag!=null&&isUseNewFlag[0]=="1"&&checkMethodMenu=="1")
              {
                  pagesOne[i].parentNode.removeChild(pagesOne[i]);
              }
              else if(isUseNewFlag!=null&&isUseNewFlag[0]!="1"&&checkMethodMenu=="2")
              {
                  pagesOne[i].parentNode.removeChild(pagesOne[i]);
              }
          }
      }
	//根据不同体系显示不同的对账页面
      var pagesOne=srcTree.selectNodes("/root/menu_node/menu_node/menu_node[@name='固定资产总账对账']");
      for(var i=0;i<pagesOne.length;i++){
          var checkMethodMenu = pagesOne[i].getAttribute("checkMethod");
          if(pCoCode!=null&&coCode=="06"&&checkMethodMenu=="1")
          {

              pagesOne[i].parentNode.removeChild(pagesOne[i]);
          }

          else if((pCoCode!=null&&coCode!="06"))
          {
              if(isUseNewFlag!=null&&isUseNewFlag[0]=="1"&&checkMethodMenu=="1")
              {
                  pagesOne[i].parentNode.removeChild(pagesOne[i]);
              }
              else if(isUseNewFlag!=null&&isUseNewFlag[0]!="1"&&checkMethodMenu=="2")
              {
                  pagesOne[i].parentNode.removeChild(pagesOne[i]);
              }
          }
      }

    }

    /*var firstNode1 = srcTree.selectNodes("/root/menu_node/menu_node[@name!='']");
    var menuNodeObj1 = srcTree.createElement('menu_node');
    menuNodeObj1.setAttribute("id", "0255");
    menuNodeObj1.setAttribute("code", "wdsc");
    menuNodeObj1.setAttribute("name", "我的收藏");
    menuNodeObj1.setAttribute("accessKey", "");
    menuNodeObj1.setAttribute("hotKey", "");
    menuNodeObj1.setAttribute("icon", "");
    menuNodeObj1.setAttribute("flag", "true");
    firstNode1[0].parentNode.insertBefore(menuNodeObj1, firstNode1[0]);
    var idNode1 = srcTree.selectNodes("/root/menu_node/menu_node[@name ='我的收藏']")[0];
    var pageObj1 = srcTree.createElement('page');
    pageObj1.setAttribute("src", "hbos/acct/acct/mycollection/mycollection.jsp");
    idNode1.appendChild(pageObj1);

    var permNode1 = srcTree.selectNodes("/root/menu_node/menu_node/page[@src ='hbos/acct/acct/mycollection/mycollection.jsp']")[0];
    var permObj1 = srcTree.createElement('perm');
    permObj1.setAttribute("name", "syswdsc");
    permObj1.setAttribute("desc", "我的收藏");
    permNode1.appendChild(permObj1);*/

		/*if(window.module=="acct"){
		}	cyz	2018/3/29	全景导航*/
		var firstNode= srcTree.selectNodes("/root/menu_node/menu_node[@name!='']");
  	var menuNodeObj= srcTree.createElement('menu_node');
  	menuNodeObj.setAttribute("id","0200");
  	menuNodeObj.setAttribute("code","qjt");
  	menuNodeObj.setAttribute("name","全景导航"); 
  	menuNodeObj.setAttribute("accessKey","");  
  	menuNodeObj.setAttribute("hotKey",""); 
  	menuNodeObj.setAttribute("icon","");  
  	menuNodeObj.setAttribute("flag","true");  
  	firstNode[0].parentNode.insertBefore(menuNodeObj,firstNode[0]);
  	var idNode = srcTree.selectNodes("/root/menu_node/menu_node[@name ='全景导航']")[0];
  	var pageObj = srcTree.createElement('page');
  	pageObj.setAttribute("src","hbos/welcome.html");
  	idNode.appendChild(pageObj);
  	
  	var permNode = srcTree.selectNodes("/root/menu_node/menu_node/page[@src ='hbos/welcome.html']")[0];
  	var permObj = srcTree.createElement('perm');
  	permObj.setAttribute("name","sysqjt");
  	permObj.setAttribute("desc","全景导航查看");
  	permNode.appendChild(permObj);

	 var firstNode1= srcTree.selectNodes("/root/menu_node/menu_node[@name!='']");
     var menuNodeObj1= srcTree.createElement('menu_node');
     menuNodeObj1.setAttribute("id","0255");
     menuNodeObj1.setAttribute("code","wdsc");
     menuNodeObj1.setAttribute("name","我的收藏");
     menuNodeObj1.setAttribute("accessKey","");
     menuNodeObj1.setAttribute("hotKey","");
     menuNodeObj1.setAttribute("icon","");
     menuNodeObj1.setAttribute("flag","true");
     firstNode1[0].parentNode.insertBefore(menuNodeObj1,firstNode1[0]);
     var idNode1 = srcTree.selectNodes("/root/menu_node/menu_node[@name ='我的收藏']")[0];
     var pageObj1 = srcTree.createElement('page');
     pageObj1.setAttribute("src","hbos/acct/acct/mycollection/mycollection.jsp");
     idNode1.appendChild(pageObj1);

  var xsltTree= new ActiveXObject("Microsoft.XMLDOM");
  xsltTree.async = false;
  xsltTree.load("base/xsl/menu.xsl");
  srcTree.loadXML(srcTree.transformNode(xsltTree));

  var selectNodes = srcTree.selectNodes("/root/*");
  if(selectNodes.length==0 ){
    alert("你没有权限访问此模块");
    window.close();
    //window.open('./login.html');
    window.open(top.prefix);
    is_close_alert='0';
    return false;
  }
  //Modified BY ChenHaifeng 01/14/2013 BEGIN
  if(getCopyCode!=null){
  	window.xmlhttp.post("sys_copy_name_for_id","<comp>"+getCompCode()+"</comp><copy>"+getCopyCode()+"</copy>","?isCheck=false");
		var doc = window.xmlhttp._object.responseXML;
		var arr = doc.getElementsByTagName("td");
		var copy_name;
		if(arr.length==1){
			copy_name = arr[0].firstChild.xml
		}else{
			copy_name = '';
		}
		document.title=CONST_MODULE_NAMES[window.module]+" - "+copy_name
  }else{
  	document.title=CONST_MODULE_NAMES[window.module];
  }
  //Modified 01/14/2013 END
	//document.title=CONST_MODULE_NAMES[window.module]+" "+window.herpVersion;
	window.menu_xml = srcTree.xml;
	_menu_xml_setp++;
	mainMenuInitTimer=window.setInterval(function(){
		if(_menu_xml_setp<2)
			return;
		try{
			
			mainMainMenuTable.setMenuData(parent.menu_xml,"first");
			showCompName();
			window.clearInterval(mainMenuInitTimer);
		}catch(E){};
	},20);

  var moduleName =getModuleName(window.module);


	//工作流加载菜单-------begin pengjin
	if(module!="sys" && module!="work"){//系统平台、工作流模块不单独加载工作流菜单
		 window.xmlhttp.post("loginWork", "<user><module>work</module></user>"); 
		 var strWork=window.xmlhttp._object.responseText;
		 
		 var srcTreeWork = new ActiveXObject("Microsoft.XMLDOM");
		 srcTreeWork.async=false;
		 srcTreeWork.loadXML(strWork);
		 
		 ////----- add by yangqing begin
		 var xsltTree_work= new ActiveXObject("Microsoft.XMLDOM");
  	 xsltTree_work.async = false;
  	 xsltTree_work.load("base/xsl/menu.xsl");
  	 srcTreeWork.loadXML(srcTreeWork.transformNode(xsltTree_work));
  	 window.menu_work_xml=srcTreeWork.xml;
  	 
	}
		
	
  
  
   //工作流加载菜单-------end

  var year="";
  var sty=" style='cursor:hand;color:#FFFFFF;font-size:12px;' >";
  moduleName="<a  onClick='chooseModule()'"+sty+moduleName+"</a>";
  if(window._drugDept!=null)
    moduleName+="　<a onClick='chooseDrugHouse()'"+sty+window._drugDeptName+'</a>';

  if(window.module=="acct")
    moduleName+="&nbsp;&nbsp;&nbsp;&nbsp;<a onClick='chooseAcctYear()'"+sty+"<span id='llm'>"+getAcctYear()+"年度</span></a>";
  else if(window.module=="budg")
    moduleName+="&nbsp;&nbsp;&nbsp;&nbsp;<a onClick='chooseBudgYear()'"+sty+"<span id='llm'>"+getBudgYear()+"预算</span></a>";
  else
    moduleName+="&nbsp;&nbsp;&nbsp;&nbsp;<a onClick='chooseAcctYear()'"+sty+"<span id='llm'></span></a>";

	//document.getElementById("oper").innerText = getEmpName();  //HJJ comment out 现在3.0版本没有"oper"这个对象
}
var CONST_MODULE_NAMES={
//"sys":"SmartHERP 系统平台",
//"acct":"SmartHERP-Accounting 医院会计核算与财务管理系统",
//"rep":"SmartHERP-Reporting 报表管理系统",
//"mate":"SmartHERP-Inventory 医院物流管理系统",
//"equi":"SmartHERP-Fixed Asset 医院固定资产管理系统",
//"hr":"SmartHERP-Human Resource 医院人力资源管理系统",
//"drug":"SmartHERP-Pharmacy 医院药品管理系统",
//"ven":"SmartHERP-Supplier 医院供应商管理系统",
//"wage":"SmartHERP-Salary 医院工资管理系统",
//"budg":"SmartHERP-Budgeting 医院预算管理系统",
//"payctl":"SmartHERP-Expense Control 医院支出控制系统",
//"cbcs":"SmartHERP-CBCS 医院成本核算经济管理系统",
//"perf":"SmartHERP-Performance 医院绩效管理系统",
//"stat":"SmartHERP-Stat 医院综合统计系统",
//"uinfo":"SmartHERP-Unit Info 望海单位信息",
//"pote":"SmartHERP-Potecary 望海药房管理",
//"bonus":"SmartHERP-Bonus 望海奖金分配",
//"dev":"SmartHERP-Dev 医院医疗设备单机效能分析系统",
//"hisc":"SmartHERP-HIS 望海HIS收费"
"sys":"业务基础平台",
"acct":"医院会计核算和财务管理系统",
"rep":"报表管理系统",
"mate":"医院物流管理系统",
"equi":"医院固定资产管理系统",
"hr":"医院人力资源管理系统",
"drug":"医院药品管理系统",
"ven":"医院供应商管理系统",
"wage":"医院工资管理系统",
"budg":"医院预算管理系统",
"payctl":"医院资金支出控制系统",
"cbcs":"医院成本核算经济管理信息系统",
"perf":"医院绩效管理系统",
"stat":"医院综合统计系统",
"uinfo":"望海单位信息",
"pote":"望海药房管理",
"bonus":"医院奖金管理系统",
"dev":"医院医疗设备单机效能分析系统",
"hisc":"望海HIS收费",
"imma":"医院无形资产管理系统",
"pact":"医院合同管理系统",
"work":"医院工作流管理系统",
"fund":"医院科研基金管理系统",
"bankroll":"资金管理",
"bdgt":"全面预算"
};

function getModuleName(moduleEn) {
	if(moduleEn=="sys")
		moduleName = "望海系统平台";
	else if(moduleEn=="acct")
		moduleName = "望海会计核算";
	else if(moduleEn=="rep")
		moduleName = "望海报表";
	else if(moduleEn=="mate")
		moduleName = "望海医院物流";
	else if(moduleEn=="equi")
		moduleName = "望海医院固定资产";
	else if(moduleEn=="hr")
		moduleName = "望海人力资源";
	else if(moduleEn=="drug")
		moduleName = "望海医院药品";
	else if(moduleEn=="ven")
		moduleName = "望海医院供应商";
	else if(moduleEn=="wage")
		moduleName = "望海工资";
	else if(moduleEn=="budg")
		moduleName = "望海医院预算";
	else if(moduleEn=="payctl")
		moduleName = "望海医院支出控制";
	else if(moduleEn=="cbcs")
		moduleName = "望海医院成本核算";
	else if(moduleEn=="perf")
		moduleName = "望海医院绩效";
	else if(moduleEn=="stat")
		moduleName = "望海综合统计";
	else if(moduleEn=="uinfo")
		moduleName = "望海单位信息";
	else if(moduleEn=="pote")
		moduleName = "望海药房管理";
	else if(moduleEn=="bonus")
		moduleName = "望海奖金分配";
	else if(moduleEn=="dev")
		moduleName = "望海单机核算";
	else if(moduleEn=="hisc")
		moduleName = "望海HIS收费";	
	else if(moduleEn=="imma")
		moduleName = "望海无形资产";
	else if(moduleEn=="pact")
		moduleName = "望海合同管理";
	else if(moduleEn=="work")
		moduleName = "望海工作流管理";
	else if(moduleEn=="fund")
		moduleName = "望海科研基金管理";
	else if(moduleEn=="bankroll")
		moduleName = "望海资金管理";
	else if(moduleEn=="bdgt")
		moduleName = "望海全面预算管理";
  return moduleName;
}

function chooseModule(){
  window.showModalDialog("module.html", window, "status:0;resizable:yes;scroll:0;dialogHeight:400px;dialogWidth:500px;");
  if(window.secondOpenDialog!=null){
    eval(window.secondOpenDialog);
  }
}
function chooseDrugHouse(){
  window.showModalDialog("drughouse.html", window, "status:0;resizable:yes;scroll:1;dialogHeight:300px;dialogWidth:500px;")
}
function chooseAcctYear(){
  window.showModalDialog("acctyear.html", window, "status:0;resizable:yes;scroll:0;dialogHeight:300px;dialogWidth:500px;")
}
function chooseBudgYear(){
  window.showModalDialog("budgyear.html", window, "status:0;resizable:yes;scroll:0;dialogHeight:300px;dialogWidth:500px;")
}
function modifyPswd(){
  window.showModalDialog("password.html", window, "status:0;resizable:yes;scroll:0;dialogHeight:250px;dialogWidth:400px;")
}
function setEnv(){
  var rv = window.showModalDialog("setEnv.html", window, "status:0;resizable:yes;scroll:0;dialogHeight:300px;dialogWidth:350px;");
  if(is_quit){
  	top.window.opener=null;
  	window.open('./login.html');
  	top.close();
  	return;
  }
  if(!rv){
  	setEnv();
  }

  if("true" == rv){ 	
  	//重新加载菜单
  	try{
  		resetLoginDateInfo();
  	}catch(E){}
		start();
  	//当前情况为管理员登陆系统平台时，需要刷新当前页面
  	/*if("sys" == window.module && isAdmin() == 1){
  		if(_gMMMFLocation)
  			mainMainMainMainMainIFrame.document.location.href = _gMMMFLocation;
  	}*/
  }
}
function sendMessage(){
  window.showModalDialog("/hbos/sys/message/creat.html", window, "status:0;resizable:yes;scroll:0;dialogHeight:290px;dialogWidth:480px;")
}
function showMessage()
{
  alertMessage();
  setTimeout("showMessage()", 30000);
}
function alertMessage() {
  //return;
  //首先查数据库中没有自己的并且没有被阅读的消息列表
  var e = new Array();
  subBody.load = "sysMessage_select";
  subBody.para = "";  //清除修改状态时的参数
 	subBody.post();

  e = subBody.getHiddenVs();
  if (e == null) return;
  for (i=0; i<e.length; i++) {
    alert("发送人："+e[i][1]+"\n内容："+e[i][2]+"\n发送时间："+e[i][3]);

	  subBody.load = "sysMessage_changestate";
	  subBody.para = "<msgid>"+e[i][0]+"</msgid>";
	  subBody.post();
  }
}

//start();

//Modifier:Jack Date:2007-04-17
//Begin
/**
 *JavaScript : Map class
 *Map<_key:_value>
 */
function Map(){
	this.elements=new Array();

	this.size=function(){
		return this.elements.length;
	}
	
	this.put=function(_key,_value){
		this.elements.push({key:_key,value:_value});
	}
	
	this.remove=function(_key){
		var bln=false;
		try{   
			for (i=0;i<this.elements.length;i++){  
				if (this.elements[i].key==_key){
					this.elements.splice(i,1);
					return true;
				}
			}
		}catch(e){
			bln=false;    
		}
		return bln;
	}
	
	this.containsKey=function(_key){
		var bln=false;
		try{   
			for (i=0;i<this.elements.length;i++){  
				if (this.elements[i].key==_key){
					bln=true;
				}
			}
		}catch(e){
			bln=false;    
		}
		return bln;
	}
	
	this.get=function(_key){    
		try{   
			for (i=0;i<this.elements.length;i++){  
				if (this.elements[i].key==_key){
					return this.elements[i];
				}
			}
		}catch(e){
			return null;   
		}
	}
}

/**
 *初始化单机核算模块的系统参数
 */

function initDevParaData(){
	var devPara = new Object
	var fs=getDict("devSysPara","");
	var ps=fs.getElementsByTagName("para");
	var c,v;
	for(var i=0;i<ps.length;i++){
		c=ps[i].getAttribute("code");
		v=ps[i].getAttribute("value");
		devPara[c] = v;
	}
	setEnvionmentValue("devParaData",devPara);
}

/**
 *Initialize Performance Module Environment Data
 */
function initPerfParaData() {
  // Global Environment Data Contains
	setEnvionmentValue("_paraMap",new Map());
	
	if(getEnvionmentValue("_module") == 'perf'){
		// Params list
		var modCode = '13';
		var compCode = getCompCode() ;
		// Post request to Server-Side
		window.xmlhttp.post("initPerfEnv","<user><subfunction>initPerfParaData</subfunction><modCode>" + modCode + "</modCode><compCode>" + compCode + "</compCode></user>");
		// Receive Server-Side Data
		var paras = window.xmlhttp._object.responseText;
		if (paras.trim() == '') {
			//var _ErrorMsg = 'Data Load Error!';
			//alert(_ErrorMsg);
			//window.close();
			return false;
		}
		
		//Process Data
		var paraDatas = paras.split("\|");
		var paraCodes = paraDatas[0].split(",");
		var paraValues = paraDatas[1].split(",");

		for(var i = 0 ; i < paraCodes.length ; i++){
			getEnvionmentValue("_paraMap").put(paraCodes[i],paraValues[i]);
		}
	}
}

/**
 *Initialize Operater Current Stratagem  Data
 */
function initOprStratagem() {
	
	if(getEnvionmentValue("_module")== 'perf'){
		// Params list
		var compCode = getCompCode() ;
		var userId = getUserID();
		
		// Post request to Server-Side
		window.xmlhttp.post("confirmOprStratagem","<user><subfunction>ConfirmOperaterStratagem</subfunction><compCode>" + compCode + "</compCode><userId>" + userId + "</userId></user>");
		// Receive Server-Side Data
		var paras = window.xmlhttp._object.responseText;
		if (paras.trim() == '') {
			//var _ErrorMsg = 'Data Load Error!';
			//alert(_ErrorMsg);
			//window.close();
			setEnvionmentValue("_stratagemSeqNo","0");
			setEnvionmentValue("_stateFlag","0");
			setEnvionmentValue("_stratagemCurrentMonth","00")
			return false;
		}
		
		//Process Data
		var paraDatas = paras.split(",");
		
		window.top.window._stratagemSeqNo = paraDatas[0];
		window.top.window._stratagemYear = paraDatas[1];
		window.top.window._stratagemCurrentMonth = paraDatas[2];
		window.top.window._stateFlag = paraDatas[3];
		window.top.window._stratagemName = paraDatas[4];

		setEnvionmentValue("_stratagemSeqNo",paraDatas[0]);
		setEnvionmentValue("_stratagemYear",paraDatas[1]);
		setEnvionmentValue("_stratagemCurrentMonth",paraDatas[2]);
		setEnvionmentValue("_stateFlag",paraDatas[3]);
		setEnvionmentValue("_stratagemName",paraDatas[4]);
	}
}
function initbonusParaData(){
	var p=getValuePairBySql("bonus_get_perf_stratagem_sequence_no","")
	if(p!=null){
		setEnvionmentValue("_stratagemSeqNo",p[0]);
		setEnvionmentValue("_stateFlag",p[1]);
	}else{
		setEnvionmentValue("_stratagemSeqNo",'0');
		setEnvionmentValue("_stateFlag",' ');
	}
}

//add by b2zhangwei
function init_mate_radix_point(){
	var p=get_radix_point();//该方法在sub.js内
	if(p!=null){
		setEnvionmentValue("mate_radix_point",p);
	}else{
		setEnvionmentValue("mate_radix_point",new Array("00","00"));
	}
}
function init_is_link_budg(){
	var isLink=get_is_link_budg();//该方法在sub.js内
	
	setEnvionmentValue("isLink",isLink[0]);
	setEnvionmentValue("isControl",isLink[1]);
}

//End
//LZK ADD BEGIN
function initModulePrivateFun(w){
	for(var k in baseSystemFunMap)
			w[k]=baseSystemFunMap[k];
	for(var mk in CONST_MODULE_NAMES){
		if(eval("typeof("+mk+"ModulePrivateFunMap)")!="undefined"){
			var m=eval(mk+"ModulePrivateFunMap");
			for(var k in m){
				w[k]=m[k];
			}
		}
	}
}
//LZK ADD END
