/**
 * 名称：浮动窗口
 * @param id,width,height,left,top,title
 * @author dujuntian
 */

FloatWindow = function(){
    this.init.apply(this, arguments);
}
FloatWindow.prototype = {
    init: function(id,title,width, height, left, top){
		//全局初始化
        if (FloatWindow.INDEX == undefined) 
            FloatWindow.INDEX = 1000;
		this.win_screen_height = document.body.offsetHeight;
        this.id = id;
        this.width = width;
        this.height = height;
		if(left==undefined) left=1;
		if(top==undefined) top=this.win_screen_height -this.height;
		if(title==undefined) title="列表";
        this.left = left;
        this.top = top;
        this.title = title;
		
        if (FloatWindow.hoverColor == undefined) 
            FloatWindow.hoverColor = '#7382A0';//'orange';
        if (FloatWindow.normalColor == undefined) 
            FloatWindow.normalColor = 'slategray';
        if (FloatWindow.offx == undefined) 
            FloatWindow.offx = 6;
        if (FloatWindow.offy == undefined) 
            FloatWindow.offy = 6;
			
        this.moveable = false;
        this.__hideframe_ = "__hideframe_";
        this.__floatdiv_ = "__floatdiv_";
        this.__shadowdiv_ = "__shadowdiv_";
        this.__titlespan_ = "__titlespan_";
        this.__minspan_ = "__minspan_";
        this.__clsspan_ = "__clsspan_";
			
        FloatWindow.INDEX = FloatWindow.INDEX + 3;
        this.zIndex = FloatWindow.INDEX;
        var e;
        e = document.getElementById(this.__hideframe_ + this.id);
        if (e) 
            document.body.removeChild(e);
        e = document.getElementById(this.__floatdiv + this.id);
        if (e) 
            document.body.removeChild(e);
        e = document.getElementById(this.__shadowdiv + this.id);
        if (e) 
            document.body.removeChild(e);
        this.build(this);
        
    },
    getFocus: function(o){
		var me=o;
		var ret;
		ret=function(obj){
			if (obj == undefined) 
	            obj = this;
	        if (obj.style.zIndex != FloatWindow.INDEX+2) {
	            FloatWindow.INDEX = FloatWindow.INDEX + 3;
	            var idx = FloatWindow.INDEX;
	            obj.style.zIndex = idx;
	            obj.nextSibling.style.zIndex = idx - 1;
	        }
		}
		return ret;
       
    },
    /**
     * 最小化 private
     * @param {Object} obj
     */
    min: function(o){
        var me = o;
        var ret;
        ret = function(obj){
            obj = document.getElementById(me.__minspan_ + me.id);
            var win = obj.parentNode.parentNode;
            var sha = win.nextSibling;
            var hideframe = win.previousSibling;
            var tit = obj.parentNode;
            var msg = tit.nextSibling;
            var topvalue;
            
            var flg = msg.style.display == "none";
            if (flg) {
                //恢复
                topvalue = parseInt(win.style.top) - parseInt(me.height)+ parseInt(me.headdiv.style.height);
                topvalue= me.top;
				win.style.height = parseInt(msg.style.height) + parseInt(tit.style.height) + 2 * 2;
                sha.style.height = win.style.height;
                hideframe.style.height = win.style.height;
                msg.style.display = "block";
                obj.innerHTML = "0";
            }
            else {
                //最小化
                //topvalue = parseInt(win.style.top) + parseInt(win.style.height) - parseInt(me.headdiv.style.height);
				topvalue=me.win_screen_height- parseInt(me.headdiv.style.height)
				win.style.height = parseInt(tit.style.height) + 2 * 2;
                sha.style.height = win.style.height;
                hideframe.style.height = win.style.height;
                obj.innerHTML = "2";
                msg.style.display = "none";
            }
			win.style.top = topvalue;
            sha.style.top = topvalue;
            hideframe.style.top = topvalue;
			win.style.left = me.left;
            sha.style.left = me.left;
            hideframe.style.left = me.left;
        }
        return ret;
    },
    /**
     * private 关闭
     */
    close: function(o){
        var me = o;
        var ret;
        ret = function(obj){
            obj = document.getElementById(me.__clsspan_ + me.id);
            var win = obj.parentNode.parentNode;
            var sha = win.nextSibling;
            var hideframe = win.previousSibling;
            win.style.visibility = "hidden";
            sha.style.visibility = "hidden";
            hideframe.style.visibility = "hidden";
            document.body.removeChild(win);
            document.body.removeChild(sha);
            document.body.removeChild(hideframe);
        }
        return ret;
    },
    destory: function(){
    
    },
    drag: function(o){
		var me=o;
		var ret;
		ret=function(obj){
			if (obj == undefined) 
	            obj = document.getElementById(this.id);
	        
	        if (me.moveable) {
	            var win = obj.parentNode;
	            var sha = win.nextSibling;
	            var hideframe = win.previousSibling;
				var body_height = me.win_screen_height;//document.body.offsetHeight;
	            win.style.left = x1 + event.clientX - x0;
	            win.style.top = y1 + event.clientY - y0;
				
				if ((parseInt(win.style.top))<0) win.style.top=0;
				
				if ((parseInt(win.style.top)) > parseInt(body_height)-20) {
					win.style.top=parseInt(body_height)-20;
				}
				
	            sha.style.left = parseInt(win.style.left);// + FloatWindow.offx;
	            sha.style.top = parseInt(win.style.top);// + FloatWindow.offy;
				
	            hideframe.style.left = win.style.left;
	            hideframe.style.top = win.style.top;
	        }
		}
		return ret;
    },
    startDrag: function(o){
		var me=o;
		var ret;
		ret=function(obj){
			if (obj == undefined) 
	            obj = document.getElementById(this.id);
	        if (event.button == 1) {
	            //锁定标题栏;
	            obj.setCapture();
	            //定义对象;
	            var win = obj.parentNode;
	            var sha = win.nextSibling;
	            var hideframe = win.previousSibling;
	            //记录鼠标和层位置;
	            x0 = event.clientX;
	            y0 = event.clientY;
	            x1 = parseInt(win.style.left);
	            y1 = parseInt(win.style.top);
	            //记录颜色;
	            FloatWindow.normalColor = obj.style.backgroundColor;
	            //改变风格;
	            obj.style.backgroundColor = FloatWindow.hoverColor;
	            win.style.borderColor = FloatWindow.hoverColor;
	            obj.nextSibling.style.color = FloatWindow.hoverColor;
	            sha.style.left = x1 + FloatWindow.offx;
	            sha.style.top = y1 + FloatWindow.offy;
	            
	            //hideframe.style.width = parseInt(hideframe.style.width) + FloatWindow.offx;
	            hideframe.style.height = parseInt(hideframe.style.height) + FloatWindow.offy;
	            me.moveable = true;
	        }
		}
		return ret;
    },
    
    stopDrag: function(o){
		var me=o;
		var ret;
		ret=function(obj){
			if (obj == undefined) 
	            obj = document.getElementById(this.id);
	        
	        if (me.moveable) {
				
	            var win = obj.parentNode;
	            var sha = win.nextSibling;
	            var msg = obj.nextSibling;
	            var hideframe = win.previousSibling;
	            win.style.borderColor = FloatWindow.normalColor;
	            obj.style.backgroundColor = FloatWindow.normalColor;
	            msg.style.color = FloatWindow.normalColor;
	            sha.style.left = obj.parentNode.style.left;
	            sha.style.top = obj.parentNode.style.top;
	            me.moveable = false;
	            //hideframe.style.width = parseInt(hideframe.style.width) - FloatWindow.offx;
	            hideframe.style.height = parseInt(hideframe.style.height) - FloatWindow.offy;
	            obj.releaseCapture();
	        }
		}
		return ret;
    },
    /**
     * 创建UI private
     * @param {Object} obj
     */
    build: function(){
        //创建一个Iframe
        var ret;
        ret = function(obj){
			var me = obj;
			var body_width=document.body.offsetWidth;
			var headdiv_width;
			var titlespan_width;
			var btn_width;
			if (me.ispercent(me.width)){
				headdiv_width =parseInt(me.width)- 4/body_width*100;
				titlespan_width =headdiv_width- 24/body_width*100;
				btn_width= 14/body_width*100;
				
				headdiv_width=headdiv_width+"%";
				titlespan_width=titlespan_width+"%";
				btn_width=btn_width+"%";
				
			}else{
				headdiv_width=me.width - 4;
				titlespan_width = me.width - 28;
				btn_width=12;
				
			}
			
            var hideframe = document.createElement("iframe");
            hideframe.id = me.__hideframe_ + me.id;
            hideframe.style.width = me.width;
            hideframe.style.height = me.height;
            hideframe.style.left = me.left;
            hideframe.style.top = me.top;
            hideframe.style.position = "absolute";
            hideframe.style.border = "0px solid black";
            hideframe.scrolling = "no";
            hideframe.frameBorder = 0;
            
            var floatdiv = document.createElement("div");
            floatdiv.id = me.__floatdiv + me.id;
            floatdiv.style.width = me.width;
            floatdiv.style.height = me.height;
            floatdiv.style.left = me.left;
            floatdiv.style.top = me.top;
            floatdiv.style.backgroundColor = FloatWindow.normalColor;
            floatdiv.style.color = FloatWindow.normalColor;
            floatdiv.style.fontSize = "12px";
            floatdiv.style.fontFamily = "Verdana";
            floatdiv.style.position = "absolute";
            floatdiv.style.cursor = "default";
            floatdiv.style.border = "2px solid " + FloatWindow.normalColor;
            floatdiv.onmousedown = me.getFocus(me);
            //me.floatdiv = floatdiv;
            
            var headdiv = document.createElement("div");
            headdiv.id = "__headdiv_" + me.id;
            headdiv.style.background = FloatWindow.normalColor;
            headdiv.style.width = '100%';
            headdiv.style.height = 20;
            headdiv.style.color = "white";
            headdiv.onmousedown = me.startDrag(me);
            headdiv.onmouseup = me.stopDrag(me);
            headdiv.onmousemove = me.drag(me);
            headdiv.ondblclick = me.min(me);
            //me.headdiv = headdiv;
            var titlespan = document.createElement("span");
            titlespan.id = me.__titlespan + me.id;
            titlespan.style.width = titlespan_width;
            titlespan.style.paddingLeft = "3px";
            titlespan.innerText = me.title;
            
            var minspan = document.createElement("span");
            minspan.id = me.__minspan_ + me.id;
            minspan.style.width = btn_width;
            minspan.style.borderWidth = "0px";
            minspan.style.color = "white";
            minspan.style.fontFamily = "webdings";
            minspan.onclick = me.min(me);
            minspan.innerHTML = 0;
            
            var clsspan = document.createElement("span");
            clsspan.id = me.__clsspan_ + me.id;
            clsspan.style.width = btn_width;
            clsspan.style.borderWidth = "0px";
            clsspan.style.color = "white";
            clsspan.style.fontFamily = "webdings";
            clsspan.onclick = me.close(me);
            clsspan.innerText = "r";
            
			titlespan.style.width='96%';
            headdiv.appendChild(titlespan);
            headdiv.appendChild(minspan);
            headdiv.appendChild(clsspan);
            me.headdiv = headdiv;
            
            var bodydiv = document.createElement("div");
            bodydiv.id = "__bodydiv_" + me.id;
            bodydiv.style.width =  '100%';
            bodydiv.style.height = me.height - 20 - 4;
            bodydiv.style.backgroundColor = "white";
            bodydiv.style.lineHeight = "14px";
            bodydiv.style.wordBreak = "break-all";
            bodydiv.style.padding = "3px";
            var bodyframe = document.createElement("iframe");
            bodyframe.id = "__bodyframe_" + me.id;
            //bodyframe.style.zIndex = me.zIndex + 1;
            bodyframe.style.width =  '100%';
            bodyframe.style.height = parseInt(bodydiv.style.height) - 10;
            //bodyframe.src = "javascript:";
            me.bodyframe = bodyframe;
            bodydiv.appendChild(bodyframe);
            floatdiv.appendChild(headdiv);
            floatdiv.appendChild(bodydiv);
            
            var shadowdiv = document.createElement("div");
            shadowdiv.id = me.__shadowdiv + me.id;
            shadowdiv.style.width = me.width;
            shadowdiv.style.height = me.height;
            shadowdiv.style.top = me.top;
            shadowdiv.style.left = me.left;
            
            shadowdiv.style.position = "absolute";
            shadowdiv.style.backgroundColor = "black";
            shadowdiv.style.filter = "alpha(opacity=40)";
            
			hideframe.style.zIndex = me.zIndex;
			shadowdiv.style.zIndex = me.zIndex + 1;
			floatdiv.style.zIndex = me.zIndex + 2;
			
            //放入容器
            document.body.appendChild(hideframe);
            document.body.appendChild(floatdiv);
            document.body.appendChild(shadowdiv);
        };
        return ret;
    }(),
    setBodyFrameSrc: function(url){
        this.bodyframe.src = url
    },
	
	ispercent:function (source) {    
		return /^((-|\+)?\d{1,2}(\.\d+)?|100)%$/.test(source);   
	}
}
