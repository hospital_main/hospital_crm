//非模太窗口打印 0 模太窗口  1 普通窗口
var _printWindow = 0



var xmlHTTP

//String的处理
String.prototype.trim = function(){
  return this.replace(/(^\s+)|\s+$/g, "");
};

//function的处理
Function.READ = 1;
Function.WRITE = 2;
Function.READ_WRITE = 3;
Function.prototype.addProperty = function(sName, nReadWrite){
  nReadWrite = nReadWrite || Function.READ_WRITE;
  var capitalized = sName.charAt(0).toUpperCase() + sName.substr(1);
  if (nReadWrite & Function.READ)
    this.prototype["get" + capitalized] = new Function("", "return this._" + sName + ";");
  if (nReadWrite & Function.WRITE)
    this.prototype["set" + capitalized] = new Function(sName, "this._" + sName + " = " + sName + ";");
};

function XmlHttp() {
  this._object = new ActiveXObject("Microsoft.XMLHTTP");
}

XmlHttp.prototype.post = function() {
  if (arguments.length < 2) {
    alert("参数个数不对")
    return;
  }
  var postfix = ".viewhigh";
  if (arguments.length == 3) {
    postfix = postfix + arguments[2]
  }

  this._object.open("POST", arguments[0]+postfix, false);
  try {
    //var tt = String(arguments[1]).replace(/[&<>\u0080-\uffff]/g, _eaReplace);

    this._object.send("<root>"+arguments[1]+"</root>");
  } catch (exception){
    alert("连接失败，请检查网络！")
  }
}
XmlHttp.prototype.send = function() {
	if (arguments.length < 4) {
		alert("参数个数不对")
		return;
	}
	var cb=arguments[3];
	var postfix = ".viewhigh"+ arguments[2];
	var u=arguments[0]+postfix;
	var axml = new ActiveXObject("Microsoft.XMLHTTP");
	axml.open("POST",u,true);
	axml.onreadystatechange=function(){
		if(axml.readyState==4){
			if (axml.status>=400){
				alert("error loading document: "+u+" - "+axml.statusText+" - "+axml.responseText);
				__clearXmlHttpObj(axml);
				return;
      			};
			if(axml.status!=200){
				__clearXmlHttpObj(axml);
		  		return ;
		  	};
		  	cb(axml.responseText,axml.responseXML);
			__clearXmlHttpObj(axml);
		};
	};
	axml.send("<root>"+arguments[1]+"</root>");
}
XmlHttp.prototype.postWithProgress = function() {
	if (arguments.length < 4) {
		alert("参数个数不对")
		return;
	}
	var cb=arguments[3];
	var postfix = ".viewhigh"+ arguments[2];
	var u=arguments[0]+postfix;
	var axml = new ActiveXObject("Microsoft.XMLHTTP");
	axml.open("POST",u,true);
	axml.onreadystatechange=function(){
		if(axml.readyState==0){
			cb(axml.readyState,"... 正在初始化查询");
		}else if(axml.readyState==1){
			cb(axml.readyState,"... 预备提交查询请求");
		}else if(axml.readyState==2){
			cb(axml.readyState,"... 正在提交查询请求");
		}else if(axml.readyState==3){
			cb(axml.readyState,"... 正在下载数据");
		}else if(axml.readyState==4){
			if (axml.status>=400){
				alert("error loading document: "+u+" - "+axml.statusText+" - "+axml.responseText);
				__clearXmlHttpObj(axml);
				return;
      			};
			if(axml.status!=200){
				__clearXmlHttpObj(axml);
		  		return ;
		  	};
		  	cb(axml.readyState,axml.status,axml.responseText,axml.responseXML);
			__clearXmlHttpObj(axml);
		};
	};
	axml.send("<root>"+arguments[1]+"</root>");
}

function __clearXmlHttpFun(){};
function __clearXmlHttpObj(obj){
	obj.onreadystatechange=__clearXmlHttpFun;
	obj=null;
}
function setCookies(para_name,para_value){
    var expires = new Date();
    expires.setTime(expires.getTime() + 3 * 30 * 24 * 60 * 60 * 1000);
    document.cookie=para_name+"="+para_value+";expires="+expires.toGMTString();

}
function getCookies(para_name){
    var mcook=document.cookie.replace(";",",");
    mcook=mcook.replace(" ","");
	  var mma=mcook.split(",");
	  for(i=0;i<mma.length;i++){
	    mma2=mma[i].split("=");
	    if(mma2[0]==para_name){
	      return mma2[1];
	    }
	  }
	  return null;
}
function doMsg2(source, subFunc) {
			if (source.search(/<error>/)!=-1) {
				var error = source.substring(source.search(/<error>/)+"<error>".length, source.search(/<\/error>/))
				if (trim(subFunc)!='hide')
				alert(error);
				return false;
			} else if (source.search(/<msg>/)!=-1) {
				var msg = source.substring(source.search(/<msg>/)+"<msg>".length, source.search(/<\/msg>/))
				if (trim(subFunc)!='hide')
				alert(msg);
				return true;
			}
			return true;
		}
function openDialog(addr, argu, obj , scroll_flag) {
	var vUrl = document.URL.replace(/[^\/]*$/, '')

	vUrl = vUrl.replace(/<[.]*>/g, '')

	var w=screen.width-50;
	var h=screen.height-50;
	argu=argu.replace("[MAXW]",w+"px");
	argu=argu.replace("[MAXH]",h+"px");

  var count = 100;
  while (vUrl.search(/\.[^\/]*\/$/)!=-1 || vUrl.search(/<\/$/)!=-1) {
    vUrl = vUrl.replace(/[^\/]*\/$/g, "")
    if (--count<0) {
      alert('地址解析错：'+window.document.URL)
      return;
    }
  }
  if (addr.indexOf('http')!=-1) vUrl=''

	if (obj != null)
	  window._obj1 = obj;
	//if(addr.indexOf("/")!=0)
		addr=vUrl+addr;
	if (scroll_flag == "true")
	   return window.showModalDialog(addr, window, "help:no;status:no;resizable:yes;scroll:yes;"+argu)
	else
     return window.showModalDialog(addr, window, "help:no;status:no;resizable:yes;scroll:no;"+argu)
}

function _eaReplace( s )
{
	switch ( s )
	{
		case "&":
			return "&amp;";
		case "<":
			return "&lt;";
		case ">":
			return "&gt;";
		default:
			return "&#" + s.charCodeAt(0) + ";";
	}
}
xmlhttp = new XmlHttp;


try {
	window.document.oncontextmenu = window.top.onRightClick ;
}
catch(e) {
}

//判断activeX控件失去焦点
function CheckActivexFocus(table_id){
	var mo=document.getElementById(table_id);
	try{
		if(document.activeElement==mo){
			mo.SetActiveXFocus(false);
		}else{
			mo.SetActiveXFocus(true);
		}
	}catch(E){}
}
function getCompEmpCode(){
	_initGetCompEmpCode();
	return getEnvionmentValue("_CompnowEmpCode");
}
function getCompEmpName(){
	_initGetCompEmpCode();
	return getEnvionmentValue("_CompnowEmpName");
}
function getEmpCode(){
  return getEnvionmentValue("_nowEmpCode");
}
function getEmpName(){
  return getEnvionmentValue("_nowEmpName");
}
function getDBTime(){
  return getEnvionmentValue("_DBTime");
}
function getHistoryName(){
  return getEnvionmentValue("_historyName");
}
function getAcctYear(){
  return getEnvionmentValue("_Acctyear");
}
function getAcctYearBeginDate(){
  return getEnvionmentValue("_AcctyearBeginDate");
}
function getAcctYearEndDate(){
  return getEnvionmentValue("_AcctyearEndDate");
}
function getDrugDept(){
  return getEnvionmentValue("_drugDept");
}
function getDepot(){
	return getEnvionmentValue("_Depot");
}
function getBudgCode(){
  return getEnvionmentValue("_budgCode");
}
function getBudgYear(){
	return getEnvionmentValue("_budgYear");
}
function getUserID(){
	return getEnvionmentValue("_UserID");
}
function getCompCode(){
  return getEnvionmentValue("_CurCompCode");
}
function getCopyCode(){
  return getEnvionmentValue("_CurCopyCode");
}
function isAdmin(){
	return getEnvionmentValue("_IsAdmin");
}
function getCompCount(){
	return getEnvionmentValue("_CompCount");
}
function getCopyCount(){
	return getEnvionmentValue("_CopyCount");
}
function getUserSuperID(){
	return getEnvionmentValue("_UserSuperID");
}
function getCurCompCode(){
  return getEnvionmentValue("_CurCompCode");
}
function getCurCopyCode(){
  return getEnvionmentValue("_CurCopyCode");
}
function getLoginDate(){
	return getEnvionmentValue("_LoginDate");
}
function getCurModLevel(){
	return getEnvionmentValue("_CurModLevel");
}
function getCurModuleCode(){
	return _getCurModule();
}
function getCurModuleNo(){
	return getEnvionmentValue("_moduleNo");
}
function _getCurModule(){
	return getEnvionmentValue("_module");
}
//Modifier:Jack
//Date:2004-04-17
//Begin

/**
 *Method:getParaValueByCode
 *Function:According to Param<key:paraCode>
 *         get <value:paraValue>
 *
 *@param paraCode
 *
 *@return paraValue
 */
function getParaValueByCode(paraCode){
	var paraValue = '' ;
	if(getEnvionmentValue("_paraMap") != null && getEnvionmentValue("_paraMap").size()>0){
		if (getEnvionmentValue("_paraMap").containsKey(paraCode)){
			var element = getEnvionmentValue("_paraMap").get(paraCode);
			paraValue = element.value;
		}else{
			paraValue = '';
		}
	}else{
		paraValue = '' ;
	}
	return paraValue ;
}
/**
 *Get Current Stratagem NO.
 */
function getStratagemSeqNo(){
	return getEnvionmentValue("_stratagemSeqNo");
}
/**
 *Get Current Stratagrem Year
 */
function getStratagemYear(){
	return getEnvionmentValue("_stratagemYear");
}
/**
 *Get Current Month
 */
function getStratagemCurrentMonth(){
	return getEnvionmentValue("_stratagemCurrentMonth");
}

/**
 *Get State Flag
 */
function getStateFlag(){
  var tempFlag  = getEnvionmentValue("_stateFlag");
	return tempFlag.trim();
}
function _initGetCompEmpCode(){
	var c=getEnvionmentValue("_CompnowEmpCode");
	if(c!=null){
		return;
	}
	var p=getValuePairBySql("sys_user_employee","");
	var ec,en;
	if(p!=null){
		ec=p[0];
		en=p[1];
	}else{
		ec="";
		en="";
	}
	setEnvionmentValue("_CompnowEmpCode",ec);
	setEnvionmentValue("_CompnowEmpName",en);
}

/**
 *Initialize Operater Current Stratagem  Data
 */
function initOprStratagem() {

	if(getEnvionmentValue("_module")== 'perf'){
		// Params list
		var compCode = getCompCode() ;
		var userId = getUserID();

		// Post request to Server-Side
		window.xmlhttp.post("confirmOprStratagem","<user><subfunction>ConfirmOperaterStratagem</subfunction><compCode>" + compCode + "</compCode><userId>" + userId + "</userId></user>");
		// Receive Server-Side Data
		var paras = window.xmlhttp._object.responseText;
		if (paras.trim() == '') {
			//var _ErrorMsg = 'Data Load Error!';
			//alert(_ErrorMsg);
			//window.close();
			return false;
		}

		//Process Data
		var paraDatas = paras.split(",");

		setEnvionmentValue("_stratagemSeqNo",paraDatas[0]);
		setEnvionmentValue("_stratagemYear",paraDatas[1]);
		setEnvionmentValue("_stratagemCurrentMonth",paraDatas[2]);
		setEnvionmentValue("_stateFlag",paraDatas[3]);
		setEnvionmentValue("_stratagemName",paraDatas[3]);  
		setEnvionmentValue("_stratagemName",paraDatas[4]); 
	}
}

//END
var modeCodeToNameList = {
"00":"sys",
"01":"uinfo",
"02":"acct",
"03":"rep",
"04":"mate",
"05":"equi",
"06":"drug",
"07":"pote",
"08":"wage",
"09":"budg",
"10":"payctl",
"11":"ven",
"12":"cbcs",
"13":"perf",
"14":"bonus",
"15":"hisc",
"16":"dev",
"17":"imma",
"18":"fund",
"20":"stat",
"21":"hr",
"25":"pact",
"26":"work",
"22":"bankroll",
"27":"bdgt"
};

function getModCode(modValue){
	var mod_code="";
	if(modValue==null || modValue=="" || typeof(modValue)!="string"){
		return "";
	}
	for(var key in modeCodeToNameList){
		if(modeCodeToNameList[key]==modValue){
			mod_code=key;
			break;
		}
	}

	return mod_code;
}

/**页面装载或刷新时取得后台数据**/
function reloadData() {
  if(getEmpCode()==null || getEmpName()==null || getDBTime()==null){
    var strLocation = new String(window.location);
		//window.top.window.module = strLocation.replace(/^[^\?]*\?/g,'');
			window.top.window.module = strLocation.replace(/^[^\?]*\?/g,'').split('#')[0];
    window.xmlhttp.post("login","<user><subfunction>getsession</subfunction><module>"+window.module+"</module></user>");
    var str = window.xmlhttp._object.responseText;
    if (str.search(/<error>/)!=-1) {
      var error = str.substring(str.search(/<error>/)+"<error>".length, str.search(/<\/error>/));
      alert(error);
      window.close();
      return false;

    }
    var options = str.split(";");
    setEnvionmentValue("_module",window.module);
    setEnvionmentValue("_moduleNo",getModCode(window.module));
    setEnvionmentValue("_nowEmpCode",options[0]);
    setEnvionmentValue("_nowEmpName",options[1]);
    setEnvionmentValue("_historyName",options[2]);
    setEnvionmentValue("_DBTime",options[3]);

    if(getEnvionmentValue("_Acctyear")==null){
      setEnvionmentValue("_Acctyear",options[4].substring(0,4));
      setEnvionmentValue("_AcctyearBeginDate",options[5]);
      setEnvionmentValue("_AcctyearEndDate",options[6]);

	    //登陆用户id
	    setEnvionmentValue("_UserID",options[7]);

	    //当前登陆用户是否管理员、所操作的单位编码、账套编码、拥有单位总数、账套总数、登陆用户上级ID
	    setEnvionmentValue("_IsAdmin",options[8]);
	    setEnvionmentValue("_CompCode",options[9]);
	    setEnvionmentValue("_CopyCode",options[10]);
	    setEnvionmentValue("_CompCount",options[11]);
	    setEnvionmentValue("_CopyCount",options[12]);
	    setEnvionmentValue("_UserSuperID",options[13]);
	    setEnvionmentValue("_CurCompCode",options[14]);
	    setEnvionmentValue("_CurCopyCode",options[15]);
	    setEnvionmentValue("_LoginDate",options[16]);
	    setEnvionmentValue("_CurModLevel",options[17]);
    }
  }
}
reloadData();

/**************
  补充
**************/
function printView(printTable,subTitle,xslfile,topMessage, bottomMessage){
  var strXml = printTable.printXml
  var vHead = strXml.substring(strXml.search(/<thead>/i), strXml.search(/<\/thead>/i)+"</thead>".length);
  window._printXML = new ActiveXObject("Microsoft.XMLDOM");
  window._printXML.async=false;

  var vXml = new ActiveXObject("Microsoft.XMLDOM");
  vXml.async=false;
  vXml.loadXML(strXml);
  var vXsl = new ActiveXObject("Microsoft.XMLDOM");
  vXsl.async=false;

  var realxslfile = "";
  if(xslfile!=null){
  	realxslfile = xslfile;
  }else{
  	realxslfile = "printView.xsl";
  }

  vXsl.load(window.document.URL.replace(/\?.*$/g,'').replace(/[^\/]*$/, realxslfile));
  var temp = vXsl.xml
  if (temp==null || temp.length==0) {
    alert('此文件'+window.document.URL.replace(/[^\/]*$/, realxslfile)+'不符合XSL格式')
    return;
  }
  //如果是动态表头
  if(temp.search(/<thead\/>/)!=-1){
      temp = temp.replace(/<thead\/>/,vHead);
      vXsl.loadXML(temp)
  }

  //prompt('',vXsl.xml,'')
  if(topMessage != null)
    temp = temp.replace(temp.substring(temp.search(/<top>/),temp.search(/<\/top>/)+6),topMessage);
  if (bottomMessage != null)
    temp = temp.replace(temp.substring(temp.search(/<bottom>/),temp.search(/<\/bottom>/)+9),bottomMessage);
  if(subTitle != null)
    vXsl.loadXML(temp.replace(/<subtitle>.*<\/subtitle>/,"<subtitle>"+subTitle+"</subtitle>"));
  xmlHTTP = new ActiveXObject("Microsoft.XMLHTTP")
  xmlHTTP.open("POST", "print.viewhigh", false);
  try {
    // 设置原始的xml, 加编制单位
    window._printXml = vXml.transformNode(vXsl).replace(/<root/, "<root comName='"+window._comName+"'");
  	window._printXML.loadXML(vXml.transformNode(vXsl));
    xmlHTTP.send(window._printXML.xml);
  } catch (exception){
    alert(exception)
  }
  var screenHeight=window.screen.availHeight;
  var screenWidth=window.screen.availWidth;
  var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto"
  var win=window.showModalDialog(window.prefix+"base/print/print.html",window,dialogStyle)
}

// 表格单列checkbox处理 2-1
function tableDoCheck(headName, bodyName) {
  var heads = document.getElementsByName(headName)
  var bodys = document.getElementsByName(bodyName)

  for (var i=0; i<heads.length; i++) {
    heads[i].onclick = function() { // 全选或全取消
		  for(var j=0;j<bodys.length;j++) {
        if (bodys[j].checked!=this.checked) {
	        bodys[j].parentNode.parentNode.rowSubmit = true
	        bodys[j].checked=this.checked;
          if (bodys[j].checked==true)
            bodys[j].parentNode.value='<td>1</td>'
          else
            bodys[j].parentNode.value='<td>0</td>'
	      }
		  }
    }
  }

  var vFlag=true;
  for (var i=0; i<bodys.length; i++) {
    bodys[i].onclick = function() {
      this.parentNode.parentNode.rowSubmit = true

      // 修改属性
      if (this.checked==true)
        this.parentNode.value='<td>1</td>'
      else
        this.parentNode.value='<td>0</td>'

      if (trim(this.parentNode.show)==trim(this.parentNode.value)) {
        this.parentNode.parentNode.rowSubmit = null
        this.parentNode.value=null
      }

      // 全选或全取消
		  var flag;
	    for(var j=0;j<bodys.length;j++) {
	      if(bodys[j].checked==false){
				  flag=false;
				  break;
			  }
		  }

	    for(var j=0;j<heads.length;j++){
	  	 	if(flag==false)
  				heads[j].checked=false;
  			else
  			  heads[j].checked=true;
	    }
    }

    // 全选或全取消
	  if(bodys[i].checked==false && vFlag) {
			vFlag=false;
	  }
  }

  for(var j=0;j<heads.length;j++){
	 	if(vFlag==false)
			heads[j].checked=false;
		else
		  heads[j].checked=true;
  }
}

// 表格单列checkbox处理 2-2
function tableSave(btn, comp) {
  var submitStr = "";
  var tbody = comp.getElementsByTagName('tbody')[1]

  var vRecord=""
  for (var i=0; i<tbody.rows.length; i++) {
    if (tbody.rows[i].rowSubmit!=true) continue;

    for (var j=0; j<tbody.rows[i].cells.length; j++) {
      vRecord = vRecord + trim(tbody.rows[i].cells[j].value)
    }
    if (trim(vRecord)!='') {
      submitStr = submitStr + "<record>" + vRecord + "</record>"
      vRecord = ''
    }
  }
  if (trim(submitStr)=='') {
    alert('数据没有发生修改')
    return true;
  }
  window.xmlhttp.post(btn.name, submitStr)
  var str = window.xmlhttp._object.responseText
  if (!window.doMsg(str)) {
    return true;
  }

	for (var i=0; i<tbody.rows.length; i++) {
	  if (tbody.rows[i].rowSubmit!=true) continue;
	  tbody.rows[i].rowSubmit=null
	  for (var j=0; j<tbody.rows[i].cells.length; j++) {
      if (trim(tbody.rows[i].cells[j].value)!='' && trim(tbody.rows[i].cells[j].value)!=trim(tbody.rows[i].cells[j].show)) {
     		tbody.rows[i].cells[j].show = trim(tbody.rows[i].cells[j].value)
      }
    }
  }
}

function subInit(comp) {
	if (window.dialogArguments!=null)
		window.dialogArguments.subInit(comp)
	else
		window.top.window.subInit(comp)
}

/*
 * 文件输出方法，使用前先向后台发查询请求，根据返回结果输出成Excel
 * 第一个参数: 输出到文件的结果集
 * 第二个参数：xsl的名字，默认为本文件名加后缀.xsl
 * 第三个参数：提交的地址，则向后台提交为globalOutput_(第二个参数), 默认为globalOutput, 此参数尽可能不用
 */
function doOutput() {
  // 检查查询后是否有输出
  var str = window.xmlhttp._object.responseText
  if (arguments.length >= 1 && trim(arguments[0])!='') str=arguments[0]
  if (str.indexOf("td")==-1) {
    alert('没有可以输出成文件的内容')
    return false;
  }
  var vXml = new ActiveXObject("Microsoft.XMLDOM");
  vXml.async=false;
  vXml.loadXML(str.replace(/<root/, "<root comName='"+window._comName+"'"))

  //  第一个参数
  var xsl = document.URL.replace(/.html.*$/g, "_output.xsl");
  if (arguments.length >= 2 && trim(arguments[1])!='') xsl=arguments[1]
  var vXsl = new ActiveXObject("Microsoft.XMLDOM")
  vXsl.async=false;
  vXsl.load(xsl)

  // 第二个参数
  var subFunc = ''
  if (arguments.length >= 3 && trim(arguments[2])!='') subFunc=arguments[2]

  window.xmlhttp._object.open("open", "globalOutput.viewhigh?subFunc="+subFunc, false);
  window.xmlhttp._object.send(vXml.transformNode(vXsl))
  str = window.xmlhttp._object.responseText
  if (str.indexOf("<error>")!=-1) { // 错误检测
    alert(str.substring(str.search(/<error>/)+"<error>".length, str.search(/<\/error>/)))
    return false;
  }
  var path = window.prefix + str.substring(str.search(/<msg>/)+"<msg>".length, str.search(/<\/msg>/))
  window.open(path)
}

function getPrintRoot(strXml, xslFile,extData) {
  var vXsl = new ActiveXObject("Microsoft.XMLDOM");
  vXsl.async=false;
  if (trim(xslFile)=='')
    vXsl.load(window.document.URL.replace(/.html.*$/g, ".xsl"));
  else
    vXsl.load(xslFile);
  if(!vXsl.xml){
  	alert("不存在的打印格式文件!");
  	return;
  }

  var vXml = new ActiveXObject("Microsoft.XMLDOM");
  vXml.async=false;
  strXml=strXml.replace(new RegExp("&","gm"),"&amp;");
  vXml.loadXML(strXml)
  if(extData){
  	var endRow="<annex>";
  	for(var o in extData){
  		endRow+="<"+o+">"+extData[o]+"</"+o+">"
  	}
  	endRow+="</annex>";
  	var vXmlEndRow = new ActiveXObject("Microsoft.XMLDOM");
    vXmlEndRow.async=false;
    vXmlEndRow.loadXML(endRow);
    if(strXml != ""){
    	vXml.selectNodes("/root")[0].appendChild(vXmlEndRow.selectNodes("/annex")[0]);
    }
  }
  vXml.loadXML(vXml.transformNode(vXsl))
  return vXml
}

/**
 * 调整跨格
 */
function adjSpan(root) {
 try {
    //补齐cell, 注意有地方存 行宽度
    var colspan, rowspan, style;
    var trs = root.selectNodes("/root/thead/tr");
    for (var i=0; i<trs.length; i++) {
      var ths = trs[i].selectNodes("th");
      for (var j=0; j<ths.length; j++) {
        style = ths[j].getAttribute("style")
        if (style==null || style.indexOf("display:none")==-1) continue;

        // 依据上一个cell
        try  {
          if (i>0)
            rowspan=parseInt(trs[i-1].childNodes[j].getAttribute("rowspan"))
          else
            rowspan=1
        } catch(exception) {
          rowspan=1
        }
        if (isNaN(rowspan)) rowspan=1
        if (rowspan>1) {
          try  {
            colspan=parseInt(trs[i-1].childNodes[j].getAttribute("colspan"))
          } catch(exception) {
            colspan=1
          }
          if (isNaN(colspan)) colspan=1

          ths[j].setAttribute("colspan", colspan)
          ths[j].setAttribute("rowspan", new String(rowspan-1));
          if (ths[j].firstChild!=null) {
            ths[j].firstChild.nodeValue = trs[i-1].childNodes[j].firstChild.nodeValue
          } else
            ths[j].appendChild(trs[i-1].childNodes[j].firstChild.cloneNode(true))
          continue;
        }

        //  依据左一个cell
        try  {
          if (j>0)
            colspan=parseInt(ths[j-1].getAttribute("colspan"))
          else
            colspan=1
        } catch(exception) {
          colspan=1
        }
        if (isNaN(colspan)) colspan=1

        if (colspan>1) {
          try  {
            rowspan=parseInt(ths[j-1].getAttribute("rowspan"))
          } catch(exception) {
            rowspan=1
          }
          if (isNaN(rowspan)) rowspan=1
          ths[j].setAttribute("colspan", new String(colspan-1))
          ths[j].setAttribute("rowspan", rowspan)
          if (ths[j].firstChild!=null) {
            ths[j].firstChild.nodeValue=ths[j-1].firstChild.nodeValue
          } else {
            ths[j].appendChild(ths[j-1].firstChild.cloneNode(true))
          }
          continue;
        } else {
          alert('此cell i:'+i+'  j:'+j+' colspan:'+colspan+'不对')
          alert(style)
          alert(vXml.xml)
          return false;
        }
      }
    }
  } catch (exception) {
    alert(exception)
    return false;
  }
  return true;
}


/**
 * 设置页面
 * root, 数据源
 * pageWidth, pageHeight, 页长, 页宽, 单位mm, 不带单位
 * 页边踞, topMargin, bottomMargin, leftMargin, rightMargin, 单位mm, 不带单位
 * 固定列, fixCol
 * 行比例, 列比例, 字体比例, rowScale, colScale, fontScale
 * 以上参数必输
 */
function setupPage(root, pageWidth, pageHeight, topMargin, bottomMargin, leftMargin, rightMargin, fixCol,
  rowScale, colScale, fontScale) {
  //调整rowspan和colspan
  if (!adjSpan(root)) {
    return false;
  }

  var base = 3.7736
  // 初始化变量
  pageWidth=pageWidth*1
  pageHeight=pageHeight*1
  topMargin=topMargin*1
  bottomMargin=bottomMargin*1
  leftMargin=leftMargin*1
  rightMargin=rightMargin*1
  fixCol=fixCol*1
  rowScale=rowScale*1
  colScale=colScale*1

  var rootNode = root.selectNodes("/root")[0]
  rootNode.setAttribute("pageWidth", (pageWidth*base));
  rootNode.setAttribute("pageHeight", (pageHeight*base));
  rootNode.setAttribute("topMargin", (topMargin*base));
  rootNode.setAttribute("bottomMargin", (bottomMargin*base));
  rootNode.setAttribute("leftMargin", (leftMargin*base));
  rootNode.setAttribute("rightMargin", (rightMargin*base));
  rootNode.setAttribute("fixCol", fixCol);
  rootNode.setAttribute("rowScale", rowScale);
  rootNode.setAttribute("colScale", colScale);
  rootNode.setAttribute("fontScale", fontScale);

  var fixHeight=25  // 最开始的Table cellspacing=1
  // 计算标题头
  var titles = root.selectNodes("/root/top/title")
  for (var i=0; i<titles.length; i++) {
    var temp = getNum(titles[i].getAttribute("style"), "height:", 40)
    rootNode.setAttribute("titleHeight", temp)
    fixHeight=fixHeight+temp+3
  }
  titles = root.selectNodes("/root/top/maintitle") // 历史原因含 maintitle
  for (var i=0; i<titles.length; i++) {
    var temp = getNum(titles[i].getAttribute("style"), "height:", 40)
    rootNode.setAttribute("titleHeight", temp)
    fixHeight=fixHeight+temp+3
  }
  var subtitles = root.selectNodes("/root/top/subtitle")
  for (var i=0; i<subtitles.length; i++) {
    var temp = getNum(subtitles[i].getAttribute("style"), "height:", 22)
    rootNode.setAttribute("subTitleHeight", temp)
    fixHeight=fixHeight+temp+3
  }
  var topTrs = root.selectNodes("/root/top/tr")
  for (var i=0; i<topTrs.length; i++) {
    var style = topTrs[i].getAttribute("style");
    if (style == null) style=""
    if (style.indexOf("height:")==-1)
      topTrs[i].setAttribute("style", (style+";height:18;"))
    fixHeight=fixHeight+getNum(topTrs[i].getAttribute("style"), "height:", 18)+3
  }

  // 计算标题尾
  var bottomTrs = root.selectNodes("/root/bottom/tr")
  for (var i=0; i<bottomTrs.length; i++) {
    var style = bottomTrs[i].getAttribute("style");
    if (style == null) style=""
    if (style.indexOf("height:")==-1)
      bottomTrs[i].setAttribute("style", (style+";height:18;"))
    fixHeight=fixHeight+getNum(bottomTrs[i].getAttribute("style"), "height:", 18)+3
  }

  // 计算表头
  var headTrs = root.selectNodes("/root/thead/tr")
  for (var i=0; i<headTrs.length; i++) {
    var temp = getNum(headTrs[i].getAttribute("style"), "height:", 24)
    temp = temp*rowScale
    headTrs[i].setAttribute("height", temp)
    fixHeight=fixHeight+temp+3
  }

  // 加上上下边距
  fixHeight=fixHeight+(topMargin+bottomMargin)*base

  // 修正fixHeight
  if (_printWindow==0 && window.dialogArguments._fixHeight!=null && !isNaN(window.dialogArguments._fixHeight))
    fixHeight=fixHeight+window.dialogArguments._fixHeight*rowScale

  if (_printWindow==1 && window.opener._fixHeight!=null && !isNaN(window.opener._fixHeight))
    fixHeight=fixHeight+window.opener._fixHeight*rowScale


  var rowB=1, rowE=1, colB=1, colE=1;
  var num=0
  var rowgroup = root.createNode(1, "rowGroup", "");
  var bodyTrs=root.selectNodes("/root/tbody/tr");
  for (var i=0; i<bodyTrs.length; i++) {
    var temp = (getNum(bodyTrs[i].getAttribute("style"), "height:", 22)+3)*rowScale
    bodyTrs[i].setAttribute("height", temp)
    if (i==0) {
      num=fixHeight+temp
      continue;
    }
    if (parseInt((num)/(pageHeight*base))<parseInt(((num+temp))/(pageHeight*base))) {
      rowE=i; // 由于i 从0 计数,所以需要加1,
      var newNode=root.createNode(1, "page", "")
      newNode.setAttribute("rowB", rowB)
      newNode.setAttribute("rowE", rowE)
      rowB=i+1
      rowgroup.appendChild(newNode)
      num=fixHeight+temp
    } else
      num+=temp
  }
  // 最后一行
  var newNode=root.createNode(1, "page", "")
  newNode.setAttribute("rowB", rowB)
  newNode.setAttribute("rowE", bodyTrs.length)
  rowgroup.appendChild(newNode)

  // 计算固定列头
  var fixWidth = 20
  var cols=root.selectNodes("/root/colgroup/col")
  for (var i=0; i<cols.length && i<fixCol; i++) {
    var temp = (getNum(cols[i].getAttribute("style"), "width:", 80)+3)*colScale
    cols[i].setAttribute("width", temp)
    fixWidth+=temp
  }

  // 加上 左右边距
  fixWidth=fixWidth+(leftMargin+rightMargin)*base

  var colgroup = root.createNode(1, "colgroup", "")
  num=0;
  colB=fixCol+1;
  for (var i=fixCol; i<cols.length; i++) {
    var temp = (getNum(cols[i].getAttribute("style"), "width:", 80)+3)*colScale
    cols[i].setAttribute("width", temp)
    if (i==fixCol) {
      num=temp+fixWidth
      continue;
    }

    if (parseInt((num)/(pageWidth*base))<parseInt(((num+temp))/(pageWidth*base))) {
      colE=i;
      newNode=root.createNode(1, "page", "")
      newNode.setAttribute("colB", colB)
      newNode.setAttribute("colE", colE)
      colB=i+1
      colgroup.appendChild(newNode)
      num=temp+fixWidth;
      // 修正表头
      for (var j=0; j<headTrs.length; j++) {
        var ths = headTrs[j].selectNodes("th");
        if (ths.length != cols.length) {
          alert('请在thead中放置th元素,而不是td元素, 同时需要补齐隐藏cell 或者 colgroup/col 的数量和thead/th的数量不一致')
          break;
        }
        var style=ths[i].getAttribute("style")
        if (style!=null && style.indexOf("display:none")!=-1) {
          // 检查上一个cell是否跨行
          if (j>0) {
            try {
              var rowspan = parseInt(headTrs[j-1].selectNodes("th")[i].getAttribute("rowspan"))
              if (!isNaN(rowspan) && rowspan>1) {
                continue;
              }
            } catch(ex) {
            }
          }
          ths[i].setAttribute("style", style.replace(/display:none/, ''))
          try {
            var rowspan = parseInt(ths[i].getAttribute("rowspan"))
            if (rowspan>1)
              j+=(rowspan-1)
          } catch(e) {
          }
        }
      }
    } else
      num+=temp
  }

  // 最后一列
  newNode=root.createNode(1, "page", "")
  newNode.setAttribute("colB", colB)
  newNode.setAttribute("colE", cols.length)
  colgroup.appendChild(newNode)

  // 合并生成pagegroup, 列同样的道理
  var pagegroup=root.createNode(1, "pagegroup", "")
  mergeNode(pagegroup, rowgroup, colgroup, 'true')

  var root = root.selectNodes("/root")[0]
  while (root.selectNodes("/root/pagegroup").length>0)
    root.removeChild(root.selectNodes("/root/pagegroup")[0])

  root.appendChild(pagegroup)
}


function getNum(comp, str, value) {
  if (comp==null) {
    return value;
  }
  var idx = comp.indexOf(str);
  if (idx==-1) {
    return value;
  }
  var temp = comp.substring(idx)
  idx = temp.indexOf(";")
  if (idx==-1)
    idx = temp.length
  try {
    return parseFloat(temp.substring(0, idx).replace(/[^0-9\.]*/, ''))
  } catch (e) {
    return value;
  }
}

/**
 * flag - true 先横向排序, 再纵向排序
 *      - false 相反
 */
function mergeNode(group, rows, cols, flag) {
  var xmlDoc= new ActiveXObject("Microsoft.XMLDOM");
  if (flag=='true') {
    for (var i=0; i<rows.childNodes.length; i++) {
      for (var j=0; j<cols.childNodes.length; j++) {
        var temp = xmlDoc.createNode(1, "page", "")
        temp.setAttribute("rowB", rows.childNodes[i].getAttribute("rowB"))
        temp.setAttribute("rowE", rows.childNodes[i].getAttribute("rowE"))
        temp.setAttribute("colB", cols.childNodes[j].getAttribute("colB"))
        temp.setAttribute("colE", cols.childNodes[j].getAttribute("colE"))
        group.appendChild(temp)
      }
    }
  } else {
    for (var j=0; j<cols.childNodes.length; j++) {
      for (var i=0; i<rows.childNodes.length; i++) {
        var temp = root.createNode(1, "page", "")
        temp.setAttribute("rowB", rows.childNodes[i].getAttribute("rowB"))
        temp.setAttribute("rowE", rows.childNodes[i].getAttribute("rowE"))
        temp.setAttribute("colB", cols.childNodes[j].getAttribute("colB"))
        temp.setAttribute("colE", cols.childNodes[j].getAttribute("colE"))
        group.appendChild(temp)
      }
    }
  }
}

/**
 * 打印预览
 */
function printView1() {
  if (_printWindow==1) {
    var win = window.open(window.prefix+"base/print1/print1.html", "printView")
    win.focus()
    return false;
  }

  var screenHeight=window.screen.availHeight;
  var screenWidth=window.screen.availWidth;
  var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto"
  var win=window.showModalDialog(window.prefix+"base/print1/print.html",window,dialogStyle)
}

function barCodePrint() {
  var dialogStyle="dialogHeight:"+600+"px;dialogWidth:"+400+"px;dialogTop:200px;dialogLeft:200px;status:no;scrollbars:yes"
  var win=window.showModalDialog(window.prefix+"base/barCodePrint/message.html",window,dialogStyle);
}

function equiBarCodePrint() {
  var dialogStyle="dialogHeight:"+200+"px;dialogWidth:"+300+"px;dialogTop:260px;dialogLeft:300px;status:no;scrollbars:yes"
  var win=window.showModalDialog(window.prefix+"base/barCodePrint/equimessage.html",window,dialogStyle);
}

/**
 * 文件上传
 */
function uploadFile(path,actionName){
  // 创建 ADO-stream 对象
  var ado_stream = new ActiveXObject("ADODB.Stream");

  // 创建包含默认头信息和根节点的 XML文档
  var xml_dom = new ActiveXObject("MSXML2.DOMDocument");
  xml_dom.loadXML('<?xml version="1.0" ?> <root/>');
  // 指定数据类型
  xml_dom.documentElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com:datatypes");

  // 创建一个新节点，设置其为二进制数据节点
  var l_node1 = xml_dom.createElement("file_upload");
  l_node1.dataType = "bin.base64";
  // 打开Stream对象，读源文件
  ado_stream.Type = 1;  // 1=adTypeBinary
  ado_stream.Open();
  try {
    ado_stream.LoadFromFile(path);
  } catch(exception) {
    alert("文件有误，不能上传！");
    return false;
  }
  // 将文件内容存入XML节点
  //l_node1.nodeTypedValue = ado_stream.Read(-1); // -1=adReadAll
  this._object = new ActiveXObject("Microsoft.XMLHTTP");
  this._object.open("POST", actionName, false);
  this._object.send(ado_stream.Read(-1));
  ado_stream.Close();
  return window.doMsg(_object.ResponseText);
}

/**
 * 日期比较
 */
function dateCompare(d1,d2){
  var first = new Date(d1.replace(/-.*/g,''),d1.replace(/^[^-]*-|-[^-]*$/g,''),d1.replace(/.*-/g,''));
  var end = new Date(d2.replace(/-.*/g,''),d2.replace(/^[^-]*-|-[^-]*$/g,''),d2.replace(/.*-/g,''));
  if (first >= end ) {
    return true;
  } else {
    return false;
  }
}

//是不是电子邮件地址
function isEmail(src) {
	var isEmail1  = /^\w+([\.\-]\w+)*\@\w+([\.\-]\w+)*\.\w+$/;
	var isEmail2  = /^.*@[^_]*$/;
	var rg1=new RegExp(isEmail1);
	var rg2=new RegExp(isEmail2);
   	return (rg1.test(src) && rg2.test(src));
}
//是不是电话号码
function isPhone(src) {
	var Phone = /^(\+\d+ )?(\(\d+\) )?[\d ]+$/;
	var rg2=new RegExp(Phone);
   	return (rg2.test(src));
}
//是不是年龄
function isAge (src) {
	var Phone = /^(1[0-2]\d|\d{1,2})$/;
	var rg2=new RegExp(Phone);
   	return (rg2.test(src));
}
//是不是货币值
function  isMoney(src) {
	var Phone = /^[-]?(\d+(\.\d*)?)$/;
	var rg2=new RegExp(Phone);
   	return (rg2.test(src));
}
//是不是IP地址
function  isIP(src) {
	ip_ip = '(25[0-5]|2[0-4]\\d|1\\d\\d|\\d\\d|\\d)';
    	ip_ipdot = ip + '\\.';
    	address = new RegExp('^'+ip_ipdot+ip_ipdot+ipdot+ip_ip+'$');
   	return (address.test(src));
}
//是不是邮编
function  isZipCode(src) {
	var Phone = "^[\\d]{6}$";
	var rg2=new RegExp(Phone);
   	return (rg2.test(src));
}
//是不是身份证号
function  isIdCorrect(src) {
	var isEmail1  =/^\d{15}$/;
	var isEmail2  =/^\d{18}$/;
	var rg1=new RegExp(isEmail1);
	var rg2=new RegExp(isEmail2);
   	return (rg1.test(src) || rg2.test(src));
}
//是不是规范编码
function isRuleCode(value){
	var va=value.replace(/-/g,"");
	if(value.indexOf("-")<0||value.indexOf("--")>=0)
		return false;
	digitExp = new RegExp("^(\\d)*$");
   	if(digitExp.test(va)==false){
   		return false;
   	}
   	if(value.substr(0,1)=="-"||value.substr(value.length-1,1)=="-")
   		return false;
   	return true;
}
function isCanEditYear(){
	var p=getValuePairBySql("acct_get_can_edit_year","<comp_code>"+getCompCode()+"</comp_code><copy_code>"+getCopyCode()+"</copy_code><vouch_year>"+getAcctYear()+"</vouch_year>");
	if(p!=null&&p[0]=="0")
		return true;
	else
		return false;
}
function setAcctCommonPara(prefix){
	var o1,o2;
	o1=setAcctCommonParaGetObj(prefix+"beginDate");
	if(o1!=null){
		o1.maxdate="javascript:window.selAcctYearEndDate";
		o1.mindate="javascript:window.selAcctYearBeginDate";
	}
	o2=setAcctCommonParaGetObj(prefix+"endDate");
	if(o2!=null){
		o2.maxdate="javascript:window.selAcctYearEndDate";
		o2.mindate="javascript:window.selAcctYearBeginDate";
	}
	if(o1!=null&&o2!=null){
		o2.greatthan=o1.name;
	}
	o2=setAcctCommonParaGetObj(prefix+"compCode");
	if(o2){
		o2.value=getCompCode();
	}
	o2=setAcctCommonParaGetObj(prefix+"copyCode");
	if(o2){
		o2.value=getCopyCode();
	}
	o2=setAcctCommonParaGetObj(prefix+"acctYear");
	if(o2!=null){
		o2.value=getAcctYear();
	}
	o2=setAcctCommonParaGetObj(prefix+"acctSubj");
	if(o2!=null){
		o2.para="<comp_code>"+getCompCode()+"</comp_code><copy_code>"+getCopyCode()+"</copy_code><acct_year>"+getAcctYear()+"</acct_year>";
		o2.refresh();
	}
	o2=setAcctCommonParaGetObj(prefix+"vouchType");
	if(o2!=null){
		o2.para="<comp_code>"+getCompCode()+"</comp_code><copy_code>"+getCopyCode()+"</copy_code>";
		o2.refresh();
	}
}
function setAcctCommonParaGetObj(n){
	if(eval("typeof("+n+")")!="undefined")
		return eval(n);
	else
		return null;
}
function getValuePairBySql(sql, para) {
    var datXml = getDict(sql, para)
    if (!datXml)
        return null;
    var paras = datXml.documentElement.getElementsByTagName("para");
    if (paras.length < 1)
        return null;
    else {
        var res = new Array()
          var attributes = paras[0].attributes;
        for (var i = 0; i < attributes.length; i++) {
            res[attributes[i].name] = attributes[i].value
        }
        res[0] = paras[0].getAttribute("code")
        res[1] = paras[0].getAttribute("value")
        return res
    }
}
function getValueArrayBySql(sql,para){
	var datXml=getDict(sql,para);
	if(!datXml)
		return null;
	var paras=datXml.documentElement.getElementsByTagName("para");
	if(paras.length<1)
		return null;
	var ar=[];
	var attrs=paras[0].attributes;
	for(var i=0;i<attrs.length;i++){
		ar.push(attrs[i].value);
	}
	return ar;
}
function dateAddByType(dateValue,type,num){
	var p=getValuePairBySql("commom_function_dateadd","<date>"+dateValue+"</date><type>"+type+"</type><num>"+num+"</num>");
	if(p===null)
		return "";
	else
		return p[0];
}
function getCurrentDate(){
    var d=new Date();
		var year=d.getFullYear();
		var month=d.getMonth()+1;
		var day=d.getDate();
		year='0000'+year;
		year=year.substring(year.length-4);
	  month='00'+month;
	  month=month.substring(month.length-2);
	  day='00'+day;
	  day=day.substring(day.length-2);
	  return year+'-'+month+'-'+day;
}
function getMonthFirstAndLastDay(){
	 	var d=new Date();
	 	var y=d.getYear();
	 	var lastDay=[31,28,31,30,31,30,31,31,30,31,30,31]
	 	if(((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0))
  	   		maxDay = 29;
  	   	else
  	   		maxDay = 28;
  	   	lastDay[1]=maxDay;
  	   	var year=d.getFullYear();
		var month=d.getMonth()+1;
		var day=d.getDate();
		year='0000'+year;
		year=year.substring(year.length-4);
		month='00'+month;
		month=month.substring(month.length-2);
		day='00'+lastDay[d.getMonth()];
		day=day.substring(day.length-2);
		return [year+'-'+month+'-01',year+'-'+month+'-'+day];
	 }
function getCurrentDateTime(){
    var myDate=new Date();
		var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
		var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
		var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
		year='0000'+year;year=year.substring(year.length-4)
	  month='00'+month;month=month.substring(month.length-2)
	  day='00'+day; day=day.substring(day.length-2)
	  return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString()
  }
function getPrintTime(){
    return getCurrentDateTime();
  }
function getPrintBottomStr(){
    var bottom= "<bottom><tr><td colspan='7' style='text-align:right;'>"+"打印日期: "+getPrintTime()+" 制表人："+getEmpName()+"</td></tr>"+
	            "<tr><td colspan='7' style='text-align:right'>编制单位："+window._comName+"</td></tr>"+
							"<tr><td colspan='7' style='text-align:right'>北京东软望海科技有限公司  制作</td></tr></bottom>";
		return bottom;
  }
function getPrintTopStr(tableTitle,yearMonth){
	units="";
	if(typeof(document["_VhMoneyFormateName"])!="undefined")
		units="金额："+document["_VhMoneyFormateName"];
	//if(typeof(units)=="undefined")
	//	units="金额：万元";

	var tt="<top><maintitle>"+tableTitle+"</maintitle><subtitle>"+yearMonth+"</subtitle>";
             tt=tt+"<tr><td ></td><td style='text-align:right;' > 页号：[页码]/[总页数]</td></tr>";
             tt=tt+"<tr><td >"+getHistoryName()+"</td><td style='text-align:right;' >"+units+"</td></tr>";
             tt=tt+"";
             tt=tt+"</top>";
    return tt;
}

//2007-3-30 将原来printTableData替换为当前printTableData+printXmlData，根据lzk nj
function printTableData(tableCtn,xslFileName,tableTitle,subTitle,extData){
        return printXmlData(tableCtn.serverXml,xslFileName,tableTitle,subTitle,extData);
}
function printXmlData(xmlData,xslFileName,tableTitle,subTitle,extData){
	var root = getPrintRoot(xmlData, xslFileName,extData);
     	var topStr =getPrintTopStr(tableTitle,subTitle);

        var vXml = new ActiveXObject("Microsoft.XMLDOM");
        vXml.async=false;
        vXml.loadXML(topStr);
        root.selectNodes("/root")[0].appendChild(vXml.selectNodes("/top")[0]);

        var bottomStr=getPrintBottomStr();
	var vXml2 = new ActiveXObject("Microsoft.XMLDOM");
    	vXml2.async=false;
    	vXml2.loadXML(bottomStr);
    	root.selectNodes("/root")[0].appendChild(vXml2.selectNodes("/bottom")[0]);

        window._printXml = root;
        window.outputFlag = true;
        printView1();
        return true;
}

function printActiveXData(tableCtn,xslFileName,tableTitle,subTitle){
		var root = getPrintRoot(tableCtn.GetAllShowData(), xslFileName);
     	var topStr =getPrintTopStr(tableTitle,subTitle);
        var vXml = new ActiveXObject("Microsoft.XMLDOM");
        vXml.async=false;
        vXml.loadXML(topStr);
        root.selectNodes("/root")[0].appendChild(vXml.selectNodes("/top")[0]);

        var bottomStr=getPrintBottomStr();
				var vXml2 = new ActiveXObject("Microsoft.XMLDOM");
    	vXml2.async=false;
    	vXml2.loadXML(bottomStr);
    	root.selectNodes("/root")[0].appendChild(vXml2.selectNodes("/bottom")[0]);

        window._printXml = root;
        window.outputFlag = true;
        printView1();
        return true;
}

function printDubangTableData(tableCtn,xslFileName,tableTitle,subTitle){
		var root = getPrintRoot(tableCtn.serverXml, xslFileName);
     	var topStr =getPrintTopStr(tableTitle,subTitle);

        var vXml = new ActiveXObject("Microsoft.XMLDOM");
        vXml.async=false;
        vXml.loadXML(topStr);
        root.selectNodes("/root")[0].appendChild(vXml.selectNodes("/top")[0]);

        var bottomStr=getPrintBottomStr();
				var vXml2 = new ActiveXObject("Microsoft.XMLDOM");
    	vXml2.async=false;
    	vXml2.loadXML(bottomStr);
    	root.selectNodes("/root")[0].appendChild(vXml2.selectNodes("/bottom")[0]);

        window._printXml = root;
        window.outputFlag = true;
        printView3();
        return true;
}

function printXmlToCellByXsltFile2(xmlData,xslFile,setData,dirPrint,isPrview,pageUrl){

	if(dirPrint=="multiPrint"){
		_printXmlDataToCell_Dir_=dirPrint;
		_printXmlDataToCell_Param_=setData;
		printXmlDataToCellDlg2(false,null);
		return;
	}

	var vXsl = new ActiveXObject("Microsoft.XMLDOM");
	vXsl.async=false;
	vXsl.load(xslFile)
	var vXml = new ActiveXObject("Microsoft.XMLDOM");
	vXml.async=false;
	vXml.loadXML(xmlData);
	if(setData){
  	var endRow="<annex>";
  	for(var o in setData){
  		endRow+="<"+o+">"+setData[o]+"</"+o+">"
  	}
  	endRow+="</annex>";
  	var vXmlEndRow = new ActiveXObject("Microsoft.XMLDOM");
    vXmlEndRow.async=false;
    vXmlEndRow.loadXML(endRow);
    if(xmlData != ""){
    	if (vXml.selectNodes("/root")[0]) {
    		vXml.selectNodes("/root")[0].appendChild(vXmlEndRow.selectNodes("/annex")[0]);
    	}
    }
  }
	printStringDataToCell2(vXml.transformNode(vXsl),setData,dirPrint,isPrview,pageUrl);

}
function printXmlToCellByXsltFile3(xmlData,xslFile,setData){
	printXmlToCellByXsltFile(xmlData,xslFile,setData,false,false,null,'useSpecialXsl')
}
////Cell Print BEGIN d35f169cd160b4b3
function printXmlToCellByXsltFile(xmlData,xslFile,setData,dirPrint,isPrview,pageUrl,printPage){
	if(dirPrint=="multiPrint"){
		_printXmlDataToCell_Dir_=dirPrint;
		_printXmlDataToCell_Param_=setData;
		printXmlDataToCellDlg(false,null);
		return;
	}
	var vXsl = new ActiveXObject("Microsoft.XMLDOM");
	vXsl.async=false;
	vXsl.load(xslFile)
	var vXml = new ActiveXObject("Microsoft.XMLDOM");
	vXml.async=false;
	vXml.loadXML(xmlData);
	if(setData){
  	var endRow="<annex>";
  	for(var o in setData){
  		endRow+="<"+o+">"+setData[o]+"</"+o+">"
  	}
  	endRow+="</annex>";
  	var vXmlEndRow = new ActiveXObject("Microsoft.XMLDOM");
    vXmlEndRow.async=false;
    vXmlEndRow.loadXML(endRow);
    if(xmlData != ""){
    	if (vXml.selectNodes("/root")[0]) {
    		vXml.selectNodes("/root")[0].appendChild(vXmlEndRow.selectNodes("/annex")[0]);
    	}
    }
  }
	printStringDataToCell(vXml.transformNode(vXsl),setData,dirPrint,isPrview,pageUrl,printPage);
}
function printStringDataToCell(data,setData,dirPrint,isPrview,pageUrl,printPage){
	var cellXml = new ActiveXObject("Microsoft.XMLDOM");
	cellXml.async=false;
	cellXml.loadXML(data);
	printXmlDataToCell(cellXml,setData,dirPrint,isPrview,pageUrl,null,printPage);
}

function printStringDataToCell2(data,setData,dirPrint,isPrview,pageUrl){
	var cellXml = new ActiveXObject("Microsoft.XMLDOM");
	cellXml.async=false;
	cellXml.loadXML(data);
	printXmlDataToCell2(cellXml,setData,dirPrint,isPrview,pageUrl);
}

var _printXmlDataToCell_Data_=null;
var _printXmlDataToCell_Param_=null;
var _printXmlDataToCell_Dir_=null;
var _printXmlDataToCell_Prview_=null;
var _printXmlDataToCell_PageUrl_=null;
var _printXmlDataToCell_Emp_=getEmpName();
var _printXmlDataToCell_Init=false;
var _printXmlDataToCell_ShowDlg=true;
function printXmlDataToCell(data,setData,dirPrint,isPrview,pageUrl,openFrame,printPage){
	if(setData!=null){
		var ks;
		for(var o in setData){
			ks=o.split("_");
			if(ks.length!=3)
				continue;
			if(ks[0]!="thead"&&ks[0]!="tbody"&&ks[0]!="tfoot")
				continue;
			var segs=data.getElementsByTagName(ks[0]);
			if(segs.length==0)
				continue;
			var trs=segs[0].getElementsByTagName("tr");
			if(trs.length==0||trs.length<parseInt(ks[1]-1,10))
				continue;
			var tds=trs[ks[1]-1].getElementsByTagName("td");
			if(tds.length==0||tds.length<parseInt(ks[2]-1,10))
				continue;
			tds[ks[2]-1].text=setData[o];
		}
	}
	_printXmlDataToCell_Data_=data;
	_printXmlDataToCell_Param_=setData;
	_printXmlDataToCell_Dir_=dirPrint;
	_printXmlDataToCell_Prview_=isPrview;
	if(_printXmlDataToCell_ShowDlg==false)
		return;
	printXmlDataToCellDlg(dirPrint,openFrame,printPage);
}

function printXmlDataToCell2(data,setData,dirPrint,isPrview,pageUrl,openFrame){
	if(setData!=null){
		var ks;
		for(var o in setData){
			ks=o.split("_");
			if(ks.length!=3)
				continue;
			if(ks[0]!="thead"&&ks[0]!="tbody"&&ks[0]!="tfoot")
				continue;
			var segs=data.getElementsByTagName(ks[0]);
			if(segs.length==0)
				continue;
			var trs=segs[0].getElementsByTagName("tr");
			if(trs.length==0||trs.length<parseInt(ks[1]-1,10))
				continue;
			var tds=trs[ks[1]-1].getElementsByTagName("td");
			if(tds.length==0||tds.length<parseInt(ks[2]-1,10))
				continue;
			tds[ks[2]-1].text=setData[o];
		}
	}
	_printXmlDataToCell_Data_=data;
	_printXmlDataToCell_Param_=setData;
	_printXmlDataToCell_Dir_=dirPrint;
	_printXmlDataToCell_Prview_=isPrview;
	if(_printXmlDataToCell_ShowDlg==false)
		return;
	printXmlDataToCellDlg2(dirPrint,openFrame);
}

function printXmlDataToCellDlg(dirPrint,openFrame,printPage){
	var win_prefix=window.prefix;
	var screenHeight=window.screen.availHeight;
	var screenWidth=window.screen.availWidth;
	if((""+dirPrint).indexOf("export")>=0){
		screenWidth=250;
		screenHeight=60;
	}

	if(!printPage){
		var printPage="base/print1/printh2c.html"
	}else if(printPage=="useSpecialXsl"){
		var printPage="base/print1/printh2cf4sx.html"
	}
	var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto";
	
	if(openFrame==null||openFrame=='ModalDialog'){
		_printXmlDataToCell_Init=true;
		var win=window.showModalDialog(win_prefix+printPage,window,dialogStyle);

	}else if(openFrame=='_new'){
		_printXmlDataToCell_Init=true;
		var win=window.open(win_prefix+printPage,'New');

	}else{
		_printXmlDataToCell_Init=false;
		//var f=eval(openFrame);
		//f.document.location.href=	win_prefix+"base/print1/printh2c.html";
	}
}

function printXmlDataToCellDlg2(dirPrint,openFrame){
	var win_prefix=window.prefix;
	var screenHeight=window.screen.availHeight;
	var screenWidth=window.screen.availWidth;
	if((""+dirPrint).indexOf("export")>=0){
		screenWidth=250;
		screenHeight=60;
	}
	var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto";

	if(openFrame==null||openFrame=='ModalDialog'){
		_printXmlDataToCell_Init=true;
		var win=window.showModalDialog(win_prefix+"base/print1/printh2c.html",window,dialogStyle);


	}else if(openFrame=='_new'){
		_printXmlDataToCell_Init=true;
		var win=window.open(win_prefix+"base/print1/printh2c.html",'New');

	}else{
		_printXmlDataToCell_Init=false;
		//var f=eval(openFrame);
		//f.document.location.href=	win_prefix+"base/print1/printh2c.html";
	}

}

////Cell Print END 3ff51ca139496acb
var _printCellByTemplate_sql=null;
var _printCellByTemplate_ds=null;
function printCellByTemplate(sqlId,ds,isArray,customerPath){
	var win_prefix=window.prefix;
	_printCellByTemplate_sql=sqlId;
	_printCellByTemplate_ds=ds;
	var hide=false;
	var path=win_prefix+'base/print3/set.html';
	
	if(customerPath!=null && typeof(customerPath)!='undefined'){
		path=win_prefix+customerPath;
	}

	if(isArray!=null && typeof(isArray)!='undefined'){
		/*
		if (_pageIndex >= ds.length) {
			ds = null;
			return;
		}
		*/
		path = win_prefix+'base/print3/set2.html';
		if (isArray == "true") {
			div=window.document.createElement("<div style='position:absolute;top:-500;left:-500'>");
			window.document.appendChild(div);
			div.innerHTML="<iframe  src=\""+path+"?load=<isArray>"+isArray+"</isArray>\" width=\"100\" height=\"100\" scrolling=\"no\">";
		}	else {
			openDialog(path+"?load=<isArray>"+isArray+"</isArray>",'dialogWidth:'+screen.availWidth+';dialogHeight:'+screen.availHeight);
		}
	}else{
		if(ds.hide){
			if(ds.hide==true)
				hide=true;
		}
		if(hide==true){
			var frm=null;
			if(window._printCellByTemplateFrm){
				frm=window._printCellByTemplateFrm;
			}else{
				div=window.document.createElement("<div style='position:absolute;top:-500;left:-500'>");
				window.document.appendChild(div);
				div.innerHTML="<iframe  src=\""+path+"\" width=\"100\" height=\"100\" scrolling=\"no\">";
				frm=div.getElementsByTagName("iframe")[0];
			}
			//frm.document.location.href=path;alert(frm.document.URL)
		}else{
			openDialog(path,'dialogWidth:'+screen.availWidth+';dialogHeight:'+screen.availHeight);
		}
	}
}
function printView3() {
  var screenHeight=window.screen.availHeight;
  var screenWidth=window.screen.availWidth;
  var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto"
  var win=window.showModalDialog(window.prefix+"base/print1/print3.html",window,dialogStyle)
}
function clearIFdlgTag(u){
	return _clearIFdlgTag(u);
}
function _clearIFdlgTag(u){
	if(window.__ifdlgtag){
		if(u.indexOf("&"+window.__ifdlgtag+"=")>0)
			u=u.substr(0,u.indexOf("&"+window.__ifdlgtag+"="));
	}
	return u;
}
function getPageArg(name){
	if(!document._bufPageArgsXMLObject){
		var urlStr=_clearIFdlgTag(document.URL.toString());
		var xmlStr="<root>"+urlStr.substr(urlStr.indexOf("?load=")+6)+"</root>";
		document._bufPageArgsXMLObject=new ActiveXObject("Microsoft.XMLDOM");
		document._bufPageArgsXMLObject.loadXML(xmlStr);
	}
	var xml=document._bufPageArgsXMLObject;
	var path="root/"+name;
	if(xml.selectNodes(path).length>0){
		 if(xml.selectNodes(path)[0].childNodes.length>0){
		 		return xml.selectNodes(path)[0].childNodes[0].data;
		 	}
	}
		return "";
}
function getBittyXmlData(xml,name){
	if(!document._bufParseBittyXmlDataXMLObject){
		var urlStr=document.URL.toString();
		var xmlStr="<root>"+xml+"</root>";
		document._bufParseBittyXmlDataXMLObject=new ActiveXObject("Microsoft.XMLDOM");
		document._bufParseBittyXmlDataXMLObject.loadXML(xmlStr);
	}
	var xml=document._bufParseBittyXmlDataXMLObject;
	var path="root/"+name;
	if(xml.selectNodes(path).length>0){
		 if(xml.selectNodes(path)[0].childNodes.length>0)
		 	return xml.selectNodes(path)[0].childNodes[0].data;
	}
		return "";
}

function _numberAddCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1]+'00' : '.0000';
	x2 = x2.substr(0,3)
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}

	return x1 + x2;
}
String.prototype.addCommas=function(){return _numberAddCommas(this);}
Number.prototype.addCommas=function(){return _numberAddCommas(""+this);}
function getRuleCodeParentCode(code,allowLetter,sqlId,param){
	//正确时返回 父编码,不正确时返回 null
	var xml=getDict(sqlId,param);
	var para=xml.getElementsByTagName("para")[0];
	var format=para.getAttribute("code");
	var name=para.getAttribute("value");
	code=code.replace(/(^\s+)|\s+$/g, "");
	var regStr="^(\\d)*$";
	var regErr="只允许数字";
	if(typeof(allowLetter)!="undefined"&&allowLetter==true){
		regErr="只允许数字和字母";
		regStr="^([a-zA-Z0-9_.])*$";
	}
	document["_CODE_FORMATE_ERR_TEXT_"+sqlId]="不正确的"+name+" : "+format+","+regErr;
	if(code=="")
		return null;
   	digitExp = new RegExp(regStr);
   	if(digitExp.test(code)==false){
   		return null;
   	}

	var cs=format.split("-");
	var len=0,plen=0;
	for(var i=0;len<code.length&&i<cs.length;i++){
		plen=len;
		len+=parseInt(cs[i],10);
	}
	if(len!=code.length)
		return null;
	if(plen==0)
		return "";
	else
		return code.substr(0,plen);
}
function getCheckRuleCodeFormatErr(formatId){
	return document["_CODE_FORMATE_ERR_TEXT_"+formatId];
}
function checkRuleCodeFormat(code,formatId,allowLetter){
  if(formatId=="0101" || formatId=="0103" || formatId=="0104" || formatId=="0105" || formatId=="0201" || formatId=="0202" || formatId=="0203" || formatId=="0203")allowLetter = true;
	return checkRuleCodeFormatBySql(code,allowLetter,"sys_code_format_para","<code>"+formatId+"</code>",true);
}
function checkRuleCodeFormatBySql(code,allowLetter,sqlId,param,showErr){
	var res=getRuleCodeParentCode(code,allowLetter,sqlId,param);
	if(res==null&&showErr==true)
		alert(getCheckRuleCodeFormatErr(sqlId));
	return res;
}
function showRuleCodeText(obj,formatId,prefixText,comp_copy){
	var xml;
	if(typeof(comp_copy)!="undefined"&&comp_copy!=null&&comp_copy!="")
		xml=getDict("sys_code_format_comp_copy",comp_copy+"<code>"+formatId+"</code>");
	else
		xml=getDict("sys_code_format_para","<code>"+formatId+"</code>");
	var para=xml.getElementsByTagName("para")[0];
	if(para){
		var format=para.getAttribute("code");
		if(typeof(prefixText)=="string")
			format=prefixText+"："+format;
		else
			format="编码规则："+format;
		obj.innerHTML=format;
	}
}
function getSysParaData(comp,copy,code){
	return getValuePairBySql("sys_code_format_comp_copy","<comp>"+comp+"</comp><copy>"+copy+"</copy><code>"+code+"</code>");
}
function setButtonUseStatus(btnObj,u){
	if(u==true){
		btnObj.disabled=false;
	}else{
		btnObj.disabled=true;
	}
}
function getJsGuid(){
	var d=new Date();
	var s="";
	for(var i=0;i<6;i++)
		s+=parseInt(Math.random()*10000);
	return "guid"+d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds()+s;
}
function getErrorText(key){
	return 	G_CONST_ERROR_MSG[key];
}
var G_CONST_ERROR_MSG={
		"ruleCode":"不正确的编码规则，请用XX-XX-XX...形式!"
	}

function openNewVouchDlg(pk){
  var screenHeight=window.screen.availHeight;
  var screenWidth=window.screen.availWidth;
  var prardata0318=getSysParaData(getCompCode(),getCopyCode(),"0318");
  var paradata0333 = getSysParaData(getCompCode(),getCopyCode(),"0333");
  var paradata0309 = getSysParaData(getCompCode(),getCopyCode(),"0309");
  prardata0318[0]='否'

  if(paradata0333[0]=="往来集中核销"){
  	pk = pk + "<P0333>1</P0333>";
  }else{

  //无论核销方式如何，凭证界面均可核销
  	pk = pk + "<P0333>0</P0333>";
  }

	
  var paradata0334 = getSysParaData(getCompCode(),getCopyCode(),"0334");
  if(paradata0334[0]=="摘要显示"){
  	pk = pk + "<P0334>asst</P0334>";
  }else{
  	pk = pk + "<P0334>subj</P0334>";
  }
  
  var is_temp = false
  var vid = pk.indexOf("<vouch_id>");
	if(vid>=0){
		vid = pk.substr(pk.indexOf("<vouch_id>") + 10,pk.indexOf("</vouch_id>") - pk.indexOf("<vouch_id>") - 10 );
		if(vid != ""){
			var isMulti = getValuePairBySql("acctVouchGetIsMoreItems" , "<vouchId>" + vid + "</vouchId>")
			if(isMulti[0]=="0"){
				is_temp = false ;
			}else{
				is_temp = true ;
			}
		}
	}
  pk = pk + '<P0309>' + paradata0309[0] + '</P0309>'  
  
	var isOld = "0";  
	if(pk.indexOf("<old_flag>") >= 0){
		isOld = pk.substr(pk.indexOf("<old_flag>") + 10,pk.indexOf("</old_flag>") - pk.indexOf("<old_flag>") - 10 );
	}
	
	window.showModalDialog(window.prefix+'hbos/acct/vouch/query/newinsert/main.html?load='+pk, window, "help:no;status:no;resizable:no;scroll:no;minimize:yes;dialogWidth:" + screenWidth + ";dialogHeight:" + screenHeight + ";")
	/*
  if(prardata0318[0]=="否" && is_temp == false && isOld != '1'){ 
  	window.showModalDialog(window.prefix+'hbos/acct/vouch/query/newinsert/main.html?load='+pk, window, "help:no;status:no;resizable:no;scroll:no;dialogWidth:" + screenWidth + ";dialogHeight:" + screenHeight + ";")
  }else{
  	window.showModalDialog(window.prefix+'hbos/acct/vouch/query/insert/main.html?load='+pk, window, "help:no;status:no;resizable:no;scroll:no;dialogWidth:800px;dialogHeight:600px;");
	}
	*/
}

function openVouchDlg(pk){
  var screenHeight=window.screen.availHeight;
  var screenWidth=window.screen.availWidth;
  var prardata0318 = getSysParaData(getCompCode(),getCopyCode(),"0318");
  var paradata0309 = getSysParaData(getCompCode(),getCopyCode(),"0309");
  var paradata0333 = getSysParaData(getCompCode(),getCopyCode(),"0333");
  prardata0318[0]='否'

  if(paradata0333[0]=="往来集中核销"){
  	pk = pk + "<P0333>1</P0333>";
  }else{

  	pk = pk + "<P0333>0</P0333>";
  }

  var paradata0334 = getSysParaData(getCompCode(),getCopyCode(),"0334");
  if(paradata0334[0]=="摘要显示"){
  	pk = pk + "<P0334>asst</P0334>";
  }else{
  	pk = pk + "<P0334>subj</P0334>";
  }
  
  var is_temp = false
  var vid = pk.indexOf("<vouch_id>");
	if(vid>=0){
		vid = pk.substr(pk.indexOf("<vouch_id>") + 10,pk.indexOf("</vouch_id>") - pk.indexOf("<vouch_id>") - 10 );
		if(vid != ""){
			var isMulti = getValuePairBySql("acctVouchGetIsMoreItems" , "<vouchId>" + vid + "</vouchId>")
			if(isMulti[0]=="0"){
				is_temp = false ;
			}else{
				is_temp = true ;
			}
		}
	}
  pk = pk + '<P0309>' + paradata0309[0] + '</P0309>'  
  
	var isOld = "0";  
	if(pk.indexOf("<old_flag>") >= 0){
		isOld = pk.substr(pk.indexOf("<old_flag>") + 10,pk.indexOf("</old_flag>") - pk.indexOf("<old_flag>") - 10 );
	}
	
	window.showModalDialog(window.prefix+'hbos/acct/vouch/query/newinsert/main.html?load='+pk, window, "help:no;status:no;resizable:no;scroll:no;minimize:yes;dialogWidth:" + screenWidth + ";dialogHeight:" + screenHeight + ";")
	/*
  if(prardata0318[0]=="否" && is_temp == false && isOld != '1'){
  	window.showModalDialog(window.prefix+'hbos/acct/vouch/query/newinsert/main.html?load='+pk, window, "help:no;status:no;resizable:no;scroll:no;dialogWidth:" + screenWidth + ";dialogHeight:" + screenHeight + ";")
  }else{
  	window.showModalDialog(window.prefix+'hbos/acct/vouch/query/insert/main.html?load='+pk, window, "help:no;status:no;resizable:no;scroll:no;dialogWidth:800px;dialogHeight:600px;");
  }
  */
	//window.showModalDialog(window.prefix+'hbos/acct/vouch/query/insert/main.html?load='+pk, window, "help:no;status:no;resizable:no;scroll:no;dialogWidth:800px;dialogHeight:600px;");
	//window.showModalDialog(window.prefix+'hbos/acct/vouch/query/newinsert/main.html?load='+pk, window, "help:no;status:no;resizable:no;scroll:no;dialogWidth:800px;dialogHeight:550px;");
}
function openAdvanceQuery(btnObj){
	var pk="<table>"+btnObj.table+"</table><view>"+btnObj.view+"</view><sql>"+btnObj.name+"</sql>"
	if(btnObj.title)
		pk+="<title>"+btnObj.title+"</title>";
	if(btnObj.page)
		pk+="<page>"+btnObj.page+"</page>";
	if(btnObj.xsl)
		pk+="<xsl>"+btnObj.xsl+"</xsl>";
	openDialog(window.prefix+"hbos/sys/integratequery/set.html?load="+pk,"dialogWidth:760px;dialogHeight:500px;");
}
function getAdvanceQueryResult(tab){
	//tab.intQueryName
	//tab.intQueryParams
	window.xmlhttp.post(tab.intQueryName,tab.intQueryParams)
  var str = window.xmlhttp._object.responseText
	return str;
}
function printAdvanceQuery(btnObj){
	if(btnObj.query){
	}else{
		alert("请选择查询");
		return false;
	}
	var data=new Object();
	data["thead_1_1"]=btnObj.schemaName;
	var pk="<table>"+btnObj.table+"</table><sid>"+btnObj.schemaId+"</sid><comp>"+getCompCode()+"</comp><copy>"+getCopyCode()+"</copy><year>"+getAcctYear()+"</year><oper>"+getEmpCode()+"</oper>"
	xmlhttp.post(btnObj.name,pk);
	var xsl=window.prefix+"hbos/sys/integratequery/queryView.xsl";
	if(btnObj.printXsl&&btnObj.printXsl.length>0)
		xsl=btnObj.printXsl;
	printXmlToCellByXsltFile(xmlhttp._object.responseText,xsl,data,false,false);
}
function openSmallVouchListDlg(param){
	openDialog(window.prefix+'hbos/acct/monthend/adjcur/vouches.html?load='+param, 'dialogWidth:600px;dialogHeight:323px;');
}

function openSmallVouchListDlgNew(param){
	openDialog(window.prefix+'hbos/acct/monthend/adjcurNew/vouches.html?load='+param, 'dialogWidth:600px;dialogHeight:323px;');
}

_parseDateParam_=[31,29,31,30,31,30,31,31,30,31,30,31];
function parseDate(d){
	if(d==null)
		return null;
	var s="";
	if(d.indexOf(".")>0)
		s=".";
	else if(d.indexOf("-")>0)
		s="-"
	else if(d.indexOf("/")>0)
		s="/";
	var ps=[0,0,0];
	if(s!=""){
		ps=d.split(s);
		if(ps.length!=3)
			return null;
	}else{
		ps[0]=d.substr(0,4);
		ps[1]=d.substr(4,2);
		ps[2]=d.substr(6,2);
	}
	ps[0]=parseInt(ps[0],10);
	ps[1]=parseInt(ps[1],10);
	ps[2]=parseInt(ps[2],10);
	if(isNaN(ps[0])||isNaN(ps[1])||isNaN(ps[2])){
		return null;
	}
	if(ps[0]<1000||ps[0]>9999)
		return null;
	if(ps[1]<1||ps[1]>12)
		return null;
	if(ps[2]<1||ps[2]>_parseDateParam_[ps[1]-1])
		return null;
	if(ps[1]==2&&(!(((ps[0]%4==0)&&(ps[0]%100!=0))||(ps[0]%400==0))))
		if(ps[2]>28)
			return null;
	var res=new Date();
	//res.setYear(ps[0]);
	//res.setMonth(ps[1]-1);
	//res.setDate(ps[2]);
	//当前月份为31天而设置月份小于31天出现月份顺延加1的情况
	res.setFullYear(ps[0],ps[1]-1,ps[2])
	return res;
}

function encodeXmlPara(tag,value){
	return "&lt;"+tag+"&gt;"+value+"&lt;/"+tag+"&gt;";
}
function outputDataToXsl(printXml,outputXsl,outputSubFunc){
	var xsl = ""
	if(typeof(outputXsl)!="undefined"&&outputXsl!=null)
		xsl=trim(outputXsl)
	if (xsl=='')
		xsl = window.prefix+'base/xsl/output.xsl'
	var subFunc =""
	if(typeof(outputSubFunc)!="undefined"&&outputSubFunc!=null)
		trim(outputSubFunc)

	var vXml = printXml;
	var vXsl = new ActiveXObject("Microsoft.XMLDOM");
	vXsl.async=false;
	vXsl.load(xsl);
	window.xmlhttp._object.open("open", "globalOutput.viewhigh?subFunc="+subFunc, false);
	window.xmlhttp._object.send(vXml.transformNode(vXsl))
	var str = window.xmlhttp._object.responseText
	if (str.indexOf("<error>")!=-1) { // 错误检测
		alert(str.substring(str.search(/<error>/)+"<error>".length, str.search(/<\/error>/)))
		return false;
	}
	var path = window.prefix + str.substring(str.search(/<msg>/)+"<msg>".length, str.search(/<\/msg>/))
	path=path.replace(/\/\//g,"/");
	window.open(path)
}
function setDefaultAcctDate(d1,be){
	var p=getValuePairBySql("acct_get_default_date","<comp_code>"+getCompCode()+"</comp_code><copy_code>"+getCopyCode()+"</copy_code><login_date>"+getLoginDate()+"</login_date>");
	if(p==null)
		return false
	d1.value=p[0];
	if(be){
		d1.mindate=getAcctYearBeginDate();
		d1.maxdate=getAcctYearEndDate();
	}
}
function setDefaultAcctDate2(d1,d2){
	var p=getValuePairBySql("acct_get_default_date2","<comp_code>"+getCompCode()+"</comp_code><copy_code>"+getCopyCode()+"</copy_code><login_date>"+getLoginDate()+"</login_date>");
	if(p==null)
		return false
	if(d1!=null){
		d1.value=p[0];
		d1.mindate=getAcctYearBeginDate();
		d1.maxdate=getAcctYearEndDate();
	}
	if(d2!=null){
		d2.value=p[1];
		d2.mindate=getAcctYearBeginDate();
		d2.maxdate=getAcctYearEndDate();
	}
	if(d1!=null&&d2!=null)
		d2.greatthan=d1.name;
}
function setDefaultAcctDate3(d1,d2){
	var p=getValuePairBySql("acct_get_default_date3","<comp_code>"+getCompCode()+"</comp_code><copy_code>"+getCopyCode()+"</copy_code><login_date>"+getLoginDate()+"</login_date>");
	if(p==null)
		return false
	if(d1!=null){
		d1.value=p[0];
		d1.mindate=getAcctYearBeginDate();
		d1.maxdate=getAcctYearEndDate();
	}
	if(d2!=null){
		d2.value=p[1];
		d2.mindate=getAcctYearBeginDate();
		d2.maxdate=getAcctYearEndDate();
	}
	if(d1!=null&&d2!=null)
		d2.greatthan=d1.name;
}
function setDefaultAcctMonth(m1,y1,ym){
	var p=getValuePairBySql("acct_get_default_month","<comp_code>"+getCompCode()+"</comp_code><copy_code>"+getCopyCode()+"</copy_code><login_date>"+getLoginDate()+"</login_date>");
	if(p==null)
		return false
	if(m1)
		m1.setValue(p[1]);
	if(y1)
		y1.setValue(p[0]);
	if(ym)
		ym.setValue(""+p[0]+p[1]);
	return p;
}
function disabledBtnByAccFlag(){
	var res=false;
	var p=getValuePairBySql("acct_vouch_get_year_acc_flag","<comp_code>"+getAcctYear()+"</comp_code>");
	if(p==null||p[1]=="1")
		res=true;
	if(arguments.length>0&&res==true){
		for(var i=0;i<arguments.length;i++){
			arguments[i].disabled=true;
		}
	}
	return res;
}
///delay onload begin



function execinit(cmd){
	cmd();
	return;
	__onload_cmd__=cmd;
	_execinit();
}

function _execinit(){
	var init=true;

	init=_execinit_test();

	if(init==false){
		if(__onload_timer__==null)
			__onload_timer__=window.setInterval(_execinit,10);
	}
	if(init==true){
		if(__onload_timer__!=null)
			window.clearInterval(__onload_timer__);
		__onload_cmd__();
	}
}
function _execinit_test(){
	var tags=["input","div"];
	for(var i=0;i<tags.length;i++){
		if(_execinit_test2(tags[i])==false)
			return false;
	}
	return true;
}
function _execinit_test2(tag){
	var nodes=document.getElementsByTagName(tag);
	if(nodes.length>0)
		for(var i=0;i<nodes.length;i++){
			if(nodes[i].getAttribute("className")!=null&&nodes[i].getAttribute("id")!=null){
				if(_execinit_test3(nodes[i],nodes[i].getAttribute("className"),nodes[i].getAttribute("id"))==false)
					return false;
			}
		}
	return true;
}
function _execinit_test3(n,clazz,id){
	for(var k in __onload_classToMethod){
		if(clazz==k&&eval("typeof(n."+__onload_classToMethod[k]+")")=="undefined")
			return false;
	}
	return true;
}
var __onload_classToMethod={
		"inputTextA":"check",
		"inputSelect":"check",
		"inputInteger":"check",
		"inputDecimal":"check",
		"lineCtn":"check",
		"tableCtn":"refresh",
		"reportCtn":"reset"
	}
__onload_timer__=null;
__onload_cmd__=null;
////delay onload end
////LZK ADD END
function spell_code(obj,spell,define){
	// 可以接受空对象，如果只生成一个码，可以通过一下引用 spell_code(obj,null,define)
	// 可以接受空对象，如果只生成一个码，可以通过一下引用 spell_code(obj,null,define)
  var srcTree = new ActiveXObject("Microsoft.XMLDOM");
  srcTree.async=false;
  srcTree.load("/base/scripts/data.xml");
  var spell_code="";//存放检索出来的拼音码
  var define_code="";//存放检索出来的用户定义码
  var temp;//存放每次取得输入汉字的值
  var objNodeList = srcTree.getElementsByTagName("record");

	var objText = obj.value.trim();//去除多出的空格
//  alert(objNodeList.item(100).attributes.item(0).nodeValue);
  for(var i=0; i< obj.value.length; i++){
  		temp=  obj.value.charAt(i);
	   for (var j=0; j<objNodeList.length; j++) {
	     if(temp ==objNodeList.item(j).attributes.item(0).nodeValue )
	    	{
		     spell_code = spell_code + objNodeList.item(j).attributes.item(1).nodeValue ;
		     define_code = define_code + objNodeList.item(j).attributes.item(2).nodeValue  ;
		    }
	 	 }
  }

 if(spell !=null) spell.value = spell_code;
 if(define != null)  define.value = define_code;
 return;

}
//add by wsj
    function retrieve_change(){//变换当前输入法，改变系统的参数设置，然后将参数从新榜定，刷新数据源。
 			var win
   		if(event.keyCode =='123'){
			  win= window.self;//取得本身的窗口句柄

			  if(window.dialogArguments !=null)
			 		win=window.dialogArguments;//如果嵌套多层模态窗口，则取得其上层的非摸态窗口，然后由其再取得最上层中的元素，进行赋值
    		while(win.dialogArguments !=null){
   					win=win.dialogArguments;
   				}

 				setRetrieveMethod(win);//输入法切换
				retrieveRefresh();//对数据源进行刷新
 		  }
    }

    function getRetrieve(){//得到当前检索方式
    		var win= window.self;//取得本身的窗口句柄
			  if(window.dialogArguments !=null)
			  	win=window.dialogArguments;//如果嵌套多层模态窗口，则取得其上层的非摸态窗口，然后由其再取得最上层中的元素，进行赋值
			  while(win.dialogArguments !=null){
			        win=win.dialogArguments;
			   		 }
    		return win.top.retrieve_method;
    	}

	function setRetrieveMethod(win){//输入法只有两种属性0，1 ,对检索方式切换进行赋值
			  	 if(win.top.retrieve_method==0){
			     	  win.top.retrieve_method=1;
	           	win.top.retrieve.innerHTML="检索方式：自定义"
			     }
		   	 else{
			     	  win.top.retrieve_method=0;
		     		  win.top.retrieve.innerHTML="检索方式：拼音"
		      }
  	}

  function retrieveRefresh(){//将所有检索方式的下拉框，更具页面检索方式进行刷新

    	 var objNodeList = window.document.getElementsByTagName("input");
		   for (var i=0; i<objNodeList.length; i++) {
		   		if(objNodeList[i].load == "retrieve_method")
		   	  	objNodeList[i].refresh();

		   	   }
    }
//wsj


/*************** select类 *******/
function SelectObj() {}

SelectObj.prototype.setPara= function() {
  if (arguments.length != 1) {
    alert("参数个数不对")
    return;
  }
  this.para = arguments[0]
}
SelectObj.prototype.setLoad= function() {
  if (arguments.length != 1) {
    alert("参数个数不对")
    return;
  }
  this.load = arguments[0]
}
SelectObj.prototype.setXML = function() {
  if (arguments.length != 1) {
    alert("参数个数不对")
    return;
  }
  this.XML = arguments[0]
}

SelectObj.prototype.setCode = function() {
  if (arguments.length != 1) {
    alert("参数个数不对")
    return;
  }
  this.code = arguments[0]
}

SelectObj.prototype.select = function(para) {
	var p="";
	if(typeof(para)!="undefined"){
		p="?load="+para;
	}
  openDialog(window.prefix+'base/select/find.html'+p, 'dialogWidth:900px;dialogHeight:423px;')
}

selectObj = new SelectObj
///////////////VHFile BEGIN

function VHFile(frm){
	//状态值, 0表示成功, 负数表示失败
	this.status=0;
//消息
	this.message="";
//不包括客户端路径
	this.clientFileName="";
//可以包含路径
	this.serverFileName="";
	this.tempTable="";
	this.rename="true";
	this.uploadROOT="/upload/";
	this.realFileName="";
	//_____VHFileCreatFromTimer=window.setInterval(this.__createForm,200);
	document.vhFile=this;
	this.__err_msg=["操作成功",
									"文件已经存在",
									"文件不存在",
									"文件不存在",
									"没有文件",
									"没有权限",
									"未知错误"];
	this.__has_submit=false;
	this.__vhFileUploadFrame=frm;
};
//客户端选择文件
VHFile.prototype.choose=function(){
		this.__getForm().clientFileName.click();
		var fn=this.__getForm().clientFileName.value;
		if(fn.indexOf("\\")>0)
			fn=fn.substr(fn.lastIndexOf("\\")+1);
		this.clientFileName=fn;
		this.serverFileName=fn;
		return this.__getForm().clientFileName.value;
	};
VHFile.prototype.getChooseFile=function(){
		return this.__getForm().clientFileName.value;
	}
//如果文件已存在, 返回-1, 并赋值message
VHFile.prototype.insert=function(){
		this.__getForm().tempTable.value=this.tempTable;
		this.__getForm().rename.value=this.rename;
		this.__getForm().todo.value="insert";
		this.__submit();
	};
VHFile.prototype.insert2=function(){
	this.__getForm().tempTable.value=this.tempTable;
	this.__getForm().todo.value="insert2";
	this.__submit();
};
//如果文件不存在, 返回-2, 并赋值message
VHFile.prototype.update=function(){
				this.__getForm().todo.value="update";
				this.__submit();
			};
//如果删除不成功,返回-3, 并赋值message
VHFile.prototype.del=function(){
	this.__getForm().tempTable.value=this.tempTable;
				this.__getForm().todo.value="delete";
				this.__submit();
			};
VHFile.prototype.__onload=function(){
				this.rename="true";
				this.uploadROOT="/upload/";
				this.__has_submit=false;
				this.status=this.__getForm().status.value;
				this.realFileName=this.__getForm().realFileName.value;
				this.serverFileName=this.__getForm().serverFileName.value;
				this.message=this.__err_msg[this.__getForm().message.value];
				if(typeof(this.onload)!="undefined"&&this.onload!=null&&this.__getForm().isinit.value!="true")
					this.onload();
			};
VHFile.prototype.__getForm=function(){
				return this.__vhFileUploadFrame.document.vhUploadForm;
			};
VHFile.prototype.__submit=function(){
				if(this.__has_submit==true)
					return ;
				this.__has_submit=true;
				this.__getForm().uploadROOT.value=this.uploadROOT;
				this.__getForm().serverFileName.value=this.serverFileName;
				if(this.__getForm().tempTable.value!=''){
					this.__getForm().action="vhExeclFileUpload.vhexeclfile";
				}else{
					this.__getForm().action="vhFileUpload.vhfile";
				}
				this.__getForm().submit();
			};

////////////////////////////////
function VhFileUploader(frm){
	//_____VHFileCreatFromTimer=window.setInterval(this.__createForm,200);
	document.vhFileUploader=this;
	this.___callbackFun=null;
	this.___callbackObj=null;
	this.__has_submit=false;
	this.__vhFileUploadFrame=frm;
	this.__oldDocument=null;
};
VhFileUploader.prototype.clear=function(){
	var inps=this.__vhFileUploadFrame.document.getElementsByTagName("input");
	for(var i=0;i<inps.length;i++){
		if(inps[i].name.indexOf("upfile")==0)
			inps[i].parentNode.removeChild(inps[i]);
	}
};
VhFileUploader.prototype.choose=function(indx){
	if(this.__oldDocument!=document)
		this.clear();
	this.__oldDocument=document;
	if(eval("typeof(this.__getForm().upfile"+indx+")")=="undefined"){
		var ft=this.__vhFileUploadFrame.document.createElement("<input type=file id='upfile"+indx+"' name='upfile"+indx+"'/>");
		this.__vhFileUploadFrame.document.getElementById("fileSpanId").appendChild(ft);
	}
	eval("this.__getForm().upfile"+indx+".click()");
	return eval("this.__getForm().upfile"+indx+".value");
};
VhFileUploader.prototype.submit=function(fun,obj){
	if(this.__has_submit==true)
		return ;
	if(obj){
		this.___callbackObj=obj;
		obj.disabled=true;
	}
	this.___callbackFun=fun;
	this.__has_submit=true;
	this.__getForm().submit();
};
VhFileUploader.prototype.__onload=function(message,files){
	this.__has_submit=false;
	if(this.___callbackFun==null)
		return;
	this.___callbackFun(message,files);
	if(this.___callbackObj!=null)
		this.___callbackObj.disabled=false;
	this.___callbackFun=null;
	this.___callbackObj=null;
};
VhFileUploader.prototype.__getForm=function(){
	return this.__vhFileUploadFrame.document.vhUploadForm;
};

//////////////VHFile END

//titleStr格式:"银行科目,业务日期,结算方式,票据号,摘要,借方金额,贷方金额"
function creatActiveXHeadXML(titleStr){
		var titleArray = titleStr.split(",");
		var titleXML = "<?xml version='1.0' encoding='GBK'?><root><msg/><error/><record><total/></record><activeHead/><thead/><tbody><tr>";
		for(t in titleArray){
			titleXML += "<td>" + trim(titleArray[t]) + "</td>";
		}
		titleXML += "</tr></tbody></root>";
		return titleXML;
}
function creatActiveXHeadT2head(titleStr){
		var titleArray = titleStr.split(",");
		var titleXML = "<t2head><tr>";
		for(t in titleArray){
			titleXML += "<td>" + trim(titleArray[t]) + "</td>";
		}
		titleXML += "</tr></t2head>";
		return titleXML;
}
//检查编码内容的合法性
function isLegalCode(str){
	 if(!str)
	 		return false;

	 for(var i = 0; i < str.length; i++){
   	var c = str.charCodeAt(i);
   	//alert(c)
   	if(c > 255)
   		return false;
   }

   return true;
}

function checkCode(inputObj){
	if(!isLegalCode(inputObj.value)){
		alert("提交的内容中包含非法字符，请重新输入！");
		inputObj.focus();
		return false;
	}

	inputObj.value = trim(inputObj.value);
	return true;
}

function getCurDateBetween(subBodyObj) {
	return getValuePairBySql( "acct_get_default_date2", "<a>"+getCompCode()+"</a><b>"+getCopyCode()+"</b><c>" + getLoginDate() + "</c>");
}

function getBillDefaultDate(subBodyObj, cur_comp_code, cur_copy_code, table_name, date_field) {
  subBodyObj.load = "acct_bill_default_date";
  subBodyObj.para = "<a>"+cur_comp_code+"</a><b>"+cur_copy_code+"</b><c>"+table_name+"</c><d>"+date_field+"</d><e>"+getLoginDate()+"</e>";
  //alert(subBodyObj.para);
 	subBodyObj.post();

  var a = subBodyObj.getOneDim();
  if(!a)
  	return "";

  return a[0];
}

function getAcctAnalysisCurDateBetween(subBodyObj) {
  subBodyObj.load = "acct_analysis_cur_date_between";
  subBodyObj.para = "<a>"+getCompCode()+"</a><b>"+getCopyCode()+"</b><c>" + getAcctYear() + "</c>";
  //alert(subBodyObj.para);
 	subBodyObj.post();

  return subBodyObj.getOneDim();
}

function getUncheckDate(subBodyObj) {
  subBodyObj.load = "acct_uncheck_date";
  subBodyObj.para = "<a>"+getCompCode()+"</a><b>"+getCopyCode()+"</b><c>" + getAcctYear() + "</c>";
  //alert(subBodyObj.para);
 	subBodyObj.post();

  return subBodyObj.getOneDim();
}

function getDayCheckDate(subBodyObj) {
  subBodyObj.load = "acct_day_check_date";
  subBodyObj.para = "<a>"+getCompCode()+"</a><b>"+getCopyCode()+"</b><c>"+getAcctYear()+"</c>";
  //alert(subBodyObj.para);
 	subBodyObj.post();

  return subBodyObj.getOneDim();
}

function getAcctMonth(subBodyObj) {
	var p = getValuePairBySql("acct_get_default_month","<comp_code>"+getCompCode()+"</comp_code><copy_code>"+getCopyCode()+"</copy_code><login_date>"+getLoginDate()+"</login_date>");
	if(!p)
		return "";

	return p[1];
}

function getDateObj(dateStr){
	var a = dateStr.split("-");
	if(!a || a.length < 3){
		alert("非法的日期字符串(yyyy-mm-dd)！");
		return;
	}

	var year = parseInt(a[0], 10);
	var month = parseInt(a[1], 10) - 1;
	var day = parseInt(a[2], 10);
	var dObj = new Date();
	dObj.setYear(year);
	dObj.setMonth(month,day);
	dObj.setDate(day);
	dObj.setHours(0);
	dObj.setMinutes(0);
	dObj.setSeconds(0);
	dObj.setUTCMilliseconds(0);
	return dObj;
}

function checkBetweenDate(startDateObj, endDateObj){
	if(getDateObj(startDateObj.value) < getDateObj(getAcctYearBeginDate())){
		alert("开始日期不能小于" + getDisplayDateStr(getAcctYearBeginDate()));
		startDateObj.value = getAcctYearBeginDate();
		return false;
	}

	if(getDateObj(endDateObj.value) > getDateObj(getAcctYearEndDate())){
		alert("结束日期不能大于" + getDisplayDateStr(getAcctYearEndDate()));
		endDateObj.value = getAcctYearEndDate();
		return false;
	}

	if(getDateObj(startDateObj.value) > getDateObj(endDateObj.value)){
		alert("开始日期不能大于结束日期");
		return false;
	}

	return true;
}

function getDisplayDateStr(dateStr){
	var a = dateStr.split("-");
	if(!a || a.length < 3){
		alert("非法的日期字符串(yyyy-mm-dd)！");
		return;
	}

	return a[0] + "年" + a[1] + "月" + a[2] + "日";
}

function initEditActivexBtn(btnId, activexObjId){
	activexObjId.StartInit();
	activexObjId.TableEditEnable = false;
  btnId.innerText = "编辑(C)";
  btnId.accessKey = "C";
	btnId.onclick = function(){editActivexObj(this, activexObjId);}
	activexObjId.EndInit();
}

function editActivexObj(btnId, activexObjId){
	activexObjId.StartInit()
	var edit = activexObjId.TableEditEnable;
  activexObjId.TableEditEnable = !edit;
  btnId.innerText = !edit ? "查看(C)" : "编辑(C)";
  activexObjId.EndInit();
}

function getAcctYearFlag(subBodyObj) {
  subBodyObj.load = "acct_year_flag";
  subBodyObj.para = "";
  //alert(subBodyObj.para);
 	subBodyObj.post();

  return subBodyObj.getOneDim()[0];
}


//// 合acct3.1
function getAcctSysInitFlag_ty(sql_id,para) {
  window.xmlhttp.post(sql_id, para, "?isCheck=false");
	var doc = window.xmlhttp._object.responseXML;
	return doc.selectSingleNode("/root/tbody/tr/td").text;
}

function getAcctSysInitFlag() {
  window.xmlhttp.post("acct_system_init", "", "?isCheck=false");
	var doc = window.xmlhttp._object.responseXML;
	return doc.selectSingleNode("/root/tbody/tr/td").text;
}
function getAcctSysInitFlag2() {
  window.xmlhttp.post("acct_system_init2", "", "?isCheck=false");
	var doc = window.xmlhttp._object.responseXML;
	return doc.selectSingleNode("/root/tbody/tr/td").text;
}
function getAcctSysInitFlag3() {
  window.xmlhttp.post("acct_system_init3", "", "?isCheck=false");
	var doc = window.xmlhttp._object.responseXML;
	return doc.selectSingleNode("/root/tbody/tr/td").text;
}
//// end 合acct3.1

function getAcctSysInitFlag() {
  window.xmlhttp.post("acct_system_init", "", "?isCheck=false");
	var doc = window.xmlhttp._object.responseXML;
	return doc.selectSingleNode("/root/tbody/tr/td").text;
}
function getDevSysInit(){
	var p=getValuePairBySql("devGetCurYearAndMonth","")
	if(p==null)
	   return "";
	else
	 	return "1";
}
function getEquiSysInit(){
	var p=getValuePairBySql("equiStartYearAndMonth","")
	if(p==null)
	   return "";
	else
	 	return "1";
}
function getImmaSysInit(){
	var p=getValuePairBySql("imma_equiStartYearAndMonth","")
	if(p==null)
	   return "";
	else
	 	return "1";
}
function getRepFileName(report_code, comp_code, report_type, data_period){
	var fileName = (report_code + "_" + comp_code + "_" + getAcctYear());
	if(report_type != "4")
		fileName += "_" + data_period;
	fileName += ".cll";
	return fileName;
}

function getTmplFileName(report_code){
	return (getAcctYear() + "_" + report_code + ".cll");
}
function getRepPathByModCode(modCode){
	var p=getValuePairBySql("rep_get_report_path_by_mod_code","<modCode>"+modCode+"</modCode>");
	return p[0];
}
function getRepFilePath(mod_name, func_dir){
	var path = window.prefix + "Data/" + getCompCode() + "/" + mod_name + "/" + getCopyCode() + "/" + getAcctYear() + "/";
	if(func_dir)
		path += (func_dir + "/");
	return path;
}

function getPeriodValue(repType){
    var d = getLoginDate().split("-");
  	var m = parseInt(d[1], 10);
    var pv;
    switch (repType) {
      case "1":
      	pv = (m < 10 ? "0" : "") + m;
        break;
      case "2":
      	if(m < 4)
      		pv = "1";
      	else if(m < 7)
      		pv = "2";
      	else if(m < 10)
      		pv = "3";
      	else
      		pv = "4";
        break;
      case "3":
      	pv = (m > 6 ? "2" : "1");
        break;
      case "4":
      	pv = d[0];
       	break;
    }

    return pv;
}
function getAcctStoreDayReportDefaultDate(subBodyObj) {
  subBodyObj.load = "acct_store_day_report_default_date";
  subBodyObj.para = "<a>"+getCompCode()+"</a><b>"+getCopyCode()+"</b><c>" + getAcctYear() + "</c>";
  //alert(subBodyObj.para);
 	subBodyObj.post();

  return subBodyObj.getOneDim()[0];
}
function getAcctStoreDayReportDefaultDatebank(subBodyObj) {
  subBodyObj.load = "acct_store_day_report_default_datebank";
  subBodyObj.para = "<a>"+getCompCode()+"</a><b>"+getCopyCode()+"</b><c>" + getAcctYear() + "</c>";
  //alert(subBodyObj.para);
 	subBodyObj.post();

  return subBodyObj.getOneDim();
}

//在弹开窗口中打开Cell模板文件
function showModCellFile(htmlPath, report_code){
  var screenHeight=window.screen.availHeight;
  var screenWidth=window.screen.availWidth;
	openDialog(htmlPath + '?load=<report_code>' + report_code + '</report_code>', 'dialogWidth:'+screenWidth+'px;dialogHeight:'+screenHeight+'px');
}

//打印预览所有查询数据
function printViewAllData(lineCtnObj, actionName, xslFileName, tableTitle, subTitle){
    //请求要预览的所有数据
    window.xmlhttp.post(actionName, lineCtnObj.assemble());
    var serverXml = window.xmlhttp._object.responseText;
		if(!window.doMsg(serverXml)){
			return;
		}

		var root = getPrintRoot(serverXml, xslFileName);
		if(!root){
			return;
		}

		var topStr =getPrintTopStr(tableTitle, subTitle);

		var vXml = new ActiveXObject("Microsoft.XMLDOM");
		vXml.async=false;
		vXml.loadXML(topStr);
		root.selectNodes("/root")[0].appendChild(vXml.selectNodes("/top")[0]);

		var bottomStr=getPrintBottomStr();
		var vXml2 = new ActiveXObject("Microsoft.XMLDOM");
		vXml2.async=false;
		vXml2.loadXML(bottomStr);
		root.selectNodes("/root")[0].appendChild(vXml2.selectNodes("/bottom")[0]);

		window._printXml = root;
		window.outputFlag = true;
		printView1();
		return true;
}
function saveBigDataByBigDataAction(sql,subFun,str,hide,msg){
	if(str==""||str==null){
		alert("数据没有改变!");
		return true;
	}
	window.xmlhttp.post(sql, "<multiData>" + str + "</multiData>" ,subFun);
	var estr = window.xmlhttp._object.responseText;
	if(estr.indexOf("<error>")>0){
		estr=estr.substr(estr.indexOf("<error>")+7);
		estr=estr.substr(0,estr.indexOf("</error>"));
		alert(estr);
		return false;
	}
	if(hide)
		if(hide==true)
			return true;
	if(msg)
		alert(msg)
	else
		alert("保存成功!");
	return true;
}
/*
*功能：打印预览自定义页头页尾内容的所有查询数据
*参数说明：
*lineCtnObj：调用页面内查询组件（lineCtn）的ID（对象）
*actionName：提交请求的ID名称（字符串）
*xslFileName：用于打印预览的XSLT文件名称（字符串）
*topHTML：打印预览的页头内容（字符串）
*示例格式：
*	<top>
*		<maintitle>主标题</maintitle>
*		<subtitle>副标题</subtitle>
*		<tr><td style='text-align:right;'> 页号：[页码]/[总页数]</td></tr>
*		......
*	</top>
*bottomHTML：打印预览的页尾内容（字符串），可空
*示例格式：
*  <bottom>
*   <tr><td style='text-align:right;'>打印日期: xxxx年xx月xx日 制表人：xxx</td></tr>
*	  <tr><td style='text-align:right'>编制单位：xxxx单位</td></tr>
*		<tr><td style='text-align:right'>北京望海康信科技有限公司  制作</td></tr>
*		......
*	</bottom>
*/
/*
function printCustomViewAllData(lineCtnObj, actionName, xslFileName, topHTML, bottomHTML){
		if(!topHTML){
			alert("打印预览的表头内容不能为空！");
			return;
		}

    //请求要预览的所有数据
    window.xmlhttp.post(actionName, lineCtnObj.assemble());
    var serverXml = window.xmlhttp._object.responseText;
		if(!window.doMsg(serverXml)){
			return;
		}

		var root = getPrintRoot(serverXml, xslFileName);
		if(!root){
			return;
		}

		var vXml = new ActiveXObject("Microsoft.XMLDOM");
		vXml.async=false;
		vXml.loadXML(topHTML);
		root.selectNodes("/root")[0].appendChild(vXml.selectNodes("/top")[0]);

		var vXml2 = new ActiveXObject("Microsoft.XMLDOM");
		vXml2.async=false;
		if(!bottomHTML)
			vXml2.loadXML(bottomHTML);
		else
			vXml2.loadXML(getPrintBottomStr());
		root.selectNodes("/root")[0].appendChild(vXml2.selectNodes("/bottom")[0]);

		window._printXml = root;
		window.outputFlag = true;
		printView1();
		return true;
}
*/
function transformXmlByXsltFile(xml,fi){
	var vXsl = new ActiveXObject("Microsoft.XMLDOM");
  	vXsl.async=false;
  	vXsl.load(window.document.URL.replace(/\?.*$/g,'').replace(/[^\/]*$/, fi));
  	return transformXmlByXslt(xml,vXsl);
}
function transformXmlByXslt(xml,xslObj){
	if((""+typeof(xml)).toLowerCase().indexOf("object")<0){
		var xmlObj=new ActiveXObject("Microsoft.XMLDOM");
		xmlObj.async=false;return;
  		xmlObj.loadXML(xml);
  		xml=xmlObj;
	}
	return xml.transformNode(xslObj);
}
function printCustomViewAllData(actionName, xslFileName, topHTML, bottomHTML,para){
		if(!topHTML){
			alert("打印预览的表头内容不能为空！");
			return;
		}

    //请求要预览的所有数据
    window.xmlhttp.post(actionName, para,'?isCheck=false');
    var serverXml = window.xmlhttp._object.responseText;
		if(!window.doMsg(serverXml)){
			return;
		}

		var root = getPrintRoot(serverXml, xslFileName);
		if(!root){
			return;
		}

		var vXml = new ActiveXObject("Microsoft.XMLDOM");
		vXml.async=false;
		vXml.loadXML(topHTML);
		root.selectNodes("/root")[0].appendChild(vXml.selectNodes("/top")[0]);

		var vXml2 = new ActiveXObject("Microsoft.XMLDOM");
		vXml2.async=false;
		if(!bottomHTML)
			vXml2.loadXML(bottomHTML);
		else
			vXml2.loadXML(getPrintBottomStr());
		root.selectNodes("/root")[0].appendChild(vXml2.selectNodes("/bottom")[0]);

		window._printXml = root;
		window.outputFlag = true;
		printView1();
		return true;
}

//表格控件内部用到弹出拼音内容检索窗口
function DoActiveXPopValue(TableID, RowIndex, ColIndex, pySqlID, pySqlParam, CellValue, ColName)
{
	var pyparam = pySqlParam;
	pyparam = pyparam.replace(/</g, "&lt;");
	pyparam = pyparam.replace(/>/g, "&gt;");

	var sValue = window.showModalDialog("/activexpop.html?load=<sqlid>" + pySqlID + "</sqlid><pyparam>" + pyparam + "</pyparam><colname>" + ColName + "</colname><cvalue>" + CellValue + "</cvalue>", window, "scrollbars:no;resizable:no;help:no;status:no;dialogWidth:600px;dialogHeight:450px");

	if (sValue != null)
	{
		var activexID = document.getElementById(TableID);
		eval(TableID + ".SetCellData(RowIndex, ColIndex, sValue);");
		eval(TableID + ".focus();");
	}
}

//表格控件用取当前页面URL
function GetPageURL(TableID)
{
	var sURL = document.location.href.toString();
	sURL_a=sURL.split('?');

	eval(TableID + ".CurrentURL = '" + sURL_a[0] + "'");
}


function getDefinePageURL(){
	return window.prefix + "base/definePrint/define.htm"
}

function getParameter(param){
	var query = window.location.search;
	var iLen = param.length;
	var iStart = query.indexOf(param);
	if (iStart == -1)
	return "";
	iStart += iLen + 1;
	var iEnd = query.indexOf("&", iStart);
	if (iEnd == -1)
	  return query.substring(iStart);
	return query.substring(iStart, iEnd);
}
function convertRMBCurrency(currencyDigits) {
	// Constants:
	 var MAXIMUM_NUMBER = 99999999999.99;
	 // Predefine the radix characters and currency symbols for output:
	 var CN_ZERO = "零";
	 var CN_ONE = "壹";
	 var CN_TWO = "贰";
	 var CN_THREE = "叁";
	 var CN_FOUR = "肆";
	 var CN_FIVE = "伍";
	 var CN_SIX = "陆";
	 var CN_SEVEN = "柒";
	 var CN_EIGHT = "捌";
	 var CN_NINE = "玖";
	 var CN_TEN = "拾";
	 var CN_HUNDRED = "佰";
	 var CN_THOUSAND = "仟";
	 var CN_TEN_THOUSAND = "万";
	 var CN_HUNDRED_MILLION = "亿";
	 var CN_SYMBOL = "";//"人民币";
	 var CN_DOLLAR = "元";
	 var CN_TEN_CENT = "角";
	 var CN_CENT = "分";
	 var CN_INTEGER = "整";

	// Variables:
	 var integral; // Represent integral part of digit number.
	 var decimal; // Represent decimal part of digit number.
	 var outputCharacters; // The output result.
	 var parts;
	 var digits, radices, bigRadices, decimals;
	 var zeroCount;
	 var i, p, d;
	 var quotient, modulus;
	 var fuflag =0;
	// Validate input string:
	 currencyDigits = currencyDigits.toString();
	 if (currencyDigits == "") {
	  alert("Empty input!");
	  return "";
	 }
	 
	 if(currencyDigits.indexOf("-")>-1){
	 	 currencyDigits = currencyDigits.replace("-","");
	 	 fuflag=1;
	 }
	
	 if (currencyDigits.match(/[^,.\d]/) != null) {
	  alert("Invalid characters in the input string!");
	  return "";
	 }
	 if ((currencyDigits).match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/) == null) {
	  alert("Illegal format of digit number!");
	  return "";
	 }

	// Normalize the format of input digits:
	 currencyDigits = currencyDigits.replace(/,/g, ""); // Remove comma delimiters.
	 currencyDigits = currencyDigits.replace(/^0+/, ""); // Trim zeros at the beginning.
	 // Assert the number is not greater than the maximum number.
	 if (Number(currencyDigits) > MAXIMUM_NUMBER) {
	  alert("Too large a number to convert!");
	  return "";
	 }

	// Process the coversion from currency digits to characters:
	 // Separate integral and decimal parts before processing coversion:
	 parts = currencyDigits.split(".");
	 if (parts.length > 1) {
	  integral = parts[0];
	  decimal = parts[1];
	  // Cut down redundant decimal digits that are after the second.
	  decimal = decimal.substr(0, 2);
	 }
	 else {
	  integral = parts[0];
	  decimal = "";
	 }
	 // Prepare the characters corresponding to the digits:
	 digits = new Array(CN_ZERO, CN_ONE, CN_TWO, CN_THREE, CN_FOUR, CN_FIVE, CN_SIX, CN_SEVEN, CN_EIGHT, CN_NINE);
	 radices = new Array("", CN_TEN, CN_HUNDRED, CN_THOUSAND);
	 bigRadices = new Array("", CN_TEN_THOUSAND, CN_HUNDRED_MILLION);
	 decimals = new Array(CN_TEN_CENT, CN_CENT);
	 // Start processing:
	 outputCharacters = "";
	 // Process integral part if it is larger than 0:
	 if (Number(integral) > 0) {
	  zeroCount = 0;
	  for (i = 0; i < integral.length; i++) {
	   p = integral.length - i - 1;
	   d = integral.substr(i, 1);
	   quotient = p / 4;
	   modulus = p % 4;
	   if (d == "0") {
	    zeroCount++;
	   }
	   else {
	    if (zeroCount > 0)
	    {
	     outputCharacters += digits[0];
	    }
	    zeroCount = 0;
	    outputCharacters += digits[Number(d)] + radices[modulus];
	   }
	   if (modulus == 0 && zeroCount < 4) {
	    outputCharacters += bigRadices[quotient];
	   }
	  }
	  outputCharacters += CN_DOLLAR;
	 }
	 // Process decimal part if there is:
	 if (decimal != ""  && decimal != "00") {
	  for (i = 0; i < decimal.length; i++) {
	   d = decimal.substr(i, 1);
	   if (d != "0") {
	    outputCharacters += digits[Number(d)] + decimals[i];
	   }else if (i==0){
	    outputCharacters += CN_ZERO;
	    }
	  }
	 }
	 // Confirm and return the final output string:
	 if (outputCharacters == "") {
	  outputCharacters = CN_ZERO + CN_DOLLAR;
	 }
	 if (decimal == "") {
	  outputCharacters += CN_INTEGER;
	 }
	 outputCharacters = CN_SYMBOL + outputCharacters;
	 var lastC=outputCharacters.substr(outputCharacters.length-CN_CENT.length);
	 if(lastC!=CN_CENT&&lastC!=CN_INTEGER)
	 	outputCharacters += CN_INTEGER;
	 if (fuflag==1)
	  outputCharacters ='负'+outputCharacters;
	 return outputCharacters;
}
function convertDateBig(t,v){
	 var const_name=["零","壹","贰","叁","肆","伍","陆","柒","捌","玖","拾"];
	if(t=="y"){
		res="";
		for(var i=0;i<v.length;i++){
			res+=const_name[parseInt(v.substr(i,1),10)];
		}
		return res;
	}else if(t=="m"){
		res="";
		f=""
		for(var i=0;i<v.length;i++){
			f=parseInt(v.substr(i,1),10);
			if(i==0){
				if(f==0)
					res+=const_name[0];
				else
					res+=const_name[f]+const_name[10];
			}else if(i==1 && f!=0){
				res+=const_name[f];
			}else{
				res=const_name[0] + res;
			}
		}
		return res;
	}
}
  function setAll(elem){
	  var flag;
	  var inputs = elem.getElementsByTagName('input');
	  for(var i=0;i<inputs.length;i++){
		 if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TH')) {
				flag=inputs[i].checked;
		 }
	  }

	  for(var i=0;i<inputs.length;i++){
	    if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TD'&&inputs[i].disabled!=true)) {
			  inputs[i].checked=flag;
	    }
	  }
  }
    function checkAll(elem){
    var flag;
    var inputs = elem.getElementsByTagName('input');
	  for(var i=0;i<inputs.length;i++){
	    if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TD')) {
			  if(inputs[i].checked==false){
				  flag=false;
				  break;
			  }
	    }
	  }

    for(var i=0;i<inputs.length;i++){
  	  if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TH')) {
  			if(flag==false)
  				inputs[i].checked=false;
  			else
  			  	inputs[i].checked=true;
  	  }
    }
	if(typeof(this.onclick2)!="undefined")
		eval(this.onclick2);
  }
function FormatNumber(srcStr,nAfterDot)        //nAfterDot小数位数
       {
　　        var srcStr,nAfterDot;
　　        var resultStr,nTen;
　　        srcStr = ""+parseFloat(srcStr)+"";
　　        strLen = srcStr.length;
　　        dotPos = srcStr.indexOf(".",0);
　　        if (dotPos == -1){
　　　　        resultStr = srcStr+".";
　　　　        for (i=0;i<nAfterDot;i++){
　　　　　　        resultStr = resultStr+"0";
　　　　        }
　　　　        return resultStr;
　　        }
　　        else{
　　　　        if ((strLen - dotPos - 1) >= nAfterDot){
　　　　　　        nAfter = dotPos + nAfterDot + 1;
　　　　　　        nTen =1;
　　　　　　        for(j=0;j<nAfterDot;j++){
　　　　　　　　        nTen = nTen*10;
　　　　　　        }
　　　　　　        resultStr = Math.round(parseFloat(srcStr)*nTen)/nTen;
　　　　　　        return resultStr;
　　　　        }
　　　　        else{
　　　　　　        resultStr = srcStr;
　　　　　　        for (i=0;i<(nAfterDot - strLen + dotPos + 1);i++){
　　　　　　　　        resultStr = resultStr+"0";
　　　　　　        }
　　　　　　        return resultStr;
　　　　        }
　　        }
        }


//add by b2zhangwei
//一次获得两个参数减少与后台的交互次数
function get_radix_point(){
	window.xmlhttp.post("mate_get_radix_point","","?isCheck=false");
	var doc = window.xmlhttp._object.responseXML;

	var arr = doc.getElementsByTagName("td");
	var radix_point;

	if(arr.length==2){
		radix_point = new Array(arr[0].firstChild.xml,arr[1].firstChild.xml);
	}else{
		radix_point = null;
	}
	return radix_point;
}
function get_is_link_budg(){
	window.xmlhttp.post("mate_isLinkBudg","","?isCheck=false");
	var doc = window.xmlhttp._object.responseXML;

	var arr = doc.getElementsByTagName("td");
	var isLinkandControl;

	if(arr.length==2){
		isLinkandControl = new Array(arr[0].firstChild.text,arr[1].firstChild.text);
	}else{
		isLinkandControl = new Array("n","0");
	}

	return isLinkandControl;
}
var endElem
function noContextMenu(){
	if(event.srcElement.tagName=="TD")//||event.srcElement.tagName=="TH"
		endElem=event.srcElement.parentElement.parentElement.parentElement;
	else
		endElem=event.srcElement;
	
	if(!(endElem.id)||endElem.id.indexOf('_mainDataTable')<0){return true;}

	var lang = new Object();
	lang["exportExcel"]		= "导出到Excel..."
	lang["UIMenuWidth"]				= 150
	var menuStyle = "<head><link href='/base/themes/blue/menuarea.css' type='text/css' rel='stylesheet'></head><body scroll='no' onConTextMenu='event.returnValue=false;'>";
	var sMenu1 = "<TABLE border=0 cellpadding=0 cellspacing=0 class=Menu width="+lang["UIMenuWidth"]+"><tr><td width=18 valign=bottom align=center><\/td><td width="+(lang["UIMenuWidth"]-18)+" class=RightBg><TABLE border=0 cellpadding=0 cellspacing=0>";
	var sMenu2 = "<\/TABLE><\/td><\/tr><\/TABLE>";
	var s_MenuRow = "<tr><td align=center valign=middle><TABLE border=0 cellpadding=0 cellspacing=0 width="+(lang["UIMenuWidth"]-18)+"><tr ><td valign=middle height=20 class=MouseOut onMouseOver=this.className='MouseOver'; onMouseOut=this.className='MouseOut';";
	s_MenuRow += " onclick='parent.table_exportToExcel();parent.oPopupMenu.hide();'>";
	s_MenuRow += lang["exportExcel"]+"<\/td><\/tr><\/TABLE><\/td><\/tr>";
	var aheight = 24;
	var lefter = event.clientX;
	var topper = event.clientY;
	var awidth = lang["UIMenuWidth"];

	event.cancelBubble   =   true
  event.returnValue   =   false;
  oPopupMenu=window.createPopup();
  var oPopDocument = oPopupMenu.document;
  oPopDocument.open();
	oPopDocument.write(menuStyle + sMenu1+s_MenuRow+sMenu2);
	oPopDocument.close();
	if(lefter+awidth > document.body.clientWidth) lefter=lefter-awidth;
	oPopupMenu.show(lefter, topper, awidth, aheight, document.body);
  return   false;
}
function decodeXmlChar(s){
	s=s.replace(/&quot;/g,"\"");
	s=s.replace(/&amp;/g,"&");
	s=s.replace(/&gt;/g,">");
	s=s.replace(/&lt;/g,"<");
	return s;
}
function encodeXmlChar(s){
	s=s.replace(/&/g,"&amp;");
	s=s.replace(/>/g,"&gt;");
	s=s.replace(/</g,"&lt;");
	return s;
}
function encodeEscapeChar(s){
	s = s.replace(/\[/g, "[[]"); //此句一定要在最前
  s = s.replace(/\_/g, "[_]");
  s = s.replace(/%/g, "[%]");
  s = s.replace(/\\/g, "\\\\");
	return s;
}
function encodeHtttGetStr(s){
	s=(""+s).replace(/%/g,"%25");
	s=s.replace(/\s/g,"%20");
	s=s.replace(/\?/g,"%3F");
	s=s.replace(/&/g,"%26");
	s=s.replace(/=/g,"%3D");
	s=s.replace(/\+/g,"%2B");
	return s;
}
function table_exportToExcel(){
	var pe=endElem;
	while(pe!=null){
		if(pe.printh2c){
			eval(pe.printh2c);
			return;
		}
		pe=pe.parentNode;
	}
	if(window.dialogArguments!=null){
		var iwin=window.dialogArguments;
		for(;;){
			if(iwin.dialogArguments!=null)
				iwin=iwin.dialogArguments
			else
				break;

		}
		topwindow=iwin.top;
	}else
		topwindow=self.top;
	if(topwindow.activex_lib==null){
		alert("请安装望海组件");
		return false;
	}
	
	//解决导出为excel时错位的问题
	//var tmpHtml = endElem.outerHTML.replace(/<TH.*<INPUT.*<\/TH>/gi,"").replace(/<TD.*<\/INPUT>.*<\/TD>/gi,"").replace(/text-decoration.*none/gi,"").replace(/<TH.*: none.*<\/TH>/gi,"").replace(/<TD.*:[ ]{0,1}none.*<\/TD>/gi,"").replace(/<TH noWrap><\/TH>/gi,"");
	var tmpHtml = endElem.outerHTML.replace(/text-decoration.*none/gi,"").replace(/<TH.*: none.*<\/TH>/gi,"").replace(/<TD.*:[ ]{0,1}none.*<\/TD>/gi,"").replace(/<TH noWrap><\/TH>/gi,"");

//	if (tmpHtml.indexOf("<A ") < 0){
//		tmpHtml = tmpHtml.replace(/>([d+])</gi,">'$1<")
//	}
	//tmpHtml=tmpHtml.replace(/(<td[^\/>]*>)/gi,"$1'")

tmpHtml = tmpHtml.replace(/(<A.*>)(.*)(<\/A>)/gi,"$2")	

tmpHtml=tmpHtml.replace(/(<td[^\/]*>)/gi,"$1'").replace(/(<td.*right.*>)'(-?\d+)/gi,"$1$2").replace(/(<td.*numberText.*>)'(-?\d+)/gi,"$1$2");//(number|right)

topwindow.activex_lib.ExportExcel(tmpHtml);


}
document.oncontextmenu=noContextMenu;
function setPageEditStatus(obj, table, edit)
	{
		if ( edit == undefined )
		{
			table.TableEditEnable = !table.TableEditEnable;
		} else
		{
			table.TableEditEnable = edit;
		}
		obj.innerText = table.TableEditEnable?"非编辑状态(C)":"编辑状态(C)";
	}
/*
{"name":"","type":"text,select,checkbox,radio,checktype","para":"","afterFun":"fun"}
*/
function QuerySchema(listName,conf,inDlg,textName){
	var p=typeof(window.dialogArguments)=="undefined"?parent:window.dialogArguments;
	if(inDlg)
		this.settingKey=p.location.href;
	else
		this.settingKey=window.location.href;
	if(this.settingKey.indexOf("/hbos")>=0)
		this.settingKey=this.settingKey.substr(this.settingKey.indexOf("/hbos")+5);
	this.settingKey="QS"+this.settingKey;
	this.settingKey=this.settingKey.substr(0,this.settingKey.indexOf("."));
	this.settingKey=this.settingKey.replace(/\//g,"");
	this.settingKey=this.settingKey.replace(/\./g,"");
	this.conf=conf;
	this.schemaListName=listName;
	this.schemaName=textName;
	this.save=function(name){
		var str="<action>save</action><schemaPath>"+this.settingKey+"</schemaPath><schemaName>"+this.encode(name)+"</schemaName><schemaDef>"+this.encode("<root>"+this.getQueryData()+"</root>")+"</schemaDef>";
		window.xmlhttp.post("globalQuerySchema",str,"");
		var estr = window.xmlhttp._object.responseText;
		if(estr.indexOf("<error>")>0){
			estr=estr.substr(estr.indexOf("<error>")+7);
			estr=estr.substr(0,estr.indexOf("</error>"));
			alert(estr);
			return false;
		}
		alert("保存成功!");
		return true;
	}
	this.load=function(name){
		window.xmlhttp.post("globalQuerySchema","<action>load</action><schemaPath>"+this.settingKey+"</schemaPath><schemaName>"+name+"</schemaName><schemaDef></schemaDef>","");
		var doc = window.xmlhttp._object.responseXML;
		if(doc.documentElement==null)
			return;
		if(window.xmlhttp._object.responseText.indexOf("<td>")<0)
			return;
		var x = window.xmlhttp._object.responseText.substring(window.xmlhttp._object.responseText.search(/<td>/)+"<td>".length, window.xmlhttp._object.responseText.search(/<\/td>/))
		//var tds=doc.getElementsByTagName("td");
		//if(tds.length==0)
		//	return;
		//var x=this.decode(tds[0].text);
		x=this.decode(x);
		var dom=new ActiveXObject("Microsoft.XMLDOM");
		dom.loadXML(x);
		var cn=dom.documentElement.childNodes;
		for(var i=0;i<cn.length;i++){
			if(cn[i].nodeType!=1)
				continue;
			var v="";
			if(cn[i].firstChild!=null)
				v=cn[i].firstChild.nodeValue;
			v=this.decode(v);
			if(typeof(this._getConf(cn[i].nodeName))!='undefined'){
				eval("this._setvalue_"+this._getConf(cn[i].nodeName)["type"]+"(this._getConf('"+cn[i].nodeName+"'),"+cn[i].nodeName+",\""+v+"\")");
				if(this._getConf(cn[i].nodeName)["afterFun"])
					eval(this._getConf(cn[i].nodeName)["afterFun"]+"(this._getConf('"+cn[i].nodeName+"'))");
			}
		}
	}
	this.del=function(){
		var obj=eval(this.schemaListName);
		var n=obj.value;
		if(n.length>0)
			n=n.substr(1,n.length-2);
		else{
			alert("请选择!");
			return false;
		}
		var str="<action>del</action><schemaPath>"+this.settingKey+"</schemaPath><schemaName>"+this.encode(n)+"</schemaName><schemaDef>sd</schemaDef>";
		window.xmlhttp.post("globalQuerySchema",str,"");
		var estr = window.xmlhttp._object.responseText;
		if(estr.indexOf("<error>")>0){
			estr=estr.substr(estr.indexOf("<error>")+7);
			estr=estr.substr(0,estr.indexOf("</error>"));
			alert(estr);
			return false;
		}
		return true;
	}
	this._getConf=function(n){
		for(var i=0;i<this.conf.length;i++){
			if(this.conf[i]["name"]==n)
				return this.conf[i];
		}
	}
	this.getQueryData=function(){
		var str="";
		for(var i=0;i<this.conf.length;i++){
			c=this.conf[i];
			str+="<"+c["name"]+">"+this.encode(eval("this._getvalue_"+c["type"]+"(this.conf['"+i+"'],"+c["name"]+")"))+"</"+c["name"]+">";
		}
		return str;
	}
	this.refreshSchemaList=function(){
		obj=eval(this.schemaListName);
		obj.para="<pathCode>"+this.settingKey+"</pathCode><b/><c/><d/>";
		obj.refresh();
		qc=this;
		obj.onchange=function(){
			v=""
			if(this.value.length>0){
				v=this.value.substr(1,this.value.length-2);
			}
			eval(qc.schemaName).value=v;
			qc.load(v);
		}
	}
	this.selFirst=function(){
		obj=eval(this.schemaListName);
		if(obj.firstCode){
			obj.setValue("'"+obj.firstCode+"'");
			this.load(obj.firstCode);
			var no=eval(this.schemaName);
			no.value=obj.firstCode;
		}
	}
	this.encode=function(s){
		s=s.replace(/&/g,"&amp;");
		s=s.replace(/</g,"&lt;");
		s=s.replace(/>/g,"&gt;");
		return s;
	}
	this.decode=function(s){
		s=s.replace(/&amp;/g,"&");
		s=s.replace(/&gt;/g,">");
		s=s.replace(/&lt;/g,"<");
		return s;
	}
	this._getvalue_select=function(conf,obj){
		return obj.value;
	}
	this._setvalue_select=function(conf,obj,v){
		obj.setValue(v);
		if(conf["para"]){
			obj.para=eval(conf["para"]);
			obj.refresh();
		}
	}
	this._getvalue_selects=function(conf,obj){
		return obj.value+"|||"+obj.text;
	}
	this._setvalue_selects=function(conf,obj,v){
		obj.setValue(v);
		if(conf["para"]){
			obj.para=eval(conf["para"]);
			obj.refresh();
		}
	}
	this._getvalue_text=function(conf,obj){
		return obj.value;
	}
	this._setvalue_text=function(conf,obj,v){
		obj.value=v;
	}
	this._getvalue_checkbox=function(conf,obj){
		return obj.value;
	}
	this._setvalue_checkbox=function(conf,obj,v){
		obj.setValue(v);
	}
	this._getvalue_radio=function(conf,obj){
		return obj.value;
	}
	this._setvalue_radio=function(conf,obj,v){
		obj.setValue(v);
	}
	this._getvalue_checktype=function(conf,obj){
		return this.encode(obj.GetCurrentAllData());
	}
	this._setvalue_checktype=function(conf,obj,v){
		//alert(v);
		obj.SetTableFromXML(v);
	}

}

function checkKZM(s){
  var i = s.lastIndexOf(".");
  if(i < 0){
   alert("文件格式不正确!!");
   return false;
  }
  var var1 = s.substring(i+1) ;
  if(var1 != "mdb"){
		alert("此文件只能是*.mdb文件!!");
		return false;
	}
  return true ;
}

function getReportPathCode(mod_code){
	var r=getValuePairBySql("rep_getReportPathCode","<mod_code>"+mod_code+"</mod_code>");
	if(r==null){
		return "";
	}else{
		return r[0];
	}
}

function getLinkTwoModuleStatus(mod_code1,mod_code2){
	var r=getValuePairBySql("sysGetLinkTwoModule","<mod_code1>"+mod_code1+"</mod_code1><mod_code2>"+mod_code2+"</mod_code2>");
	if(r==null){
		return false;
	}else{
		if(r[0] == "1")
			return true;
		else
			return false
	}
}
function getSysParaDataValue(c){
	var e=getEnvionmentValue("_tablePageSize"+c);
	if(e==null){
		var p=getValuePairBySql("dict_default_table_page_size","<m>"+_getCurModule()+"</m><c>"+c+"</c>");
		if(p==null)
			e="";
		else
			e=p[0];
		setEnvionmentValue("_tablePageSize"+c,e);
	}
	return e;
}
function getDefaultPageSizeByMod(){
	//var v=getSysParaDataValue("0321");
	//var v="";//0321含义改变，不再获取0321
	var v=getSysParaDataValue("0399");
	if(v=="")
		v="0";
	return v;
}
function setVHTablePageSize(obj,size){
	var t=getDefaultPageSizeByMod();
	if(t=="0"){
		obj.PageSize=size;
	}
	if(t=="1"){
		obj.PageSize=0;
	}else{
		obj.PageSize=t;
	}
}


//----- HJJ insert BEGIN ----- 清空所有input控件的内容 -----
		function clearAllInputsValue() {
			var objInputs = window.document.getElementsByTagName("input");
			for (var i = 0; i < objInputs.length; i++) {
				if (objInputs[i].className == "inputTextA" || objInputs[i].className == "inputDecimal" || objInputs[i].className == "inputCalendar") {
					objInputs[i].value = "";
				} else if (objInputs[i].className == "inputSelect" || objInputs[i].className == "inputSelectS" || objInputs[i].className == "inputCheckBox") {
					objInputs[i].setValue("");
				}
			}

		}
		//----- HJJ insert  END  ----- 清空所有input控件的内容 -----


this.attachEvent("onload", function(){
				var t=eval("typeof("+_getCurModule()+"PageOnload)");
				if(getCurCompCode()!=null&&"undefined"!=t){
						var f=eval(""+_getCurModule()+"PageOnload");
						f(window);
				}
	});
/*
title,proc,xslFile,printXsl,page
*/
function initIntegrationQuery2(param){
		window.integrationQuery2=param;
		document.body.innerHTML="<iframe width=\"100%\" height=\"100%\" src=\""+window.prefix+"hbos/equip/query/integration2/main.html\" scrolling=\"no\"></iframe>";
	};
/**WXZC7 无形资产-资产查询 王羽 2017-04-11	 **/
function initIntegrationQuery2_imma(param){
		window.integrationQuery2_imma=param;		
		document.body.innerHTML="<iframe width=\"100%\" height=\"100%\" src=\""+window.prefix+"hbos/immaequip/query/integration2/main.html\" scrolling=\"no\"></iframe>";
	};
/****/	
	
function jhtcSetHtml(element,htmlStr){element.innerHTML=(htmlStr);};

function accDiv(arg1,arg2){   
  var t1=0,t2=0,r1,r2;   
  try{t1=arg1.toString().split(".")[1].length}catch(e){}   
  try{t2=arg2.toString().split(".")[1].length}catch(e){}   
 	with(Math){   
  r1=Number(arg1.toString().replace(".",""));   
  r2=Number(arg2.toString().replace(".",""));   
  return (r1/r2)*pow(10,t2-t1);   
	}   
}
/*
	功能：打开合同管理对应模块页面
	参数：
				code:单据编码
				RW_flag:读写标志 1:可写,2:只读
				modCode:01:付款合同;02:收款合同;03:付款协议;04:收款协议;11:付款单据(code为单据pay_id)
				compCode:单位编码
				copyCode:账套编码
*/
function openPactPage(code,RW_flag,modCode,compCode,copyCode){
	if(modCode=='01'){
		//判断是否期初数据
			window.xmlhttp.post('getPactPaymentIsInit','<a>'+code+'</a><a>'+compCode+'</a><a>'+copyCode+'</a>',"?isCheck=false");
			var resXml = window.xmlhttp._object.responseXml;
			if(resXml.getElementsByTagName("td").length == 0) return;
			var isInit = resXml.getElementsByTagName("td")[0].firstChild.nodeValue;
			window.showModalDialog('/hbos/pact/paypact/'+(isInit == '0'?'manager':'initManager')+'/update.html?load=<code>'
			+code+'</code><RW_flag>'+RW_flag
			+'</RW_flag><modCode>'+modCode+'</modCode><compCode>'+compCode
			+'</compCode><copyCode>'+copyCode+'</copyCode>', window, 'scrollbars:no;resizable:no;help:no;status:no;dialogWidth:1050px;dialogHeight:800px')
	}else if(modCode=='02'){
			//判断是否期初数据
			window.xmlhttp.post('getPactCollpactIsInit','<a>'+code+'</a><a>'+compCode+'</a><a>'+copyCode+'</a>',"?isCheck=false");
			var resXml = window.xmlhttp._object.responseXml;
			if(resXml.getElementsByTagName("td").length == 0) return;
			var isInit = resXml.getElementsByTagName("td")[0].firstChild.nodeValue;
			window.showModalDialog('/hbos/pact/collpact/'+ (isInit == '0'?'collcon':'initcollcon')+'/update.html?load=<code>'
				+code+'</code><RW_flag>'+RW_flag
				+'</RW_flag><modCode>'+modCode+'</modCode><compCode>'+compCode
				+'</compCode><copyCode>'+copyCode+'</copyCode>', window, 'scrollbars:no;resizable:no;help:no;status:no;dialogWidth:1050px;dialogHeight:800px')

	}else if(modCode=='03'){
			//判断是否期初数据
			window.xmlhttp.post('pact_payagree_agreeinfo_getIsInit','<a>'+code+'</a><a>'+compCode+'</a><a>'+copyCode+'</a>',"?isCheck=false");
			var resXml = window.xmlhttp._object.responseXml;
			var isInit = resXml.getElementsByTagName("td")[0].firstChild.nodeValue;
			if(isInit == 0){
				window.showModalDialog('/hbos/pact/payagree/agreeinfo/update.html?load=<protocol_code>'+code+'</protocol_code>', window, 'scrollbars:no;resizable:no;help:no;status:no;dialogWidth:1000px;dialogHeight:570px');
			}
			else{
				window.showModalDialog('/hbos/pact/payagree/initagreeinfo/update.html?load=<protocol_code>'+code+'</protocol_code>', window, 'scrollbars:no;resizable:no;help:no;status:no;dialogWidth:1000px;dialogHeight:570px');
			}
	}else if(modCode=='04'){
			//判断是否期初数据
			window.xmlhttp.post('pact_collagree_agreeinfo_getIsInit','<a>'+code+'</a><a>'+compCode+'</a><a>'+copyCode+'</a>',"?isCheck=false");
			var resXml = window.xmlhttp._object.responseXml;
			var isInit = resXml.getElementsByTagName("td")[0].firstChild.nodeValue;
			if(isInit == 0){
				window.showModalDialog('/hbos/pact/collagree/agreeinfo/update.html?load=<protocol_code>'+code+'</protocol_code>', window, 'scrollbars:no;resizable:no;help:no;status:no;dialogWidth:1000px;dialogHeight:570px');
			}
			else{
				window.showModalDialog('/hbos/pact/collagree/initagreeinfo/update.html?load=<protocol_code>'+code+'</protocol_code>', window, 'scrollbars:no;resizable:no;help:no;status:no;dialogWidth:1000px;dialogHeight:570px');
			}
	}else if(modCode=='05'){
			//window.showModalDialog('/hbos/equip/contract/manager/update.html?load=<id>'+code+'</id>', window, 'scrollbars:no;resizable:no;help:no;status:no;dialogWidth:1000px;dialogHeight:570px');
			openDialog('../../../equip/contract/manager/update.html?load=<id>'+code+'</id><mod>acct</mod>','dialogWidth:950px;dialogHeight:600px')

	}else if(modCode=='06'){
			//window.showModalDialog('/hbos/immaequip/contract/manager/update.html?load=<id>'+code+'</id>', window, 'scrollbars:no;resizable:no;help:no;status:no;dialogWidth:1000px;dialogHeight:570px');
			openDialog('../../../immaequip/contract/manager/update.html?load=<id>'+code+'</id><mod>acct</mod>','dialogWidth:950px;dialogHeight:600px')
	}else if(modCode=='11'){
			window.showModalDialog('/hbos/pact/paypact/paydocu/update.html?load=<pay_id>'+code+'</pay_id>', window, 'scrollbars:no;resizable:no;help:no;status:no;dialogWidth:1000px;dialogHeight:570px')
	}else{
		return;
	}
} 

/*
工作流发送实例方法
多个视图，用多个标签元素
<pay_bill_id>1</pay_bill_id><pay_bill_id>2</pay_bill_id>
*/
function workSend(workPara,tableCode){
	if(null==workPara || workPara==undefined || workPara=="" ){
		alert("请求参数为空！");
		return false;
	}
	if(tableCode=="" || tableCode==undefined){
		alert("表单编码为空！");
		return false;
	}
	if(workPara.indexOf("<erp>")!=-1){
		alert("<erp>为关键元素不能使用！");
		return false;
	}
	if(workPara.indexOf("<work_table_id>")!=-1){
		alert("<work_table_id>为关键元素不能使用！");
		return false;
	}

	var w=(screen.width-200); 
	var h=(screen.height-100);
	window.showModalDialog("/hbos/work/flow/example/main.html?load="+workPara+"<work_table_code>"+tableCode+"</work_table_code>",window,"scrollbars:no;resizable:yes;help:no;status:no;dialogWidth:"+w+"px;dialogHeight:"+h+"px");

}

Number.prototype.toFixed=function(d) 
{ 
  var s=this+""; 
  if(!d)d=0;     
  if(s.indexOf(".")==-1)s+="."; 
  s+=new Array(d+1).join("0");     
  if(new RegExp("^(-|\\+)?(\\d+(\\.\\d{0,"+(d+1)+"})?)\\d*$").test(s)) 
  { 
    var s="0"+RegExp.$2,pm=RegExp.$1,a=RegExp.$3.length,b=true;       
    if(a==d+2){ 
      a=s.match(/\d/g); 
      if(parseInt(a[a.length-1])>4) 
      { 
        for(var i=a.length-2;i>=0;i--){ 
          a[i]=parseInt(a[i])+1;             
          if(a[i]==10){ 
            a[i]=0; 
            b=i!=1; 
          }else break; 
        } 
      } 
      s=a.join("").replace(new RegExp("(\\d+)(\\d{"+d+"})\\d$"),"$1.$2");         
    }if(b)s=s.substr(1); 
    return (pm+s).replace(/\.$/,""); 
  }return this+"";
};


function dataExportToExcel(sqlid,head,cols, paras){
		window.xmlhttp.post("ExportExcel","<func>" + sqlid+ "</func><head>"+head+"</head><cols>"+cols+"</cols>" + paras);
		
		var str = window.xmlhttp._object.responseText;
		if(window.doMsg(str)){
			open("/FileDownloadServlet?uri=" + str);
		}
}

function dataExportToTxt(sqlid,head,cols, paras){
    window.xmlhttp.post("ExportTxt","<func>" + sqlid+ "</func><head>"+head+"</head><cols>"+cols+"</cols>" + paras);

    var str = window.xmlhttp._object.responseText;
    if(window.doMsg(str)){
        open("/FileDownloadServlet?uri=" + str);

        window.close();
    }
}
