function comm_DoubleSelectTableChild(win,div,dbclick){
	this.selBgColor="blue";
	this.unSelBgColor="white";
	this.selTextColor="white";
	this.unSelTextColor="black";
	this.msBox=div;
	this.msDbClick=null;
	this.msSelCode="";
	this.msSelText="";
	this.msId="ds"+getJsGuid();
	this.msSelRow=null;
	this.msWin=win;
	if(dbclick)
		this.msDbClick=dbclick;
	this.init=function(){
		var tableHtml="<div id=\""+this.msId+"Div\"  style=\"width:100%;height:100%;border: 1 solid #DDDDDD;width: 100%;margin:0;overflow:auto;padding:0;SCROLLBAR-FACE-COLOR: #F2F2F2;SCROLLBAR-HIGHLIGHT-COLOR: #999999;SCROLLBAR-SHADOW-COLOR:#999999;SCROLLBAR-3DLIGHT-COLOR:#ffffff;SCROLLBAR-ARROW-COLOR:#999999;SCROLLBAR-TRACK-COLOR:#ffffff;SCROLLBAR-DARKSHADOW-COLOR: #ffffff;\">";
		tableHtml+="</div>";
		this.msBox.innerHTML=tableHtml;
	}
	this.setValue=function(codes,texts,selCode){
		var tableHtml="<table cellspacing='0' cellpadding='0' style='cursor:default;width:100%;'>";
		for(var i=0;i<texts.length;i++){
			tableHtml+="<tr><td nowrap='true' code=\""+codes[i]+"\">"+texts[i]+"</td></tr>";
		}
		tableHtml+="</table>";
		this.msWin.document.getElementById(this.msId+"Div").innerHTML=tableHtml;
		var tds=this.msBox.getElementsByTagName("td");
		var MS=this;
		for(var i=0;i<tds.length;i++){
			tds[i].ondblclick=function(){MS.doDbClick(this);};
			tds[i].onclick=function(){MS.doClick(this,this.code,this.innerText);};
			if(selCode==tds[i].code)
				this.doClick(tds[i]);	
		}
	}
	this.doClick=function(td){
		if(this.msSelRow!=null){
			this.msSelRow.style["background"]=this.unSelBgColor;
			this.msSelRow.style["color"]=this.unSelTextColor;
		}
		this.msSelRow=td;
		td.style["background"]=this.selBgColor;
		td.style["color"]=this.selTextColor;
		this.msSelCode=td.code;
		this.msSelText=td.innerText
	}
	this.getSelCode=function(){
		return this.msSelCode;
	}
	this.getSelText=function(){
		return this.msSelText;
	}
	this.doDbClick=function(td){
		this.doClick(td);
		if(this.msDbClick){
			this.msDbClick();
		}
	}
	this.init();
}
function comm_DoubleSelectTable(win,div,load,para){
  this.dsBox=div;
	this.dsLoad=load;
	this.dsPara=null;
	this.dsWin=win;
	if(para)
		this.dsPara=para;
	this.selListCode=new Array();
	this.listCode=new Array();
	this.listText=new Array();
	this.dsId="ds"+getJsGuid();
	this.leftTab=null;
	this.rightTab=null;
	this.dsMaxTextLen=0;
	this.init=function(){
		var tableHtml="<div  style=\"width:100%;height:100%;border: 1 solid #838383;\">";
		tableHtml+="<table border='1' cellspacing='0' cellpadding='0' style='cursor:default;width:100%;height:100%'>";
		tableHtml+="<td id=\""+this.dsId+"Left\" width='50%' nowrap='true'></td>";
		tableHtml+="<td id=\""+this.dsId+"Center\" width='1' align='center' valign='top'><br/>";
		tableHtml+="<button  class='pageBtn' id=\""+this.dsId+"Up\">上移</button><br/><br/>";
		tableHtml+="<button  class='pageBtn' id=\""+this.dsId+"Down\">下移</button><br/><br/>";
		tableHtml+="<button class='pageBtn' id=\""+this.dsId+"Add\">右移</button><br/><br/>";
		tableHtml+="<button class='pageBtn' id=\""+this.dsId+"Del\">左移</button><br/><br/>";
		tableHtml+="<button class='pageBtn' id=\""+this.dsId+"AAdd\">全选</button><br/><br/>";
		tableHtml+="<button class='pageBtn' id=\""+this.dsId+"ADel\">全删</button><br/><br/>";
		tableHtml+="</td>";
		tableHtml+="<td id=\""+this.dsId+"Right\" width='50%' nowrap='true'></td>";
		tableHtml+="</tr></table></div>";
		this.dsBox.innerHTML=tableHtml;
		
		var dict=null;
		if (trim(this.dsPara)==""||this.dsPara=="null") {
			dict=window.getDict(this.dsLoad);
		} else {
			dict=window.getDict(this.dsLoad,this.dsPara);
		}
		var paras=dict.getElementsByTagName("para");
		for(var i=0;i<paras.length;i++){
			this.listCode.push(paras[i].getAttribute("code"));
			this.listText.push(paras[i].getAttribute("value"));
		}
		
		var DS=this;
		this.dsWin.document.getElementById(this.dsId+"Up").onclick=function(){DS.doUp();};
		this.dsWin.document.getElementById(this.dsId+"Down").onclick=function(){DS.doDown();};
		this.dsWin.document.getElementById(this.dsId+"Add").onclick=function(){DS.doAdd();};
		this.dsWin.document.getElementById(this.dsId+"Del").onclick=function(){DS.doDel();};
		this.dsWin.document.getElementById(this.dsId+"AAdd").onclick=function(){DS.doAAdd();};
		this.dsWin.document.getElementById(this.dsId+"ADel").onclick=function(){DS.doADel();};
		this.leftTab=new comm_DoubleSelectTableChild(this.dsWin,this.dsWin.document.getElementById(this.dsId+"Left"),function(){DS.doAdd();});
		this.rightTab=new comm_DoubleSelectTableChild(this.dsWin,this.dsWin.document.getElementById(this.dsId+"Right"),function(){DS.doDel();});
		
		this.doChange();
	}
	this.doUp=function(){
		for(var i=1;i<this.selListCode.length;i++){
			if(this.rightTab.getSelCode()==this.selListCode[i]){
				var t=this.selListCode[i-1];
				this.selListCode[i-1]=this.selListCode[i];
				this.selListCode[i]=t;
				break;
			}
		}
		this.doChange(this.rightTab.getSelCode());
	}
	this.doDown=function(){
		for(var i=0;i<this.selListCode.length-1;i++){
			if(this.rightTab.getSelCode()==this.selListCode[i]){
				var t=this.selListCode[i+1];
				this.selListCode[i+1]=this.selListCode[i];
				this.selListCode[i]=t;
				break;
			}
		}
		this.doChange(this.rightTab.getSelCode());
	}
	this.doAdd=function(){
		//判断当前选择的编码是否在selListCode之中
		var isAdd = true;
		for(var i=0;i< this.selListCode.length;i++){
			if(this.selListCode[i]==this.leftTab.getSelCode()){
				isAdd = false;
				break;
			}
		}
		
		if(isAdd){
			this.selListCode.push(this.leftTab.getSelCode());
		}
		this.leftTab.msSelCode = "";
		this.doChange();
	}
	this.doDel=function(){
		for(var i=0;i<this.selListCode.length;i++){
			if(this.rightTab.getSelCode()==this.selListCode[i]){
				this.removeArrayElem(this.selListCode,i);
				break;
			}
		}
		this.doChange();
	}
	this.doAAdd=function(){
		this.selListCode=this.listCode.slice(0,this.listCode.length);
		this.leftTab.msSelCode = "";
		this.doChange();
	}
	this.doADel=function(){
		this.selListCode=new Array();
		this.doChange();
	}
	this.doChange=function(selCode){
		var leftCode=this.listCode.slice(0,this.listCode.length);
		var leftText=this.listText.slice(0,this.listText.length);
		var rightCode=new Array();
		var rightText=new Array();
		var str=","+this.selListCode+",";
		for(var i=0;i<this.selListCode.length;i++){
			for(var j=0;j<leftCode.length;j++){
				if(this.selListCode[i]==leftCode[j]){
					rightCode.push(this.selListCode[i]);
					rightText.push(leftText[j]);
					this.removeArrayElem(leftCode,j);
					this.removeArrayElem(leftText,j);
					break;
				}
			}
		}
		var rightSel="";
		var leftSel="";
		if(leftCode.length>0)
			leftSel=leftCode[0];
		if(selCode){
			rightSel=selCode;
		}else if(rightCode.length>0)
			rightSel=rightCode[0];
		this.leftTab.setValue(leftCode,leftText,leftSel);
		this.rightTab.setValue(rightCode,rightText,rightSel);
	}
	this.removeArrayElem=function(a,index){
		var i;
		if(index < a.length){
			for(i=index;i <a.length-1;i++){
				a[i]=a[i+1];
			}
			a.length=a.length-1;
		}
	}
	this.getValue=function(){
		var res="";
		for(var i=0;i<this.selListCode.length;i++){
			res+="'"+this.selListCode[i]+"',"
		}
		if(res.length>0)
			res=res.substr(0,res.length-1);
		return res;
	}
	this.setValue=function(v){
		var str=v;
		if(v.length>0){
			this.selListCode=str.substr(1,str.length-2).split("','");
		}else
			this.selListCode=new Array();
		this.doChange();
	}
	this.init();
}
function comm_FormulaJob(jobid,win,lineCtn,btnObj,msg){
	this.lineCtnObj=lineCtn;
	this.buttonObj=btnObj;
	this.winObj=win;
	this.progMsg=msg;
	this.jobId=jobid;
	this.oldBtnText=btnObj.value;
	this.init=function(){
		this.buttonObj.disabled=true;
		var sf=this.jobId==null?"":"?jobid="+this.jobId;
		if(this.lineCtnObj.submit(this.buttonObj,null,null,sf)){
			var data=this.parseData(this.winObj.xmlhttp._object.responseText);
			this.jobId=data[1];
			win.curFormulaJob=this;
			win.setTimeout(function(){
				win.curFormulaJob.getProcess();
			},10);
		}
	}
	this.getProcess=function(){
		this.winObj.xmlhttp.post(this.buttonObj.name,"<root></root>", "?jobpi=prog&jobid="+this.jobId);
		if(!this.showError(this.winObj.xmlhttp._object.responseText)){
			this.end();
			return ;
		}
		var data=this.parseData(this.winObj.xmlhttp._object.responseText);
		if(data[1]=="1"){
			this.buttonObj.value=this.progMsg.replace("P",data[2]);
			win.setTimeout(function(){
				win.curFormulaJob.getProcess();
			},200);
		}else{
			this.getResult();
		}
	}
	this.showError=function(e){
		if(e.indexOf("<error>")>=0){
			window.doMsg(e,"");
			return false;
		}
		return true;
	}
	this.getResult=function(){
		this.end();
		this.lineCtnObj.submit(this.buttonObj,null,null,"?jobpi=result&jobid="+this.jobId,"");
	}
	this.parseData=function(str){
		var d=str.substr(str.indexOf("<td>"),str.lastIndexOf("</td>")-str.indexOf("<td>"));
		d=d.replace(/<td>/g,"");
		return d.split("</td>");
	}
	this.end=function(){
		this.buttonObj.value=this.oldBtnText;
		this.buttonObj.disabled=false;
	}
	this.init();
}
///////////////////////////////
_framDlgGj={};
_framDlgGj.__sequenceId=1;
_framDlgGj.getNextSid=function(){
	return this.__sequenceId++;
}
_framDlgGj.getNextZIndex=function(){
	return this.getNextSid();
}
_framDlgGj.generateId=function(){
	return "gjid"+this.getNextSid();
}
_framDlgGj.foreachElementChild=function(elem,fun){
	if(elem==null)
		return;
	var fe=elem.firstChild;
	while(fe!=null){
		if(fe.nodeType==1){
			fun(fe);
		}
		fe=fe.nextSibling;
	}
}
_framDlgGj.getElementsByAttribute=function(attName){
	var box=[];
	_framDlgGj.foreachElementChild(elem,function(ele){
		var av=ele.getAttribute(attName);
		if(av!=null)
			box.push(ele);
	});
	return box;
}
_framDlgGj.getElementsByAttributeValue=function(elem,attName,attValue){
	var box=[];
	_framDlgGj.foreachElementChild(elem,function(ele){
		var av=ele.getAttribute(attName);
		if(attValue==av)
			box.push(ele);
	});
	return box;
}
///////////////////////////
var _openIFDialog_list=null;
var _openIFDialog_html=null;
var _openIFDialog_black=null;
var _openIFDialog_key="_8_dlg_";
var _openIFDialog_titlePre=null;
var _tempButtonKey_arr=null;
var _tempButton_arr=null;
var _tempIFDialog_count=0;
var _tempIFDialog_arr=null;
function _openIFDialog_init(){
	if(_openIFDialog_list!=null)
		return;
	_openIFDialog_titlePre="H-ERP - ";
	var imgPre=window.prefix+"images2/ifdlg/";
	_openIFDialog_html="<table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td align=\"center\" valign=\"middle\" ><iframe id=\"_PARA_ID__base\"  style=\"position:absolute; \" src=\"\" width=\"30\" height=\"30\"  frameborder=\"0\" scrolling=\"no\"></iframe><table  id=\"_PARA_ID__box\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"position:absolute;\" onmousedown=\"_openIFDialog_drag_down(this)\">";
	_openIFDialog_html+="	<tr>";
	_openIFDialog_html+="		<td id=\"_PARA_ID__top\" colspan=\"3\" style=\"cursor:default\">";
	_openIFDialog_html+="			<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
	_openIFDialog_html+="				<tr>";
	_openIFDialog_html+="					<td width=\"1\" ><img src=\""+imgPre+"2_03.gif\"></td>";
	_openIFDialog_html+="					<td width=\"1\" ><img src=\""+imgPre+"2_04.gif\"></td>";
	_openIFDialog_html+="					<td width=\"100%\" background=\""+imgPre+"2_06.jpg\"><table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style='table-layout:fixed'><tr><td valign='middle' noWrap='true' id=\"_PARA_ID__title\" align=\"left\" valign=\"middle\" style=\"width:90%;height:18;font-weight:bold; color:#FFFFFF;\" onselectstart ='return false'></td></tr></table></td>";
	_openIFDialog_html+="					<td width=\"1\"  background=\""+imgPre+"2_06.jpg\"><img id=\"_PARA_ID__close\" src=\""+imgPre+"2_12.png\" onclick=\"_openIFDialog_closePageByIdx(this._dlgIdx)\"  style=\"cursor:hand\"></td>";
	_openIFDialog_html+="					<td width=\"1\" ><img src=\""+imgPre+"2_08.gif\" ></td>";
	_openIFDialog_html+="					<td width=\"1\" ><img src=\""+imgPre+"2_09.gif\" ></td>";
	_openIFDialog_html+="				</tr>";
	_openIFDialog_html+="			</table>";
	_openIFDialog_html+="		</td>";
	_openIFDialog_html+="		</tr>";
	_openIFDialog_html+="		<tr>";
	_openIFDialog_html+="		<td background=\""+imgPre+"2_14.jpg\"></td>";
	_openIFDialog_html+="		<td width=\"100%\" height=\"100%\"><iframe id=\"_PARA_ID__iframe\" src=\"\" width=\"100%\" height=\"100%\"  frameborder=\"0\" scrolling=\"auto\"></iframe></td>";
	_openIFDialog_html+="		<td background=\""+imgPre+"2_18.jpg\"></td>";
	_openIFDialog_html+="		</tr>";
	_openIFDialog_html+="		<tr>";
	_openIFDialog_html+="		<td><img src=\""+imgPre+"2_21.jpg\" ></td>";
	_openIFDialog_html+="		<td background=\""+imgPre+"2_22.jpg\"></td>";
	_openIFDialog_html+="		<td><img id=\"_PARA_ID__resize\" src=\""+imgPre+"2_24.jpg\" ";
		//Dialog_html+="  onmousedown=\"_openIFDialogS_drag_down(this)\" style=\"position:relative;cursor:se-resize\" ";
	_openIFDialog_html+="	></td>";
	_openIFDialog_html+="	</tr>";
	_openIFDialog_html+="</table></td></tr></table>";
	
	_openIFDialog_black="<div style=\"position:absolute; left:0px; top:0px; width:100%; height:100%; z-index:100000000000\"><table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td bgcolor='#FFFFFF'></td></tr></table></div>";
	_openIFDialog_list=[];
	for(var i=0;i<1;i++){
		_openIFDialog_create();
	}
}
function _openIFDialog_create(){
	var div=window.document.createElement("div");
	var id=_framDlgGj.generateId();
	div.style["position"]="absolute";
	//div.style["display"]="none";
	div.innerHTML=_openIFDialog_html.replace(/_PARA_ID_/g,id);
	div.style["top"]=0;
	div.style["left"]=0;
	div.id=id;
	div.__isUse=false;
	window.document.body.appendChild(div);
	div.__callback=null;
	div.__title=window.document.getElementById(id+"_title");
	div.__close=window.document.getElementById(id+"_close");
	div.__base=window.document.getElementById(id+"_base");
	div.__resize=window.document.getElementById(id+"_resize");
	div.__box=window.document.getElementById(id+"_box");
	div.__box._dlg=div;
	div.__box._top=window.document.getElementById(id+"_top");
	//div.__iframe=window.document.getElementById(id+"_iframe");
	div.__iframe=eval(id+"_iframe");
	div.__level=_openIFDialog_list.length;
	div.__close._dlgIdx=div.__level;
	div.__resize._box=div.__box;
	
	div.style["zIndex"]=_framDlgGj.getNextZIndex();
	div.__base.style["zIndex"]=_framDlgGj.getNextZIndex();
	div.__box.style["zIndex"]=_framDlgGj.getNextZIndex();
	_openIFDialog_list.push(div);
	return div;
}
function _openIFDialog_nextDlg(){
	var dlg=null;
	for(var i=0;i<_openIFDialog_list.length;i++){
		if(_openIFDialog_list[i].__isUse==false){
			dlg=_openIFDialog_list[i];
			break;
		}
	}
	if(dlg==null){
		dlg=_openIFDialog_create();
	}
	dlg.__isUse=true;
	dlg.__close.style.display="";
	return dlg;
}
function _openIFDialog_closePage(win){
	if(win.__ifdlgIdx){
		_openIFDialog_closePageByIdx(parseInt(win.__ifdlgIdx,10));
	}else
		win.close();
}
var _openIFDialog_drag_obj=null,_openIFDialog_drag_x=0,_openIFDialog_drag_y=0,_openIFDialog_drag_w=0,_openIFDialog_drag_h=0;
function _openIFDialog_drag_down(obj){
	if(!obj._top.contains(event.srcElement))
		return;
	_openIFDialog_drag_obj=obj;
	_openIFDialog_drag_x=event.x-obj.style.pixelLeft;
	_openIFDialog_drag_y=event.y-obj.style.pixelTop;
	window.document.attachEvent("onmousemove",_openIFDialog_drag_move);
	window.document.attachEvent("onmouseup",_openIFDialog_drag_up);
	obj.setCapture();
}
function _openIFDialog_drag_move(){
	if(_openIFDialog_drag_obj!=null){
		var x=event.x-_openIFDialog_drag_x;
		var y=event.y-_openIFDialog_drag_y;
		if(x<0)
			x=0;
		if(x>document.body.clientWidth)
			x=document.body.clientWidth;
		if(y<0)
			y=0;
		if(y>document.body.clientHeight)
			x=document.body.clientHeight;
		_openIFDialog_drag_obj.style.left=x;
		_openIFDialog_drag_obj.style.top=y;
		_openIFDialog_resizebase(_openIFDialog_drag_obj._dlg);
	}
}
function _openIFDialog_drag_up(){
	if(_openIFDialog_drag_obj!=null){
		_openIFDialog_drag_obj.releaseCapture();
		window.document.detachEvent("onmousemove",_openIFDialog_drag_move);
		window.document.detachEvent("onmouseup",_openIFDialog_drag_up);
		_openIFDialog_drag_obj=null;
	}
}
function _openIFDialogS_drag_down(obj){
	_openIFDialog_drag_obj=obj;
	_openIFDialog_drag_w=parseInt(obj._box.style.width.replace("px",""),10);;
	_openIFDialog_drag_h=parseInt(obj._box.style.height.replace("px",""),10);
	_openIFDialog_drag_x=event.clientX;
	_openIFDialog_drag_y=event.clientY;
	event.cancelBubble = true
	window.document.attachEvent("onmousemove",_openIFDialogS_drag_move);
	window.document.attachEvent("onmouseup",_openIFDialogS_drag_up);
	obj.setCapture();
}
function _openIFDialogS_drag_move(){
	if(_openIFDialog_drag_obj!=null){
		var x=event.clientX-_openIFDialog_drag_x;
		var y=event.clientY-_openIFDialog_drag_y;
		_openIFDialog_drag_obj._box.style.width=_openIFDialog_drag_w+x;
		_openIFDialog_drag_obj._box.style.height=_openIFDialog_drag_h+y;
		_openIFDialog_resizebase(_openIFDialog_drag_obj._box._dlg);
	}
}
function _openIFDialogS_drag_up(){
	if(_openIFDialog_drag_obj!=null){
		_openIFDialog_drag_obj.releaseCapture();
		window.document.detachEvent("onmousemove",_openIFDialogS_drag_move);
		window.document.detachEvent("onmouseup",_openIFDialogS_drag_up);
		_openIFDialog_drag_obj=null;
	}
}
function _openIFDialog_resizebase(dlg){
	dlg.__base.style.top=dlg.__box.style.top;
	dlg.__base.style.left=dlg.__box.style.left;
	dlg.__base.style.width=dlg.__box.style.width;
	dlg.__base.style.height=dlg.__box.style.height;
}
function _openIFDialog_closePageByIdx(dlgIdx){
	var dlg=_openIFDialog_list[dlgIdx];
	if(dlg.__iframe.onclosepage){
		if(!dlg.__iframe.onclosepage())
			return;
	}
	dlg.style.display="none";
	dlg.__isUse=false;
	dlg.__curWin=null;
	//var div=dlg.__iframe.document.createElement("div");
	//div.innerHTML=_openIFDialog_black;
	//dlg.__iframe.document.body.appendChild(div);
	//dlg.__iframe.document.body.innerHTML="";
	dlg.__iframe.document.location.href=window.prefix+"blank.htm";
	if(dlg.__callback)
		dlg.__callback();
	dlg.__callback=null;
	if(dlgIdx>0){
		_openIFDialog_list[dlgIdx-1].__iframe.focus();
	}
	
	//对应bug HBOSTHIRDNEW-2762
	//修改思路为 	步骤一：弹出新层时把上级层所有按钮快捷键保存后置空 	步骤二：关闭当前层时把上级层所有按钮快捷键恢复
	//这里对应的是上述步骤 二    ---李乃坤
	if(_tempIFDialog_count > 0){
		var _tmpBtns = _tempButton_arr[_tempIFDialog_count-1];
		var _tmpBtnKeys = _tempButtonKey_arr[_tempIFDialog_count-1];
		for(var z=0;z<_tmpBtns.length;z++){
			var _tmpBtn = _tmpBtns[z];
			_tmpBtn.accessKey = _tmpBtnKeys[_tmpBtn.innerText];
		}
		_tempIFDialog_count--;
	}
}
function _openIFDialog_parseIdx(urlStr){
	if(urlStr.indexOf(_openIFDialog_key+"=")>0){
		urlStr=urlStr.substr(urlStr.indexOf(_openIFDialog_key+"=")+(_openIFDialog_key+"=").length);
		return parseInt(urlStr,10);
	}else
		return -1;
}
function _openIFDialog_pageonload(win){
	var tit=_framDlgGj.getElementsByAttributeValue(win.document.body,"className","titleHeader");
	var titText;
	if(tit.length>0)
		titText=tit[0].innerText;
	else
		titText=win.document.title;
	win.setPageTitle(titText);
	win.detachEvent("onload",win.__ifdlgOnload);
	win.__ifdlgOnload=null;
}
function _openIFDialog_pageInit(win){
	var pp=null;
	var dlgIdx=_openIFDialog_parseIdx(win.document.URL.toString());
	if(dlgIdx>=0){
		win.setPageTitle=function(t){_openIFDialog_list[win.__ifdlgIdx].__title.innerText=_openIFDialog_titlePre+t;};
		var ol=function(){_openIFDialog_pageonload(win)};
		win.__ifdlgOnload=ol;
		win.attachEvent("onload",ol);
		win.__ifdlgIdx=""+dlgIdx;
		pp=_openIFDialog_list[dlgIdx].__curWin;
		win.focus();
	}else{
		pp=typeof(win.dialogArguments)=="undefined"?parent:win.dialogArguments;
		win.setPageTitle=function(t){win.document.title=_openIFDialog_titlePre+t;};
	}
	win.__ifdlgtag=_openIFDialog_key;
	win.parentPage=pp;
	win.closePage=function(){_openIFDialog_closePage(this);}
	win.openIFDialog=openIFDialog;
}
// curWin: 当前窗口,url:地址,features:对话框样式(close,maxsize,dialogWidth,dialogHeight),cb:关闭调用
function openIFDialog(curWin,url,features,cb,vhFlag){
	if (vhFlag != null && vhFlag == true) {
		window.prefix = window.prefix.replace('/vh/','/') 
	}
	_openIFDialog_init();
	dlg=_openIFDialog_nextDlg();
	dlg.style.width=document.body.clientWidth;
	dlg.style.height=document.body.clientHeight;
	dlg.__iframe.document.location.href=window.prefix+"blank.htm";
	var dlgW=500;
	var dlgH=400;
	if(features){
		var feas=features.toLowerCase().split(";");
		var max=false;
		for(var i=0;i<feas.length;i++){
			var feass=feas[i].split(":");
			if(feass[0].trim()=="dialogwidth")
				dlgW=feass[1].replace(/px/g,"");
			if(feass[0].trim()=="dialogheight")
				dlgH=feass[1].replace(/px/g,"");
			if(feass[0].trim()=="maxsize"&&feass[1].trim()=="yes"){
				max=true;
			}
			if(feass[0].trim()=="close"&&feass[1].trim()=="no")
				dlg.__close.style.display="none";
		}
		if(dlgW=="[maxw]"||dlgH=="[maxh]")
			max=true;
		if(max==true){
			dlgW=document.body.clientWidth-15;
			dlgH=document.body.clientHeight-15;
		}
	}
	dlg.__curWin=curWin;
	dlg.__box.style.top=(document.body.clientHeight-dlgH)/2;
	dlg.__box.style.left=(document.body.clientWidth-dlgW)/2;
	dlg.__box.style.width=dlgW;
	dlg.__box.style.height=dlgH;
	_openIFDialog_resizebase(dlg);
	if(url.indexOf("?")>0){
		if (vhFlag == null || vhFlag == false) {
			url+="&"+_openIFDialog_key+"="+dlg.__level;
		}
	}else{
		url+="?"+_openIFDialog_key+"="+dlg.__level;
	}
	dlg.__callback=cb;
	dlg.__iframe.document.location.href=url;
	dlg.style.display="block";
	//对应bug HBOSTHIRDNEW-2762
	//修改思路为 	步骤一：弹出新层时把上级层所有按钮快捷键保存后置空 	步骤二：关闭当前层时把上级层所有按钮快捷键恢复
	//这里对应的是上述步骤一    ---李乃坤
	var _tmpBtns = null
	var _tmpBtnKeys = {};
	_tempIFDialog_count++;
	if(_tempIFDialog_count == 1){
		_tempButton_arr = new Array();
		_tempButtonKey_arr = new Array();
		_tempIFDialog_arr = new Array();
		_tmpBtns = curWin.document.getElementsByTagName("button");
		for(var z=0;z<_tmpBtns.length;z++){
			var _tmpBtn = _tmpBtns[z];
			_tmpBtnKeys[_tmpBtn.innerText] = _tmpBtn.accessKey;
			_tmpBtn.accessKey = null;
		}
		_tempIFDialog_arr[0] = curWin;
		_tempButton_arr[0] = _tmpBtns;
		_tempButtonKey_arr[0] = _tmpBtnKeys;
		_tempIFDialog_arr[1] = dlg.__iframe;
	}else if(_tempIFDialog_count > 1){
		_tmpBtns = _tempIFDialog_arr[_tempIFDialog_count-1].document.getElementsByTagName("button");
		for(var z=0;z<_tmpBtns.length;z++){
			var _tmpBtn = _tmpBtns[z];
			_tmpBtnKeys[_tmpBtn.innerText] = _tmpBtn.accessKey;
			_tmpBtn.accessKey = null;
		}
		_tempButton_arr[_tempIFDialog_count-1] = _tmpBtns;
		_tempButtonKey_arr[_tempIFDialog_count-1] = _tmpBtnKeys;
		_tempIFDialog_arr[_tempIFDialog_count] = dlg.__iframe;
	}
	return dlg;
}
var baseSystemFunMap={
//////////////baseSystemFunMap BEGIN comm_
"createDoubleSelectTable":function(win,div,load,para){
		return new comm_DoubleSelectTable(win,div,load,para); 
	},
"createFormulaJob":function(jobid,win,lineCtn,btnObj,msg){
	var f=new comm_FormulaJob(jobid,win,lineCtn,btnObj,msg);
	return f;
},
"_framDlgGj":_framDlgGj,
"openIFDialog":openIFDialog
/////////baseSystemFunMap end comm_
}