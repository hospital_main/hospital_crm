var acctModulePrivateFunMap={
	//后面注释  
	/*参数说明
	startRow：最小开始行
	sumCol：承前页、过次页所在列
	sumFiled：需要合计的列，格式：[column1,column2]
	resField：该列直接取上一行，格式：[column1,column2]
	fun：处理函数
	sumOriValue：基准值，格式：[column1,column2]需要跟sumFiled对应
	*/
	acctPrintAccountOnBefore:function(cellObj,startRow,sumCol,sumFiled,resField,fun,sumOriValue){
		window.__acctPrintRow=[];
		var sumValue=[],resValue=[];
		var r=startRow;
		for(var i=0;i<sumFiled.length;i++){
			sumValue[i]=0.00;
		}
		while(r<=cellObj.GetRows(0)){
			var f=fun(cellObj,r);/// 1 加 0 清零加 -1 不加
			if(f==0){
				for(var i=0;i<sumFiled.length;i++){
					sumValue[i]=0.00;
				}
			}
			if(f==3){
				for(var i=0;i<sumFiled.length;i++){
					sumValue[i]=parseFloat(sumOriValue[i]);
				}
			}
			if(f==2){
				for(var i=0;i<sumFiled.length;i++){
					var v=cellObj.GetCellString(sumFiled[i],r,0).replace(/\,/g,"").replace(/\s/g,"").replace(/　/g,"");
					if(v.length>0)
						sumValue[i]=parseFloat(v=="Q"?"0":v);
				}
			}
			if(f==0||f==1){
				for(var i=0;i<sumFiled.length;i++){
					var v=cellObj.GetCellString(sumFiled[i],r,0).replace(/\,/g,"").replace(/\s/g,"").replace(/　/g,"");
					if(v.length>0)
						sumValue[i]+=parseFloat(v=="Q"?"0":v);
				}
			}
			if(r<cellObj.GetRows(0)-1){
				if(cellObj.IsRowPageBreak(r+2)==1){
					cellObj.InsertRow(r+1,2,0);
					cellObj.S(sumCol,r+1,0,"过次页");
					cellObj.S(sumCol,r+2,0,"承前页");
					for(var i=0;i<resField.length;i++){
						cellObj.S(resField[i],r+1,0,cellObj.GetCellString(resField[i],r,0));
						cellObj.S(resField[i],r+2,0,cellObj.GetCellString(resField[i],r,0));
					}
					if(sumFiled.length>0){
						for(var i=0;i<sumFiled.length;i++){
							cellObj.SetCellNumType(sumFiled[i],r+1,0,1);
							cellObj.SetCellNumType(sumFiled[i],r+2,0,1);
							cellObj.SetCellSeparator(sumFiled[i],r+1,0,2);
							cellObj.SetCellSeparator(sumFiled[i],r+2,0,2);
							cellObj.SetCellDouble(sumFiled[i],r+1,0,sumValue[i]);
							cellObj.SetCellDouble(sumFiled[i],r+2,0,sumValue[i]);
						}
					}
					window.__acctPrintRow.push(r+1);
					window.__acctPrintRow.push(r+2);
					r+=2;
				}
				r++;
			}else{
				r++;
			}
		}
	},
	acctPrintAccountOnAfter:function(cellObj){
		for(var i=window.__acctPrintRow.length-1;i>=0;i--){
			cellObj.DeleteRow(window.__acctPrintRow[i],1,0);
		}
		window.__acctPrintRow=null;
	}
	
}