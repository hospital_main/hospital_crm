///Autor:Liu Zhikun Create Date:2007-12-12
var budgModulePrivateFunMap={
//function begin
"budgMonthListInit":function(obj){
		obj.para="<y>"+getAcctYear()+"</y>";
		obj.refresh();
	},
"budgPrintXmlToCell":function(){
		if(arguments.length!=4 && arguments.length!=6 && arguments.length!=7){
			alert("参数个数不正确");
			return;
		}
		var data;
		var isTrue;
		var isPreview;
		var xlturl;
		var pageurl;
		
		if(arguments.length==4){//直接贯入数据
			if(typeof(arguments[0])=="Object"){
				printXmlDataToCell(arguments[0],arguments[1],arguments[2],arguments[3]);
			}else{
				printStringDataToCell(arguments[0],arguments[1],arguments[2],arguments[3]);
			}
		}
		if(arguments.length==6){//需要通过样式表转换
			data=arguments[2];
			isTrue=arguments[3];
			isPreview=arguments[4];
			pageurl=arguments[5];
			
			if(pageurl.indexOf("?")>0)
				pageurl = pageurl.substring(0,pageurl.indexOf("?"));
				
			xlturl=pageurl.substring(0,pageurl.lastIndexOf("/")+1)+arguments[1];
			printXmlToCellByXsltFile(arguments[0],xlturl,data,isTrue,isPreview,pageurl);
		}
		
		if(arguments.length==7){//查询出所有数据
			data=arguments[3];
			isTrue=arguments[4];
			isPreview=arguments[5];
			pageurl=arguments[6];
			var _xmlhttp=new XmlHttp();
			_xmlhttp.post(typeof(arguments[0])=="object" ? arguments[0].name:arguments[0],arguments[1]);
			var result=_xmlhttp._object.responseText;
			
			if(pageurl.indexOf("?")>0)
				pageurl = pageurl.substring(0,pageurl.indexOf("?"));
				
			xlturl=pageurl.substring(0,pageurl.lastIndexOf("/")+1)+arguments[2];
			printXmlToCellByXsltFile(result,xlturl,data,isTrue,isPreview,pageurl);
		}
	},
"openChartPage":function(para,stl){
		var rootPath=window.prefix;
		var path=rootPath+"hbos/budg2/common/chart/chart.html";
		var s;
		if(!stl){
			s='dialogWidth:425px;dialogHeight:380px';
			
		}else{
			s=stl;
		}
		openDialog(path+'?load='+para,s);
	},
"doDateValidate":function(obj,dateValueObj){
		//var objRegExp = /^\d{4}(\-|\/|\.)\d{1,2}\1\d{1,2}$/;
		if(dateValueObj==null || dateValueObj.value==""){
			//alert("日期格式错误,必须为'2007-12-01'格式!");
			return;
		}
		var objRegExp = /^\d{4}(\-)\d{1,2}\1\d{1,2}$/;
		var istrue=objRegExp.test(dateValueObj.value);
		if(!istrue){
			obj.alert("日期格式错误,必须为'2007-12-01'格式!");
			dateValueObj.focus();
			return;
		}
	},
"budgSetCalendarRang":function(b,e){
		var bd=getAcctYearBeginDate();
		var ed=getAcctYearEndDate();
		if(b){
			b.mindate=bd;
			b.maxdate=ed;
		}
		if(e){
			e.mindate=bd;
			e.maxdate=ed;
		}
		if(b&&e){
			b.value=bd;
			e.value=ed;
			e.greatthan=b.name;
		}else{
			if(getCurrentDate()>ed)
				b.value=ed;
			else
				b.value=getCurrentDate();
		}
	},
"budgBtnDisabledByYearFlag":function(){
		var p=getValuePairBySql("budg_edit_year_flag","<y>"+getAcctYear()+"</y>");
		var res=true;
		if(p==null||p[0]=="0")
			res=false;
		else
			res=true;
		if(arguments.length>0){
			for(var i=0;i<arguments.length;i++){
				arguments[i].disabled=res;
			}
		}
		return res;
	}
//function end
};