<%@ page language="java" import="java.util.*" pageEncoding="GBK"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns:vh>
  <head>
    <title>图形展示页面</title>	
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="this is my page">
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<link href="" rel="stylesheet" type="text/css">
    <script language="javascript" id="scriptID"></script>
    <script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>
    <script language="JavaScript" src="FusionCharts.js"></script>
  </head>
  <body class="subBody" id="subBody" onLoad="init();">
    <div id="FunsionChart_div"></div>
  </body>
  <script>
    function init(){
			var args = GetUrlParms();
			var xml=window.parentPage.FusionChartsXML;
			var type = args['type'];
			if(xml!=null&&type!=null){
				var chart;
				if(type==1){
					chart =new FusionCharts("MSLine.swf", "ChartId", "100%", "100%", "0", "0");
				}else if(type==2){
					chart =new FusionCharts("MSColumn2D.swf", "ChartId", "100%", "100%", "0", "0");
				}else if(type==3){
					chart =new FusionCharts("Pie2D.swf", "ChartId", "100%", "100%", "0", "0");
				}
				chart.setDataXML(xml);   
				chart.render("FunsionChart_div");
			}else{
				alert("传入参数不足！");
			}
		}
		function GetUrlParms(){
		    var args=new Object();   
		    var query=location.search.substring(1);//获取查询串   
		    query=decodeURI(query);
		    var pairs=query.split("&");//在逗号处断开   
		    for(var   i=0;i<pairs.length;i++){   
		        var pos=pairs[i].indexOf('=');//查找name=value   
		            if(pos==-1)   continue;//如果没有找到就跳过   
		            var argname=pairs[i].substring(0,pos);//提取name   
		            var value=pairs[i].substring(pos+1);//提取value   
		            args[argname]=unescape(value);//存为属性   
		    }
		    return args;
		}

	</script>
</html>

