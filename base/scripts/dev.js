
var devModulePrivateFunMap={
//function begin
"devCheckFormulaChar":function(obj){
		var n=obj.value;
		if(n.indexOf("+")>=0
			||n.indexOf("-")>=0
			||n.indexOf("*")>=0
			||n.indexOf("/")>=0
			||n.indexOf(")")>=0
			||n.indexOf("(")>=0
			||n.indexOf(".")>=0
			||n.indexOf("\"")>=0
		){
			alert(obj.htcLabel+"不能输入特殊字符!");
			return false;
		}
		return true;
	},
"getDevParaValue":function(para){
	if(para=="CUR_YEAR" || para=="CUR_MONTH"){
		var p=getValuePairBySql("devGetCurYearAndMonth","")
		if(p==null)return "";
		if(para=="CUR_YEAR"){
			return p[0];
		}else{
			return p[1];
		}
	}else{
		var obj = getEnvionmentValue("devParaData")
		return obj[para];
		}
	},
"getDevPerfState":function(){
	var obj = getValuePairBySql("devPerfState","");
	if(obj[0] == "1")
		return "已下达"
	else
		return "未下达";
	}
	
//function end
};