(function inputValid(){
	inputValid = {
		invalidChars : ['<','>','"','&','\'','\\','/'],
		validChars : validChars
	}
	
	function validChars(para)
	{
		if(para != undefined && para != null)
		{
			with(inputValid)
			{
				for(var i=0,j=invalidChars.length; i<j; i++)
				{
					if(para.indexOf(invalidChars[i]) != -1)
					{
						return true;
					}
				}
			}
		}
		return false;
	}
})();