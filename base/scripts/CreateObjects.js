//调用图形窗口
function openGraphDialog(gtype,gTitle,xmlData,xAxes,yAxes,isTipShow){
	if(xmlData=="<root></root>" || xmlData=="<root>undefined</root>"){
		alert("请选择数据！");
		return;
	}
	fusionchartsShow(gtype,gTitle,xmlData,xAxes,yAxes,isTipShow);
//	openGraphDialog_old(gtype,gTitle,xmlData,xAxes,yAxes,isTipShow);
}

function openGraphDialog_old(gtype,gTitle,xmlData,xAxes,yAxes,isTipShow){ 
	__dept_sign__ = xmlData;
	__gTitle__ = gTitle;
	if(gtype=="2"){
		__yAxes__='元';
		if( typeof(yAxes) !="undefined" ){
			__yAxes__=yAxes;
		}
		__xAxes__='';
		if( typeof(xAxes) !="undefined" ){
			__xAxes__=xAxes;
		}		
	}
	if(gtype=="1"){
		__yAxes__='元';
		if( typeof(yAxes) !="undefined" ){
			__yAxes__=yAxes;
		}
		__xAxes__='';
		if( typeof(xAxes) !="undefined" ){
			__xAxes__=xAxes;
		}
		
		openDialog(window.prefix+"base/print/LineGraph.html", 'dialogWidth:800px;dialogHeight:423px');
	}else if(gtype=="2"){
		openDialog(window.prefix+"base/print/ShapeGraph.html", 'dialogWidth:800px;dialogHeight:423px');
	}else{
		__tipShow__=true;
		if(typeof(isTipShow)!="undefined"){
			if(!isTipShow){
				__tipShow__=false;
			}else{
				__tipShow__=true;
			}
			
		}
		openDialog(window.prefix+"base/print/PieGraph.html", 'dialogWidth:800px;dialogHeight:423px');
	}
	// return __getDeptsFromListTemp__;
}

// 创建控件
function CreateObjectControl(objID, ControlTYPE, ControlWIDTH, ControlHEIGHT, editDiv, isvisible,iswrite)
{
	var sClsID = "";
	var sCodeBase = "";
	var sURL = window.prefix;
	var auth = document.cookie;

  var screenHeight=window.screen.availHeight;
  var screenWidth=window.screen.availWidth;

	var tmpWidth=ControlWIDTH;
	var tmpHeight=ControlHEIGHT;
	
	if((""+ControlWIDTH).indexOf("%")>0){
		tmpWidth = screenWidth * parseFloat(ControlWIDTH.replace('%',''))/100
	}
	if((""+ControlHEIGHT).indexOf("%")>0){
		tmpHeight = screenHeight * parseFloat(ControlHEIGHT.replace('%',''))/100
	}
	
	if (ControlTYPE == "asst"){
		sClsID = "CLSID:EAB16609-F6BF-4BEA-B6FD-11C64927196F";
		sCodeBase = sURL + "base/activex/asstTable.CAB#version=2,1,0,112";
	} 
  	else if (ControlTYPE == "VHTable")
	{
		sClsID = "CLSID:62C27227-C64C-44ED-B184-E9B54D582A59";
		sCodeBase = "";// sURL +
						// "base/activex/vhControlTableB.CAB#version=3,0,0,117";
	}
	else if (ControlTYPE == "Vouch")
	{
		sClsID = "CLSID:E10E9E28-110A-4146-ABF8-A61DC89FE7CC";
		sCodeBase = "";// sURL +
						// "base/activex/AccountCtl.CAB#version=2,1,0,138";//2.1.140
						// 来自田万龙的修改
	}
	else if (ControlTYPE == "vhchart")
	{
		sClsID = "CLSID:88404FA1-2B35-4C75-BBD6-DCAFEED65C52";	
		sCodeBase = sURL +"base/activex/VHChart.CAB#version=1,0,0,39";
	}
	else if (ControlTYPE == "webcell")
	{
		sClsID = "CLSID:3F166327-8030-4881-8BD2-EA25350E574A";
		sCodeBase = "";//sURL + "base/activex/webCell.CAB#version=5.2.6.1210";
	}
	else if (ControlTYPE == "vhBC")
	{
		sClsID="CLSID:5D31608A-9038-4E9F-ABBD-C2348B852460";
		sCodeBase= sURL + "base/activex/vhBarCode.CAB#version=1,0,0,86";
	}
	else if (ControlTYPE == "treeview")
	{
		var windowPageWidth=window.document.body.clientWidth;
		editDiv=document.getElementById("treeDiv");
		editDiv.style.overflow="hidden" 
		if (editDiv.getAttribute("rightFix")==null){
			if(screenWidth>1024)
				editDiv.rightFix=''+1000*windowPageWidth/screenWidth;
			else
				editDiv.rightFix=''+700*windowPageWidth/screenWidth;
		}
		if (editDiv.getAttribute("bottomFix")==null)
		editDiv.bottomFix="0"
		sClsID = "CLSID:6442D769-4366-424B-9A8B-626177373CB3";
		sCodeBase = sURL + "base/activex/vhTreeView.CAB#version=1,1,0,15";
	}
	else if (ControlTYPE == "Formula")
	{
		sClsID = "CLSID:801CC3CC-DA03-4740-845F-27D8458F531F";
		sCodeBase = "";//sURL + "base/activex/vhFormulaCtrl.CAB#version=1,0,0,52";
	}	
	else if (ControlTYPE == "newVouch")
	{
		sClsID = "CLSID:269D57F1-6799-4C3D-8284-52619FBF6B49";
		sCodeBase = "";// sURL +
						// "base/activex/vhSuperVouch.CAB#version=4,1,0,138";
	}	
	else if (ControlTYPE == "newVouchNew")
	{
		//sClsID = "CLSID:AF822624-5EB9-4391-8B1D-503BE25B4E17";
		sClsID = "CLSID:6E4A932F-07A1-4C5F-9E22-345BB1D1B598";
		// sClsID = "CLSID:2534E78C-EF1A-427C-8BA8-24BED94C1A1A";
		sCodeBase = "";// sURL +
						// "base/activex/vhSuperVouch.CAB#version=4,1,0,138";
	}	
	else if (ControlTYPE == "newVouchVertical")
	{
		//sClsID = "CLSID:AF822624-5EB9-4391-8B1D-503BE25B4E17";
		sClsID = "CLSID:691DD8D2-BF54-4551-B37D-FCC64B447A78";
		// sClsID = "CLSID:2534E78C-EF1A-427C-8BA8-24BED94C1A1A";
		sCodeBase = "";// sURL +
						// "base/activex/vhSuperVouch.CAB#version=4,1,0,138";
	}	
	else if (ControlTYPE == "equiBar")
	{
		sClsID = "CLSID:DFA5328F-7B5E-4358-8954-10283EC16A13";
		sCodeBase = sURL + "base/activex/vhBcode2.CAB#version=1,0,0,10";
	}

	var strControl = "";
	strControl += '<OBJECT id="' + objID + '" ';
	if(isvisible!="undefined"){
		if(isvisible=="none"){
			strControl=strControl + ' style="display:none" '		
			}	
	}
	strControl += 'CLASSID="' + sClsID + '" ';
	strControl += 'CODEBASE="' + sCodeBase + '" ';
	strControl += 'WIDTH="' + tmpWidth + '" ';
	strControl += 'HEIGHT="' + tmpHeight + '">';
	strControl += '<param name="WebURL" value="' + sURL + '">'
	strControl += '<param name="TableID" value="' + objID + '">';
	strControl += '</OBJECT>';
	
	if(iswrite){
		 editDiv.innerHTML=strControl;
		 return 
	}
	
	if(ControlTYPE == "vhchart"){
		strControl = '<div id="' + objID + '" HEIGHT="' + tmpHeight + '" WIDTH="' + tmpWidth + '"></div>';
		document.write(strControl);
		return;　
	}
	
	if(ControlTYPE == "Condition"){
		strControl='<object classid="clsid:D0BEE905-65D9-11D7-BF64-0000E8E49768" id="'+objID+'" width="'+tmpWidth+'" height="'+tmpHeight+'" codebase="">'
				  +'<param name="Visible" value="-1">'
				  +'<param name="AutoScroll" value="0">'
				  +'<param name="AutoSize" value="0">'
				  +'<param name="AxBorderStyle" value="1">'
				  +'<param name="Caption" value="条件生成器">'
				  +'<param name="Color" value="16185078">'
				  +'<param name="Font" value="宋体">'
				  +'<param name="KeyPreview" value="0">'
				  +'<param name="PixelsPerInch" value="96">'
				  +'<param name="PrintScale" value="1">'
				  +'<param name="Scaled" value="0">'
				  +'<param name="DropTarget" value="0">'
				  +'<param name="HelpFile" value>'
				  +'<param name="DoubleBuffered" value="0">'
				  +'<param name="Enabled" value="-1">'
				  +'<param name="Cursor" value="0">'
				  +'<param name="Ver" value="100">'
				  +'</object>';
	}

	//获取当前ip 和端口
    var url = window.location.href;
    var arr = url.split("/");
    var domai;
    var port;
   	if(arr[2].indexOf(":")>0){
       arr=arr[2].split(":");
        domai=arr[0];
        port=arr[1];
	}else{
        domai=arr[2];
    	port="";
	}
	// alert(auth);
    strControl=strControl.replace('</object>','<param name="Auth" value="' + auth + '"><param name="DoMain" value="'+domai+'"><param name="Port" value="'+port+'"></object>');
    strControl=strControl.replace('</OBJECT>','<param name="Auth" value="' + auth + '"><param name="DoMain" value="'+domai+'"><param name="Port" value="'+port+'"></OBJECT>');
	document.write(strControl);
	CreateObjectControlUnload(objID);
	if (ControlTYPE == "webcell")
	{
		eval(objID + ".Login('望海康信' , '', '13100104488', '3140-1444-7731-6004');");
		eval(objID + ".LocalizeControl(0x804);");
	}
	if(editDiv==null)
		return ;
	adjPositionObjectControl(objID,editDiv);
	
	window.attachEvent("onresize", function(){adjPositionObjectControl(objID,editDiv)});
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},50);
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},100);
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},200);
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},400);
}

function CreateTableControl(objID, ControlTYPE, ControlWIDTH, ControlHEIGHT, editDiv)
{
	var sClsID = "";
	var sCodeBase = "";
	var sURL = window.prefix;

	if (ControlTYPE == "VHTable")
	{
		sClsID = "CLSID:62C27227-C64C-44ED-B184-E9B54D582A59";
		sCodeBase = "";// sURL +
						// "base/activex/vhControlTableB.CAB#version=3,0,0,117";
	}

	if(editDiv!=null)
	{
		ControlWIDTH = 1;
		ControlHEIGHT = 1;
	}

	var strControl = "";
	strControl += '<OBJECT id="' + objID + '" ';
	strControl += 'CLASSID="' + sClsID + '" ';
	strControl += 'CODEBASE="' + sCodeBase + '" ';
	strControl += 'WIDTH="' + ControlWIDTH + '" ';
	strControl += 'HEIGHT="' + ControlHEIGHT + '">';
	strControl += '<param name="WebURL" value="' + sURL + '">';
	strControl += '<param name="TableID" value="' + objID + '">';
	strControl += '<param name="TableShow" value=false>';
	strControl += '</OBJECT>';
	document.write(strControl);
	CreateObjectControlUnload(objID);
	if(editDiv==null)
		return;
	adjPositionObjectControl(objID,editDiv);
	window.attachEvent("onresize", function(){adjPositionObjectControl(objID,editDiv)});
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},50);
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},100);
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},200);
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},400);
}

function adjPositionObjectControl(axObjId,divObj){
	if (divObj == null || divObj == "") return;
	var axObj=document.getElementById(axObjId);
	if(axObj==null)
		return;
	var rightFix=divObj.getAttribute("rightFix");
	if(rightFix==null)
		rightFix="0";
	var bottomFix=divObj.getAttribute("bottomFix");
	if(bottomFix==null)
		bottomFix="40"; //标注窗口的两个按钮压着窗口边缘线了
	parentObj =divObj;
    baseDivTop = baseDivLeft = 0
    try{
	    while(parentObj.tagName != "BODY") {
		    baseDivTop += parentObj.offsetTop;
		    baseDivLeft += parentObj.offsetLeft;
		    parentObj = parentObj.offsetParent;
	    }
    }catch(E){return;}
    
    if(rightFix.substr(rightFix.length-1)=="%"){
   		divObj.style.pixelWidth = parentObj.clientWidth -  2 - parentObj.clientWidth*parseFloat(rightFix.replace(/%/g,""))/100;
    }else
    	{
    	divObj.style.pixelWidth = parentObj.clientWidth -  2 - parseFloat(rightFix);
    }
    
    if(bottomFix.substr(bottomFix.length-1)=="%")
    	divObj.style.pixelHeight = parentObj.clientHeight - baseDivTop - 4 - parentObj.clientHeight*parseFloat(bottomFix.replace(/%/g,""))/100
   else
       	divObj.style.pixelHeight = parentObj.clientHeight - baseDivTop - 4 - parseFloat(bottomFix)

    if(typeof(axObj)!="undefined"&&divObj.style.pixelWidth>10&&divObj.style.pixelHeight>0){
    	//alert(divObj.innerHTML)
   		axObj.width=divObj.style.pixelWidth;
   		axObj.height=divObj.style.pixelHeight;
    }else
    	window.setTimeout(function(){adjPositionObjectControl(axObjId,divObj)},50);
}
function CreateObjectControlUnload(objId){
	window.attachEvent("onbeforeunload", function(){
		//卡片维护画面，点击VHtable控件中的图片后，会出现脚本错误。
		//解决办法，打开超链接时不执行清除控件对象的操作。
	  //start
		var targHref = window.document.activeElement.href; 
    if(targHref && targHref.toLowerCase().indexOf("javascript:")==0) {
        return false;
    }
    //end
		var obj=document.getElementById(objId);
		if(obj!=null)
			obj.parentNode.removeChild(obj);
	});	
}
	
var i9=0;

//FUSIONCHARTS图形展示---------lishix start
		function fusionchartsShow(gtype,gTitle,xmlData,xAxes,yAxes,isTipShow){
			var FusionChartsXML="";
			xAxes==null?xAxes="":xAxes;
			yAxes==null?yAxes="元":yAxes;
			var xmlDoc; 
			if(window.ActiveXObject){ 
				xmlDoc=new ActiveXObject("Microsoft.XMLDOM"); 
			}else if(document.implementation && document.implementation.createDocument){ 
				xmlDoc=document.implementation.createDocument("", "root", null); 
			} 
			
			xmlDoc.async="false";
			xmlDoc.loadXML(xmlData); 

			var chart;
			if(gtype==1){
				FusionChartsXML= encodeURI(Line(gTitle,xmlDoc,xAxes,yAxes,isTipShow));
			}else if(gtype==2){
				FusionChartsXML= encodeURI(Column(gTitle,xmlDoc,xAxes,yAxes,isTipShow));
			}else if(gtype==3){
				FusionChartsXML= encodeURI(Pie(gTitle,xmlDoc,isTipShow));
			}		
			window.FusionChartsXML=FusionChartsXML;
			openEg('/base/scripts/FusionChartsShow.jsp?'+encodeURI('type='+gtype),800,423,false,false);//在屏幕中心弹出窗口
		}
		
		function Line(gTitle,xmlDoc,xAxes,yAxes,isTipShow){
			var xmlFusion="<chart imageSave='1' imageSaveURL='/base/scripts/FusionChartsSave.jsp' thousandSeparator=',' formatNumberScale='0' rotateYAxisName='0' xAxisName='"+xAxes+"' yAxisName='"+yAxes+"' caption='"+gTitle+"' palette='1' showValues='0'";
			if(yAxes=="元"){
				xmlFusion+=" numberPrefix='￥'>";
			}else{
				xmlFusion+=" >";
			}
			var items=xmlDoc.documentElement.childNodes;
			xmlFusion+="<categories>";
			for(i=0;i<items.item(0).childNodes.length;i++){ 
				var category =items.item(0).childNodes.item(i).getAttribute("name") ; 
				xmlFusion+="<category label='"+category+"'/>";
			}
			xmlFusion+="</categories>";
			for(i=0;i<items.length;i++){ 
				var item=items.item(i);
				if(item.hasChildNodes()){
					xmlFusion+="<dataset seriesName='"+item.getAttribute("name")+"'>";
					for(j=0;j<item.childNodes.length;j++){
						var node=item.childNodes.item(j);
						xmlFusion+="<set value='"+node.getAttribute("value").replace(/,/g,'')+"'/>";
					}
					xmlFusion+="</dataset>";
				}
			}
			xmlFusion+="</chart>";
			return xmlFusion;
		}

		function Pie(gTitle,xmlDoc,isTipShow){
			var xmlFusion="<chart imageSave='1' imageSaveURL='/base/scripts/FusionChartsSave.jsp' caption='"+gTitle+"' showPercentValues='1' showValues='1'>";
			var items=xmlDoc.documentElement.childNodes;
			for(i=0;i<items.length;i++){ 
				var item=items.item(i);
				for(j=0;j<item.childNodes.length;j++){
					var node=item.childNodes.item(j);
					if(node.getAttribute("name")==null){
						return Pie3D(gTitle,xmlDoc,isTipShow);
					}
					//xmlFusion+="<set label='"+node.getAttribute("name")+"' value='"+node.getAttribute("value")+"' />";
					//xmlFusion+="<set label='"+node.getAttribute("name")+"' value='"+node.getAttribute("value").replace(/,/g,'')+"' toolText='"+node.getAttribute("name")+","+node.getAttribute("value")+"'/>";
					xmlFusion+="<set label='"+node.getAttribute("name")+"' value='"+node.getAttribute("value").replace(/,/g,'')+"' />";
				}
			}
			xmlFusion+="</chart>";
			return xmlFusion;
		}

		function Pie3D(gTitle,xmlDoc,isTipShow){
			var xmlFusion="<chart imageSave='1' imageSaveURL='/base/scripts/FusionChartsSave.jsp' caption='"+gTitle+"' showPercentValues='1' showValues='1'>";
			var items=xmlDoc.documentElement.childNodes;

			for(i=0;i<items.length;i++){ 
				var item=items.item(i);
				xmlFusion+="<set label='"+item.getElementsByTagName("name")[0].text+"' value='"+item.getElementsByTagName("value")[0].text+"' toolText='"+item.getElementsByTagName("name")[0].text+","+item.getElementsByTagName("value")[0].text+"'/>";
			}
			xmlFusion+="</chart>";
			//alert(xmlFusion)
			return xmlFusion;
		}
		
		function Column(gTitle,xmlDoc,xAxes,yAxes,isTipShow){
			return Line(gTitle,xmlDoc,xAxes,yAxes,isTipShow);
		}
		
		function openEg(url,width,height,left,top){
			 var temp = "channelmode=no;location=no;menubar=no;toolbar=no;directories=no;scrollbars=no;resizable=yes";
			 if (width) {
			  temp += ';dialogWidth=' + width+'px';
			 } else {
			  width = 0;
			 }
			 if (height) {
			  temp += ';dialogHeight=' + height+'px';
			 } else {
			  height = 0;
			 }
			 if (left) {
			  temp += ';left=' + left;
			 } else {
			  temp += ';left='
			    + Math.round((window.screen.width - parseInt(width)) / 2);
			 }
			 if (top) {
			  temp += ';top=' + top;
			 } else {
			  temp += ';top='
			    + Math.round((window.screen.height - parseInt(height)) / 2);
			 }
		   //window.openIFDialog(window,url, temp,null);//这种方式打开的界面不是在最上层，屏蔽掉20170705
		   window.openDialog( window.prefix.replace('/vh/','/')+url, temp,null);
		}
		
//FUSIONCHARTS图形展示---------lishix end

function translateXmlToDoc(sourceXML){
			var xmlDoc; 
			if(window.ActiveXObject){ 
				xmlDoc=new ActiveXObject("Microsoft.XMLDOM"); 
			}else if(document.implementation && document.implementation.createDocument){ 
				xmlDoc=document.implementation.createDocument("", "root", null); 
			} 
			xmlDoc.async="false";
			xmlDoc.loadXML(sourceXML); 
			return xmlDoc
}
