function jhtc_lineCtn(win,jhtc_obj){
	return jhtc_lineCtn_c(__jhtcElementFun(jhtc_obj));
}	
function jhtc_lineCtn_c(_e){
	var initV = new Array();
	function init(nPara) {
		var element=_e();
		if (window.trim(element.load) != "") {
			var para = getPara();
			if(typeof(nPara)!="undefined")
				para=nPara;
			if(element.__inInit==true){
				
			}else{
				element.__inInit=false;
				window.xmlhttp.post(element.load, para, '?subFunc=load')
				showValues(xmlhttp._object.responseXML);
			}
		}else{
			element.__inInit=false;
		}
	}
	
	function getPara(){
		var element=_e();
		if(element.para!="")
			return element.para;
		return _clearIFdlgTag(window.location.href).replace(/(^[^=\n]*=)|(\?[^\?\n]*$)/g, "");
	}
	
	function showValues(srcTree){
			var element=_e();
			var list1 = srcTree.getElementsByTagName("tbody");
			for (var i=0; i<list1.length; i++) {
				var list2 = list1.item(i).getElementsByTagName("td");
				initV.length = list2.length
				for (var j=0; j<list2.length; j++) {
					if (list2.item(j).firstChild != null) {
						initV[j] = list2.item(j).firstChild.nodeValue
					} else
						initV[j] = "";
				}
			}
			if(initV.length==0)
				return ;
			
			var inputs = element.getElementsByTagName("input")
			for (var i=0, j=0; i<inputs.length; i++) {
				if (window.trim(inputs[i].name) != "") {
					//alert(initV[j])
					if (inputs[i].className != null && (inputs[i].className.search(/Select/i) != -1 || inputs[i].className == "inputRadio" || inputs[i].className == "inputCheckBox" || inputs[i].className == "inputTextarea")) {
						inputs[i].setValue(initV[j]);
						j++;
					} else if (inputs[i].className != null && inputs[i].className.search(/input/i) != -1) {
						inputs[i].value= initV[j];
						j++;
					}
				}
			}	
	}
	
	function showValues2(srcTree){
			var element=_e();
			var list1 = srcTree.getElementsByTagName("td");
			
			if(list1.length==0)
				return ;
				
			if (list1.item(0).firstChild != null) {
				var td = list1.item(0).firstChild.nodeValue
			} else
				return;
				
			var atx=new ActiveXObject("Microsoft.XMLDOM");
			atx.loadXML("<root>"+td+"</root>");
			
			var item =atx.documentElement.firstChild;

			var ij=0;
			while(item!=null){
				if(item.nodeType==1){
					ij++;
				};	
				item=item.nextSibling;
			};
			initV.length = ij;
			var jj=0;
			item =atx.documentElement.firstChild;
			
			while(item!=null){
				if(item.nodeType==1){
					if(item.firstChild !=null)
						initV[jj]=item.firstChild.nodeValue;
					else
						initV[jj]="";
					
					jj++;
				};
				item=item.nextSibling;
			};
			
			if(initV.length==0)
				return ;
			
			var inputs = element.getElementsByTagName("input")
			if(initV.length !=inputs.length)
				return;
			
			for (var i=0, j=0; i<inputs.length; i++) {
				if (window.trim(inputs[i].name) != "") {
					//alert(initV[j])
					if (inputs[i].className != null && (inputs[i].className.search(/Select/i) != -1 || inputs[i].className == "inputRadio" || inputs[i].className == "inputCheckBox" || inputs[i].className == "inputTextarea")) {
						inputs[i].setValue(initV[j]);
						j++;
					}else if (inputs[i].className != null && inputs[i].className.search(/input/i) != -1) {
						inputs[i].value= initV[j];
						j++;
					}
				}
			}	
	}
	
	function getResult() {
		return initV;
	}
	
	function check() {
		var element=_e();
		var inputs= element.getElementsByTagName("input")
		for (var i=0; i<inputs.length; i++) {
			if (window.trim(inputs[i].name) == ""||inputs[i].type=='checkbox'||inputs[i].type=='radio') continue;
			if (inputs[i].parentNode.style.display=="none") continue;
			if(inputs[i].className == "inputMultiSelect" || inputs[i].className == "inputRadio" || inputs[i].className == "inputCheckBox"){//单独处理多选、radio和checkbox
			    continue;
			} else {
			if (inputs[i].check())
				continue;
			else
				return false;
			}
		}
		return true
	}
	function submitForXML(btn,subFunc){
		if (window.trim(btn.name)=="") {
			alert('请设置name')
			return false;
		}
		if (subFunc == null) {
			subFunc = '';
		}
		if (!check()) return false
		window.xmlhttp.post(btn.name,assemble(),subFunc)
		
		var str = window.xmlhttp._object.responseText
		return str;
	}
	function queryForPrint(callBackOnFinished,btn,subFunc,hideMsg,showMsg){
		if (window.trim(btn.name)=="") {
			alert('请设置name')
			return false;
		}
		if (subFunc == null) {
			subFunc = '';
		}
		// 检测
		if (!check()) return false;
		window.xmlhttp.postWithProgress(btn.name,assemble(),subFunc
		,function(readyState,statusOrInfo,rText,rXml){
			if(readyState==1){
				jhtcBeginQuery();
			}else if(readyState==2){
			}else if(readyState==3){
			}else if(readyState==4){
				if(statusOrInfo==200){
					if(window.doMsg(rText,hideMsg, showMsg))
						callBackOnFinished(rText);
						jhtcEndQuery();
				}
			}
		});
	}
	function submit(btn, table, isTurn, subFunc, hideMsg, showMsg) {
		if (window.trim(btn.name)=="") {
			alert('请设置name')
			return false;
		}
		if (subFunc == null) {
			subFunc = '';
		}
		// 检测

		if (!check()) return false;

		var ttype=getDefaultPageSizeByMod(),tsize=0;
		if(ttype=="0"){
		
		}else if(ttype=="1"){
			isTurn=null;
		}else{
			tsize=ttype;
		}
		//yaoqingsong 修改 HERP330 问题HERP-139 2018-05-02
		//tsize=100; 
		if(isNaN(tsize))
			tsize=100;
		// -- TBD 关于错误处理, 如果无记录，如何处理，有记录如何处理
		if (table != null && table != '') {
		//window.prompt("装配好的xml请求:","<root>" + assemble() + "</root>");
		if(typeof(table.refresh)!="undefined"){
			
			save_page_condition(get_url());
			
			if(tsize!=0){
				if(table["_set_count_page"]){}else{
					table.count_page=tsize;
					table["_set_count_page"]=true;
				}
			}
			if (table.refresh(btn.name, assemble(), isTurn, subFunc, hideMsg)) {
				return true;
			} else 
				return false;
			}else{
				if(ttype=="1"){
					table.PageSize=0;
					isTurn=false;
				}
				if(tsize!=0){
					table.PageSize=tsize;
				}
				var res=table.TableRefresh(btn.name, assemble(), isTurn, subFunc, hideMsg);
				if (res) {
					save_page_condition(get_url());
					return true;
				} else 
					return false;
			}
			
		} else {
			window.xmlhttp.post(btn.name,assemble(),subFunc)
			
			var str = window.xmlhttp._object.responseText
			
			if (window.dialogArguments!=null) {
				try {
					window.dialogArguments._obj1.refresh()
				} catch (exception){
				}
			}
			return window.doMsg(str,hideMsg, showMsg);
		}
	}
	
	/*
	* 获得插入数据库中的id
	*/
	function get_url(){
		var element=_e();
		var urlStr=document.URL;
		var urlStr2=urlStr.substr(urlStr.lastIndexOf(":"));
		var urlStr3=urlStr2.substr(urlStr2.indexOf("/")+1);
		
		if(urlStr3.indexOf("?") !=-1){
			urlStr=urlStr3.substr(0,urlStr3.indexOf("?"));
		}else{
			urlStr=urlStr3;
		}
		if(typeof(jQuery(element).attr("id"))!="undefined"){
			urlStr+="#"+jQuery(element).attr("id");
		}
		return urlStr;
	}
	
	
	/*
	* 把页面的查询条件插入到数据库中
	*/
	function save_page_condition(durl){
		var element=_e();
		if(element.isSavecond=="false")return;
		window.xmlhttp.post("page_condition_insert","<aa_bb_id>"+durl+"</aa_bb_id><aa_bb_content>"+encodeXmlChar(assemble2())+"</aa_bb_content><aa_bb_oper>"+getEmpCode()+"</aa_bb_oper>","?isCheck=false")
	}
	
	/*
	* 简单表格的XML组装
	*/
	function assemble() {
		var element=_e();
		var result = "";
		var inputs= element.getElementsByTagName("input")
		for (var i=0; i<inputs.length; i++) {
			if (window.trim(inputs[i].name) == "") continue;
			if (window.trim(inputs[i].isAssemble) == "false") continue;
				// -- TBD 对于变量需要转换一些特殊符号，如/、<、>等
			if(inputs[i].type=='checkbox' || inputs[i].type=='radio'){
				//result+="<" + inputs[i].name + ">" + inputs[i].checked+"</" + inputs[i].name +">";
			}else if(inputs[i].className=="inputFDecimal"){
				result = result + "<" + inputs[i].name + ">" + encodeInputValue(inputs[i].getValue())+"</" + inputs[i].name +">";
			}else{
				result = result + "<" + inputs[i].name + ">" + encodeInputValue(inputs[i].value)+"</" + inputs[i].name +">";
			}
		}
		element.submitXml = result;
		return result;
	}
	
	function assemble2() {
		var element=_e();
		var result = "";
		var inputs= element.getElementsByTagName("input")
		for (var i=0; i<inputs.length; i++) {
			
			if (window.trim(inputs[i].name) == "") continue;
			if (window.trim(inputs[i].isAssemble) == "false") continue;
				// -- TBD 对于变量需要转换一些特殊符号，如/、<、>等
			
			var c=" c='"+inputs[i].className+"'";
			
			if(inputs[i].type=='checkbox' || inputs[i].type=='radio'){
				//result+="<" + inputs[i].name + ">" + inputs[i].checked+"</" + inputs[i].name +">";
			}else if(inputs[i].className=="inputFDecimal"){
				result = result + "<" + inputs[i].name + c+">" + encodeInputValue(inputs[i].getValue())+"</" + inputs[i].name +">";
			}else if(inputs[i].className=="inputSelectS"){
				if(inputs[i].code=="true")
					result = result + "<" + inputs[i].name + c+">" + encodeInputValue(inputs[i].value)+"|||"+encodeInputValue(inputs[i].text.substr(inputs[i].text.indexOf(" ")+2))+"</" + inputs[i].name +">";
				else
					result = result + "<" + inputs[i].name + c+">" + encodeInputValue(inputs[i].value)+"|||"+encodeInputValue(inputs[i].text)+"</" + inputs[i].name +">";
			}else if(inputs[i].className=="inputSelects"){
				if(inputs[i].code=="true")
					result = result + "<" + inputs[i].name + c+">" + encodeInputValue(inputs[i].value)+"|||"+encodeInputValue(inputs[i].text.substr(inputs[i].text.indexOf(" ")+2))+"</" + inputs[i].name +">";
				else
					result = result + "<" + inputs[i].name + c+">" + encodeInputValue(inputs[i].value)+"|||"+encodeInputValue(inputs[i].text)+"</" + inputs[i].name +">";
			}else{
				result = result + "<" + inputs[i].name + c+">" + encodeInputValue(inputs[i].value)+"</" + inputs[i].name +">";
			}
		}
		return result;
	}
	
	function encodeInputValue(v){
		var element=_e();
		if(element.encodeValue=="true")
			return encodeXmlChar(v);
		else
			return v;
	}
	function copyContents(){
		var element=_e();
		var inputs= element.getElementsByTagName("input")
		var result="";
		for (var i=0; i<inputs.length; i++) {
			if (window.trim(inputs[i].name) == "") continue;
			if(inputs[i].type=='checkbox' || inputs[i].type=='radio'){
				//result+="," + inputs[i].checked;
			}else{
				if(inputs[i].className.toLowerCase()=='inputselects'){
					if((inputs[i].code=="true")&&(inputs[i].value!="")){
						mt=inputs[i].text.split('  ');
						result+="," + inputs[i].value+'|||'+mt[1];
					}else 
						result+="," + inputs[i].value+'|||'+inputs[i].text;
				}else 
					result+="," + inputs[i].value;
			}
		}
		clipboardData.setData('TEXT',result.substring(1))
		//alert(clipboardData.getData('TEXT'));
		//window.top.vhclipBoard=result.substring(1);
	}
	function pasteContents(){
		var element=_e();
		var clipBoard=clipboardData.getData('TEXT');
		var clipBoard_a=clipBoard.split(",");
		var inputs= element.getElementsByTagName("input")
		var j=0;
		for (var i=0; i<inputs.length; i++) {
			if (window.trim(inputs[i].name) == "") continue;
			
			if (inputs[i].className != null && (inputs[i].className.search(/Select/i) != -1 || inputs[i].className == "inputRadio" || inputs[i].className == "inputCheckBox" || inputs[i].className == "inputTextarea")) {
				if(clipBoard_a.length<j+1) return
				if((inputs[i].parentNode.style.display!="none")&&(inputs[i].readOnly!=true)&&(inputs[i].disabled!=true)){
					inputs[i].setValue(clipBoard_a[j]);
				}
				j++;
			} else if (inputs[i].className != null && inputs[i].className.search(/input/i) != -1) {
				if(clipBoard_a.length<j+1) return
				if((inputs[i].parentNode.style.display!="none")&&(inputs[i].readOnly!=true)&&(inputs[i].disabled!=true))
					inputs[i].value= clipBoard_a[j];
					j++;
			}
		}
	
	}
	/**
	* 重置
	*/
	function reset(nPara) {
		var element=_e();
		if (trim(element.load) != "") {
			init(nPara)
			return
		}
		// -- TBD select的处理
		var inputs= element.getElementsByTagName("input")
		for (var i=0; i<inputs.length; i++) {
			inputs[i].value="";
		}
		if (inputs.length>0)
			inputs[0].focus()
	}
	/**清空
	*/
	function clear(){
		var element=_e();
		var inputs= element.getElementsByTagName("input")
			for (var i=0; i<inputs.length; i++) {
		  		inputs[i].value="";
			}
	}
	function trim(str) {
		if (str == null) return "";
		return str.replace(/(^\s+)|\s+$/g, "");
	}
	
	function _initAttr(){
		var element=_e();
		var cururl = window.location.href;	
		if(cururl.indexOf("/cbcs")>0){
			element.__inInit=false;
		}else{
			element.__inInit=true;
		}
		jhtc_attr_init(element,"load",null);
		jhtc_attr_init(element,"para","");
		jhtc_attr_init(element,"submitXml","");
		jhtc_attr_init(element,"encodeValue","true");
		jhtc_attr_init(element,"isAssemble","true");
		jhtc_attr_init(element,"isSavecond","true");
		
		element.check=check;
		element.submit=submit;
		element.reset=reset;
		element.clear=clear;
		element.getResult=getResult;
		element.assemble=assemble;
		element.copyContents=copyContents;
		element.pasteContents=pasteContents;
		element.submitForXML=submitForXML;
		element.showValues=showValues;
		element.showValues2=showValues2;
		element.init=init;
		element.jhtcInit=init;
		element.queryForPrint=queryForPrint;
		element.exportExcel = exportExcel;
		element.exportTxt = exportTxt;
	}
	_initAttr();
	if(_e().load!=null){
		return {
			type:"sql",
			name:_e().load,
			data:getPara(),
			parameter:'?subFunc=load',
			onData:function(data){_e()._values=data;},
			onAll:function(){
					var srcTree = new ActiveXObject("Microsoft.XMLDOM");
					srcTree.async=false;
					srcTree.loadXML(_e()._values);
					_e().showValues(srcTree);
				}
			};
	}else{
		return {
			type:"sql",
			name:"page_condition_select",
			data:"<a>"+get_url()+"</a><b>"+getEmpCode()+"</b>",
			parameter:'',
			onData:function(data){_e()._values=data;},
			onAll:function(){
					var srcTree = new ActiveXObject("Microsoft.XMLDOM");
					srcTree.async=false;
					srcTree.loadXML(_e()._values);
					_e().showValues2(srcTree);
				}
			};
	};
};
jhtc_class_map["lineCtn"]=jhtc_lineCtn;
jhtc_class_map["formCtn"]=jhtc_lineCtn;


	function exportExcel(btn,head,cols){
		if (window.trim(btn.name)=="") {
			alert('请设置name')
			return false;
		}
		if(typeof(head)=='undefined')head="";
		if(typeof(cols)=='undefined')cols="";
				
		if (!check()) return false;
		
		dataExportToExcel(btn.name,head,cols, assemble());
		
	}

function exportTxt(btn,head,cols){
    if (window.trim(btn.name)=="") {
        alert('请设置name')
        return false;
    }
    if(typeof(head)=='undefined')head="";
    if(typeof(cols)=='undefined')cols="";

    if (!check()) return false;

    dataExportToTxt(btn.name,head,cols, assemble());

}