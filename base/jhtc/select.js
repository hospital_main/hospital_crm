function jhtc_select(win,jhtc_obj){
	return jhtc_select_c(__jhtcElementFun(jhtc_obj));
}	
function jhtc_select_c(_e){
	var objListId=jhtcGetNextId();
	var objInputId=jhtcGetNextId();
	var iframeId=jhtcGetNextId();
	var _objListE=__jhtcElementFunById(objListId);
	var _objInputE=__jhtcElementFunById(objInputId);
	var _iframeE=__jhtcElementFunById(iframeId);
	var srcTree = new ActiveXObject("Microsoft.XMLDOM");
	var xsltTree= new ActiveXObject("Microsoft.XMLDOM");
	srcTree.async=false;
	xsltTree.async=false;
	var hasList = "0";
	var vListMaxHigh = 81;
	var objXML;
	var oldSetValue;
	var paraItemCount=0;
	var paraItemsXml=null;
	var is_onload;
	var overImgPath='/base/themes/blue/images/select/over.png';
	var outImgPath='/base/themes/blue/images/select/normal.png';
	var downImgPath='/base/themes/blue/images/select/normal.png';
	var disabledImgPath='/base/themes/blue/images/select/disabled.png';
	var blandImgPath='/base/themes/blue/images/select/blank.gif';
	var is_loaded=false;
	var is_load=true;
	var is_enabled=true;
	var proxyLabel=null;
	var resultMxLength=0;
	
	function reloadPic(){
		var objInput=_objInputE();
		var element=_e();
		element.style.width = element.extent;
		element.style.color = "#FFFFFF";
		
		element.style.backgroundImage='url("'+outImgPath+'")';
		if ("true"== trim(element.required)) {
			element.style.backgroundColor=JHTC_COLOR.REQUIRED;
		}
		with (objInput) {
			accessKey = element.accessKey;
			readOnly = element.readOnly;
			className = element.className+"_text";
			style.position = "absolute";
			
			var obj=element.parentNode.parentNode.parentNode.parentNode;
			if((obj.className=='lineCtn')||(obj.className=='mainCtn')){
				style.marginTop="5px";
				style.marginLeft="1px";
			}else{
				style.marginTop="2px";
				style.marginLeft="1px";
			}
			
			
			try{
				style.height = element.offsetHeight - 2;
				style.width = element.extent-22;
			}catch(e){}
			maxLength = element.maxInput;
			if ("true"== trim(element.required)) {
				style.backgroundColor=JHTC_COLOR.REQUIRED;
			}
			
			onmouseover = overBtn;
			onmouseout = outBtn;
			onkeydown = navigateKeys;
			onblur = clickDocument;
		}
		objInput.onkeyup = objInput.ondragend = changeInput;
		element.style.cursor="hand";
		with(element){
			onmouseover=overBtn;
			onmouseout=outBtn;
			onclick=clickBtn;
		}
		element.accessKey="";
		element.tabIndex=-1;
		element.insertAdjacentElement("beforeBegin", objInput);
		is_onload=element.initLoad;
		if(element._dfv==null){
			if(element.required=="true")
				is_load=true;
			else
				is_load=false;    
		}else if(element._dfv=="false"){
			is_load=false;
		}else{
			is_load=true;
		}
		
	}
	function setEnabled(e){
		var objInput=_objInputE();
		var element=_e();
		if(e==true){
			element.style.backgroundImage='url("'+outImgPath+'")';
			element.disabled=false;
			objInput.disabled=false;
			is_enabled=true;
		}else{
			element.style.backgroundImage='url("'+disabledImgPath+'")';
			is_enabled=false;
			objInput.disabled=true;
			element.disabled=true;
		}
	}
	function hasResult(){
		if (hasList == "0")
			return false;
		else 
			return true;
	}
	
	function setText(){
		var objInput=_objInputE();
		var element=_e();
		return element.text = objInput.value;
	}
	
	function initBegin() {
		var element=_e();
		if(element.label=="preTd"){
			var preTd=element.parentNode;
			while(preTd!=null){
				if(preTd.tagName.toLowerCase()=="td"){
					if(preTd.previousSibling!=null){
						proxyLabel=preTd.previousSibling.innerText;
						if(proxyLabel.indexOf(":")>0)
							proxyLabel=proxyLabel.split(":")[0];
						if(proxyLabel.indexOf("：")>0)
							proxyLabel=proxyLabel.split("：")[0];
					}
					break;
				}
				preTd=preTd.parentNode;
			}
			element.label=null;
		}else if(element.label!=null)
			proxyLabel=element.label;
		element.htcLabel=proxyLabel==null?"":proxyLabel;
		if (trim(element.load)=="") {
			alert("此select组件，没有指定load属性!");
			return;
		}
		if (element.label != null) {
			if(element.labelFix=="true"){
				// label 的基本长度
				var lableLength = 120;
				//var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
				//if (spanWidth < lableLength) spanWidth = lableLength;
				var spanWidth = lableLength;
				// 快捷键
				var aKey = "";
				if (trim(element.accessKey)!="") {
					//aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
				}
				element.insertAdjacentHTML("beforeBegin", "<span title='" + element.label + "' id='labelCtn' nowrap style='text-align:right;width:"+spanWidth+"px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";font-weight:"+element.currentStyle.fontWeight+";white-space: nowrap;text-overflow:ellipsis; overflow:hidden;'>"+element.label+aKey+(element.label==''?"":"：")+"</span>");
			}else{
				// label 的基本长度
				var lableLength = 120;
				var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
				if (spanWidth < lableLength) spanWidth = lableLength;

				// 快捷键
				var aKey = "";
				if (trim(element.accessKey)!="") {
					aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
				}
				element.insertAdjacentHTML("beforeBegin", "<span title='" + element.label + "' id='labelCtn' nowrap style='text-align:right;width:"+spanWidth+"px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";font-weight:"+element.currentStyle.fontWeight+";'>"+element.label+aKey+(element.label==''?"":"：")+"</span>");
			}
		} else {
			var aKey = "";
			if (trim(element.accessKey)!="") {
				aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
			}
			element.insertAdjacentHTML("beforeBegin", "<span id='labelCtn' nowrap style='text-align:right;width:0px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";font-weight:"+element.currentStyle.fontWeight+";'></span>");
		}
		element.parentNode.noWrap = true;
		
		// 设置输入框的样式
		var objInput = element.document.createElement("<input id='"+objInputId+"'/>");
		__jhtcBindPropertyChange(objInput,"value",setText);
		element.style.width = element.extent;
		element.style.color = "#FFFFFF";
		element.style.backgroundImage='url("'+outImgPath+'")';
		if ("true"== trim(element.required)) {
			element.style.backgroundColor=JHTC_COLOR.REQUIRED;
		}
		with (objInput) {
			accessKey = element.accessKey;
			// readOnly = element.readOnly;
            readOnly = true;
			element.readOnly = true;
			className = element.className+"_text";
			style.position = "absolute";
			
			var obj=element.parentNode.parentNode.parentNode.parentNode;
			if((obj.className=='lineCtn')||(obj.className=='mainCtn')){
				style.marginTop="6px";
				style.marginLeft="1px";
			}else{
				style.marginTop="2px";
				style.marginLeft="1px";
			
			}
			
			
			try{
				style.height = element.offsetHeight - 3;
				style.width = element.extent-22;
			}catch(e){}
			maxLength = element.maxInput;
			if ("true"== trim(element.required)) {
				style.backgroundColor=JHTC_COLOR.REQUIRED;
			}
			
			
			onmouseover = overBtn;
			onmouseout = outBtn;
			onkeydown = navigateKeys;
			onblur = clickDocument;
		}
		objInput.onkeyup = objInput.ondragend = changeInput;
		element.style.cursor="hand";
		with(element){
			onmouseover=overBtn;
			onmouseout=outBtn;
			onclick=clickBtn;
		}
		element.accessKey="";
		element.tabIndex=-1;
		element.insertAdjacentElement("beforeBegin", objInput);
		is_onload=element.initLoad;
		if(element._dfv==null){
			if(element.required=="true")
				is_load=true;
			else
				is_load=false;    
		}else if(element._dfv=="false"){
			is_load=false;
		}else{
			is_load=true;
		}
		initList(doOnDataLoaded);
	}
	
	function changeInput() {
		var objInput=_objInputE();
		var element=_e();
		var objList=_objListE();
		if ((event.keyCode>=33 && event.keyCode<=40) || event.keyCode==13 || event.keyCode==16 || event.keyCode==18 || event.keyCode==9
		|| event.keyCode==27 || event.keyCode==0 || event.keyCode>250)
			return;
		if(objInput.value == "") {
			element.value=("");
		}  
		if (trim(element.checkValue)!="true") {
			element.value=(objInput.value);
		}
		if (trim(this.value)=="" && objList.style.display=="none")
			return;
		showList();
	}
	
	function changeTitle() {
		var objInput=_objInputE();
		var element=_e();
		objInput.title = element.text;
		element.title = element.text;
	}
	function alterEnter(){
		if(event.keyCode==13){
			event.keyCode=9;
		}
	}
	function initEnd() {
		var objInput=_objInputE();
		var element=_e();
		if (objInput == null)
			return;
		// 初始化按钮
		
		var iframe=element.document.createElement("iframe");
		with(iframe){
			id=iframeId;
			style.position = "absolute";
			style.display = "none";
			style.textAlign = "center";
			style.backgroundColor = "#F6F6F6";
			className = "ds_font";
			style.zIndex =8;
			style.overflow = "visible";
			border="0";
		}
		iframe = element.parentNode.appendChild(iframe);
		//window.attachEvent("onresize", adjPosition);
		//adjPosition();
		//window.setTimeout(adjPosition,50);
		//window.setTimeout(adjPosition,200);
		//window.setTimeout(adjPosition,400);
		if (trim(jQuery(element).attr("initValue")) != "") {
			setValue(jQuery(element).attr("initValue"));
		}
		if(element.onAfterInit!=""){
			eval(element.onAfterInit);
		}
		if(element.disabled){
			setEnabled(false);
		}
	}
	
	
	// 调整各个元素的位置
	function adjPosition() { // 调整位置
		var objList=_objListE();
		var objInput=_objInputE();
		var iframe=_iframeE();
		var element=_e();
		// 取得element的绝对位置
		var form = element;
		var elementTop=0, elementLeft=0;
		var listHeight=element.listRows*16;
		
		while(form.tagName != "BODY"&&form.tagName != "DIV") {
			elementTop = elementTop + form.offsetTop + form.clientTop;
			elementLeft = elementLeft + form.offsetLeft+form.clientLeft;
			form = form.offsetParent;
		}
		
		with (objList.style) {
			zIndex =10;
			left = elementLeft-1;
			if(elementTop+element.offsetHeight-1+parseInt(objList.style.height.replace(/px/,'')) > window.document.body.offsetHeight)
				top = elementTop + element.offsetHeight-1-listHeight;
			else
				top = elementTop + element.offsetHeight-1;
			height=listHeight;
		}
		
		with (iframe.style) {
			left = elementLeft-1;
			top=objList.style.top;
		}	
		adjustWidth();
		if(objList.style.width<2)
			return ;
		iframe.style.width=parseInt(objList.style.width);
		iframe.style.height=listHeight;//objInput.clientHeight;
		
	}
	var _adjPositionadjPosTimer_;
	function overBtn(){
		var element=_e();
		element.style.backgroundImage='url("'+overImgPath+'")';  
	}
	
	function outBtn(){ //鼠标移出Btn 或者input时的样式
		var element=_e();
		var objList=_objListE();
		if(objList && objList.style.display != "none"){
			
		}else {
			element.style.backgroundImage='url("'+outImgPath+'")';
		}
	}
	
	function clickBtn() { // Btn 按下后的样式
		var objInput=_objInputE();
		var iframe=_iframeE();
		var element=_e();
		var objList=_objListE();
		if(!is_enabled)return;
		if (objList!=null && objList.style.display != "none"){
			objList.style.display = "none";
			if(iframe!=null)
				iframe.style.display=objList.style.display;
		}
		else{
			showList();
			//add by zrc
			//objList.focus();
			//end
		}
	}
	//wsj 从showList 方法中抽取出来，专门用于加载数据源，而不显示。
	function loadListdata(){
		var objList=_objListE();
		var objInput=_objInputE();
		var element=_e();
		var lists = window.document.getElementsByTagName("DIV")
		for (var i=0; i<lists.length; i++) {
			if (lists[i].className == "select_list")
				lists[i].style.display = "none";
		}
		
		if (!element.readOnly && false) {
			srcTree.loadXML(objXML.replace(/key='(.*)'/,"key='"+trim(objInput.value)+"'"))
			setObjListHtml(srcTree.transformNode(xsltTree));
			
			var trs = objList.getElementsByTagName("TR")
			for (var i=0; i<trs.length; i++) {
				trs[i].onmouseover = function() {
					if (this.parentNode.parentNode.choseIndex!=null) {
						with (this.parentNode.parentNode.rows[this.parentNode.parentNode.choseIndex].runtimeStyle) {
							backgroundColor="";
							color="";
						}
					}
					
					this.parentNode.parentNode.choseIndex = this.rowIndex;
					this.runtimeStyle.backgroundColor="darkblue";
					this.runtimeStyle.color="white";
				}
				trs[i].onmousedown = choose;
				if (i%2==0) {
					trs[i].style.backgroundColor="whitesmoke";
				}
			}
		}
		
	}
	//wsj
	// 显示列表
	function setObjListHtml(html){
		var objList=_objListE();
		objList.innerHTML=html;
	}
	function showList() {
		if(!is_enabled)
			return
		if(!is_loaded){
			is_load=true;
			initList(showListCallBack);
		}else
			showListCallBack();
	}
	function showListCallBack(){
		var objList=_objListE();
		var objInput=_objInputE();
		var iframe=_iframeE();
		loadListdata();	
		adjPosition();
		
		objList.style.display = "";
		iframe.style.display=objList.style.display 
		
		
		var trs = objList.getElementsByTagName("TR");
		if (trs.length==0)
			return false;
		var i=0;
		for (; i<trs.length; i++) {
			if (trs[i].innerText.indexOf(objInput.value)==0) {
				break;
			}
		}
		
		if (i==trs.length)
			i=0;
		
		if (trs[i].parentNode.parentNode.choseIndex!=null) {
			with (trs[i].parentNode.parentNode.rows[trs[i].parentNode.parentNode.choseIndex].runtimeStyle) {
				backgroundColor = "";
				color = "";
			}
		}
		trs[i].runtimeStyle.backgroundColor="darkblue";
		trs[i].runtimeStyle.color="white";
		trs[i].parentNode.parentNode.choseIndex = trs[i].rowIndex;
		// 移动div scroll 1.取trs[i]的绝对top
		var baseDivTop = 0;
		var parentObj = trs[i];
		while (parentObj.tagName != "DIV") {
			baseDivTop += parentObj.offsetTop;
			parentObj = parentObj.offsetParent;
		}
		parentObj.scrollTop = baseDivTop;
		
		window.document.attachEvent("onmousedown",clickDocument);
	}
	function setLoaded(in_is_load){
		is_loaded=in_is_load;
	}
	function initListByXML(str) {
		var element=_e();
		var objList=_objListE();
		var objInput=_objInputE();
		var iframe=_iframeE();
		if (trim(element.code)=="true") {
			objXML = str.replace("<root>","<root code='true' qtype='0' key=''>");
		} else
			objXML = str.replace("<root>","<root code='false' qtype='0' key=''>");
		srcTree.loadXML(objXML);
		xsltTree.load(window.prefix+"base/xsl/select1.xsl");
		setObjListHtml(srcTree.transformNode(xsltTree));
		with (objList) {
			style.display = "none";
			iframe.style.display=objList.style.display 
			style.top = objInput.offsetTop+objInput.offsetHeight;
			style.width = element.offsetWidth;
		}
		var trs = objList.getElementsByTagName("TR")
		var firstTr=null;
		for (var i=0; i<trs.length; i++) {
			if(i==0&&element._dfv=="true")
			firstTr=trs[i];
			trs[i].onmouseover = function() {
				if (this.parentNode.parentNode.choseIndex!=null) {
					with (this.parentNode.parentNode.rows[this.parentNode.parentNode.choseIndex].runtimeStyle) {
						backgroundColor=''
						color=''
					}
				}
				
				this.parentNode.parentNode.choseIndex = this.rowIndex
				this.runtimeStyle.backgroundColor='darkblue'
				this.runtimeStyle.color='white'
			}
			trs[i].onmousedown = choose
			if (i%2==0) {
				trs[i].style.backgroundColor='whitesmoke'
			}
		}
		if(firstTr!=null)
			firstTr.parentNode.removeChild(firstTr);
		if (trim(element.required)=="true" && objXML.indexOf("code=\"")!=-1) {
			var str = objXML.substring(objXML.indexOf("code=\"")+"code=\"".length)
			element.value=(str.substring(0, str.indexOf("\"")))
			str = str.substring(str.indexOf("value=\"")+"value=\"".length)
			objInput.value = str.substring(0, str.indexOf("\""))
			if (trim(element.code)=="true")
				objInput.value = element.value+"　"+objInput.value
		}
	}
	function selectItem(index){
		var objList=_objListE();
		if(index<0||index>(getItemCount()-1))
			return ;
		var p=objList.getElementsByTagName("TR")[index+1].cells[0].value;
		setValue(p);
	}
	function getItemCount(){
		return paraItemCount;
	}
	function getDict_html(in_load,in_para,in_code,fun){
		if(_e().__inInit==true){
			_e().__inInit=false;  //HJJ 修改 将初始属性赋成false
			_e().__inInitFun=fun;
			return;	
		}
		if(_e().async=="true"){
			if (in_para != null){
				xmlhttp.send("global_select_html", in_para, "?selectID="+in_load+","+in_code,function(text,xml){getDict_html2(text,xml,fun)});
			}
			else
				xmlhttp.send("global_select_html", '', "?selectID="+in_load+","+in_code,function(text,xml){getDict_html2(text,xml,fun)});
			return;	
		}else{
			if (in_para != null) { 
				xmlhttp.post("global_select_html", in_para, "?selectID="+in_load+","+in_code);
			}
			else
				xmlhttp.post("global_select_html", '', "?selectID="+in_load+","+in_code);
			getDict_html2(xmlhttp._object.responseText,xmlhttp._object.responseXML,fun);
		}
	}
	function getDict_html2(text,xml,fun){
		is_loaded=true;
		if (window.doMsg(text)) {
			fun(text);
		}
	}
	var empty_list_data='0*<TBODY><TABLE><TR id=top_line style="FONT-SIZE: 12px"><TD style="PADDING-LEFT: 5px; FONT-SIZE: 12px; PADDING-TOP: 1px; HEIGHT: 16px" noWrap value=""></TD></TR></TABLE></TBODY>';
	// 初始化显示列表
	function initList(listCallback) {
		var element=_e();
		//objInput.value = "";
		element.value=("");
		var objList = element.document.createElement("<div id='"+objListId+"' class='select_list' style='display:none'></div>");
		element.insertAdjacentElement("afterEnd", objList);
		//added by wsj		
		var loadXML = "";
		/*wsj1.1 输入法检索*/    
		if (trim(element.load)=="retrieve_method") {
			//   	para=para+"<text>"+element.text+"</text">;
			
			if( element.para.indexOf("<retrieve>")>0){
				element.para= element.para.substring(0,element.para.indexOf("<retrieve>"));//如果不是初始状态，则取得，在尾部已经给了输入法的数值，去掉将后面的输入法的数值
				
				element.para=element.para+'<retrieve>'+getRetrieve()+'</retrieve>' 
			} 
		
		}
		
		//added by twl
		var elem_code;
		
		if(trim(element.code)=="true"){
			elem_code="true";
		}else{
			elem_code="false"
		}
		if ((trim(element.para)=="")&& (is_load==true)) {
			getDict_html(element.load,'',elem_code,function(loadHTML){initListCallback(loadHTML);listCallback();});
		} else if((is_onload=="true") && (is_load==true)) {
			getDict_html(element.load,trim(_getParaString()),elem_code,function(loadHTML){initListCallback(loadHTML);listCallback();});
		}else{
			element.__inInit=false;
			loadHTML=empty_list_data;
			initListCallback(loadHTML);listCallback();
		}
	}
	function initListCallback(loadHTML){
		var objList=_objListE();
		var objInput=_objInputE();
		var element=_e();
		if(loadHTML==null)
			loadHTML=empty_list_data;
		var marr=loadHTML.split('*');
		resultMxLength = parseInt(marr[0]);
		if(marr[0]=="xml"){
			var allxml=loadHTML.substring(loadHTML.indexOf('*')+1);
			if (trim(element.code)=="true") {
				objXML = allxml.replace("<root>","<root code='true' qtype='0' key=''>");
			} else
				objXML = allxml.replace("<root>","<root code='false' qtype='0' key=''>");
			
			srcTree.loadXML(objXML);
			element.serverObj=srcTree;
			var objNodeList = srcTree.getElementsByTagName("para");
			for (var j=0; j<objNodeList.length; j++) {
				if (resultMxLength < objNodeList.item(j).attributes.item(1).nodeValue.length) {
					resultMxLength = objNodeList.item(j).attributes.item(1).nodeValue.length;
				}
			}
			xsltTree.load(window.prefix+"base/xsl/select1.xsl");
			setObjListHtml(srcTree.transformNode(xsltTree));
			

		}else{
			var allhtml=loadHTML.substring(loadHTML.indexOf('*')+1);
			setObjListHtml(allhtml);
		}
		insertEditorLink();
		
		
		
		with (objList) {
			style.display = "none";
			style.top = objInput.offsetTop+objInput.offsetHeight;
			style.height = vListMaxHigh;
		}
		paraItemCount=objList.getElementsByTagName("TR").length-1;
		adjustWidth();
		
		var trs = objList.getElementsByTagName("TR")
		
		//add by zrc 06-9-22
		objList.onkeydown = function(){	
		event.cancelBubble=true;
		//如果是回车，则选中
		if(event.keyCode==13){
			choose();
			
			event.keyCode=9;
			return;
		}
		
		var table = objList.getElementsByTagName("TABLE")[0] ;
		var oldchooseindex = table.choseIndex ;
		var newchooseindex = table.choseIndex ;
		
		//向下 s
		if(event.keyCode==40){
			
				//如果选项的是最后一项，则不操作，否则选择项索引加一
			if (table.choseIndex+1==table.rows.length)
				return;
			else{
				table.choseIndex = table.choseIndex+1;
				newchooseindex = table.choseIndex;
				objList.scrollTop = objList.scrollTop + table.rows[table.choseIndex].offsetHeight;
			}
		}
		
		//向上 w
		if(event.keyCode==38){

			//如果选项的是最后一项，则不操作，否则选择项索引加一
			if(table.choseIndex==0)
				return;
			else{
				table.choseIndex = table.choseIndex-1;
				newchooseindex = table.choseIndex;
				objList.scrollTop = objList.scrollTop - table.rows[table.choseIndex].offsetHeight;
			}
		}	
		
		with (table.rows[oldchooseindex].runtimeStyle) {
			backgroundColor="";
			color="";
		}
		
		with (table.rows[newchooseindex].runtimeStyle) {
			backgroundColor="darkblue";
			color="white";
		}
		
		}
		//end
		
		
		
		if (trs.length != 0) {
			hasList = "1";
		} else {
			hasList = "0";
		}
		for (var i=0; i<trs.length; i++) {
			trs[i].onmouseover = _trMouseOver;
			trs[i].onmousedown = choose;
			
			if (i%2==0) {
				trs[i].style.backgroundColor="whitesmoke";
			}
		}
		
		var flag = false;
		if (trim(element._dfv)=="true" ) {
			flag = true;
		}
		
		if (trim(element.required)=="true") {
			if(objList.all("top_line")!=null)
				objList.all("top_line").style.display = "none";
			objInput.style.backgroundColor=JHTC_COLOR.REQUIRED;
		} else {
			objInput.style.backgroundColor="#FFFFFF";
		}
		
		if (trim(element.required)=="true" && trim(element._dfv) != "false" ) {
			flag = true;
		}
		if (flag) {
			var tr=objList.getElementsByTagName("TR");
			var rowIdx=1;
			if(element._editorUrl!=null){
				rowIdx=2;
			}
			if(tr.length>rowIdx){
				element.value=(tr[rowIdx].cells[0].value);
				objInput.value = tr[rowIdx].cells[0].innerText;
			}else{
				element.value=("");
				objInput.value="";
			}
			
			changeTitle();
		}
		if (!flag) {
			objInput.value=''
			element.value=('')
		}
	}
	function _trMouseOver(){
		if (this.parentNode.parentNode.choseIndex!=null) {
			with (this.parentNode.parentNode.rows[this.parentNode.parentNode.choseIndex].runtimeStyle) {
				backgroundColor="";
				color="";
			}
		}
		
		this.parentNode.parentNode.choseIndex = this.rowIndex;
		this.runtimeStyle.backgroundColor="darkblue";
		this.runtimeStyle.color="white";
	}
	function adjustWidth(){
		var objList=_objListE();
		var element=_e();
		if (resultMxLength <= 9) {
			objList.style.width = element.offsetWidth;
		} else {
			var temp = parseFloat(element.offsetWidth)+parseFloat(((resultMxLength-9)*20>140)?140:(resultMxLength-9)*20);
			if(isNaN(temp))
				temp=140;
			objList.style.width = temp>140?(element.extent>140?element.extent:140):temp;
		}	
	}
	function insertEditorLink(){
		var objList=_objListE();
		var element=_e();
		element._editorUrl=null;
		if(element.editor==""||typeof(window.jhtc_select_dict_editor_init)=="undefined"){
			return;
		}else{
			jhtc_select_dict_editor_init(element,objList.all("top_line"));
		}
	}
	// 刷新页面
	function refresh() {
		var objList=_objListE();
		var element=_e();
		is_onload="true";
		//alert(element.parentNode.outerHTML)
		var aKey = "";
		if (trim(element.accessKey)!="") {
			aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
		}
		if (trim(element.label)!="") {
			element.parentNode.getElementsByTagName("SPAN")[0].innerText = element.label+aKey+(element.label==''?"":"：");
		}
		//added by wsj 在重新加载的时候，检查select控件是否下拉。
		if(element.load!=""){
			var dis=objList.style.display; //检查select控件是否下拉。
			if(element._dfv==null){
				if(element.required=="true")
					is_load=true;
				else
					is_load=false;    
			}else if(element._dfv=="false"){
				is_load=false;
			}else{
				is_load=true;
			}
			if(is_loaded)
				is_load=true;
			//alert("fun1: "+element.name+onDataLoaded)
			initList(onRefreshDataLoaded);
		}
		
	}
	function onRefreshDataLoaded(){
		var element=_e();
		if(element.load=="retrieve_method"){
				if (dis!= "none"){
					showList();
				}
				else{
					loadListdata();	
				}			
			}
			//adjPosition();
			if(typeof(oldSetValue)!="undefined")
				setValue(oldSetValue);
			doOnDataLoaded();	
	}
	function doOnDataLoaded(){
		var element=_e();
		//alert("fun2: "+element.name)
		if(element.async=="true"&&element.onDataLoaded!=""){
			eval(element.onDataLoaded);
		}
	}
	// 点击选择项响应事件
	function choose() { 
		var objList=_objListE();
		var objInput=_objInputE();
		var iframe=_iframeE();
		var element=_e();
		var table = objList.getElementsByTagName("TABLE")[0];
		if (table.choseIndex==null) {
			return false;
		}
		if(table.rows[table.choseIndex].cells.length>0 &&table.rows[table.choseIndex].cells[0].value=="__editor_url__"){
			objList.style.display = "none";
			iframe.style.display=objList.style.display 
			jhtc_select_dict_editor_open(element);
			element.refresh();
			return;
		}
		objInput.value = table.rows[table.choseIndex].innerText;
		changeTitle();
		objList.style.display = "none";
		iframe.style.display=objList.style.display 
		element.style.backgroundImage='url("'+outImgPath+'")';
		
		if (table.rows[table.choseIndex].cells.length>0 &&
			element.value!=table.rows[table.choseIndex].cells[0].value) {
			element.value=(table.rows[table.choseIndex].cells[0].value);
			//var oEvent = createEventObject();
			//oEvent.result = element.value;
			__jhtcDispatchEvent(element,"onchange");
			//add by 
		}
		return;
		objInput.select();
		objInput.focus();
	}
	
	// 点击按钮等响应事件
	function clickDocument() { 
		var objList=_objListE();
		var iframe=_iframeE();
		var element=_e();
		if(objList==null) return;
		
		var objSrc = window.event.srcElement;
		if (element.contains(objSrc) || objList.contains(objSrc) ) {
			if (objSrc.value != null) {
				element.value=(objSrc.value)
			}
			return;
		} 
		
		objList.style.display="none";
		iframe.style.display=objList.style.display 
		element.style.backgroundImage='url("'+outImgPath+'")';
		window.document.detachEvent("onmousedown",clickDocument);
	
	}
	
	// 键盘响应
	function navigateKeys() {
		var objList=_objListE();
		var objInput=_objInputE();
		var iframe=_iframeE();
		var element=_e();
		var nKeyCode=event.keyCode;
		switch(nKeyCode){
			case 113:  // F2 
				if(element.findpage!=null)
					openDialog(element.findpage, 'dialogWidth:900px;dialogHeight:550px')
				else{
					selectObj.setPara(_getParaString());
					selectObj.setLoad(element.load);
					selectObj.select()
					if (trim(selectObj.value)!=''){
						setValue(selectObj.value)
						if(typeof(onchange)!='undefined'){
							if(onchange!=null){
								var str=""+onchange;
								
								str = str.substring(22,str.length - 2);
								str=str.replace("this","element");
								
								eval(str);
							}
						}
					}  
				}
				return true;
				break;
			case 114:
				if(element.InsertSQL==''){
					return false;
				}else{
					if(element.text==''){
						alert("内容为空，不能保存!");
						return false;
					}else{
						window.xmlhttp.post(element.InsertSQL, element.InsertPARA+'<td>'+element.text+'</td>', '?isCheck=false');
						var estr = window.xmlhttp._object.responseText;
									if(estr.indexOf("<error>")>0){
										estr=estr.substr(estr.indexOf("<error>")+7);
										estr=estr.substr(0,estr.indexOf("</error>"));
										alert(estr);
										return false;
									}else{
										alert("保存成功!");
										element.refresh();
										return true;
									}
					}
				}
				break;
			case 38://^
				if (objList!=null && objList.style.display=="") {
					scrollUpList();
					return false;
				}
				break;
			case 40://\|/
				if (1==1) {
					if (objList==null || objList.style.display=="none") {
						if(is_enabled)
							showList();
						return false;
					}
				}
				if (objList!=null && objList.style.display=="") {
					scrollDownList();
					return false;
				}
				break;
			case 13://Enter
				
				event.keyCode=9
				if (element.value == "") {
					__jhtcDispatchEvent(element,"onchange");
					event.keyCode=9;
				}
				if (objList!=null && objList.style.display=="") {
					event.keyCode=9;
					choose();
					
					objInput.onkeyup = objInput.ondragend = null;
					
					
					return false;
				}
				break;
			case 27:// Esc
				if (objList!=null){
					objList.style.display="none";
					iframe.style.display=objList.style.display
				}
				return false;
			case 33://pageup
				if (objList!=null && objList.style.display=="") {
					scrollPageUpList();
					return false;
				}
			case 34://pagedown
			if (objList!=null && objList.style.display=="") {
				scrollPageDownList();
				return false;
			}
			default:
				objInput.onkeyup = objInput.ondragend = changeInput;
				return true;
		}
		return true;
	}
	
	//向上翻页滚动列表
	function scrollPageUpList() {
		var objList=_objListE();
		var table = objList.getElementsByTagName("TABLE")[0];
		if (table.rows.length>0) {
			var count = objList.offsetHeight/table.rows[0].offsetHeight-1;
			for (var i=0; i<count; i++) {
				scrollUpList();
			}
		}
	}
	
	//向下翻页滚动列表
	function scrollPageDownList() {
		var objList=_objListE();
		var table = objList.getElementsByTagName("TABLE")[0];
		if (table.rows.length>0) {
			var count = objList.offsetHeight/table.rows[0].offsetHeight-1;
			for (var i=0; i<count; i++) {
				scrollDownList();
			}
		}
	}
	
	//向下滚动列表
	function scrollDownList() {
		var objList=_objListE();
		var table = objList.getElementsByTagName("TABLE")[0];
		if (table.rows.length<1)
			return;
		if (table.choseIndex>=table.rows.length-1)
			return;
		
		if (table.choseIndex!=null) {
			with (table.rows[table.choseIndex].runtimeStyle) {
				backgroundColor = "";
				color = "";
			}
		} else {
			table.choseIndex = 0;
		}
		
		table.choseIndex++;
		
		var baseDivTop = 0;
		var parentObj = table.rows[table.choseIndex];
		while (parentObj.tagName != "DIV") {
			baseDivTop += parentObj.offsetTop;
			parentObj = parentObj.offsetParent;
		}
		
		if (baseDivTop+table.rows[table.choseIndex].offsetHeight*2 > parentObj.scrollTop+parentObj.offsetHeight) {
			parentObj.scrollTop = parentObj.scrollTop + table.rows[table.choseIndex].offsetHeight;
		}
		
		table.rows[table.choseIndex].runtimeStyle.backgroundColor="darkblue";
		table.rows[table.choseIndex].runtimeStyle.color="white";
	}
	
	//向上滚动列表
	function scrollUpList() {
		var objList=_objListE();
		var table = objList.getElementsByTagName("TABLE")[0];
		
		if (table.choseIndex==null || table.choseIndex==0) {
			table.choseIndex = 0;
			return;
		} else {
			with (table.rows[table.choseIndex].runtimeStyle) {
				backgroundColor = "";
				color = "";
			}
		}
		
		table.choseIndex--;
		
		table.rows[table.choseIndex].runtimeStyle.backgroundColor="darkblue";
		table.rows[table.choseIndex].runtimeStyle.color="white";
		
		var baseDivTop = 0;
		var parentObj = table.rows[table.choseIndex];
		while (parentObj.tagName != "DIV") {
			baseDivTop += parentObj.offsetTop;
			parentObj = parentObj.offsetParent;
		}
		
		if (baseDivTop < parentObj.scrollTop) {
			parentObj.scrollTop = parentObj.scrollTop - table.rows[table.choseIndex].offsetHeight;
		}
	}
	
	//检查输入项是否正确
	function check() {
		var objList=_objListE();
		var objInput=_objInputE();
		var element=_e();
		if (trim(element.checkValue)!="true") {
			return true;
		}
		if ("true"==element.required) {
			if (window.trim(objInput.value)=="" && trim(element.checkValue)=="true")  {
				if (proxyLabel != null) {
					alert(proxyLabel+"不能为空！");
				} else {
					alert("请选择必选项！");
				}
				objInput.focus();
				objInput.select();
				return false;
			}
		}
		
		var os=objList.innerText.replace(/\s/g,"");
		var oi=objInput.value.replace(/\s/g,"");
		os=os.replace(/&gt;/g,">")
		os=os.replace(/&lt;/g,"<")
		os=os.replace(/&quot;/g,"\"")
		
		/*
		if (trim(code)=="true" && os.indexOf("\""+trim(oi).replace(/　.*$/, "")+"\"value=\""+trim(oi).replace(/^.*　/, "")+"\"")==-1) {
		if ((trim(required)=="true" || trim(oi)!="") && trim(checkValue)=="true") {
			alert("请选择合法的值！");
			objInput.select();
			return false;
		}
		}
		
		if (trim(code)!="true" && os.indexOf("value=\""+oi+"\"")==-1 ) {
		if ((trim(required)=="true" || trim(oi)!="") && trim(checkValue)=="true") {
			alert("请选择合法的值！");
			objInput.select();
			return false;
		}
		}
		*/
		if (objInput.value != "") {
			var table = objList.getElementsByTagName("TABLE")[0];
			/*此段代码导致的问题是：到鼠标从一个选择项滑动到另外的选择项，但不点击确定，但结果仍然有效，因此屏蔽
			if (table.choseIndex != null) {
				objInput.value = table.rows[table.choseIndex].innerText;
				if(objInput.value==""&&trim(element.required)=="true"){
					if (proxyLabel != null) {
						alert(proxyLabel+"不能为空！");
					} else {
						alert("请选择必选项！");
					}
					objInput.focus();
					objInput.select();
					return false;	
				}
				changeTitle();
				if (table.rows[table.choseIndex].cells.length>0 &&
				element.value!=table.rows[table.choseIndex].cells[0].value) {
					element.value=(table.rows[table.choseIndex].cells[0].value);
				}
			}
			*/
		}
		return true;
	}
	
	function _getParaString(){
		var element=_e();
		if(element.para=="")
			return element.para;
		if(element.para.indexOf("javascript:")==0){
			return window.eval(element.para);
		}
		return element.para;
	}
	//给SELECT赋值
	function setValue(str) {
			var element=_e();
			if(is_loaded==false||element.__inInit==false ){
				is_load=true;
				if(element.fastFlag=="false"){
					initList(function(){setValueCallBack(str)});
				}else{
					setValueCallBack(str);
				}
			}else
				setValueCallBack(str);
	}
	function setValueCallBack(str){
		var objList=_objListE();
		var objInput=_objInputE();
		var iframe=_iframeE();
		var element=_e();
		oldSetValue=str;
		if (str == null || trim(str)==""){
			objInput.value="";
			element.value=("");
			return;
		}
		var trs = objList.getElementsByTagName("TR");
		element.value=(str);
		
		var j=0;
		for (j=0; j<trs.length; j++) {
			if (trs[j].cells[0].value==str) {
				break;
			}
		}
		
		if(j==trs.length) j=0;
		objInput.value = trs[j].cells[0].innerText;
		
		//wsj added 06-1-11  
		//loadListdata();	
		if(is_enabled){
			objList.style.display = "";
			if(iframe!=null)
				iframe.style.display=objList.style.display 
		}    
		if (trs.length==0)
			return false;
		var i=0;
		for (; i<trs.length; i++) {
			if (trs[i].innerText.indexOf(objInput.value)==0) {
				break;
			}
		}
		if (i==trs.length)
			i=0;
		if (trs[i].parentNode.parentNode.choseIndex!=null) {
			trs[i].parentNode.parentNode.choseIndex = trs[i].rowIndex; 
			trs[i].runtimeStyle.backgroundColor="darkblue";
			trs[i].runtimeStyle.color="white";  
		}
		clickBtn()
		//wsj added 06-1-11      
		changeTitle();
	}
	function _init(){
		var element=_e();
		element.initBegin();
		element.initEnd();
		
	}
	function _initAttr(){
		var element=_e();
		element._dfv=null;
		var oh=element.outerHTML.toLowerCase().replace(/"/g,"").replace(/'/g,"");
		if(oh.indexOf("defaultvalue=true")>0){
			element._dfv="true";
		}
		if(oh.indexOf("defaultvalue=false")>0){
			element._dfv="false";
		}
		//alert(jQuery(element).attr("initValue"))
		jhtc_attr_init(element,"label",null);
		jhtc_attr_init(element,"load",null);
		jhtc_attr_init(element,"para","");
		jhtc_attr_init(element,"findpage",null);
		jhtc_attr_init(element,"initValue",null);
		jhtc_attr_init(element,"initLoad","true");
		jhtc_attr_init(element,"code","false");
		jhtc_attr_init(element,"required","false");
		jhtc_attr_init(element,"checkValue","true");
		jhtc_attr_init(element,"extent","140");
		jhtc_attr_init(element,"maxInput","50");
		jhtc_attr_init(element,"text","");
		jhtc_attr_init(element,"onAfterInit","");
		jhtc_attr_init(element,"async","false");
		jhtc_attr_init(element,"onDataLoaded","");
		jhtc_attr_init(element,"listRows","5");
		jhtc_attr_init(element,"InsertSQL","");
		jhtc_attr_init(element,"InsertPARA","");
		jhtc_attr_init(element,"serverObj",null);
		jhtc_attr_init(element,"editor","");
		jhtc_attr_init(element,"labelFix","false");
		jhtc_attr_init(element,"fastFlag","false");

	  	//alert(element.initValue)
	  	element.__inInit=true;
		element.initListByXML=initListByXML;
		element.selectItem=selectItem;
		element.getItemCount=getItemCount;
		element.setEnabled=setEnabled;
		element.hasResult=hasResult;
		element.setValue=setValue;
		element.check=check;
		element.refresh=refresh;
		element.reloadPic=reloadPic;
		element.setLoaded=setLoaded;
		element.initBegin=initBegin;
		element.initEnd=initEnd;
		element.jhtcInit=_init;
		element.adjPosition=adjPosition;
	}
	_initAttr();
	if(_e().load!=null) {
		return {type:"dict",
			name:_e().load+","+_e().code,
			data:_getParaString(),
			onData:function(xmlStr){
					_e().__inInit=false;
					is_loaded=true;
					initListCallback(xmlStr);
					onRefreshDataLoaded();
				}
			};
	}else{
		return null;
	}
};
jhtc_class_map["inputSelect"]=jhtc_select;
jhtc_class_map["inputselect"]=jhtc_select;
