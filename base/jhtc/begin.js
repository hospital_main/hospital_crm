/*
Javascript HTC begin
Author:liu zhikun
*/
function jhtc_attr_init(obj,key,dv){if(typeof(jQuery(obj).attr(key))!="undefined"){}else{obj[key]=dv;};};jhtc_tag_id=0;jhtc_class_map={};jhtc_tag_map={};jhtc_onbeforeload_funs=[];jhtc_onload_funs=[];
function jhtcParseXml(xmlStr){var vXmlEndRow = new ActiveXObject("Microsoft.XMLDOM");vXmlEndRow.async=false;vXmlEndRow.loadXML(xmlStr);return vXmlEndRow;};
function jhtcLoadXml(xmlFile){var vXml = new ActiveXObject("Microsoft.XMLDOM");vXml.async=false;vXml.load(xmlFile);return vXml;};
function jhtcSetHtml(element,htmlStr,fun){$(element).html(htmlStr);__jhtcEndRunQuery(element,fun);};
function jhtcGetHtml(element){return $(element).html();};
function jhtcSetText(element,text){$(element).text(text);};
function jhtcGetText(element){return $(element).text;};
function jhtcGetAttr(obj,key,dv){if(typeof(jQuery(obj).attr(key))!="undefined"){return jQuery(obj).attr(key);}else{if(typeof(dv)!="undefined"){return dv;}else{return null;}};};
function jhtcSetClass(element,clazz){$(element).removeClass("*");$(element).addClass(clazz);};
function jhtcAddClass(element,clazz){$(element).addClass(clazz);};
function jhtcRemoveClass(element,clazz){$(element).removeClass(clazz);};
function jhtcAddOnBeforeload(fun){jhtc_onbeforeload_funs.push(fun);};
function jhtcAddOnloaded(fun){jhtc_onload_funs.push(fun);};
function jhtcGetNextId(){jhtc_tag_id++;return "_jhtc_"+jhtc_tag_id;};
function jhtcCheckAndGetId(element){jhtc_attr_init(element,"id",jhtcGetNextId());if(jQuery(element).attr("id")==""){jQuery(element).attr("id",jhtcGetNextId());};return jQuery(element).attr("id");};
function jhtcAjax(urlStr,data,isPost,isAsync,callback){//callback(xmlHttpObj,xml,text);
	
};
function jhtcDecodeXmlString(s){
	s=s.replace(/&quot;/g,"\"");
	s=s.replace(/&amp;/g,"&");
	s=s.replace(/&gt;/g,">");
	s=s.replace(/&lt;/g,"<");
	return s;
}
function jhtcEncodeXmlString(s){
	s=s.replace(/&/g,"&amp;");
	s=s.replace(/>/g,"&gt;");
	s=s.replace(/</g,"&lt;");
	return s;
}
function __jhtcBindPropertyChange(obj,name,fun){
	obj["__jhtcOld"+name]=obj[name];
	obj.attachEvent('onpropertychange',function(evt){
		if(evt.propertyName!=name)
			return;
		fun(obj,name,obj["__jhtcOld"+name],obj[name]);
		obj["__jhtcOld"+name]=obj[name];
	});
}
function __jhtcDispatchEvent(element,evtName){
	if(element[evtName]){
		var evfun=element[evtName];
		if(typeof(evfun)!="function")
			evfun=new Function(element[evtName]);
		if(typeof(evfun)=="function"){
			return evfun.call(element);
		}
	}
	return null;
};
function __jhtcElementFun(element){
	return __jhtcElementFunById(jhtcCheckAndGetId(element));
};
function __jhtcElementFunById(id){
	var iid="#"+id;
	return function(){return jQuery(iid)[0];};
};
function __parseDefaultValue(element,defv){
	var key="defaultvalue";
	if(typeof(jQuery(element).attr(key))=="undefined"||jQuery(element).attr(key)==""){
		var str=element.outerHTML;
		if(str.indexOf("defaultValue")>0){
			str=str.replace(/'/g,"").replace(/"/g,"").replace(/\\/g,"").replace(/>/g,"");
			str=str.substr(str.indexOf("defaultValue")+"defaultValue".length+1);
			if(str.indexOf(" ")>0){
				str=str.substr(0,str.indexOf(" "));
			}
			element[key]=str;
			return;
		}
		element[key]=defv;
	}
};
function jhtcBeginQuery(){
	jQuery(document.body).append("<div id='__begin_query__' style=\"position:absolute;top:0px;left:0px;width:100%;Height:100%; \"><table border='0' width='98%' height='98%'><tr><td align='center' valign='middle'><img src='/vh/img/loading.gif'/></td></tr></table></div>");
};
function jhtcEndQuery(){
	jQuery("#__begin_query__").remove();
};
var JHTC_COLOR={};JHTC_COLOR.REQUIRED="#FFFCD7";JHTC_COLOR.SELECTED="#FFFCD7";