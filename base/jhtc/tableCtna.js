function jhtc_tableCtna(win,jhtc_obj){
  var pageWin=win;
  var element=jhtc_obj;
  /***************
  onInitInput=function(){
        var temp_xml = '<root length="'+(5+initV.length)+'">';
        temp_xml = temp_xml + '<input type="text" index="1" name="daf" class="inputTextTableMoney" maxInput="14"/>';
        temp_xml = temp_xml + '<select name="deom" codeindex="2" index="5" load="acct_month"/>';
        temp_xml = temp_xml + '</root>';
        window.event.xml = temp_xml;
      }
  **********/
  var baseDivTop=0, baseDivLeft=0, baseDiv, headDiv, parentObj; // 调整位置
  var addr, submitXml; // 后台交互信息
  var begin=-1, page=18, total=-1, isTurn; // 翻页信息
  var vBtnHTML; // 左上的按钮
  var noUpdate=false //默认数据修改提交，noUpdate='true' 数据没有修改提交
  var vAvtiveHeads = 0;
  var _affectCount=0;
  var is_page=false;
  var glo_hidemsg;
  
  var _keepedHead = '';//如果keepHead属性为true,把处理后的表头文件保存在这里

  //xml Dom Element 保存请求返回的数据
  var response = null;
  element.cellInputs = new Array();

  var vexpression = null;
  var mymainTab = null;

  var selectInputOpenFindDlg=false;
  function init() {
    //1.取得Btn
    page=parseInt(element.count_page);
    vBtnHTML = element.innerHTML

    //3.生成innerHTML
    var vXml = new ActiveXObject("Microsoft.XMLDOM");
    vXml.async = false;
    vXml.loadXML("<root/>");
    
    element.innerHTML = vXml.transformNode(genBaseXslt()).replace(/<td isBtn="true"><\/td>/, "<td>"+vBtnHTML+"</td>")

    //4.调整表格不折行
    var theads = element.getElementsByTagName('THEAD')
    for (var k=0; k<theads.length; k++) {
      for (var i=0; i<theads[k].rows.length; i++) {
        for (var j=0; j<theads[k].rows[i].cells.length; j++) {
          theads[k].rows[i].cells[j].noWrap = 'true'
        }
      }
    }
    //5.调整位子参数
    adjPosition()
    window.attachEvent("onresize", adjPosition);
    window.attachEvent("onresize", function(){window.setTimeout(adjPosition,100)});
		element.attachEvent("onclick", sortTableTr);
    // -- TBD
    // 加行以适应大小
    sortTableTrSetCursor(-1);
    // -- TBD
    // 加行以适应大小
  }

  /* name,提交地址， vXmlStr,提交XML信息， flag,是否翻页标识 */
  function refresh(name, vXmlStr, flag, subFunc, hideMsg) {
    
    glo_hidemsg=hideMsg;
    // 1.与后台进行数据交换
    if (name != null && vXmlStr!=null) {
      addr = name
      submitXml = vXmlStr
      submitXml = submitXml + "<_ActiveHeads>"+getActiveHeads()+"</_ActiveHeads>";
      begin = 1
      if (flag) {
        isTurn = true
      }
    }
    if (addr==null)
      return
    if (subFunc == null) {
      subFunc = '';
    }
    //获得element的位置
    
    parentObj = baseDiv = element.document.getElementById('_base');
    baseDivTop = baseDivLeft = 0
    try{
	    while(parentObj.tagName != "BODY") {
		    baseDivTop += parentObj.offsetTop;
		    baseDivLeft += parentObj.offsetLeft;
		    parentObj = parentObj.offsetParent;
	    }
    }catch(e){
    	return ;
    }
    
    element.insertAdjacentHTML("afterBegin","<table width=100% height=100% style='position:absolute'><tr><td align=center valign=middle><img src='/images2/tablectn/wait.gif'></td></tr></table>");
    //element.innerHTML="<table width=100% height=100%><tr><td align=center valign=middle>正在导入请稍后。。。</td></tr></table>";
    mxmlhttp=getAnsycHttp();
	  mxmlhttp.onreadystatechange = getReady;
    if (isTurn) {
      if (begin<1) begin = 1
      if (subFunc != '') {
        mxmlhttp.open("POST",addr+".viewhigh"+subFunc+"&isTurn=ture&isCheck=false",true);
        mxmlhttp.send("<root>"+"<_begin>"+begin+"</_begin><_end>"+(begin+page-1)+"</_end>"+submitXml+"</root>");
        //window.xmlhttp.post(addr, "<_begin>"+begin+"</_begin><_end>"+(begin+page-1)+"</_end>"+submitXml, subFunc+'&isTurn=ture&isCheck=false')
      } else {
        mxmlhttp.open("POST",addr+".viewhigh"+"?isCheck=false&isTurn=ture",true);
        mxmlhttp.send("<root>"+"<_begin>"+begin+"</_begin><_end>"+(begin+page-1)+"</_end>"+submitXml+"</root>");
        //window.xmlhttp.post(addr, "<_begin>"+begin+"</_begin><_end>"+(begin+page-1)+"</_end>"+submitXml, '?isCheck=false&isTurn=ture')
      }
    } else {
      //window.xmlhttp.post(addr, submitXml, subFunc)
      
      mxmlhttp.open("POST",addr+".viewhigh"+subFunc,true);
      mxmlhttp.send("<root>"+submitXml+"</root>");
        
    }

    

	  //prompt("element.outerHTML",element.outerHTML);
  }
function getReady(){
    if(mxmlhttp.readyState==4)
     {
      if(mxmlhttp.status==200)
      {
                 	var source = mxmlhttp.responseText;
                 	 // -- TBD 关于错误处理, 如果无记录，如何处理，有记录如何处理
                    // 得到总条数
                  if (source.search(/<total>/)!=-1) {
                    total = eval(source.substring(source.search(/<total>/)+"<total>".length, source.search(/<\/total>/)))
                    if (total<1) begin = 0 // ???
                  }
              
                  // 3. 加入补充信息，生成innerHTML
                  var vXml = inputXML(source);
                  response = vXml.selectSingleNode("//root").cloneNode(true);
              
                  refreshTab(source);
                  sortTableTrSetCursor(-1);
                  window.setTimeout(adjPosition,1);
                  window.setTimeout(adjPosition,100);
                  //alert(source)
                  if (!window.doMsg(source, glo_hidemsg)) {
                    return false;
                  } else 
                    return true;
  			
      }
      else
      {
       alert("抱歉，装载数据失败。原因：" + mxmlhttp.statusText);
      }
     }
  }
  function getAnsycHttp(){
  
    return new ActiveXObject("Microsoft.XMLHTTP");
  }
  // 生成Base Xslt
  function genBaseXslt(serverStr) {
    // 取得自定义xsl 的head，body
    var vXml = new ActiveXObject("Microsoft.XMLDOM");
    vXml.async=false;
    if(element.xslFile=="")
    	vXml.load(element.document.URL.replace(/.html.*$/g, ".xsl"));
    else
    	vXml.load(element.xslFile);
    var temp = vXml.xml
		try{
    if (temp==null || temp.length==0) {
      alert('此文件'+element.document.URL.replace(/.html.*$/g, ".xsl")+'不符合XSL格式')
      return;
    }
    }catch(e){}
    var vCol = ''
    if (temp.search(/<colgroup>/i)!=-1)
      vCol = temp.substring(temp.search(/<colgroup>/i), temp.search(/<\/colgroup>/i)+"</colgroup>".length)

    var vHead = temp.substring(temp.search(/<thead>/i), temp.search(/<\/thead>/i)+"</thead>".length)
    var vBody = temp.substring(temp.search(/<tbody>/i), temp.search(/<\/tbody>/i)+"</tbody>".length)

    // 取得后台的变化表头，并替换
    if (serverStr != null && serverStr.search(/<thead>/i) != -1 && trim(element.headXsl)=='false' ) {
      vHead = serverStr.substring(serverStr.search(/<thead>/i), serverStr.search(/<\/thead>/i)+"</thead>".length)
    }

    //生成基础xslt
    vXml.load(window.prefix+"base/xsl/mainTable.xsl");
    var str = vXml.xml
      
	// 替换表头  
    if (vHead!=null && vHead.length>="</thead>".length) {
      		str = str.replace(/<thead\/>/, vCol+vHead)
    }
    
    // 替换体数据格式
    if (vBody!=null && vBody.length>="</tbody>".length) {
      str = str.replace(/<tbody>(.|\n)*<\/tbody>/, vBody)
    }

    //////////////////////////////////////////////////////////
    str = replaceActiveHeads(str);

    //////////////////////////////////////////////////////////
    vXml.loadXML(str)
    return vXml
  }

  function refreshTab(source)
  {
    element.serverXml = source
		refreshTabData(source);
		refreshTabStyle(source);
  }
  function refreshTabData(source)
  {
	/********这里处理保存的表头 added by zrc********/
	if(isTurn==true && element.keepHead=='true' && begin!=1){
		if(element.xslFileNonFirestPage==''){
			alert('请为xslFileNonFirestPage属性指定xsl文件路径');
			return;
		}
		element.xslFile = element.xslFileNonFirestPage ;
	}
	else if(element.keepHead == 'true'){	
		element.xslFile = "";
    }
	/********结束处理保存的表头********/	  
  	var xslt = genBaseXslt(response.xml);
  	var vXml = inputXML(response.xml);
    var str = vXml.transformNode(xslt);    
    if (str.indexOf("<td isBtn=\"true\"><\/td>")!=-1) { // 按钮
      if (window.trim(element.remove) != '' && /<pk>/.test(source)) {
        str = str.replace(/<td isBtn="true"><\/td>/, "<td>"+vBtnHTML+"<button id='"+element.remove+"' class='tableBtn' style='display:"+element.remove_display+"' accessKey='D'>删除</button></td>")
      } else  {
        str = str.replace(/<td isBtn="true"><\/td>/, "<td>"+vBtnHTML+"</td>")
      }
    }
    if (isTurn && str.indexOf("<td isTurn=\"true\"></td>")!=-1) { // 翻页
      var end = begin+page-1
      if (total<end) end = total

      var pageStr = "&nbsp;<span style='cursor:hand;color:#F89800' id='_home'>首页</span>&nbsp;<span style='cursor:hand;color:#F89800' id='_up'>上页</span>"
      if (begin <= 1)
        pageStr = "&nbsp;首页&nbsp;上页"

      if (end != total)
        pageStr = pageStr + "&nbsp;<span style='cursor:hand;color:#F89800' id='_down'>下页</span>&nbsp;跳至<input type='text' size='2' id='_turn_to'>&nbsp;<span style='cursor:hand;color:#F89800' id='_end'>尾页</span></td>"
      else
      	pageStr = pageStr + "&nbsp;下页&nbsp;跳至<input type='text' size='2' id='_turn_to'>&nbsp;尾页"

      var pc=parseInt(begin/page+0.9999,10)+"/"+parseInt(total/page+0.9999,10)
      var temp = "<td align='right' noWrap='true' style='font-family:\"宋体\";font-size:13px;'>总共"+total+"条 每页"+page+"条 第"+pc+"页" + pageStr
      str = str.replace(/<td isTurn="true"><\/td>/, temp)
    }
    // 动态表头
    var danHeader = ""
    if (trim(element.fixCol) != "") {
      var temp = str.search(/<colgroup>/i)!=-1?str.substring(str.search(/<colgroup>/i), str.search(/<\/colgroup>/i)+"</colgroup>".length):""
      temp = "<root>" + temp + str.substring(str.search(/<thead>/i), str.search(/<\/thead>/i)+"</thead>".length)+"</root>"
      vXml.loadXML(temp)
      danHeader = vXml.transformNode(genHeadXslt(temp))
    }

	/********这里处理保存的表头 added by zrc********/
	if(isTurn==true && element.keepHead=='true' && begin!=1){
		str = str.replace(/<thead>(.|\n)*<\/thead>/,_keepedHead);
	}
	else{	
		_keepedHead = str.substring(str.search(/<thead>/i), str.search(/<\/thead>/i)+"</thead>".length);
    	//element.innerHTML  = str + danHeader;
    }
    str += danHeader;
    element.innerHTML  = str ;
	/********结束处理保存的表头********/
    
    if (trim(element.fixCol) != ""){
    	element.document.getElementById("_mainTableHead_").attachEvent("onclick", function(){sortTableTr(1)});
    }
    
    //2.初始化cellInputs
   // var oEvent = createEventObject();
   // evtInitInput.fire(oEvent);
    __jhtcDispatchEvent(element,"onInitInput");

    var xml  ;//oEvent.xml    
    if (xml!=null && xml!='') {
      var vXml = new ActiveXObject("Microsoft.XMLDOM");
      vXml.async = false;//?
      vXml.loadXML(xml)
      
      var root = vXml.getElementsByTagName("root").item(0);
     
      for (var i=0; i < root.attributes.length; i++) {
        if (root.attributes.item(i).name=='length') {
          element.cellInputs.length = root.attributes.item(i).value
          break;
        } else {
          alert('root的属性length未赋值');
          return false;
        }
      }

      var tempDiv = document.createElement("<div/>");
      tempDiv.innerHTML = xml.replace(/^<root>|<\/root>$/gi, "");
      for (var i=0; i<tempDiv.childNodes.length; i++) {
        if (tempDiv.childNodes[i].index >= element.cellInputs.length) {
          alert('此name：'+tempDiv.childNodes[i]+'index值非法');
          return;
        }
        if (tempDiv.childNodes[i].name == null) {
          alert('存在name未赋值');
          return false;
        }
        element.cellInputs[tempDiv.childNodes[i].index] = tempDiv.childNodes[i]
        tempDiv.childNodes[i].kind = tempDiv.childNodes[i].type
        if (tempDiv.childNodes[i].tagName=="SELECT") {
          element.cellInputs[tempDiv.childNodes[i].index].kind = 'select';
        }
      }
    }

    // 初始化cellInputs
    for (var i=0; i<element.cellInputs.length; i++) {
      if (element.cellInputs[i]!=null) {
        if (element.cellInputs[i].kind == 'text') {
          element.cellInputs[i].onkeyup = element.cellInputs[i].ondragend = function() {
            if (this.parentNode.innerText != this.value) {
            	if (this.parentNode.childNodes.length>1)
            		this.parentNode.childNodes[0].data = this.value
            	else
              	this.parentNode.insertAdjacentHTML("afterBegin", this.value)
              this.parentNode.parentNode.rowSubmit = true
              if (trim(this.name)!='') {
                this.parentNode.value = "<" + this.name + ">" + this.value+"</" + this.name +">";
              } else {
                this.parentNode.value = "<td>" + this.value+"</td>";
              }
              adjPosition()
            }
          }
          element.cellInputs[i].onchange=function(){
              //alert(this.value);
              if (this.parentNode.childNodes.length>1)
            		this.parentNode.childNodes[0].data = this.value
            	else
              	this.parentNode.insertAdjacentHTML("afterBegin", this.value)
              this.parentNode.parentNode.rowSubmit = true
              if (trim(this.name)!='') {
                this.parentNode.value = "<" + this.name + ">" + this.value+"</" + this.name +">";
              } else {
                this.parentNode.value = "<td>" + this.value+"</td>";
              }
              adjPosition()
            }
        } else if (element.cellInputs[i].kind == 'checkbox') {
          element.cellInputs[i].onclick = function() {
            var temp=0;
            if (this.checked) {
              this.parentNode.insertAdjacentHTML("afterBegin", '√')
              temp = 1;
            } else {
              this.parentNode.childNodes[0].data = ''
            }
            this.parentNode.parentNode.rowSubmit = true
            if (trim(this.name)!='') {
              this.parentNode.value = "<" + this.name + ">" + temp+"</" + this.name +">";
            } else {
              this.parentNode.value = "<td>" + temp+"</td>";
            }
          }
        }
        ////处理Select BEGIN
        else if (element.cellInputs[i].kind == 'select') {
          element.cellInputs[i].onblur=function(){
            if(typeof(this.required)!="undefined"&&this.value==""&&this.required=="true")
              return ;
            if(selectInputOpenFindDlg==true)
              return ;
            var v=this.options[this.selectedIndex].value;
            var t=this.options[this.selectedIndex].text;
            var p=this.parentNode;
            p.removeChild(this);
            p.parentNode.cells[this["codeindex"]].innerText=v;
            p.innerText=t;
          }
        }
        ////处理Select END
      }
    }
    //取得table对象
	  mymainTab = element.getElementsByTagName("table").item(1);
  }
  
  function refreshTabStyle(source) {
	 // 4. 默认处理 aa)选择提交 a)删除，b)修改，c)上翻页，d)下翻页

    // aa)加入选择提交的处理
    if (window.trim(element.choose) != '' && /<pk>/.test(response.xml)) {
      // 显示选择框
      var inputs = element.getElementsByTagName('input')

      for (var i=0; i<inputs.length; i++) {
        if (inputs[i].type == 'checkbox') {
          inputs[i].parentNode.style.display = ''
		      if(inputs[i].parentNode.tagName=='TH')
			      inputs[i].onclick = setAll; // 全选或全取消
		      else {
    				if(inputs[i].parentNode.tagName=='TD')
    				{
    				  inputs[i].onclick = checkAll; // 检测是否全选或全取消
    				  if(inputs[i].parentNode.nextSibling!=null){
	    				  if(inputs[i].parentNode.nextSibling.childNodes[0].data=='1')
	    					  inputs[i].checked = true
    				  }
    				  if(getCheckboxOldChecked(inputs[i])==true){
    				  	inputs[i].checked = true
    				  }
    				}
    			}
        }
      }
    }

    // a)加入删除的处理
    if (window.trim(element.remove) != '' && /<pk>/.test(response.xml)) {
      // 加入按钮响应事件
      element.document.getElementById(element.remove).onclick = del

      // 显示删除
      var inputs = element.getElementsByTagName('input')
      for (var i=0; i<inputs.length; i++) {
        if (inputs[i].type == 'checkbox') {
          inputs[i].parentNode.style.display = ''
		      if(inputs[i].parentNode.tagName=='TH')
			      inputs[i].onclick = setAll; // 全选或全取消
		      else {
    				if(inputs[i].parentNode.tagName=='TD'){
   			      inputs[i].onclick = checkAll; // 检测是否全选或全取消
   			      inputs[i].checked=getCheckboxOldChecked(inputs[i]);
   			    }
    			}
        }
      }
    }

    // b)加入修改的处理
    if (window.trim(element.update) != '' && /<pk>/.test(response.xml)) {
      // -- TBD 检查权限

      // 加下划线和相应方法
      var vHref = element.getElementsByTagName('a');
      for (var i=0; i<vHref.length; i++) {
        if (vHref[i].href == '') {
          vHref[i].style.textDecoration = 'underline'
          vHref[i].style.cursor = 'hand'
          vHref[i].style.color  = 'blue'

          vHref[i].onclick = modify
        }
      }
    }

    // c) 加入上翻页处理
    var objUp = element.document.getElementById('_up')
    if (objUp != null) {
      objUp.onclick = pageUp
    }
    // d) 加入下翻页处理
    var objDown = element.document.getElementById('_down')
    if (objDown != null) {
      objDown.onclick = pageDown
    }
    var objDown = element.document.getElementById('_turn_to')
    if (objDown != null) {
      objDown.onkeydown = turnTo
    }
    
    var ojbHome = element.document.getElementById('_home')
    if (ojbHome != null) {
      ojbHome.onclick = pageHome
    }

    var objEnd = element.document.getElementById('_end')
    if (objEnd != null) {
      objEnd.onclick = pageEnd
    }

    // 5. 调整nowrap
    var tbody = element.getElementsByTagName('TBODY')[1]
    for (var i=0; i<tbody.rows.length; i++) {
    	tbody.rows[i].style.height='22'
      if (i%2 == 0) { // 两色相间
        tbody.rows[i].style.backgroundColor = window.oddColor
      } else {
        tbody.rows[i].style.backgroundColor = window.evenColor
      }
      tbody.rows[i].onmouseout = function() {
        if (this.rowIndex==element.rowIndex)
          return
        this.runtimeStyle.backgroundColor = ''
      }
      for (var j=0; j<tbody.rows[i].cells.length; j++) {
        tbody.rows[i].cells[j].noWrap = 'true'
        tbody.rows[i].cells[j].onfocus = tdActive
      }
    }

    for (var i=0; i<tbody.rows.length; i++) {
      var hideCount=0;
      for (var k=0; k<tbody.rows[i].cells.length; k++) {
        if (tbody.rows[i].cells[k].style.display=='none') {
          hideCount++
        }
      }
      
      for (var j=0; j<element.cellInputs.length; j++) {
        if (element.cellInputs[j]!=null) {  
        	if(element.is_autofocus=="true"){                    
          	tbody.rows[i].cells[j+hideCount].setActive()
          }
          break;
        }
      }
      break;
    }

    // 6. 调整位置
    adjPosition()

    // 7. 加入键盘响应
    tbody.onkeydown = navigateKeys
    if(element.onrefresh!=''){
    	eval(element.onrefresh)
    }
  }

  // 生成Head Xslt
  function genHeadXslt(serverStr) {
    // 取得自定义xsl 的head，body
    var vXml = new ActiveXObject("Microsoft.XMLDOM");
    vXml.async=false;
    if(element.xslFile=="")
    	vXml.load(element.document.URL.replace(/.html.*$/g, ".xsl"));
    else
    	vXml.load(element.xslFile);
    var temp = vXml.xml
    if (temp==null || temp.length==0) {
      alert('此文件'+element.document.URL.replace(/.html.*$/g, ".xsl")+'不符合XSL格式')
      return;
    }
    
    var vCol = ''
    if (temp.search(/<colgroup>/i)!=-1)
      vCol = temp.substring(temp.search(/<colgroup>/i), temp.search(/<\/colgroup>/i)+"</colgroup>".length)
    var vHead = temp.substring(temp.search(/<thead>/i), temp.search(/<\/thead>/i)+"</thead>".length)

    // 取得后台的变化表头，并替换
    if (serverStr != null && serverStr.search(/<thead>/i) != -1) {
      vHead = serverStr.substring(serverStr.search(/<thead>/i), serverStr.search(/<\/thead>/i)+"</thead>".length)
    }
		
    //生成表头xslt
    vXml.load(window.prefix+"base/xsl/mainHead.xsl");
    var str = vXml.xml
    // 替换表头
    if (vHead!=null && vHead.length>="</thead>".length) {
        str = str.replace(/<thead\/>/, vCol+vHead)
        str = replaceActiveHeads(str);
    }
    vXml.loadXML(str)

    return vXml
  }

  function  replaceActiveHeads(str)
  {
	 var actives = 0;
	 actives = parseInt(vAvtiveHeads);

     if(actives != 0)
     {
		if(response == null)
		{
			for( i =1; i<= actives;i++)
			{
				str = str.replace("@active_"+i,"");
			}
		}
		else
		{
			for(i= 1 ; i<= actives;i++)
			{
				var tempStr = addr+"_head_"+i;
				var activeNode = response.selectSingleNode("//"+tempStr);
				if(activeNode == null)
				{
					//alert("返回结果中不存在"+tempStr+"节点");
					return;
				}
				var cols = activeNode.childNodes.length;
				var replaceStr = activeNode.xml;
				replaceStr = replaceStr.replace("<"+tempStr+">","");
				replaceStr = replaceStr.replace("</"+tempStr+">","");
				if (str.indexOf("<th>@active_"+i+"</th>") != -1)
				  str = str.replace("<th>@active_"+i+"</th>",replaceStr);
				else
				  str = str.replace('<th noWrap="true">@active_'+i+'</th>',replaceStr);
				str = str.replace("active_"+i,cols);
			}
		}
    }
    return str;

  }

  // 生成Col  Xslt
  // -- TBD
  // 复制Head Xslt为Common Xslt
  // -- TBD

  function adjPosition() {
    //1. 调整baseDiv的位子
    parentObj = baseDiv = element.document.getElementById('_base');
    baseDivTop = baseDivLeft = 0
    try{
	    while(parentObj.tagName != "BODY") {
		    baseDivTop += parentObj.offsetTop;
		    baseDivLeft += parentObj.offsetLeft;
		    parentObj = parentObj.offsetParent;
	    }
    }catch(e){
    	return ;
    }

    if(element.rightFix.substr(element.rightFix.length-1)=="%")
		baseDiv.style.pixelWidth = parentObj.clientWidth - baseDivLeft -  parentObj.clientWidth*parseFloat(element.rightFix.replace(/%/g,""))/100
	else
    	baseDiv.style.pixelWidth = parentObj.clientWidth - baseDivLeft -  parseFloat(element.rightFix)
    element.style.pixelWidth=baseDiv.style.pixelWidth+2;
    if(element.bottomFix.substr(element.bottomFix.length-1)=="%")
   		baseDiv.style.pixelHeight = parentObj.clientHeight - baseDivTop - 2 - parentObj.clientHeight*parseFloat(element.bottomFix.replace(/%/g,""))/100
    else
   		baseDiv.style.pixelHeight = parentObj.clientHeight - baseDivTop - 2 - parseFloat(element.bottomFix)
   	element.style.pixelHeight=baseDiv.style.pixelHeight

    //2. ......

    //3. 调整输入域的位置
    var tbody = baseDiv.getElementsByTagName('tbody')[0]

    if (this.rowIndex!=null && this.rowIndex>-1) {
      var hideCount=0;
      if (tbody.parentNode.rows.length>this.rowIndex) {
	      for (i=0; i<tbody.parentNode.rows[this.rowIndex].cells.length; i++) {
	        if (tbody.parentNode.rows[this.rowIndex].cells[i].style.display=='none') {
	          hideCount++
	        }
	      }
      }

      for (var i=0; i<this.cellInputs.length; i++) {
        if (this.cellInputs[i]!=null && this.cellInputs[i].style.display=='' && tbody.parentNode.rows.length>this.rowIndex) {
          var temp = tbody.parentNode.rows[this.rowIndex].cells[i+hideCount]
          var comp = tbody.parentNode.rows[this.rowIndex].cells[i+hideCount]
          var vLeft=0, vTop=0;
          while (temp.tagName != "DIV") {
            vTop += temp.offsetTop;
  	        vLeft += temp.offsetLeft;
  	        temp = temp.offsetParent;
  	      }

          with (this.cellInputs[i].style) {
            pixelHeight = comp.clientHeight
            pixelWidth = comp.clientWidth
  	        left = vLeft + eval(comp.parentNode.parentNode.parentNode.border)
  	        top = vTop + eval(comp.parentNode.parentNode.parentNode.border)
          }
          break;
        }
      }
    }

    //调整headDiv的位子
    headDiv = element.document.getElementById('_head');
    if (headDiv==null) return
    baseDiv.onscroll = function() {
      headDiv.scrollLeft = baseDiv.scrollLeft
    }

    var tHead1s = baseDiv.getElementsByTagName('thead')
    var tHead2s = headDiv.getElementsByTagName('thead')

    for (var i=0; i<tHead1s[0].rows.length; i++) {
      for (var j=0; j<tHead1s[0].rows[i].cells.length; j++) {
        tHead1s[0].rows[i].cells[j].noWrap = 'true'
      }
    }

    with (headDiv.style)  {
      pixelTop = baseDivTop;
      pixelLeft = baseDivLeft
      pixelWidth = baseDiv.clientWidth
      pixelHeight = tHead1s[0].offsetHeight + tHead1s[0].parentNode.border*2
    } // -- study baseDiv.offsetWidth-baseDiv.offsetWidth+baseDiv.clientWidth, tHead1s[0].offsetHeight + tHead1s[0].parentNode.border*2

    for (var i=0; i<tHead1s[0].rows.length; i++) {
      for (var j=0; j<tHead1s[0].rows[i].cells.length; j++) {
      	try {
	        tHead2s[0].rows[i].cells[j].noWrap = 'true'
	        tHead2s[0].rows[i].cells[j].style.pixelWidth = tHead1s[0].rows[i].cells[j].offsetWidth
	        tHead2s[0].rows[i].cells[j].style.pixelHeight = tHead1s[0].rows[i].cells[j].offsetHeight
				}catch(e){}
      }
    }
    headDiv.style.display=''
  }

  // 激活td
  function tdActive() {
    for (var i=0; i<element.cellInputs.length; i++) {
      if (element.cellInputs[i]!=null)
        element.cellInputs[i].style.display = 'none'
    }

    var objTD = event.srcElement
    var parentObj = objTD
    var tdTop = tdLeft = 0
    while (parentObj.tagName != "DIV") {
	    tdTop += parentObj.offsetTop;
	    tdLeft += parentObj.offsetLeft;
	    parentObj = parentObj.offsetParent;
    }

    objTD.parentNode.runtimeStyle.backgroundColor = window.choseColor
    if (element.rowIndex!=null && objTD.parentNode.parentNode.parentNode.rows.length>element.rowIndex && 
        element.rowIndex!=objTD.parentNode.rowIndex) {
      objTD.parentNode.parentNode.parentNode.rows[element.rowIndex].runtimeStyle.backgroundColor = ''
    }
    element.rowIndex = objTD.parentNode.rowIndex
   
    var cellInput = element.cellInputs[objTD.cellIndex]
    if (cellInput!=null) {
      cellInput.style.display = ''
      cellInput.focus()
      if (objTD.contains(cellInput)) { 
        return;
      }

      if (objTD.childNodes.length>0 && objTD.childNodes[0].data!=null)
        cellInput.value = objTD.childNodes[0].data.replace(/,/g,"");
      else
      	cellInput.value = ''

     	if (cellInput.value=='X') {
    		cellInput.style.display='none'
    		cellInput.value=''
    		return;
    	}
    	
    	if(cellInput.kind == 'select')
    	  objTD.innerText="";
      objTD.appendChild(cellInput)
      cellInput.style.top = tdTop + 1
      cellInput.style.left = tdLeft + (objTD.cellIndex>0?1:0) // 第一个cell不需要偏移1px
      if(cellInput.kind == 'select'){
        cellInput.style.width ="98%"
        cellInput.style.height ="90%"
        
      }else{
        cellInput.style.width = objTD.clientWidth
        cellInput.style.height = objTD.clientHeight
      }
      cellInput.focus()

      if (cellInput.kind == 'text') {
        var range = cellInput.createTextRange()
        range.collapse(true);
 				range.moveStart('character',-1);
        range.select();
      } else if (cellInput.kind == 'checkbox') {
        if (objTD.childNodes[0].data == '√') {
          cellInput.checked = true
        } else {
          cellInput.checked = false
        }
      } else if (cellInput.kind == 'select') {
        var select=cellInput;
        if(typeof(select.load)=="undefined"||select.load==null)
          return ;
        if(typeof(select.hasLoad)!="undefined")
          return ;
        select.hasLoad=true;
        var dom=getDict(select.load,select.para)
        var data=dom.documentElement;
        var nodes=data.selectNodes("/*/para");
        var code,value;
        if(typeof(select.required)=="undefined"||select.required!="true"){
          o=new Option(value,code);
          select.options[0]=o;
        }
        if(nodes.length>0){
          for(var i=0;i<nodes.length;i++){
            code=nodes[i].getAttribute("code");
            value=nodes[i].getAttribute("value");
            o=new Option(value,code);
            select.options[select.options.length]=o;
          }
        }
        //F2 begin
        select.onkeydown=function(){
          if(event.keyCode==113){
              selectInputOpenFindDlg=true;
              findXml= dom.xml.replace("<root>","<root code='false' qtype='0' key=''>");
              selectObj.setXML(findXml)
              selectObj.select()
              if (trim(selectObj.value)!='')
                this.value=selectObj.value;
              }
              selectInputOpenFindDlg=false;  
         }
        //F2 end
        if(select.options.length>0)
          select.options[0].selected=true;
      }
    }
  }
  //页面跳至 add by twl
  function turnTo(){
    var mo=element.document.getElementById("_turn_to");
    if(event.keyCode==13){
      if(mo.value==""){
        
      }else if(isNaN(mo.value)){
        mo.value=""
      }else if(mo.value>parseInt(total/page+0.9999,10)){
					//alert('超出总页数!');
					//mo.value="";
					mo.value=parseInt(total/page+0.9999,10);
	       	__jhtcDispatchEvent(element,"onpagechange_start");
	        if(element.is_alterpage=="false") return ;
	
	        page_num=mo.value;
	        begin=page*(page_num-1)+1
	        refresh();
	        //change_end.fire();
	        __jhtcDispatchEvent(element,"onpagechange_end");
      }else{
       // change_start.fire();
       __jhtcDispatchEvent(element,"onpagechange_start");
        if(element.is_alterpage=="false") return ;

        page_num=mo.value;
        begin=page*(page_num-1)+1
        refresh();
        //change_end.fire();
        __jhtcDispatchEvent(element,"onpagechange_end");
      }
    }
  }
  //得到光标的位置
  function getCursorPosition(obj){
    var qswh="^#@%166!^"
    if (obj.maxLength!=null && obj.maxLength<2000) {
      obj.maxLength = obj.maxLength+qswh.length
    }
    obj.focus();
    var rng=document.selection.createRange();
    rng.text=qswh;
    var nPosition=obj.value.indexOf(qswh)
    rng.moveStart("character", -qswh.length)
    rng.text="";
    if (obj.maxLength!=null && obj.maxLength<2000+qswh.length) {
      obj.maxLength = obj.maxLength-qswh.length
    }
    return nPosition;
  }

  // 键盘响应
  function navigateKeys() {
    var parentObj = event.srcElement
    while (parentObj.tagName!='BODY') {
      if (parentObj.tagName=='TD') break;
      parentObj = parentObj.parentNode
    }
    var objTD = parentObj;
    var nCell=parentObj.cellIndex;

    while (parentObj.tagName!='BODY') {
      if (parentObj.tagName=='TR') break;
      parentObj = parentObj.parentNode
    }
    var objTR = parentObj;
    var nRow=parentObj.rowIndex;

    var objTBODY=objTR.parentNode;
    var objTHEAD=objTBODY.parentNode.getElementsByTagName('thead')[0]

    var hideCount=0;
    for (i=0; i<objTR.cells.length; i++) {
      if (objTR.cells[i].style.display=='none') {
        hideCount++
      }
    }

		var flagX = false;
    var nKeyCode=event.keyCode;
    do {
       var isIn=(event.srcElement.tagName=='INPUT' && event.srcElement.type=='text')||(event.srcElement.tagName=='SELECT')
      switch(nKeyCode){
        case 37://<-
          if (isIn==true && getCursorPosition(event.srcElement)> 0)
            return true;
          nCell--;
          break;
        case 38://^
          nRow--;
          break;
        case 39://->
          if (isIn==true && getCursorPosition(event.srcElement)< event.srcElement.value.length )
            return true;
          nCell++;
          break;
        case 40://\|/
          if (event.altKey) {
            objList.style.display=''
            return true;
          }
          nRow++;
          break;
        case 9:// tab
          if (event.ctrlKey) {
            return true;
          }
          if (event.shiftKey) {
            nCell--;
            break;
          }
        case 13://Enter
          nCell++;
          break;
        case 33: // pageUp
          if (event.ctrlKey) {
            pageUp()
            return false;
          }
        case 34: // pageDown
          if (event.ctrlKey) {
            pageDown()
            return false;
          }
        case 35: // end
          if (event.ctrlKey) {
            pageEnd()
            return false;
          }
        case 36: // home
          if (event.ctrlKey) {
            pageHome()
            return false;
          }
        default:
          return true;
      }

      if (nCell==-1){
        nRow--;//跳转到上一行
        nCell=objTR.cells.length-hideCount-1;//最后一列
      }
      if (nCell==objTR.cells.length-hideCount) {
        nRow++;//跳转到下一行首位置
        nCell=0;//第一列
      }
      if (nRow == objTHEAD.rows.length - 1) {
        nRow = objTHEAD.rows.length
        return false
      }
      if (nRow == objTHEAD.rows.length + objTBODY.rows.length) {
        nRow = objTHEAD.rows.length + objTBODY.rows.length - 1
        return false;
      }
      flagX = false;
      if (element.cellInputs[nCell]!=null) {
      	if (objTBODY.parentNode.rows.length>nRow && objTBODY.parentNode.rows[nRow].cells.length>nCell &&
      			objTBODY.parentNode.rows[nRow].cells[nCell+hideCount].childNodes!=null &&
      			objTBODY.parentNode.rows[nRow].cells[nCell+hideCount].childNodes.length>0) {
      		if (objTBODY.parentNode.rows[nRow].cells[nCell+hideCount].childNodes[0].data == 'X') {
      			flagX = true
      		}
      	}
      }
    } while (element.cellInputs.length>0 && (element.cellInputs[nCell]==null || flagX));

    if (nRow < objTR.rowIndex) {
      var offLine = objTR.rowIndex-objTHEAD.rows.length -1;
      if (offLine>=0 &&  objTBODY.childNodes(offLine).offsetTop <= baseDiv.scrollTop+objTBODY.childNodes(offLine).clientHeight) {
        baseDiv.scrollTop = baseDiv.scrollTop - objTBODY.childNodes(offLine).clientHeight
      } // 调整滚动条
    } else if (nRow > objTR.rowIndex) {
      if (objTBODY.rows(objTR.rowIndex).offsetTop+objTBODY.rows(objTR.rowIndex).offsetHeight >=
            (baseDiv.scrollTop+baseDiv.offsetHeight)) {
        baseDiv.scrollTop = baseDiv.scrollTop + objTR.clientHeight
      } // 调整滚动条
    }

    if (nRow == objTHEAD.rows.length) {
      baseDiv.scrollTop = 0
    }
    if (nRow == objTBODY.rows.length) {
      baseDiv.scrollTop = baseDiv.scrollHeight
    }

    objTBODY.rows[nRow-objTHEAD.rows.length].cells[nCell+hideCount].setActive()    
    return false;
  }
  
  function checkChange() {   //检测数据有没有被修改
    var submitStr = assemble();

    if (trim(submitStr)=='') {
      return false;
    } else 
      return true;
  }
  
  /*
   * isClear --  情况原有标记
   */
  function submit(btn,addXML,hideMsg,subFunc,isClear,anoUpdate) {
    if (window.trim(btn.name)=='') {
      alert('请设置name')
      return false;
    }
    
    if (subFunc==null) {
      subFunc = '';
    }    
     if (anoUpdate!=null) {
      noUpdate = anoUpdate; 
       
    }        
    
    var submitStr = assemble();
    if(submitStr.toLowerCase() == 'false')
      return   
    if (trim(submitStr)=='') {
      alert('数据没有发生修改')
      return false;
    }
    window.xmlhttp.post(btn.name, submitStr, subFunc)
    var str = window.xmlhttp._object.responseText
    if (!window.doMsg(str,hideMsg)) {
      return false;
    }
    // 清除rowSubmit
    var tbody = baseDiv.getElementsByTagName('tbody')[0]
    for (var i=0; i<tbody.rows.length; i++) {
      if (tbody.rows[i].rowSubmit != null && tbody.rows[i].rowSubmit && trim(isClear)!='false') {
        tbody.rows[i].rowSubmit = null
      }
    }

    if (window.top.window.document.body.getAttribute(window.location)!=null) {
      window.top.window.document.body.getAttribute(window.location).refresh()
    }
    return true;
  }

  /*
   * 简单表格的XML组装
   */
  function assemble2() { // -- TBD待处理，给此行加个标记，并检查此行
    var result = "";
    var tbody = baseDiv.getElementsByTagName('tbody')[0]
    if(tbody.rows[0].storeFlag != null && tbody.rows[0].storeFlag.toLowerCase() == 'false')
      if(!confirm('该月份数据已保存，确定要覆盖？'))
        return 'false'
    for (var i=0; i<tbody.rows.length; i++) {

        for (var j=0; j<tbody.rows[i].cells.length; j++) {
          if (tbody.rows[i].cells[j].value!=null && trim(tbody.rows[i].cells[j].value) != ''){
          result = result + "<record>"
            result = result + tbody.rows[i].cells[j].value
          result = result + "</record>"
          }
        }
    }
    return result;
  }
  function checksubmit(btn) {
    if (window.trim(btn.name)=='') {
      alert('请设置name')
      return false;
    }

    var submitStr = assemble2();
    if(submitStr.toLowerCase() == 'false')
      return

    if (trim(submitStr)=='') {
      alert('数据没有发生修改')
      return true;
    }
    window.xmlhttp.post(btn.name, submitStr)
    var str = window.xmlhttp._object.responseText
    if (!window.doMsg(str)) {
      return true;
    }
    // 清除rowSubmit
    var tbody = baseDiv.getElementsByTagName('tbody')[0]
    for (var i=0; i<tbody.rows.length; i++) {
      if (tbody.rows[i].rowSubmit != null && tbody.rows[i].rowSubmit) {
        tbody.rows[i].rowSubmit = null;
      }
    }

    if (window.top.window.document.body.getAttribute(window.location)!=null) {
      window.top.window.document.body.getAttribute(window.location).refresh()
    }
  }

function assemble() { // -- TBD待处理，给此行加个标记，并检查此行
	_affectCount=0;
    var result = "";
    var tbody = baseDiv.getElementsByTagName('tbody')[0]
    if(tbody.rows.length<1)
      return result;
    if(tbody.rows[0].storeFlag != null && tbody.rows[0].storeFlag.toLowerCase() == 'false')
      if(!confirm('该月份数据已保存，确定要覆盖？'))
        return 'false'
    for (var i=0; i<tbody.rows.length; i++) {
      if (!noUpdate){
      
      if (tbody.rows[i].rowSubmit != null && tbody.rows[i].rowSubmit) {
        result = result + "<record>"
        _affectCount++;
        for (var j=0; j<tbody.rows[i].cells.length; j++) {
          if (trim(tbody.rows[i].cells[j].value) != '')
            result = result + tbody.rows[i].cells[j].value
        }
        result = result + "</record>"
      }
      }
      else
      {       
         result = result + "<record>"
         _affectCount++;
        for (var j=0; j<tbody.rows[i].cells.length; j++) {
          if (trim(tbody.rows[i].cells[j].value) != '')
            result = result + tbody.rows[i].cells[j].value
        }
        result = result + "</record>"
      }
    }
    return result;
  }

  function setAll(){
	  var flag;
	  var inputs = element.getElementsByTagName('input');
	  for(var i=0;i<inputs.length;i++){
		 if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TH')) {
				flag=inputs[i].checked;
		 }
	  }

	  for(var i=0;i<inputs.length;i++){
	    if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TD'&&inputs[i].disabled!=true)) {
			  inputs[i].checked=flag;
	    }
	  }
  }



  function checkAll(){
    var flag;
    var inputs = element.getElementsByTagName('input');
	  for(var i=0;i<inputs.length;i++){
	    if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TD')) {
			  if(inputs[i].checked==false){
				  flag=false;
				  break;
			  }
	    }
	  }

    for(var i=0;i<inputs.length;i++){
  	  if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TH')) {
  			if(flag==false)
  				inputs[i].checked=false;
  			else
  			  	inputs[i].checked=true;
  	  }
    }
	if(typeof(this.onclick2)!="undefined")
		eval(this.onclick2);
  }

  // -- TBD del 和 modify值得改进，对于值的存储的定位不妥
  function del() {
    var data = element.getElementsByTagName('tbody')[1];
    var trs = data.getElementsByTagName("tr")
    var flag = false;

    for (var i=0; i<trs.length; i++){
      var inputs =  trs[i].getElementsByTagName("input");
      var errorFlag=false;
      for (var j=0; j<inputs.length; j++) {
        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
          if (!flag){
						if(confirm('确定要删除吗?')==false)
							return;
							flag = true;
		  		}
				  window.xmlhttp.post(element.remove, inputs[j].value)
				  var str = window.xmlhttp._object.responseText;
				  if (str.indexOf("<error/>")==-1) errorFlag = true;
				  if (errorFlag) {
		        if (!window.doMsg(str)) return true;
		      }
          trs[i].parentNode.removeChild(trs[i]);
          i--;
          // -- TBD 检测是否成功
          // -- TBD 报错
          // -- TBD 返回
        }
      }
    }
    if (!flag) {
      alert('请先选择')
      return;
    }
	  refresh()
	  if(element.onafterdel!=""){
	    var f=eval(element.onafterdel);
	    f();
	  }
	  //alert('删除成功')
  }

  // choose
  function submit_choose(btn,addXML,isAll,isOnly,hideMsg,subFunc) {
  	_affectCount=0;
    if (window.trim(element.choose) == '') {
      alert('choose属性设置错误！');
    }
    
    if (subFunc==null) {
      subFunc = '';
    }
    
    var data = element.getElementsByTagName('tbody')[1];
    var trs = data.getElementsByTagName("tr")
    var temp_xml = '';
    var flag = false;
    var onlyFlag = false;
    
    for (var i=0; i<trs.length; i++){
      var inputs =  trs[i].getElementsByTagName("input")
      for (var j=0; j<inputs.length; j++) {
        if (isAll != null && isAll != "") {
          if (inputs[j].type == 'checkbox' && inputs[j].checked) {
  				  temp_xml = temp_xml + '<record>' + inputs[j].value + '<checked>1</checked></record>';
          } else {
            temp_xml = temp_xml + '<record>' + inputs[j].value + '<checked>0</checked></record>';
          }
          _affectCount++;
          flag = true;
        } else {
          if (inputs[j].type == 'checkbox' && inputs[j].checked) {
          	_affectCount++;
            if (isOnly != null && isOnly != "") {
              if (!onlyFlag) {
      				  temp_xml = temp_xml + '<record>' + inputs[j].value + '</record>';
                flag = true;
                onlyFlag = true;
              } else {
                alert("一次只能选择一条记录！");
                return false;
              }
            } else {
              temp_xml = temp_xml + '<record>' + inputs[j].value + '</record>';
              flag = true;
            }
          }
        }
      }
    }

    if (!flag) {
      alert('请先选择')
      return false;
    }
    if (addXML != null && addXML != "") {
      temp_xml = addXML + '<multiData>' + temp_xml + '</multiData>'
  	}
  	//prompt('',temp_xml);return false;
	  window.xmlhttp.post(btn, temp_xml ,subFunc)
	  var str = window.xmlhttp._object.responseText

    if (window.doMsg(str,hideMsg)) {
      return true;
    } else 
      return false;

	  refresh();

  }
  function submit_choose_base(btn,addXML,isAll,isOnly,hideMsg,subFunc) {
  	_affectCount=0;
    if (window.trim(element.choose) == '') {
      alert('choose属性设置错误！');
    }
    
    if (subFunc==null) {
      subFunc = '';
    }
    
    
    var data = element.getElementsByTagName('tbody')[1];
    var trs = data.getElementsByTagName("tr")
    var temp_xml = '';
    var flag = false;
    var onlyFlag = false;
    
    for (var i=0; i<trs.length; i++){
      var inputs =  trs[i].getElementsByTagName("input")
      for (var j=0; j<inputs.length; j++) {
        if (isAll != null && isAll != "") {
          if (inputs[j].type == 'checkbox' && inputs[j].checked) {
  				  temp_xml = temp_xml + '<record>' + inputs[j].value + '<checked>1</checked></record>';
          } else {
            temp_xml = temp_xml + '<record>' + inputs[j].value + '<checked>0</checked></record>';
          }
          _affectCount++;
          flag = true;
        } else {
          if (inputs[j].type == 'checkbox' && inputs[j].checked) {
          	_affectCount++;
            if (isOnly != null && isOnly != "") {
              if (!onlyFlag) {
      				  temp_xml = temp_xml + '<record>' + inputs[j].value + '</record>';
                flag = true;
                onlyFlag = true;
              } else {
                alert("一次只能选择一条记录！");
                return false;
              }
            } else {
              temp_xml = temp_xml + '<record>' + inputs[j].value + '</record>';
              flag = true;
            }
          }
        }
      }
    }

   
    
  	//prompt('',temp_xml);
  	temp_xml="<root>"+temp_xml+"</root>"
		if(addXML!=null)
	  window.xmlhttp.post(btn, "<abcde>"+temp_xml.replace(/</g, "&lt;").replace(/>/g, "&gt;")+"</abcde>"+addXML,subFunc)
	  else
	  window.xmlhttp.post(btn, "<abcde>"+temp_xml.replace(/</g, "&lt;").replace(/>/g, "&gt;")+"</abcde>",subFunc)
	  var str = window.xmlhttp._object.responseText

    if (window.doMsg(str,hideMsg)) {
      return true;
    } else 
      return false;

	  refresh();

  }
  // modify
  function submit_modify(btn,addXML,isAll,isOnly,hideMsg,subFunc) {
  		_affectCount=0;
    if (subFunc==null) {
      subFunc = '';
    }
    
    var data = element.getElementsByTagName('tbody')[1];
    var trs = data.getElementsByTagName("tr")
    var temp_xml = '';
    var flag = false;
    var onlyFlag = false;
    
    for (var i=0; i<trs.length; i++){
      var inputs =  trs[i].getElementsByTagName("input")
      for (var j=0; j<inputs.length; j++) {
        if (isAll != null && isAll != "") {
          if (inputs[j].type == 'checkbox' && inputs[j].checked!=getCheckboxOldChecked(inputs[j])) {
  				  temp_xml = temp_xml + '<record>' + getSubmitModifyPkStr(inputs[j]) + '<checked>1</checked></record>';
          } else {
            temp_xml = temp_xml + '<record>' + getSubmitModifyPkStr(inputs[j]) + '<checked>0</checked></record>';
          }
          _affectCount++;
          flag = true;
        } else {
          if (inputs[j].type == 'checkbox' && inputs[j].checked!=getCheckboxOldChecked(inputs[j])) {
          	_affectCount++;
            if (isOnly != null && isOnly != "") {
              if (!onlyFlag) {
      				  temp_xml = temp_xml + '<record>' + getSubmitModifyPkStr(inputs[j]) + '</record>';
                flag = true;
                onlyFlag = true;
              } else {
                alert("一次只能选择一条记录！");
                return false;
              }
            } else {
              temp_xml = temp_xml + '<record>' + getSubmitModifyPkStr(inputs[j]) + '</record>';
              flag = true;
            }
          }
        }
      }
    }

    if (!flag) {
      alert('请先选择')
      return false;
    }
    if (addXML != null && addXML != "") {
      temp_xml = addXML + '<multiData>' + temp_xml + '</multiData>'
  	}
	  window.xmlhttp.post(btn, temp_xml ,subFunc)
	  var str = window.xmlhttp._object.responseText

    if (window.doMsg(str,hideMsg)) {
      return true;
    } else 
      return false;

	  refresh();

  }
  function getCheckboxOldChecked(chk){
  	var str=chk.value;
  	if(typeof(str)=="undefined"||str.indexOf("checkbox_value")<0)
  		return chk.checked;
  	if(str.indexOf("<checkbox_value>1</checkbox_value>")>=0)
  		return true;
  	else
  		return false;
  }
  function getSubmitModifyPkStr(chk){
  	var value=chk.value;
  	if(getCheckboxOldChecked(chk)==true)
  		value=value.replace("<checkbox_value>1</checkbox_value>","")
  	else
  		value=value.replace("<checkbox_value>0</checkbox_value>","")
  	if(chk.checked==true)
  		return value+"<checkbox_value>1</checkbox_value>";
  	else
  		return value+"<checkbox_value>0</checkbox_value>";
  }

  function modify() {
    //var vLoad = this.parentNode.previousSibling.children(0).value
    var vLoad = this.parentNode.parentNode.children(0).children(0).value;

    var width = '350px'
    var height = '350px'
    if(element.dialogWidth != null)
      width = element.dialogWidth;
    if(element.dialogHeight != null)
      height = element.dialogHeight;

    openDialog(element.openPage+'?load='+vLoad, 'dialogWidth:'+width+';dialogHeight:'+height, element)
  }

  function pageUp() {
    if (!isTurn) return;
    if (begin<=1) {
      alert('此为首页')
      return false;
    }
    //change_start.fire();
    __jhtcDispatchEvent(element,"onpagechange_start");
    if(element.is_alterpage=="false") return ;
    
    begin = begin - page
    if (begin < 1) begin = 1
    refresh()
    //change_end.fire();
    __jhtcDispatchEvent(element,"onpagechange_end");
  }

  function pageDown() {
    if (!isTurn) return;

    if (total<=begin+page-1) {
      alert('此为尾页')
      return false
    }
    //change_start.fire();
    __jhtcDispatchEvent(element,"onpagechange_start");
    if(element.is_alterpage=="false") return ;

    begin = begin + page
    refresh();
    //change_end.fire();
    __jhtcDispatchEvent(element,"onpagechange_end");
  }

  function pageHome() {
    if (!isTurn) return;
    //change_start.fire();
    __jhtcDispatchEvent(element,"onpagechange_start");
    if(element.is_alterpage=="false") return ;
    begin = 1
    refresh();
    //change_end.fire();
    __jhtcDispatchEvent(element,"onpagechange_end");
  }

  function pageEnd() {
    if (!isTurn) return;
    //change_start.fire();
    __jhtcDispatchEvent(element,"onpagechange_start");
    if(element.is_alterpage=="false") return ;

    begin = parseInt(total/page)*page + 1
    refresh();
    //change_end.fire();
    __jhtcDispatchEvent(element,"onpagechange_end");
  }

  function checkFormula(formula){
  }

  function setActiveHeads()
  {
	  vAvtiveHeads = element.activeHeads;
  }

  function getActiveHeads()
  {
	  return vAvtiveHeads;
  }

  function getTabData()
  {
  	if(response == null)
  	{
  		return "<root/>"
  	}
  	return response.cloneNode(true);
  }

  function inputXML(argSource){ ////objXMLDoc接收XML文件数据
    var objXMLDoc = null;
    try{
      switch(typeof(argSource)){
        case "string"://XML字节流
          if(argSource.search(/\./) != -1 && argSource.search(/\</) == -1){ //xml file's location

            objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");
            objXMLDoc.async = false;
            if(argSource.search(/\+/) != -1) argSource = eval(argSource);
              objXMLDoc.load(argSource);
              break;
            }
            if(argSource.search(/\</) != -1){ //xml string
              objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");
              objXMLDoc.loadXML(argSource);
              break;
            }
            objXMLDoc = eval(argSource);
            objXMLDoc = (objXMLDoc.XMLDocument)?(objXMLDoc.XMLDocument):null; //xml data island

            break;
        case "object"://XMLDOM对象
            if(argSource.xml) return objXMLDoc = argSource; //xml document object
            break;
            default:
            objXMLDoc = null;
      }
      if (!objXMLDoc.xml) objXMLDoc = null;
    } catch(err) { objXMLDoc = null; }

    if(objXMLDoc)
    {
  		objXMLDoc.setProperty("SelectionLanguage","XPath");
  		objXMLDoc.setProperty("SelectionNamespaces","xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
    }
    return objXMLDoc;
   }

  /*
	通过xmlNode选取数据列
	xml选取方式定义如下
	<root>
		<rows>
			<row idx='1'>
		</rows>
		<cols>
			<col name='abc' colIdx='1' />
			<col name='def' colIdx='4' />
			<col name='fgh' colIdx='7' />
			<col name='xyz' colIdx='8' />
		</cols>
	</root>
	表示，选取数据中第1行，第1，4，7，8列，组成统计图，各列分别命名对应其name,分别为abc,def,fgh,xyz
  */
  function getTabDataByXmlNodeNoGroup(xmlmode)
  {
  	if(xmlmode == null)
  	{
  		alert("getTabDataByXmlNodeNoGroup()输入参数为NULL");
  		return ;
  	}
  	var selectNode = inputXML(xmlmode);
  	var template = "<?xml version='1.0' encoding='gb2312'?><xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:output method='xml' encoding='gb2312'/><xsl:template match='/'><xsl:element name='root'><xsl:apply-templates select='td[@name]'/></xsl:element></xsl:template><xsl:template match='td[@name]'><xsl:element name='item'><xsl:element name='name'><xsl:value-of select='@name'/></xsl:element><xsl:element name='value'><xsl:value-of select='text()'/></xsl:element></xsl:element></xsl:template></xsl:stylesheet>";
  	var simpletemplate = inputXML(template);
  	var temp = selectNode.selectSingleNode("//row");

  	var rowIdx = temp.getAttribute("idx");
  	if(response == null)
  	{
  		return "<root/>"
  	}
  	var trObj = response.selectSingleNode("//tbody/tr[position()="+rowIdx+"]").cloneNode(true);
  	if(trObj == null)
  	{
  		alert("tabCtn指定行["+rowIdx+"]不存在");
  		return "<root/>"
  	}
  	var nodeList = selectNode.selectNodes("//col");
  	for(i = 0 ;i<nodeList.length;i++)
  	{
  		var tempItem = nodeList.item(i);
  		var tdNode = trObj.selectSingleNode(".//td[position()="+tempItem.getAttribute("colIdx")+"]");
  		tdNode.setAttribute("name",tempItem.getAttribute("name"));
  	}

  	var lastres = trObj.transformNode(simpletemplate);
  	return lastres

  }

  function changeTabData(expressionStr)
  {
  	if(expressionStr)
  	{
  		vexpression = expressionStr;
  	}
  	if(response == null)
  	{
  		return;
  	}
  	var tbodyNode = response.selectSingleNode("//tbody");
  	if(tbodyNode == null)
  	{
  		return;
  	}
  	tbodyNode = response.removeChild(tbodyNode);
  	var temp_template = constructTemplate();
  	if(temp_template == null)
  	{
  		return;
  	}
  	var resStr = tbodyNode.transformNode(temp_template);
  	tbodyNode = inputXML(resStr).selectSingleNode("//tbody");
  	response.appendChild(tbodyNode);
    refreshTab();
  }

  //生成指定的模板
  function constructTemplate()
  {
    // var template = calculateXSLT.selectSingleNode("//xsl:stylesheet").cloneNode(true);
    var templateStr = "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"><xsl:output method=\"xml\" encoding=\"gb2312\" /><xsl:template match=\"/\"><xsl:element name=\"tbody\"><xsl:copy-of select=\"@*\" /><xsl:apply-templates select=\"tr\" /></xsl:element></xsl:template><xsl:template match=\"tr\"><xsl:element name=\"tr\"><xsl:copy-of select=\"@*\" /><xsl:apply-templates select=\"td\" /></xsl:element></xsl:template><xsl:template match=\"td_Exp\"><xsl:element name=\"td\"><xsl:copy-of select=\"@*\" /><xsl:value-of select=\"expression\" /></xsl:element></xsl:template><xsl:template match=\"td\"><xsl:copy-of select=\".\" /></xsl:template></xsl:stylesheet>";
    var template = inputXML(templateStr);
    var reg = /@(\d*)/i;
    var resCol = vexpression.match(reg);
    resCol = resCol[0].substring(1);
    if(isNaN(resCol))
    {
      return null;
    }
    var splitCalculateExpStr = vexpression.substring(vexpression.indexOf('=')+1);
    splitCalculateExpStr = splitCalculateExpStr.replace(/\//g," div ");
    var calculateExpStr =splitCalculateExpStr.replace(/@(\d*)/g,"../td[position()='$1']/text()");

    var calculateTemplateNode = template.selectSingleNode("//xsl:template[@match=\"td_Exp\"]");
    calculateTemplateNode.setAttribute("match","td[position()=\'"+resCol+"\']");
    var expressionNode = calculateTemplateNode.selectSingleNode(".//xsl:value-of[@select='expression']");
    expressionNode.setAttribute("select",calculateExpStr);

    return template;
  }

  //设置公式
  function setExpression()
  {
		vexpression = element.expression;
		changeTabData();
  }

  //取得公式
  function getExpression()
  {
	  return vexpression;
  }

  //根据参数选取数据
  //captionColIdx 用作名称的列
  //valColIdx 用作统计对比值得列
  //tempRowStart 行起点
  //tempRowEnd 行终点
  function getTabDataForChart(captionColIdx,valColIdx,tempRowStart,tempRowEnd) {
    if(mymainTab.tBodies[0].rows(0) == null) {
      alert("没有结果，不能做图！");
      return "";
    }

  	var rowStart = -1;
  	var rowEnd = -1;
  	if(!tempRowStart) {
  	  rowStart ='0';
  	} else {
  		rowStart =tempRowStart;
  	}
  	if(!tempRowEnd) {
  		rowEnd = getRows();
  	} else {
  		rowEnd = tempRowEnd;
  	}
  	var resStr = "<root>";
  	var tempBody = mymainTab.tBodies[0];
  	for(i = rowStart;i<rowEnd;i++) {
  		var tempRow = tempBody.rows[i];
  		resStr= resStr +"<item><name>"+tempRow.cells[captionColIdx].innerText+"</name>"+"<value>"+(tempRow.cells[valColIdx].innerText).replace(/,/g,"")+"</value>"+"</item>";
  	}
  	resStr = resStr +"</root>"
  	return resStr;
  }

  //专为比较分析使用,即同一行有两列数据比较   //add by xsh
  
  //根据参数选取数据
  //captionColIdx 用作名称的列
  //valColIdx 用作统计对比值得列
  //tempRowStart 行起点
  //tempRowEnd 行终点
  function getTabDataForChart_comp(captionColIdx,valColIdx,tempRowStart,tempRowEnd) {
    if(mymainTab.tBodies[0].rows(0) == null) {
      alert("没有结果，不能做图！");
      return "";
    }

  	var rowStart = -1;
  	var rowEnd = -1;
  	if(!tempRowStart) {
  	  rowStart ='0';
  	} else {
  		rowStart =tempRowStart;
  	}
  	if(!tempRowEnd) {
  		rowEnd = getRows();
  	} else {
  		rowEnd = tempRowEnd;
  	}
  	var resStr = "<root>";
  	var tempBody = mymainTab.tBodies[0];
  	for(i = rowStart;i<rowEnd;i++) {
  		var tempRow = tempBody.rows[i];
  		resStr= resStr +"<item><name>"+tempRow.cells[captionColIdx].innerText+"</name>"+"<value>"+(tempRow.cells[valColIdx].innerText).replace(/,/g,"")+"</value>"+"</item><item><name></name>"+"<value>"+(tempRow.cells[valColIdx+1].innerText).replace(/,/g,"")+"</value>"+"</item>";
  	}
  	resStr = resStr +"</root>"
  	return resStr;
  }




  //colStart 列起点
  //colEnd   列终点
  function getTabDataForPLChart(tempColStart) {
    if(mymainTab.tBodies[0].rows(0) == null) {
      alert("没有结果，不能做图！");
      return "";
    }

  	var rowStart = "0";
  	var rowEnd = getRows();
  	var colStart = "2";
  	var colEnd = getCols();
  	if(!tempColStart) {
  		colStart = tempColStart;
  	}
  	var resStr = "<?xml version='1.0' encoding='gb2312'?><root>";
  	var tempHead = mymainTab.tHead;
  	var tempBody = mymainTab.tBodies[0];

    for(i = colStart;i<colEnd;i++) {
      resStr = resStr + "<col title='" + tempHead.rows[0].cells[i].innerText + "' >";
      for(j = rowStart;j<rowEnd;j++) {
    		var tempRow = tempBody.rows[j];
    		resStr= resStr +"<r text='"+tempRow.cells[0].innerText+"."+tempRow.cells[1].innerText+"' value='"+(tempRow.cells[i].innerText).replace(/,/g,"")+"' />";
    	}
    	resStr = resStr + "</col>";
  	}

  	resStr = resStr +"</root>"
  	return resStr;
  }

  function getCols() {
  	try {
  	  return mymainTab.tBodies[0].rows(0).cells.length;
  	} catch (e) {
  	  return 0;
  	}
  }

  function getRows() {
    try {
  	  return mymainTab.tBodies[0].rows.length;
  	} catch (e) {
  	  return 0;
  	}  
  }
  
  function getSumSelectData(colum) {
    var flag = false;
   
    var data = element.getElementsByTagName('tbody')[1];
    var trs = data.getElementsByTagName("tr");    
 		var resStr = 0 ;
    for (var i=0; i<trs.length; i++){      
      var inputs =  trs[i].getElementsByTagName("input")
	    for (var j=0; j<inputs.length; j++) {	        
        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
        		var tempv = trs[i].cells[colum].innerText;
				  	resStr += parseFloat(tempv.replace(',',''), 10);				  	
				    flag = true;
        }
	    }		
		}	  
    if (!flag) {
      alert('请先选择')
      return false;
    }
      	
  	return resStr;  	
  }
  
  function getSelectData(row,col){
    var data = element.getElementsByTagName('tbody')[1];
    var trs  = data.getElementsByTagName("tr");
    var value = trs[row].cells[col].innerText;
    return value.replace(',','')
    
  }  
  
  function setDataXml(source){
    var vXml = inputXML(source);
    response = vXml.selectSingleNode("//root").cloneNode(true);

    refreshTab(source);
  	window.setTimeout(adjPosition,100);
  }
  function getAffectCount(){
  	return _affectCount;
  }
  function getSelectXml(){
  	  	var refreshXML="<?xml version='1.0' encoding='GBK'?><root><msg/><error/><record><total/></record><activeHead/><thead/><tbody>";
        
        var data = element.getElementsByTagName('tbody')[1];
    		var trs = data.getElementsByTagName("tr");    
 		    
			  for (var i=0; i<trs.length; i++){      
			    var inputs =  trs[i].getElementsByTagName("input")
				    for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			          
			           var tds= trs[i].getElementsByTagName("td");
			           var resultXML ="<tr>" ;
			           if(inputs[j].value!=""){
			              resultXML = resultXML + "<pk>"+inputs[j].value+"</pk>";
			           }
			  
			           for(x=1;x<tds.length;x++){
			               resultXML = resultXML + "<td>"+trs[i].cells[x].innerText+"</td>";
			           }
			           
			           refreshXML = refreshXML + resultXML +"</tr>";  
			           
			        }
				    }
				  }
			   refreshXML = refreshXML + "</tbody></root>";
  
         return refreshXML;
  }
  /// order table LZK ADD BEGIN
  function sortTableTrSetCursor(thIndex){
  	if(element.orderby=="false")
  		return ;
  	var table=element.document.getElementById("_mainDataTable");
  	if (trim(element.fixCol) != ""&&element.document.getElementById("_mainTableHead_")){
  		table=element.document.getElementById("_mainTableHead_");
  	}
  	var ths=table.getElementsByTagName("th");
  	if(ths.length==0)
  		return ;
  	var th;
  	for(var i=0;i<ths.length;i++){
  		th=ths[i];
  		var tdIndex=th.cellIndex;
		if(th.getAttribute("dataIndex")!=null)
			tdIndex=th.getAttribute("dataIndex");

  		th["_orderTh"]=false;
  		if(th.parentNode.parentNode.childNodes.length!=th.parentNode.rowIndex+th.rowSpan)
  			continue;
  		if(th.getElementsByTagName("input").length>0)
  			continue;
  		var text=th.innerText;
  		th.innerHTML=th.innerText+sortTableTrDefaultSpace;
  		th["_orderTh"]=true;
  		th.style.cursor="hand";
  		if(tdIndex==thIndex){
	  		if(element["_orderBy"]=="desc")
	  			th.innerHTML="<table BORDER=0 CELLPADDING=0 CELLSPACING=0 width='100%' height='98%'><tr  noWrap='true'><td align='center' noWrap='true' style='color: #FFFFFF; font-weight: bold;  font-size: 13px;' _innerOrderTd='1' >"+text+"</td><td width=10  _innerOrderTd='1' ><img  _innerOrderTd='1'  src='"+window.prefix+"/base/themes/blue/images/button/down1.gif' width='10' height='9'></td></tr></table>";//"<span style='font-family: Wingdings 3;color:red'>q</span>";//"<img src='"+window.prefix+"/base/themes/blue/images/button/down1.gif' width='10' height='9'>";//
	  		else
	  			th.innerHTML="<table BORDER=0 CELLPADDING=0 CELLSPACING=0 width='100%' height='98%'><tr noWrap='true'><td align='center' noWrap='true' style='color: #FFFFFF; font-weight: bold;  font-size: 13px;'  _innerOrderTd='1' >"+text+"</td><td width=10  _innerOrderTd='1' ><img  _innerOrderTd='1'  src='"+window.prefix+"/base/themes/blue/images/button/up1.gif' width='10' height='9'></td></tr></table>";//"<span style='font-family: Wingdings 3;color:red'>p</span>";//"<img src='"+window.prefix+"/base/themes/blue/images/button/up1.gif' width='10' height='9'>";//
  		}
  	}
  	element["_oldOrderThIndex"]=thIndex;
  }
  var sortTableTrDefaultSpace="<img style='visibility:hidden;' src='"+window.prefix+"/base/themes/blue/images/button/up1.gif' width='10' height='9'>";//"<span style='visibility:hidden;font-family: Wingdings 3;color:red'>q</span>";
  function sortTableTr(){
  	if(element.orderby=="false")
  		return ;
  	if(typeof(event.srcElement.tagName)=="undefined")
  		return ;
  	var th=event.srcElement;
  	if(event.srcElement.tagName.toLowerCase()=="td"){
  		th=sortTableTrGetTh(th);
  		if(th==null)
  			return ;
  	}
  	if(typeof(th["_orderTh"])=="undefined"||th["_orderTh"]==false)
  		return ;
	var tdIndex=th.cellIndex;
	if(th.getAttribute("dataIndex")!=null)
		tdIndex=th.getAttribute("dataIndex");
	
	var orderBy=element["_orderBy"];
	
  	var orderText="";
  	if(typeof(orderBy)=="undefined"||element["_oldOrderThIndex"]!=tdIndex)
		orderBy="desc";
	if(orderBy=="asc"){
		orderBy="desc";
	}else{
		orderBy="asc";
	}
	element["_orderBy"]=orderBy;
	var table= new ActiveXObject("Microsoft.XMLDOM");
    table.loadXML(element.serverXml);
  	var tbodys=table.getElementsByTagName("tbody");
  	if(tbodys.length==0)
  		return ;
  	var trs=tbodys[0].getElementsByTagName("tr");
  	if(trs.length==0)
  		return ;

  	var minIndex=element.orderby.split(":")[0];
  	var maxIndex=trs.length-element.orderby.split(":")[1]-1;
  	if(maxIndex<1)
  		return ;
  	///trs,tdIndex,minIndex,maxIndex
  	var trArray=new Array();
  	for(var i=0;i<maxIndex-minIndex+1;i++){
  		trArray[i]=trs[parseInt(i)+parseInt(minIndex)];
  	}
	var sortFunction=sortTableTrByNum;

  	trArray.sort(
  		function(a,b){
  			return sortFunction(a.childNodes[tdIndex].text,b.childNodes[tdIndex].text,orderBy);
  		}
  	);
  	for(var i=0;i<trArray.length;i++){
  		if(trs.length-1==maxIndex){
  			tbodys[0].removeChild(trArray[i]);
  			tbodys[0].appendChild(trArray[i]);
  		}
  		else
  			tbodys[0].insertBefore(trArray[i],tbodys[0].childNodes[maxIndex+1]);
  	}
  	setDataXml(table.xml);
  	sortTableTrSetCursor(tdIndex);
  }
  function sortTableTrGetTh(th){
  	try{
	  	if(typeof(th["_innerOrderTd"])!="undefined"){
	  		while(th.tagName.toLowerCase()!="th")
	  			th=th.parentNode;
	  		return th;
	  	}else
	  		return null;
  	}catch(e){
  		return null;
  	}
  }
  function sortTableTrByText(a,b,orderBy){
  	var at=a;
  	var bt=b;
  	if(orderBy=="desc"){
  		at=b;
  		bt=a;
  	}
  	if(at>bt)
		return 1;
	else if(at<bt)
		return -1;
	else
		return 0;
  }
  function sortTableTrByNum(a,b,orderBy){
  	var at=a;
  	var bt=b;
  	if(orderBy=="desc"){
  		at=b;
  		bt=a;
  	}
  	if(isNaN(parseFloat(at))||isNaN(parseFloat(bt)))
  		return sortTableTrByText(a,b,orderBy);
  	if(parseFloat(at)>parseFloat(bt))
		return 1;
	else if(parseFloat(at)<parseFloat(bt))
		return -1;
	else
		return 0;
  }
  /// order table LZK ADD END


  	jhtc_attr_init(element,"bottomFix","0");
  	jhtc_attr_init(element,"rightFix","0");
  	jhtc_attr_init(element,"choose",null);
  	jhtc_attr_init(element,"checkflag",null);
  	jhtc_attr_init(element,"remove",null);
  	jhtc_attr_init(element,"remove_display",null);
  	jhtc_attr_init(element,"update",null);
  	jhtc_attr_init(element,"dialogWidth",null);
  	jhtc_attr_init(element,"dialogHeight",null);
  	jhtc_attr_init(element,"serverXml","");
  	jhtc_attr_init(element,"onrefresh","");
  	jhtc_attr_init(element,"onafterdel","");
  	jhtc_attr_init(element,"openPage","update.html");
  	jhtc_attr_init(element,"fixCol","1");
  	jhtc_attr_init(element,"activeHeads","");
  	jhtc_attr_init(element,"headXsl","false");
  	jhtc_attr_init(element,"orderby","false");
  	jhtc_attr_init(element,"xslFile","");
  	jhtc_attr_init(element,"keepHead","false");
  	jhtc_attr_init(element,"xslFileNonFirestPage","");
  	jhtc_attr_init(element,"count_page","18");
  	jhtc_attr_init(element,"is_alterpage","true");
  	jhtc_attr_init(element,"is_autofocus","true");
  	jhtc_attr_init(element,"expression","");
  	
  	__jhtcBindPropertyChange(element,"activeHeads",setActiveHeads);
  	__jhtcBindPropertyChange(element,"expression",setExpression);
	element.checkChange=checkChange;
	element.refresh=refresh;
	element.refreshTab=refreshTab;
	element.changeTabData=changeTabData;
	element.submit=submit;
	element.submit_choose=submit_choose;
	element.submit_choose_base=submit_choose_base;
	element.submit_modify=submit_modify;
	element.checksubmit=checksubmit;
	element.checkFormula=checkFormula;
	element.getTabData=getTabData;
	element.getTabDataByXmlNodeNoGroup=getTabDataByXmlNodeNoGroup;
	element.getTabDataForChart=getTabDataForChart;
	element.getTabDataForChart_comp=getTabDataForChart_comp;
	element.getTabDataForPLChart=getTabDataForPLChart;
	element.getRows=getRows;
	element.getSumSelectData=getSumSelectData;
	element.getSelectData=getSelectData;
	element.adjPosition=adjPosition;
	element.assemble=assemble;
	element.setDataXml=setDataXml;
	element.getSelectXml=getSelectXml;
	element.getAffectCount=getAffectCount;
	element.init=init;
	if(element.activeHeads !=null && element.activeHeads !=""){
		setActiveHeads();
	}
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_class_map["tableCtna_use"]=jhtc_tableCtna;
