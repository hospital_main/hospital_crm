 // <public:property name="text" get="getText"/>

  //<public:event name="ondblclick" id="evtOnDblClick"/>

function jhtc_multiSelectm(win,jhtc_obj){
	var pageWin=win;
	var element=jhtc_obj;
	function init(){
		var rowHeight=15;
		var selBgColor="blue",unSelBgColor="white";
		var selTextColor="white",unSelTextColor="black",divBgColor=unSelBgColor;
		var oh=element.outerHTML.toLowerCase().replace(/'/g,"\"");
		var temp = oh.indexOf("width=\"") + 7;
		if(temp>7){
			var temp2=oh.indexOf("\"",temp);
			element._width=oh.substring(temp,temp2);
		}
		var w=element._width;
		var h=element.line*rowHeight;
		var n=element.name;
		var v=element.value;
		var change=element.onchange;
		var dbclick=element.ondblclick;
		var para=element.para;
		var labelStrBegin="",labelStrEnd="";
		var label="";
		if(element.label!=null&&element.label!=""){
			var lableLength = 120
		    	var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
		    	if(spanWidth<lableLength)
		    		spanWidth = lableLength;
			labelStrBegin="<table cellspacing='0' cellpadding='0'><tr><td width=\""+spanWidth+"\" align=\"right\" valign=\"top\">"+element.label+"：</td><td>";
			labelStrEnd="</td></tr></table>";
			label=element.label;
		}
		if ("true"== trim(element.required)) {
			divBgColor=JHTC_COLOR.REQUIRED;
		}
		var htmlStr=labelStrBegin+"<div id=\""+n+"\" name=\""+n+"\" class=\"inputMultiSelectStyle\" style=\"background:"+divBgColor+";width:"+w+";height:"+h+"\"";
		htmlStr+=" load=\""+element.load+"\" ";
		htmlStr+=" para=\"\" ";
		htmlStr+=" code=\""+element.code+"\" ";
		htmlStr+=" isMultiSelect=\""+element.isMultiSelect+"\" ";
		htmlStr+=" required=\""+element.required+"\" ";
		htmlStr+=" display=\""+element.display+"\" ";
		htmlStr+=" maxselect=\""+element.maxselect+"\" ";
		htmlStr+=" text=\"\" ";
		htmlStr+=" value=\"\" >";
		
		//htmlStr+="</div>"+labelStrEnd+"<input name=\""+n+"_input\" type='text' style=\"display:none\"/>";
		//----- HJJ 修改 增加input的样式，为了在lineCtn.js里调用reset方法的时候赋值用 -----
		htmlStr+="</div>"+labelStrEnd+"<input name=\""+n+"_input\" type='text' class=\"inputMultiSelectStyle\" style=\"display:none\"/>";
		
		jhtcSetHtml(element.parentNode,htmlStr);
		jQuery("#"+n).attr("para",para);
		jQuery("#"+n).attr("value",v);
		jQuery("#"+n).attr("label",label);
		
		var obj=window.document.getElementById(n);
		var inp=window.document.getElementById(n+"_input");
		obj.inputObj=inp;
		inp.tableObj=obj;
		obj.onchange=change;
		obj.ondbclick=dbclick;
		inp.value=v;
		obj.setValue=function(v){
			this.inputObj.setValue(v);
		}
		obj.check=function(){
			if(this.required=="true"&&this.inputObj.value==""){
				if(this.label!="null")
					alert(this.label+"必输");
				return false;
			}
			return true;
		}
		obj.refresh=function(){
			var tempValue=","+this.inputObj.value+",";
			if (trim(this.para)==""||this.para=="null") {
				loadXML=window.getDict(this.load);
			} else {
				loadXML=window.getDict(this.load,this.para);
			}
			var htmlStr="<table cellspacing='0' cellpadding='0' style='cursor:default;width:100%;'>";
			var paras=loadXML.getElementsByTagName("para"),text,code,sel;
			for(var i=0;i<paras.length;i++){
				code=paras[i].getAttribute("code");
				text=paras[i].getAttribute("value");
				if(i==0){
					this.firstCode=code;
					this.firstText=text;
				}
				if(this.code=="true")
					text=code+"　"+text;
				else
					text=text;
				if(tempValue.indexOf(",'"+code+"',")>=0)
					sel=" style=\"background-color:"+selBgColor+";color:"+selTextColor+"\" isSel=\"true\" ";
				else
					sel=" style=\"color:"+unSelTextColor+"\" isSel=\"false\" ";
				htmlStr+="<tr><td code=\""+paras[i].getAttribute("code")+"\" height=\""+rowHeight+"\" "+sel+" noWrap='true'>"+text+"</td></tr>";
			}
			htmlStr+="</table>";
			//this.innerHTML=htmlStr;
			
			jhtcSetHtml(this,htmlStr);

			var tds=this.getElementsByTagName("td");
			for(var i=0;i<tds.length;i++){
				tds[i].inputObj=this.inputObj;
				tds[i].setSelStyle=function(sel){
					if(sel==false){
						this.isSel="false";
						this.style["background"]=divBgColor;
						this.style["color"]=unSelTextColor;
					}else{
						this.isSel="true";
						this.style["background"]=selBgColor;
						this.style["color"]=selTextColor;
					}
				}
				tds[i].changeSelStyle=function(){
					if(this.isSel=="true"){
						this.setSelStyle(false);
					}else{
						this.setSelStyle(true);
					}
				}
				tds[i].ondblclick=function(){
					var v="'"+this.code+"'";
					var text="'"+this.innerHTML+"'";
					this.inputObj.value=v;
					this.inputObj.tableObj.value=v;
					this.inputObj.text=text;
					this.inputObj.tableObj.text=text;
					if(this.inputObj.tableObj.ondbclick!=null)
						this.inputObj.tableObj.ondbclick();
				}
				tds[i].onclick=function(){
					this.changeSelStyle();
					
					this.parentNode.parentNode.lastTd=this;
					var inTds;
					if(this.parentNode.parentNode.mytds)
						inTds=mytds;
					else
						inTds=this.parentNode.parentNode.getElementsByTagName("td");
					var v="";
					var text="";
					for(var m=0;m<inTds.length;m++){
						if(this.inputObj.tableObj.isMultiSelect!="true"&&inTds[m]!=this){
							inTds[m].setSelStyle(false);
							continue;
						}	
						if(inTds[m].isSel=="true"){
							if(v==""){
								v="'"+inTds[m].code+"'";
								text="'"+inTds[m].innerHTML+"'";	
							}else{
								v+=",'"+inTds[m].code+"'";
								text=",'"+inTds[m].innerHTML+"'";	
							}
						}
					}
					this.inputObj.value=v;
					this.inputObj.tableObj.value=v;
					this.inputObj.text=text;
					this.inputObj.tableObj.text=text;
					if(this.inputObj.tableObj.onchange!=null)
						this.inputObj.tableObj.onchange();
				}
			}
		}
		inp.setValue=function(v){
			this.value=v;
			this.tableObj.value=v;
			var tempValue=","+v+",";
			var tds=this.tableObj.getElementsByTagName("td");
			for(var i=0;i<tds.length;i++){
				if(tempValue.indexOf(",'"+tds[i].code+"',")>=0)
					tds[i].setSelStyle(true);
				else
					tds[i].setSelStyle(false);
			}
		}
		inp.check=function(){
			return this.tableObj.check();
		}
		obj.refresh();
	}
	
  	jhtc_attr_init(element,"label",null);
  	jhtc_attr_init(element,"load",null);
  	jhtc_attr_init(element,"para","");
  	jhtc_attr_init(element,"code","true");
  	jhtc_attr_init(element,"_width","130");
  	jhtc_attr_init(element,"isMultiSelect",null);
  	jhtc_attr_init(element,"required","false");
  	jhtc_attr_init(element,"line","3");
  	jhtc_attr_init(element,"display","true");
  	jhtc_attr_init(element,"maxselect","8");
  	jhtc_attr_init(element,"text","");


	//element.setValue=setValue;
	//element.check=check;
	//element.refresh=refresh;
	element.init=init;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_class_map["inputMultiSelectm"]=jhtc_multiSelectm;