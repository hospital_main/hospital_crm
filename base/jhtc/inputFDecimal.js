function jhtc_inputFDecimal(win,jhtc_obj){
		var pageWin=win;
		var element=jhtc_obj;
		var textFPairObj=null;
		var proxyLabel=null;
		function init() {
			if(element.label=="preTd"){
				var preTd=element.parentNode;
				while(preTd!=null){
					if(preTd.tagName.toLowerCase()=="td"){
						if(preTd.previousSibling!=null){
							proxyLabel=preTd.previousSibling.innerText;
							if(proxyLabel.indexOf(":")>0)
								proxyLabel=proxyLabel.split(":")[0];
							if(proxyLabel.indexOf("：")>0)
								proxyLabel=proxyLabel.split("：")[0];
						}
						break;
					}
					preTd=preTd.parentNode;
				}
				element.label=null;
			}else if(element.label=="preText"){
				var preTd=element.parentNode;
				var preT=preTd.firstChild;
				if(preT!=element)
					proxyLabel=preT.nodeValue.replace("：","").replace(":","");
				element.label=null;
			}else if(element.label!=null)
				proxyLabel=element.label;
			element.htcLabel=proxyLabel==null?"":proxyLabel;
			
			if (element.label != null) {
				// -- label 的基本长度
				var lableLength = 120
				var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
				if (spanWidth < lableLength)
					spanWidth = lableLength;
				
				var aKey = ""
				if (window.trim(accessKey) != '' && !element.readOnly)
					aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)"
				element.insertAdjacentHTML("beforeBegin", "<span nowrap style='text-align:right;width:"+spanWidth+"px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";font-weight:normal;'>"+element.label+aKey+"：</span>");
			}
			if(element.parentNode==null)
				return;
			element.parentNode.noWrap = true;
			
			if (element.readOnly) {
				element.style.backgroundColor='#EDEDED'
				element.style.fontWeight = 'bold'
			} else if ('true'==element.required) {
				element.style.backgroundColor='#DBFCFF'
			}
			element.onkeydown=alterEnter;
			element.onkeypress = ignoreIllegal;
			element.onkeyup = ignorePoint;
			element.onblur=doblur;
		}
		function alterEnter(){
		if(event.keyCode==13){
			event.keyCode=9;
		}
		}
		//阻止输入域输入数字和 '.+-'之外的字符
		function ignoreIllegal() {
			if (((event.keyCode>=48) && (event.keyCode<=57))||event.keyCode==46||event.keyCode==43||event.keyCode==45) {
				return true;
			} else {
				return false;
			}
		}
		//阻止输入小数位多于point位
		function ignorePoint() {
		if (element.value.indexOf(".")!=-1 && element.value.length -element.value.indexOf(".")-1 > element.point) {
			element.value = element.value.substring(0,element.value.indexOf(".")+(1+parseInt(element.point)))
		}
		}
		function doblur(){
			element.value =formateNumber(element.value.replace(/,/g,""));
		}
		
		// 刷新页面
		function refresh() {
		if (trim(element.required.toLowerCase())=="true") {
			element.style.backgroundColor="#DBFCFF";
		} else {	
			element.style.backgroundColor="#FFFFFF";
		}
		}
		
		function check() {
			ev=element.value.replace(/,/g,"");
			if ('true'==element.required) {
				if (window.trim(ev) == '')  {
					if (proxyLabel != null) {
						alert(proxyLabel+"不能为空！")
					}else{
						alert("请输入必输项！");
					}
					element.focus()
					element.select()
					return false
				}
			}
			
			if (element.min!=null) {
				if (isNaN(element.min)) {
					alert("属性min应为数字")
					return false;
				}
				
				if (isNaN(ev)) {
					if (proxyLabel != null) {
						alert(proxyLabel+"应为数字")
					}
					element.focus()
					element.select()
					return false
				}
				
				if (parseFloat(ev)<element.min) {
					if (proxyLabel != null) {
						alert(proxyLabel+"应大于"+element.min)
					}
					element.focus()
					element.select()
					return false
				}
			}
			
			if (element.max!=null) {
				if (isNaN(element.max)) {
					alert("属性max应为数字")
					return false;
				}
				
				if (isNaN(ev)) {
					if (proxyLabel != null) {
						alert(proxyLabel+"应为数字")
					}
					element.focus()
					element.select()
					return false
				}
				
				if (parseFloat(ev)>element.max) {
					if (proxyLabel != null) {
						alert(proxyLabel+"应小于"+element.max)
					}
					element.focus()
					element.select()
					return false
				}
			}
			if (isNaN(ev)) {
				if (proxyLabel != null) {
					alert(proxyLabel+"应为数字")
				} else {
					alert("该文本框输入应为数字")
				}
				element.focus()
				element.select()
				return false
			}
			return true;
		}
		function formateNumber(v){
			v=v.replace(/,/g,"");
			if(v.length==0)
				return "";
			hasPoint=false;
			if(v.indexOf(".")>0){
				v=v+"0000000000000000000000000000000";
				if(element.point==0)
					v=v.substr(0,v.indexOf("."));
				else
					v=v.substr(0,v.indexOf("."))+"."+v.substr(v.indexOf(".")+1,element.point);
			}
			if(element.point==0)
				hasPoint=true;
			str="";
			j=1;
			prex="";
			v=v.replace(/\./g,"");
			if(v.substr(0,1)=="-"||v.substr(0,1)=="+"){
				prex=v.substr(0,1);
				v=v.substr(1);	
			}
			for(var i=0;i<v.length;i++){
				if(hasPoint==false){
					str=v.substr(v.length-i-1,1)+str;
					if(i==element.point-1){
						str="."+str;
						hasPoint=true;
					}
				}else{
					str=v.substr(v.length-i-1,1)+str;
					if(j%3==0&&i<v.length-1)
						str=","+str;
					j++;
				}
			}
			return prex+str;
		}
		function setText(){
			return element.text = element.value;
		}
		function setValue(v){
			if(isNaN(v))
				element.value="";
			else
				element.value=formateNumber(v.replace(/,/g,""));
		}
		function getValue(v){
			if(check())
				return element.value.replace(/,/g,"");
			return "";
		}
		function setDisabled(mboole){
			element.disabled = (mboole.toString().toLowerCase() == 'true') || (mboole.toString().toLowerCase() == '1') ? true : false;
		}


	__jhtcBindPropertyChange(element,"value",setText);
  	jhtc_attr_init(element,"label",null);
  	jhtc_attr_init(element,"required","false");
  	jhtc_attr_init(element,"extent","140");
  	jhtc_attr_init(element,"min",null);
  	jhtc_attr_init(element,"max",null);
  	jhtc_attr_init(element,"point","2");
  	jhtc_attr_init(element,"text","");
  	
	element.check=check;
	element.getValue=getValue;
	element.setValue=setValue;
	element.setDisabled=setDisabled;
	element.init=init;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_class_map["inputFDecimal"]=jhtc_inputFDecimal;