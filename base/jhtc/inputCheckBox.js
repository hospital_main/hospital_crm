function jhtc_inputCheckBox(win,jhtc_obj){
	return jhtc_inputCheckBox_c(__jhtcElementFun(jhtc_obj));
}	
function jhtc_inputCheckBox_c(_e){
	var serialNameID;
	function init() {
		var element=_e();
		element.style.display = "none";
		if (trim(element.name)=="") {
			alert("checkBox没有指定name！");
			return;
		}
		
		var booleanDisabled = false;
		if(element.disabled && element.disabled.toString() == "true"){
			booleanDisabled = true;
		}
		
		var objXMLDom = getXMLDocumentInst();
		if(element.load != null){
			objXMLDom.loadXML(element.load);
			if(objXMLDom && objXMLDom.xml != ""){
				var objCheckBoxOptionNode;
				var objCheckBoxOptionSpan;
				var objLabelSpan = window.document.createElement("<span style='font-family:" + element.currentStyle.fontFamily + ";font-size:" + element.currentStyle.fontSize + ";'>");
				if(element.labelFix=="true"){
					objLabelSpan = window.document.createElement("<span title='" + element.label + "' nowrap style='text-align:right;width:120px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";white-space: nowrap;text-overflow:ellipsis; overflow:hidden;'>"+element.label+(element.label==''?"":"：")+"</span>");
				}
				if (element.label != null && element.label != "") {
				  	objLabelSpan.innerText = element.label + "：";
				} else {
				  	objLabelSpan.innerText = "";
				}
				element.insertAdjacentElement("beforeBegin",objLabelSpan);
				var objItemList = objXMLDom.selectNodes("//item");
				var strInputCheckBoxVal = "";
				serialNameID = "yyyyyyyxxxxxx" + (Math.random()*10).toString();//产生checkBox的nameID
				for(var i=0;i<objItemList.length;i++){
					//if(objItemList[i].getAttribute("selected") && objItemList[i].getAttribute("selected").toString() == "true"){
					if(objItemList[i].getAttribute("value") == "1"){
						objCheckBoxOptionNode = window.document.createElement("<input type='checkbox' name='" + serialNameID + "' value='" + objItemList[i].getAttribute("value") + "' checked>");
						strInputCheckBoxVal += objItemList[i].getAttribute("value") + ",";
					} else {
						objCheckBoxOptionNode = window.document.createElement("<input type='checkbox' name='" + serialNameID + "' value='" + objItemList[i].getAttribute("value") + "'>");
					}
					
					objCheckBoxOptionNode.disabled = booleanDisabled?true:false;
					objCheckBoxOptionSpan = window.document.createElement("<span style='font-family:" + element.currentStyle.fontFamily + ";font-size:" + element.currentStyle.fontSize + ";'>");
					objCheckBoxOptionSpan.innerText = objItemList[i].getAttribute("name") + " ";
					objCheckBoxOptionNode.onclick = setElementVal;
					element.insertAdjacentElement("beforeBegin",objCheckBoxOptionNode);
					element.insertAdjacentElement("beforeBegin",objCheckBoxOptionSpan);
				}
				element.value = strInputCheckBoxVal.substring(0,strInputCheckBoxVal.length - 1);
				if(element.value == "") element.value = '0';
			}else{// load失败
				alert("load属性值设置错误");
			}
		}else{// 未指定load属性或未指定label属性
			alert("请指定load属性");
		}
	}
	
	function setElementVal(){// 设置element.value
		var element=_e();
		var strCheckBoxSecVal = "";
		var objSrcElementName = event.srcElement.name;
		var objCBList = document.all(objSrcElementName);
		if(!objCBList) return;
		if(objCBList.length){// 多个checkBox
			for(var i=0;i<objCBList.length;i++){
				if(objCBList[i].checked){
					strCheckBoxSecVal += objCBList[i].value + ",";
				}
			}
			element.value = strCheckBoxSecVal.substring(0,strCheckBoxSecVal.length - 1);
		} else {// 单个checkBox
			element.value = (objCBList.checked)?"1":"0";
		}
		__jhtcDispatchEvent(element,"onchange");
	}
	
	function setValue(arg){
		var element=_e();
		element.value = arg;
		if(typeof(serialNameID) != "undefined" && document.all(serialNameID)){
			document.all(serialNameID).checked = (arg  == "1") ? true : false;
		}
	}
	function setDisabled(arg){
		var element=_e();
		if(typeof(serialNameID) != "undefined" && document.all(serialNameID)){
			document.all(serialNameID).disabled=arg;
		}
		if(element.value=="true"||element.value=="false")
			element.value=(arg==true?"1":"0");
	}
	function check(){
		return true;
	}
	
	function getXMLDocumentInst(){//获取一个XML DOM实例
		var objXMLDom;
		objXMLDom = new ActiveXObject("MSXML2.DOMDocument");
		objXMLDom.async = false;
		objXMLDom.setProperty("ServerHTTPRequest",true);
		objXMLDom.setProperty("SelectionLanguage", "XPath");
		return objXMLDom;
	}
	function _initAttr(){
		var element=_e();
		jhtc_attr_init(element,"label",null);
		jhtc_attr_init(element,"required",null);
		jhtc_attr_init(element,"load",null);
		jhtc_attr_init(element,"readonly",null);
		jhtc_attr_init(element,"disabled",null);
		jhtc_attr_init(element,"labelFix","false");
		
		element.setDisabled=setDisabled;
		element.setValue=setValue;
		element.check=check;
		element.init=init;
	}
	_initAttr();
	_e().jhtcInit=function(){
		_e().init();
	};
	
	return null;
};
jhtc_class_map["inputCheckBox"]=jhtc_inputCheckBox;