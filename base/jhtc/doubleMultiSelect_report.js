function jhtc_doubleMultiSelect_report(win,jhtc_obj){
  var pageWin=win;
  var element=jhtc_obj;
  var objXMLDom = null;
  var objLeftList = null;
  var objRightList = null;
  var objExtendListDiv = null;
  var objArrCurCheckItem = new Array();
  var objExtendContainersDiv = null;

  var intDoubleListMultiSelectWidth = 260;
  var intDoubleListMultiSelectHeight = 3;
  var intExtendListWidth = 260;
  var intExtendListHeight = 105;
  var intBorderWeight = 0;
  var strBorderColor = "#BDD9F7";

  var intDoubleListMultiSelectButtonListWidth = 30;
  var intDoubleListMultiSelectListWidth = 0;//

   var iframe=null;

  function initShape(){// initialize doubleListMultiSelect

	if ( element.width ){ //width
      intDoubleListMultiSelectWidth = parseInt(element.width);
      intExtendListWidth = parseInt(element.width);
    }
    if ( element.line ){ //doubleListMultiSelect -> height
      intDoubleListMultiSelectHeight = parseInt(element.line);
    }
    if ( element.eListHeight ){ //doubleListMultiSelect -> ExtendList -> height
      intExtendListHeight = parseInt(element.eListHeight);
    }

    if ( element.borderWeight ){ //doubleListMultiSelect -> ExtendList -> borderWeight
      intBorderWeight = element.borderWeight;
    }

    if ( element.borderColor ){ //doubleListMultiSelect -> ExtendList -> borderColor
      strBorderColor = element.borderColor;
    }

    intDoubleListMultiSelectListWidth = parseInt(( intDoubleListMultiSelectWidth - intDoubleListMultiSelectButtonListWidth ) / 2);
  }

	function getDict_html(in_load,in_para,in_code){

	  if (in_para != null) {
				xmlhttp.post("global_select_html_report", in_para, "?selectID="+in_load+","+in_code);
			}
			else
				xmlhttp.post("global_select_html_report", '', "?selectID="+in_load+","+in_code);

			if (window.doMsg(xmlhttp._object.responseText)) {

				return xmlhttp._object.responseText;

			}
	}

    function htmlToXML(fragment){
        if(fragment == null)
            return;

        var tds = fragment.getElementsByTagName("TD");
        var xmlStr = "<root>";
        for(var i = 1; i < tds.length; i++){
            var para = "<para code='"+tds[i].getAttribute("value")+"' value = '"+tds[i].firstChild.nodeValue+"' />";
            xmlStr += para;
        }

        xmlStr += "</root>";

        return xmlStr;
    }

  function init(){
  	if(element.load == null || element.load == ""){
  		alert("请设置doubleListMultiSelect的load属性");
  		return;
  	} else {
      var loadXML = "";

        var elem_code;
        if(trim(this.code)=="true")
        {
          elem_code="true";
        }else{elem_code="false"}

        if (trim(element.para)=="") {
          var div = window.document.createElement("DIV");
          div.innerHTML = getDict_html(element.load,'',elem_code).split("*")[1];
          loadXML =  htmlToXML(div);
        } else {
           var div = window.document.createElement("DIV");
          div.innerHTML = getDict_html(element.load,element.para,elem_code).split("*")[1];
          loadXML =  htmlToXML(div);
        }

  		element.load = '<root><para code="DT001" value="条目一"/><para code="DT002" value="条目二"/><para code="DT003" value="条目三"/><para code="DT004" value="条目四"/><para code="DT005" value="条目五"/><para code="DT006" value="条目六"/><para code="DT007" value="条目七"/><para code="DT008" value="条目八"/></root>';
  		objXMLDom = inputXML(loadXML);
    }

	  if (element.name == ''){
      alert("请设置doubleListMultiSelect的name属性")
      return;
    }

    initShape();

    if (element.label != null) {
  		element.style.visibility = "hidden";
  		var objContainersTable = window.document.createElement("<table style='border:solid " + intBorderWeight + "px " + strBorderColor + "'>");
  		objContainersTable.width = intDoubleListMultiSelectWidth;
  		objContainersTable.oncontextmenu = function(){event.returnValue = false;};

  		var objTitleTR = objContainersTable.insertRow();
  		//var objTitleTD = objTitleTR.insertCell();
  		//objTitleTD.colSpan = 3;
  		//if (label != "") {
  		//  objTitleTD.innerText = label + "：";
  		//}

  		var objMainTR = objContainersTable.insertRow();
  		var objMainLeftTD = objMainTR.insertCell();
  		objMainLeftTD.style.width = intDoubleListMultiSelectListWidth;
  		objMainLeftTD.innerHTML = getLeftListInnerHTML();
  		////////////////////////////////////////////////////
  		objLeftList = objMainLeftTD.firstChild;
  		objLeftList.ondblclick = processListDblClickEvent;

  		var objMainCenterTD = objMainTR.insertCell();
  		objMainCenterTD.style.width = intDoubleListMultiSelectButtonListWidth;
  		objMainCenterTD.vAlign = "middle";
  		objMainCenterTD.innerHTML = getCenterListInnerHTML();
  		var objBtnList = objMainCenterTD.all.tags("textbox");
  		for( var i =0; i < objMainCenterTD.all.tags("textbox").length; i ++ ){
  			objBtnList[i].onclick = processBtnClickEvent;
  		}
  		objBtnList = objMainCenterTD.all.tags("roundrect");
  		for( var i =0; i < objBtnList.length; i ++ ){
  			objBtnList[i].onclick = processBtnClickEvent;
  		}
  		objBtnList = objMainCenterTD.all.tags("polyline");

  		for( var i =0; i < objBtnList.length; i ++ ){
  			objBtnList[i].onclick = processBtnClickEvent;
  		}
  		var objMainRightTD = objMainTR.insertCell();
  		objMainRightTD.style.width = intDoubleListMultiSelectListWidth;
  		objMainRightTD.innerHTML = getRightListInnerHTML();
  		objRightList = objMainRightTD.firstChild;
  		objRightList.ondblclick = processListDblClickEvent;

  		//////////////////////////////////////////////////////////////////

  		objExtendContainersDiv = window.document.createElement("<div style='position:absolute;border:solid " + intBorderWeight + "px " + strBorderColor + ";'>");
//  		window.document.body.removeChild( objExtendContainersDiv );
//  		objExtendContainersDiv = window.document.body.appendChild( objExtendContainersDiv );
  		element.parentNode.appendChild(objExtendContainersDiv);
//  		alert(element.parentNode.innerHTML);

  		objExtendContainersDiv.innerHTML = getExtendListInnerHTML();
  		objExtendListDiv = objExtendContainersDiv.firstChild;
  		objExtendContainersDiv.oncontextmenu = function(){event.returnValue = false;};
  		var objCheckBoxList = objExtendListDiv.all.tags("input");
  		for( var i = 0; i < objCheckBoxList.length; i ++ ){
  			if( objCheckBoxList[i].type.toLowerCase() == "checkbox" ){
  				objCheckBoxList[i].onclick = processCheckBoxClickEvent;
  			}
  		}

  		element.insertAdjacentElement("beforeBegin",objContainersTable);
  		objExtendContainersDiv.style.height = intExtendListHeight;
  		objExtendContainersDiv.style.width = (intBorderWeight != 0) ? intDoubleListMultiSelectWidth + intBorderWeight*2 + 5 : intDoubleListMultiSelectWidth - 1;
  		objExtendContainersDiv.style.visibility = "hidden";
  		if( element.display && element.display == "false" ) objContainersTable.style.display = "none";
    }

    iframe=element.document.createElement("iframe");
	with(iframe){
		style.position = "absolute";
		style.display = "none";
		style.textAlign = "center";
		style.backgroundColor = "#F6F6F6";
		className = "ds_font";
		style.zIndex =8;
		style.overflow = "visible";
		border="0";
	}
	iframe = element.parentNode.appendChild(iframe);
	objExtendContainersDiv.style.zIndex =10;
  }

  function setValue(strArg){

	/*
	 *  Input Arguments:"'DT001','DT003','DT006','DT007'"
	 */
	 var objTransArr = new Array();
	 var objTempArr = new Array();
	 var objArrVal = new Array();
	 var objArrVal = strArg.replace(/'/g,"").split(",");

	 for( var i = 0; i < objRightList.children.length; i ++ ){
		objTempArr[objTempArr.length] = objRightList.children[i];
	 }
	 moveingItem( objTempArr,"left","s" );
	 for( var i = 0; i <objArrVal.length; i ++ ){
		for( var j = 0; j < objLeftList.children.length; j ++ ){
			if( objLeftList.children[j].getAttribute("value") == objArrVal[i] ){
				objTransArr[objTransArr.length]	= objLeftList.children[j];
				break;
			}
		}
	 }
	 moveingItem( objTransArr,"right","s" );
  }

  function processListDblClickEvent(){
	switch(event.srcElement.id){
		case "hzh_dms_left_List":
			moveingItem( getSelectedItem(objLeftList),"right","s" );
			break;

		case "hzh_dms_right_List":
			moveingItem( getSelectedItem(objRightList),"left","s" );
			break;
	}
  }

  function processCheckBoxClickEvent(){
	objArrCurCheckItem.pop();
	switch( event.srcElement.checked ){
		case true:
			for( var i = 0; i < objLeftList.children.length; i ++ ){
				if( objLeftList.children[i].getAttribute("value") == event.srcElement.value ){
					objArrCurCheckItem[objArrCurCheckItem.length] = objLeftList.children[i];
					moveingItem( objArrCurCheckItem,"right" );
					break;
				}
			}
			break;
		case false:
			for( var i = 0; i < objRightList.children.length; i ++ ){
				if( objRightList.children[i].getAttribute("value") == event.srcElement.value ){
					objArrCurCheckItem[objArrCurCheckItem.length] = objRightList.children[i];
					moveingItem( objArrCurCheckItem,"left" );
					break;
				}
			}
			break;
	}
  }


  function processBtnClickEvent(){// precess button event
	var objSrcElement = event.srcElement;
	switch(objSrcElement.id){
		case "hzh_dms_left_Btn":// right to left
			moveingItem( getSelectedItem(objRightList),"left","s" );
			break;

		case "hzh_dms_right_Btn":// left to right
			moveingItem( getSelectedItem(objLeftList),"right","s" );
			break;

		case "hzh_dms_down_Btn":

			if(objSrcElement.tagName.toLowerCase() == "roundrect"){
				objSrcElement = objSrcElement.nextSibling;
			}

			if(event.srcElement.tagName.toLowerCase() == "roundrect"){
				objExtendContainersDiv.style.left = event.clientX - event.offsetX  - objLeftList.offsetWidth - 3;
				objExtendContainersDiv.style.top = event.clientY - event.offsetY + event.srcElement.offsetHeight + 5  + ( intDoubleListMultiSelectHeight - 3 )*15;
			}else{
				objExtendContainersDiv.style.left = event.clientX - event.offsetX - 10 - objLeftList.offsetWidth;
				objExtendContainersDiv.style.top = event.clientY - event.offsetY + event.srcElement.offsetHeight + 8  + ( intDoubleListMultiSelectHeight - 3 )*15;
			}

			if( objExtendListDiv.style.visibility == "hidden" ){
				iframe.style.display="";
				objExtendContainersDiv.style.visibility = "visible";
				objExtendListDiv.style.visibility = "visible";
				objSrcElement.fillcolor = "red";
				objSrcElement.strokecolor = "red";
				objSrcElement.points.value = " 0,3 4,0 8,3 0,3";
				objSrcElement.style.top = 7;
			}else{
				iframe.style.display="none";
				objExtendContainersDiv.style.visibility = "hidden";
				objExtendListDiv.style.visibility = "hidden";
				objSrcElement.fillcolor = "#7B9EBD";
				objSrcElement.strokecolor = "#7B9EBD";
				objSrcElement.points.value = " 0,3 4,6 8,3 0,3";
				objSrcElement.style.top = 5;
			}
			iframe.style.left = objExtendContainersDiv.style.left;
			iframe.style.top = objExtendContainersDiv.style.top;
			iframe.style.width = objExtendContainersDiv.style.width;
			iframe.style.height = 126;
			break;
	}
  }

  function moveingItem(objItemList,direction,tagFlag){// move items selected
	 var intRightListItemUID = 0;
	 var intLeftListLength = 0;
	 switch( direction ){
		case "left":// move to left
			if( typeof(tagFlag) != "undefined" ) setCheckBoxStatus ( objItemList,"left" );
			for( var i = 0; i < objItemList.length; i ++ ){
				intLeftListLength = objLeftList.children.length;
				if( intLeftListLength == 0 ){// move to right
					objLeftList.appendChild(objItemList[i]);
					continue;
				}
				intRightListItemUID = parseInt(objItemList[i].getAttribute("uid"));
				for( var j = 0; j < intLeftListLength; j ++ ){
					if( parseInt(objLeftList.children[j].getAttribute("uid")) > intRightListItemUID ){
						objLeftList.children[j].insertAdjacentElement("beforeBegin",objItemList[i]);
						break;
					}else{// process if there are no bigger items
						objLeftList.appendChild(objItemList[i]);
					}
				}
			}
			break;
		case "right":// move to right
			if( typeof(tagFlag) != "undefined" ) setCheckBoxStatus ( objItemList,"right" );
			for( var i = 0 ; i < objItemList.length; i ++ ){
				objItemList[i].removeAttribute("selected");
				objRightList.appendChild(objItemList[i]);
			}
			break;
	 }
	 setItemCode();
  }

  function setCheckBoxStatus(objItemList,direction){// set status of checkbox

	var objCheckBoxList = objExtendListDiv.all.tags("input");
	for( var i = 0; i < objItemList.length; i ++ ){
		var vVal = objItemList[i].getAttribute("value");
		for( var j = 0; j < objCheckBoxList.length; j ++ ){
			if( objCheckBoxList[j].type.toLowerCase() == "checkbox" && objCheckBoxList[j].getAttribute("value") == vVal ){
				switch( direction ){
					case "left":
						objCheckBoxList[j].checked = false;
						break;
					case "right":
						objCheckBoxList[j].checked = true;
						break;
				}
			}
		}
	}
  }


  function getSelectedItem(objList){// return selected items of the list
	 var objSelectedOptionArray = new Array();
	 for( var i = 0; i < objList.children.length; i ++ ){
		if(objList.children[i].selected){
			objSelectedOptionArray[objSelectedOptionArray.length] = objList.children[i];
		}
	 }
	 return objSelectedOptionArray;
  }

  function getLeftListInnerHTML(){// get left list's innerHTML
	var vXSLT = ( element.ifLeftMultiSec == "false" ) ? '<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="xml" version="1.0" encoding="gb2312" indent="yes"/><xsl:template match="/"><select size="' + intDoubleListMultiSelectHeight + '" id="hzh_dms_left_List" style="width:' + intDoubleListMultiSelectListWidth + ';"><xsl:apply-templates select="root"/></select></xsl:template><xsl:template match="para"><option value="{@code}"><xsl:attribute name="uid"><xsl:value-of select="position() - 1"/></xsl:attribute><xsl:value-of select="@value"/></option></xsl:template></xsl:stylesheet>' : '<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="xml" version="1.0" encoding="gb2312" indent="yes"/><xsl:template match="/"><select size="' + intDoubleListMultiSelectHeight + '" multiple="" id="hzh_dms_left_List" style="width:' + intDoubleListMultiSelectListWidth + ';"><xsl:apply-templates select="root"/></select></xsl:template><xsl:template match="para"><option value="{@code}"><xsl:attribute name="uid"><xsl:value-of select="position() - 1"/></xsl:attribute><xsl:value-of select="@value"/></option></xsl:template></xsl:stylesheet>';
	return transformXML(objXMLDom,vXSLT);
  }

  function getCenterListInnerHTML(){// get center list's innerHTML
	var strRightBtn = "<v:group style='height:16px;width:13;' coordsize='14,16'><v:roundrect arcsize='6554f' strokeweight='.75pt' style='position:absolute;top:3;right:550;width:20;height:12;cursor:hand' strokecolor='#4F789D' coordsize='21600,21600' onmouseover='this.fillcolor=\"#eeeeee\"' onmouseout='this.fillcolor=\"#ffffff\"' id='hzh_dms_right_Btn'></v:roundrect><v:textbox id='hzh_dms_right_Btn' style='position:absolute;top:2;right:4;color:#7B9EBD;font-family:arial black;font-size:5pt;font-weight:bold;cursor:hand;zIndex:0'>\>\></v:textbox></v:group>";
	var strLeftBtn = "<v:group style='height:16px;width:13;' coordsize='14,16'><v:roundrect arcsize='6554f' strokeweight='.75pt' style='position:absolute;top:3;right:550;width:20;height:12;cursor:hand' strokecolor='#4F789D' coordsize='21600,21600' onmouseover='this.fillcolor=\"#eeeeee\"' onmouseout='this.fillcolor=\"#ffffff\"'id='hzh_dms_left_Btn'></v:roundrect><v:textbox id='hzh_dms_left_Btn' style='position:absolute;top:2;right:3;color:#7B9EBD;font-family:arial black;font-size:5pt;font-weight:bold;cursor:hand;zIndex:0'>\<\<</v:textbox></v:group>";
	var strDownBtn = "<v:group style='height:16px;width:13;' coordsize='14,16'><v:roundrect arcsize='6554f' strokeweight='.75pt' style='position:absolute;top:3;right:550;width:20;height:12;cursor:hand' strokecolor='#4F789D' coordsize='21600,21600' onmouseover='this.fillcolor=\"#eeeeee\"' onmouseout='this.fillcolor=\"#ffffff\"'id='hzh_dms_down_Btn'></v:roundrect><v:polyline id='hzh_dms_down_Btn' style='position:absolute;left:7;top:5;' points='0,3 4,6 8,3 0,3' fillcolor='#7B9EBD' strokecolor='#7B9EBD'/></v:group>";
	var strReturn = '<table cellpadding="0" cellspacing="0" width="100%" height="100%"><tr><td>' + strRightBtn + '</td></tr><tr><td>' + strLeftBtn + '</td></tr><tr><td>' + strDownBtn + '</td></tr></table>';
	if(element.hasExtendBtn && element.hasExtendBtn.toString() == "no"){
		strReturn = '<table cellpadding="0" cellspacing="0" width="100%" height="100%"><tr><td>' + strRightBtn + '</td></tr><tr><td>' + strLeftBtn + '</td></tr></table>';
	}
	return strReturn;
  }

  function getRightListInnerHTML(){// gete right list's innerHTML
	return '<select id="hzh_dms_right_List" size="' + intDoubleListMultiSelectHeight + '" multiple style="width:' + intDoubleListMultiSelectListWidth + ';"></select>';
  }

  function getExtendListInnerHTML(){// get extend list's innerHTML
	var strExtendTitleColor = "#BDD9F7";
	var strExtendTableColor = "#A7CCF4";
	var strJumpColor = "#E9F2FC";
	var vXSLT = '<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="xml" version="1.0" encoding="gb2312" indent="yes"/><xsl:template match="/"><div style="border:solid 1px ' + strExtendTableColor + ';width:100%;visibility:hidden;"><table cellpadding="0" cellspacing="1" style="width:100%;background:' + strExtendTableColor + '"><tr height="16" align="center"><td bgcolor="' + strExtendTitleColor + '" width="30" style="padding-left:2;font-weight:bold;font-size:9pt;">选择</td><td bgcolor="' + strExtendTitleColor + '" width="' + ( intDoubleListMultiSelectWidth - 30 ) + '" style="padding-left:2;font-weight:bold;font-size:9pt;">名称</td></tr></table><div style="border:solid 0px ' + strExtendTableColor + ';width:100%;height:' + intExtendListHeight + ';overflow:auto"><table bgcolor="' + strExtendTableColor + '" cellpadding="0" cellspacing="1" style="width:100%;"><xsl:apply-templates select="root"/></table></div></div></xsl:template><xsl:template match="para"><tr align="center"><xsl:choose><xsl:when test="position() mod 2 = 0"><td bgcolor="' + strExtendTitleColor + '" width="33"><input type="checkbox" value="{@code}"/></td><td bgcolor="' + strJumpColor + '" width="' + ( intDoubleListMultiSelectWidth - 30 ) + '"><span onmouseover="this.style.cursor=\'hand\'" title="指标代码:{@code}&#13;指标名称:{@value}"><xsl:value-of select="@value"/></span></td></xsl:when><xsl:otherwise><td bgcolor="' + strExtendTitleColor + '" width="33"><input type="checkbox" value="{@code}"/></td><td bgcolor="white" width="' + ( intDoubleListMultiSelectWidth - 30 ) + '"><span onmouseover="this.style.cursor=\'hand\'" title="指标代码:{@code}&#13;指标名称:{@value}"><xsl:value-of select="@value"/></span></td></xsl:otherwise></xsl:choose></tr></xsl:template></xsl:stylesheet>';
	return transformXML(objXMLDom,vXSLT);
  }

  function setItemCode(){ //001,002,003
	var strReturn = "";
	for( var i = 0; i < objRightList.children.length; i ++ ){
		strReturn += objRightList.children[i].value + ",";
	}
	strReturn = strReturn.substr( 0,strReturn.length - 1 );
	return element.value=strReturn;
  }
  ////////////////////////////////
  function inputXML(argSource){

  		var objXMLFragment = null;
  		var vTempSource = "";
  		var objXMLDoc = getXMLDocumentInst();
		try{
			switch(typeof(argSource)){
				case "string":
					if(argSource.search(/\./) != -1 && argSource.search(/\</) == -1){
				  		if(argSource.search(/\+/) != -1) argSource = eval(argSource);
				  		objXMLDoc.async = false;
						objXMLDoc.load(argSource);
						objXMLFragment = objXMLDoc.createDocumentFragment();
						objXMLFragment.appendChild(objXMLDoc.documentElement);
						break;
					}
					if(argSource.search(/\</) != -1){
						argSource = argSource.replace(/xmlns:fo=\"\"/g,"");
						objXMLDoc.loadXML(argSource);
						objXMLFragment = objXMLDoc.createDocumentFragment();
						objXMLFragment.appendChild(objXMLDoc.documentElement);
						break;
					}
					try{
						argSource = argSource.replace(/xmlns:fo=\"\"/g,"");
						objXMLFragment = eval(argSource);
					}catch(e){
					}
					break;

				case "object":
					if(argSource.xml)
						vTempSource = argSource.xml;
						vTempSource = vTempSource.replace(/xmlns:fo=\"\"/g,"");
						objXMLDoc.loadXML(vTempSource);
						objXMLFragment=objXMLDoc.createDocumentFragment();
						objXMLFragment.appendChild(objXMLDoc.documentElement);
					break;
					case "undefined":
					objXMLFragment = objXMLDoc.createDocumentFragment();
					break;
				default:
					objXMLFragment = null;
			}

		}catch(err){
			objXMLFragment = null;
		}
		return objXMLFragment;
	}

	function transformXML(argXML,argXSLT){
      var objXMLFragment,objXSLTFragment;
      var vResult = "";
      var objNode;

      objXMLFragment = inputXML(argXML);
      if(objXMLFragment){
        objXSLTFragment = inputXML(argXSLT);

        if(objXSLTFragment){
          vResult = objXMLFragment.transformNode(objXSLTFragment.childNodes(0));
          while(objXSLTFragment && objXSLTFragment.childNodes(0)){
            objNode = objXSLTFragment.removeChild(objXSLTFragment.childNodes(0));
            objNode = null;
          }
          objXSLTFragment = null;
          delete objXSLTFragment;

  	    }
        while(objXMLFragment && objXMLFragment.childNodes(0)){
          objNode = objXMLFragment.removeChild(objXMLFragment.childNodes(0));
          objNode = null;
        }
        objXMLFragment = null;
        delete objXMLFragment;

      }
      vResult = vResult.replace(/<\?xml[0-9a-zA-Z\s\'\"=\-.\?]{0,}>/g,"");

      return vResult;
    }

    function getXMLDocumentInst(){
		var objReturnXMLDom;
		objReturnXMLDom = new ActiveXObject("MSXML2.DOMDocument");
		objReturnXMLDom.async = false;
		objReturnXMLDom.setProperty("ServerHTTPRequest",true);
		objReturnXMLDom.setProperty("SelectionLanguage", "XPath");
		return objReturnXMLDom;
	}

  //检查数据正确性
  function check(){
  	return true;
  }

  function getRightList(){
  	return objRightList;
  }

  function getLeftList(){
  	return objLeftList;
  }


	jhtc_attr_init(element,"label",null);
  	jhtc_attr_init(element,"load",null);
  	jhtc_attr_init(element,"para",null);
  	jhtc_attr_init(element,"width","500");
  	jhtc_attr_init(element,"line","10");
  	jhtc_attr_init(element,"eListHeight",null);
  	jhtc_attr_init(element,"ifLeftMultiSec",null);
  	jhtc_attr_init(element,"hasExtendBtn",null);
  	jhtc_attr_init(element,"borderWeight",null);
  	jhtc_attr_init(element,"borderColor",null);
  	jhtc_attr_init(element,"display",null);
  	jhtc_attr_init(element,"value","");
  	
	element.getRightList=getRightList;
	element.getLeftList=getLeftList;
	element.setValue=setValue;
	element.check=check
	element.init=init;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_class_map["inputMultiSelect_report"]=jhtc_doubleMultiSelect_report;