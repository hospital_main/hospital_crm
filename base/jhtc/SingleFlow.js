function jhtc_SingleFlow(win,jhtc_obj){
  var pageWin=win;
  var element=jhtc_obj;
  function refresh(){
    initialize() ;
  }
  
  var imagePath = "/base/themes/blue/images/SingleFlowS/"
  function initialize(){  
	  var xmlDoc=new ActiveXObject("MSXML2.DOMDocument");   
	  xmlDoc.async=false; 
		var isXML = true;
		var isURL = true ;
		
		if(element.inputXML == null || element.inputXML == "")
		{
			isXML = false;
		}
		if(element.inputURL == null || element.inputURL == "")
		{
			isURL = false;
		}

		if(( element.inputXML == null || element.inputXML == "" ) && ( element.inputURL == null || element.inputURL == "" )) {
		  return false ;
		}
		
		if(isXML){
		  if(xmlDoc.loadXML(element.inputXML)){   
	  		assembleTable(xmlDoc);   
		  }else{   
		  	alert(xmlDoc.parseError.reason);   
	  	}   
  	}else if(isURL){
  		if(xmlDoc.load(element.inputURL)){   
	  		assembleTable(xmlDoc);   
		  }else{   
		  	alert(xmlDoc.parseError.reason);   
	  	} 
  	}else{
  		alert("please set properties (inputXML | inputURL)!")
  		return;
  	}
  }   
   
  function  assembleTable(xmlDoc){   
  	var strHtml="";
		strHtml += "<table  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		strHtml += "  <tr>";
		strHtml += "    <td>";
		strHtml += "    	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		strHtml += "        <tr>";   
  	var oNodes=xmlDoc.selectNodes("//block"),oNode=null;   
	  for(var i=0;i<oNodes.length;i++){   
		  oNode=oNodes(i);
		  var strTitle = oNode.getAttribute("title");
		  var strLink = oNode.getAttribute("link");    
		  var strState = oNode.getAttribute("state");
		  var prefix = "" ;
		  switch(parseInt(strState)){
		  	case 0:
		  		prefix = "ju" ;
		  		break;
		  	case 1:
		  	  prefix = "lan"
		  	  break;
		  	case 2:
		  	  prefix = "red";
		  	  break;
		  	case 3:
		  	  prefix = "lu";
		  	  break;
		  	case 4:
		  	  prefix = "ju"
		  	  break;
		  }
		  var strContent = null;
		  var strArrow = null ;
		  
		  for(var j=0;j<oNode.childNodes.length;j++){   
		  	if(oNode.childNodes(j).nodeName == "content")
		  		strContent = oNode.childNodes(j).text ;
		  	if(oNode.childNodes(j).nodeName == "arrow")
		  		strArrow = oNode.childNodes(j).text ;
	  	}   
			strHtml += "					 <td width=\"4%\" rowspan=\"2\" align=\"left\" valign=\"top\">";
			strHtml += "					  <table width=\"35%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			strHtml += "					    <tr>";
			strHtml += "					      <td width=\"151\" height=\"34\" align=\"center\" valign=\"middle\" background=\""+imagePath+prefix+"1.png\" style=\"font-size: 12px\">";
			if ( strLink != '' && strLink != null ){
			  strHtml += "					       	<a href=\"#\" onclick=\"openDialog('"+ strLink +"','dialogWidth:850px;dialogHeight:650px','result')\"><strong style=\"font-size: 12px\">"+ strTitle +"</strong></a>";
			}
			else {
			  strHtml += "					       	<strong style=\"font-size: 12px\">"+ strTitle +"</strong>";
			}
			strHtml += "					      </td>";
			strHtml += "					    </tr>";
			strHtml += "					    <tr>";
			strHtml += "					      <td height=\"108\" align=\"middle\" valign=\"middle\" background=\""+imagePath+prefix+"2.png\">";
			strHtml += "					      	<table><tr><td><span style=\"font-size: 12px\">"+ strContent +"</span></td></tr></table>";
			strHtml += "					     	</td>";
			strHtml += "					    </tr>";
			strHtml += "					    <tr>";
			strHtml += "					       <td><img src=\""+imagePath+prefix+"3.png\" width=\"151\" height=\"30\"></td>";
			strHtml += "					    </tr>";
			strHtml += "					  </table>";
			strHtml += "					 </td>";
			if(strArrow != "0"){
			  var arrowName = "" ;
				switch(parseInt(strArrow)){
					case 1:
						arrowName = "lj";
						break;
					case 2:
						arrowName = "gj";
						break;
				}
				strHtml += "					<td width=\"4%\" height=\"130\" valign=\"middle\"><img src=\""+imagePath+arrowName+".png\" width=\"54\" height=\"26\"></td>";
			}
	  }   
    strHtml += "	  	  </tr>"; 
		strHtml += "        <tr>"; 
		strHtml += "          <td>&nbsp;</td>"; 
		strHtml += "          <td width=\"4%\">&nbsp;</td>"; 
		strHtml += "          <td width=\"4%\">&nbsp;</td>"; 
		strHtml += "          <td width=\"4%\" align=\"left\" valign=\"top\">&nbsp;</td>"; 
		strHtml += "        </tr>"; 
		strHtml += "    	</table>"; 
		strHtml += "   	</td>"; 
		strHtml += "  </tr>"; 
		strHtml += "</table>"; 
	  
	  element.insertAdjacentHTML("beforeEnd",strHtml);   
  } 


  	jhtc_attr_init(element,"inputXML","");
  	jhtc_attr_init(element,"inputURL","");
  	
	element.refresh=refresh;
	element.initialize=initialize;
	element.jhtcInit=function(){
		element.initialize();
	};
	return null;
};
jhtc_class_map["SingleFlow"]=jhtc_SingleFlow;