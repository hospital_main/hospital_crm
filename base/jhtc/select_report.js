function jhtc_select_report(win,jhtc_obj){
  var pageWin=win;
  var element=jhtc_obj;
  //整个SELECT由两个输入框，一个按钮和一个显示列表组成
  var objBtn;
  var objList;
  var objInput;
  var iframe=null;
  var srcTree = new ActiveXObject("Microsoft.XMLDOM");
  var xsltTree= new ActiveXObject("Microsoft.XMLDOM");
  srcTree.async=false;
  xsltTree.async=false;
  var hasList = "0";
  var vListMaxHigh = 81;
  var objXML;
  var oldSetValue;//LZK ADD
  var paraItemCount=0;
  var paraItemsXml=null;
  var is_onload;
  var overImgPath='/base/themes/blue/images/select/over.png';
  var outImgPath='/base/themes/blue/images/select/normal.png';
  var downImgPath='/base/themes/blue/images/select/normal.png';
  var disabledImgPath='/base/themes/blue/images/select/disabled.png';
  var blandImgPath='/base/themes/blue/images/select/blank.gif';
  var is_loaded=false;
  var is_load=true;
  var is_enabled=true;
  function reloadPic(){
    element.style.width = element.extent;
    element.style.color = "#FFFFFF";
    
    element.style.backgroundImage='url("'+outImgPath+'")';
    if ("true"== trim(element.required)) {
        element.style.backgroundColor="#DBFCFF";
    }
    with (objInput) {
      accessKey = element.accessKey;
      readOnly = element.readOnly;
      className = element.className+"_text";
      style.position = "absolute";
     
      var obj=element.parentNode.parentNode.parentNode.parentNode;
      if((obj.className=='lineCtn')||(obj.className=='mainCtn')){
        style.marginTop="5px";
        style.marginLeft="1px";
      }else{
        style.marginTop="2px";
        style.marginLeft="1px";

      }
      
      
      try{
      style.height = element.offsetHeight - 2;
      style.width = element.extent-22;
      }catch(e){}
      maxLength = element.maxInput;
      if ("true"== trim(element.required)) {
        style.backgroundColor="#DBFCFF";
      }
      

      onmouseover = overBtn;
      onmouseout = outBtn;
      onkeydown = navigateKeys;
      onblur = function() {
      clickDocument();
      }
    }
    objInput.onkeyup = objInput.ondragend = changeInput;
    element.style.cursor="hand";
    with(element){
      onmouseover=overBtn;
      onmouseout=outBtn;
      onclick=clickBtn;
    }
    element.accessKey="";
    element.tabIndex=-1;
    element.insertAdjacentElement("beforeBegin", objInput);
    is_onload=element.initLoad;
    if(element.defaultValue==null){
        if(element.required=="true")
          is_load=true;
        else
          is_load=false;    
    }else if(element.defaultValue=="false"){
      is_load=false;
    }else{
      is_load=true;
    }
      
  }
  function setEnabled(e){
  	if(e==true){
  	  element.style.backgroundImage='url("'+outImgPath+'")';
  	  element.disabled=false;
  		objInput.disabled=false;
  		is_enabled=true;
  	}else{
  	  element.style.backgroundImage='url("'+disabledImgPath+'")';
  		is_enabled=false;
  		objInput.disabled=true;
  		element.disabled=true;
  	}
  }
  function hasResult(){
    if (hasList == "0")
      return false;
    else 
      return true;
  }

  function setText(){
    return element.text=objInput.value;
  }

  function initBegin() {
    if (trim(element.load)=="") {
      alert("此select组件，没有指定load属性!");
      return;
    }
    if (element.label != null) {
      // label 的基本长度
      var lableLength = 120;
      var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
      if (spanWidth < lableLength) spanWidth = lableLength;

      // 快捷键
      var aKey = "";
      if (trim(element.accessKey)!="") {
        aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
      }
      element.insertAdjacentHTML("beforeBegin", "<span id='labelCtn' nowrap style='text-align:right;width:"+spanWidth+"px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";font-weight:"+element.currentStyle.fontWeight+";'>"+element.label+aKey+(element.label==''?"":"：")+"</span>");
    } else {
      var aKey = "";
      if (trim(element.accessKey)!="") {
        aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
      }  
      element.insertAdjacentHTML("beforeBegin", "<span id='labelCtn' nowrap style='text-align:right;width:0px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";font-weight:"+element.currentStyle.fontWeight+";'></span>");
    }
    element.parentNode.noWrap = true;
    
    // 设置输入框的样式
    objInput = element.document.createElement("<input/>");
    __jhtcBindPropertyChange(objInput,"value",setText);
    element.style.width = element.extent;
    element.style.color = "#FFFFFF";
    element.style.backgroundImage='url("'+outImgPath+'")';
    if ("true"== trim(element.required)) {
        element.style.backgroundColor="#DBFCFF";
    }
    with (objInput) {
      accessKey = element.accessKey;
      readOnly = element.readOnly;
      className = element.className+"_text";
      style.position = "absolute";
     
      var obj=element.parentNode.parentNode.parentNode.parentNode;
      if((obj.className=='lineCtn')||(obj.className=='mainCtn')){
        style.marginTop="5px";
        style.marginLeft="1px";
      }else{
        style.marginTop="2px";
        style.marginLeft="1px";

      }
      
      
      try{
      style.height = element.offsetHeight - 2;
      style.width = element.extent-22;
      }catch(e){}
      maxLength = element.maxInput;
      if ("true"== trim(element.required)) {
        style.backgroundColor="#DBFCFF";
      }
      

      onmouseover = overBtn;
      onmouseout = outBtn;
      onkeydown = navigateKeys;
      onblur = function() {
      clickDocument();
      }
    }
    objInput.onkeyup = objInput.ondragend = changeInput;
    element.style.cursor="hand";
    with(element){
      onmouseover=overBtn;
      onmouseout=outBtn;
      onclick=clickBtn;
    }
    element.accessKey="";
    element.tabIndex=-1;
    element.insertAdjacentElement("beforeBegin", objInput);
    is_onload=element.initLoad;
    if(element.defaultValue==null){
        if(element.required=="true")
          is_load=true;
        else
          is_load=false;    
    }else if(element.defaultValue=="false"){
      is_load=false;
    }else{
      is_load=true;
    }
      
    initList();
  }
  
  function changeInput() {
  	if ((event.keyCode>=33 && event.keyCode<=40) || event.keyCode==13 || event.keyCode==16 || event.keyCode==18 || event.keyCode==9
          || event.keyCode==27 || event.keyCode==0 || event.keyCode>250)
      return;
    if(objInput.value == "") {
    	element.value = "";
    }  
  	if (trim(element.checkValue)!="true") {
  	  element.value = objInput.value;
  	}
		if (trim(this.value)=="" && objList.style.display=="none") return;
		showList();
  }
  
  function changeTitle() {
    objInput.title = element.text;
    element.title = element.text;
  }
  function alterEnter(){
    
    if(event.keyCode==13){
      event.keyCode=9;
    }
  }
  function initEnd() {
    if (objInput == null) return;
    // 初始化按钮
   
    iframe=element.document.createElement("iframe");
	with(iframe){
		style.position = "absolute";
		style.display = "none";
		style.textAlign = "center";
		style.backgroundColor = "#F6F6F6";
		className = "ds_font";
		style.zIndex =8;
		style.overflow = "visible";
		border="0";
	}
	iframe = element.parentNode.appendChild(iframe);
  	//window.attachEvent("onresize", adjPosition);
  	//adjPosition();
  	//window.setTimeout(adjPosition,50);
  	//window.setTimeout(adjPosition,200);
  	//window.setTimeout(adjPosition,400);
  	//objBtn.style.display=''
  	if (trim(element.initValue) != "") {
  	  setValue(element.initValue);
  	}
  }
  
  
  // 调整各个元素的位置
  function adjPosition() { // 调整位置
    // 取得element的绝对位置
    var form = element;
    var elementTop=0, elementLeft=0;

   while(form.tagName != "BODY"&&form.tagName != "DIV") {
	    elementTop = elementTop + form.offsetTop + form.clientTop;
	    elementLeft = elementLeft + form.offsetLeft+form.clientLeft;
	    form = form.offsetParent;
    }

    with (objList.style) {
		zIndex =10;
     	left = elementLeft-1;
     	if(elementTop+element.offsetHeight-1+parseInt(objList.style.height.replace(/px/,'')) > window.document.body.offsetHeight)
     	  top = elementTop + element.offsetHeight-1-100;
     	else
  		  top = elementTop + element.offsetHeight-1;
    }
    
	with (iframe.style) {
		left = elementLeft-1;
		top=objList.style.top;
	}	
	if(objList.style.width<2)		return ;
		iframe.style.width=parseInt(objList.style.width);
		iframe.style.height=81;//objInput.clientHeight;
	
  }
	var _adjPositionadjPosTimer_;
  function overBtn(){ //鼠标移动到objBtn上的样式
    element.style.backgroundImage='url("'+overImgPath+'")';  
  }

  function outBtn(){ //鼠标移出Btn 或者input时的样式
  	if(objList && objList.style.display != "none"){}
  	else {
  		element.style.backgroundImage='url("'+outImgPath+'")';
  	}
  }

  function clickBtn() { // Btn 按下后的样式
    if(!is_enabled)return;
    if (objList!=null && objList.style.display != "none"){
      objList.style.display = "none";
      if(iframe!=null)
       iframe.style.display=objList.style.display;
      }
    else{
  	  showList();
  	  //add by zrc
      //objList.focus();
      //end
  	}
  }
  //wsj 从showList 方法中抽取出来，专门用于加载数据源，而不显示。
 function loadListdata(){
 		var lists = window.document.getElementsByTagName("DIV")
		for (var i=0; i<lists.length; i++) {
			if (lists[i].className == "select_list")
				lists[i].style.display = "none";
		}

		if (!element.readOnly && false) {
			srcTree.loadXML(objXML.replace(/key='(.*)'/,"key='"+trim(objInput.value)+"'"))
			objList.innerHTML = srcTree.transformNode(xsltTree);

	    var trs = objList.getElementsByTagName("TR")
	    for (var i=0; i<trs.length; i++) {
	      trs[i].onmouseover = function() {
	        if (this.parentNode.parentNode.choseIndex!=null) {
	          with (this.parentNode.parentNode.rows[this.parentNode.parentNode.choseIndex].runtimeStyle) {
	            backgroundColor="";
	            color="";
	          }
	        }

	        this.parentNode.parentNode.choseIndex = this.rowIndex;
	        this.runtimeStyle.backgroundColor="darkblue";
	        this.runtimeStyle.color="white";
	      }
	      trs[i].onmousedown = choose;
	      if (i%2==0) {
	        trs[i].style.backgroundColor="whitesmoke";
	      }
	    }
    }
	
 }
 //wsj
  // 显示列表
  function showList() {
    if(!is_enabled) return
    if(!is_loaded){
      is_load=true;
      initList();
    } 
    
		loadListdata();	
    adjPosition();

    objList.style.display = "";
	  iframe.style.display=objList.style.display 


    var trs = objList.getElementsByTagName("TR");
    if (trs.length==0) return false;
    var i=0;
    for (; i<trs.length; i++) {
      if (trs[i].innerText.indexOf(objInput.value)==0) {
        break;
      }
    }

    if (i==trs.length) i=0;

    if (trs[i].parentNode.parentNode.choseIndex!=null) {
      with (trs[i].parentNode.parentNode.rows[trs[i].parentNode.parentNode.choseIndex].runtimeStyle) {
        backgroundColor = "";
        color = "";
      }
    }
    trs[i].runtimeStyle.backgroundColor="darkblue";
    trs[i].runtimeStyle.color="white";
    trs[i].parentNode.parentNode.choseIndex = trs[i].rowIndex;
    // 移动div scroll 1.取trs[i]的绝对top
    var baseDivTop = 0;
    var parentObj = trs[i];
    while (parentObj.tagName != "DIV") {
	    baseDivTop += parentObj.offsetTop;
	    parentObj = parentObj.offsetParent;
    }
    parentObj.scrollTop = baseDivTop;

    window.document.attachEvent("onmousedown",clickDocument);
  }
  function initListByXML(str) {
    if (trim(element.code)=="true") {
      objXML = str.replace("<root>","<root code='true' qtype='0' key=''>");
    } else
      objXML = str.replace("<root>","<root code='false' qtype='0' key=''>");
    srcTree.loadXML(objXML);
    xsltTree.load(window.prefix+"base/xsl/select1.xsl");
    objList.innerHTML = srcTree.transformNode(xsltTree);
		with (objList) {
  		style.display = "none";
		  iframe.style.display=objList.style.display 
      style.top = objInput.offsetTop+objInput.offsetHeight;
    	style.width = element.offsetWidth;
  	}

  	var trs = objList.getElementsByTagName("TR")
    for (var i=0; i<trs.length; i++) {
      trs[i].onmouseover = function() {
        if (this.parentNode.parentNode.choseIndex!=null) {
          with (this.parentNode.parentNode.rows[this.parentNode.parentNode.choseIndex].runtimeStyle) {
            backgroundColor=''
            color=''
          }
        }

        this.parentNode.parentNode.choseIndex = this.rowIndex
        this.runtimeStyle.backgroundColor='darkblue'
        this.runtimeStyle.color='white'
      }
      trs[i].onmousedown = choose
      if (i%2==0) {
        trs[i].style.backgroundColor='whitesmoke'
      }
    }

  	if (trim(element.required)=="true" && objXML.indexOf("code=\"")!=-1) {
			var str = objXML.substring(objXML.indexOf("code=\"")+"code=\"".length)
			element.value = str.substring(0, str.indexOf("\""))
			str = str.substring(str.indexOf("value=\"")+"value=\"".length)
			objInput.value = str.substring(0, str.indexOf("\""))
			if (trim(element.code)=="true")
				objInput.value = element.value+"　"+objInput.value
  	}
  }
  	function selectItem(index){
  		if(index<0||index>(getItemCount()-1)||paraItemsXml==null)
  			return ;
  		var p=objList.getElementsByTagName("TR")[index+1].cells[0].value;
  		setValue(p);
  	}
	function getItemCount(){
		return paraItemCount;
	}
	function getDict_html(in_load,in_para,in_code){
	
	  if (in_para != null) { 
				xmlhttp.post("global_select_html_report", in_para, "?selectID="+in_load+","+in_code);
			}
			else
				xmlhttp.post("global_select_html_report", '', "?selectID="+in_load+","+in_code);
			
			if (window.doMsg(xmlhttp._object.responseText)) {
				
				return xmlhttp._object.responseText;
				
			}
	}
  // 初始化显示列表
  function initList(str) {
  	objInput.value = "";
  	element.value = "";
    objList = element.document.createElement("<div class='select_list'></div>");
  	element.insertAdjacentElement("afterEnd", objList);
		if (trim(str)!='') {
		  element.load=str
		}
//added by wsj		
    var loadXML = "";
/*wsj1.1 输入法检索*/    
    if (trim(element.load)=="retrieve_method") {
 //   	para=para+"<text>"+element.text+"</text">;
    	 
      if( element.para.indexOf("<retrieve>")>0){
 	     element.para= element.para.substring(0,element.para.indexOf("<retrieve>"));//如果不是初始状态，则取得，在尾部已经给了输入法的数值，去掉将后面的输入法的数值

   	   element.para=element.para+'<retrieve>'+getRetrieve()+'</retrieve>' 
   	  } 

    }

//added by twl
    var elem_code;
    if(trim(element.code)=="true")
    {
      elem_code="true";
    }else{elem_code="false"}
    if ((trim(element.para)=="")&& (is_load==true)) {
      loadHTML = getDict_html(element.load,'',elem_code);
      is_loaded=true;
    } else if((is_onload=="true") && (is_load==true) &&(trim(element.para)!='multi')) {
      loadHTML= getDict_html(element.load,trim(element.para),elem_code);
      is_loaded=true;
    }else{
      loadHTML='0*<TBODY><TABLE><TR id=top_line style="FONT-SIZE: 12px"><TD style="PADDING-LEFT: 5px; FONT-SIZE: 12px; PADDING-TOP: 1px; HEIGHT: 16px" noWrap value=""></TD></TR></TABLE></TBODY>';
    }
    
    
    var marr=loadHTML.split('*');
	  var maxLength = parseInt(marr[0]);
	  if(marr[0]=="xml"){
     
        if (trim(element.code)=="true") {
          objXML = marr[1].replace("<root>","<root code='true' qtype='0' key=''>");
        } else
          objXML = marr[1].replace("<root>","<root code='false' qtype='0' key=''>");
    
        srcTree.loadXML(objXML);
        element.serverObj=srcTree;
        var objNodeList = srcTree.getElementsByTagName("para");
        var maxLength = 0;
        for (var j=0; j<objNodeList.length; j++) {
          if (maxLength < objNodeList.item(j).attributes.item(1).nodeValue.length) {
            maxLength = objNodeList.item(j).attributes.item(1).nodeValue.length;
          }
        }
        
        xsltTree.load(window.prefix+"base/xsl/select1.xsl");
        objList.innerHTML = srcTree.transformNode(xsltTree);

      
	  }else{
	    objList.innerHTML=marr[1];
	  }
	  
	  
    

		with (objList) {
  		style.display = "none";
      style.top = objInput.offsetTop+objInput.offsetHeight;
      style.height = vListMaxHigh;
  	}
   paraItemCount=objList.getElementsByTagName("TR").length-1;
  	if (maxLength <= 9) {
  	  objList.style.width = element.offsetWidth;
  	} else {
  	  var temp = parseFloat(element.offsetWidth)+parseFloat(((maxLength-9)*20>200)?200:(maxLength-9)*20);
  	  if(isNaN(temp))
  	  	temp=120
  	  objList.style.width = temp>200?(element.extent>200?element.extent:200):temp;
  	}

  	var trs = objList.getElementsByTagName("TR")

//add by zrc 06-9-22
	objList.onkeydown = function(){	
		event.cancelBubble=true;
		//如果是回车，则选中
		if(event.keyCode==13){
			choose();
			
			event.keyCode=9;
			return;
		}
		
	    var table = objList.getElementsByTagName("TABLE")[0] ;
	    var oldchooseindex = table.choseIndex ;
	    var newchooseindex = table.choseIndex ;
	    
	    //向下 s
		if(event.keyCode==40){
	
			//如果选项的是最后一项，则不操作，否则选择项索引加一
	    	if (table.choseIndex+1==table.rows.length)
	    		return;
	    	else{
		    	table.choseIndex = table.choseIndex+1;
		    	newchooseindex = table.choseIndex;
		    	objList.scrollTop = objList.scrollTop + table.rows[table.choseIndex].offsetHeight;
		    }
		}
		
		//向上 w
		if(event.keyCode==38){
	
			//如果选项的是最后一项，则不操作，否则选择项索引加一
	    	if(table.choseIndex==0)
	   			return;
	    	else{
		    	table.choseIndex = table.choseIndex-1;
		    	newchooseindex = table.choseIndex;
		    	objList.scrollTop = objList.scrollTop - table.rows[table.choseIndex].offsetHeight;
			}
		}	
	
	    with (table.rows[oldchooseindex].runtimeStyle) {
	            backgroundColor="";
	            color="";
	    }
	    
	    with (table.rows[newchooseindex].runtimeStyle) {
	            backgroundColor="darkblue";
	            color="white";
	    }
	
	}
//end


  	
  	if (trs.length != 0) {
  	  hasList = "1";
  	} else {
  	  hasList = "0";
  	}
    for (var i=0; i<trs.length; i++) {
      trs[i].onmouseover = function() {
        if (this.parentNode.parentNode.choseIndex!=null) {
          with (this.parentNode.parentNode.rows[this.parentNode.parentNode.choseIndex].runtimeStyle) {
            backgroundColor="";
            color="";
          }
        }

        this.parentNode.parentNode.choseIndex = this.rowIndex;
        this.runtimeStyle.backgroundColor="darkblue";
        this.runtimeStyle.color="white";
      }
      trs[i].onmousedown = choose;
      
      if (i%2==0) {
        trs[i].style.backgroundColor="whitesmoke";
      }
    }

    var flag = false;
  	if (trim(element.defaultValue)=="true" ) {
			flag = true;
  	}
  	
    if (trim(element.required)=="true") {
      objList.all("top_line").style.display = "none";
			objInput.style.backgroundColor="#DBFCFF";
  	} else {
  	  objInput.style.backgroundColor="#FFFFFF";
  	}
    
  	if (trim(element.required)=="true" && trim(element.defaultValue) != "false" ) {
			flag = true;
  	}
  	if (flag) {
			var tr=objList.getElementsByTagName("TR");
			if(tr.length>1){
			  element.value=tr[1].cells[0].value;
			  objInput.value = tr[1].cells[0].innerText;
			 }else{
			  element.value="";
			  objInput.value="";
			 }
			
			changeTitle();
  	}
  	
  	if (trim(str)!='' && !flag) {
	    objInput.value=''
	    element.value=''
	  }
  }
  
  // 刷新页面
  function refresh() {
    is_onload="true";
    //alert(element.parentNode.outerHTML)
    var aKey = "";
    if (trim(element.accessKey)!="") {
      aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
    }
    if (trim(element.label)!="") {
      element.parentNode.getElementsByTagName("SPAN")[0].innerText = element.label+aKey+(element.label==''?"":"：");
    }
    //added by wsj 在重新加载的时候，检查select控件是否下拉。
    if(element.load!=""){
    	var dis=objList.style.display; //检查select控件是否下拉。
      if(element.defaultValue==null){
          if(element.required=="true")
            is_load=true;
          else
            is_load=false;    
      }else if(element.defaultValue=="false"){
        is_load=false;
      }else{
        is_load=true;
      }
      if(is_loaded) is_load=true;
	  	initList();
	  	if(element.load=="retrieve_method"){
				if (dis!= "none"){
	      	showList();
	     	 }
		    else{
		  	  loadListdata();	
		  		}			
	    	}
    //wsj added
  	//adjPosition();
  	if(typeof(oldSetValue)!="undefined")
  	setValue(oldSetValue);
  	}
  	 
  }
  
  // 点击选择项响应事件
	function choose() { 
    var table = objList.getElementsByTagName("TABLE")[0];
    if (table.choseIndex==null) {
      return false;
    }
		objInput.value = table.rows[table.choseIndex].innerText;
		changeTitle();
    objList.style.display = "none";
	  iframe.style.display=objList.style.display 
    element.style.backgroundImage='url("'+outImgPath+'")';
    
    if (table.rows[table.choseIndex].cells.length>0 &&
    	element.value!=table.rows[table.choseIndex].cells[0].value) {
    	element.value=table.rows[table.choseIndex].cells[0].value;
    	//var oEvent = createEventObject();
		//	oEvent.result = element.value;
	   	//changeID.fire();
	   	__jhtcDispatchEvent(element,"onchange");
	   	//add by 
    }

    objInput.select();
    objInput.focus();
  }
  
  // 点击按钮等响应事件
  function clickDocument() { 
  	if(objList==null) return;

		var objSrc = window.event.srcElement;
		if (element.contains(objSrc) || objList.contains(objSrc) || (objBtn!=null && objBtn.contains(objSrc))) {
  	  if (objSrc.value != null) {
  	    element.value = objSrc.value;
  	  }
  	  return;
  	} 

  	objList.style.display="none";
	  iframe.style.display=objList.style.display 
    element.style.backgroundImage='url("'+outImgPath+'")';
    window.document.detachEvent("onmousedown",clickDocument);

  }

  // 键盘响应
  function navigateKeys() {
		var nKeyCode=event.keyCode;
    switch(nKeyCode){
      case 113:  // F2 
      	if(element.findpage!=null)
      		openDialog(element.findpage, 'dialogWidth:900px;dialogHeight:550px')
      	else{
	        selectObj.setPara(element.para);
	        selectObj.setLoad(element.load);
	        selectObj.select()
	        if (trim(selectObj.value)!=''){
	          setValue(selectObj.value)
	          
	          if(onchange!=null){
	          	 var str=""+onchange;
	          	
	             str = str.substring(22,str.length - 2);
	             str=str.replace("this","element");
	             
	            eval(str);
	          }
	        }  
        }
        return true
      case 38://^
        if (objList!=null && objList.style.display=="") {
          scrollUpList();
          return false;
        }
        break;
      case 40://\|/
        if (1==1) {
          if (objList==null || objList.style.display=="none") {
            if(is_enabled) showList();
            return false;
          }
        }
        if (objList!=null && objList.style.display=="") {
          scrollDownList();
          return false;
        }
        break;
      case 13://Enter
      
        event.keyCode=9
        if (element.value == "") {
          //changeID.fire();
          __jhtcDispatchEvent(element,"onchange");
          event.keyCode=9;
        }
        if (objList!=null && objList.style.display=="") {
          event.keyCode=9;
          choose();
         
          objInput.onkeyup = objInput.ondragend = null;
          
          
         	return false;
        }
        break;
      case 27:// Esc
        if (objList!=null){
          objList.style.display="none";
          iframe.style.display=objList.style.display
         }
        return false;
      case 33://pageup
        if (objList!=null && objList.style.display=="") {
          scrollPageUpList();
          return false;
        }
      case 34://pagedown
      	if (objList!=null && objList.style.display=="") {
          scrollPageDownList();
          return false;
        }
      default:
        objInput.onkeyup = objInput.ondragend = changeInput;
        return true;
    }
    return true;
  }
  
  //向上翻页滚动列表
	function scrollPageUpList() {
    var table = objList.getElementsByTagName("TABLE")[0];
    if (table.rows.length>0) {
      var count = objList.offsetHeight/table.rows[0].offsetHeight-1;
      for (var i=0; i<count; i++) {
        scrollUpList();
      }
    }
  }
  
  //向下翻页滚动列表
  function scrollPageDownList() {
    var table = objList.getElementsByTagName("TABLE")[0];
    if (table.rows.length>0) {
      var count = objList.offsetHeight/table.rows[0].offsetHeight-1;
      for (var i=0; i<count; i++) {
        scrollDownList();
      }
    }
  }
  
  //向下滚动列表
  function scrollDownList() {
    var table = objList.getElementsByTagName("TABLE")[0];
    if (table.rows.length<1) return;
    if (table.choseIndex>=table.rows.length-1) return;

    if (table.choseIndex!=null) {
      with (table.rows[table.choseIndex].runtimeStyle) {
        backgroundColor = "";
        color = "";
      }
    } else {
      table.choseIndex = 0;
    }

    table.choseIndex++;

    var baseDivTop = 0;
    var parentObj = table.rows[table.choseIndex];
    while (parentObj.tagName != "DIV") {
	    baseDivTop += parentObj.offsetTop;
	    parentObj = parentObj.offsetParent;
    }

    if (baseDivTop+table.rows[table.choseIndex].offsetHeight*2 > parentObj.scrollTop+parentObj.offsetHeight) {
      parentObj.scrollTop = parentObj.scrollTop + table.rows[table.choseIndex].offsetHeight;
    }

    table.rows[table.choseIndex].runtimeStyle.backgroundColor="darkblue";
    table.rows[table.choseIndex].runtimeStyle.color="white";
  }

  //向上滚动列表
  function scrollUpList() {
    var table = objList.getElementsByTagName("TABLE")[0];

    if (table.choseIndex==null || table.choseIndex==0) {
      table.choseIndex = 0;
      return;
    } else {
      with (table.rows[table.choseIndex].runtimeStyle) {
        backgroundColor = "";
        color = "";
      }
    }

    table.choseIndex--;

    table.rows[table.choseIndex].runtimeStyle.backgroundColor="darkblue";
    table.rows[table.choseIndex].runtimeStyle.color="white";

    var baseDivTop = 0;
    var parentObj = table.rows[table.choseIndex];
    while (parentObj.tagName != "DIV") {
	    baseDivTop += parentObj.offsetTop;
	    parentObj = parentObj.offsetParent;
    }

    if (baseDivTop < parentObj.scrollTop) {
      parentObj.scrollTop = parentObj.scrollTop - table.rows[table.choseIndex].offsetHeight;
    }
  }
  
  //检查输入项是否正确
  function check() {
    
    if (trim(element.checkValue)!="true") {
  		return true;
  	}
   	if ("true"==element.required) {
      if (window.trim(objInput.value)=="" && trim(element.checkValue)=="true")  {
        if (element.label != null) {
          alert(element.label+"不能为空！");
        } else {
          alert("请选择必选项！");
        }
        objInput.focus();
        objInput.select();
        return false;
      }
    }

    var os=objList.innerText.replace(/\s/g,"");
  	var oi=objInput.value.replace(/\s/g,"");
  	os=os.replace(/&gt;/g,">")
  	os=os.replace(/&lt;/g,"<")
  	os=os.replace(/&quot;/g,"\"")
  	
/*
    if (trim(code)=="true" && os.indexOf("\""+trim(oi).replace(/　.*$/, "")+"\"value=\""+trim(oi).replace(/^.*　/, "")+"\"")==-1) {
    	if ((trim(required)=="true" || trim(oi)!="") && trim(checkValue)=="true") {
	  		alert("请选择合法的值！");
	  		objInput.select();
	  		return false;
  		}
  	}

  	if (trim(code)!="true" && os.indexOf("value=\""+oi+"\"")==-1 ) {
  		if ((trim(required)=="true" || trim(oi)!="") && trim(checkValue)=="true") {
	  		alert("请选择合法的值！");
	  		objInput.select();
	  		return false;
  		}
  	}
*/
    if (objInput.value != "") {
      var table = objList.getElementsByTagName("TABLE")[0];
      if (table.choseIndex != null) {
    		objInput.value = table.rows[table.choseIndex].innerText;
    		changeTitle();
        if (table.rows[table.choseIndex].cells.length>0 &&
        	element.value!=table.rows[table.choseIndex].cells[0].value) {
        	element.value=table.rows[table.choseIndex].cells[0].value;
        }
      }
    }
    return true;
  }

  
  //给SELECT赋值
  function setValue(str) {
    if(!is_loaded){
      is_load=true;
      initList();
    } 
 	  oldSetValue=str;
    if (str == null ){
    	objInput.value=element.value="";
    	return;
    }
    
    var trs = objList.getElementsByTagName("TR");
  	element.value = str;
  	
  	var j=0;
  	for (j=1; j<trs.length; j++) {
      if (trs[j].cells[0].value==str) {
        break;
      }
    }
  
  	if(j==trs.length){
  	  if((element.required=="true")&&(trs.length>1))
  	    j=1;
  	  else
  	    j=0;
  	}
  	
  	objInput.value = trs[j].cells[0].innerText;
    
 //wsj added 06-1-11  
		//loadListdata();	
    if(is_enabled){
	    objList.style.display = "";
	    if(iframe!=null)
	      iframe.style.display=objList.style.display 
	}    
    if (trs.length==0) return false;
    var i=0;
    for (; i<trs.length; i++) {
      if (trs[i].innerText.indexOf(objInput.value)==0) {
        break;
      }
    }
    if (i==trs.length) i=0;
    if (trs[i].parentNode.parentNode.choseIndex!=null) {
    trs[i].parentNode.parentNode.choseIndex = trs[i].rowIndex; 
    trs[i].runtimeStyle.backgroundColor="darkblue";
    trs[i].runtimeStyle.color="white";  
    }
    clickBtn()
//wsj added 06-1-11      
    changeTitle();
  }


  	jhtc_attr_init(element,"label",null);
  	jhtc_attr_init(element,"load",null);
  	jhtc_attr_init(element,"para","");
  	jhtc_attr_init(element,"findpage",null);
  	jhtc_attr_init(element,"defaultValue",null);
  	jhtc_attr_init(element,"initValue",null);
  	jhtc_attr_init(element,"initLoad","true");
  	jhtc_attr_init(element,"code","false");
  	jhtc_attr_init(element,"required","false");
  	jhtc_attr_init(element,"checkValue","true");
  	jhtc_attr_init(element,"extent","140");
  	jhtc_attr_init(element,"maxInput","50");
  	jhtc_attr_init(element,"text","");
  	jhtc_attr_init(element,"serverObj","");
  	
	element.initListByXML=initListByXML;
	element.selectItem=selectItem;
	element.getItemCount=getItemCount
	element.setEnabled=setEnabled;
	element.hasResult=hasResult;
	element.setValue=setValue;
	element.check=check;
	element.refresh=refresh;
	element.reloadPic=reloadPic;
	element.initBegin=initBegin;
	element.initEnd=initEnd;
	element.jhtcInit=function(){
		element.initBegin();
		element.initEnd();
	};
	return null;
};
jhtc_class_map["inputSelect_report"]=jhtc_select_report;
