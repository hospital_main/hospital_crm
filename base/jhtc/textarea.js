 //<public:event name="ondblclick" id="evtOnDblClick"/>
function jhtc_textarea(win,jhtc_obj){
  var pageWin=win;
  var element=jhtc_obj;
  var objTextArea = null;
  var vText = "";

  function init(){// 组件初始化
  	
  	if (trim(element.name) == "") {
      alert("请设置TextArea的name！")
      return false;
    }
    
    if (element.label != null) {
      // -- label 的基本长度
      var lableLength = 120;
      var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
      if (spanWidth < lableLength) spanWidth = lableLength;
  		
  	  var vWidth = element.cols;
  	  var vSize = element.rows;
  	  
  	  var booleanDisplay = true;
  	  if(element.display && element.display.toString() == "false"){
  		  booleanDisplay = false;
  	  }
  	  
      var aKey = "";
      if ((element.accessKey) != '' && !element.readOnly)
        aKey = "(<span style='font-family:" + element.currentStyle.fontFamily + ";font-size:" + element.currentStyle.fontSize + ";'>)";
        
      var objTable = window.document.createElement("<table cellspacing='0' cellpadding='0' border='0'>");
      if (element.direction == "0") {
        var objTr = objTable.insertRow();
        if(element.label!=""){
	        var objTDLeft = objTr.insertCell();
	        with(objTDLeft){
	      		align = "right";
	      		vAlign = "top";
	      		noWrap = "true";
	      		width=lableLength;
	      		innerText = (element.label==null||element.label=="")?"":element.label + aKey + "：";
	        }
        }
        
        var objTDRight = objTr.insertCell();
        with(objTDRight){
      		valign = "top";
      		noWrap = "true";
        }
        if(!element.readOnly){
  		    objTextArea = window.document.createElement("<textarea cols='" + element.cols + "' rows='" + element.rows + "' maxlength='" + element.maxinput + "'>");
        }else{
  		    objTextArea = window.document.createElement("<textarea cols='" + element.cols + "' rows='" + element.rows + "' maxlength='" + element.maxinput + "' readOnly>");
        }
        if(!booleanDisplay){// 不显示
      		objTable.style.display = "none";
        }
        objTextArea = objTDRight.appendChild(objTextArea);
      } else {
        var objTRTop = objTable.insertRow();
        var objTdTop = objTRTop.insertCell();
        with(objTdTop){
      		align = "left";
      		vAlign = "top";
      		noWrap = "true";
      		innerText = (element.label==null||element.label=="")?"":element.label + aKey + "：";
        }
        
        var objTRBottom = objTable.insertRow();
        var objTdBottom = objTRBottom.insertCell();
        with(objTdBottom){
      		valign = "top";
      		noWrap = "true";
        }
        if(!element.readOnly){
  		    objTextArea = window.document.createElement("<textarea cols='" + element.cols + "' rows='" + element.rows + "'>");
        }else{
  		    objTextArea = window.document.createElement("<textarea cols='" + element.cols + "' rows='" + element.rows + "' readOnly>");
        }
        if(!booleanDisplay){// 不显示
      		objTable.style.display = "none";
        }
        objTextArea = objTdBottom.appendChild(objTextArea);
      }
      objTextArea.onchange = setTextVal;
      objTextArea.onkeypress = checkIllegal;
      if(element.required=="true"){
      	objTextArea.style["background"]=JHTC_COLOR.REQUIRED;
      }
      
      //element.style.visibility = "hidden";
      element.style.display = "none";
      element.insertAdjacentElement("beforeBegin",objTable);
      element.value = "";
    }
    __jhtcBindPropertyChange(element,"value",setText);
  }
  
  function checkIllegal() {
    if(this.value.length == element.maxinput) return false;
  }
  
  function setTextVal(){
    var temp = objTextArea.value;
  	element.value = temp//.replace(/\n/g,'DdDdDd');
  	vText = temp//.replace(/\n/g,'DdDdDd');
  }

  function setValue(val){
    if(val == null) return;
    objTextArea.value = val//.replace(/DdDdDd/g,'\n')
  	element.value = val;
  	vText = val;
  }
  function setText(){
  	var val = element.value;
    if(val == null) return;
    objTextArea.value = val;//.replace(/DdDdDd/g,'\n')
  	vText = val;
  }

  function getText(){
	  return vText;
  }
  function setfocus(){
  	objTextArea.focus();
  }
  //检查数据正确性
  function check(){
  	if(element.required=="true"&&(objTextArea.value==null||objTextArea.value.length==0)){
  		var l="";
  		if(element.label!=null)
  			l=element.label;
  		alert(l+"不能为空");
  		objTextArea.focus();
  		return false;
  	}
  	if(objTextArea.value.indexOf("'")>=0&&element.anyLetter=="false"){
		try{
			alert("不能输入单引号");
			objTextArea.focus();
			return false;
	        }catch(E){
	        	return false;
	        }
	}
    if (objTextArea.value != null && objTextArea.value.length > element.maxinput) {
      alert("输入文本超过长度！");
      return false;
    }
    if (element.value.indexOf('<') != -1 || element.value.indexOf('>') != -1) {
      alert("文本区域中不能包括< >该类字符！");
      return false;
    }
  	return true;
  }


  	jhtc_attr_init(element,"label",null);
  	jhtc_attr_init(element,"direction","0");
  	jhtc_attr_init(element,"required","false");
  	jhtc_attr_init(element,"display","true");
  	jhtc_attr_init(element,"cols","20");
  	jhtc_attr_init(element,"rows","2");
  	jhtc_attr_init(element,"maxinput","200");
  	jhtc_attr_init(element,"value","");
  	jhtc_attr_init(element,"anyLetter","false");
  	element.setfocus=setfocus;
	element.setValue=setValue;
	element.check=check;
	element.init=init;
	element.jhtcInit=function(){
		element.init();
		element.setValue();
	};
	return null;
};
jhtc_class_map["inputTextarea"]=jhtc_textarea;