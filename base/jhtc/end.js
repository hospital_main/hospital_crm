/*
Javascript HTC end
Author:liu zhikun
*/
function __jhtcEndSubmitData(param){
	var str="";
	var hasData=false;
	for(var id in param.datas){
		if(typeof(param.datas[id]["data"])!="undefined"){
			hasData=true;
			str+="<"+param.datas[id]["type"]+jhtc_suffix_dataType+" id=\""+id+"\" ";
			for(var k in param.datas[id]){
				if(k.substr(0,2)=="on"||k=="data"||k=="type"||k=="id")
					continue;
				str+=k+"=\""+jhtcEncodeXmlString(param.datas[id][k])+"\" ";
			}
			str+=">"+jhtcEncodeXmlString("<root>"+param.datas[id]["data"]+"</root>")+"</"+param.datas[id]["type"]+jhtc_suffix_dataType+">";
		}
	};
	str+="";
	if(hasData==true){
		var axml = new ActiveXObject("Microsoft.XMLHTTP");
		axml.open("POST","jhtcData.viewhigh",false);
		axml.send("<root>"+str+"</root>");
		if (axml.status>=400){
			alert("error loading document: jhtc data - "+axml.statusText+" - "+axml.responseText);
		}else{
			param.onData(axml.responseXML);
		}
		axml=null;
	}else{
		param.onData(null);
	}
};
function __jhtconreadystatechange(){}
function __jhtcEndRunQuery(parentObj,fun){
	if(fun){}else{
		fun=function(){};
	}
	var params=__jhtcCreateParams(fun);
	for(var k in jhtc_class_map){
		__jhtcEndReadyQuery(params,parentObj,'.'+k,jhtc_class_map[k]);
	};
	for(var k in jhtc_tag_map){
		__jhtcEndReadyQuery(params,parentObj,k,jhtc_tag_map[k]);
	};
	__jhtcEndSubmitData(params);
	params.init();
};
function __jhtcEndReadyQuery(params,parentObj,k,fun){
	try{
	$(k,parentObj).each(function(){
		params.regElem(fun,this);
	});
	} catch(e){};
};
function __jhtcCreateParams(fun){
	return {
		subFun:fun,
		hasInit:false,
		hasData:false,
		idx:0,
		elems:[],
		datas:{},
		regElem:function(fun,elem){
			if(elem["__jhtc"]){
				return;	
			};
			this.elems.push(elem);
			elem["__jhtc"]="vh";
			var param=fun(window,elem);
			if(param!=null){
				if(param instanceof Array){
					for(var i = 0;i<param.length;i++){
						this.addData(param[i]);
					}
				}else{
					this.addData(param);
				}
			}
		},
		addData:function(data){
			data.id="id"+(this.idx++);
			this.datas[data.id]=data;
		},
		init:function(){
			for(var i=0;i<this.elems.length;i++){
				this.elems[i].jhtcInit();
				this.elems[i]=null;
			}
			this.elems=null;
			this.hasInit=true;
			this.showData();
		},
		onData:function(dat){
			this.data={};
			if(dat!=null){
				var item=dat.documentElement.firstChild;
				while(item!=null){
					if(item.nodeType==1){
						this.data[item.getAttribute("id")]=""+item.firstChild.nodeValue;
					};	
					item=item.nextSibling;
				};
				item=null;	
			}
			
			this.hasData=true;
			this.showData();
		},
		showData:function(){
			if(this.hasInit==false||this.hasData==false)
				return;
			for(var id in this.data){
				this.datas[id]["onData"](this.data[id]);
				this.data[id]=null;
			};
			for(var k in this.datas){
				if(this.datas[k]["onAll"])
					this.datas[k]["onAll"]();
				this.datas[k]=null;	
			};
			this.datas=null;
			this.subFun();
		}
	};	
};
$(document).ready(function(){
	window.__onload=window.document.body.onload;
	window.document.body.onload=function(){};
	__jhtcEndRunQuery(window.document,function(){
		for(var i=0;i<jhtc_onbeforeload_funs.length;i++){
			jhtc_onbeforeload_funs[i]();
		};
		jhtc_onbeforeload_funs=[];
		if(window.__onload!=null){
			window.__onload();
			window.__onload=function(){};
		}
		for(var i=0;i<jhtc_onload_funs.length;i++){
			jhtc_onload_funs[i]();
		};
		jhtc_onload_funs=[];
	});
});


document.attachEvent("onkeydown",function(){   
   if ((event.keyCode==116)||                  // F5 
       (event.ctrlKey && event.keyCode==82)){ //Ctrl + R   
       top._isDestroySession = false;
   }   
});
window.focus();
window.attachEvent("onbeforeunload", function(){
	if(top.sysLogout){
	//	top.sysLogout();
	}
});	