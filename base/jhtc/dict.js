function jhtc_select_dict_editor(kind,para){
	var editor={
		"sys_emp":"hbos/wage/emp/info/main.html",
		"acct_subj":"hbos/acct/sys/itemcode/main.html",
		"sys_dept":"hbos/sys/dicts/unitinfo/deptinfo/main.html",
		"budg_project":"hbos/sys/dicts/unitinfo/project/main.html",
		"sys_vendor_dict":"hbos/sys/dicts/unitinfo/ven/main.html",
		"acct_self_item":"hbos/acct/sys/check/checkitem/main.html",
		"sys_money_resource":"hbos/sys/basiccode/fundsource/main.html",
		"sys_store_dict":"hbos/sys/dicts/unitinfo/storeinfo/main.html",
		"sys_acc":"hbos/sys/accdicts/payouteconomydicts/main.html",
		"acct_cash_item":"hbos/acct/sys/cash/cashitem/main.html",
		"ctrl_pay_type":"hbos/payctl3/base/paytype/main.html",
		"sys_cur":"hbos/sys/basiccode/currency/main.html",
		"imma_equi_code_dict":"hbos/immaequip/settings/dict/equipdic/main.html",
		"acct_imma_equi_code_dict":"hbos/acct/immaequip/settings/dict/equipdic/main.html",
		"equi_code_dict":"hbos/equip/settings/dict/equipdic/main.html",
		"acct_equi_code_dict":"hbos/acct/equip/settings/dict/equipdic/main.html",
		"ven_code":"hbos/sys/dicts/unitinfo/ven/main.html",
		"sys_customer_dict":"hbos/pact/sys/custinfo/main.html"
	}; 
	if(editor[kind]){
		return 	{label:"�½�...",width:"850px",height:"500px",url:editor[kind]}
	}else
		return null;
};
function jhtc_select_dict_editor_init(element,topLine){
	element._editorUrl=jhtc_select_dict_editor(element.editor,element.para);
	if(element._editorUrl==null)
		return;
	var row=topLine.parentNode.insertRow(1);
	var cell=row.insertCell();
	cell.className="inputSelect_td";
	cell.innerText=element._editorUrl.label;
	cell.value="__editor_url__";
};

function jhtc_select_dict_editor_open(element){
	openDialog(window.prefix+element._editorUrl.url+"?load="+element.para,"dialogWidth:"+element._editorUrl.width+";dialogHeight:"+element._editorUrl.height+";");
}

function openDictPage(kind){
	var dialogProperty = jhtc_select_dict_editor(kind,"");
	openDialog(window.prefix+dialogProperty.url+"?load=","dialogWidth:800px;dialogHeight:600px;");	
};