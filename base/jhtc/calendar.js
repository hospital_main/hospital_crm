function jhtc_calendar(win,jhtc_obj){
	return jhtc_calendar_c(__jhtcElementFun(jhtc_obj));
}	
function jhtc_calendar_c(_e){
	var objListId=jhtcGetNextId();
	var iframeId=jhtcGetNextId();
	var _objListE=__jhtcElementFunById(objListId);
	var _iframeE=__jhtcElementFunById(iframeId);
	var vDisplay = 'true';
	var myDate=new Date();
	var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
	var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
	var date=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
	//定义inputName属性，即输入框的name，默认值为空。注意：在同一页中出现多个日期输入框，不能有重复的name！
	var overImgPath='/base/themes/blue/images/select/over_calendar.png';
	var outImgPath='/base/themes/blue/images/select/normal_calendar.png';
	var downImgPath='/base/themes/blue/images/select/normal_calendar.png';
	var blandImgPath='/base/themes/blue/images/select/blank.gif';
	var disabledImgPath='/base/themes/blue/images/select/disabled_calendar.png';
	var is_enabled=true;
	var proxyLabel=null;
  
	function putDisplay(){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		var flag=element.display;
		if( element==null)
			return;
		if(flag.toLowerCase()=="false"){
		    element.style.display = 'none';
		 }else if(flag.toLowerCase()=="true"){
	  		element.style.display = 'block';
		 }
		return;
	}
  
	function setEnabled(e){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		if(e==true){
		  	element.style.backgroundImage='url("'+outImgPath+'")';
			element.disabled=false;
			is_enabled=true;
		}else{
		  	element.style.backgroundImage='url("'+disabledImgPath+'")';
			is_enabled=false;
			element.disabled=true;
		}
	}
	function init(){
		var element=_e();
		element.attachEvent("onchange",check);    //给调用组件添加事件onchange
		var objList=_objListE();
		var iframe=_iframeE();
		if(element.label=="preTd"){
			var preTd=element.parentNode;
			while(preTd!=null){
				if(preTd.tagName.toLowerCase()=="td"){
					if(preTd.previousSibling!=null){
						proxyLabel=preTd.previousSibling.innerText;
						if(proxyLabel.indexOf(":")>0)
							proxyLabel=proxyLabel.split(":")[0];
						if(proxyLabel.indexOf("：")>0)
							proxyLabel=proxyLabel.split("：")[0];
					}
					break;
				}
				preTd=preTd.parentNode;
			}
			element.label=null;
		}else if(element.label=="preText"){
			var preTd=element.parentNode;
			var preT=preTd.firstChild;
			if(preT!=element)
				proxyLabel=preT.nodeValue.replace("：","").replace(":","");
			element.label=null;
		}else if(element.label!=null)
			proxyLabel=element.label;
		element.htcLabel=proxyLabel==null?"":proxyLabel;
		if(element.label != null) {
			if(element.labelFix=="true"){
			// -- label 的基本长度
				var lableLength = 120;
			//var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
			//if (spanWidth < lableLength) 
				var spanWidth = lableLength;
				element.insertAdjacentHTML("beforeBegin", "<span title='" + element.label + "' nowrap style='text-align:right;width:"+ spanWidth + "px;font-weight:normal;white-space: nowrap;text-overflow:ellipsis; overflow:hidden;'>"+element.label+"：</span>");
			}else{
				var lableLength = 120;
				var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
				if (spanWidth < lableLength) spanWidth = lableLength;
				element.insertAdjacentHTML("beforeBegin", "<span title='" + element.label + "' nowrap style='text-align:right;width:"+ spanWidth + "px;font-weight:normal;'>"+element.label+"：</span>");
			}
		}
		
		if (element.readOnly) {
			element.style.backgroundColor='#EDEDED'
			element.style.fontWeight = 'bold'
		} else if ('true'==element.required && element.className.indexOf("vouchCal")==-1) {
			element.style.backgroundColor=JHTC_COLOR.REQUIRED
		}
		
		element.parentNode.noWrap = true;
		
		
		with(element){
			style.textAlign = "left";
			//wsj相对于1。20来说,增加了一个属性dateadd，可以对年，月的默认值进行增加或减少
			//如果想增加1月(M)，则格式为  defaultvalue="true" dateadd="M-1",同样，年defaultvalue="true" dateadd="Y-1"
			if(element.defaultvalue!=null && trim(element.defaultvalue.toLowerCase())!="false" && trim(element.value)=='')	  {
				if(element.dateadd!=null){	  
					var flag,num;
					element.dateadd=element.dateadd.toLowerCase(); 
					flag=element.dateadd.substring(0,1); 
					num=Number( element.dateadd.substring(1));   
					if(flag=="m"){   
						if(num>12 || num<-12) alert('月增减不能超过12');
							else if(month+num<=0){ month=month+num+12; year=year-1;}
							else if(month+num>12){month=month+num-12; year=year+1;}
						  	else  month=month+num;  
					}
					if(flag=="y"){ 
						if(num>5 || num<-5) alert('年增减不能超过5');
						else year=year+num; 
					} 
				}
			
				var maxDay = 31;
				if(month == 2){
					if(((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
						maxDay = 29;
					else
						maxDay = 28;
				}else if(month == 4 || month == 6 || month == 9 || month == 11){
					maxDay = 30;
				}
				
				if(date > maxDay)
					date = maxDay; 
				var month2='000'+month;
				month2=month2.substr(month2.length-2);
				
				if(element.defaultvalue.toLowerCase()=="monthbegin")
					value=year+"-"+month2+"-01";
				else if(element.defaultvalue.toLowerCase()=="monthend")
					value=year+"-"+month2+"-"+maxDay;
				else{
					var md='00'+date;
					md=md.substring(md.length-2);
					value = year+"-"+month2+"-"+md;
				}
				value=value+getTimeStr();
			}
			//wsj     
			maxLength = 10;
			onmousedown =_onmousedown;
			onkeydown=alterEnter;
		}
		element.style.backgroundImage='url("'+outImgPath+'")';
		if (element.offsetHeight >0) {// 没隐藏的
			with(element){
				//style.width = style.height = (element.offsetHeight-3)+"px";
				onclick = displayDate;
				onmouseover = overBtn;
				onmouseout = outBtn;
				onmousemove=moveBtn;
				onblur=evtOnBlur;
			}
		}
		
		iframe=element.document.createElement("iframe");
		with(iframe){
			id=iframeId;
			style.position = "absolute";
			style.display = "none";
			style.textAlign = "center";
			style.backgroundColor = "#F6F6F6";
			className = "ds_font";
			style.zIndex =8;
			width="100"
			style.overflow = "visible";
			border="0";
		}
		iframe = element.parentNode.appendChild(iframe);
		if(element.hastime!="false"){
			element.style.width="143";
			element.maxLength=19;
			iframe.style.width="140";
		}
		
		//window.attachEvent("onresize", adjPosition);
		//adjPosition();
		//window.setTimeout(adjPosition,50);
		//window.setTimeout(adjPosition,200);
		//window.setTimeout(adjPosition,400);
	}
	function _onmousedown(){
		var element=_e();
		element.focus();	
	}
	function evtOnBlur(){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		if(element.value!=''){
			var objSrc = window.event.srcElement;
			if (element.contains(objSrc) || objList.contains(objSrc) ) {
				return;
			} 
			check();
		}
	
	}
  function overBtn(){
  	var element=_e();
	var objList=_objListE();
	var iframe=_iframeE();
    element.style.backgroundImage='url("'+overImgPath+'")';  
  }
  function moveBtn(){
  	var element=_e();
	var objList=_objListE();
	var iframe=_iframeE();
    if(event.offsetX>72){
      if(element.style.cursor!="hand"){
        element.style.cursor="hand";
      }
    }else{
      if(element.style.cursor!="text"){
        element.style.cursor="text";
      }
    }
  }
  function outBtn(){ //鼠标移出Btn 或者input时的样式
  	var element=_e();
	var objList=_objListE();
	var iframe=_iframeE();
	element.style.backgroundImage='url("'+outImgPath+'")';
  }
  function alterEnter(){
  	var element=_e();
	var objList=_objListE();
	var iframe=_iframeE();
	if(event.keyCode==13){
		event.keyCode=9;
	}else{
		//displayDate();
	}
	try{
		objList.style.display="none";
		iframe.style.display=objList.style.display;
	}catch(e){}
  }
  //显示下拉选择日期
	function displayDate(){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		if(objList!=null && objList.style.display != 'none'){
			objList.style.display = 'none';
			iframe.style.display=objList.style.display 
			window.document.detachEvent("onmousedown",clickDocument);
			return;
		}
		
		var datetime=trim(element.value)
		if (datetime == "") {
			element.value = "";
		}
		clickElement();
		if(element.value!=""){
			var d=trim(element.value).split(" ")[0].split("-");
			if(d.length==3){
				year=d[0];
				month=d[1];
				date=d[2];
			}
		}
		var option = ((trim(element.value)=='')?(year+'-'+month+'-'+date):trim(element.value)).split(" ")[0].split("-");
		if(isNaN(parseInt(option[0]))){
			element.value="";
			return;
		}
		
		//设置缺省日期(年月不全的情况下，默认当年的1月1日)
		option = setDefaultDate(option);
		
		try{
		if(objList!=null){
			var myObj=objList.childNodes[0].cells[2].childNodes
			myObj[0].innerHTML = (option[0]);
			myObj[2].innerHTML = (option[1]);
			dateShow(eval(option[0]),eval(option[1]));
			objList.style.display = '';
			iframe.style.display=objList.style.display 
			return;
		}
		}catch(e){}

		var week=new Array('日','一','二','三','四','五','六');
		objList = element.document.createElement("div");
		with(objList){
			id=objListId;
			style.position = "absolute";
			style.display = "none";
			style.textAlign = "center";
			style.width = 142;
			style.height = 165;
			style.backgroundColor = "#F6F6F6";
			style.border = "1px solid #245B7D";
			className = "ds_font";
			style.zIndex =10;
			style.overflow = "visible";
			onselectstart = function(){return false;}
		}
		objList = element.parentNode.appendChild(objList);
		
		
		//第一行，头
		table_1 = element.document.createElement("table");
		with(table_1){
			cellpadding = 0;
			cellspacing = 1;
			style.width = 140;
			style.height = 20;
			style.backgroundColor = "#CEDAE7";
		}
		table_1 = objList.appendChild(table_1);
		tr = table_1.insertRow();
		tr.align = "center";
		//减小年份
		td = tr.insertCell();
		with(td){
			style.cursor = "hand";
			style.width = "11%";
			onmouseover = function(){className='ds_border'}
			onmouseout = function(){className=''}
			onclick = function(){subYear()}
			title = '减小年份';
			innerHTML = "&lt;&lt;";
		}
		//减小月份
		td = tr.insertCell();
		with(td){
			style.cursor = "hand";
			style.width = "11%";
			onmouseover = function(){className='ds_border'}
			onmouseout = function(){className=''}
			onclick = function(){subMonth()}
			title = '减小月份';
			innerHTML = "&lt;"
		}
		//编辑年月
		td = tr.insertCell();
		td.width = "56%";
		td.noWrap = "true";
		try{
			b = element.document.createElement("<b id='yearValue'></b>");
			b.innerText = option[0];//xujm
			td.appendChild(b);
			
			b = element.document.createElement("<b></b>");
			b.innerText = "年";
			td.appendChild(b);
			b = element.document.createElement("<b id='monthValue'></b>");
			b.innerText = option[1];
			td.appendChild(b);
			b = element.document.createElement("<b></b>");
			b.innerText = "月";
			td.appendChild(b);
			//增加月份
			td = tr.insertCell();
			with(td){
			  style.cursor = "hand";
			  style.width = "11%";
			  onmouseover = function(){className='ds_border'}
			  onmouseout = function(){className=''}
			  onclick = function(){addMonth()}
			  title = '增加月份';
			  innerHTML = "&gt;";
			}
			//增加年份
			td = tr.insertCell();
			with(td){
			  style.cursor = "hand";
			  style.width = "11%";
			  onmouseover = function(){className='ds_border'}
			  onmouseout = function(){className=''}
			  onclick = function(){addYear()}
			  title = '增加年份';
			  innerHTML ="&gt;&gt;";
		   } 
		}catch(e){}
		//第二行，汉字
		table_2 = element.document.createElement("table");
		with(table_2){
			cellpadding = 0;
			cellspacing = 0;
			style.width = 140;
			style.height = 20;
		}
		table_2 = objList.appendChild(table_2);
		tr = table_2.insertRow();
		tr.align = "center";
		for(i=0;i<7;i++){
			td = tr.insertCell();
			td.innerText = week[i];
		}
		table_3 = element.document.createElement("table");
		with(table_3){
			cellpadding = 0;
			cellspacing = 2;
			style.width = 140;
			style.backgroundColor = "#EEEEEE";
		}
		//第三行，选择日期
		table_3 = objList.appendChild(table_3);
		for(i=0;i<6;i++){
			tr = table_3.insertRow();
			tr.align = "center";
			for(j=0;j<7;j++){
				td = tr.insertCell();
				with(td){
					style.cursor = "hand";
					style.width = "10%";
					style.height = 16;
					onmouseover = function(){
					                if(innerText!='' && className!='ds_border2')
					                  className='ds_border'
					              }
					onmouseout = function(){
					               if(className != 'ds_border2')
									  className=''
					             }
					onclick = function(){getValue(this)}
				}
			}
		}
		span = element.document.createElement("div");
		var timeHtml="<table border='0' cellpadding = '0' cellspacing = '0'><tr><td><select id='"+element.name+"_hh' >";
		for(var i=0;i<24;i++){
			var v="00"+i;
			timeHtml+="<option value='"+v.substr(v.length-2)+"'>"+v.substr(v.length-2)+"</option>";	
		}
		timeHtml+="</select><td>:</td><td><select id='"+element.name+"_mm' >";
		for(var i=0;i<60;i++){
			var v="00"+i;
			timeHtml+="<option value='"+v.substr(v.length-2)+"'>"+v.substr(v.length-2)+"</option>";	
		}
		timeHtml+="</select><td>:</td><td><select id='"+element.name+"_ss' >";
		for(var i=0;i<60;i++){
			var v="00"+i;
			timeHtml+="<option value='"+v.substr(v.length-2)+"'>"+v.substr(v.length-2)+"</option>";	
		}
		timeHtml+="</select></td></tr></table>";
		if(element.hastime!="false"){
			objList.appendChild(span);
			span.innerHTML=timeHtml;
			var tiValue=trim(element.value).split(" ");
			if(tiValue.length>1){
				var hhfun=function(){
					element.value=trim(element.value).split(" ")[0]+" "+element.document.getElementById(element.name+"_hh").value+":"+element.document.getElementById(element.name+"_mm").value+":"+element.document.getElementById(element.name+"_ss").value;	
				}
				tiValue=tiValue[1].split(":");
				var hhobj=element.document.getElementById(element.name+"_hh");
				hhobj.value=tiValue[0];
				hhobj.onchange=hhfun;
				hhobj=element.document.getElementById(element.name+"_mm");
				hhobj.value=tiValue[1];	
				hhobj.onchange=hhfun;
				hhobj=element.document.getElementById(element.name+"_ss");
				hhobj.value=tiValue[2];	
				hhobj.onchange=hhfun;
			}
		}
		
		span = element.document.createElement("span");
		with(span){
			style.cursor = "hand";
			onclick = function(){
				element.value =getCurrentDate();// tempyear + "-" + tempmonth + "-" + tempdate;
				objList.style.display = 'none';
				iframe.style.display=objList.style.display 
				__jhtcDispatchEvent(element,"onchange");
				window.document.detachEvent("onmousedown",clickDocument);
			}
			var cd=getCurrentDate();
			cd=cd.replace("-","年");
			cd=cd.replace("-","月");
			cd="今天:"+cd+"日";
			innerText = cd;//"今天:" + year + "年" + month + "月" + date + "日";
		}
		
		objList.appendChild(span);
		dateShow(eval(option[0]),eval(option[1]));
		adjPosition2()
		objList.style.display = 'block'
		iframe.style.display=objList.style.display
	}
	function adjPosition2(){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		var form = element
		var elementTop=0, elementLeft=0;
		
		while(form.tagName != "BODY"&&form.tagName != "DIV") {
			elementTop = elementTop + form.offsetTop + form.clientTop;
			elementLeft = elementLeft + form.offsetLeft+form.clientLeft;
			form = form.offsetParent;
		}
		
		if(objList != null){
			with (objList.style) {
			left = elementLeft;
			if(elementTop+element.offsetHeight-1+parseInt(objList.style.height.replace(/px/,'')) > window.document.body.offsetHeight)
			  top = elementTop - parseInt(objList.style.height.replace(/px/,''))-1
			else
			    top = elementTop + element.offsetHeight-1;
			}
			with (iframe.style) {
				left = elementLeft;
				top=objList.style.top;
				window.setTimeout(function(){
						width=objList.clientWidth+2;
						height=objList.clientHeight;
			    },10);
			}
		}
	}
  	//减小年份
	function subYear(){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		var myObj=objList.childNodes[0].cells[2].childNodes
		myObj[0].innerHTML=eval(myObj[0].innerHTML)-1;
		dateShow(eval(myObj[0].innerHTML),eval(myObj[2].innerHTML))
	}

	//增加年份
	function addYear(){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		var myObj=objList.childNodes[0].cells[2].childNodes
		myObj[0].innerHTML=eval(myObj[0].innerHTML)+1;
		dateShow(eval(myObj[0].innerHTML),eval(myObj[2].innerHTML))
	}

	//减小月份
	function subMonth(){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		var myObj=objList.childNodes[0].cells[2].childNodes
		var month=eval(myObj[2].innerHTML)-1;
		if(month==0){
			month=12;
			subYear();
		}
		myObj[2].innerHTML=month;
		dateShow(eval(myObj[0].innerHTML),eval(myObj[2].innerHTML))
	}

	//增加月份
	function addMonth(){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		var myObj=objList.childNodes[0].cells[2].childNodes
		var month=eval(myObj[2].innerHTML)+1;
		if(month==13){
			month=1;
			addYear();
		}
		myObj[2].innerHTML=month;
		dateShow(eval(myObj[0].innerHTML),eval(myObj[2].innerHTML))
	}

	//显示各月份的日
	function dateShow(year,month){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		//设置题头为录入的年月
		var myDate=new Date(year,month-1,1);
		var today=new Date();
		var day=myDate.getDay();
		var selectDate=trim(element.value).split(" ")[0].split('-');
		var length;
		switch(month){
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				length=31;
			break;
			case 4:
			case 6:
			case 9:
			case 11:
				length=30;
			break;
			case 2:
			if(isLeapYear(year))
				length=29;
			else
				length=28;
		}
		var obj = objList.childNodes[2];
		for(i=0;i<obj.cells.length;i++){
			obj.cells[i].innerHTML='';
			obj.cells[i].style.color='';
			obj.cells[i].className='';
		}
		for(i=0;i<length;i++){
			obj.cells[i+day].innerHTML=(i+1);
			if(year==today.getFullYear() && (month-1)==today.getMonth() && (i+1)==today.getDate())
				obj.cells[i+day].style.color='red';
			if(year==eval(selectDate[0]) && month==eval(selectDate[1]) && (i+1)==eval(selectDate[2]))
				obj.cells[i+day].className='ds_border2';
				obj.cells[i+day].style.background='';
			if(year==selectDate[0] && month==selectDate[1] && (i+1)==selectDate[2])
				obj.cells[i+day].style.background='#99CCFF';
		}
	}

	//把选择的日期传给输入框
	function getValue(obj){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		var myObj=objList.childNodes[0].cells[2].childNodes;
		var mday='00'+obj.innerHTML;
		var mmon='00'+myObj[2].innerHTML;
		mmon=mmon.substring(mmon.length-2);
		mday=mday.substring(mday.length-2);
		if(obj.innerHTML){
			element.value=myObj[0].innerHTML+"-"+mmon+"-"+mday;
		  if(element.hastime!="false")
		  	element.value=myObj[0].innerHTML+"-"+mmon+"-"+mday+" "+element.document.getElementById(element.name+"_hh").value+":"+element.document.getElementById(element.name+"_mm").value+":"+element.document.getElementById(element.name+"_ss").value;
		}
		objList.style.display = 'none';
		iframe.style.display=objList.style.display
		__jhtcDispatchEvent(element,"onchange");
		window.document.detachEvent("onmousedown",clickDocument);
	}
	function clickDocument(){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		var objSrc
		objSrc = window.event.srcElement;
		//if(element==objSrc)return;
		if(objList.contains(objSrc)) return;
		objList.style.display='none';
		iframe.style.display=objList.style.display 
		window.document.detachEvent("onmousedown",clickDocument);
	}

	function clickElement(){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		window.document.attachEvent("onmousedown",clickDocument);
	}
	function getTimeStr(){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		if(element.hastime=="0")
	   		return " 00:00:00";
	   	if(element.hastime=="1"){
	   		v=" ";
	   		var td=new Date();
	   		var tt="00"+td.getHours();
	   		v+=tt.substr(tt.length-2);
	   		tt="00"+td.getMinutes();
	   		v+=":"+tt.substr(tt.length-2);
	   		tt="00"+td.getSeconds();
	   		v+=":"+tt.substr(tt.length-2);	
	   		return v;
	   	}
	   	return  "";
	}

	//检查
	function check(){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		var date = new Date()
		var mmon='00'+(date.getMonth()+1);
		mmon=mmon.substring(mmon.length-2);
		var mday='00'+date.getDate();
		mday=mday.substring(mday.length-2);
		var str = date.getFullYear()+'-'+mmon+'-'+mday+getTimeStr();
		var emValue=trim(element.value);
		var ttiValue=[],tiValue="";
		
		if(element.hastime!="false"&&emValue.split(" ").length>1)
			ttiValue=emValue.split(" ")[1].split(":");
		if(element.hastime!="false")
			emValue=emValue.split(" ")[0];
			
		if(ttiValue.length>0&&element.hastime!="false"){
			if(ttiValue.length!=3){
				alert("时间格式不正确!");
				return false;
			}	
			var h=parseInt(ttiValue[0],10);
			var m=parseInt(ttiValue[1],10);
			var s=parseInt(ttiValue[2],10);
			if(isNaN(h)||isNaN(m)||isNaN(s)){
				alert("时间格式不正确!");
				return false;
			}
			if(h<0||h>23){
				alert("小时应在0-23之间!");
				return false;
			}
			if(m<0||h>59){
				alert("分钟应在0-59之间!");
				return false;
			}
			if(s<0||s>59){
				alert("秒应在0-59之间!");
				return false;
			}
			h="00"+h;
			m="00"+m;
			s="00"+s;
			tiValue=" "+h.substr(h.length-2)+":"+m.substr(m.length-2)+":"+s.substr(s.length-2);
		}
		
		if ('true'==element.required) {
			if (window.trim(emValue) == '')  {
				alert((proxyLabel==null?'此项':proxyLabel)+"必输")
				element.value = str+tiValue;
				element.focus()
				element.select()
				return false
			}
		}
		
		var datetime=emValue
		var year, month, day;
		var gone,gtwo;
		if (trim(datetime)!="") {
			var sep="";
			if(datetime.indexOf(".")>0)
				sep=".";
			else if(datetime.indexOf("-")>0)
				sep="-"
			else if(datetime.indexOf("/")>0)
				sep="/";
			if(sep==""&&datetime.length==8){
				sep="-";
				datetime=datetime.substr(0,4)+"-"+datetime.substr(4,2)+"-"+datetime.substr(6,2);
			}
			
			if(sep==""){
				alert("请按格式yyyy-mm-dd输入日期");
				element.value = str+tiValue
				element.focus()
				element.select()
				return false;
			}
			
			option = datetime.split(sep);	
			if(datetime.length>10 || datetime.length<8 || option.length!=3 || isNaN(option[0]) || isNaN(option[1]) || isNaN(option[2])){
				alert("请按格式yyyy-mm-dd输入日期");
				element.value = str+tiValue
				element.focus()
				element.select()
				return false;
		  }
		year = option[0];
		month = option[1];
		day = option[2];
		if(year<1900||year>3000){
			alert("年份必须在1900和3000年之间");
			element.value = str+tiValue
			element.focus();
			element.select()
			return false;
		}
		if(month<1||month>12) {
			alert("月份必须在01和12之间!");
			element.value = str+tiValue
			element.focus();
			element.select()
			return false;
		}
		if(day<1||day>31){
			alert("日期必须在01和31之间!");
			element.value = str+tiValue
			element.focus();
			element.select()
			return false;
		}else{
			if(month==2){
				if(isLeapYear(year) && day>29){
					alert("二月份日期必须在01到29之间!");
					element.value = str+tiValue
					element.focus();
					return false;
				}
				if(!isLeapYear(year) && day>28){
					alert("二月份日期必须在01到28之间!");
					element.value = str+tiValue
					element.focus();
					return false;
				}
			}
			if((month==4 || month==6 || month==9 || month==11)&&(day>30)){
				alert("在四，六，九，十一月份 \n日期必须在01到30之间!");
				element.value = str+tiValue
				element.focus();
				return false;
			}
		}
		//LZK ADD BEGIN 
		var curd,maxd,mind;
		var maxds,minds;
		curd=new Date(emValue.replace(/-/g, "\/"));
		if(element.maxdate!=null){
			if(element.maxdate.substr(0,11)=="javascript:")
			  	maxds=eval(element.maxdate.substr(11));
			else
			  	maxds=element.maxdate;
			maxd=new Date(maxds.replace(/-/g, "\/"));
			if((curd.getTime()-maxd.getTime())>0){
				alert("日期值不能大于 "+maxds);
				element.value = maxds+tiValue
				element.focus();
				element.select()
				return false;
			}
		}
		if(element.mindate!=null){
			if(element.mindate.substr(0,11)=="javascript:")
			  	minds=eval(element.mindate.substr(11));
			else
			  	minds=element.mindate;
			mind=new Date(minds.replace(/-/g, "\/"));
			if((curd.getTime()-mind.getTime())<0){
				alert("日期值不能小于 "+minds);
				element.value = minds+tiValue
				element.focus();
				element.select()
				return false;
			}
		}
		if(element.greatthan!=null){
			minds=eval(element.greatthan+".value");
			if(minds!=''){
				mind=new Date(minds.replace(/-/g, "\/"));
				if((curd.getTime()-mind.getTime())<0){
					alert("日期值不能小于 "+minds);
					element.value = minds+tiValue
					element.focus();
					element.select()
					return false;
				}
			}
		}
		
		//LZK ADD END
		year='0000'+year;year=year.substring(year.length-4)
		month='00'+month;month=month.substring(month.length-2)
		day='00'+day; day=day.substring(day.length-2)
		element.value = year+'-'+month+'-'+day;
		if(element.hastime!="false"){
		 	element.value=year+'-'+month+'-'+day+tiValue;
		}
		  }
		
		if (trim(element.required)=='true' && trim(emValue)=='') {
			var myDate=new Date();
			year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
			month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
			day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
			year='0000'+year;year=year.substring(year.length-4)
			month='00'+month;month=month.substring(month.length-2)
			day='00'+day; day=day.substring(day.length-2)
			element.value=year+'-'+month+'-'+day
			if(element.hastime!="false"){
				element.value=year+'-'+month+'-'+day+tiValue;
			}
		} 
		return true;
	}

	//是否是润年
	function isLeapYear(year){
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		if((year%4==0 && year%100!=0) || (year%400==0))
			return true;
		return false;
	}
  　
	// 调整位置
	function adjPosition() {
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		// 取得element的绝对位置
		var form = element
		var elementTop=0, elementLeft=0;
		
		while(form.tagName != "BODY"&&form.tagName != "DIV") {
			elementTop = elementTop + form.offsetTop + form.clientTop;
			elementLeft = elementLeft + form.offsetLeft+form.clientLeft;
		form = form.offsetParent;
		}
	}
	function checkDate(){ //将手工输入的yymmdd格式转化为yy-mm-dd格式
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		if (window.trim(element.value) == '')  {
			element.value = "";
			return;
		}
		var date = new Date()
		var str = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
		
		var year,month,day;
		var date=event.srcElement.value
		var datetime=trim(date)
		option = datetime.split("-");
		if(option.length==1||option.length==3){
			if(option.length==1){
				if(option[0].length!=8){
					alert("请按yyyymmdd或yyyy-mm-dd方式输入日期！");
					element.value = str;
					// element.focus();
					return false;
				}
				var obj=new Array()
				obj[0]=datetime.substring(0,4)
				obj[1]=datetime.substring(4,6)
				obj[2]=datetime.substring(6,8)
				if(isNaN(obj[0]) || isNaN(obj[1]) || isNaN(obj[2])){
					alert("请输入数字");
					element.focus();
					return false;
				}
				year = obj[0];
				month = obj[1];
				day = obj[2];
			}
			else{
				year=option[0]
				month=option[1]
				day=option[2]
			}
			if(year<1900||year>3000){
				alert("年份必须在1900和3000年之间");
				event.srcElement.focus();
				return false;
			}
			if(month<1||month>12) {
				alert("月份必须在01和12之间!");
				event.srcElement.focus();
				return false;
			}
			if(day<1||day>31){
				alert("日期必须在01和31之间!");
				event.srcElement.focus();
				return false;
			}
			else{
				if(month==2){
					if(isLeapYear(year) && day>29){
						alert("二月份日期必须在01到29之间!");
						event.srcElement.focus();
						return false;
					}
					if(!isLeapYear(year) && day>28){
						alert("二月份日期必须在01到28之间!");
						event.srcElement.focus();
						return false;
					}
				}
				if((month==4 || month==6 || month==9 || month==11)&&(day>30)){
					alert("在四，六，九，十一月份 \n日期必须在01到30之间!");
					event.srcElement.focus();
					return false;
				}
			}
		}
		else {
		  	alert("请按yyyymmdd或yyyy-mm-dd方式输入日期！");
		}
		__jhtcDispatchEvent(element,"onchange");
		event.srcElement.value=year+"-"+month+"-"+day
	}

	// 刷新页面
	function refresh() {
		var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
		if (trim(element.required.toLowerCase())=="true") {
			element.style.backgroundColor=JHTC_COLOR.REQUIRED;
		} else {
		  	element.style.backgroundColor="#FFFFFF";
		}
	}
	
		//设置缺省日期(年月不全的情况下，默认当年的1月1日)
	  function setDefaultDate(oriDate) {
	  	var element=_e();
		var objList=_objListE();
		var iframe=_iframeE();
	  	var destDate = oriDate;
	  	if (destDate.length == 1) {
	  		destDate[1] = '01';
	  		destDate[2] = '01';
	  	} else if (oriDate.length == 2) {
	  		destDate[2] = '01';
	  	}
	  	return destDate
	  }

	function _initAttr(){
		var element=_e();
	  	jhtc_attr_init(element,"required","false");
	  	jhtc_attr_init(element,"label",null);
	  	__parseDefaultValue(element,"false")
	  	
	  	jhtc_attr_init(element,"dateadd",null);
	  	jhtc_attr_init(element,"maxdate",null);
	  	jhtc_attr_init(element,"mindate",null);
	  	
	  	jhtc_attr_init(element,"greatthan",null);
	  	jhtc_attr_init(element,"hastime","false");
	  	jhtc_attr_init(element,"display","");
	  	jhtc_attr_init(element,"labelFix","false");
	  	 	
	  	
	  	__jhtcBindPropertyChange(element,"display",putDisplay);
		element.setEnabled=setEnabled;
		element.check=check;
		element.refresh=refresh;
		element.init=init;
	}
	_initAttr();
	_e().jhtcInit=function(){
		_e().init();
	};
	return null;
};
jhtc_class_map["inputCalendar"]=jhtc_calendar;