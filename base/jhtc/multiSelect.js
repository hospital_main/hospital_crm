function jhtc_multiSelect(win,jhtc_obj){
  var pageWin=win;
  var element=jhtc_obj;
  var objMultiSelect = null;
  var vText = "";
  function init2(){
    objMultiSelect.style.visibility="visible";
  }
  function setValue(val){
  	/*
  		参数格式:'001','002','003'
  	*/
  	var tempval=val;
    val = val.replace(/\'/g,"");

  	if(objMultiSelect && objMultiSelect.firstChild){//
  		if(typeof(val) == "undefined") return;
  		var objArraySetItemValue = val.split(",");
  		  		
  		element.value = "";
  		element.text = "";
  		var valLength = objArraySetItemValue.length;
  		var optionCnt = objMultiSelect.children.length;
  		if(element.isMultiSelect && element.isMultiSelect.toString() == "false" && valLength > 1){
  			valLength = 1;
  		}
  		//var strSecInputVal = "";
  		for(var i=0;i<optionCnt;i++){//option 遍历
  			if(objMultiSelect.children[i].selected){
  				objMultiSelect.children[i].selected = false;
  			}
  			for(var j=0;j<valLength;j++){//参数值遍历

  				if(objMultiSelect.children[i].value == objArraySetItemValue[j]){
  					objMultiSelect.children[i].selected = "true";
  					//strSecInputVal += objMultiSelect.childNodes[i].value + ",";
  					break;	
  				}
  			}//End of 参数值遍历
  		}//End of option 遍历
  		//element.value = strSecInputVal.substring(0,strSecInputVal.length - 1);
  		element.value = tempval;
  	}
  }

  function init(){// 组件初始化
  	
  	if (trim(element.name) == '') {
        alert("请设置MultiSelect的name")
        return;
      }
      if(vSize < 2){
  		alert("请设置正确的size属性值");
  		return;
      }
    
    if (element.label != null) {
      // -- label 的基本长度
      var lableLength = 120
      var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
      if (spanWidth < lableLength) spanWidth = lableLength;
  		
  	  var vWidth = 100;	
  	  if(element.extent) vWidth = element.extent;
  	  var vSize = 15;
  	  if(element.line!=null) vSize = element.line;

  	  var booleanDisplay = true;
  	  if(element.display && element.display.toString() == "false"){
  		booleanDisplay = false;
  	  }
  	  
      var aKey = ""
      if ((element.accessKey) != '' && !element.readOnly)
        aKey = "(<span style='font-family:" + element.currentStyle.fontFamily + ";font-size:" + element.currentStyle.fontSize + ";'>)"
      var objTable = window.document.createElement("<table cellspacing='0' cellpadding='0'>");
      var objTr = objTable.insertRow();
      var objTDLeft = objTr.insertCell();
      with(objTDLeft){
    		align = "right";
    		vAlign = "top";
    		noWrap = "true";
    		if(element.label=="")
    			innerText = element.label + aKey + "　";
    		else
    			innerText = element.label + aKey + "：";
      }
      
      var objTDRight = objTr.insertCell();
      with(objTDRight){
    		valign = "top";
    		noWrap = "true";
      }
      if(!element.isMultiSelect || element.isMultiSelect.toString() == "true" || element.isMultiSelect.toString() != "false"){
		    objMultiSelect = window.document.createElement("<select id='"+element.name+"Select' MULTIPLE='true' style='width:" + vWidth + ";visibility:hidden;' size='" + vSize + "'>");
      }else{
		    objMultiSelect = window.document.createElement("<select id='"+element.name+"Select' style='width:" + vWidth + ";visibility:hidden;' size='" + vSize + "'>");
      }
      if(!booleanDisplay){// 不显示
    		objMultiSelect.style.display = "none";
    		objTDLeft.innerText = "";
      }
      objMultiSelect = objTDRight.appendChild(objMultiSelect);
      objMultiSelect.onchange = setSelectVal;
      objMultiSelect.ondblclick = setSelectText;
        
      initList();
      
      element.style.visibility = "hidden";
      element.insertAdjacentElement("beforeBegin",objTable);
      element.value = "";
      
    }
    
    if (element.readOnly) {
      element.style.backgroundColor='#EDEDED'
      element.style.fontWeight = 'bold'
    } else if ('true'==element.required) {
      element.style.backgroundColor='#DBFCFF'
    }
  }
  
  function initList() {
    var loadXML = "";
    if (trim(element.para)=="") {
      loadXML = window.getDict(element.load);
    } else {
      loadXML = window.getDict(element.load,element.para);
    }  
    
    objMultiSelect.innerHTML = "";

    if(loadXML && loadXML.xml != ""){
  		var objParaList = loadXML.selectNodes("//para");
  		var objOptionNode;
  		if(objParaList.length){
  			objOptionNode = window.document.createElement("<option>");
  			objOptionNode.value = "#";
  			objOptionNode.innerText = "";
  			objOptionNode.selected = true;
  			objMultiSelect.appendChild(objOptionNode);
  			for(var i=0;i<objParaList.length;i++){
  				objOptionNode = window.document.createElement("<option>");
  				with(objOptionNode){
  					value = objParaList[i].getAttribute("code");
  					innerText = objParaList[i].getAttribute("value");
  				}
  				objMultiSelect.appendChild(objOptionNode);
  			}
  		}
    } 
  }
  
	function setSelectVal(){ //设置用户选定的值 格式:001,002,003
		var strSelectVal = "";
		var strSelectText = "";
		if(objMultiSelect && objMultiSelect.firstChild){
			var optionCnt = objMultiSelect.childNodes.length;
			var num = 0;
			for(var i=0;i<optionCnt;i++){
				if(objMultiSelect.childNodes[i].selected && objMultiSelect.childNodes[i].value != "#"){
				  num ++;
				  if (element.maxflag=="true" && num > element.maxselect) {
				    alert("选择项目不能超过"+element.maxselect+"项！");
			      break;
				  }
					strSelectVal += "'" + objMultiSelect.childNodes[i].value + "',";
					strSelectText += "'" + objMultiSelect.childNodes[i].innerText + "',";
				}
			}
		}
		element.value = strSelectVal.substring(0,strSelectVal.length - 1);
		vText = strSelectText.substring(0,strSelectText.length - 1);
		element.text=vText;
		if(element.onchange2 !=null)
		eval(element.onchange2);
	}
	
  function setSelectText(){
    try {
    	vText = "'"+objMultiSelect.children[objMultiSelect.selectedIndex].innerText+"'";
    	//evtOnDblClick.fire();
    	__jhtcDispatchEvent(element,"ondblclick");
    } catch (e) {}
  }

  function getText(){
	  return element.text = vText;
  }
  function getXMLDocumentInst(){//获取一个XML DOM实例
  	var objReturnXMLDom;
  	objReturnXMLDom = new ActiveXObject("MSXML2.DOMDocument");
  	objReturnXMLDom.async = false;
  	objReturnXMLDom.setProperty("ServerHTTPRequest",true);
  	objReturnXMLDom.setProperty("SelectionLanguage", "XPath");
  	return objReturnXMLDom;
  }
  
  //检查数据正确性
  function check(){
  	if(element.required.toString() == "true"){
  		if (window.trim(element.value) == '')  {
        if (element.label != null) {
          alert(element.label + "必输");
        }
        return false;
      }
  	}
  	return true;
  }
  
  // 刷新页面
  function refresh() {
  	initList();
  }
    //查询复选框返回值
    window.xmlhttp.post("sysDictsUnitinfoSysParas_select1","<para_code>0109</para_code>", '?isCheck=false');
  	var vid=window.xmlhttp._object.responseXML.getElementsByTagName("td")[0].text;
    jhtc_attr_init(element,"label",null);
  	jhtc_attr_init(element,"load",null);
  	jhtc_attr_init(element,"para","");
  	jhtc_attr_init(element,"code","true");
  	jhtc_attr_init(element,"width",null);
  	jhtc_attr_init(element,"isMultiSelect",null);
  	jhtc_attr_init(element,"required","false");
  	jhtc_attr_init(element,"line","3");
  	jhtc_attr_init(element,"display","true");
    jhtc_attr_init(element,"maxselect",vid); //获取复选框返回的值 
  	jhtc_attr_init(element,"maxflag","true");
  	jhtc_attr_init(element,"text","");
		jhtc_attr_init(element,"extent","100");
  	jhtc_attr_init(element,"onchange2",null);
  	__jhtcBindPropertyChange(element,"vText",getText);
    
    
    
  	
  	
  	
	element.setValue=setValue;
	element.check=check;
	element.refresh=refresh;
	element.init2=init2;
	element.init=init;
	element.jhtcInit=function(){
		element.init();
		element.init2();	
	};
	return null;
};
jhtc_class_map["inputMultiSelect"]=jhtc_multiSelect;