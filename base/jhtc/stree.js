function jhtc_stree(win,jhtc_obj){
		var pageWin=win;
		var element=jhtc_obj;
		var SELECT_BG_COLOR="#9999FF";
		var MOUSEOVER_BG_COLOR="#009999";
		var WIDTH_HEIGHT=" width='19' height='16' ";
		var MAX_COL=100;
		var TR_STRAT_INDEX=1;
		var oldSelectedTreeItemSpan=null;
		var oldSelectedRadioImg=null;
		var treeItemDataSource="";
		var treeItemDefaultPara="";
		
		function init() {
			if(element.tagname==null){
				if(element.kind=="check")
					element.tagname="CHECKBOX_VALUE";
				if(element.kind=="radio")
					element.tagname="RADIOBOX_VALUE";
			}
		}
		function refresh(){
			oldSelectedTreeItemSpan=null;
			oldSelectedRadioImg=null;
			var vXml =getTreeDom();
    		if(vXml==null)
    			return;
    		var trs=vXml.getElementsByTagName("tr");
    		if(trs.length<1)
    			return ;
    		var tempSbliding=new Array();
    		var resultHtml="<table id='_mainDataTable'  border='0' cellpadding='0' cellspacing='0'>";
    		resultHtml+="<tr>";
    		for(var i=0;i<100;i++)
    			resultHtml+="<td width='1'></td>";
    		resultHtml+="</tr>";
    		var tds;
    		var v_level,v_label,v_para,v_pk;
    		var i_prex,i_icon,i_image,i_label,i_hid;
    		var i_tIimage="",i_lIimage="",i_fIimage="",i_colspan,i_childShow,i_submit,i_value,i_def;
    		var has_def=false;
    		var nstr,tr=trs[0];
    		while(tr!=null){
    			tds=tr.getElementsByTagName("td");
    			v_level=getTrLevel(tr);
    			v_label=tds[1].text;
    			v_para=tds[2].text;
    			v_pk=getTrPkStr(tr);
    			tempSbliding[v_level]=isHasSibling(tr,v_level);
    			
    			i_tIimage="Tplus.png";
    			i_lIimage="Lplus.png";
    			i_fIimage="folderclose.png";
    			i_hid="none";
    			i_childShow="hide";
    			if(parseInt(element.expand,10)>=parseInt(v_level,10)){
    				i_hid="block";
    			}else{
    				;
    			}
    			if(parseInt(element.expand,10)>parseInt(v_level,10)){
    				i_tIimage="Tminus.png";
    				i_lIimage="Lminus.png";
    				i_fIimage="folderopen.png";
    				i_childShow="show";
    			}
    				
    			i_prex="";
    			if(2<v_level){
    				var beg=2;
    				for(var i=beg;i<v_level;i++){
    					if(tempSbliding[i]==true)
    						i_prex+="<td"+WIDTH_HEIGHT+"><img src='"+getImagePath("vline.png")+"'/></td>";
    					else
    						i_prex+="<td"+WIDTH_HEIGHT+"><img src='"+getImagePath("blank.png")+"'/></td>";
    				}
    			}
    			
    			i_icon="";
    			if(v_level>1){
	    			if(tempSbliding[v_level]==true){
	    				if(isHasChild(tr,v_level))
	    					i_icon="<td"+WIDTH_HEIGHT+"><img treeItemType='state' treeItemLast='0' src='"+getImagePath(i_tIimage)+"'/></td>";
	    				else
	    					i_icon="<td"+WIDTH_HEIGHT+"><img src='"+getImagePath("T.png")+"'/></td>";
	    			}else{
	    				if(isHasChild(tr,v_level))
	    					i_icon="<td"+WIDTH_HEIGHT+"><img treeItemType='state'  treeItemLast='1'  src='"+getImagePath(i_lIimage)+"'/>"+"</td>";
	    				else
	    					i_icon="<td"+WIDTH_HEIGHT+"><img src='"+getImagePath("L.png")+"'/></td>";
	    			}
    			}
    			
    			
    			if(isHasChild(tr,v_level))
    				i_submit="0";
    			else
    				i_submit="1";
    			if(element.kind==""){
    				if(isHasChild(tr,v_level))
    					i_image="<td style='cursor:hand;'"+WIDTH_HEIGHT+"><img treeItemType='floder' src='"+getImagePath(i_fIimage)+"'/></td>";
    				else
    					i_image="<td"+WIDTH_HEIGHT+"><img src='"+getImagePath("file.png")+"'/></td>";
    			}else if(element.kind=="check"){
    				if(v_pk.indexOf("<"+element.tagname+">1</"+element.tagname+">")>=0)
    					i_image="<td  style='cursor:hand;'"+WIDTH_HEIGHT+"><img treeItemType='check'  treeItemValue='1'  src='"+getImagePath("checkset.png")+"'/></td>";
    				else
    					i_image="<td  style='cursor:hand;'"+WIDTH_HEIGHT+"><img treeItemType='check'  treeItemValue='0'  src='"+getImagePath("checknul.png")+"'/></td>";
    			}else if(element.kind=="radio"){
    				if(v_pk.indexOf("<"+element.tagname+">1</"+element.tagname+">")>=0)
    					i_value="1";
    				else
    					i_value="0";
    				if(isHasChild(tr,v_level))
    					i_image="<td  style='cursor:hand;'"+WIDTH_HEIGHT+"><img treeItemType='floder'  treeItemValue='"+i_value+"' src='"+getImagePath(i_fIimage)+"'/></td>";
    				else
    					i_image="<td  style='cursor:hand;'"+WIDTH_HEIGHT+"><img treeItemType='radio'  treeItemValue='"+i_value+"' src='"+getImagePath("radionul.png")+"'/></td>";
    			}else{
    				i_image="<td"+WIDTH_HEIGHT+"><img src='"+getImagePath("blank.png")+"'/></td>";
    			}
    			
    			i_colspan=100-v_level;
    			i_label="<td colspan='"+i_colspan+"' align='left'  nowrap='true'>&nbsp;<span>"+v_label+"</span></td>";
    			
    			if((treeItemDefaultPara!=""&&treeItemDefaultPara==v_para)
    				||(treeItemDefaultPara=="absfirst"&&has_def==false)){
    				i_def=" treeItemDefault='true' ";
    				has_def=true;
    			}else if((treeItemDefaultPara!=""&&treeItemDefaultPara==v_para)
    				||(treeItemDefaultPara=="first"&&has_def==false&&isHasChild(tr,v_level)==false)){
    				i_def=" treeItemDefault='true' ";
    				has_def=true;
    			}else
    				i_def="";
    			resultHtml+="<tr treeItemPk='"+v_pk+"' oldTreeItemPk='"+v_pk+"' treeLevel='"+v_level+"' treeOldState='"+i_hid+"'  treeItemShow='"+i_childShow+"' treeItemPara='"+v_para+"' treeItemSubmit='"+i_submit+"' "+i_def+" style='display:"+i_hid+";'>"+i_prex+i_icon+i_image+i_label+"</tr>";
    			tr=tr.nextSibling;
    		}
    		resultHtml+="</table>";
    		//alert(resultHtml);
			element.innerHTML=resultHtml;
			initAction();
		}
		function initAction(){
			var defaultTr=null;
			var trs=element.getElementsByTagName("tr");
			if(trs.length==0)
				return ;
			for(var i=0;i<trs.length;i++){
				var imgs=trs[i].getElementsByTagName("img");
				for(var j=0;j<imgs.length;j++){
					if(imgs[j].getAttribute("treeItemType")==null)
						continue;
					if(imgs[j].getAttribute("treeItemType")=="state"){
						imgs[j].onclick=function(){showHideChild(this)};
					}else if(imgs[j].getAttribute("treeItemType")=="floder"){
						imgs[j].ondblclick=function(){showHideChild(this)};
					}
					else if(imgs[j].getAttribute("treeItemType")=="check"){
						imgs[j].ondblclick=function(){showHideChild(this)};
						imgs[j].setCheckState=function(check){
								this.setCheckImg(check);
								if(element.checkCascade=="false")
									return ;
								var tr=this;
								while(tr.tagName.toLowerCase()!="tr")
									tr=tr.parentNode;
								var level=tr["treeLevel"];
								tr=tr.nextSibling;
								while(tr!=null&&parseInt(tr["treeLevel"])>parseInt(level)){
									var checkImg=checkGetCheckImg(tr);
									if(checkImg!=null)
										checkImg.setCheckState(check);
									tr=tr.nextSibling;
								}	
							}
						imgs[j].setCheckImg=function(check){
								var tr=this;
								while(tr.tagName.toLowerCase()!="tr")
									tr=tr.parentNode;
								if(check==true){
									this.src=getImagePath("checkset.png");
									this.treeItemValue="1";
									tr.treeItemPk=tr.treeItemPk.replace("<"+element.tagname+">0</"+element.tagname+">","<"+element.tagname+">1</"+element.tagname+">");
								}else{
									this.src=getImagePath("checknul.png");
									this.treeItemValue="0";
									tr.treeItemPk=tr.treeItemPk.replace("<"+element.tagname+">1</"+element.tagname+">","<"+element.tagname+">0</"+element.tagname+">");
								}
							}
						imgs[j].onclick=function(){
								if(this.treeItemValue=="1"){
									this.setCheckState(false);
								}else{
									this.setCheckState(true);
								}
								if(element.checkCascade=="true")
									checkAllTree();
							}
					}
				else if(imgs[j].getAttribute("treeItemType")=="radio"){
						imgs[j].onclick=function(){
								if(oldSelectedRadioImg!=null){
									oldSelectedRadioImg.treeItemValue="0";
									oldSelectedRadioImg.src=getImagePath("radionul.png");
								}
								this.treeItemValue=="1";
								this.src=getImagePath("radioset.png");
								oldSelectedRadioImg=this;
							}
						if(imgs[j].treeItemValue=="1"){
							imgs[j].onclick();
						}
					}
				}
				var spans=trs[i].getElementsByTagName("span");
				if(element.action!=""&&spans.length>0&&trs[i].getAttribute("treeItemPara")!=""){
					spans[0].style["cursor"]="hand";
					spans[0].onmouseover=function(){
							this.old_mouse_color=this.style.color;
							this.style.color=MOUSEOVER_BG_COLOR;
						};
					spans[0].onmouseout=function(){
							this.style.color=this.old_mouse_color;
						};
					spans[0].onclick=function(){
							if(oldSelectedTreeItemSpan!=null)
								oldSelectedTreeItemSpan.style["background"]="";
							this.style["background"]=SELECT_BG_COLOR;
							var a=element.action;
							var tr=this;
							while(tr.tagName.toLowerCase()!="tr")
								tr=tr.parentNode;
							oldSelectedTreeItemSpan=this;
							if(tr.getAttribute("treeItemPara")!=null&&tr.getAttribute("treeItemPara")!=""){
								var fun=window.eval(a); 
								fun(tr.getAttribute("treeItemSubmit")=="1"?true:false,tr.getAttribute("treeItemPk"),tr.getAttribute("treeItemPara"),this.innerText);
							}
						};
				}
				if(trs[i].getAttribute("treeItemDefault")!=null)
					defaultTr=trs[i];
			}
			clickTreeItem(defaultTr);
		}
		function showHideChild(img){
			var tr=img;
			while(tr.tagName.toLowerCase()!="tr")
				tr=tr.parentNode;
			var level=parseInt(tr["treeLevel"])+1;
			var ts=tr.nextSibling;
			while(ts!=null&&ts["treeLevel"]>=level){
				if(tr.treeItemShow=="show"){
					ts.style["display"]="none";
					if(ts["treeLevel"]==level){
						ts["treeOldState"]="none";
					}
				}else{
					if(ts["treeLevel"]==level){
						ts.style["display"]="block";
						ts["treeOldState"]="block";
					}else
						ts.style["display"]=ts["treeOldState"];
				}
				ts=ts.nextSibling;
			}
			
			var imgs=tr.getElementsByTagName("img");
			for(var i=0;i<imgs.length;i++){
				if(imgs[i].getAttribute("treeItemType")==null)
						continue;
				if(imgs[i].getAttribute("treeItemType")=="state"){
					if(imgs[i].treeItemLast=="0")
						imgs[i].src=getImagePath(tr.treeItemShow=="show"?"Tplus.png":"Tminus.png");
					else
						imgs[i].src=getImagePath(tr.treeItemShow=="show"?"Lplus.png":"Lminus.png");
				}else if(imgs[i].getAttribute("treeItemType")=="floder"){
					imgs[i].src=getImagePath(tr.treeItemShow=="show"?"folderclose.png":"folderopen.png");
				}
			}
			
			tr.treeItemShow=(tr.treeItemShow=="show"?"hide":"show");
			
		}
		function clickTreeItem(tr){
			if(tr==null)
				return ;
			var level=tr["treeLevel"];
			var pt=tr.previousSibling;
			while(pt!=null&&pt["treeLevel"]>=1){
				if(pt["treeLevel"]==level-1&&pt["treeItemShow"]!="show"){
					var imgs=pt.getElementsByTagName("img");
					for(var j=0;j<imgs.length;j++){
						if(imgs[j].getAttribute("treeItemType")==null)
							continue;
						if(imgs[j].getAttribute("treeItemType")=="floder")
							imgs[j].ondblclick();
					}
					level--;
				}
				pt=pt.previousSibling;
			}
			var spans=tr.getElementsByTagName("span");
			if(element.action!=""&&spans.length>0&&tr.getAttribute("treeItemPara")!="")
				spans[0].onclick();
		}
		function getTreeDom(){
    		if(treeItemDataSource.length<10)
    			return null;
			var vXml = new ActiveXObject("Microsoft.XMLDOM");
			vXml.async = false;
    		if(vXml.loadXML(treeItemDataSource)==false)
    			return null;
    		return vXml;
		}
		function isHasSibling(tr,level){
			tr=tr.nextSibling;
			var l;
			while(tr!=null){
				l=getTrLevel(tr)
				if(l==level)
					return true;
				if(l<level)
					return false;
				tr=tr.nextSibling;
			}
			return false;
		} 
		function isHasChild(tr,level){
			tr=tr.nextSibling;
			if(tr==null)
				return false;
			if(parseInt(getTrLevel(tr))>parseInt(level))
				return true;
			return false;
		}
		function checkAllTree(){
			var trs=element.getElementsByTagName("tr");
			if(trs.length>0){
				if(element.cascadeMode == "" || element.cascadeMode == "All"){
					checkAllTree2(trs[TR_STRAT_INDEX]);
				}
				else{
					checkParentTree(trs[TR_STRAT_INDEX]);
				}
			}
		}
		function checkAllTree2(tr){
			var c=true;
			var level=parseInt(tr["treeLevel"])+1;
			var nt=tr.nextSibling;
			var hasChild=false;
			while(nt!=null&&parseInt(nt["treeLevel"])>=level){
				if(parseInt(nt["treeLevel"])==level&&checkAllTree2(nt)==false)
					c=false;
				nt=nt.nextSibling;
				hasChild=true;
			}
			var checkImg=checkGetCheckImg(tr);
			if(hasChild==true){
				checkImg.setCheckImg(c);
				return c;
			}
			
			return checkImg["treeItemValue"]=="1"?true:false;
		}
		
		function checkParentTree(tr){
			var c=false;
			var level=parseInt(tr["treeLevel"])+1;
			var nt=tr.nextSibling;
			var hasChild=false;
			while(nt!=null&&parseInt(nt["treeLevel"])>=level){
				if(parseInt(nt["treeLevel"])==level&&checkParentTree(nt)==true)
					c=true;
				nt=nt.nextSibling;
				hasChild=true;
			}
			var checkImg=checkGetCheckImg(tr);
			if(hasChild==true){
				checkImg.setCheckImg(c);
				return c;
			}
			
			return checkImg["treeItemValue"]=="1"?true:false;
		}
		function checkGetCheckImg(tr){
			var imgs=tr.getElementsByTagName("img");
			for(var i=0;i<imgs.length;i++){
				if(imgs[i].getAttribute("treeItemType")=="check")
					return imgs[i];
			}
			return null;
		}
		function getTrLevel(tr){
			tds=tr.getElementsByTagName("td");
			return tds[0].text;
		}
		function getTrPkStr(tr){
			var pks=tr.getElementsByTagName("pk");
			if(pks.length==0)
				return "";
			if(pks[0].childNodes.length==0)
				return ;
			var pkStr="";
			for(var i=0;i<pks[0].childNodes.length;i++){
				var c=pks[0].childNodes[i];
				pkStr+="<"+c.tagName+">"+c.text+"</"+c.tagName+">";
			}
			return pkStr;
		}
		function getImagePath(name){
			return window.prefix+"base/themes/blue/images/treetable/"+name;
		}
		function submit(){
			if(element.update==null)
				return ;
			var str="";
			if(element.kind=="")
				str=getCommonSubmitStr();
			else if(element.kind=="check")
				str=getCheckSubmitStr();
			else if(element.kind=="radio")
				str=getRadioSubmitStr();
			window.xmlhttp.post(element.update, str);
		    var rstr = window.xmlhttp._object.responseText;
		    if (!window.doMsg(rstr,hideMsg)) {
		      return false;
		    }
		    return true;
		}
		function getCommonSubmitStr(){
			return "";
		}
		function getCheckSubmitStr(){
			var trs=element.getElementsByTagName("tr");
			if(trs.length==0)
				return "";
			var tr=trs[TR_STRAT_INDEX];
			var result="";
			while(tr!=null){
				if(tr["treeItemSubmit"]=="1")
					result+="<record>"+tr.treeItemPk+"</record>";
				tr=tr.nextSibling;
			}
			if(result.length>0)
				result="<multiData>"+result+"</multiData>";
			return result;
		}
		function getRadioSubmitStr(){
			if(oldSelectedRadioImg==null)
				return "";
			return "<record>"+getSelectedPk()+"</record>";
		}
		
		function setSqlData(sqlid,para,defaultPara,ischeck){
			element.innerHTML="";
			treeItemDefaultPara="";
			if(typeof(para)=="undefined")
				para="";
			if(ischeck){
				window.xmlhttp.post(sqlid, para);
			}else{
				window.xmlhttp.post(sqlid, para,"?isCheck=false");
			}
	    	treeItemDataSource = window.xmlhttp._object.responseText;
	    	treeItemDefaultPara=defaultPara;
			refresh();
		}
		
		function setMenuData(name,data,defaultPara){
			treeItemDefaultPara="";
			var vXml = new ActiveXObject("Microsoft.XMLDOM");
			vXml.async = false;
    		if(vXml.loadXML(data)==false)
    			return;
    		var level="1";
    		var xmlData="<?xml version='1.0' encoding='GB2312'?><root>";
    		xmlData+="<tr><td>1</td><td>"+name+"</td><td></td></tr>";
    		var menus=vXml.getElementsByTagName("menu_node");
    		if(menus.length==0)
    			return ;
    		var menu=menus[0];
    		while(menu!=null){
    			xmlData+=genDataFromMenu(menu,parseInt(level)+1);
    			menu=menu.nextSibling;
    		}
    		xmlData+="</root>";
    		treeItemDataSource=xmlData;
    		treeItemDefaultPara=defaultPara;
			refresh();
		}
		function genDataFromMenu(node,level){
			var str="<tr><td>"+level+"</td>";
			var label="",para="";
			var child="";
			label=node.getAttribute("name");
			if(node.childNodes.length>0){
				for(var i=0;i<node.childNodes.length;i++){
					if(node.childNodes[i].tagName=="menu_node"){
						child+=genDataFromMenu(node.childNodes[i],parseInt(level)+1);
					}
					if(node.childNodes[i].tagName=="page"){
						para=node.childNodes[i].getAttribute("src");
					}
				}
			}
			str+="<td>"+label+"</td><td>"+para+"</td></tr>"+child;
			return str;
		}
		function getSelectedPk(){
			if(oldSelectedRadioImg==null)
				return "";
			var tr=oldSelectedRadioImg;
			while(tr.tagName.toLowerCase()!="tr")
				tr=tr.parentNode;
			return tr.treeItemPk.replace("<"+element.tagname+">0</"+element.tagname+">","<"+element.tagname+">1</"+element.tagname+">")
		}
		function getCheckValues(con){
			if(typeof(con)=="undefined")
				con="all";
			var tables=element.getElementsByTagName("table");
			if(tables.length<1)
				return "";
			var trs=tables[0].getElementsByTagName("tr");
			var res="";
			var tem;
			for(var i=0;i<trs.length;i++){
				if(typeof(trs[i].treeItemPk)!="undefined"){
					tem="";
					if(con=="all"){
						tem=trs[i].treeItemPk;
					}else if(con=="on"){
						if(trs[i].treeItemPk.indexOf("<"+element.tagname+">1</"+element.tagname+">")>=0){
							tem=trs[i].treeItemPk.replace("<"+element.tagname+">1</"+element.tagname+">","");
						}
					}else if(con=="off"){
						if(trs[i].treeItemPk.indexOf("<"+element.tagname+">0</"+element.tagname+">")>=0){
							tem=trs[i].treeItemPk.replace("<"+element.tagname+">0</"+element.tagname+">","");
						}
					}else if(con=="change"){
						if(trs[i].treeItemPk!=trs[i].oldTreeItemPk){
							tem=trs[i].treeItemPk;
						}
					}
					if(tem!="")
						res+="<record>"+tem+"</record>";
				}
			}
			return res;
		}


  	jhtc_attr_init(element,"update",null);
  	jhtc_attr_init(element,"action","");
  	jhtc_attr_init(element,"kind","");
  	jhtc_attr_init(element,"expand","1");
  	jhtc_attr_init(element,"tagname",null);
  	jhtc_attr_init(element,"checkCascade","true");
  	jhtc_attr_init(element,"cascadeMode","All");
  	
	element.setMenuData=setMenuData;
	element.setSqlData=setSqlData;
	element.getSelectedPk=getSelectedPk;
	element.getCheckValues=getCheckValues;
	element.submit=submit;
	element.init=init;
	element.jhtcInit=function(){
		element.init();	
	};
	return null;
};
jhtc_class_map["sTreeCtn"]=jhtc_stree;