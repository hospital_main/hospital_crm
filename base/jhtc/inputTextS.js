function jhtc_inputTextS(win,jhtc_obj){
  var pageWin=win;
  var element=jhtc_obj;
  //整个SELECT由两个输入框，一个按钮和一个显示列表组成
  var objList;
  var objInput;
  var iframe=null;
  var srcTree = new ActiveXObject("Microsoft.XMLDOM");
  var xsltTree= new ActiveXObject("Microsoft.XMLDOM");
  srcTree.async=false;
  xsltTree.async=false;
  var hasList = "0";
  var vListMaxHigh = 81;
  var objXML;
  var oldSetValue;//LZK ADD
  var paraItemCount=0;
  var paraItemsXml=null;
  var is_onload;
  
  function setEnabled(e){
  	if(e==true){
  		objInput.disabled=false;
  	}else{
  		objInput.disabled=true;
  	}
  }
  function getDict_html(in_load,in_para,in_code){
	
	  if (in_para != null) { 
				xmlhttp.post("global_select_html", in_para, "?selectID="+in_load+","+in_code);
			}
			else
				xmlhttp.post("global_select_html", '', "?selectID="+in_load+","+in_code);
			
			if (window.doMsg(xmlhttp._object.responseText)) {
				
				return xmlhttp._object.responseText;
				
			}
	}
  function hasResult(){
    if (hasList == "0")
      return false;
    else 
      return true;
  }
  function setText(){
    return element.text =objInput.value;
  }

  function initBegin() {
    if (trim(element.load)=="") {
      alert("此select组件，没有指定load属性!");
      return;
    }

    if (element.label != null) {
      // label 的基本长度
      var lableLength = 120;
      var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
      if (spanWidth < lableLength) spanWidth = lableLength;

      // 快捷键
      var aKey = "";
      if (trim(element.accessKey)!="") {
        aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
      }
      element.insertAdjacentHTML("beforeBegin", "<span id='labelCtn' nowrap style='text-align:right;width:"+spanWidth+"px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";font-weight:"+element.currentStyle.fontWeight+";'>"+element.label+aKey+(element.label==''?"":"：")+"</span>");
    } else {
      var aKey = "";
      if (trim(element.accessKey)!="") {
        aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
      }  
      element.insertAdjacentHTML("beforeBegin", "<span id='labelCtn' nowrap style='text-align:right;width:0px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";font-weight:"+element.currentStyle.fontWeight+";'></span>");
    }
    element.parentNode.noWrap = true;
    
    // 设置输入框的样式
    objInput = element.document.createElement("<input/>");
    __jhtcBindPropertyChange(objInput,"value",setText);
    element.style.width = element.extent;
    element.style.color = "#FFFFFF";
    
    with (objInput) {
      accessKey = element.accessKey;
      readOnly = element.readOnly;
      className = element.className+"_text";
      style.position = "absolute";
      style.height = element.offsetHeight - 4;
      style.width = element.extent-5;
      maxLength = element.maxInput;
      if ("true"== trim(element.required)) {
        style.backgroundColor="#DBFCFF";
      }
      onkeydown = navigateKeys;
      onblur = function() {
      	clickDocument();
      }
    }
    objInput.onkeyup = objInput.ondragend = changeInput;

    element.accessKey="";
    element.tabIndex=-1;
    element.insertAdjacentElement("beforeBegin", objInput);
	 objList = element.document.createElement("<div class='select_list'></div>");
  	element.insertAdjacentElement("afterEnd", objList);
  	objList.style.display="none";
  	is_onload="false";
  	initList();
  	is_onload="true";
  }
  
  function changeInput() {
  	if ((event.keyCode>=33 && event.keyCode<=40) || event.keyCode==13 || event.keyCode==16 || event.keyCode==18 || event.keyCode==9
          || event.keyCode==27 || event.keyCode==0 || event.keyCode>250)
      return;
    if(objInput.value == "") {
    	element.value = "";
    }  
  	//if (trim(checkValue)!="true") {
  	  element.value = objInput.value;
  	  
  	//}
		//if (trim(this.value)=="" && objList.style.display=="none") return;
		showList();
  }
  
  function changeTitle() {
    objInput.title = element.text;
    element.title = element.text;
  }
  
  function initEnd() {
    if (objInput == null) return;
    // 初始化按钮
    iframe=element.document.createElement("iframe");
	with(iframe){
		style.position = "absolute";
		style.display = "none";
		style.textAlign = "center";
		style.backgroundColor = "#F6F6F6";
		className = "ds_font";
		style.zIndex =8;
		style.overflow = "visible";
		border="0";
	}
	iframe = element.parentNode.appendChild(iframe);
	
  	window.attachEvent("onresize", adjPosition);
  	adjPosition();
  	window.setTimeout(adjPosition,50);
  	window.setTimeout(adjPosition,200);
  	window.setTimeout(adjPosition,400);
  	if (trim(element.initValue) != "") {
  	  setValue(element.initValue);
  	}
  }
  
  // 调整各个元素的位置
  function adjPosition() { // 调整位置
    // 取得element的绝对位置
    var form = element;
    var elementTop=0, elementLeft=0;

    while(form.tagName != "BODY"&&form.tagName != "DIV") {
	    elementTop = elementTop + form.offsetTop + form.clientTop;
	    elementLeft = elementLeft + form.offsetLeft+form.clientLeft;
	    form = form.offsetParent;
    }

    with (objInput.style) {
      top = elementTop;
      left = elementLeft;
    }

    with (objList.style) {
    	zIndex =10;
     	left = elementLeft-1;
     	if(elementTop+element.offsetHeight-1+parseInt(objList.style.height.replace(/px/,'')) > window.document.body.offsetHeight)
     	  top = elementTop + element.offsetHeight-1-101;
     	else
  		  top = elementTop + element.offsetHeight-1;
    }
    with (iframe.style) {
		left = elementLeft;
		if(elementTop+objInput.offsetHeight-1+parseInt(objList.style.height.replace(/px/,'')) > window.document.body.offsetHeight)
			top = elementTop - parseInt(objList.style.height.replace(/px/,''))
		else
			top = elementTop + objInput.offsetHeight;
		window.setTimeout(function(){
				//iframe.style.width=objInput.clientWidth+4;
				iframe.style.width=objList.style.width;
				iframe.style.height=82;//objInput.clientHeight;
			},10);
	}
  }
  //wsj 从showList 方法中抽取出来，专门用于加载数据源，而不显示。
 function loadListdata(){
 		var lists = window.document.getElementsByTagName("DIV")
		for (var i=0; i<lists.length; i++) {
			if (lists[i].className == "select_list")
				lists[i].style.display = "none";
		}

		if (!element.readOnly && false) {
			srcTree.loadXML(objXML.replace(/key='(.*)'/,"key='"+trim(objInput.value)+"'"))
			objList.innerHTML = srcTree.transformNode(xsltTree);

	    var trs = objList.getElementsByTagName("TR")
	    for (var i=0; i<trs.length; i++) {
	      trs[i].onmouseover = function() {
	        if (this.parentNode.parentNode.choseIndex!=null) {
	          with (this.parentNode.parentNode.rows[this.parentNode.parentNode.choseIndex].runtimeStyle) {
	            backgroundColor="";
	            color="";
	          }
	        }

	        this.parentNode.parentNode.choseIndex = this.rowIndex;
	        this.runtimeStyle.backgroundColor="darkblue";
	        this.runtimeStyle.color="white";
	      }
	      trs[i].onmousedown = choose;
	      if (i%2==0) {
	        trs[i].style.backgroundColor="whitesmoke";
	      }
	    }
    }
	
 }
 //wsj
  // 显示列表
  var ccc=0;
  function showList() {ccc++;
		//loadListdata();
		initList()
		iframe.style.width=objList.style.width;
    objList.style.display = "";
    iframe.style.display=objList.style.display 

    var trs = objList.getElementsByTagName("TR");
    if (trs.length==0) return false;
    var i=0;
    for (; i<trs.length; i++) {
      if (trs[i].innerText.indexOf(objInput.value)==0) {
        break;
      }
    }

    if (i==trs.length) i=0;

    if (trs[i].parentNode.parentNode.choseIndex!=null) {
      with (trs[i].parentNode.parentNode.rows[trs[i].parentNode.parentNode.choseIndex].runtimeStyle) {
        backgroundColor = "";
        color = "";
      }
    }
   
    if(i!=0){
      trs[i].runtimeStyle.backgroundColor="darkblue";
      trs[i].runtimeStyle.color="white";
      trs[i].parentNode.parentNode.choseIndex = trs[i].rowIndex;
    }else if(trs.length>1){
      trs[1].runtimeStyle.backgroundColor="darkblue";
      trs[1].runtimeStyle.color="white";
      trs[1].parentNode.parentNode.choseIndex = trs[1].rowIndex;
    }else{
      trs[0].runtimeStyle.backgroundColor="darkblue";
      trs[0].runtimeStyle.color="white";
      trs[0].parentNode.parentNode.choseIndex = trs[0].rowIndex;
    }
    // 移动div scroll 1.取trs[i]的绝对top
    var baseDivTop = 0;
    var parentObj = trs[i];
    while (parentObj.tagName != "DIV") {
	    baseDivTop += parentObj.offsetTop;
	    parentObj = parentObj.offsetParent;
    }
    parentObj.scrollTop = baseDivTop;

    window.document.attachEvent("onmousedown",clickDocument);
  }
  function selectItem(index){
	    if(index<0||index>(getItemCount()-1))
  			return ;
  		var p=objList.getElementsByTagName("TR")[index+1].cells[0];
  		setValue(p.value+"|||"+p.innerText);
	//setValue(p.getAttribute("code")+"|||"+p.getAttribute("value"));
  }
  
  function getItemCount(){
		return paraItemCount;
	}
	function selectItemByItemCount(isInsert){
		if (getItemCount()==1 ){
			selectItem(0)
		}
	}
  // 初始化显示列表
  function initList(str) {
  
		if (trim(str)!='') {
		  element.load=str
		}
//added by wsj		
    var loadXML = "";
/*wsj1.1 输入法检索*/    
    if (trim(element.load)=="retrieve_method") {
 //   	para=para+"<text>"+element.text+"</text">;
    	 
      if( element.para.indexOf("<retrieve>")>0)
 	     element.para= element.para.substring(0,element.para.indexOf("<retrieve>"));//如果不是初始状态，则取得，在尾部已经给了输入法的数值，去掉将后面的输入法的数值
   	   element.para=element.para+'<retrieve>'+getRetrieve()+'</retrieve>'  
    }
    
//added by twl
    var elem_code;
    if(trim(element.code)=="true")
    {
      elem_code="true";
    }else{elem_code="false"}
    if(is_onload=="true") {
      loadHTML= getDict_html(element.load,trim(element.para)+"<last>"+element.text+"</last>",elem_code);
      
    }else{
      loadHTML='0*<TBODY><TABLE><TR id=top_line style="FONT-SIZE: 12px"><TD style="PADDING-LEFT: 5px; FONT-SIZE: 12px; PADDING-TOP: 1px; HEIGHT: 16px" noWrap value=""></TD></TR></TABLE></TBODY>';
    }
   
    var marr=loadHTML.split('*');
    
	  var maxLength = parseInt(marr[0]);
	  if(marr[0]=="xml"){
     
        if (trim(this.code)=="true") {
          objXML = marr[1].replace("<root>","<root code='true' qtype='0' key=''>");
        } else
          objXML = marr[1].replace("<root>","<root code='false' qtype='0' key=''>");
    
        srcTree.loadXML(objXML);
        serverObj=srcTree;
        var objNodeList = srcTree.getElementsByTagName("para");
        var maxLength = 0;
        for (var j=0; j<objNodeList.length; j++) {
          if (maxLength < objNodeList.item(j).attributes.item(1).nodeValue.length) {
            maxLength = objNodeList.item(j).attributes.item(1).nodeValue.length;
          }
        }
        xsltTree.load(window.prefix+"base/xsl/select1.xsl");
        objList.innerHTML = srcTree.transformNode(xsltTree);
	  }else{
	    var allhtml=loadHTML.substring(loadHTML.indexOf('*')+1);
	    
	    objList.innerHTML=allhtml;
	  }
	  
	  
		with (objList) {
  		style.display = "none";
      style.top = objInput.offsetTop+objInput.offsetHeight;
      style.height = vListMaxHigh;
  	}
     paraItemCount=objList.getElementsByTagName("TR").length-1;
  	if (maxLength <= 9) {
  	  objList.style.width = element.offsetWidth;
  	} else {
  	  var temp = parseFloat(element.offsetWidth)+parseFloat(((maxLength-9)*20>200)?200:(maxLength-9)*20);
  	  objList.style.width = temp>200?(element.extent>200?element.extent:200):temp;
  	}

  	var trs = objList.getElementsByTagName("TR")
  	if (trs.length != 0) {
  	  hasList = "1";
  	} else {
  	  hasList = "0";
  	}
    for (var i=0; i<trs.length; i++) {
      trs[i].onmouseover = function() {
        if (this.parentNode.parentNode.choseIndex!=null) {
          with (this.parentNode.parentNode.rows[this.parentNode.parentNode.choseIndex].runtimeStyle) {
            backgroundColor="";
            color="";
          }
        }

        this.parentNode.parentNode.choseIndex = this.rowIndex;
        this.runtimeStyle.backgroundColor="darkblue";
        this.runtimeStyle.color="white";
      }
      trs[i].onmousedown = choose;
      if (i%2==0) {
        trs[i].style.backgroundColor="whitesmoke";
      }
    }

    var flag = false;
  	if (trim(element.defaultValue)=="true" ) {
			flag = true;
  	}
  	
    if (trim(element.required)=="true") {
      objList.all("top_line").style.display = "none";
			objInput.style.backgroundColor="#DBFCFF";
  	} else {
  	  objInput.style.backgroundColor="#FFFFFF";
  	}
  
    /*
  	if (trim(required)=="true" && trim(defaultValue) != "false" && objXML.indexOf("code=\"")!=-1) {
			flag = true;
  	}
  	if (flag) {
			var str = objXML.substring(objXML.indexOf("code=\"")+"code=\"".length);
			element.value = str.substring(0, str.indexOf("\""));
			str = str.substring(str.indexOf("value=\"")+"value=\"".length);
			objInput.value = str.substring(0, str.indexOf("\""));
			if (trim(code)=="true")
			  objInput.value = element.value+"　"+objInput.value;
			changeTitle();
  	}
  	
  	if (trim(str)!='' && !flag) {
	    objInput.value=''
	    element.value=''
	  }*/
  }
  
  // 刷新页面
  function refresh() {
    //alert(element.parentNode.outerHTML)
    var aKey = "";
    if (trim(element.accessKey)!="") {
      aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
    }
    if (trim(element.label)!="") {
      element.parentNode.getElementsByTagName("SPAN")[0].innerText = element.label+aKey+(element.label==''?"":"：");
    }
    //added by wsj 在重新加载的时候，检查select控件是否下拉。
    if(element.load!=""){
    	var dis=objList.style.display; //检查select控件是否下拉。
	  	initList();
	  	if(element.load=="retrieve_method"){
				if (dis!= "none"){
	      	showList();
	     	 }
		    else{
		  	  loadListdata();	
		  		}			
	    	}
    //wsj added
  	adjPosition();
  	if(typeof(oldSetValue)!="undefined")
  	setValue(oldSetValue);
  	}
  	 
  }
  
  // 点击选择项响应事件
	function choose() { 
    var table = objList.getElementsByTagName("TABLE")[0];
    if (table.choseIndex==null) {
      return false;
    }
		//objInput.value = table.rows[table.choseIndex].innerText;
		//element.code = table.rows[table.choseIndex].cells[0].value;
		changeTitle();
    objList.style.display = "none";
    iframe.style.display=objList.style.display 
    
    if(table.rows[table.choseIndex].cells.length>0){
	    var v=table.rows[table.choseIndex].cells[0].value;
	    if(element.useCode=="false"){
	    	v=table.rows[table.choseIndex].cells[0].text;
	    	if(element.code=="true"){
	    		var vv=v.split("　");
	    		if(vv.length>=2){
	    			v=vv[0];
	    		}
	    	}
	    }
	    
	    if (element.value!=v) {
	    	element.value=v;
	    	oldSetValue=v;
	    	objInput.value=v;
	    	//var oEvent = createEventObject();
			//oEvent.result = element.value;
		   	__jhtcDispatchEvent(element,"onchange"); 
		   	//add by 
	    }
    }else{
    	element.value=objInput.value;
    }
    /*
    if(trim(element.code)=="true"){
		oldSetValue=objInput.value.replace("　","|||");
	}else
		oldSetValue=element.value+"|||"+objInput.value;
	*/
    objInput.select();
  }
  
  // 点击按钮等响应事件
  function clickDocument() { 
  	if(objList==null) return;

	var objSrc = window.event.srcElement;
	if (objList==objSrc||objInput==objSrc||element.contains(objSrc) || objList.contains(objSrc)) {
  	  if (objSrc.value != null) {
  	    ;//element.value = objSrc.value;
  	  }
  	  return;
  	} 

  	objList.style.display="none";
  	iframe.style.display=objList.style.display 
    window.document.detachEvent("onmousedown",clickDocument);
	//setValue(oldSetValue);
  }


  // 键盘响应
  function navigateKeys() {
		var nKeyCode=event.keyCode;
    switch(nKeyCode){
    case 113:  // F2 
    	if(element.findpage=="no")
    		return true;
      	if(element.findpage!=null)
      		openDialog(element.findpage, 'dialogWidth:900px;dialogHeight:550px')
      	/*else{
	        selectObj.setXML(objXML)
	        selectObj.select("<addText>true</addText>");
	        if (trim(selectObj.value)!=''){
	          setValue(selectObj.value)
	          
	          if(onchange!=null){
	          	 var str=""+onchange;
	          	
	             str = str.substring(22,str.length - 2);
	             str=str.replace("this","element");
	             
	            eval(str);
	          }
	        }  
        }*/
        return true
      case 38://^
        if (objList!=null && objList.style.display=="") {
          scrollUpList();
          return false;
        }
        break;
      case 40://\|/
        if (1==1) {
          if (objList==null || objList.style.display=="none") {
            showList();
            return false;
          }
        }
        if (objList!=null && objList.style.display=="") {
          scrollDownList();
          return false;
        }
        break;
      case 13://Enter
        if (element.value == "") {
          __jhtcDispatchEvent(element,"onchange");
        }
        if (objList!=null && objList.style.display=="") {
        	choose();
        	event.keyCode=9;
          objInput.onkeyup = objInput.ondragend = null;
         	//return false;
        }
        break;
      case 27:// Esc
        if (objList!=null){
          objList.style.display="none";
          iframe.style.display=objList.style.display 
        }
        return false;
      case 33://pageup
        if (objList!=null && objList.style.display=="") {
          scrollPageUpList();
          return false;
        }
      case 34://pagedown
      	if (objList!=null && objList.style.display=="") {
          scrollPageDownList();
          return false;
        }
      default:
        objInput.onkeyup = objInput.ondragend = changeInput;
        return true;
    }
    return true;
  }
  
  //向上翻页滚动列表
	function scrollPageUpList() {
    var table = objList.getElementsByTagName("TABLE")[0];
    if (table.rows.length>0) {
      var count = objList.offsetHeight/table.rows[0].offsetHeight-1;
      for (var i=0; i<count; i++) {
        scrollUpList();
      }
    }
  }
  
  //向下翻页滚动列表
  function scrollPageDownList() {
    var table = objList.getElementsByTagName("TABLE")[0];
    if (table.rows.length>0) {
      var count = objList.offsetHeight/table.rows[0].offsetHeight-1;
      for (var i=0; i<count; i++) {
        scrollDownList();
      }
    }
  }
  
  //向下滚动列表
  function scrollDownList() {
    var table = objList.getElementsByTagName("TABLE")[0];
    if (table.rows.length<1) return;
    if (table.choseIndex>=table.rows.length-1) return;

    if (table.choseIndex!=null) {
      with (table.rows[table.choseIndex].runtimeStyle) {
        backgroundColor = "";
        color = "";
      }
    } else {
      table.choseIndex = 0;
    }

    table.choseIndex++;

    var baseDivTop = 0;
    var parentObj = table.rows[table.choseIndex];
    while (parentObj.tagName != "DIV") {
	    baseDivTop += parentObj.offsetTop;
	    parentObj = parentObj.offsetParent;
    }

    if (baseDivTop+table.rows[table.choseIndex].offsetHeight*2 > parentObj.scrollTop+parentObj.offsetHeight) {
      parentObj.scrollTop = parentObj.scrollTop + table.rows[table.choseIndex].offsetHeight;
    }

    table.rows[table.choseIndex].runtimeStyle.backgroundColor="darkblue";
    table.rows[table.choseIndex].runtimeStyle.color="white";
  }

  //向上滚动列表
  function scrollUpList() {
    var table = objList.getElementsByTagName("TABLE")[0];

    if (table.choseIndex==null || table.choseIndex==0) {
      table.choseIndex = 0;
      return;
    } else {
      with (table.rows[table.choseIndex].runtimeStyle) {
        backgroundColor = "";
        color = "";
      }
    }

    table.choseIndex--;

    table.rows[table.choseIndex].runtimeStyle.backgroundColor="darkblue";
    table.rows[table.choseIndex].runtimeStyle.color="white";

    var baseDivTop = 0;
    var parentObj = table.rows[table.choseIndex];
    while (parentObj.tagName != "DIV") {
	    baseDivTop += parentObj.offsetTop;
	    parentObj = parentObj.offsetParent;
    }

    if (baseDivTop < parentObj.scrollTop) {
      parentObj.scrollTop = parentObj.scrollTop - table.rows[table.choseIndex].offsetHeight;
    }
  }
  
  //检查输入项是否正确
  function check() {
    element.value = objInput.value;
    if (trim(element.checkValue)!="true") {
  		return true;
  	}
   	if ("true"==element.required) {
      if (window.trim(objInput.value)=="" && trim(element.checkValue)=="true")  {
        if (element.label != null) {
          alert(element.label+"不能为空！");
        } else {
          alert("请选择必选项！");
        }
        objInput.focus();
        objInput.select();
        return false;
      }
    }

/*
    if (objInput.value != "") {
      var table = objList.getElementsByTagName("TABLE")[0];
      if (table.choseIndex != null) {
    		objInput.value = table.rows[table.choseIndex].innerText;
    		changeTitle();
        if (table.rows[table.choseIndex].cells.length>0 &&
        	element.value!=table.rows[table.choseIndex].cells[0].value) {
        	element.value=table.rows[table.choseIndex].cells[0].value;
        }
      }
    }
   */
    return true;
  }
  
  //给SELECT赋值
  function setValue(str) {
 	  oldSetValue=str;
    if (str == null || trim(str)==""){
    	objInput.value=element.value="";
    	return;
    }
    var ves=str.split("|||");
    if(ves.length==2){
    	element.value=ves[0];
    	if(trim(element.code)=="true"){
    		objInput.value=ves[1];
    	}else
    		objInput.value=ves[1];
    	
    }
    changeTitle();
  }
	
  	jhtc_attr_init(element,"label",null);
  	jhtc_attr_init(element,"load",null);
  	jhtc_attr_init(element,"para","");
	jhtc_attr_init(element,"findpage",null);
	jhtc_attr_init(element,"defaultValue",null);
	jhtc_attr_init(element,"initValue",null);
	jhtc_attr_init(element,"code","false");
	jhtc_attr_init(element,"required","false");
	jhtc_attr_init(element,"checkValue","true");
	jhtc_attr_init(element,"extent","140");
	jhtc_attr_init(element,"maxInput","50");
	jhtc_attr_init(element,"text","");
	jhtc_attr_init(element,"useCode","true");

  	
	element.selectItem=selectItem;
	element.getItemCount=getItemCount;
	element.selectItemByItemCount=selectItemByItemCount;
	element.setEnabled=setEnabled;
	element.hasResult=hasResult;
	element.setValue=setValue;
	element.check=check;
	element.initBegin=initBegin;
	element.initEnd=initEnd;
	element.jhtcInit=function(){
		element.initBegin();
		element.initEnd();	
	};
	return null;
};
jhtc_class_map["inputTextS"]=jhtc_inputTextS;