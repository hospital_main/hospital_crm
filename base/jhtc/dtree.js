function jhtc_dtree(win,jhtc_obj){
		var pageWin=win;
		var element=jhtc_obj;
		var REQUEST_PARA_TAG="parentCode"
		var SELECT_BG_COLOR="#9999FF";
		var MOUSEOVER_BG_COLOR="#009999";
		var MAX_COL=100;
		var TR_STRAT_INDEX=1;
		var oldSelectedTreeItemSpan=null;
		var treeTable=null;
		
		function init() {
			if(element.dataid==null){
				alert("Please set attribute 'dataid'!");
				return ;
			}
			init_element(element);
		}
		function init_element(e){
			var tableHtml="<table  id='_mainDataTable'  border='0' cellpadding='0' cellspacing='0'>";
    		tableHtml+="<tr>";
    		for(var i=0;i<=MAX_COL;i++)
    			tableHtml+="<td width='1'></td>";
    		tableHtml+="</tr></table>";
    		e.innerHTML=tableHtml;
    		var tables=e.getElementsByTagName("table");
    		treeTable=tables[0];
    		inittable(tables[0],{"dataid":e.dataid,"para":e.para, "action":element.action,"selectedColor":SELECT_BG_COLOR,"mouseOverColor":MOUSEOVER_BG_COLOR,"imageWidth":"19","imageHeight":"16"});
		}
		function getCode(){
			return treeTable.getCode();
		}
		function getLabel(){
			return treeTable.getLabel();
		}
		function getTreeItem(c){
			return treeTable.getTreeItem(c);
		}
		function refresh(){
			if(treeTable!=null){
				treeTable["_treedataid"]=element.dataid;
				treeTable["_treepara"]=element.para;
				treeTable["_treeaction"]=element.action;
				loadRoot(treeTable);
			}
		}
		//part 2 begin
		function inittable(table,paras){
			table.getCode=function(){
					if(this["_treeSelectedTr"]!=null)
						return this["_treeSelectedTr"]["_treeCode"];
					else
						return "";
				};
			table.getLabel=function(){
					if(this["_treeSelectedTr"]!=null)
						return this["_treeSelectedTr"]["_treeSpan"].innerText;
					else
						return "";
				};
			table.getTreeItem=function(c){
					var child=this["_treeFirstTr_"].nextSibling;
					if(c=="")
						return child;
					while(child!=null){
						if(child["_treeCode"]==c)
							return child;
						child=child.nextSibling;
					};
					return null;
				};
			for(var k in paras){
				table["_tree"+k]=paras[k];
			}
			var trs=table.getElementsByTagName("tr");
			table["_treeSelectedTr"]=null;
			table["_treeFirstTr_"]=trs[0];
			loadRoot(table);
		}
		function loadRoot(table){
			var tr=table["_treeFirstTr_"].nextSibling;
			var ntr;
			while(tr!=null){
				ntr=tr.nextSibling;
				tr.parentNode.removeChild(tr);
				tr=ntr;
			}
			table["_treeSelectedTr"]=null;
			var trData=getTrData(0,"",table,"");
			addTreeItems(table,table["_treeFirstTr_"],trData);
		}
		function addTreeItems(table,parentTr,trData){
			var parentSiblingTr=parentTr.nextSibling;
			var previousSiblingTr=parentTr;
			var tempDiv=element.document.createElement("div");
			tempDiv.innerHTML="<table>"+trData+"</table>";
			var trs=tempDiv.getElementsByTagName("tr");
			if(trs.length==0)
				return ;
			var tr=trs[0];
			var ntr;
			var p_level,i_s_o,i_s_c,i_i_o,i_i_c,p_code,p_type,text;
			while(tr!=null){
				ntr=tr.nextSibling;
				if(ntr==null)
					tr["_treeIsLast"]=true;
				else
					tr["_treeIsLast"]=false;
				var span=tr.getElementsByTagName("span")[0];
				var td=span.parentNode.nextSibling
				var ntd;
				for(var i=0;i<7;i++){
					ntd=td.nextSibling;
					text=td.innerText;
					switch(i){
						case 0:p_level=text;break;
						case 1:i_s_o=text;break;
						case 2:i_s_c=text;break;
						case 3:i_i_o=text;break;
						case 4:i_i_c=text;break;
						case 5:p_code=text;break;
						case 6:p_type=text;break;
					}
					td.parentNode.removeChild(td);
					td=ntd;
				}
				var imgs=tr.getElementsByTagName("img");
				tr["_treeTable"]=table;
				tr["_treeCode"]=p_code;
				tr["_treeType"]=p_type;
				tr["_treeLevel"]=p_level;
				tr["_treeState"]=null;
				tr["_treeSpan"]=span;
				tr["_treeDisplay"]="block";
				tr["_treeLoadChild"]=1;
				span["_treeTr"]=tr;
				for(var i=imgs.length-1;i>=0;i--){
					if(i==imgs.length-1){
						tr["_treeIcon"]=imgs[i];
						imgs[i]["_treeOpenSrc"]=i_i_o;
						imgs[i]["_treeCloseSrc"]=i_i_c;
					}
					if(i==imgs.length-2){
						tr["_treeState"]=imgs[i];
						imgs[i]["_treeOpenSrc"]=i_s_o;
						imgs[i]["_treeCloseSrc"]=i_s_c;
					}
					imgs[i]["_treeTr"]=tr;
				}
				previousSiblingTr.parentNode.insertBefore(tr,parentSiblingTr);
				previousSiblingTr=tr;
				if(parentTr==table["_treeFirstTr_"])
					initTrFun(null,tr);
				else
					initTrFun(parentTr,tr);
				tr=ntr;
			}
		}
		function initTrFun(parentTr,tr){
			tr["_treeParentItem"]=parentTr;
			tr.getPrefixTd=function(){
				var str="";
				var p=this;
				while(p!=null&&p["_treeLevel"]!="-1"){
					if(p["_treeIsLast"]==true)
						str="B"+str;
					else
						str="L"+str;
					p=p["_treeParentItem"];	
				}
				return str;
			}
			tr.getParentTreeItem=function(){
				return this["_treeParentItem"];
			}
			initTrFunLabel(tr);
			if(tr["_treeType"]=="folder")
				initTrFunFolder(tr);
		}
		function initTrFunLabel(tr){
			tr.select=function(sel){
				if(sel==true){
					if(this["_treeTable"]["_treeSelectedTr"]!=null)
						this["_treeTable"]["_treeSelectedTr"].select(false);
					this["_treeTable"]["_treeSelectedTr"]=this;
					this["_treeSpan"]["_oldSelColor"]=this["_treeSpan"].style["background"];
					this["_treeSpan"].style["background"]=this["_treeTable"]["_treeselectedColor"];
				}else{
					this["_treeTable"]["_treeSelectedTr"]=null;
					this["_treeSpan"].style["background"]=this["_treeSpan"]["_oldSelColor"];
				}
			}
			tr["_treeSpan"].onclick=function(){
				this["_treeTr"].select(true);
				if(this["_treeTr"]["_treeCode"]!=""&&this["_treeTr"]["_treeTable"]["_treeaction"]!=null&&this["_treeTr"]["_treeTable"]["_treeaction"]!=""){
					var fun=window.eval(this["_treeTr"]["_treeTable"]["_treeaction"]);
					fun(this["_treeTr"]["_treeType"]=="file"?true:false,this["_treeTr"]["_treeCode"],this.innerText,this["_treeTr"]);
				}
			}
			tr["_treeSpan"].onmouseover=function(){
				this["_oldMouseColor"]=this.style["color"];
				this.style["color"]=this["_treeTr"]["_treeTable"]["_treemouseOverColor"];
			}
			tr["_treeSpan"].onmouseout=function(){
				this.style["color"]=this["_oldMouseColor"];
			}
		}
		function initTrFunFolder(tr){
			tr.showChildren=function(show){
				if(show==true){
					this["_treeIcon"].src=this["_treeIcon"]["_treeOpenSrc"];
					if(this["_treeState"]!=null)
						this["_treeState"].src=this["_treeState"]["_treeOpenSrc"];
				}else{
					this["_treeIcon"].src=this["_treeIcon"]["_treeCloseSrc"];
					if(this["_treeState"]!=null)
						this["_treeState"].src=this["_treeState"]["_treeCloseSrc"];
				}
				showHideChild(this,show);
			}
			tr.loadChildren=function(){
				var trData=getTrData(parseInt(this["_treeLevel"])+1,this.getPrefixTd(),tr["_treeTable"],tr["_treeCode"]);
				this["_treeLoadChild"]=2;
				addTreeItems(tr["_treeTable"],this,trData);
				this["_treeLoadChild"]=3;
				this.showChildren(true);
			}
			tr.doShowChildren=function(){
				var show=true;
				if(this["_treeIcon"].src==this["_treeIcon"]["_treeOpenSrc"])
					show=false;
				if(this["_treeLoadChild"]==3)
					this.showChildren(show);
				else if(this["_treeLoadChild"]==1&&show==true){
					this.loadChildren();
				}
			}
			tr.reloadChildren=function(){
				var level=parseInt(this["_treeLevel"])+1;
				var ts=this.nextSibling;
				var ntr;
				while(ts!=null&&ts["_treeLevel"]>=level){
					ntr=ts.nextSibling;
					ts.parentNode.removeChild(ts);
					ts=ntr;
				}
				this.loadChildren();
			}
			if(tr["_treeState"]!=null)
				tr["_treeState"].onclick=function(){
					this["_treeTr"].doShowChildren();
				}
			tr["_treeIcon"].ondblclick=function(){
					this["_treeTr"].doShowChildren();
				}
		}
		function showHideChild(tr,show){
			var level=parseInt(tr["_treeLevel"])+1;
			var ts=tr.nextSibling;
			while(ts!=null&&ts["_treeLevel"]>=level){
				if(show==true){
					if(ts["_treeLevel"]==level){
						ts.style["display"]="block";
						ts["_treeDisplay"]="block";
					}else
						ts.style["display"]=ts["_treeDisplay"];
				}else{
					ts.style["display"]="none";
					if(ts["_treeLevel"]==level){
						ts["_treeDisplay"]="none";
					}
				}
				ts=ts.nextSibling;
			}
		}
		//part 3 begin
		function getTrData(level,prefixTd,paras,itemCode){
			var data="";
			if(element.fromdict=="true"){
				if (paras["_treepara"]=="") {
					data = window.getDict(paras["_treedataid"],"<treeItemCode>"+itemCode+"</treeItemCode>");
				} else {
					data = window.getDict(paras["_treedataid"],paras["_treepara"]+"<treeItemCode>"+itemCode+"</treeItemCode>");
				}
			}else{
				xmlhttp.post(element.dataid,"<treeItemCode>"+itemCode+"</treeItemCode>");
				data = xmlhttp._object.responseXML;
			}
			if(typeof(data)=="undefined")
				return "";
			var img_blank=getImagePath("blank.png");
			var img_vline=getImagePath("vline.png");
			var img_file=getImagePath("file.png");
			var img_folder_open=getImagePath("folderopen.png");
			var img_folder_close=getImagePath("folderclose.png");
			var img_lline_leaf=getImagePath("L.png");
			var img_lline_open=getImagePath("Lminus.png");
			var img_lline_close=getImagePath("Lplus.png");
			var img_tline_leaf=getImagePath("T.png");
			var img_tline_open=getImagePath("Tminus.png");
			var img_tline_close=getImagePath("Tplus.png");
			
			prefixTd=prefixTd.replace(/B/g,"<td width='"+paras["_treeimageWidth"]+"' height='"+paras["_treeimageHeight"]+"'><img src='"+img_blank+"'/></td>");
			prefixTd=prefixTd.replace(/L/g,"<td width='"+paras["_treeimageWidth"]+"' height='"+paras["_treeimageHeight"]+"'><img src='"+img_vline+"'/></td>");
			var trsStr="";
			var v_code,v_label,v_leaf;
			var i_state_o,i_state_c,i_image_o,image_c,i_label,i_type;
			var pas=data.getElementsByTagName("para");
			for(var i=0;i<pas.length;i++){
				v_code=pas[i].getAttribute("code");
				v_label=pas[i].getAttribute("value");
				v_leaf=pas[i].getAttribute("isleaf");
				if(v_leaf=="1")
					i_type="file";
				else
					i_type="folder";
				if(i==(pas.length-1)){
					if(v_leaf==1){
						i_state_o=img_lline_leaf;
						i_state_c=img_lline_leaf;
						i_image_o=img_file;
						i_image_c=img_file;
					}else{
						i_state_o=img_lline_open;
						i_state_c=img_lline_close;
						i_image_o=img_folder_open;
						i_image_c=img_folder_close;
					}
				}else{
					if(v_leaf==1){
						i_state_o=img_tline_leaf;
						i_state_c=img_tline_leaf;
						i_image_o=img_file;
						i_image_c=img_file;
					}else{
						i_state_o=img_tline_open;
						i_state_c=img_tline_close;
						i_image_o=img_folder_open;
						i_image_c=img_folder_close;
					}
				}
				if(level>-1)
					i_state="<td width='"+paras["_treeimageWidth"]+"' height='"+paras["_treeimageHeight"]+"'><img src='"+i_state_c+"'  style='cursor:hand'/></td>"
				else
					i_state="";
				
				i_image="<td width='"+paras["_treeimageWidth"]+"' height='"+paras["_treeimageHeight"]+"'><img src='"+i_image_c+"'  style='cursor:hand'/></td>";
				i_label="<td colspan='"+(100-level)+"'  align='left'  nowrap='true'><span style='cursor:hand'>"+v_label+"</span></td>";
				i_ccc="_A_";
				/// state,image,code 5
				i_class=addTdPara(level)
					+addTdPara(i_state_o)
					+addTdPara(i_state_c)
					+addTdPara(i_image_o)
					+addTdPara(i_image_c)
					+addTdPara(v_code)
					+addTdPara(i_type);
				trsStr+="<tr >"+prefixTd+i_state+i_image+i_label+i_class+"</tr>";
			}
			return trsStr;
		}
		function getImagePath(name){
			return window.prefix+"base/themes/blue/images/treetable/"+name;
		}
		function addTdPara(v){
			return "<td>"+v+"</td>"
		}


  	jhtc_attr_init(element,"action","");
  	jhtc_attr_init(element,"expand","1");
  	jhtc_attr_init(element,"fromdict","true");
  	jhtc_attr_init(element,"dataid",null);
  	jhtc_attr_init(element,"para","");
  	
	element.refresh=refresh;
	element.getCode=getCode;
	element.getLabel=getLabel;
	element.getTreeItem=getTreeItem;
	element.init=init;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_class_map["dTreeCtn"]=jhtc_dtree;