function jhtc_inputText(win,jhtc_obj){
	return jhtc_inputText_c(__jhtcElementFun(jhtc_obj));
}	
function jhtc_inputText_c(_e){
	var textFPairObj=null;
	var proxyLabel=null;
	var invalidRegExp=new RegExp("[<>]","gi");//("[\\w|\\u4e00-\\u9fa5]","gi");
	function init() {
		var element=_e();
		if(element.label=="preTd"){
			var preTd=element.parentNode;
			while(preTd!=null){
				if(preTd.tagName.toLowerCase()=="td"){
					if(preTd.previousSibling!=null){
						proxyLabel=preTd.previousSibling.innerText;
						if(proxyLabel.indexOf(":")>0)
							proxyLabel=proxyLabel.split(":")[0];
						if(proxyLabel.indexOf("：")>0)
							proxyLabel=proxyLabel.split("：")[0];
					}
					break;
				}
				preTd=preTd.parentNode;
			}
			element.label=null;
		}else if(element.label=="preText"){
			var preTd=element.parentNode;
			var preT=preTd.firstChild;
			if(preT!=element)
				proxyLabel=preT.nodeValue.replace("：","").replace(":","");
			element.label=null;
		}else if(element.label!=null)
			proxyLabel=element.label;
		element.htcLabel=proxyLabel==null?"":proxyLabel;
		if (window.trim(element.name) == '' && element.submit == 'true' && (this.className==null || this.className.search(/^inputTextTable/)==-1)) {
			alert('此input text输入框，没有指定name')
			return;
		}
		
		if (element.label != null) {
			if(element.labelFix=="true"){//固定label的宽度
				// -- label 的基本长度
				var lableLength = 120
				//var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
				//if (spanWidth < lableLength) spanWidth = lableLength;
				var spanWidth = lableLength;
				var aKey = ""
				if (window.trim(element.accessKey) != '' && !element.readOnly){
					//aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)"
				}
				
				element.insertAdjacentHTML("beforeBegin", "<span  title='" + element.label + "'  nowrap style='text-align:right;width:"+spanWidth+"px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";font-weight:normal;white-space: nowrap;text-overflow:ellipsis; overflow:hidden;'>"+element.label+aKey+(element.label==''?"":"：")+"</span>");
		}else{
				// -- label 的基本长度
				var lableLength = 120
				var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
				if (spanWidth < lableLength) spanWidth = lableLength;
				var aKey = ""
				if (window.trim(element.accessKey) != '' && !element.readOnly){
					aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)"
				}
				
				element.insertAdjacentHTML("beforeBegin", "<span  title='" + element.label + "'  nowrap style='text-align:right;width:"+spanWidth+"px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";font-weight:normal;'>"+element.label+aKey+"：</span>");
		}
		}
		if(element.parentNode==null)
			return;
		element.parentNode.noWrap = true;
		
		if (element.readOnly) {
			element.style.backgroundColor='#EDEDED'
			element.style.fontWeight = 'bold'
		} else if ('true'==element.required) {
			element.style.backgroundColor=JHTC_COLOR.REQUIRED;
		}
		
		element.onkeydown=alterEnter;
		if (element.maxLength == 2147483647) {
		  element.maxLength = parseInt(jQuery(element).attr("maxInput"))+(element.className == 'inputDecimal'? 1:0);
	  }

		if (element.className == 'inputDecimal' || element.className == 'inputInteger') {
			element.onkeyup = ignorePoint;
		}
		if (element.className == 'inputTextTableMoney' || element.className == 'inputDecimal' || element.className == 'inputInteger') {
			element.onkeypress = ignoreIllegal;
			element.style.imeMode = 'disabled';  //禁用输入法
			element.onpaste = checkPaste;  //校验粘贴事件
		}
		if (element.className != 'inputTextTableMoney' && element.className != 'inputTextTable') {
			element.style.width = element.extent;
		}
		if (element.className == 'inputTextTableMoney') {
			element.onblur = check;
			//对应bug HBOSTHIRDNEW-2980
			//修改思路为：让获得焦点的输入框值默认选中增强系统易用性  ---李乃坤
			element.onfocus = function(){
				element.select();
        }
		}
		if(element.className=="inputTextF"){
			element.insertAdjacentHTML("afterEnd", "<input type='text'>");
			textFPairObj=element.nextSibling;
			textFPairObj.className="commonInputStyle";
			textFPairObj.style.backgroundColor=element.style.backgroundColor;
			textFPairObj.style.fontWeight=element.style.fontWeight;
			textFPairObj.style.width=element.style.width;
			textFPairObj.readOnly=true;
			element.style.display="none";
			initInputTextF();
		}
		_processAutoHidden(element,jQuery(element).attr("autohide"));
	}
	function initInputTextF(){
		_e().focus=_e().onfocus=function(){
			this.nextSibling.focus();
	
		}
		_e().select=function(){
			this.nextSibling.select();
		}
	}
	function alterEnter(){
		if(event.keyCode==13){
			event.keyCode=9;
		}
	}
	//阻止输入域输入数字和 '.+-'之外的字符
	function ignoreIllegal() {
		var element=_e();
		if (element.className == 'inputInteger') {
			if (((event.keyCode>=48) && (event.keyCode<=57))||event.keyCode==43||event.keyCode==45)
			return true;
			else
			return false;
		} else {
			if (((event.keyCode>=48) && (event.keyCode<=57))||event.keyCode==46||event.keyCode==43||event.keyCode==45) {
				return true;
			} else {
				return false;
			}
		}
	}
	function _processAutoHidden(e,v){
		if(v=="true")
	    	e.style.display="none";
	    else if(v.substr(0,1)=="p"&&v.length>1){
	    	var ps=v.substr(1);
	    	var pn=parseInt(ps,10);
	    	if(!isNaN(pn)){
	    		var pa=e;
	    		for(var i=0;i<pn;i++){
	    			pa=pa.parentNode;
	    		}
	    		if(pa!=null){
	    			pa.style.display="none";
	    		}
	    	}
	    }
	}
	//阻止输入小数位多于point位
	function ignorePoint() {
		var element=_e();
		if (element.value.indexOf(".")!=-1) {
			if(element.value.length -element.value.indexOf(".")-1 > element.point){
				element.value = element.value.substring(0,element.value.indexOf(".")+(1+parseInt(element.point)))
			}
//			if(element.value.indexOf(".")>element.maxLength-element.point){
//				element.value = element.value.substr(0,element.maxLength-element.point)
//			}
		}
		if(element.className == 'inputDecimal'){
			if (element.value.indexOf(".")==-1 && element.value.length> (element.maxLength-element.point-1)) {
				element.value = element.value.substr(0,element.maxLength-element.point-1)
			}
		}
	}
	
	//粘贴事件里做校验
	function checkPaste() {
		var element=_e();
		var selStr = document.selection.createRange();
		var selText = selStr.text;
		selStr.moveStart("character",-element.value.length); 
		if(selText ==''){
			element.value = element.value.substring(1,selStr.text.length) + '' + window.clipboardData.getData('Text') + element.value.substring(selStr.text.length  ,element.value.length);  //获取复制后文本框应该得到的值
		}else{
			element.value = element.value.replace(selStr.text,window.clipboardData.getData('Text'));
		}
		
		//alert(element.value)
		if (check()) {
			if (element.className!="inputDecimal" && element.className!="inputInteger")  /// edit by yangqing  text 类型没有onchange事件
			{
				this.onchange();
			}
			
			return true;
		} else {
			element.value = '';  //校验非法，清空文本框
			return false;
		}
	}
	
	// 刷新页面
	function refresh() {
		var element=_e();
		if (trim(element.required.toLowerCase())=="true") {
			element.style.backgroundColor=JHTC_COLOR.REQUIRED;
		} else {
			element.style.backgroundColor="#FFFFFF";
		}
	}
	
	function check(hideMsg) {
		var element=_e();
		if(typeof(hideMsg)=="undefined"){
			hideMsg=false;
		}
		if ('true'==element.required) {
			if (window.trim(element.value) == '')  {
				if (proxyLabel != null) {
					showAlert(hideMsg,proxyLabel+"不能为空！")
				}else{
					showAlert(hideMsg,"请输入必输项！");
				}
				setElementFocus()
				element.select()
				return false
			}
		}
		element.value=window.trim(element.value);
		if(element.className == 'inputTextA'&&element.value!=""&&element.anyLetter=="false"){
			if(element.value.replace(invalidRegExp,"").length!=element.value.length){
				try{
					setElementFocus()
					showAlert(hideMsg,"不能输入系统标识符!");
					setElementFocus();
				    element.select()
				    return false
			        }catch(E){
			        	return true;
			        }
			}
		}
		if(element.value.indexOf("'")>=0 && element.offsetWidth>0 ){
			try{
				element.focus();
				showAlert(hideMsg,(proxyLabel==null?"":proxyLabel)+"不能输入单引号");
				element.focus();
				element.select()
				return false;
		        }catch(E){
		        	
		        }
		}
		if (element.min!=null) {
			if (isNaN(element.min)) {
				showAlert(hideMsg,"属性min应为数字")
				return false;
			}
			
			if (isNaN(element.value)) {
				if (proxyLabel != null) {
					showAlert(hideMsg,proxyLabel+"应为数字")
				}
				setElementFocus()
				element.select()
				return false
			}
			
			if (parseFloat(element.value)<element.min) {
				if (proxyLabel != null) {
					showAlert(hideMsg,proxyLabel+"应不小于"+element.min)
				}
				setElementFocus()
				element.select()
				return false
			}
		}
		
		if (element.max!=null) {
			if (isNaN(element.max)) {
				showAlert(hideMsg,"属性max应为数字")
				return false;
			}
			
			if (isNaN(element.value)) {
				if (proxyLabel != null) {
					showAlert(hideMsg,proxyLabel+"应为数字")
				}
				setElementFocus()
				element.select()
				return false
			}
			
			if (parseFloat(element.value)>element.max) {
				if (proxyLabel != null) {
				    showAlert(hideMsg,proxyLabel+"应不大于"+element.max)
				}
				setElementFocus()
				element.select()
				return false
			}
		}
		
		if (element.className == 'inputInteger') {
			if (isNaN(element.value)) {
				if (proxyLabel != null) {
				    showAlert(hideMsg,proxyLabel+"应为数字")
				} else {
				    showAlert(hideMsg,"该文本框输入应为数字")
				}
				setElementFocus()
				element.select()
				return false
			}
		}
		
		if (element.className == 'inputDecimal' || element.className == 'inputTextTableMoney') {
			if (isNaN(element.value)) {
				if (proxyLabel != null) {
				    showAlert(hideMsg,proxyLabel+"应为数字")
				} else {
				    showAlert(hideMsg,"该文本框输入应为数字")
				}
				setElementFocus()
				element.select()
				return false
			}

			if (element.value.indexOf(".")!=-1) {
				if (element.value.length -element.value.indexOf(".")-1 > element.point){
					if (element.point == 0) {
						showAlert(hideMsg,"该文本框输入应为整数");
					} else if (proxyLabel != null) {
						showAlert(hideMsg,proxyLabel+"小数位应不大于"+element.point+"位")
					} else
						showAlert(hideMsg,"小数位应不大于"+element.point+"位")
                    setElementFocus()
                    element.select()
                    return false
                } else if (element.value.indexOf(".")  > (element.maxLength - element.point - 1)) {
                    showAlert(hideMsg, "该文本框输入整数位不应大于" + (element.maxLength - element.point) + '位');
                    setElementFocus()
                    element.select()
                    return false
                }
			}

			if(element.value.indexOf(".")==-1){
				if(element.value.length>(element.maxLength-element.point-1)){
					showAlert(hideMsg,"该文本框输入的整数位不能大于"+(element.maxLength-element.point-1)+"位");
					return false
				}
			}
		}
		return true;
	}
	function setElementFocus(){
		var element=_e();
		try{
			element.focus();
		}catch(E){}
	}
	function showAlert(hideMsg,msg){
		if(hideMsg==true)
			return;
		alert(msg);
	}
	
	function setText(){
		var element=_e();
		if(textFPairObj!=null)
			textFPairObj.value=element.text;
	}
	function setDisabled(mboole){
		var element=_e();
		element.disabled = (mboole.toString().toLowerCase() == 'true') || (mboole.toString().toLowerCase() == '1') ? true : false;
	}
	function _initAttr(){
		var element=_e();
		jhtc_attr_init(element,"label",null);
		jhtc_attr_init(element,"required","false");
		jhtc_attr_init(element,"submit","true");
		jhtc_attr_init(element,"autohide","false");
		jhtc_attr_init(element,"extent","140");
		jhtc_attr_init(element,"maxInput","50");
		jhtc_attr_init(element,"anyLetter","true");
		jhtc_attr_init(element,"min",null);
		jhtc_attr_init(element,"max",null);
		jhtc_attr_init(element,"point","2");
		jhtc_attr_init(element,"text","");
		jhtc_attr_init(element,"labelFix","false");
		
		__jhtcBindPropertyChange(element,"text",setText);
		element.check=check;
		element.refresh=refresh;
		element.setDisabled=setDisabled;
		element.init=init;
		element.jhtcInit=init;
	}
	_initAttr();
	return null;
};
jhtc_class_map["inputTextA"]=jhtc_inputText;
jhtc_class_map["inputTextF"]=jhtc_inputText;
jhtc_class_map["inputInteger"]=jhtc_inputText;
jhtc_class_map["inputDecimal"]=jhtc_inputText;
jhtc_class_map["inputTextTable"]=jhtc_inputText;
jhtc_class_map["inputTextTableMoney"]=jhtc_inputText;