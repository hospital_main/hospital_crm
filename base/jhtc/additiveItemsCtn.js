function jhtc_additiveItemsCtn(win,jhtc_obj){
		var pageWin=win;
		var element=jhtc_obj;
		var response=null;
		var initV = new Array();
		var compC='';
		function init(){
			if(trim(element.additive_type)=="" || element.additive_type==null){
				alert('请设置additive_type')
        return false;
			}
			if(element.value!='')
				compC=element.value;
			else
				compC=getCompCode();
			load();
		}
		
		function reload(code){
			load();
			if(code!=null && window.trim(code)!=""){
				submit(code,"query");
			}
		}
		
		function submit(code,action,hideMsg){
			 if (arguments.length < 2) {
			    alert("参数个数不对")
			    return;
			  }
				window.xmlhttp.post("sysAdditiveItemsAction",xmlAssemble(),"?action="+action+"&comp_code="+compC+"&additive_type="+element.additive_type+"&item_code="+code)
				var str = window.xmlhttp._object.responseText;
				if(action!="query"){
					if (window.dialogArguments!=null) {
			        try {
			          window.dialogArguments._obj1.refresh()
			        } catch (exception){}
		      }	
		      return window.doMsg(str,hideMsg);	
				}else{
		       setValues(str);
				}
		}
		
		function setValues(str){
			var srcTree = new ActiveXObject("Microsoft.XMLDOM");
      srcTree.async=false;
      srcTree.loadXML(str);
      
      var list1 = srcTree.getElementsByTagName("tbody");
 		 	
      for (var i=0; i<list1.length; i++) {
        var list2 = list1.item(i).getElementsByTagName("td");
        initV.length = list2.length
        for (var j=0; j<list2.length; j++) {
          if (list2.item(j).firstChild != null) {
            initV[j] = list2.item(j).firstChild.nodeValue
          } else
            initV[j] = "";
       }
      }
      if(initV.length==0)
 		 	return ;
			var inputs=element.getElementsByTagName("input");
			
      for (var i=0, j=0; i<inputs.length; i++) {
        if (window.trim(inputs[i].name) != "") {
          if (inputs[i].className != null && (inputs[i].className.search(/Select/i) != -1 || inputs[i].className == "inputRadio" || inputs[i].className == "inputCheckBox" || inputs[i].className == "inputTextarea")) {
            inputs[i].setValue(initV[j]);
            j++;
          } else if (inputs[i].className != null && inputs[i].className.search(/input/i) != -1) {
            inputs[i].value= initV[j];
            j++;
          }
        }
      }
 		 	
		}
		
		/*重置*/
		function reset(){
			load();
			return;
		}
		
		/*清空*/
		function clear(){
			var inputs= this.getElementsByTagName("input");
			
			if(inputs==null){
				return;
			}
    	for (var i=0; i<inputs.length; i++) {
      	inputs[i].value="";
    	}
		}
		
		function load(){ 
			
			if(compC=='' || ""+compC=="undefined" ){
				if(typeof(getCompCode)=="function"){
					compC=getCompCode();
					} 
				} 
			window.xmlhttp.post("sysAdditiveItemsAction","","?action=load&comp_code="+compC+"&additive_type="+element.additive_type);
			var source = window.xmlhttp._object.responseText;
			var vXml = inputXML(source);
			response = vXml.selectSingleNode("//root").cloneNode(true);
			
			var str=assemble();
			jhtcSetHtml(element,"<table style='width:470' class='formCtn'>"+str+"</table>");
		}
		
		function refreshTab(){
				
		}
		
		function refresh(){
				
		}
		
	  function inputXML(argSource){ ////objXMLDoc接收XML文件数据
	    var objXMLDoc = null;
	    try{
	      switch(typeof(argSource)){
	        case "string"://XML字节流
	          if(argSource.search(/\./) != -1 && argSource.search(/\</) == -1){ //xml file's location
	
	            objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");
	            objXMLDoc.async = false;
	            if(argSource.search(/\+/) != -1) argSource = eval(argSource);
	              objXMLDoc.load(argSource);
	              break;
	            }
	            if(argSource.search(/\</) != -1){ //xml string
	              objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");
	              objXMLDoc.loadXML(argSource);
	              break;
	            }
	            objXMLDoc = eval(argSource);
	            objXMLDoc = (objXMLDoc.XMLDocument)?(objXMLDoc.XMLDocument):null; //xml data island
	
	            break;
	        case "object"://XMLDOM对象
	            if(argSource.xml) return objXMLDoc = argSource; //xml document object
	            break;
	            default:
	            objXMLDoc = null;
	      }
	      if (!objXMLDoc.xml) objXMLDoc = null;
	    } catch(err) { objXMLDoc = null; }
	
	    if(objXMLDoc)
	    {
	  		objXMLDoc.setProperty("SelectionLanguage","XPath");
	  		objXMLDoc.setProperty("SelectionNamespaces","xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
	    }
	    return objXMLDoc;
	   }
	   
	   function assemble(){
		   	var str="";
		   	var trs=response.getElementsByTagName("tr");
		   	
		   	for(var i=0;i<trs.length;i++){
		   		if(i!=0){
		   			if(i%element.tdz==0){
		   				if(i!=trs.length){
		   					str+="</tr>";
		   					str+="<tr>";
		   				}
		   			}
			   	}else{
			   		str+="<tr>";
			   	}
			   	var tds=trs[i].getElementsByTagName("td");
		   		str+="<td width=80 align='right'>"; 
		  		var classtype="";
		  		if(tds[1].text=="1"){
		  			classtype="inputTextA";
		  		}else if(tds[1].text=="2"){
		  			classtype="inputDecimal";
		  		}else if(tds[1].text=="3"){
		  			classtype="inputCalendar";
		  		}
		   		str+=tds[0].text+"：</td><td ><input type=text class='"+classtype+"' name='item"+tds[2].text+"' extent='140'/>";
		   		str+="</td>";		   			
		   	}
		   	str+="</tr>";
		   	return str;
	  }
	  
	  
  /*
   * 简单表格的XML组装
   */
  function xmlAssemble() {
    var result = "";
    var inputs= element.getElementsByTagName("input")
    for (var i=0; i<inputs.length; i++) {
      if (window.trim(inputs[i].name) == "") continue;
      // -- TBD 对于变量需要转换一些特殊符号，如/、<、>等
      if(inputs[i].type=='checkbox' || inputs[i].type=='radio'){
      	//result+="<" + inputs[i].name + ">" + inputs[i].checked+"</" + inputs[i].name +">";
      }else{
		    result = result + "<" + inputs[i].name + ">" + inputs[i].value+"</" + inputs[i].name +">";
      }
    }
    //submitXml = result;
    return result;
  }


  	jhtc_attr_init(element,"additive_type",null);
  	jhtc_attr_init(element,"tdz","2");
  	
	element.clear=clear;
	element.reload=reload;
	element.reset=reset;
	element.submit=submit;
	element.init=init;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_class_map["additiveItemsCtn"]=jhtc_additiveItemsCtn;