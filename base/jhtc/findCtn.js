function jhtc_findCtn(win,jhtc_obj){
		var pageWin=win;
		var element=jhtc_obj;
		var constBgColor="yellow";
		
		function _init(){
			if(element.keycolor!=null)
				element._keyWordBgColor=element.keycolor;
			if(element.auto=="true")
				element.onchange=element.onkeyup=element.onblur=element.ondragend=element.onpaste=_onChangeText;
			else
				element.onkeydown=_onChangeText;
			element.insertAdjacentHTML("beforeBegin", "快速定位：");
			element.insertAdjacentHTML("afterEnd","<button class='pageBtn' accessKey='F' style='display:none'>查找</button>");
			var btn=element.nextSibling;
			btn.onclick=function(){
				_setWorkTable(element);
				_resetTd(element);
				_onFind(element);
			}
			element._lastIndex=0;
			if(element.keycolor!=null)
				element._keyWordBgColor=element.keycolor;
			else
				element._keyWordBgColor=constBgColor;
		}
		function _setWorkTable(element){
			element._workTable=element.document.getElementById(element.target);
		}
		function _onChangeText(){
			if(element.auto=="true"){
			_setWorkTable(this);
			_resetTd(this);
			_onFind(this);
			}else{
				if (event.keyCode == 13){
					_setWorkTable(this);
					_resetTd(this);
					_onFind(this);
				
				}
			}
		}
		function _resetTd(myElement){
			if(myElement._workTable==null)
				return ;
			var tds=myElement._workTable.getElementsByTagName("td");
			if(tds.length<1)
				return ;
			for(var i=0;i<tds.length;i++){
				if(tds[i].getAttribute("oldFindBgColor")!=null){
					tds[i].style.backgroundColor=tds[i].getAttribute("oldFindBgColor");
				}
			}
		}
		function _onFind(myElement){
			if (element.findBefore!=""){
				var myf=window.eval(element.findBefore);
				myf();
			}
			if(myElement._workTable==null)
				return ;
			if(element.value=="")
				return ;
			var tds=myElement._workTable.getElementsByTagName("td");
			if(tds.length<1)
				return ;
			var firstTd=null;
			var i=myElement._lastIndex;
			for(;i<tds.length;i++){
				if(tds[i].innerText.indexOf(element.value)>=0){
					if(firstTd==null){
						firstTd=tds[i];
					}
					tds[i]["oldFindBgColor"]=tds[i].style.backgroundColor;
					tds[i].style.backgroundColor=myElement._keyWordBgColor;
				}
			}
			if(firstTd!=null){
				firstTd.scrollIntoView(false);
				if(element.onfind!=""){
					var f=eval(element.onfind);
					f(firstTd);
				}
			}
		}
	


  	jhtc_attr_init(element,"keycolor",null);
  	jhtc_attr_init(element,"onfind","");
  	jhtc_attr_init(element,"auto","true");
  	jhtc_attr_init(element,"findBefore","");
  	jhtc_attr_init(element,"target","_mainDataTable");
  	
	element._init=_init;
	element.jhtcInit=function(){
		element._init();
	};
	return null;
};
jhtc_class_map["findCtn"]=jhtc_findCtn;


