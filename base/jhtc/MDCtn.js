function jhtc_MDCtn(win,jhtc_obj){

  var pageWin=win;

  var element=jhtc_obj;

  //*************

  //要为detail 提供名为 getInputDefine(cell)的函数才能设置detail的输入

  //输入框:inputTextA,inputInteger列表选择inputSelectZ

  //***********

  

  var baseDivTop=0, baseDivLeft=0, baseDiv, headDiv, parentObj; // 调整位置

  var vBtnHTML; // 左上的按钮

  var isinsert=false;

  function dInsert() {

    if (arguments.length<1) {

      alert('请输入添加参数')

      return false;

    }

    if (element.vDom.getElementsByTagName('tbody').length<1)

      element.vDom.appendChild(element.vDom.createElement('tbody'));



    var tbody = element.vDom.getElementsByTagName('tbody').item(0)

    var tr = element.vDom.createElement('tr')

    tbody.appendChild(tr)

    for (var i=0; i<arguments.length; i++) {

      var td = element.vDom.createElement('td')

      td.text = arguments[i]

      tr.appendChild(td)

    }

    //dRefresh()  //此处注释掉刷新，循环调用完dInsert()以后再刷新，这样提高程序运行效率，不然数据量大了速度会很慢，注意循环体外调用dRefresh()

					// 主要用于 需求计划汇总

    return true;

  }



  function dRemove() {

    var vFlag = false

    var trs = element.vDom.getElementsByTagName('tr')

    for (var i=0; i<trs.length; i++) {

      if (trim(trs[i].getAttribute("chose"))=='true' && trs[i].parentNode!=null) {

        if (vFlag==false && confirm('确定要删除吗?')==false) {

          return false;

        }

        trs[i].parentNode.removeChild(trs[i])

        vFlag = true

      }

    }

    if (vFlag==false) {

      alert('请先选择再删除')

      return false;

    }

    dRefresh()

    return true;

  }



  function dRemoveAll() {

    var vFlag = false

    var trs = element.vDom.getElementsByTagName('tr')

    for (var i=0; i<trs.length; i++) {

      trs[i].parentNode.removeChild(trs[i])

    }

    dRefresh()

    return true;

  }



  function dLoad() {

    var result = new Array()

    var td = window._obj1.parentNode

    if (td==null || td.parentNode==null) return result;

    var theadIdx=0;

    if (td.parentNode.parentNode.parentNode.getElementsByTagName('thead').length>0)

      theadIdx = td.parentNode.parentNode.parentNode.getElementsByTagName('thead')[0].rows.length

    var trIdx = td.parentNode.rowIndex-theadIdx

    // 得到当前记录

    if (element.vDom.getElementsByTagName('tbody').length<1) return result;

    var tbody = element.vDom.getElementsByTagName('tbody')[0]

    if (tbody.getElementsByTagName('tr').length<(trIdx+1)) return result;

    var tr = tbody.getElementsByTagName('tr')[trIdx];

    for (var i=0; i<tr.getElementsByTagName('td').length; i++) {

      result.length++;

      if (tr.getElementsByTagName('td')[i].firstChild!=null)

      	result[i]=tr.getElementsByTagName('td')[i].firstChild.nodeValue

     	else

     		result[i]=''

    }

    return result

  }



  function dUpdate() {

    if (arguments.length<1) {

      alert('请输入添加参数')

      return false;

    }

    var td = window._obj1.parentNode

    if (td==null || td.parentNode==null) return false;

    var theadIdx=0;

    if (td.parentNode.parentNode.parentNode.getElementsByTagName('thead').length>0)

      theadIdx = td.parentNode.parentNode.parentNode.getElementsByTagName('thead')[0].rows.length

    var trIdx = td.parentNode.rowIndex-theadIdx

    // 修改记录

    if (element.vDom.getElementsByTagName('tbody').length<1)

      element.vDom.appendChild(element.vDom.createElement('tbody'));



    var tbody = element.vDom.getElementsByTagName('tbody').item(0)

    var tr = element.vDom.createElement('tr')

    tbody.replaceChild(tr, tbody.getElementsByTagName('tr')[trIdx])

    for (var i=0; i<arguments.length; i++) {

      var td = element.vDom.createElement('td')

      td.text = arguments[i]

      tr.appendChild(td)

    }

    dRefresh()

    return true;

  }



  function reset(nPara) {

    var initV = new Array();

    if (trim(element.load)!='') {

      var para = _clearIFdlgTag(window.location.href).replace(/(^[^=\n]*=)|(\?[^\?\n]*$)/g, "");

      if(nPara)

      	para=nPara;

      para = para.replace(/#$/, '');

      window.xmlhttp.post(element.load, para, '?subFunc=load')



      var srcTree = new ActiveXObject("Microsoft.XMLDOM");

      srcTree.async=false;

      srcTree.load(xmlhttp._object.responseXML);



      var tbodys = srcTree.getElementsByTagName("tbody");

      for (var i=0; i<tbodys.length; i++) {

        var tds = tbodys.item(i).getElementsByTagName("td");

        initV.length = tds.length

        for (var j=0; j<tds.length; j++) {

          if (tds.item(j).firstChild != null)

            initV[j] = tds.item(j).firstChild.nodeValue

          else

            initV[j] = ''

        }

      }

      window.xmlhttp.post(element.load, para, '?subFunc=load_detail')

      element.vDom = xmlhttp._object.responseXML

    } else {

      element.vDom = new ActiveXObject("Microsoft.XMLDOM");

      element.vDom.async = false;

      element.vDom.loadXML("<root><tbody></tbody></root>");

    }

    dRefresh()



    var inputs = main.getElementsByTagName("input")

     if((element.load!=""&&element.load!=null)||(isinsert==true)){

    for (var i=0, j=0; i<inputs.length; i++) {

      if (window.trim(inputs[i].name) != '') {

        if (initV.length<j+1) {

          initV.length++;

          initV[j]=''

        }

        if (inputs[i].className != null && (inputs[i].className.search(/Select/i) != -1 || inputs[i].className == "inputRadio" || inputs[i].className == "inputCheckBox" || inputs[i].className == "inputTextarea")) {

          // 隐藏的值,如果是必输, 不能置空

          if (!(inputs[i].style.display=='none' && trim(initV[j])=='' && 'true'==inputs[i].required))

            if(((inputs[i].required=="true"))&&(isinsert==true))

            {}

            else

            {inputs[i].setValue(trim(initV[j]));}

          j++

        } else if (inputs[i].className != null && inputs[i].className.search(/input/i) != -1) {

          // 隐藏的值,如果是必输, 不能置空

          if (!(inputs[i].style.display=='none' && trim(initV[j])=='' && 'true'==inputs[i].required))

            if(((inputs[i].className=="inputCalendar"&&inputs[i].required=="true"))&&(isinsert==true))

            {}

            else{

            inputs[i].value=trim(initV[j])}

          j++

        }

      }

    }}

  }



  function init() {

    vBtnHTML = detail.innerHTML

    reset();

    //window.attachEvent("onresize", adjPos);

    detail.attachEvent("onclick", showInput);

    detail.attachEvent("onfocus", showInput);

  }

  

  function showInput(){

    if(showInputMoveElement==false)

      return ;

    if(showInputSrcElement==window.event.srcElement)

      return ;

    if(window.event.srcElement.tagName!="TD")

      return ;

    if(detail.getInputDefine==null)

      return ;

    var inp=detail.getInputDefine(window.event.srcElement);

    if(typeof(inp)=="undefined"||inp==null)

      return ;

    var rang=element.editRang.split(":");

    var rowIndex=window.event.srcElement.parentNode.rowIndex;

    var rowCount=window.event.srcElement.parentNode.parentNode.childNodes.length;

    var headCount=window.event.srcElement.parentNode.parentNode.parentNode.tHead.childNodes.length;

    rowIndex=rowIndex-headCount;

    if(rowIndex<parseInt(rang[0]))

    	return ;

    if(rowIndex>=rowCount-parseInt(rang[1]))

    	return ;

    showInputSrcElement=window.event.srcElement;

    showInputCompoent(window.event.srcElement,inp);

  }

  var showInputConst_Type_Text="inputTextA,inputInteger,inputDecimal";

  var showInputConst_Type_Select="inputSelectZ";

  var showInputSrcElement=null;

  var showInputMoveElement=true;

  //只用于select 的F2 事件

  var showInputOpenFindDlg=false;

  function getShowInputStyle(cell){

    var c=cell.parentNode.parentNode.rows[0].cells[cell.cellIndex];

    if(c.style.width=="")

      c.style.width=c.clientWidth;

    if(c.style.height=="")

      c.style.height=c.clientHeight;

    if(cell.style.width=="")

      cell.style.width=c.style.width;

    if(cell.style.height=="")

      cell.style.height=c.style.height;

    return "width:100%;height:18";

  }

  function setShowInputShow(cell,inputobj,value){

  		cell.childNodes(0).attachEvent("onkeyup",function(){moveCursor(cell.childNodes(0),event.keyCode)});

      var type=inputobj["class"];

      ///不同的输入类型可能有不同的设置方法

      if(type==showInputConst_Type_Select){

        //定义检测函数

        cell.childNodes(0).check=function(){

          if(typeof(this.required)!="undefined"&&this.value==""&&this.required=="true")

            return false;

          return true;

        }

        //装入数据

        loadInputSelectData(cell.childNodes(0));

      }

      cell.childNodes(0)._oldValue=value;

      if(value!=""){

        cell.childNodes(0).value=value;

    }

    cell.childNodes(0).onblur=function(){

      if(this.check()&&showInputOpenFindDlg==false){

        showInputMoveElement=true;

        showInputSrcElement=null;

        var value="";

        var code="";

        if(showInputConst_Type_Text.indexOf(this["inClass"])>=0){

          value=this.value;

          code=this.value;

        }

        if(this["inClass"]==showInputConst_Type_Select){

          if(this.selectedIndex!=-1){

            var v=this.options[this.selectedIndex].value;

            var t=this.options[this.selectedIndex].text;

            //cell.parentNode.cells[this["codeindex"]].innerText=v;

            value=t;

            code=v;

          }

        }

        cell.innerText=value;

        var tbody = element.vDom.getElementsByTagName('tbody').item(0)

        var thead_rs=(cell.parentNode.parentNode.parentNode.getElementsByTagName('thead').item(0).childNodes.length)

        if(typeof(this["codeindex"])=="undefined")

         this["codeindex"]=cell.cellIndex-1;

        tbody.childNodes(cell.parentNode.rowIndex-thead_rs).childNodes(this["codeindex"]).text=code;

    		if(typeof(this["textindex"])!="undefined"&&isNaN(parseInt(this["textindex"]))==false){

    			tbody.childNodes(cell.parentNode.rowIndex-thead_rs).childNodes(this["textindex"]).text=value;

    		}

      }else{

            showInputMoveElement=false;

      }

      if(this._oldValue!=this.value&&typeof(this.onChangeValue)!="undefined"){

      	var f=eval(this.onChangeValue);

      	f(this,this._oldValue,this.value);

      }

      this._oldValue=this.value;

    }

    cell.childNodes(0).focus();

  }

  function loadInputSelectData(select){

    var dom=getDict(select.load,select.para)

    var data=dom.documentElement;

    var nodes=data.selectNodes("/*/para");

    var code,value;

    if(typeof(select.required)=="undefined"||select.required!="true"){

      o=new Option(value,code);

      select.options[0]=o;

    }

    if(nodes.length>0){

      for(var i=0;i<nodes.length;i++){

        code=nodes[i].getAttribute("code");

        value=nodes[i].getAttribute("value");

        o=new Option(value,code);

        o=new Option(value,code);

        select.options[select.options.length]=o;

      }

    }

    //F2 begin

    select.onkeydown=function(){

      if(event.keyCode==113){

          showInputOpenFindDlg=true;

          findXml= dom.xml.replace("<root>","<root code='false' qtype='0' key=''>");

          selectObj.setXML(findXml)

          selectObj.select()

          if (trim(selectObj.value)!='')

            this.value=selectObj.value;

          }

          showInputOpenFindDlg=false;  

     }

    //F2 end

    if(select.options.length>0)

      select.options[0].selected=true;

  }

  function showInputCompoent(cell,inputobj){

    var v=cell.innerText;

    var tbody = element.vDom.getElementsByTagName('tbody').item(0)

    var thead_rs=(cell.parentNode.parentNode.parentNode.getElementsByTagName('thead').item(0).childNodes.length)

    if(typeof(inputobj["codeindex"])=="undefined")

     inputobj["codeindex"]=cell.cellIndex;

    v=tbody.childNodes(cell.parentNode.rowIndex-thead_rs).childNodes(inputobj["codeindex"]).text;

    var input;

    var type=inputobj["class"];

    

    if(showInputConst_Type_Text.indexOf(type)>=0||type==showInputConst_Type_Select){

      input="<input type='text' inClass='"+inputobj["class"]+"'  extent='100%' ";

      var a;

      for(var o in inputobj){

          a=""+inputobj[o];

          if(a.indexOf("'")>=0)

            a=a.replace(/'/g,"\"")

          input+=o+"='"+a+"' ";

      }

      input+=" style='"+getShowInputStyle(cell)+"'>";

    }

    

    if(type==showInputConst_Type_Select){

      input="<select type='text'  style='"+getShowInputStyle(cell)+"' inClass='"+inputobj["class"]+"' ";

      var sel="load,para,required,codeindex";

      for(var o in inputobj){

        input+=o+"='"+inputobj[o]+"' ";

      }

      input+="></select>";

    }

    jhtcSetHtml(cell,input);

    setShowInputShow(cell,inputobj,v);

  }

  function moveCursor(inp,key){

  	if(key==39||key==37||key==40||key==38){

  		var nexttd=null;

  		if(key==39){

	  		if(inp["inClass"]!=showInputConst_Type_Select){

	  			if(getCursorPosition(inp)<inp.value.length)

	  				return ;

	  		}

	  		var nnn=inp.parentNode.nextSibling;

	  		while(nnn!=null

	  				&&(

	  					typeof(detail.getInputDefine(nnn))=="undefined"||detail.getInputDefine(nnn)==null

	  				)

	  			){

	      		nnn=nnn.nextSibling;

	      	}

  			nexttd=nnn;

  		}

  		if(key==37){

	  		if(inp["inClass"]!=showInputConst_Type_Select){

	  			if(getCursorPosition(inp)<0)

	  				return ;

	  		}

	  		var nnn=inp.parentNode.previousSibling;

	  		while(nnn!=null

	  				&&(

	  					typeof(detail.getInputDefine(nnn))=="undefined"||detail.getInputDefine(nnn)==null

	  				)

	  			){

	      		nnn=nnn.previousSibling;

	      	}

  			nexttd=nnn;

  		}

  		if(key==38){

  			var preTr=inp.parentNode.parentNode.previousSibling;

  			var curtd=inp.parentNode.parentNode.firstChild;

  			if(preTr!=null){

  				var pretd=preTr.firstChild;

	  			while(curtd!=null&&curtd!=inp.parentNode){

	  				pretd=pretd.nextSibling;

	  				curtd=curtd.nextSibling;

	  			}

	  			nexttd=pretd;

  			}

  		}

  		if(key==40){

  			var preTr=inp.parentNode.parentNode.nextSibling;

  			var curtd=inp.parentNode.parentNode.firstChild;

  			if(preTr!=null){

	  			var pretd=preTr.firstChild;

	  			if(pretd.tagName.toLowerCase()=="td"){

		  			while(curtd!=null&&curtd!=inp.parentNode){

		  				pretd=pretd.nextSibling;

		  				curtd=curtd.nextSibling;

		  			}

		  			nexttd=pretd;

	  			}

  			}

  		}

  		if(nexttd==null)

  			return ;

  		nexttd.click();

  		var nextinp=detail.getInputDefine(nexttd);

	    if(typeof(nextinp)=="undefined"||nextinp==null)

	      return ;

  		if(nextinp["class"]!=showInputConst_Type_Select)

  			return ;

  		window.setTimeout(function(){nexttd.click();},10);

  	}

  }

  //得到光标的位置

  function getCursorPosition(obj){

    var qswh="^#@%166!^";

    if (obj.maxLength!=null && obj.maxLength<2000) {

      obj.maxLength = obj.maxLength+qswh.length

    }

    obj.focus();

    var rng=element.document.selection.createRange();

    rng.text=qswh;

    var nPosition=obj.value.indexOf(qswh)

    rng.moveStart("character", -qswh.length)

    rng.text="";

    if (obj.maxLength!=null && obj.maxLength<2000+qswh.length) {

      obj.maxLength = obj.maxLength-qswh.length

    }

    return nPosition;

  }

  function dRefresh() {

    // 判断是否有记录, 没有记录则不加入此btn

    var btnStr = trim(vBtnHTML)

    if (trim(detail.rFlag)=='true' && (element.vDom.getElementsByTagName('tr').length>0)) {

      btnStr += "<button class='tableBtn' accessKey='D' style='display:"+element.remove_display+"' onclick='multi.dRemove()'>删除</button>"

    }



    var recordMsg = ''

    if (element.vDom.getElementsByTagName('tr').length>0)

      recordMsg+="<td align='right' noWrap='true' style='font-family:\"宋体\";font-size:13px;'>一共"+element.vDom.getElementsByTagName('tr').length+"条</td>"

    // 计算合计，修改vDom

    sumTR()

    // 由于一些页面的问题报Javascript错，所以先不做浮动表头处理

    if(element.annex!=null && element.vDom.selectNodes("/root/annex").length<1 ){

	  	var endRow="<annex>";

	  	for(var o in element.annex){

	  		endRow+="<"+o+">"+element.annex[o]+"</"+o+">"

	  	}

	  	endRow+="</annex>";

	  	var vXmlEndRow = new ActiveXObject("Microsoft.XMLDOM");

	    vXmlEndRow.async=false;

	    vXmlEndRow.loadXML(endRow);

	  	element.vDom.selectNodes("/root")[0].appendChild(vXmlEndRow.selectNodes("/annex")[0]); 

	  }

    
		 var str ="";
		 //add  by  yangqing 控件浮动表头参数
		if (element.isScroll=="false")
		{
			str = element.vDom.transformNode(genBaseXslt())//+element.vDom.transformNode(genHeadXslt())
		}else{
			str = element.vDom.transformNode(genBaseXslt())+element.vDom.transformNode(genHeadXslt())
		}
			
	

    jhtcSetHtml(detail, str.replace(/<td recordMsg="true"><\/td>/, recordMsg).replace(/<td isBtn="true"><\/td>/, "<td>"+btnStr+"</td>"),

    	function(){resetButtonStats();initInput();});
	
	adjPos();

		

	}

	function initInput(){

    // checkbox 绑定 事件

    var inputs = detail.getElementsByTagName('input')

    for (var i=0; i<inputs.length; i++) {

      if (inputs[i].type == 'checkbox') {

        if (inputs[i].parentNode==null) continue;

        inputs[i].parentNode.style.display = ''

	      if(inputs[i].parentNode.tagName=='TH') { // 全选或全取消

		      inputs[i].onclick = function() {

        	  var flag;

        	  var inputs = detail.getElementsByTagName('input');

        	  for(var i=0;i<inputs.length;i++){

        		  if (inputs[i].type=='checkbox'&&(inputs[i].parentNode.tagName=='TH')) {

        				flag=inputs[i].checked;

        		  }

        	  }



        	  for(var i=0;i<inputs.length;i++){

        	    if (inputs[i].type=='checkbox'&&(inputs[i].parentNode.tagName=='TD')) {

        			  inputs[i].checked=flag;

        	    }

        	  }

        	  if (element.vDom.getElementsByTagName('tbody').length>0) {

              var tbody = element.vDom.getElementsByTagName('tbody')[0]

              for (var i=0; i<tbody.getElementsByTagName('tr').length; i++) {

                if (trim(flag)=="true")

                  tbody.getElementsByTagName('tr')[i].setAttribute("chose", "true")

                else

                  tbody.getElementsByTagName('tr')[i].setAttribute("chose", "")

              }

            }

		      }

	      } else if(inputs[i].parentNode.tagName=='TD') { // 检测是否全部选或全部取消

		      inputs[i].onclick = function() {

            var flag;

            var inputs = detail.getElementsByTagName('input');

        	  for(var i=0;i<inputs.length;i++){

        	    if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TD')) {

        			  if(inputs[i].checked==false){

        				  flag=false;

        				  break;

        			  }

        	    }

        	  }



            for(var i=0;i<inputs.length;i++){

          	  if (inputs[i].type=='checkbox'&&(inputs[i].parentNode.tagName=='TH')) {

          			if(flag==false)

          				inputs[i].checked=false;

          			else

          			  inputs[i].checked=true;

          	  }

            }

            // 给vDom的tr加上选中标识

            var tbodyIdx = this.parentNode.parentNode.rowIndex-this.parentNode.parentNode.parentNode.parentNode.getElementsByTagName('thead')[0].rows.length

            if (element.vDom.getElementsByTagName('tbody').length>0) {

              var tbody = element.vDom.getElementsByTagName('tbody')[0]

              if (tbodyIdx<=tbody.getElementsByTagName('tr').length) {

                if (this.checked)

                  tbody.getElementsByTagName('tr')[tbodyIdx].setAttribute("chose", "true")

                else

                  tbody.getElementsByTagName('tr')[tbodyIdx].setAttribute("chose", "")

              }

            }

	        }

  		  }

      }

    }



    // 根据初始的TD checkbox 状态决定 全选按钮的状态

    var flag;

	  for(var i=0;i<inputs.length;i++){

	    if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TD')) {

			  if(inputs[i].checked==false){

				  flag=false;

				  break;

			  } else

			    flag=true

	    }

	  }



    for(var i=0;i<inputs.length;i++){

  	  if (inputs[i].type=='checkbox'&&(inputs[i].parentNode.tagName=='TH')) {

  			if(flag==false)

  				inputs[i].checked=false;

  			else if (flag==true)

  			  inputs[i].checked=true;

  	  }

    }

		if(element.autoAdj==null || element.autoAdj=="true")

    	adjPos()

  }



  function genBaseXslt() {

    // 取得自定义xsl 的head，body

    var vXsl = new ActiveXObject("Microsoft.XMLDOM");

    vXsl.async=false;

    vXsl.load(element.document.URL.replace(/.html.*$/g, ".xsl"));

    var temp = vXsl.xml

    if (temp==null || temp.length==0) {

      alert('此文件'+element.document.URL.replace(/.html.*$/g, ".xsl")+'不符合XSL格式')

      return;

    }

    var vCol = ''

    if (temp.search(/<colgroup>/i)!=-1)

      vCol = temp.substring(temp.search(/<colgroup>/i), temp.search(/<\/colgroup>/i)+"</colgroup>".length)



    var vHead = ''

    if (temp.search(/<thead>/i)>-1 && temp.search(/<\/thead>/i)>-1)

      vHead = temp.substring(temp.search(/<thead>/i), temp.search(/<\/thead>/i)+"</thead>".length)



    var vBody = ''

    if (temp.search(/<tbody>/i)>-1 && temp.search(/<\/tbody>/i)>-1)

      vBody = temp.substring(temp.search(/<tbody>/i), temp.search(/<\/tbody>/i)+"</tbody>".length)



    //生成基础xslt

    vXsl.load(window.prefix+"base/xsl/detailCtn.xsl");

    var str = vXsl.xml

    // 替换表头

    if (vHead!=null && vHead.length>="</thead>".length) {

      str = str.replace(/<thead\/>/, vCol+vHead)

    }

    // 替换体数据格式

    if (vBody!=null && vBody.length>="</tbody>".length) {

      str = str.replace(/<tbody\/>/, vBody)

    }

    vXsl.loadXML(str)

    return vXsl

  }



  function genHeadXslt() {

    var vXsl = new ActiveXObject("Microsoft.XMLDOM");

    vXsl.async=false;

    vXsl.load(element.document.URL.replace(/.html.*$/g, ".xsl"));

    var temp = vXsl.xml

    if (temp==null || temp.length==0) {

      alert('此文件'+element.document.URL.replace(/.html.*$/g, ".xsl")+'不符合XSL格式')

      return;

    }



    var vCol = ''

    if (temp.search(/<colgroup>/i)!=-1)

      vCol = temp.substring(temp.search(/<colgroup>/i), temp.search(/<\/colgroup>/i)+"</colgroup>".length)

    var vHead = ''

    if (temp.search(/<thead>/i)>-1 && temp.search(/<\/thead>/i)>-1)

      vHead = temp.substring(temp.search(/<thead>/i), temp.search(/<\/thead>/i)+"</thead>".length)



    //生成表头xslt

    vXsl.load(window.prefix+"base/xsl/mainHead.xsl");

    var str = vXsl.xml

    // 替换表头

    if (vHead!=null && vHead.length>="</thead>".length) {

      str = str.replace(/<thead\/>/, vCol+vHead)

    }

    vXsl.loadXML(str)

    return vXsl

  }

	function dSetItem(r,c,v){

		var tbody = element.vDom.getElementsByTagName('tbody').item(0);

        tbody.childNodes(r).childNodes(c).text=v;

	}

	function getRowCount(){

		var tbody = element.vDom.getElementsByTagName('tbody').item(0);

        return tbody.childNodes.length;

	}

  function adjPos() {

    //1. 调整baseDiv的位子

    parentObj = baseDiv = element.document.getElementById('_base');

    baseDivTop = baseDivLeft = 0

    while (parentObj.tagName != "BODY") {

	    baseDivTop += parentObj.offsetTop;

	    baseDivLeft += parentObj.offsetLeft;

	    parentObj = parentObj.offsetParent;

    }



    baseDiv.style.pixelWidth = parentObj.clientWidth - baseDivLeft - 20

    baseDiv.style.pixelHeight = parentObj.clientHeight - baseDivTop - 60 - parseFloat(element.bottomFix)

    

   // return false;

    

    //3. 调整输入域的位置

    var tbody = baseDiv.getElementsByTagName('tbody')[0]



    //调整headDiv的位子

    headDiv = element.document.getElementById('_head');

    if (headDiv==null) return

    baseDiv.onscroll = function() {

      headDiv.scrollLeft = baseDiv.scrollLeft

    }



    var tHead1s = baseDiv.getElementsByTagName('thead')

    var tHead2s = headDiv.getElementsByTagName('thead')



    for (var i=0; i<tHead1s[0].rows.length; i++) {

      for (var j=0; j<tHead1s[0].rows[i].cells.length; j++) {

        tHead1s[0].rows[i].cells[j].noWrap = 'true'

      }

    }



    with (headDiv.style)  {

      pixelTop = baseDivTop

      pixelLeft = baseDivLeft

      pixelWidth = baseDiv.offsetWidth-baseDiv.offsetWidth+baseDiv.clientWidth

      pixelHeight = tHead1s[0].offsetHeight + tHead1s[0].parentNode.border*2

    } // -- study baseDiv.offsetWidth-baseDiv.offsetWidth+baseDiv.clientWidth, tHead1s[0].offsetHeight + tHead1s[0].parentNode.border*2



    for (var i=0; i<tHead1s[0].rows.length; i++) {

      for (var j=0; j<tHead1s[0].rows[i].cells.length; j++) {

        tHead2s[0].rows[i].cells[j].noWrap = 'true'

        tHead2s[0].rows[i].cells[j].style.pixelWidth = tHead1s[0].rows[i].cells[j].offsetWidth

        tHead2s[0].rows[i].cells[j].style.pixelHeight = tHead1s[0].rows[i].cells[j].offsetHeight

      }

    }

    headDiv.style.display=''

  }



  function submit(btn) {

    if (window.trim(btn.name)=='') {

      alert('请设置name')

      return false;

    }



    // 加入主表信息

    if (element.vDom.getElementsByTagName('root').length<1) return false;

    var inputs= main.getElementsByTagName("input")

    for (var i=0; i<inputs.length; i++) { //检测

      if (window.trim(inputs[i].name) == ''||inputs[i].type=='checkbox' || inputs[i].type=='radio' ) continue;

      if (inputs[i].check())

        continue;

      else

        return false;

    }

    var root = element.vDom.getElementsByTagName('root')[0]

    // 加前缀到名字的前面，主要是为了使属性排序, 属性最长不能超过999

    var prefix = new String("000")

    for (var i=0; i<inputs.length; i++) {

      if (window.trim(inputs[i].name) == '' || inputs[i].type=='checkbox' || inputs[i].type=='radio' ) continue;

      root.setAttribute("_"+prefix.substring(0, prefix.length-(new String(i).length))+i+"_"+inputs[i].name, trim(inputs[i].value))

    }

    //查看提交

    //window.prompt('',vDom.xml)

    window.xmlhttp._object.open("POST", trim(btn.name)+".viewhigh", false);

    window.xmlhttp._object.send(element.vDom.xml);

    if (!window.doMsg(window.xmlhttp._object.responseText)) {

      return false;

    }

    if(element.onAfterSubmit!=""){

    	var fOnAfterSubmit=eval(element.onAfterSubmit);

    	var pk=window.xmlhttp._object.responseText;

    	pk=pk.substr(pk.indexOf("<mdctn_main_pk>")+"<mdctn_main_pk>".length);

 		pk=pk.substr(0,pk.indexOf("</mdctn_main_pk>"));

    	fOnAfterSubmit(element,pk);

    }

    if (window.dialogArguments!=null) {//刷新

      try {

        window.dialogArguments._obj1.refresh()

      } catch (exception)  {

      }

    }

    isinsert=true;

    reset()

    return true

  }



  function sumTR() {

    // 是否存在tbody

    if (element.vDom.getElementsByTagName('tbody').length<1) return false

    var tbody = element.vDom.getElementsByTagName('tbody')[0]



    // 删除

    if (element.vDom.getElementsByTagName('total').length>0) {

      var temp = element.vDom.getElementsByTagName('total')[0]

      temp.parentNode.removeChild(temp)

    }



    // 计算

    var nums = new Array()

    for (var j=0; j<tbody.getElementsByTagName('tr').length; j++) {

      var tr = tbody.getElementsByTagName('tr')[j]

      for (var k=0; k<tr.getElementsByTagName('td').length; k++) {

        if (nums[k]==null) nums[k]=0;

        if (k==nums.length) nums.length++;

        var temp = tr.getElementsByTagName('td')[k]

        if (temp.firstChild==null) continue;

        if (isNaN(parseFloat(temp.firstChild.nodeValue))) continue;

        nums[k]+=parseFloat(temp.firstChild.nodeValue)

      }

    }



    var total = element.vDom.createElement("total");

    for (var i=0; i<nums.length; i++) {

      var temp = element.vDom.createElement("td");

      if (nums[i]==null) nums[i]=0;

      temp.appendChild(element.vDom.createTextNode(nums[i]))

      total.appendChild(temp)

    }



    // 添加

    element.vDom.getElementsByTagName('root')[0].appendChild(total)

  }



  function move(num) {

    // -- TBD  检测num 是整数    



    if (num==0) return false;



    if (element.vDom.getElementsByTagName('tbody').length<1)

      element.vDom.appendChild(element.vDom.createElement('tbody'));



    var tbody = element.vDom.getElementsByTagName('tbody').item(0)

    var trs = tbody.getElementsByTagName('tr')

    

    var firstTr=0;

    for(var i=0;i<trs.length;i++){

    	if(trim(trs[i].getAttribute("chose"))=='true' && trs[i].parentNode!=null){

    		firstTr++;

    	}

    }

    if(firstTr==0||firstTr>1){

    	alert("请选择一条!");

    	return false;

    }

    

      

    if (num>0) {      

      if (tbody.getElementsByTagName('tr').length < (num+1)) {

        alert('无法移动')

        return false;

      }

      for (var i=trs.length-1-num; i>=0; i--) {

        if (trim(trs[i].getAttribute("chose"))=='true' && trs[i].parentNode!=null) {

          trs[i].parentNode.insertBefore(trs[i+num], trs[i]);

        }

      }

    }

    

    if (num<0) {

      if (tbody.getElementsByTagName('tr').length < (1-num)) {

        alert('无法移动')

        return false;

      }

      for (var i=0-num; i<trs.length; i++) {

        if (trim(trs[i].getAttribute("chose"))=='true' && trs[i].parentNode!=null) {

          trs[i].parentNode.insertBefore(trs[i], trs[i+num]);

        }

      }

    }

    

    /*

    -- 以后整合到 dRefresh 中

    // 保留chose

    var flags=new Array();

    flags.length=trs.length;

    for (var i=0; i<trs.length; i++) {

      if (trim(trs[i].getAttribute("chose"))=='true' && trs[i].parentNode!=null) {

        flags[i]=true

      }

    }

    // 打勾

    for (var i=0; i<flags.length; i++) {

      if (trim(flags[i])=='true') {

        trs[i].setAttribute("chose", "true");

      }

    }    

    */    

    dRefresh();

  }

   function inputXML(argSource){ ////objXMLDoc接收XML文件数据

    var objXMLDoc = null;

    try{

      switch(typeof(argSource)){

        case "string"://XML字节流

          if(argSource.search(/\./) != -1 && argSource.search(/\</) == -1){ //xml file's location



            objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");

            objXMLDoc.async = false;

            if(argSource.search(/\+/) != -1) argSource = eval(argSource);

              objXMLDoc.load(argSource);

              break;

            }

            if(argSource.search(/\</) != -1){ //xml string

              objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");

              objXMLDoc.loadXML(argSource);

              break;

            }

            objXMLDoc = eval(argSource);

            objXMLDoc = (objXMLDoc.XMLDocument)?(objXMLDoc.XMLDocument):null; //xml data island



            break;

        case "object"://XMLDOM对象

            if(argSource.xml) return objXMLDoc = argSource; //xml document object

            break;

            default:

            objXMLDoc = null;

      }

      if (!objXMLDoc.xml) objXMLDoc = null;

    } catch(err) { objXMLDoc = null; }



    if(objXMLDoc)

    {

  		objXMLDoc.setProperty("SelectionLanguage","XPath");

  		objXMLDoc.setProperty("SelectionNamespaces","xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");

    }

    return objXMLDoc;

   }

  

  function setDataXml(source){

    var vXml = inputXML(source);

  	var data = vXml.getElementsByTagName('tbody')[0];

    var trs = data.getElementsByTagName("tr");

    

    for (var i=0; i<trs.length; i++){

      var tds =  trs[i].getElementsByTagName("td");

      var exec = "dInsert(" 

      for (var j=0; j<tds.length; j++) {

          

          exec = exec + "'"+tds[j].text+"'";

          if(j==tds.length -1){

            exec = exec + ")";

          }else{

            exec = exec + ",";

          }

			}

		 eval(exec); 

		}

		dRefresh();

  }    

  function resetButtonStats(){

			if(element.__inInit==true)

				return;

			jQuery("#"+element.id+" button").each(function(){

				if(jQuery(this).attr("__jhtc")!=undefined&&jQuery(this).attr("_oldDisabled")!=undefined){

					jQuery(this).attr("disabled",jQuery(this).attr("_oldDisabled")=="true"?true:false);

				}

			});

		}



  	jhtc_attr_init(element,"load",null);

  	jhtc_attr_init(element,"vDom",null);

  	jhtc_attr_init(element,"bottomFix","0");

  	jhtc_attr_init(element,"editRang","0:0");

  	jhtc_attr_init(element,"remove_display",null);

  	jhtc_attr_init(element,"onAfterSubmit","");

  	jhtc_attr_init(element,"autoAdj","true");

  	jhtc_attr_init(element,"annex",null);
	jhtc_attr_init(element,"isScroll","false");//是否滚动条
			 

  	

	element.dInsert=dInsert;

	element.dRemove=dRemove;

	element.dRemoveAll=dRemoveAll;

	element.dLoad=dLoad;

	element.dUpdate=dUpdate;

	element.dRefresh=dRefresh;

	element.dSetItem=dSetItem;

	element.getRowCount=getRowCount;

	element.submit=submit;

	element.reset=reset;

	element.adjPos=adjPos;

	element.sumTR=sumTR;

	element.move=move;

	element.setDataXml=setDataXml;

	element.init=init;

	element.jhtcInit=function(){

		element.init();

	};

	return null;

};

jhtc_class_map["MDCtn"]=jhtc_MDCtn;
