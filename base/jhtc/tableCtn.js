var tdIndex;
var orderBy;
function jhtc_tableCtn(win,jhtc_obj){
	return jhtc_tableCtn_c(__jhtcElementFun(jhtc_obj));
}
function jhtc_tableCtn_c(_e){
		/***************
		onInitInput=function(){
			var temp_xml = '<root length="'+(5+initV.length)+'">';
			temp_xml = temp_xml + '<input type="text" index="1" name="daf" class="inputTextTableMoney" maxInput="14"/>';
			temp_xml = temp_xml + '<select name="deom" codeindex="2" index="5" load="acct_month"/>';
			temp_xml = temp_xml + '</root>';
			window.event.xml = temp_xml;
		}
		**********/
		var baseDivTop=0, baseDivLeft=0, baseDiv, headDiv, parentObj; // 调整位置
		var addr, submitXml,argumentXml=null,argumentAction=null,argumentFunc=null;// 后台交互信息
		var begin=-1, page=18, total=-1, isTurn; // 翻页信息
		var vBtnHTML; // 左上的按钮
		var noUpdate=false //默认数据修改提交，noUpdate='true' 数据没有修改提交
		var vAvtiveHeads = 0;
		var _affectCount=0;
		var is_page=false;

		var _keepedHead = '';//如果keepHead属性为true,把处理后的表头文件保存在这里
		var _keepedDanHead="";
		var _lastHideMsg="";
		var _headRows=0;
		var _divHeadId="_head",_innerHeadId="_mainTableHead_",_innerTableId="_mainDataTable",_innerDivId="_base";
		var _ayTd11Obj=null,_ayTd12Obj=null,_ayTd21Obj=null,_ayTd21DivObj=null;
		var _ayLoadImg=null;
		var _hasJhtc=false;

		//xml Dom Element 保存请求返回的数据
		var response = null;

		var vexpression = null;
		var mymainTab = null;

		var selectInputOpenFindDlg=false;
		var selXsl='';
		function init() {
			var element=_e();
			selXsl=element.xslFile;
			element.cellInputs = new Array();
			_ayLoadImg="<img src=\""+window.prefix+"/images2/sys/Loading.gif\" width=\"16\" height=\"16\"/>"
			init__tc_f();
			if(element.linkId=="true"){
				_innerHeadId=element.id+_innerHeadId;
				_innerTableId=element.id+_innerTableId;
				_innerDivId=element.id+_innerDivId;
				_divHeadId=element.id+_divHeadId;
			}
			//1.取得Btn
			page=parseInt(element.count_page);
			if(isNaN(page))
				page=18;
			vBtnHTML = element.innerHTML;
			if (window.trim(element.remove) != '')
				vBtnHTML += "<button id='"+element.remove+"' class='tableBtn' style='display:"+element.remove_display+"' accessKey='D'>删除</button></td>";

			//3.生成innerHTML
			var vXml = new ActiveXObject("Microsoft.XMLDOM");
			vXml.async = false;
			vXml.loadXML("<root/>");
			findRightXslFile();
			if(element.initLoad=="true"){
				if(element.async=="true")
					postRefreshReq("empty","","");
				else
					jhtcSetHtml(element, vXml.transformNode(genBaseXslt()).replace(/<td isBtn="true"><\/td>/, "<td id='"+_innerDivId+"_btn'>"+vBtnHTML+"</td>"),resetButtonStats);
			}
			if(element.async=="true")
				prepareAsyncTable();

			//4.调整表格不折行
			var theads = element.getElementsByTagName('THEAD')
			for (var k=0; k<theads.length; k++) {
				for (var i=0; i<theads[k].rows.length; i++) {
					for (var j=0; j<theads[k].rows[i].cells.length; j++) {
						theads[k].rows[i].cells[j].noWrap = 'true'
					}
				}
			}
			//5.调整位子参数
			adjPosition()
			element.parentNode.attachEvent("onresize", adjPosition);
			window.attachEvent("onresize", adjPosition);
			window.attachEvent("onresize", delayAdjPosition);
			element.attachEvent("onclick", sortTableTr);
			// -- TBD
			// 加行以适应大小
			sortTableTrSetCursor(-1);
			// -- TBD
			// 加行以适应大小
			if(element.includeJhtc=="true")
				_hasJhtc=true;
		}
		function delayAdjPosition(){
			window.setTimeout(adjPosition,100);
		}
		function init__tc_f(){
			if(_e().async=="true"){
				_e().attachEvent("onclick",function(){
						var elem=event.srcElement;
						if(elem.tagName.toLowerCase()=="input"&&"checkbox"==elem.type){
							window.__tc_f.oCC(elem);
						}
					});
			}
			window.__tc_f={};
			window.__tc_f.oRO=function(tr){// mouse over
					if(tr.parentNode.tagName.toLowerCase()=="thead")
						return;
					if(tr.__oldRowBgColor){

					}else{
						tr.__oldRowBgColor=tr.rowIndex%2==1?window.oddColor:window.evenColor;
					}
					tr.style["background"]=window.overColor;
				}
			window.__tc_f.oRT=function(tr){ // mouse out
					if(tr.parentNode.tagName.toLowerCase()=="thead")
						return;
					if(tr.__oldRowBgColor){
						tr.style["background"]=tr.__oldRowBgColor;
					}
				}
			window.__tc_f.oRC=function(tr){ // onclick
					if(tr.parentNode.tagName.toLowerCase()=="thead")
						return;
					if(tr.parentNode.__lastChooseTr_){
						tr.parentNode.__lastChooseTr_.style["background"]=tr.parentNode.__lastChooseTr_.__oldRowBgColor;
					}
					tr.parentNode.__lastChooseTr_=tr;
					tr.style["background"]=window.choseColor;
				}
			window.__tc_f.oCC=function(cb){// checkbox click
					var c=cb.checked;
					var ips=cb.parentNode.parentNode.parentNode.parentNode.getElementsByTagName("input");
					var firstip=ips[0];
					if(cb.parentNode.tagName.toLowerCase()=="th"){
						for(var i=0;i<ips.length;i++){
							if(ips[i].type=="checkbox")
								ips[i].checked=c;
						}
					}else{
						c=true;
						for(var i=1;i<ips.length;i++){
							if(ips[i].type=="checkbox"&&ips[i].checked==false){
								c=false;
							}
						}
						firstip.checked=c;
					}
				}

		}
		function getArgumentXml(){
			return argumentXml;
		};
		function getArgumentAction(){
			return argumentAction;
		};
		/* name,提交地址， vXmlStr,提交XML信息， flag,是否翻页标识 */
		function refresh(name, vXmlStr, flag, subFunc, hideMsg) {
			var element=_e();
			argumentXml=vXmlStr;
			argumentAction=name;
			if(subFunc==null||subFunc==''){
				subFunc=argumentFunc;
			}else{
				argumentFunc=subFunc;
			}
			page=parseInt(element.count_page);
			if(isNaN(page))
				page=18;
			_lastHideMsg=hideMsg
			// 1.与后台进行数据交换
			if (name != null && vXmlStr!=null) {
				addr = name
				submitXml = vXmlStr
				submitXml = submitXml + "<_ActiveHeads>"+getActiveHeads()+"</_ActiveHeads>";
				begin = 1
				if (flag) {
					isTurn = true
				}else{
					isTurn = false;//pj-update-20121227--自定义报表需要判断是否分页
				}
			}
			if (addr==null)
			return
			if (subFunc == null) {
				subFunc = '';
			}
			__jhtcDispatchEvent(element,"onpagechange_start");
			//change_start.fire();
			if (isTurn) {
				if (begin<1) begin = 1;
				var rel_page=page;
				if(element.xslFileNonFirestPage!="" && begin==1){
					rel_page=(parseInt(page,10)+parseInt(element.headRows,10));
				}
				if (subFunc != '') {
					postRefreshReq(addr, "<_headRows>"+element.dynaHead+"</_headRows><_begin>"+begin+"</_begin><_end>"+(begin+rel_page-1)+"</_end>"+submitXml, subFunc+"&isTurn=ture&fromTable=true&offset="+begin+"&limit="+rel_page)
				} else {
					postRefreshReq(addr, "<_headRows>"+element.dynaHead+"</_headRows><_begin>"+begin+"</_begin><_end>"+(begin+rel_page-1)+"</_end>"+submitXml, "?isTurn=ture&fromTable=true&offset="+begin+"&limit="+rel_page)
				}
			} else {
				postRefreshReq(addr, submitXml, subFunc)
			}
			if(element.async=="true"||element.async=="data")
				return true;
			var source = window.xmlhttp._object.responseText;

			return refreshSource(source);  //多调用了一次parseResultError
			//return parseResultError(source)==""?true:false;
		}
		function refreshSource(source){
			// 2.修正全局变量：总条数
			// 得到总条数
			parseTotalNum(source);

			// 3. 加入补充信息，生成innerHTML
			var vXml = inputXML(source);
			response = vXml.selectSingleNode("//root").cloneNode(true);

			refreshTab(source);
			sortTableTrSetCursor(-1);
			window.setTimeout(adjPosition,1);
			window.setTimeout(adjPosition,100);
			doDataLoaded();
			jhtcEndQuery();
			return parseResultError(source)==""?true:false;
			//prompt("element.outerHTML",element.outerHTML);
		}
		function resetButtonStats(){
			if(_e().__inInit==true)
				return;
			jQuery("#"+_e().id+" button").each(function(){
				if(jQuery(this).attr("__jhtc")!=undefined&&jQuery(this).attr("_oldDisabled")!=undefined){
					jQuery(this).attr("disabled",jQuery(this).attr("_oldDisabled")=="true"?true:false);
				}
			});
		}
		function parseTotalNum(text){

			if (text.search(/<total>/)!=-1) {
				total = eval(text.substring(text.search(/<total>/)+"<total>".length, text.search(/<\/total>/)))
				if(total<=0)
					total=0

				if (total<1)
					begin = 0
			}
		}
		function parseResultError(text){
			var element=_e();
			var erro_res="";
			if (text.search(/<error>/)!=-1) {
				var error = text.substring(text.search(/<error>/)+"<error>".length, text.search(/<\/error>/))
				if (trim(_lastHideMsg)!='hide'){
					alert(decodeXmlChar(error))
				}
				erro_res=decodeXmlChar(error);
			} else
				erro_res="";
			__jhtcDispatchEvent(element,"onpagechange_end");
			//change_end.fire();
			return erro_res;
		}
		function postRefreshReq(ad, sx, sf){
			var element=_e();
			var rad=ad,oldAd=ad;
			if(ad=="empty"){
				ad="__tableCtn_S_";
				rad="empty";
			}

			if(element.async=="true"){
				prepareAsyncTable();
				if(findRightXslFile()==false)
					return;
				if(sf=="")
					sf="?";
				fc=jQuery(element).attr("fixCol");
				/**
				if(_headRows>0){
					var fcc=fc.split(",");
					if(fcc[0].length>0){
						fcc[0]=(parseInt(fcc[0],10)-_headRows);
						fc=fcc.join();
					}
				}
				**/
				sf+="&__tcid_="+element.id+"&__tcip_="+element.linkId+"&__tccf_="+fc+"&__tcan_="+rad+"&__tcxf_="+element.xslFile+"&__tcnd_=0";
				if(element.annex!=null){
					ann="";
					for(var o in element.annex){
						ann+=encodeHtttGetStr(o).replace(/\\/,"\\\\").replace(/,/g,"\\,").replace(/#/g,"\\35")+",,"+encodeHtttGetStr(element.annex[o]).replace(/\\/,"\\\\").replace(/,/g,"\\,").replace(/#/g,"\\35")+",,"
					}
					if(ann!="")
						ann=ann.substr(0,ann.length-2);
					sf+="&__tcann_="+ann;
				}
				sf+="&__tccc_="+((window.trim(element.choose)!=""||window.trim(element.remove)!="")?"1":"0");
				if(oldAd!="empty")
					jhtcBeginQuery();
				xmlhttp.send(ad,sx,sf,onAsyncRequest);//"__tableCtn_S_"
			}else if(element.async=="data"){
				jhtcBeginQuery();
				xmlhttp.send(ad,sx,sf,refreshSourceCb);
			}else{
				jhtcBeginQuery();
				window.xmlhttp.post(ad,sx,sf);
				jhtcEndQuery();
			}
		}
		function refreshSourceCb(text,xml){
			refreshSource(text);
		}
		function prepareAsyncTable(){
			var element=_e();
			//判断变量_ayTd11Obj不等于“null”时返回，如果变量_ayTd11Obj的属性innerHTML为空值时，变量_ayTd11Obj也没有实际意义
			if(_ayTd11Obj!=null && _ayTd11Obj.innerHTML != '')
			{return;}
			var htmlStr="<table width='100%' style='border: solid 1px #eeeeee;background-color: #F6F6F6;'  cellpadding='0' cellspacing='0' ><tr><td  id='"+element.id+"_td11'isBtn=\"true\"><div id='"+element.id+"_td11load' style='position: absolute;' ></div><span id='"+_innerDivId+"_btn'>"+vBtnHTML+"</span></td><td id='"+element.id+"_td12' isTurn=\"true\"  align='right' noWrap='true' style='font-family:\"宋体\";font-size:13px;'></td></tr><tr><td id='"+element.id+"_td21' colspan='100' style=''><div id='"+_innerDivId+"' style='overflow:auto;padding:0;SCROLLBAR-FACE-COLOR: #F2F2F2; SCROLLBAR-HIGHLIGHT-COLOR: #999999;SCROLLBAR-SHADOW-COLOR:#999999; SCROLLBAR-3DLIGHT-COLOR:#ffffff; SCROLLBAR-ARROW-COLOR:#999999; SCROLLBAR-TRACK-COLOR:#ffffff;SCROLLBAR-DARKSHADOW-COLOR: #ffffff;'></div></td></tr></table>";
			element.innerHTML=htmlStr;
			resetButtonStats();
			var ed=element.document;
			_ayTd11Obj=ed.getElementById(element.id+"_td11");
			_ayTd12Obj=ed.getElementById(element.id+"_td12");
			//_ayTd21Obj=ed.getElementById(element.id+"_td21");
			_ayTd21DivObj=ed.getElementById(_innerDivId);
			adjPosition();
		}
		var STR_SEPRATOR="__=__3ff51ca139496acb__=_SEP_",STR_BTNHTML="__=__3ff51ca139496acb__=_BTN_";
		function onAsyncRequest(text,xml){
			var element=_e();
			jhtcEndQuery();
			parseTotalNum(text);
			element.asyncError=parseResultError(text);
			if(element.asyncError==""){
				element.serverXml=text.substr(0,text.indexOf(STR_SEPRATOR));
				var htmlStr=text.substr(text.indexOf(STR_SEPRATOR)+STR_SEPRATOR.length).replace("__=__3ff51ca139496acb__=_btn_",vBtnHTML);

				if(isTurn==true && element.keepHead=='true' && begin>1){
					repHead=true;
					htmlStr = htmlStr.replace(new RegExp("<thead id=\""+element.id+"_head\">(.|\n)*<\/thead>","i"),_keepedHead);
				}else if(element.keepHead=='true'){
					var hb=htmlStr.search(new RegExp("<thead id=\""+element.id+"_head\">(.|\n)*<\/thead>","i"));
					_keepedHead = htmlStr.substring(hb, htmlStr.search(/<\/thead>/i,hb)+"</thead>".length);
				}
				_ayTd21DivObj.innerHTML=htmlStr;
				resetButtonStats();
				htmlStr=null;
				if(_ayTd12Obj!=null&&isTurn==true){
					_ayTd12Obj.innerHTML=getTurnPageHtmlStr();
				}else{
					_ayTd12Obj.innerHTML="";//pj-update-20121227--自定义报表需要判断是否分页
				}
				setTurnBtnFun();
				var headObj=element.document.getElementById(element.id+"_head");
				if(headObj!=null&&begin==1&&element.keepHead=='true'){
					_headRows=headObj.rows.length;
				}
				adjPosition();
			}
			doDataLoaded();
		}
		function doDataLoaded(){
			var element=_e();
			if(element.onDataLoaded!=""){
				eval(element.onDataLoaded);
			}
			if(element.onfinishload!=''){
				eval(element.onfinishload);
			}
		}
		// 生成Base Xslt
		function genBaseXslt(serverStr) {
			var element=_e();
			// 取得自定义xsl 的head，body
			var vXml = new ActiveXObject("Microsoft.XMLDOM");
			vXml.async=false;
			vXml.load(element.xslFile);
			var temp = vXml.xml
			try{
				if (temp==null || temp.length==0) {
					alert('此文件'+element.document.URL.replace(/.html.*$/g, ".xsl")+'不符合XSL格式')
					return;
				}
			}catch(e){}
			var vCol = ''
			if (temp.search(/<colgroup>/i)!=-1)
				vCol = temp.substring(temp.search(/<colgroup>/i), temp.search(/<\/colgroup>/i)+"</colgroup>".length)

			var vHead = temp.substring(temp.search(/<thead>/i), temp.search(/<\/thead>/i)+"</thead>".length)
			var vBody = temp.substring(temp.search(/<tbody>/i), temp.search(/<\/tbody>/i)+"</tbody>".length)

			// 取得后台的变化表头，并替换
			if (serverStr != null && serverStr.search(/<thead>/i) != -1 && trim(element.headXsl)=='false' ) {
				vHead = serverStr.substring(serverStr.search(/<thead>/i), serverStr.search(/<\/thead>/i)+"</thead>".length)
			}

			//生成基础xslt
			vXml.load(window.prefix+"base/xsl/mainTable.xsl");
			var str = vXml.xml

			// 替换表头
			if (vHead!=null && vHead.length>="</thead>".length) {
				str = str.replace(/<thead\/>/, vCol+vHead)
			}
			str=str.replace("\"_mainDataTable\"","\""+_innerTableId+"\"").replace("\"_mainTableHead_\"","\""+_innerHeadId+"\"").replace("\"_base\"","\""+_innerDivId+"\"").replace("\"_head\"","\""+_divHeadId+"\"");
			// 替换体数据格式
			if (vBody!=null && vBody.length>="</tbody>".length) {
				str = str.replace(/<tbody>(.|\n)*<\/tbody>/, vBody)
			}

			//////////////////////////////////////////////////////////
			str = replaceActiveHeads(str);


			//////////////////////////////////////////////////////////
			vXml.loadXML(str)

			return vXml
		}
		function refreshTabByXML(xml){
			response=xml;
			refreshTab(xml.xml);
		}
		function refreshTab(source){
			var element=_e();
			element.serverXml = source
			refreshTabData(source);
			refreshTabStyle(source);
		}
		function findRightXslFile(){
			var element=_e();
			/********这里处理保存的表头 added by zrc********/
			//alert(begin);
			if(isTurn==true && element.keepHead=='true' && begin>1 ){
				if(element.xslFileNonFirestPage==''){
					alert('请为xslFileNonFirestPage属性指定xsl文件路径');
					return false;
				}
				element.xslFile = element.xslFileNonFirestPage ;
			}
			else if(element.keepHead == 'true' ){
				if (element.xslFileFirst != '') {
					element.xslFile = element.xslFileFirst;
				} else {
					element.xslFile = selXsl;
				}
			}

			if(element.xslFile==""){
				element.xslFile=element.document.URL.replace(/.html.*$/g, ".xsl");
				element.xslFile=element.xslFile.substr(element.xslFile.lastIndexOf("/")+1);
			}
			return true;
			/********结束处理保存的表头********/
		}
		function refreshTabData(source){
			var element=_e();
			if(findRightXslFile()==false)
				return;

			var xslt = genBaseXslt(response.xml);
			var vXml = inputXML(response.xml);
			if(element.annex!=null){
				var endRow="<annex>";
				for(var o in element.annex){
					endRow+="<"+o+">"+element.annex[o]+"</"+o+">"
				}
				endRow+="</annex>";
				var vXmlEndRow = new ActiveXObject("Microsoft.XMLDOM");
				vXmlEndRow.async=false;
				vXmlEndRow.loadXML(endRow);
				vXml.selectNodes("/root")[0].appendChild(vXmlEndRow.selectNodes("/annex")[0]);
			}
			var str = vXml.transformNode(xslt);
			if (isTurn && str.indexOf("<td isTurn=\"true\"></td>")!=-1) { // 翻页
				str=str.replace(/<td isTurn="true"><\/td>/,"<td align='right' noWrap='true' style='font-family:\"宋体\";font-size:13px;'>"+getTurnPageHtmlStr()+"</td>");
			}
			str=addBtnHtmlStr(str)
			// 动态表头
			var danHeader = ""
			if (trim(element.fixCol) != "") {
				var temp = str.search(/<colgroup>/i)!=-1?str.substring(str.search(/<colgroup>/i), str.search(/<\/colgroup>/i)+"</colgroup>".length):""
				temp = "<root>" + temp + str.substring(str.search(/<thead>/i), str.search(/<\/thead>/i)+"</thead>".length)+"</root>"
				vXml.loadXML(temp)
				danHeader = vXml.transformNode(genHeadXslt(temp))
			}

			/********这里处理保存的表头 added by zrc********/
			if(isTurn==true && element.keepHead=='true' && begin>1){
				str = str.replace(/<thead>(.|\n)*<\/thead>/,_keepedHead);
				danHeader=_keepedDanHead;
			}
			else{
				_keepedHead = str.substring(str.search(/<thead>/i), str.search(/<\/thead>/i)+"</thead>".length);
				_keepedDanHead=danHeader;
				//element.innerHTML  = str + danHeader;
			}
			str += danHeader;
			if(_hasJhtc==true){
				jhtcSetHtml(element, str ,resetButtonStats);
			}else{
				//element.innerHTML=str;
				jhtcSetHtml(element, str);//处理完str后再innerHTML
				jhtcSetHtml("#"+_innerDivId+"_btn", vBtnHTML ,resetButtonStats);
			}
			/********结束处理保存的表头********/

			if (element.dynaHead == "0" && trim(element.fixCol) != ""){
				element.document.getElementById(_innerHeadId).attachEvent("onclick", onsortTableTr);
			}

			//2.初始化cellInputs
			//var oEvent = createEventObject();
			//evtInitInput.fire(oEvent);

			var xml=__jhtcDispatchEvent(element,"onInitInput");
			if (xml!=null && xml!='') {
				var vXml = new ActiveXObject("Microsoft.XMLDOM");
				vXml.async = false;//?
				vXml.loadXML(xml)

				var root = vXml.getElementsByTagName("root").item(0);

				for (var i=0; i < root.attributes.length; i++) {
					if (root.attributes.item(i).name=='length') {
						element.cellInputs.length = root.attributes.item(i).value
						break;
					} else {
						alert('root的属性length未赋值');
						return false;
					}
				}

				var tempDiv = document.createElement("<div/>");
				element.appendChild(tempDiv);
				xml=xml.substring(xml.indexOf(">")+1);
				jhtcSetHtml(tempDiv,xml.replace(/^<root>|<\/root>$/gi, ""));
				for (var i=0; i<tempDiv.childNodes.length; i++) {
					if (tempDiv.childNodes[i].index >= element.cellInputs.length) {
						alert('此name：'+tempDiv.childNodes[i]+'index值非法');
						return;
					}
					if (tempDiv.childNodes[i].name == null) {
						alert('存在name未赋值');
						return false;
					}
					element.cellInputs[tempDiv.childNodes[i].index] = tempDiv.childNodes[i]
					tempDiv.childNodes[i].kind = tempDiv.childNodes[i].type
					if (tempDiv.childNodes[i].tagName=="SELECT") {
						element.cellInputs[tempDiv.childNodes[i].index].kind = 'select';
					}
				}
			}

			// 初始化cellInputs
			for (var i=0; i<element.cellInputs.length; i++) {
				if (element.cellInputs[i]!=null) {
					if (element.cellInputs[i].kind == 'text') {
						/*element.cellInputs[i].onkeyup = element.cellInputs[i].ondragend = function() {
							if (this.parentNode.innerText != this.value) {
								if (this.parentNode.childNodes.length>1)
									this.parentNode.childNodes[0].data = this.value
								else
									this.parentNode.insertAdjacentHTML("afterBegin", this.value)
								this.parentNode.parentNode.rowSubmit = true
								if (trim(this.name)!='') {
									this.parentNode.value = "<" + this.name + ">" + this.value+"</" + this.name +">";
								} else {
									this.parentNode.value = "<td>" + this.value+"</td>";
								}
								adjPosition()
							}
						}*/
						element.cellInputs[i].onchange=_textonchange;
					} else if (element.cellInputs[i].kind == 'checkbox') {
						element.cellInputs[i].onclick = _checkboxonclick;
					}
					////处理Select BEGIN
					else if (element.cellInputs[i].kind == 'select') {
						element.cellInputs[i].onblur=_selectonblur;
					}
					////处理Select END
				}
			}
			//取得table对象
			mymainTab = element.getElementsByTagName("table").item(1);
		}
		function _textonchange(){
			if(this.check(true)==false){
				return;
			};
			if (this.parentNode.childNodes.length>1)
				this.parentNode.childNodes[0].data = this.value
			else
				this.parentNode.insertAdjacentHTML("afterBegin", this.value)
			this.parentNode.parentNode.rowSubmit = true
			if (trim(this.name)!='') {
				this.parentNode.value = "<" + this.name + ">" + this.value+"</" + this.name +">";
			} else {
				this.parentNode.value = "<td>" + this.value+"</td>";
			}
			adjPosition()
		}
		function _checkboxonclick() {
			var temp=0;
			if (this.checked) {
				this.parentNode.insertAdjacentHTML("afterBegin", '√')
				temp = 1;
			} else {
				this.parentNode.childNodes[0].data = ''
			}
			this.parentNode.parentNode.rowSubmit = true
			if (trim(this.name)!='') {
				this.parentNode.value = "<" + this.name + ">" + temp+"</" + this.name +">";
			} else {
				this.parentNode.value = "<td>" + temp+"</td>";
			}
		}
		function _selectonblur(){
			if(typeof(this.required)!="undefined"&&this.value==""&&this.required=="true")
				return ;
			if(selectInputOpenFindDlg==true)
				return ;
			var v=this.options[this.selectedIndex].value;
			var t=this.options[this.selectedIndex].text;
			var p=this.parentNode;
			p.removeChild(this);
			p.parentNode.cells[this["codeindex"]].innerText=v;
			p.innerText=t;
		}
		function onsortTableTr(){
			sortTableTr(1)
		}
		function getTurnPageHtmlStr(){
			var element=_e();
			var end = begin+page-1
			if (total<end)
				end = total
			var pageStr = "&nbsp;<span style='cursor:hand;color:#E4571F' id='"+element.id+"_home'>首页</span>&nbsp;<span style='cursor:hand;color:#E4571F' id='"+element.id+"_up'>上页</span>"
			if (begin <= 1)
				pageStr = "&nbsp;首页&nbsp;上页"

			if (end != total)
				pageStr = pageStr + "&nbsp;<span style='cursor:hand;color:#E4571F' id='"+element.id+"_down'>下页</span>&nbsp;跳至<input type='text' size='2' id='"+element.id+"_turn_to'>&nbsp;<span style='cursor:hand;color:#E4571F' id='"+element.id+"_end'>尾页</span>"
			else
				pageStr = pageStr + "&nbsp;下页&nbsp;跳至<input type='text' size='2' id='"+element.id+"_turn_to'>&nbsp;尾页"
			var pc=parseInt(begin/page+0.9999,10)+"/"+parseInt(total/page+0.9999,10)
			return "总共"+(total-element.headRows)+"条 每页<input type='text' size='2' id='"+element.id+"_turn_size' value='"+page+"'>条 第"+pc+"页" + pageStr;
		}
		function addBtnHtmlStr(str){
			var element=_e();
			// 按钮
			str = str.replace(/<td isBtn="true"><\/td>/, "<td id='"+_innerDivId+"_btn'>"+vBtnHTML+"</td>")
			return str;
		}
		function refreshTabStyle(source) {
			var element=_e();
			// 4. 默认处理 aa)选择提交 a)删除，b)修改，c)上翻页，d)下翻页

			setCheckBoxFun(source);
			// b)加入修改的处理
			if (window.trim(element.update) != '' && /<pk>/.test(response.xml)) {
				// -- TBD 检查权限

				// 加下划线和相应方法
				var vHref = element.getElementsByTagName('a');
				for (var i=0; i<vHref.length; i++) {
					if (vHref[i].href == '') {
						vHref[i].style.textDecoration = 'underline'
						vHref[i].style.cursor = 'hand'
						vHref[i].style.color  = 'blue'

						vHref[i].onclick = modify
					}
				}
			}

			setTurnBtnFun()

			// 5. 调整nowrap
			var tbody = element.getElementsByTagName('TBODY')[1]
			for (var i=0; i<tbody.rows.length; i++) {
				tbody.rows[i].style.height='22'
				if(element.rowColor=="true"){
					if (i%2 == 0) { // 两色相间
						tbody.rows[i].style.backgroundColor = window.oddColor
					} else {
						tbody.rows[i].style.backgroundColor = window.evenColor
					}
				}
				tbody.rows[i].onmouseout = _rowonmouseover;
				for (var j=0; j<tbody.rows[i].cells.length; j++) {
					tbody.rows[i].cells[j].noWrap = 'true'
					tbody.rows[i].cells[j].onfocus = tdActive
				}
			}

			for (var i=0; i<tbody.rows.length; i++) {
				var hideCount=0;
				for (var k=0; k<tbody.rows[i].cells.length; k++) {
					if (tbody.rows[i].cells[k].style.display=='none') {
						hideCount++
					}
				}

				for (var j=0; j<element.cellInputs.length; j++) {
					if (element.cellInputs[j]!=null) {
						if(element.is_autofocus=="true"){
							tbody.rows[i].cells[j+hideCount].setActive()
						}
						break;
					}
				}
				break;
			}

			// 6. 调整位置
			adjPosition()

			// 7. 加入键盘响应
			tbody.onkeydown = navigateKeys
            document.onkeydown = function(e) {
                e = e || event;
                if (e.keyCode == 38) {
                    return false;
                }
                if (e.keyCode == 40) {
                    return false;
                }
            }
			if(element.onrefresh!=''){
				eval(element.onrefresh)
			}
		}
		function _rowonmouseover() {
			if (this.rowIndex==_e().rowIndex)
				return
			this.runtimeStyle.backgroundColor = ''
		}
		function setCheckBoxFun(source){
			var element=_e();
			if ((window.trim(element.choose) != ''&& /<pk>/.test(source))||(window.trim(element.remove) != '' && /<pk>/.test(source))){
				if(element.remove!=null&&element.remove!=""){
					var rev= element.document.getElementById(element.remove);
					if(rev!=null)
						rev.onclick = del;
				}
				// 显示删除
				var inputs = element.getElementsByTagName('input')
				for (var i=0; i<inputs.length; i++) {
					if (inputs[i].type == 'checkbox') {
						inputs[i].parentNode.style.display = ''
						if(inputs[i].parentNode.tagName=='TH')
							inputs[i].onclick = setAll; // 全选或全取消
						else {
							if(inputs[i].parentNode.tagName=='TD'){
								inputs[i].onclick = checkAll; // 检测是否全选或全取消
								inputs[i].checked=getCheckboxOldChecked(inputs[i]);
							}
						}
					}
				}
			}
		}
		function setTurnBtnFun(){
			var element=_e();
			var objUp = element.document.getElementById(element.id+'_up')
			if (objUp != null) {
				objUp.onclick = pageUp
			}
			var objDown = element.document.getElementById(element.id+'_down')
			if (objDown != null) {
				objDown.onclick = pageDown
			}
			var objDown = element.document.getElementById(element.id+'_turn_to')
			if (objDown != null) {
				objDown.onkeydown = turnTo
			}
			var objDown = element.document.getElementById(element.id+'_turn_size')
			if (objDown != null) {
				objDown.onkeydown = turnSize
				objDown.onblur	=turnSizeBlur
			}

			var ojbHome = element.document.getElementById(element.id+'_home')
			if (ojbHome != null) {
				ojbHome.onclick = pageHome
			}

			var objEnd = element.document.getElementById(element.id+'_end')
			if (objEnd != null) {
				objEnd.onclick = pageEnd
			}
		}
		// 生成Head Xslt
		function genHeadXslt(serverStr) {
			var element=_e();
			// 取得自定义xsl 的head，body
			var vXml = new ActiveXObject("Microsoft.XMLDOM");
			vXml.async=false;

			if(element.xslFile=="")
				vXml.load(element.document.URL.replace(/.html.*$/g, ".xsl"));
			else
				vXml.load(element.xslFile);

			var temp = vXml.xml
			if (temp==null || temp.length==0) {
				alert('此文件'+element.document.URL.replace(/.html.*$/g, ".xsl")+'不符合XSL格式')
				return;
			}

			var vCol = ''
			if (temp.search(/<colgroup>/i)!=-1)
				vCol = temp.substring(temp.search(/<colgroup>/i), temp.search(/<\/colgroup>/i)+"</colgroup>".length)
			var vHead = temp.substring(temp.search(/<thead>/i), temp.search(/<\/thead>/i)+"</thead>".length)

			// 取得后台的变化表头，并替换
			if (serverStr != null && serverStr.search(/<thead>/i) != -1) {
				vHead = serverStr.substring(serverStr.search(/<thead>/i), serverStr.search(/<\/thead>/i)+"</thead>".length)
			}

			//生成表头xslt
			vXml.load(window.prefix+"base/xsl/mainHead.xsl");
			var str = vXml.xml
			// 替换表头
			if (vHead!=null && vHead.length>="</thead>".length) {
				str = str.replace(/<thead\/>/, vCol+vHead)
				str = replaceActiveHeads(str);
			}
			str=str.replace("_mainTableHead_",_innerHeadId).replace("\"_head\"","\""+_divHeadId+"\"");
			vXml.loadXML(str)

			return vXml
		}

		function  replaceActiveHeads(str){
			var element=_e();
			var actives = 0;
			actives = parseInt(vAvtiveHeads);
			if(actives != 0){
				if(response == null){
					for( i =1; i<= actives;i++){
						str = str.replace("@active_"+i,"");
					}
				}else{
					for(i= 1 ; i<= actives;i++){
						var tempStr = addr+"_head_"+i;
						var activeNode = response.selectSingleNode("//"+tempStr);
						if(activeNode == null){
							//alert("返回结果中不存在"+tempStr+"节点");
							return;
						}
						var cols = activeNode.childNodes.length;
						var replaceStr = activeNode.xml;
						replaceStr = replaceStr.replace("<"+tempStr+">","");
						replaceStr = replaceStr.replace("</"+tempStr+">","");
						if (str.indexOf("<th>@active_"+i+"</th>") != -1)
							str = str.replace("<th>@active_"+i+"</th>",replaceStr);
						else
							str = str.replace('<th noWrap="true">@active_'+i+'</th>',replaceStr);
						str = str.replace("active_"+i,cols);
					}
				}
			}
			return str;

		}

		// 生成Col  Xslt
		// -- TBD
		// 复制Head Xslt为Common Xslt
		// -- TBD

		function adjPosition() {
			var element=_e();
			//1. 调整baseDiv的位子
			var parentObj = baseDiv = element.document.getElementById(_innerDivId);
			baseDivTop = baseDivLeft = 0
			try{
				while(parentObj.tagName != "BODY") {
				baseDivTop += parentObj.offsetTop;
				baseDivLeft += parentObj.offsetLeft;
				parentObj = parentObj.offsetParent;
				}
			}catch(e){
				return ;
			}
			element.rightFix=""+element.rightFix;
			element.bottomFix=""+element.bottomFix;
			if(element.rightFix.substr(element.rightFix.length-1)=="%")
				baseDiv.style.pixelWidth = parentObj.clientWidth - baseDivLeft -  parentObj.clientWidth*parseFloat(element.rightFix.replace(/%/g,""))/100
			else
				baseDiv.style.pixelWidth = parentObj.clientWidth - baseDivLeft -  parseFloat(element.rightFix)
			element.style.pixelWidth=baseDiv.style.pixelWidth+2;
			if(element.bottomFix.substr(element.bottomFix.length-1)=="%")
				baseDiv.style.pixelHeight = parentObj.clientHeight - baseDivTop - 2 - parentObj.clientHeight*parseFloat(element.bottomFix.replace(/%/g,""))/100
			else
				baseDiv.style.pixelHeight = parentObj.clientHeight - baseDivTop - 2 - parseFloat(element.bottomFix)
			element.style.pixelHeight=baseDiv.style.pixelHeight

			//2. ......

			//3. 调整输入域的位置
			var tbody = baseDiv.getElementsByTagName('tbody')[0]

			if (this.rowIndex!=null && this.rowIndex>-1) {
				var hideCount=0;
				if (tbody.parentNode.rows.length>this.rowIndex) {
					for (i=0; i<tbody.parentNode.rows[this.rowIndex].cells.length; i++) {
						if (tbody.parentNode.rows[this.rowIndex].cells[i].style.display=='none') {
							hideCount++
						}
					}
				}

				for (var i=0; i<this.cellInputs.length; i++) {
					if (this.cellInputs[i]!=null && this.cellInputs[i].style.display=='' && tbody.parentNode.rows.length>this.rowIndex) {
						var temp = tbody.parentNode.rows[this.rowIndex].cells[i+hideCount]
						var comp = tbody.parentNode.rows[this.rowIndex].cells[i+hideCount]
						var vLeft=0, vTop=0;
						while (temp.tagName != "DIV") {
							vTop += temp.offsetTop;
							vLeft += temp.offsetLeft;
							temp = temp.offsetParent;
						}

						with (this.cellInputs[i].style) {
							pixelHeight = comp.clientHeight
							pixelWidth = comp.clientWidth
							left = vLeft + eval(comp.parentNode.parentNode.parentNode.border)
							top = vTop + eval(comp.parentNode.parentNode.parentNode.border)
						}
						break;
					}
				}
			}

			//调整headDiv的位子
			headDiv = element.document.getElementById(_divHeadId);
			if (headDiv==null) return
			baseDiv.onscroll = _baseDivOnScroll

			var tHead1s = baseDiv.getElementsByTagName('thead')
			var tHead2s = headDiv.getElementsByTagName('thead')

			for (var i=0; i<tHead1s[0].rows.length; i++) {
				for (var j=0; j<tHead1s[0].rows[i].cells.length; j++) {
					tHead1s[0].rows[i].cells[j].noWrap = 'true'
				}
			}

			with (headDiv.style)  {
				pixelTop = baseDivTop;
				pixelLeft = baseDivLeft
				pixelWidth = baseDiv.clientWidth
				pixelHeight = tHead1s[0].offsetHeight + tHead1s[0].parentNode.border*2
			} // -- study baseDiv.offsetWidth-baseDiv.offsetWidth+baseDiv.clientWidth, tHead1s[0].offsetHeight + tHead1s[0].parentNode.border*2

			var wid = [];
			var hei = [];
			for (var i=0; i<tHead1s[0].rows.length; i++) {
				for (var j=0; j<tHead1s[0].rows[i].cells.length; j++) {
					try {
						if(!wid[i])wid[i]=[];
						if(!hei[i])hei[i]=[];
						wid[i][j]=tHead1s[0].rows[i].cells[j].offsetWidth;
						hei[i][j]=tHead1s[0].rows[i].cells[j].offsetHeight;
						//tHead2s[0].rows[i].cells[j].noWrap = 'true'
						//tHead2s[0].rows[i].cells[j].style.pixelWidth = tHead1s[0].rows[i].cells[j].offsetWidth
						//tHead2s[0].rows[i].cells[j].style.pixelHeight = tHead1s[0].rows[i].cells[j].offsetHeight
					}catch(e){}
				}
			}
			for (var i=0; i<tHead1s[0].rows.length; i++) {
				for (var j=0; j<tHead1s[0].rows[i].cells.length; j++) {
					try {
						if(wid[i][j]!=0){
							tHead1s[0].rows[i].cells[j].noWrap = 'true'
							tHead1s[0].rows[i].cells[j].style.pixelWidth = wid[i][j];
							tHead1s[0].rows[i].cells[j].style.pixelHeight = hei[i][j];
						}
						tHead2s[0].rows[i].cells[j].noWrap = 'true'
						tHead2s[0].rows[i].cells[j].style.pixelWidth = wid[i][j];
						tHead2s[0].rows[i].cells[j].style.pixelHeight = hei[i][j];
					}catch(e){}
				}
			}
			
			headDiv.style.display=''
		}
		function _baseDivOnScroll(){
			var element=_e();
			var baseDiv = element.document.getElementById(_innerDivId);
			var headDiv = element.document.getElementById(_divHeadId);
			if (headDiv==null) return
			headDiv.scrollLeft = baseDiv.scrollLeft
		}
		// 激活td
		function tdActive() {
			var element=_e();
			for (var i=0; i<element.cellInputs.length; i++) {
				if (element.cellInputs[i]!=null)
					element.cellInputs[i].style.display = 'none'
			}

			var objTD = event.srcElement
			var parentObj = objTD
			var tdTop = tdLeft = 0
			while (parentObj && parentObj.tagName != "DIV") {
				tdTop += parentObj.offsetTop;
				tdLeft += parentObj.offsetLeft;
				parentObj = parentObj.offsetParent;
			}

			if(element.rowColor=="true")
				objTD.parentNode.runtimeStyle.backgroundColor = window.choseColor
			if (element.rowIndex!=null && objTD.parentNode.parentNode.parentNode.rows.length>element.rowIndex && element.rowIndex!=objTD.parentNode.rowIndex) {
				objTD.parentNode.parentNode.parentNode.rows[element.rowIndex].runtimeStyle.backgroundColor = ''
			}
			element.rowIndex = objTD.parentNode.rowIndex

			var cellInput = element.cellInputs[objTD.cellIndex]
			if (cellInput!=null) {
				cellInput.style.display = ''
				cellInput.focus()


				if (objTD.childNodes.length>0 && objTD.childNodes[0].data!=null)
					cellInput.value = objTD.childNodes[0].data.replace(/,/g,"");
				else
					cellInput.value = ''

				if (objTD.contains(cellInput)) {
					return;
				}

				if (cellInput.value=='X') {
					cellInput.style.display='none'
					cellInput.value=''
					return;
				}

				if(cellInput.kind == 'select')
					objTD.innerText="";
				objTD.appendChild(cellInput)
				cellInput.style.top = tdTop + 1
				cellInput.style.left = tdLeft + (objTD.cellIndex>0?1:0) // 第一个cell不需要偏移1px
				if(cellInput.kind == 'select'){
					cellInput.style.width ="98%"
					cellInput.style.height ="90%"

				}else{
					cellInput.style.width = objTD.clientWidth
					cellInput.style.height = objTD.clientHeight
				}
				cellInput.focus()

				if (cellInput.kind == 'text') {
					var range = cellInput.createTextRange()
					range.collapse(true);
					range.moveStart('character',-1);
					range.select();
				} else if (cellInput.kind == 'checkbox') {
					if (objTD.childNodes[0].data == '√') {
						cellInput.checked = true
					} else {
						cellInput.checked = false
					}
				} else if (cellInput.kind == 'select') {
					var select=cellInput;
					if(typeof(select.load)=="undefined"||select.load==null)
						return ;
					if(typeof(select.hasLoad)!="undefined")
						return ;
					select.hasLoad=true;
					var dom=getDict(select.load,select.para)
					var data=dom.documentElement;
					var nodes=data.selectNodes("/*/para");
					var code,value;
					if(typeof(select.required)=="undefined"||select.required!="true"){
						o=new Option(value,code);
						select.options[0]=o;
					}
					if(nodes.length>0){
						for(var i=0;i<nodes.length;i++){
							code=nodes[i].getAttribute("code");
							value=nodes[i].getAttribute("value");
							o=new Option(value,code);
							select.options[select.options.length]=o;
						}
					}
					//F2 begin
					select.onkeydown=function(){
						if(event.keyCode==113){
							selectInputOpenFindDlg=true;
							findXml= dom.xml.replace("<root>","<root code='false' qtype='0' key=''>");
							selectObj.setXML(findXml)
							selectObj.select()
							if (trim(selectObj.value)!='')
								this.value=selectObj.value;
						}
						selectInputOpenFindDlg=false;
					}
					//F2 end
					if(select.options.length>0)
						select.options[0].selected=true;
				}
			}
		}
		//页面跳至 add by twl
		function turnTo(){
			var element=_e();
			var mo=element.document.getElementById(element.id+"_turn_to");
			if(event.keyCode==13){
				if(mo.value==""){

				}else if(isNaN(mo.value)){
					mo.value="";
				}else if(mo.value>parseInt(total/page+0.9999,10)){
						//alert('超出总页数!');
						//mo.value="";
						mo.value=parseInt(total/page+0.9999,10);
					       	__jhtcDispatchEvent(element,"onpagechange_start");
					        if(element.is_alterpage=="false") return ;

					        page_num=parseInt(mo.value,10);
					        mo.value="";
					        if(isNaN(page_num)||page_num<1||page_num>total/page)
					        	return;
					        begin=page*(page_num-1)+1;
					        refresh();
					        //change_end.fire();
					        __jhtcDispatchEvent(element,"onpagechange_end");
					}else{
						if(element.is_alterpage=="false")
							return ;
						page_num=parseInt(mo.value,10);
					        if(page_num<1)
					        	return;
						page_num=mo.value;
						begin=page*(page_num-1)+1;
						mo.value="";
						refresh();
					}
			}
		}
		var _min_page_size=5;
		var _max_page_size=1000;
		function turnSize(){
			var element=_e();
			var mo=element.document.getElementById(element.id+"_turn_size");
			if(event.keyCode==13){
				if(mo.value==""){

				}else if(isNaN(mo.value)){
					mo.value=""
				}else{
						var v=parseInt(mo.value);
						if(v<_min_page_size){
							mo.value=_min_page_size;
							alert("每页最少显示"+_min_page_size+"条.");
							v=_min_page_size;
						}
						if(v>_max_page_size){
							mo.value=_max_page_size;
							alert("每页最多显示"+_max_page_size+"条.");
							v=_max_page_size;
						}
						element.count_page=v;
						refresh();
					}
			}
		}
		function turnSizeBlur(){
			var element=_e();
			var mo=element.document.getElementById(element.id+"_turn_size");
			if(isNaN(mo.value)){
				mo.value=""
			}else{
				var v=parseInt(mo.value);
				if(v<_min_page_size){
					mo.value=_min_page_size;
					alert("每页最少显示"+_min_page_size+"条.");
					v=_min_page_size;
				}
				if(v>_max_page_size){
					mo.value=_max_page_size;
					alert("每页最多显示"+_max_page_size+"条.");
					v=_max_page_size;
				}
				element.count_page=v;
			}
		}
		//得到光标的位置
		function getCursorPosition(obj){
			var element=_e();
			var qswh="^#@%166!^"
			if (obj.maxLength!=null && obj.maxLength<2000) {
				obj.maxLength = obj.maxLength+qswh.length
			}
			obj.focus();
			var rng=document.selection.createRange();
			
			//// add by yangqing 这里判断光标位置的算法有一个小bug，当全部选中的文本值的时候，
			////左右键移动会有一个问题，就是按照这个算法，实际效果是清除了文本域里面的内容
			////当焦点集中到文本域的时候，默认会全部选中text，
			////	这时如果右键，或者左键 的时候，是移动单元格还是移动文本域里光标位置，
			////添加下面判断，默认移动单元格，其他情况就是光标在文本值中移动
			if(obj.value.length==rng.text.length)
			{
					return 0;
			}
			
			
			rng.text=qswh;
			var nPosition=obj.value.indexOf(qswh)
			rng.moveStart("character", -qswh.length)
			rng.text="";
			if (obj.maxLength!=null && obj.maxLength<2000+qswh.length) {
				obj.maxLength = obj.maxLength-qswh.length
			}
			return nPosition;
		}

		//上下左右键移动单元格函数
		function arrowdown(obj) {
			var element=_e();
			var row=null;
			if(window.event.keyCode==38)//上
			{
				cell=obj.parentElement;
				while(cell.tagName!="TD")
			{
				cell=cell.parentElement;
			}
			row=cell.parentElement;
			tab=row.parentElement.parentElement;
			if(row.rowIndex>0)
			{
				cell=tab.rows[row.rowIndex-1].cells[cell.cellIndex];
				ins=cell.getElementsByTagName("INPUT");
				if(ins.length>0)
				{
					ins[0].focus();
				}
			}

			}
			if(window.event.keyCode==40)//下
			{
				cell=obj.parentElement;
				while(cell.tagName!="TD")
				{
					cell=cell.parentElement;
				}
				row=cell.parentElement;
				tab=row.parentElement.parentElement;
				if(tab.rows.length>row.rowIndex+1)
				{
					cell=tab.rows[row.rowIndex+1].cells[cell.cellIndex];
					ins=cell.getElementsByTagName("INPUT");

					if(ins.length>0)
					{
						ins[0].focus();
					}
				}
			}
			if(window.event.keyCode==37)//左
			{
				cell=obj.parentElement;
				while(cell.tagName!="TD")
				{
					cell=cell.parentElement;
				}
				row=cell.parentElement;
				tab=row.parentElement.parentElement;
				if(cell.cellIndex>0)
				{
					cell=row.cells[cell.cellIndex-1];
					ins=cell.getElementsByTagName("INPUT");

					if(ins.length>0)
					{
						ins[0].focus();
					}
				}
			}
			if(window.event.keyCode==39)//右
			{
				cell=obj.parentElement;
				while(cell.tagName!="TD")
				{
					cell=cell.parentElement;
				}
				row=cell.parentElement;
				tab=row.parentElement.parentElement;
				if(cell.cellIndex<row.cells.length-1)
				{
					cell=row.cells[cell.cellIndex+1];
					ins=cell.getElementsByTagName("INPUT");
					if(ins.length>0)
					{
					ins[0].focus();
					}
				}
			}
		}
		// 键盘响应
		function navigateKeys() {
			//alert();
			var element=_e();
			var parentObj = event.srcElement
			while (parentObj.tagName!='BODY') {
				if (parentObj.tagName=='TD')
					break;
				parentObj = parentObj.parentNode
			}
			var objTD = parentObj;
			var nCell=parentObj.cellIndex;

			while (parentObj.tagName!='BODY') {
				if (parentObj.tagName=='TR')
					break;
				parentObj = parentObj.parentNode
			}
			var objTR = parentObj;
			var nRow=parentObj.rowIndex;

			var objTBODY=objTR.parentNode;
			var objTHEAD=objTBODY.parentNode.getElementsByTagName('thead')[0]

			var hideCount=0;
			for (i=0; i<objTR.cells.length; i++) {
				if (objTR.cells[i].style.display=='none') {
					hideCount++
				}
			}

			var flagX = false;
			var nKeyCode=event.keyCode;
			do {
				var isIn=(event.srcElement.tagName=='INPUT' && event.srcElement.type=='text')||(event.srcElement.tagName=='SELECT')
				switch(nKeyCode){
					case 37://<-
					if((","+element.ignoreKeyCodes+",").indexOf(",37,")>=0){
							arrowdown(event.srcElement);
							return;
						} else {
							if (isIn==true && getCursorPosition(event.srcElement)> 0)
							return true;
						nCell--;
						break;
						}
					case 38://^
					if((","+element.ignoreKeyCodes+",").indexOf(",38,")>=0){
							arrowdown(event.srcElement);
							return;
						} else {
						nRow--;
						break;
					}
					case 39://->
					if((","+element.ignoreKeyCodes+",").indexOf(",39,")>=0){
							arrowdown(event.srcElement);
							return;
						} else {
						if (isIn==true && getCursorPosition(event.srcElement)< event.srcElement.value.length )
							return true;
						nCell++;
						break;
					}
					case 40://\|/
					if((","+element.ignoreKeyCodes+",").indexOf(",40,")>=0){
							arrowdown(event.srcElement);
							return;
						} else {
						if (event.altKey) {
							objList.style.display=''
							return true;
						}
						nRow++;
						break;
					}
					case 9:// tab
						if (event.ctrlKey) {
							return true;
						}
						if (event.shiftKey) {
							nCell--;
							break;
						}
					case 13://Enter
						if((","+element.ignoreKeyCodes+",").indexOf(",13,")>=0||(","+element.ignoreKeyCodes+",").indexOf(",9,")>=0){
							event.keyCode=9;return;
						}else{
							nCell++;
							break;
						}
					case 33: // pageUp
						if (event.ctrlKey) {
							pageUp()
							return false;
						}
					case 34: // pageDown
						if (event.ctrlKey) {
							pageDown()
							return false;
						}
					case 35: // end
					if (event.ctrlKey) {
						pageEnd()
						return false;
					}
					case 36: // home
						if (event.ctrlKey) {
							pageHome()
							return false;
						}
					default:
					return true;
				}

				if (nCell==-1){
					nRow--;//跳转到上一行
					nCell=objTR.cells.length-hideCount-1;//最后一列
				}
				if (nCell==objTR.cells.length-hideCount) {
					nRow++;//跳转到下一行首位置
					nCell=0;//第一列
				}
				if (nRow == objTHEAD.rows.length - 1) {
					nRow = objTHEAD.rows.length
					return false
				}
				if (nRow == objTHEAD.rows.length + objTBODY.rows.length) {
					nRow = objTHEAD.rows.length + objTBODY.rows.length - 1
					return false;
				}
				flagX = false;
				if (element.cellInputs[nCell]!=null) {
					if (objTBODY.parentNode.rows.length>nRow && objTBODY.parentNode.rows[nRow].cells.length>nCell &&
					objTBODY.parentNode.rows[nRow].cells[nCell+hideCount].childNodes!=null &&
					objTBODY.parentNode.rows[nRow].cells[nCell+hideCount].childNodes.length>0) {
						if (objTBODY.parentNode.rows[nRow].cells[nCell+hideCount].childNodes[0].data == 'X') {
							flagX = true
						}
					}
				}
			} while (element.cellInputs.length>0 && (element.cellInputs[nCell]==null || flagX));
			
			if (nRow < objTR.rowIndex) {
				var offLine = objTR.rowIndex-objTHEAD.rows.length -1;
				if (offLine>=0 &&  objTBODY.childNodes(offLine).offsetTop <= baseDiv.scrollTop+objTBODY.childNodes(offLine).clientHeight) {
					baseDiv.scrollTop = baseDiv.scrollTop - objTBODY.childNodes(offLine).clientHeight
				} // 调整滚动条
			} else if (nRow > objTR.rowIndex) {
				if (objTBODY.rows(objTR.rowIndex  ).offsetTop+objTBODY.rows(objTR.rowIndex ).offsetHeight >=
				(baseDiv.scrollTop+baseDiv.offsetHeight)) {
					baseDiv.scrollTop = baseDiv.scrollTop + objTR.clientHeight
				} // 调整滚动条
			}
			
			if (nRow == objTHEAD.rows.length) {
				baseDiv.scrollTop = 0
			}
			if (nRow == objTBODY.rows.length) {
				baseDiv.scrollTop = baseDiv.scrollHeight
			}

			objTBODY.rows[nRow-objTHEAD.rows.length].cells[nCell+hideCount].setActive()
			return false;
		}

		function checkChange() {   //检测数据有没有被修改
			var submitStr = assemble();

			if (trim(submitStr)=='') {
				return false;
			} else
				return true;
		}

		/*
		* isClear --  情况原有标记
		*/
		function submit(btn,addXML,hideMsg,subFunc,isClear,anoUpdate) {
			var element=_e();
			if (window.trim(btn.name)=='') {
				alert('请设置name')
				return false;
			}

			if (subFunc==null) {
				subFunc = '';
			}
			if (anoUpdate!=null) {
				noUpdate = anoUpdate;

			}

			var submitStr = assemble();
			if(submitStr.toLowerCase() == 'false')
				return
			if (trim(submitStr)=='') {
				alert('数据没有发生修改')
				return false;
			}
			window.xmlhttp.post(btn.name, submitStr, subFunc)
			var str = window.xmlhttp._object.responseText
			if (!window.doMsg(str,hideMsg)) {
				return false;
			}
			// 清除rowSubmit
			var tbody = baseDiv.getElementsByTagName('tbody')[0]
			for (var i=0; i<tbody.rows.length; i++) {
				if (tbody.rows[i].rowSubmit != null && tbody.rows[i].rowSubmit && trim(isClear)!='false') {
					tbody.rows[i].rowSubmit = null
				}
			}

			if (window.top.window.document.body.getAttribute(window.location)!=null) {
				window.top.window.document.body.getAttribute(window.location).refresh()
			}
			return true;
		}

		/*
		* 简单表格的XML组装
		*/
		function assemble2() { // -- TBD待处理，给此行加个标记，并检查此行
			var element=_e();
			var result = "";
			var tbody = baseDiv.getElementsByTagName('tbody')[0]
			if(tbody.rows[0].storeFlag != null && tbody.rows[0].storeFlag.toLowerCase() == 'false')
				if(!confirm('该月份数据已保存，确定要覆盖？'))
					return 'false'
			for (var i=0; i<tbody.rows.length; i++) {

				for (var j=0; j<tbody.rows[i].cells.length; j++) {
					if (tbody.rows[i].cells[j].value!=null && trim(tbody.rows[i].cells[j].value) != ''){
						result = result + "<"+element.trTag+">"
						result = result + tbody.rows[i].cells[j].value
						result = result + "</"+element.trTag+">"
					}
				}
			}
			return result;
		}
		function checksubmit(btn) {
			var element=_e();
			if (window.trim(btn.name)=='') {
				alert('请设置name')
				return false;
			}

			var submitStr = assemble2();
			if(submitStr.toLowerCase() == 'false')
				return

			if (trim(submitStr)=='') {
				alert('数据没有发生修改')
				return true;
			}
			window.xmlhttp.post(btn.name, submitStr)
			var str = window.xmlhttp._object.responseText
			if (!window.doMsg(str)) {
				return true;
			}
			// 清除rowSubmit
			var tbody = baseDiv.getElementsByTagName('tbody')[0]
			for (var i=0; i<tbody.rows.length; i++) {
				if (tbody.rows[i].rowSubmit != null && tbody.rows[i].rowSubmit) {
					tbody.rows[i].rowSubmit = null;
				}
			}

			if (window.top.window.document.body.getAttribute(window.location)!=null) {
				window.top.window.document.body.getAttribute(window.location).refresh()
			}
		}

		function assemble() { // -- TBD待处理，给此行加个标记，并检查此行
			var element=_e();
			_affectCount=0;
			var result = "";
			var tbody = baseDiv.getElementsByTagName('tbody')[0]
			if(tbody.rows.length<1)
				return result;
			if(tbody.rows[0].storeFlag != null && tbody.rows[0].storeFlag.toLowerCase() == 'false')
				if(!confirm('该月份数据已保存，确定要覆盖？'))
					return 'false'
			for (var i=0; i<tbody.rows.length; i++) {
				if (!noUpdate){

					if (tbody.rows[i].rowSubmit != null && tbody.rows[i].rowSubmit) {
						result = result + "<"+element.trTag+">"
						_affectCount++;
						for (var j=0; j<tbody.rows[i].cells.length; j++) {
							if (trim(tbody.rows[i].cells[j].value) != '')
								result = result + tbody.rows[i].cells[j].value
						}
						result = result + "</"+element.trTag+">"
					}
				}else{
					result = result + "<"+element.trTag+">"
					_affectCount++;
					for (var j=0; j<tbody.rows[i].cells.length; j++) {
						if (trim(tbody.rows[i].cells[j].value) != '')
							result = result + tbody.rows[i].cells[j].value
					}
					result = result + "</"+element.trTag+">"
				}
			}
			return result;
		}

		function setAll(){
			var element=_e();
			var flag;
			var inputs = element.getElementsByTagName('input');
			for(var i=0;i<inputs.length;i++){
				if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TH')) {
					flag=inputs[i].checked;
				}
			}

			for(var i=0;i<inputs.length;i++){
				if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TD'&&inputs[i].disabled!=true)) {
					inputs[i].checked=flag;
				}
			}
			if(typeof(this.onclick2)!="undefined")
				eval(this.onclick2);
		}



		function checkAll(){
			var element=_e();
			var flag;
			var inputs = element.getElementsByTagName('input');
			for(var i=0;i<inputs.length;i++){
				if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TD')) {
					if(inputs[i].checked==false){
						flag=false;
						break;
					}
				}
			}

			for(var i=0;i<inputs.length;i++){
				if (inputs[i].type == 'checkbox'&&(inputs[i].parentNode.tagName=='TH')) {
					if(flag==false)
						inputs[i].checked=false;
					else
						inputs[i].checked=true;
				}
			}
			if(typeof(this.onclick2)!="undefined")
				eval(this.onclick2);
		}

		// -- TBD del 和 modify值得改进，对于值的存储的定位不妥
		function del(sqlName) {
			var element=_e();
			var data = element.getElementsByTagName('tbody')[1];
			var trs = data.getElementsByTagName("tr")
			var flag = false;

			for (var i=0; i<trs.length; i++){
				var inputs =  trs[i].getElementsByTagName("input");
				var errorFlag=false;
				for (var j=0; j<inputs.length; j++) {
					if (inputs[j].type == 'checkbox' && inputs[j].checked) {
						if (!flag){
							if(confirm('确定要删除吗?')==false)
								return;
							flag = true;
						}
						if(typeof(sqlName)!='undefined'){
							window.xmlhttp.post(sqlName, inputs[j].value)
						}else{
							window.xmlhttp.post(element.remove, inputs[j].value)
						}
						var str = window.xmlhttp._object.responseText;
						if (str.indexOf("<error/>")==-1)
							errorFlag = true;
						if (errorFlag) {
							if (!window.doMsg(str))
								return true;
						}
						trs[i].parentNode.removeChild(trs[i]);
						i--;
						// -- TBD 检测是否成功
						// -- TBD 报错
						// -- TBD 返回
					}
				}
			}
			if (!flag) {
				alert('请先选择')
				return;
			}
			refresh()
			if(element.onafterdel!=""){
				var f=eval(element.onafterdel);
				f();
			}
			//alert('删除成功')
		}

		// choose
		// 追加新参数 delConfirm 判断是否 提示 '确定要删除吗?' (该提示信息在 检查完是否选择之后,提交删除之前)
		function submit_choose(btn,addXML,isAll,isOnly,hideMsg,subFunc,delConfirm) {
			var element=_e();
			_affectCount=0;
			if (window.trim(element.choose) == '') {
				alert('choose属性设置错误！');
			}

			if (subFunc==null) {
				subFunc = '';
			}

			var data = element.getElementsByTagName('tbody')[1];
			var trs = data.getElementsByTagName("tr")
			var temp_xml = '';
			var flag = false;
			var onlyFlag = false;

			for (var i=0; i<trs.length; i++){
				var inputs =  trs[i].getElementsByTagName("input")
				for (var j=0; j<inputs.length; j++) {
					if (isAll != null && isAll != "") {
						if (inputs[j].type == 'checkbox' && inputs[j].checked) {
							temp_xml = temp_xml + '<'+element.trTag+'>' + inputs[j].value + '<checked>1</checked></'+element.trTag+'>';
						} else {
							temp_xml = temp_xml + '<'+element.trTag+'>' + inputs[j].value + '<checked>0</checked></'+element.trTag+'>';
						}
						_affectCount++;
						flag = true;
					} else {
						if (inputs[j].type == 'checkbox' && inputs[j].checked) {
							_affectCount++;
							if (isOnly != null && isOnly != "") {
								if (!onlyFlag) {
									temp_xml = temp_xml + '<'+element.trTag+'>' + inputs[j].value + '</'+element.trTag+'>';
									flag = true;
									onlyFlag = true;
								} else {
									alert("一次只能选择一条记录！");
									return false;
								}
							} else {
								temp_xml = temp_xml + '<'+element.trTag+'>' + inputs[j].value + '</'+element.trTag+'>';
								flag = true;
							}
						}
					}
				}
			}

			if (!flag) {
				alert('请先选择')
				return false;
			}else{
				if(delConfirm != null && delConfirm != ""){
					if(confirm('确定要删除吗?')==false)
						return;
				}
			}
			if (addXML != null && addXML != "") {
				temp_xml = addXML + '<multiData>' + temp_xml + '</multiData>'
			}
			//prompt('',temp_xml);return false;
			window.xmlhttp.post(btn, temp_xml ,subFunc)
			var str = window.xmlhttp._object.responseText

			if (window.doMsg(str,hideMsg)) {
				return true;
			} else
				return false;

			refresh();

		}
		function submit_choose_base(btn,addXML,isAll,isOnly,hideMsg,subFunc) {
			var element=_e();
			_affectCount=0;
			if (window.trim(element.choose) == '') {
				alert('choose属性设置错误！');
			}

			if (subFunc==null) {
				subFunc = '';
			}


			var data = element.getElementsByTagName('tbody')[1];
			var trs = data.getElementsByTagName("tr")
			var temp_xml = '';
			var flag = false;
			var onlyFlag = false;

			for (var i=0; i<trs.length; i++){
				var inputs =  trs[i].getElementsByTagName("input")
				for (var j=0; j<inputs.length; j++) {
					if (isAll != null && isAll != "") {
						if (inputs[j].type == 'checkbox' && inputs[j].checked) {
							temp_xml = temp_xml + '<'+element.trTag+'>' + inputs[j].value + '<checked>1</checked></'+element.trTag+'>';
						} else {
							temp_xml = temp_xml + '<'+element.trTag+'>' + inputs[j].value + '<checked>0</checked></'+element.trTag+'>';
						}
						_affectCount++;
						flag = true;
					} else {
						if (inputs[j].type == 'checkbox' && inputs[j].checked) {
							_affectCount++;
							if (isOnly != null && isOnly != "") {
								if (!onlyFlag) {
									temp_xml = temp_xml + '<'+element.trTag+'>' + inputs[j].value + '</'+element.trTag+'>';
									flag = true;
									onlyFlag = true;
								} else {
									alert("一次只能选择一条记录！");
									return false;
								}
							} else {
								temp_xml = temp_xml + '<'+element.trTag+'>' + inputs[j].value + '</'+element.trTag+'>';
								flag = true;
							}
						}
					}
				}
			}



			//prompt('',temp_xml);
			temp_xml="<root>"+temp_xml+"</root>"
			if(addXML!=null)
				window.xmlhttp.post(btn, "<abcde>"+temp_xml.replace(/</g, "&lt;").replace(/>/g, "&gt;")+"</abcde>"+addXML,subFunc)
			else
				window.xmlhttp.post(btn, "<abcde>"+temp_xml.replace(/</g, "&lt;").replace(/>/g, "&gt;")+"</abcde>",subFunc)
			var str = window.xmlhttp._object.responseText

			if (window.doMsg(str,hideMsg)) {
				return true;
			} else
				return false;

			refresh();

		}
		// modify
		function submit_modify(btn,addXML,isAll,isOnly,hideMsg,subFunc) {
			var element=_e();
			_affectCount=0;
			if (subFunc==null) {
				subFunc = '';
			}

			var data = element.getElementsByTagName('tbody')[1];
			var trs = data.getElementsByTagName("tr")
			var temp_xml = '';
			var flag = false;
			var onlyFlag = false;

			for (var i=0; i<trs.length; i++){
				var inputs =  trs[i].getElementsByTagName("input")
				for (var j=0; j<inputs.length; j++) {
					if (isAll != null && isAll != "") {
						if (inputs[j].type == 'checkbox' && inputs[j].checked!=getCheckboxOldChecked(inputs[j])) {
							temp_xml = temp_xml + '<'+element.trTag+'>' + getSubmitModifyPkStr(inputs[j]) + '<checked>1</checked></'+element.trTag+'>';
						} else {
							temp_xml = temp_xml + '<'+element.trTag+'>' + getSubmitModifyPkStr(inputs[j]) + '<checked>0</checked></'+element.trTag+'>';
						}
						_affectCount++;
						flag = true;
					} else {
						if (inputs[j].type == 'checkbox' && inputs[j].checked!=getCheckboxOldChecked(inputs[j])) {
							_affectCount++;
							if (isOnly != null && isOnly != "") {
								if (!onlyFlag) {
									temp_xml = temp_xml + '<'+element.trTag+'>' + getSubmitModifyPkStr(inputs[j]) + '<'+element.trTag+'>';
									flag = true;
									onlyFlag = true;
								} else {
									alert("一次只能选择一条记录！");
									return false;
								}
							} else {
								temp_xml = temp_xml + '<'+element.trTag+'>' + getSubmitModifyPkStr(inputs[j]) + '</'+element.trTag+'>';
								flag = true;
							}
						}
					}
				}
			}

			if (!flag) {
				alert('请先选择,数据没有变化!')
				return false;
			}
			if (addXML != null && addXML != "") {
				temp_xml = addXML + '<multiData>' + temp_xml + '</multiData>'
			}
			window.xmlhttp.post(btn, temp_xml ,subFunc)
			var str = window.xmlhttp._object.responseText

			if (window.doMsg(str,hideMsg)) {
				return true;
			} else
				return false;

			refresh();

		}
		function getCheckboxOldChecked(chk){
			var element=_e();
			var str=chk.value;
			if(typeof(str)=="undefined"||str.indexOf("checkbox_value")<0)
				return chk.checked;
			if(str.indexOf("<checkbox_value>1</checkbox_value>")>=0)
				return true;
			else
				return false;
		}
		function getSubmitModifyPkStr(chk){
			var element=_e();
			var value=chk.value;
			if(getCheckboxOldChecked(chk)==true)
				value=value.replace("<checkbox_value>1</checkbox_value>","")
			else
				value=value.replace("<checkbox_value>0</checkbox_value>","")
			if(chk.checked==true)
				return value+"<checkbox_value>1</checkbox_value>";
			else
				return value+"<checkbox_value>0</checkbox_value>";
		}

		function modify() {
			var element=_e();
			//var vLoad = this.parentNode.previousSibling.children(0).value
			var vLoad = this.parentNode.parentNode.children(0).children(0).value;

			var width = '350px'
			var height = '350px'
			if(element.dialogWidth != null)
				width = element.dialogWidth;
			if(element.dialogHeight != null)
				height = element.dialogHeight;
			if(element.useIFDlg=="true")
				openIFDialog(window,element.openPage+'?load='+vLoad,'dialogWidth:'+width+';dialogHeight:'+height, null)
			else
				openDialog(element.openPage+'?load='+vLoad, 'dialogWidth:'+width+';dialogHeight:'+height, element)
		}

		function pageUp() {
			var element=_e();
			if (!isTurn)
				return;
			if (begin<=1) {
				alert('此为首页')
				return false;
			}
			if(element.is_alterpage=="false")
				return ;

			begin = begin - page
			if (begin < 1) begin = 1
				refresh()
		}

		function pageDown() {
			var element=_e();
			if (!isTurn)
				return;

			if (total<=begin+page-1) {
				alert('此为尾页')
				return false
			}
			if(element.is_alterpage=="false")
				return ;

			begin = begin + page
			refresh();
		}

		function pageHome() {
			var element=_e();
			if (!isTurn)
				return;
			if(element.is_alterpage=="false")
				return ;
			begin = 1
			refresh();
		}

		function pageEnd() {
			var element=_e();
			if (!isTurn)
				return;
			if(element.is_alterpage=="false")
				return ;

			begin = (parseInt(total/page+0.999,10)-1)*page + 1
			refresh();
		}

		function checkFormula(formula){
		}

		function setActiveHeads(){
			var element=_e();
			vAvtiveHeads = element.activeHeads;
		}
		function getActiveHeads(){
			return vAvtiveHeads;
		}

		function getTabData(){
			if(response == null){
				return "<root/>"
			}
			return response.cloneNode(true);
		}

		function inputXML(argSource){ ////objXMLDoc接收XML文件数据
			argSource = trim(argSource);
			var element=_e();
			var objXMLDoc = null;
			try{
				switch(typeof(argSource)){
					case "string"://XML字节流
						if(argSource.search(/\./) != -1 && argSource.search(/\</) == -1){ //xml file's location

							objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");
							objXMLDoc.async = false;
							if(argSource.search(/\+/) != -1)
								argSource = eval(argSource);
							objXMLDoc.load(argSource);
							break;
						}
						if(argSource.search(/\</) != -1){ //xml string
							objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");
							objXMLDoc.loadXML(argSource);
							break;
						}
						objXMLDoc = eval(argSource);
						objXMLDoc = (objXMLDoc.XMLDocument)?(objXMLDoc.XMLDocument):null; //xml data island

						break;
					case "object"://XMLDOM对象
						if(argSource.xml)
							return objXMLDoc = argSource; //xml document object
						break;
					default:
						objXMLDoc = null;
				}
				if (!objXMLDoc.xml) objXMLDoc = null;
			} catch(err) { objXMLDoc = null; }

			if(objXMLDoc){
				objXMLDoc.setProperty("SelectionLanguage","XPath");
				objXMLDoc.setProperty("SelectionNamespaces","xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
			}
			return objXMLDoc;
		}

		/*
		ü齲mlNode选取数据列
		xml选取方式定义如下
		<root>
		<rows>
		<row idx='1'>
		</rows>
		<cols>
		<col name='abc' colIdx='1' />
		<col name='def' colIdx='4' />
		<col name='fgh' colIdx='7' />
		<col name='xyz' colIdx='8' />
		</cols>
		</root>
		表示，选取数据中第1行，第1，4，7，8列，组成统计图，各列分别命名对应其name,分别为abc,def,fgh,xyz
		*/
		function getTabDataByXmlNodeNoGroup(xmlmode){
			var element=_e();
			if(xmlmode == null){
				alert("getTabDataByXmlNodeNoGroup()输入参数为NULL");
				return ;
			}
			var selectNode = inputXML(xmlmode);
			var template = "<?xml version='1.0' encoding='gb2312'?><xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:output method='xml' encoding='gb2312'/><xsl:template match='/'><xsl:element name='root'><xsl:apply-templates select='td[@name]'/></xsl:element></xsl:template><xsl:template match='td[@name]'><xsl:element name='item'><xsl:element name='name'><xsl:value-of select='@name'/></xsl:element><xsl:element name='value'><xsl:value-of select='text()'/></xsl:element></xsl:element></xsl:template></xsl:stylesheet>";
			var simpletemplate = inputXML(template);
			var temp = selectNode.selectSingleNode("//row");

			var rowIdx = temp.getAttribute("idx");
			if(response == null){
				return "<root/>"
			}
			var trObj = response.selectSingleNode("//tbody/tr[position()="+rowIdx+"]").cloneNode(true);
			if(trObj == null){
				alert("tabCtn指定行["+rowIdx+"]不存在");
				return "<root/>"
			}
			var nodeList = selectNode.selectNodes("//col");
			for(i = 0 ;i<nodeList.length;i++){
				var tempItem = nodeList.item(i);
				var tdNode = trObj.selectSingleNode(".//td[position()="+tempItem.getAttribute("colIdx")+"]");
				tdNode.setAttribute("name",tempItem.getAttribute("name"));
			}

			var lastres = trObj.transformNode(simpletemplate);
			return lastres

		}

		function changeTabData(expressionStr){
			if(expressionStr){
				vexpression = expressionStr;
			}
			if(response == null){
				return;
			}
			var tbodyNode = response.selectSingleNode("//tbody");
			if(tbodyNode == null){
				return;
			}
			tbodyNode = response.removeChild(tbodyNode);
			var temp_template = constructTemplate();
			if(temp_template == null){
				return;
			}
			var resStr = tbodyNode.transformNode(temp_template);
			tbodyNode = inputXML(resStr).selectSingleNode("//tbody");
			response.appendChild(tbodyNode);
			refreshTab();
		}

		//生成指定的模板
		function constructTemplate(){
			// var template = calculateXSLT.selectSingleNode("//xsl:stylesheet").cloneNode(true);
			var templateStr = "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"><xsl:output method=\"xml\" encoding=\"gb2312\" /><xsl:template match=\"/\"><xsl:element name=\"tbody\"><xsl:copy-of select=\"@*\" /><xsl:apply-templates select=\"tr\" /></xsl:element></xsl:template><xsl:template match=\"tr\"><xsl:element name=\"tr\"><xsl:copy-of select=\"@*\" /><xsl:apply-templates select=\"td\" /></xsl:element></xsl:template><xsl:template match=\"td_Exp\"><xsl:element name=\"td\"><xsl:copy-of select=\"@*\" /><xsl:value-of select=\"expression\" /></xsl:element></xsl:template><xsl:template match=\"td\"><xsl:copy-of select=\".\" /></xsl:template></xsl:stylesheet>";
			var template = inputXML(templateStr);
			var reg = /@(\d*)/i;
			var resCol = vexpression.match(reg);
			resCol = resCol[0].substring(1);
			if(isNaN(resCol)){
				return null;
			}
			var splitCalculateExpStr = vexpression.substring(vexpression.indexOf('=')+1);
			splitCalculateExpStr = splitCalculateExpStr.replace(/\//g," div ");
			var calculateExpStr =splitCalculateExpStr.replace(/@(\d*)/g,"../td[position()='$1']/text()");

			var calculateTemplateNode = template.selectSingleNode("//xsl:template[@match=\"td_Exp\"]");
			calculateTemplateNode.setAttribute("match","td[position()=\'"+resCol+"\']");
			var expressionNode = calculateTemplateNode.selectSingleNode(".//xsl:value-of[@select='expression']");
			expressionNode.setAttribute("select",calculateExpStr);

			return template;
		}

		//设置公式
		function setExpression(){
			var element=_e();
			vexpression = element.expression;
			changeTabData();
		}

		//取得公式
		function getExpression(){
			return vexpression;
		}

		//根据参数选取数据
		//captionColIdx 用作名称的列
		//valColIdx 用作统计对比值得列
		//tempRowStart 行起点
		//tempRowEnd 行终点
		function getTabDataForChart(captionColIdx,valColIdx,tempRowStart,tempRowEnd) {
			var element=_e();
			if(mymainTab.tBodies[0].rows(0) == null) {
				alert("没有结果，不能做图！");
				return "";
			}

			var rowStart = -1;
			var rowEnd = -1;
			if(!tempRowStart) {
				rowStart ='0';
			} else {
				rowStart =tempRowStart;
			}
			if(!tempRowEnd) {
				rowEnd = getRows();
			} else {
				rowEnd = tempRowEnd;
			}
			var resStr = "<root>";
			var tempBody = mymainTab.tBodies[0];
			for(i = rowStart;i<rowEnd;i++) {
				var tempRow = tempBody.rows[i];
				resStr= resStr +"<item><name>"+tempRow.cells[captionColIdx].innerText+"</name>"+"<value>"+(tempRow.cells[valColIdx].innerText).replace(/,/g,"")+"</value>"+"</item>";
			}
			resStr = resStr +"</root>"
			return resStr;
		}

		//专为比较分析使用,即同一行有两列数据比较   //add by xsh

		//根据参数选取数据
		//captionColIdx 用作名称的列
		//valColIdx 用作统计对比值得列
		//tempRowStart 行起点
		//tempRowEnd 行终点
		function getTabDataForChart_comp(captionColIdx,valColIdx,tempRowStart,tempRowEnd) {
			var element=_e();
			if(mymainTab.tBodies[0].rows(0) == null) {
				alert("没有结果，不能做图！");
				return "";
			}

			var rowStart = -1;
			var rowEnd = -1;
			if(!tempRowStart) {
				rowStart ='0';
			} else {
				rowStart =tempRowStart;
			}
			if(!tempRowEnd) {
				rowEnd = getRows();
			} else {
				rowEnd = tempRowEnd;
			}
			var resStr = "<root>";
			var tempBody = mymainTab.tBodies[0];
			for(i = rowStart;i<rowEnd;i++) {
				var tempRow = tempBody.rows[i];
				resStr= resStr +"<item><name>"+tempRow.cells[captionColIdx].innerText+"</name>"+"<value>"+(tempRow.cells[valColIdx].innerText).replace(/,/g,"")+"</value>"+"</item><item><name></name>"+"<value>"+(tempRow.cells[valColIdx+1].innerText).replace(/,/g,"")+"</value>"+"</item>";
			}
			resStr = resStr +"</root>"
			return resStr;
		}




		//colStart 列起点
		//colEnd   列终点
		function getTabDataForPLChart(tempColStart) {
			var element=_e();
			if(mymainTab == null || mymainTab.tBodies[0].rows(0) == null) {
				alert("没有结果，不能做图！");
				return "";
			}

			var rowStart = "0";
			var rowEnd = getRows();
			var colStart = "2";
			var colEnd = getCols();
			if(!tempColStart) {
				colStart = tempColStart;
			}
			var resStr = "<?xml version='1.0' encoding='gb2312'?><root>";
			var tempHead = mymainTab.tHead;
			var tempBody = mymainTab.tBodies[0];

			for(i = colStart;i<colEnd;i++) {
				resStr = resStr + "<col title='" + tempHead.rows[0].cells[i].innerText + "' >";
				for(j = rowStart;j<rowEnd;j++) {
					var tempRow = tempBody.rows[j];
					resStr= resStr +"<r text='"+tempRow.cells[0].innerText+"."+tempRow.cells[1].innerText+"' value='"+(tempRow.cells[i].innerText).replace(/,/g,"")+"' />";
				}
				resStr = resStr + "</col>";
			}

			resStr = resStr +"</root>"
			return resStr;
		}

		//colStart 列起点
		//colEnd   列终点
		//add by chenchuan
		function getTabDataForChartsXML(colStart,colEnd) {
			var element=_e();
			if(mymainTab == null || mymainTab.tBodies[0].rows(0) == null) {
				alert("没有结果，不能做图！");
				return "";
			} 
			var rowStart = 0;
			var rowEnd = getRows(); 
			
			if(colEnd == null){ 
				colEnd = getCols();
			}
			if(!colStart == null) {
				colStart = 2;
			}
			var resStr = "<?xml version='1.0' encoding='gb2312'?><root>";
			var tempHead = mymainTab.tHead;
			var tempBody = mymainTab.tBodies[0];

			for(i = colStart;i<colEnd;i++) {
				resStr = resStr + "<item name='" + tempHead.rows[0].cells[i].innerText + "' >";
				for(j = rowStart;j<rowEnd;j++) {
					var tempRow = tempBody.rows[j];
					resStr= resStr +"<item name='"+tempRow.cells[0].innerText+"."+tempRow.cells[1].innerText+"' value='"+(tempRow.cells[i].innerText).replace(/,/g,"")+"' />";
				}
				resStr = resStr + "</item>";
			}

			resStr = resStr +"</root>";
			return resStr;
		}
		
		function getCols() {
			try {
				return mymainTab.tBodies[0].rows(0).cells.length;
			} catch (e) {
				return 0;
			}
		}

		function getRows() {
			try {
				return mymainTab.tBodies[0].rows.length;
			} catch (e) {
				return 0;
			}
		}

		function getSumSelectData(colum) {
			var element=_e();
			var flag = false;

			var data = element.getElementsByTagName('tbody')[1];
			var trs = data.getElementsByTagName("tr");
			var resStr = 0 ;
			for (var i=0; i<trs.length; i++){
				var inputs =  trs[i].getElementsByTagName("input")
				for (var j=0; j<inputs.length; j++) {
					if (inputs[j].type == 'checkbox' && inputs[j].checked) {
						var tempv = trs[i].cells[colum].innerText;
						resStr += parseFloat(tempv.replace(',',''), 10);
						flag = true;
					}
				}
			}
			if (!flag) {
				alert('请先选择')
				return false;
			}

			return resStr;
		}

		function getSelectData(row,col){
			var element=_e();
			var data = element.getElementsByTagName('tbody')[1];
			var trs  = data.getElementsByTagName("tr");
			var value = "";
//			if(trs[row].cells[col])
			if(trs[row]!=null && trs[row].innerText!="" && trs[row].cells[col]!=null)
			{
				value = trs[row].cells[col].innerText;
			}
			return value.replace(',','')
		}

		function setDataXml(source){
			var vXml = inputXML(source);
			response = vXml.selectSingleNode("//root").cloneNode(true);

			refreshTab(source);
			window.setTimeout(adjPosition,100);
		}

		function setSelectData(row,col,value){
			var element=_e();
			var data = element.getElementsByTagName('tbody')[1];
			var trs  = data.getElementsByTagName("tr");
			trs[row].cells[col].innerHTML=value;
		}
		function getAffectCount(){
			return _affectCount;
		}
		function getTotalCount(){
			return total;
		}
		function getSelectXml(){
			var element=_e();
			var refreshXML="<?xml version='1.0' encoding='GBK'?><root><msg/><error/><record><total/></record><activeHead/><thead/><tbody>";

			var data = element.getElementsByTagName('tbody')[1];
			var trs = data.getElementsByTagName("tr");

			for (var i=0; i<trs.length; i++){
				var inputs =  trs[i].getElementsByTagName("input")
				for (var j=0; j<inputs.length; j++) {
					if (inputs[j].type == 'checkbox' && inputs[j].checked) {

						var tds= trs[i].getElementsByTagName("td");
						var resultXML ="<tr>" ;
						if(inputs[j].value!=""){
							resultXML = resultXML + "<pk>"+inputs[j].value+"</pk>";
						}

						for(x=1;x<tds.length;x++){
							resultXML = resultXML + "<td>"+trs[i].cells[x].innerText+"</td>";
						}

						refreshXML = refreshXML + resultXML +"</tr>";

					}
				}
			}
			refreshXML = refreshXML + "</tbody></root>";

			return refreshXML;
		}
		/// order table LZK ADD BEGIN
		function sortTableTrSetCursor(thIndex){
			var element=_e();
			if(element.orderby=="false")
				return ;
			var table=element.document.getElementById(_innerTableId);
			if (trim(element.dynaHead) == "0" && trim(element.fixCol) != "" && element.document.getElementById(_innerHeadId)){
				table=element.document.getElementById(_innerHeadId);
			}
			var ths=table.getElementsByTagName("th");
			if(ths.length==0)
				return ;
			var th;
			for(var i=0;i<ths.length;i++){
				th=ths[i];
				tdIndex=th.cellIndex;
				if(th.getAttribute("dataIndex")!=null)
					tdIndex=th.getAttribute("dataIndex");

				th["_orderTh"]=false;
				if(th.parentNode.parentNode.childNodes.length!=th.parentNode.rowIndex+th.rowSpan)
					continue;
				if(th.getElementsByTagName("input").length>0)
					continue;
				var text=th.innerText;
				th.innerHTML=th.innerText+sortTableTrDefaultSpace;
				th["_orderTh"]=true;
				th.style.cursor="hand";
				if(tdIndex==thIndex){
					if(element["_orderBy"]=="desc")
						th.innerHTML="<table BORDER=0 CELLPADDING=0 CELLSPACING=0 width='100%' height='98%'><tr  noWrap='true'><td align='center' noWrap='true' style='color: #000000; font-weight: bold;  font-size: 13px;' _innerOrderTd='1' >"+text+"</td><td width=10  _innerOrderTd='1' ><img  _innerOrderTd='1'  src='"+window.prefix+"/base/themes/blue/images/button/down1.gif' width='10' height='9'></td></tr></table>";//"<span style='font-family: Wingdings 3;color:red'>q</span>";//"<img src='"+window.prefix+"/base/themes/blue/images/button/down1.gif' width='10' height='9'>";//
					else
						th.innerHTML="<table BORDER=0 CELLPADDING=0 CELLSPACING=0 width='100%' height='98%'><tr noWrap='true'><td align='center' noWrap='true' style='color: #000000; font-weight: bold;  font-size: 13px;'  _innerOrderTd='1' >"+text+"</td><td width=10  _innerOrderTd='1' ><img  _innerOrderTd='1'  src='"+window.prefix+"/base/themes/blue/images/button/up1.gif' width='10' height='9'></td></tr></table>";//"<span style='font-family: Wingdings 3;color:red'>p</span>";//"<img src='"+window.prefix+"/base/themes/blue/images/button/up1.gif' width='10' height='9'>";//
				}
			}
			element["_oldOrderThIndex"]=thIndex;
		}
		var sortTableTrDefaultSpace="<img style='visibility:hidden;' src='"+window.prefix+"/base/themes/blue/images/button/up1.gif' width='10' height='9'>";//"<span style='visibility:hidden;font-family: Wingdings 3;color:red'>q</span>";
		function sortTableTr(){
			var element=_e();
			if(element.orderby=="false")
				return ;
			if(typeof(event.srcElement.tagName)=="undefined")
				return ;
			var th=event.srcElement;
			if(event.srcElement.tagName.toLowerCase()=="td"){
				th=sortTableTrGetTh(th);
				if(th==null)
					return ;
			}
			if(typeof(th["_orderTh"])=="undefined"||th["_orderTh"]==false)
				return ;
			tdIndex=th.cellIndex;
			if(th.getAttribute("dataIndex")!=null)
				tdIndex=th.getAttribute("dataIndex");

			orderBy=element["_orderBy"];

			var orderText="";
			if(typeof(orderBy)=="undefined"||element["_oldOrderThIndex"]!=tdIndex)
				orderBy="desc";
			if(orderBy=="asc"){
				orderBy="desc";
			}else{
				orderBy="asc";
			}
			element["_orderBy"]=orderBy;
			var table= new ActiveXObject("Microsoft.XMLDOM");
			table.loadXML(element.serverXml);
			var tbodys=table.getElementsByTagName("tbody");
			if(tbodys.length==0)
				return ;
			var trs=tbodys[0].getElementsByTagName("tr");
			if(trs.length==0)
				return ;

			var minIndex=element.orderby.split(":")[0];
			var maxIndex=trs.length-element.orderby.split(":")[1]-1;
			if(maxIndex<1)
				return ;
			///trs,tdIndex,minIndex,maxIndex
			var trArray=new Array();
			for(var i=0;i<maxIndex-minIndex+1;i++){
				trArray[i]=trs[parseInt(i)+parseInt(minIndex)];
			}
			var sortFunction=sortTableTrByNum;

			trArray.sort(sortTrArraySort);
			for(var i=0;i<trArray.length;i++){
				if(trs.length-1==maxIndex){
					tbodys[0].removeChild(trArray[i]);
					tbodys[0].appendChild(trArray[i]);
				}
				else
					tbodys[0].insertBefore(trArray[i],tbodys[0].childNodes[maxIndex+1]);
			}
			setDataXml(table.xml);
			sortTableTrSetCursor(tdIndex);
		}
		function sortTrArraySort(a,b){
			return sortTableTrByNum(a.childNodes[tdIndex].text,b.childNodes[tdIndex].text,orderBy);
		}
		function sortTableTrGetTh(th){
			try{
				if(typeof(th["_innerOrderTd"])!="undefined"){
					while(th.tagName.toLowerCase()!="th")
					th=th.parentNode;
					return th;
				}else
					return null;
			}catch(e){
				return null;
			}
		}
		function sortTableTrByText(a,b,orderBy){
			var at=a;
			var bt=b;
			if(orderBy=="desc"){
				at=b;
				bt=a;
			}
			return at.localeCompare(bt);
			if(at>bt)
				return 1;
			else if(at<bt)
				return -1;
			else
				return 0;
		}
		function sortTableTrByNum(a,b,orderBy){
			var at=a;
			var bt=b;
			if(orderBy=="desc"){
				at=b;
				bt=a;
			}
			if((!isMoney(at))||(!isMoney(bt)))
				return sortTableTrByText(a,b,orderBy);
			if(parseFloat(at)>parseFloat(bt))
				return 1;
			else if(parseFloat(at)<parseFloat(bt))
				return -1;
			else
				return 0;
		}
		/// order table LZK ADD END
	function _initAttr(){
		var element=_e();
		jhtc_attr_init(element,"remberXsl","false");
		jhtc_attr_init(element,"bottomFix","0");
		jhtc_attr_init(element,"rightFix","0");
		jhtc_attr_init(element,"choose",null);
		jhtc_attr_init(element,"checkflag",null);
		jhtc_attr_init(element,"remove",null);
		jhtc_attr_init(element,"remove_display",null);
		jhtc_attr_init(element,"update",null);
		jhtc_attr_init(element,"dialogWidth",null);
		jhtc_attr_init(element,"dialogHeight",null);
		jhtc_attr_init(element,"serverXml","");
		jhtc_attr_init(element,"onrefresh","");
		jhtc_attr_init(element,"onafterdel","");
		jhtc_attr_init(element,"initLoad","true");
		jhtc_attr_init(element,"async","false");
		jhtc_attr_init(element,"asyncError","");
		jhtc_attr_init(element,"onDataLoaded","");
		jhtc_attr_init(element,"linkId","false");
		jhtc_attr_init(element,"openPage","update.html");
		jhtc_attr_init(element,"useIFDlg","false");
		jhtc_attr_init(element,"fixCol","1");
		jhtc_attr_init(element,"activeHeads","");
		jhtc_attr_init(element,"headXsl","false");
		jhtc_attr_init(element,"orderby","false");
		jhtc_attr_init(element,"xslFile","");
		jhtc_attr_init(element,"xslFileFirst","");
		jhtc_attr_init(element,"keepHead","false");
		jhtc_attr_init(element,"xslFileNonFirestPage","");
		jhtc_attr_init(element,"count_page","100");
		jhtc_attr_init(element,"is_alterpage","true");
		jhtc_attr_init(element,"is_autofocus","true");
		jhtc_attr_init(element,"annex",null);
		jhtc_attr_init(element,"trTag","record");
		jhtc_attr_init(element,"headRows","0");
		jhtc_attr_init(element,"rowColor","true");
		jhtc_attr_init(element,"expression","");
		jhtc_attr_init(element,"ignoreKeyCodes","");
		jhtc_attr_init(element,"onfinishload","");
		jhtc_attr_init(element,"includeJhtc","false");
		jhtc_attr_init(element,"dynaHead","0");

	  	__jhtcBindPropertyChange(element,"activeHeads",setActiveHeads);
	  	__jhtcBindPropertyChange(element,"expression",setExpression);
	  	element.__inInit=true;
		element.checkChange=checkChange;
		element.refresh=refresh;
		element.refreshTab=refreshTab;
		element.refreshTabByXML=refreshTabByXML;
		element.changeTabData=changeTabData;
		element.submit=submit;
		element.submit_choose=submit_choose;
		element.submit_choose_base=submit_choose_base;
		element.submit_modify=submit_modify;
		element.checksubmit=checksubmit;
		element.checkFormula=checkFormula;
		element.getTabData=getTabData;
		element.getTabDataByXmlNodeNoGroup=getTabDataByXmlNodeNoGroup;
		element.getTabDataForChart=getTabDataForChart;
		element.getTabDataForChart_comp=getTabDataForChart_comp;
		element.getTabDataForPLChart=getTabDataForPLChart;
		element.getTabDataForChartsXML=getTabDataForChartsXML;
		element.getRows=getRows;
		element.getSumSelectData=getSumSelectData;
		element.getSelectData=getSelectData;
		element.adjPosition=adjPosition;
		element.assemble=assemble;
		element.setDataXml=setDataXml;
		element.setSelectData=setSelectData;
		element.getSelectXml=getSelectXml;
		element.getAffectCount=getAffectCount;
		element.getArgumentAction=getArgumentAction;
		element.getArgumentXml=getArgumentXml;
		element.getTotalCount=getTotalCount;
		element.init=init;
		element.del=del;
	}
	_initAttr();
	if(_e().activeHeads !=null && _e().activeHeads !=""){
		setActiveHeads();
	}
	_e().jhtcInit=function(){
		_e().init();
	};
	return {
		onAll:function(){_e().__inInit=false;resetButtonStats();}
	};
	return null;
};
jhtc_class_map["tableCtn"]=jhtc_tableCtn;
jhtc_class_map["tableCtna"]=jhtc_tableCtn;