function jhtc_body(win,jhtc_obj){
	return jhtc_body_c(__jhtcElementFun(jhtc_obj));
}
function jhtc_body_c(_e){
	var hiddenArray = new Array();
	var returnXML = "";
	
	// 通过load属性初始化数据，将查出的数据放到hiddenArray里(二维数组的方式)
	function init() {
		var element=_e();
		//window.onerror=function(){return true;}
		element.onkeydown=window_keyDown;
		if (window.trim(element.load) != ""){
			hiddenArray = new Array();
			window.xmlhttp.post(element.load, element.para, "?isCheck=false");
			var srcTree = new window.ActiveXObject("Microsoft.XMLDOM");
			srcTree.async=false;
			srcTree.load(xmlhttp._object.responseXML);
			returnXML = xmlhttp._object.responseXML.xml;
			
			var list1 = srcTree.getElementsByTagName("tr");
			hiddenArray.length = list1.length;
			for (var i=0; i<list1.length; i++) {
				var list2 = list1.item(i).getElementsByTagName("td");
				var tdV = new Array(list2.length);
				for (var j=0; j<list2.length; j++) {
					if (list2.item(j).firstChild != null)
						tdV[j] = list2.item(j).firstChild.nodeValue;
					else
						tdV[j] = "";
				}
				hiddenArray[i]=tdV;
			}
			if (element.hideMsg == "false"||(xmlhttp._object.responseText.indexOf("<error>")>0&&element.hideMsg=="ok"))
				return doMsg(xmlhttp._object.responseText);
			if(xmlhttp._object.responseText.indexOf("<error>")>0)
				return false;
			else
				return true;
		}	
	}
	function window_keyDown(){
		var element=_e();
		var mlinectn;
		if(element.document.activeElement.tagName!='INPUT'){
			if((event.ctrlKey)&&(event.keyCode==67)){
				mlinectn=getLineCtn();
				if(mlinectn==null) 
					return;
				//mlinectn.copyContents();
			}else if((event.ctrlKey)&&(event.keyCode==86)){
				mlinectn=getLineCtn();
				if(mlinectn==null) 
					return;
				// mlinectn.pasteContents();
			}
		}
	}
	function getLineCtn(){
		var element=_e();
		var data_win=element.document.getElementsByTagName('table');
		for(i=0;i<data_win.length;i++){
			var mo=data_win[i];
			if((mo.className=="lineCtn")&&(mo.style.display!="none")){
				return mo;
				break; 
			}
		} 
		return null;
	}
	function post() {
		return init();
	}
	
	// 得到返回XML
	function getReturnXML(){	  	
		return returnXML;
	}
	
	// 得到数据(二维数组)
	function getHiddenVs(){
		return hiddenArray;
	}
	
	// 得到一数组，适合查出的数据就是一维的
	function getOneDim(){	  	
		return hiddenArray[0];
	}
	
	// 得到以逗号分隔的字符串，适合查出的数据就是一维的
	function getOneDimAsString(){
		return hiddenArray[0].toString();
	}
	
	function _initAttr(){
		var element=_e();
	  	jhtc_attr_init(element,"load",null);
	  	jhtc_attr_init(element,"para","");
	  	jhtc_attr_init(element,"hideMsg","false");
	  	
		element.getHiddenVs=getHiddenVs;
		element.getOneDim=getOneDim;
		element.getOneDimAsString=getOneDimAsString;
		element.getReturnXML=getReturnXML;
		element.post=post;
		element.init=init;
		element.jhtcInit=function(){
			element.init();
		};
	}
	_initAttr();
	return null;
};document.onkeydown = check;function check(e) {	var code;	if (!e)		var e = window.event;	if (e.keyCode)		code = e.keyCode;	else if (e.which)		code = e.which;	if (((event.keyCode == 8) && // BackSpace	((event.srcElement.type != "text" && event.srcElement.type != "textarea" && event.srcElement.type != "password") || event.srcElement.readOnly == true))			|| ((event.ctrlKey) && ((event.keyCode == 78) || (event.keyCode == 82)))			|| // CtrlN,CtrlR			(event.keyCode == 116)) { // F5		event.keyCode = 0;		event.returnValue = false;	}	return true;}jhtc_class_map["subBody"] = jhtc_body;