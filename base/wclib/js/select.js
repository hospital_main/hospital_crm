/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createSelect=function(page,id,parentObj,props,cb){
		if(props["data"])
			props["data"]=_globaljs.getRealExpression(page,props["data"]);
		this._sendAndCreateCtrl("select","_initSelectMethod",page,id,parentObj,props,cb);
	};
_globaljs._initSelectMethod=function(page,ctrl){
		_globaljs._initCtrlMethod(page,ctrl,"ComboBox",_globaljs.CTRL_TYPE_VALUE);
		_globaljs_basecombobox_initMethod(page,ctrl);
		_globaljs_basecombobox_initListBoxMethod(page,ctrl);
		_globaljs._initSurfaceCtrl(ctrl,page._hctrl(ctrl.id+"_Input_"),false);
		
		ctrl.vhSetValue=_globaljs_select_vhSetValue;
		ctrl.vhSetData=_globaljs_select_vhSetData;
		ctrl._doShowList=_globaljs_select__doShowList;
		ctrl._onCheckValue=_globaljs_select__onCheckValue;
		ctrl._onCheckValueFuns=[
				_globaljs_text_checkRequired,
				_globaljs_text_checkMinValue,
				_globaljs_text_checkMaxValue,
				_globaljs_text_checkCompareValue
			];
		ctrl._listBoxDefaultHeight=200;
		ctrl._vhYearValue="";
		ctrl._vhMonthValue="";
		ctrl._vhDateValue="";
		ctrl.vhSetValue(ctrl._vhValue);
		ctrl._vhOldValue=ctrl._vhValue;
		ctrl._onDependUpdate=function(kind,param){ctrl.vhSetData(ctrl._vhData,null);}
		ctrl._addUpdateListener=_globaljs_select_addUpdateListener;
		ctrl._delUpdateListener=_globaljs_select_delUpdateListener;
		ctrl._addUpdateListener(ctrl._vhData);
		ctrl._initState();
		_globaljs._initSelectAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initSelectAPI=function(page,ctrl){
		_globaljs._initCtrlAPI(page,ctrl);
		ctrl.getFieldValue=function(idx){this._ctrl.vhGetFieldValue(idx);};
		ctrl.setData=function(v){this._ctrl.vhSetData(v);};
	};
function _globaljs_select_addUpdateListener(data){
		var its=_globaljs_select_getDependUpdateItems(this,data);
		for(var i=0;i<its.length;i++){
			its[i].vhAddListener("update",this._onDependUpdate);
		}
	};
function _globaljs_select_delUpdateListener(data){
		var its=_globaljs_select_getDependUpdateItems(this,data);
		for(var i=0;i<its.length;i++){
			its[i].vhRemoveListener("update",this._onDependUpdate);
		}
	};
function _globaljs_select_getDependUpdateItems(ctrl,data){
		if(data.length==0)
			return [];
		var it=data.substr(data.indexOf("(")+1);
		it=it.substr(0,it.lastIndexOf(")"))
		var its=it.split(","),c;
		for(var i=0;i<its.length;i++){
			c=ctrl.page._ctrl[its[i]];
			if(c==null){
				this.page.msgBox("����BUG: û��Ԫ�� "+its[i]+"");
				return [];
			}
			its[i]=c._ctrl;
		}
		return its;
	}
function _globaljs_select_vhSetValue(v){
		this._vhValue="";
		this._getSurfaceCtrl().value="";
		_globaljs_bscombobox__selectItem(this,null);
		var tr=this.page._hctrl(this.id+"_i"+v);
		if(tr==null){
			return;
		}
		var str=_globaljs_bscombobox__formateItem(this,tr);
		this._vhValue=v;
		this._getSurfaceCtrl().value=str;
		_globaljs_bscombobox__selectItem(this,tr);
	};
function _globaljs_select__onCheckValue(){
		_globaljs._checkCtrlValue(this);
		if(this._vhIsError==false&&this._vhOldValue!=this._vhValue)
			this._dispatchEvent("update",null);
			this._vhOldValue=this._vhValue;
	};
function _globaljs_select__doShowList(){
		_globaljs_select__showHideFirst(this);
		this._showHideListBox(true);
		if(this._vhListWidth)
			this._getListBox().style.width=this._vhListWidth;
		_globaljs_select__reSetFocus(this);
	};
function _globaljs_select__reSetFocus(ctrl){
		ctrl._getSurfaceCtrl().focus();
	}
function _globaljs_select__showHideFirst(ctrl){
		var fi=_globaljs_bscombobox__getFirstTr(ctrl);
		if(fi==null)
			return;
		if(ctrl._vhIsRequired==false)
			fi.style.display="";
		else
			fi.style.display="none";
	}
function _globaljs_select_vhSetData(data,cb){
		if(this._vhData!=data){
			this._delUpdateListener(this._vhData);
			this._addUpdateListener(data);
			this._vhData=data;
		}
		var data=_globaljs.getRealExpression(this.page,data);
		var obj={};
		var ctrl=this;
		var ccb=cb;
		obj["data"]=data;
		obj["id"]=this.id;
		obj["format"]=this._vhFormat;
		var gerPara={};
		gerPara[_globaljs.getBinActionKey()]=_globaljs.getBinNewContentKey();
		_globaljs.sendRequest(_globaljs.getPageBinName(this.page,"select"),_globaljs.objToRequeatStr(obj),function(text,xml){
				ctrl._getListBox().innerHTML=text;
				_globaljs_select__showHideFirst(ctrl);
				if(ccb)
					ccb(obj.id);
			},_globaljs.objToUrlcode(gerPara));
		
	}