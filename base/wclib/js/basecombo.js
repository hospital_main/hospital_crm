/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
function _globaljs_basecombobox_initMethod(page,ctrl){
		ctrl._listBoxDefaultHeight=200;
		ctrl._getBtn=_globaljs_bscombobox_getBtn;
		ctrl._getBgDiv=_globaljs_bscombobox_getBgDiv;
		ctrl._getListBox=_globaljs_bscombobox_getListBox;
		ctrl._onSetNormalState=_globaljs_bscombobox_onSetNormalState;
		ctrl._onSetDisabledState=_globaljs_bscombobox_onSetDisabledState;
		ctrl._onSetErrorState=_globaljs_bscombobox_onSetErrorState;
		ctrl._onSetRequiredState=_globaljs_bscombobox_onSetRequiredState;
		ctrl._onSetFocusState=_globaljs_bscombobox_onSetFocusState;
		ctrl._onKeyLoseFocus=_globaljs_bscombobox_onKeyLoseFocus;
		ctrl._onBtnClick=_globaljs_bscombobox__onBtnClick;
		ctrl._showHideListBox=_globaljs_bscombobox_showHideListBox;
		ctrl._vhOldSelItem=null;
	};
function _globaljs_bscombobox__onBtnClick(evt){
		this._doShowList();
	};
function _globaljs_bscombobox_getBtn(){
		return this.page._hctrl(this.id+"_Button_");
	};
function _globaljs_bscombobox_getBgDiv(){
		return this.page._hctrl(this.id+"_Bg_");
	};
function _globaljs_bscombobox_getListBox(){
		return this.page._hctrl(this.id+"_List_");
	};
function _globaljs_combobox_setCssClass(ctrl,css){
		ctrl.className=css;
		ctrl._getSurfaceCtrl().className=css+"Input";
		ctrl._getBtn().className=css+"Btn";
	};
function _globaljs_bscombobox_onSetNormalState(css){
		this._showHideListBox(false);
		_globaljs_combobox_setCssClass(this,css);
		this._surfaceCtrl.readonly=false;
	};
function _globaljs_bscombobox_onSetDisabledState(css){
		this._showHideListBox(false);
		_globaljs_combobox_setCssClass(this,css);
		this._surfaceCtrl.readonly=true;
	};
function _globaljs_bscombobox_onSetErrorState(css){
		this._showHideListBox(false);
		_globaljs_combobox_setCssClass(this,css);
	};
function _globaljs_bscombobox_onSetRequiredState(css){
		this._showHideListBox(false);
		_globaljs_combobox_setCssClass(this,css);
	};
function _globaljs_bscombobox_onSetFocusState(css){
		this._getSurfaceCtrl().focus();
		this._getSurfaceCtrl().select();
		_globaljs_combobox_setCssClass(this,css);
	};
function _globaljs_bscombobox_onKeyLoseFocus(evt){
		this._showHideListBox(false);
	};
function _globaljs_bscombobox_showHideListBox(show){
		if(show==true){
			_globaljs_combobox_resizeListBox(this);
			//this._getBgDiv().style.display="block";这里有个问题
			this._getListBox().style.display="block";	
		}else{
			this._getBgDiv().style.display="none";
			this._getListBox().style.display="none";
		}
	};
function _globaljs_combobox_resizeListBox(ctrl){
		var bgObj=ctrl._getBgDiv();
		var boxObj=ctrl._getListBox();
		var inpObj=ctrl._getSurfaceCtrl();
		ctrl._listOnBottom=true;
		bgObj.style["zIndex"]=_globaljs.getNextZIndex();
		boxObj.style["zIndex"]=_globaljs.getNextZIndex();
		//COPY BEGIN
		var hh=ctrl._listBoxDefaultHeight;
		var form = inpObj;
		var elementTop=0, elementLeft=0;
		while(form.tagName != "BODY"&&form.tagName != "DIV") {
			elementTop = elementTop + form.offsetTop + form.clientTop;
			elementLeft = elementLeft + form.offsetLeft+form.clientLeft;
			form = form.offsetParent;
		}
		elementLeft-=2;
		boxObj.style.width=ctrl.offsetWidth;
		boxObj.style.height=hh;
		with (boxObj.style) {
			zIndex =10;
			left = elementLeft;
			if(elementTop+inpObj.offsetHeight-1+parseInt(boxObj.style.height.replace(/px/,'')) >ctrl.page._ctrl.document.body.offsetHeight)
				top = elementTop - inpObj.offsetHeight-1-hh+11;
			else
			  top = elementTop + inpObj.offsetHeight-1;
		}
		with (bgObj.style) {
			left = elementLeft;
			top=boxObj.style.top;
		}	
		var t=parseInt(boxObj.style.top.replace(/px/,''),10);
		if(t<0){
			hh=hh+t;
			boxObj.style.height=hh;
			boxObj.style.top=0;
			bgObj.style.top=0;
			ctrl._listOnBottom=false;
		}
		if(boxObj.style.width<2)
			return ;
		bgObj.style.width=parseInt(boxObj.style.width);
		bgObj.style.height=hh;
	};
//////////PROCESS KEY EVENT
_globaljs_bscombobox_listBoxDefaultFormat={
		"code":[0],
		"name":[1],
		"codename":[0,1]
	};
function _globaljs_basecombobox_initListBoxMethod(page,ctrl){
		ctrl._vhOnInputChar=_globaljs_bscombobox__onInputChar;
		ctrl._vhOnSelItem=_globaljs_bscombobox__onSelItem;
		ctrl._onRowMOver=_globaljs_bscombobox_onRowMOver;
		ctrl._onRowMOut=_globaljs_bscombobox_onRowMOut;
		ctrl._onKeyLoseFocus=_globaljs_bscombobox_onKeyLoseFocus2;
		ctrl._dokeyup=_globaljs_bscombobox_dokeyup;
		ctrl.vhGetFieldValue=_globaljs_bscombobox_vhGetItemFieldValue;
		var inp=page._hctrl(ctrl.id+"_Input_")
	};
function _globaljs_bscombobox_onRowMOver(obj){
		obj.style.background="blue";
	};
function _globaljs_bscombobox_onRowMOut(obj){
		obj.style.background="white";
	};
function _globaljs_bscombobox_onKeyLoseFocus2(evt){
		if(evt.keyCode==13){
			if(this._vhOldSelItem!=null){
				this._vhOnSelItem(this._vhOldSelItem);
			}
		}
	};
function _globaljs_bscombobox_dokeyup(evt){
		_globaljs_combobox_onKeyUp(this,evt);
	};
function _globaljs_combobox_onKeyUp(ctrl,evt){
	if(evt.keyCode!=13&&evt.keyCode!=9){
		if(ctrl._getListBox().style.display=="none")
			ctrl._doShowList();
	}
	switch(evt.keyCode){
		case 38:_globaljs_bscombobox__scrollUp(ctrl,1);evt.cancelBubble=true;return; //LINE UP
		case 40:_globaljs_bscombobox__scrollDown(ctrl,1);evt.cancelBubble=true;return; //LINE DOWN
		case 33:_globaljs_bscombobox__scrollUp(ctrl,6);evt.cancelBubble=true;return; //PAGE UP
		case 34:_globaljs_bscombobox__scrollDown(ctrl,6);evt.cancelBubble=true;return; //PAGE DOWN
		case 27:ctrl._showHideListBox(false);evt.cancelBubble=true;return; //ESC
	}
	ctrl._vhOnInputChar(ctrl._getSurfaceCtrl().value);
};
function _globaljs_bscombobox__scrollUp(ctrl,num){
		if(ctrl._vhOldSelItem==null){
			var item=_globaljs_bscombobox__getFirstItem(ctrl);
			_globaljs_bscombobox__selectItem(ctrl,item);
			return;
		}
		var item=null;
		var tbody=ctrl._vhOldSelItem.parentNode;
		if(tbody.rows.length>0){
			var tri=ctrl._vhOldSelItem.rowIndex;
			var mi=0;
			if(ctrl._vhIsRequired==true)
				mi=1;
			if(tri-num<mi)
				item=tbody.rows[mi];
			else
				item=tbody.rows[tri-num];
		}
		_globaljs_bscombobox__selectItem(ctrl,item);
	};
function _globaljs_bscombobox__scrollDown(ctrl,num){
		if(ctrl._vhOldSelItem==null){
			var item=_globaljs_bscombobox__getFirstItem(ctrl);
			_globaljs_bscombobox__selectItem(ctrl,item);
			return;
		}
		var item=null;
		var tbody=ctrl._vhOldSelItem.parentNode;
		if(tbody.rows.length>0){
			var tri=ctrl._vhOldSelItem.rowIndex;
			if(tri+num>=tbody.rows.length)
				item=tbody.rows[tbody.rows.length-1];
			else
				item=tbody.rows[tri+num];
		}
		_globaljs_bscombobox__selectItem(ctrl,item);
	};
function _globaljs_bscombobox_vhGetItemFieldValue(i){
		if(this._vhOldSelItem==null)
			return "";
		return this._vhOldSelItem["_vhF"+i];
	}
function _globaljs_bscombobox__onInputChar(value){
		var item=_globaljs_bscombobox__findItemByKey(this,value);
		_globaljs_bscombobox__selectItem(this,item);
	}
function _globaljs_bscombobox__selectItem(ctrl,item){
		if(ctrl._vhOldSelItem!=null){
			ctrl._vhOldSelItem.className="wctrlComboBoxListBoxItem";
		}
		ctrl._vhOldSelItem=null;
		if(item==null)
			return;
		ctrl._vhOldSelItem=item;
		item.className="wctrlComboBoxListBoxSelItem";
		item.scrollIntoView(false);
	}
function _globaljs_bscombobox__formateItem(ctrl,item){
		if(_globaljs_bscombobox__getFirstItem(ctrl)==item)
			return "";
		if(_globaljs_bscombobox_listBoxDefaultFormat[ctrl._vhFormat]){
			var f=_globaljs_bscombobox_listBoxDefaultFormat[ctrl._vhFormat];
			var str="";
			for(var i=0;i<f.length;i++){
				if(str.length==0)
					str=item["_vhF"+f[i]];
				else
					str+="　"+item["_vhF"+f[i]];
			}
			return  str;
		}else{
			return "无效的格式:"+_vhFormat;
		}
	}
function _globaljs_bscombobox__onSelItem(item){
		var v=_globaljs_bscombobox__formateItem(this,item);
		this._getSurfaceCtrl().value=v;
		this._vhValue=item["_vhF0"];
		this._showHideListBox(false);
	};
function _globaljs_bscombobox__findItemByKey(ctrl,key){
		if(key.length==0)
			return null;
		var kreg=new RegExp("^"+key);
		var trs=ctrl._getListBox().getElementsByTagName("tr");
		for(var i=0;i<trs.length;i++){
			var fc=parseInt(trs[i]._vhFc,10);
			for(var j=0;j<fc;j++){
				if(trs[i]["_vhF"+j].startsWith(key))
					return trs[i];
			}
		}
		return null;
	};
function _globaljs_bscombobox__getFirstTr(ctrl){
		var fi=ctrl.page._hctrl(ctrl.id+"_f_f_item_9_");
		return fi;
	};
function _globaljs_bscombobox__getFirstItem(ctrl){
		var fi=_globaljs_bscombobox__getFirstTr(ctrl);
		if(ctrl._vhIsRequired==true)
			return fi.nextSibling;
		else
			return fi;
	};