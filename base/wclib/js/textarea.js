/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createTextArea=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("textarea","_initTextAreaMethod",page,id,parentObj,props,cb);
	};
_globaljs._initTextAreaMethod=function(page,ctrl){
		ctrl.value=ctrl._vhValue;
		this._initCtrlMethod(page,ctrl,"textarea",_globaljs.CTRL_TYPE_VALUE);
		_globaljs._initSurfaceCtrl(ctrl,ctrl,false);
		ctrl._initState();
		
		ctrl.vhGetMinLength=_globaljs_text_vhGetMinLength;
		ctrl.vhSetMinLength=_globaljs_text_vhSetMinLength;
		ctrl.vhGetMaxLength=_globaljs_text_vhGetMaxLength;
		ctrl.vhSetMaxLength=_globaljs_text_vhSetMaxLength;
		ctrl._onCheckValue=_globaljs_text__onCheckValue;
		ctrl._onCheckValueFuns=[
				_globaljs_text_checkRequired,
				_globaljs_text_checkMinLength,
				_globaljs_text_checkMaxLength
			];
		ctrl._onkeydown=function(){};
		_globaljs._initTextAreaAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initTextAreaAPI=function(page,ctrl){
		_globaljs._initCtrlAPI(page,ctrl);
		ctrl.getMinLength=function(){return this._ctrl.vhGetMinLength();};
		ctrl.setMinLength=function(l){this._ctrl.vhSetMinLength(l);};
		ctrl.getMaxLength=function(){return this._ctrl.vhGetMaxLength();};
		ctrl.setMaxLength=function(l){this._ctrl.vhSetMaxLength(l);};
	};