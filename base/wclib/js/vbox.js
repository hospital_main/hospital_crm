/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createVBox=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("vbox","_initVBoxMethod",page,id,parentObj,props,cb);
	};
_globaljs._initVBoxMethod=function(page,ctrl){
		_globaljs._initCtrlAttribute(page,ctrl,"vbox",_globaljs.CTRL_TYPE_NULL);
		ctrl.vhGetItemBox=_globaljs_vbox_vhGetItemBox;
		_globaljs._initVBoxAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initVBoxAPI=function(page,ctrl){
		ctrl.getBoxByIndex=function(idx){return this._ctrl.vhGetItemBox(idx);};
	};
function _globaljs_vbox_vhGetItemBox(indx){
		if(indx>ctrl.rows.length)
			return null;
		else
			return 	ctrl.rows[indx];
	};