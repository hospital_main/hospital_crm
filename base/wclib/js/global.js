/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
String.prototype.trim = function(){
  return this.replace(/(^\s+)|\s+$/g, "");
};
String.prototype.startsWith = function(str){return (this.match("^"+str)==str)}
String.prototype.endsWith = function(str){return (this.match(str+"$")==str)}
_globaljs.getPathPrefix=function(){
		return this._pathPrefix;
	};
_globaljs.getStyleName=function(){
		return  this._styleName;
	};
_globaljs.getBinActionKey=function(){
		return  this._binActionName;
	};
_globaljs.getBinDecoraterKey=function(){
		return this._binDecoraterName;
	};
_globaljs.getBinCreateKey=function(){
		return this._binCreateName;
	};
_globaljs.getBinNewContentKey=function(){
		return this._binNewContentName;
	};
_globaljs.getPageExt=function(){
		return ".page";
	};
_globaljs.getPageBin=function(){
		return ".pbin";
	};
_globaljs.isDebug=function(){
		return _globaljs.getEnvValue("isDebug");
	};
_globaljs.mainPage=window;
_globaljs._pageInit=function(win){
		_globaljs_initPage(win);
	};
_globaljs._windowInit=function(){
		_globaljs.initStyle();
		_globaljs._pageInit(window);
		_globaljs.setEnvValue("isDebug",_globaljs.str2bool(page._hctrl("isDebug")));
	};
_globaljs.getStyleFilePath=function(file){
		var p=_globaljs.getPathPrefix()+"/base/wclib/style/"+_globaljs.getStyleName()+"/"+file;
		return p.replace(/\/\//g,"/");
	};
_globaljs.getFilePath=function(file){
		var p=_globaljs.getPathPrefix()+"/"+file;
		return p.replace(/\/\//g,"/");
	};
_globaljs.setRootPage=function(pageName){
		if(_globaljs.__rootPageBox)
			_globaljs.__rootPageBox.vhSetPage(pageName);
		else
			alert("底层窗口不是window.page!");
	};
_globaljs.getRootDialogSeed=function(){
		if(_globaljs.__rootDialogSeed)
			return _globaljs.__rootDialogSeed;
		alert("底层窗口不是window.page!");
		return null;
	};
//取得环境变量(变量键)
_globaljs.__envionmentValue=new Object();
_globaljs.getEnvValue=function(n){
		if(this.__envionmentValue[n])
			return this.__envionmentValue[n];
		else
			return null;
	};
//设置环境变量(变量键,变量值)
_globaljs.setEnvValue=function(n,v){
		this.__envionmentValue[n]=v;
	};
//生成唯一ID
_globaljs._generateId_=0;
_globaljs.generateId=function(){
		return "hguid"+(this._generateId_++);
	};
_globaljs.generateCtrlId=function(id){
		if(id){
			return id;
		}else{
			return _globaljs.generateId();
		}
	};
_globaljs.str2bool=function(v){
		if(v){
			if(v=="true"||v==true||v=="1"||v=="yes"){
				return true;
			}
		}
		return false;
	};
_globaljs.clonObj=function(obj){
		var nObj={};
		for(var k in obj){
			nObj[k]=obj[k];
		}
	};
_globaljs.getRealExpression=function(page,data){
		var _exp=data;
		var _name=_exp.substring(0,_exp.indexOf("("));
		var valueStr=_exp.substring(_exp.indexOf("(")+1,_exp.lastIndexOf(")"));
		if(valueStr.trim().length==0)
			return _exp;
		var values=_globaljs.parseParamExp(page,valueStr);
		_name+="(";
		if(values.length>0){
			for(var i=0;i<values.length;i++){
				if(i==0)
					_name+="\""+values[i]+"\"";
				else
					_name+=",\""+values[i]+"\"";
			}
		}
		_name+=")"
		return _name;
	};
_globaljs.parseParamExp=function(page,expr){
		if(expr.length==0)
			return [];
		var values=expr.split(",");
		for(var i=0;i<values.length;i++){
			values[i]=values[i].trim();
			if(values[i].length<2){
				continue;	
			}
			var fc="";
			if(values[i].length>0)
				fc=values[i].charAt(0);
			if(values[i].substr(0,1)=="\""&&values[i].substr(values[i].length-1)=="\""){
				values[i]=values[i].substring(1,values[i].length()-1);
			}else if(values[i].length>0&&(fc=="-"||(fc>="0"&&fc<="9"))){
				
			}else{
				if(page._hctrl(values[i])){
					values[i]=page._hctrl(values[i]).vhGetValue();
				}else{
					values[i]="";
				}
			}
		}
		return values;
	}
_globaljs.isLeapYear=function(y){
		if((y%4==0 && y%100!=0) || (y%400==0))
			return true;
		return false;
	};
_globaljs._parseDateArray=["-",".","_","日","月","年"];
_globaljs._parseDateMonth=[0,31,28,31,30,31,30,31,31,30,31,30,31];
_globaljs.parseDate=function(d,m,y){
		if(d){
			if(d.length==0)
				return null;
		}else 
			return null;
		var curDate=new Date();
		var curYear=curDate.getFullYear();
		if(((curYear%4==0)&&(curYear%100!=0))||(curYear%400==0))
  	   		_globaljs._parseDateMonth[2]=29;
  	   	else
  	   		_globaljs._parseDateMonth[2]=28;
		if(d){
			var fd=false;
			for(var i=0;i<_globaljs._parseDateArray.length;i++){
				if(d.indexOf(_globaljs._parseDateArray[i])>0){
					fd=true;
					var ep=new RegExp(_globaljs._parseDateArray[i]);
					d=d.replace(ep,"-");	
				}		
			}
			if(fd==true){
				while(d.indexOf("--")>=0){
					d=d.replace("--","-");
				}
				if(d.substr(d.length-1)=="-")
					d=d.substr(0,d.length-1);
				var dd=d.split("-");
				for(var i=dd.length-1,idx=0;i>=0;i--,idx++){
					if(idx==2)
						d=dd[idx];
					if(idx==1)
						m=dd[idx];
					if(idx==0)
						y=dd[idx];	
				}
			}else{
				var dd=d;
				var ddAry=["","","","","","","",""];
				var y2=curDate.getFullYear();
				ddAry[0]=(""+y2).substr(0,1);
				ddAry[1]=(""+y2).substr(1,1);
				ddAry[2]=(""+y2).substr(2,1);
				for(var i=d.length-1,idx=7;i>=0&&idx>=0;i--,idx--){
					ddAry[idx]=dd.substr(i,1);
				}
				if(ddAry[3])
					y=ddAry[0]+ddAry[1]+ddAry[2]+ddAry[3]
				if(ddAry[5]!="")
					m=ddAry[4]+ddAry[5];
				d=ddAry[6]+ddAry[7];
			}
		}
		if(y&&y.length==0)
			y=null;
		if(m&&m.length==0)
			m=null;
		if(y){}else{
			y=curDate.getFullYear();
		}
		var iy=parseInt(y,10);
		if(isNaN(iy)||iy<1900||iy>9999)
			return null;
		if(m){}else{
			m=curDate.getMonth()+1;
		}
		var im=parseInt(m,10);
		if(isNaN(im)||im<1||im>12)
			return null;
		var nd=parseInt(d,10);
		if(isNaN(nd)||nd<0||nd>_globaljs._parseDateMonth[im])
			return null;
		curDate.setFullYear(iy);
		curDate.setMonth(im-1);
		curDate.setDate(nd);
		return curDate;
	};
_globaljs.lpad=function(v,p,n){
		v+="";
		for(var i=0;i<=n;i++){
			v=p+v;	
		}
		return v.substr(v.length-n);
	};
_globaljs.rpad=function(v,p,n){
		v+="";
		for(var i=0;i<=n;i++){
			v+=p;	
		}
		return v.substr(0,n);
	};
_globaljs.formatDate=function(dateObj,styleStr){
		if(styleStr=="年"){
			return dateObj.getFullYear()+"年"+(dateObj.getMonth()+1)+"月"+dateObj.getDate()+"日";
		}
		var year=dateObj.getFullYear();
		var month=dateObj.getMonth()+1;
		var day=dateObj.getDate();
		year='0000'+year;
		year=year.substring(year.length-4);
		month='00'+month;
		month=month.substring(month.length-2);
		day='00'+day;
		day=day.substring(day.length-2);
		return year+styleStr+month+styleStr+day;
	};
_globaljs.rstr2bool=function(v){
		if(typeof(v)=="undefined"){
			return true;
		}
		if(v=="false"||v==false||v=="0"||v=="no"){
			return false;
		}
		return true;
	};
_globaljs.isNumber=function(v){
		return /^\-?([\d]*)\.?([\d]+)$/.test(v);
	}
_globaljs.genHtmlAttrStr=function(name,value){
		return " "+name+"=\""+value+"\" ";
	};
_globaljs.decodeXmlStr=function(s){
		s=s.replace(/&quot;/g,"\"");
		s=s.replace(/&amp;/g,"&");
		s=s.replace(/&gt;/g,">");
		s=s.replace(/&lt;/g,"<");
		return s;
	};
_globaljs.encodeXmlStr=function(s){
		s=s.replace(/&/g,"&amp;");
		s=s.replace(/>/g,"&gt;");
		s=s.replace(/</g,"&lt;");
		return s;
	};
_globaljs.encodeHttpGetParam=function(s){
		s=(""+s).replace(/%/g,"%25");
		s=s.replace(/\s/g,"%20");
		s=s.replace(/\?/g,"%3F");
		s=s.replace(/&/g,"%26");
		s=s.replace(/=/g,"%3D");
		s=s.replace(/\+/g,"%2B");
		return s;
	};
_globaljs.decodeHttpGetParam=function(s){
		s=(""+s).replace(/%20/g,"s");
		s=s.replace(/%3F/g,"?");
		s=s.replace(/%26/g,"&");
		s=s.replace(/%3D/g,"=");
		s=s.replace(/%2B/g,"+");
		s=s.replace(/%25/g,"%");
		return s;
	};
_globaljs.getPageBinName=function(page,ctrlName){
		return this.getPageFullName(page,ctrlName+".pbin");
	};
_globaljs.getPageFullName=function(page,name){
		var u=""+page._ctrl.document.URL;
		if(u.indexOf("?")>0)
			u=u.substr(0,u.lastIndexOf("?"));
		if(name){
			u=u.substr(0,u.lastIndexOf("/")+1);
			u+=name;
		}
		return u;
	};
_globaljs.sendRequest=function(action,data,callBackObj,param){
		var cbo=callBackObj;
		var req=new ActiveXObject("Microsoft.XMLHTTP");
		if(param)
			req.open("POST",action+"?"+param,true);
		else
			req.open("POST",action,true);
		req.onreadystatechange=function(){
			if(req.readyState==4){
				if (req.status>=400){
					if(cbo.vhOnError){
						cbo.onError(req.statusText,req.responseText);
					}else{
						alert("请求失败：\n"+req.statusText+"\n"+req.responseText);
					}
					return;
              			};
				if(req.status!=200){
			  		return ;
			  	};
			  	cbo(req.responseText,req.responseXML);
			};
		};
		req.send(data);
	};
_globaljs.hrefToObj=function(hre){
		if(hre.indexOf("?")>0){
			return _globaljs.urlcodeToObj(hre.substr(hre.indexOf("?")+1));
		}else{
			return {};
		};
	};
_globaljs.urlcodeToObj=function(u){
		var pstr=u.split("&"),ps,res={};
		for(var i=0;i<pstr.length;i++){
			ps=pstr[i].split("=");
			if(ps.length<2){
				continue;
			}
			res[ps[0]]=_globaljs.decodeHttpGetParam(ps[1]);
		}
		return res;
	};
_globaljs.objToUrlcode=function(obj){
		if(obj==null){
			return "";	
		}
		var str="";
		for(var k in obj){
			str+=k+"="+_globaljs.encodeHttpGetParam(obj[k])+"&";
		}
		return str;
	};
_globaljs.objToJson=function(obj){
		return JSON.stringify(obj, function (key,value) {
			return value;
		})
	};
_globaljs.jsonToObj=function(str){
		if(str){
			if(str.length==0)
				return {};
		}else{
			return {};
		}
		return JSON.parse(str, function (key,value) {
			return value;
		})
	};
_globaljs.objToRequeatStr=function(obj){
		return _globaljs.objToJson(obj);
	};
_globaljs.__zIndex=1;
_globaljs.getNextZIndex=function(){
	return this.__zIndex++;
}
_globaljs.foreachElementChild=function(elem,fun){
	if(elem==null)
		return;
	var fe=elem.firstChild;
	while(fe!=null){
		if(fe.nodeType==1){
			fun(fe);
		}
		fe=fe.nextSibling;
	}
}
_globaljs.getElementsByAttribute=function(attName){
		var box=[];
		gjo.foreachElementChild(elem,function(ele){
			var av=ele.getAttribute(attName);
			if(av!=null)
				box.push(ele);
		});
		return box;
	}
_globaljs.getElementsByAttributeValue=function(elem,attName,attValue){
		var box=[];
		gjo.foreachElementChild(elem,function(ele){
			var av=ele.getAttribute(attName);
			if(attValue==av)
				box.push(ele);
		});
		return box;
	}
_globaljs.getTableFirstTr=function(tab){
		if(tab.rows.length>0)
			return tab.rows[0];
		else
			return null;
			
	}
