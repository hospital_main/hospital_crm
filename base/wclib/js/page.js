/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createPagePanel=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("pagepanel","_initPagePanelMethod",page,id,parentObj,props,cb);
	};
_globaljs._initPagePanelMethod=function(page,ctrl){
		_globaljs._initCtrlAttribute(page,ctrl,"pagepanel",_globaljs.CTRL_TYPE_NULL);
		_globaljs._initCtrlEventFun(page,ctrl,"update,destroy");
		ctrl.vhDestroy=_globaljs_ctrl_vhDestroy;
		ctrl.vhSetPage=_globaljs_pagepanel_vhSetPage;
		_globaljs._initPagePanelAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initPagePanelAPI=function(page,ctrl){
		ctrl.destroy=function(){this._ctrl.vhDestroy();};
		ctrl.open=function(src){this._ctrl.vhSetPage(src);};
		ctrl.reload=function(src){};
	};
function _globaljs_pagepanel_vhSetPage(pagesrc){
		this.page.eval(this.id).document.location.href=pagesrc;
	};
_globaljs._initPageParamMethod=function(page,ctrl){
		_globaljs._initCtrlAttribute(page,ctrl,"pageparam",_globaljs.CTRL_TYPE_VALUE);
		_globaljs._initCtrlEventFun(page,ctrl,"update,destroy");
		ctrl.vhGetValue=_globaljs_pageparam_vhGetValue;
		_globaljs._initPageParamAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initPageParamAPI=function(page,ctrl){
		ctrl.getValue=function(){return this._ctrl.vhGetValue()};
	};
function _globaljs_pageparam_vhGetValue(){
		return this.value;
	}
function _globaljs_initPage(win){
		var page={};
		//page.parent=
		win.lib=_globaljs;
		win.page=page;
		page.msgBox=win.msgBox=function(str){this._ctrl.alert(str);};
		page.open=function(src,props){this._vhSrc=src;this._ctrl.document.location.href=src;_globaljs._dialog_parsePropsFromPage(this,props);};
		page.reload=function(){this._ctrl.document.location.href=this._vhSrc;};
		page.close=function(){this._ctrl.close();};
		page.getRequestSerialNumber=function(){return this._ctrl.__page_req_serial_number;};
		page.getRequestTotalTime=function(){return this._ctrl.__page_req_total_time;};
		page.getTitle=function(){return this._ctrl.document.title;};
		page.setTitle=function(v){this._ctrl.document.title=v;_globaljs._dialog_setTitle(this,v);};
		page.eval=function(str){return this._ctrl.eval(str);};
		page.ctrls=function(id){return this._ctrl[id];};
		
		
		win._globaljs=_globaljs;
		page._ctrl=win;
		page._globaljs=_globaljs;
		page._addCtrl=function(ctrl){var i=ctrl.id.substr(0,ctrl.id.length-_globaljs._ctrlIdSubfixLen);var obj={};this._ctrl[i]=obj;obj._ctrl=ctrl;return obj;};
		page._removeCtrl=function(ctrl){this._ctrl[ctrl.id]=null;};
		page._hctrl=function(id){return this._ctrl.document.getElementById(id);};
		page._ctrlByHid=function(hid){return this._ctrl[hid.substr(0,hid.length-_globaljs._ctrlIdSubfixLen)]};
		page._dlgIndex=0;
		page._dlgMap={};
		page._setLastCtrlCtrl=null;
		page._setLastCtrlLostFocus=function(ctl){
			if(page._setLastCtrlCtrl==ctl)
				return false;
			try{
				if(page._setLastCtrlCtrl!=null){
					if(page._setLastCtrlCtrl._setLoseFocus(ctl)==true){
						page._setLastCtrlCtrl=ctl;
						return true;
					}else
						return false;
				}else{
					page._setLastCtrlCtrl=ctl;
					return true;
				}
			}catch(E){}
		};
		page._ctrlLostFocusFun=function(){
			if(page._ctrl.event.srcElement._surfaceCtrl)
				return;
			if(page._setLastCtrlCtrl!=null){
				if(page._setLastCtrlCtrl.contains(page._ctrl.event.srcElement)){
					return;
				}else{
					page._globaljs._hiddenErrorMsg(page);
					page._setLastCtrlLostFocus(null);
				}
			}
				
		};
		page._setCtrlFocusById=function(id){
			var c=page._hctrl(id+_globaljs._ctrlIdSubfix);
			if(c==null){
				this.msgBox("����BUG: û��Ԫ�� "+id+"");
				return;	
			}
			c.vhSetFocus();
		};
		
		win.document.attachEvent("onmousedown",page._ctrlLostFocusFun);
		win.document.attachEvent("onfocus",page._ctrlLostFocusFun);
		return page;
	}