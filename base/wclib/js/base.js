/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.CTRL_TYPE_NULL=0;//0:不可以用于提交数据,1:单值数据,2:记录集
_globaljs.CTRL_TYPE_VALUE=1;
_globaljs.CTRL_TYPE_RS=2;
_globaljs.__cssPref="wctrl";
_globaljs._sendAndCreateCtrl=function(ctrlClass,initMethodFun,page,id,parentObj,props,cb){
		var ccb=cb;
		props.id=this.generateCtrlId(id);
		var getParam={};
		getParam[_globaljs.getBinActionKey()]=_globaljs.getBinCreateKey();
		this.sendRequest(this.getPageBinName(page,ctrlClass),_globaljs.objToRequeatStr(props),function(text,xml){
				var span=page._ctrl.document.createElement("span");
				span.style.display="none";
				span.innerHTML=text;
				if(props["insertBefore"])
					parentObj._ctrl.insertBefore(span.firstChild,page._hctrl(props["insertBefore"]));
				else
					parentObj._ctrl.appendChild(span.firstChild);
				span=null;
				_globaljs[initMethodFun](page,page._hctrl(props.id+_globaljs._ctrlIdSubfix));
				if(ccb)
					ccb(page._ctrl[props.id]);
			},this.objToUrlcode(getParam));
	};
_globaljs._displayErrorMsg=function(ctrl){
		var mb=null;
		if(ctrl.page._wclibErrorMsgBox){
			mb=ctrl.page._wclibErrorMsgBox;
		}else{
			mb=ctrl.page._wclibErrorMsgBox=ctrl.page._ctrl.document.createElement("div");
			ctrl.page._ctrl.document.body.appendChild(mb);
			mb.style["position"]="absolute";
			mb.style.display="none";
		}
		mb.style["zIndex"]=_globaljs.getNextZIndex();
		///这里的定位不准确，我很怕处理定位问题,所有和定位有关的问题都没把握 
		var form = ctrl;
		var elementTop=0, elementLeft=0;
		while(form.tagName != "BODY"&&form.tagName != "DIV") {
			elementTop = elementTop + form.offsetTop + form.clientTop;
			elementLeft = elementLeft + form.offsetLeft+form.clientLeft;
			form = form.offsetParent;
		}
		
		if(ctrl._vhIsError==true){
			mb.className="wctrlMessageBoxError";
			mb.innerHTML=ctrl._vhErrorMsg;
			mb.style.display="block";
		}else if(ctrl._vhTipMsg!=null&&ctrl._vhTipMsg.length>0){
			mb.className="wctrlMessageBoxNormal";
			mb.innerHTML=ctrl._vhTipMsg;
			mb.style.display="block";
		}
		if(ctrl.offsetWidth+elementLeft+100>ctrl.page._ctrl.document.body.offsetWidth){
			elementTop-=ctrl.offsetWidth;
		}
		mb.style.top=elementTop;
		mb.style.left=ctrl.offsetWidth+elementLeft+5;
		
	};
_globaljs._hiddenErrorMsg=function(page){
		page._wclibErrorMsgBox.style.display="none";
	};
_globaljs._initCtrlAttribute=function(page,ctrl,ctrlClass,ctrlType){
		ctrl.page=page;
		ctrl._ctrlClass=ctrlClass;
		ctrl._ctrlType=ctrlType;
	};
function _globaljs_ctrl_vhAddListener(kind,fun){
		if(this._vhEvents[kind]){
			for(var i=0;i<this._vhEvents[kind].length;i++){
				if(this._vhEvents[kind][i]==fun){
					return;
				}
			}
			this._vhEvents[kind].push(fun);
		}else{
			this.page.msgBox("程序BUG: 不支持的事件类型 "+kind);
		}
	};
function _globaljs_ctrl_vhRemoveListener(kind,fun){
		if(this._vhEvents[kind]){
			var funCount=0;
			var newEvts=[];
			for(var i=0;i<this._vhEvents[kind].length-funCount;i++){
				if(this._vhEvents[kind][i]==fun){
					funCount++;
				}
				newEvts[i]=this._vhEvents[kind][i+funCount];
			}
			this._vhEvents[kind]=newEvts;
		}else{
			this.page.msgBox("程序BUG: 不支持的事件类型 "+kind);
		}
	};
function _globaljs_ctrl_vhSendEvent(kind,param){
		if(this._vhEvents[kind]){
			var r=true;
			for(var i=0;i<this._vhEvents[kind].length;i++){
				var rr=_globaljs.rstr2bool(this._vhEvents[kind][i](this,kind,param));
				if(rr==false)
					r=false;
			}
			return r;
		}else{
			this.page.msgBox("程序BUG: 不支持的事件类型 "+kind);
			return false;
		}
	};
_globaljs._initCtrlEventFun=function(page,ctrl,evts){
		ctrl._vhEvents={};
		_globaljs_ctrl_addEventKind(ctrl,evts);
		ctrl.vhAddEventKind=_globaljs_ctrl_vhAddEventKind;
		ctrl.vhRemoveEventKind=_globaljs_ctrl_vhRemoveEventKind;
		ctrl.vhAddListener=_globaljs_ctrl_vhAddListener;
		ctrl.vhRemoveListener=_globaljs_ctrl_vhRemoveListener;
		ctrl._dispatchEvent=_globaljs_ctrl_vhSendEvent;
		if(ctrl._vhOnEvents){
			var evts=ctrl._vhOnEvents.split(";"),evt,fun;
			for(var i=0;i<evts.length;i++){
				if(evts[i].length==0)
					continue;
				evt=evts[i].split("=");
				if(page._ctrl[evt[1]]){
					fun=page._ctrl.eval(evt[1]);
					ctrl.vhAddListener(evt[0],fun);
				}else{
					page.msgBox("程序BUG: 没有定义事件函数 "+evt[1]+"");
					continue;
				}
			}	
		}
	}
function _globaljs_ctrl_addEventKind(ctrl,evts){
		var es=evts.split(",");
		for(var i=0;i<es.length;i++){
			if(ctrl._vhEvents[es[i]]){}else{
				ctrl._vhEvents[es[i]]=[];	
			}
		}
	};
function _globaljs_ctrl_vhAddEventKind(kinds){
		_globaljs_ctrl_addEventKind(this,kinds);
	};
function _globaljs_ctrl_vhRemoveEventKind(kind){
		alert("Not implements")
	};	
function _globaljs_ctrl_onSetNormalState(css){
		this._surfaceCtrl.className=css;
		this._surfaceCtrl.readonly=false;
		this._surfaceCtrl.disabled=false;
	};
function _globaljs_ctrl_onSetDisabledState(css){
		this._surfaceCtrl.className=css;
		this._surfaceCtrl.readonly=true;
		this._surfaceCtrl.disabled=true;
	};

function _globaljs_ctrl_onSetErrorState(css){
		this._surfaceCtrl.className=css;
	};
function _globaljs_ctrl_onSetRequiredState(css){
		this._surfaceCtrl.className=css;
	};
function _globaljs_ctrl_onSetFocusState(css){
		this._surfaceCtrl.className=css;
		this._surfaceCtrl.select();
		this._surfaceCtrl.focus();
	};
function _globaljs_ctrl_onSetMouseOverState(css){
		this._surfaceCtrl.className=css;
	};
function _globaljs_ctrl_onCheckValue(){
		this._vhIsError=false;
		this._vhErrorMsg=null;
		this._vhValue=this._surfaceCtrl.value;
	};	
function _globaljs_ctrl_setMouseOver(){
		if(this._vhIsDisabled==true){
			return;
		}
		this.__oldMouseOverCss=this._surfaceCtrl.className;
		this._onSetMouseOverState(_globaljs.__cssPref+this._ctrlClass+"Focus");
	};
function _globaljs_ctrl_setMouseOut(){
		if(this._vhIsDisabled==true){
			return;
		}
		this._onSetNormalState(this.__oldMouseOverCss);
	};
function _globaljs_ctrl_onKeyLoseFocus(){
	
	};
function _globaljs_ctrl_setLoseFocus(srcElem){
		_globaljs._hiddenErrorMsg(this.page);
		this._onCheckValue();
		this._confirmState();
		this._dispatchEvent("blur",null);
		return true;
	};
function _globaljs_ctrl_onkeydown(evt){
		this._dokeydown(evt);
		if(this._vhPreTabCtrl||this._vhNextTabCtrl){}else{
			if(this._vhPreCtrlId)
				this._vhPreTabCtrl=this.page._ctrl[this._vhPreCtrlId];
			if(this._vhNextCtrlId)
				this._vhNextTabCtrl=this.page._ctrl[this._vhNextCtrlId];
		}
		if(evt.keyCode==13||evt.keyCode==9){
			this._onKeyLoseFocus(evt);
			if(evt.shiftKey){
				if(this._vhPreTabCtrl){
					this._vhPreTabCtrl._ctrl.vhSetFocus();
				}else
					evt.keyCode=9;
			}else{
				if(this._vhNextTabCtrl){
					var r=this._dispatchEvent("nextctrl",null);
					if(r==true)
						this._vhNextTabCtrl._ctrl.vhSetFocus();
				}else
					evt.keyCode=9;
			}
		}
	};
function _globaljs_ctrl_onkeyup(evt){
		this._dokeyup(evt);
	};
function _globaljs_ctrl_dokeydown(evt){
	
	};
function _globaljs_ctrl_dokeyup(evt){
		
	};
function _globaljs_ctrl_getSurfaceCtrlValue(){
		return this._surfaceCtrl.value;
	};
function _globaljs_ctrl_getSurfaceCtrl(){
		return this._surfaceCtrl;
	};
function _globaljs_ctrl_isRequiredCheck(){
		if(this._vhIsRequired==false&&this._getSurfaceCtrlValue().length==0)
			return false;
		return true;
	};
function _globaljs_ctrl_confirmLabelState(ctrl,css){
		if(ctrl._vhLabelId){
			var lobj=ctrl.page.eval(ctrl._vhLabelId);
			lobj.className=_globaljs.__cssPref+"Label"+css;
		}
	};
function _globaljs_ctrl_confirmState(){
		if(this._vhIsDisabled==true){
			_globaljs_ctrl_confirmLabelState(this,"Normal");
			this._onSetDisabledState(_globaljs.__cssPref+this._ctrlClass+"Disabled");	
		}else if(this._vhIsError==true){
			_globaljs_ctrl_confirmLabelState(this,"Error");
			this._onSetErrorState(_globaljs.__cssPref+this._ctrlClass+"Error");
		}else if(this._vhIsRequired==true){
			_globaljs_ctrl_confirmLabelState(this,"Normal");
			this._onSetNormalState(_globaljs.__cssPref+this._ctrlClass+"Required");
		}else{
			_globaljs_ctrl_confirmLabelState(this,"Normal");
			this._onSetNormalState(_globaljs.__cssPref+this._ctrlClass+"Normal");
		}
	}
function _globaljs_ctrl_initState(){
		this._vhIsDisplay=_globaljs.rstr2bool(this._vhIsDisplay);
		this._vhIsDisabled=_globaljs.str2bool(this._vhIsDisabled);
		this._vhIsError=_globaljs.str2bool(this._vhIsError);
		this._vhIsRequired=_globaljs.str2bool(this._vhIsRequired);
		this._vhIsDefault=_globaljs.str2bool(this._vhIsDefault);
		this._vhErrorMsg=null;
		if(this._vhTipMsg){}else{
				this._vhTipMsg="";
		};
		if(this._vhIsDisplay==false){
			this.style["display"]="none";	
		}
		this._confirmState();
	};
function _globaljs_ctrl_vhSetEnabled(en){
		this._vhIsDisabled=!en;
		this._confirmState();
	};
function _globaljs_ctrl_vhIsEnabled(){
		return !this._vhIsDisabled;
	};
function _globaljs_ctrl_vhSetRequired(en){
		this._vhIsRequired=en;
		this._confirmState();
	};
function _globaljs_ctrl_vhIsRequired(){
		return this._vhIsRequired;
	};
function _globaljs_ctrl_vhSetFocus(){
		if(this._vhIsDisabled==true){
			return;
		}
		this._surfaceCtrl.focus();
		this._setFocus(this);
		this._dispatchEvent("focus",null);
	}
function _globaljs_ctrl__SetFocus(srcElem){
		if(this._vhIsDisabled==true){
			return;
		}
		if(this.page._setLastCtrlLostFocus(this)==true){
			this._onSetFocusState(_globaljs.__cssPref+this._ctrlClass+"Focus");
			_globaljs._displayErrorMsg(this);
		}
	}
function _globaljs_ctrl_vhGetValue(){
		return this._vhValue;
	}
function _globaljs_ctrl_vhSetValue(v){
		this._vhValue=this._surfaceCtrl.value=v;
	}
function _globaljs_ctrl_vhCheckValue(showError){
		this._onCheckValue();
		if(this._vhIsError==true){
			if(showError)
				if(showError==true)
					alert(this._vhErrorMsg);
			return false;
		}else
			return true;
	}
function _globaljs_ctrl_vhSetPreCtrl(nextCtrl){
		this._vhPreTabCtrl=nextCtrl;
	}
function _globaljs_ctrl_vhSetNextCtrl(nextCtrl){
		this._vhNextTabCtrl=nextCtrl;
	}
function _globaljs_ctrl_vhDestroy(){
		this._dispatchEvent("destroy",null);
		this.parentNode.removeChild(this);
	};
_globaljs._initCtrlMethod=function(page,ctrl,ctrlClass,ctrlType,evtStr){
		//_globaljs._checkAttrError(page,ctrl);
		if(evtStr){}else{
			evtStr="update,focus,blur,destroy,nextctrl,prectrl";
		}
		_globaljs._initCtrlAttribute(page,ctrl,ctrlClass,ctrlType);
		_globaljs._initCtrlEventFun(page,ctrl,evtStr);
		ctrl._surfaceCtrl=null;
		ctrl._onCheckValueFuns=[];
		///可输入控件五种状态
		ctrl._onSetNormalState=_globaljs_ctrl_onSetNormalState;
		ctrl._onSetDisabledState=_globaljs_ctrl_onSetDisabledState;
		ctrl._onSetErrorState=_globaljs_ctrl_onSetErrorState;
		ctrl._onSetRequiredState=_globaljs_ctrl_onSetRequiredState;
		ctrl._onSetFocusState=_globaljs_ctrl_onSetFocusState;
		///不可输入控件两种状态
		ctrl._onSetMouseOverState=_globaljs_ctrl_onSetMouseOverState;
		
		ctrl._onCheckValue=_globaljs_ctrl_onCheckValue;	
		ctrl._setMouseOver=_globaljs_ctrl_setMouseOver;
		ctrl._setMouseOut=_globaljs_ctrl_setMouseOut;
		ctrl._onKeyLoseFocus=_globaljs_ctrl_onKeyLoseFocus;
		ctrl._setLoseFocus=_globaljs_ctrl_setLoseFocus;
		ctrl._setFocus=_globaljs_ctrl__SetFocus;
		ctrl._onkeydown=_globaljs_ctrl_onkeydown;
		ctrl._onkeyup=_globaljs_ctrl_onkeyup;
		ctrl._dokeydown=_globaljs_ctrl_dokeydown;
		ctrl._dokeyup=_globaljs_ctrl_dokeyup;
		ctrl._getSurfaceCtrlValue=_globaljs_ctrl_getSurfaceCtrlValue;
		ctrl._getSurfaceCtrl=_globaljs_ctrl_getSurfaceCtrl;
		ctrl._isRequiredCheck=_globaljs_ctrl_isRequiredCheck;
		ctrl._confirmState=_globaljs_ctrl_confirmState;
		ctrl._initState=_globaljs_ctrl_initState;
		ctrl.vhDestroy=_globaljs_ctrl_vhDestroy;
		ctrl.vhSetEnabled=_globaljs_ctrl_vhSetEnabled;
		ctrl.vhIsEnabled=_globaljs_ctrl_vhIsEnabled;
		ctrl.vhSetRequired=_globaljs_ctrl_vhSetRequired;
		ctrl.vhIsRequired=_globaljs_ctrl_vhIsRequired;
		ctrl.vhSetFocus=_globaljs_ctrl_vhSetFocus;
		ctrl.vhGetValue=_globaljs_ctrl_vhGetValue;
		ctrl.vhSetValue=_globaljs_ctrl_vhSetValue;
		ctrl.vhCheckValue=_globaljs_ctrl_vhCheckValue;
		ctrl.vhSetPreCtrl=_globaljs_ctrl_vhSetPreCtrl;
		ctrl.vhSetNextCtrl=_globaljs_ctrl_vhSetNextCtrl;
	};
_globaljs._initCtrlAPI=function(page,ctrl){
		ctrl.addEventListener=function(evt,fun){this._ctrl.vhAddListener(evt,fun);};
		ctrl.removeEventListener=function(evt,fun){this._ctrl.vhRemoveListener(evt,fun);};
		ctrl.destroy=function(){this._ctrl.vhDestroy();};
		ctrl.setEnabled=function(en){this._ctrl.vhSetEnabled(en);};
		ctrl.isEnabled=function(){return this._ctrl.vhIsEnabled();};
		ctrl.setRequired=function(r){this._ctrl.vhSetRequired(r);};
		ctrl.isRequired=function(){return this._ctrl.vhIsRequired();};
		ctrl.setFocus=function(){this._ctrl.vhSetFocus();};
		ctrl.getValue=function(){return this._ctrl.vhGetValue();};
		ctrl.setValue=function(v){this._ctrl.vhSetValue(v);};
		ctrl.checkValue=function(){return this._ctrl.vhCheckValue();};
		ctrl.setPreCtrl=function(c){this._ctrl.vhSetPreCtrl(c);};
		ctrl.setNextCtrl=function(c){this._ctrl.vhSetNextCtrl(c);};
	};
_globaljs._initSurfaceCtrl=function(ctrl,surfaceCtrl,useMouseOver){
		ctrl._surfaceCtrl=surfaceCtrl;
		ctrl._surfaceCtrl._boxCtrl=ctrl;
		if(useMouseOver==true){
			ctrl._surfaceCtrl.onmouseover=function(){this._boxCtrl._setMouseOver()};
			ctrl._surfaceCtrl.onmouseout=function(){this._boxCtrl._setMouseOut()};
		}else{
			//ctrl._surfaceCtrl.onfocus=function(){this._boxCtrl.vhSetFoucs()};
			ctrl._surfaceCtrl.attachEvent("onfocus",function(){surfaceCtrl._boxCtrl._setFocus(this)});
			ctrl._surfaceCtrl.attachEvent("onkeydown",function(){surfaceCtrl._boxCtrl._onkeydown(surfaceCtrl._boxCtrl.page._ctrl.event)});
			ctrl._surfaceCtrl.attachEvent("onkeyup",function(){surfaceCtrl._boxCtrl._onkeyup(surfaceCtrl._boxCtrl.page._ctrl.event)});
		}
		//ctrl._surfaceCtrl.
	};
_globaljs._checkAttrError=function(page,ctrl){
		if(ctrl._vhAttError&&ctrl._vhAttError.length>0){
			page.msgBox("程序BUG："+ctrl.id.substr(0,ctrl.id.length-_globaljs._ctrlIdSubfixLen)+"\n"+ctrl._vhAttError.replace(";:;","\n"));	
			return true;
		}
		return false;
	};
_globaljs._checkCtrlValue=function(ctrl){
		ctrl._vhIsError=false;
		var funs=ctrl._onCheckValueFuns;
		if(ctrl._vhOnCheckValue){
			var fu=ctrl.page.eval(ctrl._vhOnCheckValue);
			funs.push(fu);
		}
		for(var i=0;i<funs.length;i++){
			ctrl._vhErrorMsg=funs[i](ctrl);
			if(ctrl._vhErrorMsg!=null){
				ctrl._vhIsError=true;
				return;
			}
		}
	};