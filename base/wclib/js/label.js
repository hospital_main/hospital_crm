/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createLabel=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("label","_initLabelMethod",page,id,parentObj,props,cb);
	};
_globaljs._initLabelMethod=function(page,ctrl){
		this._initCtrlMethod(page,ctrl,"label",_globaljs.CTRL_TYPE_NULL);
		ctrl.vhSetText=_globaljs_label_vhSetText;
		ctrl._vhIsColon=_globaljs.str2bool(ctrl._vhIsColon);
		_globaljs._initLabelAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initLabelAPI=function(page,ctrl){
		_globaljs._initCtrlAPI(page,ctrl);
		ctrl.setText=function(t){this._ctrl.vhSetText(t);};
	};
function _globaljs_label_vhSetText(t){
		this._vhValue=t;
		if(this._vhIsColon==true&&t.length>0)
			t+="��";
		this.page._hctrl(this.id+"_box").innerText=t;
	};