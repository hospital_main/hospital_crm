/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createDialogSeed=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("dialogseed","_initDialogSeedMethod",page,id,parentObj,props,cb);
	};
_globaljs.createDialog=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("dialog","_initDialogMethod",page,id,parentObj,props,cb);
	};
_globaljs._initDialogSeedMethod=function(page,ctrl){
		_globaljs._initCtrlAttribute(page,ctrl,"dialogseed",_globaljs.CTRL_TYPE_VALUE);
		_globaljs._initCtrlEventFun(page,ctrl,"destroy");
		ctrl.vhDestroy=_globaljs_ctrl_vhDestroy;
		ctrl.vhOpen=_globaljs_dialogseed_vhOpen;
		_globaljs._initDialogSeedAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initDialogSeedAPI=function(page,ctrl){
		ctrl.destroy=function(){this._ctrl.vhDestroy();};
		ctrl.open=function(page,src,props,closeCb){this._ctrl.vhOpen(page,src,props,closeCb);};
	};
_globaljs._initDialogMethod=function(page,ctrl){
		_globaljs._initCtrlAttribute(page,ctrl,"dialog",_globaljs.CTRL_TYPE_VALUE);
		_globaljs._initCtrlEventFun(page,ctrl,"destroy");
		ctrl.vhDestroy=_globaljs_ctrl_vhDestroy;
		ctrl.vhOpen=_globaljs_dialog_vhOpen;
		ctrl.vhReload=_globaljs_dialog_vhReload;
		ctrl._vhCreated=false;
		_globaljs._initDialogAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initDialogAPI=function(page,ctrl){
		ctrl.destroy=function(){this._ctrl.vhDestroy();};
		ctrl.open=function(page,src,props,closeCb){this._ctrl.vhOpen(page,src,props,closeCb);};
		ctrl.reload=function(src){this._ctrl.vhReload(src);};
	};
function _globaljs_dialogseed_vhOpen(page,src,props,closeCb){
		if(props){}else{
			props={};
		}
		props["src"]=src;
		_globaljs.createDialog(page,null,this.page._ctrlByHid(this.id),props,function(ctrl){
			ctrl.open(page,src,props,function(ctrl){
				var dlgId=ctrl.id+_globaljs._ctrlIdSubfix;
				ctrl.destroy();
				if(closeCb)
					closeCb();
				_globaljs_dialogseed_clearMap(page,dlgId);
			});
		});
	};
function _globaljs_dialogseed_clearMap(page,dlgId){
		if(page._dlgMap[dlgId]){
			page._dlgMap[dlgId]=null;	
		}
	};
var _globaljs_dialog_index=100;
function _globaljs_dialog_vhOpen(page,src,props,closeCb){
		if(src){}else{
			src=this._vhSrc;
		}
		this._vhSrc=src;
		var idx=this.id;
		var dlg={};
		dlg.dlg=this;
		dlg.closeCb=null;
		if(closeCb)
			dlg.closeCb=closeCb;
		page._dlgMap[""+idx]=dlg;
		//if(this._vhDlgSrc&&this._vhDlgSrc==src){
		//	page._hctrl(this.id+"_box").style.display="block";
		//	return;
		//}
		this._vhDlgSrc=src;
		_globaljs_dialog_create(this.page,this);
		this.style["zIndex"]=""+_globaljs_dialog_index++;
		if(src.indexOf("?")>0){
			src+="&"+_globaljs._dialogIdKey+"="+idx;
		}else{
			src+="?"+_globaljs._dialogIdKey+"="+idx;
		};
		
		page._ctrl.eval(this.id+"_iframe").document.location.href=src;
		var box=page._hctrl(this.id+"_box");
		var dlgW=500;
		var dlgH=400;
		var chide=false;
		if(props){
			if(props["width"]){
				dlgW=props["width"];
			}
			if(props["height"]){
				dlgW=props["height"];
			}
			if(props["maxsize"]&&props["maxsize"]=="yes"){
				dlgW=page._ctrl.document.body.clientWidth-15;
				dlgH=page._ctrl.document.body.clientHeight-15;
			}
			if(props["close"]&&props["close"]=="no"){
				chide=true;
			}
			if(props["title"]){
				page._hctrl(this.id+"_title").innerText=props["title"];	
			}
		}
		if(dlgW>page._ctrl.document.body.clientWidth-15)
			dlgW=page._ctrl.document.body.clientWidth-15;
		if(dlgH>page._ctrl.document.body.clientHeight-15)
			dlgH=page._ctrl.document.body.clientHeight-15;
		page._hctrl(this.id+"_close").style.display=chide==true?"none":"";
		box.style.top=(page._ctrl.document.body.clientHeight-dlgH)/2;
		box.style.left=(page._ctrl.document.body.clientWidth-dlgW)/2;
		box.style.width=dlgW;
		box.style.height=dlgH;
	};
function _globaljs_dialog_create(page,ctrl){
		if(ctrl._vhCreated)
			return;
		ctrl._vhCreated=true;
		var did=ctrl.id;
		var imgPre=_globaljs.getStyleFilePath("images/");
		var htmlStr="<table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td align=\"center\" valign=\"middle\" ><table id=\""+did+"_box\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"position:absolute;top:1;left:1;width:20;Height:20\" onmousedown=\"_globaljs._dialog_drag_down(page,"+did+",this,event)\">";
		htmlStr+="	<tr>";
		htmlStr+="		<td id=\""+did+"_top\" colspan=\"3\" style=\"cursor:default\">";
		htmlStr+="			<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		htmlStr+="				<tr>";
		htmlStr+="					<td width=\"1\" ><img src=\""+imgPre+"dlg_lefttop.gif\"></td>";
		htmlStr+="					<td width=\"100%\" background=\""+imgPre+"dlg_titlebg.gif\"><table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style='table-layout:fixed'><tr><td valign='middle' noWrap='true' id=\""+did+"_title\" align=\"left\" valign=\"middle\" style=\"width:90%;height:18;font-weight:bold; color:#FFFFFF;\" onselectstart ='return false'></td></tr></table></td>";
		htmlStr+="					<td width=\"1\"  background=\""+imgPre+"dlg_titlebg.gif\"><img id=\""+did+"_close\" src=\""+imgPre+"dlg_close.gif\" onclick=\"_globaljs._dialog_closeByIndex(page,"+did+")\"  style=\"cursor:hand\"></td>";
		htmlStr+="					<td width=\"1\" ><img src=\""+imgPre+"dlg_righttop.gif\" ></td>";
		htmlStr+="				</tr>";
		htmlStr+="			</table>";
		htmlStr+="		</td>";
		htmlStr+="		</tr>";
		htmlStr+="		<tr>";
		htmlStr+="		<td background=\""+imgPre+"dlg_left.gif\"></td>";
		htmlStr+="		<td width=\"100%\" height=\"100%\"><iframe id=\""+did+"_iframe\" src=\"\" width=\"100%\" height=\"100%\"  frameborder=\"0\" scrolling=\"auto\"></iframe></td>";
		htmlStr+="		<td background=\""+imgPre+"dlg_right.gif\"></td>";
		htmlStr+="		</tr>";
		htmlStr+="		<tr>";
		htmlStr+="		<td><img src=\""+imgPre+"dlg_leftbottom.gif\" ></td>";
		htmlStr+="		<td background=\""+imgPre+"dlg_bottombg.gif\"></td>";
		htmlStr+="		<td><img id=\""+did+"_resize\" src=\""+imgPre+"dlg_rightbottom.gif\" ";
		htmlStr+="	></td>";
		htmlStr+="	</tr>";
		htmlStr+="</table></td></tr></table>";
		ctrl.innerHTML=htmlStr;
	};
function _globaljs_dialog_vhReload(){
		
	};
_globaljs._dialog_closeByIndex=function(page,dlg){
		dlg.style.dispaly="none";
		page._hctrl(dlg.id+"_box").style.display="none";
		if(page._dlgMap[""+dlg.id]){
			if(page._dlgMap[""+dlg.id]["closeCb"])
				page._dlgMap[""+dlg.id]["closeCb"](page._ctrlByHid(dlg.id));	
		}
	};
_globaljs._dialog_setTitle=function(page,ti){
		if(page._ctrl[_globaljs._dialogIdKey]){
			if(page._ctrl.parent.page._dlgMap[page._ctrl[_globaljs._dialogIdKey]]){
				var dlg=page._ctrl.parent.page._dlgMap[page._ctrl[_globaljs._dialogIdKey]];
				page._ctrl.parent.page._hctrl(dlg.dlg.id+"_title").innerText=ti;
			}	
		}
	};
_globaljs._dialog_parsePropsFromPage=function(page,props){
		
	};
var _globaljs_dialog_drag_page=null,_globaljs_dialog_drag_dlg=null,_globaljs_dialog_drag_obj=null,_globaljs_dialog_drag_x=0,_globaljs_dialog_drag_y=0,_globaljs_dialog_drag_w=0,_globaljs_dialog_drag_h=0;
_globaljs._dialog_drag_down=function(page,dlg,obj,evt){
	var t=page._hctrl(dlg.id+"_top");
	if(!t.contains(evt.srcElement))
		return;
	_globaljs_dialog_drag_page=page;
	_globaljs_dialog_drag_dlg=dlg;
	_globaljs_dialog_drag_obj=obj;
	_globaljs_dialog_drag_x=evt.x-obj.style.pixelLeft;
	_globaljs_dialog_drag_y=evt.y-obj.style.pixelTop;
	page._ctrl.document.attachEvent("onmousemove",_globaljs._dialog_drag_move);
	page._ctrl.document.attachEvent("onmouseup",_globaljs._dialog_drag_up);
	obj.setCapture();
}
_globaljs._dialog_drag_move=function(){
	if(_globaljs_dialog_drag_obj!=null){
		var x=_globaljs_dialog_drag_page._ctrl.event.x-_globaljs_dialog_drag_x;
		var y=_globaljs_dialog_drag_page._ctrl.event.y-_globaljs_dialog_drag_y;
		if(x<0)
			x=0;
		if(x>_globaljs_dialog_drag_page._ctrl.document.body.clientWidth)
			x=_globaljs_dialog_drag_page._ctrl.document.body.clientWidth;
		if(y<0)
			y=0;
		if(y>_globaljs_dialog_drag_page._ctrl.document.body.clientHeight)
			x=_globaljs_dialog_drag_page._ctrl.document.body.clientHeight;
		_globaljs_dialog_drag_obj.style.left=x;
		_globaljs_dialog_drag_obj.style.top=y;
	}
}
_globaljs._dialog_drag_up=function(){
	if(_globaljs_dialog_drag_obj!=null){
		_globaljs_dialog_drag_obj.releaseCapture();
		_globaljs_dialog_drag_page._ctrl.document.detachEvent("onmousemove",_globaljs._dialog_drag_move);
		_globaljs_dialog_drag_page._ctrl.document.detachEvent("onmouseup",_globaljs._dialog_drag_up);
		_globaljs_dialog_drag_page=null;
		_globaljs_dialog_drag_dlg=null;
		_globaljs_dialog_drag_obj=null;
	}
}
