/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createCombobox=function(page,id,parentObj,props,cb){
		if(props["data"])
			props["data"]=_globaljs.getRealExpression(page,props["data"]);
		this._sendAndCreateCtrl("select","_initComboboxMethod",page,id,parentObj,props,cb);
	};
_globaljs._initComboboxMethod=function(page,ctrl){
		_globaljs._initCtrlMethod(page,ctrl,"ComboBox",_globaljs.CTRL_TYPE_VALUE);
		_globaljs_basecombobox_initMethod(page,ctrl);
		_globaljs_basecombobox_initListBoxMethod(page,ctrl);
		_globaljs._initSurfaceCtrl(ctrl,page._hctrl(ctrl.id+"_Input_"),false);
		
		ctrl._vhOnInputChar=_globaljs_combobox__onInputChar;
		ctrl.vhSetValue=_globaljs_combobox_vhSetValue;
		ctrl.vhSetData=_globaljs_combobox_vhSetData;
		ctrl._doShowList=_globaljs_combobox__doShowList;
		ctrl._onCheckValue=_globaljs_select__onCheckValue;
		ctrl._onCheckValueFuns=[
				_globaljs_text_checkRequired,
				_globaljs_text_checkMinValue,
				_globaljs_text_checkMaxValue,
				_globaljs_text_checkCompareValue
			];
		ctrl._listBoxDefaultHeight=200;
		ctrl._vhYearValue="";
		ctrl._vhMonthValue="";
		ctrl._vhDateValue="";
		ctrl._addUpdateListener=_globaljs_select_addUpdateListener;
		ctrl._delUpdateListener=_globaljs_select_delUpdateListener;
		_globaljs_combobox_vhSetValueCb(ctrl,ctrl._vhValue);
		ctrl._initState();
		_globaljs._initComboboxAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initComboboxAPI=function(page,ctrl){
		_globaljs._initCtrlAPI(page,ctrl);
		ctrl.setData=function(v){this._ctrl.vhSetData(v);};
	};
function _globaljs_combobox_vhSetValue(v,cb){
		this._vhSetValue=v;
		this._vhValue="";
		this._getSurfaceCtrl().value="";
		var ctrl=this;
		this._vhOnInputChar(v,function(){
				_globaljs_combobox_vhSetValueCb(ctrl,v);
				if(cb)
					cb();
			});
	};
function _globaljs_combobox_vhSetValueCb(ctrl,v){
		_globaljs_bscombobox__selectItem(ctrl,null);
		var tr=ctrl.page._hctrl(ctrl.id+"_i"+v);
		if(tr==null)
			return;
		var str=_globaljs_bscombobox__formateItem(ctrl,tr);
		ctrl._vhValue=v;
		ctrl._getSurfaceCtrl().value=str;
		_globaljs_bscombobox__selectItem(ctrl,tr);
	};
function _globaljs_combobox__doShowList(){
		var fi=_globaljs_bscombobox__getFirstTr(this);
		if(this._vhIsRequired==false)
			fi.style.display="";
		else
			fi.style.display="none";
		this._showHideListBox(true);
		if(this._vhListWidth)
			this._getListBox().style.width=this._vhListWidth;
		_globaljs_select__reSetFocus(this);
	};
function _globaljs_combobox_vhSetData(data){
		this._vhData=data;
		this._vhOldValue=this._vhValue=this._getSurfaceCtrl().value="";
	}
function _globaljs_combobox__onInputChar(value,cb){
		if(this._vhRealData){}else{
			this._vhRealData=_globaljs.getRealExpression(this.page,this._vhData);
		}
		var data=this._vhRealData;
		var obj={};
		var ctrl=this;
		obj["_cb_py_v_"]=value;
		obj["data"]=data;
		obj["id"]=this.id;
		obj["format"]=this._vhFormat;
		obj["maxrows"]=this._vhMaxRows;
		var getPara={};
		getPara[_globaljs.getBinActionKey()]=_globaljs.getBinNewContentKey();
		_globaljs.sendRequest(_globaljs.getPageBinName(this.page,"combobox"),_globaljs.objToRequeatStr(obj),function(text,xml){
				_globaljs_combobox__onInputCharCb(ctrl,text,xml);
				if(cb)
					cb();
			},_globaljs.objToUrlcode(getPara));
	}
function _globaljs_combobox__onInputCharCb(ctrl,text,xml){
		ctrl._getListBox().innerHTML=text;
		_globaljs_select__showHideFirst(ctrl);
		var item=_globaljs_bscombobox__findItemByKey(ctrl,ctrl._getSurfaceCtrl().value);
		_globaljs_bscombobox__selectItem(ctrl,item);
	}