/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createBorderLayout=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("borderlayout","_initBorderLayoutMethod",page,id,parentObj,props,cb);
	};
_globaljs._initBorderLayoutMethod=function(page,ctrl){
		_globaljs._initCtrlAttribute(page,ctrl,"bordrlayout",_globaljs.CTRL_TYPE_NULL);
		ctrl.vhGetTopBox=_globaljs_bl_vhGetTopBox;
		ctrl.vhGetLeftBox=_globaljs_bl_vhGetLeftBox;
		ctrl.vhGetCenterBox=_globaljs_bl_vhGetCenterBox;
		ctrl.vhGetRightBox=_globaljs_bl_vhGetRightBox;
		ctrl.vhGetBottomBox=_globaljs_bl_vhGetBottomBox;
		_globaljs._initBorderLayoutAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initBorderLayoutAPI=function(page,ctrl){
		_globaljs._initCtrlAPI(page,ctrl);
		ctrl.getTopBox=function(){return this._ctrl.vhGetTopBox();};
		ctrl.getLeftBox=function(){return this._ctrl.vhGetLeftBox();};
		ctrl.getCenterBox=function(){return this._ctrl.vhGetCenterBox();};
		ctrl.getRightBox=function(){return this._ctrl.vhGetRightBox();};
		ctrl.getBottomBox=function(){return this._ctrl.vhGetBottomBox();};
	};
function _globaljs_bl_vhGetTopBox(){return this.page._hctrl(this.id+"_topbox");};
function _globaljs_bl_vhGetLeftBox(){return this.page._hctrl(this.id+"_leftbox");};
function _globaljs_bl_vhGetCenterBox(){return this.page._hctrl(this.id+"_centerbox");};
function _globaljs_bl_vhGetRightBox(){return this.page._hctrl(this.id+"_rightbox");};
function _globaljs_bl_vhGetBottomBox(){return this.page._hctrl(this.id+"_bottombox");};