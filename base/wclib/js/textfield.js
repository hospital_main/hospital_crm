/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createTextField=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("textfield","_initTextFieldMethod",page,id,parentObj,props,cb);
	};
_globaljs._initTextFieldMethod=function(page,ctrl){
		ctrl.value=ctrl._vhOldValue=ctrl._vhValue;
		this._initCtrlMethod(page,ctrl,"textfield",_globaljs.CTRL_TYPE_VALUE);
		_globaljs._initSurfaceCtrl(ctrl,ctrl,false);
		ctrl._initState();
		
		ctrl.vhGetRegExp=_globaljs_text_vhGetRegExp;
		ctrl.vhSetRegExp=_globaljs_text_vhSetRegExp;
		ctrl.vhGetMinLength=_globaljs_text_vhGetMinLength;
		ctrl.vhSetMinLength=_globaljs_text_vhSetMinLength;
		ctrl.vhGetMaxLength=_globaljs_text_vhGetMaxLength;
		ctrl.vhSetMaxLength=_globaljs_text_vhSetMaxLength
		ctrl.vhGetMinValue=_globaljs_text_vhGetMinValue;
		ctrl.vhSetMinValue=_globaljs_text_vhSetMinValue;
		ctrl.vhGetMaxValue=_globaljs_text_vhGetMaxValue;
		ctrl.vhSetMaxValue=_globaljs_text_vhSetMaxValue;
		ctrl.vhSetCompare=_globaljs_text_vhSetCompare;
		ctrl._onCheckValue=_globaljs_text__onCheckValue;
		ctrl._onCheckValueFuns=[
				_globaljs_text_checkRequired,
				_globaljs_text_checkRegExp,
				_globaljs_text_checkMinLength,
				_globaljs_text_checkMaxLength,
				_globaljs_text_checkMinValue,
				_globaljs_text_checkMaxValue,
				_globaljs_text_checkCompareValue
			];
		_globaljs._initTextFieldAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initTextFieldAPI=function(page,ctrl){
		_globaljs._initCtrlAPI(page,ctrl);
		ctrl.getRegExp=function(){return this._ctrl.vhGetRegExp();};
		ctrl.setRegExp=function(re){this._ctrl.vhSetRegExp(re);};
		ctrl.getMinLength=function(){return this._ctrl.vhGetMinLength();};
		ctrl.setMinLength=function(l){this._ctrl.vhSetMinLength(l);};
		ctrl.getMaxLength=function(){return this._ctrl.vhGetMaxLength();};
		ctrl.setMaxLength=function(l){this._ctrl.vhSetMaxLength(l);};
		ctrl.getMinValue=function(){return this._ctrl.vhGetMinValue();};
		ctrl.setMinValue=function(v){this._ctrl.vhSetMinValue(v);};
		ctrl.getMaxValue=function(){return this._ctrl.vhGetMaxValue();};
		ctrl.setMaxValue=function(v){this._ctrl.vhSetMaxValue(v);};
		ctrl.setCompare=function(v){this._ctrl.vhSetCompare(v);};
	};
	
////text api begin
function _globaljs_text_vhGetRegExp(){return this._vhRegexp;};
function _globaljs_text_vhSetRegExp(re){this._vhRegexp=re;};
function _globaljs_text_vhGetMinLength(){return this._vhMinLength;};
function _globaljs_text_vhSetMinLength(ml){this._vhMinLength=ml;};
function _globaljs_text_vhGetMaxLength(){return this._vhMaxLength;};
function _globaljs_text_vhSetMaxLength(ml){this._vhMaxLength=ml;};
function _globaljs_text_vhGetMinValue(){return this._vhMinValue;};
function _globaljs_text_vhSetMinValue(mv){this._vhMinValue=mv;};
function _globaljs_text_vhGetMaxValue(){return this._vhMaxValue;};
function _globaljs_text_vhSetMaxValue(mv){this._vhMaxValue=mv;};
function _globaljs_text_vhSetCompare(comp){this._vhCompare=comp;};
function _globaljs_text__onCheckValue(){
		this._vhValue=this._surfaceCtrl.value=this._surfaceCtrl.value.replace(/(^\s+)|\s+$/g, "");
		if(this._vhCharCase){
			if(this._vhCharCase=="lower")
				this._vhValue=this._surfaceCtrl.value=this._vhValue.toLowerCase();
			else
				this._vhValue=this._surfaceCtrl.value=this._vhValue.toUpperCase();
		}
		_globaljs._checkCtrlValue(this);
		if(this._vhIsError==false&&this._vhOldValue!=this._vhValue){
			this._dispatchEvent("update",null);
			this._vhOldValue=this._vhValue;
		}
	};
///text check begin
function _globaljs_text_checkRequired(ctrl){
		if(ctrl._vhIsRequired==false){
			return null;
		}
		if(ctrl._getSurfaceCtrlValue().length==0)
			return "必输"+ctrl._vhTipMsg;
		else
			return null;
	};
function _globaljs_text_checkRegExp(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		if(ctrl._vhRegexp){}else{
			return null;
		}
		var re=new RegExp(ctrl._vhRegexp);
		if(re.test(ctrl._getSurfaceCtrlValue())==false)
			return ctrl._vhTipMsg;
		else
			return null;
	};
function _globaljs_text_checkMinLength(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		if(ctrl._vhMinLength){}else{
			return null;
		}
		if(ctrl._getSurfaceCtrlValue().length<parseInt(ctrl._vhMinLength,10))
			return "字符数不能少于"+ctrl._vhMinLength+"个";
		else
			return null;
	};
function _globaljs_text_checkMaxLength(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		if(ctrl._vhMaxLength){}else{
			return null;
		}
		if(ctrl._getSurfaceCtrlValue().length>parseInt(ctrl._vhMaxLength,10))
			return "字符数不能超过"+ctrl._vhMaxLength+"个";
		else
			return null;
	};
function _globaljs_text_checkMinValue(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		if(ctrl._vhMinValue){}else{
			return null;
		}
		if(ctrl._getSurfaceCtrlValue()<ctrl._vhMinValue)
			return "不能小于"+ctrl._vhMinValue;
		else
			return null;
	};
function _globaljs_text_checkMaxValue(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		if(ctrl._vhMinValue){}else{
			return null;
		}
		if(ctrl._getSurfaceCtrlValue()<ctrl._vhMinValue)
			return "不能大于"+ctrl._vhMinValue;
		else
			return null;
	};
function _globaljs_text_checkCompareValue(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		if(ctrl._vhCompare){}else{
			return null;
		}
		var cl=1;
		if(ctrl._vhCompare.length>2){
			var pc=ctrl._vhCompare.substr(0,2);
			if(pc==">="||pc=="<=")
				cl=2;
		}
		var cc=ctrl._vhCompare.substr(0,cl);
		var cv=ctrl._vhCompare.substr(cl);
		var ct=ctrl.page._hctrl(cv);
		if(ct==null)
			return "程序BGU:比较的控件不存在:"+cv;
		cv=ct.vhGetValue();
		if(cc==">"&&ctrl._getSurfaceCtrlValue()<=cv){
			return "必须大于"+cv;
		}else if(cc==">="&&ctrl._getSurfaceCtrlValue()<cv){
			return "必须大于等于"+cv;
		}else if(cc=="="&&ctrl._getSurfaceCtrlValue()!=cv){
			return "必须等于"+cv;
		}else if(cc=="<"&&ctrl._getSurfaceCtrlValue()>=cv){
			return "必须小于"+cv;
		}else if(cc=="<="&&ctrl._getSurfaceCtrlValue()>cv){
			return "必须小于等于"+cv;
		}else
			return null;
	};