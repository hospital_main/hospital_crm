/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createNumber=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("number","_initNumberMethod",page,id,parentObj,props,cb);
	};
_globaljs._initNumberMethod=function(page,ctrl){
		ctrl.value=ctrl._vhValue;
		this._initCtrlMethod(page,ctrl,"number",_globaljs.CTRL_TYPE_VALUE);
		_globaljs._initSurfaceCtrl(ctrl,ctrl,false);
		ctrl._initState();
		
		ctrl.vhGetMinValue=_globaljs_text_vhGetMinValue;
		ctrl.vhSetMinValue=_globaljs_text_vhSetMinValue;
		ctrl.vhGetMaxValue=_globaljs_text_vhGetMaxValue;
		ctrl.vhSetMaxValue=_globaljs_text_vhSetMaxValue;
		ctrl.vhSetCompare=_globaljs_text_vhSetCompare;
		ctrl._onCheckValue=_globaljs_text__onCheckValue;
		ctrl._onCheckValueFuns=[
				_globaljs_text_checkRequired,
				_globaljs_number_checkNumberValue,
				_globaljs_number_checkMinValue,
				_globaljs_number_checkMaxValue,
				_globaljs_number_checkCompareValue
			];
		
		
		_globaljs._initNumberAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initNumberAPI=function(page,ctrl){
		_globaljs._initCtrlAPI(page,ctrl);
		ctrl.getMinValue=function(){return this._ctrl.vhGetMinValue();};
		ctrl.setMinValue=function(v){this._ctrl.vhSetMinValue(v);};
		ctrl.getMaxValue=function(){return this._ctrl.vhGetMaxValue();};
		ctrl.setMaxValue=function(v){this._ctrl.vhSetMaxValue(v);};
		ctrl.setCompare=function(v){this._ctrl.vhSetCompare(v);};
	};

function _globaljs_number_checkNumberValue(ctrl){
		if(ctrl._vhIsRequired==false){
			return null;
		}
		var v=ctrl._getSurfaceCtrlValue();
		if(_globaljs.isNumber(ctrl._getSurfaceCtrlValue())==false)
			return "必须是数字";
		var dotIdx=v.indexOf(".");
		var scale=parseInt(ctrl._vhScale,10);
		if(dotIdx>=0&&scale==0){
			return "必须是整位";
		}
		if(dotIdx>=0&&(v.length-dotIdx)>scale){
			return "小数位数不能大于"+ctrl._vhScale;
		}
		return null;
	};
function _globaljs_number_checkMinValue(ctrl){
		if(ctrl._vhIsRequired==false){
			return null;
		}
		if(ctrl._vhMinValue){}else{
			return null;
		}
		var v=ctrl._getSurfaceCtrlValue();
		if(parseFloat(v)<parseFloat(ctrl._vhMinValue))
			return "不能小于"+ctrl._vhMinValue;
		return null;
	};
function _globaljs_number_checkMaxValue(ctrl){
		if(ctrl._vhIsRequired==false){
			return null;
		}
		if(ctrl._vhMaxValue){}else{
			return null;
		}
		var v=ctrl._getSurfaceCtrlValue();
		if(parseFloat(v)>parseFloat(ctrl._vhMaxValue))
			return "不能大于"+ctrl._vhMaxValue;
		return null;
	};
function _globaljs_number_checkCompareValue(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		if(ctrl._vhCompare){}else{
			return null;
		}
		var cl=1;
		if(ctrl._vhCompare.length>2){
			var pc=ctrl._vhCompare.substr(0,2);
			if(pc==">="||pc=="<=")
				cl=2;
		}
		var cc=ctrl._vhCompare.substr(0,cl);
		var cv=ctrl._vhCompare.substr(cl);
		var ct=ctrl.page._hctrl(cv);
		if(ct==null)
			return "程序BGU:比较的控件不存在:"+cv;
		cv=ct.vhGetValue();
		if(cc==">"&&parseFloat(ctrl._getSurfaceCtrlValue())<=parseFloat(cv)){
			return "必须大于"+cv;
		}else if(cc==">="&&parseFloat(ctrl._getSurfaceCtrlValue())<parseFloat(cv)){
			return "必须大于等于"+cv;
		}else if(cc=="="&&parseFloat(ctrl._getSurfaceCtrlValue())!=parseFloat(cv)){
			return "必须等于"+cv;
		}else if(cc=="<"&&parseFloat(ctrl._getSurfaceCtrlValue())>=parseFloat(cv)){
			return "必须小于"+cv;
		}else if(cc=="<="&&parseFloat(ctrl._getSurfaceCtrlValue())>parseFloat(cv)){
			return "必须小于等于"+cv;
		}else
			return null;
	};