/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createCalendar=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("calendar","_initCalendarMethod",page,id,parentObj,props,cb);
	};
_globaljs._initCalendarMethod=function(page,ctrl){
		_globaljs._initCtrlMethod(page,ctrl,"ComboBox",_globaljs.CTRL_TYPE_VALUE);
		_globaljs_basecombobox_initMethod(page,ctrl);
		_globaljs._initSurfaceCtrl(ctrl,page._hctrl(ctrl.id+"_Input_"),false);
		
		ctrl._parseValue=_globaljs_calendar__parseValue;
		ctrl._doShowList=_globaljs_calendar__doShowList;
		ctrl._createList=_globaljs_calendar__createList;
		ctrl._changeDate=_globaljs_calendar__changeDate;
		ctrl._onCheckValue=_globaljs_calendar__onCheckValue;
		ctrl._onCheckValueFuns=[
				_globaljs_text_checkRequired,
				_globaljs_calendar_checkDateValue,
				_globaljs_calendar_checkMinValue,
				_globaljs_calendar_checkMaxValue,
				_globaljs_calendar_checkCompareValue
			];
		ctrl._listBoxDefaultHeight=100;
		ctrl._vhYearValue="";
		ctrl._vhMonthValue="";
		ctrl._vhDateValue="";
		ctrl._parseValue(ctrl._vhValue);
		ctrl._vhOldValue=ctrl._vhValue;
		ctrl._selSubYear=_globaljs_calendar_selSubYear;
		ctrl._selSubMonth=_globaljs_calendar_selSubMonth;
		ctrl._selAddYear=_globaljs_calendar_selAddYear;
		ctrl._selAddMonth=_globaljs_calendar_selAddMonth;
		ctrl._selGetValue=_globaljs_calendar_selGetValue;
		ctrl._initState();
		_globaljs._initCalendarAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initCalendarAPI=function(page,ctrl){
		_globaljs._initCtrlAPI(page,ctrl);
	};

function _globaljs_calendar__parseValue(va){
		va=va.replace(/(^\s+)|\s+$/g, "");
		var v=_globaljs.parseDate(va,this._vhMonthValue,this._vhYearValue);
		if(v!=null){
			this._vhYearValue=v.getFullYear();
			this._vhMonthValue=v.getMonth()+1;
			this._vhDateValue=v.getDate();	
			this._vhValue=this._getSurfaceCtrl().value=_globaljs.formatDate(v,"-");
		}else{
			this._vhValue=this._getSurfaceCtrl().value="";
		}
	};
function _globaljs_calendar__onCheckValue(){
		this._tempDateObj=null;
		_globaljs._checkCtrlValue(this);
		if(this._vhIsError==true)
			;//this._getSurfaceCtrl().value=this._vhValue;
		else{
			this._parseValue(this._getSurfaceCtrl().value);
			if(this._vhOldValue!=this._vhValue){
				this._dispatchEvent("update",null);
				this._vhOldValue=this._vhValue;
			}
		}
	};
function _globaljs_calendar_checkDateValue(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		var v=_globaljs.parseDate(ctrl._getSurfaceCtrl().value);
		if(v==null)
			return "不是有效的日期值";
		ctrl._tempDateObj=v;
		return null;
	};
function _globaljs_calendar_checkMinValue(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		if(ctrl._vhMinValue){}else{
			return null;
		}
		var v=_globaljs.parseDate(ctrl._vhMinValue);
		if(v==null)
			return null;
		if(ctrl._tempDateObj.getTime()<v.getTime())
			return "不能小于"+ctrl._vhMinValue;
		return null;
	};
function _globaljs_calendar_checkMaxValue(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		if(ctrl._vhMaxValue){}else{
			return null;
		}
		var v=_globaljs.parseDate(ctrl._vhMaxValue);
		if(v==null)
			return null;
		if(ctrl._tempDateObj.getTime()>v.getTime())
			return "不能大于"+ctrl._vhaxValue;
		return null;
	};
function _globaljs_calendar_checkCompareValue(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		var cl=1;
		if(ctrl._vhCompare){}else{
			return null;
		}
		if(ctrl._vhCompare.length>2){
			var pc=ctrl._vhCompare.substr(0,2);
			if(pc==">="||pc=="<=")
				cl=2;
		}
		var cc=ctrl._vhCompare.substr(0,cl);
		var cv=ctrl._vhCompare.substr(cl);
		var ct=ctrl.page._hctrl(cv);
		if(ct==null)
			return "程序BGU:比较的控件不存在:"+cv;
		cv=_globaljs.parseDate(ct.vhGetValue());
		if(cv==null)
			return null;
		if(cc==">"&&ctrl._tempDateObj.getTime()<=cv.getTime()){
			return "必须大于"+ct.vhGetValue();
		}else if(cc==">="&&ctrl._tempDateObj.getTime()<cv.getTime()){
			return "必须大于等于"+ct.vhGetValue();
		}else if(cc=="="&&ctrl._tempDateObj.getTime()!=cv.getTime()){
			return "必须等于"+ct.vhGetValue();
		}else if(cc=="<"&&ctrl._tempDateObj.getTime()>=cv.getTime()){
			return "必须小于"+ct.vhGetValue();
		}else if(cc=="<="&&ctrl._tempDateObj.getTime()>cv.getTime()){
			return "必须小于等于"+ct.vhGetValue();
		}else
			return null;
	};
function _globaljs_calendar__doShowList(){
		this._showHideListBox(true);
		this._getListBox().style["overflow"]="visible";
		this._createList();
		_globaljs_calendar__reSetFocus(this);
	};
function _globaljs_calendar__reSetFocus(ctrl){
		ctrl._getSurfaceCtrl().focus();
		ctrl._getSurfaceCtrl().select();
	}
function _globaljs_calendar_selSubYear(){
		var myObj=this._getListBox().childNodes[0].cells[2].childNodes
		myObj[0].innerHTML=eval(myObj[0].innerHTML)-1;
		this._changeDate(eval(myObj[0].innerHTML),eval(myObj[2].innerHTML))
	};
function _globaljs_calendar_selSubMonth(){
		var myObj=this._getListBox().childNodes[0].cells[2].childNodes
		var month=eval(myObj[2].innerHTML)-1;
		if(month==0){
			month=12;
			this._selSubYear();
		}
		myObj[2].innerHTML=month;
		this._changeDate(eval(myObj[0].innerHTML),eval(myObj[2].innerHTML))
	};
function _globaljs_calendar_selAddYear(){
		var myObj=this._getListBox().childNodes[0].cells[2].childNodes
		myObj[0].innerHTML=eval(myObj[0].innerHTML)+1;
		this._changeDate(eval(myObj[0].innerHTML),eval(myObj[2].innerHTML))
	};
function _globaljs_calendar_selAddMonth(){
		var myObj=this._getListBox().childNodes[0].cells[2].childNodes
		var month=eval(myObj[2].innerHTML)+1;
		if(month==13){
			month=1;
			this._selAddYear();
		}
		myObj[2].innerHTML=month;
		this._changeDate(eval(myObj[0].innerHTML),eval(myObj[2].innerHTML))
	};
function _globaljs_calendar_selGetValue(obj){
		var myObj=this._getListBox().childNodes[0].cells[2].childNodes;
		var mday='00'+obj.innerHTML;
		var mmon='00'+myObj[2].innerHTML;
		mmon=mmon.substring(mmon.length-2);
		mday=mday.substring(mday.length-2);
		if(obj.innerHTML)
			this._vhValue=this._getSurfaceCtrl().value=myObj[0].innerHTML+"-"+mmon+"-"+mday;
		this._showHideListBox(false);
		_globaljs_calendar__reSetFocus(this);
	};
function _globaljs_calendar__createList(){
		var hstr="";
		///以下内容是从 calendar.htc中的 displayDate拷贝而来
		objList=this._getListBox();
		if(objList.__hasCreate){
			return;
		}
		objList.__hasCreate=true;
		var calObj=this;
		
		var option = (this._vhValue==''?_globaljs.formatDate(new Date(),"-"):this._vhValue).split("-");
		var week=new Array('日','一','二','三','四','五','六');
		
		//第一行，头
		table_1 = this.page._ctrl.document.createElement("table");
		with(table_1){
			cellpadding = 0;
			cellspacing = 1;
			style.width = 140;
			style.height = 20;
			style.backgroundColor = "#CEDAE7";
		}
		table_1 = objList.appendChild(table_1);
		tr = table_1.insertRow();
		tr.align = "center";
		//减小年份
		td = tr.insertCell();
		with(td){
			style.cursor = "hand";
			style.width = "11%";
			onmouseover = function(){className='ds_border'}
			onmouseout = function(){className=''}
			onclick = function(){calObj._selSubYear()}
			title = '减小年份';
			innerHTML = "&lt;&lt;";
		}
		//减小月份
		td = tr.insertCell();
		with(td){
			style.cursor = "hand";
			style.width = "11%";
			onmouseover = function(){className='ds_border'}
			onmouseout = function(){className=''}
			onclick = function(){calObj._selSubMonth()}
			title = '减小月份';
			innerHTML = "&lt;"
		}
		//编辑年月
		td = tr.insertCell();
		td.width = "56%";
		td.noWrap = "true";
		try{
			b = this.page._ctrl.document.createElement("<b id='yearValue'></b>");
			b.innerText = option[0];//xujm
			td.appendChild(b);
			
			b = this.page._ctrl.document.createElement("<b></b>");
			b.innerText = "年";
			td.appendChild(b);
			b = this.page._ctrl.document.createElement("<b id='monthValue'></b>");
			b.innerText = option[1];
			td.appendChild(b);
			b = this.page._ctrl.document.createElement("<b></b>");
			b.innerText = "月";
			td.appendChild(b);
			//增加月份
			td = tr.insertCell();
			with(td){
			style.cursor = "hand";
			style.width = "11%";
			onmouseover = function(){className='ds_border'}
			onmouseout = function(){className=''}
			onclick = function(){calObj._selAddMonth()}
			title = '增加月份';
			innerHTML = "&gt;";
			}
			//增加年份
			td = tr.insertCell();
			with(td){
				style.cursor = "hand";
				style.width = "11%";
				onmouseover = function(){className='ds_border'}
				onmouseout = function(){className=''}
				onclick = function(){calObj._selAddYear()}
				title = '增加年份';
				innerHTML ="&gt;&gt;";
			} 
		}catch(e){}
		//第二行，汉字
		table_2 = this.page._ctrl.document.createElement("table");
		with(table_2){
			cellpadding = 0;
			cellspacing = 0;
			style.width = 140;
			style.height = 20;
		}
		table_2 = objList.appendChild(table_2);
		tr = table_2.insertRow();
		tr.align = "center";
		for(i=0;i<7;i++){
			td = tr.insertCell();
			td.innerText = week[i];
		}
		table_3 = this.page._ctrl.document.createElement("table");
		with(table_3){
			cellpadding = 0;
			cellspacing = 2;
			style.width = 140;
			style.backgroundColor = "#EEEEEE";
		}
		//第三行，选择日期
		table_3 = objList.appendChild(table_3);
		for(i=0;i<6;i++){
			tr = table_3.insertRow();
			tr.align = "center";
			for(j=0;j<7;j++){
				td = tr.insertCell();
				with(td){
				style.cursor = "hand";
				style.width = "10%";
				style.height = 16;
				onmouseover = function(){
					if(innerText!='' && className!='ds_border2')
					className='ds_border'
				}
				onmouseout = function(){
					if(className != 'ds_border2')
					className=''
				}
				onclick = function(){calObj._selGetValue(this)}
				}
			}
		}
		
		
		span = this.page._ctrl.document.createElement("span");
		with(span){
			style.cursor = "hand";
			onclick = function(){
				calObj._vhValue=calObj._getSurfaceCtrl().value=_globaljs.formatDate(new Date(),"-");
				calObj._showHideListBox(false);
				_globaljs_calendar__reSetFocus(calObj);
			}
			innerHTML = "<table width='100%' border=0><tr><td nowrap='no'>今天:"+_globaljs.formatDate(new Date(),"年")+"</td></tr></table>";
		}
		objList.appendChild(span);
		this._changeDate(eval(option[0]),eval(option[1]));
	};
function _globaljs_calendar__changeDate(year,month){
		//设置题头为录入的年月
		var myDate=new Date(year,month-1,1);
		var today=new Date();
		var day=myDate.getDay();
		var selectDate=this._vhValue.split('-');
		var length;
		switch(month){
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				length=31;
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				length=30;
				break;
			case 2:
			if(_globaljs.isLeapYear(year))
				length=29;
			else
				length=28;
		}
		var obj = objList.childNodes[2];
		for(i=0;i<obj.cells.length;i++){
			obj.cells[i].innerHTML='';
			obj.cells[i].style.color='';
			obj.cells[i].className='';
		}
		for(i=0;i<length;i++){
			obj.cells[i+day].innerHTML=(i+1);
			if(year==today.getFullYear() && (month-1)==today.getMonth() && (i+1)==today.getDate())
			obj.cells[i+day].style.color='red';
			if(year==eval(selectDate[0]) && month==eval(selectDate[1]) && (i+1)==eval(selectDate[2]))
			obj.cells[i+day].className='ds_border2';
			obj.cells[i+day].style.background='';
			if(year==selectDate[0] && month==selectDate[1] && (i+1)==selectDate[2])
			obj.cells[i+day].style.background='#99CCFF';
		}
		_globaljs_calendar__reSetFocus(this);
	}