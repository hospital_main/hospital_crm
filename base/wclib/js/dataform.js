/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createDataForm=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("dataform","_initDataFormMethod",page,id,parentObj,props,cb);
	};
_globaljs._initDataFormMethod=function(page,ctrl){
		this._initCtrlMethod(page,ctrl,"button",_globaljs.CTRL_TYPE_NULL);
		this._initSurfaceCtrl(ctrl,ctrl,true);
		ctrl._surfaceCtrl.onclick=_globaljs_dataform_onclick;
		ctrl._buildRequestParam=_globaljs_dataform_buildRequestParam;
		ctrl._defaultCb=_globaljs_dataform_defaultCb;
		ctrl._doSubmit=_globaljs_dataform_doSubmit;
		ctrl.vhSubmit=_globaljs_dataform_submit;
		ctrl.vhSetFieldValues=_globaljs_dataform_vhSetFieldValues;
		ctrl.vhSetField=_globaljs_dataform_vhSetField;
		ctrl._initState();
		_globaljs._initDataFormAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initDataFormAPI=function(page,ctrl){
		ctrl.submit=function(action,argus,cb){this._ctrl.vhSubmit(action,argus,cb);};
		ctrl.setFieldValues=function(values){return this._ctrl.vhSetFieldValues(values);};
		ctrl.vhSetField=function(fields){return this._ctrl.vhSetField(values);};
	};
function _globaljs_dataform_vhSetFieldValues(values){
		if(values==null)
			values=[];
		var fs=this._vhFields.split(",");
		for(var i=0,j=0;i<fs.length;i++,j++){
			if(j<values.length)
				this.page._hctrl(fs[i]).vhSetValue(values[i]);
			else
				this.page._hctrl(fs[i]).vhSetValue("");
		}
	};
function _globaljs_dataform_vhSetField(field){
		this._vhFields=field;
	};
function _globaljs_dataform_defaultCb(action,isOk,errorMsg,values){
		if(isOk==false){
			this.page.msgBox(errorMsg);
			return;
		}
		this.vhSetFieldValues(values);
	};
function _globaljs_dataform_buildRequestParam(){
		if(this._vhFields.length==0)
			return [];
		var values=this._vhFields.split(",");
		var ct;
		var hasError=false;
		for(var i=0;i<values.length;i++){
			ct=this.page._hctrl(values[i]+_globaljs._ctrlIdSubfix);
			ct._onCheckValue();
			if(ct._vhIsError==true)
				hasError=true;
			values[i]=ct.vhGetValue();
		}
		if(hasError==true)
			return null;
		return values;
	};
function _globaljs_dataform_onclick(){
		this._boxCtrl.page._setLastCtrlLostFocus(null);
		this.vhSubmit();
	};
function _globaljs_dataform_submit(action,argus,cb){
		var ctrl=this;
		var  ac,ar,acb;
		if(action){
			ac=action;	
		}else if(this._vhAction){
			ac=this._vhAction;
		}else{
			this.page.msgBox("程序BUG: 没有指定 action !");
			return false;
		}
		if(argus){
			ar=argus;	
		}else if(this._vhParam){
			ar=this.page._hctrl(this._vhParam+_globaljs._ctrlIdSubfix);
		}else{
			this.page.msgBox("程序BUG: 没有指定 param !");
			return false;
		}
		if(cb){
			acb=cb;
		}else{
			acb=function(action,isOk,errorMsg,values){ctrl._defaultCb(action,isOk,errorMsg,values);};
		}
		return this._doSubmit(ac,ar,acb);
	}
function _globaljs_dataform_doSubmit(action,argus,cb){
		this.vhSetEnabled(false);
		var dt=argus._buildRequestParam();
		if(dt==null){
			this.vhSetEnabled(true);
			return;
		}
		var param={};
		param["action"]=action;
		param["data"]=dt;
		var ctrl=this;
		var ccb=cb;
		var gerPara={};
		gerPara[_globaljs.getBinActionKey()]=_globaljs.getBinNewContentKey();
		_globaljs.sendRequest(_globaljs.getPageBinName(this.page,"dataform"),_globaljs.objToJson(param),function(text,xml){
				var obj=_globaljs.jsonToObj(text);
				if(ccb)
					ccb(action,_globaljs.str2bool(obj["isOk"]),obj["errorMsg"],obj["resultValues"]);
				ctrl.vhSetEnabled(true);
			},_globaljs.objToUrlcode(gerPara));
	};