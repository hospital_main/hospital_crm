/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createCheckbox=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("textfield","_initCheckboxMethod",page,id,parentObj,props,cb);
	};
_globaljs._initCheckboxMethod=function(page,ctrl){
		var v=_globaljs.str2bool(ctrl._vhValue);
		ctrl._vhValue=(v==true?"1":"0");
		this._initCtrlMethod(page,ctrl,"checkbox",_globaljs.CTRL_TYPE_VALUE);
		_globaljs._initSurfaceCtrl(ctrl,page._hctrl(ctrl.id+"_cbox_"),false);
		ctrl._onSetNormalState=_globaljs_checkbox_onSetNormalState;
		ctrl._onSetDisabledState=_globaljs_checkbox_onSetDisabledState;
		ctrl._onSetFocusState=_globaljs_checkbox_onSetFocusState;
		ctrl._initState();
		_globaljs._initCheckboxAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initCheckboxAPI=function(page,ctrl){
		_globaljs._initCtrlAPI(page,ctrl);
	};
function _globaljs_checkbox_onSetCssClass(ctrl,css){
		ctrl.className=css;
		ctrl.page._hctrl(ctrl.id+"_cbox_").className=css+"Btn";
		ctrl.page._hctrl(ctrl.id+"_label_").className=css+"Label";
	};
function _globaljs_checkbox_onSetNormalState(css){
		_globaljs_checkbox_onSetCssClass(this,css);
	};
function _globaljs_checkbox_onSetDisabledState(css){
		_globaljs_checkbox_onSetCssClass(this,css);
	};
function _globaljs_checkbox_onSetFocusState(css){
		_globaljs_checkbox_onSetCssClass(this,css);
	};