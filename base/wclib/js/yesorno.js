/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createYesOrNo=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("yesorno","_initYesOrNoMethod",page,id,parentObj,props,cb);
	};
_globaljs._initYesOrNoMethod=function(page,ctrl){
		this._initCtrlMethod(page,ctrl,"button",_globaljs.CTRL_TYPE_NULL);
		this._initSurfaceCtrl(ctrl,ctrl,true);
		ctrl._surfaceCtrl.onclick=_globaljs_dataform_onclick;
		ctrl._defaultCb=_globaljs_yesorno__defaultCb;
		ctrl._doSubmit=_globaljs_dataform_doSubmit;
		ctrl.vhSubmit=_globaljs_dataform_submit;
		ctrl.vhGetValues=_globaljs_yesorno_getvalues;
		ctrl._initState();
		ctrl._vhValues=[];
		_globaljs._initYesOrNoAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initYesOrNoAPI=function(page,ctrl){
		_globaljs._initCtrlAPI(page,ctrl);
		ctrl.submit=function(action,argus,cb){this._ctrl.vhSubmit(action,argus,cb);};
		ctrl.getValues=function(){return this._ctrl.vhGetValues();};
	};
function _globaljs_yesorno__defaultCb(action,isOk,errorMsg,values){
		this._vhValues=[];
		if(this._vhHideMsg=="all")
			return;
		if(isOk==true){
			this._vhValues=values;
			if(this._vhHideMsg!="msg")
				this.page.msgBox("�����ɹ�");
		}else{
			if(this._vhHideMsg!="error")
				this.page.msgBox(errorMsg);
		}
	};
function _globaljs_yesorno_getvalues(){
		return this._vhValues;
	};