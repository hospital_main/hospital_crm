/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createButton=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("button","_initButtonMethod",page,id,parentObj,props,cb);
	};
_globaljs._initButtonMethod=function(page,ctrl){
		this._initCtrlMethod(page,ctrl,"button",_globaljs.CTRL_TYPE_NULL,"click");
		this._initSurfaceCtrl(ctrl,ctrl,true);
		ctrl._surfaceCtrl.onclick=_globaljs_button_onclick;
		ctrl.vhDoAction=_globaljs_button_vhDoAction;
		ctrl._initState();
		_globaljs._initButtonAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initButtonAPI=function(page,ctrl){
		_globaljs._initCtrlAPI(page,ctrl);
		ctrl.doAction=function(){this._ctrl.vhDoAction();};
	};
function _globaljs_button_onclick(){
		this._boxCtrl.page._setLastCtrlLostFocus(null);
		this.vhDoAction();
	};
function _globaljs_button_vhDoAction(){
		this._dispatchEvent("click",null);
	};