/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createHidden=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("hidden","_initHiddenMethod",page,id,parentObj,props,cb);
	};
_globaljs._initHiddenMethod=function(page,ctrl){
		ctrl.value=ctrl._vhValue;
		_globaljs._initCtrlAttribute(page,ctrl,"hidden",_globaljs.CTRL_TYPE_VALUE);
		_globaljs._initCtrlEventFun(page,ctrl,"update");
		ctrl._surfaceCtrl=ctrl;
		ctrl.vhDestroy=_globaljs_ctrl_vhDestroy;
		ctrl.vhGetValue=_globaljs_ctrl_vhGetValue;
		ctrl.vhSetValue=_globaljs_ctrl_vhSetValue;
		_globaljs._initHiddenAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initHiddenAPI=function(page,ctrl){
		ctrl.destroy=function(){this._ctrl.vhDestroy();};
		ctrl.getValue=function(){return this._ctrl.vhGetValue();};
		ctrl.setValue=function(v){this._ctrl.vhSetValue(v);};
	};