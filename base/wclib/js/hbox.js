/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createHBox=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("hbox","_initHBoxMethod",page,id,parentObj,props,cb);
	};
_globaljs._initHBoxMethod=function(page,ctrl){
		_globaljs._initCtrlAttribute(page,ctrl,"hbox",_globaljs.CTRL_TYPE_NULL);
		ctrl.vhGetItemBox=_globaljs_hbox_vhGetItemBox;
		_globaljs._initHBoxAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initHBoxAPI=function(page,ctrl){
		ctrl.getBoxByIndex=function(idx){return this._ctrl.vhGetItemBox(idx);};
	};
function _globaljs_hbox_vhGetItemBox(indx){
		var ft=_globaljs.getTableFirstTr(ctrl).cells;
		if(indx>ft.cells.length)
			return null;
		else
			return 	ft.cells[indx];
	};