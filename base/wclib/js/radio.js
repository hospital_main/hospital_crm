/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createRadioGroup=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("radiogroup","_initRadioGroupMethod",page,id,parentObj,props,cb);
	};
_globaljs._initRadioGroupMethod=function(page,ctrl){
		ctrl.value=ctrl._vhValue;
		_globaljs._initCtrlAttribute(page,ctrl,"radiogroup",_globaljs.CTRL_TYPE_VALUE);
		ctrl._setFocus=_globaljs_ctrl__SetFocus;
		ctrl.vhSetFocus=_globaljs_radiogroup_vhSetFocus;
		ctrl._surfaceCtrl=ctrl;
		ctrl._radioItems={};
		ctrl.vhGetItemByValue=_globaljs_radiogroup_getItemByValue;
		ctrl.vhGetValue=_globaljs_ctrl_vhGetValue;
		ctrl.vhSetValue=_globaljs_radiogroup_vhSetValue;
		_globaljs._initRadioGroupAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initRadioGroupAPI=function(page,ctrl){
		//ctrl.getItemByValue=function(v){return this._ctrl.vhGetItemByValue(v);};
		ctrl.setFocus=function(){this._ctrl.vhSetFocus();};
		ctrl.getValue=function(){return this._ctrl.vhGetValue();};
		ctrl.setValue=function(v){this._ctrl.vhSetValue(v);};
	};
function _globaljs_radiogroup_getItemByValue(v){
		v=""+v;
		if(this._radioItems[v]){}else{
			return null;
		}
		var obj=this.page._hctrl(this._radioItems[v]);
		if(obj==null)
			return null;
		return obj;
	};
function _globaljs_radiogroup_vhSetFocus(){
		if(this._vhIsDisabled==true){
			return;
		} this.page.mymsg.value=this._vhValue
		var obj=this.vhGetItemByValue(this._vhValue);
		if(obj==null)
			return ;
		obj.vhSetFocus();
		this.page._hctrl(obj.id+"_cbox_").focus();
	}
function _globaljs_radiogroup_vhSetValue(v){
		var obj=this.vhGetItemByValue(v);
		if(obj==null)
			return ;
		this._vhValue=v;
		obj._setChecked();
	};
_globaljs.createRadio=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("radio","_initRadioMethod",page,id,parentObj,props,cb);
	};
_globaljs._initRadioMethod=function(page,ctrl){
		this._initCtrlMethod(page,ctrl,"radio",_globaljs.CTRL_TYPE_NULL);
		_globaljs._initSurfaceCtrl(ctrl,page._hctrl(ctrl.id+"_cbox_"),false);
		ctrl._onSetNormalState=_globaljs_radio_onSetNormalState;
		ctrl._onSetDisabledState=_globaljs_radio_onSetDisabledState;
		ctrl._onSetFocusState=_globaljs_radio_onSetFocusState;
		ctrl._setChecked=_globaljs_radio_setChecked;
		page._ctrl[ctrl._vhGroupId]._ctrl._radioItems[ctrl._vhValue]=ctrl.id;
		ctrl._initState();
	};
function _globaljs_radio_setChecked(){
		this.page._hctrl(this._vhGroupId)._vhValue=this._vhValue;
		this.page._hctrl(this.id+"_cbox_").checked=true;
	};
function _globaljs_radio_onSetCssClass(ctrl,css){
		ctrl.className=css;
		ctrl.page._hctrl(ctrl.id+"_cbox_").className=css+"Btn";
		ctrl.page._hctrl(ctrl.id+"_label_").className=css+"Label";
	};
function _globaljs_radio_onSetNormalState(css){
		_globaljs_radio_onSetCssClass(this,css);
	};
function _globaljs_radio_onSetDisabledState(css){
		_globaljs_radio_onSetCssClass(this,css);
	};
function _globaljs_radio_onSetFocusState(css){
		this._setChecked();
		_globaljs_radio_onSetCssClass(this,css);
	};