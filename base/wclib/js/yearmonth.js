/*
* Author: Liu Zhikun
* Create date: 2008
* Links: http://www.viewhigh.com
*/
_globaljs.createYearmonth=function(page,id,parentObj,props,cb){
		this._sendAndCreateCtrl("calendar","_initYearmonthMethod",page,id,parentObj,props,cb);
	};
_globaljs._initYearmonthMethod=function(page,ctrl){
		_globaljs._initCtrlMethod(page,ctrl,"ComboBox",_globaljs.CTRL_TYPE_VALUE);
		_globaljs_basecombobox_initMethod(page,ctrl);
		_globaljs._initSurfaceCtrl(ctrl,page._hctrl(ctrl.id+"_Input_"),false);
		
		ctrl._doShowList=_globaljs_yearmonth__doShowList;
		ctrl._createList=_globaljs_yearmonth__createList;
		ctrl._onCheckValue=_globaljs_yearmonth__onCheckValue;
		ctrl._onCheckValueFuns=[
				_globaljs_text_checkRequired,
				_globaljs_yearmonth_checkDateValue,
				_globaljs_yearmonth_checkMinValue,
				_globaljs_yearmonth_checkMaxValue,
				_globaljs_yearmonth_checkCompareValue
			];
		ctrl._listBoxDefaultHeight=30;
		ctrl._vhYearValue=_globaljs_yearmonth_getCurYearMonth().substr(0,4);
		ctrl._vhMonthValue=_globaljs_yearmonth_getCurYearMonth().substr(4);
		ctrl._vhIsQuarter=_globaljs.str2bool(ctrl._vhIsQuarter);
		
		ctrl._parseValue=_globaljs_yearmonth_parseValue;
		ctrl._setInputValue=_globaljs_yearmonth_setInputValue;
		ctrl._setYearMonth=_globaljs_yearmonth_setYearMonth;
		ctrl.vhSetQuarter=_globaljs_yearmonth_vhSetQuarter;
		ctrl.vhIsQuarter=_globaljs_yearmonth_vhIsQuarter;
		
		ctrl._incYear=_globaljs_yearmonth_incYear;
		ctrl._decYear=_globaljs_yearmonth_decYear;
		
		ctrl.vhSetQuarter(ctrl._vhIsQuarter);
		ctrl._setInputValue(ctrl._vhValue);
		ctrl._initState();
		_globaljs._initYearmonthAPI(page,page._addCtrl(ctrl));
	};
_globaljs._initYearmonthAPI=function(page,ctrl){
		_globaljs._initCtrlAPI(page,ctrl);
		ctrl.setQuarter=function(b){this._ctrl.vhSetQuarter(b);};
		ctrl.isQuarter=function(){return this._ctrl.vhIsQuarter();};
	};
_globaljs_yearmonth_yearName="年";
_globaljs_yearmonth_quarterName="季";
_globaljs_yearmonth_monthName="月";
_globaljs_yearmonth_numLabel_1= {"01":"01",
		"02":"02",
		"03":"03",
		"04":"04",
		"05":"05",
		"06":"06",
		"07":"07",
		"08":"08",
		"09":"09",
		"10":"10",
		"11":"11",
		"12":"12"
	};
_globaljs_yearmonth_numLabel_2={"01":"01",
		"04":"02",
		"07":"03",
		"10":"04"
	};
_globaljs_yearmonth_numLabel_3= {"01":"01",
		"02":"04",
		"03":"07",
		"04":"10"
	};
function _globaljs_yearmonth_vhSetQuarter(b){
		this._vhIsQuarter=b;
		if(b){
			this._numLabel=_globaljs_yearmonth_numLabel_2;
			this._monthLabel=_globaljs_yearmonth_quarterName;
		}else{
			this._monthLabel=_globaljs_yearmonth_monthName;
			this._numLabel=_globaljs_yearmonth_numLabel_1;
		}
	};
function _globaljs_yearmonth_vhIsQuarter(b){
		return this._vhIsQuarter;
	};
function _globaljs_yearmonth__onCheckValue(){
		this._tempDateObj=null;
		_globaljs._checkCtrlValue(this);
		if(this._vhIsError==true)
			;//this._getSurfaceCtrl().value=this._vhValue;
		else{
			if(this._tempDateObj==null)
				return;
			this._setInputValue(""+this._tempDateObj.getFullYear()+_globaljs.lpad(this._tempDateObj.getMonth()+1,"0",2));
		}
	};
function _globaljs_yearmonth_parseValue(v){
		if(v){}else{
			return null;
		}
		v=""+v;
		var vs=[0,0,0,0,0,0,0];
		vs[1]=this._vhYearValue.substr(0,1);
		vs[2]=this._vhYearValue.substr(1,1);
		vs[3]=this._vhYearValue.substr(2,1);
		vs[4]=this._vhYearValue.substr(3,1);
		for(var i=v.length-1,j=6;i>=0&&j>1;i--,j--){
			vs[j]=v.substr(i,1);
		}
		var y=""+vs[1]+vs[2]+vs[3]+vs[4];
		var m=""+vs[5]+vs[6];
		var iy=parseInt(y,10);
		var im=parseInt(m,10);

		if(this._vhIsQuarter==true){
			if(im<0||im>4)
				return null;
			im=parseInt(_globaljs_yearmonth_numLabel_3[_globaljs.lpad(im,"0",2)],10);
		}else{
			if(im<0||im>12)
				return null;
		}
		
		var curDate=new Date();
		curDate.setFullYear(iy);
		curDate.setMonth(im-1);
		curDate.setDate(1);
		return curDate;
	};
function _globaljs_yearmonth_checkDateValue(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		var v=ctrl._parseValue(ctrl._getSurfaceCtrl().value);
		if(v==null){
			return "无效的年"+ctrl._monthLabel+"值";
		} 
		var m=_globaljs.lpad(v.getMonth()+1,"0",2);
		if(ctrl._numLabel[m]){}else{
			return "不是有效的年"+ctrl._monthLabel+"值";
		}
		ctrl._tempDateObj=v;
		return null;
	};
function _globaljs_yearmonth_checkMinValue(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		if(ctrl._vhMinValue){}else{
			return null;
		}
		var v=ctrl._parseValue(ctrl._vhMinValue);
		if(v==null)
			return null;
		if(ctrl._tempDateObj.getTime()<v.getTime())
			return "不能小于"+ctrl._vhMinValue;
		return null;
	};
function _globaljs_yearmonth_checkMaxValue(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		if(ctrl._vhMaxValue){}else{
			return null;
		}
		var v=ctrl._parseValue(ctrl._vhMaxValue);
		if(v==null)
			return null;
		if(ctrl._tempDateObj.getTime()>v.getTime())
			return "不能大于"+ctrl._vhaxValue;
		return null;
	};
function _globaljs_yearmonth_checkCompareValue(ctrl){
		if(ctrl._isRequiredCheck()==false)
			return null;
		var cl=1;
		if(ctrl._vhCompare){}else{
			return null;
		}
		if(ctrl._vhCompare.length>2){
			var pc=ctrl._vhCompare.substr(0,2);
			if(pc==">="||pc=="<=")
				cl=2;
		}
		var cc=ctrl._vhCompare.substr(0,cl);
		var cv=ctrl._vhCompare.substr(cl);
		var ct=ctrl.page._hctrl(cv);
		if(ct==null)
			return "程序BGU:比较的控件不存在:"+cv;
		cv=ctrl._parseValue(ct.vhGetValue());
		if(cv==null)
			return null;
		if(cc==">"&&ctrl._tempDateObj.getTime()<=cv.getTime()){
			return "必须大于"+ct.vhGetValue();
		}else if(cc==">="&&ctrl._tempDateObj.getTime()<cv.getTime()){
			return "必须大于等于"+ct.vhGetValue();
		}else if(cc=="="&&ctrl._tempDateObj.getTime()!=cv.getTime()){
			return "必须等于"+ct.vhGetValue();
		}else if(cc=="<"&&ctrl._tempDateObj.getTime()>=cv.getTime()){
			return "必须小于"+ct.vhGetValue();
		}else if(cc=="<="&&ctrl._tempDateObj.getTime()>cv.getTime()){
			return "必须小于等于"+ct.vhGetValue();
		}else
			return null;
	};
function _globaljs_yearmonth__doShowList(){
		this._showHideListBox(true);
		this._getListBox().style["overflow"]="visible";
		this._createList();
		_globaljs_yearmonth__reSetFocus(this);
	};
function _globaljs_yearmonth_getCurYearMonth(){
		var myDate=new Date();
		var year=myDate.getFullYear();
		var month=myDate.getMonth()+1; 
		month=myDate.getMonth()+1; 
		month="asd00000"+month;
		month=month.substr(month.length-2);
		return year+month;
		}
function _globaljs_yearmonth__createList(){
		var todayMonth="";
		var tableStr="";
		tableStr="<table border='0' width='100%' cellpadding='2' cellspacing='0' >";
		tableStr+="	<tr>";
		tableStr+="		<td bgcolor='#FFFFFF'>";
		tableStr+="			<table width='100%'  border='0' cellspacing='4' cellpadding='0'>";
		if(this._vhIsQuarter){
			todayMonth=_globaljs_yearmonth_getCurYearMonth().substr(4);
			todayMonth=parseInt(todayMonth/3+0.9)*3-2;
			todayMonth="0000"+todayMonth;
			todayMonth=todayMonth.substr(todayMonth.length-2);
			tableStr+="				<tr>";
			for(var i in this._numLabel){
				if(this._vhMonthValue==i)
					bg="seletedBgColor";
				else
					bg="white";
				tableStr+="					<td align='center' valign='middle' bgcolor='"+bg+"'><span onclick=\"page._hctrl('"+this.id+"')._setYearMonth('"+this._vhYearValue+i+"')\" >"+this._numLabel[i]+this._monthLabel+"</span></td>";
			}
			tableStr+="				</tr>";
		}else{
			todayMonth=_globaljs_yearmonth_getCurYearMonth().substr(4);
			for(var i in this._numLabel){
				if(i%3==1)
					tableStr+="				<tr>";
				if(this._vhMonthValue==i)
					bg="seletedBgColor";
				else
					bg="white";
				tableStr+="					<td align='center' valign='middle' bgcolor='"+bg+"'><span onclick=\"page._hctrl('"+this.id+"')._setYearMonth('"+this._vhYearValue+i+"')\" >"+this._numLabel[i]+"</span></td>";
				if(i%3==0)
					tableStr+="				</tr>";
			}
			
		}
		tableStr+="			</table>";
		tableStr+="		</td>";
		tableStr+="	</tr>";
		tableStr+="	<tr>";
		tableStr+="		<td bgcolor='#FFFFFF'>";
		tableStr+="			<table width='100%'  border='0' cellspacing='0' cellpadding='0'>";
		tableStr+="				<tr>";
		tableStr+="					<td align='center' ><span onclick=\"page._hctrl('"+this.id+"')._decYear()\"><font face='Webdings'>3</font></span></td>";
		tableStr+="					<td align='center' valign='middle'><span onclick=\"page._hctrl('"+this.id+"')._setYearMonth('"+this._vhYearValue+this._vhMonthValue+"')\">"+this._vhYearValue+"</span></td>";
		tableStr+="					<td align='center'><span onclick=\"page._hctrl('"+this.id+"')._incYear()\"><font face='Webdings'>4</font></span></td>";
		tableStr+="				</tr>";
		tableStr+="			</table>";
		tableStr+="		</td>";
		tableStr+="	</tr>";
		tableStr+="	<tr>";
		tableStr+="		<td noWrap=\"true\" bgcolor='#FFFFFF'><span onclick=\"page._hctrl('"+this.id+"')._setYearMonth('"+_globaljs_yearmonth_getCurYearMonth().substr(0,4)+todayMonth+"')\">今天:"+_globaljs_yearmonth_getCurYearMonth().substr(0,4)+_globaljs_yearmonth_yearName+this._numLabel[todayMonth]+this._monthLabel+"</span></td>";
		tableStr+="	</tr>";
		tableStr+="</table>";
		this._getListBox().innerHTML=tableStr;
		var spans=this._getListBox().getElementsByTagName("span");
		for(var i=0;i<spans.length;i++){
			spans[i].style["cursor"]="hand";
			spans[i].onmouseover=function(){this.parentNode.oldBgColor=this.parentNode.bgColor;this.parentNode.bgColor="#0066FF"};
			spans[i].onmouseout=function(){this.parentNode.bgColor=this.parentNode.oldBgColor};
		}
		_globaljs_yearmonth__reSetFocus(this);
	}
function _globaljs_yearmonth__reSetFocus(ctrl){
		ctrl._getSurfaceCtrl().focus();
		ctrl._getSurfaceCtrl().select();
	};
function _globaljs_yearmonth_incYear(){
		this._vhYearValue++;
		this._createList();
	};
function _globaljs_yearmonth_decYear(){
		this._vhYearValue--;
		this._createList();
	};
function _globaljs_yearmonth_setYearMonth(ym){
		this._setInputValue(ym);
		this._showHideListBox(false);
		_globaljs_calendar__reSetFocus(this);
	};
function _globaljs_yearmonth_setInputValue(v){
		if(v==""||v==null)
			return ;
		if(v.length!=6)
			return ;
		var y=v.substr(0,4);
		var m=v.substr(4);
		if(typeof(this._numLabel[m])=="undefined")
			return ;
		this._vhYearValue=y;
		this._vhMonthValue=m;
		this._getSurfaceCtrl().value=this._vhYearValue+_globaljs_yearmonth_yearName+this._numLabel[this._vhMonthValue]+this._monthLabel;
		this._vhValue=v;
	}