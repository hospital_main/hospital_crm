function submitData(obj){
	
	var res=checkInputs();
	//alert(res)
	if(res==null||res=="")
		return false;

	if(sql_id.value.indexOf("acctBook_subj_Print_0")>=0 ||sql_id.value.indexOf("acctBook_subj_Print_1")>=0|| sql_id.value.indexOf("acctBook_money_cash_Print_1")>=0|| sql_id.value.indexOf("acctBook_money_bank_Print_1")>=0) {
		var start_row = 0;
		var end_row = 0;
		var start_n = 0;
		var end_n = 0;
		
		jQuery("#_mainDataTable").find("tr").each(function(){
			if(jQuery(this).find("td").eq(1).text() == '明细记录的起始行')
				start_row = jQuery(this).find("td").eq(2).find("input:first").val();
			if(jQuery(this).find("td").eq(1).text() == '明细记录的结尾行')
				end_row = jQuery(this).find("td").eq(2).find("input:first").val();
		});
		start_n = getValuePairBySql("AcctPrintParaSet_selectMX" , "<para_name>明细记录的起始行</para_name><sql_code>"+sql_id.value+"</sql_code>")			
		end_n = getValuePairBySql("AcctPrintParaSet_selectMX" , "<para_name>明细记录的结尾行</para_name><sql_code>"+sql_id.value+"</sql_code>")			

		if(parseInt(end_n[0])-parseInt(start_n[0])!=parseInt(end_row)-parseInt(start_row)){
				alert("因参数变化需重新进入打印页面！")	;
		}
	}
	
	if(sql_id.value.indexOf("acctBook_subj_Print") >= 0) {
			var start_row = 0;
			var end_row = 0;
			var total_row = parent.iframe1.CellWeb1.GetRows(0)-1;
			
			
			jQuery("#_mainDataTable").find("tr").each(function(){
				if(jQuery(this).find("td").eq(1).text() == '明细记录的起始行')
					start_row = jQuery(this).find("td").eq(2).find("input:first").val();
				if(jQuery(this).find("td").eq(1).text() == '明细记录的结尾行')
					end_row = jQuery(this).find("td").eq(2).find("input:first").val();
			});
		
			
			if(parseInt(start_row) > parseInt(end_row)){
				alert("明细记录的起始行不能大于明细记录的结尾行！");
				return false;
			}
			if(parseInt(end_row) > parseInt(total_row)){
				alert("明细记录的结尾行不能大于"+total_row);
				return false;
			}
			 
	}

	window.xmlhttp.post(obj.name, res,"")
	var str = window.xmlhttp._object.responseText

	if(str.indexOf("<error>")>0)
		if (!window.doMsg(str,"")) {
		  return false;
		}
	//sysDictsUnitinfoCopyParas_select.click();
	return true;
}
function checkInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	var res="";
	for(var i=0;i<trs.length;i++){
		if(trs[i].getAttribute("_editParaType")==null){ 
			
		continue;}
		if(trs[i]._editDataValue==trs[i]._editInputValue){ 
		continue;}
		res+="<record>";
		if(checkValue(trs[i],trs[i]._editParaType,trs[i]._editInputValue))
			res+=trs[i]._editPk+"<value>"+trs[i]._editInputValue+"</value>";
		else{
			return null;
		}
		res+="</record>";
	}
	return res;
}
function checkValue(tr,type,value){
	var msg=tr.cells[0].innerText+" "+tr.cells[1].innerText+" :要求是" ;
	var res=true;
	if(type=="1"&&value!=""){
		msg+=" 文本 ";
		res=true;
	}else if(type=="2" &&value!=""){
		msg+=" 正整数 ";
		var n=parseInt(value);
		if(isNaN(n)||n<0||n!=value){
			res=false;
		}
	}/*else  if(type=="3"&&value!=""){
		msg+=" 列行坐标 ";
		var vv=value.split(",");
		if(vv.length!=2)
			res=false;
		else{
			var v0=parseInt(vv[0],10);
			var v1=parseInt(vv[1],10);
			if(isNaN(v0)||isNaN(v1)||v0<0||v1<0||v0!=vv[0]||v1!=vv[1])
				res=false;
		}
	}*/else if(type=="4"&&value!=""){
		msg+=" 列行坐标 ";
		var vv=value.split(",");
		if(vv.length!=2)
			res=false;
		else{
			var v0=parseInt(vv[0],10);
			var v1=parseInt(vv[1],10);
			if(isNaN(v0)||isNaN(v1)||v0<0||v1<0||v0!=vv[0]||v1!=vv[1])
				res=false;
		}
	}else if(type=="5"){
	
	}
	if(res==false)
		alert(msg);
	return res;
}
function initInputs(tableObj){
	var table=document.getElementById("_mainDataTable");
	if(tableObj)
		table=tableObj;
	var trs=table.getElementsByTagName("tr");
	for(var i=0;i<trs.length;i++)
		initTrInputs(trs[i]);
}



function initTrInputs(tr){
	var pt=tr.getAttribute("_editParaType");
	var dv=tr.getAttribute("_editDataValue");
	var di=tr.getAttribute("_editDataId");
	
	if(pt==null)
		return ;
	var inp="";
	var id=getJsGuid();
	var tag="";
	var chk="";
	tr._editInputId=id;
	if(pt=="2"||pt=="4"||pt=="6"){
		if (di=='7006') {
			inp="<input type='text' id='"+id+"' name='"+id+"' value='"+dv+"' readonly /><button  id='button"+id+"' name='button"+id+"' value='' onclick = 'setBtnChange(this)'>采集坐标</button>";
			tag="input";
		}else if (di=='8104') {
			inp="<input type='text' id='"+id+"' name='"+id+"' value='"+dv+"' class='di8104' readonly/>";
			tag="input";
		}else if (di=='8105') {
			inp="<input type='text' id='"+id+"' name='"+id+"' value='"+dv+"' />";
			tag="input";
		}else{
			inp="<input type='text' id='"+id+"' name='"+id+"' value='"+dv+"' /><button  id='button"+id+"' name='button"+id+"' value='' onclick = 'setBtnChange(this)'>采集坐标</button>";
			tag="input";
		}
	}else if(pt=="5"){//是否套打
		var ops=tr.cells[3].innerText.split("\/");
		inp="<select id='"+id+"' name='"+id+"' >";
		tag="select"
		for(var i=0;i<ops.length;i++){
			if(dv==ops[i])
				chk=" selected ";
			else
				chk="";
			inp+="<option "+chk+" value='"+ops[i]+"'>"+ops[i]+"</option>";
		}
		inp+="</select>";
	}else
		return ;
	tr.cells[2].innerHTML=inp;
	var inpus=tr.cells[2].getElementsByTagName("input");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	inpus=tr.cells[2].getElementsByTagName("select");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	
}
function setInputChange(tr,inp){
	//inp.onblur=
	inp.onclick=function(){
		if(this.previousSibling==null) return;
		//this.previousSibling.setAttribute('atr',parent.iframe1.CellWeb1.GetCurrentCol()+','+parent.iframe1.CellWeb1.GetCurrentRow()) ;
		if(this.parentNode.parentNode.getAttribute("_editDataType") == "5")
			this.previousSibling.value=parent.iframe1.CellWeb1.GetCurrentCol();
		else if(this.parentNode.parentNode.getAttribute("_editDataType") == "7"){
			if(this.parentNode.parentNode.getAttribute("_editDataId") == "7006") return
			this.previousSibling.value=parent.iframe1.CellWeb1.GetCurrentRow();
		}
		else
			this.previousSibling.value=parent.iframe1.CellWeb1.GetCurrentCol()+','+parent.iframe1.CellWeb1.GetCurrentRow();
		this.onkeyup();
		};
	inp.onkeyup=function(){if(this.className=="di8104"){this.value=this.value.replace(/^(\d*)\D.*$/,"$1")};tr._editInputValue=this.value};
	inp.onchange=function(){tr._editInputValue=this.value};
}
function setBtnChange(btn){
	if(btn.previousSibling==null) return;
		//this.previousSibling.setAttribute('atr',parent.iframe1.CellWeb1.GetCurrentCol()+','+parent.iframe1.CellWeb1.GetCurrentRow()) ;
		if(btn.parentNode.parentNode.getAttribute("_editDataType") == "5")
			btn.previousSibling.value=parent.iframe1.CellWeb1.GetCurrentCol();
		else if(btn.parentNode.parentNode.getAttribute("_editDataType") == "7"){
			if(btn.parentNode.parentNode.getAttribute("_editDataId") == "7006") return
			btn.previousSibling.value=parent.iframe1.CellWeb1.GetCurrentRow();
		} else if(btn.parentNode.parentNode.getAttribute("_editDataId") == "8002")
			btn.previousSibling.value=parent.iframe1.CellWeb1.GetCurrentCol();
		else
			btn.previousSibling.value=parent.iframe1.CellWeb1.GetCurrentCol()+','+parent.iframe1.CellWeb1.GetCurrentRow();
		btn.parentNode.parentNode._editInputValue=btn.previousSibling.value;
	
}

