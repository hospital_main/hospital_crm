var selectedTD
var selectedTR
var selectedTBODY
var selectedTable
var oriInfoList = new Array();

function TableInsert(){
	if (!isTableSelected()){
		showDialog('table.htm', true);
	}
}

function TableProp(){
	if (isTableSelected()||isCursorInTableCell()){
		showDialog('table.htm?action=modify', true);
	}
}

function TableCellProp(){
	if (isCursorInTableCell()){
		showDialog('tablecell.htm', true);
	}
}

function TableCellSplit(){
	if (isCursorInTableCell()){
		showDialog('tablecellsplit.htm',true);
	}
}

function TableRowProp(){
	if (isCursorInTableCell()){
		showDialog('tablecell.htm?action=row', true);
	}
}

function TableRowInsertAbove() {

//	if (isCursorInTableCell())
{
		var numCols = 0

		allCells = selectedTR.cells
		for (var i=0;i<allCells.length;i++) {
		 	numCols = numCols + allCells[i].getAttribute('colSpan')
		}

		var newTR = selectedTable.insertRow(selectedTR.rowIndex)
	
		for (i = 0; i < numCols; i++) {
		 	newTD = newTR.insertCell()
			newTD.innerHTML = "&nbsp;"

			if (borderShown == "yes") {
				newTD.runtimeStyle.border = "1px dotted #BFBFBF"
			}
		}
	}	
}

function TableRowInsertBelow() {

//	if (isCursorInTableCell())
	{
		
		var numCols = 0

		allCells = selectedTR.cells
		for (var i=0;i<allCells.length;i++) {
		 	numCols = numCols + allCells[i].getAttribute('colSpan')
		}

		var newTR = selectedTable.insertRow(selectedTR.rowIndex+1)

		for (i = 0; i < numCols; i++) {
		 	newTD = newTR.insertCell()
			newTD.innerHTML = "&nbsp;"
			
			if (borderShown == "yes") {
				newTD.runtimeStyle.border = "1px dotted #BFBFBF"
			}
		}
	}
}

function TableRowMerge() {
//	if (isCursorInTableCell()) 
	{

		var rowSpanTD = selectedTD.getAttribute('rowSpan')
		allRows = selectedTable.rows
		if (selectedTR.rowIndex +1 != allRows.length) {

			var allCellsInNextRow = allRows[selectedTR.rowIndex+selectedTD.rowSpan].cells
			var addRowSpan = allCellsInNextRow[selectedTD.cellIndex].getAttribute('rowSpan')
			var moveTo = selectedTD.rowSpan

			if (!addRowSpan) addRowSpan = 1;

			selectedTD.rowSpan = selectedTD.rowSpan + addRowSpan
			allRows[selectedTR.rowIndex + moveTo].deleteCell(selectedTD.cellIndex)
		}
	}
}

function TableRowSplit(nRows){
//	if (!isCursorInTableCell()) return;
	if (nRows<2) return;

	var addRows = nRows - 1;
	var addRowsNoSpan = addRows;

	var nsLeftColSpan = 0;
	for (var i=0; i<selectedTD.cellIndex; i++){
		nsLeftColSpan += selectedTR.cells[i].colSpan;
	}

	var allRows = selectedTable.rows;

	// rowspan>1
	while (selectedTD.rowSpan > 1 && addRowsNoSpan > 0){
		var nextRow = allRows[selectedTR.rowIndex+selectedTD.rowSpan-1];
		selectedTD.rowSpan -= 1;

		var ncLeftColSpan = 0;
		var position = -1;
		for (var n=0; n<nextRow.cells.length; n++){
			ncLeftColSpan += nextRow.cells[n].getAttribute('colSpan');
			if (ncLeftColSpan>nsLeftColSpan){
				position = n;
				break;
			}
		}

		var newTD=nextRow.insertCell(position);
		newTD.innerHTML = "&nbsp;";

		if (borderShown == "yes") {
			newTD.runtimeStyle.border = "1px dotted #BFBFBF";
		}
			
		addRowsNoSpan -= 1;
	}

	// rowspan=1
	for (var n=0; n<addRowsNoSpan; n++){
		var numCols = 0

		allCells = selectedTR.cells
		for (var i=0;i<allCells.length;i++) {
			numCols = numCols + allCells[i].getAttribute('colSpan')
		}

		var newTR = selectedTable.insertRow(selectedTR.rowIndex+1)

		for (var j=0; j<selectedTR.rowIndex; j++){
			for (var k=0; k<allRows[j].cells.length; k++){
				if ((allRows[j].cells[k].rowSpan>1)&&(allRows[j].cells[k].rowSpan>=selectedTR.rowIndex-allRows[j].rowIndex+1)){
					allRows[j].cells[k].rowSpan += 1;
				}
			}
		}

		for (i = 0; i < allCells.length; i++) {
			if (i!=selectedTD.cellIndex){
				selectedTR.cells[i].rowSpan += 1;
			}else{
				newTD = newTR.insertCell();
				newTD.colSpan = selectedTD.colSpan;
				newTD.innerHTML = "&nbsp;";

				if (borderShown == "yes") {
					newTD.runtimeStyle.border = "1px dotted #BFBFBF";
				}
			}
		}
	}

}

function TableRowDelete() {
//	if (isCursorInTableCell()) 
	{
		selectedTable.deleteRow(selectedTR.rowIndex)
	}
}

function TableColInsertLeft() {
//	if (isCursorInTableCell()) 
	{
		moveFromEnd = (selectedTR.cells.length-1) - (selectedTD.cellIndex)
		allRows = selectedTable.rows
		for (i=0;i<allRows.length;i++) {
			rowCount = allRows[i].cells.length - 1
			position = rowCount - moveFromEnd
			if (position < 0) {
				position = 0
			}
			newCell = allRows[i].insertCell(position)
			newCell.innerHTML = "&nbsp;"

			if (borderShown == "yes") {
				newCell.runtimeStyle.border = "1px dotted #BFBFBF"
			}
		}
	}
}

function TableColInsertRight() {
//	if (isCursorInTableCell()) 
	{
		moveFromEnd = (selectedTR.cells.length-1) - (selectedTD.cellIndex)
		allRows = selectedTable.rows
		for (i=0;i<allRows.length;i++) {
			rowCount = allRows[i].cells.length - 1
			position = rowCount - moveFromEnd
			if (position < 0) {
				position = 0
			}
			newCell = allRows[i].insertCell(position+1)
			newCell.innerHTML = "&nbsp;"

			if (borderShown == "yes") {
				newCell.runtimeStyle.border = "1px dotted #BFBFBF"
			}
		}	
	}
}

function TableColMerge() {
//	if (isCursorInTableCell()) 
	{

		var colSpanTD = selectedTD.getAttribute('colSpan')
		allCells = selectedTR.cells

		if (selectedTD.cellIndex + 1 != selectedTR.cells.length) {
			var addColspan = allCells[selectedTD.cellIndex+1].getAttribute('colSpan')
			selectedTD.colSpan = colSpanTD + addColspan
			selectedTR.deleteCell(selectedTD.cellIndex+1)
		}	
	}
}

function TableColDelete() {
//   	if (isCursorInTableCell()) 
	{
		moveFromEnd = (selectedTR.cells.length-1) - (selectedTD.cellIndex)
		allRows = selectedTable.rows
		for (var i=0;i<allRows.length;i++) {
			endOfRow = allRows[i].cells.length - 1
			position = endOfRow - moveFromEnd
			if (position < 0) {
				position = 0
			}

			allCellsInRow = allRows[i].cells
				
			if (allCellsInRow[position].colSpan > 1) {
				allCellsInRow[position].colSpan = allCellsInRow[position].colSpan - 1
			} else { 
				allRows[i].deleteCell(position)
			}
		}
	}
}

function TableColSplit(nCols){
//	if (!isCursorInTableCell()) return;
	if (nCols<2) return;

	var addCols = nCols - 1;
	var addColsNoSpan = addCols;
	var newCell;

	var nsLeftColSpan = 0;
	var nsLeftRowSpanMoreOne = 0;
	for (var i=0; i<selectedTD.cellIndex; i++){
		nsLeftColSpan += selectedTR.cells[i].colSpan;
		if (selectedTR.cells[i].rowSpan > 1){
			nsLeftRowSpanMoreOne += 1;
		}
	}

	var allRows = selectedTable.rows
	// colSpan>1
	while (selectedTD.colSpan > 1 && addColsNoSpan > 0) {
		newCell = selectedTR.insertCell(selectedTD.cellIndex+1);
		newCell.innerHTML = "&nbsp;"
		if (borderShown == "yes") {
			newCell.runtimeStyle.border = "1px dotted #BFBFBF"
		}
		selectedTD.colSpan -= 1;
		addColsNoSpan -= 1;
	}
	// colSpan=1
	for (i=0;i<allRows.length;i++) {
		var ncLeftColSpan = 0;
		var position = -1;
		for (var n=0; n<allRows[i].cells.length; n++){
			ncLeftColSpan += allRows[i].cells[n].getAttribute('colSpan');
			if (ncLeftColSpan+nsLeftRowSpanMoreOne>nsLeftColSpan){
				position = n;
				break;
			}
		}
		
		if (selectedTR.rowIndex!=i){
			if (position!=-1){
				allRows[i].cells[position+nsLeftRowSpanMoreOne].colSpan += addColsNoSpan;
			}
		}else{
			for (var n=0; n<addColsNoSpan; n++){
				newCell = allRows[i].insertCell(selectedTD.cellIndex+1)
				newCell.innerHTML = "&nbsp;"
				newCell.rowSpan = selectedTD.rowSpan;

				if (borderShown == "yes") {
					newCell.runtimeStyle.border = "1px dotted #BFBFBF"
				}
			}
		}
	}
}

function isTableSelected() {
	if (editor.document.selection.type == "Control") {
		var oControlRange = editor.document.selection.createRange();
		if (oControlRange(0).tagName.toUpperCase() == "TABLE") {
			selectedTable = editor.document.selection.createRange()(0);
			return true;
		}	
	}
} 

function isCursorInTableCell() {
	if (editor.document.selection.type != "Control") {
		//var elem = editor.document.selection.createRange().parentElement()
		var elem = editor.event.srcElement;
//		if(arguments.length==0)
//			return true;
//		var elem = event.srcElement;
		while (elem.tagName.toUpperCase() != "TD" && elem.tagName.toUpperCase() != "TH"){
			elem = elem.parentElement
			if (elem == null)
                break
		}
		if (elem) {
			if(selectedTD != null)
				cellBgColor(false);
				
			selectedTD = elem
			selectedTR = selectedTD.parentElement
			selectedTBODY =  selectedTR.parentElement
			selectedTable = selectedTBODY.parentElement
			cellBgColor(true);
			return true
		}
	}
}

function TableColData(){
	if(isBottomCell())
		oriInfoList[0] = selectedTD.getAttribute('width');
	else
		oriInfoList[0] = '';
		
	oriInfoList[1] = selectedTD.getAttribute('align');
	oriInfoList[2] = selectedTD.getAttribute('fIdx');
	oriInfoList[3] = trim(selectedTD.innerText);
	var propertylist = showModalDialog("CellProperty.htm",window,"dialogWidth:250px;dialogHeight:230px;help:no;scroll:no;status:no");
	
	if(propertylist!=null){
		if(propertylist[0] != 'px')
			selectedTD.width = propertylist[0];
			
		selectedTD.align = propertylist[1];
		selectedTD.innerText = propertylist[4];
		if(isBottomCell()){
			selectedTD.fIdx = propertylist[2];
		}
	}
}

//单元格是最后一行的单元格
function isBottomCell(){
	var tdRows;
	if(selectedTD.getAttribute('rowSpan')==null)
		tdRows = 1;
	else
		tdRows = selectedTD.getAttribute('rowSpan')

	if(selectedTR.rowIndex+tdRows == selectedTable.rows.length)
		return true;
	
	return false;
}

function getTDInfo(){
		return oriInfoList;
}

function cellBgColor(sel){
	if(sel)
		selectedTD.bgColor = '#CCCCCC';
	else
		selectedTD.bgColor = '#FCB724';
}