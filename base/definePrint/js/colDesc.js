	function getXMLDoc(xmlFile){
		var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async = false;
		xmlDoc.resolveExternals = false;
		xmlDoc.load(xmlFile);
		if(xmlDoc.parseError.errorCode != 0){
		   //var myErr = xmlDoc.parseError;
		   //alert("格式错误的XML文件！");
		   //alert(myErr.reason);
		   return;
		}

		return xmlDoc;
	}

	function initColOptions(oSelect, xmlDoc){
		if(!xmlDoc){
			alert("不存在或格式错误的列配置文件！");
			return;
		}
	
		var objNodeList = xmlDoc.documentElement.selectNodes("/root/cols/col");
		var nullOp = document.createElement("OPTION");
		oSelect.options.add(nullOp);
		nullOp.innerText = "请选择……";
		nullOp.value = "";
		
		for (var i = 0; i < objNodeList.length; i++){
			var objNode = objNodeList.item(i);
			//alert(objNode.xml);
			var oOption = document.createElement("OPTION");
			oSelect.options.add(oOption);
			oOption.innerText = objNode.getAttribute("name");
			oOption.value = objNode.getAttribute("idx");
		}
		
		return objNodeList;
	}
	
	function createHtmlData(xmlDoc){
		if(!xmlDoc){
			alert("未发现列配置文件！");
			return;
		}
	
		var html = "<table border='1'><tbody><tr>";
		var objNodeList = xmlDoc.documentElement.selectNodes("/cols/col");
		for (var i = 0; i < objNodeList.length; i++){
			var objNode = objNodeList.item(i);
			var td = "<td";
			td += (" fIdx='" + objNode.getAttribute("idx") + "'");
			td += (" width='" + objNode.getAttribute("width") + "'");
			td += ">";
			td +=	objNode.getAttribute("name");
		  td += "</td>";
		  html += td;
		}
		html += "</tr></tbody></table>";
		return html;
	}