var lang = new Object();

lang["TableColData"]		= "单元格属性..."
//lang["TableCellSplit"]			= "拆分单元格..."
//lang["TableRowProp"]			= "表格行属性..."
lang["TableRowInsertAbove"]		= "向上插入行"
lang["TableRowInsertBelow"]		= "向下插入行"
lang["TableRowMerge"]			= "向下合并行"
lang["TableRowSplit"]			= "拆分行"
lang["TableRowDelete"]			= "删除行"
lang["TableColInsertLeft"]		= "向左插入列"
lang["TableColInsertRight"]		= "向右插入列"
lang["TableColMerge"]			= "向右合并列"
lang["TableColSplit"]			= "拆分列"
lang["TableColDelete"]			= "删除列"

lang["UIMenuWidth"]				= 150
