var borderShown = "0";

function showDialog(url, optValidate){
	var sName;
	var nIndex = url.indexOf(".");
	if (nIndex<0){
		sName = url;
		url = url + ".htm";
	}else{
		sName = url.substring(0, nIndex);
	}
	url = "dialog/" + url;
	sName = sName.toLowerCase();
	url = url.toLowerCase();

	/*if (optValidate) {
		if (!validateMode()) {return;}
	}*/
	//editor.focus();
	//if (!history.saved){saveHistory();}
	var arr = showModalDialog(url, window, "dialogWidth:0px;dialogHeight:0px;help:no;scroll:no;status:no");
	//saveHistory();
	//editor.focus();
}

	function initEditor(editorObj, htmlCode){
		editorObj.document.open();
		editorObj.document.write("<html>")
		editorObj.document.write("<head>")
		editorObj.document.write("<style type='text/css'>");
		
		editorObj.document.write("table {border-collapse:collapse; font-family:'宋体'; font-size:12px; background-color: white; padding : 1;}");
		editorObj.document.write("tr {font-weight: bold;	border-collapse:collapse;	font-family:'宋体';	color:#472300;text-decoration:none;background:#FFDDA6;text-align:center;vertical-align:bottom;CELLSPACING : 0;background-color:#FCB724;}");
		editorObj.document.write("td {white-space:nowrap;overflow:scroll;text-align:center;vertical-align:bottom;}");//控制将td格式撑乱
		editorObj.document.write("</style>");
		editorObj.document.write("</head>")
		editorObj.document.write("<body onselectstart='return false;'></body>")
		editorObj.document.write("</html>")
		editorObj.document.close();

		editorObj.document.body.innerHTML = htmlCode;
	}

	function trim(str){
	  return str.replace(/(^\s+)|\s+$/g, "");
	}

	function isModify(htmlContent, editorObj){
		return (htmlContent != editorObj.document.body.innerHTML);
	}