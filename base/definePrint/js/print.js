/*********************************************************************************/
/*包含全局信息的打印上下文对象*/
function PrintContext(name, path){
	this.name = name;
	this.path = path;
	this.userDataDir = "user_data/define_xsl";
	this.descFile = "PrintDesc.xml";
	this.formateFile = "PrintFormate.xml";
	this.printViewFile = "PrintView.xsl";
	this.getFullFormateFile = getFullFormateFile;
	this.getFullViewFile = getFullViewFile;
	this.getFullDescFile = getFullDescFile;
	this.toXMLString = toXMLString;
}

function getFullFormateFile(){
	return "_" + this.name + "_" + this.formateFile;
}

function getFullViewFile(){
	return "_" + this.name + "_" + this.printViewFile;
}

function getFullDescFile(){
	return "_" + this.name + "_" + this.descFile;
}

function toXMLString(){
	var xml = "";
	xml += "<Context>";
	xml += ("<UserDataDir>" + this.userDataDir + "</UserDataDir>");
	xml += ("<Name>" + this.name + "</Name>");
	xml += ("<Path>" + this.path + "</Path>");
	xml += ("<DescFile>" + this.getFullDescFile() + "</DescFile>");
	xml += ("<FormateFile>" + this.getFullFormateFile() + "</FormateFile>");
	xml += ("<ViewFile>" + this.getFullViewFile() + "</ViewFile>");
	xml += "</Context>";
	return xml;
}
/*********************************************************************************/
/*创建列描述文件时的操作对象*/
function PrintDescInfo(context, descXML){
	this.context = context;
	this.descContent = descXML;
	this.toDescXML = toDescXML;
}

function toDescXML(){
	var xml = "<?xml version='1.0' encoding='GBK' ?>";
	xml += "<root>";
	xml += "<Action>SaveDesc</Action>";
	xml += this.context.toXMLString();
	xml += "<XMLContent>";
	xml += ("<Desc><![CDATA[" + this.descContent + "]]></Desc>");
	xml += "</XMLContent>";
	xml += "</root>";
	return xml;
}
/*********************************************************************************/
/*创建打印预览文件时的操作对象*/
function PrintViewInfo(context, colDescDoc, editorObj, titleCtr, subTitleCtr, titleTagName){
	this.context = context;
	this.editorObj = editorObj;
	this.colDescContent = colDescDoc.xml;
	this.getFormateData = getFormateData;
	this.titleTagName = titleTagName;
	this.titleCtr = titleCtr;
	this.subTitleCtr = subTitleCtr;
	this.toViewXML = toViewXML;
}

function getFormateData(){
	return getXMLCode(this.editorObj);
}

function checkHTMLCode(editorObj){
	//alert("checkHTMLCode...");
	if(!editorObj)
		return false;
		
	var tb = editorObj.document.body.firstChild;
	var tds = tb.getElementsByTagName("TD");

	//检验所有的td
	for(var i=0;i<tds.length;i++){
		var tdRows;
		if(tds[i].getAttribute('rowSpan')==null)
			tdRows = 1;
		else
			tdRows = tds[i].getAttribute('rowSpan')

		if(tds[i].parentNode.rowIndex + tdRows == tb.rows.length){
			if(tds[i].getAttribute('fIdx')==null){
				alert('请设置要显示的列数据！');
				if(selectedTD != null)
					cellBgColor(false);
				tds[i].bgColor = "#CCCCCC";
				return false;
			}
		}
	}

	return true;
}

function filteHTMLCode(editorObj){
	if(!editorObj)
		return;
		
	var tb = editorObj.document.body.firstChild;
	var tds = tb.getElementsByTagName("TD");

	//检验所有的td
	for(var i=0;i<tds.length;i++){
		if(!tds[i].innerText){
			tds[i].innerText = " ";
			//alert(tds[i].innerText);
		}
	}
}

function getXMLCode(editorObj){
	//alert("getXMLCode...");
	if(!checkHTMLCode(editorObj))
		return;

	var htmlDoc = new ActiveXObject("Microsoft.XMLDOM");
	htmlDoc.async = false;
	var initXml = "<root><" + titleTagName + ">" + this.titleCtr.value + "</" + titleTagName + "><subtitle>" + this.subTitleCtr.value + "</subtitle></root>";
	htmlDoc.loadXML(initXml);
	assembleXML(editorObj.document.body.firstChild, htmlDoc.documentElement);
	//alert(htmlDoc.xml);
	return htmlDoc.documentElement.xml;
}

function assembleXML(htmlNode, xmlNode){
	var NewNode = xmlDoc.createElement(htmlNode.tagName);
	xmlNode.appendChild(NewNode);

	//如果是单元格，则需要取属性
	if(htmlNode.tagName == "TD"){
		if(htmlNode.getAttribute("align"))
			NewNode.setAttribute("align", htmlNode.getAttribute("align"));

		if(htmlNode.getAttribute("fIdx"))
			NewNode.setAttribute("fIdx", htmlNode.getAttribute("fIdx"));

		if(htmlNode.getAttribute("colSpan"))
			NewNode.setAttribute("colSpan", htmlNode.getAttribute("colSpan"));

		//if(htmlNode.getAttribute("width") && htmlNode.getAttribute("colSpan") == "1")
		if(htmlNode.getAttribute("width"))
			NewNode.setAttribute("width", htmlNode.getAttribute("width"));

		if(htmlNode.getAttribute("rowSpan"))
			NewNode.setAttribute("rowSpan", htmlNode.getAttribute("rowSpan"));

		//NewNode.text = (trim(htmlNode.innerText)=="")?" ":trim(htmlNode.innerText);
		NewNode.text = htmlNode.innerText;
	}
	else if(htmlNode.tagName == "TABLE"){
		if(htmlNode.getAttribute("border"))
			NewNode.setAttribute("border", htmlNode.getAttribute("border"));
	}

	for(var i = 0; i < htmlNode.childNodes.length; i++){
		if(htmlNode.childNodes[i].tagName){
			assembleXML(htmlNode.childNodes[i], NewNode);
		}
	}
}

function toViewXML(){
	var data = this.getFormateData();
	//alert(data);
	if(!data){
		return;
	}

	var xml = "<?xml version='1.0' encoding='GBK' ?>";
	xml += "<root>";
	xml += "<Action>SaveView</Action>";
	xml += this.context.toXMLString();
	xml += "<XMLContent>";
	xml += ("<ColDesc><![CDATA[" + this.colDescContent + "]]></ColDesc>");
	xml += ("<Formate><![CDATA[" + data + "]]></Formate>");
	xml += "</XMLContent>";
	xml += "</root>";
	
	return xml;
}
/*********************************************************************************/
function getPrintContext(winObj){
	var urlPathName = winObj.document.location.pathname;
	//alert(urlPathName);
	var bIdx = urlPathName.lastIndexOf("/");
	var eIdx = urlPathName.lastIndexOf(".");
	var name = urlPathName.substring(bIdx + 1, eIdx);
	var path = urlPathName.substring(0, bIdx + 1);
	if(path.charAt("0") != "/"){
		path = "/" + path;
	}

	return new PrintContext(name, path);
}

	function postXML(xmlReq, showMsg){
		if(!xmlReq){
			return;
		}
		
		var reqDoc = new ActiveXObject("Microsoft.XMLHTTP");
		reqDoc.open("POST", "customprint.viewhigh", false);
		reqDoc.send(xmlReq);
		//alert(reqDoc.responseXML.xml);
		
		return msg(reqDoc.responseXML, showMsg);
	}
	
	function msg(node, showMsg){
		var errNode = node.selectSingleNode("/root/error");
		if(errNode.text){
			alert(errNode.text);
			return false;
		}

		if(!showMsg)
			return true;
			
		var msgNode = node.selectSingleNode("/root/msg");
		if(msgNode.text){
			alert(msgNode.text);
			return true;
		}
		
		return false;
	}
	