var sMenuHr="<tr><td align=center valign=middle height=2><TABLE border=0 cellpadding=0 cellspacing=0 width="+(lang["UIMenuWidth"]-22)+" height=2><tr><td height=1 class=HrShadow><\/td><\/tr><tr><td height=1 class=HrHighLight><\/td><\/tr><\/TABLE><\/td><\/tr>";
var sMenu1="<TABLE border=0 cellpadding=0 cellspacing=0 class=Menu width="+lang["UIMenuWidth"]+"><tr><td width=18 valign=bottom align=center><\/td><td width="+(lang["UIMenuWidth"]-18)+" class=RightBg><TABLE border=0 cellpadding=0 cellspacing=0>";
var sMenu2="<\/TABLE><\/td><\/tr><\/TABLE>";

var menuStyle = "<head><link href='css/menuarea.css' type='text/css' rel='stylesheet'></head><body scroll='no' onConTextMenu='event.returnValue=false;'>";
var oPopupMenu = window.createPopup();

function showContextMenu(event){
	var width = lang["UIMenuWidth"];
	var height = 0;
	var lefter = event.clientX;
	var topper = event.clientY;

	var oPopDocument = oPopupMenu.document;
	var oPopBody = oPopupMenu.document.body;

	var sMenu="";
	
	if (isCursorInTableCell()){
		sMenu += getTableMenuRow("TableCell");
	///sMenu += sMenuHr;
		height += 225;
	}

	if(!sMenu){
		return false;
	}
	
	sMenu = sMenu1 + sMenu + sMenu2;

	oPopDocument.open();
	oPopDocument.write(menuStyle + sMenu);
	oPopDocument.close();

	height+=2;
	if(lefter+width > document.body.clientWidth) lefter=lefter-width;
	//if(topper+height > document.body.clientHeight) topper=topper-height;

	oPopupMenu.show(lefter, topper, width, height, editor.document.body);
	return false;
}

function getTableMenuRow(what){
	var s_Menu = "";
	var s_Disabled = "disabled";
	switch(what){
	case "TableCell":
		if (isCursorInTableCell()) 
		s_Disabled="";
		s_Menu += getMenuRow(s_Disabled, "TableColData()", "tablecoldata.gif", lang["TableColData"])
		s_Menu += sMenuHr;
		
		//s_Menu += getMenuRow(s_Disabled, "TableCellProp()", "tablecellprop.gif", lang["TableCellProp"])
		//s_Menu += getMenuRow(s_Disabled, "TableCellSplit()", "tablecellsplit.gif", lang["TableCellSplit"])
		//s_Menu += sMenuHr;
		//s_Menu += getMenuRow(s_Disabled, "TableRowProp()", "tablerowprop.gif", lang["TableRowProp"])
		
		//向上插入行，在普通情况下都可以
		s_Disabled="";
		s_Menu += getMenuRow(s_Disabled, "TableRowInsertAbove()", "tablerowinsertabove.gif", lang["TableRowInsertAbove"]);
		
		//向下插入行，最后一行不允许
		if (selectedTR.rowIndex +1 == selectedTable.rows.length) 
			s_Disabled="disabled";
		else
			s_Disabled="";
			
		s_Menu += getMenuRow(s_Disabled, "TableRowInsertBelow()", "tablerowinsertbelow.gif", lang["TableRowInsertBelow"]);
		//向下合并行 同上		
		s_Menu += getMenuRow(s_Disabled, "TableRowMerge()", "tablerowmerge.gif", lang["TableRowMerge"]);
		
		//拆分行 同上
		s_Menu += getMenuRow(s_Disabled, "TableRowSplit(2)", "tablerowsplit.gif", lang["TableRowSplit"]);
		
		//删除行 同上
		if (selectedTR.rowIndex +1 == selectedTable.rows.length) 
			s_Disabled="disabled";
		else
			s_Disabled="";		
		s_Menu += getMenuRow(s_Disabled, "TableRowDelete()", "tablerowdelete.gif", lang["TableRowDelete"]);
		
		s_Menu += sMenuHr;
		
		s_Disabled="";	
		//向左插入列
		s_Menu += getMenuRow(s_Disabled, "TableColInsertLeft()", "tablecolinsertleft.gif", lang["TableColInsertLeft"]);
		//向右插入列
		s_Menu += getMenuRow(s_Disabled, "TableColInsertRight()", "tablecolinsertright.gif", lang["TableColInsertRight"]);
		
		//向右合并列
		if (selectedTD.cellIndex+1==selectedTR.cells.length) 
			s_Disabled="disabled";
		else
			s_Disabled="";		
		s_Menu += getMenuRow(s_Disabled, "TableColMerge()", "tablecolmerge.gif", lang["TableColMerge"]);
		
		s_Disabled="";	
		s_Menu += getMenuRow(s_Disabled, "TableColSplit(2)", "tablecolsplit.gif", lang["TableColSplit"]);
		
		//删除列
		if(selectedTR.cells.length==1)
			s_Disabled="disabled";
		else
			s_Disabled="";							
		s_Menu += getMenuRow(s_Disabled, "TableColDelete()", "tablecoldelete.gif", lang["TableColDelete"]);
		//s_Menu += sMenuHr;
		
		break;
	}
	return s_Menu;
}

function getMenuRow(s_Disabled, s_Event, s_Image, s_Html) {
	var s_MenuRow = "";
	s_MenuRow = "<tr><td align=center valign=middle><TABLE border=0 cellpadding=0 cellspacing=0 width="+(lang["UIMenuWidth"]-18)+"><tr "+s_Disabled+"><td valign=middle height=20 class=MouseOut onMouseOver=this.className='MouseOver'; onMouseOut=this.className='MouseOut';";
	if (s_Disabled==""){
		s_MenuRow += " onclick=\"parent."+s_Event+";parent.oPopupMenu.hide();\"";
	}
	s_MenuRow += ">"
	/*if (s_Image !=""){
		s_MenuRow += "&nbsp;<img border=0 src='buttonimage/"+config.ButtonDir+"/"+s_Image+"' width=20 height=20 align=absmiddle "+s_Disabled+">&nbsp;";
	}else{
		s_MenuRow += "&nbsp;";
	}*/
	s_MenuRow += "&nbsp;";
	s_MenuRow += s_Html+"<\/td><\/tr><\/TABLE><\/td><\/tr>";
	return s_MenuRow;
}