<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="gb2312"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="com.viewhigh.base.util.Preference"%><html
	xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8,9,10,11"/>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>医院综合运营管理系统</title>
<link href="css/login_init.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript"
	src="/base/scripts/base64.js"></script>
<script type="text/javascript" language="javascript"
	src="/base/scripts/login.js"></script>
<script type="text/javascript" language="javascript"
	src="/base/scripts/login_init.js"></script>
<script type="text/javascript" language="javascript">
	function entryKey(evt){
		if(evt.keyCode==13){
			if(document.getElementById("userPwd").value==""){
				setTimeout("document.getElementById('userPwd').focus();",30);
				return;
				}
			//document.getElementById("submitBtn").click();
			document.getElementById("login").click();
		}
	}
	g_theme="default";

	function openOther(){
		/*
		var expires = new Date();
		expires.setTime(expires.getTime() + 3 * 30 * 24 * 60 * 60 * 1000);
		if(savePwd.checked==true){
			document.cookie="user=#vhuser#"+userName.value+"#/vhuser#,pwd=#vhpwd#"+userPwd.value+"#/vhpwd#,save=#vhsav#"+savePwd.checked+"#/vhsav#,theme=#theme#style2#/theme#;path=/;expires="+expires.toGMTString();
		}else{
			document.cookie="user=#vhuser#"+userName.value+"#/vhuser#,pwd=#vhpwd##/vhpwd#,save=#vhsav#"+savePwd.checked+"#/vhsav#,theme=#theme#style2#/theme#;path=/;expires="+expires.toGMTString();
		}
		*/
		document.location = "../style2/login.jsp";
	}
	var dogState = 0;

    function _loginSys(){

    	if(typeof _readDog=='function'){
    		_readDog();
    	}
        if(dogState==0){
    		login(document.getElementById('userName'),document.getElementById('userPwd'),document.getElementById('savePwd'),document.getElementById('md5'));
        }
   }
</script>
</head>

<div style="display: none;">
<%
	if(Preference.getDogCheckModle()==1){
%>
<object classid="clsid:8DE9546F-6D19-4068-B53A-7BEF6541D18A"
	id="WebSecurityKey" codebase="base/activex/WebSecurity.ocx">
	<param name="Visible" value="0">
	<param name="AutoScroll" value="0">
	<param name="AutoSize" value="0">
	<param name="AxBorderStyle" value="1">
	<param name="Caption" value="主窗体">
	<param name="Color" value="4278190095">
	<param name="Font" value="宋体">
	<param name="KeyPreview" value="0">
	<param name="PixelsPerInch" value="96">
	<param name="PrintScale" value="1">
	<param name="Scaled" value="-1">
	<param name="DropTarget" value="0">
	<param name="HelpFile" value>
	<param name="ScreenSnap" value="0">
	<param name="SnapBuffer" value="10">
	<param name="DoubleBuffered" value="0">
	<param name="Enabled" value="-1">
	<param name="URLSoftwareFirms" value>
</object>
	<script type="text/javascript" language="javascript">
	function _readDog(){
	      var ret;
	      //设置软件商号对应的URL
	      var wsObj = document.getElementById("WebSecurityKey");
		  if(wsObj){
			  try{
			      wsObj.URLSoftwareFirms='<%="http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()%>/security?SecurityKeyFlag=SoftwareFirms';
			      wsObj.URLHasPassword='<%="http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()%>/security?SecurityKeyFlag=HasPassword';
			      wsObj.URLPasswordRandom='<%="http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()%>/security?SecurityKeyFlag=PasswordRandom';
			      wsObj.URLBuffer='<%="http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()%>/security?SecurityKeyFlag=ReadBuffer';
			      ret=wsObj.Read_Main(0,179);

			      if(ret){
			      		ret = ret.substring(0,ret.indexOf(';'));
						dogState = 0;
			      }else{
			    	    dogState = 1;
						alert("没有检测到加密狗，请插入！");

			      }
		      }catch(e){alert("狗驱动异常！")}
		      document.getElementById("md5").value=ret;
		  }
	}
	</script>
<%
	}
%>
</div>


<body	onload="init(document.getElementById('userName'),document.getElementById('userPwd'),document.getElementById('savePwd'));login_init();">
<input type='hidden' id='md5' />
<div class="container">
	<div class="logo logoImg" id='logoImg'> <img src='images/newYear/loginNewYear.png'/></div>
	<div class="main">
	<h1><img src='images/newYear/title_viewh.png' id='logoImgTitle' onload="getHeight('logoImgTitle','27px')"></h1>
		<table>
			<tr><td style="text-align:center"><span id="title" style="font-family:微软雅黑;">用户登录</span></td></tr>
			<tr>
				<td>
					<div class="input">
						<div class="label_name">用户名</div>
						<div class="content">
							<input type="text" id="userName" name="userName" onkeydown="entryKey(window.event)" />
						</div>
						<div class="hover"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="input">
						<div class="label_pwd">密码</div>
						<div class="content">
							<input type="password" id="userPwd" name="password" onkeydown="entryKey(window.event)" />
						</div>
						<div class="hover"></div>
					</div>
				</td>
			</tr>
			<tr><td style="font-size:12px;"><input id="savePwd" name="savePwd" type="checkbox" value="" /><span style="font-family:微软雅黑;">记住密码</span>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="open('/employee_login.html', '', '')" style="font-family:微软雅黑;">个人工资查询</a></td></tr>
			<tr><td><a href="javascript:void(0);" id="login" onclick="_loginSys()" style="font-family:微软雅黑;">登 录</a></td></tr>
			<!--<tr><td><a href="javascript:void(0);" id="reset" style="font-family:微软雅黑;">重 置</a></td></tr>-->
			<tr><td><a href="#" style="float:left;font-size:12px;font-family:微软雅黑;" onclick="showCode()">扫描下载</a><a href="#" style="float:right;font-size:12px;font-family:微软雅黑;" onclick="open('/base/activex/vhInstall.exe', '', '')">组件安装</a></td></tr>
		</table>
	</div>

	<div class="footer">
		<div>
			<div class="wraper">
				<div class="code_wraper" onclick="hideCode()">
					<div class="qrcode">
						<div>
							<img src="images/app-integrate-release.png"/>
							<p>移动应用APP</p>
						</div>
						<!--<div>
							<img src="images/check.png">
							<p>盘点</p>
						</div>
						<div>
							<img src="images/pick.png"  >
							<p>拣货</p>
						</div>
						<div>
							<img src="images/verify.jpg"  >
							<p>验收</p>
						</div>
						<div>
							<img src="images/selfhelp-release.png"  >
							<p>薪酬查询</p>
						</div>-->
					</div>
				</div>
			<!--<a href="javascript:void(0);" id="qrcode_a" style="font-family:微软雅黑;">扫描下载安卓客户端</a>-->
			</div>
			<!--
			<span>|</span>
			<a href="#" style="font-family:微软雅黑;" onclick="open('/base/activex/vhInstall.exe', '', '')">组件安装</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family:微软雅黑;">客服电话：400-660-8860</span>
			-->
		</div>
		<div>
			<span style="font-family:微软雅黑;">北京东软望海科技有限公司&copy;版权所有</span>
		</div>
	</div>
</div>

</script>
</body>
</html>
