<?xml version="1.0" encoding="GB2312"?>
<sql-list>
<sql id="BonusControlTreat" type="proc"><![CDATA[
/*-------------------------------------------------------------------------
|| 过程名称 ：BonusControlTreat
|| 功能描述 ：对基本指标(源)进行上下限判断处理。
|| 参数描述 ：参数标识        名称                输入/输出    类型
||            -------------------------------------------------------------
||            Prm_Comp_Code        单位代码            输入       nvarchar 
||            prm_Type_Id          期间类型            输入       int
||            prm_Year             考核年度            输入       nvarchar
||            prm_Period           考核期间            输入       nvarchar  
||	      Prm_Factor_Target    基本指标(目标)      输入       nvarchar
||            Prm_Factor_Target    基本指标(上限)      输入       nvarchar
||            Prm_Factor_Target    基本指标(下限)      输入       nvarchar
||            Prm_Factor_Source    基本指标(源)        输入       nvarchar
|| 作    者 ：韩光      完成日期 ：2008-04-20
||-------------------------------------------------------------------------
|| 修改记录 ：
||-------------------------------------------------------------------------*/

                                                  
	@Prm_Comp_Code 				nvarchar(20)          , --单位代码      
	@Prm_Type_Id 				int                   , --期间类型      
	@Prm_Year                               nvarchar(20)          , --考核年度      
	@Prm_Period 				nvarchar(8)           , --考核期间
	@Prm_Factor_Target                      nvarchar(20)          , --基本指标(目标)
	@Prm_Factor_Upper                       nvarchar(20)          , --基本指标(上限)
        @Prm_Factor_floor                       nvarchar(20)          , --基本指标(下限)
        @Prm_Factor_Source                      nvarchar(20)            --基本指标(源)
   AS   	                                                                        
begin 
   
   --变量声明
    DECLARE   @str_dept_kind_code      	nvarchar(20)           --科室类别代码        
    DECLARE   @str_dept_code      	nvarchar(20)           --科室代码             
    DECLARE   @n_sequence_no            int                    --主表序列号
    DECLARE   @n_count_main        	int 		       --主表记录数
    DECLARE   @n_count_detail      	int 		       --明细表记录数
    DECLARE   @n_factor_value      	numeric(20,6)          --指标值    
    DECLARE   @n_upper_value            numeric(20,6)          --指标值(上限)
    DECLARE   @n_floor_value            numeric(20,6)          --指标值(下限)
    DECLARE   @n_Source_value           numeric(20,6)          --指标值(源)
    
    --定义科室游标
    DECLARE Cur_Bonus_Dept  CURSOR
    FOR 
       SELECT dept_kind_code,  --科室类别代码
              dept_code        --科室代码  
    	from v_bonus_dept 
       WHERE comp_code = @Prm_Comp_Code
        
   --初始化   
   SET @str_dept_kind_code              = '';
   SET @str_dept_code                   = '';
   SET @n_sequence_no			= 0 ;	 
   SET @n_count_main	      	        = 0 ;
   SET @n_count_detail                  = 0 ;
   SET @n_factor_value                  = 0 ;
   SET @n_upper_value                   = 0 ;
   SET @n_floor_value                   = 0 ;
   SET @n_Source_value                  = 0 ;    
   
   --判断基本指标录入主表是否存在值
   SELECT @n_count_main = count(*)
      FROM perf_factor_input
   WHERE comp_code     = @Prm_Comp_Code 
     AND type_id       = @Prm_Type_Id  
     AND perf_year     = @Prm_Year
     AND period_id     = @Prm_Period
     AND factor_code   = @Prm_Factor_Target   
     
   IF ( @n_count_main = 0 ) 
   BEGIN   
      --插入基本指标录入主表
      INSERT INTO perf_factor_input (            
                comp_code   ,         --单位代码
                type_id     ,         --期间类型
                perf_year   ,         --考核年度
                period_id   ,         --考核期间
                factor_code ,         --基本指标代码
                state_flag            --状态标识
                )
          VALUES(
                @Prm_Comp_Code      ,
                @Prm_Type_Id        , 
                @Prm_Year           , 
                @Prm_Period         ,
                @Prm_Factor_Target  ,'confirm'	 
          )  
   END         
  
   --提取基本指标录入主表序列号
   SELECT @n_sequence_no = sequence_no
      FROM perf_factor_input
   WHERE comp_code     = @Prm_Comp_Code 
     AND type_id       = @Prm_Type_Id  
     AND perf_year     = @Prm_Year
     AND period_id     = @Prm_Period
     AND factor_code   = @Prm_Factor_Target
     
   --打开科室游标	       
   open Cur_Bonus_Dept    
	     
   --提取科室代码
   fetch next from Cur_Bonus_Dept into @str_dept_kind_code,@str_dept_code
   
   while @@fetch_status = 0
   begin
            
      --提取上限指标值
      SELECT @n_upper_value = factor_value 
         FROM perf_factor_input_detail 
      WHERE perf_unit_code    = @str_dept_kind_code 
        AND dept_code         = @str_dept_code
        AND main_sequence_no  = (select sequence_no 
                                   from perf_factor_input
                                  where comp_code     = @Prm_Comp_Code 
				    AND type_id       = @Prm_Type_Id  
				    AND perf_year     = @Prm_Year
				    AND period_id     = @Prm_Period
				    AND factor_code   = @Prm_Factor_Upper
                              )
      --提取下限指标值
      SELECT @n_floor_value = factor_value 
         FROM perf_factor_input_detail 
      WHERE perf_unit_code    = @str_dept_kind_code 
        AND dept_code         = @str_dept_code
        AND main_sequence_no  = (select sequence_no 
                                   from perf_factor_input
                                  where comp_code     = @Prm_Comp_Code 
				    AND type_id       = @Prm_Type_Id  
				    AND perf_year     = @Prm_Year
				    AND period_id     = @Prm_Period
				    AND factor_code   = @Prm_Factor_Floor
                              )

      --提取下限指标值
      SELECT @n_Source_value = factor_value 
         FROM perf_factor_input_detail 
      WHERE perf_unit_code    = @str_dept_kind_code 
        AND dept_code         = @str_dept_code
        AND main_sequence_no  = (select sequence_no 
                                   from perf_factor_input
                                  where comp_code     = @Prm_Comp_Code 
				    AND type_id       = @Prm_Type_Id  
				    AND perf_year     = @Prm_Year
				    AND period_id     = @Prm_Period
				    AND factor_code   = @Prm_Factor_Source
                              )

      if (@n_Source_value >= @n_upper_value )
      begin
          set @n_factor_value = @n_upper_value
      end
      
      if (@n_Source_value < @n_upper_value  and @n_Source_value > @n_floor_value)
      begin
	   set @n_factor_value = @n_Source_value
      end 

      if (@n_Source_value <= @n_floor_value)
      begin
	   set @n_factor_value = @n_floor_value
      end 

      --判断基本指标录入明细表是否存在值
      SELECT @n_count_detail = count(*) 
         FROM perf_factor_input_detail 
      WHERE perf_unit_code    = @str_dept_kind_code 
        AND dept_code         = @str_dept_code
        AND main_sequence_no  = @n_sequence_no
      
      IF ( @n_count_detail = 0 )                
      BEGIN   
         --插入基本指标录入明细表
         INSERT INTO perf_factor_input_detail (     
                   main_sequence_no    ,   --序列号       
                   perf_unit_code      ,   --科室类别代码
                   dept_code           ,   --科室代码 
                   factor_value        ,   --因素值
                   state_flag          ,   --状态标识
                   factor_property         --因素标识 0:全院 1:科别 2:科室
                   )
             VALUES(
                   @n_sequence_no       ,
                   @str_dept_kind_code  ,
                   @str_dept_code       ,
                   @n_factor_value      , 
                   'confirm'	        ,
                   '2'
             ) 
      END
      ELSE
      BEGIN
         --更新基本指标录入明细表
         UPDATE  perf_factor_input_detail 
            SET   factor_value =  @n_factor_value
         WHERE  main_sequence_no = @n_sequence_no AND  perf_unit_code   = @str_dept_kind_code
           AND  dept_code        = @str_dept_code
           AND  factor_property  = '2'
      END  
     
     --提取科室代码
     fetch next from Cur_Bonus_Dept into @str_dept_kind_code,@str_dept_code    
  END  
  
    --关闭科室游标
    close Cur_Bonus_Dept
    deallocate Cur_Bonus_Dept 
END
]]></sql>
</sql-list>