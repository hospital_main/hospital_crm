<?xml version="1.0" encoding="GB2312"?>
<sql-list>
<sql id="BONUS_P_KSDF" type="proc"><![CDATA[
/*-------------------------------------------------------------------------
|| 过程名称 ：BONUS_P_KSDF
|| 功能描述 ：取科室核算奖金
|| 参数描述 ：参数标识        名称                输入/输出    类型
||            -------------------------------------------------------------
||            @Prm_Comp_Code       单位代码            输入       nvarchar 
||            @Prm_Type_Id  	     期间类型            输入       int
||            @Prm_Year            考核年度            输入       nvarchar
||            @Prm_Period          考核期间            输入       nvarchar 
||            @Prm_Item_Code       项目代码            输入       nvarchar 
||            @Prm_Factor          核算月份            输入       nvarchar
||
|| 作    者 ：韩光      完成日期 ：2008-04-28
||-------------------------------------------------------------------------
|| 修改记录 ：
||-------------------------------------------------------------------------*/  
   @Prm_Comp_Code 				nvarchar(20)          , --单位代码      
   @Prm_Type_Id 				  int                   , --期间类型      
   @Prm_Year              nvarchar(4)           , --考核年度      
   @Prm_Period 				    nvarchar(2)           , --考核期间
   @prm_Item_Code				  nvarchar(20)          , --项目代码
   @Prm_Factor            nvarchar(20)            --基本指标
   AS   	                                                                        
BEGIN
    --变量声明
    DECLARE   @str_dept_kind_code      	nvarchar(20)           --科室类别代码        
    DECLARE   @str_dept_code      	nvarchar(20)           --科室代码             
    DECLARE   @n_sequence_no            int                    --主表序列号
    DECLARE   @n_count_main        	int 		       --主表记录数
    DECLARE   @n_count_detail      	int 		       --明细表记录数
    DECLARE   @n_factor_value      	numeric(20,6)          --指标值    
    
    --定义科室游标
    DECLARE Cur_Bonus_Dept  CURSOR
    FOR 
       SELECT dept_kind_code,  --科室类别代码
              dept_code        --科室代码  
    	from v_bonus_dept 
       WHERE comp_code = @Prm_Comp_Code
    
    --初始化   
    SET @str_dept_kind_code              = '';
    SET @str_dept_code                   = '';
    SET @n_sequence_no										= 0 ;	 
    SET @n_count_main	      	        	= 0 ;
    SET @n_count_detail                  = 0 ;
    SET @n_factor_value                  = 0 ;
       
    --判断基本指标录入主表是否存在值
    SELECT @n_count_main = count(*)
      FROM perf_factor_input
     WHERE comp_code     = @Prm_Comp_Code 
       AND type_id       = @Prm_Type_Id  
       AND perf_year     = @Prm_Year
       AND (@Prm_Type_Id = 4 or (@Prm_Type_Id != 4  and period_id     = @Prm_Period))
       AND factor_code   = @Prm_Factor   
     
   IF ( @n_count_main = 0 ) 
   BEGIN   
      --插入基本指标录入主表
      INSERT INTO perf_factor_input (            
                comp_code   ,         --单位代码
                type_id     ,         --期间类型
                perf_year   ,         --考核年度
                period_id   ,         --考核期间
                factor_code ,         --基本指标代码
                state_flag            --状态标识
                )
          VALUES(
                @Prm_Comp_Code      ,
                @Prm_Type_Id        , 
                @Prm_Year           , 
                @Prm_Period         ,
                @Prm_Factor         , 
                'confirm'	 
          )  
   END         
  
   --提取基本指标录入主表序列号
   SELECT @n_sequence_no = sequence_no
      FROM perf_factor_input
   WHERE comp_code     = @Prm_Comp_Code 
     AND type_id       = @Prm_Type_Id  
     AND perf_year     = @Prm_Year
     AND (@Prm_Type_Id = 4 or (@Prm_Type_Id != 4  and period_id     = @Prm_Period))
     AND factor_code   = @Prm_Factor
     
   --打开科室游标	       
   open Cur_Bonus_Dept    
	     
   --提取科室代码
   fetch next from Cur_Bonus_Dept into @str_dept_kind_code,@str_dept_code
   
   while @@fetch_status = 0
   begin
      
   --提取科室待分配奖金额
   SELECT @n_factor_value = Round(isnull(pend_distribute_money,0),2)
	   FROM bonus_pool_dept
    WHERE comp_code      = @Prm_Comp_Code
      AND type_id        = @Prm_Type_Id
	    AND bonus_year     = @Prm_Year
	    AND (@Prm_Type_Id = 4 or (@Prm_Type_Id != 4  and bonus_period   = @Prm_Period))
	    AND dept_kind_code = @str_dept_kind_code
	    AND dept_code      = @str_dept_code 
	    AND item_code      = @Prm_Item_Code
      AND item_property  = '03'   
      
      --判断基本指标录入明细表是否存在值
      SELECT @n_count_detail = count(*) 
         FROM perf_factor_input_detail 
      WHERE perf_unit_code    = @str_dept_kind_code 
        AND dept_code         = @str_dept_code
        AND main_sequence_no  = @n_sequence_no  
      
      IF ( @n_count_detail = 0 )                
      BEGIN   
         --插入基本指标录入明细表
         INSERT INTO perf_factor_input_detail (     
                   main_sequence_no    ,   --序列号       
                   perf_unit_code      ,   --科室类别代码
                   dept_code           ,   --科室代码
                   factor_value        ,   --因素值
                   state_flag          ,   --状态标识
                   factor_property         --因素标识 0:全院 1:科别 2:科室
                   )
             VALUES(
                 	@n_sequence_no       ,
                  @str_dept_kind_code  ,
                  @str_dept_code       ,
                  @n_factor_value      , 
                  'confirm'	           ,
                  '2'
             ) 
      END
      ELSE
      BEGIN
         --更新基本指标录入明细表
         UPDATE  perf_factor_input_detail 
              SET   factor_value =  @n_factor_value
         WHERE  main_sequence_no = @n_sequence_no
           AND  perf_unit_code   = @str_dept_kind_code
           AND  dept_code        = @str_dept_code
           AND  factor_property  = '2'
      END  
     
     --提取科室代码
     fetch next from Cur_Bonus_Dept into @str_dept_kind_code,@str_dept_code    
  END  
  
    --关闭科室游标
    close Cur_Bonus_Dept
    deallocate Cur_Bonus_Dept  
                        
END
]]></sql>
</sql-list>