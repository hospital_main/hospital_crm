-- =============================================
-- Create table function (TF)
-- =============================================
IF EXISTS (SELECT * 
	   FROM   sysobjects 
	   WHERE  name = N'getGrade')
	DROP FUNCTION getGrade
GO

CREATE FUNCTION getGrade(@table nvarchar(20),@top nvarchar(20))
RETURNS @result TABLE (order_id int  identity(1,1),code nvarchar(20),name nvarchar(30),level int,signs int)
AS
BEGIN

	 DECLARE @current char(20)
		SET @current =@top
	 DECLARE @name char(20)
		set @name=''
	 DECLARE @level int, @line char(20) 
		SET @level = 1

  if(@table='dict_acct_dept') 
	begin
	delete @result

	 SELECT @name=dept_name from dict_acct_dept where dept_code=@current
	 INSERT INTO @result VALUES (@current,@name,1,0)

	 WHILE @level > 0 
	 BEGIN 	
		IF EXISTS (SELECT * FROM @result WHERE level = @level and signs=0)     
	 		BEGIN	
				SELECT top 1 @current = code,@name=name FROM @result WHERE level = @level and signs=0    
	 			INSERT  @result (code,name,level,signs) values( @current,@name,@level,1)      
	  		DELETE  @result WHERE level = @level AND code = @current and signs=0
	 			INSERT  @result SELECT dept_code,dept_name,@level + 1,0 FROM dict_acct_dept WHERE supper_dept = @current --and app_level<>1 and app_level<>5      
	  		IF @@ROWCOUNT > 0	
					SELECT @level = @level + 1     
	 		END	
		ELSE	
			SELECT @level = @level - 1      
	  END
	end
	else 
	if(@table='dict_cost_subj')
	begin
--select * from dict_cost_subj
	delete @result

	 SELECT @name=cost_subj_name from dict_cost_subj where cost_subj_code=@current
	 INSERT INTO @result VALUES (@current,@name,1,0)

	 WHILE @level > 0 
	 BEGIN 	
		IF EXISTS (SELECT * FROM @result WHERE level = @level and signs=0)     
	 		BEGIN	
				SELECT top 1 @current = code,@name=name FROM @result WHERE level = @level and signs=0    
	 			INSERT  @result (code,name, level,signs) values( @current,@name,@level,1)      
	  		DELETE  @result WHERE level = @level AND code = @current and signs=0
	 			INSERT  @result SELECT cost_subj_code,cost_subj_name,@level + 1,0 FROM dict_cost_subj WHERE supp_item_code = @current
	  		IF @@ROWCOUNT > 0	
					SELECT @level = @level + 1     
	 		END	
		ELSE	
			SELECT @level = @level - 1      
	  END

	end
delete from @result where code='top'
RETURN 
END
GO