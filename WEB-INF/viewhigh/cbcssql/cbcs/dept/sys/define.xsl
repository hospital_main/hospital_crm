<?xml version="1.0" encoding="gb2312"?>
<sql-list>
  <sql id="cbcsDeptSysSettingDefine_select" desc="行政科室收入成本定义查询"><![CDATA[
  DECLARE @fromdate NVARCHAR(8)
  DECLARE @fromdatey NVARCHAR(8)
  DECLARE @enddate NVARCHAR(8)
  DECLARE @enddatey NVARCHAR(8)
  
  SET @fromdate= ?
  SET @fromdatey=?  
  SET @enddate= ?
  SET @enddatey=?
  
  select '1','1','1'
]]></sql>
<sql id="cbcsDeptSysSettingDefine_update" desc="行政科室收入成本定义更新"><![CDATA[
  DECLARE @fromdate NVARCHAR(8)
  DECLARE @fromdatey NVARCHAR(8)
  DECLARE @enddate NVARCHAR(8)
  DECLARE @enddatey NVARCHAR(8)
  
  SET @fromdate= ?
  SET @fromdatey=?  
  SET @enddate= ?
  SET @enddatey=?

]]></sql>

<sql id="cbcsDeptSysSettingDefine_update_load" desc="行政科室收入成本定义更新_加载"><![CDATA[
  DECLARE @fromdate NVARCHAR(8)
  DECLARE @fromdatey NVARCHAR(8)
  DECLARE @enddate NVARCHAR(8)
  DECLARE @enddatey NVARCHAR(8)

]]></sql>
<sql id="cbcsDeptSysSettingDefine_insert" desc="行政科室收入成本定义保存"><![CDATA[

]]></sql>

<sql id="cbcsDeptSysSettingDefine_delete" desc="行政科室收入成本定义删除"><![CDATA[


]]></sql>
 
</sql-list>