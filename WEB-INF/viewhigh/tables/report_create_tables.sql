create table acct_report_check (
   report_check_code    nvarchar(20)         not null,
   report_check_name    nvarchar(100)        null
)
go


alter table acct_report_check
   add constraint PK_ACCT_REPORT_CHECK primary key  (report_check_code)
go


create table acct_report_check_detail (
   check_item_id        int                  identity(1 , 1),
   report_check_code    nvarchar(20)         not null,
   check_item_name      nvarchar(100)        null,
   check_item_formula_name nvarchar(1500)       not null,
   check_item_formula   nvarchar(1500)       not null
)
go


alter table acct_report_check_detail
   add constraint PK_ACCT_REPORT_CHECK_DETAIL primary key  (check_item_id)
go


create table report_def (
   report_code          nvarchar(20)         not null,
   report_name          nvarchar(100)        not null,
   head_num             int                  null default 0,
   report_type          char(1)              null
)
go


alter table report_def
   add constraint PK_REPORT_DEF primary key  (report_code)
go


create table report_def_data (
   report_code          nvarchar(20)         not null,
   data_period          nvarchar(100)        not null,
   head_num             int                  null default 0,
   dept_custom_code     varchar(30)          not null
)
go


alter table report_def_data
   add constraint PK_REPORT_DEF_DATA primary key  (report_code, data_period, dept_custom_code)
go


create table report_def_detail (
   report_code          nvarchar(20)         not null,
   row                  int                  not null,
   col                  int                  not null,
   rowspan              int                  not null default 1,
   colspan              int                  not null default 1,
   formula              varchar(3500)         default '',
   style                varchar(3500)         default '',
   format               int                   default 0,
   type                 int                  null
)
go


alter table report_def_detail
   add constraint PK_REPORT_DEF_DETAIL primary key  (report_code, row, col)
go


create table report_def_detail_data (
   report_code          nvarchar(20)         not null,
   data_period          nvarchar(100)        not null,
   dept_custom_code     varchar(30)          not null,
   row                  int                  not null,
   col                  int                  not null,
   rowspan              int                  not null default 1,
   colspan              int                  not null default 1,
   data                 varchar(3500)        not null default '',
   style                varchar(3500)        not null default '',
   format               int                  not null default 0,
   src_row              int                  null,
   src_col              int                  null,
   is_last              bit                  null,
   type                 int                  null
)
go


alter table report_def_detail_data
   add constraint PK_REPORT_DEF_DETAIL_DATA primary key  (report_code, data_period, dept_custom_code, row, col)
go


alter table acct_report_check_detail
   add constraint " FK_acct_report_check_detail_acct_report_check" foreign key (report_check_code)
      references acct_report_check (report_check_code)
go


alter table report_def_data
   add constraint FK_acct_def_data_acct_def foreign key (report_code)
      references report_def (report_code)
go


alter table report_def_detail
   add constraint FK_acct_def_detail_acct_def foreign key (report_code)
      references report_def (report_code)
go


alter table report_def_detail_data
   add constraint FK_acct_def_detail_data_acct_def_data foreign key (report_code, data_period, dept_custom_code)
      references report_def_data (report_code, data_period, dept_custom_code)
go

--自定义报表科室权限设置
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[report_sd_right]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[report_sd_right]
GO

CREATE TABLE [dbo].[report_sd_right] (
	[report_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[group_id] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[is_limit] [bit] NOT NULL 
) ON [PRIMARY]
GO

--自定义科室成本分摊表
create table te_st_dept_cost_clinic_shel(
year_month char(6),--月份
dept_code nvarchar(20),--科室编码
cost_subj_code nvarchar(20)  , 
apport_cost_t money NOT NULL default 0 ,--分摊
apport_cost_f money NOT NULL default 0 ,
CONSTRAINT  PK_te_st_dept_cost_clinic_shel primary key  (year_month,cost_subj_code,dept_code) 
)

go
  
create TABLE dbo.acct_report_dept_data 
	(
	year_month char(6) NOT NULL,
	dept_code char(20) NOT NULL,
	prime_cost money NOT NULL default 0,--直接记入成本
	apport_direct_cost money NOT NULL default 0,--分摊直接成本002
	public_cost money NOT NULL default 0,--公用成本003
	manage_cost  money NOT NULL default 0, --管理成本004
	apport_cost_s money NOT NULL default 0 ,--管理成本一次分摊004
	apport_cost_t money NOT NULL default 0, --医辅成本005
	apport_cost_f money NOT NULL default 0, --医技成本006
	fixed_cost money NOT NULL default 0, --固定成本007 
	dy_cost money NOT NULL default 0 ,--变动成本008
	control_cost money NOT NULL default 0 ,--可控成本009
	uncontrol_cost money NOT NULL default 0 ,--不可控成本010 
	
	manpower_cost money NOT NULL default 0 ,--人力成本012
	retire_cost money NOT NULL default 0 ,--离退休人员成本013
	material_cost money NOT NULL default 0 ,--材料成本014
	pure_M_cost money NOT NULL default 0 ,--净药品成本015
	depreciation_cost money NOT NULL default 0 ,--折旧成本016
	other_cost money NOT NULL default 0 ,--其它成本017
	O_T_income money NOT NULL default 0 ,--门诊医疗收入018
	I_T_income money NOT NULL default 0 ,--住院医疗收入019
	O_M_income money NOT NULL default 0 ,--门诊药品收入020
	I_M_income money NOT NULL default 0 ,--门诊药品收入021
	fund_subsidy_income money NOT NULL default 0 ,--财政补助收入022
	self_exec_income money NOT NULL default 0 ,--本科开单本科执行的收入023

	CONSTRAINT  PK_acct_report_dept_data primary key  (year_month,dept_code)
)   
GO

alter TABLE [dict_clinic_dept] add function_kind char(1) null,function_code char(1) null

go

alter table dict_clinic_dept_map add ident_name varchar(40) null,order_no int not null,dept_type char(1) null