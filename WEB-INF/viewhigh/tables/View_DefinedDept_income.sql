Drop View Report_defineDept_income
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO 
create    view Report_defineDept_income  as 
  -- wsj edition  2.14
 select base.year_month,dept_clin_code,base.dept_code,dept_type,base.charge_kind_code, dict.out_or_in,dict.app_level,
	dept_prop.dept_class_code dept_proper, isnull(direct_income,0) direct_income,isnull(indirect_income,0) indirect_income 
from ( --在自定义科室后面列举出处所有年月，以便后续处理
select a.dept_clin_code,charge_kind_code,a.dept_code,dept_type,b.year_month from dict_clinic_dept_map a,
	(
	select distinct year_month,dict.charge_kind_code from  st_dept_income ,b_dict_charge_detail_kind dict
	)b 
)
 base
	left join 
	(	-- 选择出来所有科室的直接收入
		select year_month,perform_by,charge_kind_code,sum(amount) direct_income from st_dept_income   
			-- where year_month =  200510  
		group by perform_by,year_month,charge_kind_code
	)direct_I
		 on base.dept_code = direct_I.perform_by and base.year_month = direct_I.year_month 
			and base.charge_kind_code =direct_I.charge_kind_code
	left join
	(	-- 选择出来所有门诊，住院科室的间接收入
		select year_month,charge_kind_code,income.ordered_by,sum(amount) indirect_income
			 from st_dept_income income,dict_acct_dept dict,dict_clinic_dept_map map1 ,dict_clinic_dept_map map2			
		   where  dict.dept_code = income.perform_by and income.perform_by = map1.dept_code and income.ordered_by = map2.dept_code	
				and map1.dept_clin_code <> map2.dept_clin_code --dict.app_level='5'and 是否应该加上此条件，是不是只有5级科室才有间接收入？
				group by income.ordered_by,year_month,charge_kind_code
	)indirect_I 
		on base.dept_code =   indirect_I.ordered_by and direct_I.year_month = indirect_I.year_month 
				and base.charge_kind_code = indirect_I.charge_kind_code   
 	left join 
 	( --得到科室的属性，东西院
		select  dept_code,class.dept_class_code 
				from te_dept_class_map map ,te_dict_dept_class class
		 where map.dept_class_Code = class.dept_class_code
	)dept_prop
		on base.dept_code =dept_prop.dept_code	
 	,dict_acct_dept dict 
	where base.dept_code = dict.dept_code 
/*
 select base.year_month,dept_clin_code,base.dept_code, dict.out_or_in,dict.app_level,dict_acct_dept.dept_proper, 
	isnull(direct_income,0) direct_income,isnull(indirect_income,0) indirect_income 
from ( --在自定义科室后面列举出处所有年月，以便后续处理
	select a.dept_clin_code,a.dept_code,b.year_month from dict_clinic_dept_map a,
	(
	select distinct year_month from  st_dept_cost 
	)b
)
 base
	left join
	(	-- 选择出来所有科室的直接收入
		select year_month,perform_by,sum(amount) direct_income from st_dept_income   
			-- where year_month =  200510  
		group by perform_by,year_month
	)direct_I
		on base.dept_code = direct_I.perform_by and base.year_month = direct_I.year_month
	left join
	(	-- 选择出来所有门诊，住院科室的间接收入
		select  year_month,income.ordered_by,sum(amount) indirect_income
			 from st_dept_income income,dict_acct_dept dict,dict_clinic_dept_map map1 ,dict_clinic_dept_map map2			
		   where  dict.dept_code = income.perform_by and income.perform_by = map1.dept_code and income.ordered_by = map2.dept_code	
				and map1.dept_clin_code <> map2.dept_clin_code --dict.app_level='5'and 是否应该加上此条件，是不是只有5级科室才有间接收入？
				group by income.ordered_by,year_month
	)indirect_I
		on base.dept_code = indirect_I.ordered_by and base.year_month = indirect_I.year_month 
  	left join dict_acct_dept
  on base.dept_code = dict_acct_dept.dept_code
 	,dict_acct_dept dict 
	where base.dept_code = dict.dept_code 
*/
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

