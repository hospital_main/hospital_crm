 

	declare @year_month nvarchar(6) 
	set @year_month = ?
	
	drop table #subj_level
	drop table #stack
	drop table #result
	create table #result(
	id int,
	cost_subj_code nvarchar(20),
	level int,
	supp_item_code nvarchar(20),
	year_month nvarchar(6),
	tot_amount money,
	prime_cost money,
	adm_cost money,
	apport_cost_s money,
	apport_cost_t money,
	apport_cost_f money
	) 

	CREATE TABLE #subj_level( 
		id int IDENTITY(1,1),
		cost_subj_code char(20),
		level int
	)
	declare @current char(20)
	set @current = 'TOP'
	DECLARE @level int, @line char(20)
	CREATE TABLE #stack (item char(20), level int)
	INSERT INTO #stack VALUES (@current, 1)
	SELECT @level = 1
	 WHILE @level > 0
	BEGIN
	   IF EXISTS (SELECT * FROM #stack WHERE level = @level)
	      BEGIN
	 SELECT top 1 @current = item
	           FROM #stack
	           WHERE level = @level
	
	 INSERT  #subj_level ( cost_subj_code, level) values( @current,@level) 
	      DELETE FROM #stack
	           WHERE level = @level
	             AND item = @current
	         INSERT #stack 
	
	          SELECT cost_subj_code, @level + 1
	             FROM dict_cost_subj
	               WHERE supp_item_code = @current     
	 
	           IF @@ROWCOUNT > 0
	            SELECT @level = @level + 1
	      END
	   ELSE
	      SELECT @level = @level - 1
	END
  
declare cur_month  cursor
	for --选出该分摊月份及以前没有被整理的月份的月份信息
 --可以改进,不用游标来计算???
	select distinct year_month from st_dept_cost 
		where year_month not in (select distinct year_month from report_dept_cost_subj)
					and year_month <= @year_month 
			order by year_month    
	open cur_month
	fetch next from cur_month into @year_month
	while @@fetch_status=0
	begin
	delete from #result -- 晴空数据表#temp
	insert into #result
	select  #subj_level.*,supp_item_code,@year_month year_month,isnull(tot_amount,0) tot_amount, isnull(prime_cost,0) prime_cost, isnull(adm_cost,0) adm_cost,
				isnull(apport_cost_s,0) apport_cost_s,isnull(apport_cost_t,0) apport_cost_t,
				isnull(apport_cost_f,0) apport_cost_f 
	 from #subj_level
	left join  
		(
		select year_month,cost_subj_code,sum(tot_amount) tot_amount,sum(prime_cost) prime_cost, sum(adm_cost) adm_cost,
			sum(apport_cost_s) apport_cost_s,	sum(apport_cost_t) apport_cost_t,sum(apport_cost_f)apport_cost_f from  st_dept_cost
		  where year_month = @year_month  --@year_month 
		group by cost_subj_code,year_month  
		) data
		on  #subj_level.cost_subj_code =  data.cost_subj_code
	left join dict_cost_subj dict
	on #subj_level.cost_subj_code =  dict.cost_subj_code 
	  
	 	 select @level=max(level) from #result--取得最大级别
		   while (@level>=1)
		    begin	    
		     update #result set prime_cost=mid.prime_cost , adm_cost = mid.adm_cost , apport_cost_s = mid.apport_cost_s ,
						apport_cost_t= mid.apport_cost_t , apport_cost_f = mid.apport_cost_f
		      from (select supp_item_code,sum(prime_cost) prime_cost ,sum(adm_cost) adm_cost , sum(apport_cost_s) apport_cost_s , 
				sum(apport_cost_t) apport_cost_t , sum(apport_cost_f) apport_cost_f  
		              from #result 
		              where level=@level 
		              group by supp_item_code
		            ) mid --按照级别进行汇总
		      where #result.cost_subj_code=mid.supp_item_code       
		    
		    set @level=@level-1
		    end
	insert into report_dept_cost_subj 
	select year_month,cost_subj_code,tot_amount,prime_cost,adm_cost,apport_cost_s,apport_cost_t,apport_cost_f from #result
	
	fetch next from cur_month into @year_month
  
	end 
close cur_month
deallocate cur_month 
 
 