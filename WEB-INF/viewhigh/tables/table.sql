
--表

IF EXISTS(SELECT name 
	  FROM 	 sysobjects 
	  WHERE  name = N'report_def_detail_data')
    DROP TABLE report_def_detail_data
GO

IF EXISTS(SELECT name 
	  FROM 	 sysobjects 
	  WHERE  name = N'report_def_data')
    DROP TABLE report_def_data
GO

IF EXISTS(SELECT name 
	  FROM 	 sysobjects 
	  WHERE  name = N'report_def_detail')
    DROP TABLE report_def_detail
GO

IF EXISTS(SELECT name 
	  FROM 	 sysobjects 
	  WHERE  name = N'report_def')
    DROP TABLE report_def
GO




create table report_def (
   report_code          nvarchar(20)         	not null,
   report_name          nvarchar(100)        	not null,
   head_num             int                  	null default 0,
	 report_row 					[varchar] (3500)  		NULL ,
	 report_col 					[varchar] (3500)  		NULL ,
   report_type          char(1)              	null
)
go


alter table report_def
   add constraint PK_REPORT_DEF primary key  (report_code)
go

create table report_def_data (
   report_code          nvarchar(20)         not null,
   data_period          nvarchar(100)        not null,
   head_num             int                  null default 0,
   dept_custom_code     varchar(30)          not null
)
go


alter table report_def_data
   add constraint PK_REPORT_DEF_DATA primary key  (report_code, data_period, dept_custom_code)
go



create table report_def_detail (
   report_code          nvarchar(20)         not null,
   row                  int                  not null,
   col                  int                  not null,
   rowspan              int                  not null default 1,
   colspan              int                  not null default 1,
   formula              varchar(3500)         default '',
   style                varchar(3500)         default '',
   format               int                   default 0,
   type                 int                  null
)
go


alter table report_def_detail
   add constraint PK_REPORT_DEF_DETAIL primary key  (report_code, row, col)
go

create table report_def_detail_data (
   report_code          nvarchar(20)         not null,
   data_period          nvarchar(100)        not null,
   dept_custom_code     varchar(30)          not null,
   row                  int                  not null,
   col                  int                  not null,
   rowspan              int                  not null default 1,
   colspan              int                  not null default 1,
   data                 varchar(3500)        not null default '',
   style                varchar(3500)        not null default '',
   format               int                  not null default 0,
   src_row              int                  null,
   src_col              int                  null,
   is_last              bit                  null,
   type                 int                  null
)
go


alter table report_def_detail_data
   add constraint PK_REPORT_DEF_DETAIL_DATA primary key  (report_code, data_period, dept_custom_code, row, col)
go

alter table report_def_data
   add constraint FK_acct_def_data_acct_def foreign key (report_code)
      references report_def (report_code)
go


alter table report_def_detail
   add constraint FK_acct_def_detail_acct_def foreign key (report_code)
      references report_def (report_code)
go


alter table report_def_detail_data
   add constraint FK_acct_def_detail_data_acct_def_data foreign key (report_code, data_period, dept_custom_code)
      references report_def_data (report_code, data_period, dept_custom_code)
go

--自定义报表科室权限设置
if exists (select * from sysobjects where id = object_id(N'[report_sd_right]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [report_sd_right]
GO

CREATE TABLE [report_sd_right] (
	[report_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[group_id] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[is_limit] [bit] NOT NULL 
) ON [PRIMARY]
GO
IF EXISTS(SELECT name 
	  FROM 	 sysobjects 
	  WHERE  name = N'te_st_dept_cost_clinic_shel')
    DROP TABLE te_st_dept_cost_clinic_shel
GO
--自定义科室成本分摊表
create table te_st_dept_cost_clinic_shel(
year_month char(6),--月份
dept_code nvarchar(20),--科室编码
cost_subj_code nvarchar(20)  , 
apport_cost_t money NOT NULL default 0 ,--分摊
apport_cost_f money NOT NULL default 0 ,
CONSTRAINT  PK_te_st_dept_cost_clinic_shel primary key  (year_month,cost_subj_code,dept_code) 
)

GO
--自定义科室主附表
if exists (select * from dbo.sysobjects where id = object_id(N'[report_dict_clinic_dept_map]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
drop table [report_dict_clinic_dept_map]
END
CREATE TABLE [report_dict_clinic_dept_map] (
	[dept_clin_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[dept_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[indent_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NULL ,
	[order_no] [int] NULL ,
	[dept_type] [char] (1) COLLATE Chinese_PRC_CI_AS NULL ,
	CONSTRAINT [FK_report_dict_clinic_dept_map_dict_acct_dept] FOREIGN KEY 
	(
		[dept_code]
	) REFERENCES [dict_acct_dept] (
		[dept_code]
	)
) ON [PRIMARY]
GO
if exists (select * from dbo.sysobjects where id = object_id(N'[report_dict_clinic_dept]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
drop table [report_dict_clinic_dept]
END
CREATE TABLE [report_dict_clinic_dept] (
	[dept_clin_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[dept_clin_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[clin_type] char(1),
	CONSTRAINT [PK_report_dict_clinic_dept] PRIMARY KEY  CLUSTERED 
	(
		[dept_clin_code]
	)  ON [PRIMARY] 
) ON [PRIMARY]

GO

IF EXISTS(SELECT name 
	  FROM 	 sysobjects 
	  WHERE  name = N'acct_report_dept_data')
    DROP TABLE acct_report_dept_data
GO
create TABLE acct_report_dept_data 
	(
	year_month char(6) NOT NULL,
	dept_code char(20) NOT NULL,
	prime_cost money NOT NULL default 0,--直接记入成本
	apport_direct_cost money NOT NULL default 0,--分摊直接成本002
	public_cost money NOT NULL default 0,--公用成本003
	manage_cost  money NOT NULL default 0, --管理成本004
	apport_cost_s money NOT NULL default 0 ,--管理成本一次分摊004
	apport_cost_t money NOT NULL default 0, --医辅成本005
	apport_cost_f money NOT NULL default 0, --医技成本006
	fixed_cost money NOT NULL default 0, --固定成本007 
	dy_cost money NOT NULL default 0 ,--变动成本008
	control_cost money NOT NULL default 0 ,--可控成本009
	uncontrol_cost money NOT NULL default 0 ,--不可控成本010 
	
	manpower_cost money NOT NULL default 0 ,--人力成本012
	retire_cost money NOT NULL default 0 ,--离退休人员成本013
	material_cost money NOT NULL default 0 ,--材料成本014
	pure_M_cost money NOT NULL default 0 ,--净药品成本015
	depreciation_cost money NOT NULL default 0 ,--折旧成本016
	other_cost money NOT NULL default 0 ,--其它成本017
	O_T_income money NOT NULL default 0 ,--门诊医疗收入018
	I_T_income money NOT NULL default 0 ,--住院医疗收入019
	O_M_income money NOT NULL default 0 ,--门诊药品收入020
	I_M_income money NOT NULL default 0 ,--门诊药品收入021
	fund_subsidy_income money NOT NULL default 0 ,--财政补助收入022
	self_exec_income money NOT NULL default 0 ,--本科开单本科执行的收入023
	
	
	O_T_cost money NOT NULL default 0 ,--门诊医疗成本024
	I_T_cost money NOT NULL default 0 ,--住院医疗成本025
	O_M_cost money NOT NULL default 0 ,--门诊药品成本026
	I_M_cost money NOT NULL default 0 ,--门诊药品成本027

	total_bed_used_days  int default 0,--床日数 041
	emp_num int default 0,						 --职工人数	042
	outp_num int default 0,						 --门诊人次 043

	CONSTRAINT  PK_acct_report_dept_data primary key  (year_month,dept_code)
)   

GO
if exists (select * from sysobjects where id = object_id(N'[report_dept_cost_subj]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [report_dept_cost_subj]
GO

CREATE TABLE [report_dept_cost_subj] (
	[year_month] nvarchar(6)   COLLATE Chinese_PRC_CI_AS NOT NULL,
	[cost_subj_code] [nvarchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL,
	[tot_amount] [money] NOT NULL ,
	[prime_cost] [money] NOT NULL ,
	[adm_cost] [money] NOT NULL ,
	[apport_cost_s] [money] NOT NULL ,
	[apport_cost_t] [money] NOT NULL ,
	[apport_cost_f] [money] NOT NULL ,
	otot_amount money NOT NULL,
	itot_amount money NOT NULL,
	tot_amount3 money NOT NULL,
	tot_amount4 money NOT NULL,
	ttot_amount money not null
) ON [PRIMARY]
GO
if exists (select * from sysobjects where id = object_id(N'[FK_REPORT_C_REFERENCE_REPORT_C]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [report_cols_detail] DROP CONSTRAINT FK_REPORT_C_REFERENCE_REPORT_C
GO

if exists (select * from sysobjects where id = object_id(N'[FK_REPORT_Cost_detail_REFERENCE_REPORT_Cost]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [report_cost_detail] DROP CONSTRAINT FK_REPORT_Cost_detail_REFERENCE_REPORT_Cost
GO

if exists (select * from sysobjects where id = object_id(N'[FK_REPORT_Dept_detail_REFERENCE_REPORT_Dept]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [report_dept_detail] DROP CONSTRAINT FK_REPORT_Dept_detail_REFERENCE_REPORT_Dept
GO

if exists (select * from sysobjects where id = object_id(N'[FK_REPORT_Income_detail_REFERENCE_REPORT_Income]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [report_income_custom_detail] DROP CONSTRAINT FK_REPORT_Income_detail_REFERENCE_REPORT_Income
GO

if exists (select * from sysobjects where id = object_id(N'[report_cols]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [report_cols]
GO

if exists (select * from sysobjects where id = object_id(N'[report_cost]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [report_cost]
GO

if exists (select * from sysobjects where id = object_id(N'[report_cost_detail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [report_cost_detail]
GO

if exists (select * from sysobjects where id = object_id(N'[report_dept]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [report_dept]
GO

if exists (select * from sysobjects where id = object_id(N'[report_dept_detail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [report_dept_detail]
GO

if exists (select * from sysobjects where id = object_id(N'[report_income_custom]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [report_income_custom]
GO

if exists (select * from sysobjects where id = object_id(N'[report_income_custom_detail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [report_income_custom_detail]
GO

CREATE TABLE [report_cols] (
	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[list_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [report_cost] (
	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[list_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[function_kind] [char] (1) COLLATE Chinese_PRC_CI_AS NULL ,
	[function_code] [char] (1) COLLATE Chinese_PRC_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [report_cost_detail] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[cost_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[indent_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[order_no] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [report_dept] (
	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[list_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [report_dept_detail] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[dept_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[indent_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[order_no] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [report_income_custom] (
	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[list_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[function_kind] [char] (1) COLLATE Chinese_PRC_CI_AS NULL ,
	[function_code] [char] (1) COLLATE Chinese_PRC_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [report_income_custom_detail] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[income_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[indent_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[order_no] [int] NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [report_cols] ADD 
	CONSTRAINT [PK_REPORT_COLS] PRIMARY KEY  CLUSTERED 
	(
		[list_code]
	)  ON [PRIMARY] 
GO

ALTER TABLE [report_cost] ADD 
	CONSTRAINT [PK_REPORT_COST] PRIMARY KEY  CLUSTERED 
	(
		[list_code]
	)  ON [PRIMARY] 
GO

ALTER TABLE [report_cost_detail] ADD 
	CONSTRAINT [PK_REPORT_COST_DETAIL] PRIMARY KEY  CLUSTERED 
	(
		[id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [report_dept] ADD 
	CONSTRAINT [PK_REPORT_DEPT] PRIMARY KEY  CLUSTERED 
	(
		[list_code]
	)  ON [PRIMARY] 
GO

ALTER TABLE [report_dept_detail] ADD 
	CONSTRAINT [PK_REPORT_DEPT_DETAIL] PRIMARY KEY  CLUSTERED 
	(
		[id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [report_income_custom] ADD 
	CONSTRAINT [PK_REPORT_INCOME_CUSTOM] PRIMARY KEY  CLUSTERED 
	(
		[list_code]
	)  ON [PRIMARY] 
GO

ALTER TABLE [report_income_custom_detail] ADD 
	CONSTRAINT [PK_REPORT_INCOME_CUSTOM_DETAIL] PRIMARY KEY  CLUSTERED 
	(
		[id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [report_cost_detail] ADD 
	CONSTRAINT [FK_REPORT_Cost_detail_REFERENCE_REPORT_Cost] FOREIGN KEY 
	(
		[list_code]
	) REFERENCES [report_cost] (
		[list_code]
	)
GO

ALTER TABLE [report_dept_detail] ADD 
	CONSTRAINT [FK_REPORT_Dept_detail_REFERENCE_REPORT_Dept] FOREIGN KEY 
	(
		[list_code]
	) REFERENCES [report_dept] (
		[list_code]
	)
GO

ALTER TABLE [report_income_custom_detail] ADD 
	CONSTRAINT [FK_REPORT_Income_detail_REFERENCE_REPORT_Income] FOREIGN KEY 
	(
		[list_code]
	) REFERENCES [report_income_custom] (
		[list_code]
	)
GO
/****** Object:  Table [sys_default_setup]    Script Date: 2006-3-1 14:03:05 ******/
if not exists (select * from dbo.sysobjects where id = object_id(N'[sys_default_setup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [sys_default_setup] (
	[default_id] [int] IDENTITY (1, 1) NOT NULL ,
	[default_user] [nvarchar] (20) COLLATE Chinese_PRC_CI_AS NULL ,
	[default_code] [nvarchar] (200) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[page_setup] [nvarchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[page_width] [numeric](16, 2) NOT NULL ,
	[page_height] [numeric](16, 2) NOT NULL ,
	[direction] [bit] NULL ,
	[top_margin] [numeric](16, 2) NOT NULL ,
	[bottom_margin] [numeric](16, 2) NOT NULL ,
	[left_margin] [numeric](16, 2) NOT NULL ,
	[right_margin] [numeric](16, 2) NOT NULL ,
	[row_scale] [numeric](16, 2) NOT NULL ,
	[col_scale] [numeric](16, 2) NOT NULL ,
	[font_scale] [numeric](16, 2) NOT NULL ,
	[fix_col] [int] NOT NULL 
) ON [PRIMARY]
END

GO


/****** Object:  Table [sys_prn_setup]    Script Date: 2006-3-1 14:03:22 ******/
if not exists (select * from dbo.sysobjects where id = object_id(N'[sys_prn_setup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [sys_prn_setup] (
	[setup_code] [nvarchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[setup_name] [nvarchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[page_setup] [nvarchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[page_width] [numeric](16, 2) NOT NULL ,
	[page_height] [numeric](16, 2) NOT NULL ,
	[direction] [bit] NULL ,
	[top_margin] [numeric](16, 2) NOT NULL ,
	[bottom_margin] [numeric](16, 2) NOT NULL ,
	[left_margin] [numeric](16, 2) NOT NULL ,
	[right_margin] [numeric](16, 2) NOT NULL ,
	[row_scale] [numeric](16, 2) NOT NULL ,
	[col_scale] [numeric](16, 2) NOT NULL ,
	[font_scale] [numeric](16, 2) NOT NULL ,
	[fix_col] [int] NOT NULL 
) ON [PRIMARY]
END

GO

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[report_comp_status]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
 BEGIN
CREATE TABLE [report_comp_status] (
	[year_month] [nvarchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[report_code] [nvarchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,
	[is_comp] [bit] NULL ,
	CONSTRAINT [PK_REPORT_COMP_STATUS] PRIMARY KEY  CLUSTERED 
	(
		[year_month],
		[report_code]
	)  ON [PRIMARY] 
) ON [PRIMARY]
END

GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[te_st_bonus_per_type]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[te_st_bonus_per_type]
GO

CREATE TABLE [dbo].[te_st_bonus_per_type] (
	[per_type] [int] IDENTITY (1, 1) NOT NULL ,
	[per_type_name] [nvarchar] (50) COLLATE Chinese_PRC_CI_AS NOT NULL 
) ON [PRIMARY]
GO



insert into report_cols values('ll_m_x_w','院区集合')
insert into report_def (report_code,report_name,head_num,report_row,report_col,report_type) values('_bonus_dis','奖金分配月报表','0','50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;','50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;','d')

insert into report_def (report_code,report_name,head_num,report_row,report_col,report_type) 
values('_wrapup_dis','科室综合指标表','0','50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;','50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;50px;','d')

