go

IF EXISTS (SELECT * 
	   FROM   sysobjects 
	   WHERE  name = N'get_cell')
	DROP FUNCTION get_cell
GO

create function get_cell(@row int,@col int,@report_code nvarchar(20),@byear nvarchar(4),@bm nvarchar(2),@nyear nvarchar(4),@nm nvarchar(2))
returns decimal(20,4)
as
begin

declare @result decimal(20,4)
--单元运算(row,col,report_code)
select @result=data from report_def_detail_data
where row=@row
and col=@col
and report_code=@report_code
and data_period=@byear+@bm+'-'+@nyear+@nm

return isnull(@result,0)
end

go

IF EXISTS (SELECT * 
	   FROM   sysobjects 
	   WHERE  name = N'sd_round')
	DROP FUNCTION sd_round
GO
--取整
create function sd_round(@param decimal)
returns int
as 
begin
declare @result int
select @result=@param
return @result
end
go
IF EXISTS (SELECT * 
	   FROM   sysobjects 
	   WHERE  name = N'sd_divide')
	DROP FUNCTION sd_divide
GO
--除法
create function sd_divide(@param1 decimal(20,4),@param2 decimal(20,4))
returns decimal(20,4)
as begin
	if(@param2=0)
	return 0
	
return isnull(@param1/@param2,0)
end

--------------------------------------------------------






--全院函数列表
$list_type$
全院集合			A01
科室集合   		D99
收费集合   		I99
成本集合    	C99

$detail_code$
A01 ''
D99 科室代码
I99 收入项目代码
C99 医疗项目代码

get_base_data($list_type$,'001',$detail_code$,@by,@bm,@ny,@nm,@report_code) 直接记入成本      
get_base_data($list_type$,'002',$detail_code$,@by,@bm,@ny,@nm,@report_code) 分摊直接成本      
get_base_data($list_type$,'003',$detail_code$,@by,@bm,@ny,@nm,@report_code) 公用成本      
get_base_data($list_type$,'004',$detail_code$,@by,@bm,@ny,@nm,@report_code) 管理成本一次分摊      
get_base_data($list_type$,'005',$detail_code$,@by,@bm,@ny,@nm,@report_code) 医辅成本  
get_base_data($list_type$,'006',$detail_code$,@by,@bm,@ny,@nm,@report_code) 医技成本  
get_base_data($list_type$,'007',$detail_code$,@by,@bm,@ny,@nm,@report_code) 固定成本		  
get_base_data($list_type$,'008',$detail_code$,@by,@bm,@ny,@nm,@report_code) 变动成本  
get_base_data($list_type$,'009',$detail_code$,@by,@bm,@ny,@nm,@report_code) 可控成本            
get_base_data($list_type$,'010',$detail_code$,@by,@bm,@ny,@nm,@report_code) 不可控成本            
get_base_data($list_type$,'011',$detail_code$,@by,@bm,@ny,@nm,@report_code) 管理成本            
get_base_data($list_type$,'012',$detail_code$,@by,@bm,@ny,@nm,@report_code) 人力成本          
get_base_data($list_type$,'013',$detail_code$,@by,@bm,@ny,@nm,@report_code) 离退休人员成本            
get_base_data($list_type$,'014',$detail_code$,@by,@bm,@ny,@nm,@report_code) 材料成本            
get_base_data($list_type$,'015',$detail_code$,@by,@bm,@ny,@nm,@report_code) 净药品成本      
get_base_data($list_type$,'016',$detail_code$,@by,@bm,@ny,@nm,@report_code) 折旧成本            
get_base_data($list_type$,'017',$detail_code$,@by,@bm,@ny,@nm,@report_code) 其它成本          
get_base_data($list_type$,'018',$detail_code$,@by,@bm,@ny,@nm,@report_code) 门诊医疗收入            
get_base_data($list_type$,'019',$detail_code$,@by,@bm,@ny,@nm,@report_code) 住院医疗收入            
get_base_data($list_type$,'020',$detail_code$,@by,@bm,@ny,@nm,@report_code) 门诊药品收入         
get_base_data($list_type$,'021',$detail_code$,@by,@bm,@ny,@nm,@report_code) 住院药品收入            
get_base_data($list_type$,'022',$detail_code$,@by,@bm,@ny,@nm,@report_code) 财政补助收入            
get_base_data($list_type$,'023',$detail_code$,@by,@bm,@ny,@nm,@report_code) 本科开单本科执行的收入        

          
get_base_data($list_type$,'031',$detail_code$,@by,@bm,@ny,@nm,@report_code) 门诊医疗全成本            
get_base_data($list_type$,'032',$detail_code$,@by,@bm,@ny,@nm,@report_code) 住院医疗全成本          
get_base_data($list_type$,'033',$detail_code$,@by,@bm,@ny,@nm,@report_code) 门诊药品全成本            
get_base_data($list_type$,'034',$detail_code$,@by,@bm,@ny,@nm,@report_code) 住院药品全成本            
get_base_data($list_type$,'035',$detail_code$,@by,@bm,@ny,@nm,@report_code) 门诊医疗科研全成本      
get_base_data($list_type$,'036',$detail_code$,@by,@bm,@ny,@nm,@report_code) 住院医疗科研全成本            
get_base_data($list_type$,'037',$detail_code$,@by,@bm,@ny,@nm,@report_code) 门诊药品科研全成本          
get_base_data($list_type$,'038',$detail_code$,@by,@bm,@ny,@nm,@report_code) 住院药品科研全成本            
get_base_data($list_type$,'039',$detail_code$,@by,@bm,@ny,@nm,@report_code) 未纳入成本项目            
get_base_data($list_type$,'040',$detail_code$,@by,@bm,@ny,@nm,@report_code) 直接计算记入成本 



--自定义科室函数列表

--自定义科室函数（自定义科室代码 not null，科室类别【O，I，T，F，DC（自定义科室直接成本），IDC（自定义科室间接成本）】 not null，函数功能代码(001~008)，项目，东西院）
--001 直接成本 作用于（O，I，T，F）
--002 间接成本 公用分摊  作用于（O，I，T，F）
--003 间接成本 管理分摊  作用于（O，I，T，F）
--004 间接成本 医辅分摊  作用于（O，I，T，F）
--005 间接成本 医技分摊  作用于（O，I，T，F）
--006 间接成本  作用于（O，I，T，F）
--007 直接收入  作用于（O，I，T，F）
--008 间接收入  作用于（O，I，T，F）

get_sd_base_date(@sd_code,@function_code,001,@detail_code,@type,@by,@bm,@ny,@nm,@report_code,@list_code,@期间类型) 
get_sd_base_date(@sd_code,@function_code,002,@detail_code,@type,@by,@bm,@ny,@nm,@report_code,@list_code,@期间类型) 
get_sd_base_date(@sd_code,@function_code,003,@detail_code,@type,@by,@bm,@ny,@nm,@report_code,@list_code,@期间类型) 
get_sd_base_date(@sd_code,@function_code,004,@detail_code,@type,@by,@bm,@ny,@nm,@report_code,@list_code,@期间类型) 
get_sd_base_date(@sd_code,@function_code,005,@detail_code,@type,@by,@bm,@ny,@nm,@report_code,@list_code,@期间类型) 
get_sd_base_date(@sd_code,@function_code,006,@detail_code,@type,@by,@bm,@ny,@nm,@report_code,@list_code,@期间类型) 
get_sd_base_date(@sd_code,@function_code,007,@detail_code,@type,@by,@bm,@ny,@nm,@report_code,@list_code,@期间类型) 
get_sd_base_date(@sd_code,@function_code,008,@detail_code,@type,@by,@bm,@ny,@nm,@report_code,@list_code,@期间类型) 


--公共函数
get_cell(@row,@col,@report_code,@by,@bm,@ny,@nm) 			单元运算
get_this_sum(@row,@col,@report_code,@by,@bm,@ny,@nm) 	本年累计
get_last_sum(@row,@col,@report_code,@by,@bm,@ny,@nm) 	上年累计
get_last_period(@row,@col,@report_code,@by,@bm,@ny,@nm)上年同期
get_this_avg(@row,@col,@report_code,@by,@bm,@ny,@nm) 	本年平均
get_little_sum(@src_row,@src_col,@report_code,@by,@bm,@ny,@nm) 小计
--get_name($list_type$,@list_code,@detail_code) 取行集名字
get_name(@row,@col,@src_row,@src_col,@report_code) --取名字

sd_round(@param) 取整
sd_divide(@param1,@param2)除法

--------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[FK_REPORT_C_REFERENCE_REPORT_C]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)

ALTER TABLE [cbcs].[report_cols_detail] DROP CONSTRAINT FK_REPORT_C_REFERENCE_REPORT_C

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[FK_REPORT_Cost_detail_REFERENCE_REPORT_Cost]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)

ALTER TABLE [cbcs].[report_cost_detail] DROP CONSTRAINT FK_REPORT_Cost_detail_REFERENCE_REPORT_Cost

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[FK_REPORT_Dept_detail_REFERENCE_REPORT_Dept]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)

ALTER TABLE [cbcs].[report_dept_detail] DROP CONSTRAINT FK_REPORT_Dept_detail_REFERENCE_REPORT_Dept

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[FK_REPORT_Income_detail_REFERENCE_REPORT_Income]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)

ALTER TABLE [cbcs].[report_income_custom_detail] DROP CONSTRAINT FK_REPORT_Income_detail_REFERENCE_REPORT_Income

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_cols]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_cols]

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_cost]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_cost]

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_cost_detail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_cost_detail]

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_dept]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_dept]

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_dept_detail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_dept_detail]

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_income_custom]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_income_custom]

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_income_custom_detail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_income_custom_detail]

GO



CREATE TABLE [cbcs].[report_cols] (

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[list_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL 

) ON [PRIMARY]

GO



CREATE TABLE [cbcs].[report_cost] (

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[list_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[function_kind] [char] (1) COLLATE Chinese_PRC_CI_AS NULL ,

	[function_code] [char] (1) COLLATE Chinese_PRC_CI_AS NULL 

) ON [PRIMARY]

GO



CREATE TABLE [cbcs].[report_cost_detail] (

	[id] [int] IDENTITY (1, 1) NOT NULL ,

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[cost_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[indent_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[order_no] [int] NOT NULL 

) ON [PRIMARY]

GO



CREATE TABLE [cbcs].[report_dept] (

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[list_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[function_kind] [char] (1) COLLATE Chinese_PRC_CI_AS NULL ,

	[function_code] [char] (1) COLLATE Chinese_PRC_CI_AS NULL 

) ON [PRIMARY]

GO



CREATE TABLE [cbcs].[report_dept_detail] (

	[id] [int] IDENTITY (1, 1) NOT NULL ,

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[dept_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[indent_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[order_no] [int] NOT NULL 

) ON [PRIMARY]

GO



CREATE TABLE [cbcs].[report_income_custom] (

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[list_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[function_kind] [char] (1) COLLATE Chinese_PRC_CI_AS NULL ,

	[function_code] [char] (1) COLLATE Chinese_PRC_CI_AS NULL 

) ON [PRIMARY]

GO



CREATE TABLE [cbcs].[report_income_custom_detail] (

	[id] [int] IDENTITY (1, 1) NOT NULL ,

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[income_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[indent_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[order_no] [int] NOT NULL 

) ON [PRIMARY]

GO



ALTER TABLE [cbcs].[report_cols] ADD 

	CONSTRAINT [PK_REPORT_COLS] PRIMARY KEY  CLUSTERED 

	(

		[list_code]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_cost] ADD 

	CONSTRAINT [PK_REPORT_COST] PRIMARY KEY  CLUSTERED 

	(

		[list_code]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_cost_detail] ADD 

	CONSTRAINT [PK_REPORT_COST_DETAIL] PRIMARY KEY  CLUSTERED 

	(

		[id]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_dept] ADD 

	CONSTRAINT [PK_REPORT_DEPT] PRIMARY KEY  CLUSTERED 

	(

		[list_code]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_dept_detail] ADD 

	CONSTRAINT [PK_REPORT_DEPT_DETAIL] PRIMARY KEY  CLUSTERED 

	(

		[id]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_income_custom] ADD 

	CONSTRAINT [PK_REPORT_INCOME_CUSTOM] PRIMARY KEY  CLUSTERED 

	(

		[list_code]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_income_custom_detail] ADD 

	CONSTRAINT [PK_REPORT_INCOME_CUSTOM_DETAIL] PRIMARY KEY  CLUSTERED 

	(

		[id]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_cost_detail] ADD 

	CONSTRAINT [FK_REPORT_Cost_detail_REFERENCE_REPORT_Cost] FOREIGN KEY 

	(

		[list_code]

	) REFERENCES [cbcs].[report_cost] (

		[list_code]

	)

GO



ALTER TABLE [cbcs].[report_dept_detail] ADD 

	CONSTRAINT [FK_REPORT_Dept_detail_REFERENCE_REPORT_Dept] FOREIGN KEY 

	(

		[list_code]

	) REFERENCES [cbcs].[report_dept] (

		[list_code]

	)

GO



ALTER TABLE [cbcs].[report_income_custom_detail] ADD 

	CONSTRAINT [FK_REPORT_Income_detail_REFERENCE_REPORT_Income] FOREIGN KEY 

	(

		[list_code]

	) REFERENCES [cbcs].[report_income_custom] (

		[list_code]

	)

GO


-----------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[FK_REPORT_C_REFERENCE_REPORT_C]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)

ALTER TABLE [cbcs].[report_cols_detail] DROP CONSTRAINT FK_REPORT_C_REFERENCE_REPORT_C

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[FK_REPORT_Cost_detail_REFERENCE_REPORT_Cost]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)

ALTER TABLE [cbcs].[report_cost_detail] DROP CONSTRAINT FK_REPORT_Cost_detail_REFERENCE_REPORT_Cost

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[FK_REPORT_Dept_detail_REFERENCE_REPORT_Dept]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)

ALTER TABLE [cbcs].[report_dept_detail] DROP CONSTRAINT FK_REPORT_Dept_detail_REFERENCE_REPORT_Dept

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[FK_REPORT_Income_detail_REFERENCE_REPORT_Income]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)

ALTER TABLE [cbcs].[report_income_custom_detail] DROP CONSTRAINT FK_REPORT_Income_detail_REFERENCE_REPORT_Income

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_cols]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_cols]

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_cost]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_cost]

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_cost_detail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_cost_detail]

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_dept]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_dept]

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_dept_detail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_dept_detail]

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_income_custom]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_income_custom]

GO



if exists (select * from dbo.sysobjects where id = object_id(N'[cbcs].[report_income_custom_detail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)

drop table [cbcs].[report_income_custom_detail]

GO



CREATE TABLE [cbcs].[report_cols] (

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[list_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL 

) ON [PRIMARY]

GO



CREATE TABLE [cbcs].[report_cost] (

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[list_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[function_kind] [char] (1) COLLATE Chinese_PRC_CI_AS NULL ,

	[function_code] [char] (1) COLLATE Chinese_PRC_CI_AS NULL 

) ON [PRIMARY]

GO



CREATE TABLE [cbcs].[report_cost_detail] (

	[id] [int] IDENTITY (1, 1) NOT NULL ,

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[cost_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[indent_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[order_no] [int] NOT NULL 

) ON [PRIMARY]

GO



CREATE TABLE [cbcs].[report_dept] (

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[list_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[function_kind] [char] (1) COLLATE Chinese_PRC_CI_AS NULL ,

	[function_code] [char] (1) COLLATE Chinese_PRC_CI_AS NULL 

) ON [PRIMARY]

GO



CREATE TABLE [cbcs].[report_dept_detail] (

	[id] [int] IDENTITY (1, 1) NOT NULL ,

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[dept_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[indent_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[order_no] [int] NOT NULL 

) ON [PRIMARY]

GO



CREATE TABLE [cbcs].[report_income_custom] (

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[list_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[function_kind] [char] (1) COLLATE Chinese_PRC_CI_AS NULL ,

	[function_code] [char] (1) COLLATE Chinese_PRC_CI_AS NULL 

) ON [PRIMARY]

GO



CREATE TABLE [cbcs].[report_income_custom_detail] (

	[id] [int] IDENTITY (1, 1) NOT NULL ,

	[list_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[income_code] [varchar] (20) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[indent_name] [varchar] (40) COLLATE Chinese_PRC_CI_AS NOT NULL ,

	[order_no] [int] NOT NULL 

) ON [PRIMARY]

GO



ALTER TABLE [cbcs].[report_cols] ADD 

	CONSTRAINT [PK_REPORT_COLS] PRIMARY KEY  CLUSTERED 

	(

		[list_code]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_cost] ADD 

	CONSTRAINT [PK_REPORT_COST] PRIMARY KEY  CLUSTERED 

	(

		[list_code]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_cost_detail] ADD 

	CONSTRAINT [PK_REPORT_COST_DETAIL] PRIMARY KEY  CLUSTERED 

	(

		[id]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_dept] ADD 

	CONSTRAINT [PK_REPORT_DEPT] PRIMARY KEY  CLUSTERED 

	(

		[list_code]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_dept_detail] ADD 

	CONSTRAINT [PK_REPORT_DEPT_DETAIL] PRIMARY KEY  CLUSTERED 

	(

		[id]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_income_custom] ADD 

	CONSTRAINT [PK_REPORT_INCOME_CUSTOM] PRIMARY KEY  CLUSTERED 

	(

		[list_code]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_income_custom_detail] ADD 

	CONSTRAINT [PK_REPORT_INCOME_CUSTOM_DETAIL] PRIMARY KEY  CLUSTERED 

	(

		[id]

	)  ON [PRIMARY] 

GO



ALTER TABLE [cbcs].[report_cost_detail] ADD 

	CONSTRAINT [FK_REPORT_Cost_detail_REFERENCE_REPORT_Cost] FOREIGN KEY 

	(

		[list_code]

	) REFERENCES [cbcs].[report_cost] (

		[list_code]

	)

GO



ALTER TABLE [cbcs].[report_dept_detail] ADD 

	CONSTRAINT [FK_REPORT_Dept_detail_REFERENCE_REPORT_Dept] FOREIGN KEY 

	(

		[list_code]

	) REFERENCES [cbcs].[report_dept] (

		[list_code]

	)

GO



ALTER TABLE [cbcs].[report_income_custom_detail] ADD 

	CONSTRAINT [FK_REPORT_Income_detail_REFERENCE_REPORT_Income] FOREIGN KEY 

	(

		[list_code]

	) REFERENCES [cbcs].[report_income_custom] (

		[list_code]

	)

GO



GO

/****** 对象:  用户定义的函数 cbcs.get_base_data    脚本日期: 2006-1-18 13:25:05 ******/
CREATE FUNCTION dbo.get_base_data 
	(@function_code nvarchar(10),
	@f_logic nvarchar(20),
	@code nvarchar(30),
	@byear nvarchar(4),
	@bm nvarchar(2),
	@nyear nvarchar(4),
	@nm nvarchar(2),
	@report_code nvarchar(30),
	@time_type nvarchar(30)
)
RETURNS  decimal(16,4)
AS
BEGIN
-- 上年同期
if @time_type = 'time_type_last_period' 
begin
  return dbo.get_base_data(
	@function_code,
	@f_logic,
	@code,
	convert(nvarchar(4), convert(int, @byear) - 1),
	@bm,
	convert(nvarchar(4), convert(int, @nyear) - 1),
	@nm,
	@report_code,
	'time_type_this_period'
		)
end else
-- 本期累计
if @time_type = 'time_type_this_sum' 
begin
  return dbo.get_base_data(
	@function_code,
	@f_logic,
	@code,
	@nyear,
	'01',
	@nyear,
	@nm,
	@report_code,
	'time_type_this_period'
		)
end else
-- 上期累计
if @time_type = 'time_type_last_sum' 
begin
  return (
	dbo.get_base_data(
	@function_code,
	@f_logic,
	@code,
	@nyear,
	'01',
	@nyear,
	@nm,
	@report_code,
	'time_type_this_period'
			)
	 / convert(int, @nm)
	)

end else
-- 本年平均
if @time_type = 'time_type_this_avg' 
begin
  return dbo.get_base_data(
	@function_code,
	@f_logic,
	@code,
	convert(nvarchar(4), convert(int, @nyear) - 1),
	'01',
	convert(nvarchar(4), convert(int, @nyear) - 1),
	@nm,
	@report_code,
	'time_type_this_period'
		)
end else
-- 本期
begin

	--内部参数
	declare @result decimal(16,4)
	declare @acct_byear nvarchar(4)
	declare @begin_m nvarchar(2)
	declare @acct_nyear nvarchar(4)
	declare @end_m nvarchar(2)



	set @result=0
	
	select @acct_byear=@byear,@acct_nyear=@nyear,@begin_m=@bm,@end_m=@nm
	--select top 1 @acct_byear=acct_year,@begin_m=start_month,@end_m=end_month,@report_code=report_code from acct_report_time
	
	--    管理小计   		D01
	--    医辅小计   		D02
	--    医技小计   		D03
	--    直接医疗小技 	D04
	--    科室集合   		D99
	--    收费集合   		I99
	--    成本集合    	C99
	
	
	-- 直接记入成本			001        
	-- 分摊直接成本			002        
	-- 公用成本					003            
	-- 管理成本一次分摊	004    
	-- 医辅成本					005            
	-- 医技成本					006            
	-- 固定成本					007            
	-- 变动成本					008            
	-- 可控成本					009            
	-- 不可控成本				010          
	-- 管理成本					011            
	-- 人力成本					012            
	-- 离退休人员成本		013      
	-- 材料成本					014            
	-- 净药品成本				015          
	-- 折旧成本					016            
	-- 其它成本					017            
	-- 门诊医疗收入			018        
	-- 住院医疗收入			019        
	-- 门诊药品收入			020        
	-- 住院药品收入			021                
	-- 财政补助收入			022                
	-- 本科开单本科执行的收入23      
	-- 
	-- 门诊医疗全成本 			031             
	-- 住院医疗全成本 			032      
	-- 门诊药品全成本 			033      
	-- 住院药品全成本 			034      
	-- 门诊医疗科研全成本 035  
	-- 住院医疗科研全成本 036  
	-- 门诊药品科研全成本 037  
	-- 住院药品科研全成本 038  
	-- 未纳入成本项目  		039  
	-- 直接计算记入成本   040
	
	
	--逻辑
	if(@function_code='A01')--全院总计 A01
	begin
		if('031'=@f_logic)--门诊医疗全成本
			select @result=sum(tot_amount) --O_T_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_subj
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dict_cost_subj.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
				and out_or_in='O' 
				and treat_or_med='T' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('032'=@f_logic)--住院医疗全成本
			select @result=sum(tot_amount) --I_T_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_subj
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dict_cost_subj.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and out_or_in='I' 
				and treat_or_med='T' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('033'=@f_logic)--门诊药品全成本
			select @result=sum(tot_amount) --O_M_cost
		  from st_dept_cost s,dict_acct_dept d,dict_cost_subj
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dict_cost_subj.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and out_or_in='O' 
				and treat_or_med='M' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('034'=@f_logic)--住院药品全成本   034
			select @result=sum(tot_amount) --I_M_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_subj
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dict_cost_subj.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and out_or_in='I' 
				and treat_or_med='M' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N'))
	
		else if('035'=@f_logic)--门诊医疗科研全成本  035
			select @result=sum(tot_amount) --O_T_Cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_subj
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dict_cost_subj.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
				and out_or_in='O' 
				and treat_or_med='T' 
				and (app_level=4 and cost_app_ind='N')
	
		else if('036'=@f_logic)--住院医疗科研全成本  036
			select @result=sum(tot_amount) --O_T_Cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_subj
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dict_cost_subj.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and out_or_in='I' 
				and treat_or_med='T' 
				and (app_level=4 and cost_app_ind='N')
	
		else if('037'=@f_logic)--门诊药品科研全成本   037
			select @result=sum(tot_amount) --O_T_Cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_subj
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dict_cost_subj.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and out_or_in='O' 
				and treat_or_med='M' 
				and (app_level=4 and cost_app_ind='N')
	
		else if('038'=@f_logic)--住院药品科研全成本   038
			select @result=sum(tot_amount) -- O_T_Cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_subj
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dict_cost_subj.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and out_or_in='I' 
				and treat_or_med='M' 
				and (app_level=4 and cost_app_ind='N')
	
		else if('007'=@f_logic)--固定成本007
			select @result=sum(tot_amount) -- fixed_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dc.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and stop_mark='N' 
				and cost_type_code='001' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('008'=@f_logic)--变动成本008
			select @result=sum(tot_amount) -- dy_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dc.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and stop_mark='N' 
				and cost_type_code='002' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('009'=@f_logic)--可控成本009
			select @result=sum(tot_amount) -- contral_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dc.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and stop_mark='N' 
				and cost_type_code='003' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('010'=@f_logic)--不可控成本010
			select @result=sum(tot_amount) -- uncontral_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dc.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and stop_mark='N' 
				and cost_type_code='004' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('011'=@f_logic)--管理成本011  
			select @result=sum(tot_amount) -- manage_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dc.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and stop_mark='N' 
				and cost_type_code='008' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('012'=@f_logic)--人力成本012
			select @result=sum(tot_amount) -- manpower_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dc.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and stop_mark='N' 
				and cost_type_code='010' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('013'=@f_logic)--离退休人员成本013
			select @result=sum(tot_amount) -- retire_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dc.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and stop_mark='N' 
				and cost_type_code='012' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('014'=@f_logic)--材料成本014
			select @result=sum(tot_amount) -- material_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dc.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and stop_mark='N' 
				and cost_type_code='011' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('015'=@f_logic)--净药品成本015
			select @result=sum(tot_amount) -- pdurg_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dc.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and stop_mark='N' 
				and cost_type_code='005' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('016'=@f_logic)--折旧成本016
			select @result=sum(tot_amount) -- depreciation_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dc.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and stop_mark='N' 
				and cost_type_code='009' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	
		else if('017'=@f_logic)--其它成本017
			select @result=sum(tot_amount) -- oth_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dc.cost_subj_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and stop_mark='N' 
				and cost_type_code='013' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
		else if('039'=@f_logic)--未纳入成本项目039
		begin
			select @result=sum(amount) -- bq 
					from cost_detail 
					where dept_code='UNCOUNT'
					and pay_date>= (--根据给定的起始年月结合存储的日数值构造起始日期（为了解决存储日期超过本月最后一天）
													--基本思想：（基本年月）＋（一个月）－（一天）得到该月的最后一天
													--比较存储的日与本月的最后一天的大小
													--存储日小于本月最后一日：用基本年月同存储日构造日期
													--否则：用本月的最后一天构造日期
													select case when start_day<(
																											select day(
																																	dateadd(
																																					day,-1,dateadd(
																																													month,1,convert(datetime,@acct_byear+@begin_m+'01')
																																												)
																																					)
																																)
																											) 
																	then @acct_byear+'-'+@begin_m+('-'+cast(start_day as nvarchar)) 
																	else 
																	dateadd(
																					day,-1,dateadd(
																													month,1,convert(datetime,@acct_byear+@begin_m+'01')
																												)
																	)
																	end 
													from com_info
												) 
					and pay_date<(--思想同上，唯一不同点是期间的概念需要使得在结束月的基础上加一
												select case when start_day<(
																										select day(
																																dateadd(
																																				day,-1,dateadd(
																																												month,2,convert(datetime,@acct_byear+@begin_m+'01')
																																											)
																																				)
																															)
																										) 
																then convert(nvarchar(7),dateadd(month,1,convert(datetime,@acct_byear+@begin_m+'01')),120)+(+'-'+cast(start_day as nvarchar)) 
																else 
																dateadd(
																				day,-1,dateadd(
																												month,2,convert(datetime,@acct_byear+@begin_m+'01')
																											)
																)
																end 
												from com_info
												)
		end
	end
	else if(@function_code='D99')--科室集合 D99
	begin
	--select '科室集合'
		
		if('001'=@f_logic)--直接记入成本001
			select @result=sum(prime_cost)--直接记入成本001
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
	
		else if('002'=@f_logic)--分摊直接成本002
			select @result=sum(apport_direct_cost)--分摊直接成本002
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
	
		else if('003'=@f_logic)--公用成本003
			select @result=sum(public_cost)--公用成本003
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
	
		else if('004'=@f_logic)--管理成本一次分摊004
			select @result=sum(manage_cost_appor)--管理成本一次分摊004
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
	
		else if('005'=@f_logic)--医辅成本005
			select @result=sum( apport_cost_t)--医辅成本005
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
	
		else if('006'=@f_logic)--医技成本006
			select @result=sum( apport_cost_f)--医技成本006
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
	
		else if('007'=@f_logic)--固定成本007
			select @result=sum( fixed_cost)--固定成本007
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
	
		else if('008'=@f_logic)--变动成本008
			select @result=sum( dy_cost)--变动成本008
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
	
		else if('009'=@f_logic)--可控成本009
			select @result=sum( control_cost)--可控成本009
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
	
		else if('010'=@f_logic)--不可控成本010
			select @result=sum( uncontrol_cost)--不可控成本010
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
	
		else if('011'=@f_logic)--管理成本011   
			select @result=sum( manage_cost)--管理成本011
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m	
				
		else if('012'=@f_logic)--人力成本012
			select @result=sum( manpower_cost)--人力成本012
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
				
		else if('013'=@f_logic)--离退休人员成本013
			select @result=sum( retire_cost)--离退休人员成本013
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m	
					
		else if('014'=@f_logic)--材料成本014
			select @result=sum( material_cost)--材料成本014
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m	
					
		else if('015'=@f_logic)--净药品成本015
			select @result=sum( pure_M_cost)--净药品成本015
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m		
		else if('016'=@f_logic)--折旧成本016
			select @result=sum( depreciation_cost)--折旧成本016
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
				
		else if('017'=@f_logic)--其它成本017
			select @result=sum( other_cost)--其它成本017
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m	
					
		else if('018'=@f_logic)--门诊医疗收入018
			select @result=sum( O_T_income)--门诊医疗收入018
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
				
		else if('019'=@f_logic)--住院医疗收入019
			select @result=sum( I_T_income)--住院医疗收入019
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
				
		else if('020'=@f_logic)--门诊药品收入020
			select @result=sum( O_M_income)--门诊药品收入020
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m	
				and year_month<=@acct_nyear+@end_m
				
		else if('021'=@f_logic)--住院药品收入021
			select @result=sum( I_M_income)--住院药品收入021
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
				
		else if('022'=@f_logic)--财政补助收入022
			select @result=sum(fund_subsidy_income) --财政补助收入022
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
				
		else if('023'=@f_logic)--本科开单本科执行的收入023
			select @result=sum(self_exec_income) --本科开单本科执行的收入23
			from acct_report_dept_data
				where dept_code=@code
				and year_month>=@acct_byear+@begin_m
				and year_month<=@acct_nyear+@end_m
	
	 end
	-- else if(@function_code='D01')--管理小计   D01
	-- begin
	-- select '管理小计'
	-- 
	-- end
	-- else if(@function_code='D02')--医辅小计   D02
	-- begin
	-- select '医辅小计'
	-- 
	-- end
	-- else if(@function_code='D03')--医技小计   D03
	-- begin
	-- select '医技小计'
	-- 
	-- end
	-- else if(@function_code='D04')--直接医疗小技  D04
	-- begin
	-- select '直接医疗小技'
	-- 
	-- end
	else if(@function_code='I99')--收费集合   I99
	begin
	--select '收费集合'
	
		if('018'=@f_logic)--门诊医疗收入018
		  select @result=sum(amount) -- idx_value
		  from st_dept_income s, dict_acct_dept d,b_dict_charge_detail_kind
				where s.ordered_by=d.dept_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and s.charge_kind_code=b_dict_charge_detail_kind.charge_kind_code 
				and income_type='T'
				and out_or_in='O'
				and s.charge_kind_code='006'
	
		else if('019'=@f_logic)--住院医疗收入019
		  select @result=sum(amount) -- idx_value
		  from st_dept_income s, dict_acct_dept d,b_dict_charge_detail_kind
				where s.ordered_by=d.dept_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and s.charge_kind_code=b_dict_charge_detail_kind.charge_kind_code 
				and income_type='T'
				and out_or_in='I'
				and s.charge_kind_code='006'
	
		else if('020'=@f_logic)--门诊药品收入020
		  select @result=sum(amount) -- idx_value
		  from st_dept_income s, dict_acct_dept d,b_dict_charge_detail_kind
				where s.ordered_by=d.dept_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and s.charge_kind_code=b_dict_charge_detail_kind.charge_kind_code 
				and income_type='M'
				and out_or_in='O' 
				and s.charge_kind_code='006'
	
		else if('021'=@f_logic)--住院药品收入021
		  select @result=sum(amount) -- idx_value
		  from st_dept_income s, dict_acct_dept d,b_dict_charge_detail_kind
				where s.ordered_by=d.dept_code 
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and s.charge_kind_code=b_dict_charge_detail_kind.charge_kind_code 
				and income_type='M'
				and out_or_in='I' 
				and s.charge_kind_code='006'
	
	
	end
	else if(@function_code='C99')--成本集合    C99
	begin
	--select '成本集合'
	
		if('040'=@f_logic)--直接计算记入记入成本 040
			select @result=sum(adm_cost) 
			from report_dept_cost_subj,dict_cost_type_subj
			where year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m  
			and cost_type_code='006'
			and report_dept_cost_subj.cost_subj_code=dict_cost_type_subj.cost_subj_code
			and report_dept_cost_subj.cost_subj_code=@code
		else if('003'=@f_logic)--公用成本003
			select @result=sum(adm_cost) 
			from report_dept_cost_subj,dict_cost_type_subj
			where year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m  
			and cost_type_code<>'006'
			and report_dept_cost_subj.cost_subj_code=dict_cost_type_subj.cost_subj_code
			and report_dept_cost_subj.cost_subj_code=@code
	
		else
			select @result=case @f_logic 
											when '001' then sum(prime_cost)--直接记入成本001
											when '011' then sum(apport_cost_s)--管理成本011
											when '005' then sum(apport_cost_t)--医辅成本013
											when '006' then sum(apport_cost_f)--医技成本006
											end			
			from report_dept_cost_subj
				where year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
				and cost_subj_code=@code
	end
end
return isnull(@result,0)
end

GO

--取名字
--??当列集合和行集合同时存在时，会有问题，有待解决。
CREATE function dbo.get_name(@row int,@col int,@src_row int, @src_col int,@report_code nvarchar(20))
returns nvarchar(20)
as begin
	--内部参数
  declare @result nvarchar(3500)
  declare @row1 int
  declare @col1 int
  declare @src_row1 int
  declare @src_col1 int 
  declare @report_code1 nvarchar(20)

  set @result=0
  select @row1=@row,@col1=@col,@src_row1=@src_row,@src_col1=@src_col,@report_code1=@report_code
  
  if exists(select 1 from report_def_detail where row=0 and col=@src_col1 and report_code=@report_code1 and formula like '%:%')
    select @result=data from report_def_detail_data where row=0 and col=@col1 and report_code=@report_code1 
  
  if exists(select 1 from report_def_detail where row=@src_row1 and col=0 and report_code=@report_code1 and formula like '%:%')
    select @result=data from report_def_detail_data where row=@row1 and col=0 and report_code=@report_code1
  
  
  
  return isnull(@result,0)
end

GO

CREATE function dbo.get_little_sum(@report_code nvarchar(20),@byear nvarchar(4),@bm nvarchar(2),@nyear nvarchar(4),@nm nvarchar(2), @sum_type nvarchar(2), @new_row_col int, @src_row int, @src_col int)
returns decimal(20,4)
as
begin
declare @result decimal(20,4)

--行小计
if @sum_type = 'r'
begin

	select @result=sum(cast(data as decimal(12, 6))) from report_def_detail_data
	where src_row=@src_row
	and src_col=@src_col
	and report_code=@report_code
	and data_period=@byear+@bm+'-'+@nyear+@nm
	and is_last='1'
	and row = @new_row_col

end else
-- 列小计
if @sum_type = 'c'
begin
	select @result=sum(cast(data as decimal(12, 6))) from report_def_detail_data
	where src_row=@src_row
	and src_col=@src_col
	and report_code=@report_code
	and data_period=@byear+@bm+'-'+@nyear+@nm
	and is_last='1'
	and col = @new_row_col

end else
--小计
begin
	select @result=sum(cast(data as decimal(12, 6))) from report_def_detail_data
	where src_row=@src_row
	and src_col=@src_col
	and report_code=@report_code
	and data_period=@byear+@bm+'-'+@nyear+@nm
	and is_last='1'

end

return isnull(@result,0)
end

GO

---------------------------------------





---------------------------------------------

IF EXISTS (SELECT * 
	   FROM   sysobjects 
	   WHERE  name = N'get_sd_base_date')
	DROP FUNCTION get_sd_base_date

GO

/****** 对象:  用户定义的函数 dbo.get_sd_base_date    脚本日期: 2006-2-17 15:33:21 ******/


create FUNCTION get_sd_base_date 
	(@sd_code nvarchar(30),
	@function_code nvarchar(10),
	@f_logic nvarchar(20),
	@code nvarchar(30),
	@type nvarchar(10),
	@byear nvarchar(4),
	@bm nvarchar(2),
	@nyear nvarchar(4),
	@nm nvarchar(2),
	@report_code nvarchar(30),
	@list_code nvarchar(30),
	@time_type nvarchar(30)
	)
RETURNS  decimal(16,4)
AS
BEGIN

if @time_type = 'time_type_last_period' 
begin
  return dbo.get_sd_base_date(
	@sd_code,
	@function_code,
	@f_logic,
	@code,
	@type,
	convert(nvarchar(4), convert(int, @byear) - 1),
	@bm,
	convert(nvarchar(4), convert(int, @nyear) - 1),
	@nm,
	@report_code,
	@list_code,
	'time_type_this_period'
		)
end else
-- 本期累计
if @time_type = 'time_type_this_sum' 
begin
  return dbo.get_sd_base_date(
	@sd_code,
	@function_code,
	@f_logic,
	@code,
	@type,
	@nyear,
	'01',
	@nyear,
	@nm,
	@report_code,
	@list_code,
	'time_type_this_period'
		)
end else
-- 上期累计
if @time_type = 'time_type_last_sum' 
begin
  return (
	dbo.get_sd_base_date(
	@sd_code,
	@function_code,
	@f_logic,
	@code,
	@type,
	@nyear,
	'01',
	@nyear,
	@nm,
	@report_code,
	@list_code,
	'time_type_this_period'
			)
	)

end else
-- 本年平均
if @time_type = 'time_type_this_avg' 
begin
  return dbo.get_sd_base_date(
	@sd_code,
	@function_code,
	@f_logic,
	@code,
	@type,
	convert(nvarchar(4), convert(int, @nyear) - 1),
	'01',
	convert(nvarchar(4), convert(int, @nyear) - 1),
	@nm,
	@report_code,
	@list_code,
	'time_type_this_period'
		)

	 / convert(int, @nm)

end else
-- 本期
begin

	--自定义科室函数（自定义科室代码 not null，科室类别【O，I，T，F，DC（自定义科室直接成本），IDC（自定义科室间接成本）】 not null，函数功能代码(001~008)，项目，东西院）
	--001 直接成本 作用于（O，I，T，F）
	--002 间接成本 公用分摊  作用于（O，I，T，F）
	--003 间接成本 管理分摊  作用于（O，I，T，F）
	--004 间接成本 医辅分摊  作用于（O，I，T，F）
	--005 间接成本 医技分摊  作用于（O，I，T，F）
	--006 间接成本  作用于（O，I，T，F）
	--007 直接收入  作用于（O，I，T，F）
	--008 间接收入  作用于（O，I，T，F）
	
	
	
	--自定义科室集合 D98
	declare @result decimal(16,4)
	set @result=0
	
	
	declare @acct_byear nvarchar(4)
	declare @begin_m nvarchar(2)
	declare @acct_nyear nvarchar(4)
	declare @end_m nvarchar(2)
	
	select @acct_byear=@byear,@acct_nyear=@nyear,@begin_m=@bm,@end_m=@nm
	--select top 1 @acct_byear=acct_year,@begin_m=start_month,@end_m=end_month,@report_code=report_code from acct_report_time
	
	
		if('O'=@function_code)--门诊成本
		begin
			select @result=case @f_logic 
							when '001' then sum(D_cost)--自定义科室门诊直接成本(自定义科室，项目，东西院)
							when '002' then sum(adm_cost)--自定义科室门诊间接成本门诊公用分摊		
							when '003' then sum(apport_cost_s)--门诊管理分摊
							when '004' then sum(acf)--间接成本 医辅分摊
							when '005' then sum(act)--间接成本 医技分摊
							when '006' then sum(tot_cost)--间接成本 间接成本
						 end					 							 
			 from report_sd_cost-- select * from report_sd_cost
				where dept_clin_code=@sd_code
				and dept_type='O'
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
				and (cost_subj_code=@code or @code='') 
				and (list_code=@list_code or @list_code='') 
				and dept_proper=@type
		end	
		else if('I'=@function_code)--住院成本
		begin
			select @result=case @f_logic 
							when '001' then sum(D_cost)--自定义科室住院直接成本(自定义科室，项目，东西院)
							when '002' then sum(adm_cost)--间接成本 公用分摊
							when '003' then sum(apport_cost_s)--间接成本 管理分摊
							when '004' then sum(acf)--间接成本 医辅分摊
							when '005' then sum(act)--间接成本 医技分摊 
							when '006' then sum(tot_cost)--间接成本 间接成本
						 end		
			from report_sd_cost
				where dept_clin_code=@sd_code
				and dept_type='I'
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
				and (cost_subj_code=@code or @code='')
				and (list_code=@list_code or @list_code='') 
				and dept_proper=@type
		end
		else if('T'=@function_code)--医技成本
		begin	
			select @result=case @f_logic 
							when '001' then sum(D_cost)--自定义科室医技直接成本(自定义科室，项目，东西院)
							when '002' then sum(adm_cost)--间接成本 公用分摊
							when '003' then sum(apport_cost_s)--间接成本 管理分摊
							when '004' then sum(acf)--间接成本 医辅分摊
							when '005' then sum(act)--间接成本 医技分摊
							when '006' then sum(tot_cost)--间接成本 间接成本
						 end
			from report_sd_cost
				where dept_clin_code=@sd_code
				and dept_type='T'
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
				and (cost_subj_code=@code or @code='')
				and (list_code=@list_code or @list_code='') 
				and dept_proper=@type			
		end
		else if('F'=@function_code)--医辅成本
		begin	
			select @result=case @f_logic 
							when '001' then sum(D_cost)--自定义科室医辅直接成本(自定义科室，项目，东西院)
							when '002' then sum(adm_cost)--自定义科室医辅间接成本 公用分摊
							when '003' then sum(apport_cost_s)--自定义科室医辅间接成本 管理分摊
							when '004' then sum(acf)--自定义科室医辅间接成本 医辅分摊
							when '005' then sum(act)--自定义科室医辅间接成本 医技分摊
							when '006' then sum(tot_cost)--间接成本 间接成本
						 end
			from report_sd_cost
				where dept_clin_code=@sd_code
				and dept_type='F'
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
				and (cost_subj_code=@code or @code='')
				and (list_code=@list_code or @list_code='') 
				and dept_proper=@type
		end	
		else
		begin
			 select @result=case @function_code 
										  when 'DC'  then sum(D_cost)--自定义科室 直接成本
											when 'IDC' then sum(tot_cost)--自定义科室 间接成本
						 end					 							 
			 from report_sd_cost
				where dept_clin_code=@sd_code
				and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
		end
	if(@f_logic='007')--直接收入
	begin
		select  @result=
-- case @function_code 
-- 					when 'O' then (
						--直接收入
						--门诊
						(select sum(direct_income) from Report_defineDept_income
						where dept_clin_code=@sd_code
						and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
						and (dept_type=@function_code or @function_code='')
						and (charge_kind_code=@code or @code=''))
-- 					)
-- 					when 'I' then (
-- 						--住院
-- 						select sum(direct_income) from Report_defineDept_income
-- 						where dept_clin_code=@sd_code
-- 						and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
-- 						and dept_type='I'
-- 						and (charge_kind_code=@code or @code='')
-- 					)
-- 					when 'T' then (
-- 						--医技
-- 						select sum(direct_income) from Report_defineDept_income
-- 						where dept_clin_code=@sd_code
-- 						and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
-- 						and dept_type='T'
-- 						and (charge_kind_code=@code or @code='')
-- 					)
-- 					when 'F' then (
-- 						--医辅
-- 						select sum(direct_income) from Report_defineDept_income
-- 						where dept_clin_code=@sd_code
-- 						and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
-- 						and dept_type='F'
-- 						and (charge_kind_code=@code or @code='')
-- 					) 
-- 					end
	end
	else if(@f_logic='008')--间接收入
	begin
	
		select @result=
-- case @function_code
-- 						when 'O' then (
							--间接收入
							--门诊
							(select sum(indirect_income) from Report_defineDept_income
							where dept_clin_code=@sd_code
							and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
							and (dept_type=@function_code or @function_code='')
							and (charge_kind_code=@code or @code=''))
-- 						)
-- 						when 'I' then (
-- 							--住院
-- 							select sum(indirect_income) from Report_defineDept_income
-- 							where dept_clin_code=@sd_code
-- 							and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
-- 				and dept_type='I'
-- 							and (charge_kind_code=@code or @code='')
-- 						)
-- 						when 'T' then (
-- 							--医技
-- 							select sum(indirect_income) from Report_defineDept_income
-- 							where dept_clin_code=@sd_code
-- 							and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
-- 				and dept_type='T'
-- 							and (charge_kind_code=@code or @code='')
-- 						)
-- 						when 'F' then (
-- 							--医辅
-- 							select sum(indirect_income) from Report_defineDept_income
-- 							where dept_clin_code=@sd_code
-- 							and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
-- 				and dept_type='F'
-- 							and (charge_kind_code=@code or @code='')
-- 						)
-- 						end
	end
end

return isnull(@result,0)
end

-------------------------------------------------------
if exists(select * from sysobjects where name='report_sd_cost')
drop view report_sd_cost
go
create view report_sd_cost
as

SELECT b.dept_clin_code, b.dept_clin_name, b.dept_code, b.out_or_in, b.app_level, dept_type,
      b.year_month, b.cost_subj_code, b.prime_cost+isnull(c.adm_cost,0) D_cost,d.adm_cost,d.apport_cost_s,act,acf,isnull(d.adm_cost,0)+isnull(apport_cost_s,0)+isnull(act,0)+isnull(acf,0) tot_cost,dept_proper,list_code
FROM (SELECT a.dept_clin_code, a.dept_clin_name, a.dept_code, a.out_or_in, a.app_level, 
      a.dept_type, st_dept_cost.year_month, st_dept_cost.cost_subj_code, 
      st_dept_cost.prime_cost, a.dept_proper, report_cost_detail.list_code
			FROM (SELECT dict_clinic_dept_map.dept_clin_code, dict_clinic_dept_map.dept_type, 
              dict_clinic_dept.dept_clin_name, dict_clinic_dept_map.dept_code, 
              dict_acct_dept.out_or_in, dict_acct_dept.app_level, 
              te_dept_class_map.dept_class_code dept_proper
        FROM dict_clinic_dept INNER JOIN
              dict_clinic_dept_map ON 
              dict_clinic_dept.dept_clin_code = dict_clinic_dept_map.dept_clin_code INNER JOIN
              dict_acct_dept ON 
              dict_clinic_dept_map.dept_code = dict_acct_dept.dept_code LEFT OUTER JOIN
              te_dept_class_map ON 
              dict_clinic_dept_map.dept_code = te_dept_class_map.dept_code) 
      a INNER JOIN
      st_dept_cost ON 
      a.dept_code COLLATE Chinese_PRC_CI_AS = st_dept_cost.dept_code LEFT OUTER
       JOIN
      report_cost_detail ON 
      st_dept_cost.cost_subj_code = report_cost_detail.cost_code) 
      b LEFT OUTER JOIN
          (SELECT st_dept_cost.year_month, st_dept_cost.dept_code, 
               st_dept_cost.cost_subj_code, dict_cost_type_subj.cost_type_code, 
               st_dept_cost.adm_cost
         FROM st_dept_cost INNER JOIN
               dict_cost_type_subj ON 
               st_dept_cost.cost_subj_code = dict_cost_type_subj.cost_subj_code
         WHERE (dict_cost_type_subj.cost_type_code = '006')) c ON 
      b.year_month = c.year_month AND b.dept_code = c.dept_code AND 
      b.cost_subj_code = c.cost_subj_code
left join (
SELECT st_dept_cost.year_month, st_dept_cost.dept_code, 
      st_dept_cost.cost_subj_code, st_dept_cost.adm_cost, 
      st_dept_cost.apport_cost_s, 
      st_dept_cost.apport_cost_t - te_st_dept_cost_clinic_shel.apport_cost_t AS act,
       st_dept_cost.apport_cost_f - te_st_dept_cost_clinic_shel.apport_cost_f AS acf
FROM te_st_dept_cost_clinic_shel INNER JOIN
      st_dept_cost ON 
      te_st_dept_cost_clinic_shel.year_month = st_dept_cost.year_month AND 
      te_st_dept_cost_clinic_shel.dept_code = st_dept_cost.dept_code AND 
      te_st_dept_cost_clinic_shel.cost_subj_code = st_dept_cost.cost_subj_code
) d
on  b.year_month = d.year_month AND b.dept_code = d.dept_code AND 
      b.cost_subj_code = d.cost_subj_code

      
------------------------------------------------------IF EXISTS (SELECT * 
	   FROM   sysobjects 
	   WHERE  name = N'get_base_data')
	DROP FUNCTION get_base_data
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** 对象:  用户定义的函数 cbcs.get_base_data    脚本日期: 2006-1-18 13:25:05 ******/
ALTER  FUNCTION get_base_data 
	(@function_code nvarchar(10),
	@f_logic nvarchar(20),
	@code nvarchar(30),
	@byear nvarchar(4),
	@bm nvarchar(2),
	@nyear nvarchar(4),
	@nm nvarchar(2),
	@report_code nvarchar(30)
)
RETURNS  decimal(16,4)
AS
BEGIN

--内部参数
declare @result decimal(16,4)
declare @acct_byear nvarchar(4)
declare @begin_m nvarchar(2)
declare @acct_nyear nvarchar(4)
declare @end_m nvarchar(2)



set @result=0

select @acct_byear=@byear,@acct_nyear=@nyear,@begin_m=@bm,@end_m=@nm
--select top 1 @acct_byear=acct_year,@begin_m=start_month,@end_m=end_month,@report_code=report_code from acct_report_time

--    管理小计   		D01
--    医辅小计   		D02
--    医技小计   		D03
--    直接医疗小技 	D04
--    科室集合   		D99
--    收费集合   		I99
--    成本集合    	C99


-- 直接记入成本			001        
-- 分摊直接成本			002        
-- 公用成本					003            
-- 管理成本一次分摊	004    
-- 医辅成本					005            
-- 医技成本					006            
-- 固定成本					007            
-- 变动成本					008            
-- 可控成本					009            
-- 不可控成本				010          
-- 管理成本					011            
-- 人力成本					012            
-- 离退休人员成本		013      
-- 材料成本					014            
-- 净药品成本				015          
-- 折旧成本					016            
-- 其它成本					017            
-- 门诊医疗收入			018        
-- 住院医疗收入			019        
-- 门诊药品收入			020        
-- 住院药品收入			021                
-- 财政补助收入			022                
-- 本科开单本科执行的收入23      
-- 
-- 门诊医疗全成本 			031             
-- 住院医疗全成本 			032      
-- 门诊药品全成本 			033      
-- 住院药品全成本 			034      
-- 门诊医疗科研全成本 035  
-- 住院医疗科研全成本 036  
-- 门诊药品科研全成本 037  
-- 住院药品科研全成本 038  
-- 未纳入成本项目  		039  
-- 直接计算记入成本   040


--逻辑
if(@function_code='A01')--全院总计 A01
begin
	if('031'=@f_logic)--门诊医疗全成本
		select @result=sum(tot_amount) --O_T_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_subj
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dict_cost_subj.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
			and out_or_in='O' 
			and treat_or_med='T' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('032'=@f_logic)--住院医疗全成本
		select @result=sum(tot_amount) --I_T_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_subj
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dict_cost_subj.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and out_or_in='I' 
			and treat_or_med='T' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('033'=@f_logic)--门诊药品全成本
		select @result=sum(tot_amount) --O_M_cost
	  from st_dept_cost s,dict_acct_dept d,dict_cost_subj
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dict_cost_subj.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and out_or_in='O' 
			and treat_or_med='M' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('034'=@f_logic)--住院药品全成本   034
		select @result=sum(tot_amount) --I_M_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_subj
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dict_cost_subj.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and out_or_in='I' 
			and treat_or_med='M' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N'))

	else if('035'=@f_logic)--门诊医疗科研全成本  035
		select @result=sum(tot_amount) --O_T_Cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_subj
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dict_cost_subj.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m
			and out_or_in='O' 
			and treat_or_med='T' 
			and (app_level=4 and cost_app_ind='N')

	else if('036'=@f_logic)--住院医疗科研全成本  036
		select @result=sum(tot_amount) --O_T_Cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_subj
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dict_cost_subj.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and out_or_in='I' 
			and treat_or_med='T' 
			and (app_level=4 and cost_app_ind='N')

	else if('037'=@f_logic)--门诊药品科研全成本   037
		select @result=sum(tot_amount) --O_T_Cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_subj
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dict_cost_subj.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and out_or_in='O' 
			and treat_or_med='M' 
			and (app_level=4 and cost_app_ind='N')

	else if('038'=@f_logic)--住院药品科研全成本   038
		select @result=sum(tot_amount) -- O_T_Cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_subj
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dict_cost_subj.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and out_or_in='I' 
			and treat_or_med='M' 
			and (app_level=4 and cost_app_ind='N')

	else if('007'=@f_logic)--固定成本007
		select @result=sum(tot_amount) -- fixed_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and stop_mark='N' 
			and cost_type_code='001' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('008'=@f_logic)--变动成本008
		select @result=sum(tot_amount) -- dy_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and stop_mark='N' 
			and cost_type_code='002' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('009'=@f_logic)--可控成本009
		select @result=sum(tot_amount) -- contral_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and stop_mark='N' 
			and cost_type_code='003' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('010'=@f_logic)--不可控成本010
		select @result=sum(tot_amount) -- uncontral_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and stop_mark='N' 
			and cost_type_code='004' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('011'=@f_logic)--管理成本011  
		select @result=sum(tot_amount) -- manage_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and stop_mark='N' 
			and cost_type_code='008' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('012'=@f_logic)--人力成本012
		select @result=sum(tot_amount) -- manpower_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and stop_mark='N' 
			and cost_type_code='010' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('013'=@f_logic)--离退休人员成本013
		select @result=sum(tot_amount) -- retire_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and stop_mark='N' 
			and cost_type_code='012' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('014'=@f_logic)--材料成本014
		select @result=sum(tot_amount) -- material_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and stop_mark='N' 
			and cost_type_code='011' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('015'=@f_logic)--净药品成本015
		select @result=sum(tot_amount) -- pdurg_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and stop_mark='N' 
			and cost_type_code='005' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('016'=@f_logic)--折旧成本016
		select @result=sum(tot_amount) -- depreciation_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and stop_mark='N' 
			and cost_type_code='009' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 

	else if('017'=@f_logic)--其它成本017
		select @result=sum(tot_amount) -- oth_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and stop_mark='N' 
			and cost_type_code='013' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
	else if('039'=@f_logic)--未纳入成本项目039
	begin
		select @result=sum(amount) -- bq 
				from cost_detail 
				where dept_code='UNCOUNT'
				and pay_date>= (--根据给定的起始年月结合存储的日数值构造起始日期（为了解决存储日期超过本月最后一天）
												--基本思想：（基本年月）＋（一个月）－（一天）得到该月的最后一天
												--比较存储的日与本月的最后一天的大小
												--存储日小于本月最后一日：用基本年月同存储日构造日期
												--否则：用本月的最后一天构造日期
												select case when start_day<(
																										select day(
																																dateadd(
																																				day,-1,dateadd(
																																												month,1,convert(datetime,@acct_byear+@begin_m+'01')
																																											)
																																				)
																															)
																										) 
																then @acct_byear+'-'+@begin_m+('-'+cast(start_day as nvarchar)) 
																else 
																dateadd(
																				day,-1,dateadd(
																												month,1,convert(datetime,@acct_byear+@begin_m+'01')
																											)
																)
																end 
												from com_info
											) 
				and pay_date<(--思想同上，唯一不同点是期间的概念需要使得在结束月的基础上加一
											select case when start_day<(
																									select day(
																															dateadd(
																																			day,-1,dateadd(
																																											month,2,convert(datetime,@acct_byear+@begin_m+'01')
																																										)
																																			)
																														)
																									) 
															then convert(nvarchar(7),dateadd(month,1,convert(datetime,@acct_byear+@begin_m+'01')),120)+(+'-'+cast(start_day as nvarchar)) 
															else 
															dateadd(
																			day,-1,dateadd(
																											month,2,convert(datetime,@acct_byear+@begin_m+'01')
																										)
															)
															end 
											from com_info
											)
	end
end
else if(@function_code='D99')--科室集合 D99
begin
--select '科室集合'
	
	if('001'=@f_logic)--直接记入成本001
		select @result=sum(prime_cost)--直接记入成本001
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m

	else if('002'=@f_logic)--分摊直接成本002
		select @result=sum(apport_direct_cost)--分摊直接成本002
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m

	else if('003'=@f_logic)--公用成本003
		select @result=sum(public_cost)--公用成本003
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m

	else if('004'=@f_logic)--管理成本一次分摊004
		select @result=sum(manage_cost_appor)--管理成本一次分摊004
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m

	else if('005'=@f_logic)--医辅成本005
		select @result=sum( apport_cost_t)--医辅成本005
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m

	else if('006'=@f_logic)--医技成本006
		select @result=sum( apport_cost_f)--医技成本006
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m

	else if('007'=@f_logic)--固定成本007
		select @result=sum( fixed_cost)--固定成本007
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m

	else if('008'=@f_logic)--变动成本008
		select @result=sum( dy_cost)--变动成本008
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m

	else if('009'=@f_logic)--可控成本009
		select @result=sum( control_cost)--可控成本009
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m

	else if('010'=@f_logic)--不可控成本010
		select @result=sum( uncontrol_cost)--不可控成本010
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m

	else if('011'=@f_logic)--管理成本011   
		select @result=sum( manage_cost)--管理成本011
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m	
			
	else if('012'=@f_logic)--人力成本012
		select @result=sum( manpower_cost)--人力成本012
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m
			
	else if('013'=@f_logic)--离退休人员成本013
		select @result=sum( retire_cost)--离退休人员成本013
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m	
				
	else if('014'=@f_logic)--材料成本014
		select @result=sum( material_cost)--材料成本014
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m	
				
	else if('015'=@f_logic)--净药品成本015
		select @result=sum( pure_M_cost)--净药品成本015
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m		
	else if('016'=@f_logic)--折旧成本016
		select @result=sum( depreciation_cost)--折旧成本016
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m
			
	else if('017'=@f_logic)--其它成本017
		select @result=sum( other_cost)--其它成本017
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m	
				
	else if('018'=@f_logic)--门诊医疗收入018
		select @result=sum( O_T_income)--门诊医疗收入018
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m
			
	else if('019'=@f_logic)--住院医疗收入019
		select @result=sum( I_T_income)--住院医疗收入019
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m
			
	else if('020'=@f_logic)--门诊药品收入020
		select @result=sum( O_M_income)--门诊药品收入020
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m	
			and year_month<=@acct_nyear+@end_m
			
	else if('021'=@f_logic)--住院药品收入021
		select @result=sum( I_M_income)--住院药品收入021
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m
			
	else if('022'=@f_logic)--财政补助收入022
		select @result=sum(fund_subsidy_income) --财政补助收入022
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m
			
	else if('023'=@f_logic)--本科开单本科执行的收入023
		select @result=sum(self_exec_income) --本科开单本科执行的收入23
		from acct_report_dept_data
			where dept_code=@code
			and year_month>=@acct_byear+@begin_m
			and year_month<=@acct_nyear+@end_m

 end
-- else if(@function_code='D01')--管理小计   D01
-- begin
-- select '管理小计'
-- 
-- end
-- else if(@function_code='D02')--医辅小计   D02
-- begin
-- select '医辅小计'
-- 
-- end
-- else if(@function_code='D03')--医技小计   D03
-- begin
-- select '医技小计'
-- 
-- end
-- else if(@function_code='D04')--直接医疗小技  D04
-- begin
-- select '直接医疗小技'
-- 
-- end
else if(@function_code='I99')--收费集合   I99
begin
--select '收费集合'

	if('018'=@f_logic)--门诊医疗收入018
	  select @result=sum(amount) -- idx_value
	  from st_dept_income s, dict_acct_dept d,b_dict_charge_detail_kind
			where s.ordered_by=d.dept_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and s.charge_kind_code=b_dict_charge_detail_kind.charge_kind_code 
			and income_type='T'
			and out_or_in='O'
			and s.charge_kind_code='006'

	else if('019'=@f_logic)--住院医疗收入019
	  select @result=sum(amount) -- idx_value
	  from st_dept_income s, dict_acct_dept d,b_dict_charge_detail_kind
			where s.ordered_by=d.dept_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and s.charge_kind_code=b_dict_charge_detail_kind.charge_kind_code 
			and income_type='T'
			and out_or_in='I'
			and s.charge_kind_code='006'

	else if('020'=@f_logic)--门诊药品收入020
	  select @result=sum(amount) -- idx_value
	  from st_dept_income s, dict_acct_dept d,b_dict_charge_detail_kind
			where s.ordered_by=d.dept_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and s.charge_kind_code=b_dict_charge_detail_kind.charge_kind_code 
			and income_type='M'
			and out_or_in='O' 
			and s.charge_kind_code='006'

	else if('021'=@f_logic)--住院药品收入021
	  select @result=sum(amount) -- idx_value
	  from st_dept_income s, dict_acct_dept d,b_dict_charge_detail_kind
			where s.ordered_by=d.dept_code 
			and year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and s.charge_kind_code=b_dict_charge_detail_kind.charge_kind_code 
			and income_type='M'
			and out_or_in='I' 
			and s.charge_kind_code='006'


end
else if(@function_code='C99')--成本集合    C99
begin
--select '成本集合'

	if('040'=@f_logic)--直接计算记入记入成本 040
		select @result=sum(adm_cost) 
		from report_dept_cost_subj,dict_cost_type_subj
		where year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m  
		and cost_type_code='006'
		and report_dept_cost_subj.cost_subj_code=dict_cost_type_subj.cost_subj_code
		and report_dept_cost_subj.cost_subj_code=@code
	else if('003'=@f_logic)--公用成本003
		select @result=sum(adm_cost) 
		from report_dept_cost_subj,dict_cost_type_subj
		where year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m  
		and cost_type_code<>'006'
		and report_dept_cost_subj.cost_subj_code=dict_cost_type_subj.cost_subj_code
		and report_dept_cost_subj.cost_subj_code=@code

	else
		select @result=case @f_logic 
										when '001' then sum(prime_cost)--直接记入成本001
										when '011' then sum(apport_cost_s)--管理成本011
										when '005' then sum(apport_cost_t)--医辅成本013
										when '006' then sum(apport_cost_f)--医技成本006
										end			
		from report_dept_cost_subj
			where year_month>=@acct_byear+@begin_m	and year_month<=@acct_nyear+@end_m 
			and cost_subj_code=@code
end
return isnull(@result,0)
end

GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

