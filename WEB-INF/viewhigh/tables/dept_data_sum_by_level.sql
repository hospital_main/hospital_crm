 
create TABLE #result
	(
	year_month char(6) NOT NULL,
	dept_code char(20) NOT NULL,
	level int, --本科室级别
	supper_dept nvarchar(20), --上级科室
	prime_cost money ,--直接记入成本
	apport_direct_cost money ,--分摊直接成本002
	public_cost money ,--公用成本003
	manage_cost  money , --管理成本004
	apport_cost_s money  ,--管理成本一次分摊004
	apport_cost_t money , --医辅成本005
	apport_cost_f money , --医技成本006
	fixed_cost money , --固定成本007 
	dy_cost money  ,--变动成本008
	control_cost money  ,--可控成本009
	uncontrol_cost money  ,--不可控成本010 	
	manpower_cost money  ,--人力成本012
	retire_cost money  ,--离退休人员成本013
	material_cost money  ,--材料成本014
	pure_M_cost money  ,--净药品成本015
	depreciation_cost money  ,--折旧成本016
	other_cost money  ,--其它成本017
	O_T_income money  ,--门诊医疗收入018
	I_T_income money  ,--住院医疗收入019
	O_M_income money  ,--门诊药品收入020
	I_M_income money  ,--门诊药品收入021
	fund_subsidy_income money  ,--财政补助收入022
	self_exec_income money  ,--本科开单本科执行的收入023 	
	O_T_cost money  ,--门诊医疗成本024
	I_T_cost money  ,--住院医疗成本025
	O_M_cost money  ,--门诊药品成本026
	I_M_cost money  ,--门诊药品成本027
)  --创建临时表,存放中间数据

CREATE TABLE #dept_level( id int IDENTITY(1,1),
dept_code char(20),level int)
declare @current char(20)
set @current = 'TOP'
DECLARE @level int, @line char(20)
CREATE TABLE #stack (item char(20), level int)
INSERT INTO #stack VALUES (@current, 1)
SELECT @level = 1
 WHILE @level > 0
BEGIN
   IF EXISTS (SELECT * FROM #stack WHERE level = @level)
      BEGIN
 SELECT top 1 @current = item
           FROM #stack
           WHERE level = @level

 INSERT  #dept_level ( dept_code, level) values( @current,@level) 
      DELETE FROM #stack
           WHERE level = @level
             AND item = @current
         INSERT #stack 

          SELECT dept_code, @level + 1
             FROM dict_acct_dept
               WHERE supper_dept = @current     
 
           IF @@ROWCOUNT > 0
            SELECT @level = @level + 1
      END
   ELSE
      SELECT @level = @level - 1
END
 
declare @year_month char(6)
set @year_month = 200509

insert into #result
select  @year_month year_month,base.dept_code,level,supper_dept,
	 isnull(prime_cost,0) prime_cost,isnull(apport_direct_cost,0) apport_direct_cost,isnull(public_cost,0) public_cost,
	 isnull(manage_cost,0) manage_cost,isnull(apport_cost_s,0) apport_cost_s,isnull(apport_cost_t,0) apport_cost_t,
	 isnull(apport_cost_f,0) apport_cost_f,isnull(fixed_cost,0) fixed_cost,isnull(dy_cost,0) dy_cost,isnull(control_cost,0) control_cost,
	 isnull(uncontrol_cost,0) uncontrol_cost,isnull(manpower_cost,0) manpower_cost ,isnull(retire_cost,0) retire_cost ,
	 isnull(material_cost,0) material_cost,isnull(pure_M_cost,0) pure_M_cost ,isnull(depreciation_cost,0) depreciation_cost,
	 isnull(other_cost,0) other_cost,
	 isnull(O_T_income,0) O_T_income,isnull(O_M_income,0) O_M_income,isnull(I_T_income,0) I_T_income,
	 isnull(I_M_income,0) I_M_income,
	 isnull(O_T_cost,0) O_T_cost,isnull(O_M_cost,0) O_M_cost,isnull(I_T_cost,0) I_T_cost,
	 isnull(I_M_cost,0) I_M_cost,
	 isnull(fund_subsidy_income,0) fund_subsidy_income,isnull(self_exec_income,0) self_exec_income
  from #dept_level base
left join 
(----直接记入成本001 管理成本一次分摊004 --医辅成本005 --医技成本006 
   select dept_code,sum(prime_cost) prime_cost,sum(apport_cost_s)apport_cost_s,
			sum(apport_cost_t)apport_cost_t,sum(apport_cost_f) apport_cost_f  
		from st_dept_cost
			where year_month= @year_month
			group by dept_code 
)prime 
  on base.dept_code = prime.dept_code
left join 
(		select dept_code,sum(adm_cost) apport_direct_cost
		from st_dept_cost,dict_cost_type_subj
		where year_month= @year_month
		and cost_type_code='006'
		and st_dept_cost.cost_subj_code=dict_cost_type_subj.cost_subj_code
		group by dept_code
)app_direct --分摊直接成本002
 on base.dept_code = app_direct.dept_code
left join 
(
		select dept_code,sum(adm_cost) public_cost
		from st_dept_cost,dict_cost_type_subj
		where year_month= @year_month
		and cost_type_code<>'006'
		and st_dept_cost.cost_subj_code=dict_cost_type_subj.cost_subj_code
		group by dept_code
)pub_cost --公用成本003
 on base.dept_code = pub_cost.dept_code
left join
(--管理费用成本011 
	select dept_code,sum(amount) manage_cost
		from st_dept_cost_child			
			where year_month= @year_month
			group by dept_code 
)manage_c 
 on base.dept_code = manage_c.dept_code
left join
(----固定成本007
		select d.dept_code , sum(tot_amount) fixed_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month= @year_month
			and stop_mark='N' 
			and cost_type_code='001' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
			group by d.dept_code 
)fixed 
 on base.dept_code = fixed.dept_code
left join
(----变动成本008
		select d.dept_code ,sum(tot_amount) dy_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month= @year_month
			and stop_mark='N' 
			and cost_type_code='002' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
			group by d.dept_code 
)unfixed 
 on base.dept_code = unfixed.dept_code
left join
(----可控成本009
		select d.dept_code,sum(tot_amount) control_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month= @year_month
			and stop_mark='N' 
			and cost_type_code='003' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N'))
			group by d.dept_code 

)control  on base.dept_code = control.dept_code
left join
(----不可控成本010
		select d.dept_code ,sum(tot_amount) uncontrol_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month= @year_month
			and stop_mark='N' 
			and cost_type_code='004' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
			group by d.dept_code 
)uncontrol   on base.dept_code = uncontrol.dept_code
left join
(----人力成本012
		select  d.dept_code ,sum(tot_amount) manpower_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month= @year_month
			and stop_mark='N' 
			and cost_type_code='010' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
			group by d.dept_code 
)manpower  on base.dept_code = manpower.dept_code
left join
(----离退休人员成本013
		select d.dept_code,sum(tot_amount) retire_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month= @year_month
			and stop_mark='N' 
			and cost_type_code='012' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
			group by d.dept_code
)retired  on base.dept_code = retired.dept_code
left join
(	--材料成本
	select d.dept_code,sum(tot_amount) material_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month=@year_month 
			and stop_mark='N' 
			and cost_type_code='011' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
		group by d.dept_code 
)material on base.dept_code = material.dept_code
left join
(----净药品成本015
		select  d.dept_code,sum(tot_amount) pure_M_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dc.cost_subj_code 		and year_month= @year_month
				and stop_mark='N' 			and cost_type_code='005' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
			group by d.dept_code
)pure_drug  on base.dept_code = pure_drug.dept_code 
left join
(----折旧成本016
		select d.dept_code,sum(tot_amount) depreciation_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month= @year_month
			and stop_mark='N' 
			and cost_type_code='009' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N'))
		group by d.dept_code		 
) depreciation  on base.dept_code = depreciation.dept_code 
left join
(--其它成本017
		select d.dept_code ,sum(tot_amount) other_cost
		from st_dept_cost s,dict_acct_dept d,dict_cost_type_subj dc 
			where s.dept_code=d.dept_code 
			and s.cost_subj_code=dc.cost_subj_code 
			and year_month= @year_month
			and stop_mark='N' 
			and cost_type_code='013' 
			and (app_level=5 or (app_level=4 and cost_app_ind='N'))
		group by d.dept_code 
)other
on base.dept_code = other.dept_code
left join
(----门诊医疗收入018
	  select d.dept_code, sum(amount) O_T_income
	  from st_dept_income s, dict_acct_dept d,b_dict_charge_detail_kind
			where s.ordered_by=d.dept_code 
			and year_month= @year_month
			and s.charge_kind_code=b_dict_charge_detail_kind.charge_kind_code 
			and income_type='T'
			and out_or_in='O'
			and app_level=5 
			group by d.dept_code		 
)O_T  on base.dept_code = O_T.dept_code
left join
(--住院医疗收入019
 select d.dept_code, sum(amount) I_T_income
	  from st_dept_income s, dict_acct_dept d,b_dict_charge_detail_kind
			where s.ordered_by=d.dept_code 
			and year_month= @year_month
			and s.charge_kind_code=b_dict_charge_detail_kind.charge_kind_code 
			and income_type='T'
			and out_or_in='I'
			and app_level=5
		group by d.dept_code	
)I_T on base.dept_code = I_T.dept_code
left join 
(--门诊药品收入020
	  select ordered_by dept_code,sum(amount) O_M_income
	  from st_dept_income s, dict_acct_dept d,b_dict_charge_detail_kind
			where s.ordered_by=d.dept_code 
			and year_month= @year_month
			and s.charge_kind_code=b_dict_charge_detail_kind.charge_kind_code 
			and income_type='M'
			and out_or_in='O' 
			and app_level=5
			group by ordered_by

)O_M on base.dept_code = O_M.dept_code
left join 
(--住院药品收入021
	  select ordered_by dept_code,sum(amount) I_M_income
	  from st_dept_income s, dict_acct_dept d,b_dict_charge_detail_kind
			where s.ordered_by=d.dept_code 
			and year_month= @year_month
			and s.charge_kind_code=b_dict_charge_detail_kind.charge_kind_code 
			and income_type='M'
			and out_or_in='I' 
			and app_level=5
			group by ordered_by		 

)I_M  on base.dept_code = I_M.dept_code
left join 
(--财政补助收入022
		select dept_code,sum(tot_amount) fund_subsidy_income
		from st_fins_in_allot 
			where year_month= @year_month
			group by dept_code

)subsidy on base.dept_code = subsidy.dept_code
left join 
(--本科开单本科执行的收入023
		select ordered_by dept_code,sum(amount) self_exec_income
		from st_dept_income 
			where ordered_by=perform_by
			and year_month= @year_month
			group by ordered_by
)self  on base.dept_code = self.dept_code
left join
(--门诊医疗成本024
		select s.dept_code,sum(tot_amount) --O_T_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_subj
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dict_cost_subj.cost_subj_code 
				and year_month=@year_month
				and out_or_in='O' 
				and treat_or_med='T' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
			group by s.dept_code
)O_T_C on O_T_C.dept_code=base.dept_code
left join
(--住院医疗成本025
		select s.dept_code,sum(tot_amount) --I_T_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_subj
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dict_cost_subj.cost_subj_code 
				and year_month=@year_month
				and out_or_in='I' 
				and treat_or_med='T' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
			group by s.dept_code
)I_T_C on I_T_C.dept_code=base.dept_code
left join
(--门诊药品成本026
		select s.dept_code,sum(tot_amount) --O_M_cost
		  from st_dept_cost s,dict_acct_dept d,dict_cost_subj
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dict_cost_subj.cost_subj_code 
				and year_month=@year_month
				and out_or_in='O' 
				and treat_or_med='M' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N')) 
			group by s.dept_code
)O_M_C on O_M_C.dept_code=base.dept_code
left join
(--门诊药品成本027
		select s.dept_code,sum(tot_amount) --I_M_cost
			from st_dept_cost s,dict_acct_dept d,dict_cost_subj
				where s.dept_code=d.dept_code 
				and s.cost_subj_code=dict_cost_subj.cost_subj_code 
				and year_month=@year_month
				and out_or_in='I' 
				and treat_or_med='M' 
				and (app_level=5 or (app_level=4 and cost_app_ind='N'))
			group by s.dept_code
)I_M_C on I_M_C.dept_code=base.dept_code
, dict_acct_dept
where base.dept_code = dict_acct_dept.dept_code

-- 进行逐级累计结果
  select @level=max(level) from #result--取得最大级别
     while (@level>=1)
    begin
     update #result        
			set  prime_cost = mid.prime_cost,apport_direct_cost = mid.apport_direct_cost,public_cost = mid.public_cost,
			 manage_cost = mid.manage_cost,apport_cost_s = mid.apport_cost_s,apport_cost_t = mid.apport_cost_t,
			 apport_cost_f = mid.apport_cost_f,fixed_cost = mid.fixed_cost,dy_cost = mid.dy_cost,control_cost = mid.control_cost,
			 uncontrol_cost = mid.uncontrol_cost,manpower_cost = mid.manpower_cost ,retire_cost = mid.retire_cost ,
			 material_cost = mid.material_cost,pure_M_cost = mid.pure_M_cost ,depreciation_cost = mid.depreciation_cost,
			 other_cost = mid.other_cost,
			 O_T_income = mid.O_T_income,I_T_income = mid.I_T_income,
			 O_M_income = mid.O_M_income,I_M_income = mid.I_M_income,
			 O_T_cost = mid.O_T_cost,I_T_cost = mid.I_T_cost,
			 O_M_cost = mid.O_M_cost,I_M_cost = mid.I_M_cost,
			 fund_subsidy_income = mid.fund_subsidy_income,self_exec_income = mid.self_exec_income
      from (
         select supper_dept, sum(prime_cost) prime_cost,sum(apport_direct_cost) apport_direct_cost,sum(public_cost) public_cost,
				 sum(manage_cost) manage_cost,sum(apport_cost_s) apport_cost_s,sum(apport_cost_t) apport_cost_t,
				 sum(apport_cost_f) apport_cost_f,sum(fixed_cost) fixed_cost,sum(dy_cost) dy_cost,sum(control_cost) control_cost,
				 sum(uncontrol_cost) uncontrol_cost,sum(manpower_cost) manpower_cost ,sum(retire_cost) retire_cost ,
				 sum(material_cost) material_cost,sum(pure_M_cost) pure_M_cost ,sum(depreciation_cost) depreciation_cost,
				 sum(other_cost) other_cost,
				 sum(O_T_income) O_T_income,sum(I_T_income) I_T_income,
				 sum(O_M_income) O_M_income,sum(I_M_income) I_M_income,
				 sum(O_T_cost) O_T_cost,sum(I_T_cost) I_T_cost,
				 sum(O_M_cost) O_M_cost,sum(I_M_cost) I_M_cost,
				 sum(fund_subsidy_income) fund_subsidy_income,sum(self_exec_income) self_exec_income 
       from #result 
         where level=@level 
          group by supper_dept
)mid --按照级别进行汇总
      where #result.dept_code=mid.supper_dept       
    
    set @level=@level-1
    end
   
insert into acct_report_dept_data
select year_month,dept_code,prime_cost,apport_direct_cost,public_cost,manage_cost,apport_cost_s,apport_cost_t,
apport_cost_f,fixed_cost,dy_cost,control_cost,uncontrol_cost,manpower_cost,retire_cost,material_cost,
pure_M_cost,depreciation_cost,other_cost,O_T_income,I_T_income,O_M_income,I_M_income,fund_subsidy_income,self_exec_income  from #result
 
	
	drop table #result
	drop table #dept_level
	drop table #stack
