<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<xsl:decimal-format NaN=''/>
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' colspan="3">收入</th>
				<th nowrap='true' colspan="3">支出</th> 
				<th nowrap='true' colspan="3">收入排名</th> 
			</tr>              
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' >项目</th>
				<th nowrap='true' >当天发生</th>
				<th nowrap='true' >本年累计</th> 
				<th nowrap='true' >项目</th>
				<th nowrap='true' >当天发生</th>
				<th nowrap='true' >本年累计</th> 
				<th nowrap='true' >名次</th> 
				<th nowrap='true' >科室</th>
				<th nowrap='true' >收入</th> 
			</tr>              
		</thead>
	<tbody>
		<xsl:for-each select="/root/data/r">
			<tr>
				<xsl:for-each select="c">
					<xsl:choose>
						<xsl:when test="position() = 2 or position()=3 or position() =5 or position()=6 or position() = 9">
							<td align="right">
								<xsl:if test=". != '' and . != 0">
									<xsl:value-of select="format-number(.,'#,##0.00')" />
								</xsl:if> 
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="." /> </td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

