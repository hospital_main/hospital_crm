<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' colspan="3">门诊</th>
				<th nowrap='true' colspan="6">住院</th> 
			</tr>              
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' >名次</th> 
				<th nowrap='true' >科室</th>
				<th nowrap='true' >诊次</th> 
				<th nowrap='true' >名次</th> 
				<th nowrap='true' >科室</th>
				<th nowrap='true' >出院人数</th> 
				<th nowrap='true' >人均住院费用</th> 
				<th nowrap='true' >其中:医保出院人数</th> 
				<th nowrap='true' >医保均次费用</th> 
			</tr>              
		</thead>
	<tbody>
		<xsl:for-each select="/root/data/r">
			<tr>
				<xsl:for-each select="c">
					<xsl:choose>
						<xsl:when test="position() = 7 or position() = 9 ">
							<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')" /> </td>
						</xsl:when>
						<xsl:when test="position() = 1 or position() = 4 ">
							<td align="center"><xsl:value-of select="." /> </td>
						</xsl:when>
						<xsl:when test="position() = 3 or position() = 6">
							<td align="right"><xsl:value-of select="." /> </td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="." /> </td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

