<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/modpassword.jsp,v 1.1 2012/03/12 01:59:20 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:20 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.base.mvc.controller.MainJspPage" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  { 
  	if(isEmpty(template.password))
    {
      alert('新密码不能为空!');
      return;
    }
   if(isTooLong(template.password,20))
    {
      alert('密码不能高于20个字符!');
      return;
    }
     if(template.password.value!=template.ensurepassword.value)
     { alert("确认密码输入不正确");
       template.ensurepassword.value="";
       template.ensurepassword.focus();
       return;
     }
     template.submit();
    return true;
  }
  
</Script>

<html:html clazz="main">
<form name="template" method="post" targer="_self" action="modpassword.viewhigh">

  <!-- 信息提示栏 -->
  <%=new com.viewhigh.base.mvc.view.MessageTip(request)%>

  <!-- 标题栏 -->
  <%=new com.viewhigh.base.mvc.view.MessageTitle("用户密码修改页面")%>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">旧密码：</td>
      <td class="normalText" nowrap="nowrap"><input type=password name="oldpassword" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">新密码:</td>
      <td class="normalText" nowrap="nowrap"><input type=password name="password" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">请再次输入新密码：</td>
      <td class="normalText" nowrap="nowrap"><input type=password name="ensurepassword" class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2">
      <button class="pageBtn" name=""   onclick="create();"  >保存</button>
      <button class="pageBtn" onclick="template.reset();"  >重置</button> 
      <!--<img src="images/reset.gif" class="mouse" onclick="template.reset();" />-->
      	<img id='retButton' src="images/return.gif" class="mouse" onclick="window.close();" />
      </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="store"/>
</form>
</html:html>
