/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/javascript/tree.js,v 1.1 2012/03/12 01:59:21 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:59:21 $
 * $Modtime: $
 * $Revision: 1.1 $
 */
function Folder(folderDescription, hreference) //constructor
{
  //constant data
  this.desc = folderDescription
  this.hreference = hreference
  this.id = nEntries
  this.navObj = 0
  this.iconImg = 0
  this.nodeImg = 0
  this.isLastNode = 1

  //dynamic data
  this.isOpen = true
  this.iconSrc = "images/biao.gif"
  this.children = new Array
  this.nChildren = 0

  //methods
  this.initialize = initializeFolder
  this.setState = setStateFolder
  this.addChild = addChild
  this.createIndex = createEntryIndex
  this.hide = hideFolder
  this.display = display
  this.renderOb = drawFolder
  this.subEntries = folderSubEntries
  this.outputLink = outputFolderLink
}

function setStateFolder(isOpen)
{
  this.isOpen = isOpen
  propagateChangesInState(this)
}

function propagateChangesInState(folder)
{
  var i=0
  if (folder.isOpen)
  {
    if (folder.nodeImg)
      if (folder.isLastNode)
        folder.nodeImg.src = "images/ftv2blank.gif"
      else
	  folder.nodeImg.src = "images/ftv2blank.gif"
    folder.iconImg.src = "images/biao.gif"
    for (i=0; i<folder.nChildren; i++)
      folder.children[i].display()
  }
  else
  {
    if (folder.nodeImg)
      if (folder.isLastNode)
        folder.nodeImg.src = "images/ftv2blank.gif"
      else
	  folder.nodeImg.src = "images/ftv2blank.gif"
    folder.iconImg.src = "images/biao1.gif"
    for (i=0; i<folder.nChildren; i++)
      folder.children[i].hide()
  }
}

function hideFolder()
{
  this.navObj.style.display = "none"
  this.setState(0)
}

function initializeFolder(level, lastNode, leftSide)
{
var j=0
var i=0
var numberOfFolders
var numberOfDocs
var nc

  nc = this.nChildren
  this.createIndex()

  var auxEv = ""
  auxEv = "<a>"
  if (level>0)
    if (lastNode) //the last 'brother' in the children array
    {
      this.renderOb(leftSide + auxEv + "<img name='nodeIcon" + this.id + "' src='images/ftv2blank.gif' width=16 height=22 border=0></a>")
      leftSide = leftSide + "<img src='images/ftv2blank.gif' width=16 height=22>"
      this.isLastNode = 1
    }
    else
    {
      this.renderOb(leftSide + auxEv + "<img name='nodeIcon" + this.id + "' src='images/ftv2blank.gif' width=16 height=22 border=0></a>")
      leftSide = leftSide + "<img src='images/ftv2blank.gif' width=16 height=22>"
      this.isLastNode = 0
    }
  else
    this.renderOb("")

  if (nc > 0)
  {
    level = level + 1
    for (i=0 ; i < this.nChildren; i++)
    {
      if (i == this.nChildren-1)
        this.children[i].initialize(level, 1, leftSide)
      else
        this.children[i].initialize(level, 0, leftSide)
      }
  }
}

function drawFolder(leftSide)
{
  doc.write("<table id='node" + this.id + "' style='position:block;' border=0 cellspacing=0 cellpadding=0>")
  doc.write("<tr><td>")
  doc.write(leftSide)
  this.outputLink()
  doc.write("<img name='folderIcon" + this.id + "' src='" + this.iconSrc+"' border=0></a></td><td valign=middle nowrap>")
  if (USETEXTLINKS)
  {
    this.outputLink()
    doc.write(this.desc + "</a>")
  }
  else
    doc.write(this.desc)
  doc.write("</td>")
  doc.write("</table>")
  this.navObj = doc.all["node"+this.id]
  this.iconImg = doc.all["folderIcon"+this.id]
  this.nodeImg = doc.all["nodeIcon"+this.id]
}

function outputFolderLink()
{
  if (this.hreference){
    doc.write("<a href='" + this.hreference + "' TARGET=\"Iframe_table\" onClick='javascript:clickOnFolder("+this.id+")'>")}
  else
    //doc.write("<a>")
  doc.write("<a href='javascript:clickOnNode("+this.id+")'>")
}

function addChild(childNode)
{
  this.children[this.nChildren] = childNode
  this.nChildren++
  return childNode
}

function folderSubEntries()
{
  var i = 0
  var se = this.nChildren

  for (i=0; i < this.nChildren; i++){
    if (this.children[i].children) //is a folder
      se = se + this.children[i].subEntries()
  }

  return se
}


// Definition of class Item (a document or link inside a Folder)
// *************************************************************

function Item(itemDescription, iconSrc, itemLink) // Constructor
{
  // constant data
  this.desc = itemDescription
  this.link = itemLink
  this.id = -1 //initialized in initalize()
  this.navObj = 0 //initialized in render()
  this.iconImg = 0 //initialized in render()
  this.nodeImg = 0
  this.iconSrc = "I"+iconSrc+".gif"

  // methods
  this.initialize = initializeItem
  this.createIndex = createEntryIndex
  this.hide = hideItem
  this.display = display
  this.renderOb = drawItem
}

function hideItem()
{
  if (this.navObj.style.display == "none")
    return
  this.navObj.style.display = "none"
}

function initializeItem(level, lastNode, leftSide)
{
  this.createIndex()

  if (level>0)
    if (lastNode) //the last 'brother' in the children array
    {
      this.renderOb(leftSide + "<img name='nodeIcon" + this.id + "' src='images/ftv2blank.gif' width=16 height=22>")
      leftSide = leftSide + "<img src='images/ftv2blank.gif' width=16 height=22>"
    }
    else
    {
      this.renderOb(leftSide + "<img name='nodeIcon" + this.id + "' src='images/ftv2blank.gif' width=16 height=22>")
      leftSide = leftSide + "<img src='images/ftv2blank.gif' width=16 height=22>"
    }
  else
    this.renderOb("")
}

function drawItem(leftSide)
{
  doc.write("<table id='node" + this.id + "' style='position:block;' border=0 cellspacing=0 cellpadding=0>")
  doc.write("<tr><td>")
  doc.write(leftSide)
  doc.write("<a href=" + this.link + "'><img id='itemIcon"+this.id+"' src='images/"+this.iconSrc+"' border=0></a></td><td valign=middle nowrap>")
  if (USETEXTLINKS)
    doc.write("<a href=" + this.link + "'>" + this.desc + "</a>")
  else
    doc.write(this.desc)
  doc.write("</table>")
  this.navObj = doc.all["node"+this.id]
  this.iconImg = doc.all["itemIcon"+this.id]
  this.nodeImg = doc.all["nodeIcon"+this.id]
}


// Methods common to both objects (pseudo-inheritance)
// ********************************************************

function display()
{
  this.navObj.style.display = "block"
}

function createEntryIndex()
{
  this.id = nEntries
  indexOfEntries[nEntries] = this
  nEntries++
}


// Events
// *********************************************************

function clickOnFolder(folderId)
{
  var clicked = indexOfEntries[folderId]
    clickOnNode(folderId)
  return
}

function clickOnNode(folderId)
{
  var clickedFolder = 0
  var state = 0
  selectednode.value=folderId
  clickedFolder = indexOfEntries[folderId]
  state = clickedFolder.isOpen

  clickedFolder.setState(!state) //open<->close
}

function initializeDocument()
{
  foldersTree.initialize(0, 1, "")
  foldersTree.display()

    doc.write("<layer top="+indexOfEntries[nEntries-1].navObj.top+">&nbsp;</layer>")
    // close the whole tree
    clickOnNode(0)
    // open the root folder
    clickOnNode(0)

}

function addfolder(Node)
{
  var str = "<table id='node"+Node.id+"' style='position:block;' border=0 cellspacing=0 cellpadding=0><tr><td>"
  str+=drawlevle(Node)
  if (Node.hreference)
    str+="<img name='nodeIcon"+Node.id+"' src='images/ftv2blank.gif' width=16 height=22 border=0><a href='" + Node.hreference + "' TARGET=\"Iframe_table\" onClick='javascript:clickOnNode("+Node.id+")'><img name='folderIcon"+Node.id+"' src='images/biao1.gif' border=0></a></td><td valign=middle nowrap><a href='" + Node.hreference + "' TARGET=\"Iframe_table\" onClick='javascript:clickOnNode("+Node.id+")'>"+Node.desc+"</a></td></table>"
  else
    str+="<img name='nodeIcon"+Node.id+"' src='images/ftv2blank.gif' width=16 height=22 border=0><a href='javascript:clickOnNode("+Node.id+")'><img name='folderIcon"+Node.id+"' src='images/biao1.gif' border=0></a></td><td valign=middle nowrap><a href='javascript:clickOnNode("+Node.id+")'>"+Node.desc+"</a></td></table>"
  return str
}

function additem(Node)
{
  var str = "<table id='node"+Node.id+"' style='position:block;' border=0 cellspacing=0 cellpadding=0><tr><td>"
  str+=drawlevle(Node)
  str+="<img name='nodeIcon"+Node.id+"' src='images/ftv2blank.gif' width=16 height=22 border=0><a href=" + Node.link + "'><img name='itemIcon"+Node.id+"' src='images/I0.gif' border=0></a></td><td valign=middle nowrap><a href=" + Node.link + "'>"+Node.desc+"</a></td></table>"
  return str
}

function getLastChildren(Node)
{
  if(Node.nChildren>0)
    return getLastChildren(Node.children[Node.nChildren-1])
  else
    return Node
}

function drawlevle(Node)
{
  var str=""
  if(Node.parent==foldersTree)
    return str
  else
  {
    str+=drawlevle(Node.parent)
    if(Node.parent.isLastNode==1)
      str+="<img src='images/ftv2blank.gif' width=16 height=22>"
    else
      str+="<img src='images/ftv2blank.gif' width=16 height=22>"
  }
  return str
}

function addNode(Node)
{
  Node.createIndex()
  if(Folder.prototype.isPrototypeOf(Node)){
    if(Node.parent.nChildren>1)
    {
      if(Folder.prototype.isPrototypeOf(Node.parent.children[Node.parent.nChildren-2]))
        Node.parent.children[Node.parent.nChildren-2].nodeImg.src="images/ftv2blank.gif"
      else
        Node.parent.children[Node.parent.nChildren-2].nodeImg.src="images/ftv2blank.gif"
      Node.parent.children[Node.parent.nChildren-2].isLastNode = 0
      if(Node.parent.children[Node.parent.nChildren-2].nChildren==0)
        Node.parent.children[Node.parent.nChildren-2].navObj.insertAdjacentHTML("AfterEnd",addfolder(Node))
      else
        getLastChildren(Node.parent.children[Node.parent.nChildren-2]).navObj.insertAdjacentHTML("AfterEnd",addfolder(Node))
    }else
    {
      Node.parent.navObj.insertAdjacentHTML("AfterEnd",addfolder(Node))
    }
    Node.navObj = doc.all["node"+Node.id]
    Node.iconImg = doc.all["folderIcon"+Node.id]
    Node.nodeImg = doc.all["nodeIcon"+Node.id]
  }
  else{
    if(Node.parent.nChildren>1)
    {
      if(Folder.prototype.isPrototypeOf(Node.parent.children[Node.parent.nChildren-2]))
        Node.parent.children[Node.parent.nChildren-2].nodeImg.src="images/ftv2blank.gif"
      else
        Node.parent.children[Node.parent.nChildren-2].nodeImg.src="images/ftv2blank.gif"
      Node.parent.children[Node.parent.nChildren-2].isLastNode = 0
      if(Node.parent.children[Node.parent.nChildren-2].nChildren==0)
        Node.parent.children[Node.parent.nChildren-2].navObj.insertAdjacentHTML("AfterEnd",additem(Node))
      else
        getLastChildren(Node.parent.children[Node.parent.nChildren-2]).navObj.insertAdjacentHTML("AfterEnd",additem(Node))
    }else
    {
      Node.parent.navObj.insertAdjacentHTML("AfterEnd",additem(Node))
    }
    Node.navObj = doc.all["node"+Node.id]
    Node.iconImg = doc.all["itemIcon"+Node.id]
    Node.nodeImg = doc.all["nodeIcon"+Node.id]
  }
  clickOnNode(Node.parent.id)
  clickOnNode(Node.parent.id)
}

function removeNode(NodeId)
{
  i=indexOfEntries[selectednode.value].parent.nChildren-1
//需要去掉父子关系！！
//  while(indexOfEntries[selectednode.value].parent.children[i]!=indexOfEntries[selectednode.value])
//  {
//    temp=indexOfEntries[selectednode.value].parent.children[i-1]
//    indexOfEntries[selectednode.value].parent.children[i-1]=indexOfEntries[selectednode.value].parent.children[i]
//    i--
//  }
  doc.all["node"+NodeId].outerText=""
}
// Auxiliary Functions for Folder-Tree backward compatibility
// *********************************************************

function gFld(description, hreference)
{
  folder = new Folder(description, hreference)
  return folder
}

function gLnk(target, iconSrc, description, linkData)
{
  fullLink = ""

  if (target==0)
  {
    fullLink = "'"+linkData+"' target=\"_top\""
  }
  else
  {
    if (target==1)
       fullLink = "'"+linkData+"' target=_blank "
    else
       fullLink = "'"+linkData+"' target=\"Iframe_table\""
  }

  linkItem = new Item(description, iconSrc, fullLink)
  return linkItem
}

function insFld(parentFolder, childFolder)
{
  childFolder.parent = parentFolder
  return parentFolder.addChild(childFolder)
}

function insDoc(parentFolder, document)
{
  document.parent = parentFolder
  return parentFolder.addChild(document)
}

// Global variables
// ****************

USETEXTLINKS = 1
indexOfEntries = new Array
nEntries = 0
doc = document
