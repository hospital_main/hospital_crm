/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/javascript/chart.js,v 1.1 2012/03/12 01:59:21 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:59:21 $
 * $Modtime: $
 * $Revision: 1.1 $
 */
function adjustShape(){
	if (cewolf1==null) {
    alert('请先进行分析！');
    return false;
	}

	comp1 = document.getElementById("cewolf1");
	comp2 = document.getElementById("cewolf1Big");
	if (comp2.style.display=="none") {
		comp1.style.display="none";
		comp2.style.display="block";
	} else {
		comp2.style.display="none";
		comp1.style.display="block";
	}

	comp = document.getElementById("cewolf1Zoom");
	if (comp.innerText=='放大(B)') {
		comp.innerText = "缩小(B)"
	} else {
		comp.innerText = "放大(B)"
	} 	
}

function saveChart() {
	
	if (typeof(cewolf1)=="undefined") {
    alert('请先进行分析！');
    return false;
	}	
  if (cewolf1==null) {
    alert('请先进行分析！');
    return false;
	}	
	
	if (cewolf1!=null) {
		if (document.getElementById("cewolf1Big")!=null) {
			cewolf1BigIframe.document.execCommand('saveas');
		} else {
			cewolf1Iframe.document.execCommand('saveas');
		} 
	}	
}