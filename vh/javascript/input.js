//阻止输入域输入数字和 '+-'之外的字符
function isInt(event){
  if (((event.keyCode>=48) && (event.keyCode<=57))||event.keyCode==45||event.keyCode==43||((event.keyCode>=96) && (event.keyCode<=105))||event.keyCode==8||event.keyCode ==46){
    return true;
  } else {
    return false;    
  }
  
}
//阻止输入域输入数字和 '.+-'之外的字符
function isFloat(event){
  if (((event.keyCode>=48) && (event.keyCode<=57))||event.keyCode==45||event.keyCode==46||event.keyCode==43)
    return true;
  else
    return false;
  
}
//num为需要判断的数，front为需要判断的小数点前几位，back为需要判断的小数点后几位
function isNum(id){
  var obj=document.getElementById(id);
  var num=obj.value;
  if(trim(num)==""){
    alert("数字不能为空！");
    obj.value=0;
    return false;
  }
  if(num!=""){
  if(num=="+"){
  	alert("数字不能为一个加号！");
    obj.focus();
    return false;
  }
   if(num=="-"){
  	alert("数字不能为一个减号！");
    obj.focus();
    return false;
  }
  var number = new Number(num);
  if (isNaN(number))
  {alert('不是数字');
   obj.focus();
   return false;
  }
  var count1=0;//'.'个数
  var count2=0;//'-'或者'+'个数
  var index1=-1;//'.'位置
  var index2=-1;//'-'或者'+'位置
  for(var i=0;i<num.length;i++){ 
   var a=num.charAt(i);
   if(a=='.') { count1++;index1=i;}
   if(a=='-') { count2++;index2=i;}
   if(a=='+') { count2++;index2=i;}
   }
  if(count1>1||count2>1){
    alert("多于一个的小数点，负号或者加号！");
    obj.focus();
    return false;
  }  
  if(index2>0){ 
    alert("符号位置有误！");
    obj.focus();
    return false;
  }
  //1.如果有'+'或'-'号，则小数点不应在符号后
  //2.如果没有'+'或'-'号，小数点不应在第一位
  //3.小数点不应在最后一位
  if((index2==0&&index1==1)||(index2==-1&&index1==0)||index1==num.length-1){ 
  	  alert("小数点位置有误！");
      obj.focus();
      return false;
    }
    return true;
   }
 
  else return true; 
 }
//判断小数点前的位数是否正确
function bef(id,bef,message){
  var obj=document.getElementById(id);
  var num=obj.value;
  if(num!=""){
  bef=parseInt(bef);
  var a=num.charAt(0);
  var sign=false;
  if(a=='+'||a=='-'){
    sign=true;
  }
  var idx=num.indexOf(".");//得到小数点的位数,这个数字代表了小数点前公有几位
  if(idx==-1){//如果没有小数点
    idx=num.length;
  }
 
  if(sign==true){//如果有'+'或'-'
     if((idx-1)<=bef){
     	//alert("数字正确");
     	return true;
     }
     else { 
	alert(message);
        obj.focus();
	return false;
     }
    }
  else {//如果没有'+'或'-'
     if(idx<=bef){ 
     	//alert("数字正确");
     	return true;
     }
     else { 
	alert(message);
        obj.focus();
	return false;
    }
  }
 }
else return true;
}
//判断小数点后的位数是否正确
function aft(id,aft,message){
  var obj=document.getElementById(id);
  var num=obj.value;
  if(num!=""){
  aft=parseInt(aft);
var idx=num.indexOf(".");//得到小数点的位数
if(idx==-1){//如果没有小数点
  idx=num.length;
}
var length=num.length;//得到数字的长度
 if((length-idx-1)>aft){
    alert(message);
    obj.focus();
    return false;
  }
 else{
    //alert("数字正确");
    return true;
 }
}
else return true;
}
//判断数字的符号是否正确
function sign(id,sign,message){
 
 var obj=document.getElementById(id);
 var num=obj.value;
 if(num!=""){
 var c=num.charAt(0);
 if(sign=='+'){
    if(c=='-'){
      alert(message);
      obj.focus();
      return false;
    }
    else{
   // alert("数字正确");
    return true;
    }
   }
 else {  
    if(c!='-'){
    alert(message);
    obj.focus();
    return false;
    }
  else{
  //  alert("数字正确");
    return true;
    }
   }
  }
 else return true; 
}
//判断数字的上限
function max(id,max,message){
 var obj=document.getElementById(id);
 var num=obj.value;
 if(num!=""){
 max=parseFloat(max);
 if(parseFloat(num)<=parseFloat(max)){
   // alert("数字正确");
    return true;
 }
 else {
   alert(message);
   obj.focus();
   return false;
  }
}
else return true;
}
//判断数字的下限
function min(id,min,message){
 var obj=document.getElementById(id);
 var num=obj.value;
 if(num!=""){
 if(parseFloat(num)>=parseFloat(min)){
   // alert("数字正确");
    return true;
   }
 else {
    alert(message);
    obj.focus();
    return false;
   }
}
else return true;
}
function hidden(obj,all_hide_id){ 
  var index=obj.selectedIndex;
  for(var i=0;i<all_hide_id.length;i++){
   if(i==index){
   document.getElementById(all_hide_id[i]).style.display='block';
   }
   else  document.getElementById(all_hide_id[i]).style.display='none';
  }
}



//年月日组件处理
function selectDate(YearId,MonthId,DayId,id)
{
  if(id.value==""&&YearId.value==""&&MonthId.value=="")
    {
       id.value="";
       return false ;
     }
  if(id.value!=""&&(YearId.value==""||MonthId.value=="")||(id.value!=""&&DayId.value==""))
    {
      YearId.options[0].selected=true;
      MonthId.options[0].selected=true;
      DayId.length=1;
      DayId.options[0].value="";
      DayId.options[0].text="--";
      id.value="";
      return false ;
    }
  if(id.value==""&&YearId.value!=""&&MonthId.value=="")
    { MonthId.options[1].selected=true;//月列表第一项被选中
       showDay(YearId,MonthId,DayId);
    }
  if(id.value==""&&YearId.value==""&&MonthId.value!="")
   { YearId.options[1].selected=true;//年列表第一项被选中
     showDay(YearId,MonthId,DayId);
   }
      myMonth=parseInt(MonthId.options[MonthId.selectedIndex].value);
      if(myMonth<=9)
         month='0'+myMonth;
      else
         month=myMonth;
      myDay=parseInt(DayId.value);
      if(myDay<=9)
         day='0'+myDay;
      else
         day=myDay;
  id.value=YearId.value+"-"+month+"-"+day;
}
//显示天(day)列表
function showDay(YearId,MonthId,DayId)
{
      myMonth=parseInt(MonthId.options[MonthId.selectedIndex].value);
      myYear=parseInt(YearId.options[YearId.selectedIndex].value);
      mx_day=howManyDays(myYear,myMonth);
      DayId.length=mx_day;
      DayId.options[0].value="";
      DayId.options[0].text="--";
      var i=1;
      while(i<mx_day)
        {
          DayId.options[i].value=i;
          DayId.options[i].text=i;
          if(i==1)
            { DayId.options[i].selected=true;//日列表第一项被选中
            }
          i++;
        }
}
//一个月有多少天
function howManyDays(myYear,myMonth)
{
  if(myMonth==12)//时间处理
    { tMonth=1;
      tYear=myYear+1;
    }
  else
    { tYear=myYear;
      tMonth=myMonth+1;
    }
  var time1=myMonth+"/"+"01/"+myYear+" 0:0:0";
  var time2=tMonth+"/"+"01/"+tYear+" 0:0:0";
  var h1=Date.parse(time1);//time1的格式设为：01/01/2003 0:0:0
  var h2=Date.parse(time2 );
  var h3=h2-h1;
  var days=Math.floor(h3/86400000)+1;
  return days;
}
//年组件处理
function selectYear(YearId,id)
{
  if(id.value==""&&YearId.value=="")
    return false;
  if(id.value!=""&&YearId.value=="")
    {
      YearId.options[0].selected=true;
      id.value="";
    }
  id.value=YearId.options[YearId.selectedIndex].value;

    
}
//年季组件处理
function selectYearQ(YearId,QuarterId,id)
{
 if(id.value==""&&YearId.value==""&&QuarterId.value=="")
   {
     return false ;}
  if(id.value!=""&&(YearId.value==""||QuarterId.value==""))
    {
      YearId.options[0].selected=true;
      QuarterId.options[0].selected=true;
      id.value="";
      return false ;
    }
  if(id.value==""&&YearId.value!=""&&QuarterId.value=="")
    {
      QuarterId.options[1].selected=true;
    }
  if(id.value==""&&QuarterId.value!=""&&YearId.value=="")
    {
       YearId.options[1].selected=true;
    }
  id.value=YearId.options[YearId.selectedIndex].value+QuarterId.options[QuarterId.selectedIndex].value;

}
//年月组件处理
function selectYearM(YearId,MonthId,id)
{
  var iMonth;
  if(id.value==""&&YearId.value==""&&MonthId.value=="")
   {
     return false ;}
   if(id.value!=""&&YearId.value==""&&MonthId.value=="")
   { id.value=="";
     return false ;
   }
  if(id.value!=""&&(YearId.value==""||MonthId.value==""))
    {
      YearId.options[0].selected=true;
      MonthId.options[0].selected=true;
      id.value="";
      return false ;
    }
  if(id.value==""&&YearId.value!=""&&MonthId.value=="")
    {
      MonthId.options[1].selected=true;
    }
  if(id.value==""&&MonthId.value!=""&&YearId.value=="")
    {
       YearId.options[1].selected=true;
    }
  iMonth=parseInt(MonthId.options[MonthId.selectedIndex].value);
  if(iMonth<=9&&iMonth>0)
     id.value=YearId.options[YearId.selectedIndex].value+"0"+iMonth;
  else
      id.value=YearId.options[YearId.selectedIndex].value+iMonth;

}



function createTable(tableId,id){
  var text;
  var input;
  var td;
  var table=document.getElementById(tableId);
	var tbody=table.firstChild;
	var length=tbody.childNodes.length;
	var tr=document.createElement("tr");
	tbody.appendChild(tr);
	//添加一个为多选按钮的列
	td=document.createElement("<td align='center'/>");
	tr.appendChild(td);
	var check=document.createElement("<input type='checkbox' name='box'/>");
	td.appendChild(check);
  
	//添加一个为年的列
	td=document.createElement("<td align='center'/>");
	tr.appendChild(td);
	var text=document.createTextNode(""+length);
	td.appendChild(text);
  //添加一个输入域的列
  td=document.createElement("<td align='center'/>");
	tr.appendChild(td);
	input=document.createElement("<input id='income1"+length+"' type='text'  class='textInputF' maxlength='12' onKeypress='return isFloat(event)' onblur=\"isNum( 'income1"+length+"' )\"  />");
	input.name='income'+length;
	td.appendChild(input);
	//添加一个输入域的列
  td=document.createElement("<td align='center'/>");
	tr.appendChild(td);
	input=document.createElement("<input id='cost1"+length+"' type='text'  class='textInputF' maxlength='12' onKeypress='return isFloat(event)' onblur=\"isNum( 'cost1"+length+"' )\"/>");
	input.name='cost'+length;
	td.appendChild(input);
	
	//添加一个输入域的列
  td=document.createElement("<td align='center'/>");
	tr.appendChild(td);
	input=document.createElement("<input id='deprec1"+length+"' type='text' class='textInputF' maxlength='12' onKeypress='return isFloat(event)' onblur=\"isNum( 'deprec1"+length+"' )\"/>");
	input.name='deprec'+length;
	td.appendChild(input);
	//添加一个空列的列
	td=document.createElement("<td align='center'/>");
	tr.appendChild(td);
  text=document.createTextNode("   ");
	td.appendChild(text);
	//添加一个输入域的列
  td=document.createElement("<td align='center'/>");
	tr.appendChild(td);
	input=document.createElement("<input id='quot1"+length+"' type='text' class='textInputF' maxlength='6' onKeypress='return isFloat(event)' onblur=\" if(isNum( 'quot1"+length+"' )) {   min('quot1"+length+"','0','现值系数不应小于0'); max('quot1"+length+"','100','现值系数不应大于100');}\"  />");
	input.name='quot'+length;
	td.appendChild(input);
	//添加一个空列的列
	td=document.createElement("<td align='center'/>");
	tr.appendChild(td);
	text=document.createTextNode("   ");
	td.appendChild(text);
	//添加一个空列的列
	td=document.createElement("<td align='center'/>");
	tr.appendChild(td);
	text=document.createTextNode("   ");
	td.appendChild(text);
	//给隐藏域赋值
  var hidden=document.getElementById(id);
  hidden.value=tbody.childNodes.length-1;
 }
 function Hiszero(id){
 	var vlu=document.getElementById(id);
 	if(Math.abs(vlu.value)==0){
 		return true;
 		}
 		else{
 		return false;
 		}
 	}
function deleteTable(tableId,id){
	var j=0;
  var table=document.getElementById(tableId);
	var tbody=table.firstChild;

  var flag=false;
	for(var i=1;i<tbody.childNodes.length;i++){
		  var td=tbody.childNodes[i].firstChild;
		  var box=td.firstChild;
		  if(box.checked==true){
		    flag = true;
		    break;
		  }
	}  
	if(!flag)
	{
		alert( "请先选择,再删除!");
	}
  else if (confirm('是否删除')) {
		for(var i=1;i<tbody.childNodes.length;i++){
			  var td=tbody.childNodes[i].firstChild;
			  var box=td.firstChild;
			  if(box.checked==true){
			  tbody.removeChild(tbody.childNodes[i]);
			  i--;
			  }
		}
		tbody=table.firstChild;
		for(var i=1;i<tbody.childNodes.length;i++){
			  var text=tbody.childNodes[i].childNodes[1].firstChild;
			  text.data=""+i;
			  var input;
			  input=tbody.childNodes[i].childNodes[2].firstChild;
			  input.name='income'+i;
			  input=tbody.childNodes[i].childNodes[3].firstChild;
			  input.name='cost'+i;
			  input=tbody.childNodes[i].childNodes[4].firstChild;
			  input.name='deprec'+i;
			  input=tbody.childNodes[i].childNodes[6].firstChild;
			  input.name='quot'+i;
			  
			  }
		var hidden=document.getElementById(id);
	  hidden.value=tbody.childNodes.length-1;
    return true;
  } else
      return false;	

}