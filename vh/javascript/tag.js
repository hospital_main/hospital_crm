/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/javascript/tag.js,v 1.1 2012/03/12 01:59:21 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:59:21 $
 * $Modtime: $
 * $Revision: 1.1 $
 */

/*
 * 修正按钮的状态
 */
function biSelectAdjustButton(selectPre, buttonPre, idPost) {	
	if (document.getElementById(selectPre+idPost).selectedIndex > -1) {
		document.getElementById(buttonPre+idPost).src = "img/biSelect/"+buttonPre+"/normal.gif";			
		document.getElementById(buttonPre+idPost).style.cursor = "hand";
	} else {
		document.getElementById(buttonPre+idPost).src = "img/biSelect/"+buttonPre+"/disabled.gif";
		document.getElementById(buttonPre+idPost).style.cursor = "default";
	}
}

/*
 * 移动selectPre1的选择项到selectPre2, 同时在表格上反应
 */
function biSelectMoveSelect(selectPre1, selectPre2, idPost, biId) {
  var comp1 = document.getElementById(selectPre1 + idPost); 
	var comp2 = document.getElementById(selectPre2+ idPost);
	var isChoose = false; // 是否被选中

  for (var i=comp1.length-1; i>=0; i--) {
  	if (comp1.options[i].selected==true) {
  		isChoose = true;

  		refreshTable('check'+idPost+comp1.options[i].value);
  		
  		// 添加
  		comp2.length++;
  		comp2.options[comp2.length-1].value = comp1.options[i].value;
  		comp2.options[comp2.length-1].text = comp1.options[i].text;
  		
  		// 移去
  		if (i==comp1.length-1) {
				comp1.length = comp1.length-1;
  		} else {
  			comp1.options[i].value = comp1.options[comp1.length-1].value;
  			comp1.options[i].text = comp1.options[comp1.length-1].text;
  			comp1.options[i].selected = false;
				comp1.length = comp1.length-1;
  		}
  	}
  }
  
  // 没有被选中的
  if (!isChoose) return false;

	biSelectAdjustButton('left', 'add', idPost);
	biSelectAdjustButton('right', 'remove', idPost);
	
	copySelect('right'+idPost, biId);
	
  return false;
}


/*
 * 复制selectId1到selectId2
 */
function copySelect(selectId1, selectId2) {
	var comp1 = document.getElementById(selectId1);
	var comp2 = document.getElementById(selectId2);
	
	comp2.length = comp1.length;
	for (var i=0; i<comp1.length; i++) {
		comp2.options[i].value = comp1.options[i].value;
		comp2.options[i].selected = true;
	}
}

/*
 * 显示表格
 */
function biSelectdrawTable(idPost) {	
  
  //重新定位div的位置
  var object=document.getElementById("left"+idPost);
  var top = object.offsetTop+object.offsetHeight+10;
  var left = object.offsetLeft;
  var obj = object.offsetParent;
  while(obj.tagName != "BODY") {
		top += obj.offsetTop;
		left += obj.offsetLeft;
		obj = obj.offsetParent;
	}
	
	var divObject = document.getElementById("div"+idPost);
	divObject.style.top = top;
	divObject.style.left = left;
	
	var iframeObject = document.getElementById("iframe"+idPost);
	iframeObject.style.top = top;
	iframeObject.style.left = left;	
	
	var controlImage = document.getElementById("control"+idPost);
	if(divObject.style.display=='none') {
		divObject.style.display='block';
		controlImage.src="img/biSelect/close.gif";
		iframeObject.style.display='block';
	} else {
		divObject.style.display='none';	
		controlImage.src="img/biSelect/show.gif";;
		iframeObject.style.display='none';
	}
	return false;
}

/* 
 * 更新表格内容与下拉列表同步
 */
function refreshTable(checkId) { 
	var comp = document.getElementById(checkId);
	if (comp.checked) {
		comp.checked = false;
	} else {
		comp.checked = true;
	}
}

/*
 * 响应表格的选择
 */
function respondTable(value, idPost, biId) {
	var check = document.getElementById("check"+idPost+value);
	var comp1 = document.getElementById("left"+idPost);
	var comp2 = document.getElementById("right"+idPost);
		
	if (check.checked) {
		comp1 = document.getElementById("left"+idPost);
		comp2 = document.getElementById("right"+idPost);
	} else {
		comp2 = document.getElementById("left"+idPost);
		comp1 = document.getElementById("right"+idPost);
	}
	
	for (var i=0; i<comp1.length; i++) {
		comp1.options[i].selected = false;
		if (comp1.options[i].value==value) {
			comp2.length++;
			comp2.options[comp2.length-1].value = comp1.options[i].value;
			comp2.options[comp2.length-1].text = comp1.options[i].text;
			if (comp1.length-1 == i) {
				comp1.length--;
			} else {
				comp1.options[i].value = comp1.options[comp1.length-1].value;
				comp1.options[i].text = comp1.options[comp1.length-1].text;
				comp1.length--;
			}				
		}
	}
	
	copySelect('right'+idPost, biId);
	
	return true;
}
