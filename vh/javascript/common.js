/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/javascript/common.js,v 1.1 2012/03/12 01:59:21 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:59:21 $
 * $Modtime: 03-09-03 16:48 $
 * $Revision: 1.1 $
 *

/*******************************************************
function HowManyDays()
purpose
  To know how many days in a month
  strMonth:which month
  strYear:which year
********************************************************/
function howManyDays(strYear, strMonth) {
	var strDate1=strMonth+"-"+"01"+"-"+strYear
	strMonth=parseInt(strMonth,10)+1
	var strDate2=strMonth +"-"+"01"+"-"+strYear
	var date1=new Date(strDate1)
	var date2=new Date(strDate2)
	var days=(date2 - date1)/24/60/60/1000
	return days;
}


/*******************************************************
function selectQuarterToInput()
purpose
  this is a component for year-quarter
  yearID: the year id
  quarterID: the quarter id
  year_quarterID: the year_quarter id
********************************************************/
function selectQuarterToInput(yearID, quarterID, year_quarterID) {

  if (year_quarterID.value!='' && (yearID.options[yearID.selectedIndex].value=='' || quarterID.options[quarterID.selectedIndex].value=='')) {
    yearID.options[0].selected=true;
    quarterID.options[0].selected=true;
    year_quarterID.value='';
    return false;
  }

  if (year_quarterID.value=='' && yearID.options[yearID.selectedIndex].value=='') {
    yearID.options[1].selected=true;
  }

  if (year_quarterID.value=='' && quarterID.options[quarterID.selectedIndex].value=='') {
    quarterID.options[1].selected=true;
  }

  year_quarterID.value=yearID.value+quarterID.value;
}



/*******************************************************
function selectMonthToInput()
purpose
  this is a component for year-month
  yearID: the year id
  monthID: the month id
  year_monthID: the year_month id
********************************************************/
function selectMonthToInput(yearID, monthID, year_monthID) {

  if (year_monthID.value!='' && (yearID.options[yearID.selectedIndex].value=='' || monthID.options[monthID.selectedIndex].value=='')) {
    yearID.options[0].selected=true;
    monthID.options[0].selected=true;
    year_monthID.value='';
    return false;
  }

  if (year_monthID.value=='' && yearID.options[yearID.selectedIndex].value=='') {
    yearID.options[1].selected=true;
  }

  if (year_monthID.value=='' && monthID.options[monthID.selectedIndex].value=='') {
    monthID.options[1].selected=true;
  }

  year_monthID.value=yearID.value+monthID.value;
}


/*******************************************************
function selectDateToInput()
purpose
  this is a component for date
  yearID: the year id
  monthID: the month id
  dayID: the day of the month
  dateID: the year_month id
********************************************************/
function selectDateToInput(yearID, monthID, dayID, dateID) {

  if (dateID.value!='' && (yearID.options[yearID.selectedIndex].value=='' || monthID.options[monthID.selectedIndex].value=='' || dayID.options[dayID.selectedIndex].value=='')) {
    yearID.options[0].selected=true;
    monthID.options[0].selected=true;
    dayID.options[0].selected=true;
    dayID.length=32;
    dayID.options[29].value='29';
    dayID.options[29].text='29';
    dayID.options[30].value='30';
    dayID.options[30].text='30';
    dayID.options[31].value='31';
    dayID.options[31].text='31';

    dateID.value='';
    return false;
  }

  if (dateID.value=='' && yearID.options[yearID.selectedIndex].value=='') {
    yearID.options[1].selected=true;
  }

  if (dateID.value=='' && monthID.options[monthID.selectedIndex].value=='') {
    monthID.options[1].selected=true;
  }

  if (dateID.value=='' && dayID.options[dayID.selectedIndex].value=='') {
    dayID.options[1].selected=true;
  }

  var iDay = howManyDays(yearID, monthID);
  if (iDay==29) {
    dayID.length=30;
    dayID.options[29].value='29';
    dayID.options[29].text='29';
  } else if (iDay==30) {
    dayID.length=31;
    dayID.options[29].value='29';
    dayID.options[29].text='29';
    dayID.options[30].value='30';
    dayID.options[30].text='30';
  } else if (iDay==31) {
    dayID.length=32;
    dayID.options[29].value='29';
    dayID.options[29].text='29';
    dayID.options[30].value='30';
    dayID.options[30].text='30';
    dayID.options[31].value='31';
    dayID.options[31].text='31';
  }

  dateID.value=yearID.value+'-'+monthID.value+'-'+dayID.value;
}



/*******************************************************
function checkNumber(e)
purpose
  this function only allow input number
  E: key
********************************************************/
function checkNumber(e) {
  if ((e.keyCode>=48) && (e.keyCode<=57))
    return true;
  else
    return false;
}



/*******************************************************
函数描述：
		截取字符串
	函数参数：
		str：一个字符串
	返回值：
		字符串
********************************************************/
function trim(str) {
	str=""+str;
  if(str.length<1) return str;
  var i=0;
	for (i=0; i<str.length; i++) {
		if (str.charAt(i) != ' ') break;
	}
	str = str.substring(i, str.length);
	for (i=str.length-1; i>=0; i--) {
		if (str.charAt(i)!= ' ') break;
	}
	str = str.substring(0, i+1);
	return str;
}


/*******************************************************
函数描述：
		检查文本框的输入值是否大于maxLength
	函数参数：
		comp:       文本框
	  maxLength:  最大长度
********************************************************/
function isTooLong(comp, maxLength) {
	var s = comp.value;
	if(s == null || s.length == 0)
		return false;
	var byteLength = 0;
	var ch;
	for(var j=0; j < s.length;j++) {
		ch = s.charCodeAt(j);
		if (ch > 255) //这个字符非键盘字符，而是汉字字符
			byteLength++;
		byteLength++;
	}

	if(byteLength > eval(maxLength)){
		comp.focus();
		comp.select();
		return true;
	}	else
		return false;
}


/*******************************************************
函数描述：
		检查文本框的输入值是否为空
	函数参数：
		comp:       文本框
********************************************************/
function isEmpty(comp) {
  var str = comp.value;
  str = trim(str);
  if (str == '') {
    comp.select();
    comp.focus();
    return true;
  } else
    return false;
}



/**
 将value精确的小数点后percision位
**/
function roundOff(value, precision) {
  var flag=1
  if(value<0){
    flag=0;
    value=-value
  }
  value = "" + value //convert value to string
  precision = parseInt(precision);

  var whole = "" + Math.round(value * Math.pow(10, precision));

  var decPoint = whole.length - precision;

  if(decPoint > 0) {
    result = whole.substring(0, decPoint);
    result += ".";
    result += whole.substring(decPoint, whole.length);
  } else {
    result = "0.";
    for (; decPoint<0; decPoint++)
      result += "0";
    result += whole;
  }
  if(flag==0)
    result = -result
  return result;
}
/*******************************************************
函数描述：
		检查文本框的输入值是否为数字型
	函数参数：
		comp:       comp文本框
    sign：      "+"表示只能为正；"-"表示只能为负；无此参数表示正负均可
example: isNumber(text1,+)  只能为正
         isNumber(text1,-)  只能为负
         isNumber(text1)    正负均可
********************************************************/
function isNumber(comp,sign) {
  var number;
  var string = comp.value;
  if (string==null) return false;
  if ((sign!=null) && (sign!='-') && (sign!='+'))
  {
   alert('IsNumber(string,sign)的参数出错：nsign为null或"-"或"+"');
   return false;
  }
  number = new Number(string);
  if (isNaN(number))
  {
   return false;
  }
  else if ((sign==null) || (sign=='-' && number<0) || (sign=='+' && number>0))
  {
   return true;
  }
  else
   return false;
}

/*******************************************************
函数描述：
		检查文本框的输入值是否为数字型
	函数参数：
		comp:       comp文本框
    sign：      "+"表示只能为正；"-"表示只能为负；无此参数表示正负均可
example: isNumber(text1,+)  只能为正
         isNumber(text1,-)  只能为负
         isNumber(text1)    正负均可
********************************************************/
function checkLength(o,length)
{
	if(isNaN(o.value)) return true;
	if(o.value.indexOf('.')>=0&&o.value.substr(o.value.indexOf('.')+1,o.value.length).length>length)
	{
		alert("注意小数最多有"+length+"位! ");
		o.focus();
		return false;
  }

	if(o.value.indexOf('.')>=0&&o.value.substr(0,o.value.indexOf('.')).length>8)
	{
		alert("注意数据过大! ");
		o.focus();
		return false;
	}
	else if(o.value.indexOf('.')<0&&o.value.length>8)
	{
		alert("注意数据过大! ");
		o.focus();
		return false;
	}
	return true;
}
function maxNum(com){
var maxNum=100;
var num=com.value;
if(com.value>maxNum)
   return false;
else true;
}
/*******************************************************
函数描述：
		检查文本框的输入值是否为实数型格式
	函数参数：
		comp:       comp文本框
    integerdig：   整数部分最大长度
    decimaldig：   小数部分最大长度
********************************************************/
function isDouble(comp , integerdig, decimaldig) {
  var strSeparator='.';
  var strNumberArray;
  if (!isNumber(comp))
    {
      comp.focus();
      comp.select();
      return 0;//判断是否是数字格式
    }
  strNumberArray = comp.value.split(strSeparator);
  if(strNumberArray[0]==null) strNumberArray[0]=0;
  if(strNumberArray[1]==null) strNumberArray[1]=0;
  if (strNumberArray[0].length>integerdig)
    {
     comp.focus();
     comp.select();
     return 1;//判断整数位数是否过长
    }
  if (strNumberArray[0].length==0)
    {
      comp.focus();
      comp.select();
      return 2;//判断是否有整数部分
    }
  if (strNumberArray[1].length>decimaldig)
    {
     comp.focus();
     comp.select();
     return 3;//判断小数部分是否过长
     }
}



/*
 *
 * 函数描述： 检查金额
 */
function checkMoney(comp) {
	if (comp.value=='') {
		alert('数字不能为空');
		comp.focus();
    comp.select();
    return false;
	}
  switch (isDouble(comp, 11, 2)) {
    case 0 :
      alert('请输入数字');
      comp.focus();
      comp.select();
      return false;
    case 1 :
      alert('数字太大');
      comp.focus();
      comp.select();
      return false;
    case 3 :
      alert('请仅输入两位小数');
      comp.focus();
      comp.select();
      return false;
  }
  return true;
}


/*
 *
 * 函数描述： 检查考核分数
 */
function checkMark(comp) {
	if (comp.value=='') {
		alert('数值不能为空');
		comp.focus();
    comp.select();
    return false;
	}
  switch (isDouble(comp, 2, 2)) {
    case 0 :
      alert('请输入数值');
      comp.focus();
      comp.select();
      return false;
    case 1 :
      if (eval(comp.value)<=100) {
        return true;
      }
      alert('请输入不大于100的数值');
      comp.focus();
      comp.select();
      return false;
    case 3 :
      alert('请仅输入两位小数');
      comp.focus();
      comp.select();
      return false;
  }
  if (parseFloat(comp.value)>100 ) {
    alert('请输入不大于100的数值');
    comp.focus();
    comp.select();
    return false;
  }
  return true;
}


/*
 *
 * 函数描述： 检查比例
 *
 */
function checkScale(comp) {
	if (comp.value=='') {
		alert('数值不能为空');
		comp.focus();
    comp.select();
    return false;
	}
  switch (isDouble(comp, 10, 2)) {
    case 0 :
      alert('请输入数值');
      comp.focus();
      comp.select();
      return false;
    case 3 :
      alert('请仅输入两位小数');
      comp.focus();
      comp.select();
      return false;
  }
  return true;
}


/*
 *
 * 函数描述： 快速录入
 *	函数参数：
 *		e:							键值
 *   	inputComp:			输入框
 *   	selectComp:   	选择框
 *
 */
function quickInput(e, inputComp, selectComp) {
	if (e.keyCode==13) {
		inputComp.value = selectComp.options[selectComp.selectedIndex].text.substring(selectComp.options[selectComp.selectedIndex].value.length+1)
		return false;
	}
	if (e.keyCode==40) {
		if (selectComp.selectedIndex+1<selectComp.options.length)
		selectComp.options[selectComp.selectedIndex+1].selected = true;
		return false;
	}
	if (e.keyCode==38 ) {
		if (selectComp.selectedIndex>0)
			selectComp.options[selectComp.selectedIndex-1].selected = true;
		return false;
	}

	var length = selectComp.options.length
	for (var i=0; i<length; i++) {
		var value = selectComp.options[i].value
		if (inputComp.value.length>value.length)continue;
		if (inputComp.value == value.substring(0, inputComp.value.length)) {
			if (i+4>=length) {
				selectComp.options[length-1].selected=true;
				selectComp.options[i].selected=true;
			}
			else {
				selectComp.options[i+3].selected=true;
				selectComp.options[i].selected=true;
			}
			break;
		}
	}
}
/*
 *
 * 函数描述： 检查输入字符是否合法（即不含“与‘）
 */
function checkLegality(comp) {
  var str=comp.value
  var ch
  for(var i=0; i < str.length;i++){
    ch = str.charCodeAt(i);
    if(ch==34 || ch==39)
      return false
  }
  return true;
}



// 显示颜色
var trTemp,trColor='';
function showColor(comp) {
	if (trTemp!=null) {
		trTemp.bgColor=trColor;
	}
	trColor=comp.bgColor;
	comp.bgColor='#CFCFCF';
	trTemp=comp;
}
function _clearIFdlgTag(u){
	if(window.__ifdlgtag){
		if(u.indexOf("&"+window.__ifdlgtag+"=")>0)
			u=u.substr(0,u.indexOf("&"+window.__ifdlgtag+"="));
	}
	return u;
}
/*
 * flag - 子页面的参数，flag 为空的时候为主页面打印， flag 为1的时候为子页面打印
 */
function pageInit(obj){
	var curWin=obj;
	obj.pageInit=pageInit;
	obj.prefix=window.top.prefix;
	obj.getCompCode=function(){
		return "";	
	}
	obj.getCopyCode=function(){
		return "";	
	}
	obj.getAcctYear=function(){
		return "";	
	}
	obj.getPageArg=function(name){
		if(!curWin.document._bufPageArgsXMLObject){
			var urlStr=_clearIFdlgTag(curWin.document.URL.toString());
			var xmlStr="<root>"+urlStr.substr(urlStr.indexOf("?load=")+6)+"</root>";
			curWin.document._bufPageArgsXMLObject=new ActiveXObject("Microsoft.XMLDOM");
			curWin.document._bufPageArgsXMLObject.loadXML(xmlStr);
		}
		var xml=curWin.document._bufPageArgsXMLObject;
		var path="root/"+name;
		if(xml.selectNodes(path).length>0){
			 if(xml.selectNodes(path)[0].childNodes.length>0){
			 		return xml.selectNodes(path)[0].childNodes[0].data;
			 	}
		}
			return "";
	}
	obj.openDialog=function(addr, argu, obj , scroll_flag) {
		var vUrl = document.URL.replace(/[^\/]*$/, '')
	
		vUrl = vUrl.replace(/<[.]*>/g, '')
		
		var w=screen.width-50;
		var h=screen.height-50;
		argu=argu.replace("[MAXW]",w+"px");
		argu=argu.replace("[MAXH]",h+"px");
		
		return curWin.showModalDialog(addr, curWin, "help:no;status:no;resizable:yes;scroll:yes;"+argu)
	}
	obj.getDict=function(argu,para){
			var xmlhttp;
 			if(window.XMLHttpRequest){
				xmlhttp=new XMLHttpRequest();
			}else if(window.ActiveXObject){
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.open("POST","global_select.hbviewhigh?selectID="+argu,false);
      if (para != null) {
        xmlhttp.send("<root>"+para+"</root>");
      }
      else{
        xmlhttp.send("<root>"+para+"</root>");
      }
      if (xmlhttp.responseText.indexOf("<error>")<0) {
        var vXml= new ActiveXObject("Microsoft.XMLDOM");
        vXml.async = false;
        vXml.loadXML(xmlhttp.responseText)
	      return vXml;
      }
    }
  obj.xmlhttp={};
  obj.xmlhttp._object={}
  obj.xmlhttp.post = function() {
  if (arguments.length < 2) {
    alert("参数个数不对")
    return;
  }
  var postfix = ".hbviewhigh";
  if (arguments.length >= 3) {
    postfix = postfix + arguments[2]
  }
	////////////////
	var xmlhttp;
 			if(window.XMLHttpRequest){
				xmlhttp=new XMLHttpRequest();
			}else if(window.ActiveXObject){
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.open("POST",arguments[0]+postfix,false);
      xmlhttp.send("<root>"+arguments[1]+"</root>");
      this._object.responseText=xmlhttp.responseText;
      this._object.responseXML=xmlhttp.responseXML;
}
obj.doMsg=function(source, subFunc) {
      if (source.search(/<error>/)!=-1) {
        var error = source.substring(source.search(/<error>/)+"<error>".length, source.search(/<\/error>/))
        if (trim(subFunc)!='hide')
        	window.alert(error)
        return false;
      } else if (source.search(/<msg>/)!=-1) {
        var msg = source.substring(source.search(/<msg>/)+"<msg>".length, source.search(/<\/msg>/))
        if (trim(subFunc)!='hide')
        	alert(msg)
        return true;
      }
      return true;
    }
}
function preparedPrint(flag){
  // var win = window.open("preparePrint.jspviewhigh?dataOnly=true&flag="+flag,"","width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1")
  var x = new ActiveXObject("Microsoft.XMLHTTP");
  x.open("GET", "preparePrint.jspviewhigh?dataOnly=true&flag="+flag, false);
  x.send(null);
  var xmlStr = '<?xml version="1.0" encoding="GBK"?><root>'+x.responseText.trim().replace(/\&nbsp;/g, "")+'</root>';
 
  printStringDataToCell( xmlStr, null, false, false );
}

function preparedPrintWithIndent(flag){ //保留缩进
  // var win = window.open("preparePrint.jspviewhigh?dataOnly=true&flag="+flag,"","width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1")
  var x = new ActiveXObject("Microsoft.XMLHTTP");
  x.open("GET", "preparePrint.jspviewhigh?dataOnly=true&flag="+flag, false);
  x.send(null);
  var xmlStr = '<?xml version="1.0" encoding="GBK"?><root>'
            +x.responseText.trim().replace(/\&amp;/g, "&").replace(/\&nbsp;&nbsp;/g, "　").replace(/\&nbsp;/g, "")
            +'</root>';
 // document.write(x.responseText.trim());
 printStringDataToCell( xmlStr, null, false, false );
}


/////////////////////////////
////Cell Print BEGIN d35f169cd160b4b3
function printXmlToCellByXsltFile(xmlData,xslFile,setData,dirPrint,isPrview,pageUrl){
	var vXsl = new ActiveXObject("Microsoft.XMLDOM");
	vXsl.async=false;
	vXsl.load(xslFile)
	var vXml = new ActiveXObject("Microsoft.XMLDOM");
	vXml.async=false;
	vXml.loadXML(xmlData);
	if(setData){
  	var endRow="<annex>";
  	for(var o in setData){
  		endRow+="<"+o+">"+setData[o]+"</"+o+">"
  	}
  	endRow+="</annex>";
  	var vXmlEndRow = new ActiveXObject("Microsoft.XMLDOM");
    vXmlEndRow.async=false;
    vXmlEndRow.loadXML(endRow);
    if(xmlData != ""){
    	vXml.selectNodes("/root")[0].appendChild(vXmlEndRow.selectNodes("/annex")[0]);
    }
  }
	printStringDataToCell(vXml.transformNode(vXsl),setData,dirPrint,isPrview,pageUrl);
}
function printStringDataToCell(data,setData,dirPrint,isPrview,pageUrl){
	var cellXml = new ActiveXObject("Microsoft.XMLDOM");
	cellXml.async=false;
	if(data.indexOf('nbsp')!=-1){
		data = data.replace(/&amp;/g, "");
		data = data.replace(/&/g, "");
		data = data.replace(/nbsp/g, "");
		
	}
	cellXml.loadXML(data);
	printXmlDataToCell(cellXml,setData,dirPrint,isPrview,pageUrl);
}
var _printXmlDataToCell_Data_=null;
var _printXmlDataToCell_Param_=null;
var _printXmlDataToCell_Dir_=null;
var _printXmlDataToCell_Prview_=null;
var _printXmlDataToCell_PageUrl_=null;
var _printXmlDataToCell_Emp_=null;
var _printXmlDataToCell_Init=false;
function printXmlDataToCell(data,setData,dirPrint,isPrview,pageUrl,openFrame){
	
	var win_prefix=window.top.prefix;
	var screenHeight=window.screen.availHeight;
	var screenWidth=window.screen.availWidth;
	var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto";
	if(setData!=null){
		var ks;
		for(var o in setData){
			ks=o.split("_");
			if(ks.length!=3)
				continue;
			if(ks[0]!="thead"&&ks[0]!="tbody"&&ks[0]!="tfoot")
				continue;
			var segs=data.getElementsByTagName(ks[0]);
			if(segs.length==0)
				continue;
			var trs=segs[0].getElementsByTagName("tr");
			if(trs.length==0||trs.length<parseInt(ks[1]-1,10))
				continue;
			var tds=trs[ks[1]-1].getElementsByTagName("td");
			if(tds.length==0||tds.length<parseInt(ks[2]-1,10))
				continue;
			tds[ks[2]-1].text=setData[o];
		}
	}
	_printXmlDataToCell_Data_=data;	
	_printXmlDataToCell_Param_=setData;
	_printXmlDataToCell_Dir_=dirPrint;
	_printXmlDataToCell_Prview_=isPrview;
	_printXmlDataToCell_Emp_=window.top.window._nowEmpName;
	if(openFrame==null||openFrame=='ModalDialog'){
		_printXmlDataToCell_Init=true;
		var win=window.showModalDialog("/base/print1/printh2c.html",window,dialogStyle);

	}else if(openFrame=='_new'){
		_printXmlDataToCell_Init=true;
		var win=window.open("/base/print1/printh2c.html",'New');

	}else{
		_printXmlDataToCell_Init=false;
		//var f=eval(openFrame);
		//f.document.location.href=	win_prefix+"base/print1/printh2c.html";
	}
}
////Cell Print END 3ff51ca139496acb

/**
 * sortColIndex - 需要排序的列， 从0 开始记数
 * sortFlag - 排序的方式，true升序， false降序
 */
function printForSort(sortColIndex, sortFlag) {
  var win = window.open("preparePrint.jspviewhigh?sortColIndex="+sortColIndex+"&sortFlag="+sortFlag,"","width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1")
}

/**
 * ajax组件*
 */
var netA =new Object();

	netA.R_S_UNINIT=0;
	netA.R_S_LOADING=1;
	netA.R_S_LOADED=2;
	netA.R_S_INTERACTIVE=3;
	netA.R_S_COMPLATED=4;

	netA.Gsend=function(url,onload,onerror){
		if(url.indexOf('?')>0){
			url=url+"&ajax=ajax";
		}
		else{
			url=url+"?ajax=ajax";
		}
		this.url=url;
		this.req=null;
		this.onload=onload;
		this.onerror=(onerror)? onerror:this.defaultError;

		this.loadXMLDoc(url);
	}

	netA.loadXMLDoc=function(url){
			if(window.XMLHttpRequest){
				this.req=new XMLHttpRequest();
			}else if(window.ActiveXObject){
				this.req=new ActiveXObject("Microsoft.XMLHTTP");
			}
			if(this.req){
				try{
					var loader=this;
					this.req.onreadystatechange=function(){
						loader.onReadyState.call(loader);
					}
					this.req.open('GET',url,true);
					this.req.send(null);
				}catch(err){
					this.onerror.call(this);
				}
			}
	};
	netA.onReadyState=function(){
		var req=this.req;
		var ready=req.readyState;
		if(ready==netA.R_S_COMPLATED){
			var httpStatus=req.status;
			if(httpStatus==200||httpStatus==0){
				this.onload.call(this);
			}else{
				this.onerror.call(this);
			}
		}
	};
	netA.defaultError=function(){
	alert("错误信息："+"\n \n readyState:"+this.req.readyState+"\n status:"
			+this.req.status+"\nheaders:"
			+this.req.getAllResponseHeaders());
	};

	function showMessage(){
		alert(netA.req.responseText)
	}

	function noMessage(){}
//hawkliu+
//for help
	function _inits_selfhelp(){
		var _parent_hand;
		if(parent!=undefined){
			_parent_hand=parent;
		}
		if(window.dialogArguments!=undefined){
			_parent_hand=window.dialogArguments;
		}
		var k=0;
		while(_parent_hand.selfshowhelp==undefined){
			if(_parent_hand.parent!=undefined){
				_parent_hand=_parent_hand.parent;
			}
			if(_parent_hand.dialogArguments!=undefined){
				_parent_hand=_parent_hand.dialogArguments;
			}
			k++;
			if(k>10){return;}
		}
			window.onhelp=function(){window.showHelp();return false;};
			window.showHelp=_parent_hand.selfshowhelp;
	}

	_inits_selfhelp();
	var endElem
function noContextMenu(){

	if(event.srcElement.tagName=="TD"||event.srcElement.tagName=="TH")
		endElem=event.srcElement.parentElement.parentElement.parentElement;
	else
		endElem=event.srcElement;
	if(endElem.border!='1'){return true;}

	var lang = new Object();
	lang["exportExcel"]		= "导出到Excel..."
	lang["UIMenuWidth"]				= 150
	var menuStyle = "<head><link href='/vh/base/themes/blue/menuarea.css' type='text/css' rel='stylesheet'></head><body scroll='no' onConTextMenu='event.returnValue=false;'>";
	var sMenu1 = "<TABLE border=0 cellpadding=0 cellspacing=0 class=Menu width="+lang["UIMenuWidth"]+"><tr><td width=18 valign=bottom align=center><\/td><td width="+(lang["UIMenuWidth"]-18)+" class=RightBg><TABLE border=0 cellpadding=0 cellspacing=0>";
	var sMenu2 = "<\/TABLE><\/td><\/tr><\/TABLE>";
	var s_MenuRow = "<tr><td align=center valign=middle><TABLE border=0 cellpadding=0 cellspacing=0 width="+(lang["UIMenuWidth"]-18)+"><tr ><td valign=middle height=20 class=MouseOut onMouseOver=this.className='MouseOver'; onMouseOut=this.className='MouseOut';";
	s_MenuRow += " onclick='parent.table_exportToExcel();parent.oPopupMenu.hide();'>";
	s_MenuRow += lang["exportExcel"]+"<\/td><\/tr><\/TABLE><\/td><\/tr>";
	var aheight = 24;
	var lefter = event.clientX;
	var topper = event.clientY;
	var awidth = lang["UIMenuWidth"];

	event.cancelBubble   =   true
  event.returnValue   =   false;
  oPopupMenu=window.createPopup();
  var oPopDocument = oPopupMenu.document;
  oPopDocument.open();
	oPopDocument.write(menuStyle + sMenu1+s_MenuRow+sMenu2);
	oPopDocument.close();
	if(lefter+awidth > document.body.clientWidth) lefter=lefter-awidth;
	oPopupMenu.show(lefter, topper, awidth, aheight, document.body);
  return   false;
}
function table_exportToExcel(){

	if(topwindow.activex_lib==null){
		alert("请安装望海组件");
		return false;
	}

	topwindow.activex_lib.ExportExcel(endElem.outerHTML.replace(/<TD.*<\/INPUT><\/TD>/gi,"<TD></TD>").replace(/<TD.*: none.*<\/TD>/gi,"").replace(/<TH.*: none.*<\/TH>/gi,""))

}
document.oncontextmenu=noContextMenu;

oWin = self.top;

while ( oWin.window.dialogArguments != undefined )
{
	oWin = oWin.window.dialogArguments;
	if( oWin.top )
		oWin = oWin.top;
}
topwindow = oWin;

if ( topwindow.activex_lib == undefined ) {
	document.write('<OBJECT id="activex_lib"  style="display:none" CLASSID="CLSID:62C27227-C64C-44ED-B184-E9B54D582A59" CODEBASE="base/ActiveX/vhControlTableB.CAB#version=3.0.0.109" WIDTH="1" HEIGHT="1"><param name="WebURL" value="undefined"><param name="TableID" value="activex_lib"><param name="VHActionExt" value=".hbviewhigh"></OBJECT>');
}

//////////////////////////////////////

var xmlHTTP

//String的处理
String.prototype.trim = function(){
  return this.replace(/(^\s+)|\s+$/g, "");
};

//function的处理
Function.READ = 1;
Function.WRITE = 2;
Function.READ_WRITE = 3;
Function.prototype.addProperty = function(sName, nReadWrite){
  nReadWrite = nReadWrite || Function.READ_WRITE;
  var capitalized = sName.charAt(0).toUpperCase() + sName.substr(1);
  if (nReadWrite & Function.READ)
    this.prototype["get" + capitalized] = new Function("", "return this._" + sName + ";");
  if (nReadWrite & Function.WRITE)
    this.prototype["set" + capitalized] = new Function(sName, "this._" + sName + " = " + sName + ";");
};

function XmlHttp() {
  this._object = new ActiveXObject("Microsoft.XMLHTTP");
}

XmlHttp.prototype.post = function() {
  if (arguments.length < 2) {
    alert("参数个数不对")
    return;
  }
  var postfix = ".hbviewhigh";
  if (arguments.length >= 3) {
    postfix = postfix + arguments[2]
  }

 	if (arguments.length == 4){
 		//alert( arguments[3]);
 		//alert("4");

  	this._object.open("POST", arguments[0]+postfix, true);
	}
	else
	{
		//alert("3");
  	this._object.open("POST", arguments[0]+postfix, false);
	}
  try {
    //var tt = String(arguments[1]).replace(/[&<>\u0080-\uffff]/g, _eaReplace);

    this._object.send("<root>"+arguments[1]+"</root>");
  } catch (exception){
    alert("连接失败，请检查网络！")
  }
}

 function doMsg(source, subFunc) {
      if (source.search(/<error>/)!=-1) {
        var error = source.substring(source.search(/<error>/)+"<error>".length, source.search(/<\/error>/))
        if (trim(subFunc)!='hide')
        	alert(error)
        return false;
      } else if (source.search(/<msg>/)!=-1) {
        var msg = source.substring(source.search(/<msg>/)+"<msg>".length, source.search(/<\/msg>/))
        if (trim(subFunc)!='hide')
        	alert(msg)
        return true;
      }
      return true;
    }

function _eaReplace( s )
{
	switch ( s )
	{
		case "&":
			return "&amp;";
		case "<":
			return "&lt;";
		case ">":
			return "&gt;";
		default:
			return "&#" + s.charCodeAt(0) + ";";
	}
}
xmlhttp = new XmlHttp;

function jspReload(){
	window.prefix=window.top.prefix==null?"":window.top.prefix;
	if(!window.top.window.cbcs_year1 || window.top.window.cbcs_year1.length!=4){
    	window.xmlhttp.post("sys_get_year_month","<key>cbcs_year1</key>","?isCheck=false");
    	str = window.xmlhttp._object.responseText;
    	if (str.search(/<td>/)!=-1) {
	    	window.top.window.cbcs_year1 = str.substring(str.search(/<td>/)+"<td>".length, str.search(/<\/td>/));
	    }else{
	    	window.top.window.cbcs_year1 = ""
	    }
    }

    if(!window.top.window.cbcs_month1 || window.top.window.cbcs_month1.length!=2){
    	window.xmlhttp.post("sys_get_year_month","<key>cbcs_month1</key>","?isCheck=false");
    	str = window.xmlhttp._object.responseText;
    	if (str.search(/<td>/)!=-1) {
	    	window.top.window.cbcs_month1 = str.substring(str.search(/<td>/)+"<td>".length, str.search(/<\/td>/));
	    }else{
	    	window.top.window.cbcs_month1 = ""
	    }
    }

    if(!window.top.window.cbcs_month2 || window.top.window.cbcs_month2.length!=2){
    	window.xmlhttp.post("sys_get_year_month","<key>cbcs_month2</key>","?isCheck=false");
    	str = window.xmlhttp._object.responseText;
    	if (str.search(/<td>/)!=-1) {
	    	window.top.window.cbcs_month2 = str.substring(str.search(/<td>/)+"<td>".length, str.search(/<\/td>/));
	    }else{
	    	window.top.window.cbcs_month2 = ""
	    }
    }
}
jspReload();

function getCBCS_Year1(){
  return window.top.window.cbcs_year1;
}
function getCBCS_Month1(){
  return window.top.window.cbcs_month1;
}
function getCBCS_Month2(){
  return window.top.window.cbcs_month2;
}
function setCBCS_Year1(cbcs_year1){
	if(window.top.window.cbcs_year1!=cbcs_year1){
		window.top.window.cbcs_year1 = cbcs_year1;
		window.xmlhttp.post("sys_set_year_month","<key>cbcs_year1</key><value>"+window.top.window.cbcs_year1+"</value>","?isCheck=false");
	}
}
function setCBCS_Month1(cbcs_month1){
	if(window.top.window.cbcs_month1!=cbcs_month1){
		window.top.window.cbcs_month1 = cbcs_month1;
		window.xmlhttp.post("sys_set_year_month","<key>cbcs_month1</key><value>"+window.top.window.cbcs_month1+"</value>","?isCheck=false");
	}
}
function setCBCS_Month2(cbcs_month2){
	if(window.top.window.cbcs_month2!=cbcs_month2){
		window.top.window.cbcs_month2 = cbcs_month2;
		window.xmlhttp.post("sys_set_year_month","<key>cbcs_month2</key><value>"+window.top.window.cbcs_month2+"</value>","?isCheck=false");
	}
}
/////////////////////////////
////////////////////////////////
function VhFileUploader(frm){
	//_____VHFileCreatFromTimer=window.setInterval(this.__createForm,200);
	document.vhFileUploader=this;
	this.___callbackFun=null;
	this.___callbackObj=null;
	this.__has_submit=false;
	this.__vhFileUploadFrame=frm;
	this.__oldDocument=null;
};
VhFileUploader.prototype.clear=function(){
	var inps=this.__vhFileUploadFrame.document.getElementsByTagName("input");
	for(var i=0;i<inps.length;i++){
		if(inps[i].name.indexOf("upfile")==0)
			inps[i].parentNode.removeChild(inps[i]);	
	}
};
VhFileUploader.prototype.choose=function(indx){
	if(this.__oldDocument!=document)
		this.clear();
	this.__oldDocument=document;
	if(eval("typeof(this.__getForm().upfile"+indx+")")=="undefined"){
		var ft=this.__vhFileUploadFrame.document.createElement("<input type=file id='upfile"+indx+"' name='upfile"+indx+"'/>");
		this.__vhFileUploadFrame.document.getElementById("fileSpanId").appendChild(ft);
	}
	eval("this.__getForm().upfile"+indx+".click()");
	return eval("this.__getForm().upfile"+indx+".value");
};
VhFileUploader.prototype.submit=function(fun,obj){
	if(this.__has_submit==true)
		return ;
	if(obj){
		this.___callbackObj=obj;
		obj.disabled=true;
	}
	this.___callbackFun=fun;
	this.__has_submit=true;
	this.__getForm().submit();
};
VhFileUploader.prototype.__onload=function(message,files){
	this.__has_submit=false;
	if(this.___callbackFun==null)
		return;
	this.___callbackFun(message,files);
	if(this.___callbackObj!=null)
		this.___callbackObj.disabled=false;
	this.___callbackFun=null;
	this.___callbackObj=null;
};
VhFileUploader.prototype.__getForm=function(){
	return this.__vhFileUploadFrame.document.vhUploadForm;
};
function getCurrentDate(){
    var d=new Date();
		var year=d.getFullYear();
		var month=d.getMonth()+1;
		var day=d.getDate();
		year='0000'+year;
		year=year.substring(year.length-4);
	  month='00'+month;
	  month=month.substring(month.length-2);
	  day='00'+day;
	  day=day.substring(day.length-2);
	  return year+'-'+month+'-'+day;
}
function getDict(argu,para) {

	if (para != null) { 
		xmlhttp.post("global_select", para, "?selectID="+argu);
	}
	else
		xmlhttp.post("global_select", '', "?selectID="+argu);
	
	if (window.doMsg(xmlhttp._object.responseText)) {
		var vXml= new ActiveXObject("Microsoft.XMLDOM");
		vXml.async = false;
		vXml.loadXML(xmlhttp._object.responseText)
		return vXml;
	}
}

function getValuePairBySql(sql,para){
	var datXml=getDict(sql,para)
	if(!datXml)
		return null;
	var paras=datXml.documentElement.getElementsByTagName("para");
	if(paras.length<1)
		return null;
	else
		return [paras[0].getAttribute("code"),paras[0].getAttribute("value")];
}

 function getCode(str){
	  var _str ;
	  _str = str.replace(/(^\s+)|\s+$/g, "") ;
	  return _str.replace(/[\+%&]/g, "");
}
//////////////VHFile END
