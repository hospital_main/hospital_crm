var Main_Tab	= null;
var cur_row	= null;
var cur_col	= null;
var cur_cell	= null;
var Org_con	= "";
var sort_col	= null;
var this_col	= null;
var show_col	= false;
var charMode	= true;
var act_bgc	= "#BEC5DE";
var act_fc	= "black";
var cur_bgc	= "#ccffcc";
var cur_fc	= "black";

arrowUp = document.createElement("SPAN");
arrowUp.innerHTML	= "5";
arrowUp.style.cssText 	= "PADDING-RIGHT: 0px; MARGIN-TOP: -3px; PADDING-LEFT: 0px; FONT-SIZE: 10px; MARGIN-BOTTOM: 2px; PADDING-BOTTOM: 2px; OVERFLOW: hidden; WIDTH: 10px; COLOR: blue; PADDING-TOP: 0px; FONT-FAMILY: webdings; HEIGHT: 11px";

arrowDown = document.createElement("SPAN");
arrowDown.innerHTML	= "6";
arrowDown.style.cssText = "PADDING-RIGHT: 0px; MARGIN-TOP: -3px; PADDING-LEFT: 0px; FONT-SIZE: 10px; MARGIN-BOTTOM: 2px; PADDING-BOTTOM: 2px; OVERFLOW: hidden; WIDTH: 10px; COLOR: blue; PADDING-TOP: 0px; FONT-FAMILY: webdings; HEIGHT: 11px";


function get_Element(the_ele,the_tag){
	the_tag = the_tag.toLowerCase();
	if(the_ele.tagName.toLowerCase()==the_tag)return the_ele;
	while(the_ele=the_ele.offsetParent){
		if(the_ele.tagName.toLowerCase()==the_tag)return the_ele;
	}
	return(null);
}
//表头与数据在同一个table中
function clickIt(col){
	event.cancelBubble=true;
	var the_obj = event.srcElement;
	var i = 0 ,j = 0;
	if(the_obj.tagName.toLowerCase() != "table" && the_obj.tagName.toLowerCase() != "tbody" && the_obj.tagName.toLowerCase() != "tr"){
		var the_td	= get_Element(the_obj,"td");
		if(the_td==null) return;
		var the_tr	= the_td.parentElement;
		var the_table	= get_Element(the_td,"table");

		var i = 0;
		cur_row = the_tr.rowIndex;
		cur_col = the_td.cellIndex;

	  the_td.mode = !the_td.mode;
		if(sort_col!=null){
		  with(the_table.rows[cur_row].cells[sort_col])
				removeChild(lastChild);
		}
		with(the_table.rows[cur_row].cells[cur_col])
			appendChild(the_td.mode?arrowUp:arrowDown);
		sort_tab(the_table,cur_row+1,col,the_td.mode);
		sort_col=cur_col;
	}
}

function sort_tab(the_tab,row,col,mode){
	var tab_arr = new Array();
	var i;
	var a;
	var start=new Date;
	for(i=row;i<the_tab.rows.length;i++){
	  a = the_tab.rows[i].cells[col].innerText.toLowerCase();
	  while(a.indexOf(",")>=0)
      a = a.replace(',','0');
		tab_arr.push(new Array(a,the_tab.rows[i]));
	}
	function SortArr(mode) {
		return function (arr1, arr2){
			var flag;
			var a,b;
			a = Number(arr1[0]);
			b = Number(arr2[0]);
			if(/^(\+|-)?\d+($|\.\d+$)/.test(a) && /^(\+|-)?\d+($|\.\d+$)/.test(b)){
				a=eval(a);
				b=eval(b);
				flag=mode?(a-b):(b-a);
			}else{
				a=a.toString();
				b=b.toString();
				flag=mode?(a-b):(b-a);
			}
			return flag;
		};
	}
	tab_arr.sort(SortArr(mode));
	for(i=0;i<tab_arr.length;i++){
		the_tab.lastChild.appendChild(tab_arr[i][1]);
	}

	window.status = " (Time spent: " + (new Date - start) + "ms)";
}

//表头与数据在不同的table中
function taxis(element){
  var the_obj = event.srcElement;
  var i = 0 ,j = 0;
  if(the_obj.tagName.toLowerCase() != "table" && the_obj.tagName.toLowerCase() != "tbody" && the_obj.tagName.toLowerCase() != "tr"){
    var the_td	= get_Element(the_obj,"td");
 	  if(the_td==null)
 	    return;
	  var the_tr	= the_td.parentElement;
	  var the_table	= get_Element(the_td,"table");
    var i = 0;
 		this_cur_row = the_tr.rowIndex;
	  this_cur_col = the_td.cellIndex;
  	if(this_col!=null){
 		  with(the_table.rows[this_cur_row].cells[this_col])
		    removeChild(lastChild);
	  }
	  element.click();
	  with(get_Element(element,"table").rows[cur_row].cells[sort_col])
		  removeChild(lastChild);
		sort_col = null;
	  with(the_table.rows[this_cur_row].cells[this_cur_col])
		  appendChild(element.mode?arrowUp:arrowDown);
		this_col=this_cur_col;
	}
}