<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/login.jsp,v 1.1 2012/03/12 01:59:20 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:59:20 $
  $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>

<%
String appError = (String)request.getAttribute("appError");
if (appError==null) appError="";
else appError=appError+",";
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title>CBCSV2.0.0 - 科室核算系统</title>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
p {font-size:9pt}; 
td {font-size:9pt}; 
li {font-size:9pt}; 
select {font-size:9pt}; 

.input01 {
	background-image: url(images/login/inputbg.png);
	font-size: 13px;
	color: #1452AF;
	text-decoration: none;
	clear: none;
	float: none;
	border: 0px none;
	height: 16px;
	width: 130px;
	line-height: 16px;
	text-align: left;
	vertical-align: top;
	left: 0px;
	top: 0px;
	right: 0px;
	bottom: 0px;
	background-attachment: fixed;
	background-position: top;
}
.style1 {
	font-size: 12px;
	color: #2B497E;
}
.xiala {
	font-size: 13px;
	color: #1452AF;
	text-decoration: none;
	clear: none;
	float: none;
	height: 16px;
	width: 141px;
	line-height: 16px;
	border: thin dotted;
}
-->
</style>

<script language="JavaScript" type="text/JavaScript">
<!--
	parent.relogin();
	if ((parent.parent.parent.parent.window.opener!=null && !parent.parent.parent.parent.window.opener.closed) ||
			(parent.window.opener!=null && !parent.window.opener.closed) ||
			(parent.parent.window.opener!=null && !parent.parent.window.opener.closed) ) {
		alert('<%=appError%>');
		parent.parent.parent.parent.window.close();
	}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('images/login/dl03.gif','images/login/dl02.gif','images/login/cz03.gif','images/login/cz02.gif')" topmargin="25" leftmargin="0" scroll="no">
<table width="792" height="534"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="231" valign="top" background="images/login/bg.png"><table width="780" height="407"  border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="443" height="248"></td>
        <td width="338" height="248"></td>
      </tr>
      <tr>
        <td></td>
        <td align="left"></td>
      </tr>
      <tr>
        <td height="20"></td>
        <td height="20" valign="top"><table width="141" height="18"  border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="20" align="center" valign="top" background="images/login/inputbg2.png"><input name="textfield" type="" class="input01" id="ID_user_id" accesskey='U'  onKeyPress='return checkEnter(event)'>
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="34"></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td height="20" valign="top"><table width="141" height="20"  border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="20" align="center" valign="top" background="images/login/inputbg2.png"><input name="textfield" type="password" class="input01" id="ID_password" accesskey='P' onKeyPress='return checkEnter(event)'>
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="55"></td>
        <td valign="top"><table width="100%" height="42"  border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="22"></td>
            </tr>
            <tr>
              <td height="20" valign="top"><select name="select" class="xiala"  id="ID_app" accesskey='S' onkeypress='return checkEnter(event)'>
                  <option value="dept">科室核算系统</option>
                  <option value="proj">项目核算系统</option>
                  <option value="disease">病种核算系统</option>
                  <option value="dist">内部分配系统</option>
                  <option value="eco">经营分析评价系统</option>
                  <option value="payout">支出控制系统</option>
                  <option value="decide">经营预测投资决策支持系统</option>
                  <option value="rate">项目作业法模型</option>
              </select></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td></td>
        <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="102"><table width="95" height="41"  border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="center" valign="middle" background="images/login/db.png"><img a href="#" onMouseOut="MM_swapImgRestore()"  onMouseUp="MM_swapImage('Image1','','images/login/dl03.gif',1)" onMouseOver="MM_swapImage('Image1','','images/login/dl03.gif',1)" onMouseDown="MM_swapImage('Image1','','images/login/dl02.gif',1)" src="images/login/dl01.gif" name="Image1" width="82" height="30" border="0" onClick='return login();' style='cursor:hand'></td>
                  </tr>
              </table></td>
              <td align="left"><table width="92" height="41"  border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="center" valign="middle" background="images/login/cb.png"><img a href="#" onMouseOut="MM_swapImgRestore()" onMouseUp="MM_swapImage('Image2','','images/login/cz03.gif',1)"   onMouseOver="MM_swapImage('Image2','','images/login/cz03.gif',1)" onMouseDown="MM_swapImage('Image2','','images/login/cz02.gif',1)"src="images/login/cz01.gif" name="Image2" width="80" height="30" border="0" onClick='return p_reset();' style='cursor:hand'></td>
                  </tr>
              </table></td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="100%"  border="0" cellspacing="4" cellpadding="0" align="center" >
  <tr>
    <td height="25" align="center" valign="middle"><span class="style1">CBCS V1.7.0 &copy;2003-2006 北京望海康信科技有限公司</span></td>
  </tr>
</table>
</body>
</html>


<script>

// 清空密码
function p_reset() {
  ID_user_id.value=''
  ID_password.value=''
}

// 检测
function checkEnter(e) {
  if (e.keyCode==13)
    return login();
}

// 登陆
function login() {
  if (ID_user_id.value.length==0) {
      alert("用户代码不能为空，请您重新输入！");
      ID_user_id.focus();
      return false;
    }
    if (ID_password.value.length==0) {
      alert("用户密码不能为空，请您重新输入！");
      ID_password.focus();
      return false;
    }

    if (document.all)
      var maxW = screen.width, maxH = screen.height;
    else if (document.layers)
      var maxW = window.outerWidth, maxH = window.outerHeight;
    else
      var maxW = 800, maxH= 600;

    var childTitle='cbcs';
    ID_user_id.focus();
    var strSubmit = 'baseSign.jspviewhigh?rerrwrsafbjvcjfjgjghjghjgj=wewwerasdfzxcvxvzxcvzxcvzxcvsdfawer&subFunction=signIn&user_id='+ID_user_id.value+'&SDF234RSDFDSF2345R243RFDASDFAS234RSDDDRFFFFFFFFFFFFFFFF=qdrqerqqaRFDGfGSDF&password='+ID_password.value+'&appModuleName='+ID_app.value
    
    if (window.opener!=null) {
    	if (!window.opener.closed) { // 当父窗口还没有被关闭
    		window.location.href=strSubmit
    		return true;
    	}
    }
    var newWin = window.open(strSubmit, childTitle, "width="+screen.availWidth+", height="+screen.availHeight+", top=0, left=0,  scrollbars=1,resizable=1");
    ID_user_id.value='';
    ID_password.value='';
    newWin.focus();
    return false;
}

</script>