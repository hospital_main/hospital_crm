var menu_map = new Array();
menu_map = [ 
          ['dept', '科室核算', '6'],
	      ['_dept_1_', '数据交换', '1'],
              ['_dept_1_1_', '数据交换',  '2'],
              ['_dept_2_', '收入数据', '1'],
              ['_dept_2_3_', '数据维护', '2'],
              ['_dept_2_3_1_', '收入数据维护', '3'],
              ['_dept_2_3_2_', '财政补贴收入数据', '3'],
              ['_dept_2_4_', '收入数据统计', '2'],
              ['_dept_2_4_1_', '科室开单统计',  '3'],
              ['_dept_2_4_2_', '科室执行统计',  '3'],
              ['_dept_2_4_3_', '项目开单统计',  '3'],
              ['_dept_2_4_4_', '项目执行统计',  '3'],
              ['_dept_2_5_', '工作量维护', '2'],
              ['_dept_2_5_1_', '门诊工作量', '3'],
              ['_dept_2_5_2_', '住院工作量', '3'],
              ['_dept_3_', '成本数据', '1'],
              ['_dept_3_1_', '直接成本', '2'],
              ['_dept_3_1_1_', '数据维护','3'],
              ['_dept_3_1_2_', '财务凭证导入', '3'],
              ['_dept_3_1_3_', '科室汇总', '3'],
              ['_dept_3_1_4_', '项目汇总', '3'],
              ['_dept_3_2_', '内部服务', '2'],
              ['_dept_3_2_1_', '数据维护', '3'],
              ['_dept_3_2_2_', '受益科室汇总', '3'],
              ['_dept_3_2_3_', '服务科室汇总', '3'],
              ['_dept_3_3_', '固定资产折旧','2'],
              ['_dept_3_4_', '预算成本', '2'],
              ['_dept_4_', '财务数据', '1'],
              ['_dept_4_1_', '资产负债表', '2'],
              ['_dept_4_2_', '收入支出总表', '2'],
              ['_dept_4_3_', '药品收支明细表', '2'],
              ['_dept_4_4_', '医疗收支明细表', '2'],
              ['_dept_4_5_', '医疗基本数字表', '2'],
              ['_dept_4_6_', '基金变动情况表','2'],
              ['_dept_5_', '成本分摊', '1'],
              ['_dept_5_1_', '成本分摊', '2'],
              ['_dept_5_2_', '分摊配置', '2'],
              ['_dept_5_3_', '参数配置', '2'],
              ['_dept_5_4_', '科室关系', '2'],
              ['_dept_6_', '成本分析', '1'],
              ['_dept_6_1_', '结余分析', '2'],
              ['_dept_6_1_1_', '收益总表一', '3'],
              ['_dept_6_1_2_', '收益总表二','3'],
              ['_dept_6_1_3_', '直接医疗分析一','3'],
              ['_dept_6_1_4_', '直接医疗分析二','3'],
              ['_dept_6_1_5_', '医技科室分析','3'],
              ['_dept_6_2_', '结余分析附表', '2'],
              ['_dept_6_2_1_', '收益总表一附表','3'],
              ['_dept_6_2_2_', '收益总表二附表','3'],
              ['_dept_6_2_3_', '直接医疗分析二附表', '3'],
              ['_dept_6_3_', '成本构成分析', '2'],
              ['_dept_6_3_1_', '医院成本构成',  '3'],
              ['_dept_6_3_2_', '直接医疗成本构成','3'],
              ['_dept_6_3_3_', '医技科室成本构成', '3'],
              ['_dept_6_3_4_', '医辅成本构成',  '3'],
              ['_dept_6_3_5_', '管理成本构成',  '3'],
              ['_dept_6_4_', '成本分类分析', '2'],
              ['_dept_6_4_1_', '全院成本分类分析表（一）',  '3'],
              ['_dept_6_4_2_', '全院成本分类分析表（二）',  '3'],
              ['_dept_6_4_3_', '全院成本分类分析表（三）',  '3'],
              ['_dept_6_4_4_', '全院成本分类分析表（四）',  '3'],
              ['_dept_6_5_', '成本比较分析', '2'],
              ['_dept_6_5_1_', '科室成本比较', '3'],
              ['_dept_6_5_2_', '项目成本比较',  '3'],
              ['_dept_6_5_3_', '医疗成本比较',  '3'],
              ['_dept_6_5_4_', '科室直接成本比较',  '3'],
              ['_dept_6_6_', '分摊汇总', '2'],
              ['_dept_6_6_1_', '科室成本分摊表', '3'],
              ['_dept_6_6_2_', '成本项目分摊表',  '3'],
              ['_dept_6_7_', '绩效考核数据',  '2'],
              ['_dept_6_7_1_', '自定义科室', '3'],
              ['_dept_6_7_2_', '直接医疗收益排名',  '3'],
              ['_dept_6_7_1_', '医技收益排名', '3'],
              ['_dept_7_', '自定义报表', '1'],
              ['_dept_7_1_', '报表定义', '2'],
              ['_dept_7_1_1_', '报表定义', '3'],
              ['_dept_7_1_2_', '报表权限定义',  '3'],
              ['_dept_7_2_', '报表查询', '2'],
              ['_dept_7_3_', '报表数据准备', '2'],
              ['_dept_7_4_', '院区定义', '2'],
              ['_dept_7_5_', '集合定义', '2'],
              ['_dept_7_5_1_', '科室集合定义', '3'],
              ['_dept_7_5_2_', '成本集合定义',  '3'],
              ['_dept_7_5_3_', '收入集合定义', '3'],
              ['_dept_7_5_4_', '自定义科室集合定义',  '3']
              ['_dept_8_', '资料维护', '1'],
              ['_dept_8_1_', '基础资料', '2'],
              ['_dept_8_1_1_', '科室分类', '3'],
              ['_dept_8_1_2_', '核算科室',  '3'],
              ['_dept_8_1_3_', '收入项目',  '3'],
              ['_dept_8_1_4_', '收费类别',  '3'],
              ['_dept_8_1_5_', '医疗项目', '3'],
              ['_dept_8_1_6_', '成本项目','3'],
              ['_dept_8_1_7_', '折旧类型', '3'],
              ['_dept_8_1_8_', '服务项目',  '3'],
              ['_dept_8_1_9_', '服务材料',  '3'],
              ['_dept_8_1_10_', '科室代码映射',  '3'],
              ['_dept_8_2_', '公共资料', '2'],
              ['_dept_8_2_1_', '职工信息',  '3'],
              ['_dept_8_2_2_', '职务信息', '3'],
              ['_dept_8_2_3_', '人员类别信息',  '3'],
              ['_dept_8_2_4_', '职称信息', '3'],
              ['_dept_8_2_5_', '学历信息',  '3'],
              ['_dept_8_3_', '成本分摊资料', '2'],
              ['_dept_8_3_1_', '科室类别',  '3'],
              ['_dept_8_3_2_', '成本类型',  '3'],
              ['_dept_8_3_3_', '成本所属类型', '3'],
              ['_dept_8_3_4_', '大用户', '3'],
              ['_dept_8_4_', '其他资料', '2'],
              ['_dept_8_4_1_', '自定义科室',  '3'],
              ['_dept_8_4_2_', '自定义科室定义', '3'],
              ['_dept_9_', '系统设置', '1'],
              ['_dept_9_1_', '单位基本信息','2'],
          //['payout', '支出控制系统', '92'],
              //['_payout_1_', '支出审批', '1'],
              //['_payout_1_1_', '支出申请',  '2'],
              //['_payout_1_2_', '支出审批',  '2'],
              //['_payout_1_3_', '财务稽核',  '2'],
              //['_payout_1_4_', '支付',  '2'],
          ['proj', '项目成本核算', '92'],
              ['_proj_1_', '数据采集', '1'],
              ['_proj_1_1_', '数据导入', '2'],
              ['_proj_1_2_', '收入数据维护',  '2'],
              ['_proj_1_3_', '合作成本定义',  '2'],
              ['_proj_1_4_', '单位成本数据',  '2'],
              ['_proj_1_4_1_', '单位成本定义',  '3'],
              ['_proj_1_4_2_', '单位成本查询',  '3'],
              ['_proj_1_5_', '直接成本数据',  '2'],
              ['_proj_1_5_1_', '直接成本维护',  '3'],
              ['_proj_1_5_2_', '材料单价维护',  '3'],
              ['_proj_2_', '项目成本核算',  '1'],
              ['_proj_2_1_', '项目成本核算',  '2'],
              ['_proj_2_2_', '平均成本管理',  '2'],
              ['_proj_2_2_1_', '平均成本核算',  '3'],
              ['_proj_2_2_2_', '标准成本管理',  '3'],
              ['_proj_2_2_3_', '标准成本查询',  '3'],
              ['_proj_3_', '项目成本分析',  '1'],
              ['_proj_3_1_', '项目收益分析', '2'],
              ['_proj_3_1_1_', '全院收益分析',  '3'],
              ['_proj_3_1_2_', '科室收益分析',  '3'],
              ['_proj_3_2_', '项目构成分析',  '2'],
              ['_proj_3_2_1_', '全院单位成本构成分析',  '3'],
              ['_proj_3_2_2_', '科室单位成本构成分析',  '3'],
              ['_proj_3_2_3_', '全院单位成本分类分析',  '3'],
              ['_proj_3_2_4_', '科室单位成本分类分析',  '3'],
              ['_proj_3_3_', '项目比较分析',  '2'],
              ['_proj_3_3_1_', '全院单位收益比较分析',  '3'],
              ['_proj_3_3_2_', '科室单位收益比较分析',  '3'],
              ['_proj_3_3_3_', '全院单位成本比较分析',  '3'],
              ['_proj_3_3_4_', '科室单位成本比较分析',  '3'],
              ['_proj_3_4_', '项目趋势分析','2'],
              ['_proj_3_4_1_', '全院单位收益趋势分析',  '3'],
              ['_proj_3_4_2_', '科室单位收益趋势分析',  '3'],
              ['_proj_3_4_3_', '全院单位成本趋势分析',  '3'],
              ['_proj_3_4_4_', '科室单位成本趋势分析',  '3'],
              ['_proj_3_5_', '项目同期分析','2'],
              ['_proj_3_5_1_', '全院单位收益同期分析',  '3'],
              ['_proj_3_5_2_', '科室单位收益同期分析',  '3'],
              ['_proj_3_5_3_', '全院单位成本同期分析',  '3'],
              ['_proj_3_5_4_', '科室单位成本同期分析',  '3'],
              ['_proj_4_', '资料维护', '1'],
              ['_proj_4_1_', '成本分类定义', '2'],
              ['_proj_4_1_1_', '成本分类',  '3'],
              ['_proj_4_1_2_', '成本所属分类',  '3'],
              ['_proj_4_2_', '成本对应关系',  '2'],
              ['_proj_4_3_', '物资信息维护',  '2'],
              ['_proj_4_3_1_', '物资分类',  '3'],
              ['_proj_4_3_2_', '材料信息',  '3'],
              ['disease', '病种成本核算', '105'],
              ['_disease_1_', '数据采集', '', '1'],
              ['_disease_1_1_', '病人收入导入',  '2'],
              ['_disease_1_2_', '病人数据维护',  '2'],
              ['_disease_1_3_', '药品平均数据',  '2'],
              ['_disease_1_4_', '药品加成率',  '2'],
              ['_disease_2_', '成本核算', '1'],
              ['_disease_2_1_', '病种成本核算',  '2'],
              ['_disease_2_2_', '目标成本核算',  '2'],
              ['_disease_2_2_1_', '目标成本核算', '3'],
              ['_disease_2_2_2_', '目标成本管理',  '3'],
              ['_disease_2_3_', '病人成本查询',  '2'],
              ['_disease_2_3_1_', '住院', '3'],
              ['_disease_2_3_2_', '门诊',  '3'],
              ['_disease_2_4_', '病种成本查询', '2'],
              ['_disease_2_4_1_', '住院', '3'],
              ['_disease_2_4_2_', '门诊',  '3'],
              ['_disease_3_', '病种成本分析', '1'],
              ['_disease_3_1_', '总收益分析', '2'],
              ['_disease_3_1_1_', '全院分析', '3'],
              ['_disease_3_1_2_', '科室分析', '3'],
              ['_disease_3_2_', '单位收益分析', '2'],
              ['_disease_3_2_1_', '全院分析', '3'],
              ['_disease_3_2_2_', '科室分析', '3'],
              ['_disease_3_3_', '成本时间对比', '2'],
              ['_disease_3_3_1_', '全院病种成本',  '3'],
              ['_disease_3_3_2_', '科室病种成本', '3'],
              ['_disease_3_4_', '成本同期对比', '2'],
              ['_disease_3_4_1_', '全院病种成本', '3'],
              ['_disease_3_4_2_', '科室病种成本', '3'],
              ['_disease_3_5_', '病种对比分析', '2'],
              ['_disease_3_5_1_', '单位收益比较分析', '3'],
              ['_disease_3_5_2_', '单位成本比较分析', '3'],
              ['_disease_3_5_3_', '单位临床路径收益分析', '3'],
              ['_disease_4_', '资料维护', '1'],
              ['_disease_4_1_', '病种定义', '2'],
              ['_disease_4_2_', 'DRG分组定义','2'],
              ['_disease_4_3_', '标准临床路径',  '2'],
              ['_disease_4_4_', '药品分类信息',  '2'],
              ['_disease_4_5_', '药品信息',  '2'],
              ['_disease_4_6_', '药品项目定义',  '2'],
          ['dist', '内部分配系统', '134'],
              ['_dist_1_', '奖金分配定义', '1'],
              ['_dist_1_1_', '奖金项目', '2'],
              ['_dist_1_2_', '收入比例',  '2'],
              ['_dist_1_3_', '成本项目', '2'],
              ['_dist_1_4_', '考核指标',  '2'],
              ['_dist_1_5_', '科室考核',  '2'],
              ['_dist_1_6_', '个人考核',  '2'],
              ['_dist_1_7_', '奖金分配方案',  '2'],
              ['_dist_2_', '科室奖金分配', '1'],
              ['_dist_2_1_', '控制定额', '2'],
              ['_dist_2_1_1_', '定额维护',  '3'],
              ['_dist_2_1_2_', '执行查询', '3'],
              ['_dist_2_2_', '经营科室',  '2'],
              ['_dist_2_3_', '管理科室',  '2'],
              ['_dist_2_4_', '服务科室', '2'],
              ['_dist_2_5_', '其它奖金', '2'],
              ['_dist_2_6_', '分配状态', '2'],
              ['_dist_3_', '个人奖金分配', '1'],
              ['_dist_3_1_', '奖金分配',  '2'],
              ['_dist_3_2_', '奖金查询',  '2'],
              ['_dist_3_3_', '分配状态',  '2'],
              ['_dist_4_', '汇总查询分析', '1'],
              ['_dist_4_1_', '科室奖金汇总',  '2'],
              ['_dist_4_2_', '科室总金额分析',  '2'],
              ['_dist_4_3_', '人均金额分析',  '2'],
              ['_dist_4_4_', '个人奖金分析',  '2'],
              ['_dist_4_5_', '人员类别分析',  '2'],
              ['_dist_4_6', '考核成绩查询', '1'],
              ['_dist_4_6_1_', '科室成绩',  '2'],
              ['_dist_4_6_2_', '个人成绩',  '2'],
          ['eco', '经营分析评价系统', '154'],
              ['_eco_1_', '管理视角', '1'],
              ['_eco_1_1_', '管理分析',  '2'],
              ['_eco_1_2_', '视角定义',  '2'],
              ['_eco_2_', '全院分析', '1'],
              ['_eco_2_1_', '基本指标', '2'],
              ['_eco_2_2_', '资源配置指标', '2'],
              ['_eco_2_2_1_', '人力资源',  '3'],
              ['_eco_2_2_2_', '物力资源', '3'],
              ['_eco_2_2_3_', '财力资源', '3'],
              ['_eco_2_3_', '运营能力指标', '2'],
              ['_eco_2_4_', '财务指标', '2'],
              ['_eco_2_4_1_', '收入指标',  '3'],
              ['_eco_2_4_2_', '成本指标',  '3'],
              ['_eco_2_4_3_', '收益指标', '3'],
              ['_eco_2_5_', '综合分析',  '2'],
              ['_eco_3_', '科室分析', '1'],
              ['_eco_3_1_', '收入分析', '2'],
              ['_eco_3_1_1_', '门诊住院收入', '3'],
              ['_eco_3_1_2_', '医疗药品收入',  '3'],
              ['_eco_3_1_3_', '医技收入',  '3'],
              ['_eco_3_2_', '成本分析', '2'],
              ['_eco_3_2_1_', '门诊住院成本',  '3'],
              ['_eco_3_2_2_', '医疗药品成本', '3'],
              ['_eco_3_2_3_', '医技成本',  '3'],
              ['_eco_3_2_4_', '管理成本', '3'],
              ['_eco_3_2_5_', '医辅成本', '3'],
              ['_eco_3_3_', '收益分析', '2'],
              ['_eco_3_3_1_', '门诊住院收益', '3'],
              ['_eco_3_3_2_', '医疗药品收益', '3'],
              ['_eco_3_3_3_', '直接医疗收益',  '3'],
              ['_eco_3_3_3_', '医技收益',  '3'],
              ['_eco_3_4_', '综合分析', '2'],
              ['_eco_4_', '项目分析', '1'],
              ['_eco_4_1_', '项目收入',  '2'],
              ['_eco_4_2_', '项目成本',  '2'],
              ['_eco_4_3_', '项目收益',  '2'],
              ['_eco_4_4_', '综合分析',  '2'],
              ['_eco_5_', '病种分析', '1'],
              ['_eco_5_1_', '收入分析',  '2'],
              ['_eco_5_2_', '成本分析',  '2'],
              ['_eco_5_3_', '收益分析',  '2'],
              ['_eco_5_4_', '综合分析','2'],
              ['_eco_6_', '数据采集', '1'],
              ['_eco_6_1_', '全院基本指标', '2'],
              ['_eco_6_1_1_', '直接录入', '3'],
              ['_eco_6_1_2_', '自动采集',  '3'],
              ['_eco_6_1_3_', '数据继承',  '3'],
              ['_eco_6_2_', '科室基本指标', '2'],
              ['_eco_6_2_1_', '直接录入',  '3'],
              ['_eco_6_2_2_', '自动采集', '3'],
              ['_eco_6_2_3_', '数据继承',  '3'],
              ['_eco_6_3_', '项目基本指标', '2'],
              ['_eco_6_3_1_', '直接录入',  '3'],
              ['_eco_6_3_2_', '自动采集',  '3'],
              ['_eco_6_3_3_', '数据继承',  '3'],
              ['_eco_6_4_', '病种基本指标', '2'],
              ['_eco_6_4_1_', '直接录入', '3'],
              ['_eco_6_4_2_', '自动采集', '3'],
              ['_eco_6_4_3_', '数据继承', '3'],
              ['_eco_7_', '指标计算', '1'],
              ['_eco_7_1_', '全院指标',  '2'],
              ['_eco_7_2_', '科室指标', '2'],
              ['_eco_7_3_', '项目指标', '2'],
              ['_eco_7_4_', '病种指标',  '2'],
              ['_eco_8_', '资料维护', '1'],
              ['_eco_8_1_', '指标组定义', '2'],
              ['_eco_8_2_', '指标分组', '2'],
              ['_eco_8_3_', '指标自定义', '2'],
          ['decide', '经营预测投资决策系统', '187'],
              ['_decide_1_', '成本查询', '1'],
              ['_decide_1_1_', '床日成本查询',  '2'],
              ['_decide_1_2_', '诊次成本查询', '2'],
              ['_decide_2_', '保本点预测', '1'],
              ['_decide_2_1_', '保本点查询', '2'],
              ['_decide_2_1_1_', '住院科室', '3'],
              ['_decide_2_1_2_', '门诊科室',  '3'],
              ['_decide_2_2_', '安全边际查询', '2'],
              ['_decide_2_2_1_', '住院科室', '3'],
              ['_decide_2_2_2_', '门诊科室',  '3'],
              ['_decide_3_', '收益预测', '1'],
              ['_decide_3_1_', '本量利法', '2'],
              ['_decide_3_1_1_', '住院科室', '3'],
              ['_decide_3_1_2_', '门诊科室',  '3'],
              ['_decide_3_2_', '安全边际法', '2'],
              ['_decide_3_2_1_', '住院科室',  '3'],
              ['_decide_3_2_2_', '门诊科室', '3'],
              ['_decide_3_3_', '基期收益率法', '2'],
              ['_decide_3_3_1_', '住院科室',  '3'],
              ['_decide_3_3_2_', '门诊科室', '3'],
              ['_decide_4_', '目标预测', '1'],
              ['_decide_4_1_', '收益敏感实验', '2'],
              ['_decide_4_2_', '目标预测分析', '2'],
              ['_decide_4_2_1_', '住院科室', '3'],
              ['_decide_4_2_2_', '门诊科室',  '3'],
              ['_decide_5_', '投资决策管理', '1'],
              ['_decide_5_1_', '投资决策分析',  '2'],
              ['_decide_6_', '数据采集', '1'],
              ['_decide_6_1_', '计划数据管理', '2'],  
              ['rate', '项目作业法模型', '92'],
              ['_rate_1_', '数据采集', '1'],
              ['_rate_1_1_', '数据导入', '2'],
              ['_rate_1_2_', '收入数据维护',  '2'],
              ['_rate_2_', '项目作业管理',  '1'],
              ['_rate_2_1_', '项目作业定义',  '2'],
              ['_rate_2_2_', '人工作业维护',  '2'],
              ['_rate_2_2_1_', '工作时间维护',  '3'],
              ['_rate_2_2_2_', '人工作业管理',  '3'],
              ['_rate_2_2_3_', '人员工资维护',  '3'],
              ['_rate_2_3_', '作业成本维护',  '2'],
              ['_rate_2_3_', '直接成本数据',  '2'],
              ['_rate_2_3_1_', '直接成本数据',  '3'],
              ['_rate_2_3_2_', '材料单价维护',  '3'],
              ['_rate_3_', '分配参数管理',  '1'],
              ['_rate_3_1_', '项目权重定义', '2'],
              ['_rate_3_2_', '分配参数设置',  '2'], 
              ['_rate_4_', '项目成本计算',  '1'],
              ['_rate_4_1_', '项目成本计算',  '2'],
              ['_rate_4_2_', '项目成本验算',  '2'],
              ['_rate_4_3_', '项目成本发布',  '2'],
              ['_rate_4_3_1_', '全院平均（单文件）',  '3'],
              ['_rate_4_3_2_', '全院平均（多文件）',  '3'],
              ['_rate_4_3_3_', '科室项目',  '3'],
              ['_rate_4_3_4_', '科室内医疗项目',  '3'], 
              ['_rate_5_', '项目成本查询',  '1'],
              ['_rate_5_1_', '科室项目成本',  '2'],
              ['_rate_5_2_', '全院项目成本',  '2'],
              ['_rate_5_3_', '项目作业成本',  '2'],              
              ['_rate_6_', '资料维护', '1'],
              ['_rate_6_1_', '成本分类定义', '2'],
              ['_rate_6_1_1_', '成本分类',  '3'],
              ['_rate_6_1_2_', '成本所属分类',  '3'],
              ['_rate_6_2_', '成本对应关系',  '2'],
              ['_rate_6_3_', '物资信息维护',  '2'],
              ['_rate_6_3_1_', '物资分类',  '3'],
              ['_rate_6_3_2_', '材料信息',  '3'], 
              ['_rate_6_4_', '配置编码管理',  '2'], 
              ['_rate_6_5_', '定义核算科室',  '2'],
              ['_rate_6_6_', '项目作业定义',  '2'],
              ['_rate_6_7_', '工资项目定义',  '2']  ];
              
function Folder(folderDescription, hreference) //constructor
{
  //constant data
  this.desc = folderDescription
  this.hreference = hreference
  this.id = nEntries
  this.navObj = 0
  this.iconImg = 0
  this.nodeImg = 0
  this.isLastNode = 1

  //dynamic data
  this.isOpen = true
  this.iconSrc = "image/biao.gif"
  this.children = new Array
  this.nChildren = 0

  //methods
  this.initialize = initializeFolder
  this.setState = setStateFolder
  this.addChild = addChild
  this.createIndex = createEntryIndex
  this.hide = hideFolder
  this.display = display
  this.renderOb = drawFolder
  this.subEntries = folderSubEntries
  this.outputLink = outputFolderLink
}

function setStateFolder(isOpen)
{
  this.isOpen = isOpen
  propagateChangesInState(this)
}

function propagateChangesInState(folder)
{
  var i=0
  if (folder.isOpen)
  {
    if (folder.nodeImg)
      if (folder.isLastNode)
        folder.nodeImg.src = "image/ftv2blank.gif"
      else
	  folder.nodeImg.src = "image/ftv2blank.gif"
    folder.iconImg.src = "image/biao.gif"
    for (i=0; i<folder.nChildren; i++)
      folder.children[i].hide()
  }
  else
  {
    if (folder.nodeImg)
      if (folder.isLastNode)
        folder.nodeImg.src = "image/ftv2blank.gif"
      else
	  folder.nodeImg.src = "image/ftv2blank.gif"
    folder.iconImg.src = "image/biao1.gif"
    for (i=0; i<folder.nChildren; i++)
      folder.children[i].hide()
  }
}

function hideFolder()
{
  this.navObj.style.display = "none"
  this.setState(0)
}

function initializeFolder(level, lastNode, leftSide)
{
var j=0
var i=0
var numberOfFolders
var numberOfDocs
var nc

  nc = this.nChildren
  this.createIndex()

  var auxEv = ""
  auxEv = "<a>"
  if (level>0)
    if (lastNode) //the last 'brother' in the children array
    {
      this.renderOb(leftSide + auxEv + "<img name='nodeIcon" + this.id + "' src='image/ftv2blank.gif' width=16 height=22 border=0></a>")
      leftSide = leftSide + "<img src='image/ftv2blank.gif' width=16 height=22>"
      this.isLastNode = 1
    }
    else
    {
      this.renderOb(leftSide + auxEv + "<img name='nodeIcon" + this.id + "' src='image/ftv2blank.gif' width=16 height=22 border=0></a>")
      leftSide = leftSide + "<img src='image/ftv2blank.gif' width=16 height=22>"
      this.isLastNode = 0
    }
  else
    this.renderOb("")

  if (nc > 0)
  {
    level = level + 1
    for (i=0 ; i < this.nChildren; i++)
    {
      if (i == this.nChildren-1)
        this.children[i].initialize(level, 1, leftSide)
      else
        this.children[i].initialize(level, 0, leftSide)
      }
  }
}

function drawFolder(leftSide)
{
  doc.write("<table id='node" + this.id + "' style='position:block;' border=0 cellspacing=0 cellpadding=0>")
  doc.write("<tr><td>")
  doc.write(leftSide)
  this.outputLink()
  doc.write("<img name='folderIcon" + this.id + "' src='" + this.iconSrc+"' border=0></a></td><td valign=middle nowrap>")
  if (USETEXTLINKS)
  {
    this.outputLink()
    doc.write(this.desc + "</a>")
  }
  else
    doc.write(this.desc)
  doc.write("</td>")
  doc.write("</table>")
  this.navObj = doc.all["node"+this.id]
  this.iconImg = doc.all["folderIcon"+this.id]
  this.nodeImg = doc.all["nodeIcon"+this.id]
}

function outputFolderLink()
{
  if (this.hreference){
    doc.write("<a href='" + this.hreference + "' TARGET=\"mainFrame\" onClick='javascript:clickOnFolder("+this.id+")'>")}
  else
    //doc.write("<a>")
  doc.write("<a href='javascript:clickOnNode("+this.id+")'>")
}

function addChild(childNode)
{
  this.children[this.nChildren] = childNode
  this.nChildren++
  return childNode
}

function folderSubEntries()
{
  var i = 0
  var se = this.nChildren

  for (i=0; i < this.nChildren; i++){
    if (this.children[i].children) //is a folder
      se = se + this.children[i].subEntries()
  }

  return se
}


// Definition of class Item (a document or link inside a Folder)
// *************************************************************

function Item(itemDescription, iconSrc, itemLink) // Constructor
{
  // constant data
  this.desc = itemDescription
  this.link = itemLink
  this.id = -1 //initialized in initalize()
  this.navObj = 0 //initialized in render()
  this.iconImg = 0 //initialized in render()
  this.nodeImg = 0
  this.iconSrc = "I"+iconSrc+".gif"

  // methods
  this.initialize = initializeItem
  this.createIndex = createEntryIndex
  this.hide = hideItem
  this.display = display
  this.renderOb = drawItem
}

function hideItem()
{
  if (this.navObj.style.display == "none")
    return
  this.navObj.style.display = "none"
}

function initializeItem(level, lastNode, leftSide)
{
  this.createIndex()

  if (level>0)
    if (lastNode) //the last 'brother' in the children array
    {
      this.renderOb(leftSide + "<img name='nodeIcon" + this.id + "' src='image/ftv2blank.gif' width=16 height=22>")
      leftSide = leftSide + "<img src='image/ftv2blank.gif' width=16 height=22>"
    }
    else
    {
      this.renderOb(leftSide + "<img name='nodeIcon" + this.id + "' src='image/ftv2blank.gif' width=16 height=22>")
      leftSide = leftSide + "<img src='image/ftv2blank.gif' width=16 height=22>"
    }
  else
    this.renderOb("")
}

function drawItem(leftSide)
{
  doc.write("<table id='node" + this.id + "' style='position:block;' border=0 cellspacing=0 cellpadding=0>")
  doc.write("<tr><td>")
  doc.write(leftSide)
  doc.write("<a href=" + this.link + "'><img id='itemIcon"+this.id+"' src='image/"+this.iconSrc+"' border=0></a></td><td valign=middle nowrap>")
  if (USETEXTLINKS)
    doc.write("<a id=item"+this.id+" href=" + this.link + ">" + this.desc + "</a>")
  else
    doc.write(this.desc)
  doc.write("</table>")
  this.navObj = doc.all["node"+this.id]
  this.iconImg = doc.all["itemIcon"+this.id]
  this.nodeImg = doc.all["nodeIcon"+this.id]
}


// Methods common to both objects (pseudo-inheritance)
// ********************************************************

function display()
{
  this.navObj.style.display = "block"
}

function hide()
{
  this.navObj.style.display = "none"
}

function createEntryIndex()
{
  this.id = nEntries
  indexOfEntries[nEntries] = this
  nEntries++
}


// Events
// *********************************************************

function clickOnFolder(folderId)
{
  var clicked = indexOfEntries[folderId]
    clickOnNode(folderId)
  return
}

function clickOnNode(folderId)
{
  var clickedFolder = 0
  var state = 0
  selectednode.value=folderId
  clickedFolder = indexOfEntries[folderId]
  state = clickedFolder.isOpen

  clickedFolder.isOpen = !state //open<->close
  expendsFolder(clickedFolder)
}

function clickOnRoot(folderId)
{
  var clickedFolder = 0
  var state = 0
  selectednode.value=folderId
  clickedFolder = indexOfEntries[folderId]
  state = clickedFolder.isOpen

  clickedFolder.setState(!state) //open<->close
}

var _module = "";
var _1st_menu = "";
var _2nd_menu = "";
var _3rd_menu = "";
function initializeDocument()
{
  var idx = 13;
  for(i=0;i<menu_map.length;i++)
  {
 		menu_map[i][2]="" +idx+ "";  		
 		idx ++;
  }
  foldersTree.initialize(0, 1, "")
  foldersTree.display()

  doc.write("<layer top="+indexOfEntries[nEntries-1].navObj.top+">&nbsp;</layer>")
  // close the whole tree
  clickOnRoot(0)
  // open the root folder
  clickOnRoot(0)
  foldersTree.children[0].display() 
  foldersTree.children[1].display() 
  foldersTree.children[2].display() 
  foldersTree.children[3].display() 
  foldersTree.children[4].display() 
  
  getCurrentId()
  
  var openFolder = 0
  if(_module!="")
  {
    openFolder = indexOfEntries[_module]
    openFolder.display()
  	clickOnNode(eval(_module));
  	expendsFolder(openFolder)
	  if(_1st_menu!="")
	  {
	    openFolder = indexOfEntries[_1st_menu]
	    openFolder.display()
	  	clickOnNode(eval(_1st_menu));
	  	expendsFolder(openFolder)
		  if(_2nd_menu!="")
		  {
			  openFolder = indexOfEntries[_2nd_menu]
		    openFolder.display()
		  	clickOnNode(eval(_2nd_menu));
				expendsFolder(openFolder)
			  if(_3rd_menu!="")
			  {
			  	eval("item"+_3rd_menu).click();  
			  }
			  else
			  {
			  	eval("item"+_2nd_menu).click();
			  }
		  }
	  }
  }
}
function getCurrentId()
{
	_module = parent._module;
	_1st_menu = parent._1st_menu;
	_2nd_menu = parent._2nd_menu;
	_3rd_menu = parent._3rd_menu;
  _module = matching(_module);
  _1st_menu = matching(_1st_menu);
  _2nd_menu = matching(_2nd_menu);
  _3rd_menu = matching(_3rd_menu);
}

function expendsFolder(folder)
{
  var i=0
  if (folder.isOpen)
  {
    if (folder.nodeImg)
      if (folder.isLastNode)
        folder.nodeImg.src = "image/ftv2blank.gif"
      else
	  folder.nodeImg.src = "image/ftv2blank.gif"
    folder.iconImg.src = "image/biao.gif"
    for (i=0; i<folder.nChildren; i++)
      folder.children[i].display()
  }
  else
  {
    if (folder.nodeImg)
      if (folder.isLastNode)
        folder.nodeImg.src = "image/ftv2blank.gif"
      else
	  folder.nodeImg.src = "image/ftv2blank.gif"
    folder.iconImg.src = "image/biao1.gif"
    for (i=0; i<folder.nChildren; i++)
      folder.children[i].hide()
  }
}

function matching(old)
{
	for(i=0;i<menu_map.length;i++)
	{
		if(menu_map[i][0]==old)
			return menu_map[i][2];
	}
	return "";
}

function addfolder(Node)
{
  var str = "<table id='node"+Node.id+"' style='position:block;' border=0 cellspacing=0 cellpadding=0><tr><td>"
  str+=drawlevle(Node)
  if (Node.hreference)
    str+="<img name='nodeIcon"+Node.id+"' src='image/ftv2blank.gif' width=16 height=22 border=0><a href='" + Node.hreference + "' TARGET=\"mainFrame\" onClick='javascript:clickOnNode("+Node.id+")'><img name='folderIcon"+Node.id+"' src='image/biao1.gif' border=0></a></td><td valign=middle nowrap><a href='" + Node.hreference + "' TARGET=\"mainFrame\" onClick='javascript:clickOnNode("+Node.id+")'>"+Node.desc+"</a></td></table>"
  else
    str+="<img name='nodeIcon"+Node.id+"' src='image/ftv2blank.gif' width=16 height=22 border=0><a href='javascript:clickOnNode("+Node.id+")'><img name='folderIcon"+Node.id+"' src='image/biao1.gif' border=0></a></td><td valign=middle nowrap><a href='javascript:clickOnNode("+Node.id+")'>"+Node.desc+"</a></td></table>"
  return str
}

function additem(Node)
{
  var str = "<table id='node"+Node.id+"' style='position:block;' border=0 cellspacing=0 cellpadding=0><tr><td>"
  str+=drawlevle(Node)
  str+="<img name='nodeIcon"+Node.id+"' src='image/ftv2blank.gif' width=16 height=22 border=0><a href=" + Node.link + "'><img name='itemIcon"+Node.id+"' src='image/I0.gif' border=0></a></td><td valign=middle nowrap><a id='item"+Node.id+"' href=" + Node.link + "'>"+Node.desc+"</a></td></table>"
  return str
}

function getLastChildren(Node)
{
  if(Node.nChildren>0)
    return getLastChildren(Node.children[Node.nChildren-1])
  else
    return Node
}

function drawlevle(Node)
{
  var str=""
  if(Node.parent==foldersTree)
    return str
  else
  {
    str+=drawlevle(Node.parent)
    if(Node.parent.isLastNode==1)
      str+="<img src='image/ftv2blank.gif' width=16 height=22>"
    else
      str+="<img src='image/ftv2blank.gif' width=16 height=22>"
  }
  return str
}

function addNode(Node)
{
  Node.createIndex()
  if(Folder.prototype.isPrototypeOf(Node)){
    if(Node.parent.nChildren>1)
    {
      if(Folder.prototype.isPrototypeOf(Node.parent.children[Node.parent.nChildren-2]))
        Node.parent.children[Node.parent.nChildren-2].nodeImg.src="image/ftv2blank.gif"
      else
        Node.parent.children[Node.parent.nChildren-2].nodeImg.src="image/ftv2blank.gif"
      Node.parent.children[Node.parent.nChildren-2].isLastNode = 0
      if(Node.parent.children[Node.parent.nChildren-2].nChildren==0)
        Node.parent.children[Node.parent.nChildren-2].navObj.insertAdjacentHTML("AfterEnd",addfolder(Node))
      else
        getLastChildren(Node.parent.children[Node.parent.nChildren-2]).navObj.insertAdjacentHTML("AfterEnd",addfolder(Node))
    }else
    {
      Node.parent.navObj.insertAdjacentHTML("AfterEnd",addfolder(Node))
    }
    Node.navObj = doc.all["node"+Node.id]
    Node.iconImg = doc.all["folderIcon"+Node.id]
    Node.nodeImg = doc.all["nodeIcon"+Node.id]
  }
  else{
    if(Node.parent.nChildren>1)
    {
      if(Folder.prototype.isPrototypeOf(Node.parent.children[Node.parent.nChildren-2]))
        Node.parent.children[Node.parent.nChildren-2].nodeImg.src="image/ftv2blank.gif"
      else
        Node.parent.children[Node.parent.nChildren-2].nodeImg.src="image/ftv2blank.gif"
      Node.parent.children[Node.parent.nChildren-2].isLastNode = 0
      if(Node.parent.children[Node.parent.nChildren-2].nChildren==0)
        Node.parent.children[Node.parent.nChildren-2].navObj.insertAdjacentHTML("AfterEnd",additem(Node))
      else
        getLastChildren(Node.parent.children[Node.parent.nChildren-2]).navObj.insertAdjacentHTML("AfterEnd",additem(Node))
    }else
    {
      Node.parent.navObj.insertAdjacentHTML("AfterEnd",additem(Node))
    }
    Node.navObj = doc.all["node"+Node.id]
    Node.iconImg = doc.all["itemIcon"+Node.id]
    Node.nodeImg = doc.all["nodeIcon"+Node.id]
  }
  clickOnNode(Node.parent.id)
  clickOnNode(Node.parent.id)
}

function removeNode(NodeId)
{
  i=indexOfEntries[selectednode.value].parent.nChildren-1
//需要去掉父子关系！！
//  while(indexOfEntries[selectednode.value].parent.children[i]!=indexOfEntries[selectednode.value])
//  {
//    temp=indexOfEntries[selectednode.value].parent.children[i-1]
//    indexOfEntries[selectednode.value].parent.children[i-1]=indexOfEntries[selectednode.value].parent.children[i]
//    i--
//  }
  doc.all["node"+NodeId].outerText=""
}
// Auxiliary Functions for Folder-Tree backward compatibility
// *********************************************************

function gFld(description, hreference)
{
  folder = new Folder(description, hreference)
  return folder
}

function gLnk(target, iconSrc, description, linkData)
{
  fullLink = ""

  if (target==0)
  {
    fullLink = "'"+linkData+"' target=\"_top\""
  }
  else
  {
    if (target==1)
       fullLink = "'"+linkData+"' target=_blank "
    else
       fullLink = "'"+linkData+"' target=\"mainFrame\""
  }

  linkItem = new Item(description, iconSrc, fullLink)
  return linkItem
}

function insFld(parentFolder, childFolder)
{
  childFolder.parent = parentFolder
  return parentFolder.addChild(childFolder)
}

function insDoc(parentFolder, document)
{
  document.parent = parentFolder
  return parentFolder.addChild(document)
}

// Global variables
// ****************

USETEXTLINKS = 1
indexOfEntries = new Array
nEntries = 0
doc = document
