/**********************************************************************************************************************
 * $Archive: /viewhigh/CBCS/projects/ViewHigh/defaultroot/vh/javascript/system.js $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:59:05 $
 * $Modtime: 03-08-29 10:42 $
 * $Revision: 1.1 $
 *********************************************************************************************************************/


//-----------------------------------
// 检测浏览器版本
// 得到版本数字
//-----------------------------------

function detectVersion()
{
version = parseInt(navigator.appVersion);
return version;
}

//-----------------------------------
// 检测操作系统
// 是苹果系统或是WINDOWS系统
//-----------------------------------
function detectOS()
{
if(navigator.userAgent.indexOf('Win') == -1) {
OS = 'Macintosh';
} else {
OS = 'Windows';
}
return OS;
}

//-----------------------------------
// 检测浏览器为IE或是NETSCAPE
//-----------------------------------
function detectBrowser()
{
if(navigator.appName.indexOf('Netscape') == -1) {
browser = 'IE';
} else {
browser = 'Netscape';
}
return browser;
}

//-----------------------------------
// Fullscreen()
// 上述功能检测完毕，执行全屏功能
// 
//
// 下面的几个重要文件
// oldbrowser.html - 如果你使用的浏览器版本低于3.0，将会打开这个页面，提示升级等信息。

// windowversion.html - 你可以把它改为想要全屏显示的页面，如果是IE，它将全屏显示，如果是NETSCAPE，它将最大化显示。
//
// thefullscreen.html - 如果一切正常，这就是我们要全屏显示的页面。
//
// 如何去掉滚动条
//果你测试你的页面时发现有滚动条，那么你将你的页面放进一个框架中，TOP框架设为100%，BOTTOM隐藏，这样就解决了。
// 注意，如果你使用框架，不要忘了HTML中的 javascript:self.close()
// FLASH中的按钮改为 target "parent".
//-----------------------------------
function FullScreen(){

var adjWidth;
var adjHeight;

if((detectOS() == 'Macintosh') && (detectBrowser() == 'Netscape')) {
adjWidth = 20;
adjHeight = 35;
}
if((detectOS() == 'Macintosh') && (detectBrowser() == 'IE')) {
adjWidth = 20;
adjHeight = 35;
winOptions = 'fullscreen=yes';
}
if((detectOS() == 'Windows') && (detectBrowser() == 'Netscape')) {
adjWidth = 30;
adjHeight = 30;
}
if(detectVersion() < 4) {
self.location.href = 'oldbrowser.html';
} else {
var winWidth = screen.availWidth - adjWidth;
var winHeight = screen.availHeight - adjHeight;
var winSize = 'width=' + winWidth + ',height=' + winHeight;
var thewindow = window.open('windowversion.html', 'WindowName', winSize);
thewindow.moveTo(0,0);
}
}

function MakeItSo(){
  if((detectOS() == 'Windows') && (detectBrowser() == 'IE')) {
  window.open('thefullscreen.html','windowname','fullscreen=yes');
  } else {
  onload=FullScreen();
  }
}



