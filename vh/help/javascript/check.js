
    // 判断Radio是否至少有一个被选中
    function isRadioChecked( element )
    {
        for( i = 0 ; i < 2; i++ )
        {
            if( element[ i ].checked == true )
            {
                return true;
            }
        }
        return false;
    }

    //用于判断一个字符串是否是日期格式的字符串
    function isDate(element ,Dilimeter, isYearMonth)
    {
        var DateString = element.value;

        if (DateString==null)
        {
            element.focus();
            element.select();
            return false;
        }
        if(isYearMonth)
        {
    	    DateString = DateString + '-1';
        }

        if (Dilimeter=='' || Dilimeter==null)
            Dilimeter = '-';
        var tempy='';
        var tempm='';
        var tempd='';
        var tempArray;
        if (DateString.length<8 || DateString.length>10)
        {
            element.focus();
            element.select();
            return false;
        }
        tempArray = DateString.split(Dilimeter);
        if (tempArray.length!=3)
        {
            element.focus();
            element.select();
            return false;
        }
        if(tempArray[1].length>2 || tempArray[2].length>2)
        {
                element.focus();
                element.select();
                return false;
        }

        if (tempArray[0].length==4)
        {
            tempy = tempArray[0];
            tempd = tempArray[2];
        }
        else
        {
            element.focus();
            element.select();
            return false;
        }
        //else
        //{
            //tempy = tempArray[2];
            //tempd = tempArray[1];
        //}
        tempm = tempArray[1];
        if( tempArray[1].length>1&&tempArray[1].substring(1,2) == '/')
        {
            element.focus();
            element.select();
            return false;
        }
        var tDateString = tempy + '/'+tempm + '/'+tempd+' 8:0:0';//加八小时是因为我们处于东八区
        var tempDate = new Date(tDateString);
        if (isNaN(tempDate))
        {
            element.focus();
            element.select();
            return false;
        }

        if (((tempDate.getUTCFullYear()).toString()==tempy) && (tempDate.getMonth()==parseInt(tempm)-1) && (tempDate.getDate()==parseInt(tempd)))
        {
            return true;
        }
        else
        {
            element.focus();
            element.select();
            return false;
        }
    }
