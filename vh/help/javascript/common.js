/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/help/javascript/common.js,v 1.1 2012/03/12 01:59:05 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:59:05 $
 * $Modtime: 03-09-03 16:48 $
 * $Revision: 1.1 $
 *

/*******************************************************
function HowManyDays()
purpose
  To know how many days in a month
  strMonth:which month
  strYear:which year
********************************************************/
function howManyDays(strYear, strMonth) {
	var strDate1=strMonth+"-"+"01"+"-"+strYear
	strMonth=parseInt(strMonth,10)+1
	var strDate2=strMonth +"-"+"01"+"-"+strYear
	var date1=new Date(strDate1)
	var date2=new Date(strDate2)
	var days=(date2 - date1)/24/60/60/1000
	return days;
}


/*******************************************************
function selectQuarterToInput()
purpose
  this is a component for year-quarter
  yearID: the year id
  quarterID: the quarter id
  year_quarterID: the year_quarter id
********************************************************/
function selectQuarterToInput(yearID, quarterID, year_quarterID) {

  if (year_quarterID.value!='' && (yearID.options[yearID.selectedIndex].value=='' || quarterID.options[quarterID.selectedIndex].value=='')) {
    yearID.options[0].selected=true;
    quarterID.options[0].selected=true;
    year_quarterID.value='';
    return false;
  }

  if (quarterID.value=='' && yearID.options[yearID.selectedIndex].value=='') {
    yearID.options[1].selected=true;
  }

  if (quarterID.value=='' && quarterID.options[quarterID.selectedIndex].value=='') {
    quarterID.options[1].selected=true;
  }

  year_quarterID.value=yearID.value+quarterID.value;
}



/*******************************************************
function selectMonthToInput()
purpose
  this is a component for year-month
  yearID: the year id
  monthID: the month id
  year_monthID: the year_month id
********************************************************/
function selectMonthToInput(yearID, monthID, year_monthID) {

  if (year_monthID.value!='' && (yearID.options[yearID.selectedIndex].value=='' || monthID.options[monthID.selectedIndex].value=='')) {
    yearID.options[0].selected=true;
    monthID.options[0].selected=true;
    year_monthID.value='';
    return false;
  }

  if (year_monthID.value=='' && yearID.options[yearID.selectedIndex].value=='') {
    yearID.options[1].selected=true;
  }

  if (year_monthID.value=='' && monthID.options[monthID.selectedIndex].value=='') {
    monthID.options[1].selected=true;
  }

  year_monthID.value=yearID.value+monthID.value;
}


/*******************************************************
function selectDateToInput()
purpose
  this is a component for date
  yearID: the year id
  monthID: the month id
  dayID: the day of the month
  dateID: the year_month id
********************************************************/
function selectDateToInput(yearID, monthID, dayID, dateID) {

  if (dateID.value!='' && (yearID.options[yearID.selectedIndex].value=='' || monthID.options[monthID.selectedIndex].value=='' || dayID.options[dayID.selectedIndex].value=='')) {
    yearID.options[0].selected=true;
    monthID.options[0].selected=true;
    dayID.options[0].selected=true;
    dayID.length=32;
    dayID.options[29].value='29';
    dayID.options[29].text='29';
    dayID.options[30].value='30';
    dayID.options[30].text='30';
    dayID.options[31].value='31';
    dayID.options[31].text='31';

    dateID.value='';
    return false;
  }

  if (dateID.value=='' && yearID.options[yearID.selectedIndex].value=='') {
    yearID.options[1].selected=true;
  }

  if (dateID.value=='' && monthID.options[monthID.selectedIndex].value=='') {
    monthID.options[1].selected=true;
  }

  if (dateID.value=='' && dayID.options[dayID.selectedIndex].value=='') {
    dayID.options[1].selected=true;
  }

  var iDay = howManyDays(yearID, monthID);
  if (iDay==29) {
    dayID.length=30;
    dayID.options[29].value='29';
    dayID.options[29].text='29';
  } else if (iDay==30) {
    dayID.length=31;
    dayID.options[29].value='29';
    dayID.options[29].text='29';
    dayID.options[30].value='30';
    dayID.options[30].text='30';
  } else if (iDay==31) {
    dayID.length=32;
    dayID.options[29].value='29';
    dayID.options[29].text='29';
    dayID.options[30].value='30';
    dayID.options[30].text='30';
    dayID.options[31].value='31';
    dayID.options[31].text='31';
  }

  dateID.value=yearID.value+'-'+monthID.value+'-'+dayID.value;
}



/*******************************************************
function checkNumber(e)
purpose
  this function only allow input number
  E: key
********************************************************/
function checkNumber(e) {
  if ((e.keyCode>=48) && (e.keyCode<=57))
    return true;
  else
    return false;
}



/*******************************************************
函数描述：
		截取字符串
	函数参数：
		str：一个字符串
	返回值：
		字符串
********************************************************/
function trim(str) {
  if(str.length<1) return str;
  var i=0;
	for (i=0; i<str.length; i++) {
		if (str.charAt(i) != ' ') break;
	}
	str = str.substring(i, str.length);
	for (i=str.length-1; i>=0; i--) {
		if (str.charAt(i)!= ' ') break;
	}
	str = str.substring(0, i+1);
	return str;
}


/*******************************************************
函数描述：
		检查文本框的输入值是否大于maxLength
	函数参数：
		comp:       文本框
	  maxLength:  最大长度
********************************************************/
function isTooLong(comp, maxLength) {
	var s = comp.value;
	if(s == null || s.length == 0)
		return false;
	var byteLength = 0;
	var ch;
	for(var j=0; j < s.length;j++) {
		ch = s.charCodeAt(j);
		if (ch > 255) //这个字符非键盘字符，而是汉字字符
			byteLength++;
		byteLength++;
	}

	if(byteLength > eval(maxLength)){
		comp.focus();
		comp.select();
		return true;
	}	else
		return false;
}


/*******************************************************
函数描述：
		检查文本框的输入值是否为空
	函数参数：
		comp:       文本框
********************************************************/
function isEmpty(comp) {
  var str = comp.value;
  str = trim(str);
  if (str == '') {
    comp.select();
    comp.focus();
    return true;
  } else
    return false;
}



/**
 将value精确的小数点后percision位
**/
function roundOff(value, precision) {
  var flag=1
  if(value<0){
    flag=0;
    value=-value
  }
  value = "" + value //convert value to string
  precision = parseInt(precision);

  var whole = "" + Math.round(value * Math.pow(10, precision));

  var decPoint = whole.length - precision;

  if(decPoint > 0) {
    result = whole.substring(0, decPoint);
    result += ".";
    result += whole.substring(decPoint, whole.length);
  } else {
    result = "0.";
    for (; decPoint<0; decPoint++)
      result += "0";
    result += whole;
  }
  if(flag==0)
    result = -result
  return result;
}
/*******************************************************
函数描述：
		检查文本框的输入值是否为数字型
	函数参数：
		comp:       comp文本框
    sign：      "+"表示只能为正；"-"表示只能为负；无此参数表示正负均可
example: isNumber(text1,+)  只能为正
         isNumber(text1,-)  只能为负
         isNumber(text1)    正负均可
********************************************************/
function isNumber(comp,sign) {
  var number;
  var string = comp.value;
  if (string==null) return false;
  if ((sign!=null) && (sign!='-') && (sign!='+'))
  {
   alert('IsNumber(string,sign)的参数出错：nsign为null或"-"或"+"');
   return false;
  }
  number = new Number(string);
  if (isNaN(number))
  {
   return false;
  }
  else if ((sign==null) || (sign=='-' && number<0) || (sign=='+' && number>0))
  {
   return true;
  }
  else
   return false;
}

/*******************************************************
函数描述：
		检查文本框的输入值是否为数字型
	函数参数：
		comp:       comp文本框
    sign：      "+"表示只能为正；"-"表示只能为负；无此参数表示正负均可
example: isNumber(text1,+)  只能为正
         isNumber(text1,-)  只能为负
         isNumber(text1)    正负均可
********************************************************/
function checkLength(o,length)
{
	if(isNaN(o.value)) return true;
	if(o.value.indexOf('.')>=0&&o.value.substr(o.value.indexOf('.')+1,o.value.length).length>length)
	{
		alert("注意小数最多有"+length+"位! ");
		o.focus();
		return false;
  }

	if(o.value.indexOf('.')>=0&&o.value.substr(0,o.value.indexOf('.')).length>8)
	{
		alert("注意数据过大! ");
		o.focus();
		return false;
	}
	else if(o.value.indexOf('.')<0&&o.value.length>8)
	{
		alert("注意数据过大! ");
		o.focus();
		return false;
	}
	return true;
}
function maxNum(com){
var maxNum=100;
var num=com.value;
if(com.value>maxNum)
   return false;
else true;
}
/*******************************************************
函数描述：
		检查文本框的输入值是否为实数型格式
	函数参数：
		comp:       comp文本框
    integerdig：   整数部分最大长度
    decimaldig：   小数部分最大长度
********************************************************/
function isDouble(comp , integerdig, decimaldig) {
  var strSeparator='.';
  var strNumberArray;
  if (!isNumber(comp))
    {
      comp.focus();
      comp.select();
      return 0;//判断是否是数字格式
    }
  strNumberArray = comp.value.split(strSeparator);
  if(strNumberArray[0]==null) strNumberArray[0]=0;
  if(strNumberArray[1]==null) strNumberArray[1]=0;
  if (strNumberArray[0].length>integerdig)
    {
     comp.focus();
     comp.select();
     return 1;//判断整数位数是否过长
    }
  if (strNumberArray[0].length==0)
    {
      comp.focus();
      comp.select();
      return 2;//判断是否有整数部分
    }
  if (strNumberArray[1].length>decimaldig)
    {
     comp.focus();
     comp.select();
     return 3;//判断小数部分是否过长
     }
}


/*
 *
 * 函数描述： 检查考核分数
 */
function checkMark(comp) {
	if (comp.value=='') {
		alert('数值不能为空');
		comp.focus();
    comp.select();
    return false;
	}
  switch (isDouble(comp, 3, 2)) {
    case 0 :
      alert('请输入数值');
      comp.focus();
      comp.select();
      return false;
    case 1 :
      alert('请输入不大于100的数值');
      comp.focus();
      comp.select();
      return false;
    case 3 :
      alert('请仅输入两位小数');
      comp.focus();
      comp.select();
      return false;
  }
  if (parseFloat(comp.value)>100 ) {
    alert('请输入不大于100的数值');
    comp.focus();
    comp.select();
    return false;
  }
  return true;
}


/*
 *
 * 函数描述： 检查比例
 *
 */
function checkScale(comp) {
	if (comp.value=='') {
		alert('数值不能为空');
		comp.focus();
    comp.select();
    return false;
	}
  switch (isDouble(comp, 3, 2)) {
    case 0 :
      alert('请输入数值');
      comp.focus();
      comp.select();
      return false;
    case 1 :
      alert('请输入不大于100的数值');
      comp.focus();
      comp.select();
      return false;
    case 3 :
      alert('请仅输入两位小数');
      comp.focus();
      comp.select();
      return false;
  }
  return true;
}


/*
 *
 * 函数描述： 快速录入
 *	函数参数：
 *		e:							键值
 *   	inputComp:			输入框
 *   	selectComp:   	选择框
 *
 */
function quickInput(e, inputComp, selectComp) {
	if (e.keyCode==13) {
		inputComp.value = selectComp.options[selectComp.selectedIndex].text.substring(selectComp.options[selectComp.selectedIndex].value.length+1)
		return false;
	}
	if (e.keyCode==40) {
		if (selectComp.selectedIndex+1<selectComp.options.length)
		selectComp.options[selectComp.selectedIndex+1].selected = true;
		return false;
	}
	if (e.keyCode==38 ) {
		if (selectComp.selectedIndex>0)
			selectComp.options[selectComp.selectedIndex-1].selected = true;
		return false;
	}

	var length = selectComp.options.length
	for (var i=0; i<length; i++) {
		var value = selectComp.options[i].value
		if (inputComp.value.length>value.length)continue;
		if (inputComp.value == value.substring(0, inputComp.value.length)) {
			if (i+4>=length) {
				selectComp.options[length-1].selected=true;
				selectComp.options[i].selected=true;
			}
			else {
				selectComp.options[i+3].selected=true;
				selectComp.options[i].selected=true;
			}
			break;
		}
	}
}



