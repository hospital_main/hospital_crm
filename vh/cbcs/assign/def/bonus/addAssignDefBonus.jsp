<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/def/bonus/addAssignDefBonus.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create(){
    template.subFunction.value='addAssignDefBonus';
    var str = template.income_subj_code.value;
    if(template.bonus_name.value=="")
     { alert("项目名称不能为空");
       return;
      }

    template.income_subj_code_copy.value = str.substring(0, str.indexOf("-"));
    if(isTooLong(template.bonus_formula,255))
    {
      alert('公式太长!');
      return false;
    }
    if(isTooLong(template.memo,255))
    {
      alert('说明太长!');
      return false;
    }

    template.submit();
    return true;
  }

  // 返回
  function back(element) {
    template.subFunction.value='listAssignDefBonus';
    element.submit();
  }
</script>
<!-- 信息提示栏 -->

<%
	String[][] result = (String[][])session.getAttribute("preparedCreate");
%>

<html:html clazz="main">

<form name="template" method="post" action="assignDefBonus.jspviewhigh">
<html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>奖金项目</html:title>

<br>

<!-- 简单信息 -->
<html:table clazz="simple">

  <tr>
 		<td class="signText" nowrap="nowrap">
 			收入项目
 		</td>
    <td width="75%" class="normalText" nowrap="nowrap">

    	<select type="select" name="income_subj_code">
<%
	if (result!=null) {
	  for (int i=0; i<result.length; i++) {
	  	for(int j=0;j<result[i].length;j++){
	    	if(result[i][j]==null||result[i][j].equals(""))
	      	result[i][j]="&nbsp";
	    }
%>
    	<option value="<%=result[i][0]%>-<%=result[i][1]%>"><%=result[i][0]%>:<%=result[i][1]%></option>
<%
		}
	}
%>
    	</select>
    </td>
  </tr>
  <tr>
    <td class="signText" nowrap="nowrap">项目名称</td>
    <td class="normalText" nowrap="nowrap"><input type="text" name="bonus_name" size="20" maxlength="20"></td>
  </tr>
  <tr>
    <td class="signText" nowrap="nowrap">公 式</td>
    <td class="normalText" nowrap="nowrap">
    		<textarea name='bonus_formula' rows='2' cols='40' maxlength="20"></textarea>
    </td>
  </tr>
  <tr>
    <td class="signText" nowrap="nowrap">说 明</td>
    <td class="normalText" nowrap="nowrap">
    		<textarea name='memo' rows='2' cols='40' maxlength="20"></textarea>
    </td>
  </tr>
  <tr>
      <td colspan="2">
      <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      	<button class="pageBtn" onclick="return back(template);">返回</button> 
      	<!--      	<img src="images/create.gif" class="mouse" onclick="return create();" />
<img src="images/reset.gif" class="mouse" onclick="return reset();" />
      	<img src="images/return.gif" class="mouse" onclick="return back(template);" />-->
      </td>
  </tr>
</html:table>
<input type="hidden" name="subFunction" >
<input type="hidden" name="income_subj_code_copy" >
</form>
</html:html>
