<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/def/bonus/listAssignDefBonus.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function find() {
	  template.subFunction.value='listAssignDefBonus';
    template.submit();
    return true;
	}
  function create(){
    template.subFunction.value='preAddAssignDefBonus';
    template.submit();
    return true;
  }


  function remove(){

    var flag = false;

    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey' && template.elements[i].checked == true)
        flag = true;
    }

    if( flag != false){

      if (confirm('是否停用')){
        template.subFunction.value='remove';
        template.submit();
        return true;
      }else{
        return false;
      }
    }else{
      alert( "请先选择,再停用!");
      return false;
    }
  }

  function selectAll(){
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey'){
        if(template.elements[i].disabled == false){
          template.elements[i].checked = true;
        }
      }
    }
  }
</script>
<%
	BaseRO ro = (BaseRO)request.getAttribute("baseRO");
  TableMarge oper = new TableMarge(ro, "find();");
  oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
  oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
  oper.addOptionButton("images/stop.gif", "return remove()");   //  停用
  oper.addNeedButton("images/create.gif", "return create()");     //  添加
%>

<html:html clazz="main">
<form name="template" method="post" action="assignDefBonus.jspviewhigh">
<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<html:title clazz='module'>奖金项目</html:title>
<br>
<!-- 复杂信息 -->
<html:table clazz="complex">

	 	<!-- 操作 -->
   	<tr><td><%=oper%></td></tr>

		<!-- 结果集 -->
</html:table>
				<html:table clazz="result">
				  <html:tr clazz='label'>
				  	<td>选择</td>
				    <td>项目编号</td>
				    <td>项目名称</td>
				    <td>公 式 </td>
				    <td>收入项目</td>
				    <td>状态</td>
				    <td>说 明</td>
				  </html:tr>
				  <%
				  	if (ro!=null) {
					    String[][] result = ro.getTableResult();
					    if (result!=null) {
					    	for (int i=0; i<result.length; i++) {
					      	String primaryKey = result[ i ][ 0 ];
					        for(int j=0;j<result[i].length;j++){
					        	if(result[i][j]==null||result[i][j].equals(""))
					          	result[i][j]="&nbsp";
					        }

					        String rowColor = "rowGray";
          				if (i/2*2==i) rowColor = "rowWhite";

				  %>
				  <tr class="<%=rowColor%>">
				    <td >
				    	<input name="primaryKey" type="checkbox" value="<%=primaryKey%>" <%= (Integer.parseInt(primaryKey)<4?"disabled":"")%>>
				    </td>
				    <td class="normalText"><%=primaryKey%></td>
				    <td class="normalText"><%=result[i][1]%></td>
				    <td class="normalText"><%=result[i][2]%></td>
				    <td class="normalText"><%=result[i][3]%></td>
				    <td class="normalText"><%=result[i][5].equals("N")?"可用":"停用"%></td>
				    <td class="normalText"><%=result[i][4]%></td>
				  </tr>
				  <%
					  		}
					  	}
					  }
				  %>
				</html:table>


<input type="hidden" name="subFunction" >
</form>
</html:html>
