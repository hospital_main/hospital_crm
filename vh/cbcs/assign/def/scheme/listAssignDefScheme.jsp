<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/def/scheme/listAssignDefScheme.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,java.text.*,
                 com.viewhigh.cbcs.base.sql.BaseRO" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function find() {
	  template.subFunction.value='listAssignDefScheme';
    template.submit();
    return true;
	}
  function create(){
    template.subFunction.value='addAssignDefScheme';
    template.ActionType.value='goAddPrepared';
    template.submit();
    return true;
  }


  function update(){
    var flag = false;
    var count = 0;
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey' && template.elements[i].checked == true) {
        count = count +1;
        flag = true;
      }
    }

    if( flag!=false && count==1){
      template.subFunction.value='updateAssignDefScheme';
      template.ActionType.value='goUpdateAssignDefScheme';
      template.submit();
      return true;
    } else if(flag!=false && count!=1){
      alert("请先选择一项,再修改!");
      return false;
    }else{
      alert("请先选择,再修改!");
      return false;
    }
  }

  function remove(){

    var flag = false;

    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey' && template.elements[i].checked == true)
        flag = true;
    }

    if( flag != false){

      if (confirm('是否删除')){
        template.subFunction.value='removeAssignDefScheme';
        template.submit();
        return true;
      }else{
        return false;
      }
    }else{
      alert( "请先选择,再删除!");
      return false;
    }
  }

	function selectAll(){
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey'){
        if(template.elements[i].disabled == false){
          template.elements[i].checked = true;
        }
      }
    }
  }
</script>

<%
	BaseRO ro = (BaseRO)request.getAttribute("baseRO");
  TableMarge oper = new TableMarge(ro, "find();");
  oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
  oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
  oper.addOptionButton("images/remove.gif", "return remove()");     //  删除
  oper.addNeedButton("images/create.gif", "return create()");     //  添加
%>

<html:html clazz="main">
<form name="template" method="post" action="assignDefScheme.jspviewhigh">
<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<html:title clazz='module'>科室奖金分配方案</html:title>
<br>
<!-- 复杂信息 -->
<html:table clazz="complex">

	 	<!-- 操作 -->
   	<tr><td><%=oper%></td></tr>

		<!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
				  <tr>
				    <td class="resultLabel">选择</td>
				    <td class="resultLabel">科室编码</td>
				    <td class="resultLabel">科室名称</td>
				    <td class="resultLabel">奖金项目编码</td>
				    <td class="resultLabel">奖金项目名称</td>
				    <td class="resultLabel">提取比例</td>
				    <td class="resultLabel">成本调控额</td>
				    <td class="resultLabel">成本比例</td>
				  </tr>
				  <%
				  	if (ro!=null) {
					    String[][] result = ro.getTableResult();
					    if (result!=null) {
					    	for (int i=0; i<result.length; i++) {
					      	String primaryKey = result[ i ][ 0 ];
					        for(int j=0;j<result[i].length;j++){
					        	if(result[i][j]==null||result[i][j].equals(""))
					          	result[i][j]="&nbsp";
					        }
                   DecimalFormat percentFormat = new DecimalFormat("#0.00%");
                   DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
					        String rowColor = "rowGray";
          				if (i/2*2==i) rowColor = "rowWhite";

				  %>
				  <tr class="<%=rowColor%>">
				    <td class="normalText"><input name="primaryKey" type="checkbox" value="<%= result[i][0]%>-<%= result[i][2]%>"></td>
				    <td class="normalText"><a href="assignDefScheme.jspviewhigh?subFunction=updateAssignDefScheme&ActionType=goUpdateAssignDefScheme&primaryKey=<%= result[i][0]%>-<%= result[i][2]%>"><%= result[i][0]%></a></td>
				    <td class="normalText"><%= result[i][1]%></td>
				    <td class="normalText"><%= result[i][2]%></td>
				    <td class="normalText"><%= result[i][3]%></td>
				    <td class="normalText"><%= percentFormat.format(Double.parseDouble(result[i][4]))%></td>
				    <td class="normalText"><%= moneyFormat.format(Double.parseDouble(result[i][5]))%></td>
				    <td class="normalText"><%= percentFormat.format(Double.parseDouble(result[i][6]))%></td>

				  </tr>
				  <%
					  		}
					  	}
					  }
				  %>
				</html:table>
    	</td>
    </tr>

</html:table>
<input type="hidden" name="subFunction">
<input type="hidden" name="ActionType">
</form>
</html:html>
