<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/def/scheme/addAssignDefScheme.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function checkQuery() {
		if (template.dept_code.value=='<%=request.getParameter("dept_code")%>')
			return true;
		else {
			alert('请重新查询');
			return false;
		}
	}

	function find() {
	  template.subFunction.value='addAssignDefScheme';
	  template.ActionType.value='goAddPrepared';
	  show_wait();
    template.submit();
    return true;
	}
  function create() {
  	if (!checkQuery()) {
  		return false;
  	}
    var flag = false;
    var flagStr = "";
    var oldKey="";
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey'){
        if(template.elements[i].checked == true){
          var key = template.elements[i].value;
          key = key.substring(0, key.indexOf("-"));
          if (key == "1") {
            flag = true;
            flagStr = key;
          } else if (key == "2") {
            if (flag) {
              alert("效益奖金, 平均奖金, 服务奖金只能选其一！");
              return false;
            }
            flag = true;
            flagStr = key;
          } else if (key == "3") {
            if (flag) {
              alert("效益奖金, 平均奖金, 服务奖金只能选其一！");
              flag = true;
              return false;
            }
            flag = true;
            flagStr = key;
          } else if (flagStr == "2" || flagStr=="3") {
            alert("项目奖金仅能和效益奖金组合！");
            flag = true;
            return false;
          }
          oldKey = key;
        }
      }
    }
    if (!flag)  {
      alert("必须选择效益奖金, 平均奖金, 服务奖金之一！");
      return false;
    }

    var code = template.dept_code.value;
    var newDeptCode = code.substring(0, code.indexOf("-"));
    template.deptCode.value = newDeptCode;

    var cDeptCode;
    var cBonusCode
    for (var i = 0; i < template.elements.length; i++){

      if (template.elements[i].name == 'cdept_code'){
        cDeptCode = template.elements[i].value;
        cBonusCode = template.elements[i+1].value;

      }

      if (newDeptCode==cDeptCode && oldKey==cBonusCode) {
        alert("已经存在，请重新选择！");
        return false;
      }
    }

    template.dept_name.value = code.substring(code.indexOf("-") + 1);
    template.subFunction.value='addAssignDefScheme';
    template.ActionType.value='goAddSecod';
    template.submit();
    return true;
  }

	function selectAll(){
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey'){
        if(template.elements[i].disabled == false){
          template.elements[i].checked = true;
        }
      }
    }
  }

  // 返回
  function back(element) {
    template.subFunction.value='listAssignDefScheme';
    element.submit();
  }

  function search(){
    template.subFunction.value='exam';
    show_wait();
    template.submit();
    return true;
  }
</script>

<html:html clazz="main">
<form name="template" method="post" action="assignDefScheme.jspviewhigh">
<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<html:title clazz='module'>科室奖金分配方案</html:title>
<%
	String[][] result = (String[][])request.getAttribute("preparedCreate");
	String[][] check = (String[][])request.getAttribute("check");
  String dept;
  String me;
%>
<!-- 简单信息 -->
<html:table clazz="simple">
  <tr>
    <td class="signText" nowrap="nowrap">科室：</td>
    <td class="normalText" nowrap="nowrap">
      <select name="dept_code">
<%
	if (result!=null) {
	  for (int i=0; i<result.length; i++) {
	  	for(int j=0;j<result[i].length;j++){
	    	if(result[i][j]==null)
	      	result[i][j]="";
	    }
%>
    	<option <% dept=request.getParameter("dept_code");
                 me=request.getParameter("me");
                 if(dept!=null && me!=null){
                   if(dept.substring(0,dept.indexOf('-')).equals(result[i][0]))
                     out.print("selected");}else if(dept!=null && me==null){if(dept.equals(result[i][0])) out.print("selected");}%> value="<%=result[i][0]%>-<%=result[i][1]%>" ><%=result[i][0]%>:<%=result[i][1]%></option>
<%
		}
	}
%>
      </select>
    </td>
    <td><button class="pageBtn" onclick="return search()">查询</button></td>
  </tr>
</html:table>
<br>
<!-- 复杂信息 -->

<html:table clazz="complex">
	<!-- 操作 -->
	<tr><td><img src="images/selectedAll.gif" class="mouse" onclick="return selectAll()"/> 
<button class="pageBtn" onclick="return create();" >添加</button>	
<button class="pageBtn" onclick="return reset();" >重置</button>
	<button class="pageBtn" onclick="return back(template);">返回</button>  
	<!--<img src="images/create.gif" class="mouse" onclick="return create()"/> 
	<img src="images/reset.gif" class="mouse" onclick="return reset()"/>	
	<img src="images/return.gif" class="mouse" onclick="return back(template)"/> --></td></tr>
	<tr><td><br></td></tr>
  <!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
				  <html:tr clazz='label'>
				    <td>选择</td>
				    <td>奖金项目编码</td>
				    <td>奖金项目名称</td>
				  </html:tr>
				  <%
					    result = (String[][])request.getAttribute("table_result");
					    if (result!=null) {
					    	for (int i=0; i<result.length; i++) {
					        for(int j=0;j<result[i].length;j++){
					        	if(result[i][j]==null||result[i][j].equals(""))
					          	result[i][j]="&nbsp";
					        }

					        String rowColor = "rowGray";
          				if (i/2*2==i) rowColor = "rowWhite";

				  %>
				  <tr class="<%=rowColor%>">
				    <td align='center'><input name="primaryKey" type="checkbox" value="<%= result[i][0]%>-<%= result[i][1]%>" <%if(check!=null){for(int k=0; k<check.length; k++) if(check[k][1].equals(result[i][0])) out.print("checked disabled");}%>></td>
				    <td class="normalText"><%= result[i][0]%></td>
				    <td class="normalText"><%= result[i][1]%></td>
				  </tr>
				  <%
					  		}
					    }
				  %>
				</html:table>
    	</td>
    </tr>


	<!-- 操作 -->
	<tr><td><br></td></tr>

</html:table>
<%
if (check != null) {
  for (int i=0; i<check.length; i++) {
%>
  <input type="hidden" name="cdept_code" value="<%= check[i][0]%>">
  <input type="hidden" name="cbonus_code" value="<%= check[i][1]%>">
<%
  }
}
%>
<input type="hidden" name="me" value='me'>
<input type="hidden" name="subFunction">
<input type="hidden" name="ActionType">
<input type="hidden" name="dept_name">
<input type="hidden" name="deptCode">
</form>
</html:html>
