<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/def/scheme/addAssignDefSchemeSecod.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create() {
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].type == "text"){
        if(template.elements[i].value == ""){
          alert("输入框必须输入！");
          template.elements[i].focus();

          return false;
        }
      }
      if (template.elements[i].name=="cost_contorl"){
          switch(isDouble(template.elements[i],8,2)){
             case 0:
              alert('成本调控额应为数字型');
            return false;
          case 1 :
            alert('成本调控额整数部分不能高于8个字符');
            return false;
          case 3 :
            alert('成本调控额小数部分不能高于2个字符');
            return false;
            }
          }
      if (template.elements[i].name == "bonus_scale"
          || template.elements[i].name == "cost_scale") {

        if (!checkScale(template.elements[i]))
        	return false;
      }
    }
    template.subFunction.value='addAssignDefScheme';
    template.ActionType.value='addAssignDefScheme';
    template.submit();
    return true;
  }

	// 返回
  function back(element) {
    template.subFunction.value='addAssignDefScheme';
    template.ActionType.value='goBackPrepared';
    template.dept_name.value="";
    template.deptCode.value="";
    element.submit();
  }
</script>
<html:html clazz="main">
<form name="template" method="post" action="assignDefScheme.jspviewhigh">
<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<html:title clazz='module'>科室奖金分配方案</html:title>
<%
	String[][] result = (String[][])request.getAttribute("result");
%>
<!-- 简单信息 -->
<html:table clazz="simple">
  <tr>
    <td class="signText" nowrap="nowrap">科室编码</td>
    <td class="normalText" nowrap="nowrap">
      <input type="text" value="<%= request.getParameter("deptCode")%>" readonly>
    </td>
    <td class="signText" nowrap="nowrap">科室名称</td>
    <td class="normalText" nowrap="nowrap">
      <input type="text" value="<%= request.getParameter("dept_name")%>" readonly>
    </td>
  </tr>
</html:table>

<br>

<!-- 复杂信息 -->
<html:table clazz="complex">
<!-- 操作 -->
  <tr>
    <td colspan="5">
    <button class="pageBtn" onclick="return create();" >添加</button>
    <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--      <img src="images/create.gif" class="mouse" onclick="return create();" />
<img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />-->
    </td>
  </tr>
  <!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
          <html:tr clazz='label'>
				    <td>奖金项目编码</td>
				    <td>奖金项目名称</td>
				    <td>比例系数</td>
<%if(result[0][0].equals("2")){%>
				    <td>成本调控额</td>
				    <td>成本比例</td>
<%}%>
				  </html:tr>
				  <%
					 if (result!=null) {
					   for (int i=0; i<result.length; i++) {
					     String primaryKey = result[ i ][ 0 ];
					     String rowColor = "rowGray";
          	   if (i/2*2==i) rowColor = "rowWhite";

				  %>
				  <tr class="<%=rowColor%>">
				    <td class="normalText"><%= result[i][0]%><input type="hidden" value="<%= result[i][0]%>" name="bonus_code"></td>
				    <td class="normalText"><%= result[i][1]%><input type="hidden" value="<%= request.getParameter("deptCode")%>" name="dept_code"></td>
				    <td class="normalText"><input name="bonus_scale" type="text" value=""> %</td>
<%if(result[0][0].equals("2")){%>
				    <td class="normalText"><input name="cost_contorl" type="text" value=""></td>
				    <td class="normalText"><input name="cost_scale" type="text" value=""> %</td>
<%}%>
				  </tr>
				  <%

					  }
					}
				  %>
				</html:table>
    	</td>
    </tr>

</html:table>
<input type="hidden" name="dept_name" value="">
<input type="hidden" name="deptCode" >
<input type="hidden" name="subFunction">
<input type="hidden" name="ActionType">
</form>
</html:html>
