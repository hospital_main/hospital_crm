<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/def/scheme/updateAssignScheme.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function update() {
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].type == "text"){
        if(template.elements[i].value == ""){
          alert("输入筐必须输入！");
          tempalte.elements[i].focus();
          return false;
        }
      }
      if (template.elements[i].name == "bonus_scale"
          || template.elements[i].name == "cost_scale") {
        if (!checkScale(template.elements[i])) return false;
      }
    }
    template.subFunction.value='updateAssignDefScheme';
    template.submit();
    return true;
  }

	// 返回
  function back(element) {
    template.subFunction.value='listAssignDefScheme';
    element.submit();
  }
</script>

 <%
	BaseRO ro = (BaseRO)request.getAttribute("baseRO");
  TableMarge oper = new TableMarge(ro, "find();");
  oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
  oper.addOptionButton("images/save.gif", "return update()");     //  修改
  oper.addNeedButton("images/return.gif", "return back(template)");     //  返回
%>
<html:html clazz="main">
<form name="template" method="post" action="assignDefScheme.jspviewhigh">

<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<html:title clazz='module'>修改科室奖金分配方案</html:title>

<br>
<%
	String[] result = ro.getTableResult()[0];
%>
<!-- 简单信息 -->
<html:table clazz="simple">
  <tr>
    <td class="signText" nowrap="nowrap">科室编码</td>
    <td class="normalText" nowrap="nowrap">
      <input type="text" value="<%= result[0]%>" name="dept_code" readonly>
    </td>
    <td class="signText" nowrap="nowrap">科室名称</td>
    <td class="normalText" nowrap="nowrap">
      <input type="text" value="<%= result[1]%>" readonly>
    </td>
  </tr>
</html:table>

<!-- 复杂信息 -->
<html:table clazz="complex">
<!-- 操作 -->
  <tr>
    <td colspan="5">
    <button class="pageBtn" onclick="return update();">保存</button> 
    <button class="pageBtn" onclick="return reset();" >重置</button>
    <button class="pageBtn" onclick="return back(template);">返回</button>  
     <!-- <img src="images/save.gif" class="mouse" onclick="return update();" />
      <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />-->
    </td>
  </tr>

  <!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
				  <html:tr clazz='label'>
				    <td>奖金项目编码</td>
				    <td>奖金项目名称</td>
				    <td>比例系数</td>
<%if(result[2].equals("2")){%>
				    <td>成本调控额</td>
				    <td>成本比例</td>
<%}%>
				 </html:tr>

				  <tr class="rowWhite">
				    <td class="normalText"><%= result[2]%><input type="hidden" value="<%= result[2]%>" name="bonus_code"></td>
				    <td class="normalText"><%= result[3]%></td>
				    <td class="normalText"><input name="bonus_scale" type="text" value="<%= result[4]%>"> %</td>
<%if(result[2].equals("2")){%>
				    <td class="normalText"><input name="cost_contorl" type="text" value="<%= result[5]%>"></td>
				    <td class="normalText"><input name="cost_scale" type="text" value="<%= result[6]%>"> %</td>
<%}%>
				  </tr>

				</html:table>
    	</td>
    </tr>


	<!-- 操作 -->

</html:table>
<input type="hidden" name="subFunction">
<input type="hidden" name="ActionType">
</form>
</html:html>
