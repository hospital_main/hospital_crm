<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/def/income/listAssignDefIncome.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO,java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function find() {
	  template.subFunction.value='listAssignDefIncome';
    template.submit();
    return true;
	}
  function create() {

    template.subFunction.value='create';
    template.submit();
    return true;
  }


  function update(){
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'income_scale') {
        if (!checkScale(template.elements[i]))
        	return false;
      }
    }
  	template.subFunction.value='updateAssignDefIncome';
    template.submit();
    return true;
  }

	function change(obj) {
		var names = obj.name.substring(0, obj.name.lastIndexOf("_"));
		var count = obj.name.substring(obj.name.lastIndexOf("_") + 1);
		var newObj = eval("template." +names + "[" + count + "]");
		if (obj.value == 1) {
			newObj.value = 0;
			obj.value = 0;
		}else {
			newObj.value = 1;
			obj.value = 1;
		}
	}
</script>
<%
	BaseRO ro = (BaseRO)request.getAttribute("baseRO");
  TableMarge oper = new TableMarge(ro, "find();");
  oper.addOptionButton("images/save.gif", "return update()");     //  保存
  oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
  oper.addNeedButton("images/create.gif", "return create()");     //  添加
%>

<html:html clazz="main">
<form name="template" method="post" action="assignDefIncome.jspviewhigh">
<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<html:title clazz='module'>收入比例</html:title>
<!-- 简单信息 -->
<html:table clazz="simple">
	<tr>
  	<td nowrap class="signText" >科室类别名称：</td>
    <td nowrap class="normalText">
    	<%=new com.viewhigh.cbcs.base.mvc.view.component.Select(request.getAttribute("dept"), "subj_kind_code", request.getParameter("subj_kind_code"), true, false)%>
    </td>
    <td nowrap class="signText" >收费类型名称：</td>
    <td nowrap class="normalText">
    	<%=new com.viewhigh.cbcs.base.mvc.view.component.Select(request.getAttribute("income"), "charge_kind_code", request.getParameter("charge_kind_code"), true, false)%>
    </td>
    <td><img src='images/find.gif' style='cursor:hand' onclick='template.subFunction.value="listAssignDefIncome";show_wait();template.submit();'/></td>
	</tr>
</html:table>

<!-- 复杂信息 -->
<html:table clazz="complex">

	 	<!-- 操作 -->
   	<tr><td><%=oper%></td></tr>

		<!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
				  <html:tr clazz='label'>
				    <td>医疗项目类型编码</td>
				    <td>医疗项目类型名称</td>
				    <td>收入项目</td>
				    <td>科室类型编码</td>
				    <td>科室类型名称</td>
				    <td>收入比例</td>
				  </html:tr>
				  <%
				  	if (ro!=null) {
					    String[][] result = ro.getTableResult();
					    if (result!=null) {
					    	for (int i=0; i<result.length; i++) {
					      	String primaryKey = result[ i ][ 0 ];
					        for(int j=0;j<result[i].length;j++){
					        	if(result[i][j]==null||result[i][j].equals(""))
					          	result[i][j]="&nbsp";
					        }
                   DecimalFormat percentFormat = new DecimalFormat("#0.00");
					        String rowColor = "rowGray";
          				if (i/2*2==i) rowColor = "rowWhite";

				  %>
				  <tr class="<%=rowColor%>">
				    <td class="normalText"><%= result[i][0]%><input type="hidden" name="charge_kind_code_1" value="<%= result[i][0]%>"></td>
				    <td class="normalText"><%= result[i][1]%></td>
				    <td class="normalText"><%= result[i][2]%></td>
				    <td class="normalText"><%= result[i][3]%><input type="hidden" name="subj_kind_code_1" value="<%= result[i][3]%>"></td>
				    <td class="normalText"><%= result[i][4]%></td>
				    <td class="normalText"><input name="income_scale" type="text" value="<%= percentFormat.format(Double.parseDouble(result[i][5]))%>">%</td>
				  </tr>
				  <%
					  		}
					  	}
					  }
				  %>
				</html:table>
    	</td>
    </tr>

</html:table>
<input type="hidden" name="subFunction" >
</form>
</html:html>
