<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/def/mark/updateAssignDefMark.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function update(){
    template.subFunction.value='updateAssignDefMark';
    if(isTooLong(template.check_formula,255))
    {
      alert('公式太长!');
      return false;
    }
    template.submit();
    return true;
  }

  // 返回
  function back(element) {
    template.subFunction.value='listAssignDefMark';
    element.submit();
  }
</script>

<%
  String[][] result = (String[][])request.getAttribute("baseRO");
%>
<html:html clazz="main">
<form name="template" method="post" action="assignDefMark.jspviewhigh">
<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<html:title clazz='module'>添加考核指标</html:title>
<br>
<!-- 简单信息 -->
<html:table clazz="simple">
  <tr>
    <td class="signText" nowrap="nowrap">指标名称</td>
    <td class="normalText" nowrap="nowrap"><input type="text" name="check_name" size="20" maxlength="20" value="<%= result[0][1]%>"></td>
  </tr>
  <tr>
    <td class="signText" nowrap="nowrap">类别</td>
    <td class="signText" nowrap="nowrap">
    		<input type="radio" name="check_kind" value="1" <%= result[0][2].equals("1")?"checked":""%>>科室
    		<input type="radio" name="check_kind" value="2" <%= result[0][2].equals("2")?"checked":""%>>个人
    </td>
  </tr>
  <tr>
    <td class="signText" nowrap="nowrap">公式</td>
    <td class="normalText" nowrap="nowrap">
    		<textarea name='check_formula' rows='2' cols='40' maxlength="20"><%= result[0][3] %></textarea>
    </td>
  </tr>
  <tr>
      <td colspan="2">
      <button class="pageBtn" name=""   onclick="return update();"   >保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/reset.gif" class="mouse" onclick="return reset();" />
      	<img src="images/return.gif" class="mouse" onclick="return back(template);" />-->
      </td>
  </tr>
</html:table>
<input type="hidden" name="subFunction">
<input type="hidden" name="check_code" value="<%= result[0][0]%>">
</form>
</html:html>
