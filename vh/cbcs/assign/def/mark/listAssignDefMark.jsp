<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/def/mark/listAssignDefMark.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function find() {
	  template.subFunction.value='listAssignDefMark';
    template.submit();
    return true;
	}
  function create(){
    template.subFunction.value='addAssignDefMark';
    template.submit();
    return true;
  }


  function update(){
    var flag = false;
    var count = 0;
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey' && template.elements[i].checked == true) {
        count = count +1;
        flag = true;
      }
    }

    if( flag!=false && count==1){
      template.subFunction.value='updateAssignDefMark';
      template.submit();
      return true;
    } else if(flag!=false && count!=1){
      alert("请先选择一项,再修改!");
      return false;
    }else{
      alert("请先选择,再修改!");
      return false;
    }
  }

  function remove(){

    var flag = false;

    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey' && template.elements[i].checked == true)
        flag = true;
    }

    if( flag != false){

      if (confirm('是否删除')){
        template.subFunction.value='removeAssignDefMark';
        template.submit();
        return true;
      }else{
        return false;
      }
    }else{
      alert( "请先选择,再删除!");
      return false;
    }
  }

	function selectAll(){
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey'){
        if(template.elements[i].disabled == false){
          template.elements[i].checked = true;
        }
      }
    }
  }
</script>
<%
	BaseRO ro = (BaseRO)request.getAttribute("baseRO");
  TableMarge oper = new TableMarge(ro, "find();");
  oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
  oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
  oper.addOptionButton("images/remove.gif", "return remove()");     //  删除
  oper.addNeedButton("images/create.gif", "return create()");     //  添加
%>

<html:html clazz="main">
<form name="template" method="post" action="assignDefMark.jspviewhigh">
<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<html:title clazz='module'>考核指标</html:title>
<br>
<!-- 复杂信息 -->
<html:table clazz="complex">

	 	<!-- 操作 -->
   	<tr><td><%=oper%></td></tr>

		<!-- 结果集 -->
</html:table>
				<html:table clazz="result">
				  <html:tr clazz='label'>
				    <td>选择</td>
				    <td>指标编码</td>
				    <td>指标名称</td>
				    <td>类 别</td>
				    <td>公 式</td>
				  </html:tr>
				  <%
				  	if (ro!=null) {
					    String[][] result = ro.getTableResult();
					    if (result!=null) {
					    	for (int i=0; i<result.length; i++) {
					      	String primaryKey = result[ i ][ 0 ];
					        for(int j=0;j<result[i].length;j++){
					        	if(result[i][j]==null||result[i][j].equals(""))
					          	result[i][j]="&nbsp";
					        }

					        String rowColor = "rowGray";
          				if (i/2*2==i) rowColor = "rowWhite";

				  %>
				  <tr class="<%=rowColor%>">
				    <td class="normalText"><input name="primaryKey" type="checkbox" value="<%= result[i][0]%>"></td>
				    <td class="normalText"><a href="assignDefMark.jspviewhigh?subFunction=updateAssignDefMark&primaryKey=<%=primaryKey%>"><%= result[i][0]%></a></td>
				    <td class="normalText"><%= result[i][1]%></td>
				    <td class="normalText"><%= result[i][2].equals("1")?"科室":"个人"%></td>
				    <td class="normalText"><%= result[i][3]%></td>
				  </tr>
				  <%
					  		}
					  	}
					  }
				  %>
				</html:table>
	<!-- 操作 -->
<input type="hidden" name="subFunction" >
</form>
</html:html>
