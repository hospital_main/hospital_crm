<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/def/cost/listAssignDefCost.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function group() {
	  template.subFunction.value='listAssignDefCost';
    template.submit();
    return true;
	}
	function find() {
	  template.subFunction.value='listAssignDefCost';
    template.submit();
    return true;
	}
  function create(){
    template.subFunction.value='addAssignDefCost';
    template.submit();
    return true;
  }


  function update(){
  	template.subFunction.value='updateAssignDefCost';
    template.submit();
    return true;
  }

	function change(obj) {
		var names = obj.name.substring(0, obj.name.lastIndexOf("_"));
		var count = obj.name.substring(obj.name.lastIndexOf("_") + 1);
		var newObj = eval("template." +names + "[" + count + "]");
		if (obj.value == 1) {
			newObj.value = 0;
			obj.value = 0;
		}else {
			newObj.value = 1;
			obj.value = 1;
		}
	}
</script>

<%
	BaseRO ro = (BaseRO)request.getAttribute("baseRO");
  TableMarge oper = new TableMarge(ro, "find();");
  oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
  oper.addOptionButton("images/save.gif", "return update()");     //  重置
  oper.addNeedButton("images/create.gif", "return create()");     //  添加
%>

<html:html clazz="main">
<form name="template" method="post" action="assignDefCost.jspviewhigh">
 <!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<html:title clazz='module'>成本项目</html:title>
<!-- 简单信息 -->
<html:table clazz="simple">
	<tr>
  	<td nowrap class="signText" >科室类别：</td>
    <td nowrap class="normalText" colspan='3'>
    	<%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(session.getAttribute("result"), "subj_kind_code", request.getParameter("subj_kind_code"), true, true)%>
    </td>
    <td> <button class="pageBtn" onclick="group();" >查询</button></td>
	</tr>
</html:table>

<!-- 复杂信息 -->
<html:table clazz="complex">

	 	<!-- 操作 -->
   	<tr><td><%=oper%></td></tr>

		<!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
				  <html:tr clazz='label'>
				    <td>项目编号</td>
				    <td>项目名称</td>
				    <td>直接成本</td>
				    <td>公用成本</td>
				    <td>管理成本</td>
				    <td>医疗辅助</td>
				    <td>医疗技术</td>
				  </html:tr>
				  <%
				  	if (ro!=null) {
					    String[][] result = ro.getTableResult();
					    if (result!=null) {
					    	for (int i=0; i<result.length; i++) {
					      	String primaryKey = result[ i ][ 0 ];
					        for(int j=0;j<result[i].length;j++){
					        	if(result[i][j]==null||result[i][j].equals(""))
					          	result[i][j]="&nbsp";
					        }

					        String rowColor = "rowGray";
          				if (i/2*2==i) rowColor = "rowWhite";

				  %>
				  <tr class="<%=rowColor%>">
				    <td class="normalText"><%= result[i][0]%><input type="hidden" name="dept_code" value="<%= result[i][0]%>"></td>
				    <td class="normalText"><%= result[i][2]%><input type="hidden" name="subj_kind_codes" value="<%= result[i][1]%>"></td>
				    <td class="normalText"><input name="prime_cost_<%= i%>" type="checkbox" value="<%= result[i][3]%>" <%= (result[i][3].equals("1")?"checked":"")%> onClick="change(this)"><input type="hidden" name="prime_cost" value="<%= result[i][3]%>"></td>
				    <td class="normalText"><input name="in_adm_cost_<%= i%>" type="checkbox" value="<%= result[i][4]%>" <%= (result[i][4].equals("1")?"checked":"")%> onClick="change(this)"><input type="hidden" name="in_adm_cost" value="<%= result[i][4]%>"></td>
				    <td class="normalText"><input name="in_apport_cost_s_<%= i%>" type="checkbox" value="<%= result[i][1]%>-<%= result[i][5]%>" <%= (result[i][5].equals("1")?"checked":"")%> onClick="change(this)"><input type="hidden" name="in_apport_cost_s" value="<%= result[i][5]%>"></td>
				    <td class="normalText"><input name="in_apport_cost_t_<%= i%>" type="checkbox" value="<%= result[i][6]%>" <%= (result[i][6].equals("1")?"checked":"")%> onClick="change(this)"><input type="hidden" name="in_apport_cost_t" value="<%= result[i][6]%>"></td>
				    <td class="normalText"><input name="in_apport_cost_f_<%= i%>" type="checkbox" value="<%= result[i][7]%>" <%= (result[i][7].equals("1")?"checked":"")%> onClick="change(this)"><input type="hidden" name="in_apport_cost_f" value="<%= result[i][7]%>"></td>
				  </tr>
				  <%
					  		}
					  	}
					  }
				  %>
				</html:table>
    	</td>
    </tr>

</html:table>
<input type="hidden" name="subFunction" >
</form>
</html:html>
