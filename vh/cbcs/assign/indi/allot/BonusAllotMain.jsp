<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/indi/allot/BonusAllotMain.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.*,
  com.viewhigh.cbcs.base.mvc.view.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
  String[][] depts = (String[][])request.getAttribute("depts");
  String flag = "";
  if ( depts == null )
  	flag = "alert('您的科室不存在，不能进行查询');return false;";
%>


<Script language="javascript">

  function add() {
    <%= flag%>
    if (template.yearMonth.value == '') {
      alert('请选择核算年月!');
      return false;
    }
    var win =
      window.open(
        'bonusAllot.jspviewhigh?subFunction=prepareBonusAllot&yearMonth=' +
        template.yearMonth.value + '&deptCode=' + template.deptCode.value,
        "","width = 900, height = 480, top=220, left=50, scrollbars = 1, resizable = 1");
    return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="bonusAllot.jspviewhigh">

  <!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>奖金分配</html:title>
	<br>
  <html:table clazz="simple">
    <tr>
      <td>
        科室:
      </td>
      <td>
        <%=new SingleSelect(
          depts, "deptCode", request.getParameter("deptCode"), true, true)
        %>
      </td>
      <td>
        核算月:
      </td>
      <td>
        <%=new MonthComponent("yearMonth", request.getParameter("yearMonth"))%>
      </td>
      <td><button class="pageBtn" onclick="return add();">添加</button>
      </td>
    </tr>
  </html:table>
</form>
</html:html>



