<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/indi/query/IndividualBonusQuery.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.util.*,
  java.text.*, com.viewhigh.cbcs.base.mvc.view.*,
  com.viewhigh.cbcs.base.mvc.view.component.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
  String[][] depts = (String[][]) request.getAttribute("depts");
  String[][] result = (String[][]) request.getAttribute("table_result");
  String queryType = request.getParameter("queryType");
  String flag = "";
  if ( depts == null )
  	flag = "alert('没有科室所选，不能进行查询');return false;";
%>

<Script language="javascript">
  function query() {
    if (template.queryType[1].checked) {
      <%= flag%>
    }
    if (template.yearMonth.value == '') {
      alert('请选择核算年月!');
      return false;
    }

    if (template.queryType.value == 'emp') {
      if (template.empName.value.trim() == '') {
        alert('请输入职员名字!');
        return false;
      }
    }
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="individualBonusQuery.jspviewhigh">

  <!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>奖金查询</html:title>

  <!-- 简单信息 -->
  <html:table clazz="simple">
     <tr>
      <td>
        <input type="radio" name="queryType" value="all"
          <% if (queryType == null || queryType.equals("all")) {%>
          checked
          <%}%>>全部
      </td>
      <td>
        <input type="radio" name="queryType" value="dept"
        <% if (queryType != null && queryType.equals("dept")) {%>
          checked
        <%}%>
        >科室
      </td>
      <td>
         <%=new SingleSelect(
          depts, "deptCode", request.getParameter("deptCode"), true, true)
        %>
      </td>
      <td>
        <input type="radio" name="queryType" value="emp"
          <% if (queryType != null && queryType.equals("emp")) {%>
          checked
          <%}%>
        >职工名字
      </td>
      <td>
        <input type="text" name="empName"
          <%if (request.getParameter("empName") != null) {%>
            value=<%=request.getParameter("empName")%>
          <%}%>>
      </td>
      <td nowrap class="signText" >核算月： </td>
      <td nowrap class="normalText" colspan='3'><%=new MonthComponent("yearMonth",request.getParameter("yearMonth"))%></td>
    </tr>

    <tr>
      <td><button class="pageBtn" onclick="return query();" >查询</button></td>
    </tr>
  </html:table>

  <br>

  <!-- 复杂信息 -->
  <html:table clazz="complex">

  <!-- 结果集 -->
  <tr>
    <td>
      <html:table clazz="result">

        <html:tr clazz='label'>
          <td>科室编码</td>
          <td>科室名称</td>
          <td>姓名</td>
          <td>金额</td>
        </html:tr>

        <%
          DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");

          if (result != null) {
            for (int i = 0; i < result.length; i++) {
              String rowColor = "rowGray";
              for (int j = 0; j < result[i].length; j++) {
                if (result[i][j] == null || result[i][j].trim().equals("")) {
                  result[i][j] = "&nbsp;";
                }
              }

              if (!result[i][3].equals("&nbsp;")) {
                result[i][0] = "&nbsp;";
                result[i][1] = "&nbsp;";
                rowColor = "rowWhite";
              }
        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText"><%=result[i][0]%></td>
          <td class="normalText"><%=result[i][1]%></td>
          <td class="normalText"><%=result[i][3]%></td>
          <td class="numberText"><%=moneyFormat.format(Double.parseDouble(result[i][4]))%></td>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>
  </html:table>

  <input type="hidden" name="subFunction" value="query" />
</form>
</html:html>


