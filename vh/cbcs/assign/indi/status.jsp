<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/indi/status.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
function find() {
    var sign=false

    for(var i=0;i<template.year.length;i++)
      { if(template.year.options[i].value!="")
          {sign=true;
           break;
          }
      }
     if(sign==false)
       { alert("请选择年");
         return false
        }

    template.subFunction.value='find';
    template.submit();return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="individual_assign_status.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>分配状态</html:title>
  <!-- 查询信息 -->
    <%
      BaseRO  ro= (BaseRO)request.getAttribute("ro");
      String[][] result;
      if(ro!=null)
       result =ro.getTableResult();
      else   result=null;

      TableMarge oper = new TableMarge(ro, "return find()");
    %>
  <html:table clazz="simple">
    <tr>

      <td nowrap class="normalText">请选择年：
      <%String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};%>

      <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year",request.getParameter("year"),false,false)%>年
     </td>
     <td nowrap class="normalText">请选择科室：
       <select name="deptType" >
              <%String deptType=request.getParameter("deptType");%>
              <option value="1" <%if(deptType!=null&&deptType.equals("1")) out.print("selected");%>>经营科室</option>
              <option value="2" <%if(deptType!=null&&deptType.equals("2")) out.print("selected");%>>管理科室</option>
              <option value="3" <%if(deptType!=null&&deptType.equals("3")) out.print("selected");%>>服务科室</option>
      </select>
     </td>
      <td><button class="pageBtn" onclick="return find();" >查询</button></td>
</tr>

   </html:table>

  <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>
     <html:table clazz="result">
		        <html:tr clazz='label'>
          <td class="resultLabel">科室名称</td>
          <td class="resultLabel">1月</td>
          <td class="resultLabel">2月</td>
          <td class="resultLabel">3月</td>
          <td class="resultLabel">4月</td>
          <td class="resultLabel">5月</td>
          <td class="resultLabel">6月</td>
          <td class="resultLabel">7月</td>
          <td class="resultLabel">8月</td>
          <td class="resultLabel">9月</td>
          <td class="resultLabel">10月</td>
          <td class="resultLabel">11月</td>
         <td class="resultLabel">12月</td>

         </html:tr>

        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {

                String primaryKey = result[i][0];
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j]=="") result[i][j]="&nbsp;";
                }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText"><%=result[ i ][ 0]%></td>
          <td class="normalText"><%=result[ i ][ 1]%></td>
          <td class="normalText"><%=result[ i ][ 2]%></td>
          <td class="normalText"><%=result[ i ][ 3]%></td>
          <td class="normalText"><%=result[ i ][ 4]%></td>
          <td class="normalText"><%=result[ i ][ 5]%></td>
          <td class="normalText"><%=result[ i ][ 6]%></td>
          <td class="normalText"><%=result[ i ][ 7]%></td>
          <td class="normalText"><%=result[ i ][ 8]%></td>
          <td class="normalText"><%=result[ i ][ 9]%></td>
          <td class="normalText"><%=result[ i ][ 10]%></td>
          <td class="normalText"><%=result[ i ][ 11]%></td>
          <td class="normalText"><%=result[ i ][ 12]%></td>
        </tr>

        <%
              }
            }
        %>
       </html:table>
    </td>
  </tr>
  <!-- 操作 -->

  </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>


