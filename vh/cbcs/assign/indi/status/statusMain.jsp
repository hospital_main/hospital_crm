<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/indi/status/statusMain.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function find(){
    if(template.year.value==""){
      alert("请选择年");
      return false
    }
    template.subFunction.value='findAll';
    template.submit();
    show_wait();
    return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="assignIndiStatus.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>分配状态</html:title>
    <br>

  <!-- 查询信息 -->
    <%
      String[][] result = (String[][])request.getAttribute("table_result");
    %>

  <html:table clazz="simple">
    <tr>

      <td nowrap class="signText">请选择年：
      <%String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};%>

      <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year",request.getParameter("year"),false,false)%>年
     </td>
      <td><button class="pageBtn" onclick="return find();" >查询</button></td>
</tr>


  </html:table>
    <br>
  <html:table clazz="complex">
  <!-- 操作 -->

  <!-- 结果集 -->
  <tr>
    <td>
      <html:table clazz="result">
        <html:tr clazz='label'>
          <td>核算类型</td>
          <td>1月</td>
          <td>2月</td>
          <td>3月</td>
          <td>4月</td>
          <td>5月</td>
          <td>6月</td>
          <td>7月</td>
          <td>8月</td>
          <td>9月</td>
          <td>10月</td>
          <td>11月</td>
          <td>12月</td>
        </html:tr>

        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {

                String primaryKey = result[i][0];
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j]=="") result[i][j]="&nbsp;";
                }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
<%
     for(int j=0;j<13;j++){
       if((result[i][j].equals("完成")) && (!result[i][0].equals("科室核算"))){
                 %>
          <td class="normalText"><a href="assignIndiStatus.jspviewhigh?subFunction=detail&dept=<%=i%>&year_month=<%=request.getParameter("year")%><%=j%>"><%=result[i][j]%></a></td>
     <%}else{%>
          <td class="normalText"><%=result[i][j]%></td>
      <%}
     } %>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>
  <!-- 操作 -->
  </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>
