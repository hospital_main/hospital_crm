<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/indi/status/report_show.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
function find() {
   if(template.year_month.value=='')
     {alert("请选择年月")
      return false
     }
   if(template.kind.value=='')
      {alert("科室类别")
      return false
     }
    template.subFunction.value='findAll';
    template.submit();return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="assignIndiStatus.jspviewhigh">
  <html:message/>
	  <html:title clazz='module'><%if(request.getParameter("dept").equals("1")) out.print("经营科室");else if(request.getParameter("dept").equals("2")) out.print("管理科室");else if(request.getParameter("dept").equals("3")) out.print("服务科室"); out.print(((String)request.getParameter("year_month")).substring(0,4)+"年"+request.getParameter("year_month").substring(4)+"月奖金分配表");%></html:title>
  <!-- 查询信息 -->
    <%

      DecimalFormat nf = new DecimalFormat("#,##0.00");
      DecimalFormat percentFormat = new DecimalFormat("#0.00%");
      String[][] result = (String[][])request.getAttribute("ro");
      String[][] allName=(String[][])request.getAttribute("allName");
    %>
<br>
  <html:table clazz="complex">

		<tr>
      <td><button class="pageBtn"  onclick="history.back(-1)">返回</button> </td>
		</tr>
		<tr><td><br></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>
   <%  if ( result != null )
            {
   %>
      <html:table clazz="result">
   <%      String kind=request.getParameter("dept");
           if(kind.equals("1")||kind.equals("3")) {
   %>
		        <html:tr clazz='label'>
            <td >科室编号</td>
            <td >科室名称</td>
            <td >人数</td>
            <td >直接收入</td>
            <td >成本 </td>
            <td >比例 </td>
            <td >考核分数 </td>
           <%if(allName!=null)
              for(int i=0;i<allName.length;i++)
                { if(!allName[i][0].equals("1"))
           %>
               <td ><%=allName[i][1]%> </td>
           <%     }
           %>
            <td >应发金额 </td>
            <td >调控金额 </td>
            <td >实发金额</td>
        </html:tr>
        <% }
           if(kind.equals("2")) {
        %>
            <html:tr clazz='label'>
            <td >科室编号</td>
            <td >科室名称</td>
            <td >人数</td>
            <td >人均奖金</td>
            <td >比例数 </td>
            <td >考核分数 </td>
            <td >金额 </td>
            <td >成本金额 </td>
            <td >成本调控金额 </td>
            <td >调控比例 </td>
            <td >应发金额 </td>
            <td >调控金额 </td>
            <td >实发金额</td>
            <td >人均金额</td>
             </html:tr>
        <%}%>

        <%
           if(kind.equals("1")||kind.equals("3")){
              for (int i = 0; i < result.length; i++ )
              {
          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText"><%=result[ i ][ 0]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 1]%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 2]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 3]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 4]))%></td>
          <td nowrap class="numberText"><%=percentFormat.format(Double.parseDouble(result[ i ][ 5]))%></td>
          <td nowrap class="numberText"><%=percentFormat.format(Double.parseDouble(result[ i ][ 6]))%></td>
         <%for(int j=7;j<result[0].length;j++)
            {
          %>
           <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ j]))%></td>
          <%}%>
        </tr>

        <%
              }
           }

        %>
          <%
           if(kind.equals("2")){
              for (int i = 0; i < result.length; i++ )
              {
          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText"><%=result[ i ][ 0]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 1]%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 2]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 3]))%></td>
          <td nowrap class="numberText"><%=percentFormat.format(Double.parseDouble(result[ i ][ 4]))%></td>
          <td nowrap class="numberText"><%=percentFormat.format(Double.parseDouble(result[ i ][ 5]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 6]))%></td>
           <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 7]))%></td>
           <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 8]))%></td>
           <td nowrap class="numberText"><%=percentFormat.format(Double.parseDouble(result[ i ][ 9]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 10]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 11]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 12]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 13]))%></td>

        </tr>

        <%
              }
           }

        %>
     </html:table>
     <% }
        %>
    </td>
  </tr>

  </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>


