<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/dept/mana/succeedAssignDeptMana.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO,java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>



<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  function group() {
    if (template.year_month.value == "") {
      alert("请选择年月！");
      return false
    }
	  template.subFunction.value = "calculateAssignDeptMana";
    show_wait();
    template.submit();
    return true;
	}
	function save() {
   	for(var i=0;i<template.elements.length;i++){
    if(template.elements[i].name=="check_mark"){
       if (!checkMark(template.elements[i])) return false;
		} else if(template.elements[i].name=="control_amount"){
     switch(isDouble(template.elements[i],8,2)) {
        case 0 : alert('调控金额必须为数字型'); return;
        case 1 : alert('调控金额整数部分不能高于8个字符'); return;
        case 2 : alert('调控金额没有整数部分'); return;
        case 3 : alert('调控金额小数部分不能高于2个字符'); return;
      }
	}
}

	  template.subFunction.value = "saveAssignDeptMana";
    show_wait();
    template.submit();

    return true;
	}
	function finish() {
	  template.subFunction.value = "finishAssignDeptMana";
	  template.submit();
    return true;
	}
</script>
 <html:html clazz="main">
<!-- 信息提示栏 -->
	  <html:message/>

<!-- 标题栏 -->
 <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("管理科室奖金分配")%>


<form name="template" method="post" action="assignDeptMana.jspviewhigh">

<!-- 简单信息 -->
<table  width="100%" cellspacing="2" border="0" >
	<tr>
  	<td nowrap class="signText" >核算月:</td>
    <td nowrap class="normalText" colspan='3'>
    	<!-- 年月组件 -->
      <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
    </td>
    <td><button class="pageBtn" onclick="group();" >查询</button></td>
	</tr>
</table>

<br>

<%
  DecimalFormat pf=new DecimalFormat("#0.00");
  DecimalFormat mf=new DecimalFormat("#,##0.00");
  String[][] result=(String[][])request.getAttribute("result");
%>

<!-- 复杂信息 -->
<html:table clazz="complex">

	 	<!-- 操作 -->
  <tr>
   	<td>
   	<button class="pageBtn" onclick="reset();" >重置</button> 
   	 <!-- <img src="images/reset.gif" class="mouse" onclick="reset();" />-->
   	  <% if (result != null && result.length >0) {%>
   	  <button class="pageBtn" onclick="save();">保存</button> 
   	  <!--  <img src="images/save.gif" class="mouse" onclick="save();" />-->
   	    <img src="images/finish.gif" class='mouse' onclick='finish();' />
   	  <% } %>
   	</td>
  </tr>

		<!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
				  <tr>
				    <td class="resultLabel">科室编号</td>
				    <td class="resultLabel">科室名称</td>
				    <td class="resultLabel">人数</td>
				    <td class="resultLabel">人均奖金</td>
				    <td class="resultLabel">比例数</td>
				    <td class="resultLabel">考核分数</td>
				    <td class="resultLabel">金额</td>

				    <td class="resultLabel">成本金额</td>
				    <td class="resultLabel">成本调控金额</td>
				    <td class="resultLabel">调控比例</td>
				    <td class="resultLabel">应发金额</td>
				    <td class="resultLabel">调控金额</td>
				    <td class="resultLabel">实发金额</td>
				    <td class="resultLabel">人均金额</td>
				  </tr>
				  </tr>
				  <%

					    if (result!=null) {
					      int rowLength = result.length;
					      int colLength = result[0].length;
					    	for (int i=0; i<result.length-1; i++) {
					      	String primaryKey = result[ i ][ 0 ];
					        for(int j=0;j<result[i].length;j++){
					        	if(result[i][j]==null||result[i][j].equals(""))
					          	result[i][j]="&nbsp";
					        }
                  DecimalFormat nf = new DecimalFormat("#,##0.00");
                  DecimalFormat np2 = new DecimalFormat("#0.00");
                  DecimalFormat np = new DecimalFormat("#0.00%");
					        String rowColor = "rowGray";
          				if (i/2*2==i) rowColor = "rowWhite";

				  %>
				  <tr class="<%=rowColor%>">
				    <td class="normalText"><input type="hidden" name="dept_code" value="<%= result[i][0]%>"><%= result[i][0]%></td>
				    <td class="normalText"><%= result[i][1]%></td>
				    <td class="normalText">
				      <input type="hidden" name="person_amount" value="<%= result[i][2]%>">
				      <%= result[i][2]%>
				    </td>
				    <td class="normalText">
				      <input type="hidden" name="person_bonus" value="<%= result[i][3]%>">
				      <%= nf.format(Double.parseDouble(result[i][3]))%>
				    </td>
				    <td class="normalText">
				      <input type="hidden" name="bonus_scale" value="<%= result[i][4]%>">
				      <%= np.format(Double.parseDouble(result[i][4]))%>
				    </td>

				    <td class="normalText"><input name="check_mark" type="text" value="<%= np2.format(Double.parseDouble(result[i][5]))%>" size="12" maxlength="9"><%="&nbsp;%"%></td>


				    <td class="normalText"><%= result[i][6]%></td>
				    <td class="normalText">
				      <input type="hidden" name="cost_amount" value="<%= result[i][7]%>">
				      <a href="assignDeptMana.jspviewhigh?subFunction=costdetail&dept_code=<%= result[i][0]%>&year_month=<%= request.getParameter("year_month")%>&dept_name=<%= result[i][1]%>&total_mount=<%=mf.format(Double.parseDouble(result[i][7]))%>">
				        <%= nf.format(Double.parseDouble(result[i][7]))%>
				      </a>
				    </td>
				    <td class="normalText">
				      <input type="hidden" name="cost_contorl" value="<%= result[i][8]%>">
				      <%= nf.format(Double.parseDouble(result[i][8]))%>
				    </td>
				    <td class="normalText">
				      <input type="hidden" name="cost_scale" value="<%= result[i][9]%>">
				      <%= np.format(Double.parseDouble(result[i][9]))%>
				    </td>
				    <td class="normalText">
				      <%= nf.format(Double.parseDouble(result[i][10]))%>
				    </td>
				    <td class="normalText"><input name="control_amount" type="text" value="<%= np2.format(Double.parseDouble(result[i][11]))%>" size="12" maxlength="9"></td>
				    <td class="normalText"><%= nf.format(Double.parseDouble(result[i][12]))%></td>
				    <td class="normalText"><%= nf.format(Double.parseDouble(result[i][13]))%></td>
				  </tr>
				  <%
				        }DecimalFormat nf = new DecimalFormat("#,##0.00");
				  %>
				  <tr class="rowWhite">
				    <td class="normalText">总计：</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">
				      <%= nf.format(Double.parseDouble(result[result.length-1][10]))%>
				    </td>
				    <td class="normalText"><%= nf.format(Double.parseDouble(result[result.length-1][11]))%></td>
				    <td class="normalText"><%= nf.format(Double.parseDouble(result[result.length-1][12]))%></td>
				    <td class="normalText">&nbsp;</td>
				  </tr>
					<%


					  }
				  %>
				</html:table>
    	</td>
    </tr>
</html:table>
<input type="hidden" name="subFunction">
<input type="hidden" name="ActionType">
</form>

</html:html>
