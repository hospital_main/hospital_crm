<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/dept/mana/finishAssignDeptMana.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO,
                 java.util.*,java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>



<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function group() {
    if (template.year_month.value == "") {
      alert("请选择年月！");

      return false
    }
	  template.subFunction.value = "calculateAssignDeptMana";
    show_wait();
    template.submit();
    return true;
	}
	function save() {
	  template.subFunction.value = "saveAssignDeptMana";
    show_wait();
    template.submit();
    return true;
	}
</script>
 <html:html clazz="main">
<!-- 信息提示栏 -->
	  <html:message/>

<!-- 标题栏 -->
<%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("管理科室奖金分配")%>


<form name="template" method="post" action="assignDeptMana.jspviewhigh">

<!-- 简单信息 -->
<table  width="100%" cellspacing="2" border="0" >
	<tr>
  	<td nowrap class="signText" >核算月:</td>
    <td nowrap class="normalText" colspan='3'>
    	<!-- 年月组件 -->
      <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
    </td>
    <td><button class="pageBtn" onclick="group();" >查询</button></td>
	</tr>
</table>

<br>

<%
	String[][] result = (String[][])request.getAttribute("result");
  DecimalFormat pf2=new DecimalFormat("#0.00");
  DecimalFormat pf=new DecimalFormat("#0.00%");
  DecimalFormat mf=new DecimalFormat("#,##0.00");
%>


<!-- 复杂信息 -->
<html:table clazz="complex">


		<!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
				  <tr>
				    <td class="resultLabel">科室编号</td>
				    <td class="resultLabel">科室名称</td>
				    <td class="resultLabel">人数</td>
				    <td class="resultLabel">人均奖金</td>
				    <td class="resultLabel">比例数</td>
				    <td class="resultLabel">考核分数</td>
				    <td class="resultLabel">金额</td>

				    <td class="resultLabel">成本金额</td>
				    <td class="resultLabel">成本调控金额</td>
				    <td class="resultLabel">调控比例</td>
				    <td class="resultLabel">应发金额</td>
				    <td class="resultLabel">调控金额</td>
				    <td class="resultLabel">实发金额</td>
				    <td class="resultLabel">人均金额</td>
				  </tr>
				  </tr>
				  <%

					    if (result!=null) {
					      int rowLength = result.length;
					      int colLength = result[0].length;
					    	for (int i=0; i<result.length-1; i++) {
					      	String primaryKey = result[ i ][ 0 ];
					        for(int j=0;j<result[i].length;j++){
					        	if(result[i][j]==null||result[i][j].equals(""))
					          	result[i][j]="&nbsp";
					        }
					        String rowColor = "rowGray";
          				if (i/2*2==i) rowColor = "rowWhite";

				  %>
				  <tr class="<%=rowColor%>">
				    <td class="normalText"><input type="hidden" name="dept_code" value="<%= result[i][0]%>"><%= result[i][0]%></td>
				    <td class="normalText"><%= result[i][1]%></td>
				    <td class="normalText">
				      <input type="hidden" name="person_amount" value="<%= result[i][2]%>">
				      <%= result[i][2]%>
				    </td>
				    <td class="normalText">
				      <input type="hidden" name="person_bonus" value="<%= result[i][3]%>">
				      <%= mf.format(Double.parseDouble(result[i][3]))%>
				    </td>
				    <td class="normalText">
				      <input type="hidden" name="bonus_scale" value="<%= result[i][4]%>">
				      <%= pf.format(Double.parseDouble(result[i][4]))%>
				    </td>
				    <td class="normalText"><input name="check_mark" type="text" value="<%= pf2.format(Double.parseDouble(result[i][5]))%>%" size="12" maxlength="9" disabled="true"></td>

				    <td class="normalText"><%= result[i][6]%></td>
				    <td class="normalText">
				      <input type="hidden" name="cost_amount" value="<%= result[i][7]%>">
				      <a href="assignDeptMana.jspviewhigh?subFunction=costdetail&dept_code=<%= result[i][0]%>&year_month=<%= request.getParameter("year_month")%>&dept_name=<%= result[i][1]%>&total_mount=<%=mf.format(Double.parseDouble(result[i][7]))%>">
				        <%= mf.format(Double.parseDouble(result[i][7]))%>
				      </a>
				    </td>
				    <td class="normalText">
				      <input type="hidden" name="cost_contorl" value="<%= result[i][8]%>">
				      <%= mf.format(Double.parseDouble(result[i][8]))%>
				    </td>
				    <td class="normalText">
				      <input type="hidden" name="cost_scale" value="<%= result[i][9]%>">
				      <%= pf.format(Double.parseDouble(result[i][9]))%>
				    </td>
				    <td class="normalText">
				      <%= mf.format(Double.parseDouble(result[i][10]))%>
				    </td>
				    <td class="normalText"><input name="control_amount" type="text" value="<%= mf.format(Double.parseDouble(result[i][11]))%>" size="12" maxlength="9" disabled="true"></td>
				    <td class="normalText"><%= mf.format(Double.parseDouble(result[i][12]))%></td>
				    <td class="normalText"><%= mf.format(Double.parseDouble(result[i][13]))%></td>
				  </tr>
				  <%
				        }
				  %>
				  <tr class="rowWhite">
				    <td class="normalText">总计：</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">
				      <%= mf.format(Double.parseDouble(result[result.length-1][10]))%>
				    </td>
				    <td class="normalText"><%= mf.format(Double.parseDouble(result[result.length-1][11]))%></td>
				    <td class="normalText"><%= mf.format(Double.parseDouble(result[result.length-1][12]))%></td>
				    <td class="normalText">&nbsp;</td>
				  </tr>
					<%


					  }
				  %>
				</html:table>
    	</td>
    </tr>

	<!-- 操作 -->

</html:table>
<input type="hidden" name="subFunction">
</form>

</html:html>
