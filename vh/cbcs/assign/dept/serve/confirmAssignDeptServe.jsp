<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/dept/serve/confirmAssignDeptServe.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript">
  function group() {
    if (template.year_month.value == "") {
      alert("请选择年月！");
      return false
    }
	  template.subFunction.value = "calculateAssignDeptServe";
    show_wait();
    template.submit();
    return true;
	}
</script>


<Script Language="JavaScript" src="javascript/check.js" ></Script>

<html:html clazz="main">
<!-- 信息提示栏 -->
	  <html:message/>

<!-- 标题栏 -->
 <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("管理科室奖金分配")%>


<form name="template" method="post" action="assignDeptServe.jspviewhigh">

<!-- 简单信息 -->
<table  width="100%" cellspacing="2" border="0" >
	<tr>
  	<td nowrap class="signText" >核算月:</td>
    <td nowrap class="normalText" colspan='3'>
    	<!-- 年月组件 -->
      <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
    </td>
    <td><button class="pageBtn" onclick="group();" >查询</button></td>
	</tr>
</table>
<input type="hidden" name="subFunction">
</form>

<Script Language="JavaScript">
  confirmStr();
  function confirmStr() {
    var flag = confirm("确认要进行本月份的科室奖金分配吗？");
    if (!flag) {
      template.subFunction.value = "goListAssignDeptServe";
      template.submit();
      return true;
    } else {
      template.subFunction.value="confirmAssignDeptServe";
      template.submit();
      return true;
    }
  }
</script>
</html:html>
