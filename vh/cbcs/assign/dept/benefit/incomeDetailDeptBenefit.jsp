<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/dept/benefit/incomeDetailDeptBenefit.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO,java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function group() {
    if (template.year_month.value == "") {
      alert("请选择年月！");
      return false
    }
	  template.subFunction.value = "calculateAssignDeptBenefit";
    show_wait();
    template.submit();
    return true;
	}
</script>
<html:html clazz="main">
<br>
<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("收入明细表")%>

<%
  String[][] result = (String[][])request.getAttribute("result");
  DecimalFormat mf=new DecimalFormat("#,##0.00");
%>
<form name="template" method="post" action="assignDeptBenefit.jspviewhigh">

	<table width=100%>
	  <tr>
	    <td colspan="4" align="right"><button class="pageBtn" onclick="history.go(-1)">返回</button>
	    <!--<img src="images/return1.gif" style="cursor:hand" onclick="history.go(-1)">--></td>
	  </tr>
  </table>

<!-- 简单信息 -->
<table  width="100%" cellspacing="2" border="0" >
	<tr>
  	<td nowrap class="signText" >科室编码:</td>
    <td nowrap class="normalText"><%= request.getParameter("dept_code")%></td>
    <td nowrap class="signText">科室名称:</td>
    <td nowrap class="normalText"><%= request.getParameter("dept_name")%></td>
    <td nowrap class="signText">期间年月:</td>
    <td nowrap class="normalText"><%= request.getParameter("year_month")%></td>
    <td nowrap class="signText" >合计金额:</td>
    <td nowrap class="normalText"><%= mf.format(Double.parseDouble(request.getParameter("total_mount")))%></td>
	</tr>
</table>

<!-- 复杂信息 -->
<html:table clazz="complex">

		<!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
				  <tr>
				    <td class="resultLabel">收费类别编码</td>
				    <td class="resultLabel">收费类别名称</td>
				    <td class="resultLabel">收入项目</td>
				    <td class="resultLabel">金额</td>
				  </tr>
				  <%
					  if (result!=null) {
					    for (int i=0; i<result.length; i++) {
					      String primaryKey = result[ i ][ 0 ];
					      for(int j=0;j<result[i].length;j++){
					        if(result[i][j]==null||result[i][j].equals(""))
					          result[i][j]="&nbsp;";
					      }

					      String rowColor = "rowGray";
          		  if (i/2*2==i) rowColor = "rowWhite";
					      if (i != result.length-1)   {

				  %>
				  <tr class="<%=rowColor%>">
				    <td class="normalText"><%= result[i][0]%></td>
				    <td class="normalText"><%= result[i][1]%></td>
				    <td class="normalText"><%= result[i][2]%></td>
				    <td class="normalText"><%= mf.format(Double.parseDouble(result[i][3]))%></td>
				  </tr>
				  <%
				      }
					  }
				  %>
				  <tr class="rowWhite">
				    <td class="normalText">合 计</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText"><%= mf.format(Double.parseDouble(result[result.length-1][3]))%></td>
				  </tr>
				<%
				  }
				%>
				</html:table>
    	</td>
    </tr>

</html:table>
<input type="hidden" name="subFunction">
</form>
</html:html>



