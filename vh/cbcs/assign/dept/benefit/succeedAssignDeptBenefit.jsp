<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/dept/benefit/succeedAssignDeptBenefit.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO,
                 java.util.*,java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>



<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function group() {
    if (template.year_month.value == "") {
      alert("请选择年月！");
      return false
    }
	  template.subFunction.value = "calculateAssignDeptBenefit";
	  show_wait();
    template.submit();
    return true;
	}
	function save() {
         for(var i=0; i<template.elements.length; i++){
      if(template.elements[i].name=="check_mark"){
        if (!checkMark(template.elements[i])) return false;
      }else if(template.elements[i].name=="control_mark"){
       switch(isDouble(template.elements[i],8,2))
      {
        case 0 : alert('调控金额必须为数字型'); return;
        case 1 : alert('调控金额整数部分不能高于8个字符'); return;
        case 2 : alert('调控金额没有整数部分'); return;
        case 3 : alert('调控金额小数部分不能高于2个字符'); return;
      } }
    }

	  template.subFunction.value = "saveAssignDeptBenefit";
	  template.finishFlag.value = 'false';
	  show_wait();
    template.submit();
    return true;
	}
	function finish() {
	  template.subFunction.value = "finishAssignDeptBenefit";
	  show_wait();
	  template.submit();
    return true;
	}
</script>

<html:html>
<br>
	<html:message/>

	<html:title clazz='module'>经营科室奖金分配</html:title>


<form name="template" method="post" action="assignDeptBenefit.jspviewhigh">

<!-- 简单信息 -->
<table  width="100%" cellspacing="2" border="0" >
	<tr>
  	<td nowrap class="signText" >核算月:</td>
    <td nowrap class="normalText" colspan='3'>
    	<!-- 年月组件 -->
      <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
    </td>
    <td><button class="pageBtn" onclick="group();" >查询</button></td>
	</tr>
</table>

<br>

<%
  DecimalFormat percent=new DecimalFormat("#0.00");
  DecimalFormat showMoney=new DecimalFormat("#,##0.00"), saveMoney=percent;


  String[][] result = (String[][])request.getAttribute("baseRO");
  Map map = (Map)request.getAttribute("map");
  Object[] bonus = new Object[]{};
  if (map!=null)
  	bonus = map.keySet().toArray();
%>

<!-- 复杂信息 -->
<html:table clazz="complex">

	 	<!-- 操作 -->
  <tr>
   	<td>
   	<button class="pageBtn" onclick="reset();" >重置</button> 
   	<!-- <img src="images/reset.gif" class="mouse" onclick="reset();" />-->
   	  <% if (result != null && result.length >0) {%>
   	  <button class="pageBtn" onclick="save();">保存</button> 
   	  <!--  <img src="images/save.gif" class="mouse" onclick="save();" />-->
   	    <img src="images/finish.gif" class='mouse' onclick='finish();' />
   	  <% } %>
   	</td>
  </tr>
  		<!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
				  <tr>
				    <td class="resultLabel">科室编号</td>
				    <td class="resultLabel">科室名称</td>
				    <td class="resultLabel">人数</td>
				    <td class="resultLabel">直接收入</td>
				    <td class="resultLabel">成本</td>
				    <td class="resultLabel">比例</td>
				    <td class="resultLabel">考核分数</td>

				  <%
				    for (int i=0; i<bonus.length; i++) {

				  %>
				    <td class="resultLabel"><%= map.get(bonus[i])%>&nbsp;</td>
				  <%
				    }
				  %>

				    <td class="resultLabel">应发金额</td>
				    <td class="resultLabel">调控金额</td>
				    <td class="resultLabel">实发金额</td>
				  </tr>
				  <%

					    if (result!=null) {
					      int rowLength = result.length;
					      int colLength = result[0].length;
					    	for (int i=0; i<result.length-1; i++) {
					      	String primaryKey = result[ i ][ 0 ];
					        for(int j=0;j<result[i].length;j++){
					        	if(result[i][j]==null||result[i][j].equals(""))
					          	result[i][j]="0";
					        }
                  String rowColor = "rowGray";
          				if (i/2*2==i) rowColor = "rowWhite";

				  %>
				  <tr class="<%=rowColor%>">
				    <td class="normalText"><input type="hidden" name="dept_code" value="<%= result[i][0]%>"><%= result[i][0]%></td>
				    <td class="normalText"><%= result[i][1]%></td>
				    <td class="normalText"><%= result[i][2]%></td>
				    <td class="normalText">
				      <input type="hidden" name="income_amount" value="<%= result[i][3]%>">
				      <a href="assignDeptBenefit.jspviewhigh?subFunction=incomedetail&dept_code=<%=result[i][0]%>&year_month=<%= request.getParameter("year_month")%>&dept_name=<%= result[i][1]%>&total_mount=<%=percent.format(Double.parseDouble(result[i][3]))%>"><%=showMoney.format(Double.parseDouble(result[i][3]))%></a>
				    </td>
				    <td class="normalText">
				      <input type="hidden" name="cost_amount" value="<%= result[i][4]%>">
				      <a href="assignDeptBenefit.jspviewhigh?subFunction=costdetail&dept_code=<%=result[i][0]%>&year_month=<%= request.getParameter("year_month")%>&dept_name=<%= result[i][2]%>&total_mount=<%= result[i][4].substring(0, result[i][4].length()-2)%>"><%= result[i][4].substring(0, result[i][4].length()-2)%></a>
				    </td>
				    <td class="normalText"><input type="hidden" name="bonus_scale" value="<%= result[i][5]%>"><%= percent.format(Double.parseDouble(result[i][5]))%>%</td>
				    <td class="normalText"><input name="check_mark" type="text" value="<%= percent.format(Double.parseDouble(result[i][6]))%>" size="12" maxlength="9"/><%="&nbsp;%"%></td>

				  <%
				    for (int j=0; j<bonus.length; j++) {
				      String value = result[i][7+j];
              String temp = value;
				      if (!value.equals("0")) {
				        temp = (String)showMoney.format(Double.parseDouble(value));
				      }
				  %>
				    <td class="normalText"><%= temp%><input type="hidden" name="jiangjin" value="<%= value%>"></td>
				  <%
				    }
				  %>

				    <td class="normalText"><%= showMoney.format(Double.parseDouble(result[i][7+bonus.length]))%></td>
				    <td class="normalText">
				      <input name="control_mark" type="text" value="<%= saveMoney.format(Double.parseDouble(result[i][8+bonus.length]))%>" size="12" maxlength="9">
				    </td>
				    <td class="normalText"><%= showMoney.format(Double.parseDouble(result[i][9+bonus.length]))%></td>
				  </tr>
				  <%
					  		}

					%>

					<tr class="rowWhite">
				    <td class="normalText">总计：</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>

				  <%
				    for (int j=0; j<bonus.length; j++) {
				  %>
				    <td class="normalText">&nbsp;</td>
				  <%
				    }
				  %>

				    <td class="normalText">
				      <%= showMoney.format(Double.parseDouble(result[rowLength-1][colLength-3]))%>
				    </td>
				    <td class="normalText">
				      <%= showMoney.format(Double.parseDouble(result[rowLength-1][colLength-2]))%>
				    </td>
				    <td class="normalText">
				      <%= showMoney.format(Double.parseDouble(result[rowLength-1][colLength-1]))%>
				    </td>
				  </tr>

					<%

					  }
				  %>
				</html:table>
    	</td>
    </tr>
</html:table>
<input type="hidden" name="subFunction">
<input type="hidden" name="ActionType">
<input type="hidden" name="finishFlag" value='true'>
</form>

</html:html>
