<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/dept/benefit/finishAssignDeptBenefit.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO,
                 java.util.*,java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function group() {
    if (template.year_month.value == "") {
      alert("请选择年月！");
      return false
    }
	  template.subFunction.value = "calculateAssignDeptBenefit";
	  show_wait();
    template.submit();
    return true;
	}
	function save() {
	  template.subFunction.value = "saveAssignDeptBenefit";
	  show_wait();
    template.submit();
    return true;
	}
</script>
<html:html clazz="main">
<br>
<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
 <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("经营科室奖金分配")%>


<form name="template" method="post" action="assignDeptBenefit.jspviewhigh">

<!-- 简单信息 -->
<table  width="100%" cellspacing="2" border="0" >
	<tr>
  	<td nowrap class="signText" >核算月:</td>
    <td nowrap class="normalText" colspan='3'>
    	<!-- 年月组件 -->
      <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
    </td>
    <td> <button class="pageBtn" onclick="group();" >查询</button></td>
	</tr>
</table>

<br>

<%
  DecimalFormat percentFormat = new DecimalFormat("#0.00");
  DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
  DecimalFormat moneyFormatt = new DecimalFormat("#0.00");
  DecimalFormat mf = new DecimalFormat("#,##0.00");

	String[][] result = (String[][])request.getAttribute("baseRO");
  Map map = (Map)request.getAttribute("map");
  Object[] bonus = map.keySet().toArray();
%>

<!-- 复杂信息 -->
<html:table clazz="complex">

		<!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
				  <tr>
				    <td class="resultLabel">科室编号</td>
				    <td class="resultLabel">科室名称</td>
				    <td class="resultLabel">人数</td>
				    <td class="resultLabel">直接收入</td>
				    <td class="resultLabel">成本</td>
				    <td class="resultLabel">比例</td>
				    <td class="resultLabel">考核分数</td>

				  <%
				    for (int i=0; i<bonus.length; i++) {
				  %>
				    <td class="resultLabel"><%= map.get(bonus[i])%>&nbsp;</td>
				  <%
				    }
				  %>

				    <td class="resultLabel">应发金额</td>
				    <td class="resultLabel">调控金额</td>
				    <td class="resultLabel">实发金额</td>
				  </tr>
				  <%

					    if (result!=null) {
					      int rowLength = result.length;
					      int colLength = result[0].length;
					    	for (int i=0; i<result.length-1; i++) {
					      	String primaryKey = result[ i ][ 0 ];
					        for(int j=0;j<result[i].length;j++){
					        	if(result[i][j]==null||result[i][j].equals(""))
					          	result[i][j]="&nbsp";
					        }
					        String rowColor = "rowGray";
          				if (i/2*2==i) rowColor = "rowWhite";

				  %>
				  <tr class="<%=rowColor%>">
				    <td class="normalText"><input type="hidden" name="dept_code" value="<%= result[i][0]%>"><%= result[i][0]%></td>
				    <td class="normalText"><%= result[i][1]%></td>
				    <td class="normalText"><%= result[i][2]%></td>
				    <td class="normalText">
				      <input type="hidden" name="income_amount" value="<%= result[i][3]%>">
				      <a href="assignDeptBenefit.jspviewhigh?subFunction=incomedetail&dept_code=<%= result[i][0]%>&year_month=<%= request.getParameter("year_month")%>&dept_name=<%= result[i][1]%>&total_mount=<%=percentFormat.format(Double.parseDouble(result[i][3]))%>"><%= moneyFormat.format(Double.parseDouble(result[i][3]))%></a>
				    </td>
				    <td class="normalText">
				      <input type="hidden" name="cost_amount" value="<%= result[i][4]%>">
				      <a href="assignDeptBenefit.jspviewhigh?subFunction=costdetail&dept_code=<%= result[i][0]%>&year_month=<%= request.getParameter("year_month")%>&dept_name=<%= result[i][1]%>&total_mount=<%=percentFormat.format(Double.parseDouble(result[i][4]))%>"><%= moneyFormat.format(Double.parseDouble(result[i][4]))%></a>
				    </td>
				    <td class="normalText"><input type="hidden" name="bonus_scale" value="<%= result[i][5]%>"><%= percentFormat.format(Double.parseDouble(result[i][5]))%>%</td>
				    <td class="normalText"><input name="check_mark" type="text" value="<%=percentFormat.format(Double.parseDouble( result[i][6]))%>" size="12" maxlength="9" disabled="true"/><%="&nbsp;%"%></td>

				  <%
				    for (int j=0; j<bonus.length; j++) {
				      String value = result[i][7+j];
              String temp = value;
				      if (!value.equals("&nbsp")) {
				        temp = (String)moneyFormat.format(Double.parseDouble(value));
				      }
				  %>
				    <td class="normalText"><%= temp%></td>
				  <%
				    }
				  %>

				    <td class="normalText"><%=moneyFormat.format(Double.parseDouble(result[i][7+bonus.length]))%></td>
				    <td class="normalText">
				      <input name="control_mark" type="text" value="<%= moneyFormatt.format(Double.parseDouble(result[i][8+bonus.length]))%>" size="12" maxlength="9" disabled="true">
				    </td>
				    <td class="normalText"><%= moneyFormat.format(Double.parseDouble(result[i][9+bonus.length]))%></td>
				  </tr>
				  <%
					  		}

					%>

					<tr class="rowWhite">
				    <td class="normalText">总计：</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>
				    <td class="normalText">&nbsp;</td>

				  <%
				    for (int j=0; j<bonus.length; j++) {
				  %>
				    <td class="normalText">&nbsp;</td>
				  <%
				    }
				  %>

				    <td class="normalText">
				      <%= mf.format(Double.parseDouble(result[rowLength-1][colLength-3]))%>
				    </td>
				    <td class="normalText">
				      <%= mf.format(Double.parseDouble(result[rowLength-1][colLength-2]))%>
				    </td>
				    <td class="normalText">
				      <%= mf.format(Double.parseDouble(result[rowLength-1][colLength-1]))%>
				    </td>
				  </tr>

					<%
					    }
				  %>
				</html:table>
    	</td>
    </tr>

	<!-- 操作 -->

</html:table>
<input type="hidden" name="subFunction">
</form>
</html:html>
