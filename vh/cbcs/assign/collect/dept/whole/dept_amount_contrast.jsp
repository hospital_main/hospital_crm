<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/collect/dept/whole/dept_amount_contrast.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
function find() {
    if(template.year_month_from.value=='')
     {alert("请选择年月")
      return false
     }
    if(template.year_month_to.value=='')
     {alert("请选择年月")
      return false
     }
    if(template.year_month_from.value>template.year_month_to.value)
    {  alert("起始年月不能大于终止年月")
        return false
     }
    template.subFunction.value='findAll';
    template.submit();return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="dept_amount_contrast.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
   <html:title clazz='module'>科室总金额对比</html:title>
  <!-- 查询信息 -->
    <%
      DecimalFormat nf = new DecimalFormat("#,##0.00");

      BaseRO  ro= (BaseRO)request.getAttribute("ro");
      String[] allYearMonth=(String[])request.getAttribute("allYearMonth");
      String[][] result;
      if(ro!=null)
       result =ro.getTableResult();
      else   result=null;
     TableMarge oper = new TableMarge(ro, "return find()");
    %>
  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText">时间范围： </td>
      <td nowrap>
      <%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%>
     </td>
      <td><button class="pageBtn" style='cursor:hand' onclick="return find();" >查询</button></td>
</tr>

  </html:table>
<br>
<html:title clazz='table'>科室奖金总金额对比</html:title>
  <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>
      <html:table clazz="result">
		        <html:tr clazz='label'>
     <% if(allYearMonth!=null&&result!=null){%>
          <td >科室名称</td>

          <% for(int i=0;i<allYearMonth.length;i++)
               {
          %>
          <td ><%=allYearMonth[i]%></td>

        <%}}%>
         </html:tr>

        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText"><%=result[ i ][ 0]%></td>
         <%for(int j=1;j<result[0].length;j++)
            {
          %>
            <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ j]))%></td>
          <%}%>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>
   </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>


