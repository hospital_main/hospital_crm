<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/assign/collect/dept/detail/dept_bonus_collect.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
function find() {
   if(template.year_month.value=='')
     {alert("请选择年月")
      return false
     }
   if(template.kind.value=='')
      {alert("科室类别")
      return false
     }
    template.subFunction.value='findAll';
    template.submit();return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="dept_bonus_collect.jspviewhigh">
  <html:message/>

	  <html:title clazz='module'>科室奖金汇总</html:title>
  <!-- 查询信息 -->
    <%
      DecimalFormat nf = new DecimalFormat("#,##0.00");
      DecimalFormat percentFormat = new DecimalFormat("#0.00%");
      BaseRO  ro= (BaseRO)request.getAttribute("ro");
      String[][] allName=(String[][])request.getAttribute("allName");
      String[][] result;
      if(ro!=null)
       result =ro.getTableResult();
      else   result=null;
     TableMarge oper = new TableMarge(ro, "return find()");
    %>
  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText">核算月：</td>
      <td nowrap>
       <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
     </td>
     <td nowrap class="signText">科室类别 </td>
     <td>
        <select name="kind">
                         <option value=''></option>
                         <option value='1' <%if(request.getParameter("kind")!=null && request.getParameter("kind").equals("1")) out.print("selected");%> >经营科室</option>
                         <option value='2' <%if(request.getParameter("kind")!=null && request.getParameter("kind").equals("2")) out.print("selected");%>>管理科室</option>
                         <option value='3' <%if(request.getParameter("kind")!=null && request.getParameter("kind").equals("3")) out.print("selected");%>>服务科室</option>
        </select>
     </td>
      <td><button class="pageBtn" style='cursor:hand' onclick="return find();" >查询</button></td>
</tr>

   </html:table>
<br>
<html:title clazz='table'>科室奖金汇总表</html:title>
  <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>
   <%  if ( result != null )
            {
   %>
      <html:table clazz="result">
          <%String kind=request.getParameter("kind");%>
          <%if(kind.equals("1")||kind.equals("3")) {%>
		        <html:tr clazz='label'>
            <td >科室编号</td>
            <td >科室名称</td>
            <td >人数</td>
            <td >直接收入</td>
            <td >成本 </td>
            <td >比例 </td>
            <td >考核分数 </td>
           <%if(allName!=null)
              for(int i=0;i<allName.length;i++)
                { if(!allName[i][0].equals("1"))
           %>
               <td ><%=allName[i][1]%> </td>
           <%     }
           %>
            <td >应发金额 </td>
            <td >调控金额 </td>
            <td >实发金额</td>
        </html:tr>
        <%}%>
          <%if(kind.equals("2")) {%>
              <html:tr clazz='label'>
            <td >科室编号</td>
            <td >科室名称</td>
            <td >人数</td>
            <td >人均奖金</td>
            <td >比例数 </td>
            <td >考核分数 </td>
            <td >金额 </td>
            <td >成本金额 </td>
            <td >成本调控金额 </td>
            <td >调控比例 </td>
            <td >应发金额 </td>
            <td >调控金额 </td>
            <td >实发金额</td>
            <td >人均金额</td>
             </html:tr>
        <%}%>


        <%
           if(kind.equals("1")||kind.equals("3")){
              for (int i = 0; i < result.length; i++ )
              {
          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText"><%=result[ i ][ 0]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 1]%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 2]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 3]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 4]))%></td>
          <td nowrap class="numberText"><%=percentFormat.format(Double.parseDouble(result[ i ][ 5]))%></td>
          <td nowrap class="numberText"><%=percentFormat.format(Double.parseDouble(result[ i ][ 6]))%></td>
         <%for(int j=7;j<result[0].length;j++)
            {
          %>
           <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ j]))%></td>
          <%}%>
        </tr>

        <%
              }
           }

        %>
          <%
           if(kind.equals("2")){
              for (int i = 0; i < result.length; i++ )
              {
          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText"><%=result[ i ][ 0]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 1]%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 2]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 3]))%></td>
          <td nowrap class="numberText"><%=percentFormat.format(Double.parseDouble(result[ i ][ 4]))%></td>
          <td nowrap class="numberText"><%=percentFormat.format(Double.parseDouble(result[ i ][ 5]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 6]))%></td>
           <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 7]))%></td>
           <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 8]))%></td>
           <td nowrap class="numberText"><%=percentFormat.format(Double.parseDouble(result[ i ][ 9]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 10]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 11]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 12]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 13]))%></td>

        </tr>

        <%
              }
           }

        %>
     </html:table>
     <% }
        %>
    </td>
  </tr>

  </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>


