<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
		    <th noWrap="true" width="5%"></th>
		    <th noWrap="true" dataIndex="0">医辅科室</th>
				<th noWrap="true" dataIndex="1">成本</th>
				<th noWrap="true" dataIndex="2">比例</th>
			</tr>	
		</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <xsl:if test="td[1]!= '合计'">
            	<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              		<xsl:attribute name="value" >
                		<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              		</xsl:attribute>
            	</input>
            </xsl:if>
          </td>
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
              	<xsl:when test="position()=1">
	  	            <xsl:value-of select="."/>
	                </xsl:when>
                <xsl:when test="position()=2">
	                <xsl:attribute name='class'>numberText</xsl:attribute>
	  	            <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </xsl:when>
                <xsl:when test="position()=3">
	                <xsl:attribute name='class'>numberText</xsl:attribute>
	  	            <xsl:value-of select="format-number(.,'##0.00%')"/>
	                </xsl:when>
                <xsl:otherwise>
                 
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
		</xsl:template>
</xsl:stylesheet>
