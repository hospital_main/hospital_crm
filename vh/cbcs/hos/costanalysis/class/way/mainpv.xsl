<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;colspan:colcount">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td[position() &gt; 1])'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 3]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:subtitle;colspan:colcount">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td[position() &gt; 1])'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 3]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr >
			<td noWrap="true"  class="mainHead" style="fontsize:subtitle;colspan:colcount;align:right">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td[position() &gt; 1])'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 3]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
			<tr noWrap="true" class="mainHead">
				<xsl:for-each select="/root/tbody/tr[td[3]='项目']/td[ position()!=1 and position()!=2]">
					<xsl:choose>
					  <xsl:when test="position()=1">
			    		<td noWrap="true" rowspan="2"><xsl:value-of select="."/></td>
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		<td noWrap="true"><xsl:value-of select="."/></td>
			    	</xsl:when>
			    	<xsl:when test="(position() mod 2)=1">
			    		<td noWrap="true" colspan="2"><xsl:value-of select="."/></td>
			    		<td noWrap="true" style="display:none"><xsl:value-of select="."/></td>
			    	</xsl:when>              	
          </xsl:choose>
	    	</xsl:for-each>
	    </tr>
	    <tr noWrap="true" class="mainHead">
				<xsl:for-each select="/root/tbody/tr[td[3]='项目']/td[ position()!=1 and position()!=2]">
					<xsl:choose>
					  <xsl:when test="position()=1">
			    		<td noWrap="true" style="display:none"></td>
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		<td noWrap="true">数值</td>
			    	</xsl:when>   
			    	<xsl:when test="(position() mod 2)=1">
			    		<td noWrap="true">数值</td>
			    		<td noWrap="true">百分比</td>
			    	</xsl:when>           	
          </xsl:choose>
	    	</xsl:for-each>
	    </tr>
		</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr[td[3]!='项目']">
        <tr>
          <xsl:for-each select="td[position()!=1 and position()!=2]">
            <xsl:choose>
              <xsl:when test="position()=1">
                <xsl:if test="../td[2]='yes'">
                  <td style="text-align:center"><xsl:value-of select="."/></td>
                </xsl:if>
                <xsl:if test="../td[2]='no'">
                  <td><xsl:value-of select="."/></td>
                </xsl:if>
              </xsl:when>
              <xsl:when test="position()=2">
                <td class="numberText">
                <xsl:attribute name="style">align:right</xsl:attribute>
                <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:when test="(position() mod 2)=1">
                <td class="numberText">
                <xsl:attribute name="style">align:right</xsl:attribute>
                <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <xsl:if test="../td[2]='no'">
                  <td class="numberText">
                  <xsl:attribute name="style">align:right</xsl:attribute>
                  <xsl:value-of select="format-number(.,'#,##0.00%')"/>
                </td>
                </xsl:if>
                <xsl:if test="../td[2]='yes'">
                  <td></td>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
     <tfoot>
		<tr noWrap="true" class="mainHead">
			<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td[position() &gt; 1])'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 2]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr noWrap="true" class="mainHead">
			<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td[position() &gt; 1])'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 2]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
    </tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>
