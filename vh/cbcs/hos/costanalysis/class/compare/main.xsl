	<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<thead>
			<tr class="mainHead" nowrap="true">  	
			<td nowrap="true"  rowspan="2">选择</td>
      <td nowrap="true"  rowspan="2">项目</td>
      <td nowrap="true"  colspan="1">本期</td>
      <td nowrap="true"  colspan="3">预算</td>
      <td nowrap="true"  colspan="3">上期</td>
      <td nowrap="true"  colspan="3">同期</td>
      <td nowrap="true"  colspan="3">院平均</td>
      </tr>
		<tr class="mainHead" nowrap="true">    
			<th noWrap="true"  colspan="1">成本</th>
			<th noWrap="true"  colspan="1">成本</th>
			<th noWrap="true"  colspan="1">差异</th>
			<th noWrap="true"  colspan="1">差异率</th>
			<th noWrap="true"  colspan="1">成本</th>
			<th noWrap="true"  colspan="1">差异</th>
			<th noWrap="true"  colspan="1">差异率</th>
			<th noWrap="true"  colspan="1">成本</th>
			<th noWrap="true"  colspan="1">差异</th>
			<th noWrap="true"  colspan="1">差异率</th>
			<th noWrap="true"  colspan="1">成本</th>
			<th noWrap="true"  colspan="1">差异</th>
			<th noWrap="true"  colspan="1">差异率</th>
		</tr>
	</thead>
	<tbody>
	<xsl:for-each select="/root/tbody/tr[1]/td">
	<xsl:variable name="td_now" select="position() -2"/>
	<xsl:variable name="gy" select="/root/tbody/tr[1]/td[last()-1]"/>
	<xsl:variable name="yf" select="/root/tbody/tr[1]/td[last()]"/>
	<xsl:if test="$td_now>0">
	<xsl:choose>
	
		<xsl:when test="position()=last()">
		
		</xsl:when>
		<xsl:when test="position()=last()-1">
		
		</xsl:when>
	
		<xsl:when test="position()=4 and $gy =0">
		
		</xsl:when>
		
		<xsl:when test="position()=6 and $yf =0">
		
		</xsl:when>
		
		<xsl:otherwise>
	<tr noWrap="true">
		<xsl:choose>
			<xsl:when test="$td_now=1">
				<td align='center'></td>
				<td><xsl:attribute name="bgColor">yellow</xsl:attribute>科室成本分类</td>
			</xsl:when>
			<xsl:when test="$td_now=2">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>公用分摊成本</td>
			</xsl:when>
			<xsl:when test="$td_now=3">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>管理科室成本</td>
			</xsl:when>
			<xsl:when test="$td_now=4">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>医辅科室成本</td>
			</xsl:when>
			<xsl:when test="$td_now=5">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>医技科室成本</td>
			</xsl:when>
			<xsl:when test="$td_now=6">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>直接医疗成本</td>
			</xsl:when>
			<xsl:when test="$td_now=7">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>未纳入成本</td>
			</xsl:when>
			<xsl:when test="$td_now=8">
				<td align='center'></td>
				<td><xsl:attribute name="bgColor">yellow</xsl:attribute>成本习性</td>
			</xsl:when>
			<xsl:when test="$td_now=9">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>固定成本</td>
			</xsl:when>
			<xsl:when test="$td_now=10">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>变动成本</td>
			</xsl:when>
			<xsl:when test="$td_now=11">
				<td align='center'></td>
				<td><xsl:attribute name="bgColor">yellow</xsl:attribute>成本分类</td>
			</xsl:when>
			<xsl:when test="$td_now=12">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>人力成本</td>
			</xsl:when>
			<xsl:when test="$td_now=13">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>离退休人员成本</td>
			</xsl:when>
			<xsl:when test="$td_now=14">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>折旧成本</td>
			</xsl:when>
			<xsl:when test="$td_now=15">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>材料成本</td>
			</xsl:when>
			<xsl:when test="$td_now=16">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>药品成本</td>
			</xsl:when>
			<xsl:when test="$td_now=17">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>其他成本</td>
			</xsl:when>
			<xsl:when test="$td_now=18">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>无形资产摊销</td>
			</xsl:when>
			<xsl:when test="$td_now=19">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>提取医疗风险基金</td>
			</xsl:when>
			<xsl:otherwise>
				<td><xsl:value-of select="$td_now"/></td>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:variable name="less_than" select="td[2]"/>
			<td class="numberText"><xsl:value-of select="format-number(td[$td_now+2],'#,##0.00')"/></td>
			<xsl:if test="position()>1">
				<xsl:variable name="td_compare" select="../tr[1]/td[$td_now+2] -td[$td_now+2] "/>
				<xsl:variable name="td_compare2" select="$td_compare div td[$td_now+2]"/>
				<td class="numberText"><xsl:value-of select="format-number($td_compare,'#,##0.00')"/></td>
				<xsl:element name="td">
						<xsl:if test="td[$td_now+2]=0">
							<xsl:attribute name="class">numberText<xsl:if test="$less_than &lt; 0">Warnning</xsl:if></xsl:attribute>
						<xsl:value-of select="format-number(0,'#,##0.00%')" />
					</xsl:if>
					<xsl:if test="td[$td_now+2]!=0">
						<xsl:attribute name="class">numberText<xsl:if test="$less_than &lt; $td_compare2">Warnning</xsl:if></xsl:attribute>
						<xsl:value-of select="format-number($td_compare2,'#,##0.00%')"/>
					</xsl:if>
					
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
	</tr>
	</xsl:otherwise>
	</xsl:choose>
	</xsl:if>
	</xsl:for-each>
   </tbody>
  </xsl:template>
</xsl:stylesheet>

