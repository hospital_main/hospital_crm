<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">  
			<th noWrap="true" rowspan="2">选择</th>
			<th noWrap="true" rowspan="2">项目</th>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:if test="position()=1">
					<th noWrap="true" colspan="1"><xsl:value-of select="td[2]"/></th>
				</xsl:if>
				<xsl:if test="position()>1">
					<th noWrap="true" colspan="3"><xsl:value-of select="td[2]"/></th>
				</xsl:if>
			</xsl:for-each>

		</tr>
		<tr noWrap="true" class="mainHead">  
			<xsl:for-each select="/root/tbody/tr">
				<th noWrap="true" colspan="1">收入</th>
				<xsl:if test="position()>1">
					<th noWrap="true" colspan="1">差异</th>
					<th noWrap="true" colspan="1">差异率</th>
				</xsl:if>
			</xsl:for-each>
		</tr>
	</thead>
	<tbody>
	<xsl:for-each select="/root/tbody/tr[1]/td">
	<xsl:variable name="td_now" select="position() -2"/>
	<xsl:if test="$td_now>0">
	<tr noWrap="true">
		<xsl:choose>
			<xsl:when test="$td_now=1">
				<td align='center'></td>
				<td>科室成本分类</td>
			</xsl:when>
			<xsl:when test="$td_now=2">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>公用分摊成本</td>
			</xsl:when>
			<xsl:when test="$td_now=3">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>管理科室成本</td>
			</xsl:when>
			<xsl:when test="$td_now=4">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>医辅科室成本</td>
			</xsl:when>
			<xsl:when test="$td_now=5">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>医技科室成本</td>
			</xsl:when>
			<xsl:when test="$td_now=6">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>直接医疗成本</td>
			</xsl:when>
			<xsl:when test="$td_now=7">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>未纳入成本</td>
			</xsl:when>
			<xsl:when test="$td_now=8">
				<td align='center'></td>
				<td>成本习性</td>
			</xsl:when>
			<xsl:when test="$td_now=9">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>固定成本</td>
			</xsl:when>
			<xsl:when test="$td_now=10">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>变动成本</td>
			</xsl:when>
			<xsl:when test="$td_now=11">
				<td align='center'></td>
				<td>成本分类</td>
			</xsl:when>
			<xsl:when test="$td_now=12">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>人力成本</td>
			</xsl:when>
			<xsl:when test="$td_now=13">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>离退休人员成本</td>
			</xsl:when>
			<xsl:when test="$td_now=14">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>折旧成本</td>
			</xsl:when>
			<xsl:when test="$td_now=15">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>材料成本</td>
			</xsl:when>
			<xsl:when test="$td_now=16">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>药品成本</td>
			</xsl:when>
			<xsl:when test="$td_now=17">
				<td align='center'><input type='checkbox' TABINDEX='-1' style='font-size:8px;'></input></td>
				<td>其他成本</td>
			</xsl:when>
			<xsl:otherwise>
				<td><xsl:value-of select="$td_now"/></td>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:variable name="less_than" select="td[2]"/>
			<td class="numberText"><xsl:value-of select="format-number(td[$td_now+2],'#,##0.00')"/></td>
			<xsl:if test="position()>1">
				<xsl:variable name="td_compare" select="td[$td_now+2] - ../tr[1]/td[$td_now+2]"/>
				<xsl:variable name="td_compare2" select="$td_compare div ../tr[1]/td[$td_now+2]"/>
				<td class="numberText"><xsl:value-of select="format-number($td_compare,'#,##0.00')"/></td>
				<xsl:element name="td">
					<xsl:attribute name="class">numberText</xsl:attribute>
					<xsl:if test="$less_than>$td_compare2*100">
						<xsl:attribute name="style">color:red</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="format-number($td_compare2,'#,##0.00%')"/>
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
	</tr>
	</xsl:if>
	</xsl:for-each>

    </tbody>
  </xsl:template>
</xsl:stylesheet>

