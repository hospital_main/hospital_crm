<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">  
			<th noWrap="true">选择</th>
			<th noWrap="true">科室名称</th>
			<th noWrap="true">科室名称</th>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
		<tr>
			<td align="center">
			<INPUT TYPE="checkbox" NAME="sel"/>
			</td>
			<xsl:for-each select="td">
				<td><xsl:value-of select="."/></td>
			</xsl:for-each>
		</tr>
		</xsl:for-each>

	</tbody>
	</xsl:template>
</xsl:stylesheet>

