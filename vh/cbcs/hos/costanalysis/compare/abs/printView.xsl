<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/costanalysis/compare/abs/printView.xsl,v 1.1 2012/03/12 01:58:12 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:12 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<colgroup>		       
	  	   <col style = 'width:100m'/>
	  		<xsl:for-each select="/root/activeHead/cbcsHosCostAnalysisCompareAbs_Select_head_1/th">
	      <col style = 'width:100m'/>
	      </xsl:for-each>
		</colgroup>
		<thead>
			<tr noWrap="true" class="mainHead">  
			<th noWrap="true">��Ŀ</th>
		 		<xsl:for-each select="/root/activeHead/cbcsHosCostAnalysisCompareAbs_Select_head_1/th">
	            <th noWrap="true"><xsl:value-of select="."/></th>
	         </xsl:for-each>
	    </tr>
	    </thead>
	    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>        
					<xsl:for-each select="td[position()!=1]">
						<xsl:if test="position()= 1">
							<td><xsl:value-of select="."/></td>
						</xsl:if>  
						<xsl:if test="position()>=2">
							<td>
								<xsl:attribute name='class'>numberText</xsl:attribute>
								<xsl:value-of select="format-number(.,'#0.00')"/>
							</td>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
