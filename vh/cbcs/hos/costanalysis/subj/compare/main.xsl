<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/costanalysis/subj/compare/main.xsl,v 1.1 2012/03/12 01:58:12 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:12 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<thead>
			<tr class="mainHead" nowrap="true">  	
			<td nowrap="true"  rowspan="2">选择</td>
			<th nowrap="true"  rowspan="2">成本项目</th>
			<th nowrap="true"  colspan="1">本期</th>
			<th nowrap="true"  colspan="3">预算</th>
			<th nowrap="true"  colspan="3">上期</th>
			<th nowrap="true"  colspan="3">同期</th>
			<th nowrap="true"  colspan="3">院平均</th>
		</tr>
			<tr class="mainHead" nowrap="true">  	
			<th nowrap="true" >成本额</th>
			<th nowrap="true" >成本额</th>
			<th nowrap="true" >差异</th>
			<th nowrap="true" >差异率</th>
			<th nowrap="true" >成本额</th>
			<th nowrap="true" >差异</th>
			<th nowrap="true" >差异率</th>
			<th nowrap="true" >成本额</th>
			<th nowrap="true" >差异</th>
			<th nowrap="true" >差异率</th>
			<th nowrap="true" >成本额</th>
			<th nowrap="true" >差异</th>
			<th nowrap="true" >差异率</th>
		</tr>
	</thead>
	<tbody>
	<xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center' style='display:block'>  
              <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
                <xsl:attribute name="value" >
                  <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
                </xsl:attribute>
              </input>
       		</td>

          <xsl:for-each select="td[position()!=15]">
              <xsl:choose>
                <xsl:when test="position()=1">
	          		<td nowrap="true">
	  	            	<xsl:value-of select="."/>
            		</td>
                </xsl:when>
                
				<xsl:when test="position()>=2 and position()!=15">
            		<td nowrap="true">
						
							<xsl:attribute name="class">numberText</xsl:attribute>
							<xsl:if test="position() = 2 or ((position()-2) mod 3) != 0">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</xsl:if>
							<xsl:if test="position() != 2 and ((position()-2) mod 3) = 0">
							<xsl:if test=". &gt; ../td[15]">
								<xsl:attribute name="style">font-weight:bold;color:orange</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="format-number(.,'#,##0.00%')"/>
						</xsl:if>
						
            		</td>
				</xsl:when>
				<xsl:when test="position()>=15">
        			<td style="display:none">
						<xsl:value-of select="format-number(.,'#,##0.00')"/>
        			</td>
				</xsl:when>
                <xsl:otherwise>
            		<td nowrap="true">
                  		<xsl:value-of select="."/>
            		</td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>