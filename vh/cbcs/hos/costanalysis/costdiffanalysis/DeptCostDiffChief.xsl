<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:vh="http://www.w3.org/1999/XSL/Transforms">
	<xsl:template match="/">
	<thead>
		<!--医疗技术科室收益排名表-->
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" rowspan="3" class="mainHead" >科室名称</th>
			<th noWrap="true" rowspan="2" colspan="2" class="mainHead" >收入</th>
			<th noWrap="true" colspan="6" class="mainHead" >成本</th>
			<th noWrap="true" rowspan="2" colspan="2" class="mainHead" >收益</th>
			<th noWrap="true" rowspan="2" colspan="2" class="mainHead" >工作量</th>
			<th noWrap="true" style="display:none"></th>	
			<th noWrap="true" style="display:none"></th>
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" colspan="2" class="mainHead" >直接成本</th>
			<th noWrap="true" colspan="2" class="mainHead" >间接成本</th>
			<th noWrap="true" colspan="2" class="mainHead" >合计</th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>		
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
		</tr>
	</thead>
	<tbody>
	<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">          
            <xsl:choose>                         
              <xsl:when test="position()!=1 and position()!=14 and position()!=15 and position()!=2 and position()!=3 and position()!=4 and position()!=5 and position()!=6 and position()!=7">
             	  <td align="right" class="moneyCol">
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
              	  </td>
              </xsl:when>
              
              <xsl:when test="position()=2">
             	  <td align="right" class="moneyCol">
                	<a>
                 	<xsl:attribute name="href" >
              			javascript:              				
              				showdetail('<xsl:value-of select ="../td[position()=14]"/>','C','C');
              				
      			</xsl:attribute>
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                 	</a>
              	  </td>
              </xsl:when>
              
              <xsl:when test="position()=3">
             	  <td align="right" class="moneyCol">
                	<a>
                 	<xsl:attribute name="href" >
              			javascript:              				
              				showdetail('<xsl:value-of select ="../td[position()=14]"/>','C','S');
              				
      			</xsl:attribute>
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                 	</a>
              	  </td>
              </xsl:when>
              
              <xsl:when test="position()=4">
             	  <td align="right" class="moneyCol">
                	<a>
                 	<xsl:attribute name="href" >
              			javascript:              				
              				showCostDetail('<xsl:value-of select ="../td[position()=14]"/>','C','C');
              				
      			</xsl:attribute>
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                 	</a>
              	  </td>
              </xsl:when>
              
              <xsl:when test="position()=5">
             	  <td align="right" class="moneyCol">
                	<a>
                 	<xsl:attribute name="href" >
              			javascript:              				
              				showCostDetail('<xsl:value-of select ="../td[position()=14]"/>','C','S');
              				
      			</xsl:attribute>
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                 	</a>
              	  </td>
              </xsl:when>
              
              <xsl:when test="position()=6">
             	  <td align="right" class="moneyCol">
                	<a>
                 	<xsl:attribute name="href" >
              			javascript:              				
              				showInDirctCostDetail('<xsl:value-of select ="../td[position()=14]"/>','C','C');
              				
      			</xsl:attribute>
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                 	</a>
              	  </td>
              </xsl:when>
              
              <xsl:when test="position()=7">
             	  <td align="right" class="moneyCol">
                	<a>
                 	<xsl:attribute name="href" >
              			javascript:              				
              				showInDirctCostDetail('<xsl:value-of select ="../td[position()=14]"/>','C','C');
              				
      			</xsl:attribute>
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                 	</a>
              	  </td>
              </xsl:when>
              
              <xsl:when test="position()=14 or position()=15">
             	 <td align='center' style='display:none'>
                	<xsl:value-of select="."/>
              	 </td>
              </xsl:when>

              <xsl:otherwise>
                <td nowrap="true">
              		<xsl:value-of select="."/>
              	</td>
              </xsl:otherwise>
            </xsl:choose>
  	   </xsl:for-each>
  	</tr>
   	</xsl:for-each>		
	</tbody>
	</xsl:template>
</xsl:stylesheet>
