
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
			<th noWrap="true" rowspan="2" class="mainHead" >成本分类</th>
			<th noWrap="true" colspan="2" class="mainHead" >公用成本分摊</th>
			<th noWrap="true" colspan="2" class="mainHead" >管理成本分摊</th>
			<th noWrap="true" colspan="2" class="mainHead" >医辅成本分摊</th>
			<th noWrap="true" colspan="2" class="mainHead" >医技成本分摊</th>
			<th noWrap="true" colspan="2" class="mainHead" >合计</th>	
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()!=1">
		  <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
		  </td>
	        </xsl:when>
		<xsl:otherwise>
		  <td  noWrap='true' >
		      <xsl:value-of select="."/>
		 </td>
                </xsl:otherwise>
              </xsl:choose>
  	 </xsl:for-each>
  	</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
