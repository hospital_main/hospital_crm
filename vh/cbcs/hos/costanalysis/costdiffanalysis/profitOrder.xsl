<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:vh="http://www.w3.org/1999/XSL/Transforms">
	<xsl:template match="/">
	<thead>
		<!--直接医疗科室收益排名表-->
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" rowspan="3" class="mainHead" >科室名称</th>
			<th noWrap="true" colspan="4" class="mainHead" >总收入</th>
			<th noWrap="true" colspan="4" class="mainHead" >总成本</th>
			<th noWrap="true" rowspan="2" colspan="2" class="mainHead" >收入合计</th>
			<th noWrap="true" rowspan="2" colspan="2" class="mainHead" >成本合计</th>
			<th noWrap="true" rowspan="2" colspan="2" class="mainHead" >总收益</th>
			<th noWrap="true" rowspan="2" colspan="2" class="mainHead" >工作量</th>	
			<th style="display:none"/>	
			<th style="display:none"/>	
		</tr>
		<tr noWrap="true" class="mainHead">
			<th style="display:none"/>
			<th noWrap="true" colspan="2" class="mainHead" >本科开单本科执行</th>
			<th noWrap="true" colspan="2" class="mainHead" >本科开单医技执行</th>
			<th noWrap="true" colspan="2" class="mainHead" >直接成本</th>
			<th noWrap="true" colspan="2" class="mainHead" >间接成本</th>
			<th style="display:none"/>
			<th style="display:none"/>
			<th style="display:none"/>
			<th style="display:none"/>
			<th style="display:none"/>
			<th style="display:none"/>
			<th style="display:none"/>
			<th style="display:none"/>
			<th style="display:none"/>
			<th style="display:none"/>		
		</tr>
		<tr noWrap="true" class="mainHead">
			<th style="display:none"/>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th style="display:none"/>
			<th style="display:none"/>
		</tr>
	</thead>
	<tbody>
	<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">          
            <xsl:choose> 
            	<!--直接医疗科室——本科开单本科执行/医技执行收入明细-->                        
              <xsl:when test="position()=2">
             	  <td align="right" class="moneyCol">
                	<a>
                 	<xsl:attribute name="href" >
              			javascript:              				
              				showdetail('<xsl:value-of select ="../td[position()=19]"/>','D','C');
              				
      			</xsl:attribute>
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                 	</a>
              	  </td>
              </xsl:when>
              
              <xsl:when test="position()=3">
             	  <td align="right" class="moneyCol">
                	<a>
                 	<xsl:attribute name="href" >
              			javascript:              				
              				showdetail('<xsl:value-of select ="../td[position()=19]"/>','D','S');
              				
      			</xsl:attribute>
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                 	</a>
              	  </td>
              </xsl:when>
              <!--直接医疗科室——本科开单本科执行/医技执行收入明细-->                        
              <xsl:when test="position()=4">
             	  <td align="right" class="moneyCol">
                	<a>
                 	<xsl:attribute name="href" >
              			javascript:              				
              				showdetail('<xsl:value-of select ="../td[position()=19]"/>','C','C');
              				
      			</xsl:attribute>
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                 	</a>
              	  </td>
              </xsl:when>
              
              <xsl:when test="position()=5">
             	  <td align="right" class="moneyCol">
                	<a>
                 	<xsl:attribute name="href" >
              			javascript:              				
              				showdetail('<xsl:value-of select ="../td[position()=19]"/>','C','S');
              				
      			</xsl:attribute>
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                 	</a>
              	  </td>
              </xsl:when>
              <!--总成本 直接成本-->
              <xsl:when test="position()=6 or position()=7">
             	  <td align="right" class="moneyCol">
                	<a>
                 	<xsl:attribute name="href" >
              			javascript:              				
              				showDirctCostDetail('<xsl:value-of select ="../td[position()=19]"/>');
              				
      			</xsl:attribute>
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                 	</a>
              	  </td>
              </xsl:when>
              <!--总成本 间接成本-->
              <xsl:when test="position()=8 or position()=9">
             	  <td align="right" class="moneyCol">
                	<a>
                 	<xsl:attribute name="href" >
              			javascript:              				
              				showInDirctCostDetail('<xsl:value-of select ="../td[position()=19]"/>','D');	
      			</xsl:attribute>
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                 	</a>
              	  </td>
              </xsl:when>
              
              <!--其它数字项-->
              <xsl:when test="position()=10 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17">
             	  <td align="right" class="moneyCol">
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
              	  </td>
              </xsl:when>
              
              <xsl:when test="position()=18 or position()=19">
             	 <td align='center'  style='display:none'>
                	<xsl:value-of select="."/>
              	 </td>
              </xsl:when>

              <xsl:otherwise>
                <td nowrap="true">
              		<xsl:value-of select="."/>
              	</td>
              </xsl:otherwise>
            </xsl:choose>
  	   </xsl:for-each>
  	</tr>
   	</xsl:for-each>		
	</tbody>
	</xsl:template>
</xsl:stylesheet>
