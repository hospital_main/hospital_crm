<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:vh="http://www.w3.org/1999/XSL/Transforms">
	<xsl:template match="/">
	<thead>
		<!--管理科室成本核算报表-->
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" rowspan="3" class="mainHead" >科室名称</th>
			<th noWrap="true" colspan="2" class="mainHead" >直接成本</th>
			<th noWrap="true" colspan="2" class="mainHead" >公用成本分摊</th>
			<th noWrap="true" colspan="2" class="mainHead" >合计</th>
			<th noWrap="true" colspan="2" class="mainHead" >工作量</th>	
			<th style="display:none"></th>	
			<th style="display:none"></th>	
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
		</tr>
	</thead>
	<tbody>
	<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">          
            <xsl:choose>                         
              <xsl:when test="position()!=1 and position()!=10 and position()!=11">
             	  <td align="right" class="moneyCol">
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
              	  </td>
              </xsl:when>
              
              <xsl:when test="position()=10 or position()=11">
             	 <td align='center'  style='display:none'>
                	<xsl:value-of select="."/>
              	 </td>
              </xsl:when>

              <xsl:otherwise>
                <td nowrap="true">
              		<xsl:value-of select="."/>
              	</td>
              </xsl:otherwise>
            </xsl:choose>
  	   </xsl:for-each>
  	</tr>
   	</xsl:for-each>		
	</tbody>
	</xsl:template>
</xsl:stylesheet>
