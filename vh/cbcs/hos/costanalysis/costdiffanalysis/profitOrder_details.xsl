<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
      <xsl:template match="/">
            <thead>
                  <tr noWrap="true" class="mainHead"> </tr>
            </thead>
            <tbody>
                  <xsl:for-each select="/root/tbody/tr">
                        <xsl:variable name="income_type" select="td[1]" />
                        <xsl:variable name="income_subj_type" select="td[2]" />
                        <xsl:variable name="cur_pos" select="position()" />
                        <xsl:variable name="income_type_rowspan" select="count(/root/tbody/tr[td[1]=$income_type])" />
                        <xsl:variable name="income_subj_type_rowspan" select="count(/root/tbody/tr[td[2]=$income_subj_type])" />
                        <tr>
                              <xsl:for-each select="td">
                                    <xsl:choose>
                                          <xsl:when test="position()=1">
                                                <xsl:if test="$cur_pos = 1 or $income_type != ../../tr[$cur_pos - 1]/td[1]">
                                                      <td valign='middle' align='center'>
                                                            <xsl:attribute name="rowspan">
                                                                  <xsl:value-of select="$income_type_rowspan"/>
                                                            </xsl:attribute>
                                                            <xsl:value-of select="."/>
                                                      </td>
                                                </xsl:if>
                                          </xsl:when>
                                          <xsl:when test="position()=2">
                                                <xsl:if test="$cur_pos = 1 or $income_subj_type != ../../tr[$cur_pos - 1]/td[2]">
                                                      <td valign='middle' align='center'>
                                                            <xsl:attribute name="rowspan">
                                                                  <xsl:value-of select="$income_subj_type_rowspan"/>
                                                            </xsl:attribute>
                                                            <xsl:value-of select="."/>
                                                      </td>
                                                </xsl:if>
                                          </xsl:when>
                                          <xsl:when test="position()=4">
                                                <td align='right'>
                                                      <xsl:value-of select="format-number(.,'#,##0.00')"/>
                                                </td>
                                          </xsl:when>
                                          <xsl:otherwise>
                                                <td noWrap='true' >
                                                      <xsl:value-of select="."/>
                                                </td>
                                          </xsl:otherwise>
                                    </xsl:choose>
                              </xsl:for-each>
                        </tr>
                  </xsl:for-each>
            </tbody>
      </xsl:template>
</xsl:stylesheet>