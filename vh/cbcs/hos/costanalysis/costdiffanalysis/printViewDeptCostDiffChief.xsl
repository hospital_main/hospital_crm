<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:center"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
	  		<tr noWrap="true" class="mainHead">
					<td rowspan="3">科室名称</td>
					<td rowspan="2" colspan="2">收入</td>
					<td style="display:none"/>
					<td colspan="6">成本</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td colspan="2">收益</td>
					<td style="display:none"/>
					<td colspan="2">工作量</td>
					<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan="2">直接成本</td>
				<td style="display:none"></td>
				<td colspan="2">间接成本</td>
				<td style="display:none"></td>
				<td colspan="2">合计</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td style="display:none"></td>
				<td >本期</td>
				<td >累计</td>
				<td >本期</td>
				<td >累计</td>
				<td >本期</td>
				<td >累计</td>
				<td >本期</td>
				<td >累计</td>
				<td >本期</td>
				<td >累计</td>
				<td >本期</td>
				<td >累计</td>
			</tr>
  		</thead>
	  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr">
	  		<tr>
          <xsl:for-each select="td">          
            <xsl:choose>                         
              <xsl:when test="position()!=1 and position()!=14 and position()!=15">
             	  <td align="right" class="moneyCol">
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
              	  </td>
              </xsl:when>

              <xsl:otherwise>
                <td nowrap="true">
              		<xsl:value-of select="."/>
              	</td>
              </xsl:otherwise>
            </xsl:choose>
  	   </xsl:for-each>
  	</tr>
				</xsl:for-each>
	  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>