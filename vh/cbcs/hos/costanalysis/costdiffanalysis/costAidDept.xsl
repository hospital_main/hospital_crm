<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:vh="http://www.w3.org/1999/XSL/Transforms">
	<xsl:template match="/">
	<thead>
		<!--直接医疗科室收益排名表-->
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" rowspan="3" class="mainHead" >科室名称</th>
			<th noWrap="true" rowspan="2" colspan="2" class="mainHead" >直接成本</th>
			<th noWrap="true" colspan="4" class="mainHead" >间接成本</th>
			<th noWrap="true" rowspan="2" colspan="2" class="mainHead" >合计</th>
			<th noWrap="true" rowspan="2" colspan="2" class="mainHead" >工作量</th>	
			<th noWrap="true" style="display:none"></th>	
			<th noWrap="true" style="display:none"></th>	
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" rowspan="3" style="display:none"></th>
			<th noWrap="true" colspan="2" class="mainHead" >公用成本分摊</th>
			<th noWrap="true" colspan="2" class="mainHead" >管理成本分摊</th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>		
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" class="mainHead" >本期</th>
			<th noWrap="true" class="mainHead" >累计</th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" style="display:none"></th>
		</tr>
	</thead>
	<tbody>
	<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">          
            <xsl:choose>                         
              <xsl:when test="position()!=1 and position()!=12 and position()!=13">
             	  <td align="right" class="moneyCol">
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
              	  </td>
              </xsl:when>
              
              <xsl:when test="position()=12 or position()=13">
             	 <td align='center' style='display:none'>
                	<xsl:value-of select="."/>
              	 </td>
              </xsl:when>

              <xsl:otherwise>
                <td nowrap="true">
              		<xsl:value-of select="."/>
              	</td>
              </xsl:otherwise>
            </xsl:choose>
  	   </xsl:for-each>
  	</tr>
   	</xsl:for-each>		
	</tbody>
	</xsl:template>
</xsl:stylesheet>
