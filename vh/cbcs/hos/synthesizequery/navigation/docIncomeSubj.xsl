<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
				<th noWrap="true" >收入项目</th>
				<th noWrap="true" > 合计 </th>
				<th noWrap="true" > 占比</th>
				<th noWrap="true" > 开单收入 </th>
				<th noWrap="true" >执行收入</th>
  		</tr>
  		 
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
				<xsl:variable name="trnum" select="position()" />
				<xsl:variable name="income_subj_name" select="td[position()=1]"/>
				<xsl:variable name="flag" select="td[position()=6]"/>
          <xsl:for-each select="td">
          	<xsl:variable name="tdsix" select="../td[6]" />
              <xsl:choose>
              	<xsl:when test="(position()=1 and $trnum!=1 and $flag!='wwww')">
              		 <td>
										<a href="#">
											<xsl:attribute name="onclick">
												   openDialog('docChargeIncome.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;income_subj_name&gt;<xsl:value-of select='$income_subj_name'/>&lt;/income_subj_name&gt;', 'dialogWidth:1024px;dialogHeight:768px')
											</xsl:attribute>
										<xsl:value-of select="."/>
                   	</a>
                  </td>
              	</xsl:when>
              	<xsl:when test="(position()>1 and position()&lt;6 and $flag='wwww')">
              		 <td>
										
                  </td>
              	</xsl:when>
              	
              		<xsl:when test="position()=3 and $trnum=1 ">
              		 <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
              	</xsl:when>

              	<xsl:when test="position()=3 and string-length($tdsix)>0">
              		 <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
              	</xsl:when>

              	 <xsl:when test="position()>=6">
              		 <td style="display:none"><xsl:value-of select="."/></td>
              	</xsl:when>
                <xsl:when test="(position()=2  and $trnum!=1 and $flag!='wwww')">
                  <td  align="right">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:when>
                
                 <xsl:when test="(position()=2  and $trnum!=1 and string-length($tdsix)=0)">
                  <td>
                  </td>
                </xsl:when>
                <xsl:when test="(position()>1 and $trnum=1 and string-length($tdsix)=0)">
                  <td align="right">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:when>
                
                <xsl:when test="(position()>1 and $trnum>1 and string-length($tdsix)=0)">
                  <td>
                  </td>
                </xsl:when>
                
                 <xsl:when test="(position()>1 and $trnum>1 and string-length($tdsix)!=0)">
                  <td align="right">
										 <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  </xsl:template>
</xsl:stylesheet>
