<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr class="mainHead">
      	<th rowspan="2"  width='10%'>科室</th>
      	<th colspan="2">收入</th>
      	<th colspan="2">成本</th>
      	<th colspan="2">收益</th>
      	<th colspan="2">成本收益率 </th>
      	<th rowspan="2" align="center"><input type='checkbox' value="all" onclick="clickChk(this)"/></th>
      </tr>
      <tr class="mainHead"> 
      	<th>本期</th>
      	<th>累计</th>
      	<th>本期</th>
      	<th>累计</th>
      	<th>本期</th>
      	<th>累计</th>
      	<th>本期</th>
      	<th>累计</th>
      </tr>  		
    </thead>
    
  </xsl:template>
</xsl:stylesheet>

