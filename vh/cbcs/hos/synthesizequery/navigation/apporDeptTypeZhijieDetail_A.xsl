<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
  	<thead>
	<tr noWrap="true" class="mainHead">
		<th noWrap="true">成本单号</th>
		<th noWrap="true">支付日期</th>
		<th noWrap="true">摘要</th>
		<th noWrap="true">成本项目名称</th>
		<th noWrap="true">金额</th>
		<th noWrap="true">录入人</th>
		<th noWrap="true">是否已分摊</th>
		<th noWrap="true">数据来源</th>
	</tr>
  	</thead>
  	<tbody>
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
		<xsl:choose>
			<xsl:when test="position() &gt; 1 and position() != 6">
	               		<td><xsl:value-of select="."/></td>
			</xsl:when>
			<xsl:when test="position() = 6">
	               		<td align="right"><xsl:value-of select="format-number(., '#,##0.00')"/></td>
			</xsl:when>
		</xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>