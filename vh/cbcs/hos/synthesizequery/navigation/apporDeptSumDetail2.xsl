<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
  	<thead>
  	<tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="3">成本项目</th>
  	  	<th noWrap="true" colspan="2" rowspan="2">总成本</th>
  	  	<th noWrap="true" colspan="4">直接成本</th>
		<th noWrap="true" colspan="8">间接成本</th>
  	</tr>
	<tr noWrap="true" class="mainHead">
		<th noWrap="true" colspan="2">直接计入</th>
		<th noWrap="true" colspan="2">间接计入</th>
		<th noWrap="true" colspan="2">公用成本</th>
		<th noWrap="true" colspan="2">管理分摊</th>
		<th noWrap="true" colspan="2">医辅分摊</th>
		<th noWrap="true" colspan="2">医技分摊</th>
	</tr>
	<tr noWrap="true" class="mainHead">
		<th noWrap="true">金额</th>
		<th noWrap="true">比例</th>
		<th noWrap="true">成本</th>
		<th noWrap="true">比例</th>
		<th noWrap="true">成本</th>
		<th noWrap="true">比例</th>
		<th noWrap="true">成本</th>
		<th noWrap="true">比例</th>
		<th noWrap="true">成本</th>
		<th noWrap="true">比例</th>
		<th noWrap="true">成本</th>
		<th noWrap="true">比例</th>
		<th noWrap="true">成本</th>
		<th noWrap="true">比例</th>
	</tr>
  	</thead>
  	<tbody>
	  <xsl:variable name="rowCount" select="count(/root/tbody/tr)" />
  	  <xsl:for-each select="/root/tbody/tr">
		<xsl:variable name="curRowNum" select="position()" />
	      <tr>
	        <xsl:for-each select="td">
		<xsl:choose>
			<xsl:when test="position() = 1">
	               		<td><xsl:value-of select="."/></td>
			</xsl:when>
			<xsl:when test="position() &gt; 1 and position() mod 2 = 1">
				<td align="right"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
			</xsl:when>
			<xsl:when test="position() =  4 and  $curRowNum != $rowCount">
				<td align="right">
					<xsl:attribute name="style">color:blue;text-decoration:underline;cursor:pointer</xsl:attribute>
					<xsl:attribute name="onclick">
					//openDetailPage('<xsl:value-of select="position()"/>', '<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>')
					openDialog('apporDeptTypeZhijieDetail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each><begin_date>'+begin_date.value+'</begin_date><end_date>'+end_date.value+'</end_date><dept_code>'+dept_code.value+'</dept_code>', 'dialogWidth:1024px;dialogHeight:768px');
					</xsl:attribute>
					<xsl:value-of select="format-number(.,'#,##0.00')"/>
				</td>
			</xsl:when>
			<xsl:otherwise>
				<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			</xsl:otherwise>
		</xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>