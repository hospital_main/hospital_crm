<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">
			<xsl:for-each select="/root/tbody/tr[td[1]='0']/td">
				<xsl:choose>
					<xsl:when test="position()=1">
						<td style="display:none"><xsl:value-of select="."/></td>
					</xsl:when>
						<xsl:when test="position()=last()+1">
						<td style="display:none"><xsl:value-of select="."/></td>
					</xsl:when>
					<xsl:otherwise>
						<th noWrap="true"><xsl:value-of select="."/></th>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr[td[1]!='0']">
			<tr>
				<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td style="display:none"><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=2">
							  <td noWrap='true'>
							    <xsl:if test="../td[1]!='z'">
		  							<a href="#">
		  								<xsl:attribute name="onclick">
		  		            						openDetail("detail_allcost_wage_detail.html", '<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', this);
		  								</xsl:attribute>
										<xsl:value-of select="."/>
		  							</a>
	  							</xsl:if>
	  							<xsl:if test="../td[1]='z'">
	  							  <xsl:value-of select="."/>
	  							</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()=4">
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
							</xsl:when>
									<xsl:when test="position()=last()">
											<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:otherwise>
						</xsl:choose>
				</xsl:for-each>


			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
