<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>

  		 <tr class="mainHead">
				<th noWrap="true">收费类别</th>
				<th noWrap="true">开单收入</th>
				<th noWrap="true">占比</th>
				<th noWrap="true">执行收入</th>
				<th noWrap="true">占比</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position()=1 ">
              		 <td><xsl:value-of select="."/></td>
              	</xsl:when>
              	<xsl:when test="position()=2 ">
              	
              	 <td align = 'right'>
			<a href="#">
				<xsl:attribute name="onclick">
					openDialog('docIncomeChargedetail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:1024px;dialogHeight:768px')
				</xsl:attribute>
				<xsl:value-of select="format-number(.,'#,##0.00')"/>
                   	</a>
                  </td>
              	</xsl:when>
              	
              	<xsl:when test="position()=3 or position()=5 ">
              		 <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
              	</xsl:when>
                <xsl:otherwise>
                  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  </xsl:template>
</xsl:stylesheet>
