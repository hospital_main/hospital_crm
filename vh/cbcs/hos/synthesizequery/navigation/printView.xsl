<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
  	  	<tr class="mainHead">
				<td noWrap="true" rowspan = '2'>核算月</td>
				<td noWrap="true" colspan = '2'>收入</td>
				<td style="display:none"/>
				<td noWrap="true" colspan ='2' >成本</td>
				<td style="display:none"/>
				<td noWrap="true" colspan = '2' >收益</td>
				<td style="display:none"/>
				<td noWrap="true" colspan = '2'>成本收益率</td>
				<td style="display:none"/>
  		</tr>
  		 <tr class="mainHead">
				<td noWrap="true" style ='display:none'></td>
				<td noWrap="true">本期</td>
				<td noWrap="true">累计</td>
				<td noWrap="true">本期</td>
				<td noWrap="true">累计</td>
				<td noWrap="true">本期</td>
				<td noWrap="true">累计</td>
				<td noWrap="true">本期</td>
				<td noWrap="true">累计</td>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>

          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td>
			<a href="#">
				<xsl:attribute name="onclick">
					openDialog('deptIncome.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;curmonth&gt;<xsl:value-of select="."/>&lt;/curmonth&gt;', 'dialogWidth:850px;dialogHeight:540px')
				</xsl:attribute>
			<xsl:value-of select="."/>
                   	</a>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>