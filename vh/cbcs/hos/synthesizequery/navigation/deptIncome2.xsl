<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
      <xsl:for-each select="/root/tbody/tr">
      	<tr >
      		<xsl:variable name='trone' select='./td[position()=2]' />
      		<xsl:attribute name="TREEITEMPK">
      			<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			</xsl:attribute>
				<xsl:attribute name="TREEITEMISLEAF"><xsl:value-of select="td[1]"/></xsl:attribute>
				<xsl:variable name="dept_name" select="td[position()=2]"/>
      	  <xsl:for-each select="td[position()&gt;1]">
      	    <xsl:choose>
      	      <xsl:when test="position()=1">
      	        <td noWrap="true"  align='left'>
      	          <xsl:value-of select="."/>
      	        </td>
      	      </xsl:when>
      	       <xsl:when test="(position()=2 or position()=3) and $trone!='合计'">
      	        <td noWrap="true"  align='right'>
      	           <a href="#">
			<xsl:attribute name="onclick">
				openDialog('docIncome.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;dept_name&gt;<xsl:value-of select='$dept_name'/>&lt;/dept_name&gt;&lt;col_no&gt;<xsl:value-of select='position()'/>&lt;/col_no&gt;', 'dialogWidth:1024px;dialogHeight:768px')
			</xsl:attribute>
			<xsl:value-of select="format-number(.,'#,##0.00')"/>
                   </a>
      	        </td>
      	      </xsl:when>
      	       <xsl:when test="(position()=4 or position()=5) and $trone!='合计'">
      	        <td noWrap="true"  align='right'>
      	           <a href="#">
			<xsl:attribute name="onclick">
				openDialog('apporDeptSumDetail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;col_no&gt;<xsl:value-of select='position()'/>&lt;/col_no&gt;', 'dialogWidth:1024px;dialogHeight:768px')
			</xsl:attribute>
			<xsl:value-of select="format-number(.,'#,##0.00')"/>
                   </a>
      	        </td>
      	      </xsl:when>
      	         <xsl:when test="(position()=8 or position()=9)">
      	        <td noWrap="true"  align='right'>
			<xsl:value-of select="format-number(.,'#,##0.00%')"/>
      	        </td>
      	      </xsl:when>
      	      <xsl:otherwise>
      	        <td noWrap="true" align='right'>
      	          <xsl:value-of select="format-number(.,'#,##0.00')"/>
      	        </td>
      	      </xsl:otherwise>
      	    </xsl:choose>
      	  </xsl:for-each>
      	  <td align="center"><xsl:if test="td[2] != '合计'"><input type='checkbox' onclick="clickChk(this)"><xsl:attribute name="value"><xsl:value-of select="td[2]"/></xsl:attribute></input></xsl:if></td>
      	</tr>
      </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>

