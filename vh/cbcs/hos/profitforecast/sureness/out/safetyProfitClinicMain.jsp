<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/profitforecast/sureness/out/safetyProfitClinicMain.jsp,v 1.1 2012/03/12 01:58:27 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:27 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function judgeDate(){
    if(template.type.value=="month"){
      if(template.yearMonth.value==""){
        alert("请选择核算时间");
        return false;
      }
    }
    else if(template.type.value=="quarter"){
      if(template.yearQuarter.value==""){
        alert("请选择核算时间"); 
        return false;
      }
    }
    else{
      if(template.year.value==""){
        alert("请选择核算时间");
        return false;
      }
    }
 }
  function find(){
   if(judgeDate()==false){
      return false;
    }
    template.subFunction.value = "goClinicSafetyProfitMain";
    template.cbcs_module.value = "hospital_synthesize";//医院综合运营分析平台模块
    show_wait();
    template.submit();
  }
  function preparedPrint() {
    // 报表名称
    grid.prn.title1='门诊科室收益预测(本量利法)';
    // 年月
    grid.prn.title2 ='<%=judgeTitle()%>';
    // 表头行数
    grid.prn.tabHead = 2;
    // 打印
    grid.print();
  }
</Script>
<%
	  String[][] result = (String[][])request.getAttribute("result");
	%>

<html:html clazz="main" isPrint="true" fixRows="2" >
<form name="template" method="post" action="hosProfitClinicSafety.jspviewhigh">
	<!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
	<html:title clazz='module'>门诊科室收益预测(安全边际法)</html:title>

  <!-- 查询项 -->
  <html:table clazz="simple">
  	<tr>
    	<td class="normalText" width="40">　</td>
    	<td class="signText" width="72">分析单位：</td>
    	<td class="normalText">
       <%String[][] s1={{"month","月"},
                        {"quarter","季"},
                        {"year","年"}
                       };%>
      	<input:vanish name="type" options="<%=s1%>" cssclass="selectBg">
    	</td >
    	<td nowrap class="signText" width="72">核算时间：</td>
    	<td>
      	<input:vanishEle >
        	<input:yearM name="yearMonth"></input:yearM>
        </input:vanishEle>
        <input:vanishEle >
        	<input:yearQ name="yearQuarter" ></input:yearQ>
        </input:vanishEle>
        <input:vanishEle >
        	<input:year name="year" ></input:year>
        </input:vanishEle>
        </input:vanish>
    	</td>
    	<td nowrap class="signText" >数据来源：</td>
    	<td nowrap="nowrap">
      	<input type="radio" name="kind" value="fact" style="border:0px" <%if(request.getParameter("kind")==null||(request.getParameter("kind")!=null&&request.getParameter("kind").equals("fact"))) out.print("checked");%> >实际
        <input type="radio" name="kind" value="paln" style="border:0px" <%if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("paln")) out.print("checked");%>>计划
    	</td>
    	<!--<td>科室：
    		<%=new Select(request.getAttribute("depts"), "depts", request.getParameter("depts")==null? "":request.getParameter("depts"), false, true)%>
    		
    		</td>
    		-->
    </tr>
    <tr>
    <td nowrap colspan="7" align="right"><button class="pageBtn" onclick="return find();">计算</button>
      <%if(result!=null){%>
      <button class="pageBtn" onclick="return preparedPrint();">打印</button></td>
    	<%}%>
    </tr>
  </html:table>

  <br>

  <!-- 查询结果 -->
  <html:title clazz='table'>
  	门诊科室收益预测(安全边际法-<%
    if(request.getParameter("kind")==null||(request.getParameter("kind")!=null&&request.getParameter("kind").equals("fact"))) out.print("实际");
    if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("paln")) out.print("计划");
    %>)
  	</html:title>

 <vh:vhFixTable fixRow=2 fixCol=0>
			<table  class="resultSetTable" >
			<colgroup id='tg'>
          		 <col style = 'width:14.4%'>
      	  		 <col style = 'width:14.2%'>
      	  		  <col style = 'width:14.2%'>
      	  		   <col style = 'width:14.2%'>
      	  		    <col style = 'width:14.2%'>
      	  		     <col style = 'width:14.2%'>
      	  		      <col style = 'width:14.2%'>     	  		      
          	</colgroup>
        		
      	<tr class="resultLabel">
          <td nowrap>门诊科室</td>
          <td  nowrap class="resultLabel">保本工作量<br>(门诊人次)</td>
      		<td  nowrap class="resultLabel">工作量<br>(门诊人次)</td>
      		<td  nowrap class="resultLabel">安全边际量<br>(门诊人次)</td>
      		<td  nowrap class="resultLabel">单位变动成本<br>(元)</td>
      		<td  nowrap class="resultLabel">单位收入<br>(元)</td>
      		<td  nowrap class="resultLabel">预计收益<br>(元)</td>
        </tr>
       	<% if(result!=null){%>
        <%
            for (int i = 0; i < result.length; i++ ) {
		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
        %>
	          <tr CLASS="<%=rowColor%>">
            	<td class="normalText" nowrap style="text-align:left"><%=result[i][0]%></td>
              <td nowrap class="numberText"><%=changeFormat(result[i][1])%></td>
              <td nowrap class="numberText"><%=result[i][2]%></td>
              <td nowrap class="numberText"><%=result[i][3]%></td>
              <td nowrap class="numberText"><%=result[i][4]%></td>
              <td nowrap class="numberText"><%=result[i][5]%></td>
              <td nowrap class="numberText"><%=result[i][6]%></td>
	          </tr>
		    <%
		          }
        %>
        <%
        }
		    %>
		  </table>
 </vh:vhFixTable>

  <input type='hidden' name="subFunction"/>
  <input type='hidden' name="cbcs_module"/>

</form>
</html:html>
