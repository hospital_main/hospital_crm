<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th noWrap="true">标准颁发单位</th>
				<th noWrap="true">诊次收入</th>
				<th noWrap="true">诊次成本</th>
				<th noWrap="true">诊次固定成本</th>
				<th noWrap="true">诊次变动成本</th>
				<th noWrap="true">床日收入</th>
				<th noWrap="true">床日成本</th>
				<th noWrap="true">床日固定成本</th>
				<th noWrap="true">床日变动成本</th>
				<th noWrap="true">医技单位收入</th>
				<th noWrap="true">医技单位成本</th>
				<th noWrap="true">医技单位固定成本</th>
				<th noWrap="true">医技单位变动成本</th>
				<th noWrap="true">收益率</th>
				<th noWrap="true">每职工收入</th>
				<th noWrap="true">每职工成本</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center' style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td>
                    <a tabindex='-1'><xsl:value-of select="."/></a>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  </xsl:template>
</xsl:stylesheet>
