<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead"> 
  		<th style="display:none">报告号</th> 		  
				<th noWrap="true">年月</th>
				<th noWrap="true">报告时间</th>
				<th noWrap="true">科室名称</th>
				<th noWrap="true">报告人</th>
				<th noWrap="true">报告摘要</th>
		 </tr>
  	</thead>
  	<tbody>
  	<xsl:for-each select="/root/tbody/tr">
					<tr>
					
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=2">
								<td>
								<xsl:variable name="year" select="."></xsl:variable>
									<xsl:value-of select="substring($year,1,4)"></xsl:value-of>年<xsl:value-of select="substring($year,5,2)"></xsl:value-of>月 - <xsl:value-of select="substring($year,7,2)"></xsl:value-of>月
								</td>	
							</xsl:when>
							
							<xsl:when test="position()=3">
								<td>
								<xsl:variable name="year" select="."></xsl:variable>
									<xsl:value-of select="substring($year,1,4)"></xsl:value-of>年<xsl:value-of select="substring($year,5,2)"></xsl:value-of>月<xsl:value-of select="substring($year,7,2)"></xsl:value-of>日
								</td>	
							</xsl:when>

							<xsl:when test=" position()=5 or position()=6">
								<td><xsl:value-of select="."></xsl:value-of></td>	
							</xsl:when>
							<xsl:when test="position()=4">
									<td>
										<a href="#">
											<xsl:attribute name="onclick">
								openDialog('../trace/summary.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px', result)
								</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>

						</xsl:choose>
						
					</xsl:for-each>

					
					</tr>
				</xsl:for-each>
  	<!--
	    <xsl:for-each select="/root/tbody/tr">
        <tr>          
          <xsl:for-each select="td">
              <xsl:choose> 
                <xsl:when test="position()=1">
                		</xsl:when>
              <xsl:when test="position()=4">
								<td>
								<a href="#">
								<xsl:attribute name="onclick" >
								openDialog('../trace/summary.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px', result)
								</xsl:attribute><xsl:value-of select="."/></a></td>
								</xsl:when>
									
              <xsl:otherwise>
                 <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each> 
   		--> 	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
