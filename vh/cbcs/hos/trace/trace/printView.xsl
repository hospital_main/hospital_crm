<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/trace/trace/printView.xsl,v 1.1 2012/03/12 01:58:27 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:27 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:120mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:150mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:140mm'/>	
				<col style = 'width:120mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>
			</colgroup>
			<thead>
				<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>
	  		<tr noWrap='true'>
	        <td style='fontsize:subtitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>
			</thead>
			
			<tbody>
			<tr noWrap="true" >
			<td noWrap="true">报告时间：</td>
			<td noWrap="true"><xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[2]"/></td>
			<td noWrap="true">科室：</td>
			<td noWrap="true">
				<xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[3]"/>
			</td>
			<td noWrap="true">核算月：</td>
			<xsl:variable name="year" select="/root/tbody/tr[td[1]=1][1]/td[4]"></xsl:variable>
			
			<td noWrap="true">
				<xsl:value-of select="substring($year,1,4)"></xsl:value-of>年<xsl:value-of select="substring($year,5,2)"></xsl:value-of>月<xsl:value-of select="substring($year,7,2)"></xsl:value-of>日

			</td>
		</tr>
		<tr noWrap="true" >
			<td noWrap="true">摘要：</td>
			<td noWrap="true" colspan="5">
		<xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[5]"/>
			</td>
		</tr>
		<tr>
			<td style="display:none">
				<input name="analy_track_id" id="analy_track_id"  class="commonInputStyle"  style="width:100%">
				<xsl:attribute name="value"><xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[7]"/></xsl:attribute>
				</input>
			</td>
			</tr>

		<tr noWrap="true" >
			<td noWrap="true" colspan="6">科室基本情况：</td>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=2]">
			<tr>          
				<xsl:for-each select="td[position()!=1]">
				<xsl:variable name="pp" select="position()"/>
					<xsl:choose> 
						<xsl:when test="(position() mod 2)=0">
							<xsl:if test="substring(../td[ $pp ],1,3) = '202' or substring(../td[ $pp ],1,3) = '203' ">
										<td align="right">
											<xsl:if test="number(.) &lt; 0">
										 -
										</xsl:if>
											<xsl:if test="number(.) >= 0">
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
											</xsl:if>
										</td>
									</xsl:if>
									<xsl:if test="substring(../td[ $pp ],1,3) != '202' and substring(../td[ $pp ],1,3) != '203' ">
										<td align="right">
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</td>
									</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<td ><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each> 
		<tr noWrap="true" >
			<td noWrap="true" colspan="6"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
		</tr> 	
		<tr noWrap="true" >
			<td noWrap="true" colspan="6">分析纪录：</td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=3]">
			<tr noWrap="true" >
				<td noWrap="true">序号：</td>
				<td noWrap="true"><xsl:value-of select="td[2]"/></td>
				<td noWrap="true" colspan="2">时间：<xsl:value-of select="td[3]"/></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">发言人：<xsl:value-of select="td[4]"/></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
			</tr>
			<tr noWrap="true" >
				<td noWrap="true" colspan="2">分析层信息：</td>
				<td noWrap="true" colspan="5"><xsl:value-of select="td[5]"/></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
			</tr>
			<tr noWrap="true" >
				<td noWrap="true" colspan="6">
				发言人分析：<br/><br/>
				<xsl:value-of select="td[6]"/>
				</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
			</tr>
		</xsl:for-each> 
	
	</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>