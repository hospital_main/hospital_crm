<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
			<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true" colspan="2">影响收益因素</th> 
				<th noWrap="true" rowspan="2">变动程度%</th>
				<th noWrap="true" rowspan="2">变动量</th>
				<th noWrap="true" rowspan="2">测算结果</th>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
				<th noWrap="true">因素</th>
				<th noWrap="true">基础数据</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<xsl:variable name='rows' select='position()'/>
				<tr>
				<xsl:if test='position()=1'>
					<xsl:for-each select="td">
						<td>
						<xsl:value-of select="."/>
						</td>
					</xsl:for-each>  	
				</xsl:if>
				<xsl:if test='position()=6'>
					<xsl:for-each select="td">
						<td>
						<xsl:value-of select="."/>
						</td>
					</xsl:for-each>  	
				</xsl:if>
				<xsl:if test='position()!=1 and position()!=6'>
					<xsl:for-each select="td">
						<td>
						<xsl:choose>
							<xsl:when test='position()=2'>
								<xsl:attribute name="id">inputs_<xsl:value-of select='$rows'/></xsl:attribute>
								<xsl:attribute name="style">text-align:right</xsl:attribute>
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</xsl:when>
							<xsl:when test='position()=5'>
								<xsl:attribute name="id">inputs_<xsl:value-of select='$rows'/>_end</xsl:attribute>
								<xsl:attribute name="style">text-align:right</xsl:attribute>
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</xsl:when>
							<xsl:when test='position()=3 and $rows!=7 and $rows!=8'>
							<xsl:attribute name="style">text-align:center</xsl:attribute>
								<input type='text' class='inputDecimal' point='2' onchange='caculate(this);'>
									<xsl:attribute name='name'>inputs_<xsl:value-of select='$rows'/>_3</xsl:attribute>
									
									<xsl:attribute name='value'>0</xsl:attribute>
								</input>
							</xsl:when>
							<xsl:when test='position()=4 and $rows!=8 and $rows!=7'>
							<xsl:attribute name="style">text-align:center</xsl:attribute>
								<input type='text' class='inputDecimal' point='2' onchange='caculate(this);'>
									<xsl:attribute name='name'>inputs_<xsl:value-of select='$rows'/>_4</xsl:attribute>
									
									<xsl:attribute name='value'>0</xsl:attribute>
								</input>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>
							</xsl:otherwise>
						</xsl:choose>
						</td>
					</xsl:for-each>
				</xsl:if>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
