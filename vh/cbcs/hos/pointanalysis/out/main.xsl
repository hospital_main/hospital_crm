<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
			<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true" rowspan="2">门诊科室</th> 
				<th noWrap="true" colspan="4">实际</th>
				<th noWrap="true" colspan="2">保本</th>
				<th noWrap="true" rowspan="2">收益测算</th>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
				<th noWrap="true">总收入</th>
				<th noWrap="true">总成本</th>
				<th noWrap="true">总收益</th>
				<th noWrap="true">诊次数量</th>
				<th noWrap="true">保本诊次</th>
				<th noWrap="true">保本总收入</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
								<xsl:when test="position()=1">
								<td>
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()&gt;=2 and position()&lt;=7">
									<td>
										<xsl:attribute name="style">text-align:right</xsl:attribute>
										<xsl:if test="position()!=6 and position()!=7">
											<xsl:if test=". &lt; 0 and position()=4">
												<xsl:attribute name="style">text-align:right;font-weight:bold;color:orange</xsl:attribute>
											</xsl:if>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</xsl:if>
										<xsl:if test="position()=6">
											<xsl:attribute name="style">text-align:right</xsl:attribute>
											<xsl:if test="string(number(.))='NaN'">
												-
											</xsl:if>
											<xsl:if test="string(number(.))!='NaN'">									
												<a href="#">
													<xsl:attribute name="onclick">
														openTooMuchLink(pageMap,6,'&lt;v1&gt;<xsl:value-of select="../td[6]"/>&lt;/v1&gt;&lt;v2&gt;<xsl:value-of select="../td[7]"/>&lt;/v2&gt;&lt;v3&gt;<xsl:value-of select="../td[8]"/>&lt;/v3&gt;&lt;menzhenName&gt;<xsl:value-of select="../td[1]"/>&lt;/menzhenName&gt;&lt;fromDateYear&gt;<xsl:value-of select="../td[10]"/>&lt;/fromDateYear&gt;&lt;fromDateMonth&gt;<xsl:value-of select="../td[11]"/>&lt;/fromDateMonth&gt;&lt;endDateMonth&gt;<xsl:value-of select="../td[12]"/>&lt;/endDateMonth&gt;');
													</xsl:attribute>
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
												</a>
											</xsl:if>
										</xsl:if>
										<xsl:if test="position()=7">
											<xsl:attribute name="style">text-align:right</xsl:attribute>
												<xsl:if test="string(number(.))='NaN'">
													-
												</xsl:if>
												<xsl:if test="string(number(.))!='NaN'">									
													<xsl:value-of select="format-number(.,'#,##0.00')"/>
												</xsl:if>
										</xsl:if>
									</td>
								</xsl:when>
							</xsl:choose>
					</xsl:for-each>
					<td align="center">
						<a href="#">
							<xsl:attribute name="onclick">
								openTooMuchLink(pageMap,8,'&lt;deptname&gt;<xsl:value-of select="./td[1]"/>&lt;/deptname&gt;&lt;bedUse&gt;<xsl:value-of select="./td[5]"/>&lt;/bedUse&gt;&lt;income&gt;<xsl:value-of select="./td[2]"/>&lt;/income&gt;&lt;chCost&gt;<xsl:value-of select="./td[9]"/>&lt;/chCost&gt;&lt;fixCost&gt;<xsl:value-of select="./td[8]"/>&lt;/fixCost&gt;&lt;pureIncome&gt;<xsl:value-of select="./td[4]"/>&lt;/pureIncome&gt;');
							</xsl:attribute>
							<img src='../../../../images/track.gif' border='0'>
								<xsl:attribute name="title">
									<xsl:value-of select="td[1]"/>
								</xsl:attribute>
							</img>
						</a>
					</td>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
