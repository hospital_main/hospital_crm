<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/pointanalysis/out/printView.xsl,v 1.1 2012/03/12 01:58:26 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:26 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:5;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:subtitle;colspan:2;align:right"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" rowspan="2">门诊科室</td>
				<td noWrap="true" colspan="4">实际</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" colspan="2">保本</td>
				<td style="display:none"/>
		  </tr>
		  <tr noWrap="true" class="mainHead">
		  	<td noWrap="true"></td>
				<td noWrap="true">总收入</td>
				<td noWrap="true">总成本</td>
				<td noWrap="true">总收益</td>
				<td noWrap="true">诊次数量</td>
				<td noWrap="true">保本诊次</td>
				<td noWrap="true">保本总收入</td>
			</tr>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr noWrap="true">
					<xsl:if test="floor(td[5])&lt;floor(td[6]) or string(number(td[6]))='NaN'">
						<xsl:attribute name="style">
							color:red
						</xsl:attribute>
					</xsl:if>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
								</xsl:when>
								<xsl:when test="position()&gt;=2 and position()&lt;=7">
									<td noWrap="true">
										<xsl:attribute name="style">align:right</xsl:attribute>
										<xsl:if test="position()!=6 and position()!=7">
											<xsl:if test=". &lt; 0">
												<xsl:attribute name="style">align:right;font-weight:bold;color:orange</xsl:attribute>
											</xsl:if>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</xsl:if>
										<xsl:if test="position()=6">
											<xsl:attribute name="style">align:right</xsl:attribute>
											<xsl:if test="string(number(.))='NaN'">
												-
											</xsl:if>
											<xsl:if test="string(number(.))!='NaN'">
												<a href="#">
													<xsl:attribute name="onclick">
														openTooMuchLink(pageMap,6,'&lt;v1&gt;<xsl:value-of select="../td[6]"/>&lt;/v1&gt;&lt;v2&gt;<xsl:value-of select="../td[7]"/>&lt;/v2&gt;&lt;v3&gt;<xsl:value-of select="../td[8]"/>&lt;/v3&gt;');
													</xsl:attribute>
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
												</a>
											</xsl:if>
										</xsl:if>
										<xsl:if test="position()=7">
											<xsl:attribute name="style">align:right</xsl:attribute>
												<xsl:if test="string(number(.))='NaN'">
													-
												</xsl:if>
												<xsl:if test="string(number(.))!='NaN'">
													<xsl:value-of select="format-number(.,'#,##0.00')"/>
												</xsl:if>
										</xsl:if>
									</td>
								</xsl:when>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:3;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
  </root>
	</xsl:template>
</xsl:stylesheet>
