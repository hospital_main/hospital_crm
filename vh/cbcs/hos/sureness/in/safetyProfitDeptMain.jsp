<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/sureness/in/safetyProfitDeptMain.jsp,v 1.1 2012/03/12 01:58:27 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:27 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
function judgeDate(){
    if(template.type.value=="month"){
      if(template.yearMonth.value==""){
        alert("请选择核算时间");
        return false;
      }
    }
    else if(template.type.value=="quarter"){
      if(template.yearQuarter.value==""){
        alert("请选择核算时间");
        return false;
      }
    }
    else{
      if(template.year.value==""){
        alert("请选择核算时间");
        return false;
      }
    }
 }
  function find(){
    if(judgeDate()==false){
      return false;
    }
    template.subFunction.value = "goDeptSafetyProfitMain";
    show_wait();
    template.submit();
  }
  function preparedPrint() {
     <%String s="";
     if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("fact")){
       s="住院科室收益预测(安全边际法-实际)";
     }
     if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("paln")){
       s="住院科室收益预测(安全边际法-计划)";
     }
    %>
    // 报表名称
    grid.prn.title1='<%=s%>';
    // 年月
    grid.prn.title2 ='<%=judgeTitle()%>';
    // 表头行数
    grid.prn.tabHead = 1;
    // 打印
    grid.print();
  }
</Script>
	<%
	   String[][] result = (String[][])request.getAttribute("result");

	%>
<html:html clazz="main" isPrint="true" fixRows="1">
<form name="template" method="post" action="hosProfitDeptSafety.jspviewhigh">
	<!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
	<html:title clazz='module'>住院科室</html:title>

  <!-- 查询项 -->
  <html:table clazz="simple">
  	<tr>
    	<td class="normalText" width="40">　</td>
    	<td class="signText" width="72">分析单位:</td>
    	<td class="normalText">
       <%String[][] s1={{"month","月"},
                        {"quarter","季"},
                        {"year","年"}
                       };%>
      	<input:vanish name="type" options="<%=s1%>" cssclass="selectBg">
    	</td >
    	<td nowrap class="signText" width="72">核算时间:</td>
    	<td>
      	<input:vanishEle >
        	<input:yearM name="yearMonth"></input:yearM>
        </input:vanishEle>
        <input:vanishEle >
        	<input:yearQ name="yearQuarter" ></input:yearQ>
        </input:vanishEle>
        <input:vanishEle >
        	<input:year name="year" ></input:year>
        </input:vanishEle>
        </input:vanish>
    	</td>
    	<td nowrap class="signText" width="72">数据来源：</td>
    	<td>
      	<input type="radio" name="kind" value="fact" <%if(request.getParameter("kind")==null||(request.getParameter("kind")!=null&&request.getParameter("kind").equals("fact"))) out.print("checked");%> >实际
        <input type="radio" name="kind" value="paln" <%if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("paln")) out.print("checked");%>>计划
    	</td>
    	<td nowrap width="100"><button class="pageBtn" onclick="return find();">计算</button></td>
    	<%if(result!=null){%>
      <td nowrap width="100"><button class="pageBtn" onclick="return preparedPrint();">打印</button></td>
      <%}%>
    	<td class="normalText" width="20">　</td>
  	</tr>
  </html:table>

  <br>

  <!-- 查询结果 -->
  <html:title clazz='table'>
    住院科室收益预测(安全边际法-<%
    if(request.getParameter("kind")==null||(request.getParameter("kind")!=null&&request.getParameter("kind").equals("fact"))) out.print("实际");
    if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("paln")) out.print("计划");
    %>)
  </html:title>
					<html:webprint clazz='new' >
        			<colgroup id='tg'>
          		<col style = 'width:200px'>
      	  		<col style = 'width:150px'>
          		<col style = <%=DisplayWidth.MONEY_WIDTH%>>
          		<col style = 'width:150px'>
          		<col style = 'width:150px'>
          		<col style = <%=DisplayWidth.MONEY_WIDTH%>>
            	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
          	</colgroup>
          	<html:tr clazz='label'>
          		<td class="resultLabel" nowrap >住院科室</td>
          		<td class="resultLabel" nowrap>保本工作量<br>(床日)</td>
          		<td class="resultLabel" nowrap>工作量<br>(床日)</td>
          		<td class="resultLabel" nowrap>安全边际量<br>(床日)</td>
          		<td class="resultLabel" nowrap>单位变动成本<br>(元)</td>
          		<td class="resultLabel" nowrap>单位收入<br>(元)</td>
          		<td class="resultLabel" nowrap>预计收益<br>(元)</td>
        		</html:tr>
         </html:webprint>
        <% if(result!=null){%>
           <div style='border:1px solid #000000;overflow:auto;height:378px'>
            <table border='1' bgColor=white borderColor=black style='border:2px solid #000000;BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
              <colgroup id='tg'>
          		<col style = 'width:200px'>
      	  		<col style = 'width:150px'>
          		<col style = <%=DisplayWidth.MONEY_WIDTH%>>
          		<col style = 'width:150px'>
          		<col style = 'width:150px'>
          		<col style = <%=DisplayWidth.MONEY_WIDTH%>>
            	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
          	</colgroup>
        <%
            for (int i = 0; i < result.length; i++ ) {
		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
        %>
	          <tr CLASS="<%=rowColor%>">
            	<td class="normalText" nowrap style="text-align:left"><%=result[i][0]%></td>
              <td nowrap class="numberText"><%=result[i][1]%></td>
              <td nowrap class="numberText"><%=result[i][2]%></td>
              <td nowrap class="numberText"><%=checkSafetyWorkload(result[i][3])%></td>
              <td nowrap class="numberText"><%=changeFormat(result[i][4])%></td>
              <td nowrap class="numberText"><%=changeFormat(result[i][5])%></td>
              <td nowrap class="numberText"><%=changeFormat(result[i][6])%></td>
	          </tr>
		    <%
		          }
        %>
        </table>
      </div>
        <%
        }
		    %>

  <input type='hidden' name="subFunction"/>
</form>
</html:html>
