<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/basicincome/in/periodProfitDeptMain.jsp,v 1.1 2012/03/12 01:58:12 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:12 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function judgeDate(){
     if(template.type.value=="month"){
      if(template.beginYearM.value==""){
        alert("请选择基期");
        return false;
      }
      if(template.endYearM.value==""){
        alert("请选择计划期");
        return false;
      }
    }
    else if(template.type.value=="quarter"){
      if(template.beginYearQ.value==""){
        alert("请选择基期");
        return false;
      }
      if(template.endYearQ.value==""){
        alert("请选择计划期");
        return false;
      }
    }
    else{
      if(template.beginYear.value==""){
        alert("请选择基期");
        return false;
      }
      if(template.endYear.value==""){
        alert("请选择计划期");
        return false;
      }
    }
  }
  function find(){
    if(judgeDate()==false){
      return false;
    }
    template.subFunction.value = "goDeptPeriodProfitMain";
    show_wait();
    template.submit();
  }
  function preparedPrint() {
    // 报表名称
    grid.prn.title1='住院科室收益预测(基期收益率法)';
    // 年月
    grid.prn.title2 ='<%=judgeTitlePeriod()%>';
    // 表头行数
    grid.prn.tabHead = 1;
    // 打印
    grid.print();
  }
</Script>
	<%
	  String[][] result = (String[][])request.getAttribute("result");
	%>
<html:html clazz="main" isPrint="true" fixRows="1">
<form name="template" method="post" action="hosProfitDeptPeriod.jspviewhigh">
	<!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
	<html:title clazz='module'>住院科室</html:title>
  <!-- 查询项 -->
  <html:table clazz="simple">
  	<tr>
    	<td class="normalText" width="40">　</td>
      <td nowrap class="signText" width="72">分析单位：</td>
      <td>
           <%String[][] s={{"month","月"},
                           {"quarter","季"},
                           {"year","年"}
                           };%>

         <input:vanish name="type" options="<%=s%>" cssclass="selectBg">
       </td>
       <td colspan="2">
         <input:vanishEle >
           <table>
             <tr>
               <td nowrap class="signText">基期:</td><td><input:yearM name="beginYearM"></input:yearM></td>
               <td>&nbsp;&nbsp;</td>
               <td nowrap class="signText">计划期:</td><td><input:yearM name="endYearM"></input:yearM></td>
             </tr>
           </table>
         </input:vanishEle>
         <input:vanishEle >
           <table>
             <tr>
               <td nowrap class="signText">基期:</td><td><input:yearQ name="beginYearQ"></input:yearQ></td>
               <td>&nbsp;&nbsp;</td>
               <td nowrap class="signText">计划期:</td><td><input:yearQ name="endYearQ"></input:yearQ></td>
             </tr>
           </table>
         </input:vanishEle>
         <input:vanishEle >
           <table>
             <tr>
               <td nowrap class="signText">基期:</td><td><input:year name="beginYear" ></input:year></td>
               <td>&nbsp;&nbsp;</td>
               <td nowrap class="signText">计划期:</td><td><input:year name="endYear"></input:year></td>
             </tr>
           </table>
         </input:vanishEle>
         </input:vanish>
       </td>
       <td nowrap width="100"><button class="pageBtn" onclick="return find();">计算</button></td>
       <%if(result!=null){%>
       <td nowrap width="100"><button class="pageBtn" onclick="return preparedPrint();">打印</button></td>
       <%}%>
       <td class="normalText" width="20">　</td>
     </tr>
  </html:table>

  <br>

  <!-- 查询结果 -->
  <html:title clazz='table'>住院科室收益预测(基期收益率法)</html:title>


	     <html:webprint clazz='new' >
         <colgroup id='tg'>
            <col style = 'width:200px'>
            <col style = 'width:125px'>
            <col style = 'width:125px'>
            <col style = 'width:120px'>
            <col style = 'width:125px'>
            <col style = 'width:120px'>
          </colgroup>
          <html:tr clazz='label'>
            <td class="resultLabel" nowrap>住院科室</td>
          	<td class="resultLabel" nowrap>基期收益总额<br>(元)</td>
            <td class="resultLabel" nowrap>基期收入总额<br>(元)</td>
            <td class="resultLabel" nowrap>基期收益率%</td>
            <td class="resultLabel" nowrap>计划收入总额<br>(元)</td>
            <td class="resultLabel" nowrap>计划收益<br>(元)</td>
          </html:tr>
          </html:webprint>
        <%if(result!=null){%>
          <div style='border:1px solid #000000;overflow:auto;height:378px'>
            <table border='1' bgColor=white borderColor=black style='border:2px solid #000000;BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
               <colgroup id='tg'>
            <col style = 'width:200px'>
            <col style = 'width:125px'>
            <col style = 'width:125px'>
            <col style = 'width:120px'>
            <col style = 'width:125px'>
            <col style = 'width:120px'>
          </colgroup>
         <%
            for (int i = 0; i < result.length; i++ ) {
		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
        %>
	        <tr CLASS="<%=rowColor%>">
            <td class="normalText" nowrap style="text-align:left"><%=result[i][0]%></td>
            <td nowrap class="numberText"><%=changeFormat(result[i][1])%></td>
            <td nowrap class="numberText"><%=changeFormat(result[i][2])%></td>
            <td nowrap class="numberText"><%=changeRateFormat(result[i][3])%></td>
            <td nowrap class="numberText"><%=changeFormat(result[i][4])%></td>
            <td nowrap class="numberText"><%=changeFormat(result[i][5])%></td>
	        </tr>
		    <%
		          }
        %>
          </table>
        </div>
        <%
          }
		    %>

  <input type='hidden' name="subFunction"/>
</form>
</html:html>
