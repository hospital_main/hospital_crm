<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/allanalysis/analysis/detailprint41.xsl,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:150mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
			</colgroup>
			<thead>
				<tr noWrap="true" class="mainHead">
					<th noWrap="true">科室</th>
					<th noWrap="true">收 入</th>
					<th noWrap="true">成本</th>
					<th noWrap="true">收益</th>
					<th noWrap="true">成本收益率</th>
					<th noWrap="true">工作量</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
									<td><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:when test="position()=5">
									<td align="right" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td align="right" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
