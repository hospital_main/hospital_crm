<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:vh="http://www.w3.org/1999/XSL/Transforms">
	<xsl:template match="/">
	<thead>

	</thead>
	<tbody>
		<!--收益状况分析-->
		<tr>
			<td colspan="9" id="synthesisinfanalyTd1" style="background-color:#FFE488; font-size:13px; font-weight: bold;">收益状况分析</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" rowspan="2" class="mainHead" >项目</th>
			<th noWrap="true" colspan="4" class="mainHead" >全院</th>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<th noWrap="true" rowspan="2" class="mainHead" >药占比</th>
			<th noWrap="true" colspan="3" class="mainHead" >每职工</th>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" class="mainHead" >收入</th>
			<th noWrap="true" class="mainHead" >成本</th>
			<th noWrap="true" class="mainHead" >收益</th>
			<th noWrap="true" class="mainHead" >成本收益率</th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" class="mainHead" >收入</th>
			<th noWrap="true" class="mainHead" >成本</th>
			<th noWrap="true" class="mainHead" >收益</th>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=11]">
			<tr>          
				<xsl:for-each select="td[position()!=1]">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<td>
								<a href="#">
									<xsl:attribute name="onclick" >
										openDialog('detail11.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px')
									</xsl:attribute>
								<xsl:value-of select="."/></a>
							</td>
						</xsl:when>
						<xsl:when test="position()=5 or position()=6">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>  	
		<!--收益状况分析-->
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" rowspan="2" class="mainHead" >项目</th>
			<th noWrap="true" colspan="4" class="mainHead" >医疗（含药品）</th>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<th noWrap="true" colspan="4" class="mainHead" >药品</th>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" class="mainHead" >收入</th>
			<th noWrap="true" class="mainHead" >成本</th>
			<th noWrap="true" class="mainHead" >收益</th>
			<th noWrap="true" class="mainHead" >成本收益率</th>
			<th noWrap="true" class="mainHead" >收入</th>
			<th noWrap="true" class="mainHead" >成本</th>
			<th noWrap="true" class="mainHead" >收益</th>
			<th noWrap="true" class="mainHead" >成本收益率</th>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=12]">
			<tr>          
				<xsl:for-each select="td[position()!=1]">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<td>
								<a href="#">
									<xsl:attribute name="onclick" >
										openDialog('detail12.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px', result)
									</xsl:attribute>
								<xsl:value-of select="."/></a>
							</td>
						</xsl:when>
						<xsl:when test="position()=5 or position()=9">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>  
		<!--成本状况分析-->
		<tr>
			<td colspan="9" id="synthesisinfanalyTd2" style="background-color:#FFE488; font-size:13px; font-weight: bold;" >成本状况分析</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr id="viewstr"><td id="views" height="196" colspan="9" ></td></tr>
		<tr style="display:none" noWrap="true" class="mainHead">
			<th noWrap="true" class="mainHead" >分类方法</th>
			<th noWrap="true" class="mainHead">成本分类</th>
			<td noWrap="true" class="mainHead" >金额</td>
			<td noWrap="true" class="mainHead" >百分比</td>
			<td noWrap="true" class="mainHead" colspan="2">分类方法</td>
			<td style="display:none"></td>
			<td noWrap="true" class="mainHead" >成本分类</td>
			<th noWrap="true" class="mainHead" >金额</th>
			<td noWrap="true" class="mainHead" >百分比</td>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=2]">
			<xsl:variable name="pos" select="position()"/>
			<tr style="display:none">          
				<xsl:for-each select="td[position()!=1]">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<xsl:if test="1=$pos">
								<td rowspan="8">科室类别</td>
							</xsl:if>
							<xsl:if test="1!=$pos">
								<td style="display:none"></td>
							</xsl:if>
						</xsl:when>
						<xsl:when test="position()=6">
							<xsl:if test="1=$pos">
								<td rowspan="2" colspan="2">成本习性</td><td style="display:none"></td>
							</xsl:if>
							<xsl:if test="3=$pos">
								<td rowspan="6" colspan="2">
									<a href="#" onclick="alert('未定');openDialog('未知.html','dialogWidth:850px;dialogHeight:540px',result)">项目构成</a>
								</td>
								<td style="display:none"></td>
							</xsl:if>
							<xsl:if test="1!=$pos and 3!=$pos">
								<td style="display:none"></td>
							</xsl:if>
						</xsl:when>
						<xsl:when test="position()=2">
							<td>
								<a href="#">
									<xsl:attribute name="onclick" >
										openDialog('detail21.html?load=&lt;dateYear&gt;<xsl:value-of select="../pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="../pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="../pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;itemCode&gt;<xsl:value-of select="../td[2]"/>&lt;/itemCode&gt;&lt;itemName&gt;<xsl:value-of select="../td[3]"/>&lt;/itemName&gt;', 'dialogWidth:850px;dialogHeight:540px', result)
									</xsl:attribute>
								<xsl:value-of select="."/></a>
							</td>
						</xsl:when>
						<xsl:when test="position()=7">
							<td>
								<a href="#">
									<xsl:attribute name="onclick" >
										openDialog('detail21.html?load=&lt;dateYear&gt;<xsl:value-of select="../pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="../pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="../pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;itemCode&gt;<xsl:value-of select="../td[7]"/>&lt;/itemCode&gt;&lt;itemName&gt;<xsl:value-of select="../td[8]"/>&lt;/itemName&gt;', 'dialogWidth:850px;dialogHeight:540px', result)
									</xsl:attribute>
								<xsl:value-of select="."/></a>
							</td>
						</xsl:when>
						<xsl:when test="position()=3 ">
							<xsl:if test="7!=$pos and 8!=$pos">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:if>
							<xsl:if test="7=$pos or 8=$pos">
							<td ></td>
							</xsl:if>
						</xsl:when>
						<xsl:when test=" position()=8">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:when test="position()=4 ">
							<xsl:if test="7!=$pos and 8!=$pos">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
							</xsl:if>
							<xsl:if test="7=$pos or 8=$pos">
							<td ></td>
							</xsl:if>
						</xsl:when>
						<xsl:when test="position()=9">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
						<xsl:when test="position()=5">
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
		<!--保本点分析-->
		<tr>
			<td colspan="9" id="synthesisinfanalyTd3" style="background-color:#FFE488; font-size:13px; font-weight: bold;">本量利分析</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>               
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" class="mainHead" >项目</th>
			<th  noWrap="true" class="mainHead" >固定成本</th>
			<td noWrap="true" class="mainHead">变动成本</td>
			<td noWrap="true" class="mainHead">工作量</td>
			<td noWrap="true" class="mainHead">单位收入</td>
			<th noWrap="true" class="mainHead">单位变动成本</th>
			<th noWrap="true" class="mainHead">单位收益</th>
			<td noWrap="true" class="mainHead">保本工作量</td>
			<td noWrap="true" class="mainHead">保本收入</td>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=3]">
			<tr>          
				<xsl:for-each select="td[position()!=1]">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<td>
							<xsl:if test=".='门诊'">
								<a href="#">								
									<xsl:attribute name="onclick" >
								 		openDialog('safepoint/out/main.html?load=&lt;dateYear&gt;<xsl:value-of select="../pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="../pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="../pk/dateToMonth"/>&lt;/dateToMonth&gt;','dialogWidth:850px;dialogHeight:540px',result)
								 	</xsl:attribute>
								 	<xsl:value-of select="."/>
								</a>
							</xsl:if>
							
							<xsl:if test=".='住院'">
								<a href="#">								
									<xsl:attribute name="onclick" >
								 		openDialog('safepoint/in/main.html?load=&lt;dateYear&gt;<xsl:value-of select="../pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="../pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="../pk/dateToMonth"/>&lt;/dateToMonth&gt;','dialogWidth:850px;dialogHeight:540px',result)
								 	</xsl:attribute>
								 	<xsl:value-of select="."/>
								</a>
							</xsl:if>
							
							<xsl:if test=".='医技'">
								<a href="#">								
									<xsl:attribute name="onclick" >
								 		openDialog('safepoint/tec/main.html?load=&lt;dateYear&gt;<xsl:value-of select="../pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="../pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="../pk/dateToMonth"/>&lt;/dateToMonth&gt;','dialogWidth:850px;dialogHeight:540px',result)
								 	</xsl:attribute>
								 	<xsl:value-of select="."/>
								</a>
							</xsl:if>
							</td>
						</xsl:when>
						<xsl:when test="position()=8">
							<td class="numberText">
							<xsl:if test="../td[3]!=''">
								<a href="#">
									<xsl:attribute name="onclick" >
										openChart('cbcs/decide/costpoint/queryCostpointChart.jsp?dept=<xsl:value-of select="../td[2]"/>&amp;dateType=month&amp;dateValue=<xsl:value-of select="../pk/dateYear"/><xsl:value-of select="../pk/dateFromMonth"/>&amp;perIncome=<xsl:value-of select="../td[6]"/>&amp;perChangeCost=<xsl:value-of select="../td[7]"/>&amp;fixedCost=<xsl:value-of select="../td[3]"/>');
									</xsl:attribute>
								<xsl:value-of select="format-number(.,'#,##0.00')"/></a>
							</xsl:if>
							
							<xsl:if test="../td[3]=''">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</xsl:if>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each> 
		<!--排名-->
		<tr>
			<td colspan="9" id="synthesisinfanalyTd10" style="background-color:#FFE488; font-size:13px; font-weight: bold;">科室排名分析</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>            
		<tr noWrap="true" class="mainHead" id="synthesisinfanalyTd4">
			<!--&lt;open_type&gt;hos_all_ana&lt;open_type&gt;-->
			<th noWrap="true" colspan="4" class="mainHead" >临床科室盈余前5名 <a href="#" ><xsl:attribute name="onclick">openDialog('../../../dept/hospcostanalysis/synthesisanaly/deptprofitlost/main.html?load=&lt;date_year&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateYear"/>&lt;/date_year&gt;&lt;date_month&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateFromMonth"/>&lt;/date_month&gt;&lt;date_month_end&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateToMonth"/>&lt;/date_month_end&gt;&lt;incomeStats&gt;1&lt;/incomeStats&gt;','dialogWidth:850px;dialogHeight:540px',result)</xsl:attribute>更多...</a></th>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<th noWrap="true" colspan="5" class="mainHead" >临床科室亏损科室前5名 <a href="#" ><xsl:attribute name="onclick">openDialog('../../../dept/hospcostanalysis/synthesisanaly/deptprofitlost/main.html?load=&lt;date_year&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateYear"/>&lt;/date_year&gt;&lt;date_month&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateFromMonth"/>&lt;/date_month&gt;&lt;date_month_end&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateToMonth"/>&lt;/date_month_end&gt;&lt;incomeStats&gt;2&lt;/incomeStats&gt;','dialogWidth:850px;dialogHeight:540px',result)</xsl:attribute>更多...</a></th>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr noWrap="true" class="mainHead"> 
			<th noWrap="true" class="mainHead" >排名</th>
			<th noWrap="true" class="mainHead" >科室</th>
			<th noWrap="true" class="mainHead" >收益</th>
			<th noWrap="true" class="mainHead" >收益率</th>
			<th noWrap="true" class="mainHead" >排名</th>
			<th noWrap="true" class="mainHead" >科室</th>
			<th noWrap="true" class="mainHead" >收益</th>
			<th noWrap="true" class="mainHead" >收益率</th>
			<th noWrap="true" class="mainHead" ></th>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=4]">
			<xsl:variable name="pos" select="position()"/>
			<tr>          
				<xsl:for-each select="td[position()!=1]">
					<xsl:choose> 
						<xsl:when test="position()=1 or position()=5">
							<td align="center"><xsl:value-of select="$pos"/></td>
						</xsl:when>
						<xsl:when test="position()=2 or position()=6">
							<xsl:variable name="codepos" select="position()"/>
							<td>
								<xsl:value-of select="."/>
							</td>
						</xsl:when>
						<xsl:when test="position()=7">
							<td align="center" style='display:none'></td>
						</xsl:when>
						<xsl:when test="position()=3 or position()=8">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				<td></td>
			</tr>
		</xsl:for-each>  	
	</tbody>
	</xsl:template>
</xsl:stylesheet>
