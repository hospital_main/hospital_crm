<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
  <root>
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
  		<tr noWrap="true" class="mainHead">
  			<td noWrap="true" >成本分类</td>
  			<td noWrap="true" >金额</td>
  			<td noWrap="true" >百分比</td>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
  			<tr>
  				<xsl:for-each select="td">
  					<xsl:choose>
  						<xsl:when test="position()=1">
  							<td><xsl:value-of select="."/></td>
  						</xsl:when>
  						<xsl:when test="position()=2">
  							<td align="right" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
  						</xsl:when>
  						<xsl:when test="position()=3">
  							<td align="right" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
  						</xsl:when>
  					</xsl:choose>
  				</xsl:for-each>
  			</tr>
  		</xsl:for-each>
  	</tbody>
  	<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:2;align:right"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>
