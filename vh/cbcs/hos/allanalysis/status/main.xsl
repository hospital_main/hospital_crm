<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
			<tr noWrap="true" class="mainHead">
				<th noWrap="true">项目</th>
				<th noWrap="true">收入</th>    
				<th noWrap="true">成本</th>
				<th noWrap="true">收益</th>
				<th noWrap="true">门诊工作量</th>
				<th noWrap="true">住院工作量</th>
				<th noWrap="true">实际占用总床日数</th>
				<th noWrap="true">出院人数</th>
				<th noWrap="true">手术台次</th>
				<th noWrap="true">医保统筹人次</th>
				<th noWrap="true">医保统筹人均费用</th>
		  </tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:choose>
						<xsl:when test="position()=1 or position()=2 or position()=3">
						<xsl:for-each select="td">
							<td>
								<xsl:choose> 
									<xsl:when test="position()=1">
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=2 or position()=3 or position()=4 or position()=5 or position()=6 or position() &gt; 6">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:when>
								</xsl:choose>
							</td>
						</xsl:for-each>
						</xsl:when>
						<xsl:when test="position()=4">
						<xsl:for-each select="td">
							<td>
								<xsl:choose> 
									<xsl:when test="position()=1">
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=2 or position()=3 or position()=4 or position()=5 or position()=6">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</xsl:when>
								</xsl:choose>
							</td>
						</xsl:for-each>
						</xsl:when>
					</xsl:choose>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
