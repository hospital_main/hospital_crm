<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/targetforecast/experiment/experimentTargetAnalyse.jsp,v 1.1 2012/03/12 01:58:27 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:27 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<%String[][] result = (String[][])request.getAttribute("result");%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
 function showith(chang,change){
   if(Math.abs(document.getElementById(chang).value)>0){
     document.getElementById(change).value=Math.abs(document.getElementById(chang).value)
     document.getElementById(chang).value='-'+document.getElementById(change).value
   }else{
     document.getElementById(change).value=Math.abs(document.getElementById(chang).value)
     document.getElementById(chang).value=''
     }
   }
  function analyse(){
    template.subFunction.value = "analyse";
    show_wait();
    template.submit();
  }
  function back(){
    template.subFunction.value = "return";
    show_wait();
    template.submit();
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="hosTargetExperiment.jspviewhigh">

	<html:message/>

	<html:title clazz='module'>收益敏感实验</html:title>

  <html:table clazz="simple">
  	<tr>
    	<td class="normalText" width="40">　</td>
    	<td class="normalText">　</td>
      <td class="normalText">　</td>
      <td class="normalText">　</td>
      <td class="normalText">　</td>
     	<td nowrap align="right"><button class="pageBtn" onclick="return analyse();">分析</button>
		<button class="pageBtn" onclick="return preparedPrint();">打印</button>
		<button class="pageBtn" onclick="return back();">返回</button></td>
      <td class="normalText" width="20">　</td>
  	</tr>
  </html:table>

  <br>

	<html:title clazz='table'>收益敏感实验(<%=request.getParameter("dateValue")%>)</html:title>
  <html:table clazz="complex">
  <!-- 结果集 -->
  	<tr>
    	<td>
      	<html:table clazz="result" id="table">
					<html:tr clazz='label'>
    				<td class="resultLabel" colspan="2">影响收益因素</td>
            <td class="resultLabel" rowspan="2">变动程度%</td>
    				<td class="resultLabel" colspan="2">影响范围</td>
    				<td class="resultLabel" rowspan="2">增加收入</td>
    				<td class="resultLabel" rowspan="2">降低成本</td>
    				<td class="resultLabel" rowspan="2">增加收益</td>
    				<td class="resultLabel" rowspan="2">影响程度</td>
          </html:tr>
          <html:tr clazz='label'>
         	  <td class="resultLabel" >因素</td>
    				<td class="resultLabel" >基础数据</td>
    				<td class="resultLabel" >业务收入</td>
    				<td class="resultLabel" >医疗成本</td>
  				</html:tr>
  				  <%
      				if(result!=null){
        				request.getSession().setAttribute("result",result);
            %>
  				<tr CLASS="rowWhite">
						<td class="normalText" style="text-align:left">一、门诊</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr CLASS="rowGray">
						<td class="normalText" style="text-align:left"> 1.门诊量</td>
						<td class="numberText" ><%=changeFormat(result[0][0],"#0.00")%>人次</td>
      			<input type='hidden' name="value0" value="<%=changeFormat(result[0][0],"#0.00")%>"/>
      			<td style="align:center">
            	<input:text name="change0" dVal="<%=changeRateFormat(result[0][1])%>" type="number" numType="float"  cssclass="textInputA" maxlength="5">
     						<input:textError errorCode="min" value="0">变动程度不应小于0</input:textError>
 							</input:text>
						</td>
						<td class="numberText" ><%=changeFormat(result[0][2])%>人次</td>
						<td class="numberText" >**</td>
						<td class="numberText" ><%=changeFormat(result[0][4])%>元</td>
						<td class="numberText" >**</td>
						<td class="numberText" ><%=changeFormat(result[0][5])%>元</td>
						<td class="numberText" ><%=result[0][6]%></td>
					</tr>
					<tr CLASS="rowWhite">
						<td class="normalText" style="text-align:left">2.单位业务收入</td>
						<td class="numberText" ><%=changeFormat(result[1][0])%>元</td>
      			<input type='hidden' name="value1" value="<%=changeFormat(result[1][0],"#0.00")%>"/>
						<td style="align:center">
 							<input:text name="change1"  dVal="<%=changeRateFormat(result[1][1])%>" type="number" numType="float" cssclass="textInputA" maxlength="5">
     						<input:textError errorCode="min" value="0">变动程度不应小于0</input:textError>
 							</input:text>
						</td>
						<td class="numberText" ><%=changeFormat(result[1][2])%>元</td>
						<td class="numberText" >**</td>
						<td class="numberText" ><%=changeFormat(result[1][4])%>元</td>
						<td class="numberText" >**</td>
						<td class="numberText" ><%=changeFormat(result[1][4])%>元</td>
						<td class="numberText" ><%=result[1][6]%></td>
					</tr>
					<tr CLASS="rowGray">
						<td class="normalText" style="text-align:left">3.单位变动成本</td>
						<td class="numberText" ><%=changeFormat(result[2][0])%>元</td>
      			<input type='hidden' name="value2" value="<%=changeFormat(result[2][0],"#0.00")%>"/>
						<td style="align:center" >
						  <input type='text' name='chang2'  id ='chang2'  class='textInputA' value='<%=(Double.parseDouble(changeRateFormat(result[2][1]).replaceAll(",",""))>0? "-"+changeRateFormat(result[2][1]).replaceAll(",",""):"0.00")%>' maxlength='5'  onKeyPress='return isFloat(event)'  onblur=" if(isNum( 'chang2' )){ showith('chang2','change2');}"  />
    				  <input type='hidden' id='change2' name='change2' value='<%=Double.parseDouble(result[2][1])*100%>' >
						</td>
						<td class="numberText" >**</td>
						<td class="numberText" ><%=(Double.parseDouble(changeFormat(result[2][3]).replaceAll(",",""))>0? "-"+changeFormat(result[2][3]):"0.00")%>元</td>
						<td class="numberText" >**</td>
						<td class="numberText" ><%=changeFormat(result[2][7])%>元</td>
						<td class="numberText" ><%=changeFormat(result[2][5])%>元</td>
						<td class="numberText" ><%=result[2][6]%></td>
					</tr>
    			<tr CLASS="rowWhite">
						<td class="normalText" style="text-align:left">4.固定成本</td>
						<td class="numberText" ><%=changeFormat(result[3][0])%>元</td>
            <input type='hidden' name="value3" value="<%=changeFormat(result[3][0],"#0.00")%>"/>
						<td style="align:center">
						  <input type='text' name='chang3'  id ='chang3'  class='textInputA' value='<%=(Double.parseDouble(changeRateFormat(result[3][1]).replaceAll(",",""))>0? "-"+changeRateFormat(result[3][1]).replaceAll(",",""):"0.00")%>' maxlength='5'  onKeyPress='return isFloat(event)'  onblur=" if(isNum( 'chang3' )){showith('chang3','change3');}"  />
    				  <input type='hidden' id='change3' name='change3' value='<%=Double.parseDouble(result[3][1])*100%>'>
						</td>
						<td class="numberText" >**</td>
						<td class="numberText" ><%=(Double.parseDouble(changeFormat(result[3][3]).replaceAll(",",""))>0? "-"+changeFormat(result[3][3]):"0.00")%>元</td>
						<td class="numberText" >**</td>
						<td class="numberText" ><%=changeFormat(result[3][7])%>元</td>
						<td class="numberText" ><%=changeFormat(result[3][5])%>元</td>
						<td class="numberText" ><%=result[3][6]%></td>
					</tr>
					<tr CLASS="rowGray">
						<td class="normalText" style="text-align:left">二、住院</td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
            <td >&nbsp;</td>
						<td >&nbsp;</td>
					</tr>
					<tr CLASS="rowWhite">
						<td class="normalText" style="text-align:left"> 1.床日数</td>
						<td class="numberText" ><%=changeFormat(result[4][0],"#0.00")%>床日</td>
      			<input type='hidden' name="value4" value="<%=changeFormat(result[4][0],"#0.00")%>"/>
						<td style="align:center">
 							<input:text name="change4"  dVal="<%=changeRateFormat(result[4][1])%>" type="number" numType="float" cssclass="textInputA" maxlength="5">
     						<input:textError errorCode="min" value="0">变动程度不应小于0</input:textError>
     						<input:textError errorCode="max" value="1000">变动程度不应大于1000</input:textError>
 							</input:text>
						</td>
						<td class="numberText" ><%=changeFormat(result[4][2])%>床日</td>
						<td class="numberText" >**</td>
						<td class="numberText" ><%=changeFormat(result[4][4])%>元</td>
						<td class="numberText" >**</td>
						<td class="numberText" ><%=changeFormat(result[4][5])%>元</td>
						<td class="numberText" ><%=result[4][6]%></td>
					</tr>
					<tr CLASS="rowGray">
						<td class="normalText" style="text-align:left"> 2.单位业务收入</td>
						<td class="numberText" ><%=changeFormat(result[5][0])%>元</td>
       			<input type='hidden' name="value5" value="<%=changeFormat(result[5][0],"#0.00")%>"/>
						<td style="align:center">
 							<input:text name="change5"  dVal="<%=changeRateFormat(result[5][1])%>" type="number" numType="float" cssclass="textInputA" maxlength="5">
     						<input:textError errorCode="min" value="0">变动程度不应小于0</input:textError>
     						<input:textError errorCode="max" value="1000">变动程度不应大于1000</input:textError>
 							</input:text>
						</td>
						<td class="numberText" ><%=changeFormat(result[5][2])%>元</td>
      			<td class="numberText" >**</td>
						<td class="numberText" ><%=changeFormat(result[5][4])%>元</td>
						<td class="numberText" >**</td>
						<td class="numberText" ><%=changeFormat(result[5][4])%>元</td>
						<td class="numberText" ><%=result[5][6]%></td>
					</tr>
					<tr CLASS="rowWhite">
						<td class="normalText" style="text-align:left">3.单位变动成本</td>
						<td class="numberText" ><%=changeFormat(result[6][0])%>元</td>
       			<input type='hidden' name="value6" value="<%=changeFormat(result[6][0],"#0.00")%>"/>
						<td style="align:center">
						  <input type='text' name='chang6'  id ='chang6'  class='textInputA' value='<%=(Double.parseDouble(changeRateFormat(result[6][1]).replaceAll(",",""))>0? "-"+changeRateFormat(result[6][1]).replaceAll(",",""):"0.00")%>' maxlength='5'  onKeyPress='return isFloat(event)'  onblur=" if(isNum( 'chang6' )){ showith('chang6','change6');}"  />
    				  <input type='hidden' id='change6' name='change6' value='<%=Double.parseDouble(result[6][1])*100%>'>
					</td>
					<td class="numberText" >**</td>
					<td class="numberText" ><%=(Double.parseDouble(changeFormat(result[6][3]).replaceAll(",",""))>0? "-"+changeFormat(result[6][3]):"0.00")%>元</td>
					<td class="numberText" >**</td>
					<td class="numberText" ><%=changeFormat(result[6][7])%>元</td>
					<td class="numberText" ><%=changeFormat(result[6][5])%>元</td>
					<td class="numberText" ><%=result[6][6]%></td>
				</tr>
				<tr CLASS="rowGray">
					<td class="normalText" style="text-align:left">4.固定成本</td>
					<td class="numberText" ><%=changeFormat(result[7][0])%>元</td>
          <input type='hidden' name="value7" value="<%=changeFormat(result[7][0],"#0.00")%>"/>
					<td style="align:center">
					  <input type='text' name='chang7'  id ='chang7'  class='textInputA' value='<%=(Double.parseDouble(changeRateFormat(result[7][1]).replaceAll(",",""))>0? "-"+changeRateFormat(result[7][1]).replaceAll(",",""):"0.00")%>' maxlength='5'  onKeyPress='return isFloat(event)'  onblur=" if(isNum( 'chang7' )){ showith('chang7','change7');}"  />
   				  <input type='hidden' id='change7' name='change7' value='<%=Double.parseDouble(result[7][1])*100%>'>
					</td>
     			<td class="numberText" >**</td>
     			<td class="numberText" ><%=(Double.parseDouble(changeFormat(result[7][3]).replaceAll(",",""))>0? "-"+changeFormat(result[7][3]):"0.00")%>元</td>
     			<td class="numberText" >**</td>
     			<td class="numberText" ><%=changeFormat(result[7][7])%>元</td>
     			<td class="numberText" ><%=changeFormat(result[7][5])%>元</td>
     			<td class="numberText" ><%=result[7][6]%></td>
				</tr>
  	<%}%>
  		 </html:table>
     </td>
   </tr>
 </html:table>
  <input type='hidden' name="subFunction" value="findByCondition"/>
  <input type='hidden' name="dateValue" value='<%=request.getParameter("dateValue")%>' />
  <input type='hidden' name="type" value='<%=request.getParameter("type")%>'/>
  <input type='hidden' name="yearMonth" value='<%=request.getParameter("yearMonth")%>'/>
  <input type='hidden' name="yearQuarter" value='<%=request.getParameter("yearQuarter")%>'/>
  <input type='hidden' name="year" value='<%=request.getParameter("year")%>'/>
</form>
</html:html>
