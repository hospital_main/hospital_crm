<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/targetforecast/experiment/experimentTargetAccount.jsp,v 1.1 2012/03/12 01:58:27 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:27 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<%String[][] result = (String[][])request.getAttribute("result");%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
   function showith(chang,change){
     if(Math.abs(document.getElementById(chang).value)>1000){
     alert("变动程度不能大于1000")
     document.getElementById(chang).value=1000
     }
     document.getElementById(change).value=Math.abs(document.getElementById(chang).value)
     document.getElementById(chang).value='-'+document.getElementById(change).value
   }
   function back(){
    template.subFunction.value = "return";
    show_wait();
    template.submit();
  }
  function analyse(){
    template.subFunction.value = "analyse";
    show_wait();
    template.submit();
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="hosTargetExperiment.jspviewhigh">
	<!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
	<html:title clazz='module'>收益敏感实验</html:title>
 <!-- 查询项 -->
  <html:table clazz="simple">
  	<tr>
      <td class="normalText">　</td>
      <td class="normalText">　</td>
      <td class="normalText">　</td>
      <td class="normalText">　</td>
      <td class="normalText" width="20">　</td>
  	</tr>
	<tr>
	 <td  colspan="3" class="normalText" width="20">　</td>
	 <td nowrap align="right" colspan="3"><button class="pageBtn" onclick="return analyse();">分析</button><button class="pageBtn" onclick="return preparedPrint();">打印</button><button class="pageBtn" onclick="return back();">返回</button></td>
	</tr>
  </html:table>
  <br>

  <!-- 查询结果 -->
  <html:title clazz='table'>收益敏感实验(<%=judgeTitle()%>)</html:title>
  <html:table clazz="complex">
  <!-- 结果集 -->
  	<tr>
    	<td>
      	<html:table clazz="result" id="table">
        	<html:tr clazz='label'>
          	<td class="resultLabel" colspan="2">影响收益因素</td>
          	<td class="resultLabel" rowspan="2">变动程度%</td>
          	<td class="resultLabel" colspan="2">影响范围</td>
          	<td class="resultLabel" rowspan="2">增加收入</td>
            <td class="resultLabel" rowspan="2">降低成本</td>
          	<td class="resultLabel" rowspan="2">增加收益</td>
          	<td class="resultLabel" rowspan="2">影响程度</td>
        	</html:tr>
        	<html:tr clazz='label'>
          	<td class="resultLabel" >因素</td>
          	<td class="resultLabel" >基础数据</td>
          	<td class="resultLabel" >业务收入</td>
          	<td class="resultLabel" >医疗成本</td>
        	</html:tr>
          	<%
             if(result!=null){
                request.getSession().setAttribute("result",result);
	          %>
          <tr CLASS="rowWhite">
    				<td class="normalText">一、门诊</td>
    				<td>&nbsp;</td>
    				<td>&nbsp;</td>
    				<td>&nbsp;</td>
    				<td>&nbsp;</td>
    				<td>&nbsp;</td>
    				<td>&nbsp;</td>
    				<td>&nbsp;</td>
            <td>&nbsp;</td>
  				</tr>
  				<tr CLASS="rowGray">
    				<td class="normalText"> 1.门诊量</td>
					<td class="numberText"><%=changeFormat(result[0][0],"#0.00")%>人次</td>
            <input type='hidden' name="value0" value="<%=changeFormat(result[0][0],"#0.00")%>"/>
            <td style="align:center">
            	<input:text name="change0"  dVal="<%=result[0][1]%>" type="number" numType="float"  cssclass="textInputA" maxlength="7">
           			<input:textError errorCode="min" value="0">变动程度不应小于0</input:textError>
           			<input:textError errorCode="max" value="1000">变动程度不应大于1000</input:textError>
       				</input:text>
    				</td>
    				<td class="numberText"><%=result[0][2]%>人次</td>
    				<td class="numberText">**</td>
    				<td class="numberText"><%=result[0][4]%>元</td>
            <td class="numberText">**</td>
    				<td class="numberText"><%=result[0][5]%>元</td>
    				<td class="numberText"><%=result[0][6]%>%</td>
  				</tr>
  				<tr CLASS="rowWhite">
    				<td class="normalText">2.单位业务收入</td>
					<td class="numberText"><%=changeFormat(result[1][0])%>元</td>
            <input type='hidden' name="value1" value="<%=changeFormat(result[1][0],"#0.00")%>"/>
    				<td style="align:center">
       				<input:text name="change1"  dVal="<%=result[1][1]%>" type="number" numType="float" cssclass="textInputA" maxlength="7">
           			<input:textError errorCode="min" value="0">变动程度不应小于0</input:textError>
           			<input:textError errorCode="max" value="1000">变动程度不应大于1000</input:textError>
       				</input:text>
    				</td>
    				<td class="numberText"><%=result[1][2]%>元</td>
    				<td class="numberText">**</td>
    				<td class="numberText"><%=result[1][4]%>元</td>
            <td class="numberText">**</td>
    				<td class="numberText"><%=result[1][5]%>元</td>
    				<td class="numberText"><%=result[1][6]%>%</td>
  				</tr>
  				<tr CLASS="rowGray">
    				<td class="normalText">3.单位变动成本</td>
					<td class="numberText"><%=changeFormat(result[2][0])%>元</td>
            <input type='hidden' name="value2" value="<%=changeFormat(result[2][0],"#0.00")%>"/>
    				<td style="align:center">
       				<input type='text' name='chang2'  id ='chang2'  class='textInputA' value='' maxlength='5'  onKeyPress='return isFloat(event)'  onblur=" if(isNum( 'chang2' )&&!Hiszero('chang2')){ showith('chang2','change2');}"  /> 
    				  <input type='hidden' id='change2' name='change2' >
    				</td>
    				<td class="numberText">**</td>
    				<td class="numberText"><%=result[2][2]%>元</td>
    				<td class="numberText">**</td>
            <td class="numberText"><%=result[2][7]%>元</td>
    				<td class="numberText"><%=result[2][5]%>元</td>
    				<td class="numberText"><%=result[2][6]%>%</td>
  				</tr>
          <tr CLASS="rowWhite">
    				<td class="normalText">4.固定成本</td>
					<td class="numberText"><%=changeFormat(result[3][0])%>元</td> 
            <input type='hidden' name="value3" value="<%=changeFormat(result[3][0],"#0.00")%>"/>
    				<td style="align:center">
       				<input type='text' name='chang3'  id ='chang3'  class='textInputA' value='' maxlength='5'  onKeyPress='return isFloat(event)'  onblur=" if(isNum( 'chang3' )&&!Hiszero('chang3')){showith('chang3','change3');}"  /> 
    				  <input type='hidden' id='change3' name='change3'>
    				</td>
    				<td class="numberText">**</td>
    				<td class="numberText"><%=result[3][3]%>元</td>
    				<td class="numberText">**</td>
            <td class="numberText"><%=result[3][7]%>元</td>
    				<td class="numberText"><%=result[3][5]%>元</td>
    				<td class="numberText"><%=result[3][6]%>%</td>
  				</tr>
  				<tr CLASS="rowGray">
    				<td class="normalText">二、住院</td>
    				<td >&nbsp;</td>
    				<td >&nbsp;</td>
    				<td >&nbsp;</td>
    				<td >&nbsp;</td>
    				<td >&nbsp;</td>
    				<td >&nbsp;</td>
    				<td >&nbsp;</td>
            <td >&nbsp;</td>
  				</tr>
  				<tr CLASS="rowWhite">
    				<td class="normalText"> 1.床日数</td>
					 <td class="numberText"><%=changeFormat(result[4][0],"#0.00")%>床日</td>
            <input type='hidden' name="value4" value="<%=changeFormat(result[4][0],"#0.00")%>"/>
    				<td style="align:center">
       				<input:text name="change4"  dVal="<%=result[4][1]%>" type="number" numType="float" cssclass="textInputA" maxlength="5">
           			<input:textError errorCode="min" value="0">变动程度不应小于0</input:textError>
       				</input:text>
    				</td>
    				<td class="numberText"><%=result[4][2]%>床日</td>
    				<td class="numberText">**</td>
    				<td class="numberText"><%=result[4][4]%>元</td>
            <td class="numberText">**</td>
    				<td class="numberText"><%=result[4][5]%>元</td>
    				<td class="numberText"><%=result[4][6]%>%</td>
  				</tr>
  				<tr CLASS="rowGray">
    				<td class="normalText"> 2.单位业务收入</td>
					<td class="numberText"><%=changeFormat(result[5][0])%>元</td>
            <input type='hidden' name="value5" value="<%=changeFormat(result[5][0],"#0.00")%>"/>
    				<td style="align:center">
       				<input:text name="change5"  dVal="<%=result[5][1]%>" type="number" numType="float" cssclass="textInputA" maxlength="5">
           			<input:textError errorCode="min" value="0">变动程度不应小于0</input:textError>
       				</input:text>
    				</td>
    				<td class="numberText"><%=result[5][2]%>元</td>
            <td class="numberText">**</td>
    				<td class="numberText"><%=result[5][4]%>元</td>
            <td class="numberText">**</td>
    				<td class="numberText"><%=result[5][5]%>元</td>
    				<td class="numberText"><%=result[5][6]%>%</td>
  				</tr>
  				<tr CLASS="rowWhite">
    				<td class="normalText">3.单位变动成本</td>
					 <td class="numberText"><%=changeFormat(result[6][0])%>元</td>
            <input type='hidden' name="value6" value="<%=changeFormat(result[6][0],"#0.00")%>"/>
    				<td style="align:center">
       				<input type='text' name='chang6'  id ='chang6'  class='textInputA' value='' maxlength='5'  onKeyPress='return isFloat(event)'  onblur=" if(isNum( 'chang6' )&&!Hiszero('chang6')){ showith('chang6','change6');}"  /> 
    				  <input type='hidden' id='change6' name='change6' value='0'> 
    				</td>
    				<td class="numberText">**</td>
    				<td class="numberText"><%=result[6][3]%>元</td>
    				<td class="numberText">**</td>
            <td class="numberText"><%=result[6][7]%>元</td>
    				<td class="numberText"><%=result[6][5]%>元</td>
    				<td class="numberText"><%=result[6][6]%>%</td>
  				</tr>
  				<tr CLASS="rowGray">
    				<td class="normalText">4.固定成本</td>
					<td class="numberText"><%=changeFormat(result[7][0])%>元</td>
            <input type='hidden' name="value7" value="<%=changeFormat(result[7][0],"#0.00")%>"/>
    				<td style="align:center">
              <input type='text' name='chang7'  id ='chang7'  class='textInputA' value='' maxlength='5'  onKeyPress='return isFloat(event)'  onblur=" if(isNum( 'chang7' )&&!Hiszero('chang7')){ showith('chang7','change7');}"  /> 
   				    <input type='hidden' id='change7' name='change7' value='0'> 
   				 </td>
           <td class="numberText">**</td>
           <td class="numberText"><%=result[7][3]%>元</td>
           <td class="numberText">**</td>
           <td class="numberText"><%=result[7][7]%>元</td>
           <td class="numberText"><%=result[7][5]%>元</td>
           <td class="numberText"><%=result[7][6]%>%</td>
         </tr>
          <%}%>
       </html:table>
     </td>
    </tr>
	</html:table>
  <input type='hidden' name="subFunction" value="findByCondition"/>
  <input type='hidden' name="dateValue" value="<%=judgeTitle()%>"/>

  <input type='hidden' name="type" value='<%=request.getParameter("type")%>'/>
  <input type='hidden' name="yearMonth" value='<%=request.getParameter("yearMonth")%>'/>
  <input type='hidden' name="yearQuarter" value='<%=request.getParameter("yearQuarter")%>'/>
  <input type='hidden' name="year" value='<%=request.getParameter("year")%>'/>
</form>
</html:html>
