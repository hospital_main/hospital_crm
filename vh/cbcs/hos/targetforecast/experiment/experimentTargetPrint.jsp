<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/targetforecast/experiment/experimentTargetPrint.jsp,v 1.1 2012/03/12 01:58:27 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:27 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>
<%@ page import="com.viewhigh.cbcs.base.util.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function preparedPrin() {
     // 报表名称
    grid.prn.title1='收益敏感实验';

    <%
      String title= request.getParameter("dateValue");
    %>
    // 年月
    grid.prn.title2="<%=title%>";
    // 表头行数
    grid.prn.tabHead=2;
    // 打印
    grid.print();
     history.back(-1);
  }

</Script>
<html xmlns:fc>
<head>
  <title>登录用户：测试</title>
  <meta http-equiv='Content-Type' content='text/html; charset=GBK'>
  <link rel='stylesheet' href='vh.css' type='text/css'>
  <link rel='stylesheet' href='newVh.css' type='text/css'>
  <style>fc\:webprint {behavior: url(webprint/webgrid.htc);}</style>
  <script language='javascript' src='javascript/common.js'></script>
  <script language='javascript1.2'>
    function window_onload() {
      grid.width=840
      grid.height=448
      grid.FixRows=2
      grid.FixCols=3
      grid.prn.printFixCols=1

      //调整行高和列高
      grid.AdjustRowHeight=false;
      grid.AdjustColWidth=false;

      grid.ReadOnly=true;

      //设置每次请求页面时刷新缓存
      grid.prn.changeUserDate=true;

      //表头行数
      grid.prn.tabHead=1;

      //是否套打
      grid.prn.coverPrint= false;

      //纸张大小 428（W）*299（H）单位：毫米(mm)
      grid.prn.repWidth='<%=Preference.getWidth()%>';
      grid.prn.repHeight='<%=Preference.getHeight()%>';
      //上下左边距
      grid.prn.tabTop='<%=Preference.getTop()%>';
      grid.prn.tabBottom='<%=Preference.getBottom()%>';
      grid.prn.tabLeft='<%=Preference.getLeft()%>';
      grid.prn.tabRight='<%=Preference.getRight()%>';
      //是否横向打印
      grid.prn.level='<%=Preference.getTransverse()%>';
      grid.prn.portrait='<%=!Preference.getTransverse()%>';
      //是否自动转行
      grid.prn.AutoturnRow=false;
      //宽度超宽时压缩到一页上
      //grid.prn.compressWidth=1;
      //表格超宽超高压缩时压缩表格字体
      //grid.prn.compressFont=0;
      //inprint值为0时表示打印时先进行预览
      grid.prn.inPrint=0;
      //设置标题与附加信息
      grid.prn.top21='编制单位：'+'<%=Preference.getCom_name()%>';
      grid.prn.top22='';
      grid.prn.top13='页号：(p)/(P)';
      grid.prn.top33='金额单位：元';
      grid.prn.bottom13='.';
      grid.prn.bottom23='打印日期：'+'<%=ExtendTool.getTime().toString().substring(0, 19)%>'+'      制表人：'+'<%=LoginUser.getUser_name(request)%>';
      grid.prn.bottom33='北京望海康信科技有限公司 制作';
      grid.prn.fontTop='30';
      grid.prn.fontBottom='12';
      if (parent._1st_current!=null) parent.show();
    }
  </script>
</head>
<body bgcolor='#FFFFFF' rightMargin='0' onload='window_onload();preparedPrin();'>
<img id='_image' src='img/loading.gif' style='display:none'/>
<div id='_process'>
<form name="template" method="post" action="hosTargetExperiment.jspviewhigh">
  <html:webprint clazz='new' >
  	<colgroup id='tg'>
      <col style = <%=DisplayWidth.NAME_WIDTH%>>
      <col style = <%=DisplayWidth.MONEY_WIDTH%>>
      <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
      <col style = <%=DisplayWidth.MONEY_WIDTH%>>
      <col style = <%=DisplayWidth.MONEY_WIDTH%>>
      <col style = <%=DisplayWidth.MONEY_WIDTH%>>
      <col style = <%=DisplayWidth.MONEY_WIDTH%>>
      <col style = <%=DisplayWidth.MONEY_WIDTH%>>
    </colgroup>
		<html:tr clazz='label'>
    	<td class="resultLabel" nowrap colspan="2">影响收益因素</td>
    	<td class="resultLabel" nowrap rowspan="2">变动程度%</td>
    	<td class="resultLabel" nowrap colspan="2">影响范围</td>
    	<td class="resultLabel" nowrap rowspan="2">增加收入</td>
    	<td class="resultLabel" nowrap rowspan="2">增加收益</td>
    	<td class="resultLabel" nowrap rowspan="2">影响程度</td>
  	</html:tr>
  	<html:tr clazz='label'>
    	<td class="resultLabel" >因素</td>
    	<td class="resultLabel" >基础数据</td>
    	<td class="resultLabel" >业务收入</td>
    	<td class="resultLabel" >医疗成本</td>
  	</html:tr>
		<%
	  String[][] result = (String[][])request.getSession().getAttribute("result");
	 	if(result!=null){     //  添加
		%>
  	<tr CLASS="rowWhite">
			<td class="normalText" style="text-align:left">一、门诊</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr CLASS="rowGray">
			<td class="normalText" style="text-align:left"> 1.门诊量</td>
			<td class="numberText" ><%=changeFormat(result[0][0],"#,##0")%>人次</td>
      <input type='hidden' name="value0" value="<%=result[0][0]%>"/>
      <td style="align:center"><%=changeRateFormat(result[0][1])%></td>
			<td class="numberText" ><%=changeFormat(result[0][2])%>人次</td>
			<td class="numberText" >**</td>
			<td class="numberText" ><%=changeFormat(result[0][4])%>元</td>
			<td class="numberText" ><%=changeFormat(result[0][5])%>元</td>
			<td class="numberText" ><%=result[0][6]%></td>
		</tr>
		<tr CLASS="rowWhite">
			<td class="normalText" style="text-align:left">2.单位业务收入</td>
			<td class="numberText" ><%=changeFormat(result[1][0])%>元</td>
      <input type='hidden' name="value1" value="<%=result[1][0]%>"/>
			<td style="align:center"><%=changeRateFormat(result[1][1])%></td>
			<td class="numberText" ><%=changeFormat(result[1][2])%>元</td>
			<td class="numberText" >**</td>
			<td class="numberText" ><%=changeFormat(result[1][4])%>元</td>
			<td class="numberText" ><%=changeFormat(result[1][5])%>元</td>
			<td class="numberText" ><%=result[1][6]%></td>
		</tr>
		<tr CLASS="rowGray">
			<td class="normalText" style="text-align:left">3.单位变动成本</td>
			<td class="numberText" ><%=changeFormat(result[2][0])%>元</td>
			<td style="align:center" ><%=changeRateFormat(result[2][1])%></td>
			<td class="numberText" >**</td>
			<td class="numberText" ><%=changeFormat(result[2][3])%>元</td>
			<td class="numberText" >**</td>
			<td class="numberText" ><%=changeFormat(result[2][5])%>元</td>
			<td class="numberText" ><%=result[2][6]%></td>
		</tr>
    <tr CLASS="rowWhite">
			<td class="normalText" style="text-align:left">4.固定成本</td>
			<td class="numberText" ><%=changeFormat(result[3][0])%>元</td>
			<td style="align:center"><%=changeRateFormat(result[3][1])%></td>
			<td class="numberText" >**</td>
			<td class="numberText" ><%=changeFormat(result[3][3])%>元</td>
			<td class="numberText" >**</td>
			<td class="numberText" ><%=changeFormat(result[3][5])%>元</td>
			<td class="numberText" ><%=result[3][6]%></td>
		</tr>
		<tr CLASS="rowGray">
			<td class="normalText" style="text-align:left">二、住院</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
		</tr>
		<tr CLASS="rowWhite">
			<td class="normalText" style="text-align:left"> 1.床日数</td>
			<td class="numberText" ><%=changeFormat(result[4][0],"#,###0")%>床日</td>
			<td style="align:center"><%=changeRateFormat(result[4][1])%></td>
			<td class="numberText" ><%=changeFormat(result[4][2])%>床日</td>
			<td class="numberText" >**</td>
			<td class="numberText" ><%=changeFormat(result[4][4])%>元</td>
			<td class="numberText" ><%=changeFormat(result[4][5])%>元</td>
			<td class="numberText" ><%=result[4][6]%></td>
		</tr>
		<tr CLASS="rowGray">
			<td class="normalText" style="text-align:left"> 2.单位业务收入</td>
			<td class="numberText" ><%=changeFormat(result[5][0])%>元</td>
			<td style="align:center"><%=changeRateFormat(result[5][1])%></td>
			<td class="numberText" ><%=changeFormat(result[5][2])%>元</td>
      <td class="numberText" >**</td>
			<td class="numberText" ><%=changeFormat(result[5][4])%>元</td>
			<td class="numberText" ><%=changeFormat(result[5][5])%>元</td>
			<td class="numberText" ><%=result[5][6]%></td>
		</tr>
		<tr CLASS="rowWhite">
			<td class="normalText" style="text-align:left">3.单位变动成本</td>
			<td class="numberText" ><%=changeFormat(result[6][0])%>元</td>
			<td style="align:center"><%=changeRateFormat(result[6][1])%></td>
			<td class="numberText" >**</td>
			<td class="numberText" ><%=changeFormat(result[6][3])%>元</td>
			<td class="numberText" >**</td>
			<td class="numberText" ><%=changeFormat(result[6][5])%>元</td>
			<td class="numberText" ><%=result[6][6]%></td>
		</tr>
		<tr CLASS="rowGray">
			<td class="normalText" style="text-align:left">4.固定成本</td>
			<td class="numberText" ><%=changeFormat(result[7][0])%>元</td>
			<td style="align:center"><%=changeRateFormat(result[7][1])%></td>
     	<td class="numberText" >**</td>
     	<td class="numberText" ><%=changeFormat(result[7][3])%>元</td>
     	<td class="numberText" >**</td>
     	<td class="numberText" ><%=changeFormat(result[7][5])%>元</td>
     	<td class="numberText" ><%=result[7][6]%></td>
		</tr>
  	<%}%>
  </html:webprint>
  <input type='hidden' name="subFunction" value="findByCondition"/>
</form>
</div>
<script language='javascript'>
function show_wait() {
  document.getElementById('_image').style.display=''
  document.getElementById('_process').style.display='none'
}
</script>
</body>
</html>

