<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
				<th noWrap="true">会计科目</th>
				<th noWrap="true">收费类别</th>
			        <th noWrap="true">会计收入</th>
				<th noWrap="true">成本收入</th>
				<th noWrap="true">是否相等</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
		 <tr>
		 	<xsl:variable name="td3" select="td[3]"/>
		 	<xsl:variable name="td4" select="td[4]"/>
			<xsl:for-each select="td">
	              		<xsl:choose>
	                		<xsl:when test="position()=1 or position()=2">
	                		 	<td height="22">
	                			   <xsl:value-of select="."/>
	                			</td>
	                		</xsl:when>
	                		<xsl:otherwise>
	                		 	<td noWrap="true" class="numberText">
		                   			<xsl:if test=". != '' and . !=0">
		                   			<xsl:value-of select="format-number(.,'#,##0.00')"/>
		                   			</xsl:if>
	                   			</td>
	                		</xsl:otherwise>
	                	</xsl:choose>
			 </xsl:for-each>
			
			 <xsl:choose>
	                		<xsl:when test="$td3=$td4">
	                		 	<td noWrap="true" bgcolor="green">
	                			   相等
	                			</td>
	                		</xsl:when>
	                		<xsl:otherwise>
	                			<td noWrap="true" bgcolor="red">
	                			   不相等
	                			</td>
	                		</xsl:otherwise>
	                </xsl:choose>
  		</tr>
   	</xsl:for-each>
  	</tbody>
  </xsl:template>
</xsl:stylesheet>
