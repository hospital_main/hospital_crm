<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th noWrap="true">药品编码</th>
				<th noWrap="true">药品名称</th>
				<th noWrap="true">规格型号</th>
				<th noWrap="true">计量单位</th>
				<th noWrap="true">药品分类名称</th>
				<th noWrap="true">药品成本分类</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center' style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td[position()!=5 and position()!=6 and position()!=7]">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td align="center">
                    <a tabindex='-1'><xsl:value-of select="."/></a>
                  </td>
                </xsl:when>
                
                <xsl:otherwise>
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  </xsl:template>
</xsl:stylesheet>
