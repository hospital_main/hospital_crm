<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>	  		    		
			<th noWrap="true">服务项目代码</th>
			<th noWrap="true">服务项目名称</th>
			<th></th>
     	</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>     			
				<xsl:for-each select="td">
					<xsl:choose>
						
						<xsl:when test="position()=3">
							<td align='center'><div>
		            <input type='checkbox' TABINDEX='-1' style='font-size:8px;' >
		      			  <xsl:if test="text() = '1'">
		      			    <xsl:attribute name="checked"><xsl:value-of select="."/></xsl:attribute>
		      			  </xsl:if>
		    			  </input></div>
		          </td>	
						</xsl:when>
						
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
							
			</tr>			
		</xsl:for-each> 		
	  </tbody> 	
 	</xsl:template>
</xsl:stylesheet>


