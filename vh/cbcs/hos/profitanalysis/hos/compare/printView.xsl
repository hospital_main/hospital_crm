<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">  
				<td noWrap="true" rowspan="2">项目</td>
				<td noWrap="true">本期</td>
				<td noWrap="true" colspan="3">预算</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">上期</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">同期</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">院平均</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
				<td style="display:none"/>
				<td noWrap="true">数值</td>
			  <td noWrap="true">数值</td>
				<td noWrap="true">差异</td>
		    <td noWrap="true">差异率</td>
			  <td noWrap="true">数值</td>
				<td noWrap="true">差异</td>
		    <td noWrap="true">差异率</td>
			  <td noWrap="true">数值</td>
				<td noWrap="true">差异</td>
				<td noWrap="true">差异率</td>
        <td noWrap="true">数值</td>
				<td noWrap="true">差异</td>
		    <td noWrap="true">差异率</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td[position()!=1 and position()!=2 and position()!=17]">
            <xsl:choose>
              <xsl:when test="position()=1">
                <xsl:if test="../td[2]='yes'">
                  <td align="center"><xsl:value-of select="."/></td>
                </xsl:if>
                <xsl:if test="../td[2]='no'">
                  <td><xsl:value-of select="."/></td>
                </xsl:if>
              </xsl:when>
              <xsl:when test="position()=5 or position()=8 or position()=11 or position()=14">
                <xsl:choose>
                  <xsl:when test="../td[2]='yes'">
                    <td></td>
                  </xsl:when>
                  <xsl:when test="../td[2]='no' and . &lt; ../td[17]">
                    <td class="numberText">
                      <xsl:attribute name="style">font-weight:bold;color:orange</xsl:attribute>
                      <xsl:value-of select="format-number(.,'#,##0.00%')"/>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td class="numberText">
                      <xsl:value-of select="format-number(.,'#,##0.00%')"/>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:if test="../td[2]='no'">
                 <xsl:if test="../td[3]='药占比' or contains(../td[3],'率')">
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
                 </xsl:if>
                 <xsl:if test="../td[3]!='药占比' and not(contains(../td[3],'率'))">
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                 </xsl:if>
                </xsl:if>
                <xsl:if test="../td[2]='yes'">
                  <td></td>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:13;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>
