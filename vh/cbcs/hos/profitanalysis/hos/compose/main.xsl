<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
		   <xsl:variable name="flag" select="count(/root/tbody/tr)" />
				<xsl:if test="$flag > 0">
        				<th noWrap="true">@active_1</th>
      				</xsl:if>
				<th noWrap="true">收入</th>
				<th noWrap="true">成本</th>
				<th noWrap="true">收益</th>
				<th noWrap="true">比例</th>
				<!--th noWrap="true">比例图</th-->
			</tr>	
		</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center' style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
              	<xsl:when test="position()=1">
	  	            <xsl:value-of select="."/>
	                </xsl:when>
                <xsl:when test="position()=2">
	                <xsl:attribute name='class'>numberText</xsl:attribute>
	  	            <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </xsl:when>
                <xsl:when test="position()=3">
	                <xsl:attribute name='class'>numberText</xsl:attribute>
	  	            <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </xsl:when>
                <xsl:when test="position()=4">
	                <xsl:attribute name='class'>numberText</xsl:attribute>
	  	            <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </xsl:when>
                <xsl:when test="position()=5">
	                <xsl:attribute name='class'>numberText</xsl:attribute>
	  	            <xsl:value-of select="format-number(.,'##0.00%')"/>
	                </xsl:when>
                <xsl:otherwise>
                 
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
          <!--td>
          	<table border="0" height="12"  cellpadding='0' cellspacing='0' >
                  	<xsl:attribute name="width"><xsl:value-of select="td[3]"/>%</xsl:attribute>
                  	<tr><td bgcolor="blue"></td></tr>
             </table> 
          </td-->
        </tr>
      </xsl:for-each>
    </tbody>
		</xsl:template>
</xsl:stylesheet>
