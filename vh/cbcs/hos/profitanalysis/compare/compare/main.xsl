<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
		     <th noWrap="true">����</th>
				<xsl:variable name="flag" select="count(/root/tbody/tr)" />
				<xsl:if test="$flag > 0">
        				<th noWrap="true">@active_1</th>
      				</xsl:if>
				<th noWrap="true">���</th>
				<th noWrap="true">������</th>
		    </tr>
		</thead>	
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td">
						<td>
						<xsl:variable name="ppp" select="position()"/>
							<xsl:choose> 
								<xsl:when test="position()=1 ">
									<xsl:value-of select="."/>
								</xsl:when>
						<xsl:when test="position()=5">
	                <xsl:attribute name='class'>numberText</xsl:attribute>
	  	            <xsl:value-of select="format-number(.,'#,##0.00%')"/>
	                </xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>	



