<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<xsl:variable name="tdValue" select="/root/tbody/tr[td[2]='医技科室']/td[1]"/>
	<root>
		<!--colgroup>		 
  	  <xsl:for-each select="/root/tbody/tr[td[2]='医技科室']/td[position()!=1]">
  	    <col style = 'width:180mm'/>
      </xsl:for-each>
		</colgroup-->
		<thead>
			<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				
				<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;2]">
        	<td style="display:none"></td>
       	</xsl:for-each>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				
				<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;2]">
        <td style="display:none"></td>
      </xsl:for-each>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"></td>
				
				<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;2]">
        <td style="display:none"></td>
      </xsl:for-each>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"></td>
				
				<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;2]">
        <td style="display:none"></td>
      </xsl:for-each>
  		</tr>
			<tr noWrap="true" class="mainHead">
				<xsl:for-each select="/root/tbody/tr[td[2]='医技科室']/td[position()!=1]">
					<xsl:choose>
			    	<xsl:when test="position()=1">
			    		<td noWrap="true" rowspan="2"><xsl:value-of select="."/></td>
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		<td noWrap="true"><xsl:value-of select="format-number(.,'###0')"/></td>
			    	</xsl:when>
			    	<xsl:when test="(position() mod 2)=1">
			    		<td noWrap="true" colspan="2"><xsl:value-of select="format-number(.,'###0')"/></td>
			    		<td noWrap="true" style="display:none"><xsl:value-of select="format-number(.,'###0')"/></td>
			    	</xsl:when>              	
          </xsl:choose>
	    	</xsl:for-each>
	    </tr>
	    <tr noWrap="true" class="mainHead">
				<xsl:for-each select="/root/tbody/tr[td[2]='医技科室']/td[position()!=1]">
					<xsl:choose>
            <xsl:when test="position()=1">
			    		<td noWrap="true" style="display:none"><xsl:value-of select="."/></td>
			    	</xsl:when>
			    	<xsl:when test="position()=2">
              <xsl:if test="../td[1]='yes'">
			    		  <td noWrap="true">收益</td>
			    		</xsl:if>
			    		<xsl:if test="../td[1]='no'">
			    		  <td noWrap="true">收益率</td>
			    		</xsl:if>
			    	</xsl:when>   
			    	<xsl:when test="position()>2 and (position() mod 2)=1">
			    	  <xsl:if test="../td[1]='yes'">
  			    		<td noWrap="true">收益</td>
  			    		<td noWrap="true">比值</td>
  			    	</xsl:if>
  			    	<xsl:if test="../td[1]='no'">
  			    		<td noWrap="true">收益率</td>
  			    		<td noWrap="true">比值</td>
  			    	</xsl:if>
			    	</xsl:when>           	
          </xsl:choose>
	    	</xsl:for-each>
	    </tr>
		</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr[td[2]!='医技科室']">
        <tr>
          <xsl:for-each select="td[position()!=1]">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
	                <xsl:value-of select="."/>
	              </td>
              </xsl:when>
              <xsl:when test="position()=2">
               <xsl:if test="$tdValue = 'yes'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
               </xsl:if>
               <xsl:if test="$tdValue = 'no'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
               </xsl:if>
              </xsl:when>
              <xsl:when test="position()>1 and (position() mod 2)=1">
               <xsl:if test="$tdValue = 'yes'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
               </xsl:if>
               <xsl:if test="$tdValue = 'no'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
               </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    <tfoot>
    <tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"></td>
				
				<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;2]">
        <td style="display:none"></td>
      </xsl:for-each>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"></td>
				
				<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;2]">
        <td style="display:none"></td>
      </xsl:for-each>
  		</tr>
    </tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>
