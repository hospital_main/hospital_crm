<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<xsl:variable name="tdValue" select="/root/tbody/tr[td[2]='医技科室']/td[1]"/>
	<root>
		<!--colgroup>		       
			<xsl:for-each select="/root/tbody/tr[td[2]='医技科室']/td[position()!=1 and position()!=last()]">
  	    <col style = 'width:65mm'/>
      </xsl:for-each>
		</colgroup-->
		<thead>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">  
				<td noWrap="true" rowspan="2">医技科室</td>
				<td noWrap="true">本期</td>
				<td noWrap="true" colspan="3">预算</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">上期</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">同期</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">院平均</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
				<td style="display:none"/>
				<td noWrap="true">收益</td>
			  <td noWrap="true">收益</td>
				<td noWrap="true">差异</td>
		    <td noWrap="true">差异率</td>
			  <td noWrap="true">收益</td>
				<td noWrap="true">差异</td>
		    <td noWrap="true">差异率</td>
			  <td noWrap="true">收益</td>
				<td noWrap="true">差异</td>
				<td noWrap="true">差异率</td>
        <td noWrap="true">收益</td>
				<td noWrap="true">差异</td>
		    <td noWrap="true">差异率</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[td[2]!='医技科室']">
				<tr>          
					<xsl:for-each select="td[position()!=1 and position()!=last()]">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position()=2">
               <xsl:if test="$tdValue = 'yes'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
               </xsl:if>
               <xsl:if test="$tdValue = 'no'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
               </xsl:if>
              </xsl:when>
              <xsl:when test="(position() mod 3)=0">
               <xsl:if test="$tdValue = 'yes'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
               </xsl:if>
               <xsl:if test="$tdValue = 'no'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
               </xsl:if>
              </xsl:when>
              <xsl:when test="((position()-1) mod 3)=0">
               <xsl:if test="$tdValue = 'yes'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
               </xsl:if>
               <xsl:if test="$tdValue = 'no'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
               </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test=". &lt; ../td[last()]">
                    <td class="numberText">
                      <xsl:attribute name="style">font-weight:bold;color:orange</xsl:attribute>
                      <xsl:value-of select="format-number(.,'#,##0.00%')"/>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td class="numberText">
                      <xsl:value-of select="format-number(.,'#,##0.00%')"/>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:13;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>
