<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<xsl:variable name="tdValue" select="/root/tbody/tr[td[2]='�ٴ�����']/td[1]"/>
	<root>
<thead>
			<tr noWrap="true" class="mainHead"> 
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/> 
				<xsl:for-each select="/root/tbody/tr[td[2]='�ٴ�����']/td[position()!=last()]">
					<xsl:choose>
            <xsl:when test="position()=1">
			    		
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		
			    	</xsl:when>
			    	<xsl:when test="position()=3">
              <td style="display:none"/>
			    	</xsl:when>   
			    	<xsl:when test="position()>3 and ((position()-1) mod 3)=0">
			    	  <td style="display:none"/>
			    	  <td style="display:none"/>
			    	  <td style="display:none"/>
			    	</xsl:when>           	
          </xsl:choose>
	    	</xsl:for-each>
	    </tr>
			<tr noWrap="true" class="mainHead"> 
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/> 
				<xsl:for-each select="/root/tbody/tr[td[2]='�ٴ�����']/td[position()!=last()]">
					<xsl:choose>
            <xsl:when test="position()=1">
			    		
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		
			    	</xsl:when>
			    	<xsl:when test="position()=3">
              <td style="display:none"/>
			    	</xsl:when>   
			    	<xsl:when test="position()>3 and ((position()-1) mod 3)=0">
			    	  <td style="display:none"/>
			    	  <td style="display:none"/>
			    	  <td style="display:none"/>
			    	</xsl:when>           	
          </xsl:choose>
	    	</xsl:for-each>
	    </tr>
			<tr noWrap="true" class="mainHead"> 
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/> 
				<xsl:for-each select="/root/tbody/tr[td[2]='�ٴ�����']/td[position()!=last()]">
					<xsl:choose>
            <xsl:when test="position()=1">
			    		
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		
			    	</xsl:when>
			    	<xsl:when test="position()=3">
              <td style="display:none"/>
			    	</xsl:when>   
			    	<xsl:when test="position()>3 and ((position()-1) mod 3)=0">
			    	  <td style="display:none"/>
			    	  <td style="display:none"/>
			    	  <td style="display:none"/>
			    	</xsl:when>           	
          </xsl:choose>
	    	</xsl:for-each>
	    </tr>
			<tr noWrap="true" class="mainHead"> 
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/> 
				<xsl:for-each select="/root/tbody/tr[td[2]='�ٴ�����']/td[position()!=last()]">
					<xsl:choose>
            <xsl:when test="position()=1">
			    		
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		
			    	</xsl:when>
			    	<xsl:when test="position()=3">
              <td style="display:none"/>
			    	</xsl:when>   
			    	<xsl:when test="position()>3 and ((position()-1) mod 3)=0">
			    	  <td style="display:none"/>
			    	  <td style="display:none"/>
			    	  <td style="display:none"/>
			    	</xsl:when>           	
          </xsl:choose>
	    	</xsl:for-each>
	    </tr>
			<tr noWrap="true" class="mainHead">  
				<xsl:for-each select="/root/tbody/tr[td[2]='�ٴ�����']/td[position()!=last()]">
					<xsl:choose>
            <xsl:when test="position()=1">
			    		
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		<td noWrap="true" rowspan="2"><xsl:value-of select="."/></td>
			    	</xsl:when>
			    	<xsl:when test="position()=3">
			    		<td noWrap="true"><xsl:value-of select="."/></td>
			    	</xsl:when>
			    	<xsl:when test="position()>3 and ((position()-1) mod 3)=0">
			    		<td noWrap="true" colspan="3"><xsl:value-of select="."/></td>
			    		<td noWrap="true" style="display:none"><xsl:value-of select="."/></td>
			    		<td noWrap="true" style="display:none"><xsl:value-of select="."/></td>
			    	</xsl:when>              	
          </xsl:choose>
	    	</xsl:for-each>
	    </tr>
			<tr noWrap="true" class="mainHead">  
				<xsl:for-each select="/root/tbody/tr[td[2]='�ٴ�����']/td[position()!=last()]">
					<xsl:choose>
            <xsl:when test="position()=1">
			    		
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		<td noWrap="true"><xsl:value-of select="."/></td>
			    	</xsl:when>
			    	<xsl:when test="position()=3">
              <xsl:if test="../td[1]='yes'">
			    		  <td noWrap="true">����</td>
			    		</xsl:if>
			    		<xsl:if test="../td[1]='no'">
			    		  <td noWrap="true">������</td>
			    		</xsl:if>
			    	</xsl:when>   
			    	<xsl:when test="position()>3 and ((position()-1) mod 3)=0">
			    	  <xsl:if test="../td[1]='yes'">
  			    		<td noWrap="true">����</td>
  			    		<td noWrap="true">����</td>
  			    		<td noWrap="true">������</td>
  			    	</xsl:if>
  			    	<xsl:if test="../td[1]='no'">
  			    		<td noWrap="true">������</td>
  			    		<td noWrap="true">����</td>
  			    		<td noWrap="true">������</td>
  			    	</xsl:if>
			    	</xsl:when>           	
          </xsl:choose>
	    	</xsl:for-each>
	    </tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[td[2]!='�ٴ�����']">
        <tr>
          <xsl:for-each select="td[position()!=last()]">
            <xsl:choose>
              <xsl:when test="position()=1">
                
              </xsl:when>
               <xsl:when test="position()=2">
                <td><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position()=3">
               <xsl:if test="$tdValue = 'yes'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
               </xsl:if>
               <xsl:if test="$tdValue = 'no'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
               </xsl:if>
              </xsl:when>
              <xsl:when test="((position()-1) mod 3)=0">
               <xsl:if test="$tdValue = 'yes'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
               </xsl:if>
               <xsl:if test="$tdValue = 'no'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
               </xsl:if>
              </xsl:when>
              <xsl:when test="((position()-2) mod 3)=0">
               <xsl:if test="$tdValue = 'yes'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
               </xsl:if>
               <xsl:if test="$tdValue = 'no'">
                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
               </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test=". &lt; ../td[last()]">
                    <td class="numberText">
                      <xsl:attribute name="style">font-weight:bold;color:orange</xsl:attribute>
                      <xsl:value-of select="format-number(.,'#,##0.00%')"/>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td class="numberText">
                      <xsl:value-of select="format-number(.,'#,##0.00%')"/>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each> 	
		</tbody>
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:13;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>
