<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/unitanalysis/profit/abs/printView.xsl,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<colgroup>		       
			<col style = 'width:75mm'/>
			<col style = 'width:95mm'/>	
			<col style = 'width:95mm'/>
			<col style = 'width:95mm'/>	
			<col style = 'width:65mm'/>
			<col style = 'width:95mm'/>
			<col style = 'width:90mm'/>	
			<col style = 'width:65mm'/>
			<col style = 'width:95mm'/>	
			<col style = 'width:95mm'/>
			<col style = 'width:65mm'/>	
			<col style = 'width:95mm'/>
			<col style = 'width:95mm'/>
      <col style = 'width:65mm'/>
			<col style = 'width:95mm'/>
			<col style = 'width:95mm'/>
      <col style = 'width:65mm'/>
			<col style = 'width:95mm'/>
			<col style = 'width:95mm'/>
      <col style = 'width:65mm'/>
			<col style = 'width:95mm'/>
			<col style = 'width:95mm'/>
      <col style = 'width:65mm'/>
	      
		</colgroup>
		<thead>
			<tr noWrap="true" class="mainHead">  
			
				<th noWrap="true" rowspan="2">收费类别</th>
				<th noWrap="true">本期</th>
				<th noWrap="true" colspan="3">预算</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="3">上期</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="3">同期</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="3">院平均</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="3">三甲医院平均</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="3">二级医院平均</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="3">市平均</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
		    </tr>
		    <tr noWrap="true" class="mainHead">  
				<th style="display:none"/>
				<th noWrap="true">数值</th>
			   <th noWrap="true">数值</th>
				<th noWrap="true">差异</th>
		      <th noWrap="true">差异率</th>
			   <th noWrap="true">数值</th>
				<th noWrap="true">差异</th>
		      <th noWrap="true">差异率</th>
			   <th noWrap="true">数值</th>
				<th noWrap="true">差异</th>
				 <th noWrap="true">差异率</th>
             <th noWrap="true">数值</th>
				<th noWrap="true">差异</th>
		      <th noWrap="true">差异率</th>
             <th noWrap="true">数值</th>
				<th noWrap="true">差异</th>
		      <th noWrap="true">差异率</th>
             <th noWrap="true">数值</th>
				<th noWrap="true">差异</th>
		      <th noWrap="true">差异率</th>
             <th noWrap="true">数值</th>
				<th noWrap="true">差异</th>
		      <th noWrap="true">差异率</th>
			</tr>
		</thead>
		  <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td">
						<xsl:choose> 
							<xsl:when test="position()!=1">
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
