<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
				<th>选择</th>
				<xsl:for-each select="/root/tbody/tr[td[1]='项目']/td">
							<th><xsl:value-of select="."/></th>
	    	</xsl:for-each>
	    </tr>
		</thead>
				
		<tbody>
      <xsl:for-each select="/root/tbody/tr[td[1]!='项目']">
        <tr>
          <td align='center'  style='display:block'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td[position()!=24]">
            <td>
              <xsl:choose>
                <xsl:when test="position()=1">
									<xsl:if test="../td[1]='住院' or ../td[1]='门诊' or ../td[1]='医技' or ../td[1]='每职工'">
                  	<xsl:attribute name="bgColor">yellow</xsl:attribute>
									</xsl:if>
	  	            <xsl:value-of select="."/>
                </xsl:when>
                
								<xsl:when test="position()=2">
									<xsl:if test="../td[1]!='住院' and ../td[1]!='门诊' and ../td[1]!='医技' and ../td[1]!='每职工'">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>

 								<xsl:when test="(position() &lt; 15 and ((position()-2) mod 3)!=0)">
									<xsl:if test="../td[1]!='住院' and ../td[1]!='门诊' and ../td[1]!='医技' and ../td[1]!='每职工'">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
								
 								<xsl:when test="(position()>14 and ((position()-2) mod 3)!=0)">
									<xsl:if test="../td[1]!='住院' and ../td[1]!='门诊' and ../td[1]!='医技' and ../td[1]!='每职工' and ../td[1]!='诊次数量' and ../td[1]!='床日数量' and ../td[1]!='单位数量' and ../td[1]!='职工人数'">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
								
 								<xsl:when test="position()>14 and ((position()-2) mod 3)=0">
									<xsl:if test="../td[1]!='住院' and ../td[1]!='门诊' and ../td[1]!='医技' and ../td[1]!='每职工' and ../td[1]!='诊次数量' and ../td[1]!='床日数量' and ../td[1]!='单位数量' and ../td[1]!='职工人数'">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:if test=". &lt; ../td[24]">
											<xsl:attribute name="style">color:red</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>

 								<xsl:when test="position()>3 and ((position()-2) mod 3)=0 and position() &lt; 15">
									<xsl:if test="../td[1]!='住院' and ../td[1]!='门诊' and ../td[1]!='医技' and ../td[1]!='每职工'">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:if test=". &lt; ../td[24]">
											<xsl:attribute name="style">color:red</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
 								
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

