<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		
		<thead>
			<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;colspan:colcount">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td[position() &gt; 1])'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 2]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:subtitle;colspan:colcount">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td[position() &gt; 1])'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 2]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr >
			<td noWrap="true"  class="mainHead" style="fontsize:subtitle;colspan:colcount;align:right">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td[position() &gt; 1])'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 2]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
			<tr noWrap="true" class="mainHead">  
			<td noWrap="true">��Ŀ</td>
		 		<xsl:for-each select="/root/activeHead/cbcsHosCostAnalysisCompareAbs_AcctSelect_head_1/th">
	            <td noWrap="true"><xsl:value-of select="."/></td>
	         </xsl:for-each>
	    </tr>
	    </thead>
	    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test = "position() = 2">
								<td>
									<xsl:value-of select = '.'/>
								</td>	
							</xsl:when>
							<xsl:when test = "position() > 2">
								<td>
									<xsl:attribute name='class'>numberText</xsl:attribute>
									<xsl:attribute name="style">align:right</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>	
						</xsl:choose>	
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
		<tfoot>
		<tr noWrap="true" class="mainHead">
			<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td[position() &gt; 1])'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 2]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr noWrap="true" class="mainHead">
			<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td[position() &gt; 1])'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 2]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
    </tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>