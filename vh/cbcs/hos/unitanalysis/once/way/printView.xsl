<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<!--colgroup>
  	  <xsl:for-each select="/root/tbody/tr[td[1]='科室名称']/td">
  	    <col style = 'width:100mm'/>
      </xsl:for-each>
		</colgroup-->
		<thead>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>

				<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;1]">
        	<td style="display:none"></td>
       	</xsl:for-each>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>

				<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;1]">
        <td style="display:none"></td>
      </xsl:for-each>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"></td>

				<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;1]">
        <td style="display:none"></td>
      </xsl:for-each>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"></td>

				<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;1]">
        <td style="display:none"></td>
      </xsl:for-each>
  		</tr>

			<tr noWrap="true" class="mainHead">
				<xsl:for-each select="/root/tbody/tr[td[1]='科室名称']/td">
						<xsl:choose>
                <xsl:when test="position()=1">
					    		<td noWrap="true" rowspan="2"><xsl:value-of select="."/></td>
					    	</xsl:when>
					    	<xsl:when test="position()=2">
					    		<td noWrap="true">
					    			<xsl:value-of select="format-number(.,'###0')"/>
					    		</td>
					    	</xsl:when>
					    	<xsl:when test="(position() mod 2)=1">
					    		<td noWrap="true" colspan="2">
					    			<xsl:value-of select="format-number(.,'###0')"/>
					    		</td>
					    		<td noWrap="true" style="display:none"><xsl:value-of select="."/></td>
					    	</xsl:when>
              </xsl:choose>
	    	</xsl:for-each>
	    </tr>
	    <tr noWrap="true" class="mainHead">
				<xsl:for-each select="/root/tbody/tr[td[1]!='科室名称']/td">
						<xsl:choose>
                <xsl:when test="position()=1">
					    		<td noWrap="true" style="display:none"></td>
					    	</xsl:when>
					    	<xsl:when test="position()=2">
					    		<td noWrap="true">数值</td>
					    	</xsl:when>
					    	<xsl:when test="(position() mod 2)=1">
					    		<td noWrap="true">数值</td>
					    		<td noWrap="true">比值</td>
					    	</xsl:when>
              </xsl:choose>
	    	</xsl:for-each>
	    </tr>

		</thead>

		<tbody>
      <xsl:for-each select="/root/tbody/tr[td[1]!='科室名称']">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:otherwise>
	                  <td class="numberText">
	                  	<xsl:choose>
		 										<xsl:when test="position()&gt;2 and (position()-4) mod 2 =0">
												<xsl:attribute name="style">align:right</xsl:attribute>
									    			<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									    	</xsl:when>
									    	<xsl:otherwise>
												<xsl:attribute name="style">align:right</xsl:attribute>
									    			<xsl:value-of select="format-number(.,'#,##0.00')"/>
									    	</xsl:otherwise>
								    	</xsl:choose>
	                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    <tfoot>
    <tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"></td>

				<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;2]">
        <td style="display:none"></td>
      </xsl:for-each>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"></td>

				<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;2]">
        <td style="display:none"></td>
      </xsl:for-each>
  		</tr>
    </tfoot>
   </root>
  </xsl:template>
</xsl:stylesheet>
