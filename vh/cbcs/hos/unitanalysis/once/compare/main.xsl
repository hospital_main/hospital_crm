<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
		     <th noWrap="true" rowspan="2">选择</th>
				<th noWrap="true" rowspan="2">收费类别</th>
				<th noWrap="true" colspan="1">本期</th>
				<th noWrap="true" colspan="3">预算</th>
				<th noWrap="true" colspan="3">上期</th>
				<th noWrap="true" colspan="3">同期</th>
				<th noWrap="true" colspan="3">院平均</th>

		    </tr>
		    <tr noWrap="true" class="mainHead">  
				<th noWrap="true" colspan="1">数值</th>
			   <th noWrap="true" colspan="1">数值</th>
				<th noWrap="true" colspan="1">差异</th>
		      <th noWrap="true" colspan="1">差异率</th>
			   <th noWrap="true" colspan="1">数值</th>
				<th noWrap="true" colspan="1">差异</th>
		      <th noWrap="true" colspan="1">差异率</th>
			   <th noWrap="true" colspan="1">数值</th>
				<th noWrap="true" colspan="1">差异</th>
				 <th noWrap="true" colspan="1">差异率</th>
             <th noWrap="true" colspan="1">数值</th>
				<th noWrap="true" colspan="1">差异</th>
		      <th noWrap="true" colspan="1">差异率</th>
			</tr>
		</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:block'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td[position()!=15]">
						<xsl:choose> 
							<xsl:when test="(position()-5) mod 3=0 and position()&gt;4">
									<td class="numberText">
										<xsl:if test=". &lt; ../td[15]">
											<xsl:attribute name="style">font-weight:bold;color:orange</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</td>
							</xsl:when>
							<xsl:when test="position()!=1">
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

