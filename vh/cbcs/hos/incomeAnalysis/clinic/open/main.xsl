<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
				<th noWrap="true">开单科室</th>
				<th noWrap="true">执行科室</th>
				<th noWrap="true">收入</th>
     </tr>
		</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position()=1 or position()=2">
	                </xsl:when>
	                 <xsl:when test="position()=5">
	                <td align="right" ><xsl:value-of select="."/></td>
	                </xsl:when>
                <xsl:otherwise>
                   <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

