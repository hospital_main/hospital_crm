
<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/incomeAnalysis/clinic/open/printView2.xsl,v 1.1 2012/03/12 01:58:12 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:12 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<thead>
		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">  
				<td noWrap="true">开单科室</td>
				<td noWrap="true">执行科室</td>
				<td noWrap="true">收入</td>
	    </tr>
		</thead>
		  <tbody>
		 <xsl:for-each select="/root/tbody/tr">
		  <xsl:variable name="P" select="position()"/>
		 	<xsl:if test="position()=1">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              	<xsl:when test="position()=1 or position()=2">
	              </xsl:when>
	              <xsl:when test="position()=6">
	                <td class="numberText"><xsl:value-of select="."/></td>
	              </xsl:when>
                <xsl:otherwise>
                   <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
		 	</xsl:if>
		 	<xsl:if test="position()>1">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              	<xsl:when test="position()=1 or position()=2">
	              </xsl:when>
              	<xsl:when test="position()=3">
              		<td>
	              		<xsl:if test=" ../../tr[$P - 1]/td[3]=."> 
										</xsl:if>
	              		<xsl:if test=" ../../tr[$P - 1]/td[3] != ."> 
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
	              </xsl:when>
              	<xsl:when test="position()=4">
              	  <td>
	              		<xsl:if test="../../tr[$P - 1]/td[4]=."> 
										</xsl:if>
	              		<xsl:if test="../../tr[$P - 1]/td[4] != ."> 
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
	              </xsl:when>
	              <xsl:when test="position()=6">
	                <td class="numberText"><xsl:value-of select="."/></td>
	              </xsl:when>
                <xsl:otherwise>
                	<td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
        </xsl:if>
      </xsl:for-each>
		</tbody>
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:3;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>
