<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
				<th rowspan="2">选择</th>
				<xsl:for-each select="/root/tbody/tr[td[1]='临床科室']/td">
						<xsl:choose>
                <xsl:when test="position()=1">
					    		<th noWrap="true" rowspan="2"><xsl:value-of select="."/></th>
					    	</xsl:when>
					    	<xsl:when test="position()=2">
					    		<th noWrap="true"><xsl:value-of select="format-number(.,'###0')"/></th>
					    	</xsl:when>
					    	<xsl:when test="(position() mod 2)=1">
					    		<th noWrap="true" colspan="2"><xsl:value-of select="format-number(.,'###0')"/></th>
					    		<th noWrap="true" style="display:none"><xsl:value-of select="format-number(.,'###0')"/></th>
					    	</xsl:when>              	
              </xsl:choose>
	    	</xsl:for-each>
	    </tr>
	    <tr noWrap="true" class="mainHead">  
	    	<th style="display:none"></th>
				<xsl:for-each select="/root/tbody/tr[td[1]='临床科室']/td">
						<xsl:choose>
                <xsl:when test="position()=1">
					    		<th noWrap="true" style="display:none"></th>
					    	</xsl:when>
					    	<xsl:when test="position()=2">
					    		<th noWrap="true">收入</th>
					    	</xsl:when>   
					    	<xsl:when test="(position() mod 2)=1">
					    		<th noWrap="true">收入</th>
					    		<th noWrap="true">比值</th>
					    	</xsl:when>           	
              </xsl:choose>
	    	</xsl:for-each>
	    </tr>

		</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr[td[1]!='临床科室']">
        <tr>
          <td align='center' style='display:block'>
           <xsl:if test="td[1]!= '合计'">
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
             </xsl:if>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td>
  	                <xsl:value-of select="."/>
  	              </td>
                </xsl:when>
                <xsl:when test="position()=2">
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:when test="(position() mod 2)=1">
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:otherwise>
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

