<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2" valign="middle">科室名称</th>
  	  	<th noWrap="true" rowspan="2" valign="middle">科室编码</th>
  	  	<th noWrap="true" colspan="3">门诊收入</th>
  	  	<th style="display:none">门诊收入</th>
  	  	<th style="display:none">门诊收入</th>
  	  	<th noWrap="true" colspan="3">住院收入</th>
  	  	<th style="display:none">住院收入</th>
  	  	<th style="display:none">住院收入</th>
  	  	<th noWrap="true" colspan="3">全院收入</th>
  	  	<th style="display:none">全院收入</th>
  	  	<th style="display:none">全院收入</th>
  	  </tr>
      <tr noWrap="true" class="mainHead">
      	<th style="display:none">科室名称</th>
      	<th style="display:none">科室编码</th> 	
  			<th noWrap="true">本期</th>
  			<th noWrap="true">同期</th>
  			<th noWrap="true">增长率</th>
  			<th noWrap="true">本期</th>
  			<th noWrap="true">同期</th>
  			<th noWrap="true">增长率</th>
  			<th noWrap="true">本期</th>
  			<th noWrap="true">同期</th>
  			<th noWrap="true">增长率</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1">
		          	<td>
         					<xsl:value-of select="."/>
        			  </td>
							</xsl:when>
							<xsl:when test="position()=2">
		          	<td>
         					<xsl:value-of select="."/>
        			  </td>
							</xsl:when>
			    		<xsl:otherwise>
			          <td align='right'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>		                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>