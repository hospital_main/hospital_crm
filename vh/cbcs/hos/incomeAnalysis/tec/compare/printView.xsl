<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/incomeAnalysis/tec/compare/printView.xsl,v 1.1 2012/03/12 01:58:12 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:12 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" rowspan="2">医技科室</td>
				<td noWrap="true">本期</td>
				<td noWrap="true" colspan="3">预算</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>

				<td noWrap="true" colspan="3">上期</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>

				<td noWrap="true" colspan="3">同期</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>

				<td noWrap="true" colspan="3">院平均</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>


		    </tr>
		    <tr noWrap="true" class="mainHead">
				<td style="display:none"/>
				<td noWrap="true">收入</td>
			   <td noWrap="true">收入</td>
				<td noWrap="true">差异</td>
		      <td noWrap="true">差异率</td>
			   <td noWrap="true">收入</td>
				<td noWrap="true">差异</td>
		      <td noWrap="true">差异率</td>
			   <td noWrap="true">收入</td>
				<td noWrap="true">差异</td>
				 <td noWrap="true">差异率</td>
             <td noWrap="true">收入</td>
				<td noWrap="true">差异</td>
		      <td noWrap="true">差异率</td>
			</tr>
		</thead>
		  <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()!=1">
								<td class="numberText">
									<xsl:attribute name="style">align:right</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
									<xsl:if test="position()>3 and ((position()-2) mod 3)=0">%
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:13;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>
