<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/incomeAnalysis/compare/abs/printView.xsl,v 1.1 2012/03/12 01:58:12 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:12 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<colgroup>		       
	  		<col style = 'width:90m'/>
	      <xsl:for-each select="/root/activeHead/cbcsHosIncomeAnalysisCompareAbs_Select_head_2/th">
	      	<col style = 'width:90m'/>
	      </xsl:for-each>
		</colgroup>
		<thead>
			<tr noWrap="true" class="mainHead"> 
		 		<xsl:for-each select="/root/activeHead/cbcsHosIncomeAnalysisCompareAbs_Select_head_1/th">
	         	<th noWrap="true"><xsl:value-of select="."/></th>
	         </xsl:for-each>
	         
	         <xsl:for-each select="/root/activeHead/cbcsHosIncomeAnalysisCompareAbs_Select_head_2/th">
	            <th noWrap="true"><xsl:value-of select="."/></th>
	         </xsl:for-each>
	         
	    	</tr>
	    </thead>
	    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=2 ">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test = "position() >=2 ">
								<td>
									<xsl:attribute name='class'>numberText</xsl:attribute>
	  	            <xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
						</xsl:choose>	
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
