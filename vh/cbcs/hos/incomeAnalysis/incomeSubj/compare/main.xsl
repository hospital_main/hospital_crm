<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
				<th noWrap="true" rowspan="2">选择</th>
				<xsl:for-each select="/root/tbody/tr[td[1]='收费类别' or td[1]='收入项目']/td">
					<xsl:choose>
            <xsl:when test="position()=1">
			    		<th noWrap="true" rowspan="2"><xsl:value-of select="."/></th>
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		<th noWrap="true"><xsl:value-of select="."/></th>
			    	</xsl:when>
			    	<xsl:when test="(position() mod 3)=0 and position()!=15">
			    		<th noWrap="true" colspan="3"><xsl:value-of select="."/></th>
			    		<th noWrap="true" style="display:none"><xsl:value-of select="."/></th>
			    		<th noWrap="true" style="display:none"><xsl:value-of select="."/></th>
			    	</xsl:when>              	
          </xsl:choose>
	    	</xsl:for-each>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
		    <th style="display:none"></th>
				<xsl:for-each select="/root/tbody/tr[td[1]='收费类别' or td[1]='收入项目']/td">
					<xsl:choose>
            <xsl:when test="position()=1">
			    		<th noWrap="true" style="display:none"></th>
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		<th noWrap="true">收入</th>
			    	</xsl:when>   
			    	<xsl:when test="(position() mod 3)=0 and position()!=15">
			    		<th noWrap="true">收入</th>
			    		<th noWrap="true">差异</th>
			    		<th noWrap="true">差异率</th>
			    	</xsl:when>           	
          </xsl:choose>
	    	</xsl:for-each>
			</tr>
		</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr[td[1]!='收费类别' and td[1]!='收入项目']">
        <tr>
          <td align='center' style='display:block'>
           <xsl:if test="td[1]!= '合计'">
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
             </xsl:if>
          </td>
          <xsl:for-each select="td[position()!=15]">
            <td>
              <xsl:choose>
                <xsl:when test="position()=1">
                  <xsl:value-of select="."/>
                </xsl:when>
 								<xsl:when test="(position()>1 and ((position()-2) mod 3)!=0 ) or position()=2 ">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
 								<xsl:when test="position()>3 and ((position()-2) mod 3)=0">
									<xsl:attribute name="class">numberText<xsl:if test=". &lt; ../td[15]">Warnning</xsl:if></xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00%')"/>
								</xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

