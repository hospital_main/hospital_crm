<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">  
				<xsl:for-each select="/root/tbody/tr[td[1]='收费类别' or td[1]='收入项目']/td">
					<xsl:choose>
            <xsl:when test="position()=1">
			    		<td noWrap="true" rowspan="2"><xsl:value-of select="."/></td>
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		<td noWrap="true"><xsl:value-of select="."/></td>
			    	</xsl:when>
			    	<xsl:when test="(position() mod 3)=0 and position()!=15">
			    		<td noWrap="true" colspan="3"><xsl:value-of select="."/></td>
			    		<td noWrap="true" style="display:none"><xsl:value-of select="."/></td>
			    		<td noWrap="true" style="display:none"><xsl:value-of select="."/></td>
			    	</xsl:when>              	
          </xsl:choose>
	    	</xsl:for-each>
		  </tr>
		  <tr noWrap="true" class="mainHead"> 
				<xsl:for-each select="/root/tbody/tr[td[1]='收费类别' or td[1]='收入项目']/td">
					<xsl:choose>
            <xsl:when test="position()=1">
			    		<td noWrap="true" style="display:none"></td>
			    	</xsl:when>
			    	<xsl:when test="position()=2">
			    		<td noWrap="true">收入</td>
			    	</xsl:when>   
			    	<xsl:when test="(position() mod 3)=0 and position()!=15">
			    		<td noWrap="true">收入</td>
			    		<td noWrap="true">差异</td>
			    		<td noWrap="true">差异率</td>
			    	</xsl:when>           	
          </xsl:choose>
	    	</xsl:for-each>
			</tr>
		</thead>
	  <tbody>
  		<xsl:for-each select="/root/tbody/tr[td[1]!='收费类别' and td[1]!='收入项目']">
  			<tr>          
  				<xsl:for-each select="td">
  					<xsl:choose> 
  						<xsl:when test="position()=1">
  							<td><xsl:value-of select="."/></td>
  						</xsl:when>
							<xsl:when test="position()>3 and ((position()-2) mod 3)=0">
  							<td class="numberText">
  								<xsl:value-of select="format-number(.,'#,##0.00%')"/>
  							</td>
							</xsl:when>
  						<xsl:otherwise>
  							<td class="numberText">
	  								<xsl:value-of select="format-number(.,'#,##0.00')"/>
  							</td>
  						</xsl:otherwise>
  					</xsl:choose>
  				</xsl:for-each>
  			</tr>
  		</xsl:for-each>  	
		</tbody>
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:13;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>
