<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/hos/incomeAnalysis/incomeSubj/compose/printView.xsl,v 1.1 2012/03/12 01:58:12 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:12 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<xsl:if test="/root/tbody/tr[td[5]='1']/td">
					<td style="display:none"/>
				</xsl:if>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<xsl:if test="/root/tbody/tr[td[5]='1']/td">
					<td style="display:none"/>
				</xsl:if>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<xsl:if test="/root/tbody/tr[td[5]='1']/td">
					<td style="display:none"/>
				</xsl:if>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<xsl:if test="/root/tbody/tr[td[5]='1']/td">
					<td style="display:none"/>
				</xsl:if>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead"> 
				<xsl:if test="/root/tbody/tr[td[5]='1']/td">
					<td noWrap="true">收入项目</td>
					<td noWrap="true">收入项目明细</td>
				</xsl:if>
				<xsl:if test="/root/tbody/tr[td[5]='2']/td">
					<td noWrap="true">门诊住院</td>
				</xsl:if>
				<xsl:if test="/root/tbody/tr[td[5]='3']/td">
					<td noWrap="true">医疗药品</td>
		    		</xsl:if>
				<td noWrap="true">收入</td>
				<td noWrap="true">比例</td>
			</tr>	
		</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:variable name="pos" select="position()"/>
        	<xsl:variable name="item" select="td[1]"/>
        	<xsl:variable name="cnt" select="count(/root/tbody/tr[td[1]= $item]) "/>
          <xsl:for-each select="td[position()!=5]">
          	<xsl:choose>
	          	<xsl:when test="position()=1">
	              	
		              	<xsl:if test="$pos = 1">
				            	<td>
				            			<xsl:attribute name="rowspan"><xsl:value-of select="$cnt"/></xsl:attribute>
					  	            <xsl:value-of select="."/>
				            	</td>
			            	</xsl:if>
			            	
		            		<xsl:if test="$pos &gt; 1">
		            			<xsl:if test=". != ../../tr[$pos -1]/td[1]">
					            	<td>
					            			<xsl:attribute name="rowspan"><xsl:value-of select="$cnt"/></xsl:attribute>
						  	            <xsl:value-of select="."/>
					            	</td>
		            			</xsl:if>
		            			
		            			<xsl:if test=". = ../../tr[$pos -1]/td[1]">
		            				<td></td>
		            			</xsl:if>
		            		</xsl:if>
		            		
		            </xsl:when>
		            <xsl:when test="position()=2">
		              	<xsl:if test="../td[5]='1'">
		            		<td>
			  	            	<xsl:value-of select="."/>
		            		</td>
		            	</xsl:if>
		            </xsl:when>
	                <xsl:when test="position()=3">
	            		<td>
			                <xsl:attribute name='class'>numberText</xsl:attribute>
			  	            <xsl:value-of select="format-number(.,'#,##0.00')"/>
	            		</td>
		                </xsl:when>
	                <xsl:when test="position()=4">
	          		 	 <td>
			                <xsl:attribute name='class'>numberText</xsl:attribute>
			  	            <xsl:value-of select="format-number(.,'#,##0.00')"/>%
	            		</td>
		                </xsl:when>
	                <xsl:otherwise>
	                 
	                </xsl:otherwise>
	            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:2;align:right"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>
