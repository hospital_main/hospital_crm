<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead"> 
				<xsl:if test="/root/tbody/tr[td[5]='1']/td">
					<th noWrap="true">收入项目</th>
					<th noWrap="true">收入项目明细</th>
				</xsl:if>
				<xsl:if test="/root/tbody/tr[td[5]='2']/td">
					<th noWrap="true">门诊住院</th>
				</xsl:if>
				<xsl:if test="/root/tbody/tr[td[5]='3']/td">
					<th noWrap="true">医疗药品</th>
		    </xsl:if>
				<th noWrap="true">收入</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">比例图</th>
			</tr>	
		</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:variable name="pos" select="position()"/>
        	<xsl:variable name="item" select="td[1]"/>
        	<xsl:variable name="cnt" select="count(/root/tbody/tr[td[1]= $item]) "/>     	
					<td align='center' style='display:none'>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
							<xsl:attribute name="value">
                <xsl:for-each select="pk/*">&lt;<xsl:value-of
								select="name()" />&gt;<xsl:value-of select="." />&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td[position()!=5]">
              <xsl:choose>
              	<xsl:when test="position()=1">
              	
	              	<xsl:if test="$pos = 1">
			            	<td>
			            			<xsl:attribute name="rowspan"><xsl:value-of select="$cnt"/></xsl:attribute>
				  	            <xsl:value-of select="."/>
			            	</td>
		            	</xsl:if>
		            	
	            		<xsl:if test="$pos &gt; 1">
	            			<xsl:if test=". != ../../tr[$pos -1]/td[1]">
				            	<td>
				            			<xsl:attribute name="rowspan"><xsl:value-of select="$cnt"/></xsl:attribute>
					  	            <xsl:value-of select="."/>
				            	</td>
	            			</xsl:if>
	            			
	            			<xsl:if test=". = ../../tr[$pos -1]/td[1]">
	            			</xsl:if>
	            		</xsl:if>
	            		
	            </xsl:when>
	            <xsl:when test="position()=2">
	              	<xsl:if test="../td[5]='1'">
	            		<td>
		  	            	<xsl:value-of select="."/>
	            		</td>
	            	</xsl:if>
	            </xsl:when>
                <xsl:when test="position()=3">
            		<td>
		                <xsl:attribute name='class'>numberText</xsl:attribute>
		  	            <xsl:value-of select="format-number(.,'#,##0.00')"/>
            		</td>
	                </xsl:when>
                <xsl:when test="position()=4">
          		 	 <td>
		                <xsl:attribute name='class'>numberText</xsl:attribute>
		  	            <xsl:value-of select="format-number(.,'#,##0.00')"/>%
            		</td>
	                </xsl:when>
                <xsl:otherwise>
                 
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
          <td>
          	<table border="0" height="12"  cellpadding='0' cellspacing='0' >
                  	<xsl:attribute name="width"><xsl:value-of select="td[4]"/>%</xsl:attribute>
                  	<tr><td bgcolor="blue"></td></tr>
             </table> 
          </td>
        </tr>
      </xsl:for-each>
    </tbody>
		</xsl:template>
</xsl:stylesheet>
