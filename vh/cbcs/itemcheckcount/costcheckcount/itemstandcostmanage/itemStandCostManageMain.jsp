<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/costcheckcount/itemstandcostmanage/itemStandCostManageMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<link href="" rel="stylesheet" type="text/css">
<script language="javascript" id="scriptID"></script>
<script language="javascript">parent.pageInit(window)</script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
function dischange(){
		template.changesign.value="dischange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
}
function kindchange(){
		template.changesign.value="kindchange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
}
 	function find(){
 		template.subFunction.value='findAll';
 		show_wait();
    template.submit();
 	}
 	
  function create(){
 	 		template.subFunction.value='preparedCreate';
 	  	show_wait(); 	 		
 	 		template.submit();
 	 		}
 	 			
  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

     if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      show_wait();
      return false;
    }
  }
  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }
  
 function importData(){
		openDialog("cbcs/itemcheckcount/costcheckcount/itemstandcostmanage/import.html", 'dialogWidth:900px;dialogHeight:600px;');
	} 
</Script>    
    
<html:html clazz="main">
<form name="template" method="post" action="itemstandcostmanage.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
<html:title clazz='module'>标准收入成本管理</html:title>
   <html:table clazz="simple">
   <%  
    String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
  %>
  	<tr>
		  	<td nowrap class="signText">年　　度:</td>
				<td nowrap class="normalText"><%=new Select(year,"dyear",request.getParameter("dyear"),false,false)%></td>
				<td class="signText">收入项目：</td>
			 	<td >
				 	<%
				 	Select dict_income_subj=new Select(request.getAttribute("dict_income_subj"),"income_subj_code",request.getParameter("income_subj_code")==null? "":request.getParameter("income_subj_code"),false,false);
				 	dict_income_subj.setAttribute("onchange","dischange()");
				 	%>
				 	<%=dict_income_subj.toString() %></td>
	  		 <td class="signText">收费类别：</td>
			 	<td >
			 	<%
			 	Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"charge_kind_code",request.getParameter("charge_kind_code")==null? "":request.getParameter("charge_kind_code"),false,false);
			 	b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
			 	%>
			 	<%=b_dict_charge_detail_kind.toString() %></td>

 		 </tr>
		<tr>
			 	<td class="signText">医疗项目: </td>
			 	<td class="normalText">
			 	<%
			 	Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"charge_detail_code",request.getParameter("charge_detail_code")==null? "":request.getParameter("charge_detail_code"),false,false);
			 	%>
			 	<%=dict_charge_detail.toString() %></td>
			<td colSpan="4"><button class='tableBtn' onclick="find();">查询</button>
					<button class='tableBtn' accessKey='I' name='cbcsproject_itemstandcostmanage_import'  onclick="importData();">导入</button>
					</td>
  </tr>
</html:table>
<html:table clazz="simple"><tr><td><html:title clazz='table'>标准收入成本管理</html:title></td></tr></html:table>
<% 
  BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	if (ro != null) {
    TableMarge oper = new TableMarge(ro, "return find()"); 
    oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
    oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
    oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
    oper.addNeedButton("images/create.gif", "return create()");     //  添加
%>
 <html:table clazz="simple"><tr><td><%=oper%></td></tr></html:table> 
<% } else { %>
  <html:table clazz="simple"><tr><td></td></tr></html:table> 
<% }  %>
 <html:table clazz="complex">
	<tr>
    <td>
      <html:table clazz="result">
      	<html:tr clazz='label'>
      	<td nowrap class="resultLabel" rowspan='2'>选择</td>
      	<td nowrap class="resultLabel" rowspan='2'>年度</td>
      	<td nowrap class="resultLabel" rowspan='2'>医疗项目</td>
      	<td nowrap colSpan ="2" class="resultLabel">院内平均 </td>
      	<td nowrap colSpan ="2" class="resultLabel">市级标准 </td>
      	 <td nowrap colSpan ="2" class="resultLabel">部颁标准 </td>
      	</html:tr>
				<html:tr clazz='label'>
					<td nowrap class="resultLabel"> 成本 </td>
					<td nowrap class="resultLabel"> 单价 </td>
					<td nowrap class="resultLabel"> 成本 </td>					
					<td nowrap class="resultLabel"> 单价 </td>
					<td nowrap class="resultLabel"> 成本 </td>	
					<td nowrap class="resultLabel"> 单价 </td>
				</html:tr>
     <%
  if (ro != null) {
        String[][] result = ro.getTableResult();
         DecimalFormat mf=new DecimalFormat("#,##0.00");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[i][0]+"|^|"+result[i][8];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }
            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite"; 
       %>
      <tr CLASS="<%=rowColor%>">
         <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
          <td nowrap class="normalText"><%=result[i][0]%></td>
          <td nowrap class="normalText"><A href="itemstandcostmanage.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>" onclick="show_wait()"><%=result[i][1]%></a></td>
   				<td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[i][2]))%></td>
	        <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[i][3]))%></td>
   				<td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[i][4]))%></td>
	        <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[i][5]))%></td>	        
   				<td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[i][6]))%></td>
	        <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[i][7]))%></td>
     </tr>
  <%       
  					}
 					} 
	} %>
       </html:table>
        </td>
      </tr>
 	</html:table>
	<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
<input type="hidden" name="subFunction" value="perparedFind"/>
</form>
</html:html>

  
 
 			  
	