<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/costcheckcount/itemstandcostmanage/itemStandCostManageUpdate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ --> 
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
   if(isEmpty(template.avg_charge_cost)||isEmpty(template.avg_charge_income))
     {
       alert('字段不能为空！');
       return;
      }
 switch(isDouble(template.avg_charge_cost,12,4))
     {
      case 0 : alert('平均成本金额必须为数字型'); return;
      case 1 : alert('平均成本金额整数部分不能高于12个字符'); return;
      case 2 : alert('平均单价金额没有整数部分'); return;
      case 3 : alert('平均成本金额小数部分不能高于4个字符'); return;
    } 
 switch( isDouble(template.avg_charge_income,12,4))
     {
      case 0 : alert('平均单价金额必须为数字型'); return;
      case 1 : alert('平均单价金额整数部分不能高于12个字符'); return;
      case 2 : alert('平均单价金额没有整数部分'); return;
      case 3 : alert('平均单价金额小数部分不能高于4个字符'); return;
    } 
 
 switch( isDouble(template.c_avg_charge_cost,12,4))
     {
      case 0 : alert('市颁布成本金额必须为数字型'); return;
      case 1 : alert('市颁布成本金额整数部分不能高于12个字符'); return;
      case 3 : alert('市颁布成本金额小数部分不能高于4个字符'); return;
    } 
 
 switch( isDouble(template.c_avg_charge_income,12,4))
     {
      case 0 : alert('市颁布单价金额必须为数字型'); return;
      case 1 : alert('市颁布单价金额整数部分不能高于12个字符'); return;
      case 3 : alert('市颁布单价金额小数部分不能高于4个字符'); return;
    } 
 
 switch( isDouble(template.m_avg_charge_cost,12,4))
     {
      case 0 : alert('部颁布成本金额必须为数字型'); return;
      case 1 : alert('部颁布成本金额整数部分不能高于12个字符'); return;
      case 3 : alert('部颁布成本金额小数部分不能高于4个字符'); return;
    } 
 
 switch( isDouble(template.m_avg_charge_income,12,4))
     {
      case 0 : alert('部颁布单价金额必须为数字型'); return;
      case 1 : alert('部颁布单价金额整数部分不能高于12个字符'); return;
      case 3 : alert('部颁布单价金额小数部分不能高于4个字符'); return;
    } 
    template.subFunction.value='Update';
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back(element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
		show_wait();
    element.submit();
  }
 
  
</Script>
<html:html clazz="main">
<form name="template" method="post" action="itemstandcostmanage.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module">标准收入成本定义修改页面</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">年度:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="dyear" class="textInputC" value="<%=result[0]%>" disabled /></td>
    </tr> 
    <tr>
      <td class="signText" nowrap="nowrap">医疗项目:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="charge_detail_code" class="textInputC" value="<%=result[1]%>"  disabled /> </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">平均成本:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="avg_charge_cost" value="<%=result[2]%>" class="textInputC" /></td>
    </tr>   
    <tr>
      <td class="signText" nowrap="nowrap">平均单价:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="avg_charge_income"  value="<%=result[3]%>" class="textInputC" /></td>
    </tr> 
      <tr>
      <td class="signText" nowrap="nowrap">市颁布成本:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="c_avg_charge_cost"  value="<%=result[4]%>" class="textInputC" /></td>
    </tr>    
    <tr>
      <td class="signText" nowrap="nowrap">市颁布单价:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="c_avg_charge_income"  value="<%=result[5]%>" class="textInputC" /></td>
    </tr>
      <tr>
      <td class="signText" nowrap="nowrap">部颁布成本:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="m_avg_charge_cost"  value="<%=result[6]%>" class="textInputC" /></td>
    </tr>    
    <tr>
      <td class="signText" nowrap="nowrap">部颁布单价:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="m_avg_charge_income"  value="<%=result[7]%>" class="textInputC" /></td>
    </tr>   
     <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();" >保存</button> <button class="pageBtn" onclick="return reset();">重置</button> <button class="pageBtn" onclick="return back(template);">返回</button></td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="dyear1" value="<%=result[0]%>">
  <input type="hidden" name="charge_detail_code1" value="<%=result[8]%>" /> 
  <%}%>
</form>
</html:html>
