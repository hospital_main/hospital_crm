<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/costcheckcount/itemstandcostmanage/itemStandCostManageCreate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  //添加
    function create() {
  
    if(isEmpty(template.avg_charge_cost)||isEmpty(template.avg_charge_income))
     {
       alert('字段不能为空！');
       return;
      }
 switch(isDouble(template.avg_charge_cost,12,4))
     {
      case 0 : alert('平均成本金额必须为数字型'); return;
      case 1 : alert('平均成本金额整数部分不能高于12个字符'); return;
      case 2 : alert('平均单价金额没有整数部分'); return;
      case 3 : alert('平均成本金额小数部分不能高于4个字符'); return;
    } 
 switch( isDouble(template.avg_charge_income,12,4))
     {
      case 0 : alert('平均单价金额必须为数字型'); return;
      case 1 : alert('平均单价金额整数部分不能高于12个字符'); return;
      case 2 : alert('平均单价金额没有整数部分'); return;
      case 3 : alert('平均单价金额小数部分不能高于4个字符'); return;
    } 
 
 switch( isDouble(template.c_avg_charge_cost,12,4))
     {
      case 0 : alert('市颁布成本金额必须为数字型'); return;
      case 1 : alert('市颁布成本金额整数部分不能高于12个字符'); return;
      case 3 : alert('市颁布成本金额小数部分不能高于4个字符'); return;
    } 
 
 switch( isDouble(template.c_avg_charge_income,12,4))
     {
      case 0 : alert('市颁布单价金额必须为数字型'); return;
      case 1 : alert('市颁布单价金额整数部分不能高于12个字符'); return;
      case 3 : alert('市颁布单价金额小数部分不能高于4个字符'); return;
    } 
 
 switch( isDouble(template.m_avg_charge_cost,12,4))
     {
      case 0 : alert('部颁布成本金额必须为数字型'); return;
      case 1 : alert('部颁布成本金额整数部分不能高于12个字符'); return;
      case 3 : alert('部颁布成本金额小数部分不能高于4个字符'); return;
    } 
 
 switch( isDouble(template.m_avg_charge_income,12,4))
     {
      case 0 : alert('部颁布单价金额必须为数字型'); return;
      case 1 : alert('部颁布单价金额整数部分不能高于12个字符'); return;
      case 3 : alert('部颁布单价金额小数部分不能高于4个字符'); return;
    } 
 
       
        template.subFunction.value='create';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemstandcostmanage.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>标准收入成本定义 </html:title>  
   <%  
 		String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
    String[][]code_init1 = (String[][])request.getAttribute("charge_detail_code");
   %>
  <!-- 简单信息 -->  
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="signText" nowrap="nowrap">年度:</td>
      <td nowrap class="normalText"><%=new Select(year,"dyear",request.getParameter("dyear"),false,true)%></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">医疗项目:</td>
      <td class="normalText" nowrap="nowrap"><%=new Select(code_init1,"charge_detail_code",request.getParameter("charge_detail_code"),false,true)%></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">平均成本:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="avg_charge_cost" class="textInputC" /></td>
    </tr>   
    <tr>
      <td class="signText" nowrap="nowrap">平均单价:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="avg_charge_income" class="textInputC" /></td>
    </tr> 
      <tr>
      <td class="signText" nowrap="nowrap">市颁布成本:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="c_avg_charge_cost" class="textInputC" /></td>
    </tr>    
    <tr>
      <td class="signText" nowrap="nowrap">市颁布单价:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="c_avg_charge_income" class="textInputC" /></td>
    </tr>
      <tr>
      <td class="signText" nowrap="nowrap">部颁布成本:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="m_avg_charge_cost" class="textInputC" /></td>
    </tr>    
    <tr>
      <td class="signText" nowrap="nowrap">部颁布单价:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="m_avg_charge_income" class="textInputC" /></td>
    </tr>
      
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return create();">添加</button> <button class="pageBtn" onclick="return reset();">重置</button> <button class="pageBtn" onclick="return back(template);">返回</button></td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   