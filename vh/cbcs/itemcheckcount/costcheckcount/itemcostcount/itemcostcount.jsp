<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/costcheckcount/itemcostcount/itemcostcount.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:28 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
	function apportion(num) {
	  if (template.year_month.value=='') {
	    alert('请选择核算月');
	    return false;
	  }
          if(num==0)
            template.flag.value='no'
          else if(num==1)
            template.flag.value='complete'
	  show_wait();
	  template.submit();
	  return true;
	}
</Script>
<html:html clazz="main">
<form name="template" method="post" action="itemcostcount.jspviewhigh?subFunction=count">
  <!-- 信息提示栏 -->
   <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>项目成本核算</html:title>
  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap width="10%">核算月：</td>
      <td class="normalText"><%=new MonthComponent("year_month", request.getParameter("year_month"))%></td>
      <td align="right"><button class="pageBtn" onclick="return apportion(0)">预结</button>
      <!--<img src="images/preparedBalance.gif" class="mouse" onclick="return apportion(0)"/>-->
      <button class="pageBtn" onclick="return apportion(1)">完成</button>
      <!--<img src="images/complete.gif" class="mouse" onclick="return apportion(1)"/>--></td>
    </tr>
	</html:table>
	<input type='hidden' name="flag" value='complete'>
</form>

</html:html>



