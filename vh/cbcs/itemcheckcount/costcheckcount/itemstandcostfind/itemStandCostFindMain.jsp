<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/costcheckcount/itemstandcostfind/itemStandCostFindMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.text.*,com.viewhigh.cbcs.cbcs.util.DisplayWidth,
         java.lang.Object.*, com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<%
   String[][] result=(String[][])request.getAttribute("result");
%>

<Script language="javascript">
function dischange(){
		template.changesign.value="dischange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
}
function kindchange(){
		template.changesign.value="kindchange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
}
  function find(){
    if(template.year_from.value == "")
    {
      alert('请选择核算起始年!');
      return false;
    }
    if(template.year_to.value == "")
    {
      alert('请选择核算结束年!');
      return false;
    }
    if(template.year_from.value>template.year_to.value){
   		alert('起始年应小于结束年月!');
      return false;
    }
    if(eval(template.year_to.value) - eval(template.year_from.value)>5){
   		alert('不能超过5年!');
      return false;
    }
    show_wait();
    template.subFunction.value="findAll";
    template.submit();
    return;
  }
</Script>

<html:html clazz="main" isPrint="true" fixRows="2">
	<form name="template" method="post" action="itemstandcostfind.jspviewhigh">
		<html:message/>
		<html:title clazz='module'>标准成本查询</html:title>
  <%  
 		String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
 		String[][] para ={{"1","院内平均"},{"2","市颁标准"},{"3","部颁标准"}};
 %>
	  <table  width="100%" cellspacing="2" border="0" >
	   <tr>
			 <td class="signText"> 年　　度: </td>
			 <td class="normaltext"><%=new Select(year,"year_from", request.getParameter("year_from"),false,true)%> 至 <%=new Select(year,"year_to", request.getParameter("year_to"),false,true)%></td> 
				<td class="signText">收入项目：</td>
			 	<td class="normalText">
				 	<%
				 	Select dict_income_subj=new Select(request.getAttribute("dict_income_subj"),"dict_income_subj",request.getParameter("dict_income_subj")==null? "":request.getParameter("dict_income_subj"),false,false);
				 	dict_income_subj.setAttribute("onchange","dischange()");
				 	%>
				 	<%=dict_income_subj.toString() %></td>
		 
	   	 <td class="signText">标准选择:</td>
			 <td class="normalText"><%= new Select(para,"kind",request.getParameter("kind"),false,true)%></td>  
		 </tr>
		 <tr>
		 <td class="signText">收费类别：</td>
		 	<td class="normalText">
			 	<%
			 	Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"b_dict_charge_detail_kind",request.getParameter("b_dict_charge_detail_kind")==null? "":request.getParameter("b_dict_charge_detail_kind"),false,false);
			 	b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
			 	%>
			 	<%=b_dict_charge_detail_kind.toString() %></td>
			 	<td class="signText">医疗项目: </td>
		 	<td class="normalText">
			 	<%
			 	Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"dict_charge_detail",request.getParameter("dict_charge_detail")==null? "":request.getParameter("dict_charge_detail"),false,false);
			 	%>
			 	<%=dict_charge_detail.toString() %></td>
			 <td><button class="pageBtn" onclick="return find();">查询</button></td>
		 </tr>
	  </table>

	  <br>
	  <html:title clazz='table'>标准成本查询</html:title>

<vh:table fixRow=2 fixCol=1>
			<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
					
					
			   <colgroup id=tg>
          <col style = <%=DisplayWidth.NAME_WIDTH%>  >
					<%  
					if (result!=null) {  
						String year1= (String)request.getAttribute("year1");
						String year2=(String)request.getAttribute("year2");
						int y1= Integer.parseInt(year1);
						int y2= Integer.parseInt(year2);
						while (y1<=y2){
					%>    
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<%y1=y1+1;} 
					}%>
         </colgroup>

        <tr>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan=2 >医疗项目</td>
      <%  
      if (result!=null) {  
      String year1= (String)request.getAttribute("year1");
      String year2=(String)request.getAttribute("year2");
       int y1= Integer.parseInt(year1);
       int y2= Integer.parseInt(year2);
       while (y1<=y2){
      	 	%>    
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2"><%=y1%> </td>
    <%y1=y1+1;} 
    }%>
       	</tr>
        <tr>
      <%  
      if (result!=null) {  
      String year1= (String)request.getAttribute("year1");
      String year2=(String)request.getAttribute("year2");
       int y1= Integer.parseInt(year1);
       int y2= Integer.parseInt(year2);
       while (y1<=y2){
      	 	%> 
      	 	<td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >成本</td>  
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >单价</td>
      
     <%y1=y1+1;} 
    }%>      
        </tr>
<%
	DecimalFormat mf = new DecimalFormat("#,##0.00");
  if(result!=null){
   for(int i=0;i<result.length;i++){

%>
        <tr>
          <td nowrap class="normalText" style='text-align:left'><%=result[i][0]%></td>
          <%
          for(int j=1;j<result[i].length-1;j++){
          %>
          <td nowrap class="numberText" style='text-align:right'><%=mf.format(Double.parseDouble(result[i][j]))%></td>
          <%}%>
          <td nowrap class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[i][result[i].length-1]))%></td>
        </tr>
<%}
}%>
	  	</table>
</vh:table>


  	<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
  	<input type="hidden" name="subFunction" value="perparedFind">
	</form>
</html:html>

