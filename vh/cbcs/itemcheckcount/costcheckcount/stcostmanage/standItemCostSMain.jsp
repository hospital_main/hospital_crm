<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/costcheckcount/stcostmanage/standItemCostSMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

 	function find(){
 		if(template.fyear.value==""||template.tyear.value==""){
 			alert("请选择期间");
 			return; 			
 		} 		
 		if(template.fyear.value>template.tyear.value){
 			alert("请保证期间顺序");
 			return;
 		}else if((template.tyear.value-template.fyear.value)>4){
 			alert("期间段不要超过5年");
 			return;
 		}
 		if(template.charge_kind.value==""){
 			alert("请根据收入项目选择收费类别");
 			return;
 		}
 		template.subFunction.value='findAll';
 		show_wait();
    template.submit();
 	}
</Script>
    
    
<html:html clazz="main">
<form name="template" method="post" action="itemstandcostfind.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
<html:title clazz='module'>标准项目成本查询页面</html:title>
      <html:table clazz="simple">
   <%  
    String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
    
    String[][] seconditem=(String[][])request.getAttribute("seconditem");
    String[] names = {"values","texts", "relations"};
    out.println(com.viewhigh.cbcs.base.util.SimpleSearch.dynamicSelect(names, seconditem, "dynamicSelect", request.getParameter("charge_kind")));

    String[][] firstitem=(String[][])request.getAttribute("firstitem");
    Select super_code = new Select(firstitem, "first", "", true, false);
    
    super_code.setAttribute("onchange", "dynamicSelect(template.first, template.charge_kind)");	  
  %>
  <tr>
		<td nowrap class="signText">期间:</td>
		<td nowrap class="normalText">
			<%=new Select(year,"fyear",request.getParameter("fyear"),false,false)%>年至<%=new Select(year,"tyear",request.getParameter("tyear"),false,false)%>年
		</td>
		</td></td>
  </tr>
	<tr>
			<td class="signText" nowrap>收入项目:</td>
		  <td>
       <%
	  			out.println(super_code);
    	 %>
			</td>
			<td class="signText" nowrap>收费类别:</td>
			<td >
			<%
				Select seco_code = new Select("charge_kind");
				out.println(seco_code);
			%>
			</td>                    
	<td>
	<button class="pageBtn" name=""   onclick="find();"  >查询</button>
	<!--<img src="images/find.gif" class="mouse" onclick="find();">--></td>
  </tr>
</html:table>
<html:table clazz="simple"><tr><td><html:title clazz='table'>标准项目成本表</html:title></td></tr></html:table>
<%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro != null) {
        String [] titles=(String[])request.getAttribute("titles");
        String dcheck=request.getParameter("checksign");
			  String dyear_month=request.getParameter("year_month");
        TableMarge oper = new TableMarge(ro, "return find()");
%>

<html:table clazz="simple"><tr><td><%=oper%></td></tr></html:table>
     <html:table clazz="complex">

<tr>
        <td>
      <html:table clazz="result">
				<html:tr clazz='label'>
					<td nowrap class="resultLabel">医疗项目代码</td>
					<td nowrap class="resultLabel"  >医疗项目</td>
					<% if ( titles == null){%>
					  <td nowrap class="resultLabel"  >xxxx年</td>
					 <%}%>
					<% if ( titles != null) for(int j=0;j< titles.length;j++){%>
					<td nowrap class="resultLabel"  ><%=titles[j]%>年</td>
					<%}%>
				</html:tr>
        <%
          String[][] result = ro.getTableResult();
          DecimalFormat mf=new DecimalFormat("#,##0.00");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

       <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText"><%=result[ i ][ 0 ]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 1 ]%></td>
          <% for(int j=0+2;j< titles.length+2;j++){%>
	        <td nowrap class="numberText"  ><%=mf.format(Double.parseDouble(result[i][j]))%></td>
	        <%}%>
      </tr>
        <%
              }
            }
        %>
       </html:table>
         </td>
         </tr>

      </html:table>
<%}%>
<input type="hidden" name="subFunction" >
</form>
</html:html>  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  