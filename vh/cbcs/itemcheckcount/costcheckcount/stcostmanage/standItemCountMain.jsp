<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/costcheckcount/stcostmanage/standItemCountMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

//继承
 function create(){ 
 			if(template.syear_month.value==""||template.dyear_month.value==""||template.year.value==""){
 				alert("所有条件条件不能为空");
 				return;
 			}
	    if(template.syear_month.value>template.dyear_month.value)
	    {
	      alert('起始年月要小于终止年月');
	      return;
   	  }
   	  if(template.dyear_month.value.substring(0,4)>=template.year.value){
   	  	alert("终止年度要小于目标年度");
   	  	return;
   	  }
      if (!confirm('如果目标年度有数据，该操作将覆盖该目标年度数据['+template.year.value+'] ，请确认')) {
       return true;
      }    
      template.subFunction.value='count';
      show_wait();        
      template.submit();

      return true;
 }
</Script>
    
    
<html:html clazz="main">
<form name="template" method="post" action="itemstandcostcount.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>
<%
	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
%>
  <!-- 标题栏 -->
	  <html:title clazz='module'>标准成本核算页面</html:title>
 <html:table clazz="simple">
  <tr>	   
	 <td class="signText">起始年月：</td>
	 <td class="normalText"><%=new MonthComponent("syear_month",request.getParameter("syear_month"))%></td>
	 <td class="signText">终止年月： </td>
	 <td class="normalText"><%=new MonthComponent("dyear_month",request.getParameter("dyear_month"))%></td>              
  </tr>
	<tr>
	<td class="signText">目标年度：</td>
	<td class="normalText"><%=new Select(year,"year",request.getParameter("year")==null? "":request.getParameter("year"),false,false)%></td>
	<td colspan="2" align="right">
	<button class='pageBtn' onclick="return create()">确定</button>
	<!--	<IMG src="images/b_complete.gif"  border="0" class=mouse onclick="return create()">-->
	</td>
  </tr>
    <input type=hidden name="subFunction" value="create"/>
  </form>
</html:table>
</html:html>  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  