<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/define/seedept/seedept.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:28 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>

<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
function find() {
    template.subFunction.value='preparedfindAll';
    template.submit();
    return true;
  }


</Script>

<html:html clazz="main">
<form name="template" method="post" action="seedept.jspviewhigh">
  <!-- 信息提示栏 -->
   <html:message/>

  <!-- 标题栏 -->
   <html:title clazz='module'>核算科室查询</html:title>
  <html:table clazz="simple">
   </html:table>
  <!-- 查询信息 -->
    <%
      BaseRO  ro= (BaseRO)request.getAttribute("ro");

      String[][] result=null;
      if(ro!=null)
        {result=ro.getTableResult();
       }
      TableMarge oper = new TableMarge(ro, "return find()");
    %>
<html:title clazz='table'>参与核算科室列表</html:title>
  <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
   </html:table>
      <html:table clazz="result">
        <html:tr clazz='label'>
          <td >科室代码</td>
          <td >科室名称</td>
          <td >上级科室</td>
          <td>科室类别</td>
          <td >分摊级别</td>
          </html:tr>

        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j]=="") result[i][j]="&nbsp;";
                }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>
        <tr CLASS="<%=rowColor%>">
           <td class="normalText"><%=result[ i ][ 0 ]%></td>
           <td class="normalText"><%=result[ i ][ 1 ]%></td>
           <td class="normalText"><%=result[ i ][ 2 ]%></td>
           <td class="normalText"><%=result[ i ][ 3 ].equals("4")?"医疗技术科室":"直接医疗科室"%></td>
           <td class="normalText"><%=result[ i ][ 3 ]%></td>
        </tr>
        <%
              }
            }
        %>
     </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>


