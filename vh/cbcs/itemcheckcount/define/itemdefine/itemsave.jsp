<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/define/itemdefine/itemsave.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:58:28 $
  $Modtime: 03-09-02 10:42 $
	$Revision: 1.1 $
	$NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
<%
if ("create".equals(request.getParameter("subFunction"))) {
%>
  parent.document.getElementById('_process').style.display=''
  parent.document.getElementById('_image').style.display='none'
  <% if (request.getAttribute("appError")!=null) {%>
    parent.document.getElementById('_message').innerText = '<%=request.getAttribute("appError")%>'
    parent.document.getElementById('_message').style.color = 'red'
  <%} else {%>
    parent.document.getElementById('_message').innerText = '<%=request.getAttribute("message")%>'
<%
  }
}
%>



  function create()
  {

    if(isEmpty(template.join_avgcost))
    {
      alert('请输入单位合作成本!');
      return;
    }

    if(isEmpty(template.charge_cost_prop))
    {
      alert('请输入单位项目单位成本!');
      return;
    }

    switch (isDouble(template.join_avgcost, 8, 2)) {
      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于8个字符');
        return false;
      case 3 :
        alert('金额小数部分不能高于2个字符');
        return false;
    }
    switch (isDouble(template.charge_cost_prop, 8, 2)) {
      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于8个字符');
        return false;
      case 3 :
        alert('金额小数部分不能高于2个字符');
        return false;
    }
 


    template.submit();
    return true;
  }

  function back() {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
  	self.close();
  }


  function save() { 
  
    switch (isDouble(template.join_avgcost, 8, 2)) {
      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于8个字符');
        return false;
      case 3 :
        alert('金额小数部分不能高于2个字符');
        return false;
    }
    switch (isDouble(template.charge_cost_prop, 8, 2)) {
      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于8个字符');
        return false;
      case 3 :
        alert('金额小数部分不能高于2个字符');
        return false;
    }
 

    show_wait();
    var OD = new Date(); 
    document.getElementById("forbide").src="itemdefine.jspviewhigh?subFunction=create&charge_detail_code=<%=request.getParameter("charge_detail_code")%>&join_avgcost="+template.join_avgcost.value+"&charge_cost_prop="+template.charge_cost_prop.value+"&time="+OD.toTimeString();
  }
</Script>

<html:html clazz="main">
    <form name="template" method="post" action="itemdefine.jspviewhigh">
     <table width='100%' border='0'>
      <tr>
        <td>
          <table width='100%' border='0' >
            <tr>
              <td class='successText' align='center' id='_message'></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>



      <!--信息栏-->
       <html:title clazz='module'>修改页面</html:title>
      <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap="nowrap">项目代码：</td>
          <td class="normalText" nowrap="nowrap">
            <input type=text name="code" class="textInputC" maxlength="20" value=<%=request.getParameter("charge_detail_code")%> disabled />
            <input type=hidden name="charge_detail_code" value='<%=request.getParameter("charge_detail_code")%>'/>
         </td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">项目名称：</td>
          <td class="normalText" nowrap="nowrap">
            <input type=text name="name" class="textInputC" maxlength="40" value='<%=request.getParameter("charge_detail_name")%>' disabled/>
            <input type=hidden name="charge_detail_name" value='<%=request.getParameter("charge_detail_name")%>'/>
           </td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">医疗项目类：</td>
          <td class="normalText" nowrap="nowrap">
              <input type=text name="name1" class="textInputC" maxlength="40" value='<%=request.getParameter("charge_kind_name")%>' disabled/>
              <input type=hidden name="charge_kind_name" value='<%=request.getParameter("charge_kind_name")%>'/>
           </td>
        </tr>
         <tr>
          <td class="signText" nowrap="nowrap">收入项目：</td>
          <td class="normalText" nowrap="nowrap">
            <input type=text name="name2" class="textInputC" maxlength="40" value='<%=request.getParameter("income_subj_name")%>' disabled/>
            <input type=hidden name="income_subj_name" value='<%=request.getParameter("income_subj_name")%>'/>
           </td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">收入类型：</td>
          <td class="normalText" nowrap="nowrap">
            <input type=text name="ype" class="textInputC" maxlength="40" value='<%=request.getParameter("income_type")%>' disabled/>
            <input type=hidden name="income_type" value='<%=request.getParameter("income_type")%>'/>
           </td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">单位合作成本：</td>
          <td class="normalText" nowrap="nowrap"><input type=text name="join_avgcost" class="textInputC" maxlength="40" value='<%=request.getParameter("join_avgcost")%>' /></td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">单位项目单位成本：</td>
          <td class="normalText" nowrap="nowrap"><input type=text name="charge_cost_prop" class="textInputC" maxlength="40" value='<%=request.getParameter("charge_cost_prop")%>' /></td>
        </tr>
 
        <tr>
          <td colspan="2"> 
          <button class="pageBtn" onclick="return save();">保存</button> 
          <button class="pageBtn" onclick="return reset();" >重置</button>
          <button class="pageBtn" onclick="return back();">关闭</button>  
          <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
           <img src="images/reset.gif" class="mouse" onclick="return reset();" />
           <img src="images/close.gif" class="mouse" onclick="return back();" />--> </td>
        </tr>
      </html:table>
      <input type=hidden name="subFunction" value="create"/>
      <iframe id="forbide" src="" width="0" height="0" ></iframe>
  	</form>
</html:html>



