<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/define/itemdefine/itemdefine.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:28 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
     $NoKeywords: $ -->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>

<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
function find() {
    template.subFunction.value='findAll';
    template.submit();
    return true;
  }
function save()
{  for(i=0;i<template.cost.length;i++)
   { if(!isNumber(template.cost[i]))
    {
      alert('单位合作成本必须为数字!');
      return;
    }
   if(!checkLength(template.cost[i],2))
     {
       return;
      }
   }
    template.subFunction.value='update';
    template.submit();
    return true;
 }

</Script>

<html:html clazz="main">
<form name="template" method="post" action="itemdefine.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>核算项目定义</html:title>
  <!-- 查询信息 -->

    <%
      BaseRO  ro= (BaseRO)request.getAttribute("ro");

      String[][] result=null;
      if(ro!=null)
        {result=ro.getTableResult();
       }
      TableMarge oper = new TableMarge(ro, "return find()");
    %>
<html:table clazz="simple">
    <tr>

      <td nowrap class="signText">医疗项目类编码 ：</td>
      <td nowrap class="normalText">
      <input type="text" name="charge_detail_type" size="20" value='<%=request.getParameter("charge_detail_type")==null?"":request.getParameter("charge_detail_type")%>' /></td>
      <td nowrap class="signText">名称：</td>
      <td nowrap class="normalText">
     <input type="text" name="charge_kind_name" size="20" value='<%=request.getParameter("charge_kind_name")==null?"":request.getParameter("charge_kind_name")%>'/></td>
      <td></td>
  </tr>
    <tr>

      <td nowrap class="signText"> 医疗项目编码 ：</td>
      <td nowrap class="normalText">
      <input type="text" name="charge_detail_code" size="20" value='<%=request.getParameter("charge_detail_code")==null?"":request.getParameter("charge_detail_code")%>' /></td>
      <td nowrap class="signText">名称：</td>
      <td nowrap class="normalText">
     <input type="text" name="charge_detail_name" size="20" value='<%=request.getParameter("charge_detail_name")==null?"":request.getParameter("charge_detail_name")%>'/></td>
      <td>
      <button class="pageBtn" name="" onclick="return find();" >查询</button>
      <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
  </tr>

   </html:table>
<br>
  <html:title clazz='table'>医疗项目列表</html:title>
 <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  </html:table>
     <html:table clazz="result">
     <html:tr clazz='label'>

          <td >项目代码</td>
          <td >项目名称</td>
          <td >医疗项目类</td>
          <!--          <td >收入项目</td>          -->
          <td >单位合作成本</td>
          <td >单位项目单位成本</td>
          <!--      <td >参与核算</td>          -->

         </html:tr>

        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {  
              	String[] temp = {result[i][0], result[i][1],result[i][2],result[i][3],result[i][4],result[i][5],result[i][6]};
                String primaryKey1 = ExtendTool.arrayToString(temp);
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j]=="") result[i][j]="";
                }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>
        <tr CLASS="<%=rowColor%>">
           <td class="normalText"><input type='hidden' name='primaryKey' value='<%=result[ i ][ 0 ]%>'><%=result[ i ][ 0 ]%></td>
           <td class="normalText"><a href='#' onclick='window.showModalDialog( "itemdefine.jspviewhigh?subFunction=preparedcreate&charge_detail_code=<%=result[i][0]%>&charge_detail_name=<%=result[i][1]%>&charge_kind_name=<%=result[i][2]%>&income_subj_name=<%=result[i][3]%>&income_type=<%=result[i][4]%>&join_avgcost=<%=result[i][5]%>&charge_cost_prop=<%=result[i][7]%>&audit_if=<%=result[i][6]%>",window,"dialogTop:150px;dialogLeft:300px;dialogHeight: 350px; dialogWidth: 400px;status=no,resizable=no");
              if (template!=null && template._old_current_page!=null) {template._current_page.value = template._old_current_page.value
              }
            	
             	find();'><%=result[ i ][ 1 ]%></a></td>           
           <td class="normalText"><%=result[ i ][ 2 ]%></td>
           <!--
           <td class="normalText"><%=result[ i ][ 3 ]%></td>
           -->
           <td class="normalText"><%=result[ i ][ 5 ]%></td>
           <td class="normalText"><%=result[ i ][ 7 ]%></td>
           <!--
           <td class="normalText"> <%if(result[ i ][ 6 ]!=null&&result[ i ][ 6 ].equals("Y")) out.print("是");if(result[ i ][ 6 ]!=null&&result[ i ][ 6 ].equals("N")) out.print("否");if(result[ i ][ 6 ]==null) out.print("否");%></td>
           -->
        </tr>
        <%
              }
            }
        %>
     </html:table>

  <input type=hidden name="subFunction"/>
</form>
</html:html>

