<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemcostcollect/costthismonthcompare/costthismonthcompare.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<script language='javascript' src='javascript/chart.js'></script>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,
                com.viewhigh.cbcs.cbcs.itemcheckcount.itemcostcollect.base.ResultAndStringTransform" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

  <%
			DecimalFormat nf = new DecimalFormat("#,##0.00");
			String[] allYearMonth=(String[])request.getAttribute("allYearMonth");
			BaseRO  ro= (BaseRO)request.getAttribute("ro");

      String[][] result=null;
      if(ro!=null) {
				result=ro.getTableResult();
			}
      TableMarge oper = new TableMarge(ro, "return find()");
    %>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
function find() {
     if(template.year_from.value=='')
     {alert("请选择年")
      return false
     }
    if(template.year_to.value=='')
     {alert("请选择年")
      return false
     }
    if(template.year_to.value<template.year_from.value)
    {alert("起始年应大于终止年")
      return false
     }
    if(template.month.value=='') {
    	alert("请选择月")
      return false
    }
    show_wait();
    template.subFunction.value='findAll';
    template.submit();
    return true;
  }


  var cewolf1;
  <% if (result!=null) { %>
  cewolf1 = "";
  <% }%>


</Script>

<html:html >
	<form name="template" method="get" action="costthismonthcompare.jspviewhigh">
    <html:message/>

  	<html:title clazz='module'>成本同期对比</html:title>


		<html:table clazz="simple">
    	<tr>
     		<td nowrap="nowrap" class="normalText"  width="30%"></td>
     		<td nowrap class="signText" width="30%"><input type="radio" name="kind" value="1" <% if(request.getParameter("kind")==null) out.print("checked"); if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("1")) out.print("checked");%>>总成本</td>
     		<td nowrap class="signText" width="30%"><input type="radio" name="kind" value="2" <%if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("2")) out.print("checked");%>>单位成本</td>
			</tr>
			<tr>
    		<%String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};%>
     		<td  nowrap="nowrap" class="signText" width="25%"> 期间：<%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year_from",request.getParameter("year_from"),false,false)%>年 至
				<%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year_to",request.getParameter("year_to"),false,false)%>年 </td>
    		<%String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};%>
				<td  nowrap="nowrap" class="signText" width="25%" >对比月：<%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(month,"month",request.getParameter("month"),false,false)%>月</td>
				<td nowrap="nowrap" class="normalText" width="25%" >
				<button class="pageBtn" name="" onclick="return find();" >查询</button>
				<!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
				<td nowrap="nowrap" class="normalText" width="25%"><img src="images/saveChart.gif" style="cursor:hand" onclick="return saveChart()"/></td>
			</tr>
		</html:table>

	<%
	if (result != null) {
		ChartUtilities.createChartBar("cewolf1", result, allYearMonth, pageContext);
	%>
		<cewolf:chart id="chart" title="成本同期对比" type="horizontalBar3D">
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="830" height="250" id="cewolf1" >
			<cewolf:map tooltipgeneratorid="cewolf1Tip"/>
		</cewolf:img>
	<% } %>

		<html:table clazz="complex">

			<tr><td><%=oper%></td></tr>

			<tr>
				<td>
			<% if ( result != null ) { %>
  		<html:table clazz="result">
     		<html:tr clazz='label'>
        	<td>项目编码</td>
          <td>项目名称</td>
        <% for(int i=0;i<allYearMonth.length;i++) { %>
              <td><%=allYearMonth[i]%></td>
				<%}%>
				</html:tr>

        <%
        for (int i = 0; i < result.length; i++) {
          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>
        <tr CLASS="<%=rowColor%>">
        	<td class="normalText" nowrap><%=result[ i ][ 0 ]%></td>
          <td class="normalText" nowrap><%=result[ i ][ 1 ]%></td>
          <% for(int j=2;j<result[0].length;j++) {%>
          	<td class="numberText" nowrap><%=nf.format(Double.parseDouble(result[ i ][ j ]))%></td>
          <%}%>
        </tr>
        <%}%>
      </html:table>
    	<%}%>
    		</td>
  		</tr>

		</html:table>
		<input type=hidden name="subFunction"/>
	</form>
</html:html>



