<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemcostcollect/costcollect/costcollect.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:28 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>

<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
function find() {
    if(template.year_month.value=="")
      {alert("请选择年月");
       return;
      }
    template.subFunction.value='findAll';
    template.submit();
    return true;
  }
function save()
{   template.subFunction.value='update';
    template.submit();
    return true;
 }

</Script>

<html:html clazz="main">
<form name="template" method="post" action="costquery.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>成本查询</html:title>
  <!-- 查询信息 -->
    <%
       DecimalFormat nf = new DecimalFormat("#,##0.00");
      BaseRO  ro= (BaseRO)request.getAttribute("ro");

      String[][] result=null;
      if(ro!=null)
        {result=ro.getTableResult();
       }
      TableMarge oper = new TableMarge(ro, "return find()");
    %>
   <html:table clazz="simple">
    <tr>

      <td nowrap class="signText">科室编码 ：</td>
      <td nowrap class="normalText">
      <input type="text" name="dept_code" size="20" value='<%=request.getParameter("dept_code")==null?"":request.getParameter("dept_code")%>' /></td>
      <td nowrap class="signText">科室名称：</td>
      <td nowrap class="normalText">
      <input type="text" name="dept_name" size="20" value='<%=request.getParameter("dept_name")==null?"":request.getParameter("dept_name")%>'/></td>
      <td class="signText" nowrap>核算月：</td>
      <td nowrap>
      <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
  </tr>
    <tr>

      <td nowrap class="signText">项目编码 ：</td>
      <td nowrap class="normalText">
      <input type="text" name="charge_detail_code" size="20" value='<%=request.getParameter("charge_detail_code")==null?"":request.getParameter("charge_detail_code")%>' /></td>
      <td nowrap class="signText">项目名称：</td>
      <td nowrap class="normalText">
     <input type="text" name="charge_detail_name" size="20" value='<%=request.getParameter("charge_detail_name")==null?"":request.getParameter("charge_detail_name")%>'/></td>
      <td>
      <button class="pageBtn" name="" onclick="return find();" >查询</button>
      <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
  </tr>

  </html:table>
<br>
   <% String year_month=request.getParameter("year_month"); %>
<html:title clazz='table'><%if(year_month!=null) out.print(year_month+"&nbsp;"+"&nbsp;"+"&nbsp;");%>项目成本列表</html:title>
  <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>
      <html:table clazz="result">
     <html:tr clazz='label'>
          <td >项目编码</td>
          <td >项目名称</td>
          <td >科室编码</td>
          <td >科室名称</td>
          <td >收入金额</td>
          <td >项目工作量</td>
          <td >科室项目总成本</td>
          <td >单位合作成本</td>
          <td >项目成本</td>
          <td >项目单位成本</td>

        </html:tr>

        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j]=="") result[i][j]="&nbsp;";
                }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>
        <tr CLASS="<%=rowColor%>">
           <td class="normalText" nowrap><%=result[ i ][ 0 ]%></td>
           <td class="normalText" nowrap><%=result[ i ][ 1 ]%></td>
           <td class="normalText" nowrap><%=result[ i ][ 2 ]%></td>
           <td class="normalText" nowrap><%=result[ i ][ 3 ]%></td>
           <td class="numberText" nowrap><%=nf.format(Double.parseDouble(result[ i ][ 4 ]))%></td>
           <td class="numberText" nowrap><%=nf.format(Double.parseDouble(result[ i ][ 5 ]))%></td>
           <td class="numberText" nowrap><%=nf.format(Double.parseDouble(result[ i ][ 6 ]))%></td>
           <td class="numberText" nowrap><%=nf.format(Double.parseDouble(result[ i ][ 7 ]))%></td>
           <td class="numberText" nowrap><%=nf.format(Double.parseDouble(result[ i ][ 8 ]))%></td>
           <td class="numberText" nowrap><%=nf.format(Double.parseDouble(result[ i ][ 9 ]))%></td>
        </tr>
        <%
              }
            }
        %>
     </html:table>
    </td>
  </tr>

  </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>


