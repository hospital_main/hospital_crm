<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemcostcollect/costupmonthcompare/costupmonthcompare.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<script language='javascript' src='javascript/chart.js'></script>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.cbcs.itemcheckcount.itemcostcollect.base.ResultAndStringTransform" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

 <%
      DecimalFormat nf = new DecimalFormat("#,##0.00");
      String[] allYearMonth=(String[])request.getAttribute("allYearMonth");
      BaseRO  ro= (BaseRO)request.getAttribute("ro");
      String[][] result=null;
      if(ro!=null)
        {result=ro.getTableResult();
       }
      TableMarge oper = new TableMarge(ro, "return find()");
    %>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
function find() {
     if(template.year_month_from.value=='')
     {alert("请选择年月")
      return false
     }
    if(template.year_month_to.value=='')
     {alert("请选择年月")
      return false
     }
    if(template.year_month_to.value<template.year_month_from.value)
    {alert("起始年月应大于终止年月")
      return false
     }
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }

	function save() {
		template.subFunction.value='update';
    template.submit();
    return true;
 }


  var cewolf1;
  <% if (result!=null) { %>
  cewolf1 = "";
  <% }%>

</Script>

<html:html >
<form name="template" method="post" action="costupmonthcompare.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>成本对比趋势</html:title>
  <!-- 查询信息 -->

  <html:table clazz="simple">
    <tr>
     <td></td>
     <td nowrap class="signText"><input type="radio" name="kind" value="1" <% if(request.getParameter("kind")==null) out.print("checked"); if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("1")) out.print("checked");%>>总成本</td>
     <td nowrap class="signText"><input type="radio" name="kind" value="2" <%if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("2")) out.print("checked");%>>单位成本</td>
     <td></td>
    </tr>
    <tr>
      <td nowrap class="signText">时间范围：</td>
      <td nowrap>
      <%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%>
     </td>
      <td>
      <button class="pageBtn" name="" onclick="return find();" >查询</button>
      <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
      <td><img src="images/saveChart.gif" style="cursor:hand" onclick="return saveChart()"/></td>
 		</tr>

 	</html:table>



	<%
  if (result != null ) {
  	ChartUtilities.createChartLine("cewolf1", result, allYearMonth, pageContext);
	%>
		<cewolf:chart id="chart" title="成本对比趋势" type="line">
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="830" height="250" id="cewolf1" />
	<% } %>

  <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>
<%  if ( result != null )
            {
%>
       <html:table clazz="result">
         <html:tr clazz='label'>
          <td >项目编码</td>
          <td >项目名称</td>
          <% for(int i=0;i<allYearMonth.length;i++)
              {
            %>
              <td ><%=allYearMonth[i]%></td>
          <%}%>
       </html:tr>

        <%

              for (int i = 0; i < result.length; i++ )
              {


          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>
        <tr CLASS="<%=rowColor%>">
           <td class="normalText" nowrap><%=result[ i ][ 0 ]%></td>
           <td class="normalText" nowrap><%=result[ i ][ 1 ]%></td>
        <%   for(int j=2;j<result[0].length;j++)
             {
         %>
           <td class="numberText" nowrap><%=nf.format(Double.parseDouble(result[ i ][ j ]))%></td>
          <%}%>
        </tr>
        <%
              }
         %>
         </html:table>
        <%
            }
        %>

    </td>
  </tr>
  </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>


