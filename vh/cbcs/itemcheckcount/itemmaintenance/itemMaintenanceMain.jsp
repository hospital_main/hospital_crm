<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemmaintenance/itemMaintenanceMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,
                com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<link href="" rel="stylesheet" type="text/css">
		<script language="javascript" id="scriptID"></script>
		<script language="javascript">parent.pageInit(window)</script>
		<script language='javascript'
			src="/vh/base/scripts/CreateObjects.js"></script>


<Script Language="JavaScript">
  function showmodul(src,name,sty){
   var tim=new Date();
   window.showModalDialog(src+'&time='+tim.toLocaleString(),name,sty)
   if (template!=null && template._old_current_page!=null) {
      template._current_page.value = template._old_current_page.value
   }
   find();
    }
	function create() {
		template.subFunction.value='preparedCreate';
		show_wait();
    template.submit();
		return true;
	}

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
  		if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
  		  flag = true;
  	}

    if( flag!=false) {
    	if (confirm('是否删除')) {
	      template.subFunction.value='remove';
	      show_wait();
	      template.submit();
	      return true;
	    } else
	    	return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectedAll(){
    for (var i=0; i<template.elements.length; i++) {
    	if (template.elements[i].name == 'primaryKey'
        && template.elements[i].disabled == false) {
          template.elements[i].checked = true;
    	}
    }
  }

  function find() {
	if(template.start.value>template.end.value){
		alert('起始年月必须小于终止年月')
		return false;
		}
	switch(isDouble(template.amount,12,4))
    {
      case 0 : alert('金额 必须为数字型'); return;
      case 1 : alert('金额 整数部分不能高于12个字符'); return;
      case 3 : alert('金额 小数部分不能高于4个字符'); return;
    }
    if(!isNumber(template.workload)){
	   alert("工作量 应为整数");
	   return;
   }
    template.subFunction.value='preparedfindAll';
    show_wait();
    template.submit();
    return true;
  }
  
	function accout() {
	  var d=new Date()
	  var s=d.toString()  
		window.showModalDialog("itemmaintenance.jspviewhigh?signs=collect&subFunction=preparedfindAll&start="+template.start.value+"&end="+template.end.value+"&charge_detail_code="+template.charge_detail_code.value+"&order_by="+template.order_by.value+"&perform_by="+template.perform_by.value+"&amount="+template.amount.value+"&workload="+template.workload.value+"&s="+s, window,"status:no;dialogHeight: 140px; dialogWidth: 500px;");
}

	//alert(window.document.getElementsByName("start"))
	
 function importData(){
		openDialog("cbcs/itemcheckcount/itemmaintenance/import.html", 'dialogWidth:900px;dialogHeight:600px;');
	}
			
			function saveToExcel(obj){
			if(topwindow.activex_lib==null){
				alert("请安装望海组件");
				return false;
			}
			if (document.getElementById('tableExcel')==null){
				return;
			}
			var oh = document.getElementById('tableExcel').outerHTML.replace(/<TH.*<INPUT.*<\/TH>/gi,"").replace(/<TD.*<\/INPUT>.*<\/TD>/gi,"").replace(/text-decoration.*none/gi,"").replace(/<TH.*: none.*<\/TH>/gi,"").replace(/<TD.*:[ ]{0,1}none.*<\/TD>/gi,"");
			oh = oh.replace(/<A/g, "<span "); //去掉链接
			topwindow.activex_lib.ExportExcel(oh);
		}
			
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="itemmaintenance.jspviewhigh">
     <!-- 信息提示栏 -->
	  <html:message/>

     <!--信息栏-->
	  <html:title clazz='module'>项目收入维护主页面</html:title>

     <!-- 简单信息 -->
	  <html:table clazz="simple">
      <tr id="test">
        <td nowrap class="signText" >起止日期：</td>
         <td nowrap class="normalText" colspan='1' >
         	<%=new BiMonthComponent("start",request.getParameter("start"),"end",request.getParameter("end")) %>
         	</td>

        <td nowrap class="signText">医疗项目名称：</td>
        <td nowrap class="normalText">
        <?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea" name="charge_detail_code" value="<%=request.getParameter("charge_detail_code")==null? "":request.getParameter("charge_detail_code")%>" AdjustVal="95" previousObj="start" codeCol='charge_detail_code' indexCodeSequence="charge_detail_code|charge_detail_name|spell" textCol="charge_detail_name" width="200" top="39" left="505" Lheight="5" xmlSource="dic/dict_charge_detail.xml" init="1"/>

        <%//new com.viewhigh.base.mvc.view.component.SingleSelect(request.getAttribute("dict_charge_detail"),"charge_detail_code",request.getParameter("charge_detail_code"),false,false)%></td>
      </tr>
      


      <tr>
        <td nowrap class="signText">开单科室：</td>
        <td nowrap class="normalText">
        <hzh:QInput ID="nosNameb" name="order_by" value="<%=request.getParameter("order_by")==null? "":request.getParameter("order_by")%>" AdjustVal="95" previousObj="start" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" Lwidth="100"  top="16" left="91" Lheight="5" width="200" xmlSource="dic/dict_acct_dept_LTD.xml" init="1" />
       
        <td nowrap class="signText">执行科室：</td>
        <td nowrap class="normalText">
        <hzh:QInput ID="nosNamec" name="perform_by" value="<%=request.getParameter("perform_by")==null? "":request.getParameter("perform_by")%>" AdjustVal="95" previousObj="start" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" Lwidth="100"  top="16" left="505" width="200" Lheight="5" xmlSource="dic/asst_dict_acct_dept_LTD.xml" init="1" />

        <%//new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("init_perfor_by"),"perform_by",request.getParameter("perform_by"),true,false)%></td>
      </tr>
      <tr>
        <td nowrap class="signText">金额：</td>
        <td nowrap class="normalText"><input type="text" name="amount" size="30" value='<%=request.getParameter("amount")==null?"":request.getParameter("amount")%>'/></td>
        <td nowrap class="signText">工作量:</td>
        <td nowrap class="normalText"><input type="text" name="workload" size="30" value='<%=request.getParameter("workload")==null?"":request.getParameter("workload")%>'/></td>
      </tr>
      <tr>
        <td align="right" colspan='4'>
        <button class="pageBtn" onclick="find()">查询</button>
        <button class="pageBtn" onclick="accout()">汇总</button>
        <button class='tableBtn' accessKey='I' name='cbcsRate_project_income_data_import'  onclick="importData();">导入</button>
        <button class="pageBtn" onclick="saveToExcel();" >导出</button>	
        </td>
      </tr>
	  </html:table>

    <br>
    <html:title clazz='table'>收入数据</html:title>
      <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if(ro!=null)
       {
	    TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectedAll()");   // 全选
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
      oper.addNeedButton("images/create.gif", "return create()");     //  添加
      %>

    <!-- 复杂信息 -->
	  <html:table clazz="complex" >
      <!-- 操作 -->
      <tr><td><%=oper%></td></tr>

      <!-- 结果集 -->

	  </html:table>
	  	  <div id="tableExcel">
	      <html:table clazz="result" >
	        <html:tr clazz='label'>
	        <!--table  width="100%" class="resultSetTable" id="tableExcel"-->
                <td class="resultLabel">选择</td>
                <td class="resultLabel">统计年月</td>
                <td class="resultLabel">医疗项目</td>
	              <td class="resultLabel">开单科室</td>
	              <td class="resultLabel">执行科室</td>
                <td class="resultLabel">金额</td>
                 <td class="resultLabel">录入人</td>
                 <td class="resultLabel">录入时间</td>
                 <td class="resultLabel">工作量</td>
	        </html:tr>
	
		 
              <%
               String emp_id = (String)  request.getAttribute("empid");
              	DecimalFormat nf = new DecimalFormat("#,##0.00");
				DecimalFormat mf = new DecimalFormat("#,##0");
                if (ro!=null) {
              	String[][] result = ro.getTableResult();
              	if (result!=null) {

                  for (int i = 0; i < result.length; i++ )
                 {
                boolean isCheckout =
                  Boolean.valueOf(result[i][12]).booleanValue();
                String[] temp = {result[i][8], result[i][9],result[i][10], result[i][11]};
                String primaryKey = ExtendTool.arrayToString(temp);
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
              %>
              <tr CLASS="<%=rowColor%>">
                  <td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>" <%if(isCheckout || !emp_id.equals(result[i][12])){out.print(" disabled ");}%>></td>
                  <td class="normalText">
                    <%if(!isCheckout &&  emp_id.equals(result[i][12])) {%>
                    <a href='#' onclick="showmodul('itemmaintenance.jspviewhigh?subFunction=showmod&width=394&height=275&src=itemmaintenance.jspviewhigh?subFunction=load@@primaryKey=<%=primaryKey%>',window,'dialogTop:150px;dialogLeft:270px;dialogHeight: 320px; dialogWidth: 400px;status:no')">
                    <%}%>
                      <%=result[i][0]%>
                    <%if(!isCheckout) {%>
                    </a>
                    <%}%>
                  </td>
                  <td class="normalText"><%=result[i][1]%></td>
                  <td class="normalText"><%=result[i][2]%></td>
                  <td class="normalText"><%=result[i][3]%></td>
                  <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[i][4]))%></td>
                  <td class="normalText"><%=result[i][5]%></td>
                  <td class="normalText"><%=result[i][6]%></td>
                  <td class="numberText"><%=mf.format(Double.parseDouble(result[i][7]))%></td>
              </tr>
              <%
                    }
                  }
                }

              %>
	      </html:table>
	  </div>
      <!-- 操作 -->

		<%}%>
    <input type=hidden name="subFunction"/>
<input type="hidden" name="initsub" value="sub"/>
  </form>
     
  
</html:html>