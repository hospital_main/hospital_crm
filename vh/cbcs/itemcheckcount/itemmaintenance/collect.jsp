<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemmaintenance/collect.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:28 $
     $Modtime: 03-09-02 16:24 $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
  com.viewhigh.cbcs.base.mvc.view.TableMarge,
  com.viewhigh.cbcs.base.sql.BaseRO, 
  com.viewhigh.cbcs.cbcs.util.*,java.text.*" %>
  <%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<html:html clazz="main">
    <body>
      	  	  <html:title clazz='module'>收入数据汇总</html:title>

            <%
            DecimalFormat mf = new DecimalFormat("#,##0.00");
            DecimalFormat nf = new DecimalFormat("#,##0");
            BaseRO ro = (BaseRO)request.getAttribute("baseRO");
            if(ro!=null){
            String[][] result=ro.getTableResult();
            String a="";
            String b="";
            
            if(result!=null){
              a=result[0][0];
              b=result[0][1];
            }
            %>
   	  <html:table clazz="simple">
        <tr>
          <td nowrap class="signText">金额:</td><td nowrap class="numberText"><%=mf.format(Double.parseDouble(a))%>元</td>
  			</tr>
        <tr>
          <td nowrap class="signText">工作量:</td><td nowrap class="numberText"><%=nf.format(Double.parseDouble(b))%></td>
  			</tr>
  			<tr><td></td><td></td><td><img src="images/priClose.gif" onclick="window.close()" class="mouse"/></td></tr>
      </html:table><%}%>
    </body>
</html:html>


