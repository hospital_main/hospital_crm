<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/costratedata/itemcostratedef/itemCostRateDefMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
--> 
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent,
								 com.viewhigh.cbcs.cbcs.util.SimpleSearch" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
	<link href="" rel="stylesheet" type="text/css">
<script language="javascript" id="scriptID"></script>
<script language="javascript">parent.pageInit(window)</script>
<Script Language="JavaScript">

	function dischange(){
		template.changesign.value="dischange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function kindchange(){
		template.changesign.value="kindchange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
    function create() {
        template.subFunction.value="preparedCreate";
        show_wait();
        template.submit();
        return true;
    }
	function InheritDate(){
		template.subFunction.value="preparedInherit";
		show_wait();
		template.submit();
		return true;
	}
  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
  
  function copy(){
  	template.subFunction.value="copy";
  	show_wait();
  	template.submit();
  }
  
  
  function importData(){
		//	window.showModalDialog("cbcs/itemcheckcount/datacollection/costratedata/itemcostratedef/import.html", "","dialogWidth=900px;dialogHeight=600px");
		openDialog("cbcs/itemcheckcount/datacollection/costratedata/itemcostratedef/import.html", 'dialogWidth:900px;dialogHeight:600px;');
		}
  
</Script>

<html:html clazz="main" scrollCtl="yes" fixRows="2">
	<form name="template" method="post" action="itemcostratedef.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'> 单位成本定义 </html:title>



	  <!-- 简单信息 -->  
	  <html:table clazz="simple"> 
		<tr>	    
			<td class="signText">启用月份:</td>
			<td class="normaltext"><%=new  MonthComponent("year_month", request.getParameter("year_month"))%></td>
	   		<td nowrap class="signText">成本分类: </td>
     		<td class="normalText" nowrap="nowrap"><%=new Select(request.getAttribute("dict_cost_class"),"cost_class_code",request.getParameter("cost_class_code") ,false,false)%></td>
	 		<td class="signText">收入项目:</td>
			<td class="normalText">
			<%
				Select dict_income_subj=new Select(request.getAttribute("dict_income_subj"),"dict_income_subj",request.getParameter("dict_income_subj")==null? "":request.getParameter("dict_income_subj"),false,false);
				dict_income_subj.setAttribute("onchange","dischange()");
			%>
			<%=dict_income_subj.toString() %></td>
		 </tr>
		 <tr>
			<td class="signText">停用月份:</td>
			<td class="normaltext"><%=new  MonthComponent("stop_month", request.getParameter("stop_month"))%></td>
			<td class="signText">收费类别:</td>
			<td class="normalText">
			<%
				Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"b_dict_charge_detail_kind",request.getParameter("b_dict_charge_detail_kind")==null? "":request.getParameter("b_dict_charge_detail_kind"),false,false);
				b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
			%>
			<%=b_dict_charge_detail_kind.toString() %></td>
			<td class="signText">医疗项目: </td>
			<td class="normalText">
			<%
				Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"dict_charge_detail",request.getParameter("dict_charge_detail")==null? "":request.getParameter("dict_charge_detail"),false,false);
			%>
			<%=dict_charge_detail.toString() %></td>
	      <td class="normalText"><button class="pageBtn" onclick="return find();">查询</button>
		  <button class="pageBtn" onclick="return InheritDate();">继承</button>
		  <button class='tableBtn' accessKey='I' name='cbcsproject_itemcostratedef_import'  onclick="importData();">导入</button>	
		  </td>
	    </tr>		      
	  </html:table>
	  

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	     // oper.addOptionButton("images/copy.gif", "return copy()");   //  拷贝
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
		 // oper.addNeedButton("images/inherit.gif", "return InheritDate()");     //  继承
	  %>
	  
  
		<html:title clazz='table'> 单位成本定义 </html:title>
		<!-- 复杂信息 -->
		<html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>
	    	
		<html:table clazz="result">
			<colgroup id=tg>
			<col style = 'width:10mm' >
        	<col style = 'width:30mm' >
          	<col style = 'width:30mm' >
          	<col style = 'width:30mm' >
          	<col style = 'width:30mm' >
          
          	<col style = 'width:20mm' >
          	<col style = 'width:20mm' >
          	<col style = 'width:30mm' >
          	<col style = 'width:20mm' >
          	<col style = 'width:20mm' >
          	<col style = 'width:20mm' >
          
          	<col style = 'width:20mm' >
          	<col style = 'width:20mm' >
          	<col style = 'width:30mm' >
        </colgroup>	        
          <tr class="resultLabel">
		          <td nowrap="nowrap" class="resultLabel" >选择</td>
		          <td nowrap="nowrap" class="resultLabel" >医疗项目编码</td>
		          <td nowrap="nowrap" class="resultLabel" >医疗项目</td>
		          <td nowrap="nowrap" class="resultLabel" >成本分类编号</td>
		          <td nowrap="nowrap" class="resultLabel" >成本分类名称</td>		          
		          <td nowrap="nowrap" class="resultLabel" >单位成本 </td>
		          <td nowrap="nowrap" class="resultLabel" >直接成本</td>		          
		          <td nowrap="nowrap" class="resultLabel" >计算计入成本 </td>
		          <td nowrap="nowrap" class="resultLabel" >公用分摊</td>
		          <td nowrap="nowrap" class="resultLabel" >管理分摊</td>
		          <td nowrap="nowrap" class="resultLabel" >医辅分摊</td>
		          <td nowrap="nowrap" class="resultLabel" >启用月份</td>
		          <td nowrap="nowrap" class="resultLabel" >停用月份</td>
		          <td nowrap="nowrap" class="resultLabel" >成本范围标识</td>
		        </tr>

		        <%
		        
		          java.text.DecimalFormat mf=new java.text.DecimalFormat("#,##0.0000");
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ]+" "+result[ i ][ 2 ]+" "+result[ i ][ 10 ]+" "+result[ i ][ 13 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>
 
		        <tr CLASS="<%=rowColor%>">
		          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
		          <td nowrap="nowrap" class="normalText"><a href="itemcostratedef.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>"><%=result[ i ][0 ]%></a></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td> 
		          <td nowrap="nowrap" class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 4 ]))%></td>
		          <td nowrap="nowrap" class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 5 ]))%></td>
		          <td nowrap="nowrap" class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 6 ]))%></td>
		          <td nowrap="nowrap" class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 7 ]))%></td>
		          <td nowrap="nowrap" class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 8 ]))%></td>
		          <td nowrap="nowrap" class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 9 ]))%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 10 ]%></td> 
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 11 ]%></td>  
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 12 ]%></td>  

		        </tr>

		        <%
		              }
		            }
		        %>
		</html:table>
		<%}%>
		<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	  <input type='hidden' name="subFunction"/>
	  <input type="hidden" name="initsub" value="sub"/>
	</form>
</html:html>