<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/costratedata/itemcostratedef/itemCostRateDefCreate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                 com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
    if(template.dict_charge_detail.value =="")
    {	
     	alert('医疗项目不能为空!');
     	return false;
    }
    if(template.year_month.value =="")
     {
    	alert('启用月份不能为空!');
    	return false;
   	 }
   	 
   	switch(isDouble(template.prime_cost,10,4))
    {
      case 0 : alert('直接成本必须为数字型'); return;
      case 1 : alert('直接成本小数部分不能高于4个字符'); return;
    }
    switch(isDouble(template.apport_cost_d,10,4))
    {
      case 0 : alert('计算计入成本必须为数字型'); return;
      case 1 : alert('计算计入小数部分不能高于4个字符'); return;
    }
    switch(isDouble(template.apport_cost_s,10,4))
    {
      case 0 : alert('管理分摊成本必须为数字型'); return;
      case 1 : alert('管理分摊小数部分不能高于4个字符'); return;
    }
    switch(isDouble(template.adm_cost,10,4))
    {
      case 0 : alert('公用分摊成本必须为数字型'); return;
      case 1 : alert('公用分摊小数部分不能高于4个字符'); return;
    }
    switch(isDouble(template.apport_cost_t,10,4))
    {
      case 0 : alert('医辅分摊成本必须为数字型'); return;
      case 1 : alert('医辅分摊小数部分不能高于4个字符'); return;
    }
   	    template.subFunction.value='Create';
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemcostratedef.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>添加单位成本</html:title>
  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td nowrap class="signText">医疗项目名称:</td>
      <td class="normalText" nowrap="nowrap" >
        <?xml:namespace prefix="hzh"/>
      <hzh:QInput ID="nosNamea" name="dict_charge_detail" value="<%=request.getParameter("dict_charge_detail")==null? "":request.getParameter("dict_charge_detail")%>" AdjustVal="20" previousObj="dict_cost_class" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="37" left="155" Lheight="5" xmlSource="dic/dict_charge_detail_LV.xml" init="1"/>
	    </td>
	    <td class="signText" nowrap="nowrap" >成本类别：</td>
      <td class="normalText" nowrap="nowrap"><%=new Select(request.getAttribute("dict_cost_class"),"dict_cost_class","" ,false,false)%></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">直接成本:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="prime_cost" class="textInputC" /></td>
       <td class="signText" nowrap="nowrap">计算计入成本：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="apport_cost_d" class="textInputC" /></td>
    </tr> 
    <tr>
      <td class="signText" nowrap="nowrap">管理分摊：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="apport_cost_s" class="textInputC" /></td>
      <td class="signText" nowrap="nowrap">公用分摊：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="adm_cost" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">医辅分摊：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="apport_cost_t" class="textInputC" /></td>
    <!--  <td class="signText" nowrap="nowrap">单位成本：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="tot_amount" class="textInputC" /></td>-->
      <td class="signText" nowrap="nowrap">启用月份:</td>
			<td class="normaltext"><%=new  MonthComponent("year_month", request.getParameter("year_month"))%></td>
    </tr>
    <tr>
	  <td class="signText" nowrap="nowrap">成本范围标识：</td>
       <td class="normalText" nowrap="nowrap">
    		     内部成本<input type="radio" name="typecost" value="0" checked/>
             协作成本<input type="radio" name="typecost" value="1" />
       
       </td>
    </tr>
    <tr> <td><br><br></td></tr>      
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return create();">添加</button> <button class="pageBtn" onclick="return reset();">重置</button> <button class="pageBtn" onclick="return back(template);">返回</button></td>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="Create"/>
</form>

</html:html>   