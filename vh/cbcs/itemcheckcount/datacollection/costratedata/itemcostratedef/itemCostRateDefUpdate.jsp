<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/costratedata/itemcostratedef/itemCostRateDefUpdate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
                        com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    
    template.subFunction.value='Update';
    template.submit();
    
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="itemcostratedef.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module"> 单位成本修改</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息  year_month, charge_detail_code, cost_class_code, cvalue, stop_month -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">医疗项目:</td>
      <td width="75%" class="normalText" nowrap="nowrap"><input type="text" name="charge_detail_code" value=<%=result[4] %> class="textInputC" disabled /></td>
    </tr>
      <td class="signText" nowrap="nowrap" >成本类别：</td>
      <td class="normalText" nowrap="nowrap"><input type="text" name="cost_class_code" value=<%=result[5] %> class="textInputC" disabled /></td>
    </tr>
    <tr>
     <td class="signText" nowrap="nowrap">直接成本:</td>
      <td class="normalText" nowrap="nowrap"><input type="text" name="prime_cost" value=<%=result[6] %> class="textInputC" /></td>
     <!-- <td class="signText" nowrap="nowrap">单位成本：</td>
      <td class="normalText" nowrap="nowrap"><input type="text" name="cvalue" value=<%=result[3] %> class="textInputC" /></td> -->
    </tr>
    <tr>
    <td class="signText" nowrap="nowrap">计算计入成本：</td>
      <td class="normalText" nowrap="nowrap"><input type="text" name="apport_cost_d" value=<%=result[7] %> class="textInputC" /></td>
    </tr>
    <tr>
    <td class="signText" nowrap="nowrap">管理分摊：</td>
      <td class="normalText" nowrap="nowrap"><input type="text" name="apport_cost_s" value=<%=result[9] %> class="textInputC" /></td>
    </tr>
    <tr>
    <td class="signText" nowrap="nowrap">公用分摊：</td>
      <td class="normalText" nowrap="nowrap"><input type="text" name="adm_cost" value=<%=result[8] %> class="textInputC" /></td>
    </tr>
    <tr>
    <td class="signText" nowrap="nowrap">医辅分摊：</td>
      <td class="normalText" nowrap="nowrap"><input type="text" name="apport_cost_t" value=<%=result[10] %> class="textInputC" /></td>
    </tr>
    <tr>
    <td class="signText" nowrap="nowrap">成本范围标识：</td>
       <td class="normalText" nowrap="nowrap">
    		     内部成本<input type="radio" name="typecost" value="0" <% if(result[11]==null) out.print("checked"); if(result[11]!=null&& result[11].equals("0")) out.print("checked");%>>
             协作成本<input type="radio" name="typecost" value="1" <% if(result[11]!=null && result[11].equals("1")) out.print("checked");%>>
     </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">启用月份:</td>
			<td class="normaltext"><input type="text" name="stop_month" value=<%=result[0] %> class="textInputC" disabled /></td>
    </tr> 
     <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button> <button class="pageBtn" onclick="return reset();">重置</button> <button class="pageBtn" onclick="return back(template);">返回</button></td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="initsub" value="sub"/>
  <input type="hidden" name="year_month" value="<%=result[0]%>">
  <input type="hidden" name="charge_detail_code" value="<%=result[1]%>">
  <input type="hidden" name="cost_class_code" value="<%=result[2]%>">
  <%}%>
</form>


</html:html>
