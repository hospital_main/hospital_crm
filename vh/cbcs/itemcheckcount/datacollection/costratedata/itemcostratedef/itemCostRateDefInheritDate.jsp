<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/costratedata/itemcostratedef/itemCostRateDefInheritDate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                        com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
	//继承
	function InheritDate(){
		
		 if(template.new_charge_detail_code_class.value =="")
		 {	
			alert('新增加项目不能为空!');
			return false;
		 }
		 if(template.dict_charge_detail_code_class.value =="")
		 {	
			alert('作业模块项目不能为空!');
			return false;
		 }
		template.subFunction.value='Inherit';
        template.submit();
        return true;
	}
  
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemcostratedef.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>添加单位成本</html:title>
  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap" >新增加项目：</td>
      <td class="normalText" nowrap="nowrap"><%=new Select(request.getAttribute("new_charge_detail_code_class"),"new_charge_detail_code_class","" ,false,false)%></td>

	    <td class="signText" nowrap="nowrap" >作业模块项目：</td>
      <td class="normalText" nowrap="nowrap"><%=new Select(request.getAttribute("dict_charge_detail_code_class"),"dict_charge_detail_code_class","" ,false,false)%></td>
    </tr>
	<tr>
	</tr>
    <tr>
	 <!-- <td class="signText" nowrap="nowrap">是否继承：</td>
       <td class="normalText" nowrap="nowrap">
			是<input type="radio" name="Inheritchoose" value="0" checked/>
			否<input type="radio" name="Inheritchoose" value="1" />
       </td>-->
    </tr>
    <tr> <td><br><br></td></tr>      
    <tr>
      <td colspan="2">  <img src="images/inherit.gif" class="mouse" onclick="return InheritDate();" /> <img src="images/return.gif" class="mouse" onclick="return back(template);" /></td>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="Inherit"/>
</form>

</html:html>   