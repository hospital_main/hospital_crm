<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/costratedata/itemcostrateshow/itemCostRateShowMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent,
								 com.viewhigh.cbcs.cbcs.util.DisplayWidth,java.text.*,
								 com.viewhigh.cbcs.cbcs.util.SimpleSearch" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
	<link href="" rel="stylesheet" type="text/css">
<script language="javascript" id="scriptID"></script>
<script language="javascript">parent.pageInit(window)</script>


<Script language="javascript">
	function dischange(){
		template.changesign.value="dischange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function kindchange(){
		template.changesign.value="kindchange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function find(){
		if(template.year_month.value==''){
		alert('请选择核算月份!');
		return false;
		}
		show_wait();
		template.subFunction.value="findAll";
		template.submit();
		return;
	}
</Script>

<html:html clazz="main" scrollCtl="yes" fixRows="2" isPrint="true">
	<form name="template" method="post" action="itemcostrateshow.jspviewhigh">
		<html:message/>
		<html:title clazz='module'>医疗项目单位成本查询</html:title>
		<%
			String[][] result=null;
		%>
		<html:table clazz="simple">
			<tr>
		        <td class="signText">核算月:</td>
		        <td class="normaltext"><%=new  MonthComponent("year_month", request.getParameter("year_month"),true)%></td>		 
		        <td class="signText">成本分类:</td> 
		        <td>
		              <select multiple="multiple" name="dict_cost_class_code" size="4" >
		              <%String[][] dept=(String[][])request.getAttribute("dict_cost_class");
		              String[] dval=request.getParameterValues("dict_cost_class_code");
		              System.out.println("dept"+dept);
		              System.out.println("dval"+dval);
		              %>
		              <option value="" <%if(dval==null||(dval.length==1&&dval[0].equals(""))||(dval.length>1&&dval[0].equals(""))){
		              out.print("selected");}%>>--不限--</option >
		              <%
		              if(dept!=null){
		              for(int i=0;i<dept.length;i++){%>
		              <option value="<%=dept[i][0]%>"
		              <%if(dval!=null){for(int j=0;j<dval.length;j++){
		                if(dept[i][0].equals(dval[j])){
		                out.print("selected");
		                }}}%>>
		                <%=dept[i][1]%></option>
		              <%}}%>
		              </select>  
		         </td> 
				<td class="signText">收入项目:</td>
				<td class="normalText">
				<%
				Select dict_income_subj=new Select(request.getAttribute("dict_income_subj"),"dict_income_subj",request.getParameter("dict_income_subj")==null? "":request.getParameter("dict_income_subj"),false,false);
				dict_income_subj.setAttribute("onchange","dischange()");
				%>
				<%=dict_income_subj.toString() %></td>
				
				<td class="signText">收费类别:</td>
				<td class="normalText">
				<%
				Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"b_dict_charge_detail_kind",request.getParameter("b_dict_charge_detail_kind")==null? "":request.getParameter("b_dict_charge_detail_kind"),false,false);
				b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
				%>
				<%=b_dict_charge_detail_kind.toString() %></td>
				<td class="signText">医疗项目: </td>
				<td class="normalText">
				<%
				Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"dict_charge_detail",request.getParameter("dict_charge_detail")==null? "":request.getParameter("dict_charge_detail"),false,false);
				%>
				<%=dict_charge_detail.toString() %></td>
			</tr>
			<tr>
				<td colspan="10" align="center">
				<button class="pageBtn"  onclick="return find();">查询</button>
				<%
					BaseRO ro = (BaseRO)request.getAttribute("baseRO");
					if (ro!=null) {
						TableMarge oper = new TableMarge(ro, "return find()"); 
						result=(String[][])ro.getTableResult();
				%>
				<button class="pageBtn" align='center' onclick="return preparedPrint();">打印</button>
				<html:table clazz="complex">
			    <!-- 操作 -->
			    <tr><td><%=oper%></td></tr></html:table>				
				<%}%>
				</td>
			</tr>
		</html:table>
			
		<br>

		<%
			String Tyear=(String) request.getParameter("year_month")==null?"": request.getParameter("year_month");
			String Title="";
			if(!Tyear.trim().equals(""))
				 Title ="("+Tyear.substring(0,4)+" 年 "+Tyear.substring(4,6)+"月)";
		%>
		<html:title clazz='table'>医疗项目单位成本查询<%=Title%></html:title>
			<!-- 复杂信息 -->
	  		<html:table clazz="result">
					<colgroup id=tg>
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >	
						<col style = <%=DisplayWidth.MONEY_WIDTH%> > 	
						<col style = <%=DisplayWidth.MONEY_WIDTH%> > 	
						<col style = <%=DisplayWidth.MONEY_WIDTH%> > 	
						<col style = <%=DisplayWidth.MONEY_WIDTH%> > 	
						<col style = <%=DisplayWidth.MONEY_WIDTH%> > 	
						<col style = <%=DisplayWidth.MONEY_WIDTH%> > 
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >	
					</colgroup>
					<tr class="resultLabel">
						<td nowrap="nowrap" class="resultLabel" >医疗项目编码</td>
						<td nowrap="nowrap" class="resultLabel" >医疗项目</td>
						<td nowrap="nowrap" class="resultLabel" >成本分类编号</td>
						<td nowrap="nowrap" class="resultLabel" >成本分类名称</td>
						<td nowrap="nowrap" class="resultLabel" >单位成本</td>
						<td nowrap="nowrap" class="resultLabel" >直接成本</td>
						<td nowrap="nowrap" class="resultLabel" >计算计入成本</td>
						<td nowrap="nowrap" class="resultLabel" >公用分摊</td>
						<td nowrap="nowrap" class="resultLabel" >管理分摊</td>
						<td nowrap="nowrap" class="resultLabel" >医辅分摊</td>
						<td nowrap="nowrap" class="resultLabel" >成本范围标识</td>
					</tr>
					<%
					 java.text.DecimalFormat mf=new java.text.DecimalFormat("#,##0.0000");
					if(result!=null){
					for(int i=0;i<result.length;i++){
						String rowColor = "rowGray";
						if (i/2*2==i) rowColor = "rowWhite";
					%>
					<tr CLASS="<%=rowColor%>">
						<td nowrap class="normalText" style='text-align:left'><%=result[i][0]%></td>					
						<td nowrap class="normalText" style='text-align:left'><%=result[i][1]%></td>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][2]%></td>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][3]%></td>
					  <td nowrap class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 4 ]))%></td>
	          <td nowrap class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 5 ]))%></td>
	          <td nowrap class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 6 ]))%></td>
	          <td nowrap class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 7 ]))%></td>
	          <td nowrap class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 8 ]))%></td>
	          <td nowrap class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 9 ]))%></td>
	          <td nowrap class="normalText" style='text-align:right'><%=result[i][10]%></td>
					</tr>	
			<%}}%>				
			</html:table>
		
		<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
		<input type="hidden" name="subFunction" value="findAll">
	</form>
</html:html>