<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/costratedata/deptitemcostratedef/deptItemCostRateDefMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
--> 
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent,
								 com.viewhigh.cbcs.cbcs.util.SimpleSearch" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<link href="" rel="stylesheet" type="text/css">
<script language="javascript" id="scriptID"></script>
<script language="javascript">parent.pageInit(window)</script>
<Script Language="JavaScript">

	function dischange(){
		template.changesign.value="dischange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function kindchange(){
		template.changesign.value="kindchange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
    function create() {
        template.subFunction.value="preparedCreate";
        show_wait();
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
  	 for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
  
  function copy(){
  	template.subFunction.value="copy";
  	show_wait();
  	template.submit();
  }
  
    function importData(){
		openDialog("cbcs/itemcheckcount/datacollection/costratedata/deptitemcostratedef/import.html", 'dialogWidth:900px;dialogHeight:600px;');
	}
  
</Script>

<html:html clazz="main" scrollCtl="yes" fixRows="2">
	<form name="template" method="post" action="deptitemcostratedef.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'> 科室单位成本定义 </html:title>



	  <!-- 简单信息 -->  
	  <html:table clazz="simple"> 
		<tr>	    
			<td class="signText">启用月份:</td>
			<td class="normaltext"><%=new  MonthComponent("year_month", request.getParameter("year_month"))%></td>
		<td nowrap class="signText">成本分类： </td>
		<td  nowrap="true"><%=new Select(request.getAttribute("dict_cost_class"),"cost_class_code",request.getParameter("cost_class_code") ,false,false)%></td>
		<td class="signText">收入项目：</td>
		<td class="normalText">
			<%
			Select dict_income_subj=new Select(request.getAttribute("dict_income_subj"),"dict_income_subj",request.getParameter("dict_income_subj")==null? "":request.getParameter("dict_income_subj"),false,false);
			dict_income_subj.setAttribute("onchange","dischange()");
			%>
			<%=dict_income_subj.toString() %>
		</td>
		<td class="signText">科室: </td>
		<td class="normalText"><?xml:namespace prefix="hzh"/>
			<hzh:QInput ID="nosNamea1" name="dept_code"  value="<%=request.getParameter("dept")==null? "":request.getParameter("dept_code")%>"  AdjustVal="147" previousObj="dict_charge_detail" codeCol='dept_code' valueCol="dept_code"  textCol="dept_name" width="120" top="42" left="40" Lheight="5" xmlSource="dic/dict_acct_dept_T.xml" init="1"/>
		</td>
		</tr>
		<tr>			
			<td class="signText">停用月份:</td>
			<td class="normaltext"><%=new  MonthComponent("stop_month", request.getParameter("stop_month"))%></td>
			<td class="signText">收费类别：</td>
			<td class="normalText">
			<%
			Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"b_dict_charge_detail_kind",request.getParameter("b_dict_charge_detail_kind")==null? "":request.getParameter("b_dict_charge_detail_kind"),false,false);
			b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
			%>
			<%=b_dict_charge_detail_kind.toString() %></td>
			<td class="signText">医疗项目: </td>
			<td class="normalText">
			<%
			Select dict_charge_detail =new Select(request.getAttribute("dict_charge_detail"),"dict_charge_detail",request.getParameter("dict_charge_detail")==null? "1111":request.getParameter("dict_charge_detail"),false,false);
			%>
			
			<%=dict_charge_detail.toString() %></td>
	      <td class="normalText"><button class="tableBtn" onclick="return find();">查询</button>
	      		<button class='tableBtn' accessKey='I' name='cbcsproject_deptItemCostRateDef_import'  onclick="importData();">导入</button>
	      		</td>
	      
	    </tr>		      
	  </html:table>
	  <%
	  
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	     // oper.addOptionButton("images/copy.gif", "return copy()");   //  拷贝
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
	  
  
		<html:title clazz='table'> 单位成本定义 </html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>

		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td nowrap="nowrap" class="resultLabel" >选择</td>
		          <td nowrap="nowrap" class="resultLabel" >科室编码</td>
		          <td nowrap="nowrap" class="resultLabel" >科室名称</td>
		          <td nowrap="nowrap" class="resultLabel" >医疗项目编码</td>
		          <td nowrap="nowrap" class="resultLabel" >医疗项目</td>
		          <td nowrap="nowrap" class="resultLabel" >成本分类编号</td>
		          <td nowrap="nowrap" class="resultLabel" >成本分类名称</td>		          
		          <td nowrap="nowrap" class="resultLabel" >单位成本 </td>
		          <td nowrap="nowrap" class="resultLabel" >直接成本</td>		          
		          <td nowrap="nowrap" class="resultLabel" >计算计入成本 </td>
		          <td nowrap="nowrap" class="resultLabel" >公用分摊</td>
		          <td nowrap="nowrap" class="resultLabel" >管理分摊</td>
		          <td nowrap="nowrap" class="resultLabel" >医辅分摊</td>
		          <td nowrap="nowrap" class="resultLabel" >启用月份</td>
		          <td nowrap="nowrap" class="resultLabel" >停用月份</td>
		          <td nowrap="nowrap" class="resultLabel" >成本范围标识</td>
		        </html:tr>

		        <%
		       
		          java.text.DecimalFormat mf=new java.text.DecimalFormat("#,##0.0000");
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ]+" "+result[ i ][ 2 ]+" "+result[ i ][ 4 ]+" "+result[ i ][ 12 ]+" "+result[ i ][ 15 ];
		              for (int j=0; j<result[i].length; j++) {
		              
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		           
		        %>
 
		        <tr CLASS="<%=rowColor%>">
		          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
		          <td nowrap="nowrap" class="normalText"><a href="deptitemcostratedef.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>"><%=result[ i ][ 0 ]%></a></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td> 
		          <td nowrap="nowrap" class="normalText" style='text-align:left'><%=result[ i ][ 4 ]%></td>
		          <td nowrap="nowrap" class="normalText" style='text-align:left'><%= result[ i ][ 5 ] %></td>
		          <td nowrap="nowrap" class="normalText" style='text-align:right'><%=  mf.format(Double.parseDouble(result[ i ][ 6 ]))  %></td>
		          <td nowrap="nowrap" class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 7 ]))%></td>
		          <td nowrap="nowrap" class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 8 ]))%></td>
		           
		          <td nowrap="nowrap" class="normalText" style='text-align:right'><%=mf.format(Double.parseDouble(result[ i ][ 9 ]))%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 10 ]%></td> 
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 11 ]%></td>  
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 12 ]%></td>  
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 13 ]%></td>  
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 14 ]%></td>  

		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		<%}%>
		<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	  <input type='hidden' name="subFunction"/>
	  <input type="hidden" name="initsub" value="sub"/>
	</form>
</html:html>

<%
out.flush();
%>
<script language="javascript">
	 template.cost_class_code.style.width = '100px';
 
</script>