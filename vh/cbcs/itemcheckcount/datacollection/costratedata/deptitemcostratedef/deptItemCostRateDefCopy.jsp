<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/costratedata/deptitemcostratedef/deptItemCostRateDefCopy.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
var seed=1;
  //添加
	function confrim() {
		if (template.dict_charge_detail_s.value =="")
		{
			alert("请选择数据源的医疗项目名称!");
			return;
		}
		if (template.dict_charge_detail_t.value =="")
		{
			alert("请选择目标的医疗项目名称!");
			return;
		}
		if (template.dict_charge_detail_s.value ==template.dict_charge_detail_t.value)
		{	//目标与源数据 如果 医疗项目代码相同
			//那么 起用年月必须不为空，且不相同
			if (template.year_month_s.value ==template.year_month_t.value){
					alert("当前设置无法进行复制!");
					return;
				}
			if(''==template.year_month_s.value){
				alert("请选择数据源的启用年月!");
				return;
			}
			if(''==template.year_month_t.value){
				alert("请选择目标的启用年月!");
				return;
			}
		}else if(''==template.year_month_s.value&&''!=template.year_month_t.value){
			//如果 目标与 源数据的医疗项目代码不相同
			//那么 如果目标的启用年月不为空 源的也不能为空
			alert("请选择数据源的启用年月!");
			return;
		}	
		seed++;
		netA.Gsend("itemcostratedef.jspviewhigh?subFunction=check&dict_charge_detail_s="+template.dict_charge_detail_s.value+"&year_month_s="+template.year_month_s.value+"&dict_charge_detail_t="+template.dict_charge_detail_t.value+"&year_month_t="+template.year_month_t.value+"&seed="+seed,copyAccess);
	}
	function copyAccess(){
		var num=new Number(netA.req.responseText);
		
		if(isNaN(num)){
			alert(netA.req.responseText);
		}else{	
			if(netA.req.responseText>0){
				if(confirm("拷贝目标存在数据，确认将会覆盖已存在数据，确认？")){
					doCopy();
				}
			}else{
				doCopy();
			}
		}
  }
  function doCopy(){
		template.subFunction.value="copy";
		show_wait();
    template.submit();
    return true;
  }
     
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemcostratedef.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>    

<%
	String [][]dict_charge_detail = (String [][])request.getAttribute("dict_charge_detail");
%>
  <!-- 标题栏 -->
	 <html:title clazz='module'>单位成本数据复制界面  </html:title>  
  <!-- 简单信息 -->   
  <table  width="100%" cellspacing="2" border="0">
  	<tr><td colspan=2 >数据源: </td> <td>目标: </td>
    </tr>
    <tr>   
      <td class="signText" nowrap="nowrap">医疗项目名称:</td>
      <td nowrap class="normalText"><%=new Select(dict_charge_detail,"dict_charge_detail_s",request.getParameter("dict_charge_detail_s")==null ? "":request.getParameter("dict_charge_detail_s"),false,false)%></td>
      <td class="signText" nowrap="nowrap">医疗项目名称:</td>
      <td align="left"><%=new Select(dict_charge_detail,"dict_charge_detail_t",request.getParameter("dict_charge_detail_t")==null ? "":request.getParameter("dict_charge_detail_t"),false,false)%></td>
		</tr>
		<tr>
     <td class="signText" nowrap="nowrap">启用月份：</td>
     <td nowrap class="normalText"><%=new  MonthComponent("year_month_s", request.getParameter("year_month_s")==null ? "":request.getParameter("year_month_s"))%></td>
     <td class="signText" nowrap="nowrap">启用月份：</td>
     <td nowrap class="normalText"><%=new  MonthComponent("year_month_t", request.getParameter("year_month_t")==null ? "":request.getParameter("year_month_t"))%></td>
    </tr>
    <tr><td><br><br></td></tr>
   	<tr align="center">
      <td colSpan="4"> <img src="images/confirm.gif" class="mouse" onclick="return confrim();" />&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/return.gif" class="mouse" onclick="return back(template);" /></td>
    </tr>
    
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   