<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/directcost/itemdcostdef/itemCostDefUpdate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.util.SimpleSearch,
		com.viewhigh.cbcs.base.mvc.view.component.Select,
		com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>

<Script Language="JavaScript">
  function save()
  {  
    if(trim(template.year_month.value)==""){
	     alert("请选择年月!");
	 	   return;
     }
    if(trim(template.dept_code.value)==""){
	     alert("请选择科室!");
	 	   return;
     }
    if(trim(template.charge_detail_code.value)==""){
	     alert("请选择医疗项目!");
	 	   return;
     } 
     if(trim(template.amount.value)==""){
	     alert("请输入数量!");
	 	   return;
     }             
    if(!isNumber(template.age)){
	     alert("年龄必须为数字!");
	 	   return;
     }
 	 if(!isNumber(template.case_id)){
	     alert("病例号必须为数字!");
	 	   return;
     }
    if(!isNumber(template.amount)){
    	  alert("数量必须为数字!");
	 	   return;
     }   
    template.subFunction.value='Update';
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="itemdcostdef.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module"> 直接成本修改</html:title>

  <%
    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
  	<tr>
      <td class="signText" nowrap="nowrap">年月：</td>
	  <td nowrap class="normalText"  ><%=new MonthComponent("year_month",result[1])%></td> 
	</tr>
    <tr>
      <td class="signText" nowrap="nowrap">科室：</td>
      <td>
    	  <?xml:namespace prefix="hzh"/>
       <hzh:QInput ID="nosNamec" name="dept_code" value="<%=result[2]%>" AdjustVal="60" previousObj="year_month" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" Lwidth="100"  top="12" left="196" width="200" Lheight="5" xmlSource="dic/asst_dict_acct_dept_LTD.xml" init="1" /> 
			</td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">病历号：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="case_id"value="<%=result[7]%>"  class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">姓名：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="name" value="<%=result[8]%>"  class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">年龄：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="age" value="<%=result[9]%>" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">性别：</td> 
          <td class="normalText" nowrap="nowrap">
              <input type="radio" name="male" value="M" <% if(result[10]==null) out.print("checked"); if(result[10]!=null&& result[10].equals("M")) out.print("checked");%>>男
              <input type="radio" name="male" value="F" <% if(result[10]!=null && result[10].equals("F")) out.print("checked");%>>女 
           </td>
    </tr>
 	   <tr>
 	     <td class="signText" nowrap="nowrap">医疗项目：</td>
			<td> <?xml:namespace prefix="hzh"/> 
       <hzh:QInput ID="nosNamec" name="charge_detail_code" value="<%=result[3]%>" AdjustVal="10" previousObj="male" codeCol='charge_detail_code' indexCodeSequence="charge_detail_code|charge_detail_name|spell"  textCol="charge_detail_name" Lwidth="100"  top="158" left="370" width="200" Lheight="5" xmlSource="dic/dict_charge_detail_LV.xml" init="1" /> 
			</td> 
   </tr>
    
    <!-------联动------->
    
      <tr>
 	    <td class="signText" nowrap>材料类别:</td>		
		       <td>                      
		       <%
		    //******  开始生成javascript
		        String[][] seconditem= SimpleSearch.find(" select distinct  inv_code,inv_name +'	['+unit_name+']' inv_name,inv_type_code from dict_inv  LEFT JOIN dict_unit ON dict_inv.unit_code= dict_unit.unit_code ");
		        String[][] firstitem=SimpleSearch.find("  select distinct inv_type_code,inv_type_name from  dict_inv_type ");                      
		        String[] names = {"values","texts", "relations"};
		        out.println(com.viewhigh.base.util.SimpleSearch.dynamicSelect(names, seconditem, "dynamicSelect", request.getParameter("inv_code")));
		   //******  结束 生成javascript                      
		        Select super_code = new Select(firstitem, "inv_type_code", request.getParameter("inv_type_code"), false, false);
		   //******  开始调用javascript
		        super_code.setAttribute("onchange", "dynamicSelect(template.inv_type_code, template.inv_code)");
		  //******  结束调用javascript
		  			out.println(super_code);
		    %>
		      </td>
		  </tr>
		  <tr>
		     <td class="signText" nowrap>材料编码:</td>
		     <td ><% Select seco_code = new Select("inv_code");	out.println(seco_code); %>
		     </td> 	    	    
	    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">数量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="amount"  value="<%=result[5]%>" class="textInputC" /></td>
    </tr> 
     <tr>
      <td colspan="2"><img src="images/save.gif" class="mouse" onclick="return save();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> <img src="images/return.gif" class="mouse" onclick="return back(template);" /></td>
    </tr>
  </table>
  <input type=hidden name = inv_type_code1 value="<%=result[11]%>">
  <input type=hidden name = inv_code1 value="<%=result[4]%>">
  <input type=hidden name="charge_cost_id" value =<%=result[0]%>>
  <input type=hidden name="subFunction" value = "save"/>
  <%}%>
<input type="hidden" name="initsub" value="sub"/>
</form>


</html:html>
<script>
    template.inv_type_code.value=template.inv_type_code1.value;
    template.inv_type_code.onchange();
    template.inv_code.value=template.inv_code1.value;
</script>