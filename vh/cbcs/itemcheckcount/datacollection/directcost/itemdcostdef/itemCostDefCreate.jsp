<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/directcost/itemdcostdef/itemCostDefCreate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent,
								 com.viewhigh.cbcs.cbcs.util.SimpleSearch" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
    if(trim(template.year_month.value)==""){
	     alert("请选择年月!");
	 	   return;
     }
    if(trim(template.dept_code.value)==""){
	     alert("请选择科室!");
	 	   return;
     }
    if(trim(template.charge_detail_code.value)==""){
	     alert("请选择医疗项目!");
	 	   return;
     } 
     if(trim(template.amount.value)==""){
	     alert("请输入数量!");
	 	   return;
     }             
    if(!isNumber(template.age)){
	     alert("年龄必须为数字!");
	 	   return;
     }
 	 if(!isNumber(template.case_id)){
	     alert("病例号必须为数字!");
	 	   return;
     }
    if(!isNumber(template.amount)){
	     alert("数量必须为数字!");
	 	   return;
     }     
        template.subFunction.value='Create';
   		 	show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    show_wait();
    template.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post()" action="itemdcostdef.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>医疗项目消耗材料添加</html:title>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap" >年月：</td>
	    <td nowrap class="normalText" ><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td> 
   </tr>
   <tr>
    	<td nowrap class="signText">科室：</td>
    	<td>
    	  <?xml:namespace prefix="hzh"/>
       <hzh:QInput ID="nosNamec" name="dept_code" value="<%=request.getParameter("dept_code")==null? "":request.getParameter("dept_code")%>" AdjustVal="60" previousObj="year_month" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" Lwidth="100"  top="12" left="196" width="200" Lheight="5" xmlSource="dic/asst_dict_acct_dept_LTD.xml" init="1" /> 
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">病历号：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="case_id" class="textInputC" maxlength="20" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">姓名：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="name" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">年龄：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="age" class="textInputC"  maxlength="3"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">性别：</td>
          <td class="normalText" nowrap="nowrap">
    		     男<input type="radio" name="male" value="M" checked/>
             女<input type="radio" name="male" value="F" />
           </td>
     </tr>
     <td class="signText" nowrap="nowrap">医疗项目：</td>
			<td> <?xml:namespace prefix="hzh"/>
       <hzh:QInput ID="nosNamec" name="charge_detail_code" value="<%=request.getParameter("charge_detail_code")==null? "":request.getParameter("charge_detail_code")%>" AdjustVal="10" previousObj="male" codeCol='charge_detail_code' indexCodeSequence="charge_detail_code|charge_detail_name|spell" textCol="charge_detail_name" Lwidth="100"  top="158" left="370" width="200" Lheight="5" xmlSource="dic/dict_charge_detail_LV.xml" init="1" /> 
			</td> 
		</tr>
   
   <!-------联动------->
    
    <tr>
		       <td class="signText"  width="190" nowrap>材料类别:</td>		
		       <td>                      
		       <%
		    //******  开始生成javascript
		        String[][] seconditem= SimpleSearch.find(" select distinct  inv_code,inv_name +'	['+unit_name+']' inv_name,inv_type_code from dict_inv  LEFT JOIN dict_unit ON dict_inv.unit_code= dict_unit.unit_code ");
		        String[][] firstitem=SimpleSearch.find("  select distinct inv_type_code,inv_type_name from  dict_inv_type ");                      
		        String[] names = {"values","texts", "relations"};
		        out.println(com.viewhigh.base.util.SimpleSearch.dynamicSelect(names, seconditem, "dynamicSelect", request.getParameter("inv_code")));
		   //******  结束 生成javascript                      
		        Select super_code = new Select(firstitem, "inv_type_code", request.getParameter("inv_type_code"), false, false);
		   //******  开始调用javascript
		        super_code.setAttribute("onchange", "dynamicSelect(template.inv_type_code, template.inv_code)");
		  //******  结束调用javascript
		  			out.println(super_code);
		    %>
		      </td>
		  </tr>
		  <tr>
		     <td class="signText" nowrap>材料编码:</td>
		     <td >
		   <%									
				Select seco_code = new Select(seconditem, "inv_code", request.getParameter("inv_code"), false, false);								
				out.println(seco_code);

			 %>
		     </td> 	    
	    
	    </tr>
	       
   <!-------联动------->   
   
 
    <tr>
      <td class="signText" nowrap="nowrap">数量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="amount" class="textInputC" /></td>
    </tr> 
    
    <tr>
      <td colspan="2"> <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> <img src="images/return.gif" class="mouse" onclick="return back(template);" /></td>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="Create"/>
</form>

</html:html>   


