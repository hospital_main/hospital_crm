<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/directcost/itemdcostdef/dataDistill.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent,
								 com.viewhigh.cbcs.cbcs.util.SimpleSearch" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
	var seed=1;
  //数据提取
	function create() {
		if(trim(template.year_month.value)==""){
			alert("请选择年月!");
			return;
		}
		var time=new Date();
		netA.Gsend("itemdcostdef.jspviewhigh?subFunction=check&year_month=" + template.year_month.value + "&dept_code="+template.dept_code.value + "&charge_detail_code="+template.charge_detail_code.value+"&seed="+time.getTime(),dataDistill);
		return true;
	}
	function dataDistill(){
		var num=new Number(netA.req.responseText);
	
		if(isNaN(num)){
			alert(netA.req.responseText);
		}else{
			if(netA.req.responseText>0){
				if(confirm("数据已存在，继续进行会删除掉已有的数据，是否继续进行？")){
					doDistill();
				}
			}else{
				doDistill();
			}
		}
  }
  function doDistill(){
		template.subFunction.value='DataDistill';
		show_wait();
		template.submit();
		return true;
  }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
    	template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemdcostdef.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>直接成本数据提取</html:title>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">年月：</td>
	    <td nowrap class="normalText"  >
	    <%=new Select((String[][])request.getAttribute("year_month"),"year_month",request.getParameter("year_month")==null? "":request.getParameter("year_month"),false,false)%>
	    </td> 
   </tr>
   <tr>
    	<td nowrap class="signText">科室：</td>
    	<td>
    	<%=new Select((String[][])request.getAttribute("dept_code"),"dept_code",request.getParameter("dept_code")==null? "":request.getParameter("dept_code"),false,false)%>
      	</td>
    </tr>
    <tr>	
		<td nowrap class="signText">医疗项目：</td>
		<td>
		<%=new Select((String[][])request.getAttribute("charge_detail_code"),"charge_detail_code",request.getParameter("charge_detail_code")==null? "":request.getParameter("charge_detail_code"),false,false)%>
		</td> 
	</tr>
    
    <tr>
      <td colspan="2"> <img src="images/tq.png" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> <img src="images/close.gif" class="mouse" onclick="window.close();" /></td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="Create"/>
</form>

</html:html>   


