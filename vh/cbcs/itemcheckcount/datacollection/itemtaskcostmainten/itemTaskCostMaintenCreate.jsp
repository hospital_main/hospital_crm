<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/itemtaskcostmainten/itemTaskCostMaintenCreate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                        com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() { 
    if(template.set_cfg_code.value==""){
    	alert("请选择年月")
      return false
    }   
    if(template.cost_subj_code.value==""){
    	alert("成本项目不能为空")
    	return false
    }
    if(template.charge_detail_code.value==""){
    	alert("医疗项目不能为空")
      return false
    }
     
    if(template.work_code.value==""){
    	alert("请选择作业编号")
      return false
    }        
    if(!isNumber(template.per_cost))
    {
      alert('单位消耗需输入数字类型!');
        template.per_cost.focus();
    		template.per_cost.select();      
      return false;
    }
        template.subFunction.value='Create';
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemTaskCostMainten.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>添加医疗项目作业成本</html:title>
  <!-- 简单信息 (set_cfg_code, charge_detail_code, work_code, cost_subj_code, per_cost)-->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">年月：</td>
      <td width="75%" class="normalText" nowrap="nowrap"><%=new MonthComponent("set_cfg_code",request.getParameter("set_cfg_code"))%>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap" >医疗项目：</td>
      <td nowrap class="normalText">
        <?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea" name="charge_detail_code"  AdjustVal="95" previousObj="set_cfg_code" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="9" left="72" Lheight="5" xmlSource="dic/dict_charge_detail_LVT.xml" init="1"/>
			</td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">	作业名称：</td>
      <td class="normalText" nowrap="nowrap"><%=new Select(request.getAttribute("work_code"),"work_code","" ,false,false) %></td>
    </tr>      
    <tr>
      <td class="signText" nowrap="nowrap">成本项目：</td>
      <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea2" name="cost_subj_code"  AdjustVal="95" previousObj="set_cfg_code" codeCol='spell' valueCol="cost_subj_code" textCol="cost_subj_name" width="200" top="-20" left="72" Lheight="8" xmlSource="dic/dict_subj_cost_detail_LV.xml" init="1"/>
			</td>
		</tr>
    <tr>
      <td class="signText" nowrap="nowrap">单位消耗：</td>
			<td class="normaltext"><input type="text"  name="per_cost"  class="textInputC" /></td>
    </tr>    
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
<input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="Create"/>
</form>

</html:html>   