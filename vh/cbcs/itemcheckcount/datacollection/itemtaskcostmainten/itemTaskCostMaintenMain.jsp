<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/itemtaskcostmainten/itemTaskCostMaintenMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent,
								 com.viewhigh.cbcs.cbcs.util.SimpleSearch" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">

	function dischange(){
		template.changesign.value="dischange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function kindchange(){
		template.changesign.value="kindchange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function copy(){ 
		template.subFunction.value="praparedcopy";
		show_wait();
		template.submit();
		return;
	}	
    function create() {
        template.subFunction.value="preparedCreate";
        show_wait();
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="itemTaskCostMainten.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'> 医疗项目作业成本定义  </html:title>



	  <!-- 简单信息 -->  
	  <html:table clazz="simple"> 

		 <tr>
				<td class="signText">收入项目：</td>
				<td class="normalText">
				<%
				Select income_subj_code=new Select(request.getAttribute("income_subj_code"),"income_subj_code",request.getParameter("income_subj_code")==null? "":request.getParameter("income_subj_code"),false,false);
				income_subj_code.setAttribute("onchange","dischange()");
				%>
				<%=income_subj_code.toString() %></td>
		 
				<td class="signText">收费类别：</td>
				<td class="normalText">
				<%
				Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"b_dict_charge_detail_kind",request.getParameter("b_dict_charge_detail_kind")==null? "":request.getParameter("b_dict_charge_detail_kind"),false,false);
				b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
				%>
				<%=b_dict_charge_detail_kind.toString() %></td>
 
	     
				<td class="signText">医疗项目: </td>
				<td class="normalText">
				<%
				Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"dict_charge_detail",request.getParameter("dict_charge_detail")==null? "":request.getParameter("dict_charge_detail"),false,false);
				%>
				<%=dict_charge_detail.toString() %></td>
		  </tr>
			<tr>	     	     
				<td class="signText">年月:</td>
        <td class="normalText" nowrap="nowrap"><%=new MonthComponent("set_cfg_code",request.getParameter("set_cfg_code"))%></td>
 		    <td class="signText">成本项目:</td>
	      <td class="normalText" nowrap="nowrap">
	      <?xml:namespace prefix="hzh"/>
	      <hzh:QInput ID="nosNamea" name="cost_subj_code" AdjustVal="145" previousObj="set_cfg_code" codeCol='spell' indexCodeSequence="cost_subj_code|cost_subj_name|spell" textCol="cost_subj_name" width="200" top="65" left="430" Lheight="6" xmlSource="dic/dict_subj_cost_detail_L.xml" init="1"/>
	    </td>
	      <td class="normalText" colspan="6"  align='right'>
	      <button class="pageBtn" name="" onclick="return find();"  >查询</button>
	      <!--<img src="images/find.gif" class="mouse"   onclick="return find();" />--></td>
	    </tr>		  
	  </html:table>
	  

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	      oper.addNeedButton("images/copy.gif", "return copy()");     //  复制
	  %>
	  
  
		<html:title clazz='table'> 医疗项目作业成本定义  </html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>
           
		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td nowrap="nowrap" class="resultLabel" >选择</td>
		          <td nowrap="nowrap" class="resultLabel" >年月</td>
		          <td nowrap="nowrap" class="resultLabel" >医疗项目</td>
		          <td nowrap="nowrap" class="resultLabel" >作业名称</td>
		          <td nowrap="nowrap" class="resultLabel" >成本项目</td>		          
		          <td nowrap="nowrap" class="resultLabel" >单位消耗</td>
		        </html:tr>

		        <%
		          java.text.DecimalFormat mf=new java.text.DecimalFormat("#,##0.00");
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ]+" "+result[ i ][ 2 ]+" "+result[ i ][ 4 ]+" "+result[ i ][ 6 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>
 
		        <tr CLASS="<%=rowColor%>">
		          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
		          <td nowrap="nowrap" class="normalText"><a href="itemTaskCostMainten.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>"><%=result[ i ][1 ]%></a></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 5 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 7 ]%></td> 
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 8 ]))%></td>

		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		<%}%>		
	  <input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	  <input type='hidden' name="subFunction"/>
<input type="hidden" name="initsub" value="sub"/>
	</form>
</html:html>

