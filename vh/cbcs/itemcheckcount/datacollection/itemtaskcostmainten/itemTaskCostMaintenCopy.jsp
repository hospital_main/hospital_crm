<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/itemtaskcostmainten/itemTaskCostMaintenCopy.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
	function confrim() {

	if (template.set_cfg_code_s.value =="")
	{
		alert("请选择数据源的年月!");
		return;
	}
	if (template.set_cfg_code_d.value =="")
	{
		alert("请选择目标的年月!");
		return;
	}
	if(template.set_cfg_code_s.value==template.set_cfg_code_d.value){
		alert("目标年月与源年月不可相同！");
		return;
	}
		if(confirm("如果目标存在数据将会被新数据覆盖，是否继续？")){
			template.subFunction.value="copy";
			show_wait();
	    template.submit();
	    return true;
    }
   }
     
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemTaskCostMainten.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>    


  <!-- 标题栏 -->
	 <html:title clazz='module'>医疗项目作业复制界面</html:title>  
  <!-- 简单信息 -->   
  <table  width="100%" cellspacing="2" border="0">
  	<tr><td colspan=2 >数据源: </td> <td>目标: </td>
    </tr>
    <tr>   
      <td class="signText" nowrap="nowrap">年月:</td>
      <td nowrap class="normalText"><%=new MonthComponent("set_cfg_code_s",request.getParameter("set_cfg_code_s"))%></td>
      <td class="signText" nowrap="nowrap">年月:</td>
      <td align="left"><%=new MonthComponent("set_cfg_code_d",request.getParameter("set_cfg_code_d"))%></td>
		</tr>
		<tr>
     <td class="signText" nowrap="nowrap">医疗项目：</td>
		 <td class="normalText" nowrap="nowrap" >
	      <?xml:namespace prefix="hzh"/>
	      <hzh:QInput ID="nosNamea" name="charge_detail_code" AdjustVal="50" previousObj="set_cfg_code_s" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="150" top="34" left="143" Lheight="5" xmlSource="dic/dict_charge_detail_LV.xml" init="1"/>
	    </td>
		</tr>
    <tr><td><br><br></td></tr>
   	<tr align="center">
      <td colSpan="4"><button class="pageBtn" onclick="return confrim();" 确定</button>  
     &nbsp;&nbsp;&nbsp;&nbsp;
     <button class="pageBtn" onclick="return back(template);">返回</button>  
     <!--  <img src="images/confirm.gif" class="mouse" onclick="return confrim();" /><img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   