<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/personcostdata/itempersonsalarymainten/itemPersonSalaryMaintenCreate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
	function create() {

	if (template.set_cfg_code.value =="")
	{
		alert("请选择年月!");
		return false;
	}
	if (template.cost_subj_code.value =="")
	{
		alert("请输入成本项目!");
		return false;
	}	

	if (template.title_code.value =="")
	{
		alert("请选择职称!");
		return false;
	}
	
	if(!isNumber(template.m_avg_salary))
    {   alert('月平均工资必须为数字型');
      		template.m_avg_salary.focus();
	    		template.m_avg_salary.select();
        return false;
    }

        template.subFunction.value='create';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemPersonSalaryMainten.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>
<%
	String [][]title_code = (String [][])request.getAttribute("dict_title");
%>
  <!-- 标题栏 -->
	  <html:title clazz='module'>人员工资计算（按职称分类）添加界面 </html:title>  
  <!-- 简单信息 -->    
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="signText" nowrap="nowrap">年月:</td>
      <td nowrap class="normalText">
      <%=new MonthComponent("set_cfg_code",request.getParameter("set_cfg_code"))%></td>
	  </tr>
	  <tr>
      <td class="signText" nowrap="nowrap">职称：</td>
      <td nowrap class="normalText"><%=new Select(title_code,"title_code","",false,false)%></td>
    </tr>  
    <tr>
      <td class="signText" nowrap="nowrap">成本项目:</td>
      <td class="normalText" nowrap="nowrap" > 
      	<%=new Select(request.getAttribute("cost_subj_code"),"cost_subj_code",request.getParameter("cost_subj_code")==null? "":request.getParameter("cost_subj_code"),false,false)%>                                         
      </td>
	  </tr>
    <tr>   
      <td class="signText" nowrap="nowrap">月平均工资：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="m_avg_salary" class="textInputC" /></td>
    </tr>    

    
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   