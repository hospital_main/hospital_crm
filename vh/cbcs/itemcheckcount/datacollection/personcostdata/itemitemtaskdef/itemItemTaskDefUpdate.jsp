<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/personcostdata/itemitemtaskdef/itemItemTaskDefUpdate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript">
  //添加
function save() {
 	 if(!isNumber(template.work_minute)){
        alert('作业时间必须为数字型');
    		template.work_minute.focus();
    		template.work_minute.select();        
        return;
    }
    checkN();
        template.subFunction.value='Update';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    show_wait();
    element.submit();
  }
  
  var seed=1;
  function repeat(){
  	var sel=document.getElementById("title_code").cloneNode(true);
  	var inp=document.getElementById("amount").cloneNode(true);
  	
  	sel.id='title_codes'+seed
  	sel.name="title_codes"
  	
  	inp.id="amounts"+seed
  	inp.name="amounts"
  	inp.onkeydown=function(){
  		 return (isInt(event))
  	}
  	
  	seed++
  	var r=document.all.mytable.rows.length;
  	var row=document.all.mytable.insertRow(r);  	

  	var td=document.createElement("td");
  	var td2=document.createElement("td");
  	
  	td.align="center";
  	td2.align="center";
  	
		row.appendChild(td);
		row.appendChild(td2);
  	td.appendChild(sel);
  	td2.appendChild(inp);
	}
	
function checkN(){
	var inp=document.getElementsByTagName("input");
	for(i=0;i<inp.length;i++){
		if(inp[i].name=="amounts"){
		if(inp[i].value==''){
		}else if(isNum(inp[i].id)){
				if(inp[i].value>255||inp[i].value<0){
					inp[i].value="0-255";
					inp[i].focus();
					inp[i].select();	
					return;
				}
			}else{
				inp[i].value=0;
			}
		}
	}
}
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemItemTaskDef.jspviewhigh">  
<%
	String []result =(String[]) request.getAttribute("result");
	String [][]title_code = (String[][])request.getAttribute("dict_title");
%>
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>医疗项目作业修改界面 </html:title>  
  <!-- 简单信息 -->    <!-- 年月 医疗项目 作业序号 作业名称 作业时间（分钟） 说明 111, 0004, 1, ADFSAF, 12, 12  -->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="signText" nowrap="nowrap">年月:</td>
      <td nowrap class="normalText"><input type=text class="textInputC" value="<%=result[0]%>" disabled /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">医疗项目:</td>
      <td class="normalText" nowrap="nowrap"><input type=text   class="textInputC" value="<%=request.getParameter("charge_detail_name")%>" disabled /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">作业名称:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="work_name" class="textInputC" value="<%=result[3]%>" disabled /></td>
    </tr>   
    <tr>
      <td class="signText" nowrap="nowrap">作业时间（分钟）:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="work_minute" value="<%=result[4]%>" class="textInputC" /></td>
    </tr>    
    <tr>
      <td class="signText" nowrap="nowrap">说明:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="work_desc" value="<%=result[5]%>" class="textInputC" /></td>
    </tr>
    
    <tr>
      <td colspan="2"> 
      <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      	<!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
      	<img src="images/reset.gif" class="mouse" onclick="return reset();" />
      	<img src="images/return.gif" class="mouse" onclick="return back(template);" /> -->
				<img src="images/tjmx.png" class="mouse" onclick="repeat();" />
      </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
  <input type=hidden name="set_cfg_code" class="textInputC" value="<%=result[0]%>"/></td>
  <input type=hidden name="charge_detail_code" value="<%=result[1]%>" /></td>
  <input type=hidden name="work_code" value="<%=result[2]%>" /></td>
  
  <table width='50%' border='0' cellspacing='0' cellpadding='0' class='normalText'>                                                                                                                                                                                                                                
	<tr>                                                                                                                                                                                                                                                       
    <td>                                                                                                                                                                                                                                                     
      <div style='overflow:auto; width:830px; height:390px'>
			<table id="mytable" width='50%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >                                                                                                                                                                                                                            
      	<tr style='height:22px; font-family:"宋体";  font-size:14px; text-decoration:none; color:#000000; background:#F6D7AB; text-align:center; vertical-align:bottom;'>                                                                                                                                                                                                                              
      	<td nowrap class="resultLabel">人员职称</td>
      	<td nowrap class="resultLabel">人员数量</td>
      	</tr>                                                                                                                                                                                                                                          
		<tr style="display:none">
			<td class="normalText"nowrap="nowrap">
			<%
				Select sel=new Select(title_code,"title_code","",false,false);
				sel.setAttribute("id","title_code");
				out.println(sel.toString());
			%></td>
      <td><input type=text name="amount"  maxlength="4"  class="textInputC" /></td>
    </tr>
     <%
        String[][] result2 = (String[][])request.getAttribute("result2");
          if ( result2 != null ) {
            for (int i = 0; i < result2.length; i++ ) {
            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
       %>
       <tr>
       		<td nowrap="nowrap" align="center">
       			<%=new Select(title_code,"title_codes",result2[i][0],false,false)%>
       		</td>
       		<td  align="center">
       			<input type=text name="amounts" id="<%="amo"+i %>" onkeydown="return isInt(event);" value="<%=result2[i][1] %>"  maxlength="3"  class="textInputC" />
       		</td>
       		
       </tr>
  <%
  					}
 					}
 %>
    </tr>
  
       </table></div>                                                                                                                                                                                                                                         
        </td>                                                                                                                                                                                                                                                
      </tr>                                                                                                                                                                                                                                                  
 	</table>  
 	<input type="hidden" name="initsub" value="sub"/>
</form>

</html:html>   