<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/personcostdata/itemitemtaskdef/itemItemTaskDefCreate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript">
  //添加
	function create() {
	if (template.set_cfg_code.value =="")
	{
		alert("请选择年月!");
		return;
	}
	if (template.charge_detail_code.value =="")
	{
		alert("请输入医疗项目!");
		return;
	}
	if (template.work_code.value =="")
	{
		alert("请选择作业序号!");
		return;
	}
		
	if(!isNumber(template.work_minute))
    {    alert('作业时间必须为数字型');
    		template.work_minute.focus();
    		template.work_minute.select();
        return;
    }
	checkN();
        template.subFunction.value='create';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    show_wait();
    element.submit();
  }
  
  var seed=1;
  function repeat(){
  	var sel=document.getElementById("title_code").cloneNode(true);
  	var inp=document.getElementById("amount").cloneNode(true);
  	
  	sel.id='title_codes'+seed
  	sel.name="title_codes"
  	
  	inp.id="amounts"+seed
  	inp.name="amounts"  	
  	inp.onkeydown=function(){
  		 return (isInt(event))
  	}
  	
  	seed++
  	var r=document.all.mytable.rows.length;
  	var row=document.all.mytable.insertRow(r);  	

  	var td=document.createElement("td");
  	var td2=document.createElement("td");
  	
  	td.align="center";
  	td2.align="center";
  	
		row.appendChild(td);
		row.appendChild(td2);
  	td.appendChild(sel);
  	td2.appendChild(inp);
}

function checkN(){
	var inp=document.getElementsByTagName("input");
	for(i=0;i<inp.length;i++){
		if(inp[i].name=="amounts"){
		if(inp[i].value==''){
		}else if(isNum(inp[i].id)){
				if(inp[i].value>255||inp[i].value<0){
					inp[i].value="0-255";
					inp[i].focus();
					inp[i].select();	
					return;
				}
			}else{
				inp[i].value=0;
			}
		}
	}
}
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemItemTaskDef.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>
<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
	String [][]work_code = (String [][])request.getAttribute("dict_charge_work");
	String [][]title_code = (String[][])request.getAttribute("dict_title");
%>
  <!-- 标题栏 -->
	  <html:title clazz='module'>医疗项目作业添加界面 </html:title>  
  <!-- 简单信息 -->    <!-- 年月 医疗项目 作业序号 作业名称 作业时间（分钟） 说明   -->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="signText" nowrap="nowrap">年月:</td>
      <td nowrap class="normalText"><%=new MonthComponent("set_cfg_code",request.getParameter("set_cfg_code"))%></td>
      <td class="signText" nowrap="nowrap">收&nbsp;&nbsp;费&nbsp;&nbsp;项&nbsp;&nbsp;目&nbsp;:&nbsp;</td>
      <td>
	      <?xml:namespace prefix="hzh"/>
	      <hzh:QInput ID="nosNamea" name="charge_detail_code" AdjustVal="50" previousObj="set_cfg_code" value="<%=request.getParameter("charge_detail_code")==null ? "":request.getParameter("charge_detail_code")%>" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="35" left="125" Lheight="6" xmlSource="dic/dict_charge_detail_LVT.xml" init="1"/>
	    </td>
     </tr>
    <tr>
      <td class="signText" nowrap="nowrap">作业序号:</td>
      <td nowrap class="normalText"><%=new Select(work_code,"work_code",request.getParameter("work_code")==null?"":request.getParameter("work_code"),false,false)%></td>
      <td class="signText" nowrap="nowrap">作业时间(分钟):</td>
      <td owrap class="normalText"><input type=text name="work_minute" class="textInputC" /></td>
    </tr>    
    <tr>
      <td class="signText" nowrap="nowrap">说明:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="work_desc" class="textInputC" /></td>
    </tr>
    
    <tr>
      <td colspan="2"> 
				<button class="pageBtn" onclick="return create();" >添加</button>
				<button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
				<!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
				<img src="images/return.gif" class="mouse" onclick="return back(template);" /> -->
				<img src="images/tjmx.png" class="mouse" onclick="repeat();" />
			</td>
    </tr>


  </table>
  <input type=hidden name="subFunction" value="create"/>
                                                                                                                                                                                                                                                 
 <table width='50%' border='0' cellspacing='0' cellpadding='0' class='normalText'>                                                                                                                                                                                                                                
	<tr>                                                                                                                                                                                                                                                       
    <td>                                                                                                                                                                                                                                                     
      <div style='overflow:auto; width:830px; height:390px'>
			<table id="mytable" width='50%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >                                                                                                                                                                                                                            
      	<tr style='height:22px; font-family:"宋体";  font-size:14px; text-decoration:none; color:#000000; background:#F6D7AB; text-align:center; vertical-align:bottom;'>                                                                                                                                                                                                                              
      	<td nowrap class="resultLabel">人员职称</td>
      	<td nowrap class="resultLabel">人员数量</td>
      	</tr>                                                                                                                                                                                                                                          
     <%/*
        String[][] result = request.getAttribute("result");
         DecimalFormat mf=new DecimalFormat("#,##0.00");                                                                                                                                                                                                     
          if ( result != null ) {                                                                                                                                                                                                                            
            for (int i = 0; i < result.length; i++ ) {                                                                                                                                                                                                       
              String primaryKey = result[i][0]+"|^|"+result[i][6]+"|^|"+result[i][2];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";                                                                                                                                                                                                                     
                }                                                                                                                                                                                                                                            
              }                                                                                                                                                                                                                                              
            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
*/
       %>
		<tr style="display:none">
			<td class="normalText"nowrap="nowrap">
			<%
				Select sel=new Select(title_code,"title_code","",false,false);
				sel.setAttribute("id","title_code");
				out.println(sel.toString());
			%></td>
      <td><input type=text name="amount" onkeydown="return isInt(event);" maxlength="3"  class="textInputC" /></td>
    </tr>
  <%/*
  					}
 					}*/
 %>
    </tr>
  
       </table></div>                                                                                                                                                                                                                                         
        </td>                                                                                                                                                                                                                                                
      </tr>                                                                                                                                                                                                                                                  
 	</table>  
<input type="hidden" name="initsub" value="sub"/>
</form>


</html:html>   