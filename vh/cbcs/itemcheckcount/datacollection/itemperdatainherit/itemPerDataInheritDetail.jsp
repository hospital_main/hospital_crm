<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/itemperdatainherit/itemPerDataInheritDetail.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.mvc.view.MonthComponent"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js"></Script>
<Script Language="JavaScript">
	function doInherit(){
		var sco=document.getElementsByName("scope");
		var items="";
		for(i=0;i<sco.length;i++){
			if(sco[i].checked){
				if(items.length > 0)
				    items=items + "," + sco[i].value;
				else
					  items=sco[i].value;
			}
		}
		netA.Gsend("itemPerDataInherit.jspviewhigh?subFunction=check&items="+items+"&year_month_aim=" + template.year_month_aim.value,check);
	}
	function back(){
		template.subFunction.value="preInherit";
		show_wait();
		template.submit();
	}
	
	function check(){

		var sco=document.getElementsByName("scope");
		var check=false;
		for(i=0;i<sco.length;i++){
			if(sco[i].checked){
				check=true;
				break;
			}
		}
		if(typeof(template.year_month)!='undefined'&&check){
			if(template.year_month.value==""){
				alert("请选择年月");
				return false;
				}
		}	
		
		if(template.datasource.value == "1") {
  		if (template.year_month_aim.value == template.year_month.value){
  		   alert("目的数据所在月不能与源数据所在月相同.");
  		   return false;
  		}}
  		
		if(typeof(template.set_cfg_code)!='undefined'&&check){
			if(template.set_cfg_code.value==""){
				alert("请选择配置编号");
				return false;
				}
		}
		if(!check){
			alert("请选择继承范围");
		}
		
		if (netA.req.responseText.length > 2){
      if(!confirm(netA.req.responseText)){
	        return false;	
	    }
	}
			show_wait();
			template.submit();	

	}

</Script>

<html:html clazz="main">
	<form name="template" method="post" action="itemPerDataInherit.jspviewhigh">
		<html:message/>
		<html:title clazz="module">数据继承页面</html:title>
		<html:table clazz="simple">
			<tr>
				<td>请选择继承范围：</td>
				<td>
					<input type="checkbox" name="scope" value="P_item">人员成本-项目作业定义 
					<input type="checkbox" name="scope" value="P_time">人员成本-工作时间
					<input type="checkbox" name="scope" value="P_salary">人员成本-人员工资
				</td>
			</tr>
			<tr><td/><td>
				  <input type="checkbox" name="scope" value="inv_consume">材料消耗&nbsp;　　　　　　
					<input type="checkbox" name="scope" value="equi_cost">设备折旧成本　&nbsp;　
					<input type="checkbox" name="scope" value="others">其他直接成本　　　
				</td>
				<input type="hidden" name="others" value="其他直接成本">
			</tr>
<%if("1".equals(request.getParameter("datasource"))){ %>
			<tr>
				<td>年月：</td><td><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td>
			</tr>
<%}else{ %>
			<tr>
				<td>配置编号：</td><td><%=new Select(request.getAttribute("set_cfg_code"),"set_cfg_code","",false,false)%></td>
			</tr>
<%} %>			
			<tr>
				<td> <button class="pageBtn" onclick="doInherit()">确定</button> 
				&nbsp;&nbsp;&nbsp;
				<button class="pageBtn" name="sure" onclick="back();">返回</button> 
				<!--<img src="images/confirm.gif" name="sure" class="mouse" onclick="doInherit()"/><img src="images/return.gif"  class="mouse" onclick="back()"/>--></td>
			</tr>
		</html:table>
		<input type="hidden" name="subFunction" value="inherit"/>
		<input type="hidden" name="datasource" value="<%=request.getParameter("datasource") %>"/>
		<input type="hidden" name="year_month_aim" value="<%=request.getParameter("year_month_aim") %>"/>
	</form>
</html:html>