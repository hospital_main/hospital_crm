<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/itemperdatainherit/itemPerDataInheritMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.mvc.view.MonthComponent"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js"></Script>
<Script Language="JavaScript">
  function enter(){
		if(check()){
			show_wait();
			template.submit();
		}
	}
	
	function check(){
		if(template.year_month_aim.value==""){
			alert("请选择年月！");
			return false;
		}
		return true;
	}
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="itemPerDataInherit.jspviewhigh">
		<html:message/>
		<html:title clazz="module">数据继承页面</html:title>
		<html:table clazz="simple">
			<tr>
				<td>目的数据所在月：</td><td><%=new MonthComponent("year_month_aim",request.getParameter("year_month_aim"))%></td>
			</tr>
			<tr>
				<td>请选择继承来源：</td><td><input type="radio" name="datasource" value="1" checked="true">项目核算系统<input type="radio" name="datasource" value="2">项目作业法模型</td>
				<td><button class='pageBtn' onclick="enter()">确定</button></td>
			</tr>
		</html:table>
		<input type="hidden" name="subFunction" value="detail"/>
	</form>
</html:html>