<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/itemequipcost/itemEquipCostCopy.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
		function confrim() {
	if (template.set_cfg_code.value =="")
	{
		alert("请选择数据源的年月!");
		template.set_cfg_code.focus();
		return false;
	}
	if (template.set_cfg_code1.value =="")
	{
		alert("请选择目标的年月!");
		template.set_cfg_code1.focus();
		return false;
	}
	/*
	二层三层校验
	*/
	if(template.charge_detail_code.value != "" && template.charge_detail_code1.value == "")
	{
		alert("请选择目标的医疗项目!");
		template.charge_detail_code1.focus();
		return false;
	}
	if(template.charge_detail_code.value == "" && template.charge_detail_code1.value != "")
	{
		alert("请选择数据源的医疗项目!");
		template.charge_detail_code.focus();
		return false;
	}
	if(template.Cost_subj_code.value =="" && template.Cost_subj_code1.value != "")
	{
		alert("请选择数据源的成本项目!");
		template.work_code1.focus();
		return false;
	}
	if(template.Cost_subj_code.value !="" && template.Cost_subj_code1.value == "")
	{
		alert("请选择目标的成本项目!");
		template.work_code.focus();
		return false;
	}
	if(template.Cost_subj_code.value != "" && template.charge_detail_code.value == "")
	{
		alert("请先选择医疗项目!");
		template.charge_detail_code.focus();
		return false;
	}
	if(template.set_cfg_code.value == template.set_cfg_code1.value 
			&& template.charge_detail_code.value == template.charge_detail_code1.value
			&& template.Cost_subj_code.value  == template.Cost_subj_code1.value)
	{
		alert("数据源的数据不能和目标数据完全相同!");
		template.set_cfg_code.focus();
		return false;
	}
     netA.Gsend("itemEquipCost.jspviewhigh?subFunction=check&set_cfg_code1=" + template.set_cfg_code1.value + "&charge_detail_code1="+template.charge_detail_code1.value + "&Cost_subj_code1="+template.Cost_subj_code1.value,copyAccess);
        
    }
    	function copyAccess(){
		var num=new Number(netA.req.responseText);
		
		if(isNaN(num)){
			alert(netA.req.responseText);
		}else{	
			if(netA.req.responseText>0){
				if(confirm("拷贝目标存在数据，确认将会覆盖已存在数据，确认？")){
					doCopy();
				}
			}else{
				doCopy();
			}
		}
  }
  function doCopy(){
		template.subFunction.value="copy";
		show_wait();
    template.submit();
    return true;
  }
    
  // 返回
     
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemEquipCost.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>    

<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
	String [][]Cost_subj_code = (String[][])request.getAttribute("dict_inv_type");
%>
  <!-- 标题栏 -->
	 <html:title clazz='module'>材料消耗维护复制界面  </html:title>  
  <!-- 简单信息 -->    <!-- 配置编号 医疗项目 成本项目 设备 时间 单位折旧成本 折旧成本  -->
  <table  width="100%" cellspacing="2" border="0">
  	<tr><td colspan=2 >数据源: </td> <td>目标: </td>
    </tr>
    <tr>   
      <td class="signText" nowrap="nowrap">年月:</td>
      <td nowrap class="normalText"><%=new MonthComponent("set_cfg_code",request.getParameter("set_cfg_code"))%></td>
       <td class="signText" nowrap="nowrap">年月:</td>
       <td align="left"><%=new MonthComponent("set_cfg_code1",request.getParameter("set_cfg_code1"))%></td>
		</tr>
		<tr>
      <td class="signText" nowrap="nowrap">医疗项目:</td>
      <td nowrap class="normalText">
      	<?xml:namespace prefix="hzh"/>
				<hzh:QInput ID="nosNamea" name="charge_detail_code" value="" AdjustVal="95" previousObj="set_cfg_code" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="35" left="133" Lheight="5" xmlSource="dic/dict_charge_detail.xml" init="1"/>
      </td>
	     <td class="signText" nowrap="nowrap">医疗项目:</td>
      <td nowrap class="normalText">
      	<?xml:namespace prefix="hzh"/>
				<hzh:QInput ID="nosNamea" name="charge_detail_code1" value="" AdjustVal="95" previousObj="set_cfg_code1" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="35" left="550" Lheight="5" xmlSource="dic/dict_charge_detail.xml" init="1"/>
      </td>
    </tr>
    <tr>
    	<td class="signText" nowrap="nowrap">成本项目:</td>
      <td nowrap class="normalText"><%=new Select(Cost_subj_code,"Cost_subj_code","",false,false)%></td>
	     <td class="signText" nowrap="nowrap">成本项目:</td>
      <td nowrap class="normalText"><%=new Select(Cost_subj_code,"Cost_subj_code1","",false,false)%></td>
    </tr>
    <tr><td><br><br></td></tr>
   	<tr align="center">
      <td colSpan="4"> <button class="pageBtn" onclick="return confrim();">确定</button> 
      &nbsp;&nbsp;&nbsp;&nbsp;
      <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/confirm.gif" class="mouse" onclick="return confrim();" /><img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   