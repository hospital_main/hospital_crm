<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/coopecostdef/itemCostDefCreate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                        com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
    
    if(trim(template.charge_detail_code.value)=="")
    {
      alert('医疗项目不能为空！');
      return false;
    }
    
    if(trim(template.cost_subj_code.value)=="")
    {
      alert('成本项目编码不能为空！');
      return false;
    }
    if(trim(template.year_month.value)=="")
    {
      alert('启用月份不能为空！');
      return false;
    }
    switch(isDouble(template.join_avgcost,10,4))
    {
      case 0 : alert('合作成本必须为数字型'); return;
      case 1 : alert('合作成本整数部分不能高于10个字符'); return;
      case 2 : alert('合作成本没有整数部分'); return;
      case 3 : alert('合作成本小数部分不能高于4个字符'); return;
    }
        template.subFunction.value='Create';
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
    template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemcoopecostdef.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>添加合作成本</html:title>
  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">启用月份:</td>
			<td class="normaltext"><%=new  MonthComponent("year_month", request.getParameter("year_month"))%></td>
    </tr>     

    <tr>
      <td class="signText" nowrap="nowrap">医疗项目:</td>
      <td class="normalText" nowrap="nowrap" >
      <?xml:namespace prefix="hzh"/>
      <hzh:QInput ID="nosNamea" name="charge_detail_code" value="<%=request.getParameter("charge_detail_code")==null? "":request.getParameter("charge_detail_code")%>" AdjustVal="20" previousObj="join_avgcost" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="168" left="238" Lheight="6" xmlSource="dic/dict_charge_detail_LV.xml" init="1"/>
	    </td>
    <tr>
      <td class="signText" nowrap="nowrap" >成本项目编码</td>
      <td class="normalText" nowrap="nowrap" >
      <?xml:namespace prefix="hzh"/>
      <hzh:QInput ID="nosNamea" name="cost_subj_code" value="<%=request.getParameter("cost_subj_code")==null? "":request.getParameter("cost_subj_code")%>" AdjustVal="20" previousObj="join_avgcost" codeCol='spell' valueCol="cost_subj_code" textCol="cost_subj_name" width="200" top="141" left="238" Lheight="6" xmlSource="dic/dict_subj_cost_detail_LV.xml" init="1"/>
	    </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">合作成本</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="join_avgcost" class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
     <!--  <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="Create"/>
</form>

</html:html>   
