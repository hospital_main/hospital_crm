<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/coopecostdef/itemCostDefUpdate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
                        com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    switch(isDouble(template.join_avgcost,10,4))
    {
      case 0 : alert('合作成本必须为数字型'); return;
      case 1 : alert('合作成本整数部分不能高于10个字符'); return;
      case 2 : alert('合作成本没有整数部分'); return;
      case 3 : alert('合作成本小数部分不能高于4个字符'); return;
    }
    template.subFunction.value='Update';
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="itemcoopecostdef.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module">合作成本修改</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息    
医疗项目 charge_detail_code   
成本项目编码 cost_subj_code   
合作成本 join_avgcost 

-->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">启用月份:</td>
			<td class="normaltext"><input type="text"  name= "month" value=<%=result[0] %> class="textInputC" disabled /></td>
    </tr> 
    <tr>
      <td class="signText" nowrap="nowrap">医疗项目:</td>
      <td width="75%" class="normalText" nowrap="nowrap"><input type="text"  name="b" value=<%=result[5] %> class="textInputC" disabled /></td>
    </tr>
      <td class="signText" nowrap="nowrap" >成本项目:</td>
      <td class="normalText" nowrap="nowrap"><input type="text"  name="a" value=<%=result[6] %> class="textInputC" disabled /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">合作成本:</td>
      <td class="normalText" nowrap="nowrap"><input type="text" name="join_avgcost" value=<%=result[3] %> class="textInputC" /></td>
    </tr>
     <tr>
      <td colspan="2">
      <button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" /> <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="year_month" value="<%=result[0]%>">
  <input type="hidden" name="charge_detail_code" value="<%=result[1]%>">
  <input type="hidden" name="cost_subj_code" value="<%=result[2]%>">
  
  <%}%>
</form>


</html:html>
