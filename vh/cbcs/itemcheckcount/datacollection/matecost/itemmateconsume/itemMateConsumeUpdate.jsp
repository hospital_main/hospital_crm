<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/datacollection/matecost/itemmateconsume/itemMateConsumeUpdate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  //添加
function save() {

		switch(isDouble(template.amount, 10,6))
		    {
		      case 0 : alert('数量必须为数字型'); return false;
		      case 1 : alert('数量整数位不能超过10位'); return false;
		      case 3 : alert('数量小数位不能超过6位'); return false;
		    }
 
 if(eval(template.amount.value)>9999999999999999)
    {
    	alert('工作人员数量必须小于9999999999999999');
        template.amount.focus();
    		template.amount.select();    	
    	return false;
    }
        template.subFunction.value='Update';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemmateconsume.jspviewhigh">  
<%
	String []result =(String[]) request.getAttribute("result");
%>
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>材料消耗维护修改界面 </html:title>  
  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="signText" nowrap="nowrap">年月:</td>
      <td nowrap class="normalText"><input type=text class="textInputC" value="<%=result[0]%>" disabled /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">医疗项目:</td>
      <td class="normalText" nowrap="nowrap"><input type=text   class="textInputC" value="<%=result[1]%>" disabled /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">作业序号:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="work_code" class="textInputC" value="<%=result[2]%>" disabled /></td>
    </tr>   
    <tr>
      <td class="signText" nowrap="nowrap">消耗材料：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="title_name" value="<%=result[3]%>" class="textInputC" disabled /></td>
    </tr>    
      <td class="signText" nowrap="nowrap">数量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text  maxlength="17"  name="amount" value="<%=result[4]%>" class="textInputC" /></td>
    </tr>
    
    <tr>
      <td colspan="2"> 
      <button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
  <input type=hidden name="set_cfg_code" class="textInputC" value="<%=result[0]%>"/></td>
  <input type=hidden name="dict_income_subj" value="<%=result[5]%>" /></td>
  <input type=hidden name="dict_title" value="<%=result[6]%>" /></td>
  <input type=hidden name="inv_code" value="<%=result[7]%>" /></td>
</form>

</html:html>   