<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemcostana/itemincana/itemDeptIncAnalyseMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.text.*,
com.viewhigh.cbcs.cbcs.util.DisplayWidth,
com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<%
String[][] result=(String[][])request.getAttribute("result");
%>

<Script language="javascript">
function dischange(){
    template.changesign.value="dischange";
    template.subFunction.value="change";
    show_wait();
    template.submit();
    return;
}
function kindchange(){
    template.changesign.value="kindchange";
    template.subFunction.value="change";
    show_wait();
    template.submit();
    return;
}
function find(){
    if(template.year_month_from.value == "")
    {
        alert('请选择核算起始年月!');
        return false;
    }
    if(template.year_month_to.value == "")
    {
        alert('请选择核算结束年月!');
        return false;
    }
    if(template.year_month_from.value>template.year_month_to.value){
        alert('起始年月应小于结束年月!');
        return false;
    }
     if(template.dept.value == ""){
        alert('请选择科室!');
        return false;
    }
    show_wait();
    template.subFunction.value="findAll";
    template.submit();
    return;
}
</Script>

<html:html clazz="main" scrollCtl="yes" fixRows="2" isPrint="true">
<form name="template" method="post" action="itemDeptIncAnalyse.jspviewhigh">
<html:message/>
<html:title clazz='module'>科室医疗项目收益分析(医成本T1-02表)</html:title>
<html:table clazz="simple">
	<tr>	   
    <td class="signText">核算月:</td>
    <td class="normaltext">
		<input type="text" class="inputYearMonth" required="true" name="year_month_from"
		<%=request.getParameter("year_month_from")==null?" defaultvalue='true' ":" value='"+request.getParameter("year_month_from")+"' "%> />
		至
		<input type="text" class="inputYearMonth" required="true" name="year_month_to" 
		<%=request.getParameter("year_month_to")==null?" defaultvalue='true' ":" value='"+request.getParameter("year_month_to")+"' "%> />
    	</td>
    <td class="signText">收入项目:</td>
    <td class="normalText">
    <%
    Select dict_income_subj=new Select(request.getAttribute("dict_income_subj"),"dict_income_subj",request.getParameter("dict_income_subj")==null? "":request.getParameter("dict_income_subj"),false,false);
    dict_income_subj.setAttribute("onchange","dischange()");
    %>
    <%=dict_income_subj.toString() %></td>
    <td class="signText">收费类别:</td>
    <td class="normalText">
    <%
    Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"b_dict_charge_detail_kind",request.getParameter("b_dict_charge_detail_kind")==null? "":request.getParameter("b_dict_charge_detail_kind"),false,false);
    b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
    %>
    <%=b_dict_charge_detail_kind.toString() %></td>
    <td class="signText">医疗项目: </td>
    <td class="normalText">
    <%
    Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"dict_charge_detail",request.getParameter("dict_charge_detail")==null? "":request.getParameter("dict_charge_detail"),false,false);
    %>
    <%=dict_charge_detail.toString() %></td>
	</tr>
	<tr>
	<td class="signText">科室: </td>
    <td>
       	<?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="dept"  value="<%=request.getParameter("dept")==null? "":request.getParameter("dept")%>"  AdjustVal="147" previousObj="serve_no" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" width="200" top="42" left="116" Lheight="5" xmlSource="dic/dict_acct_dept_T.xml" init="1"/>
	</td>
	<td colspan="8" align="left">
    	<button class="pageBtn" onclick="return find();">计算</button>
    <%
		if (result!=null) {%>
        <button class="pageBtn" onclick="return preparedPrint();">打印</button>
	<%}%>
	</td>
	</tr>
</html:table>
    <BR/>
<html:title clazz='table'>科室医疗项目收益分析<%=request.getAttribute("date")==null? "":("（"+request.getAttribute("date")+"）")%></html:title>
			<html:table clazz="result">
		      <colgroup id=tg>
		        <col style = <%=DisplayWidth.NAME_WIDTH%>  >
		        <col style = <%=DisplayWidth.NAME_WIDTH%>  >
		        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
		        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
		        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
		        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
		        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
		        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
		        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
		        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
		        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
		        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	        </colgroup>
        <tr>
	        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">科室</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">医疗项目</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">总收益</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">单位收益</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">标准单位收益</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">工作量</td>
       	</tr>
        <tr>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >收入</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >成本</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >收益</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >收入</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >成本</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >收益</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >标准收入</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >成本</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >收益</td>
          </tr>
    <%
    DecimalFormat mf = new DecimalFormat("#,##0.00");
    if(result!=null){
        for(int i=0;i<result.length;i++){
            %>
				<tr>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][0] %></td>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][1]%></td>
						<%
						for(int j=2;j<result[i].length-1;j++){
						%>
						<td nowrap class="numberText" style='text-align:right'><%=result[i][j]%></td>
						<%}%>
						<td nowrap class="numberText" style='text-align:right'><%=mf.format(Double.parseDouble(result[i][result[i].length-1]))%></td>
				</tr>
					<%}}%>
			</html:table>
    <input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
    <input type="hidden" name="subFunction" value="findAll">
	</form>
</html:html>