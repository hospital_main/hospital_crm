<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemcostana/itemtrendana/itemdeptpercosttrendanalyse/Main.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,java.text.*,
com.viewhigh.cbcs.cbcs.util.DisplayWidth,com.viewhigh.cbcs.base.sql.BaseRO,com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
	BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	String[][]  result = null;
	String[] year_month = (String[]) request.getAttribute("year_month");
	String[][] graph =null;
	String []graphTitle=null;
	if(ro !=null ) result = ro.getTableResult();
	if(result!=null ){
	   graph = new String [year_month.length][result.length+2];
	   graphTitle = new String[result.length];
		for( int year=0; year<year_month.length;year++){
			 graph[year][0] = year_month[year].substring(0,4);
			 graph[year][1] = year_month[year].substring(4,6);	
		   }			        	
		 for(int row=0;row<result.length; row++){
			 graphTitle[row]=result[row][0]+result[row][1];
		 	 for(int gcol=0,col=2;col<result[0].length;gcol++){
			  	if(col<3){
			  		graph[gcol][row+2]= result[row][col];
			  		col++;	}
			  	else {
			  		graph[gcol][row+2]= result[row][col];
			  		col=col+3;	
			  		}
		  	}
	   }
	if(request.getSession(false).getAttribute("graph")!=null) 
		request.getSession(false).removeAttribute("graph");
		request.getSession(false).setAttribute("graph",graph);
	if(request.getSession(false).getAttribute("graphTitle")!=null) 
		request.getSession(false).removeAttribute("graphTitle"); 
	request.getSession(false).setAttribute("graphTitle",graphTitle);
 }
%>

<script language="JavaScript" src="/base/scripts/CreateObjects.js" ></script>
<Script language="javascript">
	function dischange(){
		template.changesign.value="dischange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function kindchange(){
		template.changesign.value="kindchange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function find(){
		if(template.year_month_from.value == "")
		{
			alert('请选择核算起始年月!');
			return false;
		}
		if(template.year_month_to.value == "")
		{
			alert('请选择核算结束年月!');
			return false;
		}
		if(template.year_month_from.value>template.year_month_to.value){
			alert('起始年月应小于结束年月!');
			return false;
		}
		if((parseInt(template.year_month_to.value)-parseInt(template.year_month_from.value)-88)>12){
			alert('查询期间不能大于12个月!');
			return false;
		}
		if(template.dept.value == "")
		{
			alert('请选择科室!');
			return false;
		}		
		if(template.dict_income_subj.value == "")
		{
			alert('请选择收入项目!');
			return false;
		}		
 		
		yf=template.year_month_from.value.substring(0,4);
		yt=template.year_month_to.value.substring(0,4)
		mf=template.year_month_from.value.substring(5,6);
		mt=template.year_month_to.value.substring(5,6); 
		if((eval(yt)-eval(yf)==1 && eval(mt)-eval(mf)>0)||(eval(yt)-eval(yf)>1)){
		//alert('时间段不能超过一年');
		//return false;
		}
		show_wait();
		template.subFunction.value="findAll";
		template.submit();
		return;
	}
  function	draw(){
 	//window.open("itemDeptPerCostTrendAnalyse.jspviewhigh?subFunction=drawGraph",null,"top=100, left=250, height=450,width=600,toolbar=no, location=no");
 	openEg('itemDeptPerCostTrendAnalyse.jspviewhigh?subFunction=drawGraph',800,423,false,false);//在屏幕中心弹出窗口
  }	
</Script>

<html:html clazz="main" scrollCtl="yes" fixRows="2" isPrint="true">
	<form name="template" method="post" action="itemDeptPerCostTrendAnalyse.jspviewhigh">
		<html:message/>
		<html:title clazz='module'>科室医疗项目单位成本趋势分析(环比)(医成本T4-04表)</html:title>
		<html:table clazz="simple">
			<tr>	   
				<td class="signText">核算月:</td>
				<td class="normaltext"><%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%></td>
				<td class="signText">收入项目:</td>
				<td class="normalText">
				<%
				Select dict_income_subj=new Select(request.getAttribute("dict_income_subj"),"dict_income_subj",request.getParameter("dict_income_subj")==null? "":request.getParameter("dict_income_subj"),false,false);
				dict_income_subj.setAttribute("onchange","dischange()");
				%>
				<%=dict_income_subj.toString() %></td>

				<td class="signText">收费类别:</td>
				<td class="normalText">
				<%
				Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"b_dict_charge_detail_kind",request.getParameter("b_dict_charge_detail_kind")==null? "":request.getParameter("b_dict_charge_detail_kind"),false,false);
				b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
				%>
				<%=b_dict_charge_detail_kind.toString() %></td>
				<td class="signText">医疗项目: </td>
				<td class="normalText">
				<%
				Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"dict_charge_detail",request.getParameter("dict_charge_detail")==null? "":request.getParameter("dict_charge_detail"),false,false);
				%>
				<%=dict_charge_detail.toString() %></td>
			</tr>
			<tr>
				<td></td><td></td>
				<td class="signText">科室: </td>
				<td>
				<% Select dept=new Select(request.getAttribute("dept"),"dept",request.getParameter("dept")==null? "":request.getParameter("dept"),false,false);
				%>
				<%=dept.toString() %>
				</td>
				<td colspan="2">
					<button class="pageBtn" align='right' onclick="return find();">计算</button>
				<%
				if (result !=null) { %>
					<button class="pageBtn" align='right'  onclick="return draw();" >图形</button>
					<button class="pageBtn" align='right' onclick="return preparedPrint();">打印</button>
				<%}%>
			</td>
			</tr>
			</html:table>
			
			<br>
			<html:title clazz='table'>科室医疗项目单位成本趋势分析(环比)</html:title>
			<%	if(ro!=null){
				 TableMarge oper = new TableMarge(ro, "return find()"); 
	   		 %>
   		    <html:table clazz="simple"><tr><td><%=oper%></td></tr></html:table>
   	 	    <%}%> 	
				<html:table clazz="result">
					<colgroup id=tg>
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >
						<col style = <%=DisplayWidth.NAME_WIDTH%> >
				<%if (result!=null) {
			   int year=0;
			   while (year<year_month.length){
			   if(year==0)
			   	{%>			   
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
				<% } else { %>
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
 				<%	}year++; 	}
 					}%>
					</colgroup>
					<tr>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">科室</td> 
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">医疗项目名称</td>
			   <%	 if (result!=null) {%>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' ><%=request.getParameter("year_month_from")%> </td>			   
			   <% int year=1;
				   	while (year<year_month.length) {
				 %>
				 	  	<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3"><%=year_month[year]%> </td>
				 <%  year++;}
				 	 }  %>
					</tr>
					<tr>
			   <%	
			   if (result!=null) {%>
			   <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>成本</td>
			   <%
				    int year=1;				    
				   	while(year<year_month.length) {
				 %>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >成本</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >差异</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' >差异率</td>
				 <% year++; }
				 }%>
					</tr>
				<%
					if(result!=null){
					  DecimalFormat mf=new DecimalFormat("#,##0.00");
					  DecimalFormat pf=new DecimalFormat("#,##0.00%");						
						for(int i=0;i<result.length;i++){
				%>
					<tr>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][0]%></td>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][1]%></td>
				<%
				for(int j=2;j<result[i].length;j++){
				if(j!=2 && (j-2)%3==0) {
				%>
						<td nowrap class="numberText" style='text-align:right'><%=pf.format(Double.parseDouble(result[i][j]))%></td>
				<%} else {%>
						<td nowrap class="numberText" style='text-align:right'><%=mf.format(Double.parseDouble(result[i][j]))%></td>
				<%}}%>	
					</tr>
				<%}
				}%>
				</html:table>
		<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
		<input type="hidden" name="subFunction" value="findAll">
	</form>
</html:html>