<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemcostana/itemcompana/itemdeptpercostcompanalyse/Main.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
com.viewhigh.cbcs.cbcs.util.DisplayWidth,
com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<%
String[][] result=(String[][])request.getAttribute("result");
%>

<Script language="javascript">
function dischange(){
    template.changesign.value="dischange";
    template.subFunction.value="change";
    show_wait();
    template.submit();
    return;
}
function kindchange(){
    template.changesign.value="kindchange";
    template.subFunction.value="change";
    show_wait();
    template.submit();
    return;
}
function find(){
    if(template.year_month.value == "")
    {
        alert('请选择核算年月!');
        return false;
    }
    show_wait();
    template.subFunction.value="findAll";
    template.submit();
    return;
}
</Script> 

<html:html clazz="main" scrollCtl="yes" fixRows="2" isPrint="true">
<form name="template" method="post" action="itemDeptPerCostCompAnalyse.jspviewhigh">
<html:message/>
<html:title clazz='module'>科室医疗项目单位成本比较分析(医成本T3-04表)</html:title>

<html:table clazz="simple">
		<tr>	   
			<td class="signText">核算月:</td>
			<td class="normaltext"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"),true)%></td>
			<td class="signText">收入项目:</td>
			<td class="normalText">
			<%
			Select dict_income_subj=new Select(request.getAttribute("dict_income_subj"),"dict_income_subj",request.getParameter("dict_income_subj")==null? "":request.getParameter("dict_income_subj"),false,false);
			dict_income_subj.setAttribute("onchange","dischange()");
			%>
			<%=dict_income_subj.toString() %></td>
			<td class="signText">收费类别:</td>
			<td class="normalText">
			<%
			Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"b_dict_charge_detail_kind",request.getParameter("b_dict_charge_detail_kind")==null? "":request.getParameter("b_dict_charge_detail_kind"),false,false);
			b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
			%>
			<%=b_dict_charge_detail_kind.toString() %></td>
			<td class="signText">医疗项目: </td>
			<td class="normalText">
			<%
			Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"dict_charge_detail",request.getParameter("dict_charge_detail")==null? "":request.getParameter("dict_charge_detail"),false,false);
			%>
			<%=dict_charge_detail.toString() %></td>
		</tr>
		<tr>
			<td></td><td></td>
			<td class="signText">科室名称:</td>
			<td class="normalText">
			<%
			Select dict_acct_dept=new Select(request.getAttribute("dept"),"dict_acct_dept",request.getParameter("dict_acct_dept")==null? "":request.getParameter("dict_acct_dept"),false,true);
			%>
			<%=dict_acct_dept.toString() %></td>
			<td>
				<button class="pageBtn" onclick="return find();">计算</button>
			<%
			if (result!=null) {%>
				<button class="pageBtn" align='right' onclick="return preparedPrint();">打印</button>
			<%}%>
			</td>
    </tr>
    </html:table>

    <BR/>
    <html:title clazz='table'>科室医疗项目单位成本比较分析</html:title>
	<html:table clazz="result">
            <colgroup id=tg>
            	<col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.NAME_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            </colgroup>
            <tr>
            	<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">核算月</td>         
	            <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">科室名称</td>           
	            <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">医疗项目</td>            
	            <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">本期成本</td>
	            <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与上期成本比</td>
	            <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与同期成本比</td>
	            <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与医院标准比</td>
	            <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与市标准比</td>
	            <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与部颁标准比</td>
            </tr>
            <tr>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >成本</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >成本</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >成本</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >成本</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >成本</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
	            <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
            </tr>
			<%
			if(result!=null){
			for(int i=0;i<result.length;i++){
			%>
			<tr>
				<td nowrap class="normalText" style='text-align:left'><%=result[i][0]%></td>
				<td nowrap class="normalText" style='text-align:left'><%=result[i][1]%></td>
				<td nowrap class="normalText" style='text-align:left'><%=result[i][2]%></td>
			<%
			for(int j=3;j<result[i].length;j++){
			%>
				<td nowrap class="numberText" style='text-align:right'><%=result[i][j]%></td>
			<%}%>
		</tr>
		<%}}%>
	</html:table>
	<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	<input type="hidden" name="subFunction" value="findAll">
</form>
</html:html>