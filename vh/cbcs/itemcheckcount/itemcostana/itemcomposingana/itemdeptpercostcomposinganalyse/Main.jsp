<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemcostana/itemcomposingana/itemdeptpercostcomposinganalyse/Main.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
     $Author: zhoulidong $  
     $Date: 2012/03/12 01:58:28 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
     $NoKeywords: $
 -->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
com.viewhigh.cbcs.cbcs.util.DisplayWidth,java.text.*,java.util.Date,
com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<%
	String[][] result=(String[][])request.getAttribute("result");
	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
	String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
 
	String Ttitle="科室医疗项目成本构成分析 ";
	String today = new SimpleDateFormat("yyyy-MM").format(new Date());
	String Tyear=(String) request.getParameter("year")==null? today.split("-")[0]:request.getParameter("year");
	String month_from = (String)request.getParameter("month_from")==null? today.split("-")[1]:request.getParameter("month_from");
	String month_to = (String)request.getParameter("month_to")==null? today.split("-")[1]:request.getParameter("month_to");
  String date = (String) request.getAttribute("date");
  if(date !=null)
   Ttitle = Ttitle +"("+date+")"; 
%>

<Script language="javascript">
	function dischange(){
		template.changesign.value="dischange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function kindchange(){
		template.changesign.value="kindchange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function find(){
	
		if(template.year.value == "")
		{
			alert('请选择核算月!');
			return false;
		}
		if( template.month_from.value!= "" && template.month_to.value == "")
		{
			alert('请选择结束月份!');
			return false;
		}
 
		if(template.month_from.value>template.month_to.value){
			alert('起始月应小于结束月!');
			return false;
		}
	
		show_wait();
		template.subFunction.value="findAll";
		template.submit();
		return;
	}
</Script>

<html:html clazz="main" isPrint="true" fixRows="2">
	<form name="template" method="post" action="itemDeptPerCostComposingAnalyse.jspviewhigh">
		<html:message/>
		<html:title clazz='module'>科室医疗项目成本构成分析 (医成本T2-02表)</html:title>
		
		<table  width="100%" cellspacing="2" border="0" >
			<tr>	   

				<td class="signText">核算月:</td>

				<td class="normaltext"> 
				<%=new Select(year,"year",Tyear,false,false)%> 年
				<%=new Select(month,"month_from",month_from,false,false)%>月 至  
				<%=new Select(month,"month_to",month_to,false,false)%>月</td>
				<td class="signText">科室: </td>
				<td>
				<% Select dept=new Select(request.getAttribute("dept"),"dept",request.getParameter("dept")==null? "":request.getParameter("dept"),false,true);
				%>
				<%=dept.toString() %></td>
				<td class="signText">收入项目：</td>
				<td class="normalText">
				<%
				Select dict_income_subj=new Select(request.getAttribute("dict_income_subj"),"dict_income_subj",request.getParameter("dict_income_subj")==null? "":request.getParameter("dict_income_subj"),false,false);
				dict_income_subj.setAttribute("onchange","dischange()");
				%>
				<%=dict_income_subj.toString() %></td>
			</tr>
			<tr>
				<td class="signText">收费类别：</td>
				<td class="normalText">
				<%
				Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"b_dict_charge_detail_kind",request.getParameter("b_dict_charge_detail_kind")==null? "":request.getParameter("b_dict_charge_detail_kind"),false,false);
				b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
				%>
				<%=b_dict_charge_detail_kind.toString() %></td>
				<td class="signText">医疗项目: </td>
				<td class="normalText">
				<%
				Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"dict_charge_detail",request.getParameter("dict_charge_detail")==null? "":request.getParameter("dict_charge_detail"),false,false);
				%>
				<%=dict_charge_detail.toString() %></td>
				<td><button class="pageBtn" onclick="return find();">计算</button></td>
				<%
				if (result!=null) { 

				%>
					<td><button class="pageBtn" align='right' onclick="return preparedPrint();">打印</button></td>
				<%}%>
			</tr>
			</table>
			
			<br>

			<html:title clazz='table'><%=Ttitle%></html:title>
			
			<vh:table fixRow=1 fixCol=1>
				<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
				<colgroup id=tg>
						<col style = <%=DisplayWidth.MONEY_WIDTH%>  >
						<col style = <%=DisplayWidth.MONEY_WIDTH%>  >	
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >											
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
					</colgroup>
					<tr>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>科室</td>
				   <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>项目编码</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >医疗项目</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >总成本</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' >单位成本</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >工作量</td>
					</tr>
					<%
						DecimalFormat pf=new DecimalFormat("#,##0.00%");						
						DecimalFormat mf=new DecimalFormat("#,##0.00");	
					if(result!=null){
					for(int i=0;i<result.length;i++){
					%>
					<tr>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][0]%></td>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][1]%></td>
						<td nowrap class="normalText" style='text-align:left'><a href="itemDeptPerCostComposingAnalyse.jspviewhigh?subFunction=CostKind&&month_from=<%=request.getAttribute("month_from")%>&&month_to=<%=request.getAttribute("month_to")%>&&charge_detail_code=<%=result[i][6]%>&&dept_code=<%=result[i][7]%>&&dept_name=<%=result[i][0]%>&&charge_detail_name=<%=result[i][2]%>"><%=result[i][2]%></a></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][3]))%></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][4]))%></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][5]))%></td>
 					</tr>
					<%}}%>
			</table>
		</vh:table>
		
		
		<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
		<input type="hidden" name="subFunction" value="findAll">
	</form>
</html:html>