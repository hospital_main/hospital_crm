<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemcostana/itemcomposingana/itemdeptpercostcomposinganalyse/DirectStuff.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
     $Author:  
     $Date: 2012/03/12 01:58:28 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
     $NoKeywords: $
 -->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
com.viewhigh.cbcs.cbcs.util.DisplayWidth,java.text.*,
com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<%
	String[][] result=(String[][])request.getAttribute("result");
%>

<Script language="javascript">
  function back() {
		 self.history.back();
   }
</Script>

<html:html clazz="main" isPrint="true" fixRows="2">
	<form name="template" method="post" action="itemDeptPerCostComposingAnalyse.jspviewhigh">
		<html:message/>
		<html:title clazz='module'>科室医疗项目单位成本成本构成 （直接材料）</html:title>
			<br>
			<table  width="100%" cellspacing="2" border="0" >
				<tr>	   
					<td class="signText" style="text-align:left;width:30%">&nbsp;&nbsp;核算月：<%=request.getAttribute("date")%> </td>
					<td class="signText" style="text-align:left;width:30%">医疗项目：<%=request.getAttribute("charge_detail_name")%></td>
					<td class="signText" style="text-align:left;">成本类：<%=request.getAttribute("cost_class_name")%></td>
				</tr>
				<tr>	
				  <td class="signText" style="text-align:left;width:40%">成本项目：<%=request.getAttribute("cost_subj_name")%></td>
				  <td class="signText" colspan="2" style="text-align:left;width:40%">&nbsp;&nbsp;&nbsp;&nbsp;科室：<%=request.getAttribute("dept_name")%></td>
				</tr>
				<tr>
					<td colspan="3" align="right"><button class="pageBtn" onclick="return back();">返回</button></td>
				</tr>
			</table>

			<table border='1' width="99%" align="center" bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
				<colgroup id=tg>  
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >						
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
					</colgroup>

					<tr>
				   <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>材料编码</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >材料名称</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' >规格型号</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >数量</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >金额</td>
					</tr>
					
					<%
					DecimalFormat mf=new DecimalFormat("#,##0.00");
					DecimalFormat pf=new DecimalFormat("#,##0.00%");	
					if(result!=null){
					for(int i=0;i<result.length;i++){
						if(result[i][0].trim().equals("合计"))
							result[i][1]=	result[i][2]=	result[i][3]="";						

					%>
					<tr>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][0]%></td>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][1]%></td>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][2]%></td>
					<% if(result[i][0].trim().equals("合计")) { %>
						<td nowrap class="numberText" ><%= result[i][3] %></td>
					<% } else { %>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][3]))%></td>
					<% } %>

						<td nowrap class="numberText"  ><%= mf.format(Double.parseDouble(result[i][4]))%></td>	
					</tr>
					<%}}%>
			</table> 		 
		<input type="hidden" name="subFunction" value="DirectStuff">
	</form>
</html:html>