<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemcostana/itemcomposingana/itemhospercostcomposinganalyse/CostSubj.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:28 $
     $Modtime:   $
     $Revision: 1.1 $
     $NoKeywords: $
 -->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
com.viewhigh.cbcs.cbcs.util.DisplayWidth,java.text.*,
com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<%
	String[][] result=(String[][])request.getAttribute("result");
	String is_direct =(String) request.getAttribute("is_direct");
%>
<Script Language="JavaScript">
 
  function back() {
		 self.history.back();
   }
 
  </Script>
<html:html clazz="main" isPrint="true" fixRows="2">
	<form name="template" method="post" action="itemHosPerCostComposingAnalyse.jspviewhigh">
		<html:message/>
		<html:title clazz='module'> 全院医疗项目成本构成分析 （成本项目）</html:title>
			<br>
			<table  width="100%" cellspacing="2" border="0" >
				<tr>	   
					<td class="signText">核算月: 	<%=request.getAttribute("date")%> </td>
					<td class="signText">医疗项目: <%=request.getAttribute("charge_detail_name")%></td>
					<td class="signText">成本类: <%=request.getAttribute("cost_class_name")%></td>
					<td><button class="pageBtn" onclick="return back();" >返回</button></td>
				</tr>
			</table>
			<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
				<colgroup id=tg>
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.PERCENT_WIDTH%> >
					</colgroup>

					<tr>
				   <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>成本项目</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >总成本</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' >单位成本</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >比例</td>
					</tr>
					
					<%
					DecimalFormat pf=new DecimalFormat("#,##0.00%");
					DecimalFormat mf=new DecimalFormat("#,##0.00");

					if(result!=null){
					for(int i=0;i<result.length;i++){
					 if(result[i][0].equals("合计")) {
						  result[i][3]="";
					 }
					%>
					<tr>
						<td nowrap class="normalText" ><%=result[i][0]%></td>
					<%if(!result[i][0].equals("合计")&&is_direct.trim().equals("1")){%>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][1]))%></td>
						<td nowrap class="numberText" ><a href ="itemHosPerCostComposingAnalyse.jspviewhigh?subFunction=DirectStuff&&month_from=<%=request.getAttribute("month_from")%>&&month_to=<%=request.getAttribute("month_to")%>&&charge_detail_code=<%=request.getAttribute("charge_detail_code")%>&charge_detail_name=<%=request.getAttribute("charge_detail_name")%>&&cost_class_name=<%=request.getAttribute("cost_class_name")%>&&cost_subj_name=<%=result[i][0]%>&&cost_subj_code=<%=result[i][4]%>"><%=mf.format(Double.parseDouble(result[i][2]))%></a></td>
					<% }
						if(!result[i][0].equals("合计") && !is_direct.trim().equals("1")){ 

					%>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][1]))%></td>
						<td nowrap class="numberText"'><%=mf.format(Double.parseDouble(result[i][2]))%></td>
					<% }if(result[i][0].equals("合计")){	
					%>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][1]))  %></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][2]))%></td>
						<td nowrap class="numberText" ><%=result[i][3]%></td>
					<% } else {%>						 
						<td nowrap class="numberText" ><%=pf.format(Double.parseDouble(result[i][3]))%></td>
					<% } %>
					</tr>
					<%}}%>
			</table> 		 
		<input type="hidden" name="subFunction" value="CostKind">
 		<input type="hidden" name="charge_detail_code" value="<%=request.getAttribute("charge_detail_code")%>">
	</form>
</html:html>