<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemcostana/itemcomposingana/itemhospercostcomposinganalyse/CostKind.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
     $Author:  
     $Date: 2012/03/12 01:58:28 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
     $NoKeywords: $
 -->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
com.viewhigh.cbcs.cbcs.util.DisplayWidth,java.text.*,
com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
 
  function back() {
		 self.history.back();
   }
 
  </Script>

<%
	String[][] result=(String[][])request.getAttribute("result");
%>

<Script language="javascript">

</Script>

<html:html clazz="main" isPrint="true" fixRows="2">
	<form name="template" method="post" action="itemHosPerCostComposingAnalyse.jspviewhigh">
		<html:message/>
		<html:title clazz='module'> 全院医疗项目成本构成分析 （成本分类）</html:title>
			<br>
			<table  width="100%" cellspacing="2" border="0" >
				<tr>	   
					<td class="signText">核算月: 	<%=request.getAttribute("date")%> </td>
					<td class="signText">医疗项目: <%=request.getAttribute("charge_detail_name")%></td>
					<td><button class="pageBtn" onclick="return back();" >返回</button></td>
				</tr>
			</table>
			<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
				<colgroup id=tg>
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.PERCENT_WIDTH%> >
					</colgroup>

					<tr>
				   <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>成本分类</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >总成本</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' >单位成本</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >比例</td>
					</tr> 
					
					<%
						DecimalFormat pf=new DecimalFormat("#,##0.00%");						
						DecimalFormat mf=new DecimalFormat("#,##0.00");	
						if(result!=null){
						for(int i=0;i<result.length;i++){
					%>
					<tr>
					<%if(!result[i][0].trim().equals("合计")){ %>
						<td nowrap class="normalText" style='text-align:left'><a href ="itemHosPerCostComposingAnalyse.jspviewhigh?subFunction=CostSubj&&month_from=<%=request.getAttribute("month_from")%>&&month_to=<%=request.getAttribute("month_to")%>&charge_detail_code=<%=request.getAttribute("charge_detail_code")%>&charge_detail_name=<%=request.getAttribute("charge_detail_name")%>&&cost_class_name=<%=result[i][0]%>&&cost_class_code=<%=result[i][4]%>&&is_direct=<%=result[i][5]%>"><%=result[i][0]%></a></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][1]))%></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][2]))%></td>
						<td nowrap class="numberText" ><%=pf.format(Double.parseDouble(result[i][3]))%></td>
					<% }else { 
										  result[i][3]="&nbsp;&nbsp;";
					 %>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][0]%></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][1]))%></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][2]))%></td>
						<td nowrap class="numberText" ><%=result[i][3]%></td>
						<%}%>	
					</tr>
				<%}}%>
			</table>
 		<input type="hidden" name="subFunction" value="CostKind">
 		<input type="hidden" name="charge_detail_code" value="<%=request.getAttribute("charge_detail_code")%>">
	</form>
</html:html>