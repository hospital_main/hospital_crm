<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemcostana/itemcomposingana/itemdeptpercostkindanalyse/Detail.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:28 $
     $Modtime:   $
     $Revision: 1.1 $
     $NoKeywords: $
 -->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
com.viewhigh.cbcs.cbcs.util.DisplayWidth,java.text.*,
com.viewhigh.cbcs.base.mvc.view.component.Select"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
	String[][] result=(String[][])request.getAttribute("result");
%>

<Script language="javascript">
	function dischange(){
		template.changesign.value="dischange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function kindchange(){
		template.changesign.value="kindchange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function find(){
	
		if(template.year.value == "")
		{
			alert('请选择核算起始年度!');
			return false;
		}
		if( template.month_from.value!= "" && template.month_to.value == "")
		{
			alert('请选择结束月份!');
			return false;
		}
 
		if(template.month_from.value>template.month_to.value){
			alert('起始月应小于结束月!');
			return false;
		}
	
		show_wait();
		template.subFunction.value="findAll";
		template.submit();
		return;
	}
</Script>

<html:html clazz="main" isPrint="true" fixRows="2">
	<form name="template" method="post" action="itemHosPerCostKindAnalyse.jspviewhigh">
		<html:message/>
		<html:title clazz='module'>科室医疗项目单位成本分类分析明细表</html:title>
			<table width="100%">
				<tr>	   
					<td class="signText" style="text-align:left;width:30%">核算月：<%=request.getAttribute("date")%> </td>
					<td class="signText" style="text-align:left;width:30%">医疗项目：<%=request.getAttribute("charge_detail_name")%></td>
					<td class="signText" style="text-align:left;">&nbsp;&nbsp;科室：<%=request.getAttribute("dept_name")%></td>
				</tr>
				<tr>
				 	
				<tr>
			  <tr>
			  	<td align="right" colspan="3"><button class="pageBtn" onclick="window.history.back();">返回</button></td>
			  </tr>
				</tr>
			</table>	

			<html:title clazz='table'>科室医疗项目单位成本分类分析明细表</html:title>

			<vh:vhFixTable fixRow=2 fixCol=1>
				<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
				<colgroup id=tg>
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >
						<col style = <%=DisplayWidth.MONEY_WIDTH%>  >						
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.PERCENT_WIDTH%> >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> >
						<col style = <%=DisplayWidth.PERCENT_WIDTH%> >
					</colgroup>
					<tr>       
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowSpan="2">成本分类 </td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowSpan="2" >合计 </td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="5" >直接成本 </td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colSpan="2" >间接成本 </td>
					</tr>
					<tr>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>直接计入 </td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >合作成本 </td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >计算计入 </td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' >小计 </td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >比例 </td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' >成本 </td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >比例 </td>
					</tr>
					
					<%
					DecimalFormat pf=new DecimalFormat("#,##0.00%"); 
					DecimalFormat mf=new DecimalFormat("#,##0.00");
					if(result!=null){
					for(int i=0;i<result.length;i++){
					%>
					<tr>
					  <%
					    boolean isTotal = false;
					    if(i == result.length -1)
					      isTotal = true;
					  %>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][0]%></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][1]))%></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][2]))%></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][3]))%></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][4]))%></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][5]))%></td>
						<td nowrap class="numberText" ><%=isTotal ? "" :pf.format(Double.parseDouble(result[i][6]))%></td>
						<td nowrap class="numberText" ><%=mf.format(Double.parseDouble(result[i][7]))%></td>
						<td nowrap class="numberText" ><%=isTotal ? "" :pf.format(Double.parseDouble(result[i][8]))%></td>						
				 
					</tr>
					<%}}%>
			</table>
		</vh:vhFixTable>
		
		
		<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
		<input type="hidden" name="subFunction" value="findAll">
	</form>
</html:html>