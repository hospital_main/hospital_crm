<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/itemcostana/itemperiodana/deptpcpanalyse/Main.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
com.viewhigh.cbcs.cbcs.util.DisplayWidth,
com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<%
String[]ayear= (String[])request.getAttribute("ayear");
String[][] result=(String[][])request.getAttribute("result");
%>
<script language="JavaScript" src="/base/scripts/CreateObjects.js" ></script>
<Script language="javascript">
function dischange(){
    template.changesign.value="dischange";
    template.subFunction.value="change";
    show_wait();
    template.submit();
    return;
}
function kindchange(){
    template.changesign.value="kindchange";
    template.subFunction.value="change";
    show_wait();
    template.submit();
    return;
}
function find(){
    if(template.syear.value == "")
    {
        alert('请选择核算起始年月!');
        return false;
    }
    if(template.eyear.value == "")
    {
        alert('请选择核算结束年月!');
        return false;
    }		
    if(template.month.value == "")
		{
			alert('请选择核算月份!');
			return false;
		}
    if(template.syear.value>template.eyear.value){
        alert('起始年月应小于结束年月!');
        return false;
    }
    if (template.eyear.value-template.syear.value>5){    
        alert('年份相隔不能超过5年!');
        return false;
      }
		if(template.dept.value == "")
		{
			alert('请选择科室!');
			return false;
		}		
		if(template.dict_income_subj.value == "")
		{
			alert('请选择收入项目!');
			return false;
		}	
    show_wait();
    template.subFunction.value="findAll";
    template.submit();
    return;
	}
   	function	draw(){
   		openEg('itemDeptPerCostPeriodAnalyse.jspviewhigh?subFunction=drawGraph',800,423,false,false);//在屏幕中心弹出窗口
  	}	
</Script>

<html:html clazz="main" isPrint="true" fixRows="2">
	<form name="template" method="post" action="itemDeptPerCostPeriodAnalyse.jspviewhigh">
	<html:message/>
	<html:title clazz='module'>科室医疗项目单位成本同期分析(环比)(医成本T5-04表) </html:title>
	
	<table  width="100%" cellspacing="2" border="0" >
  <%
  	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
  	String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
  %>
     <tr>	   
  	 <td class="signText">年度:</td>
  	 <td class="normaltext"> 
  	 <%=new Select(year,"syear",request.getParameter("syear")==null? "":request.getParameter("syear"),false,false)%>年 至  
  	 <%=new Select(year,"eyear",request.getParameter("eyear")==null? "":request.getParameter("eyear"),false,false)%>年 
  	 <%=new Select(month,"month",request.getParameter("month")==null? "":request.getParameter("month"),false,false)%>月</td>
  		
			
			<td class="signText">收入项目：</td>
			<td class="normalText">
			<%
			Select dict_income_subj=new Select(request.getAttribute("dict_income_subj"),"dict_income_subj",request.getParameter("dict_income_subj")==null? "":request.getParameter("dict_income_subj"),false,false);
			dict_income_subj.setAttribute("onchange","dischange()");
			%>
			<%=dict_income_subj.toString() %></td>
		</tr>
		<tr>
			<td class="signText">收费类别：</td>
			<td class="normalText">
			<%
			Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"b_dict_charge_detail_kind",request.getParameter("b_dict_charge_detail_kind")==null? "":request.getParameter("b_dict_charge_detail_kind"),false,false);
			b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
			%>
			<%=b_dict_charge_detail_kind.toString() %></td>
			<td class="signText">医疗项目: </td>
			<td class="normalText">
			<%
			Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"dict_charge_detail",request.getParameter("dict_charge_detail")==null? "":request.getParameter("dict_charge_detail"),false,false);
			%>
			<%=dict_charge_detail.toString() %></td>
			
			</td>
		</tr>
		 <tr><td class="signText">科室名称:</td>
      <td > 
				<%
				Select dept=new Select(request.getAttribute("dept"),"dept",request.getParameter("dept")==null? "":request.getParameter("dept"),false,true);
				%>
				<%=dept.toString() %></td>
			</td> 
			<td><button class="pageBtn" align='right' onclick="return find();">计算</button></td>
			<%
			if (result !=null) { %>
			<td><button class="pageBtn" align='right'  onclick="return draw();" >图形</button></td>		
			<td><button class="pageBtn" align='right' onclick="return preparedPrint();">打印</button></td>
			<%}%>
		</tr>
	</table>

    <br>
    <html:title clazz='table'>科室医疗项目单位成本同期分析(环比)</html:title>

			<vh:table fixRow=2 fixCol=1>
				<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
					<colgroup id=tg>
						<col style = <%=DisplayWidth.MONEY_WIDTH%>  >
						<col style = <%=DisplayWidth.NAME_WIDTH%>  >
						<col style = <%=DisplayWidth.MONEY_WIDTH%> > 
						 <%
						if (ayear!=null){
						for(int i = 0; i<(ayear.length-1)*3;i++){ 
						%>
						<col style = <%=DisplayWidth.MONEY_WIDTH%> > 
						<%
						  } //for
						}//if 
						%>
						
					</colgroup>
					<tr> 
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">科室名称</td> 
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">医疗项目名称</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' ><%= ayear==null?"":ayear[0]%></td>
			   <%	
			   if (result!=null) {
				    int year_=1;
				   	while (year_<ayear.length) 
				 { %>
				 	  	<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3"><%=ayear[year_]%> </td>
				 <% year_++;
				 }}%>
					</tr>
					<tr>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>成本</td>
			   <%	
			   if (result!=null) {
				    int year_=1;				    
				   	while(year_<ayear.length) 
				 { %>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >成本</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  >差异</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' >差异率</td>
				 <% year_++;
				 }}%>
					</tr>
					<%
					if(result!=null){
					for(int i=0;i<result.length;i++){
					%>
					<tr>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][0]%></td>
						<td nowrap class="normalText" style='text-align:left'><%=result[i][1]%></td>
						<%
						for(int j=2;j<result[i].length;j++){
						%>
						<td nowrap class="numberText" style='text-align:right'><%=result[i][j]%></td>
						<%}%>
					</tr>
					<%}}%>
			</table>
		</vh:table>
	
	<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	<input type="hidden" name="subFunction" value="findAll">
	</form>
</html:html>