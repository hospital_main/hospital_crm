<%@ page language="java" import="java.util.*" pageEncoding="GBK"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	String [][] graph = (String[][])request.getSession(false).getAttribute("graph");
	String [] graphTitle = (String[])request.getSession(false).getAttribute("graphTitle");
	String FusionChartsXML="";
	String xmlStr="<chart xAxisName='Month' yAxisName='Revenue' caption='全院医疗项目单位收益同期分析(环比)' palette='2' showValues='1' >";
	String categories="<categories>";
	String dataSet="";

	for(int i=0;i<graphTitle.length;i++){
		String seriesName=graphTitle[i];
		dataSet+="<dataset seriesName='"+seriesName+"' >";
		for(int j=0;j<graph.length;j++){
			if(i==0){
				categories+="<category label='"+graph[j][0]+"年"+graph[j][1]+"月' />";
			}
			String value=graph[j][2+i];
			dataSet+="<set value='"+value+"'/>";	
		}
		dataSet+="</dataset>";
	}
	FusionChartsXML=xmlStr+categories+"</categories>"+dataSet+"</chart>";

	request.setAttribute("xml",FusionChartsXML);
%>
<script language="JavaScript" src="/base/scripts/FusionCharts.js" ></script>
<script>
	 var FusionChartsXML="<%=request.getAttribute("xml")%>";
		window.FusionChartsXML=FusionChartsXML;
</script>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>图形展示页面</title>	
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="this is my page">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  </head>
  <body>
    <div id="FunsionChart_div"></div>
    <script>
		var args = GetUrlParms();
		var xml=window.FusionChartsXML
		var type = 1;
			var chart;
			if(type==1){
				chart =new FusionCharts("MSLine.swf", "ChartId", "100%", "100%", "0", "0");
			}else if(type==2){
				chart =new FusionCharts("MSColumn2D.swf", "ChartId", "100%", "100%", "0", "0");
			}else if(type==3){
				chart =new FusionCharts("Pie2D.swf", "ChartId", "100%", "100%", "0", "0");
			}
			chart.setDataXML(xml);   
			chart.render("FunsionChart_div");

		
		function GetUrlParms(){
		    var args=new Object();   
		    var query=location.search.substring(1);//获取查询串   
		    query=decodeURI(query);
		    var pairs=query.split("&");//在逗号处断开   
		    for(var   i=0;i<pairs.length;i++){   
		        var pos=pairs[i].indexOf('=');//查找name=value   
		            if(pos==-1)   continue;//如果没有找到就跳过   
		            var argname=pairs[i].substring(0,pos);//提取name   
		            var value=pairs[i].substring(pos+1);//提取value   
		            args[argname]=unescape(value);//存为属性   
		    }
		    return args;
		}

	</script>
  </body>
</html>

