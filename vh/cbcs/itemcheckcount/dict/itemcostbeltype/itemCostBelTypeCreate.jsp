<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/dict/itemcostbeltype/itemCostBelTypeCreate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:28 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
     $NoKeywords: $ 
--> 
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
		  if(trim(template.cost_class_code.value)=="")
		    {
		      alert('成本分类名称不能为空!');
		      return;
		    }    
		    if(trim(template.cost_subj_code.value)=="")
		    {
		      alert('成本项目编码不能为空!');
		      return;
		    }
  
        template.subFunction.value='Create';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemcostbeltype.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>成本所属分类定义添加页面 </html:title>  

  <!-- 简单信息 -->  
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">成本项目名称：</td>
        <td class="normalText" nowrap="nowrap">
         <?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea" name="cost_subj_code"  AdjustVal="95" previousObj="cost_class_code" codeCol='spell' valueCol="cost_subj_code" textCol="cost_subj_name" width="130" top="38" left="140" Lheight="5" xmlSource="dic/dict_subj_cost_detail_LV.xml" init="1"/>

 <!--     <td class="normalText" nowrap="nowrap"><%=new Select(request.getAttribute("cost_subj_code"),"cost_subj_code",request.getParameter("cost_subj_code")==null? "":request.getParameter("cost_subj_code"),false,false)%></td>
   -->
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <td class="signText" nowrap="nowrap">成本分类名称：</td>
      <td width="75%" class="normalText" nowrap="nowrap"><%=new Select(request.getAttribute("cost_class_code"),"cost_class_code",request.getParameter("cost_class_code")==null? "":request.getParameter("cost_class_code"),false,false)%></td>
    </tr>
		<tr>
      <td colspan="2">
      	<button class="pageBtn" onclick="return create();">添加</button>
      	<button class="pageBtn" onclick="return reset();">重置</button>
      	<button class="pageBtn" onclick="return back(template);">返回</button>
      	
      	<!--img src="images/create.gif" class="mouse" onclick="return create();" />
       	<img src="images/reset.gif" class="mouse" onclick="return reset();" />
       	<img src="images/return.gif" class="mouse" onclick="return back(template);" /--></td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="preparedCreate"/>
</form>

</html:html>   