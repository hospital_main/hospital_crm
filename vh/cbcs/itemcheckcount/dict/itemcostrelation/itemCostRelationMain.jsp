<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/dict/itemcostrelation/itemCostRelationMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, 
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='preparedfindAll';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="itemcostrelation.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>直接成本材料对应关系</html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
	    <tr>
	      <td nowrap class="signText">直接成本项目：</td>
	        <% String cost_subj_code = request.getParameter("cost_subj_code")==null ? "":request.getParameter("cost_subj_code");%>
	      <td><%=new Select(request.getAttribute("cost_subj"),"cost_subj_code",cost_subj_code,true,false)%></td>
	      <td nowrap class="signText">物资类别编号：</td>
	      <% String inv_code = request.getParameter("inv_code");%>
	      <td><input type=text name="inv_code" MAXLENGTH ='20' class="textInputC" <%if(inv_code!= null){ out.println(" value=" + inv_code);}%>></td>
	    </tr>
	    <tr>
	      <td nowrap class="signText">物资类别名称：</td>
	        <% String inv_name = request.getParameter("inv_name");%>
	      <td><input type=text name="inv_name" MAXLENGTH ='20' class="textInputC" <%if(inv_name != null){ out.println(" value=" + inv_name);}%>></td>
	      <td class="normalText"></td>
	      <td class="normalText">
	      	<!--img src="images/find.gif" class="mouse" onclick="return find();" /-->
	      	<button class="pageBtn" onclick="return find();">查询</button>
	      </td>
	    </tr>	

	  </html:table>

	  <br>

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
	  
  
		<html:title clazz='table'>直接成本材料对应关系</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>

		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td nowrap="nowrap" class="resultLabel" >选择</td>
		          <td nowrap="nowrap" class="resultLabel" >物资类别编码</td>
		          <td nowrap="nowrap" class="resultLabel" >物资类别名称</td>
		          <td nowrap="nowrap" class="resultLabel" >直接成本项目编码</td>
		          <td nowrap="nowrap" class="resultLabel" >直接成本项目名称</td>
		        </html:tr>

		        <%
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>
 
		        <tr CLASS="<%=rowColor%>">
		          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
		          <td nowrap="nowrap" class="normalText"><a href="itemcostrelation.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>"><%=primaryKey%></a></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td> 

		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		<%}%>
	  <input type=hidden name="subFunction"/>
	  <input type="hidden" name="initsub" value="sub"/>
	</form>
</html:html>

