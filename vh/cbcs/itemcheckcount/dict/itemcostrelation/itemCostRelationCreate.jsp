<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/dict/itemcostrelation/itemCostRelationCreate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
    		if(trim(template.inv_code.value)==""){
    			alert("物资类别名称不能为空!");
    			return false;
    		}
    		if(trim(template.cost_subj_code.value)==""){
    			alert("直接成本项目不能为空!");
    			return false;
    		}
        template.subFunction.value='Create';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='preparedfindAll';
    template.initsub.value="sub";
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemcostrelation.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>直接成本材料对应关系添加页面</html:title>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap" >物资类别名称：</td>
	      <% String inv_code = request.getParameter("inv_code")==null ? "":request.getParameter("inv_code");%>
	      <td><%=new Select(request.getAttribute("inv_type"),"inv_code",inv_code,true,false)%></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">直接成本项目：</td>
      <% String cost_subj_code = request.getParameter("cost_subj_code")==null ? "":request.getParameter("cost_subj_code");%>
	      <td><%=new Select(request.getAttribute("cost_subj"),"cost_subj_code",cost_subj_code,true,false)%></td>
    </tr>
    <tr>
      <td colspan="2"> 
      	<button class="pageBtn" onclick="return create();">添加</button>
      	<button class="pageBtn" onclick="return reset();">重置</button>
      	<button class="pageBtn" onclick="return back(template);">返回</button>
	      <!--img src="images/create.gif" class="mouse" onclick="return create();" /> 
	      <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
	      <img src="images/return.gif" class="mouse" onclick="return back(template);" /-->
	     </td>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="Create"/>
</form>

</html:html>   