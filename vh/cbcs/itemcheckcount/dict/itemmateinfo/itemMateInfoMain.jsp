<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/dict/itemmateinfo/itemMateInfoMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:28 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, 
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {
    	if(isCheckSysmodconform()=="1"){
    		alert('和物流或者科室成本系统联用,不能进行该操作！');
    		return;
    	}
        template.subFunction.value='preparedCreate';
        template.submit();
        return true;
    }

  function remove() {
  	  if(isCheckSysmodconform()=="1"){
    		alert('和物流或者科室成本系统联用,不能进行该操作！');
    		return;
    	}
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
  //判断项目核算系统与其他系统是否连用
   function isCheckSysmodconform(){
	   var p=getValuePairBySql("cbcsRate_isSysmodconform_dict","<sql_flag>1</sql_flag><mod_code1>dept</mod_code1><mod_code2>mate</mod_code2>");
	   return p[0];
	}
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="itemmateinfo.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>材料信息定义</html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
	    <tr>
	      <td nowrap class="signText">物资类别编号：</td>
	        <% String inv_type_code = request.getParameter("inv_type_code");%>
	      <td><input type=text name="inv_type_code" class="textInputC" <%if(inv_type_code != null){ out.println(" value=" + inv_type_code);}%>></td>
	      <td nowrap class="signText">物资类别名称：</td>
	      <% String inv_type_name = request.getParameter("inv_type_name");%>
	      <td><input type=text name="inv_type_name" class="textInputC" <%if(inv_type_name!= null){ out.println(" value=" + inv_type_name);}%>></td>
	    </tr>
	    <tr>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      
	      	    <tr>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      <td class="normalText">
	     		<button class="pageBtn" onclick="return find();">查询</button>
	      	<!--img src="images/find.gif" class="mouse" onclick="return find();" /-->
	      </td>
	    </tr>	
	    </tr>

	  </html:table>

	  <br>

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
	  
  
		<html:title clazz='table'>材料信息定义</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>

		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td nowrap="nowrap" class="resultLabel" >选择</td>
		          <td nowrap="nowrap" class="resultLabel" >材料编码</td>
		          <td nowrap="nowrap" class="resultLabel" >材料名称</td>
		          <td nowrap="nowrap" class="resultLabel" >物资类别</td>
		          <td nowrap="nowrap" class="resultLabel" >规格型号</td>
		          <td nowrap="nowrap" class="resultLabel" >计量单位</td>
		          <td nowrap="nowrap" class="resultLabel" >拼音码</td>
		        </html:tr>

		        <%
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>
 
		        <tr CLASS="<%=rowColor%>">
		          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
		          <td nowrap="nowrap" class="normalText"><a href="itemmateinfo.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>"><%=primaryKey%></a></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td> 
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 4 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 5 ]%></td> 

		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		<%}%>
	  <input type=hidden name="subFunction"/>
	  <input type="hidden" name="initsub" value="sub"/>
	</form>
</html:html>

