<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemcheckcount/dict/itemcosttype/itemCostTypeUpdate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ 
 -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
								com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    if(isEmpty(template.cost_class_name))
    {
      alert('成本类别名称不能为空!');
      return;
    }
    template.subFunction.value='save';
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back(element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="itemcosttype.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module">成本分类定义修改页面</html:title>

  <% String[] result = (String[])request.getAttribute("result");
	 		String[][] flag ={{"0","其他归集"},{"1","材料归集"},{"2","人工归集"},{"3","奖金归集"},{"4","离退休归集"},{"5","折旧归集"}}; 
	 		if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>     
      <td class="signText" nowrap="nowrap"> 成本分类编号：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0]%>" disabled />
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">成本分类名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="cost_class_name" value="<%=result[1]%>" class="textInputC">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">成本性质定义:</td>
      <td nowrap class="normalText" ><%=new Select(flag,"is_direct",result[2],false,true)%></td>
    </tr>  
     <tr>
      <td class="signText" nowrap="nowrap" >上级代码：</td>
      <td class="normalText" nowrap="nowrap">
        <input  disabled="disabled" type=text value="<%=result[3]%>"/>
      </td>
    </tr>  
    <tr>
      <td class="signText" nowrap="nowrap">是否停用：</td>
      <td class="normalText" nowrap="nowrap">
        是<input type="radio" name="stop_mark" value="Y" <% if(result[4].equals("Y")){out.println(" checked ");}%>/>
        否<input type="radio" name="stop_mark" value="N" <% if(result[4].equals("N")){out.println(" checked ");}%>/>
      </td>
    </tr>
    <tr>
      <td class="signText"  nowrap="nowrap">是否末级：</td>
      <td class="normalText" nowrap="nowrap" >
        是<input type="radio" disabled="disabled" name="last_levely" value="Y" <% if(result[5].equals("Y")){out.println(" checked ");}%>/>
        否<input type="radio" disabled="disabled" name="last_leveln" value="N" <% if(result[5].equals("N")){out.println(" checked ");}%>/>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">拼音码：</td>
      <td class="normalText" nowrap="nowrap">  
         <input type=text name="spell"  value="<%=result[6]%>"/>
      </td>
    </tr>
     <tr>
      <td colspan="2">
      	<button class="pageBtn" onclick="return save();">保存</button>
      	<button class="pageBtn" onclick="return reset();">重置</button>
      	<button class="pageBtn" onclick="return back(template);">返回</button>
      <!--
      <img src="images/save.gif" class="mouse" onclick="return save();" />
      <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />
      -->
      </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="cost_class_code" value="<%=result[0]%>">
  <input type="hidden" name="last_level" value="<%=result[5]%>">
  <input type="hidden" name="supp_item_code" value="<%=result[3]%>">
  <%}%>
</form>

</html:html>
