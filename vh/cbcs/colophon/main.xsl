<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">
		    <th noWrap="true">主版本号</th>
			<th noWrap="true">版本补丁号</th>
			<th noWrap="true">升级类型</th>
			<th noWrap="true">升级时间</th>
		</tr>
		
	</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>        
					<xsl:for-each select="td">
						<td>
				              <xsl:value-of select="."/>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>