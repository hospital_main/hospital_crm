<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/analyse/pergaincomp/gainThisComp.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.util.Preference,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
function find(){
	template.subFunction.value="findAll";
	show_wait();
	template.submit();

}

</Script>
<html:html clazz="main">
<form name="template" method="post" action="perGainComp.jspviewhigh">
	<html:message/>
      
    	<html:table clazz="simple"><tr><td><html:title clazz='table'>病种单位收益同期比较明细表</html:title></td></tr></html:table>
			<%
			      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
			      if (ro != null) {
			      String checksign=(String)request.getAttribute("checksign");
			         TableMarge oper = new TableMarge(ro, "return find()");
			%>

			<html:table clazz="simple">
				<tr><td><%=oper%></td></tr>
			</html:table>
			
			<html:table clazz="complex">			
			<tr>
        <td>
		      <html:table clazz="result">
						<html:tr clazz='label'>
							<td nowrap rowspan="3" class="resultLabel"  >时间</td>
							<td nowrap rowspan="3" class="resultLabel" >病种名称</td>
							<td nowrap rowspan="3" class="resultLabel" >DRGs分组</td>
							<%if(checksign!=null&&checksign.equals("D")){%>
							<td nowrap rowspan="3" class="resultLabel" >科室名称</td>
							<%}%>
							<td nowrap rowspan="2" colspan="3" class="resultLabel" >本期收益</td>
							<td nowrap colspan="9" class="resultLabel" >与同期收益比</td>
						</html:tr>
						<html:tr clazz='label'>						
							<td nowrap colspan="3" class="resultLabel" >总收益</td>
							<td nowrap colspan="3" class="resultLabel" >医疗收益</td>
							<td nowrap colspan="3" class="resultLabel" >药品收益</td>
						</html:tr>
						<html:tr clazz='label'>
							<td   nowrap class="resultLabel" ><div align="center">总收益</div></td>
							<td   nowrap class="resultLabel" ><div align="center">医疗收益</div></td>
							<td   nowrap class="resultLabel" ><div align="center">药品收益</div></td>
							<td   nowrap class="resultLabel" ><div align="center">总收益</div></td>
							<td   nowrap class="resultLabel" ><div align="center">差异</div></td>
							<td   nowrap class="resultLabel" ><div align="center">差异率</div></td>
							<td   nowrap class="resultLabel" ><div align="center">医疗收益</div></td>
							<td   nowrap class="resultLabel" ><div align="center">差异</div></td>
							<td   nowrap class="resultLabel" ><div align="center">差异率</div></td>
							<td   nowrap class="resultLabel" ><div align="center">药品收益</div></td>
							<td   nowrap class="resultLabel" ><div align="center">差异</div></td>
							<td   nowrap class="resultLabel" ><div align="center">差异率</div></td>
						</html:tr>
        <%
          String[][] result = ro.getTableResult();
          DecimalFormat mf=new DecimalFormat("#,##0.00");
          DecimalFormat mv=new DecimalFormat("#,##0.00%");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

       <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText"><%=result[ i ][ 0 ]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 1 ]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 2 ]%></td>
							<%if(checksign!=null&&checksign.equals("D")){%>
          <td nowrap class="normalText"><%=result[ i ][ 3 ]%></td>
							<%}%>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 4 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 5 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 6 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 7 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 8 ]))%></td>
          <td nowrap class="numberText"><%=mv.format(Double.parseDouble(result[ i ][ 9 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 10]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 11]))%></td>
          <td nowrap class="numberText"><%=mv.format(Double.parseDouble(result[ i ][ 12]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 13]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 14]))%></td>
          <td nowrap class="numberText"><%=mv.format(Double.parseDouble(result[ i ][ 15]))%></td>
        </tr>
        <%
              }
            }
        %>
       </html:table>
         	</td>
         </tr>
      </html:table>
<%}%>
<input type="hidden" name="subFunction" >
</form>
</html:html>
