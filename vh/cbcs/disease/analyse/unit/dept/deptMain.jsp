<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/analyse/unit/dept/deptMain.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,java.text.*,com.viewhigh.cbcs.base.util.Preference,com.viewhigh.cbcs.cbcs.util.DictCache" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
function find(){
	template.subFunction.value="findAlld";
	show_wait();
	template.submit();
}

</Script>
<html:html clazz="main">
<form name="template" method="post" action="bingfenwholedept.jspviewhigh">
      <html:message/>
      <html:title clazz='module'>科室病种单位收益分析(医成本D1-04表)</html:title>
      <html:table clazz="simple">
      <tr>

      <td class="signText" >核算月:
      </td>
      <td class="signText" ><%= new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month",request.getParameter("year_month"))%>
      </td>
      <td class="signText" >就诊类型:</td>
      <td class="signText" ><select  class="selectBg" name="checksign" size="1">
                <%String check=request.getParameter("checksign");%>
                <option value="O"<%if(check!=null&&check.equals("O")){out.print("selected");}%>>门诊</option>
                <option value="I"<%if(check!=null&&check.equals("I")){out.print("selected");}%>>住院</option>
           </select></td>
      </tr>
        <tr>
              <td class="signText">病种:</td>
              <td class="signText">
              <select  name="leave_disease_code" multiple="multiple" class="selectBg" size="4" >
                <%String[][] name=(String[][])request.getAttribute("d_name");
               String[] dva=request.getParameterValues("leave_disease_code");%>
               <option value="" <%if(dva==null||(dva.length==1&&dva[0].equals(""))||(dva.length>1&&dva[0].equals(""))){
                  out.print("selected");}%>>--不限--</option >
               <%
               if(name!=null){for(int i=0;i<name.length;i++){%>
                <option value="<%=name[i][0]%>"
                  <%if(dva!=null){for(int j=0;j<dva.length;j++){
                    if(name[i][0].equals(dva[j]))out.print("selected");}}%>>
                    <%=name[i][0]+":"+name[i][1]%></option><%}}%>
                 </select ></td>
              <td class="signText">DRGs分组:</td>
              <td class="signText"><select name="leave_disease_model" size="4" class="selectBg" multiple="multiple">
                <%String [][] type=(String[][])request.getAttribute("d_type");
               String[] tva=request.getParameterValues("leave_disease_model");%>
               <option value="" <%if(tva==null||(tva.length==1&&tva[0].equals(""))||(tva.length>1&&tva[0].equals(""))){
                  out.print("selected");}%>>--不限--</option >
               <%
               if(type!=null){for(int i=0;i<type.length;i++){%>
                <option value="<%=type[i][0]%>"
                  <%if(tva!=null){for(int j=0;j<tva.length;j++){
                    if(type[i][0].equals(tva[j]))out.print("selected");}}%>>
                    <%=type[i][0]+":"+type[i][1]%></option><%}}%>
                </select></td>
        </tr>
        <tr> <td class="signText">科室名称:</td>
              <td class="signText"><select  class="selectBg" name="dept" size="4" multiple="multiple">
                <%String[][] dept=(String[][])request.getAttribute("dept");
                String[] dval=request.getParameterValues("dept");%>
               <option value="" <%if(dval==null||(dval.length==1&&dval[0].equals(""))||(dval.length>1&&dval[0].equals(""))){
                  out.print("selected");}%>>--不限--</option >
                <%
                if(dept!=null){
                  for(int i=0;i<dept.length;i++){%>
                <option value="<%=dept[i][0]%>"
                  <%if(dval!=null){for(int j=0;j<dval.length;j++){
                    if(dept[i][0].equals(dval[j])){
                    out.print("selected");
                    }}}%>>
                    <%=dept[i][1]%></option>
                <%}}%>
                </select>
             <td >
            <button class="pageBtn" name=""  onclick="find();"  >查询</button> 
           <!--  <img src="images/find.gif" class="mouse" onclick="find();">--> <td>
        </tr>
</html:table>
    <html:table clazz="simple"><tr><td><html:title clazz='table'>科室病种单位收益分析表</html:title></td></tr></html:table>
<%
      BaseRO ro = (BaseRO)request.getAttribute("basero");
      if (ro != null) {
         TableMarge oper = new TableMarge(ro, "return find()");
%>

<html:table clazz="simple"><tr><td><%=oper%></td></tr></html:table>


     <html:table clazz="complex">

<tr>
        <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td nowrap rowspan="2" class="resultLabel"  >时间</td>
          <td nowrap rowspan="2" class="resultLabel" >病种名称</td>
          <td nowrap rowspan="2" class="resultLabel" >DRGs分组</td>
          <td nowrap rowspan="2" class="resultLabel" >科室名称</td>
          <td nowrap colspan="3" class="resultLabel" >单位收益</td>
          <td nowrap colspan="3" class="resultLabel" >医疗收益</td>
                            <%if (Preference.isDiffDrugCost()) {%>
          <td nowrap colspan="9" class="resultLabel" >药品收益</td>
          <%}else{%>
          <td nowrap colspan="3" class="resultLabel" >药品收益</td>
          <%}%>
        </html:tr>
                 <html:tr clazz='label'>

                  <td   nowrap class="resultLabel" ><div align="center">单位收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">单位成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">单位收益</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">医疗收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">医疗成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">医疗收益</div></td>
                  <%if (Preference.isDiffDrugCost()) {%>
                  <td   nowrap class="resultLabel" ><div align="center">西药收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">西药成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">西药收益</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">中成药收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">中成药成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">中成药收益</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">中草药收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">中草药成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">中草药收益</div></td>
                  <%}else{%>
                  <td   nowrap class="resultLabel" ><div align="center">药品收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">药品成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">药品收益</div></td>
                  <%}%>
                </html:tr>


        <%
          String[][] result = ro.getTableResult();
          DecimalFormat mf=new DecimalFormat("#,##0.00");
          DecimalFormat pf=new DecimalFormat("#0.00%");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

       <tr CLASS="<%=rowColor%>"> 
          <td nowrap class="normalText"><%=primaryKey%></td>
          <td nowrap class="normalText"><%=result[ i ][ 2]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 3]%></td>
          <td nowrap class="normalText"><%=result[ i ][6]%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 8]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 9]))%></td>
          <td nowrap class="numberText"><%=pf.format(Double.parseDouble(result[ i ][ 7]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 12]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 14]))%></td>
          <td nowrap class="numberText"><%=pf.format(Double.parseDouble(result[ i ][ 10]))%></td>
          <%if(Preference.isDiffDrugCost){%>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 17]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 16]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 18]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 20]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][19]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 21]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 23]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][22]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][24]))%></td>
          <%}else{%>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 13]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 15]))%></td>
          <td nowrap class="numberText"><%=pf.format(Double.parseDouble(result[ i ][ 11]))%></td>
          <%}%>
        </tr>

        <%
              }
            }
        %>
       </html:table>
         </td>
         </tr>

      </html:table>
<%}%>
<input type="hidden" name="subFunction" >
</form>
</html:html>
