<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/analyse/perpathcostcomp/pathCGAnalyMain.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.util.Preference,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
function find(){
	if(template.year_month.value==""){
		alert("请选择年月");
		return;
	}
	template.subFunction.value="findAll";
	show_wait();
	template.submit();
}
function checkdept(){
	if(template.checksign.value=="W"){
		id_deptname.style.display="none";
		td_deptname.style.display="none";
		p1.style.display="block";
		p2.style.display="block";
		p3.style.display="block";
	}else{	
		p1.style.display="none";
		p2.style.display="none";
		id_deptname.style.display="block";
		td_deptname.style.display="block";
	}
}
</Script>
<html:html clazz="main">
<form name="template" method="post" action="perPathCGAnaly.jspviewhigh">
      <html:message/>
      <html:title clazz='module'>医院病种临床路径成本分析</html:title>
      <html:table clazz="simple">
      <tr>
			
			      <td  nowrap class="signText">核算月:
			      </td>
			      <td  nowrap class="signText"><%= new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month",request.getParameter("year_month"))%>
			      </td>
			      <td nowrap class="signText">范围选择:</td>
	          <td nowrap class="signText">
	          	<select name="checksign" size="1" onchange="checkdept()">
		            <%String check=request.getParameter("checksign");%>
		            <option value="W"<%if((check!=null&&check.equals("W"))||check==null) out.print("selected");%>>全院</option>
		            <option value="D"<%if(check!=null&&check.equals("D")) out.print("selected");%>>科室</option>
	            </select>
       </td>
      </tr>
        <tr>
            <td  nowrap class="signText">病种:</td>
            <td  nowrap class="signText">
          	<select  class="selectBg" multiple="multiple" name="leave_disease_code" size="4" >
             <%String[][] name=(String[][])request.getAttribute("dict_disease");
               String[] value=request.getParameterValues("leave_disease_code");%>
            <option value="" <%if(value==null||(value.length==1&&value[0].equals(""))||(value.length>1&&value[0].equals(""))){
                out.print("selected");}%>>--不限--</option >
            <%
             if (name!=null){for(int i=0;i<name.length;i++){%>
              <option value="<%=name[i][0]%>"
                <%if(value!=null){
                  for(int j=0;j<value.length;j++){
                    if(name[i][0].equals(value[j]))
                      out.print("selected");
                  }
                }%>>
                       <%=name[i][0]+":"+name[i][1]%>
              </option>
              <%}}%>
            </select ></td>
            <td class="signText">DRGs分组:</td>
            <td class="signText"><select  class="selectBg" name="leave_disease_model" size="4" multiple="multiple">
             <%String [][] type=(String[][])request.getAttribute("dict_disease_model");
             String[] tvalue=request.getParameterValues("leave_disease_model"); %>
             <option value="" <%if(tvalue==null||(tvalue.length==1&&tvalue[0].equals(""))||(tvalue.length>1&&tvalue[0].equals(""))){
                out.print("selected");}%>>--不限--</option >
             <%if(name!=null){for(int i=0;i<type.length;i++){%>
              <option value="<%=type[i][0]%>"
                <%if(tvalue!=null){for(int j=0;j<tvalue.length;j++){
                  if(type[i][0].equals(tvalue[j])) out.print("selected");}}%>>
                  <%=type[i][0]+":"+type[i][1]%>
                  </option><%}}%>
              </select>
              </td>
        </tr>
        <tr>
        		<td class="signText" id="id_deptname">科室名称:</td>
          	<td class="signText" id="td_deptname"><select  class="selectBg" name="dept" size="4" multiple="multiple">

            <%String[][] dept=(String[][])request.getAttribute("dept");
            String[] dval=request.getParameterValues("dept");%>
            <option value="" <%if(dval==null||(dval.length==1&&dval[0].equals(""))||(dval.length>1&&dval[0].equals(""))){
              out.print("selected");}%>>--不限--</option >
            <%
            if(dept!=null){
              for(int i=0;i<dept.length;i++){%>
            <option value="<%=dept[i][0]%>"
              <%if(dval!=null){for(int j=0;j<dval.length;j++){
                if(dept[i][0].equals(dval[j])){
                out.print("selected");
                }}}%>>
                <%=dept[i][1]%></option>
            <%}}%>
            </select>
            <td id="p1"/><td id='p2'/><td id='p3'/>
          <td>
          <button class="pageBtn" name="" onclick="find();"  >查询</button>
         <!-- <img src="images/find.gif" class="mouse" onclick="find();">--></td>
        </tr>

</html:table>
    <html:table clazz="simple"><tr><td><html:title clazz='table'>医院病种临床路径病种成本分析</html:title></td></tr></html:table>
<%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro != null) {
      	String dcheck=request.getParameter("checksign");
        TableMarge oper = new TableMarge(ro, "return find()");
%>

<html:table clazz="simple"><tr><td><%=oper%></td></tr></html:table>
     <html:table clazz="complex">

<tr>
        <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td nowrap rowspan="2" class="resultLabel"  >时间</td>
          <td nowrap rowspan="2" class="resultLabel" >病种名称</td>
          <td nowrap rowspan="2" class="resultLabel" >DRGs分组</td>
       		<%if(dcheck!=null&&dcheck.equals("D")){%>
					<td nowrap rowspan="2" class="resultLabel" >科室名称</td>
					<%}%>          
          <td nowrap colspan="3" class="resultLabel" >实际临床路径</td>
          <td nowrap colspan="3" class="resultLabel" >标准临床路径</td>
          <td nowrap colspan="3" class="resultLabel" >差异</td>
        </html:tr>
                 <html:tr clazz='label'>
                  <td   nowrap class="resultLabel" ><div align="center">收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">收益</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">收益</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">收益</div></td>
                </html:tr>



        <%
          String[][] result = ro.getTableResult();
          DecimalFormat mf=new DecimalFormat("#,##0.00");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

       <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText"><%=result[ i ][ 0 ]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 1 ]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 2 ]%></td>
       		<%if(dcheck!=null&&dcheck.equals("D")){%>
          <td nowrap class="numberText"><%=result[ i ][ 3 ]%></td>
					<%}%>  
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 4]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 5]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 6]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 7]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 8]))%></td>          
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 9]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 10]))%></td>    		
          <td nowrap class="numberText"><a href="perPathCGAnaly.jspviewhigh?subFunction=findsub1&leave_disease_code=<%= result[i][13]%>&leave_disease_model=<%= result[i][2]%>&yearmonth=<%= result[i][0]%>"><%=mf.format(Double.parseDouble(result[ i ][ 11]))%></a></td> 
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 12]))%></td>
        </tr>
        <%
              }
            }
        %>
       </html:table>
         </td>
         </tr>

      </html:table>
<%}%>
<input type="hidden" name="subFunction" >
</form>
</html:html>
<script>
checkdept();
</script>