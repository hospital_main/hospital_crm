<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/analyse/perpathcostcomp/pathCGAnalysub2.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.util.Preference,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">

</Script>
<html:html clazz="main">
<form name="template" method="post" action="perPathCGAnaly.jspviewhigh">
      <html:message/>
      <html:title clazz='module'>病人项目成本列表  </html:title>
      <html:table clazz="simple">
      
			<%
			 String []note = (String[])session.getAttribute("note2");
			 if  (note!=null  ){
			%>
			     <tr> <td  nowrap class="signText">年月：<%=note[0]%> </td></tr>
			     <tr> <td  nowrap class="signText">病种：<%=note[1]%></td></tr>
			     <tr> <td  nowrap class="signText">DRG描述：<%=note[2]%></td> </tr>
			     <tr> <td  nowrap class="signText">医疗项目：<%=note[3]%></td> </tr>
			<%}%>

</html:table>
    <html:table clazz="simple"><tr><td><html:title clazz='table'>病人项目成本列表</html:title></td></tr></html:table>
<%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro != null) {
      TableMarge oper = new TableMarge(ro, "return find()");
%>
<html:table clazz="simple"><tr><td><%=oper%></td></tr></html:table>
     <html:table clazz="complex">

<tr>
        <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
		    
                  <td   nowrap class="resultLabel" ><div align="center">年月</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">科室</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">病历号</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">姓名</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">年龄</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">性别</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">单位收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">单位成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">数量</div></td>   
                  <td   nowrap class="resultLabel" ><div align="center">收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">成本</div></td>        
        </html:tr>
        <%
          String[][] result = ro.getTableResult();
          DecimalFormat mf=new DecimalFormat("#,##0.00");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

       <tr CLASS="<%=rowColor%>">
           <td nowrap class="normalText"><%=result[i][0]%>
           <td nowrap class="normalText"><%=result[i][1]%>
           <td nowrap class="normalText"><%=result[i][2]%>
           <td nowrap class="normalText"><%=result[i][3]%>
           <td nowrap class="normalText"><%=result[i][4]%>
           <td nowrap class="normalText"><%=result[i][5]%>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 6 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 7 ]))%></td> 
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 8 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 9 ]))%></td> 
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 10 ]))%></td>
          
          
          
        </tr>
        <%
              }
            }
        %>
       </html:table>
         </td>
         </tr>

      </html:table>
<%}%>
<input type="hidden" name="subFunction" >
</form>
</html:html>
<script>
checkdept();
</script>