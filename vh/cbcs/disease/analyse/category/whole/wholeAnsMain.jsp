<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/analyse/category/whole/wholeAnsMain.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,java.text.*,com.viewhigh.cbcs.base.util.Preference" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
function find(){
	template.subFunction.value="findAllw";
	show_wait();
	template.submit();
}

</Script>
<html:html clazz="main">
<form name="template" method="post" action="catewhole.jspviewhigh">
      <html:message/>
      <html:title clazz='module'>全院病种总收益分析(医成本D1-01表)</html:title>
      <html:table clazz="simple">
    	<tr>
    	  	<td nowrap class="signText">核算月：</td>
        	<td nowrap class="signText">
            <%= new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month",request.getParameter("year_month"))%>
        	</td>
          <td nowrap class="signText">就诊类型:</td>
          <td nowrap class="signText">
            <select name="checksign" size="1">
                <%String check=request.getParameter("checksign");%>
                <option value="O"<%if((check!=null&&check.equals("O"))||check==null) out.print("selected");%>>门诊</option>
                <option value="I"<%if(check!=null&&check.equals("I")) out.print("selected");%>>住院</option>
            </select>
          </td>
      </tr>
      <tr>
              <td class="signText">病种:</td>
              <td class="signText">
                <select  class="selectBg" multiple="multiple" name="leave_disease_code" size="4" >
               <%String[][] name=(String[][])request.getAttribute("d_name");
                 String[] value=request.getParameterValues("leave_disease_code");%>
              <option value="" <%if(value==null||(value.length==1&&value[0].equals(""))||(value.length>1&&value[0].equals(""))){
                  out.print("selected");}%>>--不限--</option ><%
               if (name!=null){for(int i=0;i<name.length;i++){%>
                <option value="<%=name[i][0]%>"
                  <%if(value!=null){
                    for(int j=0;j<value.length;j++){
                      if(name[i][0].equals(value[j]))
                        out.print("selected");
                    }
                  }%>><%=name[i][0]+":"+name[i][1]%></option>
                <%}}%>
              </select >
              </td>
              <td class="signText">DRGs分组:</td>
              <td class="signText">
                <select  class="selectBg" name="leave_disease_model" size="4" multiple="multiple">
               <%String [][] type=(String[][])request.getAttribute("d_type");
               String[] tvalue=request.getParameterValues("leave_disease_model");%>
               <option value="" <%if(tvalue==null||(tvalue.length==1&&tvalue[0].equals(""))||(tvalue.length>1&&tvalue[0].equals(""))){
                  out.print("selected");}%>>--不限--</option >
               <%
               if(name!=null){for(int i=0;i<type.length;i++){%>
                <option value="<%=type[i][0]%>"
                  <%if(tvalue!=null){for(int j=0;j<tvalue.length;j++){
                    if(type[i][0].equals(tvalue[j])) out.print("selected");}}%>>
                    <%=type[i][0]+":"+type[i][1]%></option><%}}%>
                </select>
                </td>
        </tr>
        <tr>
              <td>&nbsp;</td>
          	  <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>
              <button class="pageBtn" name=""  onclick="find();" >查询</button>
             <!-- <img src="images/find.gif" class="mouse" onclick="find();">--></td>
        </tr>

</html:table>
			<%
      BaseRO ro = (BaseRO)request.getAttribute("BaseRO");
      if (ro != null) {
         TableMarge oper = new TableMarge(ro, "return find()");
			%>

<html:table clazz="simple"><tr><td><html:title clazz='table'>全院病种总收益分析表</html:title></td></tr><tr><td><%=oper%></td></tr></html:table>
     <html:table clazz="complex">

       <tr>
        <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td nowrap rowspan="2" class="resultLabel"  >时间</td>
          <td nowrap rowspan="2" class="resultLabel" >病种名称</td>
          <td nowrap rowspan="2" class="resultLabel" >DRGs分组</td>
          <td nowrap colspan="3" class="resultLabel" >病种总收益</td>
          <td nowrap colspan="3" class="resultLabel" >病种医疗收益</td>
                            <%if(Preference.isDiffDrugCost){%>
          <td nowrap colspan="9" class="resultLabel" >病种药品收益</td>
          <%}else{%>
          <td nowrap colspan="3" class="resultLabel" >病种药品收益</td>
          <%}%>
        </html:tr>
                 <html:tr clazz='label'>
                  <td   nowrap class="resultLabel" ><div align="center">总收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">总成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">总收益</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">收益</div></td>
                  <%if(Preference.isDiffDrugCost){%>
                  <td   nowrap class="resultLabel" ><div align="center">西药收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">西药成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">西药收益</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">中成药收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">中成药成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">中成药收益</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">中草药收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">中草药成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">中草药收益</div></td>
                  <%}else{%>
                  <td   nowrap class="resultLabel" ><div align="center">收入</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">成本</div></td>
                  <td   nowrap class="resultLabel" ><div align="center">收益</div></td>
                  <%}%>
                </html:tr>



        <%
          String[][] result = ro.getTableResult();
          DecimalFormat mf=new DecimalFormat("#,##0.00");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText"><%=primaryKey%></td>
          <td nowrap class="normalText"><%=result[ i ][ 1]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 25 ]%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 5 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 4]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 3 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 7]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 8]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 6]))%></td>
          <%if(Preference.isDiffDrugCost){%>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 16]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 18]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 17 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 19]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 21]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 20]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 22]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 24]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 23]))%></td>
          <%}else{%>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 10]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 11]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][9]))%></td>
          <%}%>
        </tr>

        <%
              }
            }
        %>
       </html:table>
         </td>
         </tr>

      </html:table>
<%}%>
<input type="hidden" name="subFunction" >
</form>
</html:html>
