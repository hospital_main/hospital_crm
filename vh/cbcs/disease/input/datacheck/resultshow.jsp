<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/input/datacheck/resultshow.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:58:00 $
  $Modtime: 03-09-22 0:50 $
  $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent"%>

<%@taglib uri="/WEB-INF/taglib/viewhigh-html.tld"prefix="html"%>
<script language="javascript">
function back(){
  template.submit();}</script>

<html:html clazz="main">
    <form name="template" method="post" action="DiseaseInputTrans.jspviewhigh?subFunction=show">
      <html:title clazz="module">病人收入数据错误报告<html:message/></html:title>
	      <html:table clazz="simple">

        </html:table>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
            <td nowrap="nowrap">行号</td>
            <td nowrap="nowrap">记录内容(年月;就诊类型;姓名;科室;入院病种;病种分型;出院病种;病种分型;)</td>
            <td nowrap="nowrap">错误说明</td>
	        </html:tr>
          <%String [][] errdata=(String[][])request.getAttribute("result");
          String str="hehe";
          if(errdata!=null){
            for(int j=0;j<errdata.length;j++){
              if(errdata[j][2]!=null&&!errdata[j][2].equals("此行为空行")){%>
          <tr>
          <td><%=errdata[j][0]%></td>
          <td><%=(errdata[j][1].replaceAll(";@",""))%></td>
          <td><%=errdata[j][2]%></td>
          </tr>
          <%}else{ if(errdata[j][2]!=null&&errdata[j][2].equals("此行为空行")){
            str="注意：数据表中存在空数据行！";}
           }}%>
           <%if(!str.equals("hehe")){%>
          <tr>
          <td></td>
          <td></td>
          <td><%=str%></td>
          </tr>
        <%}}%>
	      </html:table>
</form>

 </html:html>
