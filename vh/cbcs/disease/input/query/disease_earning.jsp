<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/input/query/disease_earning.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
  DecimalFormat nf = new DecimalFormat("#,##0.00");
%>
<Script Language="JavaScript">
function create() {
		template.subFunction.value='preparedCreate';
		show_wait();
    template.submit();
		return true;
	}

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
  		if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
  		  flag = true;
  	}

    if( flag!=false) {
    	if (confirm('是否删除')) {
	      template.subFunction.value='remove';
	      show_wait();
	      template.submit();
	      return true;
	    } else
	    	return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectedAll(){
    for (var i=0; i<template.elements.length; i++) {
    	if (template.elements[i].name == 'primaryKey'
        && template.elements[i].disabled == false) {
          template.elements[i].checked = true;
    	}
    }
  }



	var idx;
	var idx1;
	var idx2;
	var idx3;
	var idx4;
	var idx5;
	var idx6;

	function dis(){
  	if(template.error.checked==true){
			idx=template.hospitalize_id.selectedIndex;
			idx1=template.leave_if.selectedIndex;
			idx2=template.age.value;
			idx3=template.first_disease_code.selectedIndex;
			idx4=template.first_disease_model.selectedIndex;
			idx5=template.second_disease_code.selectedIndex;
			idx6=template.second_disease_model.selectedIndex;
			template.age.value='';
			template.leave_if.options[0].selected=true;
			template.first_disease_code.options[0].selected=true;
			template.first_disease_model.options[0].selected=true;
			template.second_disease_code.options[0].selected=true;
			template.second_disease_model.options[0].selected=true;
			template.leave_if.disabled=true;
			template.age.disabled=true;
			template.first_disease_code.disabled=true;
			template.first_disease_model.disabled=true;
			template.second_disease_code.disabled=true;
			template.second_disease_model.disabled=true;
			template.hospitalize_id.disabled=true;
    }else{
      if(idx!=null&&idx!=1){
      	template.leave_if.disabled=false;
			}
			template.age.disabled=false;
			template.first_disease_code.disabled=false;
			template.first_disease_model.disabled=false;
			if(idx1!=2){
				template.second_disease_code.disabled=false;
				template.second_disease_model.disabled=false;
			}
			if(idx!=null&&idx==1&&idx1!=null&&idx1==2){
				template.second_disease_code.disabled=false;
				template.second_disease_model.disabled=false;
			}
			template.hospitalize_id.disabled=false;
			if(idx!=null&&idx1!=null&&idx2!=null&&idx3!=null&&idx4!=null&&idx5!=null&&idx6!=null){
				template.hospitalize_id.options[idx].selected=true;
				template.leave_if.options[idx1].selected=true;
				template.age.value=idx2;
				template.first_disease_code.options[idx3].selected=true;
				template.first_disease_model.options[idx4].selected=true;

				template.second_disease_code.options[idx5].selected=true;
				template.second_disease_model.options[idx6].selected=true;
			}
    }
  }

	function find() {
   if(template.year_month.value==""&&template.error.checked==true){
      alert("请选择核算月");
      return false;
    }
    if(template.age.value!=""){
      if(isNumber(template.age,"+")){
      	if(isDouble(template.age,4,0)){
       		alert("年龄输入错误")
       		return false;
        }
      }
     	else {alert("年龄输入错误");return false;}
    }
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }

  function find1() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
  function hospitalize1(){
		if(template.hospitalize_id.options[1].selected){
			template.leave_if.disabled=true;
			template.first_disease_code.disabled=true;
			template.first_disease_model.disabled=true;
		} else 
		 {	    
		    template.first_disease_code.disabled=false;
			template.first_disease_model.disabled=false;
			template.leave_if.disabled=false;
			if(template.leave_if.options[2].selected){
     			template.second_disease_code.disabled=true;
				template.second_disease_model.disabled=true;
			}
		}
    return false;
  }

  function leave1(){
    if(template.leave_if.options[2].selected){
      template.second_disease_code.disabled=true;
      template.second_disease_model.disabled=true;
    } else {
      template.second_disease_code.disabled=false;
      template.second_disease_model.disabled=false;
    }
    return false;
	}
</Script>

<html:html>
	<form name="template" method="post" onsubmit="return find()" action="disease_earning_query.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>病人收入查询</html:title>

	  <html:table clazz="simple">
	    <tr>
        <td nowrap class="signText">核算月:</td>
        <td nowrap class="signText">
           <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
        </td>
        <td nowrap class="signText">年龄:</td>
        <td nowrap class="signText">
          <% String age = request.getParameter("age");%>
         <input type="text" class="textInputB" name="age" <%if(age != null){ out.println(" value=" + age);}%>>
        </td>
      </tr>
      <tr>
	      <td nowrap class="signText">就诊类型:</td>
        <td nowrap class="signText">
           <%String[][] hospitalize_id={{"O","门诊"},
                                        {"I","住院"}
                                       }; %>
           <%Select hospitalize=new Select(hospitalize_id, "hospitalize_id", request.getParameter("hospitalize_id"), false, false);
             hospitalize.setAttribute("onchange","hospitalize1()");
           %>
           <%=hospitalize%>
	      </td>
        <td nowrap class="signText">是否出院：</td>
        <td nowrap class="signText">
           <%String[][] leave_if={{"Y","已经出院"},
                                 {"N","未出院"},
                                 {"M","出院未结算"}}; %>
           <%Select leave=new Select(leave_if, "leave_if", request.getParameter("leave_if"), false, false);
           leave.setAttribute("onchange","leave1()");
           %>
           <%=leave%>
	      </td>
      </tr>
      <tr>
        <td nowrap class="signText">入院诊断:</td>
        <td nowrap class="signText">
          <%=new Select(request.getAttribute("first_disease_code"), "first_disease_code", request.getParameter("first_disease_code"), false, false)%>
        </td>
        <td nowrap class="signText">入院分型：</td>
        <td nowrap class="signText">
            <%=new Select(request.getAttribute("first_disease_model"), "first_disease_model", request.getParameter("first_disease_model"), false, false)%>
	      </td>
      </tr>
      <tr>
        <td nowrap class="signText">出院诊断：</td>
        <td nowrap class="signText">
          <%=new Select(request.getAttribute("second_disease_code"), "second_disease_code", request.getParameter("second_disease_code"), false, false)%>
	      </td>
        <td nowrap class="signText">出院分型：</td>
        <td nowrap class="signText">
           <%=new Select(request.getAttribute("second_disease_model"), "second_disease_model", request.getParameter("second_disease_model"), false, false)%>
	      </td>
       </tr>
       <tr>
         <td nowrap class="signText">未出院没结算:</td>
        <td nowrap class="signText">

          <%String error=request.getParameter("error");%>
           <input type="checkbox" onclick="dis()" name="error" value="Y" <% if(error!=null) out.print("checked");%>>
        </td>
        <td>&nbsp;</td>
	      <td nowrap class="signText">
	      <button class="pageBtn" name="" onclick="return find();"  >查询</button>
	      <!--<img src="images/find.gif" class="mouse" onclick="return find();" />-->
	    </tr>
	  </html:table>

	  <br>
		<html:title clazz='table'>病人收入月结汇总表</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
       <%
           BaseRO ro = (BaseRO)request.getAttribute("baseRO");
           TableMarge oper = new TableMarge(ro, "return find1()");
           oper.addOptionButton("images/selectedAll.gif", "return selectedAll()");   // 全选
           oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
           oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
           oper.addNeedButton("images/create.gif", "return create()");     //  添加
           String[][] result=ro.getTableResult();
        %>
       <tr><td><%=oper%></td></tr>
		  <!-- 结果集 -->
		  <tr>
		    <td>
          <html:table clazz="result">
		        <html:tr clazz='label'>
             <td nowrap >选择</td>
		          <td nowrap >年月</td>
              <td nowrap >就诊类型</td>
              <td nowrap >病历号</td>
              <td nowrap >姓名</td>
              <td nowrap >年龄</td>
              <td nowrap >性别</td>
              <td nowrap >就诊科室</td>
              <td nowrap >是否出院</td>
              <td nowrap >就诊时间</td>
              <td nowrap >出院时间</td>
              <td nowrap >入院诊断</td>
              <td nowrap >入院分型</td>
              <td nowrap >出院诊断</td>
              <td nowrap >出院分型</td>
              <td nowrap >住院次数</td>
              <td nowrap >住院天数</td>
              <td nowrap >医疗收入</td>
<%if(com.viewhigh.cbcs.base.util.Preference.isDiffDrugCost()){%>
              <td nowrap >西药收入</td>
              <td nowrap >中成药收入</td>
              <td nowrap >中草药收入</td>
              <%}else{%>
              <td nowrap >药品收入</td>
              <%}%>
		        </html:tr>

		        <%
		          if ( result != null ){
		            for (int i = 0; i < result.length; i++ ){
		               String rowColor = "rowGray";
		               if (i/2*2==i) rowColor = "rowWhite";
                   boolean isCheckout =
                  Boolean.valueOf(result[i][22]).booleanValue();
		        %>
		        <tr CLASS="<%=rowColor%>">
              <td><input type="checkbox" name="primaryKey" value="<%=result[i][20]%>" <%if(isCheckout){out.print(" disabled ");}%>></td>
               <td nowrap="nowrap" class="numberText">
                 <%if(!isCheckout) {%>
                 <a href="disease_earning_query.jspviewhigh?subFunction=presave&primaryKey=<%=result[i][20]%>&hospitalize_id=<%=result[i][21].trim().equals("住院")?"I":"O"%>&first_time=<%=result[i][8]%>&leave_time=<%=result[i][9]%>"><%=result[i][0]%></a>
                 <%}else{%>
                 <%=result[i][0]%>
                 <%}%>
                 </td>

                <% for(int j=1;j<16;j++){%>
                  <td nowrap class="normalText"><%=result[ i ][ j ]%></td>
		           <%}%>
               <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 16 ]))%></td>
               <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 17 ]))%></td>
               <%if(com.viewhigh.cbcs.base.util.Preference.isDiffDrugCost()){%>
               <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 18 ]))%></td>
               <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 19 ]))%></td>
               <%}%>
		        </tr>
		        <%
		              }
		            }
		        %>
		      </html:table>
		    </td>
		  </tr>
	  </html:table>

	  <input type=hidden name="subFunction"/>
	</form>
<script language="javascript">
 <%String hospital=request.getParameter("hospitalize_id");
  String leave=request.getParameter("leave_if");
  String e=request.getParameter("error");
  %>
function init(){
  <%if(hospital!=null&&hospital.equals("O")){%>
    template.leave_if.disabled=true;
   	template.first_disease_code.disabled=true;
	template.first_disease_model.disabled=true;  
  <%}%>
  <%if(leave!=null&&leave.equals("N")){%>
   template.second_disease_code.disabled=true;
   template.second_disease_model.disabled=true;
  <%}%>
   <%if(e!=null){%>
   template.second_disease_code.disabled=true;
   template.second_disease_model.disabled=true;

  <%}%>
}
dis();
init();

</script>
</html:html>



