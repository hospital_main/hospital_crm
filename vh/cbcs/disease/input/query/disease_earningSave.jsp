<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/input/query/disease_earningSave.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.util.Preference,
                com.viewhigh.cbcs.base.mvc.view.BiDateComponent" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<%
  DecimalFormat nf = new DecimalFormat("###0.00");
%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

function dosome(){
  if(template.hospitalize_id.value=='I')
  {			template.in_order.disabled=false;
        template.in_day.disabled=false;
        template.leave_if.disabled=false;
        template.first_disease_code.disabled=false;
        template.first_disease_model.disabled=false;
        hide1_11522429707162307.style.display='block'

//        template.in_order.value='';
//        template.in_day.value='';
    }
    if(template.hospitalize_id.value=='O'){
      	template.in_order.value='0';
        template.in_day.value='0';
      	template.in_order.disabled=true;
        template.in_day.disabled=true;
        template.leave_if.disabled=true;
        template.first_disease_code.disabled=true;
        template.first_disease_model.disabled=true;
        hide0_11522429707162307.style.display='block'
      }
  }
	function save() {
    if(trim(template.year_month.value)=='')
    {
      alert('请选择核算月!')
      return
    }
    if(isEmpty(template.case_id))
    {
      alert('请填写病历号!')
      return
    }
    if(isEmpty(template.name) )
   {
      alert('请填写姓名!')
      return
   }
       if(isEmpty(template.age))
    {
      alert('请填写年龄!')
      return
    }
    if(isEmpty(template.in_order))
   {
      alert('请填写住院次数!')
      return
   }
       if(isEmpty(template.in_day))
   {
      alert('请填写住院天数!')
      return
   }
   if(trim(template.first_time.value)=='')
    {
      alert('请选择就诊时间!')
      return
    }
   if(template.leave_if.value=="N"){}
   else{
    if(trim(template.leave_time.value)=='')
    {
      alert('请选择出院时间!')
      return
    }
   }
     if(template.hospitalize_id.value=='I')
  {	 var leave_time=template.leave_time.value.substring(0,7);
    var first_time=template.first_time.value.substring(0,7);
    var year_month=template.year_month.value;
    var re = /-/g;
    first_time = first_time.replace(re, '');
    leave_time = leave_time.replace(re, '');
    if(year_month<first_time){
      alert('就诊时间不能大于核算月!')
      return }
    if(template.leave_if.value=="N"){}
    else{
      if(template.leave_time.value<template.first_time.value)  {
        alert('出院时间要大于就诊时间!')
        return
      }
    }
  }
    if(template.hospitalize_id.value=='O'){
      	 var leave_time=template.leave_time.value;
    var first_time=template.first_time.value;
    var year_month=template.year_month.value;
    var first_time1=template.first_time.value.substring(0,7);
    var leave_time1=template.leave_time.value.substring(0,7);
    var re = /-/g;
    first_time1 = first_time1.replace(re, '');
    leave_time1 = leave_time1.replace(re, '');
    if(year_month<first_time1){
      alert('就诊时间不能大于核算月!')
      return }
    if(leave_time<first_time)  {
      alert('出院时间要大于就诊时间!')
      return }
      }
    switch(isDouble(template.treat_income,10,2))
    {
      case 0 : alert('医疗收入必须为数字型'); return;
      case 1 : alert('医疗收入整数部分不能高于10个字符'); return;
      case 2 : alert('医疗收入没有整数部分'); return;
      case 3 : alert('医疗收入小数部分不能高于2个字符'); return;
    }
 <%if (Preference.isDiffDrugCost()) {%>
    switch(isDouble(template.medica_income,10,2))
    {
      case 0 : alert('西药收入必须为数字型'); return;
      case 1 : alert('西药收入整数部分不能高于10个字符'); return;
      case 2 : alert('西药收入没有整数部分'); return;
      case 3 : alert('西药收入小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.med_pro_income,10,2))
    {
      case 0 : alert('中成药收入必须为数字型'); return;
      case 1 : alert('中成药收入整数部分不能高于10个字符'); return;
      case 2 : alert('中成药收入没有整数部分'); return;
      case 3 : alert('中成药收入小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.med_grass_income,10,2))
    {
      case 0 : alert('中草药收入必须为数字型'); return;
      case 1 : alert('中草药收入整数部分不能高于10个字符'); return;
      case 2 : alert('中草药收入没有整数部分'); return;
      case 3 : alert('中草药收入小数部分不能高于2个字符'); return;
    }
    <%}else{%>
    switch(isDouble(template.medica_income,10,2))
    {
      case 0 : alert('药品收入必须为数字型'); return;
      case 1 : alert('药品收入整数部分不能高于10个字符'); return;
      case 2 : alert('药品收入没有整数部分'); return;
      case 3 : alert('药品收入小数部分不能高于2个字符'); return;
    }
    <%}%>
		template.subFunction.value='save';
		show_wait();
    template.submit();
		return true;
	}
function reset1( ){
template.reset()
template.year_month.value=""
}
function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="disease_earning_query.jspviewhigh">
     <!-- 信息提示栏 -->
	  <html:message/>

     <!--信息栏-->
	  <html:title clazz='module'>病人收入修改页面</html:title>

     <!-- 简单信息 -->
	  <html:table clazz="simple">
     <%String[][] result=(String[][])request.getAttribute("result");%>
     <tr>
      <td nowrap class="signText">核算月:
      </td>
      <td nowrap class="signText">
           <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month")==null? result[0][0]:request.getParameter("year_month"))%>
      </td>
      <td nowrap class="signText">就诊类型:
      </td>
      <td nowrap class="signText">
       <script language='javascript'>
     var all_hide_id11522429707162307=new Array('hide0_11522429707162307','hide1_11522429707162307');
   </script>
   <select name="hospitalize_id" class="selectBg" onchange="hidden(this,all_hide_id11522429707162307);dosome()" >
    <option value="O" <%if((result[0][1]!=null&&result[0][1].equals("O"))||request.getParameter("hospitalize_id").equals("O")) {out.print("selected='selected'");}%>>门诊</option>
    <option value="I"  <%if((result[0][1]!=null&&result[0][1].equals("I"))||request.getParameter("hospitalize_id").equals("I")) {out.print("selected='selected'");}%>>住院</option>
   </select>
      </td>
     </tr>
     <tr>

      <td nowrap class="signText">病历号:
      </td>
      <td nowrap class="signText">
           <%String case_id=request.getParameter("case_id");%>
           <input type="text" class="textInputB" name="case_id"
           <%if(case_id != null){ out.println(" value=" + case_id);
           }else{
           out.println(" value=" + result[0][2]);
           }%>>
      </td>
      <td  nowrap class="signText">科室:
      </td>
      <td>
       <span  id='hide0_11522429707162307' style='display:none;'  >
<%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("Odept_code"), "Odept_code", request.getParameter("Odept_code")==null? result[0][6]:request.getParameter("Odept_code"), true, true)%>
       </span>
       <span id='hide1_11522429707162307' style='display:none;'  >
<%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("Idept_code"), "Idept_code",request.getParameter("Idept_code")==null?  result[0][6]:request.getParameter("Idept_code"), true, true)%>
       </span>
       </td>
     </tr>
     <tr>
      <td nowrap class="signText">姓名:
      </td>
      <td nowrap class="signText">
          <% String name = request.getParameter("name");%>
         <input type="text" class="textInputB" name="name" <%if(name != null){ out.println(" value=" + name);}else{ out.println(" value=" + result[0][3]);}%>>
      </td>
      <td nowrap class="signText">年龄:
      </td>
      <td nowrap class="signText">
          <% String age = request.getParameter("age");%>
         <input type="text" class="textInputB" name="age" <%if(age != null){ out.println(" value=" + age);}else{ out.println(" value=" + result[0][4]);}%>>
           性别:
          <%String[][] sex={{"M","男"},
                                 {"F","女"},
                                 }; %>
           <%Select sex1ist=new Select(sex, "sex", request.getParameter("sex")==null?result[0][5]:request.getParameter("sex"), false, true);%>
           <%=sex1ist%>
      </td>
    </tr>
    <tr>
      <td nowrap class="signText">住院次数:</td>
      <td nowrap class="signText">
      <% String in_order = request.getParameter("in_order");%>
      <input type="text" class="textInputB" name="in_order" <%if(in_order != null){ out.println(" value=" + in_order);}else{ out.println(" value=" + result[0][14]);}%>>
      </td>
      <td nowrap class="signText">住院天数:</td>
      <td nowrap class="signText">
      <% String in_day = request.getParameter("in_day");%>
      <input type="text" class="textInputB" name="in_day" <%if(in_day != null){ out.println(" value=" + in_day);}else{ out.println(" value=" + result[0][15]);}%>>
      </td>
    </tr>
    <tr>
       <td nowrap class="signText">是否出院：
       </td>
       <td nowrap class="signText">
           <%String[][] leave_if={{"Y","已经出院"},
                                 {"N","未出院"},
                                 {"M","出院未结算"}}; %>
           <%Select leave=new Select(leave_if, "leave_if", request.getParameter("leave_if")==null? result[0][7]:request.getParameter("leave_if"), false, true);
           %>
           <%=leave%>
	     </td>
     </tr>
     <tr>
        <td nowrap class="signText">就诊时间:</td>
        <td nowrap class="signText">
          <input:date name="first_time"></input:date>
        </td>
        <td nowrap class="signText">出院时间：</td>
        <td nowrap class="signText">
          <input:date name="leave_time"></input:date>
	      </td>
     </tr>
     <tr>
        <td nowrap class="signText">入院诊断:</td>
        <td nowrap class="signText">
          <%=new Select(request.getAttribute("first_disease_code"), "first_disease_code", request.getParameter("first_disease_code")==null? result[0][10]:request.getParameter("first_disease_code"), false, true)%>
        </td>
        <td nowrap class="signText">入院分型：</td>
        <td nowrap class="signText">
            <%=new Select(request.getAttribute("first_disease_model"), "first_disease_model", request.getParameter("first_disease_model")==null? result[0][11]:request.getParameter("first_disease_model"), false, true)%>
	      </td>
     </tr>
     <tr>
        <td nowrap class="signText">出院诊断：</td>
        <td nowrap class="signText">
          <%=new Select(request.getAttribute("leave_disease_code"), "leave_disease_code", request.getParameter("leave_disease_code")==null? result[0][12]:request.getParameter("leave_disease_code"), false, true)%>
	      </td>
        <td nowrap class="signText">出院分型：</td>
        <td nowrap class="signText">
           <%=new Select(request.getAttribute("leave_disease_model"), "leave_disease_model", request.getParameter("leave_disease_model")==null? result[0][13]:request.getParameter("leave_disease_model"), false, true)%>
	      </td>
     </tr>
     <tr>
        <td nowrap class="signText">医疗收入:</td>
        <td nowrap class="signText">
      <% String treat_income = request.getParameter("treat_income");%>
        <input type="text" class="textInputB" name="treat_income" <%if(treat_income != null){ out.println(" value=" + treat_income);}else{out.println(" value=" + nf.format(Double.parseDouble(result[0][16])));}%>>
        </td>
 <%if (Preference.isDiffDrugCost()) {%>
        <td nowrap class="signText">西药收入:</td>
        <td nowrap class="signText">
      <% String medica_income = request.getParameter("medica_income");%>
        <input type="text" class="textInputB" name="medica_income" <%if(medica_income != null){ out.println(" value=" + medica_income);}else{out.println(" value=" + nf.format(Double.parseDouble(result[0][17])));}%>>
        </td>
	   </tr>
	   <tr>
        <td nowrap class="signText">中成药收入:</td>
        <td nowrap class="signText">
      <% String med_pro_income = request.getParameter("med_pro_income");%>
        <input type="text" class="textInputB" name="med_pro_income" <%if(med_pro_income != null){ out.println(" value=" + med_pro_income);}else{out.println(" value=" + nf.format(Double.parseDouble(result[0][18])));}%>>
        </td>
        <td nowrap class="signText">中草药收入:</td>
        <td nowrap class="signText">
      <% String med_grass_income = request.getParameter("med_grass_income");%>
        <input type="text" class="textInputB" name="med_grass_income" <%if(med_grass_income != null){ out.println(" value=" + med_grass_income);}else{out.println(" value=" + nf.format(Double.parseDouble(result[0][19])));}%>>
        </td>
 <%}else{%>
        <td nowrap class="signText">药品收入:</td>
        <td nowrap class="signText">
      <% String medica_income = request.getParameter("medica_income");%>
        <input type="text" class="textInputB" name="medica_income" <%if(medica_income != null){ out.println(" value=" + medica_income);}else{out.println(" value=" + nf.format(Double.parseDouble(result[0][17])));}%>>
        </td>

    <%}%>

	   </tr>
     <tr>
      <td colspan="2"> 
      <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset1();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset1();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
	  </html:table>
      <br>
      <input type=hidden name="subFunction"/>
      <input type=hidden name="primaryKey" value="<%=request.getParameter("primaryKey")%>"/>
  	</form>


</html:html>
<script>dosome()</script>


