<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/input/avgdata/inputAvgCreate.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
      if(trim(template.dyear.value)=="")
    	{
    		alert("年度不能为空");
    		return;
    	}
    	if(trim(template.med_code.value)=="")
    	{
    		alert("药品名称不能为空");
    		return;
    	}
    	template.price.value=trim(template.price.value);
    	template.per_cost.value=trim(template.per_cost.value);
    switch(isDouble(template.price,10,4))
    {
      case 0 : alert('单位收入必须为数字型'); return;
      case 1 : alert('单位收入整数部分不能高于10个字符'); return;
      case 2 : alert('单位收入没有整数部分'); return;
      case 3 : alert('单位收入小数部分不能高于4个字符'); return;
    }
    switch(isDouble(template.per_cost,10,4))
    {
      case 0 : alert('单位成本必须为数字型'); return;
      case 1 : alert('单位成本整数部分不能高于10个字符'); return;
      case 2 : alert('单位成本没有整数部分'); return;
      case 3 : alert('单位成本小数部分不能高于4个字符'); return;
    }
        template.subFunction.value='create';
        template.submit();
            show_wait();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_avgdata.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>药品标准收入成本添加页面</html:title>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
  <%  
  	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
  	
    String[][] seconditem=(String[][])request.getAttribute("seconditem");
    String[] names = {"values","texts", "relations"};
    out.println(com.viewhigh.cbcs.base.util.SimpleSearch.dynamicSelect(names, seconditem, "dynamicSelect", request.getParameter("med_code")));

    String[][] firstitem=(String[][])request.getAttribute("firstitem");
    Select super_code = new Select(firstitem, "first", "", true, false);
    
    super_code.setAttribute("onchange", "dynamicSelect(template.first, template.med_code)");	  
  %>
		<tr>
			<td class="signText" nowrap="nowrap">年度：</td>
			<td width="75%" class="signText" nowrap="nowrap">
				<%=new Select(year,"dyear","",false,false)%>
			</td>
    </tr>
  	<tr>
			<td class="signText" nowrap>药品分类:</td>
		  <td class="signText" nowrap>
       <%
	  			out.println(super_code);
    	 %>
			</td>
		</tr>
		<tr>
			<td class="signText" nowrap>药品名称:</td>
			<td class="signText" nowrap>
			<%
				Select seco_code = new Select("med_code");
				out.println(seco_code);
			%>
			</td>
		</tr>
    <tr>
      <td class="signText" nowrap="nowrap">单位收入：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="price" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">单位成本：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="per_cost" class="textInputC" /></td>
    </tr>
    
    
    
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   