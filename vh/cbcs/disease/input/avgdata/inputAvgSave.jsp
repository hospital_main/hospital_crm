<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/input/avgdata/inputAvgSave.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    if(isEmpty(template.dyear))
    {
      alert('年度不能为空!');
      return;
    }
    if(isEmpty(template.med_code))
    {
      alert('药品名称不能为空!');
      return;
    }
    template.price.value=trim(template.price.value);
   	template.per_cost.value=trim(template.per_cost.value);
    switch(isDouble(template.price,10,4))
    {
      case 0 : alert('单位收入必须为数字型'); return;
      case 1 : alert('单位收入整数部分不能高于10个字符'); return;
      case 2 : alert('单位收入没有整数部分'); return;
      case 3 : alert('单位收入小数部分不能高于4个字符'); return;
    }
    switch(isDouble(template.per_cost,10,4))
    {
      case 0 : alert('单位成本必须为数字型'); return;
      case 1 : alert('单位成本整数部分不能高于10个字符'); return;
      case 2 : alert('单位成本没有整数部分'); return;
      case 3 : alert('单位成本小数部分不能高于4个字符'); return;
    }
    template.subFunction.value='save';
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_avgdata.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module"> 药品标准收入成本修改页面</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">年度：</td>
      <td width="75%" class="signText" nowrap="nowrap">
        <input type=text value="<%=result[0]%>" disabled />
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">药品名称：</td>
      <td width="75%" class="signText" nowrap="nowrap">
        <input type=text value="<%=result[2]%>" disabled />
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">单位收入：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="price" value="<%=result[6]%>" class="textInputC">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">单位成本：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="per_cost" value="<%=result[7]%>" class="textInputC">
      </td>
    </tr>

     <tr>
      <td colspan="2">
      <button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button><button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="dyear" value="<%=result[0]%>">
  <input type="hidden" name="med_code" value="<%=result[1]%>">
  <%}%>
</form>


</html:html>
