<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/input/avgdata/inputAvgMain.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.util.DataUtil" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {

        template.subFunction.value='preparedCreate';
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
  function inherit(){
    template.subFunction.value='preparedInherit';
    show_wait();
    template.submit();
    return true;      
  }

  function group() {
    if (template.year_month.value == "") {
      alert("请选择年月！");
      return false;
    }
    }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="dictdisease_avgdata.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>药品标准成本收入定义</html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
	    <tr>
			<%
			String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
			String dyear=request.getParameter("dyear")==null ? "":request.getParameter("dyear");
			%>
              <TD width="19%" noWrap class=signText>年度：
                <%=new Select(year,"dyear",dyear,false,false)%>
              年</TD>	    
	      <td nowrap class="signText">药品编号：</td>
	        <% String med_code = request.getParameter("med_code");%>
	      <td><input type=text name="med_code" class="textInputC" <%if(med_code != null){ out.println(" value=" + med_code);}%>></td>
	      <td nowrap class="signText">药品分类编码：</td>
	      <% String med_type_code = request.getParameter("med_type_code");%>
	      <td><input type=text name="med_type_code" class="textInputC" <%if(med_type_code!= null){ out.println(" value=" + med_type_code);}%>></td>
	   </tr>
	   <tr>
	      <td>  </td>
	      <td nowrap class="signText">药品名称：</td>
	      <% String med_name = request.getParameter("med_name");%>
	      <td><input type=text name="med_name" class="textInputC" <%if(med_name!= null){ out.println(" value=" + med_name);}%>></td>
				<td nowrap class="signText">药品分类名称：</td>
	      <% String med_type_name = request.getParameter("med_type_name");%>
	      <td><input type=text name="med_type_name" class="textInputC" <%if(med_type_name!= null){ out.println(" value=" + med_type_name);}%>></td>

	   
	    </tr>
	    <tr>
	      <td class="signText"></td>
	      <td class="signText"></td>
	  <tr>
	      <td class="signText"></td>
	      <td class="signText"></td>
	      <td class="signText"></td>
	      <td class="signText"></td>
	      <td class="signText">
	      <button class="pageBtn" name=""  onclick="return find();" >查询</button>
	      <button class="pageBtn" onclick="return inherit();" >继承</button>
	      <!--<img src="images/find.gif" class="mouse" onclick="return find();" />
	      <img src="images/inherit.gif" class="mouse" onclick="return inherit();" />--></td>
	    </tr>	
	    </tr>

	  </html:table>

	  <br>

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
	  
  
		<html:title clazz='table'> 药品标准成本收入定义</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>

		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td nowrap="nowrap" class="resultLabel" >选择</td>
		          <td nowrap="nowrap" class="resultLabel" >年度</td>
		          <td nowrap="nowrap" class="resultLabel" >药品编号</td>
		          <td nowrap="nowrap" class="resultLabel" >药品名称</td>
		          <td nowrap="nowrap" class="resultLabel" >药品分类</td>
		          <td nowrap="nowrap" class="resultLabel" >规格型号</td>
		          <td nowrap="nowrap" class="resultLabel" >计量单位</td>
		          <td nowrap="nowrap" class="resultLabel" >单位收入</td>
		          <td nowrap="nowrap" class="resultLabel" >单位成本</td>
		        </html:tr>

		        <%
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String  primaryKey = result[ i ][ 0 ]+" "+result[i][1];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>
 
		        <tr CLASS="<%=rowColor%>">
		          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
		          <td nowrap="nowrap" class="normalText"><a href="dictdisease_avgdata.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>"><%=result[ i ][0 ]%></a></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 4 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 5 ]%></td>
		          <td nowrap="nowrap" class="numberText"><%=DataUtil.changeFormat(result[ i ][ 6 ])%></td>
		          <td nowrap="nowrap" class="numberText"><%=DataUtil.changeFormat(result[ i ][ 7 ])%></td>

		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		<%}%>
	  <input type=hidden name="subFunction"/>
	</form>
</html:html>

