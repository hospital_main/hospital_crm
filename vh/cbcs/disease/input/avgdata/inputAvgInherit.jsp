<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/input/avgdata/inputAvgInherit.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

//继承
 function create(){
     if( template.sdyear.value>=template.ddyear.value )
    {
      alert('目标年度必须大于源年度!');
      return;
    }
        if (!confirm('继承覆盖['+template.ddyear.value+']年数据 ，请确认')) {
         return true;
        }     
        template.subFunction.value='inherit';
        template.submit();
        show_wait();
        return true;
 }
   // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
</Script>
    
    
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_avgdata.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>药品信息复制页面</html:title>
  <table  width="100%" cellspacing="2" border="0" >
  <tr>  
      <TD class="signText" nowrap="nowrap"> 源年度 ：
        <SELECT name ="sdyear" onchange="alert(this.sdyear.value)">
          <OPTION value="">----</OPTION><OPTION value=2000>2000</OPTION>
          <OPTION value=2001>2001</OPTION>
          <OPTION value=2002>2002</OPTION>
          <OPTION value=2003>2003</OPTION>
          <OPTION value=2004 selected>2004</OPTION>
          <OPTION value=2005>2005</OPTION>
          <OPTION value=2006>2006</OPTION>
          <OPTION value=2007>2007</OPTION>
          <OPTION value=2008>2008</OPTION>
          <OPTION value=2009>2009</OPTION>
          <OPTION value=2010>2010</OPTION>
          <OPTION value=2011>2011</OPTION>
          <OPTION value=2012>2012</OPTION>
          <OPTION value=2013>2013</OPTION>
          <OPTION value=2014>2014</OPTION>
          <OPTION value=2015>2015</OPTION>
          <OPTION value=2016>2016</OPTION>
          <OPTION value=2017>2017</OPTION>
          <OPTION value=2018>2018</OPTION>
          <OPTION value=2019>2019</OPTION>
          <OPTION value=2020>2020</OPTION>
        </SELECT>
      年</TD>	              
      <TD  noWrap class=signText  align="right">目标年度：
        <SELECT name='ddyear' >
          <OPTION value="">----</OPTION><OPTION value=2000>2000</OPTION>
          <OPTION value=2001>2001</OPTION>
          <OPTION value=2002>2002</OPTION>
          <OPTION value=2003>2003</OPTION>
          <OPTION value=2004 selected>2004</OPTION>
          <OPTION value=2005>2005</OPTION>
          <OPTION value=2006>2006</OPTION>
          <OPTION value=2007>2007</OPTION>
          <OPTION value=2008>2008</OPTION>
          <OPTION value=2009>2009</OPTION>
          <OPTION value=2010>2010</OPTION>
          <OPTION value=2011>2011</OPTION>
          <OPTION value=2012>2012</OPTION>
          <OPTION value=2013>2013</OPTION>
          <OPTION value=2014>2014</OPTION>
          <OPTION value=2015>2015</OPTION>
          <OPTION value=2016>2016</OPTION>
          <OPTION value=2017>2017</OPTION>
          <OPTION value=2018>2018</OPTION>
          <OPTION value=2019>2019</OPTION>
          <OPTION value=2020>2020</OPTION>
        </SELECT>
      年</TD>	 
    </tr>

  <tr>                      
	<td class="signText" nowrap="nowrap">
		
		<button class="pageBtn" onclick="return create()">确定</button> 
		<button class="pageBtn" onclick="return back(template);">返回</button> 
		<!--<IMG src="images/confirm.gif" width="40" height="17" border="0" class=mouse onclick="return create()"><IMG src="images/return.gif" width="40" height="17" border="0" class=mouse onclick="return back(template)"> -->
	</td>
  </tr>
    <input type=hidden name="subFunction" value="create"/>
  </form>
  </table>
</html:html>  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  