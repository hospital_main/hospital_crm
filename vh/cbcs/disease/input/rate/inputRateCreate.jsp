<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/input/rate/inputRateCreate.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
	    if(trim(template.year_month.value)=="")
	    {
	      alert('年度不能为空!');
	      return;
	    }
	    if(isEmpty(template.med_code))
	    {
	      alert('药品名称不能为空!');
	      return;
	    }
	    template.rate.value=trim(template.rate.value);
	    switch(isDouble(template.rate,12,4))
	    {
	      case 0 : alert('加成率必须为数字型'); return;
	      case 1 : alert('加成率整数部分不能高于12个字符'); return;
	      case 2 : alert('加成率没有整数部分'); return;
	      case 3 : alert('加成率小数部分不能高于4个字符'); return;
	    }
        template.subFunction.value='create';
        template.submit();
            show_wait();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_rate.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>
  <%  
    String[][] seconditem=(String[][])request.getAttribute("seconditem");
    String[] names = {"values","texts", "relations"};
    out.println(com.viewhigh.cbcs.base.util.SimpleSearch.dynamicSelect(names, seconditem, "dynamicSelect", request.getParameter("med_code")));

    String[][] firstitem=(String[][])request.getAttribute("firstitem");
    Select super_code = new Select(firstitem, "first", "", true, false);
    
    super_code.setAttribute("onchange", "dynamicSelect(template.first, template.med_code)");	  
  %>
  <!-- 标题栏 -->
	  <html:title clazz='module'>药品加成率添加页面</html:title>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
	 <TD width="19%" noWrap class=signText>源年月:</td>	    
	   <td nowrap class="signText" align=right>
	   <%=new MonthComponent("year_month",request.getParameter("year_month"))%>
	   </TD>  
	  </tr>
	  <tr>
			<td class="signText" nowrap>药品分类:</td>
		  <td>
       <%
	  			out.println(super_code);
    	 %>
			</td>
		</tr>
		<tr>
			<td class="signText" nowrap>药品名称:</td>
			<td >
			<%
				Select seco_code = new Select("med_code");
				out.println(seco_code);
			%>
			</td>
		</tr>
    <tr>
      <td class="signText" nowrap="nowrap">加成率：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="rate" class="textInputC" /></td>
    </tr>    
    
    
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   