<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/input/rate/inputRateInherit.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

//继承
 function create(){ 
	    if( template.syear_month.value>=template.dyear_month.value )
	    {
	      alert('目标月份必须大于源月份!');
	      return;
	    }
        if (!confirm('继承覆盖该年月数据['+template.dyear_month.value+'] ，请确认')) {
         return true;
        }    
        template.subFunction.value='inherit';
        show_wait();        
        template.submit();

        return true;
 }
   // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
</Script>
    
    
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_rate.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>药品加成率复制页面</html:title>
  <table  width="100%" cellspacing="2" border="0" >
  <tr>  
	   
	 <TD width="19%" noWrap class=signText>源年月:</td>	    
	   <td nowrap class="signText" align=right>
	   <%=new MonthComponent("syear_month",request.getParameter("year_month"))%>
	   </TD>
	 <TD width="19%" noWrap class=signText>目标年月:</td>	    
	   <td nowrap class="signText" align=right>
	   <%=new MonthComponent("dyear_month",request.getParameter("year_month"))%>
	   </TD>
              
    </tr>

  <tr>                      
	<td class="signText" nowrap="nowrap">
		
		<button class="pageBtn" onclick="return create()">确定</button> 
		<button class="pageBtn" onclick="return back(template);">返回</button> 
		<!--<IMG src="images/confirm.gif" width="40" height="17" border="0" class=mouse onclick="return create()"><IMG src="images/return.gif" width="40" height="17" border="0" class=mouse onclick="return back(template)"> -->
	</td>
  </tr>
    <input type=hidden name="subFunction" value="create"/>
  </form>
  </table>
</html:html>  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  