<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/cost/person/balanceDetail.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,java.text.*,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<html:html clazz="main">
<script>
  function find() {
    template.subFunction.value='balance_detail';
    show_wait();
    template.submit();
    return true;
  }
  function back( element )
  {
  history.go(-1);
  }
</script>
	<form name="template" method="post" action="person_inouth.jspviewhigh">  <!--can't understand-->

	  <html:message/>

	  <html:title clazz='module'>病人收入成本详细内容</html:title>
<%
      String [][] result = (String [][])request.getAttribute("balance");  
%>

	  <!-- 简单信息 -->
<html:table clazz="simple">
<% if( result != null) {%>
	    <tr>
	       <td nowrap class="signText"> 年		月: <%= result[0][0]%></td>
	       <td nowrap class="signText"> 性		别: <%if(result[0][1].equals("M")) out.print("男"); else out.print("女"); %></td>
	     </tr>
	    <tr>
	      <td nowrap class="signText">病		种:  <%= result[0][2]%> </td>
	      <td nowrap class="signText">年		龄:  <%= result[0][3]%>  </td>
	    </tr>
	    <tr>
	      <td nowrap class="signText">DRGs分组：<%= result[0][4]%> </td>
	      <td nowrap class="signText"> 住院次数:  <%= result[0][5]%> </td>
	    </tr>
	    <tr>
	      <td nowrap class="signText">就诊时间: <%= result[0][6]%> </td>
	      <td nowrap class="signText"> 住院天数: <%= result[0][7]%>  </td>
	    </tr>
	    <tr>
	      <td nowrap class="signText"> 出院时间: <%= result[0][8]%> </td>
	     
	    </tr>
<%}%>
			<tr>
				<td/>
				<td><button class="pageBtn" onclick="return back(template);">返回</button></td>
			</tr>
</html:table>

  <%
      BaseRO ro = (BaseRO)request.getAttribute("balance_deteail");
      if (ro != null) {
        DecimalFormat mf=new DecimalFormat("#,##0.00");
        TableMarge oper = new TableMarge(ro, "return find()");
  %>
  	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>
  <html:table clazz="result">
       <html:tr clazz='label'>
          <td nowrap class="resultLabel" >年月</td>
          <td nowrap class="resultLabel" >科室</td>
          <td nowrap class="resultLabel" >医疗项目</td>
          <td nowrap class="resultLabel" >单位收入</td>
          <td nowrap class="resultLabel" >单位成本</td>
          <td nowrap class="resultLabel" >数量</td>
          <td nowrap class="resultLabel" >收入</td>
          <td nowrap class="resultLabel" >成本</td>
        </html:tr>
     
	 
     <%
          result = ro.getTableResult();
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
            <td nowrap class="normalText"><%=result[ i ][ 1]%></td>
            <td nowrap class="normalText"><%=result[ i ][ 2 ]%></td>
            <td nowrap class="normalText"><%=result[ i ][ 3 ]%></td>
            <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 4]))%></td>
            <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 5 ]))%></td>
            <td nowrap class="numberText" ><%=result[ i ][ 6]%></td>
            <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 7 ]))%></td>
            <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 8 ]))%></td>
          </tr>

        <%
              }
            }
        %>
		</html:table>
  	<%}%>
	  <input type=hidden name="subFunction"/>
	</form>
</html:html>


