<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/cost/person/personinMain.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.*,
com.viewhigh.cbcs.cbcs.util.DictCache,
java.text.*,
com.viewhigh.cbcs.base.util.Preference" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="java.util.* " %>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script language="JavaScript">
function find(){
  if (isEmpty(template.treat_income)){
  }else{
		switch (isDouble(template.treat_income,10,2)){
        case 0 : alert('医疗收入必须为数字型'); return;
        case 1 : alert('医疗收入整数部分不能高于10个字符'); return;
        case 2 : alert('医疗收入没有整数部分'); return;
        case 3 : alert('医疗收入小数部分不能高于2个字符'); return;
		}
	}
	if(isEmpty(template.treat_cost)) {
	}	else {
		switch (isDouble(template.treat_cost,10,2)){
      case 0 : alert('医疗成本必须为数字型'); return;
      case 1 : alert('医疗成本整数部分不能高于10个字符'); return;
      case 2 : alert('医疗成本没有整数部分'); return;
      case 3 : alert('医疗成本小数部分不能高于2个字符'); return;
		}
	}
	template.subFunction.value="findAlli";
  show_wait();
	template.submit();
}

</Script>

<html:html clazz="main">
<form name="template" onload method="post" action="person_inouth.jspviewhigh">

	<html:message/>

	<html:title clazz='module'>住院病人成本查询</html:title>

  <html:table clazz="simple">
		<tr>
			<td class="signText">核算月：</td>
			<td class="signText">
				<%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
			</td>
      <td class="signText">性别：
      </td>
      <td class="signText"><%=new Select(DictCache.getDict(DictCache.SEX),"sex",request.getParameter("sex"),false,false)%>
      </td>
		</tr>
		<tr>
			<td class="signText">病种：</td>
			<td class="signText"><%=new  Select( request.getAttribute("d_name"), "disease_name",request.getParameter("disease_name"), true, false)%></td>
      <td class="signText"> 年龄：
      </td>
      <td class="signText">
         <%
         String[][] age=new String[111][2];
         for (int i=0;i<111;i++){%>
        <%age[i][0]=i+"";
        age[i][1]=i+"";}%>
        <%=new Select(age,"age",request.getParameter("age"),false,false)%>
      </td>
    </tr>
		 <tr>
       <td class="signText">DRGs分组：
        </td>
       <td class="signText"><%=new  Select( request.getAttribute("d_type"), "model_name",request.getParameter("model_name"), true, false)%>
        </td>
        <td class="signText">就诊时间：</td>

        <td>
        <%=new com.viewhigh.cbcs.base.mvc.view.DateComponent("inyear", request.getParameter("inyear"), false)%>
        </td>
    </tr>
    <tr>
        <td class="signText">
          医疗收入：
        </td>
        <td >
          <input type="text" class="textInputC" name="treat_income" size="20" value="<%=request.getParameter("treat_income")==null? "":request.getParameter("treat_income")%>">
        </td>
    </tr>
    <tr>
        <td class="signText">
          医疗成本:
        </td>
        <td >
          <input type="text" class="textInputC" name="treat_cost" size="20" value="<%=request.getParameter("treat_cost")==null? "":request.getParameter("treat_cost")%>"></td>
        <td>
        </td>
        <td >
        <button class="pageBtn" name=""  onclick="find();" >查询</button>
        <!--  <img src="images/find.gif" class="mouse" onclick="find();">--></td>
        </tr>
    </html:table>
   <html:table clazz="simple">
     <tr>
       <td>
         <html:title clazz='table'>病人成本查询</html:title>
       </td>
     </tr>
   </html:table>
  <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro != null) {
        DecimalFormat mf=new DecimalFormat("#,##0.00");
         TableMarge oper = new TableMarge(ro, "return find()");
  %>


<html:table clazz="simple">
  <tr><td><%=oper%></td></tr></html:table>
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td nowrap class="resultLabel"  >结算号</td>
          <td nowrap class="resultLabel" >年月</td>
          <td nowrap class="resultLabel" >病历号</td>
          <td nowrap class="resultLabel" >姓名</td>
          <td nowrap class="resultLabel" >年龄</td>
          <td nowrap class="resultLabel" >性别</td>
          <td nowrap class="resultLabel" >病种</td>
          <td nowrap class="resultLabel" >DRGs分组</td>
          <td nowrap class="resultLabel" >医疗收入</td>
          <td nowrap class="resultLabel" >医疗成本</td>
<%if (Preference.isDiffDrugCost()) {%>
          <td nowrap class="resultLabel" >西药成本</td>
          <td nowrap class="resultLabel" >西药收入</td>
    			<td nowrap class="resultLabel" >中成药成本</td>
          <td nowrap class="resultLabel" >中成药收入</td>
					<td nowrap class="resultLabel" >中草药成本</td>
          <td nowrap class="resultLabel" >中草药收入</td>
<%}else{%>
          <td nowrap class="resultLabel" >药品收入</td>
          <td nowrap class="resultLabel" >药品成本</td>
<%}%>
          <td nowrap class="resultLabel" >就诊时间</td>
          <td nowrap class="resultLabel" >出院时间</td>
          <td nowrap class="resultLabel"  >住院次数</td>
          <td nowrap class="resultLabel" >住院天数</td>
        </html:tr>

        <%
          String[][] result = ro.getTableResult();
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText"><%=primaryKey%></td>
          <td nowrap class="normalText"><%=result[ i ][ 1]%></td>
          <%if(result[i][23].equals("0")){%>
  <td nowrap class="normalText"><%=result[ i ][ 2 ]%> </td>
<%}else{ %>  <td nowrap class = "normalText"> <a href="person_inouth.jspviewhigh?in_order=<%=result[i][16]%>&subFunction=balance_detail&case_id=<%=result[i][2]%>" onclick="show_wait()"><%=result[i][2]%> </a></td>
<%}%>
          <td nowrap class="normalText"><%=result[ i ][ 3 ]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 4 ]%></td>
<%if(result[i][5].equals("M")){result[i][5]="男";}else if(result[i][5].equals("F")){result[i][5]="女";}%>
          <td nowrap class="normalText"><%=result[ i ][ 5 ]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 6]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 7 ]%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 8]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 10]))%></td>
<%if (Preference.isDiffDrugCost()) {%>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 11 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 9 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 12]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 21 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 13]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 22]))%></td>
<%}else{%>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 9 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 11 ]))%></td>
<%}%>
          <td nowrap class="normalText"><%=result[ i ][ 14 ]%></td>
          <td nowrap class="normalText"><%=result[ i ][ 15]%></td>
          <td nowrap class="numberText"><%=result[ i ][ 16]%></td>
           <td nowrap class="numberText"><%=result[ i ][ 17]%></td>

        </tr>

        <%
              }
            }
        %>
		</html:table>
  	<%}%>
				<input type="hidden" name="subFunction" >
</form>

</html:html>
