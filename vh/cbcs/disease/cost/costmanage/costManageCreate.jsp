<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/cost/costmanage/costManageCreate.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
	    if(trim(template.dyear.value)==""){
	    	alert("年度不能为空");
	    	return;
	    }
	     if(trim(template.disease_code.value)==""){
	    	alert("病种不能为空");
	    	return;
	    }
	     if(trim(template.disease_model.value)==""){
	    	alert("DRG编号描述不能为空");
	    	return;
	    }
	     if(trim(template.hospitalize_id.value)==""){
	    	alert("就诊类型不能为空");
	    	return;
	    }
	    //1院内标准:医疗收入
	    switch(isDouble(template.h_t_income,12,4))
    {
      case 0 : alert('院内标准:医疗收入 必须为数字型'); return;
      case 1 : alert('院内标准:医疗收入 整数部分不能高于12个字符'); return;
      case 2 : alert('院内标准:医疗收入 没有整数部分'); return;
      case 3 : alert('院内标准:医疗收入 小数部分不能高于4个字符'); return;
    }
    //2院内标准:药品收入
    switch(isDouble(template.h_m_income,12,4))
    {
      case 0 : alert('院内标准:药品收入 必须为数字型'); return;
      case 1 : alert('院内标准:药品收入 整数部分不能高于12个字符'); return;
      case 2 : alert('院内标准:药品收入 没有整数部分'); return;
      case 3 : alert('院内标准:药品收入 小数部分不能高于4个字符'); return;
    }
    //3院内标准:医疗成本
    switch(isDouble(template.h_t_cost,12,4))
    {
      case 0 : alert('院内标准:医疗成本 必须为数字型'); return;
      case 1 : alert('院内标准:医疗成本 整数部分不能高于12个字符'); return;
      case 2 : alert('院内标准:医疗成本 没有整数部分'); return;
      case 3 : alert('院内标准:医疗成本 小数部分不能高于4个字符'); return;
    }
    //4院内标准:药品成本
    switch(isDouble(template.h_m_cost,12,4))
    {
      case 0 : alert('院内标准:药品成本 必须为数字型'); return;
      case 1 : alert('院内标准:药品成本 整数部分不能高于12个字符'); return;
      case 2 : alert('院内标准:药品成本 没有整数部分'); return;
      case 3 : alert('院内标准:药品成本 小数部分不能高于4个字符'); return;
    }
    //5市级标准：医疗收入
    switch(isDouble(template.c_t_income,12,4))
    {
      case 0 : alert('市级标准：医疗收入 必须为数字型'); return;
      case 1 : alert('市级标准：医疗收入 整数部分不能高于12个字符'); return;
      case 2 : alert('市级标准：医疗收入 没有整数部分'); return;
      case 3 : alert('市级标准：医疗收入 小数部分不能高于4个字符'); return;
    }
    //6市级标准：药品收入
    switch(isDouble(template.c_m_income,12,4))
    {
      case 0 : alert('市级标准：药品收入 必须为数字型'); return;
      case 1 : alert('市级标准：药品收入 整数部分不能高于12个字符'); return;
      case 2 : alert('市级标准：药品收入 没有整数部分'); return;
      case 3 : alert('市级标准：药品收入 小数部分不能高于4个字符'); return;
    }
    //7市级标准：医疗成本
    switch(isDouble(template.c_t_cost,12,4))
    {
      case 0 : alert('市级标准：医疗成本 必须为数字型'); return;
      case 1 : alert('市级标准：医疗成本 整数部分不能高于12个字符'); return;
      case 2 : alert('市级标准：医疗成本 没有整数部分'); return;
      case 3 : alert('市级标准：医疗成本 小数部分不能高于4个字符'); return;
    }
    //8市级标准：药品成本
    switch(isDouble(template.c_m_cost,12,4))
    {
      case 0 : alert('市级标准：药品成本 必须为数字型'); return;
      case 1 : alert('市级标准：药品成本 整数部分不能高于12个字符'); return;
      case 2 : alert('市级标准：药品成本 没有整数部分'); return;
      case 3 : alert('市级标准：药品成本 小数部分不能高于4个字符'); return;
    }
    //9部颁标准：医疗收入
    switch(isDouble(template.m_t_income,12,4))
    {
      case 0 : alert('部颁标准：医疗收入 必须为数字型'); return;
      case 1 : alert('部颁标准：医疗收入 整数部分不能高于12个字符'); return;
      case 2 : alert('部颁标准：医疗收入 没有整数部分'); return;
      case 3 : alert('部颁标准：医疗收入 小数部分不能高于4个字符'); return;
    }
    //10部颁标准：药品收入
    switch(isDouble(template.m_m_income,12,4))
    {
      case 0 : alert('部颁标准：药品收入 必须为数字型'); return;
      case 1 : alert('部颁标准：药品收入 整数部分不能高于12个字符'); return;
      case 2 : alert('部颁标准：药品收入 没有整数部分'); return;
      case 3 : alert('部颁标准：药品收入 小数部分不能高于4个字符'); return;
    }
    //11部颁标准：医疗成本
    switch(isDouble(template.m_t_cost,12,4))
    {
      case 0 : alert('部颁标准：医疗成本 必须为数字型'); return;
      case 1 : alert('部颁标准：医疗成本 整数部分不能高于12个字符'); return;
      case 2 : alert('部颁标准：医疗成本 没有整数部分'); return;
      case 3 : alert('部颁标准：医疗成本 小数部分不能高于4个字符'); return;
    }
    //12部颁标准：药品成本
    switch(isDouble(template.m_m_cost,12,4))
    {
      case 0 : alert('部颁标准：药品成本 必须为数字型'); return;
      case 1 : alert('部颁标准：药品成本 整数部分不能高于12个字符'); return;
      case 2 : alert('部颁标准：药品成本 没有整数部分'); return;
      case 3 : alert('部颁标准：药品成本 小数部分不能高于4个字符'); return;
    }
        template.subFunction.value='create';
        template.submit();
            show_wait();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_costmanage.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>标准成本管理添加</html:title>

  <!-- 简单信息 -->
	<%
	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
	%>    
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">年度：</td>
      <td class="signText" nowrap="nowrap"><%=new Select(year,"dyear",request.getParameter("dyear")==null? "":request.getParameter("dyear"),false,false)%></td> 
      <td nowrap class="signText">就诊类型:</td><% String hospitalize_id = request.getParameter("hospitalize_id")==null? "":request.getParameter("hospitalize_id");%>
		  <td><%=new Select(request.getAttribute("h_id"),"hospitalize_id",hospitalize_id,true,false)%></td>
		</tr>
		<tr>
		  <td nowrap class="signText">病种:</td> <% String disease_code = request.getParameter("disease_code")==null? "":request.getParameter("disease_code");%>
		  <td><%=new Select(request.getAttribute("d_disease"),"disease_code",disease_code,true,false)%></td>
		
		  <td nowrap class="signText">DRGs分组:</td><% String disease_model = request.getParameter("disease_model")==null ? "":request.getParameter("disease_model");%>
		  <td><%=new Select(request.getAttribute("d_model"),"disease_model",disease_model,true,false)%></td>
		</tr>
    </tr>
    

    
    <tr>
      <td class="signText" nowrap="nowrap">院内标准：</td>
    </tr>
    <tr>      
      <td class="signText" nowrap="nowrap">医疗收入：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="h_t_income" class="textInputC" /></td>

      <td class="signText" nowrap="nowrap"  >药品收入：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="h_m_income" class="textInputC" /></td>
    </tr>
    <tr>     
      <td class="signText" nowrap="nowrap">医疗成本：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="h_t_cost" class="textInputC" /></td>
 
      <td class="signText" nowrap="nowrap">药品成本：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="h_m_cost" class="textInputC" /></td>
    </tr>
    

    <tr>
      <td class="signText" nowrap="nowrap">市级标准：</td>
    </tr>
    <tr>      
      <td class="signText" nowrap="nowrap">医疗收入：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="c_t_income" class="textInputC" /></td>

      <td class="signText" nowrap="nowrap">药品收入：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="c_m_income" class="textInputC" /></td>
      <td class="signText"></td>
    </tr>
    <tr>     
      <td class="signText" nowrap="nowrap">医疗成本：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="c_t_cost" class="textInputC" /></td>
 
      <td class="signText" nowrap="nowrap">药品成本：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="c_m_cost" class="textInputC" /></td>
      <td class="signText"></td>
    </tr>
    

    <tr>
      <td class="signText" nowrap="nowrap">部颁标准：</td>
    </tr>
    <tr>      
      <td class="signText" nowrap="nowrap">医疗收入：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="m_t_income" class="textInputC" /></td>

      <td class="signText" nowrap="nowrap">药品收入：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="m_m_income" class="textInputC" /></td>
            <td class="signText"></td>
    </tr>
    <tr>     
      <td class="signText" nowrap="nowrap">医疗成本：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="m_t_cost" class="textInputC" /></td>
 
      <td class="signText" nowrap="nowrap">药品成本：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="m_m_cost" class="textInputC" /></td>
            <td class="signText"></td>
    </tr>
	  <tr> <td class="signText" nowrap="nowrap">治疗方案描述	：</td>  
	      <td class="signText" nowrap="nowrap" colspan='3'>
			<TEXTAREA  name="treat_scheme" COLS="85" rows="3"></TEXTAREA>
		  </td>
		  </tr>

    <tr><td class="signText" nowrap="nowrap">　</td></tr>
    <tr>
    
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
 </html:table >
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   