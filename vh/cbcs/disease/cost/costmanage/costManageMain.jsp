<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/cost/costmanage/costManageMain.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {

        template.subFunction.value='preparedCreate';
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
  function inherit(){
    template.subFunction.value='preparedInherit';
    show_wait();
    template.submit();
    return true;      
  }

  function group() {
    if (template.year_month.value == "") {
      alert("请选择年月！");
      return false;
    }
    }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="dictdisease_costmanage.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>标准成本管理</html:title>
	<%
	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
	%>    
	  <!-- 简单信息 -->
	  <html:table clazz="simple">
		<tr>
		
		  <TD  noWrap class="signText">年度：</TD><td noWrap class="signText">
		    <%=new Select(year,"dyear",request.getParameter("dyear")==null? "":request.getParameter("dyear"),false,false)%>
		  年</TD>	    
		  <td nowrap class="signText">就诊类型:</td><% String hospitalize_id = request.getParameter("hospitalize_id")==null? "":request.getParameter("hospitalize_id");%>
		  <td noWrap class="signText"><%=new Select(request.getAttribute("h_id"),"hospitalize_id",hospitalize_id,true,false)%></td>
		</tr>
		<tr>
		  <td nowrap class="signText">病种:</td> <% String disease_code = request.getParameter("disease_code")==null? "":request.getParameter("disease_code");%>
		  <td nowrap class="signText"><%=new Select(request.getAttribute("d_disease"),"disease_code",disease_code,true,false)%></td>
		
		  <td nowrap class="signText">DRG编号描述:</td><% String disease_model = request.getParameter("disease_model")==null ? "":request.getParameter("disease_model");%>
		  <td nowrap class="signText"><%=new Select(request.getAttribute("d_model"),"disease_model",disease_model,true,false)%></td>
		</tr>
	  <tr>
	      <td class="signText"></td>
	      <td class="signText"></td>
	    <tr>
	     
	      <td class="signText"></td>
	      <td class="signText"></td>
	      <td class="signText"></td>
	      <td class="signText">
	      <button class="pageBtn" name="" onclick="return find();"  >查询</button>
	      <!--<img src="images/find.gif" class="mouse" onclick="return find();" />--></td>
	    </tr>	
	   </tr>

	  </html:table>

	  <br>

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
	  
  
		<html:title clazz='table'> 药品标准收入成本定义</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>

		    	<html:table clazz="result">
		        <html:tr clazz='label'>		
   		          <TD class=resultLabel noWrap rowSpan=2>选择</td>        
		          <TD class=resultLabel noWrap rowSpan=2>年度</TD>
                    <TD class=resultLabel noWrap rowSpan=2>病种名称</TD>
                    <TD class=resultLabel noWrap rowSpan=2>DRG 编码</TD>
                    <TD colSpan=4 noWrap class=resultLabel>院内标准</TD>
                    <TD colSpan=4 noWrap class=resultLabel>市级标准</TD>
                    <TD colSpan=4 noWrap class=resultLabel>部颁标准</TD>
                  </html:tr>
                  <html:tr clazz='label'>	
                    <TD class=resultLabel noWrap><DIV align=center>医疗收入</DIV></TD>
                    <TD class=resultLabel noWrap><DIV align=center>药品收入</DIV></TD>
                    <TD class=resultLabel noWrap><DIV align=center>医疗成本</DIV></TD>
                    <TD class=resultLabel noWrap>药品成本</TD>
                    <TD noWrap class=resultLabel><DIV align=center>医疗收入</DIV></TD>
                    <TD noWrap class=resultLabel><DIV align=center>药品收入</DIV></TD>
                    <TD noWrap class=resultLabel><DIV align=center>医疗成本</DIV></TD>
                    <TD noWrap class=resultLabel>药品成本</TD>
                    <TD noWrap class=resultLabel><DIV align=center>医疗收入</DIV></TD>
                    <TD noWrap class=resultLabel><DIV align=center>药品收入</DIV></TD>
                    <TD noWrap class=resultLabel><DIV align=center>医疗成本</DIV></TD>
                    <TD noWrap class=resultLabel>药品成本</TD>
		        </html:tr>

		        <%
		          String[][] result = ro.getTableResult();
		          DecimalFormat mf=new DecimalFormat("#,##0.00");
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		            //主键dyear [0],disease_code[16],  d_cost_in_st.disease_model[2], hospitalize_id[15]
		              String  primaryKey = result[ i ][ 0 ]+" "+result[i][16]+" "+result[i][2]+" "+result[i][15];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }
 
		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>
 
		        <tr CLASS="<%=rowColor%>">
		          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
		          <td nowrap="nowrap" class="normalText"><a href="dictdisease_costmanage.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>"><%=result[ i ][0 ]%></a></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 3 ]))%></td>
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 4 ]))%></td>
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 5 ]))%></td>
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 6 ]))%></td>
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 7 ]))%></td>
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 8 ]))%></td>
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 9 ]))%></td>
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 10 ]))%></td>
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 11 ]))%></td>
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 12 ]))%></td>
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 13 ]))%></td>
		          <td nowrap="nowrap" class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 14 ]))%></td>

		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		<%}%>
	  <input type=hidden name="subFunction"/>
	</form>
</html:html>

