<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/cost/costmanage/costManageSave.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
  //1院内标准:医疗收入
	    switch(isDouble(template.h_t_income,12,4))
    {
      case 0 : alert('院内标准:医疗收入 必须为数字型'); return;
      case 1 : alert('院内标准:医疗收入 整数部分不能高于12个字符'); return;
      case 2 : alert('院内标准:医疗收入 没有整数部分'); return;
      case 3 : alert('院内标准:医疗收入 小数部分不能高于4个字符'); return;
    }
    //2院内标准:药品收入
    switch(isDouble(template.h_m_income,12,4))
    {
      case 0 : alert('院内标准:药品收入 必须为数字型'); return;
      case 1 : alert('院内标准:药品收入 整数部分不能高于12个字符'); return;
      case 2 : alert('院内标准:药品收入 没有整数部分'); return;
      case 3 : alert('院内标准:药品收入 小数部分不能高于4个字符'); return;
    }
    //3院内标准:医疗成本
    switch(isDouble(template.h_t_cost,12,4))
    {
      case 0 : alert('院内标准:医疗成本 必须为数字型'); return;
      case 1 : alert('院内标准:医疗成本 整数部分不能高于12个字符'); return;
      case 2 : alert('院内标准:医疗成本 没有整数部分'); return;
      case 3 : alert('院内标准:医疗成本 小数部分不能高于4个字符'); return;
    }
    //4院内标准:药品成本
    switch(isDouble(template.h_m_cost,12,4))
    {
      case 0 : alert('院内标准:药品成本 必须为数字型'); return;
      case 1 : alert('院内标准:药品成本 整数部分不能高于12个字符'); return;
      case 2 : alert('院内标准:药品成本 没有整数部分'); return;
      case 3 : alert('院内标准:药品成本 小数部分不能高于4个字符'); return;
    }
    //5市级标准：医疗收入
    switch(isDouble(template.c_t_income,12,4))
    {
      case 0 : alert('市级标准：医疗收入 必须为数字型'); return;
      case 1 : alert('市级标准：医疗收入 整数部分不能高于12个字符'); return;
      case 2 : alert('市级标准：医疗收入 没有整数部分'); return;
      case 3 : alert('市级标准：医疗收入 小数部分不能高于4个字符'); return;
    }
    //6市级标准：药品收入
    switch(isDouble(template.c_m_income,12,4))
    {
      case 0 : alert('市级标准：药品收入 必须为数字型'); return;
      case 1 : alert('市级标准：药品收入 整数部分不能高于12个字符'); return;
      case 2 : alert('市级标准：药品收入 没有整数部分'); return;
      case 3 : alert('市级标准：药品收入 小数部分不能高于4个字符'); return;
    }
    //7市级标准：医疗成本
    switch(isDouble(template.c_t_cost,12,4))
    {
      case 0 : alert('市级标准：医疗成本 必须为数字型'); return;
      case 1 : alert('市级标准：医疗成本 整数部分不能高于12个字符'); return;
      case 2 : alert('市级标准：医疗成本 没有整数部分'); return;
      case 3 : alert('市级标准：医疗成本 小数部分不能高于4个字符'); return;
    }
    //8市级标准：药品成本
    switch(isDouble(template.c_m_cost,12,4))
    {
      case 0 : alert('市级标准：药品成本 必须为数字型'); return;
      case 1 : alert('市级标准：药品成本 整数部分不能高于12个字符'); return;
      case 2 : alert('市级标准：药品成本 没有整数部分'); return;
      case 3 : alert('市级标准：药品成本 小数部分不能高于4个字符'); return;
    }
    //9部颁标准：医疗收入
    switch(isDouble(template.m_t_income,12,4))
    {
      case 0 : alert('部颁标准：医疗收入 必须为数字型'); return;
      case 1 : alert('部颁标准：医疗收入 整数部分不能高于12个字符'); return;
      case 2 : alert('部颁标准：医疗收入 没有整数部分'); return;
      case 3 : alert('部颁标准：医疗收入 小数部分不能高于4个字符'); return;
    }
    //10部颁标准：药品收入
    switch(isDouble(template.m_m_income,12,4))
    {
      case 0 : alert('部颁标准：药品收入 必须为数字型'); return;
      case 1 : alert('部颁标准：药品收入 整数部分不能高于12个字符'); return;
      case 2 : alert('部颁标准：药品收入 没有整数部分'); return;
      case 3 : alert('部颁标准：药品收入 小数部分不能高于4个字符'); return;
    }
    //11部颁标准：医疗成本
    switch(isDouble(template.m_t_cost,12,4))
    {
      case 0 : alert('部颁标准：医疗成本 必须为数字型'); return;
      case 1 : alert('部颁标准：医疗成本 整数部分不能高于12个字符'); return;
      case 2 : alert('部颁标准：医疗成本 没有整数部分'); return;
      case 3 : alert('部颁标准：医疗成本 小数部分不能高于4个字符'); return;
    }
    //12部颁标准：药品成本
    switch(isDouble(template.m_m_cost,12,4))
    {
      case 0 : alert('部颁标准：药品成本 必须为数字型'); return;
      case 1 : alert('部颁标准：药品成本 整数部分不能高于12个字符'); return;
      case 2 : alert('部颁标准：药品成本 没有整数部分'); return;
      case 3 : alert('部颁标准：药品成本 小数部分不能高于4个字符'); return;
    }
    template.subFunction.value='save';
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_costmanage.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module"> 标准成本管理修改</html:title>

  <%
    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

 
  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">年度：</td>
      <td width="75%" class="signText" nowrap="nowrap">
        <input type=text value="<%=result[0]%>" disabled />
      </td>

      <td class="signText" nowrap="nowrap">病种：</td>
      <td width="75%" class="signText" nowrap="nowrap">
        <input type=text value="<%=result[1]%>" disabled />
      </td>
    </tr>
    
    <tr>
      <td class="signText" nowrap="nowrap">DRGs分组：</td>
      <td width="75%" class="signText" nowrap="nowrap">
        <input type=text value="<%=result[2]%>" disabled />
      </td>

      <td class="signText" nowrap="nowrap">就诊类型：</td>
      <td width="75%" class="signText" nowrap="nowrap">
        <input type=text value="<%=result[15].equals("I")? "住院":"门诊"%>" disabled />
      </td>
    </tr>
    
	<tr><td class="signText" nowrap="nowrap">　</td></tr>    
    <tr>
      <td class="signText" nowrap="nowrap">院内标准：</td>
    </tr>    
    <tr>
      <td class="signText" nowrap="nowrap">医疗收入：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="h_t_income" value="<%=result[3]%>" class="textInputC">
      </td>
      <td class="signText" nowrap="nowrap">药品收入：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="h_m_income" value="<%=result[4]%>" class="textInputC">
      </td>
    </tr>    
    <tr>

      <td class="signText" nowrap="nowrap">医疗成本：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="h_t_cost" value="<%=result[5]%>" class="textInputC">
      </td>
      <td class="signText" nowrap="nowrap">药品成本：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="h_m_cost" value="<%=result[6]%>" class="textInputC">
      </td>
    </tr>
	<tr><td class="signText" nowrap="nowrap">　</td></tr>    
    <tr>
      <td class="signText" nowrap="nowrap">市级标准：</td>
    </tr>	   
    <tr>
      <td class="signText" nowrap="nowrap">医疗收入：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="c_t_income" value="<%=result[7]%>" class="textInputC">
      </td>

      <td class="signText" nowrap="nowrap">药品收入：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="c_m_income" value="<%=result[8]%>" class="textInputC">
      </td>
    </tr>    
     <tr>
      <td class="signText" nowrap="nowrap">医疗成本：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="c_t_cost" value="<%=result[9]%>" class="textInputC">
      </td>

      <td class="signText" nowrap="nowrap">药品成本：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="c_m_cost" value="<%=result[10]%>" class="textInputC">
      </td>
      
    </tr>    
	<tr><td class="signText" nowrap="nowrap">　</td></tr>
    <tr>          <td class="signText" nowrap="nowrap">部颁标准：</td>    </tr>    
	   
    <tr>
      <td class="signText" nowrap="nowrap">医疗收入：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="m_t_income" value="<%=result[11]%>" class="textInputC">
      </td>
      <td class="signText" nowrap="nowrap">药品收入：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="m_m_income" value="<%=result[12]%>" class="textInputC">
      </td>
 
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">医疗成本：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="m_t_cost" value="<%=result[13]%>" class="textInputC">
      </td>


      <td class="signText" nowrap="nowrap">药品成本：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="m_m_cost" value="<%=result[14]%>" class="textInputC">
      </td>   
      
    </tr>
    
    <tr>
 	 <td class="signText" nowrap="nowrap">治疗方案描述	：</td>  
	      <td class="signText" nowrap="nowrap" colspan='3'>
			<TEXTAREA  name="treat_scheme" COLS="85" rows="3"><%=result[17]%>
			</TEXTAREA>
		  </td>
	</tr>   
     
    
    <tr>
      <td colspan="2">
      <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="dyear" value="<%=result[0]%>">
  <input type="hidden" name="disease_code" value="<%=result[16]%>">
  <input type="hidden" name="disease_model" value="<%=result[2]%>">
  <input type="hidden" name="hospitalize_id" value="<%=result[15]%>">  
  <%}%>
</form>


</html:html>
