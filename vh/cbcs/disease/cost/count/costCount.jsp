<!--
   $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/cost/count/costCount.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
   $Author: zhoulidong $
   $Date: 2012/03/12 01:57:59 $
   $Modtime:  $
   $Revision: 1.1 $
   $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript">
	function apportion(num) {
	  if (template.year_month.value=='') {
	    alert('请选择核算月');
	    return false;
	  }
          if(num==0)
            template.flag.value='no'
          else if(num==1)
            template.flag.value='complete'
	  show_wait();
	  template.submit();
	  return true;
	}
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="diseaseCostCount.jspviewhigh?subFunction=count" >
      <!-- 返回信息栏 -->
	  <html:message/>

      <!-- 标题栏 -->
	  <html:title clazz='module'>病种成本核算</html:title>
<br>

      <!-- 简单信息 -->
	  <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap width="10%">核算月：</td>
          <td class="signText">
            <%String yearMonth=(String)request.getAttribute("year_month");
            if(yearMonth==null){
                 yearMonth=request.getParameter("year_month");
                }
            %>
            <%=yearMonth.substring(0,4)+"年"+yearMonth.substring(4,6)+"月"%>
            <input type="hidden" name='year_month' value="<%=yearMonth%>">
          </td>
          <td><button class="pageBtn" onclick="return apportion(0)">预结</button>
          <!--<img src="images/preparedBalance.gif" class="mouse" onclick="return apportion(0)"/>--></td>
          <td><button class="pageBtn" onclick="return apportion(1)">完成</button>
          <!--<img src="images/complete.gif" class="mouse" onclick="return apportion(1)"/>--></td>
        </tr>
	  </html:table>
<input type='hidden' name="flag" value='complete'>
  	</form>

</html:html>
