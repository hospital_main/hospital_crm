<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/cost/illness/inhospital/in_hospital.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                 java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
function find() {

    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="inhospital_query.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>病种成本查询(住院)</html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
       <tr>
	      <td nowrap class="signText">核算月：</td>
        <td>
           <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
	      </td>
        <td nowrap class="signText">&nbsp;</td>
	       <td>
	   <button class="pageBtn" name="" onclick="return find();"  >查询</button>     
	    <!--   <img src="images/find.gif" class="mouse" onclick="return find();" />--></td>
      </tr>
      <tr>
        <td nowrap class="signText">病种：</td>
	      <td nowrap class="signText">
           <% String[] value1 = request.getParameterValues("leave_disease_code");
              String[][]  leave_disease_code=(String[][]) request.getAttribute("leave_disease_code");
           %>
          <select name="leave_disease_code"  size='4' multiple>
          <option value="" <%if(value1==null||(value1.length==1&&value1[0].equals(""))||(value1.length>1&&value1[0].equals(""))){
                  out.print("selected");}%>>--不限--</option >
              <%if(leave_disease_code!=null) {
                for(int i=0;i<leave_disease_code.length;i++){%>
                  <option value=<%=leave_disease_code[i][0]%>
                   <% if(value1!=null){
                      for(int j=0;j<value1.length;j++)
                    if(leave_disease_code[i][0].equals(value1[j])) out.print("selected");
                   }
                   %> >
               <%=leave_disease_code[i][0]+":"+leave_disease_code[i][1]%></option>
            <%}}%>
         </td>
          <td nowrap class="signText">DRGs分组：</td>
	      <td nowrap class="signText">
            <% String[] value = request.getParameterValues("leave_disease_model");
               String[][] leave_disease_model=(String[][])request.getAttribute("leave_disease_model");
           %>
           <select name="leave_disease_model"  size='4' multiple>
          <option value="" <%if(value==null||(value.length==1&&value[0].equals(""))||(value.length>1&&value[0].equals(""))){
                  out.print("selected");}%>>--不限--</option >
                 <%if(leave_disease_model!=null) {
                     for(int i=0;i<leave_disease_model.length;i++){%>
                        <option value=<%=leave_disease_model[i][0]%>
                        <% if(value!=null){
                           for(int j=0;j<value.length;j++)
                           if(leave_disease_model[i][0].equals(value[j])) out.print("selected");
                        }
                        %> >
                     <%=leave_disease_model[i][0]+":"+leave_disease_model[i][1]%>
                  </option>
                <%}}%>

	      </td>
	    </tr>


	  </html:table>

	  <br>
		<html:title clazz='table'>病种成本核算(住院)表</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
        <% DecimalFormat nf = new DecimalFormat("#,##0.00");
           BaseRO ro = (BaseRO)request.getAttribute("baseRO");
           TableMarge oper = new TableMarge(ro, "return find()");
           String[][] result=ro.getTableResult();
        %>
             <tr><td><%=oper%></td></tr>

		  <!-- 结果集 -->
		  <tr>
		    <td>
          <html:table clazz="result">
           <%
            String sign=(String)request.getAttribute("sign");
              if(sign!=null && sign.equals("1")){
           %>
		        <html:tr clazz='label'>
		          <td nowrap="nowrap" rowspan=2>时间</td>
              <td nowrap="nowrap" rowspan=2>病种编码</td>
              <td nowrap="nowrap" rowspan=2>病种名称</td>
              <td nowrap="nowrap" rowspan=2>DRGs分组</td>
              <td nowrap="nowrap" colspan=3>病种总成本</td>
              <td nowrap="nowrap" colspan=4>病种单位成本</td>
		        </html:tr>
            <html:tr clazz='label'>
              <td nowrap="nowrap">总成本</td>
              <td nowrap="nowrap">医疗成本</td>
              <td nowrap="nowrap">药品成本</td>
              <td nowrap="nowrap">总成本</td>
              <td nowrap="nowrap">医疗成本</td>
              <td nowrap="nowrap">药品成本</td>
              <td nowrap="nowrap">就诊人次</td>
            </html:tr>
            <%} else {%>

		        <html:tr clazz='label'>
		          <td nowrap="nowrap" rowspan=2>时间</td>
              <td nowrap="nowrap" rowspan=2>病种编码</td>
              <td nowrap="nowrap" rowspan=2>病种名称</td>
              <td nowrap="nowrap" rowspan=2>病种分型</td>
              <td nowrap="nowrap" colspan=5>病种总成本</td>
              <td nowrap="nowrap" colspan=6>病种单位成本</td>
		        </html:tr>
            <html:tr clazz='label'>
              <td nowrap="nowrap">总成本</td>
              <td nowrap="nowrap">医疗成本</td>
              <td nowrap="nowrap">西药成本</td>
              <td nowrap="nowrap">中成药成本</td>
              <td nowrap="nowrap">中草药成本</td>
              <td nowrap="nowrap">总成本</td>
              <td nowrap="nowrap">医疗成本</td>
              <td nowrap="nowrap">西药成本</td>
              <td nowrap="nowrap">中成药成本</td>
              <td nowrap="nowrap">中草药成本</td>
              <td nowrap="nowrap">就诊人次</td>
            </html:tr>
            <%}%>
		        <%
		          if ( result != null ){
		            for (int i = 0; i < result.length; i++ ){
		               String rowColor = "rowGray";
		               if (i/2*2==i) rowColor = "rowWhite";
		        %>
		        <tr CLASS="<%=rowColor%>">
              <td nowrap="nowrap" class="normalText"><%=result[ i ][ 0 ]%></td>
              <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td>
              <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
              <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td>
               <% for(int j=4;j<result[i].length-1;j++){%>
                  <td nowrap="nowrap" class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ j ]))%></td>
		           <%}%>
              <td nowrap="nowrap" class="numberText"><%=result[ i ][ result[0].length-1 ]%>
		        </tr>
		        <%
		              }
		            }
		        %>
		      </html:table>
		    </td>
		  </tr>
	  </html:table>

	  <input type=hidden name="subFunction"/>
	</form>
</html:html>




