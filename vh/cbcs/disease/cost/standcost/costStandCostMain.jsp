<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/cost/standcost/costStandCostMain.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

//继承
 function create(){ 
 			if(template.syear.value==""||template.fmonth.value==""||template.year.value==""||template.tmonth.value==""){
 				alert("所有条件条件不能为空");
 				return;
 			}
	    if(template.fmonth.value>template.tmonth.value)
	    {
	      alert('起始月要小于终止月');
	      return;
   	  }
   	  if(template.syear.value>=template.year.value){
   	  	alert("核算年度要小于目标年度");
   	  	return;
   	  }
      if (!confirm('系统将生成 ['+template.year.value+'] 年标准成本核算数据，请确认')) {
       return true;
      }    
      template.subFunction.value='count';
      show_wait();        
      template.submit();

      return true;
 }
</Script>
    
    
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_standcost.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>
<%
	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
	String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
%>
  <!-- 标题栏 -->
	  <html:title clazz='module'>目标成本核算页面</html:title>
 <html:table clazz="simple">
  <tr>	   
	 <td class="signText">核算月：</td>	 
	 <td class="signText"> <%=new Select(year,"syear",request.getParameter("syear")==null? "":request.getParameter("syear"),false,false)%>年
	 <%=new Select(month,"fmonth",request.getParameter("fmonth")==null? "":request.getParameter("fmonth"),false,false)%>月 到 
	 <%=new Select(month,"tmonth",request.getParameter("tmonth")==null? "":request.getParameter("tmonth"),false,false)%>月</td>
  </tr>  
	<tr>
	<td> </td>
	</tr>
	<tr>
		<td class="signText">生成目标成本年度:</td>
		<td class="signText"><%=new Select(year,"year",request.getParameter("year")==null? "":request.getParameter("year"),false,false)%></td>
	</tr>
	
	<tr>
	<td> </td>
	</tr>

	<tr>
	
   <td nowrap class="signText">计算方法:</td>
    <td colspan="2" nowrap class="signText">
    <input type="radio" name="kind" value="1"checked >灰色关联法
    <input type="radio" name="kind" value="2" >标准临床路径
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="pageBtn" onclick="return create()">确定</button> 
		<!--<IMG src="images/confirm.gif" width="50" height="20" border="0" class=mouse onclick="return create()">-->
		</td>
  </tr>
    <input type=hidden name="subFunction" value="create"/>
  </form>
</html:table>
</html:html>  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  