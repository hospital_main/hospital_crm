<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/disease_cost_analysis/cost_thismonth_compare/dept/dept.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                 java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
 <%        DecimalFormat nf = new DecimalFormat("#,##0.00");
           DecimalFormat nf1 = new DecimalFormat("#0.00%");
           BaseRO ro = (BaseRO)request.getAttribute("ro");
           TableMarge oper = new TableMarge(ro, "return find()");
           String[][] result=null;
           if(ro!=null){
             result=ro.getTableResult();
           }
        %>
<Script Language="JavaScript">
function find() {
  if(template.year_from.value==""){
        alert("请输入起始年");
        return;
    }
    if(template.year_to.value==""){
        alert("请输入终止年");
        return;
    }
    if(template.year_from.value>template.year_to.value){
        alert("起始年不可大于终止年");
        return;
    }
    if(template.month.value==""){
        alert("请选择月");
        return;
    }
//  var num=0;
//    for(var i=0;i<template.leave_disease_code.options.length;i++){
//        if(template.leave_disease_code.options[i].selected)
//          num++;
//    }
//    if(num==0||template.leave_disease_code.options[0].selected){
//       alert("请选择病种");
//      return;
//    }
//    if(num>30){
//      alert("病种选择的数目不能大于30个");
//      return;
//    }
//     var num1=0;
//    for(var i=0;i<template.dept_code.options.length;i++){
//        if(template.dept_code.options[i].selected)
//          num1++;
//    }
//    if(num1==0||template.dept_code.options[0].selected){
//       alert("请选择科室");
//       return;
//    }
//    if(num1>20){
//      alert("科室选择的数目不能大于20个");
//      return;
//    }
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
function draw()
{ <%if(result==null) out.print("return");%>
  var year_from=template.year_from.value;
  var year_to=template.year_to.value;
  var month=template.month.value
  window.open("thismonth_dept_diease_cost.jspviewhigh?subFunction=drawchart&year_from="+year_from+"&year_to="+year_to+"&month="+month,null,"top=180, left=200, height=520,width=600,toolbar=no, location=no")
 }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="thismonth_dept_diease_cost.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>科室病种成本</html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
  <tr>
      <%String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};%>
     		<td  nowrap="nowrap" class="signText" > 期间:</td>
        <td  nowrap class="signText" colspan="2" ><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year_from",request.getParameter("year_from"),false,false)%>年 至
				<%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year_to",request.getParameter("year_to"),false,false)%>年 </td>
    		<%String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};%>
				<td  nowrap="nowrap" class="signText" >对比月:</td>
        <td nowrap="nowrap" class="signText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(month,"month",request.getParameter("month"),false,false)%>月</td>
  </tr>
  <tr>
     <td nowrap class="signText">成本类型:</td>
     <td   colspan="2" nowrap class="signText"><input type="radio" name="kind" value="1" <% if(request.getParameter("kind")==null) out.print("checked"); if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("1")) out.print("checked");%>>总成本
     <input type="radio" name="kind" value="2" <%if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("2")) out.print("checked");%>>单位成本</td>
     <td  nowrap class="signText">就诊类型</tr>
       <td nowrap class="signText">
        <select name="hospitalize_id">
             <%String hospitalize_id=request.getParameter("hospitalize_id");%>
             <option value='O' <%if(hospitalize_id!=null&&hospitalize_id.equals("O")) out.print("selected");%>>门诊</option>
             <option value='I' <%if(hospitalize_id!=null&&hospitalize_id.equals("I")) out.print("selected");%>>住院</option>
          </select>
        </td>
  </tr>
  <tr>
     <td nowrap class="signText">病种</td>
      <td nowrap class="signText" colspan="2">
           <% String[] value1 = request.getParameterValues("leave_disease_code");
              String[][]  leave_disease_code=(String[][]) request.getAttribute("leave_disease_code");
           %>
          <select name="leave_disease_code"  size='4' multiple>
          <option value="" <%if(value1==null||(value1.length==1&&value1[0].equals(""))||(value1.length>1&&value1[0].equals(""))){
                  out.print("selected");}%>>--不限--</option >
              <%if(leave_disease_code!=null) {
                for(int i=0;i<leave_disease_code.length;i++){%>
                  <option value=<%=leave_disease_code[i][0]%>
                   <% if(value1!=null){
                      for(int j=0;j<value1.length;j++)
                    if(leave_disease_code[i][0].equals(value1[j])) out.print("selected");
                   }
                   %> >
               <%=leave_disease_code[i][0]+":"+leave_disease_code[i][1]%></option>
            <%}}%>
          </select>
         </td>
         <td nowrap class="signText">科室名称</td>

         <td  nowrap class="signText">
          <% String[] value2 = request.getParameterValues("dept_code");
              String[][]  dept_code=(String[][]) request.getAttribute("dept_code");
           %>
          <select name="dept_code"  size='4' multiple>
          <option value="" <%if(value2==null||(value2.length==1&&value2[0].equals(""))||(value2.length>1&&value2[0].equals(""))){
                  out.print("selected");}%>>--不限--</option >
              <%if(dept_code!=null) {
                for(int i=0;i<dept_code.length;i++){%>
                  <option value=<%=dept_code[i][0]%>
                   <% if(value2!=null){
                      for(int j=0;j<value2.length;j++)
                    if(dept_code[i][0].equals(value2[j])) out.print("selected");
                   }
                   %> >
               <%=dept_code[i][0]+":"+dept_code[i][1]%></option>
            <%}}%>
           </select>
     </td>

  </tr>
  <tr>
        <td nowrap class="signText">DRGs分组：</td>
	      <td nowrap class="signText" colspan="2">
            <% String[] value = request.getParameterValues("leave_disease_model");
               String[][] leave_disease_model=(String[][])request.getAttribute("leave_disease_model");
           %>
           <select name="leave_disease_model"  size='4' multiple>
          <option value="" <%if(value2==null||(value2.length==1&&value2[0].equals(""))||(value2.length>1&&value2[0].equals(""))){
                  out.print("selected");}%>>--不限--</option >
                 <%if(leave_disease_model!=null) {
                     for(int i=0;i<leave_disease_model.length;i++){%>
                        <option value=<%=leave_disease_model[i][0]%>
                        <% if(value!=null){
                           for(int j=0;j<value.length;j++)
                           if(leave_disease_model[i][0].equals(value[j])) out.print("selected");
                        }
                        %> >
                     <%=leave_disease_model[i][0]+":"+leave_disease_model[i][1]%>
                  </option>
                <%}}%>
            </select>
	      </td>
       <td>
       <button class="pageBtn" name="" onclick="return find();">查询</button>
       <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
       <td><img src="images/picture.gif" style='cursor:hand' onclick="return draw();" ></td>
  </tr>


	  </html:table>

	  <br>
		<html:title clazz='table'>成本同期对比(科室)表</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->

             <tr><td><%=oper%></td></tr>

		  <!-- 结果集 -->
		  <tr>
		    <td>
		    	<html:table clazz="result">
            <html:tr clazz='label'>
             <td nowrap="nowrap">病种编码</td>
             <td nowrap="nowrap">病种名称</td>
             <td nowrap="nowrap">DRGs分组</td>
             <td nowrap="nowrap">科室名称</td>
             <%
               String[] allYearMonth=(String[])request.getAttribute("allYearMonth");
                  if(allYearMonth!=null){
                   for(int i=0;i<allYearMonth.length;i++){
              %>
              <td nowrap="nowrap"><%=allYearMonth[i]%></td>
             <%}}%>
             <td nowrap="nowrap">差异率</td>
            </html:tr>

		        <%
                if ( result != null ){
		            for (int i = 0; i < result.length; i++ ){
		               String rowColor = "rowGray";
		               if (i/2*2==i) rowColor = "rowWhite";
		        %>
		        <tr CLASS="<%=rowColor%>">
               <td nowrap="nowrap" class="normalText"><%=result[ i ][ 0 ]%></td>
               <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td>
               <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
               <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td>
               <td nowrap="nowrap" class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 4 ]))%></td>
               <td nowrap="nowrap" class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 5 ]))%></td>
               <td nowrap="nowrap" class="numberText"><%=nf1.format(Double.parseDouble(result[ i ][ 6 ]))%></td>
               <% for(int j=0;j<result[i].length;j++){%>

		           <%}%>
		        </tr>
		        <%
		              }
               }
		        %>
		      </html:table>


		    </td>
		  </tr>
	  </html:table>

	  <input type=hidden name="subFunction"/>
	</form>
</html:html>






