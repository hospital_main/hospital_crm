<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/disease_cost_analysis/cost_date_compare/whole/whole.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                 java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%         DecimalFormat nf = new DecimalFormat("#,##0.00");
           BaseRO ro = (BaseRO)request.getAttribute("ro");
           TableMarge oper = new TableMarge(ro, "return find()");
           String[][] result=null;
           if(ro!=null){
             result=ro.getTableResult();
           }
        %>
<Script Language="JavaScript">
function find() {
    var num=0;
    for(var i=0;i<template.leave_disease_code.options.length;i++){
        if(template.leave_disease_code.options[i].selected)
          num++;
    }
    if(num>30){
      alert("病种选择的数目不能大于30个");
      return;
    }

    if(template.year_month_from.value==""){
        alert("请输入年月");
        return;
    }
    if(template.year_month_to.value==""){
        alert("请输入年月");
        return;
    }
    if(template.year_month_from.value>template.year_month_to.value){
        alert("起始年月不可大于终止年月");
        return;
    }
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
  function draw() {
	  <%if(result==null)
	  out.print("return");%>

	  var year_month_from=template.year_month_from.value;
	  var year_month_to=template.year_month_to.value;

	  window.open("whole_diease_cost.jspviewhigh?subFunction=drawchart&year_month_from="+year_month_from+"&year_month_to="+year_month_to,null,"top=180, left=200, height=520,width=600,toolbar=no, location=no")
 	}
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="whole_diease_cost.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>全院病种成本 </html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
  <tr>
    <td nowrap class="normalText">时间范围:</td>
    <td nowrap colspan="4">
        <%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%>
    </td>

  </tr>
  <tr>
     <td nowrap class="normalText">成本类型:</td>
    <td colspan="2" nowrap class="normalText"><input type="radio" name="kind" value="1" <% if(request.getParameter("kind")==null) out.print("checked"); if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("1")) out.print("checked");%>>总成本
   <input type="radio" name="kind" value="2" <%if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("2")) out.print("checked");%>>单位成本</td>
       <td  nowrap class="normalText">就诊类型:</tr>
       <td>
        <select name="hospitalize_id">
             <%String hospitalize_id=request.getParameter("hospitalize_id");%>
             <option value='O' <%if(hospitalize_id!=null&&hospitalize_id.equals("O")) out.print("selected");%>>门诊</option>
             <option value='I' <%if(hospitalize_id!=null&&hospitalize_id.equals("I")) out.print("selected");%>>住院</option>
          </select>
        </td>
  </tr>
  <tr>
      <td  nowrap class="normalText">病种:</td>
      <td  colspan="2">
           <% String[] value1 = request.getParameterValues("leave_disease_code");
              String[][]  leave_disease_code=(String[][]) request.getAttribute("leave_disease_code");
           %>
          <select name="leave_disease_code"  size='4' multiple >
               <option value="" <%if(value1==null||(value1.length==1&&value1[0].equals(""))||(value1.length>1&&value1[0].equals(""))){
                  out.print("selected");}%>>--不限--</option >
              <%if(leave_disease_code!=null) {
                for(int i=0;i<leave_disease_code.length;i++){%>
                  <option value=<%=leave_disease_code[i][0]%>
                   <% if(value1!=null){
                      for(int j=0;j<value1.length;j++)
                    if(leave_disease_code[i][0].equals(value1[j])) out.print("selected");
                   }
                   %> >
               <%=leave_disease_code[i][0]+":"+leave_disease_code[i][1]%></option>
            <%}}%>
          </select>
         </td>
          <td nowrap class="normalText">DRGs分组:</td>
	      <td>
            <% String[] value = request.getParameterValues("leave_disease_model");
               String[][] leave_disease_model=(String[][])request.getAttribute("leave_disease_model");
           %>
           <select name="leave_disease_model"  size='4' multiple>
               <option value="" <%if(value==null||(value.length==1&&value[0].equals(""))||(value.length>1&&value[0].equals(""))){
                  out.print("selected");}%>>--不限--</option >
                 <%if(leave_disease_model!=null) {
                     for(int i=0;i<leave_disease_model.length;i++){%>
                        <option value=<%=leave_disease_model[i][0]%>
                        <% if(value!=null){
                           for(int j=0;j<value.length;j++)
                           if(leave_disease_model[i][0].equals(value[j])) out.print("selected");
                        }
                        %> >
                     <%=leave_disease_model[i][0]+":"+leave_disease_model[i][1]%>
                  </option>
                <%}}%>
            </select>
	      </td>

  </tr>

 <tr>
       <td colspan="3"></td>
       <td>
       <button class="pageBtn" name=""  onclick="return find();" >查询</button>
       <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
       <td><img src="images/picture.gif" style='cursor:hand' onclick="return draw();" ></td>
 </tr>


	  </html:table>

	  <br>
		<html:title clazz='table'>成本时间对比(全院)表</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->

             <tr><td><%=oper%></td></tr>

		  <!-- 结果集 -->
		  <tr>
		    <td>
		    	<html:table clazz="result">
            <html:tr clazz='label'>
             <td nowrap="nowrap">病种编码</td>
             <td nowrap="nowrap">病种名称</td>
             <td nowrap="nowrap">DRGs分组</td>
             <%
               String[] allYearMonth=(String[])request.getAttribute("allYearMonth");
                  if(allYearMonth!=null){
                   for(int i=0;i<allYearMonth.length;i++){
              %>
              <td nowrap="nowrap"><%=allYearMonth[i]%></td>
             <%}}%>
            </html:tr>

		        <%
           if ( result != null ){
		            for (int i = 0; i < result.length; i++ ){
		               String rowColor = "rowGray";
		               if (i/2*2==i) rowColor = "rowWhite";
		        %>
		        <tr CLASS="<%=rowColor%>">
               <td nowrap="nowrap" class="normalText"><%=result[ i ][ 0 ]%></td>
               <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td>
               <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
               <% for(int j=3;j<result[i].length;j++){%>
               <td nowrap="nowrap" class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ j ]))%></td>
		           <%}%>
		        </tr>
		        <%
                 }
              }

		        %>
		      </html:table>

		    </td>
		  </tr>
	  </html:table>

	  <input type=hidden name="subFunction"/>
	</form>
</html:html>




