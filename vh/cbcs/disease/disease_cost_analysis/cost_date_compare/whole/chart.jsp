<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/disease_cost_analysis/cost_date_compare/whole/chart.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:58:00 $
  $Date: 2012/03/12 01:58:00 $
	$Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<script language='javascript' src='javascript/chart.js'></script>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>

<Script Language="JavaScript">
	function draw() {
		show_wait();  
		template.submit();
		return true;
 	}

  var cewolf1 = "";
</Script>

<html:html leftMargin="0">
	<form name="template" method="get" action="cost.jspviewhigh">
		<table width="100%">
			<tr>
				<td align="center">
					<button class="pageBtn" onclick="return saveChart()">保存</button>
					<button class="pageBtn" onClick="window.close()" >关闭</button>
				</td>
		  </tr>
			<tr>
			 	<td align="center">
	<% 
		String[][] result = (String[][]) request.getSession().getAttribute("chart_result");
		String[] allYearMonth = com.viewhigh.cbcs.cbcs.itemcheckcount.itemcostcollect.base.MonthYearArray.getYearMonth(request);
		ChartUtilities.createChartLine("cewolf1", result, new int[]{1, 3}, allYearMonth, pageContext);
	%>
		<cewolf:chart id="chart" title="全院病种成本" type="line">
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="580" height="450" id="cewolf1"/>
				</td>
			</tr>
		</table>
	</form>
</html:html>


