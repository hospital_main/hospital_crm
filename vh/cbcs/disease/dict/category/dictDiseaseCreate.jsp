<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/dict/category/dictDiseaseCreate.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-09-02 10:08 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
    //template.subFunction.value='create';
    if(isEmpty(template.disease_code))
    {
      alert('疾病编号不能为空!');
      return;
    }
    if(isEmpty(template.disease_name))
    {
      alert('疾病名称不能为空!');
      return;
    }
    if(isEmpty(template.disease_kind))
    {
      alert('疾病类别不能为空!');
      return;
    }
    if(isTooLong(template.disease_code,10))
    {
      alert('疾病编号不能高于10个字符!');
      return;
    }
    if(isTooLong(template.disease_name,30))
    {
      alert('疾病名称不能高于30个字符!');
      return;
    }
    if(isTooLong(template.disease_kind,4))
    {
      alert('疾病类别不能高于4个字符!');
      return;
    }
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictdisease.jspviewhigh">
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>病种定义添加页面</html:title>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">疾病编号：</td>
      <td width="75%" class="signText" nowrap="nowrap"><input type=text name="disease_code" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">疾病名称：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="disease_name" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">疾病类别：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="disease_kind" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">是否参与核算：</td>
      <td class="signText" nowrap="nowrap">
		    是<input type="radio" name="audit_if" value="Y" checked/>
        否<input type="radio" name="audit_if" value="N"/>
		  </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">核算方法：</td>
      <td class="signText" nowrap="nowrap"><%=new Select(request.getAttribute("comp_method"),"comp_method","",true,true)%></td>
    </tr>
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>
