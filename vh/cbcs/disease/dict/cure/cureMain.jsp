<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/dict/cure/cureMain.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
    function create() {
        template.subFunction.value='prevcreate';
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('确定删除')) {
          template.subFunction.value='remove';
          show_wait();
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请选择删除方案");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();return true;
  }
  function check(){
   if(!isNumber(template.rx_income)){
     alert('请在治疗收入中输入数字。');
     return false;
     }
   template.subFunction.value='findAll';
   show_wait();
   template.submit();
   return true;

  }
  function check_match(){
       if(!isNumber(template.rx_income)){
     alert('请在治疗收入中输入数字。');
     return ;
     }
   }
  function show_match(){
    match.style.visibility="visible";
    bx.style.visibility="visible";}
</Script>

<html:html clazz="main">
  <form name="template" method="post"  action="dictdisease_cure.jspviewhigh">
	  <html:message/>

	  <html:title clazz='module'>治疗方案定义</html:title>


  <!-- 结果集 -->
  <html:table clazz="simple">
    	<tr>
      <td nowrap class="signText">疾病名称:</td>
      <% String income_subj_code = request.getParameter("dict_disease_code");%>
      <td class="signText"><%=new  Select(request.getAttribute("disname"), "dict_disease_code",income_subj_code, true, false)%></td>
      <td nowrap class="signText">疾病分型:</td>
      <% String income_subj_model =  request.getParameter("dict_disease_model_code");%>
      <td class="signText" ><%=new  Select(request.getAttribute("distype"), "dict_disease_model_code",income_subj_model, true, false)%></td>
      </tr>
      <tr>
      <td nowrap class="signText">平均治疗收入:</td>
      <% String income_subj_income = request.getParameter("rx_income");%>
      <td nowrap class="signText"><input   type=text  maxlength="20" onblur="check_match()" name="rx_income" class="textInputC" <%if(income_subj_code!= null){ out.println(" value=" + income_subj_income.trim());}%> >
      </td>
        <%String[][] meth=new String[3][2];
        meth[0][0]="";meth[0][1]="等于";
        meth[1][0]="over";meth[1][1]="大于";
        meth[2][0]="under";meth[2][1]="小于";
         out.print("<td><div id=\"bx\" style=\"visibility: visible;\" >大/小:</div></td><td><div id=\"match\" style=\"visibility: visible;\" >"+new  Select(meth, "desc",request.getParameter("desc"), false, true)+" </div>"); %>
      </td>
      </tr>
      <tr>
      <td colspan="3">  </td>
      <td>
      <button class="pageBtn" name=""  onclick="check();"  >查询</button>
       <!--<img src="images/find.gif" class="mouse" onclick="check();" />-->
        <input type="text" disabled="disabled" size="0" style="width:0px;hight:0px">
      </td>
    </tr>
  </html:table>

  <br>

   <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro != null) {
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   //
      oper.addOptionButton("images/reset.gif", "return reset()");     //
      oper.addOptionButton("images/remove.gif", "return remove()");   // shanchu
      oper.addNeedButton("images/create.gif", "return create()");     //   tainjia
  %>

  <html:title clazz='table'>治疗方案</html:title>

  <html:table clazz="complex">

    <tr><td><%=oper%></td></tr>


  <tr>
    <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td nowrap="nowrap" class="resultLabel">选择</td>
          <td nowrap="nowrap" class="resultLabel">疾病编号</td>
          <td nowrap="nowrap" class="resultLabel">疾病名称</td>
          <td nowrap="nowrap" class="resultLabel">疾病分型</td>
          <td nowrap="nowrap" class="resultLabel">疾病分型名称</td>
          <td nowrap="nowrap" class="resultLabel">平均治疗收入</td>
          <td nowrap="nowrap" class="resultLabel">平均治疗成本</td>
        </html:tr>

        <%
          String[][] result = ro.getTableResult();
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }
            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey+"+"+result[i][2]%>"></td>
          <td nowrap="nowrap" class="normalText"><a href="dictdisease_cure.jspviewhigh?subFunction=preparedSave&pk=<%=primaryKey%>&type=<%=result[i][2]%>"><%=primaryKey%></a></td>
          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1]%></td>
          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td>
          <td nowrap="nowrap" class="numberText"><%= changeFormat(Double.parseDouble(result[ i ][ 4 ]))%></td>
          <td nowrap="nowrap" class="numberText"><%= changeFormat(Double.parseDouble(result[ i ][ 5 ]))%></td>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>

  <!-- 按钮栏 -->
  </html:table>
	<%}%>
  <input type=hidden name="subFunction" value="findAll"/>
</form>
</html:html>
