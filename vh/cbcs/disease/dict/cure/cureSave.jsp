<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/dict/cure/cureSave.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
                com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
 function create()
 {
    //template.subFunction.value='create';
     if(isTooLong(template.rx,200)){
    alert('治疗方案应少于200个字符，100个汉字');
    return；
    }
    if(isEmpty(template.rx))
    {
        alert('请输入方案');
        return;
    }
   if(!isNumber(template.income))
    {
      alert('平均治疗收入必须为数字!');
      return;
    }

    switch (isDouble(template.income, 8, 2)) {
      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于8位');
        return false;
      case 3 :
        alert('金额小数部分不能高于2位');
        return false;
    }
   if(!isNumber(template.cost))
    {
      alert('平均治疗成本必须为数字!');
      return;
    }

    switch (isDouble(template.cost, 8, 2)) {
      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于8位');
        return false;
      case 3 :
        alert('金额小数部分不能高于2位');
        return false;
    }
    template.submit();
    return true;
  }

  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';

    element.submit();
  }
</Script>
<html:html clazz="main">
<%String [][] result=(String [][])request.getAttribute("result");%>
<form name="template" method="post" action="dictdisease_cure.jspviewhigh">
  <!-- 信息 -->
  <html:message/>

  <!-- 标题 -->
  <html:title clazz='module'>方案修改页面</html:title>

  <!-- 简单信息 -->
  <html:table clazz='simple' >

		<tr>
      <td class="signText" nowrap="nowrap">疾病名称:</td>
      <td class="signText"><input type=text name="dnname" readonly class="textInputC" maxlength="20"value="<%=result[0][1]%>"/> </td>
    </tr>
		<tr>
      <td class="signText" nowrap="nowrap">疾病分型:</td>
      <td class="signText"   ><input type=text name="dtype"  readonly class="textInputC" maxlength="20"value="<%= result[0][2]%>"/> </td>
    </tr>
		<tr>
      <td class="signText" nowrap="nowrap">治疗方案(100个汉字):</td>
      <td   class="signText" nowrap="nowrap"><textarea name="rx" rows="5"  cols="42"  ><%=result[0][6]%> </textarea></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">平均治疗收入:</td>
      <td class="signText" nowrap="nowrap"><input type=text name="income" class="textInputC" maxlength="11"value="<%=result[0][4]%>"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">平均治疗成本:</td>
          <td class="signText" nowrap="nowrap"><input type=text name="cost" class="textInputC" maxlength="11"value="<%=result[0][5]%>"/>

      </td>
    <tr>
    </tr>

    <tr>
      <td colspan="2">
      <button class="pageBtn" onclick="return create();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button><button class="pageBtn" onclick="return back(template);">返回</button>   
      <!--<img src="images/save.gif" class="mouse" onclick="return create();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </html:table>
  <input type=hidden name="subFunction" value="Save"/>
   <input type=hidden name="dname" value="<%=result[0][0]%>"/>
</form>

</html:html>
