<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/dict/cure/cureCreate.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
 function create()
 {
   if(template.desname.value==""){
     alert('疾病名称不能为空');
     return;
     }
      if(isTooLong(template.rx,200)){
    alert('治疗方案应少于200个字符，100个汉字');
    return;
    }

       if(!isNumber(template.income))
    {
      alert('平均治疗收入必须为数字!');
      return;
    }

    switch (isDouble(template.income, 8, 2)) {
      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于8位');
        return false;
      case 3 :
        alert('金额小数部分不能高于2位');
        return false;
    }

    if(!isNumber(template.cost))
    {
      alert('平均治疗成本必须为数字!');
      return;
    }

    switch (isDouble(template.income, 8, 2)) {
      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于8位');
        return false;
      case 3 :
        alert('金额小数部分不能高于2位');
        return false;
    }

    template.submit();
    return true;
  }

  // \u8FD4\u56DE
  function back( element )
  {
   for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';

    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_cure.jspviewhigh">
  <!-- 信息 -->
  <html:message/>

  <!-- 标题 -->
  <html:title clazz='module'>治疗方案添加页面</html:title>

  <!-- 简单信息 -->
  <html:table  clazz='simple' >
       <%String dename=(String)request.getAttribute("desname");
         String decode=(String)request.getAttribute("destype");
         String initn="";String initc="";
         if (dename!=null){initn=dename;}
         if (decode!=null){initc=decode;}%>
		<tr>
      <td class="signText" nowrap="nowrap">疾病名称:</td>
      <td class="signText"><%=new  Select(request.getAttribute("disname"), "desname", initn , true, true)%></td>
    </tr>
		<tr>
      <td class="signText" nowrap="nowrap">疾病分型:</td>
      <td class="signText"   ><%=new  Select(request.getAttribute("distype"), "destype", initc, true, true)%></td>
    </tr>
		<tr>
      <td class="signText" nowrap="nowrap">治疗方案(100个汉字):</td>
      <td   class="signText" nowrap="nowrap"><textarea name="rx" rows="5" cols="42"><%if(request.getParameter("rx")!=null){out.print(request.getParameter("rx"));}else{%>在此输入治疗方案<%}%></textarea></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">平均治疗收入:</td>
      <td class="signText" nowrap="nowrap"><input type=text name="income" class="textInputC" maxlength="11"<%if(request.getParameter("income")!=null) out.print("value=\""+request.getParameter("income")+"\"");%>/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">平均治疗成本:</td>
          <td class="signText" nowrap="nowrap"><input type=text name="cost" class="textInputC" maxlength="11"<%if(request.getParameter("cost")!=null) out.print("value=\""+request.getParameter("cost")+"\"");%>/>

      </td>
    <tr>
    </tr>

    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </html:table>
  <input type=hidden name="subFunction" value="create"/>
  <input type=hidden name="sign1" value="a"/>

</form>

</html:html>
