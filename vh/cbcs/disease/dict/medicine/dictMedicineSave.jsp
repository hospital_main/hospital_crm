<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/dict/medicine/dictMedicineSave.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    if(isEmpty(template.med_name))
    {
      alert('药品名称不能为空!');
      return;
    }
    if(isEmpty(template.med_model))
    {
      alert('规格型号不能为空!');
      return;
    }
    if(isEmpty(template.unit_code))
    {
      alert('计量单位不能为空!');
      return;
    }
    if(isEmpty(template.med_type_code))
    {
      alert('药品分类名称不能为空!');
      return;
    }
    template.subFunction.value='save';
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_medicine.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module"> 药品信息修改页面</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">药品编号：</td>
      <td width="75%" class="signText" nowrap="nowrap">
        <input type=text   value="<%=result[0]%>" disabled />
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">药品名称：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="med_name" value="<%=result[1]%>" class="textInputC">
      </td>
    <tr>
      <td class="signText" nowrap="nowrap">规格型号：</td>
      <td class="signText" nowrap="nowrap">
        <input type=text name="med_model" value="<%=result[2]%>" class="textInputC">
      </td>
    <tr>
      <td class="signText" nowrap="nowrap">计量单位：</td>
      <td class="signText" nowrap="nowrap">
      	<%=new  Select(request.getAttribute("unit_code"), "unit_code",result[6], true, true)%>
      </td>
    <tr>
      <td class="signText" nowrap="nowrap">药品分类名称：</td>
      <td class="signText" nowrap="nowrap">
      	<%=new  Select(request.getAttribute("med_type"), "med_type_code",result[5], true, true)%>        
      </td>
    <tr>
      <td class="signText" nowrap="nowrap">药品成本分类：</td>
      <td class="signText" nowrap="nowrap"><%=new  Select(request.getAttribute("med_cost_subj"), "med_cost_subj",result[7], true, true)%></td>
    </tr>
    <tr>
      <td colspan="2">
      <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="med_code" value="<%=result[0]%>">
  <%}%>
</form>


</html:html>
