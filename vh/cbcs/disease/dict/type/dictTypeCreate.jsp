<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/dict/type/dictTypeCreate.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
        template.subFunction.value='create';
    if(template.med_type_code.value==""){
     alert('药品分类编号不能为空！');
     return;
     }       
    if(template.med_type_name.value==""){
     alert('药品分类名称不能为空！');
     return;
     }  
			show_wait();		
			template.submit();
			return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_type.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>药品分类添加页面</html:title>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
  <%
  	String  med_type_code=request.getParameter("med_type_code")==null ? "":request.getParameter("med_type_code");
  	String  med_type_name=request.getParameter("med_type_name")==null ? "":request.getParameter("med_type_name");
  	String  supper_code=request.getParameter("supper_code")==null ? "":request.getParameter("supper_code");
  %>
    <tr>
      <td class="signText" nowrap="nowrap">药品分类编号：</td>
      <td width="75%" class="signText" nowrap="nowrap"><input type=text name="med_type_code" class="textInputC" value="<%=med_type_code%>"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">药品分类名称：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="med_type_name" class="textInputC"  value="<%=med_type_name%>"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">上级代码：</td>
      <td class="signText" nowrap="nowrap"><input type=text name="supper_code" class="textInputC" value="<%=supper_code%>"/></td>
    </tr>   
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   