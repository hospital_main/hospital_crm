<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/dict/path/dictPathCreate.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
	function subcreate(){
		if(trim(template.disease_code.value)=="")
	   {
	     alert('病种名称不能为空');
	     return;
	   }
	   if(trim(template.drg_code.value)=="")
	   {
	     alert('DRG编码不能为空');
	     return;
	   }
		if(template.disease_code.readOnly==true)
		{
			window.open("dictdisease_path.jspviewhigh?subFunction=presubcreate&disease_code="+template.disease_code.value+"&disease_name="+template.disease_name.value+"&disease_model="+template.drg_code.value, "","status=no,left=350,top=200,Height=300,width=500");
		}
		else
		{
			alert("请先保存!")
			return
		}
	}
 function create()
 {
   if(trim(template.disease_code.value)=="")
   {
     alert('病种名称不能为空');
     return;
   }
   if(trim(template.drg_code.value)=="")
   {
     alert('DRG编码不能为空');
     return;
   }
  if(!isNumber(template.hos_level)){
	   alert("医院级别应为整数");
	   return;
   }
    
   if(!isNumber(template.In_day)){
	   alert("住院天数应为整数");
	   return;
   }
   else
   {
   if(trim(template.In_day.value)=='')
	   {
	   		template.In_day.value=0;
	   }
   }  
		show_wait();
    template.submit();
    return true;
  }
function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('确定删除')) {
          template.subFunction.value='subremove';
          show_wait();
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请选择删除方案");
      return false;
    }
  }
    function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }
  function back( element )
  {
   for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';

    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_path.jspviewhigh">
  <!-- 信息 -->
  <html:message/>

  <!-- 标题 -->
  <html:title clazz='module'>标准临床路径添加页面</html:title>

  <!-- 简单信息 -->
  <html:table  clazz='simple' >
		<tr>
    	<% 
    	String disease_code = request.getParameter("disease_code");
    	String drg_code 		= request.getParameter("drg_code");
    	String hos_level 		= request.getParameter("hos_level");
    	String hos_type		 	= request.getParameter("hos_type");
    	String make_dept 		= request.getParameter("make_dept");
    	String In_day		 		= request.getParameter("In_day");
    	String disease_name = (String)request.getAttribute("disease_name");
    	%>
      <td nowrap class="signText">病种名称:</td>
      <td nowrap class="signText"><%if(request.getAttribute("mainstat")==null){%>
      <%=new  Select(request.getAttribute("disease_code"), "disease_code", disease_code, true, true)%> 
      		<%}else{%>
      <input type=hidden name="disease_code" readonly <%if(disease_code != null){ out.println(" value=" + disease_code);}%>/>		
      <input type=text name="disease_name" class="textInputC" value="<%=disease_name==null? "":disease_name%>"/>
      		<%}%>
      </td>      
      <td nowrap class="signText">DRG编码:</td>
      <td nowrap class="signText"><%if(request.getAttribute("mainstat")==null){%>
      <%=new  Select(request.getAttribute("drg_code"), "drg_code", drg_code, true, true)%> 
      <%}else{%>
      <input   type=text  maxlength="20" name="drg_code" class="textInputC" <%if(drg_code!= null){ out.println(" value=" + drg_code.trim());}%>>
      <%}%>
      </td>      
    </tr>
    <tr>
      <td nowrap class="signText">医院级别:</td>
      <td nowrap class="signText"><input type=text name="hos_level" class="textInputC" <%if(hos_level != null){ out.println(" value=" + hos_level);}%>></td>
      <td nowrap class="signText">医院性质:</td>
      <td nowrap class="signText"><input type=text name="hos_type" class="textInputC" <%if(hos_type!= null){ out.println(" value=" + hos_type);}%>></td>  
    </tr>
    <tr>
      <td nowrap class="signText">制定单位:</td>
      <td nowrap class="signText"><input type=text name="make_dept" class="textInputC" <%if(make_dept != null){ out.println(" value=" + make_dept);}%>></td>
     <!--
      <td><%=new  Select(request.getAttribute("make_dept"), "make_dept", make_dept, true, true)%> </td>
      --> 
      <td nowrap class="signText">住院天数:</td>
      <td nowrap class="signText"><input type=text name="In_day" class="textInputC" <%if(In_day!= null){ out.println(" value=" + In_day);}%>></td>  
    </tr>
    <tr>
      <td/><td/><td/>
      <td align=center><img src='images/tianjia-1.gif' class='mouse' onclick='return create()' /><img src='images/return1.gif' class='mouse' onclick='return back(template);' /></td>
      </tr>
  </html:table>
  <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro != null) {
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   //
      oper.addOptionButton("images/reset.gif", "return reset()");     //
      oper.addOptionButton("images/remove.gif", "return remove()");   // shanchu
      oper.addNeedButton("images/create.gif", "return subcreate()");     //   tainjia
  %>

  <html:table clazz="complex">

    <tr><td><%=oper%></td></tr>


  <tr>
    <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td nowrap="nowrap" class="resultLabel">选择</td>
          <td nowrap="nowrap" class="resultLabel">项目分类</td>
          <td nowrap="nowrap" class="resultLabel">项目编码</td>
          <td nowrap="nowrap" class="resultLabel">项目名称</td>
          <td nowrap="nowrap" class="resultLabel">计量单位</td>
          <td nowrap="nowrap" class="resultLabel">数量</td>
        </html:tr>

        <%
          String[][] result = ro.getTableResult();
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 2 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }
            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=(result[i][0]+"+"+result[i][1]+"+"+primaryKey+"+"+result[i][3])%>"></td>
          <td nowrap="nowrap" class="normalText"><a href="#" onclick="window.open('dictdisease_path.jspviewhigh?subFunction=preSubSave&pk=<%=(result[i][0]+"+"+result[i][1]+"+"+primaryKey+"+"+result[i][3])%>','','top=200,left=350,width=450,height=300,status=no')"><%if(primaryKey.trim().equals("1")){out.print("医疗项目");}else{out.print("药品");}%></a></td>
          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td>
          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 6 ]%></td>
          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 5 ]%></td>
          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 4 ]%></td>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>

  <!-- 按钮栏 -->
  </html:table>
	<%}%>
  
  <input type=hidden name="subFunction" value="create"/>
  <input type=hidden name="sign1" value="a"/>
  <input type=hidden name="sign2" value="create"/>
  <input type=hidden name="mainstat" value="<%=request.getAttribute("mainstat")==null ? "insert":"update"%>"/>
  

</form>

</html:html>
<script>
  if(template.mainstat.value=="update"){
  template.drg_code.readOnly=true
  template.disease_code.readOnly=true
  }
</script>