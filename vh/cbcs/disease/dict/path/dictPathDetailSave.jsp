<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/dict/path/dictPathDetailSave.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
 function save()
 {	
    if(!isNumber(template.unit_code)){
	   alert("计量单位应为整数");
	   return;
      }
    
     if(!isNumber(template.amount)){
	   alert("数量应为整数");
	   return;
     }
     
     
 		show_wait();
    template.submit();
    window.opener.create();
    return true;
  }

  function back( element )
  {
   for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';

    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_path.jspviewhigh">
  <!-- 信息 -->
  <html:message/>

  <!-- 标题 -->
  <html:title clazz='module'>标准临床路径修改子页面</html:title>

  <!-- 简单信息 -->
  <html:table  clazz='simple' >
       <%
       	 String[] result=(String[])request.getAttribute("result");
       	 String disease_code=result[0];
         String disease_model=result[1];
         String unit_code=result[5];
         String disease_name=result[7];
         String amount=result[4];
         String charge_detail_code="";String med_code="";
         if (result[2].equals("1"))
         	{charge_detail_code=result[3];}
         if (result[2].equals("2"))
         	{med_code=result[3];}
       %>
		<tr>
      <td class="signText" nowrap="nowrap">病种编号:</td>
      <td class="signText">
      <input type=hidden name="disease_code" readonly value="<%=disease_code%>"/>
      <input type=text name="disease_name" readonly class="textInputC" maxlength="11"<%if(disease_name!=null) out.print("value=\""+disease_name+"\"");%>/></td>
    </tr>
		<tr>
      <td class="signText" nowrap="nowrap">DRG编码:</td>
      <td class="signText"><input type=text name="disease_model" readonly class="textInputC" maxlength="11"<%if(disease_model!=null) out.print("value=\""+disease_model+"\"");%>/></td>
    </tr>
    <%if(result[2].equals("1")){%>
		<tr>
      <td class="signText" nowrap="nowrap">
      	<INPUT type=radio name="radio" CHECKED value="charge">医疗项目:
      </td>
      <td class="signText" nowrap="nowrap">
  			<input type=hidden name="charge_detail_code" <%if(charge_detail_code!=null) out.print("value=\""+charge_detail_code+"\"");%>/>
      	<input type=text name="charge_detail_codes" readonly class="textInputC" maxlength="11" <%if(charge_detail_code!=null) out.print("value=\""+result[6]+"\"");%>/>
      </td>
    </tr>
    <%}else{%>
    <tr>
      <td class="signText" nowrap="nowrap">
      	<INPUT type=radio name="radio" CHECKED value="med">药品:
      </td>
      <td class="signText" nowrap="nowrap">
  			<input type=hidden name="med_code" <%if(med_code!=null) out.print("value=\""+med_code+"\"");%>/>
      	<input type=text name="med_codes" readonly class="textInputC" maxlength="11"<%if(med_code!=null) out.print("value=\""+result[6]+"\"");%>/>
      </td>
    </tr>
    <%}%>
    <tr>
      <td class="signText" nowrap="nowrap">计量单位:</td>
          
          <!--
          <input type=text name="unit_code" class="textInputC" maxlength="11" <%if(unit_code!=null) out.print("value=\""+unit_code+"\"");%>/>
          -->
	  <td><%=new  Select(request.getAttribute("unit_code"), "unit_code", unit_code==null? "":unit_code, true, true)%></td>
      
     </tr>
     <tr>
      <td class="signText" nowrap="nowrap">数量:</td>
          <td class="signText" nowrap="nowrap"><input type=text name="amount" class="textInputC" maxlength="11" <%if(amount!=null) out.print("value=\""+amount+"\"");%>/>

      </td>
    <tr>
    </tr>

    <tr>
      <td colspan="2">
      <button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="window.close();" >返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="window.close();" />--> </td>
    </tr>
  </html:table>
  <input type=hidden name="subFunction" value="subsave"/>  
  <input type=hidden name="sign1" value="a"/>

</form>

</html:html>
