<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/dict/path/dictPathDetailCreate.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
		com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<link type="text/css" rel="stylesheet" href="../../../../newVh.css">
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
 function create()
 {	
    if(!isNumber(template.unit_code)){
	   alert("计量单位应为整数");
	   return;
      }
    
     if(!isNumber(template.amount)){
	   alert("数量应为整数");
	   return;
     }
    if(template.radio[1].checked&&template.charge_detail_code.value==""){
    	alert("请选择医疗项目");
    	return;
    }
    show_wait();
    template.submit();
    window.opener.create();
    return true;
  }

  function back( element )
  {
   for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';

    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictdisease_path.jspviewhigh">
  <!-- 信息 -->
  <html:message/>

  <!-- 标题 -->
  <html:title clazz='module'>标准临床路径添加子页面</html:title>

  <!-- 简单信息 -->
  <html:table  clazz='simple' >
       <%String dename=(String)request.getAttribute("desname");
         String decode=(String)request.getAttribute("destype");
         String initn="";String initc="";
         if (dename!=null){initn=dename;}
         if (decode!=null){initc=decode;}%>
		<tr>
      <td class="signText" nowrap="nowrap">病种编号:</td>
      <td class="signText"><input type=text name="disease_name" readonly class="textInputC" maxlength="11"<%if(request.getAttribute("disease_name")!=null) out.print("value=\""+request.getAttribute("disease_name")+"\"");%>/>
      						  <input type=hidden readonly name="disease_code" value="<%=request.getAttribute("disease_code")%>"/>
      </td>
    </tr>
		<tr>
      <td class="signText" nowrap="nowrap">DRG编码:</td>
      <td class="signText"><input type=text name="disease_model" readonly class="textInputC" maxlength="11"<%if(request.getAttribute("disease_model")!=null) out.print("value=\""+request.getAttribute("disease_model")+"\"");%>/></td>
    </tr>		
    <tr>
      <td class="signText" nowrap="nowrap">
      	<INPUT type=radio name="radio" CHECKED value="med">药品:
      </td>
      <td class="signText" nowrap="nowrap">
      	<%=new  Select(request.getAttribute("med_code"), "med_code", initc, true, true)%>
	 <INPUT type=radio name="radio" value="charge">医疗项目:
      </td>
      <td class="signText" nowrap="nowrap">
      <?xml:namespace prefix="hzh"/>
        <hzh:QInput name="charge_detail_code" ID="nosName" init="1" previousObj="disease_model" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="160" left="305" top="40" Lheight="5" xmlSource="dic/dict_charge_detail_LV.xml"/>
        </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">计量单位:</td>
         	  <td><%=new  Select(request.getAttribute("unit_code"), "unit_code", "", true, true)%></td>
       </tr>
      <tr>
      <td class="signText" nowrap="nowrap">  数量:</td>
          <td class="signText" nowrap="nowrap"><input type=text name="amount" class="textInputC" maxlength="11"/>

      </td>
    <tr>
    </tr>

    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button><button class="pageBtn" onclick="window.close();">返回</button>  
     <!-- <img src="images/create.gif" class="mouse" onclick="return create();" />  <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="window.close();" />--></td>
    </tr>
  </html:table>
  <input type=hidden name="subFunction" value="subcreate"/>
  <input type=hidden name="sign1" value="a"/>

</form>

</html:html>
