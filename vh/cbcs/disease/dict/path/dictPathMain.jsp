<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/dict/path/dictPathMain.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
    function create() {
    		template.disease_code.value="";
    		template.disease_name.value="";
    		template.drg_code.value="";
    		template.drg_note.value="";
    		
        template.subFunction.value='prevcreate';
        show_wait();
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('确定删除')) {
          template.subFunction.value='remove';
          show_wait();
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请选择删除方案");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();return true;
  }
  function check(){
   template.subFunction.value='findAll';
   show_wait();
   template.submit();
   return true;
  }
  function check_match(){
       if(!isNumber(template.rx_income)){
     alert('请在治疗收入中输入数字。');
     return ;
     }
   }
  function show_match(){
    match.style.visibility="visible";
    bx.style.visibility="visible";}
</Script>

<html:html clazz="main">
  <form name="template" method="post"  action="dictdisease_path.jspviewhigh">
	  <html:message/>

	  <html:title clazz='module'>标准临床路径</html:title>


  <!-- 结果集 -->
  <html:table clazz="simple">
    	<tr>
    	<% 
    	String disease_code = request.getParameter("disease_code");
    	String disease_name = request.getParameter("disease_name");
    	String drg_code 		= request.getParameter("drg_code");
    	String drg_note		 	= request.getParameter("drg_note");
    	%>
      <td nowrap class="signText">病种编号:</td>
      <td nowrap class="signText"><input type=text name="disease_code" class="textInputC" <%if(disease_code != null){ out.println(" value=" + disease_code);}%>></td>
      <td nowrap class="signText">病种名称:</td>
      <td nowrap class="signText"><input type=text name="disease_name" class="textInputC" <%if(disease_name != null){ out.println(" value=" + disease_name);}%>></td>
      </tr>
      <tr>
      <td nowrap class="signText">DRG编码:</td>
      <td nowrap class="signText"><input   type=text  maxlength="20" name="drg_code" class="textInputC" <%if(drg_code!= null){ out.println(" value=" + drg_code.trim());}%> >
      </td>
      <td nowrap class="signText">DRG描述:</td>
      <td nowrap class="signText"><input type=text name="drg_note" class="textInputC" <%if(drg_note!= null){ out.println(" value=" + drg_note);}%>></td>  
      </tr>
      <tr>
      <td colspan="3">  </td>
      <td nowrap class="signText"> 
      <button class="pageBtn" name="" onclick="check();" >查询</button>
     <!-- <img src="images/find.gif" class="mouse" onclick="check();" />-->
        <input type="text" disabled="disabled" size="0" style="width:0px;hight:0px">
      </td>
    </tr>
  </html:table>

  <br>

   <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro != null) {
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   //
      oper.addOptionButton("images/reset.gif", "return reset()");     //
      oper.addOptionButton("images/remove.gif", "return remove()");   // shanchu
      oper.addNeedButton("images/create.gif", "return create()");     //   tainjia
  %>

  <html:title clazz='table'>标准临床路径</html:title>

  <html:table clazz="complex">

    <tr><td><%=oper%></td></tr>


  <tr>
    <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td nowrap="nowrap" class="resultLabel">选择</td>
          <td nowrap="nowrap" class="resultLabel">病种编码</td>
          <td nowrap="nowrap" class="resultLabel">名称</td>
          <td nowrap="nowrap" class="resultLabel">DRG 编码</td>
          <td nowrap="nowrap" class="resultLabel">医院级别</td>
          <td nowrap="nowrap" class="resultLabel">医院性质</td>
          <td nowrap="nowrap" class="resultLabel">制定时间</td>
          <td nowrap="nowrap" class="resultLabel">制定单位</td>
        </html:tr>

        <%
          String[][] result = ro.getTableResult();
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }
            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey+"+"+result[i][2]%>"></td>
          <td nowrap="nowrap" class="normalText"><a href="dictdisease_path.jspviewhigh?subFunction=preSave&pk=<%=primaryKey+"+"+result[i][2]%>"><%=primaryKey%></a></td>
          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1]%></td>
          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td>
          <td nowrap="nowrap" class="normalText"><%= result[ i ][ 4 ]%></td>
          <td nowrap="nowrap" class="normalText"><%= result[ i ][ 5 ]%></td>
          <td nowrap="nowrap" class="normalText"><%= result[ i ][ 6 ]%></td>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>

  <!-- 按钮栏 -->
  </html:table>
	<%}%>
  <input type=hidden name="subFunction" value="findAll"/>
</form>
</html:html>
