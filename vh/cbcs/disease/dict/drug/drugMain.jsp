<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/dict/drug/drugMain.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript">
  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('确认删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "选择删除内容");
      return false;
    }
  }
  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }
  function create(){
     template.subFunction.value='preparedCreate';
     template.submit();return true;
}
  function find() {
    template.subFunction.value='findAll';
    template.submit();return true;
  }
</Script>
	<html:html clazz="main">
	<form name="template" method="post" action="dictdisease_drug.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>药品项目定义</html:title>
  <br>

   <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro != null) {
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   //
      oper.addOptionButton("images/reset.gif", "return reset()");     //
      oper.addOptionButton("images/remove.gif", "return remove()");
      oper.addNeedButton("images/create.gif", "return create()");
   %>

  <html:title clazz='table'></html:title>

  <html:table clazz="complex">

    <tr><td><%=oper%></td></tr>


  <tr>
    <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
					<td nowrap="nowrap" class="resultLabel">选择</td>
          <td nowrap="nowrap" class="resultLabel">药品成本项</td>
          <td nowrap="nowrap" class="resultLabel">收费项</td>

        </html:tr>

        <%
          String[][] result = ro.getTableResult();
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=result[i][2]+"+"+result[i][3]%>"></td>
          <td nowrap="nowrap" class="normalText"><%=primaryKey%></td>
          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1]%></td>

        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>
  </html:table>
	<%}%>
  <input type=hidden name="subFunction" />
</form>
</html:html>
