<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/disease/dict/drug/drugCreate.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%String [][] cost=(String [][])request.getAttribute("cost") ;%>
<Script Language="JavaScript">

   function create(){
      template.subFunction.value='create';
      template.submit();
  }

   function back() {
		for(var i=0;i<template.elements.length;i++)
    	template.elements[i].value="";
    template.subFunction.value='findAll';
    template.submit();
  }
	</Script>

	<html:html clazz="main">
	<form name="template" method="post" action="dictdisease_drug.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>药品项目定义添加页面</html:title>

	<html:table clazz="simple">

  	<tr>
  		<td>药品成本项：</td>
	  	<td>
				<%=new Select(request.getAttribute("field_name_init"), "field_name", request.getParameter("field_name"), false, true)%>
			</td>
		</tr>
		<tr>
  		<td>成本项目：</td>
      <td> <%=new Select(request.getAttribute("cost_subj_code_init"), "cost_subj_code", request.getParameter("cost_subj_code"), true, true)%></td><td></td>
    </tr>

		<tr>
      <td><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return back();">返回</button> 
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/return.gif" class="mouse" onclick="return back();">--></td>
    </tr>

	</html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>
