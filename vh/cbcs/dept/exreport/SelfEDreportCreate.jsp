<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/exreport/SelfEDreportCreate.jsp,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Modtime:  $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
String[][] name=(String[][])request.getAttribute("name");
String[][] group_id=(String[][])request.getAttribute("group_id");

String pname=request.getParameter("cname")==null? "":request.getParameter("cname");
String pgroupId=request.getParameter("cgroup_id")==null? "":request.getParameter("cgroup_id");
%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create(){
    if(template.cname.value==""){
      alert('请选择报表!');
      return;
    }
    if(isEmpty(template.cgroup_id))
    {
      alert('用户组不能为空!');
      return;
    }
    
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back(){
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    show_wait();
    template.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="selfEDreport.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>财政补贴收入数据添加页面</html:title>

  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td class="normalText" nowrap="nowrap" style="text-align:right">报表名称：</td>
      <td class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.Select(name,"cname",pname,false,false)%></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap" style="text-align:right">用户组：</td>
      <td class="normalText" nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.Select(group_id,"cgroup_id",pgroupId,false,false) %></td>
    </tr>
    <tr>
      <td colspan="2" align="center"> 
      <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back();">返回</button>  
      <!--      <img src="images/create.gif" class="mouse" onclick="return create();" /> 
<img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back();" />--></td>
    </tr>
     <tr height="500">
    <td>
    </td>
    </tr>
  </html:table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>
</html:html>
