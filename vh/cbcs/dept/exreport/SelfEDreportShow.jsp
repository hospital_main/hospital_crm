<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/exreport/SelfEDreportShow.jsp,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Modtime:  $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
String[][] name=(String[][])request.getAttribute("name");
String[][] group_id=(String[][])request.getAttribute("group_id");

String pname=request.getParameter("name")==null? "":request.getParameter("name");
String pgroupId=request.getParameter("group_id")==null? "":request.getParameter("group_id");
%>


<Script Language="JavaScript">
  function find() {
    template.subFunction.value='preparedShow';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="selfEDreport.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>扩展报表权限设置主页面</html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
	    <tr>
	      	<td nowrap class="normalText" style="text-align:right">报表名称：</td>
        	<td class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.Select(name,"name",pname,false,false)%></td>
        		        <td nowrap class="normalText" style="text-align:right">用户组：</td>
	        <td class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.Select(group_id,"group_id",pgroupId,false,false)%></td>
	      <td align="right"><button class="pageBtn"  onclick="return find();" >查询</button></td>
	    </tr>
	  </html:table>

	  <br>

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	  %>
		<html:title clazz='table'>扩展报表数据</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr>

		  <!-- 结果集 -->
		  <tr>
		    <td>
		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td>报表名称</td>
		          <td>用户组</td>
		        </html:tr>

		        <%
		          String[][] result = ro.getTableResult();
		          if ( result != null ){
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ]+" "+result[ i ][ 1 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }
		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>

		        <tr CLASS="<%=rowColor%>">
		          <td class="normalText"><a href="#" onclick='window.open("rep/<%=result[ i ][ 0 ]%>",null,"top=10,left=100,height=600,width=800,status=yes,toolbar=no,menubar=yes,location=yes,resizable=yes");'><%=result[ i ][ 0 ]%></a></td>
		          <td class="normalText"><%=result[ i ][ 1 ]%></td>
		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		    </td>
		  </tr>

		  <!-- 操作 -->
	  </html:table>
		<%}%>
	  <input type=hidden name="subFunction"/>
	  <input type="hidden" name="initsub" value="sub"/>
	</form>
</html:html>
