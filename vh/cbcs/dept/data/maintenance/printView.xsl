<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-2"/>
  	<root>
  	<thead>
  		<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount-1'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  	</tr>
		  <tr noWrap='true' class='mainHead'>
        <td nowrap='true'>年月</td>
        <td nowrap='true'>服务单号</td>
		  	<td nowrap='true'>受益科室</td>
		  	<td nowrap='true'>子服务项目</td>
		  	<td nowrap='true'>数量</td>
		  	<td nowrap='true'>单位</td>
		  	<td nowrap='true'>服务当量</td>
		  	<td nowrap='true'>金额</td>
		  	<td nowrap='true'>摘要</td>
		  	<td nowrap='true'>操作人</td>
		  	<td nowrap='true'>操作时间</td>
		  	<td nowrap='true'>审核状态</td>
		  	<td nowrap='true'>审核人</td>
		  	
      </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test=" position()=5 or position()=7 or position()=8 ">
								<td align='right'>
								<xsl:value-of select="format-number(.,'###,###0.00')" />
								 </td>
              </xsl:when>
               
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		 
 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>