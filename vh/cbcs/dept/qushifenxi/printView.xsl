<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/qushifenxi/printView.xsl,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<colgroup>		       
			<col style = 'width:100mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:100mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
		</colgroup>
		<thead>
			<tr noWrap="true" class="mainHead">  	    	  
				<th noWrap="true" rowspan="2">年月</th>
				<th noWrap="true" rowspan="2">科室</th>
				<th noWrap="true" rowspan="2">成本项目</th>
				<th noWrap="true" rowspan="2">总成本</th>
				<th noWrap="true" colspan="2">直接成本</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="2">公用成本</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="2">管理分摊</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="2">医辅分摊</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="2">医技分摊</th>
				<th noWrap="true" style="display:none"></th>
			</tr>
			<tr noWrap="true" class="mainHead">  	    	  
				<th noWrap="true" style="display:none">年月</th>
				<th noWrap="true" style="display:none">科室</th>
				<th noWrap="true" style="display:none">成本项目</th>
				<th noWrap="true" style="display:none">总成本</th>  
				<th noWrap="true">成本额</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">成本额</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">成本额</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">成本额</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">成本额</th>
				<th noWrap="true">比例</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
				<xsl:for-each select="td">
					<xsl:choose> 
						<xsl:when test="position()=1 or position()=2 or position()=3  or position()=4 ">
							<td><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:when test="(position() mod 2)=1">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:when test="(position() mod 2)=0">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
