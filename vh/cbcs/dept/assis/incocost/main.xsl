<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>        
					<xsl:for-each select="td">
						<td>
							<xsl:choose> 
								<xsl:when test="position()=1">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:when test="position()&gt;=4 and position()&lt;=8">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:if test="position()=5">
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</xsl:if>
									<xsl:if test="position()!=5">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=2 or position()=3">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<a href="#">
										<xsl:attribute name="onclick">
											openTooMuchLink(pageMap,<xsl:value-of select="position()"/>,'<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
										</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
									</a>
								</xsl:when>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
