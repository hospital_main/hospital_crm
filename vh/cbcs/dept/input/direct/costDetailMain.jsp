<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/input/direct/costDetailMain.jsp,v 1.4 2015/01/13 01:28:27 liyan Exp $
 $Author: liyan $
 $Date: 2015/01/13 01:28:27 $
 $Modtime: 03-08-14 10:01 $
 $Revision: 1.4 $
--> 

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.*, com.viewhigh.cbcs.base.mvc.view.component.*" %>
<%@ page import="java.util.*, com.viewhigh.cbcs.cbcs.util.*" %>
<%@ page import="java.text.*,
								com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
								
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.base.mvc.view.component.*,com.viewhigh.cbcs.cbcs.util.DisplayWidth,
  com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>
  
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
  String[][] inputTypes = (String[][])request.getAttribute("inputTypes");
%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function showmodul(src,name,style){
   var tim=new Date();
   window.showModalDialog(src+'&time='+tim.toLocaleString(),name,style)
   if (template!=null && template._old_current_page!=null) {
      template._current_page.value = template._old_current_page.value
   }

   find();
  }
  function create(){

    template.subFunction.value='preparedCreate';
    show_wait();
    template.submit();
    return true;
  }


  function remove(){

    var flag = false;

    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey' && template.elements[i].checked == true)
        flag = true;
    }

    if( flag != false){

      if (confirm('是否删除')){
        template.subFunction.value='remove';
        template.submit();
        return true;
      }else{
        return false;
      }
    }else{
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i = 0; i < template.elements.length; i++){
    //	alert(template.elements[i].name);
    //	alert(template.elements[i].disabled);
      if (template.elements[i].name == 'primaryKey'
        && template.elements[i].disabled == false) {

        template.elements[i].checked = true;
      
       // alert(1);
    	}
    }
  }

  function find() {

    switch (isDouble(template.amount, 10, 2)) {

      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于10个字符');
        return false;
      case 3 :
        alert('金额小数部分不能高于2个字符');
        return false;
    }

    if (isTooLong(template.operator,10)) {
      alert('操作员ID不能大于10个字符!');
      return false;
    }
    if (isTooLong(template.voucherType,10)) {
      alert('凭证类别不能大于10个字符!');
      return false;
    }
    if (isTooLong(template.voucherNo,10)) {
      alert('凭证号不能大于40个字符!');
      return false;
    }
    if (isTooLong(template.costNo,12)) {
      alert('成本单号不能大于12个字符!');
      return false;
    }
    if (isTooLong(template.summary, 64)) {
      alert('摘要长度不能大于64个字符!');
      return false;
    }
    if (isTooLong(template.serveNo, 12)) {
      alert('服务单号不能大于12个字符!');
      return false;
    }
   template.signs.value="find"
    template.subFunction.value = "findByCondition";
    show_wait();
    template.submit();
  }
   function counts(){
     var d=new Date()
     var s=d.toString()
     template.signs.value="collect"
     template.subFunction.value='findByCondition';
     window.showModalDialog("costDetail.jspviewhigh?signs="+template.signs.value+"&subFunction="+template.subFunction.value+"&costNo="+template.costNo.value+"&cost_subj_code="+template.cost_subj_code.value+"&dept_code="+template.dept_code.value+"&pay_date_from="+template.pay_date_from.value+"&pay_date_to="+template.pay_date_to.value+"&inputType="+template.inputType.value+"&amount="+template.amount.value+"&operator="+template.operator.value+"&voucherType="+template.voucherType.value+"&voucherNo="+template.voucherNo.value+"&serveNo="+template.serveNo.value+"&summary="+template.summary.value+"&ss="+s, window,"dialogHeight: 160px; dialogWidth: 500px;");
  }
  
  function importData(obj,v){
		   		
				var fileName=fileUploader.choose(1);
				if(fileName.length<4&&fileName.indexOf(".")<0)
					return false;
				var fileType=fileName.substr(fileName.lastIndexOf(".")+1);
				fileUploader.submit(function(m,f){
						if( m != ''){
							alert(m);
							return false;
						}
						for(var k in f){
							fileType+="="+k;
							break;
						}
						var msg = "" ;
						var queryParams = "<comp_file>"+fileType+"</comp_file>" ;
						confirmImportStyleDialog(function(v){
								if(v==0)
									return;

					        window.xmlhttp.post("hosDictsUnitinfoCostdetail_import",queryParams+"<q_flag>"+v+"</q_flag>");
					    var responseText = window.xmlhttp._object.responseText ;  
					    if (responseText.search(/<error>/)!=-1) {
				        var error = responseText.substring(responseText.search(/<error>/)+"<error>".length, responseText.search(/<\/error>/))
				        var tds = error.split('||');
				        xml = '<root><tbody>'
				        for(var i in tds){
				        	if(tds[i]!='') xml += '<tr><td>'+tds[i]+'</td></tr>'
				        }
				        xml += '</tbody></root>'
				        window.showModalDialog(window.prefix+"cbcs/trans/income/showError.html?error="+xml, window,"status:no;dialogHeight: 500px; dialogWidth: 650px;");
				        return true;
				      } else if(responseText.search(/<msg>/)!=-1){
				        var msg = responseText.substring(responseText.search(/<msg>/)+"<msg>".length, responseText.search(/<\/msg>/))
				        alert(msg)
				      }
						  if(msg!='') {
							template.signs.value="find"
    							template.subFunction.value = "findByCondition";
							show_wait();
					    		template.submit();
					    		return true;
						  }
						  },1);
					},obj
				)	
			}
</Script>
<html:html clazz="main" fixRows="1">
  <META HTTP-EQUIV="expires" VALUE="Tue, 23 Jun 1998 01:46:05 GMT">
<form name="template" method="post" action="costDetail.jspviewhigh">

  <!-- 返回信息栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>数据维护主页面</html:title>

  <!-- 获取历史信息 -->
  <%
    String cost_subj_code = request.getParameter("cost_subj_code");
    String dept_code = request.getParameter("dept_code");
    String amount = request.getParameter("amount");
    String memo = request.getParameter("summary");
    String operator = request.getParameter("operator");
  %>

  <!---------------------------- 查询录入栏 ----------------------------------------->
	  <html:table clazz="simple">

    <tr>
      <td class="signText" nowrap>成本单号：</td>
      <td class="normalText">
        <input type="text" name="costNo" value="<%=request.getParameter("costNo")==null?"":request.getParameter("costNo")%>" style="width:140px;" ></td>
       <td nowrap class="signText" >起止日期：</td>
      <td nowrap class="normalText" ><%=new BiDateComponent("pay_date_from", request.getParameter("pay_date_from"),"pay_date_to",request.getParameter("pay_date_to"))%></td>
    	<td class="signText">录入人：</td>
      <td class="normalText"><input type="text" name="operator" size=12 <%if(operator!=null&&!operator.equals(""))out.println(" value=\""+operator+"\"");%> style="width:140px;"></td>
	
    </tr>

    <tr>
     <td class="signText" nowrap>成本项目：</td>
       <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea0" name="cost_subj_code" value="<%=request.getParameter("cost_subj_code")%>"  AdjustVal="97" previousObj="costNo"  codeCol='cost_subj_code' indexCodeSequence="cost_subj_code|cost_subj_name|spell" textCol="cost_subj_name"  width="140" top="12" left="97" Lheight="5" xmlSource="dic/dict_subj_cost_detail_L.xml" init="1"/>
        </td>
        
        <td class="signText">支付科室：</td>
        <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="dept_code"  value="<%=request.getParameter("dept_code")%>"  AdjustVal="97" previousObj="costNo" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" width="140" top="12" left="370" Lheight="5" xmlSource="dic/dict_acct_dept_L.xml" init="1"/></td>
       </td>
       <td class="signText" nowrap>数据来源：</td>
       <td class="normalText">
        <%=new SingleSelect(inputTypes, "inputType", request.getParameter("inputType"), false, false)%>
      </td>
       </tr>

    <tr>
      <td class="signText">金额：</td>
      <td class="normalText"><input type="text" name="amount" <%if(amount!=null&&!amount.equals(""))out.println(" value=\""+amount+"\"");%>style="width:140px;"/></td>
        <td class="signText" nowrap>凭证类别：</td>
      <td class="normalText">
        <input type="text" style="width:140px" name="voucherType" <%if (request.getParameter("voucherType") != null) {%>value="<%=request.getParameter("voucherType")%>"<%}%>>
      </td>
      <td class="signText" nowrap>凭证号：</td>
      <td class="normalText"><input type="text" name="voucherNo" <%if (request.getParameter("voucherNo") != null) {%>value="<%=request.getParameter("voucherNo")%>"<%}%> style="width:140px;"></td>

    </tr>

    <tr>
      
      <td class="signText" nowrap>服务单号：</td>
      <td class="normalText"><input type="text" name="serveNo" <%if (request.getParameter("serveNo") != null) {%>value="<%=request.getParameter("serveNo")%>"<%}%> style="width:140px;"></td>
   
      <td class="signText" valign="top">摘要：</td>
      <td class='normalText' valign="top"><textarea name='summary' rows='2' cols='17'><%=memo==null?"":memo%></textarea>
   </td>
      <td class="signText" nowrap>常用摘要：</td>
       <td class="normalText">
       <%=new SingleSelect(request.getAttribute("dictsummary"), "dictsummary", request.getParameter("dictsummary"), false, false)%>
       </td>
      
 </tr>
 <tr>     
      <td align="right" colspan="6"><button class="pageBtn" onclick="find()" >查询</button>
	  <button class="pageBtn" name="" onclick="counts()" >汇总</button> 
      <button class="pageBtn" name="hosDictsUnitinfoCostdetail_import"
						onclick="importData(this,1)" >导入</button>  
     <!--<img src="images/huizong.GIF" class="mouse" onclick="counts()" />
      <img src="images/transfers.gif" class="mouse" name="hosDictsUnitinfoCostdetail_import"
						onclick="importData(this,1)" />--></td>
    </tr>
	  </html:table>
<!----------------------------------------------------------------------------->
  <br>


          <html:title clazz='table'>直接成本记录</html:title>
  <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if(ro!=null){
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
      oper.addNeedButton("images/create.gif", "return create()");     //  添加
  %>



    <!-- 操作 -->
    <tr><td><%=oper%></td></tr>

 <vh:vhFixTable fixRow=1 fixCol=0 zIndex="0">
	      <table  width="100%" class="resultSetTable" >
		    <colgroup id=tg>
          
          <col style = 'width:10mm' >
          
          <col style = 'width:30mm' >
          
          <col style = 'width:20mm' >
            <col style = 'width:110mm' >
          
          <col style = 'width:40mm' >
          
          <col style = 'width:40mm' >
            <col style = 'width:40mm' >
          
          <col style = 'width:30mm' >
          
          <col style = 'width:20mm' >
            <col style = 'width:30mm' >
          
          <col style = 'width:30mm' >
          
          <col style = 'width:30mm' >
          <col style = 'width:30mm' >
          
        </colgroup>	        

          <tr class="resultLabel">
            <td nowrap="nowrap">选择</td>
            <td nowrap="nowrap">成本单号</td>
            <td nowrap="nowrap">支付日期</td>
            <td nowrap="nowrap">摘要</td>
            <td nowrap="nowrap">支付部门</td>
            <td nowrap="nowrap">成本项目名称</td>
            <td nowrap="nowrap">金额</td>
            <td nowrap="nowrap">服务单号</td>
            <td nowrap="nowrap">录入人</td>
			<td nowrap="nowrap">录入时间</td>
            <td nowrap="nowrap">是否月结</td>
            <td nowrap="nowrap">数据来源</td>
            <td nowrap="nowrap">凭证类别</td>
            <td nowrap="nowrap">凭证号</td>
	        </tr>

        <%
          DecimalFormat nf = new DecimalFormat("#,##0.00");
          String[][] result = ro.getTableResult();
            if ( result != null ) {
              for (int i = 0; i < result.length; i++ ) {
                boolean isCheckout =
                  Boolean.valueOf(result[i][16]).booleanValue();
                String primaryKey = result[ i ][ 0 ];
                for (int j=0; j<result[i].length; j++) {
                  if (result[i][j] == null || result[i][j].equals("")) result[i][j]="&nbsp;";
                }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr class="<%=rowColor%>" >
          <td >
            <input type="checkbox" name="primaryKey" value="<%=result[i][14]%>" <%if(!result[i][11].equals("I") || isCheckout){out.print(" disabled ");}%>>
          </td>
          <td class="normalText">
            <%if(result[i][11].equals("I") && !isCheckout) {%><a href='#' onclick="showmodul('itemmaintenance.jspviewhigh?subFunction=showmod&width=425&height=305&src=costDetail.jspviewhigh?subFunction=preparedSave@@primaryKey=<%=result[i][14]%>',window,'scrolling:no;dialogTop:150px;status:0;dialogLeft:270px;dialogWidth: 525px;dialogHeight: 450px')"><%}%>
              <%=result[ i ][ 14 ]%>
            <%if(result[i][11].equals("I") && !isCheckout) {%></a><%}%>
          </td>
          <td class="normalText" nowrap="nowrap" ><%=result[i][0].substring(0, 10)%></td>
          <td class="normalText" nowrap="nowrap" ><%=result[ i ][ 1 ]%></td>
          <td class="normalText" nowrap="nowrap" ><%=result[ i ][ 3 ]%></td>
          <td class="normalText" nowrap="nowrap" ><%=result[ i ][ 5 ]%></td>
          <td class="numberText" nowrap="nowrap" ><%=nf.format(Double.parseDouble(result[ i ][ 6 ]))%></td>
          <td class="normalText" nowrap="nowrap" ><%=result[ i ][ 7 ]%></td>
          <td class="normalText" nowrap="nowrap" ><%=result[ i ][ 8 ]%></td>
		  <td class="normalText" nowrap="nowrap" ><%=result[ i ][ 15 ]%></td>
          <td class="normalText" nowrap="nowrap" ><%if(result[ i ][ 9 ].equals("Y")){out.print("是");}else{out.print("否");}%></td>
          <td class="normalText" nowrap="nowrap" >
            <%
              for (int j = 0; j < inputTypes.length; j++) {
                if (result[i][11].equals(inputTypes[j][0])) {
						%>
                  <%=inputTypes[j][1]%>
            <%
                  break;
                }
							}
            %>&nbsp;
          </td>
          <td class="normalText" nowrap="nowrap" ><%=result[ i ][ 12 ]%></td>
          <td class="normalText" nowrap="nowrap" ><%=result[ i ][ 13 ]%></td>
        </tr>

        <%
              }
            }
        %>
	     </table>
	  </vh:vhFixTable>
  <!------------------------------------------------------------------------>

	<%}%>

  <input type=hidden name="subFunction"/>
  <input type=hidden name="signs"/>
</form>

<script>
  document.all.dictsummary.onchange=function(){
  	document.all.summary.value=this.options[this.selectedIndex].text;	
  }
  document.all.inputType.style.width=140;
  document.all.dictsummary.style.width=140;
  </script>
  <div id="upload_file_div" style="position:absolute;display:none;">
	    <iframe src="uploadFileD.cmd" id="uploadFrame"></iframe>
	</div>
	<script Language="JavaScript" >
		_____VHFileCreatFromTimerss=window.setInterval(createVhFile,10);
		  function createVhFile(){
		    if(typeof(uploadFrame)=="undefined")
		      return ;
		    window.clearInterval(_____VHFileCreatFromTimerss);
		    fileUploader=new VhFileUploader(uploadFrame);
		  }
	</script>
</html:html>


