<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/input/direct/costDetailGroupByDept.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-14 10:01 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.*, com.viewhigh.cbcs.base.mvc.view.component.*" %>
<%@ page import="java.util.*, com.viewhigh.cbcs.cbcs.util.*" %>
<%@ page import="java.text.*,
								com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.base.mvc.view.component.*,com.viewhigh.cbcs.cbcs.util.DisplayWidth,
  com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>
  
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
    function group()
    {  if(template.pay_date_from.value==""){
          alert('请选择起止年月');
          return false;
          }
        if(template.pay_date_to.value==""){
          alert('请选择终止年月');
          return false;
          }
        show_wait();
        template.submit();
    }
</Script>
<html:html clazz="main" fixRows="1">
 <form name="template" method="post" action="costDetail.jspviewhigh" onsubmit="return group();">

  <!-- 返回信息栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>科室汇总</html:title>

  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText" >起止日期：</td>
      <td nowrap class="normalText" colspan='3'><%=new BiDateComponent("pay_date_from", request.getParameter("pay_date_from"),"pay_date_to", request.getParameter("pay_date_to"))%></td>
      <td nowrap class="normalText" ></td>
    </tr>
   <tr> 
      <td nowrap class="signText" >科室代码：</td>
      <td nowrap class="normalText" >
      	<input type="text" name="dept_code" style="width:140px" <%if(request.getParameter("dept_code")!=null) out.println(" value=\""+request.getParameter("dept_code")+"\"");%>/>
      </td> 
      <td nowrap class="signText" >科室名称：</td>
      <td nowrap class="normalText" ><input type="text" name="dept_name" <%if(request.getParameter("dept_name")!=null) out.println(" value=\""+request.getParameter("dept_name")+"\"");%> style="width:140px;"/></td>

      <td nowrap class="signText">快速定位科室：</td>
      <td nowrap class="normalText">
      	<?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNameb" name="dept_code1" value="<%=request.getParameter("dept_code1")==null? "":request.getParameter("dept_code1")%>" AdjustVal="145" previousObj="dept_name" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" Lwidth="140"  top="65" left="590" Lheight="5" width="140" xmlSource="dic/dict_acct_dept_L.xml" init="1" />
      </td> 
    </tr>
   <tr>
      <td nowrap class="signText" >项目代码：</td>
      <td nowrap class="normalText" ><input type="text" name="subj_code" style="width:140px" <%if(request.getParameter("subj_code")!=null) out.println(" value=\""+request.getParameter("subj_code")+"\"");%>/></td>
      <td nowrap class="signText" >项目名称：</td>
      <td nowrap class="normalText" ><input type="text" name="subj_name" <%if(request.getParameter("subj_name")!=null) out.println(" value=\""+request.getParameter("subj_name")+"\"");%> style="width:140px;"/></td>
      <td nowrap class="signText" >凭证号：</td>
      <td nowrap class="normalText" ><input type="text" name="voucher_no" <%if(request.getParameter("voucher_no")!=null) out.println(" value=\""+request.getParameter("voucher_no")+"\"");%> style="width:140px;"/></td>
      <td nowrap class="normalText" ></td>
      <td nowrap class="normalText" ><button class="pageBtn" onclick="group();" >查询</button></td>
    </tr>

	  </html:table>

  <br>
          <html:title clazz='table'>科室汇总</html:title>
  <!-- 复杂信息 -->
	   <vh:vhFixTable fixRow=1 fixCol=0>
	      <table  width="100%" class="resultSetTable">
		    <colgroup id=tg>
          
          <col style = 'width:54mm' >
          
          <col style = 'width:55mm' >
          
          <col style = 'width:55mm' >
          
        </colgroup>	            
	        <tr class="resultLabel">
            <td nowrap class="resultLabel">科室名称</td>
            <td nowrap class="resultLabel">成本项目</td>
            <td nowrap class="resultLabel">金额</td>
	        </tr>
        <%
        DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
        String[][] result = (String[][])request.getAttribute("table_result");
        if ( result!=null )  {
					for (int i=0; i<result.length; i++) {
						for (int j=0; j<result[i].length; j++) {
							if (result[i][j]==null || result[i][j].trim().length()==0) result[i][j] = "&nbsp;";
						}
					}


        String total [] = null;
        for (int i=0; i<result.length; i++)
        {
          for (int j=0; j<result[i].length; j++)
          {
            if (result[i][j]!=null && result[i][j]=="") result[i][j]="&nbsp;";
          }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
          String dept = result[i][0];

          // 判断是否为总计,若是,则放在total中,最后取出
          if( result[i][0].equals( "合计" ))
          {
              total = result[i];
              continue;
          }

          // 判断是否为子项,如果是,则不显示科室名称
          if( i != 0 )
          {
              if( result[i][0].equals( result[i-1][0] ))
              {
                  dept = "&nbsp;";
              }

          }



        %>
              <tr CLASS="<%=rowColor%>">
                <td class="normalText"><%=dept%></td>
                <td class="normalText"><%=result[ i ][ 1 ]%></td>
                <td class="numberText"><%=moneyFormat.format(Double.parseDouble(result[ i ][ 2 ]))%></td>
              </tr>
              <%
                    }
              %>
                 <tr CLASS="rowWhite">
                  <td class="normalText"><%=total[0]%></td>
                  <td class="normalText"><%=total[1]%></td>
                  <td class="numberText"><%=moneyFormat.format(Double.parseDouble(total[2]))%></td>
                 </tr>
          <%
              }

        %>
	 		     </table>
</vh:vhFixTable>

  <input type="hidden" name="subFunction" value="groupByDept" />
</form>

</html:html>



