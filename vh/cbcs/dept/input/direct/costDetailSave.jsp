<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/input/direct/costDetailSave.jsp,v 1.3 2013/09/26 05:40:33 liyan Exp $
 $Author: liyan $
 $Date: 2013/09/26 05:40:33 $
 $Modtime: 03-08-14 10:01 $
 $Revision: 1.3 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.*, com.viewhigh.cbcs.base.mvc.view.component.*,
com.viewhigh.cbcs.cbcs.util.*,  com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %> 
						
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%   String[] result = (String[])request.getAttribute("result");%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save() {
	  if(template.dept_code.value==''){
	alert("请选择支付科室！");
	return;
	}
    if(template.cost_subj_code.value==''){
	alert("请选择成本项目！");
	return;
	}

    switch (isDouble(template.amountx, 10, 2)) {
      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于10个字符');
        return false;
      case 3 :
        alert('金额小数部分不能高于2个字符');
        return false;
    }
    if (isTooLong(template.voucherTypex,10)) {
      alert('凭证类别不能大于10个字符!');
      return false;
    } 
    if (isTooLong(template.voucherNox,10)) {
      alert('凭证号不能大于40个字符!');
      return false;
    }
    if (isTooLong(template.summaryx, 64)) {
      alert('摘要长度不能大于64个字符!');
      return false;
    }
    template.submit();
    return true;
  }
  
  function changeCurData(){
  var temp2=nosNamea2.value
  var tempt2=nosNamea2.text
  //alert(temp2)
   if(nosNamea1.value =="AAAAAAAAAA"){
     nosNamea2.text='';
     nosNamea2.xmlSource ="dic/dict_subj_cost_forpublic_LV.xml"; 
   }else{
     nosNamea2.text='';
     nosNamea2.xmlSource ="dic/dict_subj_cost_detail_LV.xml";
   }
    return;
  }

  function back(element) {
    template.subFunction.value='findByCondition';
       window.opener.template.signs.value='find'
    self.close();
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="costDetail.jspviewhigh">

  <!-- 返回信息栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>直接成本数据维护修改页面</html:title>

  <%

    if (result != null) {
  %>

  <!-- 简单信息 -->
    <html:table clazz="simple">
     <tr height="30">
      <td class="signText" nowrap="nowrap">&nbsp;&nbsp;摘要：</td>
      <td class='normalText'><textarea name='summaryx' rows='2' cols='30' ><%=result[2]%></textarea>
      </td>
	  </tr>
	  <tr height="30">
	  <td class="signText" nowrap="nowrap">常用摘要：</td>
       <td>
    <%=new SingleSelect(request.getAttribute("dictsummary"), "dictsummary", request.getParameter("dictsummary"), false, false)%>
	</td>
    </tr>

    <tr height="30" id="select">
       <td class="signText">支付科室：</td>
        <td><?xml:namespace prefix="hzh"/>
          <hzh:QInput ID="nosNamea1" name="dept_code"  value="<%=result[3]%>"  AdjustVal="97" previousObj="costNo" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" width="155" top="12" left="370" Lheight="5" xmlSource="dic/dict_acct_dept_L.xml" init="1"/></td>
        </td>
   </tr>
     <tr height="30">
     <td class="signText" nowrap>成本项目：</td>
       <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea0" name="cost_subj_code" value="<%=result[4]%>"  AdjustVal="97" previousObj="costNo"  codeCol='cost_subj_code' indexCodeSequence="cost_subj_code|cost_subj_name|spell" textCol="cost_subj_name"  width="155" top="12" left="97" Lheight="5" xmlSource="dic/dict_subj_cost_detail_L.xml" init="1"/>
        </td>
      </tr>    
  
    <tr height="30">
      <td class="signText" nowrap="nowrap">金额： </td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text name="amountx" value="<%=result[5]%>" />
      </td>
    </tr>
  <tr height="30">
      <td class="signText" nowrap="nowrap">凭证类别：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="voucherTypex" value="<%=result[11]%>" />
      </td>

    <tr height="30">
      <td class="signText" nowrap="nowrap">凭证号： </td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text name="voucherNox" value="<%=result[12]%>" />
			  <input type="hidden" name="cost_nox" value="<%=result[0]%>" />
				<input type="hidden" name="operator" value="<%=result[6]%>" />
        
      </td>
    </tr>
    <tr>
      <td colspan="2" align="left">　　　　　　　　
      <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="return self.close();" >关闭</button>   
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/close.gif" class="mouse" onclick="return self.close();" />--></td>  
   
    </tr>
    <tr height="200">
   <td> </td>
    </tr>
	  </html:table>
  <input type=hidden name="subFunction" value = "save"/>
  <%}%>
 
</form>

<script>
		 document.all.dictsummary.onchange=function(){
  	document.all.summaryx.value=this.options[this.selectedIndex].text;	}
</script>
</html:html>
