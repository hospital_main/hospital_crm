<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/input/direct/costDetailCreate.jsp,v 1.2 2013/08/29 05:07:34 liyan Exp $
 $Author: liyan $
 $Date: 2013/08/29 05:07:34 $
 $Modtime: 03-08-14 10:01 $
 $Revision: 1.2 $
-->

<!--
	input validation:
    1.支付日期不能为空
    2.核算年月不能为空
    3.金额进行数字验证和长度验证, 用户可以不输入数据
    4.摘要进行长度验证, 用户可以不输入数据
    5.凭证类别,凭证号进行长度验证, 用户可以不输入数据
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.*,
								 com.viewhigh.cbcs.base.mvc.view.component.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  function create() {

    if (template.pay_datec.value == '') {
      alert('支付日期不能为空!');
      return false;
    }

    if (isTooLong(template.summaryc, 64)) {
      alert('摘要长度不能大于64个字符!');
      return false;
    }

    if (isTooLong(template.voucherTypec, 10)) {
      alert('凭证类别不能大于10个字符!');
      return false;
    }

    if (isTooLong(template.voucher_no, 40)) {
      alert('凭证号不能大于40个字符!');
      return false;
    }

    if(trim(template.dept_code.value)==''){
    	alert("支付科室不能为空！");
    	return false;
    }

    if(trim(template.cost_subj_code.value)==''){
    	alert("成本项目不能为空！");
    	return false;
    }

    switch (isDouble(template.amountc, 10, 2)) {
      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于10个字符');
        return false;
      case 3 :
        alert('金额小数部分不能高于2个字符');
        return false;
    }

    template.submit();
    return true;
  }  
  
  function changeCurData(){
   if(nosNamea1.value =="AAAAAAAAAA"){
    nosNamea2.xmlSource ="dic/dict_subj_cost_forpublic_LV.xml"; 
    }
  else {
    nosNamea2.xmlSource ="dic/dict_subj_cost_detail_LV.xml";
    }
   return;
 }
	function resetAll(){
		template.reset();
		document.all.dictsummary.selectedIndex=0; 
		document.all.summaryc.value = "";
	}

  function back(element) {
    template.dept_code.value = "";
    template.cost_subj_code.value = "";
    template.subFunction.value='findByCondition';
    template.signs.value="find";
    element.submit();
  }
</Script>


<html:html clazz="main">
<form name="template" method="post" action="costDetail.jspviewhigh">

  <!-- 返回信息栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>直接成本数据添加页面</html:title>

  <!-- 录入信息栏 -->
	  <html:table clazz="simple">

    <tr>
      <td class="signText" nowrap="nowrap">支付日期：</td>
      <td nowrap class="normalText">
        <%=new DateComponent("pay_datec", request.getParameter("pay_datec"))%></td>
    </tr>
     <tr>
      <td class="signText" nowrap="nowrap">凭证类别：</td>
      <td class="normalText"><input type="text" name="voucherTypec" style="width:140px;"></td>
       </tr>
     <tr>
      <td class="signText" nowrap="nowrap">凭证号：</td>
      <td class="normalText"><input type="text" name="voucher_no" style="width:140px;"></td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">摘要：</td>
      <td class='normalText'>
      <textarea name='summaryc' rows='2' cols='20' ><%=request.getParameter("summaryc")==null?"":request.getParameter("summaryc")%></textarea></td>
     </tr>
     <tr>
       <td class="signText" nowrap="nowrap">常用摘要：</td>
       <td>
       <%=new SingleSelect(request.getAttribute("dictsummary"), "dictsummary", request.getParameter("dictsummary"), false, false)%>
			</td>
    </tr>
    
   <tr>
    <tr>
        <td class="signText">支付科室：</td>
        <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="dept_code"  value="<%=request.getParameter("dept_code")%>"  AdjustVal="97" previousObj="costNo" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" width="140" top="12" left="370" Lheight="5" xmlSource="dic/dict_acct_dept_LV.xml" init="1"/></td>
     </tr>
     <tr>
    <td class="signText" nowrap>成本项目：</td>
       <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea0" name="cost_subj_code" value="<%=request.getParameter("cost_subj_code")%>"  AdjustVal="97" previousObj="costNo"  codeCol='cost_subj_code' indexCodeSequence="cost_subj_code|cost_subj_name|spell" textCol="cost_subj_name"  width="140" top="12" left="97" Lheight="5" xmlSource="dic/dict_subj_cost_detail_LV.xml" init="1"/>
        </td>
      </tr>
     <tr>
      <td class="signText" nowrap="nowrap">金额：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="amountc" class="textInputC" style="width:140px;"/></td>
    </tr>

    <tr>
      
      <td colspan="2" align="left">　　　　　　　　　　　　　　
      <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="resetAll();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
	  </html:table>
  <input type="hidden" name="signs" value="find"/>
  <input type=hidden name="subFunction" value="create"/>
</form>
<script>
		 document.all.dictsummary.onchange=function(){
  	document.all.summaryc.value=this.options[this.selectedIndex].text;}	
</script>
</html:html>
