<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>成本单号</th>
				<th>支付日期</th>
				<th>摘要</th>
				<th>支付部门</th>
				<th>成本项目名称</th>
				<th>金额</th>
				<th>服务单号</th>
				<th>录入人</th>
				<th>是否已分摊</th>
				<th>数据来源</th>
				<th>凭证类别</th>
				<th>凭证号</th>
			</tr>           
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<td >
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:when test="position()=4 or position()=5">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:when test="position()=6">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



