<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/hospcostanalysis/synthesisanaly/synthesisinfanaly/printView.xsl,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Autdor: liubinbin $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>		       
				<col />	
				<col />
				<col />	
				<col />
				<col />	
				<col />
				<col />
				<col />
				<col />
			</colgroup>
			<tdead>
	  		</tdead>
		  	<tbody>
				<!--收益状况分析-->
				<!--<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td ></td>
					<td ></td>
					<td ></td>
					<td ></td>
					<td ></td>
				</tr>-->
				<tr>
					<td colspan="9" id="syntdesisinfanalyTd1">收益状况分析</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr>
					<td noWrap="true" rowspan="2" class="tdHead" >项目</td>
					<td noWrap="true" colspan="4"  class="tdHead">全院</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td noWrap="true" rowspan="2" class="tdHead" >药占比</td>
					<td noWrap="true" colspan="3" class="tdHead" >人均</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap="true">
					<td noWrap="true" style="display:none"></td>
					<td  class="tdHead" >收入</td>
					<td class="tdHead">成本</td>
					<td class="tdHead" >收益</td>
					<td noWrap="true" class="tdHead" >成本收益率</td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" class="tdHead" >收入</td>
					<td noWrap="true" class="tdHead" >成本</td>
					<td noWrap="true" class="tdHead" >收益</td>
				</tr>
				
				<xsl:for-each select="/root/tbody/tr[td[1]=11]">
					<tr>          
						<xsl:for-each select="td[position()!=1]">
							<xsl:choose> 
								<xsl:when test="position()=1">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												openDialog('detail11.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidtd:850px;dialogHeight:540px', result)
											</xsl:attribute>
										<xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=5 or position()=6">
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>  	
				
				<tr noWrap="true" >
					<td noWrap="true" rowspan="2" class="tdHead">项目</td>
					<td noWrap="true" colspan="4" class="tdHead">医疗</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td noWrap="true" colspan="4" class="tdHead">药品</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap="true" >
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" class="tdHead">收入</td>
					<td noWrap="true" class="tdHead">成本</td>
					<td noWrap="true" class="tdHead">收益</td>
					<td noWrap="true" class="tdHead">成本收益率</td>
					<td noWrap="true" class="tdHead">收入</td>
					<td noWrap="true" class="tdHead">成本</td>
					<td noWrap="true" class="tdHead">收益</td>
					<td noWrap="true" class="tdHead">成本收益率</td>
				</tr>
				
				<xsl:for-each select="/root/tbody/tr[td[1]=12]">
					<tr>          
						<xsl:for-each select="td[position()!=1]">
							<xsl:choose> 
								<xsl:when test="position()=1">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												openDialog('detail12.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidtd:850px;dialogHeight:540px', result)
											</xsl:attribute>
										<xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=5 or position()=9">
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>  
				
				<tr>
					<td colspan="9" id="syntdesisinfanalyTd2">成本状况分析</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>        
				<tr noWrap="true" >
					<td noWrap="true" class="tdHead">分类方法</td>
					<td noWrap="true" class="tdHead">成本分类</td>
					<td noWrap="true" class="tdHead">金额</td>
					<td noWrap="true" class="tdHead">百分比</td>
					<td noWrap="true" colspan="2" class="tdHead">分类方法</td>
					<td style="display:none"></td>
					<td noWrap="true" class="tdHead">成本分类</td>
					<td noWrap="true" class="tdHead">金额</td>
					<td noWrap="true" class="tdHead">百分比</td>
				</tr>
				
				<xsl:for-each select="/root/tbody/tr[td[1]=2]">
					<xsl:variable name="pos" select="position()"/>
					<tr>          
						<xsl:for-each select="td[position()!=1]">
							<xsl:choose> 
								<xsl:when test="position()=1">
									<xsl:if test="1=$pos">
										<td rowspan="8" >科室类别</td>
									</xsl:if>
									<xsl:if test="1!=$pos">
										<td style="display:none"></td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=6">
									<xsl:if test="1=$pos">
										<td rowspan="2" colspan="2" >成本习性</td><td style="display:none"></td>
									</xsl:if>
									<xsl:if test="3=$pos">
										<td rowspan="6" colspan="2">
											<a href="#" onclick="alert('未定');openDialog('未知.html','dialogWidth:850px;dialogHeight:540px',result)">项目构成</a>
										</td>
										<td style="display:none"></td>
									</xsl:if>
									<xsl:if test="1!=$pos and 3!=$pos">
										<td style="display:none"></td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=2">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												openDialog('detail21.html?load=&lt;dateYear&gt;<xsl:value-of select="../pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="../pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="../pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;itemCode&gt;<xsl:value-of select="../td[2]"/>&lt;/itemCode&gt;&lt;itemName&gt;<xsl:value-of select="../td[3]"/>&lt;/itemName&gt;', 'dialogWidth:850px;dialogHeight:540px', result)
											</xsl:attribute>
										<xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=7">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												openDialog('detail21.html?load=&lt;dateYear&gt;<xsl:value-of select="../pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="../pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="../pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;itemCode&gt;<xsl:value-of select="../td[7]"/>&lt;/itemCode&gt;&lt;itemName&gt;<xsl:value-of select="../td[8]"/>&lt;/itemName&gt;', 'dialogWidth:850px;dialogHeight:540px', result)
											</xsl:attribute>
										<xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=3 ">
									<xsl:if test="7!=$pos and 8!=$pos">
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:if>
									<xsl:if test="7=$pos or 8=$pos">
									<td ></td>
									</xsl:if>
								</xsl:when>
								<xsl:when test=" position()=8">
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
								<xsl:when test="position()=4 ">
									<xsl:if test="7!=$pos and 8!=$pos">
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
									</xsl:if>
									<xsl:if test="7=$pos or 8=$pos">
									<td ></td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=9">
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
								</xsl:when>
								<xsl:when test="position()=5">
								</xsl:when>
								<xsl:otherwise>
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
				
				<tr>
					<td colspan="9" id="syntdesisinfanalyTd3">本量利分析</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>               
				<tr noWrap="true" >
					<td noWrap="true" class="tdHead">项目</td>
					<td noWrap="true" class="tdHead">固定成本</td>
					<td noWrap="true" class="tdHead">变动成本</td>
					<td noWrap="true" class="tdHead">工作量</td>
					<td noWrap="true" class="tdHead">单位收入</td>
					<td noWrap="true" class="tdHead">单位变动成本</td>
					<td noWrap="true" class="tdHead">单位收益</td>
					<td noWrap="true" class="tdHead">保本工作量</td>
					<td noWrap="true" class="tdHead">保本收入</td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[1]=3]">
					<tr>          
						<xsl:for-each select="td[position()!=1]">
							<xsl:choose> 
								<xsl:when test="position()=1">
									<td>
										<a href="#" onclick="alert('未定');openDialog('未知.html','dialogWidtd:850px;dialogHeight:540px',result)"><xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=8">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												openChart('cbcs/decide/costpoint/queryCostpointChart.jsp?dept=<xsl:value-of select="../td[2]"/>&amp;type=O&amp;dateType=montd&amp;dateValue=<xsl:value-of select="../pk/dateYear"/><xsl:value-of select="../pk/dateFromMontd"/>&amp;perIncome=<xsl:value-of select="../td[6]"/>&amp;perChangeCost=<xsl:value-of select="../td[7]"/>&amp;fixedCost=<xsl:value-of select="../td[3]"/>');
											</xsl:attribute>
										<xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each> 
				
				<tr noWrap="true"  id="syntdesisinfanalyTd4">
					<td noWrap="true" colspan="4" class="tdHead">直接医疗科室盈余前5名</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td noWrap="true" colspan="5" class="tdHead">直接医疗亏损科室前5名</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap="true" > 
					<td noWrap="true" class="tdHead">排名</td>
					<td noWrap="true" class="tdHead">科室</td>
					<td noWrap="true" class="tdHead">收益</td>
					<td noWrap="true" class="tdHead">收益率</td>
					<td noWrap="true" class="tdHead">排名</td>
					<td noWrap="true" class="tdHead">科室</td>
					<td noWrap="true"  ></td>
					<td noWrap="true" class="tdHead">收益</td>
					<td noWrap="true" class="tdHead">收益率</td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[1]=4]">
					<tr>          
						<xsl:for-each select="td[position()!=1]">
							<xsl:choose> 
								<xsl:when test="position()=1 or position()=5">
									<td><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:when test="position()=2 or position()=6">
									<td>
										<a href="#" onclick="alert('未定');openDialog('未知.html','dialogWidtd:850px;dialogHeight:540px',result)"><xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=3 or position()=7">
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>  
				
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
