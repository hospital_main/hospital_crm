<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/hospcostanalysis/synthesisanaly/synthesisinfanaly/detailprint11.xsl,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:150mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
			</colgroup>
			<thead>
				<tr noWrap="true" class="mainHead">
					<th noWrap="true" rowspan="2">项目</th>
					<th noWrap="true" colspan="4">全院</th>
					<th style="display:none"></th>
					<th style="display:none"></th>
					<th style="display:none"></th>
					<th noWrap="true" rowspan="2">药占比</th>
					<th noWrap="true" colspan="3">人均</th>
					<th style="display:none"></th>
					<th style="display:none"></th>
				</tr>
				<tr noWrap="true" class="mainHead">
					<th noWrap="true" style="display:none"></th>
					<th noWrap="true">收入</th>
					<th noWrap="true">成本</th>
					<th noWrap="true">收益</th>
					<th noWrap="true">成本收益率</th>
					<th noWrap="true" style="display:none"></th>
					<th noWrap="true">收入</th>
					<th noWrap="true">成本</th>
					<th noWrap="true">收益</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td">
						<xsl:choose> 
							<xsl:when test="position()=1">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise>
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
				</xsl:for-each>  	
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
