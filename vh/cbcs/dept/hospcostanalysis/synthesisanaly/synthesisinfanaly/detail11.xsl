<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" rowspan="2"  >科室</th>
			<th noWrap="true" colspan="4" >全院</th>
			<th style="display:none"></th>
			<th style="display:none"></th>
			<th style="display:none"></th>
			<th noWrap="true" colspan="4" >人均</th>
			<th style="display:none"></th>
			<th style="display:none"></th>
			<th style="display:none"></th>
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" >收入</th>
			<th noWrap="true" >成本</th>
			<th noWrap="true" >收益</th>
			<th noWrap="true" >成本收益率</th>
			<th noWrap="true" >职工人数</th>
			<th noWrap="true" >收入</th>
			<th noWrap="true" >成本</th>
			<th noWrap="true" >收益</th>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>          
				<xsl:for-each select="td">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<td><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:otherwise>
							<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
