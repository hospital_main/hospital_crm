<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>   
		    
			<tr noWrap="true" class="mainHead">
			<td nowrap="true"   rowspan="2">科室</td>
      <td nowrap="true"   rowspan="2">收入</td>
      <td nowrap="true"   colspan="2">全成本</td>
      <td nowrap="true"   colspan="2">可控成本</td>
			<td nowrap="true"   colspan="2">直接成本</td>
      <td nowrap="true"   colspan="2">不含管理成本</td> 
      <td nowrap="true"   colspan="2">变动成本</td>  	   	  
      </tr>
      
    <tr noWrap="true" class="mainHead">		  
			<td nowrap="true"  >成本</td>
      <td nowrap="true"  >收益</td>
      <td nowrap="true"  >成本</td>
      <td nowrap="true"  >收益</td>
			<td nowrap="true"  >成本</td>
      <td nowrap="true"  >收益</td> 
      <td nowrap="true"  >成本</td> 
      <td nowrap="true"  >收益</td>
      <td nowrap="true"  >成本</td> 
      <td nowrap="true"  >收益</td>  
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td">
						<td>
							<xsl:choose> 
								<xsl:when test="position()=1">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:when test="position()!=2 and (position() mod 2)=0 or position()=9">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:when test="(position()=2 or (position() mod 2)=1) and position()!=9">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<a href="#">
										<xsl:attribute name="onclick">
											openTooMuchLink(pageMap,<xsl:value-of select="position()"/>,'<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
										</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
									</a>
								</xsl:when>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
