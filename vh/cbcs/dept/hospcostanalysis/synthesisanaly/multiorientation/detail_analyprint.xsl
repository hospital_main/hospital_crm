<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/hospcostanalysis/synthesisanaly/multiorientation/detail_analyprint.xsl,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>	
			</tr>
			</thead>
			
			<tbody>
			<tr noWrap="true" >
			<td noWrap="true">报告时间：</td>
			<td noWrap="true"><xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[2]"/></td>
			<td noWrap="true">科室：</td>
			<td noWrap="true">
				<xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[3]"/>
			</td>
			<td noWrap="true">核算月：</td>
			<td noWrap="true"><xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[4]"/></td>
		</tr>
		<tr noWrap="true" >
			<td noWrap="true">摘要：</td>
			<td noWrap="true" colspan="5">
			<xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[5]"/>
			</td>
		</tr>
		

		<tr noWrap="true" >
			<td noWrap="true" colspan="6">科室基本情况：</td>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=2]">
			<tr>          
				<xsl:for-each select="td[position()!=1]">
					<xsl:choose> 
						<xsl:when test="(position() mod 2)=0">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td ><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each> 
		<tr noWrap="true" >
			<td noWrap="true" colspan="6"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
		</tr> 	
		<tr noWrap="true" >
			<td noWrap="true" colspan="6">分析纪录：</td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=3]">
			<tr noWrap="true" >
				<td noWrap="true">序号：</td>
				<td noWrap="true"><xsl:value-of select="td[2]"/></td>
				<td noWrap="true" colspan="2">时间：<xsl:value-of select="td[3]"/></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">发言人：<xsl:value-of select="td[4]"/></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
			</tr>
			<tr noWrap="true" >
				<td noWrap="true" colspan="2">分析层信息：</td>
				<td noWrap="true" colspan="5"><xsl:value-of select="td[5]"/></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
			</tr>
			<tr noWrap="true" >
				<td noWrap="true" colspan="6">
				发言人分析：<br/><br/>
				<xsl:value-of select="td[6]"/>
				</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
			</tr>
		</xsl:for-each> 
	
	</tbody>
	<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:5;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>						
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>