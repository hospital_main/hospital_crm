<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/hospcostanalysis/synthesisanaly/deptsurplusstat/printView.xsl,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" rowspan="2">具体科室情况</td>
				<td noWrap="true" colspan="2">本期</td>
				<td style="display:none"/>
				<td noWrap="true" colspan="3">上期</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" colspan="3">同期</td>
				<td style="display:none"/>
				<td style="display:none"/>
		    </tr>
		    <tr noWrap="true" class="mainHead">
		    	<td style="display:none"/>
					<td noWrap="true" colspan="1">数量</td>
					<td noWrap="true" colspan="1">占比重</td>
				  <td noWrap="true" colspan="1">数量</td>
					<td noWrap="true" colspan="1">占比重</td>
			    <td noWrap="true" colspan="1">增减</td>
				  <td noWrap="true" colspan="1">数量</td>
					<td noWrap="true" colspan="1">占比重</td>
					<td noWrap="true" colspan="1">增减</td>
			</tr>
		</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position() &lt; 3]">
						<td noWrap="true">
							<xsl:choose>
								<xsl:when test="position()=2">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:attribute name="style">align:right</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
							</td>
					</xsl:for-each>
					<td noWrap="true"/>
					<td noWrap="true">
						<xsl:attribute name="class">numberText</xsl:attribute>
						<xsl:attribute name="style">align:right</xsl:attribute>
						<xsl:value-of select="format-number(substring-after(td[5],':'),'#,##0')+format-number(substring-after(td[13],':'),'#,##0')"/>
					</td>
					<td noWrap="true"/>
					<td noWrap="true">
						<xsl:attribute name="class">numberText</xsl:attribute>
						<xsl:attribute name="style">align:right</xsl:attribute>
						<xsl:value-of select="format-number(td[7]+td[15],'#,##0')"/>
					</td>
					<td noWrap="true">
						<xsl:attribute name="class">numberText</xsl:attribute>
						<xsl:attribute name="style">align:right</xsl:attribute>
						<xsl:value-of select="format-number(substring-after(td[8],':'),'#,##0')+format-number(substring-after(td[16],':'),'#,##0')"/>
					</td>
					<td noWrap="true"/>
					<td noWrap="true">
						<xsl:attribute name="class">numberText</xsl:attribute>
						<xsl:attribute name="style">align:right</xsl:attribute>
						<xsl:value-of select="format-number(td[10]+td[18],'#,##0')"/>
					</td>
				</tr>
				<tr>
					<td noWrap="true">　　盈余科室(+)</td>
					<xsl:for-each select="td[position() &gt; 2 and position() &lt; 11]">
							<td noWrap="true" align="right">
								<xsl:choose>
									<xsl:when test="position()=1 or position()=3 or position()=6">
										<a href="#">
											<xsl:attribute name="onclick">
												openDialog('../deptprofitlost/main.html?load=&lt;open_type&gt;dept_hos_syn_dep&lt;/open_type&gt;&lt;date_year&gt;<xsl:value-of select="substring-before(substring-before(.,';'),'-')"/>&lt;/date_year&gt;&lt;date_month&gt;<xsl:value-of select="substring-after(substring-before(.,';'),'-')"/>&lt;/date_month&gt;&lt;date_year_end&gt;<xsl:value-of select="substring-before(substring-after(substring-before(.,'!'),';'),'-')"/>&lt;/date_year_end&gt;&lt;date_month_end&gt;<xsl:value-of select="substring-after(substring-after(substring-before(.,'!'),';'),'-')"/>&lt;/date_month_end&gt;&lt;incomeStats&gt;<xsl:value-of select="substring-after(substring-before(.,':'),'!')"/>&lt;/incomeStats&gt;<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px');
											</xsl:attribute>
										<xsl:value-of select="format-number(substring-after(.,':'),'#,##0')"/>
										</a>
									</xsl:when>
									<xsl:when test="position()=2 or position()=4 or position()=7">
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="format-number(.,'#,##0')"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
					</xsl:for-each>
				</tr>
				<tr>
					<td noWrap="true">　　亏损科室(-)</td>
					<xsl:for-each select="td[position() &gt; 10 and position() &lt; 19]">
							<td noWrap="true" align="right">
								<xsl:choose>
									<xsl:when test="position()=1 or position()=3 or position()=6">
										<a href="#">
											<xsl:attribute name="onclick">
												openDialog('../deptprofitlost/main.html?load=&lt;open_type&gt;dept_hos_syn_dep&lt;/open_type&gt;&lt;date_year&gt;<xsl:value-of select="substring-before(substring-before(.,';'),'-')"/>&lt;/date_year&gt;&lt;date_month&gt;<xsl:value-of select="substring-after(substring-before(.,';'),'-')"/>&lt;/date_month&gt;&lt;date_year_end&gt;<xsl:value-of select="substring-before(substring-after(substring-before(.,'!'),';'),'-')"/>&lt;/date_year_end&gt;&lt;date_month_end&gt;<xsl:value-of select="substring-after(substring-after(substring-before(.,'!'),';'),'-')"/>&lt;/date_month_end&gt;&lt;incomeStats&gt;<xsl:value-of select="substring-after(substring-before(.,':'),'!')"/>&lt;/incomeStats&gt;<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px');
											</xsl:attribute><xsl:value-of select="format-number(substring-after(.,':'),'#,##0')"/>
										</a>
									</xsl:when>
									<xsl:when test="position()=2 or position()=4 or position()=7">
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="format-number(.,'#,##0')"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
					</xsl:for-each>
				</tr>
				<tr>
					<td noWrap="true"><xsl:value-of select="td[1]"/>收益</td>
					<td noWrap="true" colspan="8"/>
				</tr>
				<tr>
					<td noWrap="true">　　盈余科室(+)</td>
					<xsl:for-each select="td[position() &gt; 18 and position() &lt; 24]">
							<td noWrap="true" align="right">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
							<xsl:if test="position()=1 or position()=2 or position()=4">
								<td/>
							</xsl:if>
					</xsl:for-each>
				</tr>
				<tr>
					<td noWrap="true">　　亏损科室(-)</td>
					<xsl:for-each select="td[position() &gt; 23 and position() &lt; 29]">
						<td noWrap="true" align="right">
							<xsl:value-of select="format-number(.,'#,##0.00')"/>
						</td>
						<xsl:if test="position()=1 or position()=2 or position()=4">
							<td/>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:8;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
