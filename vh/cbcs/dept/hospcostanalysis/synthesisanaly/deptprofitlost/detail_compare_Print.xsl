<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
	  <!--colgroup>		       
		<col style = 'width:100mm'/>	
		<col style = 'width:100mm'/>
		<col style = 'width:100mm'/>
		<col style = 'width:100mm'/>	
		<col style = 'width:60mm'/>
		<col style = 'width:100mm'/>	
		<col style = 'width:100mm'/>
		<col style = 'width:60mm'/>
		<col style = 'width:100mm'/>
		<col style = 'width:100mm'/>
		<col style = 'width:60mm'/>
		<col style = 'width:100mm'/>
		<col style = 'width:100mm'/>
		<col style = 'width:60mm'/>
		<xsl:if test="/root/tbody/tr[td[1]='2']/td">  
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:60mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:60mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:60mm'/>
		</xsl:if>
	</colgroup-->
	
	<thead> 
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<xsl:if test="/root/tbody/tr[td[1]='2']/td">  
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				</xsl:if>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<xsl:if test="/root/tbody/tr[td[1]='2']/td">  
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				</xsl:if>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<xsl:if test="/root/tbody/tr[td[1]='2']/td">  
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				</xsl:if>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>	
				<xsl:if test="/root/tbody/tr[td[1]='2']/td">  
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				</xsl:if>
			</tr>
			
			<tr noWrap="true" class="mainHead">  
				<td noWrap="true" rowspan="2">项目</td>
				<td noWrap="true" >本期</td>
				<td noWrap="true" colspan="3">预算</td>
				<td style='display:none'></td>
				<td style='display:none'></td>
				<td noWrap="true" colspan="3">上期</td>
				<td style='display:none'></td>
				<td style='display:none'></td>
				<td noWrap="true" colspan="3">同期</td>
				<td style='display:none'></td>
				<td style='display:none'></td>
				<td noWrap="true" colspan="3">院平均</td>
				<td style='display:none'></td>
				<td style='display:none'></td>
				<xsl:if test="/root/tbody/tr[td[1]='2']/td">  
					<td noWrap="true" colspan="3">三甲医院平均</td>
					<td style='display:none'></td>
					<td style='display:none'></td>
					<td noWrap="true" colspan="3">二级医院平均</td>
					<td style='display:none'></td>
					<td style='display:none'></td>
					<td noWrap="true" colspan="3">市平均</td>
					<td style='display:none'></td>
					<td style='display:none'></td>
				</xsl:if>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
		    <td style='display:none'></td>
				<td noWrap="true" colspan="1">数值</td>
			  <td noWrap="true" colspan="1">数值</td>
				<td noWrap="true" colspan="1">差异</td>
		    <td noWrap="true" colspan="1">差异率</td>
			  <td noWrap="true" colspan="1">数值</td>
				<td noWrap="true" colspan="1">差异</td>
		    <td noWrap="true" colspan="1">差异率</td>
			  <td noWrap="true" colspan="1">数值</td>
				<td noWrap="true" colspan="1">差异</td>
				<td noWrap="true" colspan="1">差异率</td>
        <td noWrap="true" colspan="1">数值</td>
				<td noWrap="true" colspan="1">差异</td>
		    <td noWrap="true" colspan="1">差异率</td>
				<xsl:if test="/root/tbody/tr[td[1]='2']/td">  
				  <td noWrap="true" colspan="1">数值</td>
					<td noWrap="true" colspan="1">差异</td>
			    <td noWrap="true" colspan="1">差异率</td>
				  <td noWrap="true" colspan="1">数值</td>
					<td noWrap="true" colspan="1">差异</td>
					<td noWrap="true" colspan="1">差异率</td>
	        <td noWrap="true" colspan="1">数值</td>
					<td noWrap="true" colspan="1">差异</td>
			    <td noWrap="true" colspan="1">差异率</td>
				</xsl:if>
			</tr>
	</thead>
	<tbody>
		<xsl:variable name="gy" select="/root/annex/is_gy"/>
		<xsl:variable name="yf" select="/root/annex/is_yf"/>
		<xsl:if test="/root/tbody/tr[td[1]='1']/td or /root/tbody/tr[td[1]='2']/td">  
			<xsl:for-each select="/root/tbody/tr">
				<tr>
          
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=1 or position()=2">
            	</xsl:when>
              <xsl:when test="position()=3">
                <xsl:if test="../td[2]='yes'">
                  <td align="center"><xsl:attribute name="bgColor">yellow</xsl:attribute><xsl:value-of select="."/></td>
                </xsl:if>
                <xsl:if test="../td[2]='no'">
                  <td><xsl:value-of select="."/></td>
                </xsl:if>
              </xsl:when>
              <xsl:when test="position()&gt;6 and ((position()-7)mod 3)=0">
                <xsl:choose>
                  <xsl:when test="../td[2]='yes'">
                    <td></td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td class="numberText">
                      <xsl:value-of select="format-number(.,'#,##0.00')"/>%
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:if test="../td[2]='no'">
                   <xsl:if test="../td[3]='药占比' or contains(../td[3],'率')">
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
                 </xsl:if>
                 <xsl:if test="../td[3] != '药占比' and not(contains(../td[3],'率'))">
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                 </xsl:if>
                </xsl:if>
                <xsl:if test="../td[2]='yes'">
                  <td></td>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
				</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</xsl:if>
		<xsl:if test="/root/tbody/tr[td[1]='3']/td">  
			<xsl:for-each select="/root/tbody/tr[1]/td">
			<xsl:variable name="td_now" select="position()-2"/>
			<xsl:if test="$td_now>0">
			
			<xsl:choose>
				<xsl:when test="position()=4 and $gy =0">
				
				</xsl:when>
				
				<xsl:when test="position()=6 and $yf =0">
				
				</xsl:when>
				
				<xsl:otherwise>
			
			<tr noWrap="true">
				<xsl:choose>
					<xsl:when test="$td_now=1">
						<td align='center'><xsl:attribute name="bgColor">yellow</xsl:attribute>科室成本分类</td>
					</xsl:when>
					<xsl:when test="$td_now=2">
						<td>公用分摊成本</td>
					</xsl:when>
					<xsl:when test="$td_now=3">
						<td>管理科室成本</td>
					</xsl:when>
					<xsl:when test="$td_now=4">
						<td>医辅科室成本</td>
					</xsl:when>
					<xsl:when test="$td_now=5">
						<td>医技科室成本</td>
					</xsl:when>
					<xsl:when test="$td_now=6">
						<td>直接医疗成本</td>
					</xsl:when>
					<xsl:when test="$td_now=7">
						<td>未纳入成本</td>
					</xsl:when>
					<xsl:when test="$td_now=8">
						<td align='center'><xsl:attribute name="bgColor">yellow</xsl:attribute>成本变动性</td>
					</xsl:when>
					<xsl:when test="$td_now=9">
						<td>固定成本</td>
					</xsl:when>
					<xsl:when test="$td_now=10">
						<td>变动成本</td>
					</xsl:when>
					
					<xsl:when test="$td_now=11">
						<td align='center'><xsl:attribute name="bgColor">yellow</xsl:attribute>计入方式</td>
					</xsl:when>
					<xsl:when test="$td_now=12">
						<td>直接成本</td>
					</xsl:when>
					<xsl:when test="$td_now=13">
						<td>间接成本</td>
					</xsl:when>

					<xsl:when test="$td_now=14">
						<td align='center'><xsl:attribute name="bgColor">yellow</xsl:attribute>成本习性</td>
					</xsl:when>
					<xsl:when test="$td_now=15">
						<td>可控成本</td>
					</xsl:when>
					<xsl:when test="$td_now=16">
						<td>不可控成本</td>
					</xsl:when>

					<xsl:when test="$td_now=17">
						<td align='center'><xsl:attribute name="bgColor">yellow</xsl:attribute>成本分类</td>
					</xsl:when>
					<xsl:when test="$td_now=18">
						<td>人力成本</td>
					</xsl:when>
					<xsl:when test="$td_now=19">
						<td>离退休人员成本</td>
					</xsl:when>
					<xsl:when test="$td_now=20">
						<td>折旧成本</td>
					</xsl:when>
					<xsl:when test="$td_now=21">
						<td>材料成本</td>
					</xsl:when>
					<xsl:when test="$td_now=22">
						<td>药品成本</td>
					</xsl:when>
					<xsl:when test="$td_now=23">
						<td>其他成本</td>
					</xsl:when>
					<xsl:when test="$td_now=24">
			
				<td>无形资产摊销</td>
			</xsl:when>
			<xsl:when test="$td_now=25">
			
				<td>提取医疗风险基金</td>
			</xsl:when>
					<xsl:otherwise>
						<td><xsl:value-of select="$td_now"/></td>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:for-each select="/root/tbody/tr">
					<td class="numberText"><xsl:value-of select="format-number(td[$td_now+2],'#,##0.00')"/></td>
					<xsl:if test="position()>1">
						<xsl:variable name="td_compare" select="td[$td_now+2] - ../tr[1]/td[$td_now+2]"/>
						<xsl:variable name="td_compare2" select="$td_compare div td[$td_now+2]"/>
						<td class="numberText"><xsl:value-of select="format-number($td_compare,'#,##0.00')"/></td>
						<xsl:element name="td">
							<xsl:attribute name="class">numberText</xsl:attribute>
							<xsl:if test="td[$td_now+2]!=0">
							<xsl:value-of select="format-number($td_compare2,'#,##0.00%')"/>
							</xsl:if>
							<xsl:if test="td[$td_now+2]=0">
								<xsl:value-of select="format-number(0,'#,##0.00%')" />
							</xsl:if>							
						</xsl:element>
					</xsl:if>
				</xsl:for-each>
			</tr>
			</xsl:otherwise>
			</xsl:choose>
			</xsl:if>
			</xsl:for-each>

		</xsl:if>
		
	</tbody>
	<tfoot>
	<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<xsl:if test="/root/tbody/tr[td[1]='2']/td">  
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				</xsl:if>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>	
				<xsl:if test="/root/tbody/tr[td[1]='2']/td">  
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				</xsl:if>
			</tr>
	</tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>
