<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead> 
	</thead>
	<tbody>
		<tr noWrap="true" style="display:none">
			<td noWrap="true" colspan="6">目前存在问题和现象：</td>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=2]">
			<tr>          
				<xsl:for-each select="td[position()!=1]">
					<xsl:choose> 
						<xsl:when test="(position() mod 2)=0">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<!--为排序需要-->
						<xsl:when test="position()=7">
						
						</xsl:when>
						<xsl:otherwise>
							<td ><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each> 
	</tbody>
	</xsl:template>
</xsl:stylesheet>
