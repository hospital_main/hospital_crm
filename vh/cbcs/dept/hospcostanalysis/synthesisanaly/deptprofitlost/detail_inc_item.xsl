<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead> 
		<tr noWrap="true" class="mainHead">
			<th noWrap="true">医疗项目</th>
			<th noWrap="true">收入</th>
			<th noWrap="true">数量</th>
			<th noWrap="true">比率</th>
			<th noWrap="true">跟踪分析</th>  
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>          
				<xsl:for-each select="td">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<td><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:when test="position()=2">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:when test="position()=3">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,###')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				<td align="center">
					<a href="#">
						<xsl:attribute name="onclick">
							openTooMuchLink(pageMap,6,'<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
					</xsl:attribute><IMG src="../../../../../images/yingkui/genzong.png" border="0"/></a>
				</td>
			</tr>
		</xsl:for-each>  	
	</tbody>
	</xsl:template>
</xsl:stylesheet>
