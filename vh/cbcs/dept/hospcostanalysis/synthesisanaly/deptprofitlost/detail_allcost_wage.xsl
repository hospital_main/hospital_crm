<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">  		    
			<xsl:for-each select="/root/tbody/tr[td[1]='0']/td">  
				<xsl:choose>
					<xsl:when test="position()=1">
						<td style="display:none"><xsl:value-of select="."/></td>
					</xsl:when>
						<xsl:when test="position()=last()+1">
						<td style="display:none"><xsl:value-of select="."/></td>
					</xsl:when>
					<xsl:otherwise>
						<th noWrap="true"><xsl:value-of select="."/></th> 
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			<th noWrap="true" >���ٷ���</th>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr[td[1]!='0']">
			<tr>	    
				<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td style="display:none"><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=2">
							  <td noWrap='true'>
							    <xsl:if test="../td[1]!='z'">
		  							<a href="#">
		  								<xsl:attribute name="onclick">
		  		            	openTooMuchLink(pageMap,1,'<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
		  								</xsl:attribute>
		  								<xsl:value-of select="."/>
		  							</a>
	  							</xsl:if>
	  							<xsl:if test="../td[1]='z'">
	  							  <xsl:value-of select="."/>
	  							</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()=4">
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
							</xsl:when>
									<xsl:when test="position()=last()">
											<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:otherwise>
						</xsl:choose>
				</xsl:for-each>
				<td align="center">
  				<xsl:if test="td[1]!='z'">
  					<a href="#">
  						<xsl:attribute name="onclick">
  							openTooMuchLink(pageMap,4,'<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
  						</xsl:attribute><IMG src="../../../../../images/yingkui/genzong.png" border="0"/>
  					</a>
  				</xsl:if>
			  </td>
			  
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
