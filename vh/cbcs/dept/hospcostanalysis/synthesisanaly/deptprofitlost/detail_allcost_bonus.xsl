<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">  		  
			<th noWrap="true">人员分类</th>
			<th noWrap="true">金额</th>
			<th noWrap="true">比例</th>
			<th noWrap="true">职工数量</th>
			<th noWrap="true">人均奖金</th>
			<th noWrap="true">跟踪分析</th>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>          
				<xsl:for-each select="td">
					<xsl:choose> 
						<xsl:when test="position()=1">
						  <td noWrap='true'>
						    <xsl:if test="../td[1]!='合计'">
  							<a href="#">
  								<xsl:attribute name="onclick">
  									openTooMuchLink(pageMap,1,'<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
  								</xsl:attribute><xsl:value-of select="."/>
  							</a>
  							</xsl:if>
  							<xsl:if test="../td[1]='合计'">
  							  <xsl:value-of select="."/>
  							</xsl:if>
							</td>
						</xsl:when>
						<xsl:when test="position()=3">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				<td align="center">
  				<xsl:if test="td[1]!='合计'">
  					<a href="#">
  						<xsl:attribute name="onclick">
  							openTooMuchLink(pageMap,6,'<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
  					</xsl:attribute><IMG src="../../../../../images/yingkui/genzong.png" border="0"/></a>
  				</xsl:if>
				</td>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
