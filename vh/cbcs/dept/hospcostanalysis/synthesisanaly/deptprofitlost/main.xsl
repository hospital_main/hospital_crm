<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
			<tr noWrap="true" class="mainHead">  		
				<th noWrap="true" rowspan="2"></th>  
				<th noWrap="true" rowspan="2">排序</th>
				<th noWrap="true" rowspan="2">科室</th>    
				<th noWrap="true" colspan="4">收益</th>
				<th noWrap="true" colspan="3">工作量</th>
				<th noWrap="true" rowspan="2">基本分析</th>
				<th noWrap="true" rowspan="2">比较分析</th>
				<th noWrap="true" rowspan="2">跟踪分析</th>
				<th noWrap="true" rowspan="2">特别关注</th>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
		  		<th noWrap="true" style="display:none"></th>
				<th noWrap="true">收入</th>
				<th noWrap="true">成本</th>
				<th noWrap="true">收益</th>
				<th noWrap="true">成本收益率</th>
				<th noWrap="true">门诊人次</th>
				<th noWrap="true">住院床日</th>
				<th noWrap="true">医技工作量</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<xsl:variable name="tr_row" select="position()"/>
			<xsl:variable name="costType" select="pk/costType"/>
			<xsl:variable name="deptkind" select="pk/dept"/>
			<xsl:variable name="deptType" select="pk/deptType"/>
				<tr>     
					<xsl:if test="pk/isImport=1">
						<xsl:attribute name="style">
							color:red
						</xsl:attribute>
					</xsl:if>
					<td align='center' style='display:none'>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<td align="center">
						<xsl:value-of select="position()"/>
					</td>
					<xsl:for-each select="td">
						<td>
							<xsl:choose> 
								<xsl:when test="position()=1">
									<xsl:if test="$deptkind=1">
										<a href="#">
											<xsl:attribute name="onclick">
												openTooMuchLink(pageMap,1,'<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
											</xsl:attribute><xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="$deptkind=2">
										<xsl:value-of select="."/>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()&gt;=4 and position()&lt;=8">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:if test="position()=5">
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</xsl:if>
									<xsl:if test="position()!=5">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=2 or position()=3">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:if test="$deptkind=1">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
									<xsl:if test="$deptkind=2 and $costType=1">
										<a href="#">
											<xsl:attribute name="onclick"><!--收入1_2、成本1_3-->
												openTooMuchLink(pageMap,'<xsl:value-of select="../pk/costType"/>_<xsl:value-of select="position()"/>','<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;tr_row&gt;<xsl:value-of select="$tr_row"/>&lt;/tr_row&gt;');
											</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
										</a>
									</xsl:if>
									<xsl:if test="$deptkind=2 and $costType!=1">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
							</xsl:choose>
						</td>
					</xsl:for-each>
					<td align="center">
						<xsl:if test="$deptType='D' or $deptType='4'">
							<a href="#">
								<xsl:attribute name="onclick">
									openTooMuchLink(pageMap,'<xsl:value-of select="pk/dept"/>_9','<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;tr_row&gt;<xsl:value-of select="$tr_row"/>&lt;/tr_row&gt;');
							</xsl:attribute><IMG src="../../../../../images/yingkui/jichu.png" border="0"/></a>
						</xsl:if>
					</td>
					<td align="center">
						<a href="#">
							<xsl:attribute name="onclick">
								openTooMuchLink(pageMap,10,'<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
						</xsl:attribute><IMG src="../../../../../images/yingkui/bijiao.png" border="0"/></a>
					</td>
					<td align="center">
						<a href="#">
							<xsl:attribute name="onclick">
								openTooMuchLink(pageMap,11,'<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;tr_row&gt;<xsl:value-of select="$tr_row"/>&lt;/tr_row&gt;');
						</xsl:attribute><IMG src="../../../../../images/yingkui/genzong.png" border="0"/></a>
					</td>
					<td align="center">
						<a href="#" name='acctHosCostanalysisDeptProfitLost_setcolor'>
						<!--<button class='pageBtn' name='acctHosCostanalysisDeptProfitLost_setcolor'>-->
							<xsl:attribute name="onclick">
								setTablePrimary(this,'isImport',<xsl:value-of select="pk/isImport"/>==1?0:1);result.submit_choose(this.name);acctHosCostanalysisDeptProfitLost_select.click();
							</xsl:attribute>
							<xsl:if test="pk/isImport=0">
								<IMG src="../../../../../images/yingkui/lu.gif" border="0"/>
							</xsl:if>
							<xsl:if test="pk/isImport=1">
								<IMG src="../../../../../images/yingkui/hong.gif" border="0"/>
							</xsl:if>
						</a>
					</td>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
