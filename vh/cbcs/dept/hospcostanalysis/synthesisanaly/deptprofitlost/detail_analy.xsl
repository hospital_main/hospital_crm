<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead> 
	</thead>
	<tbody>
		<tr noWrap="true" >
			<td noWrap="true">报告时间：</td>
			<td noWrap="true"><xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[2]"/></td>
			<td noWrap="true">报告人：</td>
			<td noWrap="true">
				<input name="analy_reportMan" id="analy_reportMan"   class="commonInputStyle"  style="width:100%">
				</input>
			</td>
			<td noWrap="true" colspan="2"></td>
		</tr>
		<tr noWrap="true" >
			<td noWrap="true">摘要：</td>
			<td noWrap="true" colspan="5">
				<input name="analy_reportRem" id="analy_reportRem"  class="commonInputStyle"  style="width:100%">
				</input>
			</td>
		</tr>
		<tr noWrap="true" >
			<td noWrap="true" colspan="6">目前存在问题和现象：</td>
		</tr>
		<tr noWrap="true" >
			<td noWrap="true">核算月：</td>
			<td noWrap="true" colspan="5"><xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[3]"/></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=2]">
			<tr>          
				<xsl:for-each select="td[position()!=1]">
					<xsl:choose> 
						<xsl:when test="(position() mod 2)=0">
							<td class="numberText"><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:otherwise>
							<td ><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each> 
		<tr noWrap="true" >
			<td noWrap="true" colspan="6"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
		</tr> 	
		<tr noWrap="true" >
			<td noWrap="true" colspan="6">分析内容：</td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
		</tr>
		<tr noWrap="true" >
			<td noWrap="true" colspan="6" height="150">
				<textarea id="analy_reportCon" name="analy_reportCon" class="commonInputStyle" style="width:100%;height:100%">
					<xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[6]"/>
				</textarea>
			</td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
		</tr>
		<tr noWrap="true" >
			<td noWrap="true" colspan="6">反馈信息：</td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
		</tr>
		<tr noWrap="true" >
			<td noWrap="true" colspan="6" height="150">
				<textarea id="analy_reportInfo" name="analy_reportInfo" class="commonInputStyle" style="width:100%;height:100%">
					<xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[7]"/>
				</textarea>
			</td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
			<td noWrap="true" style="display:none"></td>
		</tr>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
