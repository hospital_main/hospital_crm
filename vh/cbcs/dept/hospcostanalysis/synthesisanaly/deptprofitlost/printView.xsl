<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/hospcostanalysis/synthesisanaly/deptprofitlost/printView.xsl,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			
  		<!--tr noWrap="true" class="mainHead">
				<td noWrap="true">排序</td>
				<td noWrap="true">科室</td>
				<td noWrap="true">收入</td>
				<td noWrap="true">成本</td>
				<td noWrap="true">收益</td>
				<td noWrap="true">成本收益率</td>
				<td noWrap="true">门诊人次</td>
				<td noWrap="true">住院床日</td>
				<td noWrap="true">医技工作量</td>
  		</tr-->
  		
  		<tr noWrap="true" class="mainHead"> 	
				<td noWrap="true" rowspan="2">排序</td>
				<td noWrap="true" rowspan="2">科室</td>
				<td noWrap="true" colspan="4">收益</td>
				<td nowrap='true' style='display:none'/>
				<td nowrap='true' style='display:none'/>
				<td nowrap='true' style='display:none'/>
				<td noWrap="true" colspan="3">工作量</td>
				<td nowrap='true' style='display:none'/>
				<td nowrap='true' style='display:none'/>
				
		  </tr>
		  <tr noWrap="true" class="mainHead"> 
		  	<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true">收入</td>
				<td noWrap="true">成本</td>
				<td noWrap="true">收益</td>
				<td noWrap="true">成本收益率</td>
				<td noWrap="true">门诊人次</td>
				<td noWrap="true">住院床日</td>
				<td noWrap="true">医技工作量</td>
			</tr>
  		
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:if test="pk/isImport=1">
						<xsl:attribute name="style">
							color:red
						</xsl:attribute>
					</xsl:if>
					<td align="center">
						<xsl:value-of select="position()"/>
					</td>
					<xsl:for-each select="td">
						<td>
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:when test="position()&gt;=4 and position()&lt;=8">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:attribute name="align">right</xsl:attribute>
									<xsl:if test="position()=5">
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</xsl:if>
									<xsl:if test="position()!=5">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=2 or position()=3">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:attribute name="align">right</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:8;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
  </root>
	</xsl:template>
</xsl:stylesheet>
