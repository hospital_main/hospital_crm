<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:vh="http://www.w3.org/1999/XSL/Transforms">
	<xsl:template match="/">
	<thead>

	</thead>
	<tbody>
		<!--收益状况分析-->
		<tr>
			<td colspan="9" id="synthesisinfanalyTd1">收益状况分析</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" rowspan="2" class="mainHead" >项目</th>
			<th noWrap="true" colspan="4" class="mainHead" >全科</th>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<th noWrap="true" rowspan="2" class="mainHead" >药占比</th>
			<th noWrap="true" colspan="3" class="mainHead" >人均</th>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" class="mainHead" >收入</th>
			<th noWrap="true" class="mainHead" >成本</th>
			<th noWrap="true" class="mainHead" >收益</th>
			<th noWrap="true" class="mainHead" >成本收益率</th>
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" class="mainHead" >收入</th>
			<th noWrap="true" class="mainHead" >成本</th>
			<th noWrap="true" class="mainHead" >收益</th>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=11]">
			<tr>          
				<xsl:for-each select="td[position()!=1]">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<td>
								<a href="#">
									<xsl:attribute name="onclick" >
										openDialog('../../../../direct/allanalysis/analysis/detail11.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px')
									</xsl:attribute>
								<xsl:value-of select="."/></a>
							</td>
						</xsl:when>
						<xsl:when test="position()=5 or position()=6">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>  	
		<!--收益状况分析-->
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" rowspan="2" class="mainHead" >项目</th>
			<th noWrap="true" colspan="4" class="mainHead" >医疗(含药品)</th>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<th noWrap="true" colspan="4" class="mainHead" >药品</th>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" class="mainHead" >收入</th>
			<th noWrap="true" class="mainHead" >成本</th>
			<th noWrap="true" class="mainHead" >收益</th>
			<th noWrap="true" class="mainHead" >成本收益率</th>
			<th noWrap="true" class="mainHead" >收入</th>
			<th noWrap="true" class="mainHead" >成本</th>
			<th noWrap="true" class="mainHead" >收益</th>
			<th noWrap="true" class="mainHead" >成本收益率</th>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=12]">
			<tr>          
				<xsl:for-each select="td[position()!=1]">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<td>
								<a href="#">
									<xsl:attribute name="onclick" >
										openDialog('../../../../direct/allanalysis/analysis/detail12.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px', result)
									</xsl:attribute>
								<xsl:value-of select="."/></a>
							</td>
						</xsl:when>
						<xsl:when test="position()=5 or position()=9">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>  
		<!--成本状况分析-->
		<tr>
			<td colspan="9" id="synthesisinfanalyTd2">成本状况分析</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr id="viewstr"><td id="views" height="190">asfasdf</td></tr>
		<tr style="display:none" noWrap="true" class="mainHead">
			<th noWrap="true" class="mainHead" >分类方法</th>
			<th noWrap="true" class="mainHead">成本分类</th>
			<td noWrap="true" class="mainHead" >金额</td>
			<td noWrap="true" class="mainHead" >百分比</td>
			<td noWrap="true" class="mainHead" colspan="2">分类方法</td>
			<td style="display:none"></td>
			<td noWrap="true" class="mainHead" >成本分类</td>
			<th noWrap="true" class="mainHead" >金额</th>
			<td noWrap="true" class="mainHead" >百分比</td>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=2]">
			<xsl:variable name="pos" select="position()"/>
			<tr style="display:none">          
				<xsl:for-each select="td[position()!=1]">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<xsl:if test="1=$pos">
								<td rowspan="5">计入方式</td>
							</xsl:if>
							<xsl:if test="6=$pos">
								<td rowspan="3">
									<a href="#" onclick="alert('未定');openDialog('../../../../direct/allanalysis/analysis/未知.html','dialogWidth:850px;dialogHeight:540px',result)">可控性</a>
								</td>
								<td style="display:none"></td>
							</xsl:if>
							<xsl:if test="1!=$pos and 6!=$pos">
								<td style="display:none"></td>
							</xsl:if>
						</xsl:when>
						<xsl:when test="position()=6">
							<xsl:if test="1=$pos">
								<td rowspan="2" colspan="2">成本习性</td><td style="display:none"></td>
							</xsl:if>
							<xsl:if test="3=$pos">
								<td rowspan="6" colspan="2">
									<a href="#" onclick="alert('未定');openDialog('../../../../direct/allanalysis/analysis/未知.html','dialogWidth:850px;dialogHeight:540px',result)">项目构成</a>
								</td>
								<td style="display:none"></td>
							</xsl:if>
							<xsl:if test="1!=$pos and 3!=$pos">
								<td style="display:none"></td>
							</xsl:if>
						</xsl:when>
						<xsl:when test="position()=2">
							<td>
								<a href="#">
									<xsl:attribute name="onclick" >
										openDialog('../../../../direct/allanalysis/analysis/detail21.html?load=&lt;dateYear&gt;<xsl:value-of select="../pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="../pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="../pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;itemCode&gt;<xsl:value-of select="../td[2]"/>&lt;/itemCode&gt;&lt;itemName&gt;<xsl:value-of select="../td[3]"/>&lt;/itemName&gt;', 'dialogWidth:850px;dialogHeight:540px', result)
									</xsl:attribute>
								<xsl:value-of select="."/></a>
							</td>
						</xsl:when>
						<xsl:when test="position()=7">
							<td>
								<a href="#">
									<xsl:attribute name="onclick" >
										openDialog('../../../../direct/allanalysis/analysis/detail21.html?load=&lt;dateYear&gt;<xsl:value-of select="../pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="../pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="../pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;itemCode&gt;<xsl:value-of select="../td[7]"/>&lt;/itemCode&gt;&lt;itemName&gt;<xsl:value-of select="../td[8]"/>&lt;/itemName&gt;', 'dialogWidth:850px;dialogHeight:540px', result)
									</xsl:attribute>
								<xsl:value-of select="."/></a>
							</td>
						</xsl:when>
						<xsl:when test="position()=3 ">
							<xsl:if test="8!=$pos">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:if>
							<xsl:if test="8=$pos">
							<td ></td>
							</xsl:if>
						</xsl:when>
						<xsl:when test=" position()=8">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:when test="position()=4 ">
							<xsl:if test="8!=$pos">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
							</xsl:if>
							<xsl:if test="8=$pos">
							<td ></td>
							</xsl:if>
						</xsl:when>
						<xsl:when test="position()=9">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
						<xsl:when test="position()=5">
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
		<!--本量利分析-->
		<tr >
			<td colspan="9" id="synthesisinfanalyTd3"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr> 
		<tr >
			<td colspan="9" id="synthesisinfanalyTd3"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>   
		<tr>
			<td colspan="9" id="synthesisinfanalyTd3"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>            
		<tr>
			<td colspan="9" id="synthesisinfanalyTd3">本量利分析</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>               
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" class="mainHead" >项目</th>
			<th  noWrap="true" class="mainHead" >固定成本</th>
			<td noWrap="true" class="mainHead">变动成本</td>
			<td noWrap="true" class="mainHead">工作量</td>
			<td noWrap="true" class="mainHead">单位收入</td>
			<th noWrap="true" class="mainHead">单位变动成本</th>
			<th noWrap="true" class="mainHead">单位收益</th>
			<td noWrap="true" class="mainHead">保本工作量</td>
			<td noWrap="true" class="mainHead">保本收入</td>
		</tr>
		<xsl:for-each select="/root/tbody/tr[td[1]=3]">
			<tr>          
				<xsl:for-each select="td[position()!=1]">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<td>
  							<xsl:if test=".='门诊'">
  								<a href="#">								
  									<xsl:attribute name="onclick" >
  								 		openDialog('../../../../direct/allanalysis/analysis/safepoint/out/main.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px')
  								 	</xsl:attribute>
  								 	<xsl:value-of select="."/>
  								</a>
  							</xsl:if>
  							<xsl:if test=".='住院'">
  								<a href="#">								
  									<xsl:attribute name="onclick" >
  								 		openDialog('../../../../direct/allanalysis/analysis/safepoint/in/main.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px')
  								 	</xsl:attribute>
  								 	<xsl:value-of select="."/>
  								</a>
  							</xsl:if>
  							<xsl:if test=".='医技'">
  								<a href="#">								
  									<xsl:attribute name="onclick" >
  								 		openDialog('../../../../direct/allanalysis/analysis/safepoint/tec/main.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px')
  								 	</xsl:attribute>
  								 	<xsl:value-of select="."/>
  								</a>
  							</xsl:if>
							</td>
						</xsl:when>
						<xsl:when test="position()=8">
							<td class="numberText">
  							<xsl:if test="../td[3]!=''">
  								<a href="#">
  									<xsl:attribute name="onclick" >
  										openChart('cbcs/decide/costpoint/queryCostpointChart.jsp?dept=<xsl:value-of select="../td[2]"/>&amp;type=O&amp;dateType=month&amp;dateValue=<xsl:value-of select="../pk/dateYear"/><xsl:value-of select="../pk/dateFromMonth"/>&amp;perIncome=<xsl:value-of select="../td[6]"/>&amp;perChangeCost=<xsl:value-of select="../td[7]"/>&amp;fixedCost=<xsl:value-of select="../td[3]"/>');
  									</xsl:attribute>
  								<xsl:value-of select="format-number(.,'#,##0.00')"/></a>
  							</xsl:if>
  							<xsl:if test="../td[3]=''">
  								<xsl:value-of select="format-number(.,'#,##0.00')"/>
  							</xsl:if>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each> 
	</tbody>
	</xsl:template>
</xsl:stylesheet>
