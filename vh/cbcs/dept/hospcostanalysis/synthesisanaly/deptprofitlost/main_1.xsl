<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		
		<thead>            
			<tr noWrap="true" class="mainHead">  
				<th noWrap="true" rowspan="3"></th>   		  
				<th noWrap="true" rowspan="3">科室</th>    
				<th noWrap="true" colspan="9">收益</th>
				<th noWrap="true" rowspan="2" colspan="4">分析</th>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
		    <th style="display:none"></th>
		    <th noWrap="true" colspan="3">收入</th>
				<th noWrap="true" colspan="3">成本</th>
				<th noWrap="true" colspan="2">收益</th>
				<th noWrap="true" rowspan="2">工作量</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
		    <th style="display:none"></th>
				<th noWrap="true">合计</th>
				<th noWrap="true">直接</th>
				<th noWrap="true">间接</th>
				<th noWrap="true">合计</th>
				<th noWrap="true">直接</th>
				<th noWrap="true">间接</th>
				<th noWrap="true">收益</th>
				<th noWrap="true">成本收益率</th>
				<th style="display:none"></th>
				<th noWrap="true">基本分析</th>
				<th noWrap="true">比较分析</th>
        <th noWrap="true">跟踪分析</th>
        <th noWrap="true">特别关注</th>
			</tr>
			
			
		</thead>
		<tbody>
			
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:if test="pk/isImport=1">
						<xsl:attribute name="style">
							color:red
						</xsl:attribute>
					</xsl:if>
					<td align='center' style='display:none'>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose> 
							<xsl:when test="position()=1">
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()=3 or position()=4">
								<td>
								  <xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:when test="position()&gt;=6 and position()&lt;=10">
								<td>
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:if test="position()=9">
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</xsl:if>
									<xsl:if test="position()!=9">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()=2 or position()=5">
								<td>
									<xsl:attribute name="class">numberText</xsl:attribute>
									<a href="#">
										<xsl:attribute name="onclick">
											openTooMuchLink(pageMap,<xsl:value-of select="position()"/>,'<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
										</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
									</a>
								</td>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
					<td align="center">
						<a href="#">
							<xsl:attribute name="onclick">
								openTooMuchLink(pageMap,11,'<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
						</xsl:attribute><IMG src="../../../../../images/yingkui/jichu.png" border="0"/></a>
					</td>
					<td align="center">
						<a href="#">
							<xsl:attribute name="onclick">
								openTooMuchLink(pageMap,12,'<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
						</xsl:attribute><IMG src="../../../../../images/yingkui/bijiao.png" border="0"/></a>
					</td>
					<td align="center">
						<a href="#">
							<xsl:attribute name="onclick">
								openTooMuchLink(pageMap,13,'<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
						</xsl:attribute><IMG src="../../../../../images/yingkui/genzong.png" border="0"/></a>
					</td>
					<td align="center">
						<a href="#" name='cbcsDirectProfitLostAnalysisProfitLost_setcolor'>
							<xsl:attribute name="onclick">
								setTablePrimary(this,'isImport',<xsl:value-of select="pk/isImport"/>==1?0:1);result.submit_choose(this.name);cbcsDirectProfitLostAnalysisProfitLost_select.click();
							</xsl:attribute>
							<xsl:if test="pk/isImport=0">
								<IMG src="../../../../../images/yingkui/lu.gif" border="0"/>
							</xsl:if>
							<xsl:if test="pk/isImport=1">
								<IMG src="../../../../../images/yingkui/hong.gif" border="0"/>
							</xsl:if>
						</a>
					</td>
				</tr>
			</xsl:for-each>  
			<tr>
			<xsl:variable name="wnr" select="0"/>	
			<xsl:if test="/root/tbody/tr[1]/td[11]= 2"> 	
								<td align="center" colspan="15">
										医技科室数据不参与主界面汇总，仅供参考
								</td>
			</xsl:if>	
			</tr>
			
		</tbody>
	</xsl:template>
</xsl:stylesheet>
