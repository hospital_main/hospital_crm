<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/hospcostanalysis/synthesisanaly/deptprofitlost/detail_analyprint.xsl,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:120mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>
			</colgroup>
			<thead>
			</thead>
			<tbody>
				<tr noWrap="true" >
					<td noWrap="true">报告时间</td>
					<td noWrap="true"><xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[2]"/></td>
					<td noWrap="true">报告人</td>
					<td noWrap="true"><xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[4]"/></td>
					<td noWrap="true" colspan="2"></td>
					<td noWrap="true" style="display:none"></td>
				</tr>
				<tr noWrap="true" >
					<td noWrap="true">摘要</td>
					<td noWrap="true" colspan="5"><xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[5]"/></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
				</tr>
				<tr noWrap="true" >
					<td noWrap="true" colspan="6">目前存在问题和现象：</td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
				</tr>
				<tr noWrap="true" >
					<td noWrap="true">核算月：</td>
					<td noWrap="true" colspan="5"><xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[3]"/></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[1]=2]">
					<tr>          
						<xsl:for-each select="td[position()!=1]">
							<xsl:choose> 
								<xsl:when test="(position() mod 2)=0">
									<td class="numberText"><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:otherwise>
									<td ><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each> 
				<tr noWrap="true" >
					<td noWrap="true" colspan="6"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
				</tr> 	
				<tr noWrap="true" >
					<td noWrap="true" colspan="6">分析内容：</td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
				</tr>
				<tr noWrap="true" >
					<td noWrap="true" colspan="6" height="150"><xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[6]"/></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
				</tr>
				<tr noWrap="true" >
					<td noWrap="true" colspan="6">反馈信息：</td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
				</tr>
				<tr noWrap="true" >
					<td noWrap="true" colspan="6" height="150"><xsl:value-of select="/root/tbody/tr[td[1]=1][1]/td[7]"/></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" style="display:none"></td>
				</tr>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
