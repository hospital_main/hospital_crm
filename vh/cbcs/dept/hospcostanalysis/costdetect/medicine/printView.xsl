<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/hospcostanalysis/costdetect/medicine/printView.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
	  <colgroup>		       
			<col style = 'width:100mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
		</colgroup>
  	<thead>
  		<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true">发票号</th>
				<th noWrap="true">开票日期</th>
				<th noWrap="true">供货单位</th>
				<th noWrap="true">采购员</th>
				<th noWrap="true">发票金额</th>
				<th noWrap="true">已付金额</th>
				<th noWrap="true">未付金额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=5 or position()=6 or position()=7">
	               <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	            </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>
