<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/hospcostanalysis/capanalysis/beddaycost/printView.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	  	<thead>
	  	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  	</tr>
	  	<tr noWrap='true'>
	        <td style='fontsize:subtitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  	</tr>
	  	<tr noWrap='true'  align="left">
	        <td noWrap="true" colspan="8" align="left"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  	</tr>
	  		<tr noWrap='true'  align="left">
	        <td noWrap="true" colspan="8" align="left"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  	</tr>
	  	<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" rowspan="2">住院科室</td>
				<td noWrap="true" colspan="4">总收益</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">单位收益</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true">收入</td>
				<td noWrap="true">成本</td>
				<td noWrap="true">收益</td>
				<td noWrap="true">床日数</td>
				<td noWrap="true">收入</td>
				<td noWrap="true">成本</td>
				<td noWrap="true">收益</td>
			</tr>
	  	</thead>
	  	<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>          
						<xsl:for-each select="td">
							<td>
							<xsl:variable name="ppp" select="position()"/>
								<xsl:choose> 
									<xsl:when test="position()=1 ">
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>  	
			</tbody>
				<tfoot>
	<tr noWrap='true' align="right">
	
		<td noWrap="true" colspan="8" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
				
	</tr>
	<tr noWrap='true' align="right">
		
		<td noWrap="true" colspan="8" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		
	</tr>
	</tfoot>
	  </root>
	</xsl:template>
</xsl:stylesheet>
