<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true" rowspan="2">住院科室</th>
				<th noWrap="true" colspan="4">总收益</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="3">单位收益</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true">收入</th>
				<th noWrap="true">成本</th>
				<th noWrap="true">收益</th>
				<th noWrap="true">床日数</th>
				<th noWrap="true">收入</th>
				<th noWrap="true">成本</th>
				<th noWrap="true">收益</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td">
						<td>
						<xsl:variable name="ppp" select="position()"/>
							<xsl:choose> 
								<xsl:when test="position()=1 ">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>