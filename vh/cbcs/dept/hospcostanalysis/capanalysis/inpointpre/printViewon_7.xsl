<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/hospcostanalysis/capanalysis/inpointpre/printViewon_7.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
	  <colgroup>		       
			<col style = 'width:100mm'/>	
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
		</colgroup>
  	<thead>
  		<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  	</tr>
	  	<tr noWrap='true'>
	        <td style='fontsize:subtitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  	</tr>
			<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true">成本项目</td>
				<td noWrap="true">成本额</td>
				<td noWrap="true">比例</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td">
						<xsl:choose> 
							<xsl:when test="position()=2">
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=3">
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>
