<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/hospcostanalysis/capanalysis/tecpointpre/printView.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
		  <colgroup>		       
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
			</colgroup>
	  	<thead>
	  	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  	</tr>
	  	<tr noWrap='true'>
	        <td style='fontsize:subtitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  	</tr>
	  		<tr noWrap='true'  align="left">
	        <td noWrap="true" colspan="9" align="left"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  	</tr>
	  		<tr noWrap='true'  align="left">
	        <td noWrap="true" colspan="9" align="left"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  	</tr>
	  		<tr noWrap="true" class="mainHead">  		  
					<td noWrap="true">医技科室</td>
					<td noWrap="true">实际工作量</td>
					<td noWrap="true">单位收入</td>
					<td noWrap="true">单位变动成本</td>
					<td noWrap="true">单位收益</td>
					<td noWrap="true">固定成本</td>
					<td noWrap="true">变动成本</td>
					<td noWrap="true">保本工作量</td>
					<td noWrap="true">保本收入</td>
	  		</tr>
	  	</thead>
	  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="mylast" select="last()-position()"/>
				<tr>          
					<xsl:for-each select="td">
						<xsl:variable name="ppp" select="position()"/>
						<xsl:choose> 
							<xsl:when test="position()=1 ">
								<xsl:if test="$mylast!=0">
									<td>
										<xsl:if test="../td[4]&lt;../td[3]">
											<a href="#">
												<xsl:attribute name="onclick" >
													openDialog('chart.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;v1&gt;<xsl:value-of select="../td[8]"/>&lt;/v1&gt;&lt;v2&gt;<xsl:value-of select="../td[9]"/>&lt;/v2&gt;&lt;v3&gt;<xsl:value-of select="../td[6]"/>&lt;/v3&gt;', 'dialogWidth:850px;dialogHeight:540px', result)
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="../td[4]&gt;=../td[3]">
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
								</xsl:if>
								<xsl:if test="$mylast=0">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=6 or position()=7">
								<xsl:if test="$mylast!=0">
									<td>
										<xsl:attribute name="class">numberText</xsl:attribute>
										<a href="#">
											<xsl:if test="../td[2]&lt;../td[8]">
					        			<xsl:attribute name="style">color:red</xsl:attribute>
					        		 </xsl:if>
											<xsl:attribute name="onclick" >
												openDialog('on_<xsl:value-of select="$ppp"/>.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px', result)
											</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</a>
									</td>
								</xsl:if>
								<xsl:if test="$mylast=0">
									<td>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=8 or position()=9">
								<td>
									<xsl:if test="../td[4]&lt;../td[3]">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:if test="../td[2]&lt;../td[8]">
											<xsl:attribute name="style">color:red</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
									<xsl:if test="../td[4]&gt;=../td[3]">
										<xsl:attribute name="class">numberText</xsl:attribute>
										-
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:if test="../td[2]&lt;../td[8]">
										<xsl:attribute name="style">color:red</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
		<tfoot>
	<tr noWrap='true' align="right">
	
		<td noWrap="true" colspan="9" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
				
	</tr>
	<tr noWrap='true' align="right">
		
		<td noWrap="true" colspan="9" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		
	</tr>
	</tfoot>
	  </root>
	</xsl:template>
</xsl:stylesheet>
