<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true">门诊科室</th>
				<th noWrap="true">门诊人次</th>
				<th noWrap="true">单位收入</th>
				<th noWrap="true">单位变动成本</th>
				<th noWrap="true">单位收益</th>
				<th noWrap="true">固定成本</th>
				<th noWrap="true">变动成本</th>
				<th noWrap="true">保本诊次</th>
				<th noWrap="true">保本收入</th>
			</tr>
  	</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="mylast" select="last()-position()"/>
				<tr>          
					<xsl:for-each select="td">
						<xsl:variable name="ppp" select="position()"/>
						<xsl:choose> 
							<xsl:when test="position()=1 ">
								<xsl:if test="$mylast!=0">
									<td>
										<xsl:if test="../td[4]&lt;../td[3]">
											<a href="#">
												<xsl:attribute name="onclick" >
														openDialog('chart.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;v1&gt;<xsl:value-of select="../td[8]"/>&lt;/v1&gt;&lt;v2&gt;<xsl:value-of select="../td[9]"/>&lt;/v2&gt;&lt;v3&gt;<xsl:value-of select="../td[6]"/>&lt;/v3&gt;', 'dialogWidth:650px;dialogHeight:480px', result)
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="../td[4]&gt;=../td[3]">
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
								</xsl:if>
								<xsl:if test="$mylast=0">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=6 or position()=7">
								<xsl:if test="$mylast!=0">
									<td align="right">
										<xsl:attribute name="class">numberText</xsl:attribute>
											<a href="#">
												<xsl:if test="../td[2]&lt;../td[8]">
						        					<xsl:attribute name="style">color:orange</xsl:attribute>
					        					</xsl:if>
					        					<xsl:if test="../td[4]&gt;=../td[3]">
													<xsl:attribute name="style">color:orange</xsl:attribute>
												</xsl:if>
											<xsl:attribute name="onclick" >
												openDialog('on_<xsl:value-of select="$ppp"/>.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px', result)
											</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</a>
									</td>
								</xsl:if>
								<xsl:if test="$mylast=0">
									<td align="right">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=8 or position()=9">
								<td>
									<xsl:if test="../td[4]&lt;../td[3]">
										<xsl:attribute name="class">numberText</xsl:attribute>
											<xsl:if test="$mylast!=0">
												<xsl:if test="../td[2]&lt;../td[8]">
													<xsl:attribute name="style">color:orange</xsl:attribute>
												</xsl:if>
											</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
									<xsl:if test="../td[4]&gt;=../td[3]">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:if test="$mylast!=0">
											<xsl:attribute name="style">color:orange</xsl:attribute>
										</xsl:if>
										-
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:if test="$mylast!=0">
										<xsl:if test="../td[2]&lt;../td[8]">
											<xsl:attribute name="style">color:orange</xsl:attribute>
										</xsl:if>
										<xsl:if test="../td[4]&gt;=../td[3]">
											<xsl:attribute name="style">color:orange</xsl:attribute>
										</xsl:if>
									</xsl:if>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
