<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/interchange/changemain.jsp,v 1.2 2013/07/24 00:27:59 pengjin Exp $
  $Author: pengjin $
  $Date: 2013/07/24 00:27:59 $
  $Revision: 1.2 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>

<%@taglib uri="/WEB-INF/taglib/viewhigh-html.tld"prefix="html"%>

<Script Language="JavaScript">
		function selectFile(){
			fileFrame.document.getElementById("uploadFile").click();
			var fileName=fileFrame.document.getElementById("uploadFile").value;
		//	alert(fileName)
			if(fileName==undefined || fileName=="" || fileName==null){
				return;
			}
			fileFrame.document.fileForm.submit();	
			
		}
	
  function trans() {
  	if (template.year_month.value=="") {
  		alert("请输入日期");
  		return false;
 		}
 		if (template.sheet_type.value=="") {
  		alert("请选择报表");
  		return false;
 		}
  	show_wait();
    template.action =
      'cbcsInterchange.jspviewhigh?subFunction=import&year_month='+template.year_month.value+'&clean='+template.clean.value+'&force='+template.force.value+'&sheet_type='+ template.sheet_type.value;
    template.submit();
    return true;
  }

	function check() {
		if (template.year_month.value=="") {
  		alert("请输入日期");
  		return false;
 		}
 		if (template.sheet_type.value=="") {
  		alert("请选择报表");
  		return false;
 		}
  	show_wait();
    template.action =
      'cbcsInterchange.jspviewhigh?subFunction=check&year_month='+template.year_month.value+'&clean='+template.clean.value+'&force='+template.force.value+'&sheet_type='+ template.sheet_type.value;
    template.submit();
    return true;
	}

  function remove() {
    if(confirm('您确定要删除数据吗?')) {
      if(template.year_month.value == '') {
        alert('请选择年月!');
        return false;
      }
      show_wait();
      template.action = 'cbcsInterchange.jspviewhigh?subFunction=delete&year_month=' + template.year_month.value+'&sheet_type='+ template.sheet_type.value;
      template.submit();
    } else {
      return false;
    }
  }
<%
	String sheet_type = (String)request.getAttribute("sheet_type");
	String clean      = (String)request.getAttribute("clean");
		if ( clean == null ) clean = "Y";
	String force      = (String)request.getAttribute("force");
		if ( force == null ) force = "N";
%>

</Script>
<html:html clazz="main">
  <form name="template" method="post" action="cbcsInterchange.jspviewhigh?subFunction=save">
    <!-- 返回信息栏 -->
    <html:message showReturn="false"/>

    <!-- 标题栏 -->
    <html:title clazz="module">数据导入页面</html:title>

    <!-- 简单信息 -->
    <html:table clazz="simple">
      <tr>
        <td nowrap class="signText" align="right" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;核算月：</td>
        <td nowrap class="normalText" align=right><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td>
        <td class="signText">&nbsp;&nbsp;&nbsp;&nbsp;报表：</td>
        <td nowrap colspan="2">
          <html:select property="data_interchange" name='sheet_type' value='<%=sheet_type%>' maxViewList='15' extent='140'/>
          	
        </td>
        <td nowrap class="signText" >清除本月数据：</td>
      	<td>
					<html:select property="yes_no"  name='clean' value='<%=clean%>' extent='140'/>
				</td>
      </tr>
      <tr>
      	
      	<td nowrap class="signText" >强制导入：</td>
      	<td>
					<html:select property="yes_no"  name='force' value='<%=force%>' extent='140'/>

				</td>
				<td align="right" colspan="5">
			    <button class="pageBtn" accessKey="C" onclick="return check()">检查</button>
			   	<button class='pageBtn' accessKey="P" name='fileSelect' onClick="selectFile();">上传文件</button>
			    <button class="pageBtn" accessKey="C" onclick="return trans()">导入</button>
			     <button class="tableBtn" accessKey="C" onclick="return remove()">删除</button>
				</td>
			</tr>
  	</html:table>
	
	
		
		<%
    BaseRO ro = (BaseRO)request.getAttribute("baseRO");
     if(ro!=null){%>
     <!--
     <div class="for_btn" >
			    <button class="tableBtn" accessKey="C" onclick="return remove()">删除</button>
	</div>
	<br>
	-->
     <% }
	  TableMarge oper = new TableMarge(ro, "return check()");
		if (ro!=null) {
      String[][] result = ro.getTableResult();
     
      if (result!=null) {
    %>
    <table width="100%">
      <tr><td><%=oper%></td></tr>
      <tr>
        <td>
  	<html:table clazz="result">
	    <html:tr clazz='label'>
        <td class="resultLabel">序号</td>
        <td class="resultLabel">记录内容</td>
        <td class="resultLabel"> 错误说明</td>
      </html:tr>

      <%
      for (int i = 0; i < result.length; i++ ) {
       	String rowColor = "rowGray";
        if (i/2*2==i) rowColor = "rowWhite";
      %>

      <tr CLASS="<%=rowColor%>">
        <td class="normalText"><%=(i+ro.getCurrRow())%></td>
        <td class="normalText"><%=result[ i ][ 0 ]%></td>
        <td class="normalText"><%=result[ i ][ 1 ]%></td>
      </tr>
      <%}%>
		</html:table>
		    </td>
		  </tr>
		</table>

    <%}
    }%>
  </form>
  		
  		<iframe id="fileFrame" name="fileFrame"  src="UploadAction.jspviewhigh?subFunction=save" height="55%" width="100%" frameborder="0" style="display:none" ></iframe> 
</html:html>
