<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/interchange/uploadFrame.jsp,v 1.1 2013/07/24 00:17:47 pengjin Exp $
  $Author: pengjin $
  $Date: 2013/07/24 00:17:47 $
  $Revision: 1.1 $
-->
<html>
<%@ page language="java" contentType="text/html;charset=GBK" %>
<%@taglib uri="/WEB-INF/taglib/viewhigh-html.tld"prefix="html"%>
<Script Language="JavaScript">
	
		function selectFile(){
			document.getElementById("uploadFile").click();
			var fileName=document.getElementById("uploadFile").value;
		//	alert(fileName)
			if(fileName==undefined || fileName=="" || fileName==null){
				return;
			}
			document.fileForm.submit();	
		}
</Script>

<body >
  		
  	  		 <form  name="fileForm" method="post" action="UploadAction.jspviewhigh?subFunction=save" enctype="multipart/form-data">
					<input type="file"  name="uploadFile" id="uploadFile" >
					<input type="submit" id="uploadSubmit" value="上传文件">
			</form>	
				 
		 	<%
				String fileName = (String) request.getAttribute("fileName");
				String message = (String) request.getAttribute("message");
				if(message != null && !message.equals("")){
			%>
				<script>alert("<%=message%>");</script>
			<%		
				}else if(fileName != null && !fileName.equals("")){
			%>			
				<script>alert("《"+"<%=fileName%>"+"》上传成功");</script>
			<%
				}
			%>
  	
</body>
</html>
	
