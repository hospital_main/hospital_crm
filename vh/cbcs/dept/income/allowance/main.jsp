<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/income/allowance/main.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime:  $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {
      template.subFunction.value='preparedCreate';
      show_wait();
      template.submit();
      return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          show_wait();
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="FinanceIncome.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>财政补贴收入数据主页面</html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
	    <tr>
	      <td nowrap class="signText" style="text-align:right;width:15%">核算月：</td>
        <td class="normalText"><%=new MonthComponent("year_month_m", request.getParameter("year_month_m"))%></td>
	      <td align="right"><button class="pageBtn" name=""   onclick="return find();" >查询</button><!-- <img src="images/find.gif" class="mouse"/>-->
	    </tr>
	  </html:table>

	  <br>

	  <%	
	  		DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
		<html:title clazz='table'>财政补贴收入数据</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr>

		  <!-- 结果集 -->
		  <tr>
		    <td>
		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td>选择</td>
		          <td>年月</td>
		          <td>财政补贴收入</td>
		        </html:tr>

		        <%
		          String[][] result = ro.getTableResult();
		          if ( result != null ){
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }
		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>

		        <tr CLASS="<%=rowColor%>">
		          <td><input type="checkbox"  <%if (result[i][2]!=null && result[i][2].trim().equals("Y")){ out.print("disabled=\"disabled\" name=\"primaryKeyd\"");}else{out.print(" name=\"primaryKey\"");}%> value="<%=primaryKey%>"></td>
		          <td class="normalText">
               <%if (result[i][2]!=null && result[i][2].trim().equals("Y")){ out.print(primaryKey);}
              else{%>
              <a href='#' onclick="javascript:window.showModalDialog('itemmaintenance.jspviewhigh?subFunction=showmod&width=394&height=175&src=FinanceIncome.jspviewhigh?subFunction=preparedStore@@primaryKey=<%=primaryKey%>',window,'dialogTop:150px;dialogLeft:270px;dialogHeight: 220px; dialogWidth: 400px');
              if (template!=null && template._old_current_page!=null) {template._current_page.value = template._old_current_page.value
              }
            	show_wait();
             	find();">
              
              <%=primaryKey%>
            </a><%}%></td>
		          <td class="numberText" ><%=moneyFormat.format(Double.parseDouble(result[ i ][ 1 ]))%></td>
		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		    </td>
		  </tr>

		  <!-- 操作 -->
	  </html:table>
		<%}%>
	  <input type=hidden name="subFunction"/>
	  <input type="hidden" name="initsub" value=""/>
	</form>
</html:html>
