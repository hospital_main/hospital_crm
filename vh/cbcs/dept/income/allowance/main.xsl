<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
         <th style='width:30;'><input type='checkbox'/></th>
       	<th nowrap="true"  >年月</th>
				<th nowrap="true">财政项目金额</th>
				<th nowrap="true">科教项目金额</th>
				<th nowrap="true">其他金额</th>
				<th nowrap="true" style="display:none">工作量</th>

		
  		</tr>
  	</thead>
  	<tbody>
  
  	  <xsl:for-each select="/root/tbody/tr">
  	      <tr>
  	       <td align='center' style="width:30">
            <input  name="check_ids" type='checkbox' TABINDEX='-1' style='font-size:8px;'>
             <!-- <xsl:attribute name="id"><xsl:value-of select='$CurTrPos'/></xsl:attribute> --> 
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
				
  	        <xsl:for-each select="td">
  	      <xsl:choose>
  	    
						 	<xsl:when test="position() = 5">
						
						</xsl:when>
						
							 	<xsl:when test="position() = 2 or position() = 3 or position() = 4">
  			          <td align='right'>
  			          	<xsl:value-of select="format-number(.,'#,##0.00')"/>
  	                
  	              </td>
  	            		</xsl:when>
  	              	<xsl:otherwise>
									<td align='left'>
									<a tabindex='-1' href="#">
						                  <xsl:attribute name="onclick" >
						    	            openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:300px;dialogHeight:200px', result)
						  	          </xsl:attribute><xsl:value-of select="."/></a>
								 </td>

									</xsl:otherwise>
  	          		</xsl:choose>
  	        
  	        </xsl:for-each>
  	      </tr>
	    </xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>