<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/income/allowance/store.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime:  $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,java.text.DecimalFormat" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function store(){
    if(template.year_month==""){
      alert('请选择核算月!');
      return;
    }
    if(isEmpty(template.amount))
    {
      alert('财政补贴收入不能为空!');
      return;
    }
    switch(isDouble(template.amount,12,4))
    {
      case 0 : alert('财政补贴收入必须为数字型'); return;
      case 1 : alert('财政补贴收入整数部分不能高于12个字符'); return;
      case 2 : alert('财政补贴收入没有整数部分'); return;
      case 3 : alert('财政补贴收入小数部分不能高于4个字符'); return;
    }
    template.subFunction.value='store';
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back(){
    template.subFunction.value='findAll';
    show_wait();
    //template.submit();
    self.close();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="FinanceIncome.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>财政补贴收入数据修改页面</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <html:table clazz="simpleForm">
    <tr>
      <td class="signText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年月：</td>
      <td class="normalText" nowrap="nowrap"><%=result[0].substring(0,4)%>年<%=result[0].substring(4,6)%>月</td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">财政补贴收入：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="amount" value='<%=new DecimalFormat("##0.00").format(Double.parseDouble(result[1]))%>' class="textInputC" <%if(result[2]!=null && result[2].trim().equals("Y")) out.print("readonly");%> /></td>
    </tr>
    <tr>
      <td colspan="2" align="center"><%if(result[2]==null || !result[2].trim().equals("Y")){%>
	  <button class="pageBtn" name=""   onclick="return store();" />保存</button>
	  <button class="pageBtn" name=""   onclick="return reset();" />重置</button><%}%> 
	  <button class="pageBtn" name=""   onclick="return back();" />返回</button></td>
    </tr>
  </html:table>
  <input type=hidden name="subFunction" value = "store"/>
  <input type=hidden name="year_month" value = "<%=result[0]%>"/>
  <%}%>
</form>
</html:html>
