<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/income/allowance/create.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime:  $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create(){
    if(template.year_month.value==""){
      alert('请选择核算月!');
      return;
    }
    if(isEmpty(template.amount))
    {
      alert('财政补贴收入不能为空!');
      return;
    }
    switch(isDouble(template.amount,12,4))
    {
      case 0 : alert('财政补贴收入必须为数字型'); return;
      case 1 : alert('财政补贴收入整数部分不能高于12个字符'); return;
      case 2 : alert('财政补贴收入没有整数部分'); return;
      case 3 : alert('财政补贴收入小数部分不能高于4个字符'); return;
    }
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back(){
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    show_wait();
    template.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="FinanceIncome.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>财政补贴收入数据添加页面</html:title>

  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap" style="width:47%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年月：</td>
      <td class="normalText"><%=new MonthComponent("year_month", request.getParameter("year_month"))%></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">财政补贴收入：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="amount" class="textInputC"  style="width:140px;"/></td>
    </tr>
    <tr height="10">
      <td/>
      <td/>
    </tr>
    <tr>
      <td colspan="2" align="center"> 
	  <button class="pageBtn" name=""   onclick="return create();" />添加</button>
	  <button class="pageBtn" name=""   onclick="return reset();" />重置</button>
	  <button class="pageBtn" name=""   onclick="return back();"  />返回</button></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </html:table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>
</html:html>
