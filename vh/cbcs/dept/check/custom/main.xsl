<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<thead>            
			<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true">部门编码</th>
				<th noWrap="true">部门名称</th>
				<th noWrap="true">财务核算账金额</th>
				<th noWrap="true">工资</th>
				<th noWrap="true">折旧费</th>
				<th noWrap="true">变动成本金额</th>
				<th noWrap="true">合计</th>
		  </tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
					  <xsl:if test="position() &lt; 3">
  						<td align="left">
  							<xsl:value-of select="."/>
  						</td>
						</xsl:if>
					  <xsl:if test="position() &gt; 2">
  						<td align="right">
  							<xsl:value-of select="format-number(.,'#,##0.00')"/>
  						</td>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
