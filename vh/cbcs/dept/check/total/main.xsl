<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<thead>            
			<tr noWrap="true" class="mainHead">
				<th noWrap="true">部门编码</th>			  		  
				<th noWrap="true">部门名称</th>
				<xsl:for-each select="/root/tbody/tr[position() = 1]">
				  <xsl:for-each select="td">
  				  <xsl:if test="position() &gt; 5">
        				<th noWrap="true"><xsl:value-of select="."/></th>
  				  </xsl:if>
				  </xsl:for-each>
        </xsl:for-each>
		  </tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; 1]">
				<tr>     
					<xsl:for-each select="td">
					  <xsl:if test="position() = 2">
  						<td>
  							<xsl:value-of select="."/>
  						</td>
						</xsl:if>
					  <xsl:if test="position() = 5">
  						<td>
  							<xsl:value-of select="."/>
  						</td>
						</xsl:if>
					  <xsl:if test="position() &gt; 5">
  						<td align="right">
  							<xsl:value-of select="format-number(.,'#,##0.00')"/>
  						</td>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  
		</tbody>
	</xsl:template>
</xsl:stylesheet>
