<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/check/total/printType.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:80mm'/>
 				<col style = 'width:100mm'/>
 				<col style = 'width:120mm'/>
 				<col style = 'width:100mm'/>
 				<col style = 'width:100mm'/>
 				<col style = 'width:100mm'/>
 				<col style = 'width:100mm'/>
			</colgroup>
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th noWrap="true">部门编码</th>
				<th noWrap="true">部门名称</th>
				<th noWrap="true">财务核算账金额</th>
				<th noWrap="true">工资</th>
				<th noWrap="true">折旧费</th>
				<th noWrap="true">变动成本金额</th>
				<th noWrap="true">合计</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
					  <xsl:if test="position() &lt; 3">
  						<td>
  							<xsl:value-of select="."/>
  						</td>
						</xsl:if>
					  <xsl:if test="position() &gt; 2">
  						<td class="numberText">
  							<xsl:value-of select="format-number(.,'#,##0.00')"/>
  						</td>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
			<tr>
			  <td/>
			  <td>合计</td>
 				<td align="right">
 					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),'#,##0.00')"/>
 			  </td>
 				<td align="right">
 					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[4]),'#,##0.00')"/>
 			  </td>
 				<td align="right">
 					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00')"/>
 			  </td>
 				<td align="right">
 					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/>
 			  </td>
 				<td align="right">
 					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/>
 			  </td>

			</tr> 	
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
