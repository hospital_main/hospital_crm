<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/check/total/printView.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<xsl:for-each select="/root/tbody/tr[position() = 1]">
				  <xsl:for-each select="td">
  				  <xsl:if test="position() &gt; 5">
        				<td style="display:none"></td>
  				  </xsl:if>
				  </xsl:for-each>
				  </xsl:for-each>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"></td>
				<xsl:for-each select="/root/tbody/tr[position() = 1]">
				  <xsl:for-each select="td">
  				  <xsl:if test="position() &gt; 5">
        				<td style="display:none"></td>
  				  </xsl:if>
				  </xsl:for-each>
				  </xsl:for-each>
				
  		</tr>
		
		
		
			<tr noWrap='true' class='mainHead'>
				<td noWrap="true">����</td>
				<xsl:for-each select="/root/tbody/tr[position() = 1]">
				  <xsl:for-each select="td">
  				  <xsl:if test="position() &gt; 5">
        				<td noWrap="true"><xsl:value-of select="."/></td>
  				  </xsl:if>
				  </xsl:for-each>
				  </xsl:for-each>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; 1]">
				<tr>     
					<xsl:for-each select="td">
					  <xsl:if test="position() = 5">
  						<td>
  							<xsl:value-of select="."/>
  						</td>
						</xsl:if>
					  <xsl:if test="position() &gt; 5">
  						<td align="right" class='numberText'>
  							<xsl:value-of select="format-number(.,'#,##0.00')"/>
  						</td>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
