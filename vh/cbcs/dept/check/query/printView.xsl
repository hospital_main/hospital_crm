<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/check/query/printView.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:60mm'/>
				<col style = 'width:60mm'/>
				<col style = 'width:300mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
			</colgroup>
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>����</th>
				<th nowrap='true'>ƾ֤��</th>
				<th nowrap='true'>ժҪ</th>
				<th nowrap='true'>������</th>
				<th nowrap='true'>���</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=last()">
								<td class="numberText" align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td align='left'><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
