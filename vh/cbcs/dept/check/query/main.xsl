<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
			<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true">日期</th>
				<th noWrap="true">凭证号</th>    
				<th noWrap="true">摘要</th>
				<th noWrap="true">核算项</th>
				<th noWrap="true">金额</th>
		  </tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
					  <xsl:if test="position() &lt; 5">
  						<td>
  							<xsl:value-of select="."/>
  						</td>
						</xsl:if>
					  <xsl:if test="position() = 5">
  						<td align="right">
  							<xsl:value-of select="format-number(.,'#,##0.00')"/>
  						</td>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
