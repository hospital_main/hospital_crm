<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/apportion/approtionconfig/dept/printView.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
	  <colgroup>		       
			<col style = 'width:100mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
		</colgroup>
  	<thead>
  		<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true">排序</th>
				<th noWrap="true">科室</th>      
				<th noWrap="true">收入</th>
				<th noWrap="true">成本</th>
				<th noWrap="true">收益</th>
				<th noWrap="true">成本收益率</th>
				<th noWrap="true">门诊人次</th>
				<th noWrap="true">住院床日</th>
				<th noWrap="true">医技工作量</th>
  		</tr>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:if test="pk/isImport=1">
						<xsl:attribute name="style">
							color:red
						</xsl:attribute>
					</xsl:if>
					<td align="center">
						<xsl:value-of select="position()"/>
					</td>     
					<xsl:for-each select="td">
						<td>
							<xsl:choose> 
								<xsl:when test="position()=1">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:when test="position()&gt;=4 and position()&lt;=8">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:if test="position()=5">
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</xsl:if>
									<xsl:if test="position()!=5">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=2 or position()=3">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>
