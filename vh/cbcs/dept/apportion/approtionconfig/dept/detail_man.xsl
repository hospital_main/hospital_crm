<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:for-each select="/root/tbody/tr[td[1]=1]">	
			<tr noWrap="true" class="mainHead"> 
				<xsl:for-each select="td[position()!=1 and position()!=2]">	
					<th noWrap="true"><xsl:value-of select="."/></th>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr[td[1]=2]">
			<tr>          
				<xsl:for-each select="td[position()!=1 and position()!=2]">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<td>
								<xsl:if test="../td[2]!=''">
									<a href="#">
										<xsl:attribute name="onclick">
											window.showModalDialog('<xsl:value-of select="../td[2]"/>.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', window, "status:no;resizable:yes;scroll:yes;dialogWidth:840px;dialogHeight:560px")
									</xsl:attribute>
									<xsl:value-of select="."/></a>
								</xsl:if>
								<xsl:if test="../td[2]=''">
									<xsl:value-of select="."/>
								</xsl:if>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(substring-before(.,';'),substring-after(.,';'))"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>  	
	</tbody>
	</xsl:template>
</xsl:stylesheet>
