<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th noWrap="true"><input type='checkbox' id = 'checkedall'/></th>
				<th noWrap="true">年月</th>
				<th noWrap="true">科室编码</th>
				<th noWrap="true">科室名称</th>
				
				<th noWrap="true">成本分摊分组编码</th>
				<th noWrap="true">成本分摊分组名称</th>
				
				<th noWrap="true">分摊方法</th>
				<th noWrap="true">是否定向</th>
				<th noWrap="true">查看信息</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="pos" select="position()"/>
				<tr>
					<td align='center' style='display:block'>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;' >
							<xsl:attribute name="value" ><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;allMethod&gt;&lt;/allMethod&gt;&lt;dirMethod&gt;&lt;/dirMethod&gt;</xsl:attribute>
							</input>
					</td>
					<xsl:for-each select="td">
						<td>
							<xsl:choose>
                <xsl:when test="position()=8 or position()=9 or position()=10 ">
                  <xsl:attribute name="style">
                    display:none
                  </xsl:attribute>
                </xsl:when>
                <xsl:when test="position()=6 or position()=7">
                  <xsl:if test="../td[8]='Y'">
                    <select style="width:100%">
				<xsl:attribute name="deptType">
					<xsl:value-of select="../pk/deptType"/>
				</xsl:attribute>
				<xsl:attribute name="deptCode">
					<xsl:value-of select="../pk/deptCode"/>
				</xsl:attribute>
				<xsl:attribute name="allMethod">
					<xsl:value-of select="../td[6]"/>
				</xsl:attribute>
				<xsl:attribute name="isDire">
					<xsl:value-of select="../td[7]"/>
				</xsl:attribute>
				<xsl:attribute name="trIndex">
					<xsl:value-of select="$pos"/>
				</xsl:attribute>
				<xsl:attribute name="tdIndex">
					<xsl:value-of select="position()"/>
				</xsl:attribute>
				<xsl:attribute name="id">TRTR<xsl:value-of select="$pos"/>Id<xsl:value-of select="position()"/></xsl:attribute>
				<xsl:attribute name="value">
					<xsl:value-of select="."/>
				</xsl:attribute>
				<xsl:if test="position()=6">
					<xsl:value-of select="../td[9]" disable-output-escaping="yes"/>
				</xsl:if>
				<xsl:if test="position()=7">
					<xsl:value-of select="../td[10]" disable-output-escaping="yes"/>
				</xsl:if>
		  </select>
                  </xsl:if>
                </xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
				</td>
					</xsl:for-each>
					<td align="center">
						<a href="#" style="display:none">
							<xsl:attribute name="trIndex">
								<xsl:value-of select="$pos"/>
							</xsl:attribute>
							<xsl:attribute name="id">TRTR<xsl:value-of select="$pos"/>Id8</xsl:attribute>
							<xsl:attribute name="_deptPk">
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
							<xsl:attribute name="onclick">
								openTooMuchLink(pageMap,6,'<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
							</xsl:attribute>查看
						</a>
					</td>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
