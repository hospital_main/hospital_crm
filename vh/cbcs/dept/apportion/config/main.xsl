<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/apportion/config/main.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>    
			<tr noWrap='true' class='mainHead'>
			<th noWrap="true">年月</th>
			<th noWrap="true">编号</th>
			<th noWrap="true">参数名称</th>
			<th  noWrap="true" class='moneyCol'>状态</th>
			</tr>
		</thead>
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<td  noWrap="true">
							<xsl:choose>
								<xsl:when test="position()=2">
									<div align='center'>
									<xsl:value-of select="."/>
									</div>
								</xsl:when>
								<xsl:when test="position()=3">
									<a href="#">
										<xsl:attribute name="onclick" >
											window.open('apporParaConfig.jspviewhigh?subFunction=apporParaConfigInit&amp;year_month=200701&amp;app_para_code=01&amp;app_para_name=人员&amp;sign=1',null,'top =100, left=100, height=600,width=800,status=no,toolbar=no,menubar=no,location=no,scrollbars=1')
										</xsl:attribute>
										<xsl:value-of select="."/></a>
								</xsl:when>
								<xsl:otherwise>
									<div align='left'>
									<xsl:value-of select="."/>
									</div>
								</xsl:otherwise>			                        
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



