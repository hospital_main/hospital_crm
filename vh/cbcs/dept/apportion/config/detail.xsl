<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/apportion/config/detail.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>    
			<xsl:for-each select="/root/tbody/tr[td[1]=1]">
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="td[position()!=1 and position()!=2]">
						<th noWrap='true' ><xsl:value-of select="."/></th>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</thead>
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center' style='display:none'>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td[position()!=1 and position()!=2]">
						<xsl:choose>
							<xsl:when test="position()=3">
								<td>
									<xsl:if test="../td[1]=2">
										<xsl:value-of select="."/>
									</xsl:if>
									<xsl:if test="../td[1]=3">
										<input type="text" class="inputDecimal" name="a" style="width:100%" onblur="setTablePrimary(this,'paraValue',this.value)">
											<xsl:attribute name="value">
												<xsl:value-of select="."/>
											</xsl:attribute>
										</input>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()=2">
								<td width="60%">
									<xsl:if test="../td[2]!='0'">
										<a href="#">
											<xsl:attribute name="onclick" >
												openDialog('detail_<xsl:value-of select="../td[2]"/>.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px', result)
											</xsl:attribute>
										<xsl:value-of select="."/></a>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td width="40%">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



