<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	    <colgroup>		       
		    <col style = 'width:110'/>
		    <col style = 'width:150'/>
		    <col style = 'width:110'/>
		    <col style = 'width:150'/>		    
		    <xsl:for-each select="/root/tbody/tr[position()=1]/td">
 	        <xsl:if test="position()>4">
 	          <col style = 'width:90'/>
	        </xsl:if>
	      </xsl:for-each>
		  </colgroup>
	    <tbody>
	      <xsl:for-each select="/root/tbody/tr">
	        <tr>
	        	<xsl:for-each select="td">          
	            <xsl:choose>            
	              <xsl:when test="position() = 12 or position()=13 or position()=14">
                  <td class='numberText' nowrap='true'>
                    <xsl:value-of select="format-number(.,'#,##0.00')"/>       		 	
        		      </td>
                </xsl:when>
	              <xsl:when test=" position()=9">
                  <td style="display:none" nowrap='true'>
        		      </td>
                </xsl:when>                
	            	<xsl:otherwise>
  			          <td nowrap='true'>
  	                <xsl:value-of select="."/>
  	              </td>
  	            </xsl:otherwise>			
	            </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
      </tbody>
     </root>
	</xsl:template>
</xsl:stylesheet>