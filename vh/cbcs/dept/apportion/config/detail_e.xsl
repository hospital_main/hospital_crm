<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/apportion/config/detail_e.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>    
			<tr noWrap='true' class='mainHead'>
 				<th noWrap='true' rowspan="2">设备名称</th>
 				<th noWrap='true' rowspan="2">原值</th>
			</tr>
		</thead>
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td class='numberText'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>  
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



