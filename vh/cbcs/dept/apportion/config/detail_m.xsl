<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/apportion/config/detail_m.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>    
			<tr noWrap='true' class='mainHead'>
 				<th noWrap='true' rowspan="2">人员类别</th>
 				<th noWrap='true' rowspan="2">职别</th>
 				<th noWrap='true' rowspan="2">工号</th>
 				<th noWrap='true' rowspan="2">姓名</th>
 				<th noWrap='true' rowspan="2">实际出<br/>勤天数</th>
				<th noWrap='true' colspan="31">出勤状况</th>
			</tr>
			<tr noWrap='true' class='mainHead'>
 				<th noWrap='true' style="display:none"></th>
 				<th noWrap='true' style="display:none"></th>
 				<th noWrap='true' style="display:none"></th>
 				<th noWrap='true' style="display:none"></th>
 				<th noWrap='true' style="display:none"></th>
 				
 				<th noWrap='true'>1</th>
 				<th noWrap='true'>2</th>
 				<th noWrap='true'>3</th>
 				<th noWrap='true'>4</th>
 				<th noWrap='true'>5</th>
 				<th noWrap='true'>6</th>
 				<th noWrap='true'>7</th>
 				<th noWrap='true'>8</th>
 				<th noWrap='true'>9</th>
 				<th noWrap='true'>10</th>
 				<th noWrap='true'>11</th>
 				<th noWrap='true'>12</th>
 				<th noWrap='true'>13</th>
 				<th noWrap='true'>14</th>
 				<th noWrap='true'>15</th>
 				<th noWrap='true'>16</th>
 				<th noWrap='true'>17</th>
 				<th noWrap='true'>18</th>
 				<th noWrap='true'>19</th>
 				<th noWrap='true'>20</th>
 				<th noWrap='true'>21</th>
 				<th noWrap='true'>22</th>
 				<th noWrap='true'>23</th>
 				<th noWrap='true'>24</th>
 				<th noWrap='true'>25</th>
 				<th noWrap='true'>26</th>
 				<th noWrap='true'>27</th>
 				<th noWrap='true'>28</th>
 				<th noWrap='true'>29</th>
 				<th noWrap='true'>30</th>
 				<th noWrap='true'>31</th>
			</tr>
		</thead>
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() &lt;= 5">
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td class='numberText'>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



