<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true' rowspan="2">科室</th>
      	<th nowrap='true' rowspan="2">成本项目</th>
      	<th nowrap='true' colspan="3">总成本</th>
      	<th nowrap='true' colspan="3">直接成本</th>
      	<th nowrap='true' colspan="3">公共成本</th>
      	<th nowrap='true' colspan="3">管理分摊</th>
      	<th nowrap='true' colspan="3">医辅分摊</th>
      	<th nowrap='true' colspan="3">医技分摊</th>
      	 
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>合计</th>
      	<th nowrap='true'>财政</th>
      	<th nowrap='true'>科教</th>
      	<th nowrap='true'>合计</th>
      	<th nowrap='true'>财政</th>
      	<th nowrap='true'>科教</th>
      	<th nowrap='true'>合计</th>
      	<th nowrap='true'>财政</th>
      	<th nowrap='true'>科教</th>
      	<th nowrap='true'>合计</th>
      	<th nowrap='true'>财政</th>
      	<th nowrap='true'>科教</th>
      	<th nowrap='true'>合计</th>
      	<th nowrap='true'>财政</th>
      	<th nowrap='true'>科教</th>
      	<th nowrap='true'>合计</th>
      	<th nowrap='true'>财政</th>
      	<th nowrap='true'>科教</th>
      	 
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	 
      	<xsl:variable name="rowPos" select="position()"/>
        <tr>
          <xsl:for-each select="td[position()]">
              <xsl:choose>
                
                <xsl:when test="position() =1">
				            <td>
			  	            <xsl:value-of select="."/>
				            </td>
                </xsl:when>
                <xsl:when test="position() = 2">
				            <td>
			  	            <xsl:value-of select="."/>
				            </td>
                </xsl:when>
                                
                <xsl:otherwise>
			            <td align="right">
	                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

