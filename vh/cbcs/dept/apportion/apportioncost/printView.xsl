<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<thead>
			<xsl:variable name="gy" select="/root/annex/is_gy"/>
			<xsl:variable name="yf" select="/root/annex/is_yf"/>
			<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="colcount"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" ></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
		</tr>
			
			<tr noWrap="true" class="mainHead">   	  
				<td noWrap="true" rowspan="2">年月</td>
				<td noWrap="true" rowspan="2">科室</td>
				<td noWrap="true" rowspan="2">成本项目</td>
				<td noWrap="true" colspan="1">总成本</td>
				<td noWrap="true" colspan="2">直接成本</td>
				<td noWrap="true" style="display:none"></td>
				
				<xsl:if test="$gy=1">
					<td noWrap="true" colspan="2">公用成本</td>
					<td noWrap="true" style="display:none"></td>
				</xsl:if>
				
				<td noWrap="true" colspan="2">管理分摊</td>
				<td noWrap="true" style="display:none"></td>
				
				<xsl:if test="$yf=1">
					<td noWrap="true" colspan="2">医辅分摊</td>
					<td noWrap="true" style="display:none"></td>
				</xsl:if>
				
				<td noWrap="true" colspan="2">医技分摊</td>
				<td noWrap="true" style="display:none"></td>
			</tr>
			<tr noWrap="true" class="mainHead">  	    	  
				<td noWrap="true" style="display:none">年月</td>
				<td noWrap="true" style="display:none">科室</td>
				<td noWrap="true" style="display:none">成本项目</td>
				<td noWrap="true">成本额</td>
				<td noWrap="true">成本额</td>
				<td noWrap="true">比例</td>
				<xsl:if test="$gy=1">
					<td noWrap="true">成本额</td>
					<td noWrap="true">比例</td>
				</xsl:if>
				
				<td noWrap="true">成本额</td>
				<td noWrap="true">比例</td>
				
				<xsl:if test="$yf=1">
					<td noWrap="true">成本额</td>
					<td noWrap="true">比例</td>
				</xsl:if>
				
				<td noWrap="true">成本额</td>
				<td noWrap="true">比例</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
				<xsl:for-each select="td">
					<xsl:choose> 
						<xsl:when test="position()=1 or position()=2 or position()=3 ">
							<td noWrap="true"><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:when test="(position() mod 2)=0">
							<td noWrap="true" class="numberText"><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:when test="(position() mod 2)=1">
							<td noWrap="true" class="numberText"><xsl:value-of select="."/></td>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
		<tfoot>
	<tr>
		<td noWrap="true" colspan="colcount" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
	</tr>
	<tr>
		<td noWrap="true" colspan="colcount" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
	</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
