<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true">医疗项目</th>
				<th noWrap="true">科室</th>
				<th noWrap="true">分摊成本</th>
				
  		</tr>
  	</thead>
  	<tbody>
	      <xsl:for-each select="/root/tbody/tr">
        <tr>          
          <xsl:for-each select="td">
              <xsl:choose> 
              <xsl:when test="position()=3">
	               <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	            </xsl:when>
              <xsl:otherwise>
                 <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
