<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true">科室名称</th>
				<th noWrap="true">分摊成本</th>
				<th noWrap="true">比例</th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>          
					<xsl:for-each select="td">
					
						<xsl:choose> 
						
								<xsl:when test="position()=1">
									<td><xsl:attribute name="class">numberText</xsl:attribute>
									<a href="#">
									<xsl:attribute name="onclick" >
									openDialog('on_9_1.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px', result)
									</xsl:attribute><xsl:value-of select="."/></a></td>
								</xsl:when>
									<xsl:when test="position()=3">
										<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
										</xsl:when>
										<xsl:otherwise>
										<td><xsl:value-of select="."/></td>
									</xsl:otherwise>
						</xsl:choose>
					
					</xsl:for-each>
					</tr>
				</xsl:for-each>  	
			</tbody>
	</xsl:template>
</xsl:stylesheet>
