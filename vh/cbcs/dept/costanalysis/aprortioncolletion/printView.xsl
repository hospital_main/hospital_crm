<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/costanalysis/aprortioncolletion/printView.xsl,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
	  <colgroup>		       
			<col style = 'width:80mm'/>	
			<col style = 'width:80mm'/>
			<col style = 'width:80mm'/>	
			<col style = 'width:60mm'/>
			<col style = 'width:80mm'/>	
			<col style = 'width:60mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:60mm'/>	
			<col style = 'width:80mm'/>
			<col style = 'width:60mm'/>	
			<col style = 'width:80mm'/>
			<col style = 'width:60mm'/>
		</colgroup>
  	<thead>
  		<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true">科室</th>
				<th noWrap="true">总成本</th>
				<th noWrap="true">直接成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">公用成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">管理分摊成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">医辅成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">医技分摊</th>
				<th noWrap="true">比例</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=5 or position()=6 or position()=7">
	               <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	            </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>
