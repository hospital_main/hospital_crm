<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true">科室</th>
				<th noWrap="true">总成本</th>
				<th noWrap="true">直接成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">公用成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">管理分摊成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">医辅成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">医技分摊</th>
				<th noWrap="true">比例</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td">
						<td>
							<xsl:variable name="ppp" select="position()"/>
							<xsl:choose> 
								<xsl:when test="position()=3 or position()=7 or position()=9 or position()=11">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<a href="#">
									<xsl:attribute name="onclick" >
										openDialog('on_<xsl:value-of select="$ppp"/>.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px', result)
									</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/></a>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
