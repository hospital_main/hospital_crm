<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">
		  <th style="display:none"><input type="checkbox"/></th>
			<th noWrap="true">成本分摊分组编号</th>
			<th noWrap="true">成本分摊分组名称</th>
			<th noWrap="true">是否停用</th>
		</tr>
		
	</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
					<td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          
					<xsl:for-each select="td">
					
					<td><xsl:choose>
                <xsl:when test="position()=1">
                  <a tabindex='-1'><xsl:value-of select="."/></a>
                </xsl:when>
                
                <xsl:when test="position()=3">
                  <xsl:if test="../td[3]='0'">否</xsl:if>
                  <xsl:if test="../td[3]='1'">是</xsl:if>
                </xsl:when>
                
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
                
              </xsl:choose></td>
					</xsl:for-each></tr>
				</xsl:for-each>  	
			</tbody>
</xsl:template>
</xsl:stylesheet>