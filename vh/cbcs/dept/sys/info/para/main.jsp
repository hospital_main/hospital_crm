<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dept/sys/info/para/main.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" %>
<%@ page import="com.viewhigh.cbcs.base.util.*" %>
<html xmlns:vh>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<link href="" rel="stylesheet" type="text/css">
		<script language="javascript" id="scriptID"></script>
		<script language="javascript">parent.pageInit(window)</script>
		<script language="javascript">
		   function init(){
		   	subBody.load="cbcsDeptSysinfoPara_select";
		   	subBody.post();
		   	var res=subBody.getOneDim();
		   	
		   	for(var i=2;i<res.length;i++){
		   		document.all['f'+(i+1)].setValue(res[i]);
		   	}
		   }
		</script>
	</head>
	<%
		boolean nx = Preference.getFt_nx(); //逆向
		boolean pj = Preference.getFt_pj(); //平级 
	%>
	<body class="subBody" id="subBody" onLoad="init()">
		<div class="titleHeader">参数设置</div>
		<table id="query" class="lineCtn">
				<tr id='tr_f1' style='display:<%if(!nx){%>none<%}%>'>
				<!--  
				<td align="right" width>医辅科室成本分摊：</td>
				<td><input name="f1" type="text" class="inputCheckBox" load="<root><item name='' value='0'/></root>" >逆向分摊 </td>
				-->
				<td align="right">收入成本追踪设置：</td>
				<td><input name="f3" type="text" class="inputCheckBox" load="<root><item name='' value='0'/></root>" >
				  追踪收入明细 </td>
				</tr>
				<tr id='tr_f2' style='display:<%if(!pj){%>none<%}%>'>
				<!--  
				<td>&nbsp;</td>
				<td><input name="f2" type="text" class="inputCheckBox" load="<root><item name='' value='0'/></root>" >平级分摊 </td>
				-->
				<td>&nbsp;</td>
				<td><input name="f4" type="text" class="inputCheckBox" load="<root><item name='' value='0'/></root>" >
				  追踪人员成本明细 </td>
				</tr>
				<tr>
				<!--  
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				-->
				<td>&nbsp;</td>
				<td><input name="f5" type="text" class="inputCheckBox" load="<root><item name='' value='0'/></root>" >
				  追踪材料明细 </td>
				</tr>
				<tr>
				<!--  
				<td align="right">&nbsp;</td>
				<td>&nbsp;</td>
				-->
				<td>&nbsp;</td>
				<td><input name="f6" type="text" class="inputCheckBox" load="<root><item name='' value='0'/></root>" >
				  追踪奖金明细 </td>
				</tr>
				<tr>
				<!--  
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				-->
				<td>&nbsp;</td>
				<td><input name="f7" type="text" class="inputCheckBox" load="<root><item name='' value='0'/></root>" >
				  追踪药品明细 </td>
				</tr>
				<tr>
				<!--  
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				-->
				<td>&nbsp;</td>
				<td><input name="f8" type="text" class="inputCheckBox" load="<root><item name='' value='0'/></root>" >
				  追踪固定资产折旧明细 </td>
				</tr>
				<tr>
				<!--  
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				-->
				<td>&nbsp;</td>
				<td><input name="f9" type="text" class="inputCheckBox" load="<root><item name='' value='0'/></root>" >
				  报表显示 </td>
				</tr>
				<tr>
				<!--  
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				-->
				<td>&nbsp;</td>
				<td><input name="f10" type="text" class="inputCheckBox" load="<root><item name='' value='0'/></root>" >
				  追踪无形资产摊销明细 </td>
				</tr>
			<tr>
				<td colspan="4" align="center"><button class="pageBtn" accessKey="S" onClick="query.submit(this);" name="cbcsDeptSysinfoPara_save">保存</button></td>
			</tr>
			<tr height="500">
				<td>&nbsp;</td>
			</tr>
				</table> 
		
	
	</body>
</html>

      
      