<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true" rowspan="2">年月</th> 
				<th noWrap="true" colspan="2">会计核算</th>
				<th noWrap="true" colspan="2" >物流管理</th>
				<th noWrap="true" colspan="2">固定资产</th>
				<th noWrap="true" colspan="2">支出控制</th>
				<th noWrap="true" colspan="2">薪酬系统</th>
				<th noWrap="true" colspan="2">无形资产</th>
				<th noWrap="true" colspan="2">人力资源</th>
		  </tr>
		  <tr noWrap="true" class="mainHead" >  
				<th noWrap="true">共享状态</th>
				<th noWrap="true">月结状态</th>
				<th noWrap="true">共享状态</th>
				<th noWrap="true">月结状态</th>
				<th noWrap="true">共享状态</th>
				<th noWrap="true">月结状态</th>
				<th noWrap="true">共享状态</th>
				<th noWrap="true">月结状态</th>
				<th noWrap="true">共享状态</th>
				<th noWrap="true">月结状态</th>
				<th noWrap="true">共享状态</th>
				<th noWrap="true">月结状态</th>
				<th noWrap="true">共享状态</th>
				<th noWrap="true">月结状态</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
				<xsl:for-each select="td">
							<td noWrap="true"><xsl:value-of select="."/></td>
				</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
