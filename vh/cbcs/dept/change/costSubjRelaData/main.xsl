<?xml version='1.0' encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">

    <thead>

      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox' value=''/></th>
      	<th nowrap='true'>成本项目编码</th>
      	<th nowrap='true'>成本项目名称</th>
		<th nowrap='true'>编码</th>
		<th nowrap='true'>名称</th>
		<th nowrap='true'>资金性质</th>
		
      </tr>

    </thead>

    <tbody>

      <xsl:for-each select="/root/tbody/tr">
		  <xsl:variable name="cost_subj_code" select="td[1]"/>
          <xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[1]=$cost_subj_code])"/>
          <xsl:variable name="rowPos" select="position()"/>
		  
        <tr>

          <td align='center'  style='display:none'>

            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>

              <xsl:attribute name="value" >

                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>

              </xsl:attribute>

            </input>

          </td>

          <xsl:for-each select="td">
			<xsl:choose>
			
				<xsl:when test="position() = 1">
                      <xsl:if test="$rowPos = 1">
                          <td align="left">
                              <xsl:if test="$cost_subj_code != ''">
                                  <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
                              </xsl:if>
                              <a href="#">
								<xsl:attribute name="onclick">
									setMain("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
								</xsl:attribute>
								<xsl:value-of select="."/>
							</a>
                          </td>
                      </xsl:if>

                      <xsl:if test="$rowPos &gt; 1">
                          <xsl:if test="$cost_subj_code != ../../tr[$rowPos - 1 ]/td[1]">
                              <td align="left">
                                  <xsl:if test="$cost_subj_code != ''">
                                      <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
                                  </xsl:if>
                                   <a href="#">
										<xsl:attribute name="onclick">
											setMain("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
                              </td>
                          </xsl:if>

                          <xsl:if test="$cost_subj_code = ../../tr[$rowPos - 1 ]/td[1]">
                              <xsl:if test="$cost_subj_code != ''">
                                  <td align="center" style="display:none" >
                                  </td>
                              </xsl:if>
                              <xsl:if test="$cost_subj_code = ''">
                                  <td align="center" >
                                      <xsl:value-of select="../td[1]"/>
                                  </td>
                              </xsl:if>
                          </xsl:if>
                      </xsl:if>
                  </xsl:when>
				  
				<xsl:when test="position() = 2">
                      <xsl:if test="$rowPos = 1">
                          <td align="left">
                              <xsl:if test="$cost_subj_code != ''">
                                  <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
                              </xsl:if>
                              <xsl:value-of select="."/>
                          </td>
                      </xsl:if>  
  
                      <xsl:if test="$rowPos &gt; 1">
                          <xsl:if test="$cost_subj_code != ../../tr[$rowPos - 1 ]/td[1]">
                              <td align="left">
                                  <xsl:if test="$cost_subj_code != ''">
                                      <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
                                  </xsl:if>
                                  <xsl:value-of select="."/>
                              </td>
                          </xsl:if>

                          <xsl:if test="$cost_subj_code = ../../tr[$rowPos - 1 ]/td[1]">
                              <xsl:if test="$cost_subj_code != ''">
                                  <td align="left" style="display:none" >
                                  </td>
                              </xsl:if>
                              <xsl:if test="$cost_subj_code = ''">
                                  <td align="left" >
                                      <xsl:value-of select="."/>
                                  </td>
                              </xsl:if>
                          </xsl:if>
                      </xsl:if>
                  </xsl:when>
				
				<xsl:otherwise>
                    <td align="left" ><xsl:value-of select="."/></td>
                </xsl:otherwise>  
            
			</xsl:choose>
          </xsl:for-each>

        </tr>

      </xsl:for-each>

    </tbody>

  </xsl:template>

</xsl:stylesheet>



