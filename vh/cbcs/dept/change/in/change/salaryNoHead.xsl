<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[td[1]!='0']">
				<tr>          
  				<xsl:for-each select="td[position() &gt; 2]">
 						   <td><xsl:value-of select="."/></td>
  				</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
