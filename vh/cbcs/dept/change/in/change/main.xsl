<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<xsl:if test="/root/tbody/tr[1]/td[1] != 'F'">
					<xsl:for-each select="/root/tbody/tr[td[2]='0']">
						<xsl:for-each select="td[position() &gt; 2]">
							<th noWrap="true"><xsl:value-of select='.'/></th> 
						</xsl:for-each>
					</xsl:for-each>			
				</xsl:if>
				
				<xsl:if test="/root/tbody/tr[1]/td[1] = 'F'">
					<th noWrap="true" >发生日期</th> 
					<th noWrap="true" >凭证号</th> 
					<th noWrap="true" >会计科目</th>
					<th noWrap="true" >部门</th>
					<th noWrap="true" >金额</th>
					<th noWrap="true" >摘要</th>
					<th noWrap="true" >操作员</th>
				</xsl:if>
		  </tr>
		</thead>
		<tbody>
			<xsl:if test="/root/tbody/tr[td[1]= 'F']">
				<xsl:for-each select="/root/tbody/tr">
					<tr>          
	  				<xsl:for-each select="td[position() > 1 ]">
	   				  <xsl:choose>
	   				    <xsl:when test="position()=5">
	     				    <td noWrap="true" align="right">
	 		  				    <xsl:value-of select="format-number(.,'#,##0.00')"/>
				    		  </td>
	   				    </xsl:when>
	   				    <xsl:otherwise>
	 				        <td noWrap="true">
	 						      <xsl:value-of select="."/>
						      </td>
	 						  </xsl:otherwise>
	 						</xsl:choose>
	  				</xsl:for-each>
					</tr>
				</xsl:for-each> 
			</xsl:if> 	
			<xsl:if test="/root/tbody/tr[td[1] != 'F']">
				<xsl:for-each select="/root/tbody/tr[td[2]!='0']">
					<tr>          
	  				<xsl:for-each select="td[position() &gt; 2]">
	 						   <xsl:choose>
		   				    <xsl:when test="position()=5 and ../../td[1] !='Z'">
		     				    <td noWrap="true" align="right">
		 		  				    <xsl:value-of select="format-number(.,'#,##0.00')"/>
					    		  </td>
		   				    </xsl:when>
		   				    <xsl:when test="position()=5 and ../../td[1] ='Z'">
		     				    <td noWrap="true">
		 		  				    <xsl:value-of select="."/>
					    		  </td>
		   				    </xsl:when>
		   				    
		   				    <xsl:otherwise>
		 				        <td noWrap="true" align="right">
		 						      <xsl:value-of select="."/>
							      </td>
		 						  </xsl:otherwise>
		 						</xsl:choose>
	  				</xsl:for-each>
					</tr>
				</xsl:for-each>  	
			</xsl:if>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
