<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/disease/profit/chart.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:58:11 $
  $Date: 2012/03/12 01:58:11 $
	$Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<script language='javascript' src='javascript/chart.js'></script>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>

<Script Language="JavaScript">
  var cewolf1;
  <%
  String[] name = (String[])request.getAttribute("name");
  if (name!=null) { %>
  cewolf1 = "";
  <% }%>
</Script>

<html:html leftMargin="0">
	<table width="100%">
	 	<tr>
		 	<td align="center">
		 		<button class="pageBtn" onclick="return saveChart()">保存</button>
		 		<button class="pageBtn" onClick="window.close()" >关闭</button>
			</td>
	  </tr>
		<tr>
		 	<td align="center">
<%
if (name != null) {
	ChartUtilities.createChartPie("cewolf1", (String[])request.getAttribute("name"), (double[])request.getAttribute("data"), pageContext);
%>
	<cewolf:chart id="chart" title="病种收益分析表" type="pie3D">
    <cewolf:colorpaint color="#EEEEFF"/>
    <cewolf:data>
        <cewolf:producer id="cewolf1"/>
    </cewolf:data>
	</cewolf:chart>
	<cewolf:img chartid="chart" renderer="/cewolf" width="640" height="450" id="cewolf1">
		<cewolf:map tooltipgeneratorid="cewolf1Tip"/>
	</cewolf:img>
<% } %>
			</td>
		</tr>
	</table>
</html:html>
