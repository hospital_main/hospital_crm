<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/disease/cost/cost.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:58:11 $
  $Date: 2012/03/12 01:58:11 $
	$Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                 java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language='javascript' src='javascript/tag.js'></script>
<Script Language="JavaScript">
function choose()
     {
      if(template.type.value=='1'){
        mon.style.display = "block"
        ji.style.display = "none"
        nian.style.display = "none"
        qijian.style.display = "none"
        }else if(template.type.value=='2'){
        mon.style.display = "none"
        ji.style.display = "block"
        nian.style.display = "none"
        qijian.style.display = "none"
        }else if(template.type.value=='3'){
        mon.style.display = "none"
        ji.style.display = "none"
        nian.style.display = "block"
        qijian.style.display = "none"
        }else if(template.type.value=='4'){
        mon.style.display = "none"
        ji.style.display = "none"
        nian.style.display = "none"
        qijian.style.display = "block"
        }else if(template.type.value=='0'){
        mon.style.display = "none"
        ji.style.display = "none"
        nian.style.display = "none"
        qijian.style.display = "none"
        }
      }
  function isEmpty1()
   {
      if(template.type.value=='1'&&template.year_month.value=='')
        {  alert("请选择时间");
           return false;
        }else if(template.type.value=='2'&&template.year_quarter.value==''){
           alert("请选择时间");
           return false;
        }else if(template.type.value=='3'&& template.year.value==''){
           alert("请选择时间");
           return false;
        }else if(template.type.value=='4'&& template.year_month_from.value==''){
           alert("请选择时间");
           return false;
        }else if(template.type.value=='4'&&  template.year_month_to.value==''){
           alert("请选择时间");
           return false;
        }
     else return true;
}

  function account() {
    if(isEmpty1()) {
	   	show_wait();
	    template.subFunction.value='account';
	    template.submit();
	    return true;
   	}
  }

 <%String[][] result=(String[][])request.getAttribute("result");%>
 function message(primary)
  {
      <% if (result==null) out.print("return true;");%>
         window.open("cbcsEcoDiseaseCost.jspviewhigh?subFunction=chart&value="+primary,null,"height=500,width=650,toolbar=no, location=no");
         return true;

   }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="cbcsEcoDiseaseCost.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>成本分析(医成本I4-02表)</html:title>

	  <!-- 简单信息 -->
	  <table>
	   <tr class="normalText" nowrap>
           <td class="signText" nowrap>分析单位:</td>
           <td nowrap>
            <select name="type" onchange="choose()" class="selectBg">
               <option value='1' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("1")) out.print("selected");%> >月</option>
               <option value='2' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("2")) out.print("selected");%>>季</option>
               <option value='3' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("3")) out.print("selected");%>>年</option>
               <option value='4' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("4")) out.print("selected");%>>期间</option>
            </select>
           </td>
           <td class="signText" nowrap>本期点：</td>
           <td id="mon" <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("1")) out.print(" style='display:block;' "); if(request.getParameter("type")!=null&&!request.getParameter("type").equals("1")) out.print(" style='display:none;' ");if(request.getParameter("type")==null) out.print(" style='display:block;' ");%>>
            <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
           <td id="ji"   <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("2")) out.print(" style='display:block;' "); if(request.getParameter("type")!=null&&!request.getParameter("type").equals("2")) out.print(" style='display:none;' "); if(request.getParameter("type")==null) out.print(" style='display:none;' ");%> nowrap>
            <%=new com.viewhigh.cbcs.base.mvc.view.component.QuarterComponent("year_quarter", request.getParameter("year_quarter"))%> </td>
            <%String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};%>
           <td id="nian"  <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("3")) out.print(" style='display:block;' "); if(request.getParameter("type")!=null&&!request.getParameter("type").equals("3")) out.print(" style='display:none;' "); if(request.getParameter("type")==null) out.print(" style='display:none;' ");%> nowrap>
            <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year",request.getParameter("year"),false,false)%>年</td>
           <td id="qijian"  <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("4")) out.print(" style='display:block;' "); if(request.getParameter("type")!=null&&!request.getParameter("type").equals("4")) out.print(" style='display:none;' "); if(request.getParameter("type")==null) out.print(" style='display:none;' ");%> nowrap>
            <%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%></td>
       </tr>
      <tr>
        <td nowrap class="signText">病种：</td>
	      <td>
         <html:select property="eco_cust_def_define_disease"  name='leave_disease_code' size="4" biSelect="true" showTip="false"/>
         </td>
          <td nowrap class="signText">病种分型：</td>
	      <td>
            <% String[] value = request.getParameterValues("leave_disease_model");
               String[][] leave_disease_model=(String[][])request.getAttribute("leave_disease_model");
           %>
           <select name="leave_disease_model"  size='4' multiple class="selectBg">
                  <option value="">--不限--</option>
                 <%if(leave_disease_model!=null) {
                     for(int i=0;i<leave_disease_model.length;i++){%>
                        <option value=<%=leave_disease_model[i][0]%>
                        <% if(value!=null){
                           for(int j=0;j<value.length;j++)
                           if(leave_disease_model[i][0].equals(value[j])) out.print("selected");
                        }
                        %> >
                     <%=leave_disease_model[i][0]+":"+leave_disease_model[i][1]%>
                  </option>
                <%}}%>
            </select>
	      </td>

	    </tr>


      <tr>
        <td  nowrap class="normalText">就诊类型:</td>
       <td>
        <select name="hospitalize_id" class="selectBg">
             <%String hospitalize_id=request.getParameter("hospitalize_id");%>
             <option value='O' <%if(hospitalize_id!=null&&hospitalize_id.equals("O")) out.print("selected");%>>门诊</option>
             <option value='I' <%if(hospitalize_id!=null&&hospitalize_id.equals("I")) out.print("selected");%>>住院</option>
          </select>
        </td>
	      <td><button class="pageBtn" onclick="return account();" >分析</button></td>
	    </tr>
	  </table>

	  <br>
		<html:title clazz='table'>病种成本分析表</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
        <% DecimalFormat nf = new DecimalFormat("#,##0.00");
        %>


		  <!-- 结果集 -->
		  <tr>
		    <td>
		    	<html:table clazz="result">
           <html:tr clazz='label'>
              <td nowrap="nowrap" rowspan=2>病种名称</td>
              <td nowrap="nowrap" rowspan=2>病种分型</td>
              <td nowrap="nowrap" colspan=5>病种总成本(元)</td>
              <td nowrap="nowrap" colspan=6>病种单位成本(元)</td>
		        </html:tr>
            <html:tr clazz='label'>
              <td nowrap="nowrap">总成本</td>
              <td nowrap="nowrap">医疗成本</td>
              <td nowrap="nowrap">西药成本</td>
              <td nowrap="nowrap">中成药成本</td>
              <td nowrap="nowrap">中草药成本</td>
              <td nowrap="nowrap">总成本</td>
              <td nowrap="nowrap">医疗成本</td>
              <td nowrap="nowrap">西药成本</td>
              <td nowrap="nowrap">中成药成本</td>
              <td nowrap="nowrap">中草药成本</td>
              <td nowrap="nowrap">就诊人次</td>
            </html:tr>

		        <%
		          if ( result != null ){
		            for (int i = 0; i < result.length; i++ ){
		               String rowColor = "rowGray";
                   String[] value01={result[i][3],result[i][4],result[i][5],result[i][6]};
                   String primary=ExtendTool.arrayToString(value01);
		               if (i/2*2==i) rowColor = "rowWhite";
		        %>
		        <tr CLASS="<%=rowColor%>">
              <td nowrap="nowrap" class="normalText"><%=result[ i ][ 0 ]%></td>
              <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td>
              <td nowrap="nowrap" class="numberText"><a href="#" onclick="message('<%=primary%>')"><%=nf.format(Double.parseDouble(result[ i ][ 2 ]))%></a></td>
               <% for(int j=3;j<result[i].length-1;j++){%>
                  <td nowrap="nowrap" class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ j ]))%></td>
		           <%}%>
               <td nowrap="nowrap" class="normalText"><%=result[ i ][ result[0].length-1 ]%>
		        </tr>
		        <%
		              }
		            }
		        %>
		      </html:table>
		    </td>
		  </tr>
	  </html:table>

	  <input type=hidden name="subFunction"/>
	</form>
</html:html>






