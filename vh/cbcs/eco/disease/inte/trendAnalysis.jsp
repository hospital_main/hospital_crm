<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/disease/inte/trendAnalysis.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<script language='javascript' src='javascript/tag.js'></script>
<script language='javascript' src='javascript/chart.js'></script>
<%@taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.text.*" %>

<%
   DecimalFormat nf;
   String[][] result = (String[][])request.getAttribute("result");
   String[][] ind = (String[][])request.getAttribute("ind");
   String[][] init_index = (String[][])request.getAttribute("init_index");
   String[][] init_disease = (String[][])request.getAttribute("init_disease");
   String[][] init_model = (String[][])request.getAttribute("init_model");
   String[] disease = request.getParameterValues("disease");
   String[] model = request.getParameterValues("model");
   boolean flag = true;
   if(disease!=null  && model!=null && (model.length*disease.length)!=1)
     flag = false;
   String[][] init_title = (String[][])request.getAttribute("init_title");
   String indexh = "";
   String diseaseh = "";
   String[] tip_idx_code=(String[])request.getAttribute("tip_idx_code");
   String[] tip_idx_name=(String[])request.getAttribute("tip_idx_name");
   String[] tip_evaluation=(String[])request.getAttribute("tip_evaluation");
   String modelh = "";
%>
<Script Language="JavaScript">
  function analysis(){
    if(template.year_month_from.value=='' || template.year_month_to.value=='' || template.year_month_from.value>template.year_month_to.value){
      alert('请选择起止年月')
      return
    }
    else if(template.index.value==''){
      alert('请选择指标集')
      return
    }
    else if(template.disease.value==''){
      alert('请选择病种集')
      return
    }
    else if(template.model.value==''){
      alert('请选择病种分型')
      return
    }

    var r=0

     <% if(init_model!=null){%>

    for(var n=0; n<<%=init_model.length%>; n++)
      if(me[n].selected==true)
        r=r+1;
   <%}%>
    if(template.index.length>8){
      alert('最多只能选择八个指标')
      return
    }
    if((template.disease.length)*r>8){
      alert('最多只能选择八个病种病型')
      return
    }
    if(template.index.length>1 && (template.disease.length)*r>1){
      alert('指标集与病种病型集不能同时多选')
      return
    }
    show_wait();
    template.submit()
  }

  var cewolf1;
  <% if (result!=null) { %>
  cewolf1 = "";
  <% }%>
</Script>

<html:html leftMargin="0">
	<form name="template" method="post" action="ecoDiseaseInte.jspviewhigh">

  	<html:message/>

		<table >
			<tr>
				<td>
					<table >
						<tr>
							<td align="center">

	<%
  if (result != null && ind != null) {
    String[] series = new String[ind.length];
    for (int i=0; i<series.length; i++) {
    	series[i] = ind[i][1];
    }
  	ChartUtilities.createChartLine("cewolf1", series, result, new int[]{0, 1, 2}, pageContext);
	%>
		<cewolf:chart id="chart" title="综合分析" type="line">
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="400" height="250" id="cewolf1" ondblclick="return adjustShape()"/>

		<cewolf:chart id="chartBig" title="综合分析" type="line" >
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chartBig" renderer="/cewolf" width="780" height="250" id="cewolf1Big" ondblclick="return adjustShape()" style="display:none"/>
	<% } else { %>
		<div style='overflow:auto;width:400px; height:250px;' />
	<% } %>
							</td>
						</tr>
					</table>
				</td>
    		<td >
	        <html:table  clazz="simple" >
	          <tr>
	            <td class="signText" nowrap="nowrap">起止年月：</td>
	            <td class="normalText" nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%></td>
	          </tr>
	          <tr>
	            <td colspan="2" class="signText" nowrap="nowrap">指标集：</td>
	          </tr>
	          <tr>
							<%String[][] index = (String[][])request.getAttribute("index");%>
	            <td colspan="2" class="normalText" nowrap="nowrap">
								<html:select property="eco_disease_inte_idx"  size="4" name='index' biSelect="true" showTip="true"/>
	            </td>
	          </tr>
	          <tr>
	            <td colspan="2" class="signText" nowrap="nowrap">病种集：</td>
	          </tr>
	          <tr>
	            <td colspan="2" class="normalText" nowrap="nowrap">
	     					<html:select property="eco_disease_inte_disease"  size="4" name='disease' biSelect="true" showTip="false"/>
	            </td>
						</tr>
						<tr>
	            <td class="signText" nowrap="nowrap">病种分型：</td>
	            <td class="normalText" nowrap="nowrap">
	              <select name="model" multiple class="selectBg">
					<%
          if(init_model!=null)
							for(int i=0; i<init_model.length; i++){
                String selected = "";
								if (model!=null) {
              		for (int j=0; j<model.length; j++) {
                    if (model[j].equals(init_model[i][0])) {
                      selected = "selected";
                      break;
                    }
              		}
                }
        %>
	                <option id='me' value='<%=init_model[i][0]%>' <%=selected%> ><%=init_model[i][0]+":"+init_model[i][1]%></option>
					<%}%>
	              </select>
	            </td>
						</tr>
						<tr>
							<td class="signText" nowrap="nowrap">就诊类型：</td>
							<td class="normalText" nowrap="nowrap">
							 	<select name="type" class="selectBg" >
							   	<option id='te' value='O' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("O")) out.print("selected");%>>门诊</option>
							   	<option id='te' value='I' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("I")) out.print("selected");%>>住院</option>
							  </select>
							</td>
						</tr>
						<tr>
							<td align="center" colspan="2">
							<button class="pageBtn" onclick="return analysis();">分析</button>
								<img id="cewolf1Zoom" src="images/zoomIn.gif" style="cursor:hand" onclick="return adjustShape();"/>
								<button class="pageBtn" onclick="return saveChart()">保存</button>
							</td>
	          </tr>
	        </html:table>
				</td>
			</tr>
		</table>
		<hr>

<%
    if(result!=null){
%>
          <html:title clazz='table'><%=init_title[0][1]%><%if(init_title[0].length==4) out.print("("+init_title[0][2]+")");%></html:title>
        <html:table clazz="result" divWidth="780">
          <html:tr clazz='label'>
            <td class="resultLabel" >年</td>
            <td class="resultLabel" >月</td>
<%
for(int i=0; i<ind.length; i++){
%>
            <td class="resultLabel" >
                 <%if(flag==true){%>
              <html:tip name='<%=tip_idx_name[i]%>' code='<%=tip_idx_code[i]%>' message='<%=tip_evaluation[i]%>'><%=ind[i][1]%><%if(ind[i].length==5) out.print("("+ind[i][3]+")");%></html:tip>
              </td>
              <%} else{%>
               <%=ind[i][1]%><%if(ind[i].length==5) out.print("("+ind[i][3]+")");%>
              <%}%>
            </td>
<%}%>
          </html:tr>
<%
       for(int j=0; j<result.length; j++){
         String rowColor = "rowGray";
         if (j/2*2==j) rowColor = "rowWhite";
%>
          <tr CLASS="<%=rowColor%>">
<%
if(j==0 || !result[j][0].equals(result[j-1][0])){
%>
            <td align="center" rowspan="<%=result[j][ind.length+2]%>"><%=result[j][0]%></td>
<%}%>
            <td class="normalText"><%=result[j][1]%></td>
<%
  for(int k=0; k<ind.length; k++){
    if(ind[0].length==5){
      if(Integer.parseInt(ind[0][4])==0){
           nf = new DecimalFormat("#,##0");
      }else{
        String zero = new String("0.");
        for(int t=0; t<Integer.parseInt(ind[k][4]); t++){
          zero += "0";
        }
        nf = new DecimalFormat("#,##"+zero);
      }
    }else{
      if(Integer.parseInt(init_title[0][3])==0){
           nf = new DecimalFormat("#,##0");
         }else{
           String zero = new String("0.");
           for(int t=0; t<Integer.parseInt(init_title[0][3]); t++){
             zero += "0";
           }
           nf = new DecimalFormat("#,##"+zero);
         }
    }
    if(result[j][k+2]==null)
      result[j][k+2] = "0";
%>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[j][k+2]))%></td>
<%}%>
          </tr>
<%}%>
        </html:table>

<%
       modelh = ExtendTool.arrayToString(request.getParameterValues("model"));
       indexh = ExtendTool.arrayToString(request.getParameterValues("index"));
       diseaseh = ExtendTool.arrayToString(request.getParameterValues("disease"));
    }
%>
<input type="hidden" name="indexh" value="<%=indexh%>">
  <input type="hidden" name="diseaseh" value="<%=diseaseh%>">
  <input type="hidden" name="modelh" value="<%=modelh%>">
  <input type="hidden" name="subFunction" value="trend">
</form>


</html:html>
