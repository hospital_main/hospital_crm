<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/disease/inte/compareAnalysis.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<script language='javascript' src='javascript/chart.js'></script>
<script language='javascript' src='javascript/tag.js'></script>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>


<%
   DecimalFormat nf ;
   DecimalFormat percentFormat = new DecimalFormat("#0.00%");
   String[][] result = (String[][])request.getAttribute("result");
   String[][] init_index = (String[][])request.getAttribute("init_index");
   String[][] init_disease = (String[][])request.getAttribute("init_disease");
   String[][] init_model = (String[][])request.getAttribute("init_model");
   String[] disease = request.getParameterValues("disease");
   String[] model = request.getParameterValues("model");
   boolean flag = true;
   if(disease!=null && disease.length!=1 && model!=null && model.length!=1)
     flag = false;
   String[][] init_title = (String[][])request.getAttribute("init_title");
   String indexh = "";
   String diseaseh = "";
   String[] tip_idx_code=(String[])request.getAttribute("tip_idx_code");
   String[] tip_idx_name=(String[])request.getAttribute("tip_idx_name");
   String[] tip_evaluation=(String[])request.getAttribute("tip_evaluation");
   String modelh = "";
%>
<Script Language="JavaScript">
  function choose()
  {
    if(template.unit.value=='1'){
      mon1.style.display = "block"
      mon2.style.display = "block"
      ji1.style.display = "none"
      ji2.style.display = "none"
      nian1.style.display = "none"
      nian2.style.display = "none"
      qijian1.style.display = "none"
      qijian2.style.display = "none"
    }
    else if(template.unit.value=='2'){
      mon1.style.display = "none"
      mon2.style.display = "none"
      ji1.style.display = "block"
      ji2.style.display = "block"
      nian1.style.display = "none"
      nian2.style.display = "none"
      qijian1.style.display = "none"
      qijian2.style.display = "none"
    }
    else if(template.unit.value=='3'){
      mon1.style.display = "none"
      mon2.style.display = "none"
      ji1.style.display = "none"
      ji2.style.display = "none"
      nian1.style.display = "block"
      nian2.style.display = "block"
      qijian1.style.display = "none"
      qijian2.style.display = "none"
    }
    else if(template.unit.value=='4'){
      mon1.style.display = "none"
      mon2.style.display = "none"
      ji1.style.display = "none"
      ji2.style.display = "none"
      nian1.style.display = "none"
      nian2.style.display = "none"
      qijian1.style.display = "block"
      qijian2.style.display = "block"
    }
  }

  function analysis(){
    if(template.unit.value=='1' && template.year_month1.value==''){
      alert('请选择报告期')
      return
    }
    else if(template.unit.value=='1' && template.year_month2.value==''){
      alert('请选择基期')
      return
    }
    else if(template.unit.value=='2' && template.year_quarter1.value==''){
      alert('请选择报告期')
      return
    }
    else if(template.unit.value=='2' && template.year_quarter2.value==''){
      alert('请选择基期')
      return
    }
    else if(template.unit.value=='3' && template.year1.value==''){
      alert('请选择报告期')
      return
    }
    else if(template.unit.value=='3' && template.year2.value==''){
      alert('请选择基期')
      return
    }
    else if(template.unit.value=='4' && (template.year_month_from1.value=='' || template.year_month_to1.value=='')){
      alert('请选择报告期')
      return
    }
    else if(template.unit.value=='4' && (template.year_month_from2.value=='' || template.year_month_to2.value=='')){
      alert('请选择基期')
      return
    }
    if(template.unit.value=='1' && template.year_month1.value<=template.year_month2.value){
      alert('报告期应大于基期!')
      return false;
    }
    if(template.unit.value=='2' && template.year_quarter1.value<=template.year_quarter2.value){
      alert('报告期应大于基期!')
      return false;
    }
    if(template.unit.value=='3' && template.year1.value<=template.year2.value){
      alert('报告期应大于基期!')
      return false;
    }
    if(template.unit.value=='4' && template.year_month_from1.value>=template.year_month_to1.value){
      alert('报告期开始时间应小于结束时间!')
      return false;
    }
    if(template.unit.value=='4' && template.year_month_from2.value>=template.year_month_to2.value){
      alert('基期开始时间应小于结束时间!')
      return false;
    }

    if(template.unit.value=='4' && template.year_month_to1.value<=template.year_month_to2.value){
      alert('报告期结束时间应大于基期结束时间!')
      return false;
    }
    else if(template.index.value==''){
      alert('请选择指标集')
      return
    }
    else if(template.disease.value==''){
      alert('请选择病种集')
      return
    }
    else if(template.model.value==''){
      alert('请选择病种分型')
      return
    }
   var r=0
   <%if(init_model!=null){%>

    for(var n=0; n<<%=init_model.length%>; n++)
      if(me[n].selected==true)
        r=r+1;
    <%}%>
    if(template.index.length>8){

      alert('最多只能选择八个指标')
      return
    }
    if((template.disease.length)*r>8){
      alert('最多只能选择八个病种病型')
      return
    }
    if(template.index.length>1 && (template.disease.length)*r>1){
      alert('指标集与病种病型集不能同时多选')
      return
    }
    show_wait();
    template.submit()
  }


  var cewolf1;
  <% if (result!=null) { %>
  cewolf1 = "";
  <% }%>
</Script>

<html:html leftMargin="0">
	<form name="template" method="post" action="ecoDiseaseInte.jspviewhigh">

  	<html:message/>

		<table>
			<tr>
				<td>
					<table >
						<tr>
							<td align="center">
	<%
	if (result != null) {
		ChartUtilities.createChartBar("cewolf1", result, new int[]{1, 2, 3, 4}, pageContext);
	%>
		<cewolf:chart id="chart" title="综合分析" type="horizontalBar3D">
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="400" height="250" id="cewolf1" ondblclick="return adjustShape()">
			<cewolf:map tooltipgeneratorid="cewolf1Tip"/>
		</cewolf:img>

		<cewolf:chart id="chartBig" title="综合分析" type="horizontalBar3D" >
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chartBig" renderer="/cewolf" width="780" height="250" id="cewolf1Big" ondblclick="return adjustShape()" style="display:none">
			<cewolf:map tooltipgeneratorid="cewolf1Tip"/>
		</cewolf:img>
	<% } else { %>
		<div style='overflow:auto;width:400px; height:250px;' />
	<% } %>
							</td>
						</tr>
					</table>
				</td>
      	<td >
        	<table >
	          <tr>
	            <td class="signText" nowrap="nowrap">分析单位：</td>
	            <td class="signText" nowrap="nowrap">
	              <select name="unit" onchange="choose()" class="selectBg">
	                <option value='1' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("1")) out.print("selected");%> >月</option>
	                <option value='2' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("2")) out.print("selected");%>>季</option>
	                <option value='3' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("3")) out.print("selected");%>>年</option>
	                <option value='4' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("4")) out.print("selected");%>>期间</option>
	              </select>
	            </td>
						</tr>
						<tr>
	            <td class="signText" nowrap="nowrap">报告期：</td>
	            <td id="mon1" class="normalText" style='display:block;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month1", request.getParameter("year_month1"))%></td>
	            <td id="ji1" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.QuarterComponent("year_quarter1", request.getParameter("year_quarter1"))%></td>
							<%String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};%>
	            <td id="nian1" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year1",request.getParameter("year1"),false,false)%>年</td>
	            <td id="qijian1" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from1", request.getParameter("year_month_from1"),"year_month_to1",request.getParameter("year_month_to1"))%></td>
						</tr>
						<tr>
	            <td class="signText" nowrap="nowrap">基期：</td>
	            <td id="mon2" class="normalText" style='display:block;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month2", request.getParameter("year_month2"))%></td>
	            <td id="ji2" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.QuarterComponent("year_quarter2", request.getParameter("year_quarter2"))%></td>
	            <td id="nian2" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year2",request.getParameter("year2"),false,false)%>年</td>
	            <td id="qijian2" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from2", request.getParameter("year_month_from2"),"year_month_to2",request.getParameter("year_month_to2"))%></td>
	          </tr>
	          <tr>
	            <td colspan="2" class="signText" nowrap="nowrap">指标集：</td>
	          </tr>
	          <tr>
							<%String[][] index = (String[][])request.getAttribute("index");%>
	            <td colspan="2" class="normalText" nowrap="nowrap">
	 							<html:select property="eco_disease_inte_idx"  size="4" name='index' biSelect="true" showTip="true"/>
	            </td>
	          </tr>
	          <tr>
	            <td colspan="2" class="signText" nowrap="nowrap">病种集：</td>
	          </tr>
	          <tr>
	            <td colspan="2" class="normalText" nowrap="nowrap">
								<html:select property="eco_disease_inte_disease"  size="4" name='disease' biSelect="true" showTip="false"/>
	            </td>
						</tr>
						<tr>
	            <td class="signText" nowrap="nowrap">病种分型：</td>
	            <td class="normalText" nowrap="nowrap">
	              <select name="model" multiple class="selectBg">
					<%
          if(init_model!=null)
							for(int i=0; i<init_model.length; i++){
                String selected = "";
								if (model!=null) {
              		for (int j=0; j<model.length; j++) {
                    if (model[j].equals(init_model[i][0])) {
                      selected = "selected";
                      break;
                    }
              		}
                }
        %>

	                <option id='me' value='<%=init_model[i][0]%>' <%=selected%> ><%=init_model[i][0]+":"+init_model[i][1]%></option>
					<%}%>
	              </select>
	            </td>
						</tr>
						<tr>
	            <td class="signText" nowrap="nowrap">就诊类型：</td>
	            <td class="signText" nowrap="nowrap">
	              <select name="type" class="selectBg">
	                 <option id='te' value='O' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("O")) out.print("selected");%>>门诊</option>
	                 <option id='te' value='I' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("I")) out.print("selected");%>>住院</option>
	                </select>
	            </td>
						</tr>
						<tr>
							<td colspan="2" align="center">
								<button class="pageBtn"  onclick="return analysis();">分析</button>
								<img id="cewolf1Zoom" src="images/zoomIn.gif" style="cursor:hand" onclick="return adjustShape();"/>
								<button class="pageBtn" onclick="return saveChart()">保存</button>
							</td>
	          </tr>
	        </table>
				</td>
			</tr>
		</table>
		<hr>

<%
    if(result!=null){
%>
      	<html:title clazz='table'><%=init_title[0][1]%></html:title>
	      <html:table clazz="result" divWidth="780">
          <html:tr clazz='label'>
            <td class="resultLabel" rowspan=2><%if(flag==true) out.print("指标名称");else out.print("病种病型名称");%></td>
            <td class="resultLabel" rowspan=2>报告期值</td>
            <td class="resultLabel" rowspan=2>计量单位</td>
            <td class="resultLabel" colspan="3">与基期比较</td>
            <td class="resultLabel" colspan="3">与预算比较</td>
          </html:tr>
          <html:tr clazz='label'>
            <td class="resultLabel">基期值</td>
            <td class="resultLabel">差异</td>
            <td class="resultLabel">差异率</td>
            <td class="resultLabel">预算值</td>
            <td class="resultLabel">差异</td>
            <td class="resultLabel">差异率</td>
          </html:tr>
<%
       for(int i=0; i<result.length; i++){
         if(Integer.parseInt(result[i][6])==0){
           nf = new DecimalFormat("#,##0");
         }else{
           String zero = new String("0.");
           for(int j=0; j<Integer.parseInt(result[i][6]); j++){
             zero += "0";
           }
           nf = new DecimalFormat("#,##"+zero);
         }
         String rowColor = "rowGray";
         if (i/2*2==i) rowColor = "rowWhite";
%>
          <tr CLASS="<%=rowColor%>">
            <td class="normalText">
                <%if(flag==true){%>
              <html:tip name='<%=tip_idx_name[i]%>' code='<%=tip_idx_code[i]%>' message='<%=tip_evaluation[i]%>'><%=result[i][1]%></html:tip>
              </td>
              <%} else{%>
               <%=result[i][1]%>
              <%}%>
              </td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][2]))%></td>
            <td class="normalText"><%=result[i][5]%></td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][3]))%></td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][2])-Double.parseDouble(result[i][3]))%></td>
<%
  double rate;
  if(Double.parseDouble(result[i][3])==0)
    rate=0;
  else
    rate=(Double.parseDouble(result[i][2])-Double.parseDouble(result[i][3]))/Double.parseDouble(result[i][3]);
%>
            <td class="numberText"><%=percentFormat.format(rate)%></td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][4]))%></td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][2])-Double.parseDouble(result[i][4]))%></td>
<%
  if(Double.parseDouble(result[i][4])==0)
    rate=0;
  else
    rate=(Double.parseDouble(result[i][2])-Double.parseDouble(result[i][4]))/Double.parseDouble(result[i][4]);
%>
            <td class="numberText"><%=percentFormat.format(rate)%></td>
          </tr>
<%}%>
        </html:table>

<%
       modelh = ExtendTool.arrayToString(request.getParameterValues("model"));
       indexh = ExtendTool.arrayToString(request.getParameterValues("index"));
       diseaseh = ExtendTool.arrayToString(request.getParameterValues("disease"));
    }
%>

  <input type="hidden" name="indexh" value="<%=indexh%>">
  <input type="hidden" name="diseaseh" value="<%=diseaseh%>">
  <input type="hidden" name="modelh" value="<%=modelh%>">
  <input type="hidden" name="subFunction" value="compare">
</form>
<Script Language="JavaScript">
 choose()
</Script>

</html:html>
