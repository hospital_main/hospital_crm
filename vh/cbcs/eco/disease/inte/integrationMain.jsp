<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/disease/inte/integrationMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript">
  function compare(){
  	wait();
    document.tt.action="ecoDiseaseInte.jspviewhigh?subFunction=compare";
    document.tt.target="methods";
    document.tt.submit();
  }
  function structure(){
  	wait();
    document.tt.action="ecoDiseaseInte.jspviewhigh?subFunction=struct";
    document.tt.target="methods";
    document.tt.submit();
  }
  function trend(){
  	wait();
    document.tt.action="ecoDiseaseInte.jspviewhigh?subFunction=trend";
    document.tt.target="methods";
    document.tt.submit();
  }
  
  var _1st_current=1;
  function wait() {
		document.getElementById("_wait").style.display='';
		document.getElementById("_interval").style.display='none';
	}
	
	function show() {
		document.getElementById("_wait").style.display='none';
		document.getElementById("_interval").style.display='';
	}
	
</Script>

<html:html clazz="main">
  <html:message/>

  <html:title clazz='module'>综合分析</html:title>
  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td width="25%" class="signText" nowrap="nowrap">分析方法：</td>
      <td width="25%" class="signText" nowrap="nowrap"><input type="radio" name="methods" value="0" onclick="compare()" checked>比较分析</td>
      <td width="25%" class="signText" nowrap="nowrap"><input type="radio" name="methods" value="1" onclick="structure()">构成分析</td>
      <td width="25%" class="signText" nowrap="nowrap"><input type="radio" name="methods" value="2" onclick="trend()">趋势分析</td>
    </tr>
    <tr>
      <td colspan=20><hr></td>
    </tr>
    <tr>
	 		<td id='_wait' align='center' width='50'><img id='_image' src='img/loading.gif'/ onclick="show()" style="cursor:hand"></td>
			<td id='_interval' width=1023 colspan="20" style="display:none">
        <iframe  VSPACE="1" HSPACE="1" FRAMEBORDER="0" WIDTH="100%" SRC="ecoDiseaseInte.jspviewhigh?subFunction=compare" NAME="methods" HEIGHT="480" ></iframe>
      </td>
      <form name="tt" method="post"></form>
    </tr>
  </html:table>
  <input type="hidden" name="subFunction" >
</html:html>
