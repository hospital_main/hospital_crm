<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/dept/inte/structureAnalysis.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@taglib uri="/WEB-INF/taglib/viewhigh-html.tld"prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<script language='javascript' src='javascript/chart.js'></script>
<script language='javascript' src='javascript/tag.js'></script>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>


<%
   DecimalFormat nf;
   String[][] result = (String[][])request.getAttribute("result");
   String[][] init_index = (String[][])request.getAttribute("init_index");
   String[][] init_dept = (String[][])request.getAttribute("init_dept");
   String[] dept = request.getParameterValues("dept");
   boolean flag = true;
   if(dept!=null && dept.length!=1)
     flag = false;
   String[][] init_title = (String[][])request.getAttribute("init_title");
   String singleselected = "";
    String[] tip_idx_code=(String[])request.getAttribute("tip_idx_code");
   String[] tip_idx_name=(String[])request.getAttribute("tip_idx_name");
   String[] tip_evaluation=(String[])request.getAttribute("tip_evaluation");
   String multiselected = "";
%>
<Script Language="JavaScript">
  function choose()
  {
    if(template.unit.value=='1'){
      mon1.style.display = "block"
      ji.style.display = "none"
      nian.style.display = "none"
      qijian.style.display = "none"
    }
    else if(template.unit.value=='2'){
      mon1.style.display = "none"
      ji.style.display = "block"
      nian.style.display = "none"
      qijian.style.display = "none"
    }
    else if(template.unit.value=='3'){
      mon1.style.display = "none"
      ji.style.display = "none"
      nian.style.display = "block"
      qijian.style.display = "none"
    }
    else if(template.unit.value=='4'){
      mon1.style.display = "none"
      ji.style.display = "none"
      nian.style.display = "none"
      qijian.style.display = "block"
    }
  }
  function analysis(){
    if(template.unit.value=='1' && template.year_month.value==''){
      alert('请选择时间点')
      return
    }
    else if(template.unit.value=='2' && template.year_quarter.value==''){
      alert('请选择时间点')
      return
    }
    else if(template.unit.value=='3' && template.year.value==''){
      alert('请选择时间点')
      return
    }
    else if(template.unit.value=='4' && (template.year_month_from.value=='' || template.year_month_to.value=='')){
      alert('请选择时间点')
      return
    }else if(template.index.value==''){
      alert('请选择指标集')
      return
    }
    else if(template.dept.value==''){
      alert('请选择科室集')
      return
    }
    if(template.index.length>20){
      alert('最多只能选择二十个指标')
      return
    }
    if(template.dept.length>20){
      alert('最多只能选择二十个科室')
      return
    }
    if(template.dept.length>1 && template.index.length>1){
      alert('指标集与科室集不能同时多选')
      return
    }
    show_wait();
    template.submit()
  }

  var cewolf1;
  <% if (result!=null) { %>
  cewolf1 = "";
  <% }%>
	

</Script>
<html:html clazz="child" leftMargin="0">
<form name="template" method="post" action="ecoDeptInte.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

	<table >
		<tr>
			<td>
				<table >
					<tr>
						<td align="center">
	<%
	if (result != null) {
		ChartUtilities.createChartPie("cewolf1", result, new int[]{2, 1}, pageContext);
	%>
		<cewolf:chart id="chart" title="<%=init_title[0][1]%>" type="pie3D">
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="200" height="250" id="cewolf1" ondblclick="return adjustShape()">
			<cewolf:map tooltipgeneratorid="cewolf1Tip"/>
		</cewolf:img>

		<cewolf:chart id="chartBig" title="<%=init_title[0][1]%>" type="pie3D" >
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chartBig" renderer="/cewolf" width="360" height="250" id="cewolf1Big" ondblclick="return adjustShape()" style="display:none">
			<cewolf:map tooltipgeneratorid="cewolf1Tip"/>
		</cewolf:img>
	<% } else { %>
		<div style='overflow:auto;width:200px; height:250px;' />
	<% } %>
						</td>
					</tr>
				</table>
			</td>
      <td >
        <table>
          <tr>
            <td class="signText" nowrap="nowrap">分析单位：</td>
			 <td  align="left"  nowrap="nowrap" >
              <select name="unit" onchange="choose()" class="selectBg">
                <option value='1' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("1")) out.print("selected");%> >月</option>
                <option value='2' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("2")) out.print("selected");%>>季</option>
                <option value='3' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("3")) out.print("selected");%>>年</option>
                <option value='4' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("4")) out.print("selected");%>>期间</option>
              </select>
            </td>
</tr>
<tr>
            <td class="signText" nowrap="nowrap">时&nbsp;间&nbsp;点：</td>
             <td id="mon1"  style='display:block;' align="left"  nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
            <td id="ji" align="left" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.QuarterComponent("year_quarter", request.getParameter("year_quarter"))%></td>
<%String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};%>
            <td id="nian" align="left" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year",request.getParameter("year"),false,false)%>年</td>
            <td id="qijian" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%></td>
          </tr>
          <tr>
            <td  class="signText" nowrap="nowrap">指&nbsp;标&nbsp;集：</td>
          </tr>
          <tr>
		  <td></td>
            <td align="left" nowrap="nowrap">
							<html:select property="eco_dept_inte_idx"  size="4" name='index' biSelect="true" showTip="true"/>
            </td>
          </tr>
          <tr>
            <td class="signText" nowrap="nowrap">科&nbsp;室&nbsp;集：</td>
          </tr>
          <tr>
		  <td></td>
            <td align="left" nowrap="nowrap">
      <html:select property="eco_dept_inte_dept"  size="4" name='dept' biSelect="true" showTip="false"/>
            </td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<button class="pageBtn" onclick="return analysis();">分析</button>
							<button class="pageBtn" accessKey="B" id="cewolf1Zoom" onclick="return adjustShape();">放大</button>
							<button class="pageBtn" onclick="return saveChart()">保存</button>
						</td>
          </tr>
        </table>
			</td>
		</tr>
	</table>
<%
    if(result!=null){
   String[] temp = new String[result.length];
%>
      	<html:title clazz='table'><%=init_title[0][1]%></html:title>
        <html:table clazz="result" divWidth="780">
          <html:tr clazz='label'>
            <td class="resultLabel" ><%if(flag==true) out.print("指标名称");else out.print("科室名称");%></td>
            <td class="resultLabel" >本期值（<%=result[0][3]%>）</td>
          </html:tr>
          <tr CLASS="rowGray">
<%
         temp[result.length-1] = result[result.length-1][0];
         if(Integer.parseInt(result[result.length-1][4])==0){
           nf = new DecimalFormat("#,##0");
         }else{
           String zero = new String("0.");
           for(int j=0; j<Integer.parseInt(result[result.length-1][4]); j++){
             zero += "0";
           }
           nf = new DecimalFormat("#,##"+zero);
         }
%>
            <td class="normalText">
                <%if(flag==true){%>
              <html:tip name='<%=tip_idx_name[result.length-1]%>' code='<%=tip_idx_code[result.length-1]%>' message='<%=tip_evaluation[result.length-1]%>'><%=result[result.length-1][2]%></html:tip>
              </td>
              <%} else{%>
               <%=result[result.length-1][2]%>
              <%}%>
              </td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[result.length-1][1]))%></td>
          </tr>
<%
       for(int i=0; i<result.length-1; i++){
         temp[i] = result[i][0];
         if(Integer.parseInt(result[i][4])==0){
           nf = new DecimalFormat("#,##0");
         }else{
           String zero = new String("0.");
           for(int j=0; j<Integer.parseInt(result[i][4]); j++){
             zero += "0";
           }
           nf = new DecimalFormat("#,##"+zero);
         }
         String rowColor = "rowGray";
         if (i/2*2==i) rowColor = "rowWhite";
%>
          <tr CLASS="<%=rowColor%>">
            <td class="normalText">
              <%if(flag==true){%>
              <html:tip name='<%=tip_idx_name[i]%>' code='<%=tip_idx_code[i]%>' message='<%=tip_evaluation[i]%>'><%=result[i][2]%></html:tip>
              </td>
              <%} else{%>
               <%=result[i][2]%>
              <%}%>

              </td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][1]))%></td>
          </tr>
<%}%>
        </html:table>

<%
       singleselected = init_title[0][0];
       multiselected = ExtendTool.arrayToString(temp);
    }
%>

  <input type="hidden" name="singleselected" value="<%=singleselected%>">
  <input type="hidden" name="multiselected" value="<%=multiselected%>">
  <input type="hidden" name="single" value="<%if(request.getParameterValues("dept")!=null && request.getParameterValues("dept").length==1) out.print("D");else out.print("I");%>">
  <input type="hidden" name="subFunction" value="struct">

</form>

<Script Language="JavaScript">
 choose()
</Script>

</html:html>
