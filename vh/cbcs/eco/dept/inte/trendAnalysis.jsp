<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/dept/inte/trendAnalysis.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<script language='javascript' src='javascript/chart.js'></script>
<script language='javascript' src='javascript/tag.js'></script>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.text.*" %>


<%
   DecimalFormat nf;
   String[][] result = (String[][])request.getAttribute("result");
   String[][] ind = (String[][])request.getAttribute("ind");
   String[][] init_index = (String[][])request.getAttribute("init_index");
   String[][] init_dept = (String[][])request.getAttribute("init_dept");
   String[] dept = request.getParameterValues("dept");
   boolean flag = true;
   if(dept!=null && dept.length!=1)
     flag = false;
   String[][] init_title = (String[][])request.getAttribute("init_title");
   String singleselected = "";
    String[] tip_idx_code=(String[])request.getAttribute("tip_idx_code");
   String[] tip_idx_name=(String[])request.getAttribute("tip_idx_name");
   String[] tip_evaluation=(String[])request.getAttribute("tip_evaluation");
   String multiselected = "";
%>
<Script Language="JavaScript">
  function analysis(){
    if(template.year_month_from.value=='' || template.year_month_to.value=='' || template.year_month_from.value>template.year_month_to.value){
      alert('请选择起止年月')
      return
    }
    else if(template.index.value==''){
      alert('请选择指标集')
      return
    }
    else if(template.dept.value==''){
      alert('请选择科室集')
      return
    }

    if(template.index.length>8){
      alert('最多只能选择八个指标')
      return
    }
    if(template.dept.length>8){
      alert('最多只能选择八个科室')
      return
    }
    if(template.index.length>1 && template.dept.length>1){
      alert('指标集与科室集不能同时多选')
      return
    }
    show_wait();
    template.submit()
  }

  var cewolf1;
  <% if (result!=null) { %>
  cewolf1 = "";
  <% }%>

</Script>
<html:html leftMargin="0">
<form name="template" method="post" action="ecoDeptInte.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

	<table>
		<tr>
			<td>
				<table >
					<tr>
						<td align="center">

	<%
  if (result != null && ind != null) {
    String[] series = new String[ind.length];
    for (int i=0; i<series.length; i++) {
    	series[i] = ind[i][1];
    }
  	ChartUtilities.createChartLine("cewolf1", series, result, new int[]{0, 1, 2}, pageContext);
	%>
		<cewolf:chart id="chart" title="综合分析" type="line">
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="200" height="250" id="cewolf1" ondblclick="return adjustShape()"/>

		<cewolf:chart id="chartBig" title="综合分析" type="line" >
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chartBig" renderer="/cewolf" width="360" height="250" id="cewolf1Big" ondblclick="return adjustShape()" style="display:none"/>
	<% } else { %>
		<div style='overflow:auto;width:200px; height:250px;' />
	<% } %>
						</td>
					</tr>
				</table>
			</td>
      <td >
        <table>
          <tr>
            <td class="signText" nowrap="nowrap">起止年月：</td>
            <td align="left" nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%></td>
            </tr>
          <tr>
            <td class="signText" nowrap="nowrap">指&nbsp;标&nbsp;集：</td>
            </tr>
            <tr>
							<%String[][] index = (String[][])request.getAttribute("index");%>
			<td></td>
            <td  class="normalText" nowrap="nowrap">
          <html:select property="eco_dept_inte_idx"  size="4" name='index' biSelect="true" showTip="true"/>
            </td>
          </tr>
          <tr>
            <td class="signText" nowrap="nowrap">科&nbsp;室&nbsp;集：</td>
            </tr>
            <tr>
			<td></td>
            <td align="left" nowrap="nowrap">
							<html:select property="eco_dept_inte_dept"  size="4" name='dept' biSelect="true" showTip="false"/>
            </td>
					</tr>
            

					<tr>
						<td align="center" colspan="2">
						<button class="pageBtn" onclick="return analysis();">分析</button>
							<button class="pageBtn" accessKey="B" id="cewolf1Zoom" onclick="return adjustShape();">放大</button>
							<button class="pageBtn" onclick="return saveChart()">保存</button>
						</td>
          </tr>
        </table>
			</td>
		</tr>
	</table>
<%
    if(result!=null){
   String[] temp = new String[ind.length];
%>
   <html:title clazz='table' >
     <%=init_title[0][1]%><%if(init_title[0].length==4) out.print("("+init_title[0][2]+")");%>
   </html:title>

   <html:table clazz="result" divWidth="780">
     <html:tr clazz='label'>
            <td class="resultLabel" >年</td>
            <td class="resultLabel" >月</td>
<%
for(int i=0; i<ind.length; i++){
         temp[i] = ind[i][0];
%>
            <td class="resultLabel" >
                <%if(flag==true){%>
              <html:tip name='<%=tip_idx_name[i]%>' code='<%=tip_idx_code[i]%>' message='<%=tip_evaluation[i]%>'><%=ind[i][1]%><%if(ind[i].length==5) out.print("("+ind[i][3]+")");%></html:tip>
              </td>
              <%} else{%>
               <%=ind[i][1]%><%if(ind[i].length==5) out.print("("+ind[i][3]+")");%>
              <%}%>
              </td>
<%}%>
          </html:tr>
<%
       for(int j=0; j<result.length; j++){
         String rowColor = "rowGray";
         if (j/2*2==j) rowColor = "rowWhite";
%>
          <tr CLASS="<%=rowColor%>">
<%
if(j==0 || !result[j][0].equals(result[j-1][0])){
%>
            <td align="center" rowspan="<%=result[j][ind.length+2]%>"><%=result[j][0]%></td>
<%}%>
            <td class="normalText"><%=result[j][1]%></td>
<%
  for(int k=0; k<ind.length; k++){
    if(ind[0].length==5){
      if(Integer.parseInt(ind[0][4])==0){
           nf = new DecimalFormat("#,##0");
      }else{
        String zero = new String("0.");
        for(int t=0; t<Integer.parseInt(ind[k][4]); t++){
          zero += "0";
        }
        nf = new DecimalFormat("#,##"+zero);
      }
    }else{
      if(Integer.parseInt(init_title[0][3])==0){
           nf = new DecimalFormat("#,##0");
         }else{
           String zero = new String("0.");
           for(int t=0; t<Integer.parseInt(init_title[0][3]); t++){
             zero += "0";
           }
           nf = new DecimalFormat("#,##"+zero);
         }
    }
    if(result[j][k+2]==null)
      result[j][k+2] = "0";
%>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[j][k+2]))%></td>
<%}%>
          </tr>
<%}%>
        </html:table>

<%
       singleselected = init_title[0][0];
       multiselected = ExtendTool.arrayToString(temp);
    }
%>

  <input type="hidden" name="singleselected" value="<%=singleselected%>">
  <input type="hidden" name="multiselected" value="<%=multiselected%>">
  <input type="hidden" name="single" value="<%if(request.getParameterValues("dept")!=null && request.getParameterValues("dept").length==1) out.print("D");else out.print("I");%>">
  <input type="hidden" name="subFunction" value="trend">
</form>

</html:html>

