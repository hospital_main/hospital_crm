<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/dept/clinicprofit/clinicprofitchart.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:58:11 $
  $Date: 2012/03/12 01:58:11 $
	$Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<script language='javascript' src='javascript/chart.js'></script>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>

<Script Language="JavaScript">
	function draw() {
		show_wait();
		template.submit();
	  return true;
	}

  var cewolf1;
  <%
  String[] name = (String[])request.getAttribute("name");
  if (name!=null) { %>
  cewolf1 = "";
  <% }%>
</Script>

<html:html leftMargin="0">
	<form name="template" method="get" action="clinicprofit.jspviewhigh">
		<input type="hidden" name="type" value=<%=request.getParameter("type")%>>
		<input type="hidden" name="section" value=<%=request.getParameter("section")%>>
		<input type="hidden" name="col" value=<%=request.getParameter("col")%>>
		<input type="hidden" name="row" value=<%=request.getParameter("row")%>>
		<input type="hidden" name="year_month" value=<%=request.getParameter("year_month")%>>
		<input type="hidden" name="year_quarter" value=<%=request.getParameter("year_quarter")%>>
		<input type="hidden" name="year" value=<%=request.getParameter("year")%>>
		<input type="hidden" name="year_month_from" value=<%=request.getParameter("year_month_from")%>>
		<input type="hidden" name="year_month_to" value=<%=request.getParameter("year_month_to")%>>
		<input type="hidden" name="subFunction" value="clinicprofitfinishchart">
    <input type="hidden" name="title" value="<%=request.getParameter("title")%>">
		<table width="100%">
			<tr>
				<td>
					<table width="100%">
					<% String title="";
             String col=request.getParameter("col");
					   if(col!=null&&!col.equals("null")) {
                title=request.getParameter("title");
					  %>
					  <tr>
				     	<td align="center" class="numberText"><input type='radio' name="message" value="TOP"  <%if(request.getParameter("message")!=null&&request.getParameter("message").equals("TOP")) out.print("checked");%> <%if (request.getParameter("message")==null) out.println("checked");%>>一级科室</td>
				     	<td align="center" class="numberText"><input type='radio' name="message" value="Y"    <%if(request.getParameter("message")!=null&&request.getParameter("message").equals("Y")) out.print(" checked");%>>末级科室</td>
							<td align="center">
							<button class="pageBtn" onclick="draw()">分析</button>
								<button class="pageBtn" onclick="return saveChart()">保存</button>
								<button class="pageBtn" onClick="window.close()" >关闭</button>
							</td>
					  </tr>
					<%}%>
					<% String row=request.getParameter("row");
					   if(row!=null&&!row.equals("null")) {
                title=request.getParameter("title");
					  %>
						<tr>
							<td align="center" class="numberText"><input type='radio' name="message1" value="subtotal"    <%if(request.getParameter("message1")!=null&&request.getParameter("message1").equals("subtotal")) out.print("checked");%>  <%if (request.getParameter("message1")==null) out.println("checked");%>>小计</td>
						 	<td align="center" class="numberText"><input type='radio' name="message1" value="detail"  <%if(request.getParameter("message1")!=null&&request.getParameter("message1").equals("detail")) out.print("checked");%>>明细</td>
						 	<td align="center">
						 	<button class="pageBtn" onclick="draw()">分析</button>
						 		<button class="pageBtn" onclick="return saveChart()">保存</button>
								<button class="pageBtn" onClick="window.close()" >关闭</button>
							</td>
						</tr>
					<%}%>
					</table>
				</td>
			</tr>
			<tr>
			 	<td align="center">
	<%
	if (name != null) {
		ChartUtilities.createChartPie("cewolf1", (String[])request.getAttribute("name"), (double[])request.getAttribute("data"), pageContext);
	%>
		<cewolf:chart id="chart" title="<%=title%>" type="pie3D">
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="640" height="450" id="cewolf1">
			<cewolf:map tooltipgeneratorid="cewolf1Tip"/>
		</cewolf:img>
	<% } %>
				</td>
			</tr>
		</table>
	</form>
</html:html>

