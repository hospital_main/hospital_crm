<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/dept/structureprofit/structureprofit.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:58:11 $
  $Date: 2012/03/12 01:58:11 $
	$Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DisplayWidth"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript">
function choose()
  {
      if(template.type.value=='1'){
        mon.style.display = "block"
        ji.style.display = "none"
        nian.style.display = "none"
        qijian.style.display = "none"
        }else if(template.type.value=='2'){
        mon.style.display = "none"
        ji.style.display = "block"
        nian.style.display = "none"
        qijian.style.display = "none"
        }else if(template.type.value=='3'){
        mon.style.display = "none"
        ji.style.display = "none"
        nian.style.display = "block"
        qijian.style.display = "none"
        }else if(template.type.value=='4'){
        mon.style.display = "none"
        ji.style.display = "none"
        nian.style.display = "none"
        qijian.style.display = "block"
        }else if(template.type.value=='0'){
        mon.style.display = "none"
        ji.style.display = "none"
        nian.style.display = "none"
        qijian.style.display = "none"
        }
      }
function isEmpty1()
   {
      if(template.type.value=='1'&&template.year_month.value=='')
        {  alert("请选择时间");
           return false;
        }else if(template.type.value=='2'&&template.year_quarter.value==''){
           alert("请选择时间");
           return false;
        }else if(template.type.value=='3'&& template.year.value==''){
           alert("请选择时间");
           return false;
        }else if(template.type.value=='4'&& template.year_month_from.value==''){
           alert("请选择时间");
           return false;
        }else if(template.type.value=='4'&&  template.year_month_to.value==''){
           alert("请选择时间");
           return false;
        }
   else return true;
}
<%
 	String[][] result = (String[][])request.getAttribute("result");
%>
  function message(i,title)
  { 	<% if (result==null) out.print("return true;");%>
   if(isEmpty1())
    {
     var type=template.type.value
     var section="5";
    if(template.type.value=='1')
       {
          var year_month=template.year_month.value
          window.open("structureprofit.jspviewhigh?subFunction=structureprofitchart&title="+title+"&col="+i+"&year_month="+year_month+"&type="+type+"&section="+section,null,"height=500,width=650,toolbar=no, location=no")
       }else if(template.type.value=='2'){
          var year_quarter=template.year_quarter.value
          window.open("structureprofit.jspviewhigh?subFunction=structureprofitchart&title="+title+"&col="+i+"&year_quarter="+year_quarter+"&type="+type+"&section="+section+"&message=TOP",null,"top=180, left=200, height=500,width=650,toolbar=no, location=no")
       }else if(template.type.value=='3'){
          var year=template.year.value
          window.open("structureprofit.jspviewhigh?subFunction=structureprofitchart&title="+title+"&col="+i+"&year="+year+"&type="+type+"&section="+section+"&message=TOP",null,"top=180, left=200, height=500,width=650,toolbar=no, location=no")
       }else
       {
          var year_month_from=template.year_month_from.value
          var year_month_to=template.year_month_to.value
          window.open("structureprofit.jspviewhigh?subFunction=structureprofitchart&title="+title+"&col="+i+"&year_month_from="+year_month_from+"&year_month_to="+year_month_to+"&type="+type+"&section="+section+"&message=TOP",null,"top=180, left=200, height=500,width=650,toolbar=no, location=no")
        }
     return true;
    }
   }

	function account() {
   	if(isEmpty1()) {
   		show_wait();
	    template.subFunction.value='account';
	    template.submit();
	    return true;
   }
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="structureprofit.jspviewhigh">
      <html:message/>
      <html:title clazz='module'>直接医疗收益指标（医成本I2-11表）</html:title>
      <html:table clazz="simple">
        <tr class="normalText" nowrap>
          <td  class="signText" nowrap>分析单位:</td>
           <td nowrap>
            <select name="type" onchange="choose()" class="selectBg">
               <option value='1' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("1")) out.print("selected");%> >月</option>
               <option value='2' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("2")) out.print("selected");%>>季</option>
               <option value='3' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("3")) out.print("selected");%>>年</option>
               <option value='4' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("4")) out.print("selected");%>>期间</option>
            </select>
           </td>
           <td  class="signText"nowrap>本期点：</td>
           <td id="mon" <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("1")) out.print(" style='display:block;' "); if(request.getParameter("type")!=null&&!request.getParameter("type").equals("1")) out.print(" style='display:none;' ");if(request.getParameter("type")==null) out.print(" style='display:block;' ");%>>
            <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
           <td id="ji"   <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("2")) out.print(" style='display:block;' "); if(request.getParameter("type")!=null&&!request.getParameter("type").equals("2")) out.print(" style='display:none;' "); if(request.getParameter("type")==null) out.print(" style='display:none;' ");%> nowrap>
            <%=new com.viewhigh.cbcs.base.mvc.view.component.QuarterComponent("year_quarter", request.getParameter("year_quarter"))%> </td>
            <%String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};%>
           <td id="nian"  <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("3")) out.print(" style='display:block;' "); if(request.getParameter("type")!=null&&!request.getParameter("type").equals("3")) out.print(" style='display:none;' "); if(request.getParameter("type")==null) out.print(" style='display:none;' ");%> nowrap>
            <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year",request.getParameter("year"),false,false)%>年</td>
           <td id="qijian"  <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("4")) out.print(" style='display:block;' "); if(request.getParameter("type")!=null&&!request.getParameter("type").equals("4")) out.print(" style='display:none;' "); if(request.getParameter("type")==null) out.print(" style='display:none;' ");%> nowrap>
            <%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%></td>
          <td colspan=40 align=center> <button class="pageBtn" onclick="account()">分析</button></td>

       </tr>
        <tr style='display:none;'>
            <td class='signText' nowrap>科室类型：</td>
            <td nowrap>
              <input type=radio name="section" value="5" <%if(request.getParameter("section")!=null&&request.getParameter("section").equals("5")) out.print(" checked");if(request.getParameter("section")==null) out.print(" checked");%>>直接医疗科室</radio>
              <input style="display:none" type=radio name="section" value="4" <%if(request.getParameter("section")!=null&&request.getParameter("section").equals("4")) out.print(" checked");%>/>
            </td>
            <td><button class="pageBtn" onclick="account()">分析</button></td>
           </tr>
        <tr>
          <td colspan=80><html:title clazz='table'>直接医疗收益指标</html:title></td>
          <td colspan=20 align=right>单位：元</td>
        </tr>
      </html:table>
      <vh:vhFixTable fixRow=1 fixCol=1>
      <table border='1' bgColor=white borderColor=black style='border:2px solid #000000;BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
        <colgroup id=tg>
        <col style = <%=DisplayWidth.NAME_WIDTH%> >

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        </colgroup>

		          <html:tr clazz='label'>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>科室</td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'><a href="#" onclick="message(1,'收益')">收益</a></td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'><a href="#" onclick="message(2,'收入')">收入</a></td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'><a href="#" onclick="message(3,'成本')">成本</a></td>
              </html:tr>
          <%
               if (result!=null) {
                  for (int i = 0; i < result.length; i++ )
                  {  String rowColor = "rowGray";
                      if (i/2*2==i) rowColor = "rowWhite";
              %>
              <tr CLASS="<%=rowColor%>">
                  <td nowrap class="normalText"><%=result[i][0]%></td>
                 <%for(int j=1;j<result[0].length-2;j++) {%>
                  <td class="numberText"><%=result[i][j]%></td>
               <%}%>
              </tr>
              <%
                    }
                  }


              %>
      </table>
    </vh:vhFixTable>
      <input type=hidden name="subFunction"/>

</form>
</html:html>



