<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/interchange/changemain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:58:11 $
  $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,
				java.text.SimpleDateFormat,
				java.util.Calendar" %>

<%@taglib uri="/WEB-INF/taglib/viewhigh-html.tld"prefix="html"%>

<Script Language="JavaScript">
  function trans() {
  	if (template.year_month.value=="") {
  		alert("请输入日期");
  		return false;
 	}

	if(template.sheet_type.value == "excel_to_e_idx_data"){
		var screenHeight=window.screen.availHeight/2;
		var screenWidth=window.screen.availWidth/5;  
		var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:100px;dialogLeft:110px;status:no;scroll:auto";
		window.open("cbcsAnalysechange.jspviewhigh?subFunction=fileselect&year_month="+template.year_month.value,"","width = "+screenHeight+", height="+screenWidth+", top=300, left=300, scrollbars = no, resizable = no");
		return false;
	}

	show_wait();
	template.action =
	  'cbcsAnalysechange.jspviewhigh?subFunction=import&year_month='+template.year_month.value+'&clean='+template.clean.value+'&force='+template.force.value+'&sheet_type='+ template.sheet_type.value;
	template.submit();
	return true;
  }

	function check() {
		if (template.year_month.value=="") {
  		alert("请输入日期");
  		return false;
 		}
		if(template.sheet_type.value == "zchfzh_i" || template.sheet_type.value == "shrzhch_i"){
			return false;
		}

		show_wait();
		template.action =
		  'cbcsAnalysechange.jspviewhigh?subFunction=check&year_month='+template.year_month.value+'&clean='+template.clean.value+'&force='+template.force.value+'&sheet_type='+ template.sheet_type.value;
		template.submit();
		return true;
	}

  function remove() {
	if(template.sheet_type.value == "zchfzh_i" || template.sheet_type.value == "shrzhch_i"){
			return false;
	}

    if(confirm('您确定要删除数据吗?')) {
      if(template.year_month.value == '') {
        alert('请选择年月!');
        return false;
      }
	  show_wait();
      template.action = 'cbcsAnalysechange.jspviewhigh?subFunction=delete&year_month=' + template.year_month.value+'&sheet_type='+ template.sheet_type.value;
      template.submit();
    } else {
      return false;
    }
  }

<%
	String sheet_type = (String)request.getAttribute("sheet_type");
	String clean      = (String)request.getAttribute("clean");
		if ( clean == null ) clean = "Y";
	String force      = (String)request.getAttribute("force");
		if ( force == null ) force = "N";
%>
</Script>
<html:html clazz="main">
  <form name="template" method="post" action="cbcsAnalysechange.jspviewhigh?subFunction=save">
    <!-- 返回信息栏 -->
    <html:message showReturn="false"/>
    <!-- 标题栏 -->
    <html:title clazz="module">数据导入页面</html:title>
    <!-- 简单信息 -->
    <html:table clazz="simple">
      <tr>
        <td nowrap class="normalText" >核算月：</td>
        <td nowrap class="normalText" align=right><%=new MonthComponent("year_month", request.getParameter("year_month") == null ? new SimpleDateFormat("yyyyMM").format(Calendar.getInstance().getTime()) : request.getParameter("year_month"))%></td>
        <td>报表：</td>
        <td nowrap class="normalText">
          <html:select property="data_interchange_analyse" name='sheet_type' value='<%=sheet_type%>' maxViewList='15'/>
        </td>
      </tr>
      <tr>
      	<td nowrap class="normalText" >清除本月数据：</td>
      	<td>
					<html:select property="yes_no"  name='clean' value='<%=clean%>' extent='92'/>
				</td>
      	<td nowrap class="normalText" >强制导入：</td>
      	<td>
					<html:select property="yes_no"  name='force' value='<%=force%>'/>

				</td>
				<td colspan="2"><button class="pageBtn" onclick="return check()" >检查</button>
			    	<button class="pageBtn" onclick="return trans()" >导入</button>
			    	<button class="pageBtn" onclick="return remove()">删除</button>   
			   <!-- <img src="images/transfers.gif" class="mouse" onclick="return trans()"/>
			    <img src="images/shanchu-1.gif" class="mouse" onclick="return remove()"/>-->
				</td>
			</tr>
  	</html:table>
		<br>
		<%
    BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	  TableMarge oper = new TableMarge(ro, "return check()");
		if (ro!=null) {
      String[][] result = ro.getTableResult();
      if (result!=null) {
    %>
    <table width="100%">
      <tr><td><%=oper%></td></tr>
      <tr>
        <td>
  	<html:table clazz="result">
	    <html:tr clazz='label'>
        <td class="resultLabel">序号</td>
        <td class="resultLabel">记录内容</td>
        <td class="resultLabel"> 错误说明</td>
      </html:tr>

      <%
      for (int i = 0; i < result.length; i++ ) {
       	String rowColor = "rowGray";
        if (i/2*2==i) rowColor = "rowWhite";
      %>

      <tr CLASS="<%=rowColor%>">
        <td class="normalText"><%=(i+ro.getCurrRow())%></td>
        <td class="normalText"><%=result[ i ][ 0 ]%></td>
        <td class="normalText"><%=result[ i ][ 1 ]%></td>
      </tr>
      <%}%>
		</html:table>
		    </td>
		  </tr>
		</table>

    <%}
    }%>
  </form>
</html:html>
