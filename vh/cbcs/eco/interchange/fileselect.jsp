<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/interchange/fileselect.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:58:11 $
  $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="javascript">
<%
String ym = request.getParameter("year_month");
String isExistDataInMap = (String)request.getSession().getAttribute("isExistDataInMap");

 if(ym == null){
 	ym = (String)request.getSession().getAttribute("excel_importdata_year_month");
 }
%>	 
function openFile(flag){
if(parseInt(flag)<=0){
		alert("映射表没有数据,请先设置映射关系!");		//cbcs_rpt_excel_map没有数据
		return false;
	}
	return true;
}
function confirmsubmit(){
	 var upload = document.getElementById("upload");
	if(confirm("确定提交文件吗?")){
		if(document.getElementById("filename").value == ''){
			alert("请选择excel文件!");
			return false;
		}
			document.getElementById("template").enctype="multipart/form-data"
			document.getElementById("template").action="cbcsAnalysechange.jspviewhigh?subFunction=exceldata"
   		document.getElementById("template").submit();
   		alert("操作成功!")
 	 }
 	 
}


</Script>
 <body onload="">
<html:html clazz="main">
	<html:title clazz="module">数据源</html:title>
	<form name="template" id="template" method="post" enctype="multipart/form-data" action="cbcsAnalysechange.jspviewhigh?subFunction=exceldata">
 	 <html:table clazz="simple" divWidth="100">
		<tr>
			<td>选择excel文件：
				<!--<input  class="normalText" colspan='3' name="submitfile"/>
				<img src="images/create.gif" style='cursor:hand' onclick="return filename.click()" >
				 <img src="images/complete.gif" style='cursor:hand' onclick="confirmsubmit()" >-->
				<input type="file" name="filename" id='filename'  value='' onclick="return openFile(<%=isExistDataInMap%>)" style="display:block"/>
				<input  name="upload" id="upload" type="submit" 	value="确认"/>
				<input type="hidden" name="year_month" id="year_month" value="<%=ym%>">
			</td>
		</tr>
  	</html:table>
	</form>
</html:html>
</body>