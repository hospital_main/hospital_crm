<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.*,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent,
								 com.viewhigh.cbcs.cbcs.util.SimpleSearch" %>
								 
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript">
   function Check() {
   	if ( confirm("校验操作可能覆盖上一次检验内容，是否继续？") ) {
	    template.subFunction.value='check';
	    show_wait();
	    template.submit();
	    //find();
    }
    return true;
  }
  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
  
  
</Script>
<%
	 String[][] CheckTypes = (String[][]) request.getAttribute("CheckTypes");
	 String CheckType = (String) request.getAttribute("CheckType");
%>
<html:html clazz="main" scrollCtl="yes">
	<form name="template" method="post" action="ecoCheckApportion.jspviewhigh">
		<html:message/>
	  <html:title clazz='module'>数据校验</html:title>
	  <html:table clazz="simple"> 
			<tr>	    
		    <td nowrap class="normalText">核算月：</td>
		    <td nowrap class="normalText"  ><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td> 
		    <td>校验类别：
          <%=new SingleSelect(
          CheckTypes, "CheckType", CheckType, false, false)
        	%>		    
        </td>
		    <td class="normalText">
	  	    <button class="pageBtn" onclick="return Check();" >校验</button>
	  	    <button class="pageBtn" onclick="return find();" >查询</button>
      	</td>
    	</tr>
  	</html:table>
	  <br>
	  <%
	    String[][] result = (String[][])request.getAttribute("result");
	    if (result!=null) {
	  %>
	  <html:table clazz="complex">
	    <tr>
		    <td>
		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td nowrap="nowrap" class="resultLabel" >检测项目</td>		          
		          <td nowrap="nowrap" class="resultLabel" >检测内容</td>
		          <td nowrap="nowrap" class="resultLabel" >检测结果</td>
  	        </html:tr>
		        <%
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }
				          String rowColor = "rowGray";
				          if (i/2*2==i) rowColor = "rowWhite";
		        %>
						<tr CLASS="<%=rowColor%>">
			      <%
		        	for(int j=0;j<3;j++){
		          	if ("&nbsp;".equals(result[i][1])){
		            out.println("<td nowrap=\"nowrap\" colspan=3>"+result[i][0]+"</td>");
		            break;
		          	}
  		        	if ("失败".equals(result[i][2])){
  		          	out.println("<td class='normalText' nowrap=\"nowrap\" style=\"color:#FF0000;\">"+result[i][j]+"</td>");
  		        	}else{
  		          out.println("<td class='normalText' nowrap=\"nowrap\" class=\""+rowColor+"\">"+result[i][j]+"</td>");
  		        	}
		        	}
		        %>
	        	</tr>
		        <%
	              }
	            }
		        %>
					</html:table>
		    </td>
		   </tr>
			</html:table>
		<%}%>
	  <input type='hidden' name="subFunction"/>
	  <input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	</form>
</html:html>
