<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/para/paraConfig.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:58:11 $
  $Modtime: 03-09-22 0:50 $
  $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../error.jsp"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="javaScript">
<%String[][] result=(String[][])request.getAttribute("result");%>
function save(){
  template.subFunction.value="save";
  template.submit();}
  function forward(){
    template.subFunction.value="show";
    show_wait();
    template.submit();
    }
</script>
<html:html clazz="main">
  <form name="template" method="POST" action="paraConfig.jspviewhigh">
    <html:title clazz="module">自动采集参数设置页面</html:title>
      <html:message/>
    <%if(result!=null){%>
    <html:table clazz="simple">
      <tr>
        <td align="center"><input type="checkbox" value="whole" name="wholtarget" <%if(result[0][0].equals("Y")){out.print(" checked=\"checked\" ");}%>/>全院基本指标
      </tr>
      <tr>
        <td align="center"><input type="checkbox" value="dept" name="depttarget" <%if(result[0][1].equals("Y")){out.print(" checked=\"checked\" ");}%>>科室基本指标
      </tr>
      <tr>
        <td align="center"><input type="checkbox" value="item" name="itemtarget" <%if(result[0][2].equals("Y")){out.print(" checked=\"checked\" ");}%>>项目基本指标
        </tr>
        <tr>
        <td align="center"><input type="checkbox" value="diss" name="disstarget" <%if(result[0][3].equals("Y")){out.print(" checked=\"checked\" ");}%>>病种基本指标
      </tr>
      <tr>
      </tr>
      <tr>
        <td colspan="2" align="center">
        <button class="pageBtn" onclick="return save();">保存</button>
        <button class="pageBtn" onclick="return reset();" >重置</button>  
        <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
         <img src="images/reset.gif" class="mouse" onclick="return reset();" />--></td>
      </tr>
    </html:table>
    <%}else{%>
    <html:table clazz="simple">
      <tr>
        <td align="center"><input type="checkbox" value="whole" name="wholtarget" <%if(request.getParameter("wholtarget")!=null&&request.getParameter("wholtarget").equals("whole")){out.print(" checked=\"checked\" ");}%>/>全院基本指标
      </tr>
      <tr>
        <td align="center"><input type="checkbox" value="dept" name="depttarget" <%if(request.getParameter("depttarget")!=null&&request.getParameter("depttarget").equals("dept")){out.print(" checked=\"checked\" ");}%>>科室基本指标
      </tr>
      <tr>
        <td align="center"><input type="checkbox" value="item" name="itemtarget" <%if(request.getParameter("itemtarget")!=null&&request.getParameter("itemtarget").equals("item")){out.print(" checked=\"checked\" ");}%>>项目基本指标
        </tr>
        <tr>
        <td align="center"><input type="checkbox" value="diss" name="disstarget" <%if(request.getParameter("disstarget")!=null&&request.getParameter("disstarget").equals("diss")){out.print(" checked=\"checked\" ");}%>>病种基本指标
      </tr>
      <tr>
      </tr>
      <tr>
        <td align="center"><button class="pageBtn" onclick="return save();">保存</button>
        <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return forward();" >返回</button> 
        <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
        <img src="images/reset.gif" class="mouse" onclick="return reset();" />
        <img src="images/return.gif" class="mouse" onclick="return forward();" />--> </td>
      </tr>
    </html:table>
    <%}%>
    <input type="hidden" name="subFunction">
  </form>
</html:html>

























