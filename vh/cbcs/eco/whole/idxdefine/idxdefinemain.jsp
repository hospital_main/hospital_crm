<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/whole/idxdefine/idxdefinemain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,
               com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function create() {
        template.subFunction.value='preparedCreate';
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }
  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }
  function find() {
    template.subFunction.value='find';
    template.submit();return true;
  }

  function idxevaluation(detail) {
    if(detail == "&nbsp;") {
     detail = "无指标评价";
    }
    window.showModalDialog("idxDefine.jspviewhigh?subFunction=detail&evaluation="+detail,window,"dialogWidth=820px;dialogHeight=620px");
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="idxDefine.jspviewhigh">
  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>指标定义</html:title>

  <!-- 查询信息 -->
    <%
      String[][] result=null;
      BaseRO  ro= (BaseRO)request.getAttribute("ro");
      if(ro!=null)
       {  result=ro.getTableResult();
       }
      String[][] index_name=(String[][])request.getAttribute("index_name");
      TableMarge oper = new TableMarge(ro, "return find()");
    %>
	  <html:table clazz="simple">
    <tr>

      <td nowrap class="signText">指标名称：</td>
      <td nowrap class="signText">
       <%=new SingleSelect(request.getAttribute("index_name"), "select_idx_name", request.getParameter("select_idx_name"), false, false)%>
      </td>
     <td nowrap class="signText"><input type="text" name="input_idx_name" size="20" value='<%=request.getParameter("input_idx_name")==null?"":request.getParameter("input_idx_name")%>' /></td>
      <td nowrap class="signText">指标评价：</td>
      <td nowrap class="signText">
     <textarea name="evaluation" ><%=request.getParameter("evaluation")==null?"":request.getParameter("evaluation")%></textarea>
     </td>
      <td>
      <button class="pageBtn" name="" onclick="return find();" >查询</button>
      <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
</tr>

	  </html:table>
          <html:title clazz='table'>指标定义</html:title>
	  <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
          <td class="resultLabel">指标名称</td>
          <td class="resultLabel">指标评价</td>

	        </html:tr>

        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {

                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j].equals("")) result[i][j]="&nbsp;";
                }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText"><a href="#" onclick="idxevaluation('<%=result[i][2].trim()%>')"><%=result[i][0]%></a></td>
          <td class="normalText"><%=result[ i ][ 2 ]%></td>
        </tr>

        <%
              }
            }
        %>
	      </html:table>
    </td>
  </tr>

	      </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>
