<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/whole/basic/BasicIndexComp.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<script language='javascript' src='javascript/chart.js'></script>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.*,
  com.viewhigh.cbcs.base.mvc.view.component.*,
  com.viewhigh.cbcs.cbcs.util.*, java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DialogOrWin" %>


<%
  String[][] years = {
    {"2002", "2002"}, {"2003", "2003"}, {"2004", "2004"},
    {"2005", "2005"}, {"2006", "2006"}, {"2007", "2007"},
    {"2008", "2008"}, {"2009", "2009"}, {"2010", "2010"},
    {"2011", "2011"}, {"2012", "2012"},
  };

  String[][] choices = {
    {"month", "月"}, {"quarter", "季"},
    {"year", "年"}, {"course", "期间"},
  };
  String[][] result = (String[][])request.getAttribute("table_result");
  String compareUnit = request.getParameter("compareUnit");
  Format percentFormat = Format.getPercentFormat();
  Format moneyFormat =
    Format.getPrecisionFormat(Integer.parseInt(request.getParameter("prec")));
%>


<Script language="javascript">

  function chooseCompareUnit() {

    month1.style.display = 'none';
    month2.style.display = 'none';
    year1.style.display = "none";
    year2.style.display = "none";
    quarter1.style.display = "none";
    quarter2.style.display = "none";
    course1.style.display = "none";
    course2.style.display = "none";

    if (template.compareUnit.value == 'month') {
      month1.style.display = 'block';
      month2.style.display = 'block';
    } else if (template.compareUnit.value == 'year') {
      year1.style.display = "block";
      year2.style.display = "block";
    } else if (template.compareUnit.value == 'quarter') {
      quarter1.style.display = "block";
      quarter2.style.display = "block";
    } else if (template.compareUnit.value == 'course') {
      course1.style.display = "block";
      course2.style.display = "block";
    }
  }

  function analyze() {

    if (template.compareUnit.value == 'month'
      && (template.monthStart.value == '' || template.monthStop.value == '')) {

      alert('请选择时间');
      return false;
    }

    if (template.compareUnit.value == 'quarter'
      &&
      (template.quarterStart.value == '' || template.quarterStop.value == '')) {
      alert('请选择时间');
      return false;
    }

    if (template.compareUnit.value == 'course'
      &&
      (template.courseStart1.value == '' || template.courseStart2.value == ''
        || template.courseStop1.value == '' || template.courseStop2.value == '')
    ) {
      alert('请选择时间');
      return false;
    }
    if(template.compareUnit.value=='month' && template.monthStart.value<=template.monthStop.value){
      alert('报告期应大于基期!')
      return false;
    }
    if(template.compareUnit.value=='quarter' && template.quarterStart.value<=template.quarterStop.value){
      alert('报告期应大于基期!')
      return false;
    }
    if(template.compareUnit.value=='year' && template.yearStart.value<=template.yearStop.value){
      alert('报告期应大于基期!')
      return false;
    }
    if(template.compareUnit.value=='course' && template.courseStart1.value>=template.courseStart2.value){
      alert('报告期开始时间应小于结束时间!')
      return false;
    }
    if(template.compareUnit.value=='course' && template.courseStop1.value>=template.courseStop2.value){
      alert('基期开始时间应小于结束时间!')
      return false;
    }

    if(template.compareUnit.value=='course' && template.courseStart2.value<=template.courseStop2.value){
      alert('报告期结束时间应大于基期结束时间!')
      return false;
    }
		show_wait();
    template.submit();
    return true;
  }

  var cewolf1;
  <% if (result!=null) { %>
  cewolf1 = "";
  <% }%>
</Script>

<html:html clazz="child" leftMargin="0" topMargin="0">
<form name="template" method="post" action="cbcsEcoBasicIndexesAnalysis.jspviewhigh" />
	<!-- 返回信息栏 -->
 	<html:message/>
  <table width="100%">
    <tr>
      <td align="left">
        <table width="100%">
         	<tr>
           	<td class="signText" nowrap="nowrap">分析单位：</td>
            <td class="signText" nowrap="nowrap">
			        <select name="compareUnit" onchange="chooseCompareUnit()" class="selectBg" >
			          <option value='month' class="normalText"
			          <%if (compareUnit == null || compareUnit.equals("month")) {%>
			            selected
			          <%}%>
			          >
			            月
			          </option>
			          <option value='quarter' class="normalText"
			          <%if (compareUnit != null && compareUnit.equals("quarter")) {%>
			          selected
			          <%}%>
			          >
			            季
			          </option>
			          <option value='year' class="normalText"
			          <%if (compareUnit != null && compareUnit.equals("year")) {%>
			            selected
			          <%}%>
			          >
			            年
			          </option>
			          <option value='course' class="normalText"
			          <%if (compareUnit != null && compareUnit.equals("course")) {%>
			          selected
			          <%}%>
			          >
			            期间
			          </option>
			        </select>
			      </td>
						<td colspan="2" align="center">
							<button class="pageBtn" onclick="return analyze();">分析</button>
							<button class="pageBtn" onclick="return saveChart()">保存</button>
						</td>
					</tr>
					<tr>
            <td class="signText" nowrap="nowrap">报告期：</td>
           	<td class="signText" nowrap="nowrap" id="month1"
			        <%if (compareUnit == null || compareUnit.equals("month")) {%>
			          style="display:block"
			        <%} else {%>
			          style="display:none"
			        <%}%>>
			        <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("monthStart", request.getParameter("monthStart"))%>
			      </td>
			      <td class="signText" nowrap="nowrap" id="quarter1"
			        <%if (compareUnit != null && compareUnit.equals("quarter")) {%>
			          style="display:block"
			        <%} else {%>
			        style="display:none"
			        <%}%>>
			        <%=new QuarterComponent("quarterStart",
			          request.getParameter("quarterStart"))%>
			      </td>
			      <td class="signText" nowrap="nowrap" id="year1"
			        <%if (compareUnit != null && compareUnit.equals("year")) {%>
			          style="display:block"
			        <%} else {%>
			        style="display:none"
			        <%}%>
			        >
			        <%=new SingleSelect(years, "yearStart",
			          request.getParameter("yearStart"), false, true)%>年
			      </td>
			      <td class="signText" nowrap="nowrap" id="course1"
			        <%if (compareUnit != null && compareUnit.equals("course")) {%>
			          style="display:block"
			        <%} else {%>
			        style="display:none"
			        <%}%>
			        >
			        <%=new BiMonthComponent("courseStart1",
			          request.getParameter("courseStart1"),
			          "courseStart2",
			          request.getParameter("courseStart2"))%>
			      </td>
			    </tr>
					<tr>
            <td class="signText" nowrap="nowrap">基期：</td>
            <td class="signText" nowrap="nowrap" id="month2"
			        <%if (compareUnit == null || compareUnit.equals("month")) {%>
			          style="display:block"
			        <%} else {%>
			          style="display:none"
			        <%}%>>
			        <%=new MonthComponent("monthStop",
			          request.getParameter("monthStop"))%>
			      </td>
			      <td class="signText" nowrap="nowrap" id="quarter2"
			        <%if (compareUnit != null && compareUnit.equals("quarter")) {%>
			          style="display:block"
			        <%} else {%>
			        style="display:none"
			        <%}%>>
			        <%=new QuarterComponent("quarterStop",
			          request.getParameter("quarterStop"))%>
			      </td>
			      <td class="signText" nowrap="nowrap" id="year2"
			        <%if (compareUnit != null && compareUnit.equals("year")) {%>
			          style="display:block"
			        <%} else {%>
			        style="display:none"
			        <%}%>>
			        <%=new SingleSelect(years, "yearStop",
			          request.getParameter("yearStop"), false, true)%>年
			      </td>
			      <td class="signText" nowrap="nowrap" id="course2"
			        <%if (compareUnit != null && compareUnit.equals("course")) {%>
			          style="display:block"
			        <%} else {%>
			        style="display:none"
			        <%}%>>
			        <%=new BiMonthComponent("courseStop1",
			          request.getParameter("courseStop1"),
			          "courseStop2",
			          request.getParameter("courseStop2"))%>
      			</td>
    			</tr>
	  		</table>
			</td>
		</tr>
		<tr>
      <td align="left">
        <table >
          <tr>
            <td >
<%
if (result != null) {
	ChartUtilities.createChartBar("cewolf1", new String[][]{{"", result[0][0], 
																														result[0][1], result[0][4]}},new int[]{0, 1, 2, 3}, 
																														pageContext);
	String chartTitle = request.getParameter("idx_name");
	if (chartTitle==null) chartTitle = "";
%>
	<cewolf:chart id="chart" title="<%=chartTitle%>" type="horizontalBar3D">
    <cewolf:colorpaint color="#EEEEFF"/>
    <cewolf:data>
        <cewolf:producer id="cewolf1"/>
    </cewolf:data>
	</cewolf:chart>
	<cewolf:img chartid="chart" renderer="/cewolf" width="600" height="240" id="cewolf1" >
		<cewolf:map tooltipgeneratorid="cewolf1Tip"/>
	</cewolf:img>
<% } else { %>
	<div style='overflow:auto;width:400px; height:250px;' />
<% } %>
            </td>
          </tr>
        </table>
      </td>
    </tr>
	</table>
	<hr>

  <%
    if (result != null) {
      for (int i = 0; i < result[0].length; i++) {

        if (i != 0 && ((i % 3) == 0)) {
          result[0][i] =
            percentFormat.format(Double.parseDouble(result[0][i]));
          continue;
        }
        result[0][i] =
          moneyFormat.format(Double.parseDouble(result[0][i]));
      }
  %>
	      <html:table clazz="result" divWidth="600" divHeight="70">
	        <html:tr clazz='label'>
			      <td class="resultLabel" rowspan=2>指标</td>
			      <td class="resultLabel" rowspan=2>单位</td>
			      <td class="resultLabel" rowspan=2>报告期值</td>
			      <td class="resultLabel" colspan="3">与基期比较</td>
			      <td class="resultLabel" colspan="3">与预算比较</td>
	        </html:tr>

	        <html:tr clazz='label'>
			      <td class="resultLabel">基期值</td>
			      <td class="resultLabel">差异</td>
			      <td class="resultLabel">差异率</td>
			      <td class="resultLabel">预算值</td>
			      <td class="resultLabel">差异</td>
			      <td class="resultLabel">差异率</td>
	        </html:tr>

			    <tr CLASS="rowWhite">
			    	<td class="normalText" style="text-align:center" id="idx" style="cursor:help">			    		
			    	</td>
			      <td class="normalText" style="text-align:center"><%=request.getParameter("unitName")%></td>
			      <td class="numberText"><%=result[0][0]%></td>
			      <td class="numberText"><%=result[0][1]%></td>
			      <td class="numberText"><%=result[0][2]%></td>
			      <td class="numberText"><%=result[0][3]%></td>
			      <td class="numberText"><%=result[0][4]%></td>
			      <td class="numberText"><%=result[0][5]%></td>
			      <td class="numberText"><%=result[0][6]%></td>
			    </tr>

	      </html:table>
  <%
    }
  %>

  <input type="hidden" name="subFunction" value="compAnalysis">
  <input type="hidden" name="basicIndexCode" value="<%=request.getParameter("basicIndexCode")%>">
  <input type="hidden" name="isSumMonth" value="<%=request.getParameter("isSumMonth")==null?"Y":"Y"%>">
  <input type="hidden" name="unitName" value="<%=request.getParameter("unitName")%>">
  <input type="hidden" name="prec" value="<%=request.getParameter("prec")%>">
  
  <input type="hidden" name="idx_name" value="<%=request.getParameter("idx_name")%>"/>
</form>

</html:html>

<script language="javascript">
	if (document.getElementById("idx")!=null) {
		document.getElementById("idx").innerText = parent.test.tip_idx_name.value;
		str = "指标代码:"+parent.test.tip_idx_code.value+"\n指标名称:"+parent.test.tip_idx_name.value+"\n指标定义及评价:"+parent.test.tip_evaluation.value;
		document.getElementById("idx").title = str;
	}
</script>