<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/whole/basic/BasicIndexTrend.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<script language='javascript' src='javascript/chart.js'></script>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.*,
  com.viewhigh.cbcs.base.mvc.view.component.*,
  com.viewhigh.cbcs.cbcs.util.*, java.util.*" %>

<%
  String[][] result = (String[][])request.getAttribute("table_result");
  Format percentFormat = Format.getPercentFormat();
  Format moneyFormat =
  Format.getPrecisionFormat(Integer.parseInt(request.getParameter("prec")));
%>

<Script language="javascript">
  function analyze() {
    if (template.yearMonthStart.value == ''
      || template.yearMonthStop.value == '') {
      alert('请选择时间');
      return false;
    }

		show_wait();
    template.submit();
    return true;
  }

  var cewolf1;
  <% if (result!=null) { %>
  cewolf1 = "";
  <% }%>

</Script>
<html:html clazz="child" leftMargin="0">
<form name="template" method="post" action="cbcsEcoBasicIndexesAnalysis.jspviewhigh" />
  <!-- 返回信息栏 -->
  <html:message/>
  <table width="100%">
  	<tr>
    	<td >
  			<html:table clazz="complex">
        	<tr>
          	<td class="signText" nowrap="nowrap">起止年月：</td>
          	<td class="signText" nowrap="nowrap">
			        <%=new BiMonthComponent("yearMonthStart",
			          request.getParameter("yearMonthStart"),
			          "yearMonthStop",
			          request.getParameter("yearMonthStop"))%>
			      </td>
          	<td class="signText" nowrap="nowrap" align="center" colspan="2">
          	<button class="pageBtn" onclick="return analyze();">分析</button>
		<button class="pageBtn" onclick="return saveChart()">保存</button>
						</td>
        	</tr>
				</html:table>
			</td>
		</tr>
		<tr>
			<td>
  			<table >
					<tr>
						<td align="center">
<%
if (result != null) {
	String[] temp = null;
	ArrayList list = new ArrayList();
	for (int i=0; i<result.length; i++) {
		if (result[i][0].length()==6) {
			temp = new String[3];
			temp[0]=result[i][0].substring(0, 4);
			temp[1]=result[i][0].substring(4);
			temp[2]=result[i][1];
			list.add(temp);
		}
	}
	Object[] objs = list.toArray();
	String[][] chartData = new String[objs.length][3];
	for (int i=0; i<chartData.length; i++) {
		chartData[i] = (String[])objs[i];
	}
	ChartUtilities.createChartLine("cewolf1", new String[]{""}, chartData, new int[]{0, 1, 2}, pageContext);
	String chartTitle = request.getParameter("idx_name");
	if (chartTitle==null) chartTitle = "";
%>
	<cewolf:chart id="chart" title="<%=chartTitle%>" type="line">
    <cewolf:colorpaint color="#EEEEFF"/>
    <cewolf:data>
        <cewolf:producer id="cewolf1"/>
    </cewolf:data>
	</cewolf:chart>
	<cewolf:img chartid="chart" renderer="/cewolf" width="590" height="250" id="cewolf1" />

<% } else { %>
	<div style='overflow:auto;width:400px; height:250px;' />
<% } %>
						</td>
					</tr>
  			</table>
			</td>
		</tr>
	</table>
	<hr>

  <%
    if (result != null) {
  %>
	<span class="normalText">单位:<%=request.getParameter("unitName")%></span>
  <html:table clazz="result" divWidth="590">
    <html:tr clazz='label'>
      <td class="resultLabel">年</td>
      <td class="resultLabel">月</td>
      <td class="resultLabel">基本值</td>
    </html:tr>

<%
  for (int i = 0; i < result.length; i++) {

  if (Integer.parseInt(result[i][2]) != -1) {
%>
    <tr>
      <td width="33%" rowspan="<%=Integer.parseInt(result[i][2])%>"><%=result[i][0]%></td>
      <td width="33%"><%=result[i + 1][0].substring(4)%></td>
      <td width="33%" class="numberText"><%=moneyFormat.format(Double.parseDouble(result[i + 1][1]))%></td>
    </tr>
<%
    i++;
    continue;
  }
%>
    <tr>
      <td width="33%"><%=result[i][0].substring(4)%></td>
      <td width="33%" class="numberText"><%=moneyFormat.format(Double.parseDouble(result[i][1]))%></td>
    </tr>
<%

  }
%>
  </html:table>
<%
  }
%>

  <input type="hidden" name="subFunction" value="trendAnalysis">
  <input type="hidden" name="basicIndexCode" value="<%=request.getParameter("basicIndexCode")%>">
  <input type="hidden" name="isSumMonth" value="<%=request.getParameter("isSumMonth")%>">
  <input type="hidden" name="unitName" value="<%=request.getParameter("unitName")%>">
  <input type="hidden" name="prec" value="<%=request.getParameter("prec")%>">

  <input type="hidden" name="idx_name" value="<%=request.getParameter("idx_name")%>"/>
</form>

</html:html>
