<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/whole/basic/BasicIndexMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
   String[] tip_idx_code=(String[])request.getAttribute("tip_idx_code");
   String[] tip_idx_name=(String[])request.getAttribute("tip_idx_name");
   String[] tip_evaluation=(String[])request.getAttribute("tip_evaluation");
%>

<script language="javascript">
  function chooseCompMethod(compMethod) {
  	wait();
  	if (compMethod == 'comp') {
			document.test.action = 'cbcsEcoBasicIndexesAnalysis.jspviewhigh?subFunction=compAnalysisMain&basicIndexCode=<%=request.getParameter("basicIndexCode")%>&unitName=<%=request.getParameter("unitName")%>&prec=<%=request.getParameter("prec")%>&idx_name=<%=tip_idx_name[0]%>';
    } else if (compMethod == 'trend') {
      document.test.action = 'cbcsEcoBasicIndexesAnalysis.jspviewhigh?subFunction=trendAnalysisMain&basicIndexCode=<%=request.getParameter("basicIndexCode")%>&unitName=<%=request.getParameter("unitName")%>&prec=<%=request.getParameter("prec")%>&idx_name=<%=tip_idx_name[0]%>';
    }
    document.test.target = 'analysis';
    document.test.submit();
  }

  var _1st_current=1;
  function wait() {
		document.getElementById("_wait").style.display='';
		document.getElementById("_interval").style.display='none';
	}

	function show() {
		document.getElementById("_wait").style.display='none';
		document.getElementById("_interval").style.display='';
	}

</script>

<html:html clazz="child" leftMargin="0" topMargin="0">
	<!--信息栏-->
	<html:message/>
	<html:title clazz='module'>
    <html:tip code='<%=tip_idx_code[0]%>' name='<%=tip_idx_name[0]%>' message='<%=tip_evaluation[0]%>'>
    	<%=request.getParameter("basicIndexName")%>
    </html:tip>
  </html:title>

	<table width="100%">
	  <tr>
	    <td width="33%" class="signText">分析方法：</td>
	    <td width="33%" class="signText"><input type="radio" name="compMethods" onclick="chooseCompMethod('comp')" checked>比较分析</td>
	    <td width="33%" class="signText"><input type="radio" name="compMethods" onclick="chooseCompMethod('trend')">趋势分析</td>
	  </tr>
	  <tr>
	    <td colspan=20><hr></td>
	  </tr>
	  <tr>
	 		<td id='_wait' align='center' width='50'><img id='_image' src='img/loading.gif' onclick="show()" style="cursor:hand"></td>
			<td id='_interval' width=1023 colspan="20" style="display:none">
	      <iframe VSPACE="1" HSPACE="1" FRAMEBORDER="0" WIDTH="100%" SRC="cbcsEcoBasicIndexesAnalysis.jspviewhigh?subFunction=compAnalysisMain&basicIndexCode=<%=request.getParameter("basicIndexCode")%>&unitName=<%=request.getParameter("unitName")%>&prec=<%=request.getParameter("prec")%>&idx_name=<%=tip_idx_name[0]%>" NAME="analysis" HEIGHT="430" ></iframe>
	    </td>
	  </tr>
	</table>
<form name="test" method="post">
	<input type=hidden name="tip_idx_code" value="<%=tip_idx_code[0]%>" />
	<input type=hidden name="tip_idx_name" value="<%=tip_idx_name[0]%>" />
	<input type=hidden name="tip_evaluation" value="<%=tip_evaluation[0]%>" />
</form>
</html:html>
