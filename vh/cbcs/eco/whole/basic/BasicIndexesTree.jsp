<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/whole/basic/BasicIndexesTree.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script src="javascript/tree.js"></script>

<%
String[][] result = (String[][])request.getAttribute("table_result");
%>
<html:html clazz="child" scrollCtl="true" leftMargin="0">
<input type="hidden" name="selectednode">
<!-- 导入显示树所必须的js文件 -->

<Script language="javascript">

  foldersTree = gFld("<font class='normalText'>基本指标</font>");
    aux1 = insFld(foldersTree, gFld("<font class='normalText'>人</font>"));
    <%
      if (result != null) {
        for (int i = 0; i < result.length; i++) {
          if (result[i][0].trim().startsWith("BH")){
    %>
            insDoc(aux1,
              gLnk(2, 0,
                "<font class='normalText'><%=result[i][1]%></font>", "cbcsEcoBasicIndexesAnalysis.jspviewhigh?subFunction=toMain&basicIndexCode=<%=result[i][0]%>&basicIndexName=<%=result[i][1]%>&isSumMonth=<%=result[i][4]%>&unitName=<%=result[i][5]%>&prec=<%=result[i][6]%>"));

    <%
          }
        }
      }
    %>
    aux1 = insFld(foldersTree, gFld("<font class='normalText'>财</font>"));
    <%
      if (result != null) {
        for (int i = 0; i < result.length; i++) {
          if (result[i][0].trim().startsWith("BF")){
    %>
            insDoc(aux1,
              gLnk(2, 0,
                "<font class='normalText'><%=result[i][1]%></font>", "cbcsEcoBasicIndexesAnalysis.jspviewhigh?subFunction=toMain&basicIndexCode=<%=result[i][0]%>&basicIndexName=<%=result[i][1]%>&isSumMonth=<%=result[i][4]%>&unitName=<%=result[i][5]%>&prec=<%=result[i][6]%>"));

    <%
          }
        }
      }
    %>
    aux1 = insFld(foldersTree, gFld("<font class='normalText'>物</font>"));
    <%
      if (result != null) {
        for (int i = 0; i < result.length; i++) {
          if (result[i][0].trim().startsWith("BM")){
    %>
            insDoc(aux1,
              gLnk(2, 0,
                "<font class='normalText'><%=result[i][1]%></font>", "cbcsEcoBasicIndexesAnalysis.jspviewhigh?subFunction=toMain&basicIndexCode=<%=result[i][0]%>&basicIndexName=<%=result[i][1]%>&isSumMonth=<%=result[i][4]%>&unitName=<%=result[i][5]%>&prec=<%=result[i][6]%>"));

    <%
          }
        }
      }
    %>
  initializeDocument();
</Script>
</html:html>
