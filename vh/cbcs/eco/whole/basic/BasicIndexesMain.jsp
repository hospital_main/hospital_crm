<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/whole/basic/BasicIndexesMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import = "com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.base.mvc.view.margin.Header,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<html:html clazz="main">

	<!-- 信息提示栏 -->
  <html:message/>

	<!--信息栏-->
  <html:title clazz='module'>基本指标分析</html:title>

	<!-- 复杂信息 -->
  <table>
  	<tr>
    	<td>
	  		<table>
	        <tr>
	          <td>
	            <iframe  VSPACE="1" HSPACE="1" FRAMEBORDER="0" WIDTH="180" SRC="cbcsEcoBasicIndexesAnalysis.jspviewhigh?subFunction=getAllBasicIndexes" NAME="basicIndexesTree" HEIGHT="518" ></iframe>
	          </td>
	        </tr>
	  		</table>
	    </td>
	    <td >
		  	<table>
	        <tr>
	          <td>
	            <iframe  VSPACE="1" HSPACE="1" FRAMEBORDER="0" WIDTH="645" SRC="" NAME="Iframe_table" HEIGHT="518" ></iframe>
	          </td>
	        </tr>
		  	</table>
	    </td>
 		</tr>
	</table>
</html:html>
