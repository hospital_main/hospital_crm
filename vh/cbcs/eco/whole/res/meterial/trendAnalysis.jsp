<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/whole/res/meterial/trendAnalysis.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<script language='javascript' src='javascript/tag.js'></script>
<script language='javascript' src='javascript/chart.js'></script>
<%@ page import="java.text.*, com.viewhigh.cbcs.base.chart.*" %>

<%
   DecimalFormat nf;
   String[][] result = (String[][])request.getAttribute("result");
   String[][] ind = (String[][])request.getAttribute("ind");
   String[][] init_index = (String[][])request.getAttribute("init_index");
   String[] tip_idx_code=(String[])request.getAttribute("tip_idx_code");
   String[] tip_idx_name=(String[])request.getAttribute("tip_idx_name");
   String[] tip_evaluation=(String[])request.getAttribute("tip_evaluation");
   String selected = "";
%>
<Script Language="JavaScript">
  function analysis(){
    if(template.year_month_from.value=='' || template.year_month_to.value=='' || template.year_month_from.value>template.year_month_to.value){
      alert('请选择起止年月')
      return
    }
    else if(template.index_trend.value==''){
      alert('请选择指标集')
      return
    }

    if(template.index_trend.length>8){
      alert('最多只能选择八个指标')
      return
    }
    show_wait();
    template.submit()
  }


  var cewolf1;
  <% if (result!=null) { %>
  cewolf1 = "";
  <% }%>
</Script>
<html:html clazz="child" leftMargin="0">
<form name="template" method="post" action="ecoWholeResMeterial.jspviewhigh">
  <!-- 信息提示栏 -->
	  <html:message/>
	  <table >
      <tr>
        <td>
	        <table >
            <tr>
              <td align="center">
	<%
  if (result != null && ind != null) {
    String[] series = new String[ind.length];
    for (int i=0; i<series.length; i++) {
    	series[i] = ind[i][1];
    }
  	ChartUtilities.createChartLine("cewolf1", series, result, new int[]{0, 1, 2}, pageContext);
	%>
		<cewolf:chart id="chart" title="物力资源指标" type="line">
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="200" height="250" id="cewolf1" ondblclick="return adjustShape()"/>

		<cewolf:chart id="chartBig" title="物力资源指标" type="line" >
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chartBig" renderer="/cewolf" width="360" height="250" id="cewolf1Big" ondblclick="return adjustShape()" style="display:none"/>
	<% } else { %>
		<div style='overflow:auto;width:200px; height:250px;' />
	<% } %>
              </td>
           </tr>
	        </table>
        </td>
        <td >
	        <table >
          <tr>
            <td class="signText" nowrap="nowrap">起止年月：</td>
            <td align="left" nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%></td>
          </tr>
          <tr>
            <td class="signText" nowrap="nowrap">指&nbsp;标&nbsp;集：</td>
          </tr>
          <tr>
<%String[][] index = (String[][])request.getAttribute("index");%>
           <td></td>
            <td align="left" nowrap="nowrap">
        <html:select property="eco_whole_res_meterial"  name='index_trend' biSelect="true" showTip="true"/>
            </td>
           </tr>
           <tr>
						<td align="center" colspan="2">
            	<button class="pageBtn" onclick="return analysis();">分析</button>
							<button class="pageBtn" accessKey="B" id="cewolf1Zoom" onclick="return adjustShape();">放大</button>
							<button class="pageBtn" onclick="return saveChart()">保存</button>
						</td>
					</tr>
	       </table >
      </td>
    </tr>
  </table>

<%
    if(result!=null){
   String[] temp = new String[ind.length];
%>
	      <html:table clazz="result" divWidth="780">
	        <html:tr clazz='label'>
            <td class="resultLabel" >年</td>
            <td class="resultLabel" >月</td>
<%
for(int i=0; i<ind.length; i++){
         temp[i] = ind[i][0];
%>
            <td class="resultLabel" >
            <html:tip name='<%=tip_idx_name[i]%>' code='<%=tip_idx_code[i]%>' message='<%=tip_evaluation[i]%>'>
              <%=ind[i][1]%>(<%=ind[i][3]%>)
            </html:tip>
            </td>
<%}%>
	        </html:tr>
<%
       for(int j=0; j<result.length; j++){
         String rowColor = "rowGray";
         if (j/2*2==j) rowColor = "rowWhite";
%>
          <tr CLASS="<%=rowColor%>">
<%
if(j==0 || !result[j][0].equals(result[j-1][0])){
%>
            <td align="center" rowspan="<%=result[j][ind.length+2]%>"><%=result[j][0]%></td>
<%}%>
            <td class="normalText"><%=result[j][1]%></td>
<%
  for(int k=0; k<ind.length; k++){
    if(result[j][k+2]==null)
      result[j][k+2] = "0";
    if(Integer.parseInt(ind[k][4])==0){
      nf = new DecimalFormat("#,##0");
    }else{
      String zero = new String("0.");
      for(int t=0; t<Integer.parseInt(ind[k][4]); t++){
        zero += "0";
    }
    nf = new DecimalFormat("#,##"+zero);
    }
%>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[j][k+2]))%></td>
<%}%>
          </tr>
<%}%>
	      </html:table>

<%
       selected = ExtendTool.arrayToString(temp);
    }
%>
  <input type="hidden" name="selected" value="<%=selected%>">
  <input type="hidden" name="subFunction" value="trend">
</form>

</html:html>
