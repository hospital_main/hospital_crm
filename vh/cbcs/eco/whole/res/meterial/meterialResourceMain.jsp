<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/whole/res/meterial/meterialResourceMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript">
  function compare(){
  	wait();
    document.tt.action="ecoWholeResMeterial.jspviewhigh?subFunction=compare";
    document.tt.target="methods";
    document.tt.submit();
  }
  function trend(){
  	wait();
    document.tt.action="ecoWholeResMeterial.jspviewhigh?subFunction=trend";
    document.tt.target="methods";
    document.tt.submit();
  }

  var _1st_current=1;
  function wait() {
		document.getElementById("_wait").style.display='';
		document.getElementById("_interval").style.display='none';
	}

	function show() {
		document.getElementById("_wait").style.display='none';
		document.getElementById("_interval").style.display='';
	}
</Script>

<html:html clazz="main">
  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>物力资源指标(医成本I1-03表)</html:title>

  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td width="33%" class="signText" nowrap="nowrap">分析方法：</td>
      <td width="33%" class="signText" nowrap="nowrap"><input type="radio" name="methods" value="0" onclick="compare()" checked>比较分析</td>
      <td width="33%" class="signText" nowrap="nowrap"><input type="radio" name="methods" value="2" onclick="trend()">趋势分析</td>
    </tr>
    <tr>
      <td colspan=20><hr></td>
    </tr>
    <tr>
			<td id='_wait' align='center' width='50'><img id='_image' src='img/loading.gif'/ onclick="show()" style="cursor:hand"></td>
			<td id='_interval' width=1023 colspan="20" style="display:none">
        <iframe  VSPACE="1" HSPACE="1" FRAMEBORDER="0" WIDTH="100%" SRC="ecoWholeResMeterial.jspviewhigh?subFunction=compare" NAME="methods" HEIGHT="450" ></iframe>
      </td>
      <form name="tt" method="post"></form>
    </tr>
	  </html:table>
  <input type="hidden" name="subFunction" >

</html:html>
