<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/input/inherit/dept/dept.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language='javascript' src='javascript/tag.js'></script>
<Script language="javascript">
  function inherit() {
    if (template.year_Month1.value == '') {
      alert('请选择数据源时间!');
      return false;
    }
    if (template.year_Month2.value == '') {
      alert('请选择目标时间!');
      return false;
    }
    if(template.parameters[0].checked==false && template.parameters[1].checked==false){
      alert('请选择继承项目!');
      return false;
    }
    if(template.index.value==''){
      alert('请选择指标!');
      return false;
    }
    show_wait();
	  template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="ecoInputInheritDept.jspviewhigh">

  <!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>科室指标数据继承</html:title>

   <table width='50%' cellspacing='6' border='0' align="center" class='normalText'>
    <tr>
      <td class="normalText">
        数据源时间：
      </td>
      <td class="normalText">
        <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_Month1", request.getParameter("year_Month1"))%>
      </td>
      <td class="normalText">
        目标时间：
      </td>
      <td class="normalText">
        <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_Month2", request.getParameter("year_Month2"))%>
      </td>
    </tr>
    <%String[] check = request.getParameterValues("parameters");%>
    <tr>
      <td class="normalText">
        继承项目：
      </td>
      <td class="normalText">
        <input type="checkbox" name="parameters" value="0" <%if(check!=null && check[0].equals("0")) out.print("checked");%>>本期值&nbsp;&nbsp;
        <input type="checkbox" name="parameters" value="1" <%if(check!=null && (check[0].equals("1") || check.length==2)) out.print("checked");%>>预测值
      </td>
    </tr>
    <tr>
      <td class="normalText">
        科室基本指标：
      </td>
    </tr>
    <tr>
      <td colspan="2" class="normalText" nowrap="nowrap">
        <html:select property="eco_input_inherit_dept"  name='index' biSelect="true" showTip="true"/>
      </td>
      <td class="normalText"><button class="pageBtn" onclick="return inherit();" >继承</button>
      </td>
    </tr>
  </table>
  <input type='hidden' name="subFunction" value='inherit'>
</form>
</html:html>
