
<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/input/direct/whole/WBIDirectInputSubMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.text.*" %>
<%
  DecimalFormat nf ;
  String[][] result = (String[][])request.getAttribute("table_result");
%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script language="javascript">
  function save() {

    for(i = 0; i < template.parameters.length; i++) {

      switch (isDouble(document.all['curr' + i], 12, 6)) {
      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于12个字符');
        return false;
      case 3 :
        alert('金额小数部分不能高于6个字符');
        return false;
      }
      switch (isDouble(document.all['plan' + i], 12, 6)) {
      case 0 :
        alert('金额必须为数字型');
        return false;
      case 1 :
        alert('金额整数部分不能高于12个字符');
        return false;
      case 3 :
        alert('金额小数部分不能高于6个字符');
        return false;
      }
    }

    for(i = 0; i < template.parameters.length; i++) {
      if (document.all['curr' + i].value == '') {
        document.all['curr' + i].value = 'null';
      }
      if (document.all['plan' + i].value == '') {
        document.all['plan' + i].value = 'null';
      }
      template.parameters[i].value =
				template.parameters[i].value +
        '|^|' + document.all['curr' + i].value + '|^|' +
        document.all['plan' + i].value;
      template.parameters[i].checked = true;
    }
    show_wait();
    template.submit();
    return true;
  }


</Script>

<html:html leftMargin="0">

<form name="template" method="post" action="WBIDirectInput.jspviewhigh">

  <html:message/>

  <html:table clazz="complex">

	    <!-- 操作 -->
      <%if (result != null) {%>
	    <tr>
        <td><button class="pageBtn" onclick="save()">保存</button> 
        <button class="pageBtn" onclick="parent.release()" >关闭</button> 
          <!--<img src="images/save.gif" style="cursor:hand" onclick="save()">
          <img src="images/close.gif" class="mouse" onclick="parent.release()">-->
        </td>
      </tr>
      <%}%>

	    <!-- 结果集 -->
	    <tr>
	      <td>
	      <html:table clazz="result">
	         <html:tr clazz='label'>
            <td>指标</td>
            <td>是否必填</td>
            <td>采集方式</td>
            <td>本期值</td>
            <td>预算值</td>
          </html:tr>
		      <%
          if (result != null) {          
            for (int i = 0; i < result.length; i++) {
              if(Integer.parseInt(result[i][6])==0){
                nf = new DecimalFormat("###0");
              }else{
                String zero = new String("0.");
                for(int j=0; j<Integer.parseInt(result[i][6]); j++){
                  zero += "0";
                }
                nf = new DecimalFormat("###"+zero);
              }
          %>

          <tr>
            <input type="checkbox" name="parameters" value="<%=result[i][0]%>" style="display:none"/>
            <td class="normalText"><%=(result[i][0] + ":" + result[i][1])%></td>
            <td class="normalText">
            	<%=result[i][7].equals("N")?"否":"是"%>
            </td>
            <td class="normalText">
            	<%=result[i][8].equals("1")?"自动":"录入"%>
            </td>
            <td>
            <input class="textInputC" style="border:1 solid gray" type="text" id="<%="curr" + i%>" name="<%="curr" + i%>"
             value="<%=result[i][4].length()!=0?nf.format(Double.parseDouble(result[i][4])):""%>" <%if(request.getAttribute("flag")!=null && request.getAttribute("flag").equals("R")) out.print("disabled");%>>
            </td>
            <td class="numberText">
              <input class="textInputC" type="text" id="<%="plan" + i%>" name="<%="plan" + i%>"
             value="<%=result[i][5].length()!=0?nf.format(Double.parseDouble(result[i][5])):"" %>" <%if(request.getAttribute("flag")!=null && request.getAttribute("flag").equals("R")) out.print("disabled");%>>
            </td>
        </tr>
		      <%
            }
          }
          %>
	      </html:table>
	      </td>
	    </tr>
	  </html:table>

    <input type="hidden" name="yearMonth"
      value="<%=request.getParameter("yearMonth")%>">
    <input type="hidden" name="subFunction" value="WBIConfig">
</form>
</html:html>






