<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/input/direct/subj/SubjIndexConfig.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="java.text.*" %>
<%
  DecimalFormat nf ;
  String[][] result = (String[][])request.getAttribute("table_result");
  String subjIndexName = request.getParameter("subjIndexName");
  int precision = Integer.parseInt(request.getParameter("precision"));
  StringBuffer formatStr = new StringBuffer("###0");
  for (int i = 0; i < precision; i++) {
    if (i == 0) {
      formatStr.append(".");
    }
    formatStr.append("0");
  }
  java.text.DecimalFormat format = new java.text.DecimalFormat(formatStr.toString());
%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script language="javascript">

  function save() {

    for(i = 0; i < template.parameters.length; i++) {
      switch (isDouble(document.all['curr' + i], 12, <%=precision%>)) {
        case 0 :
          alert('本期值必须为数字型');
          return false;
        case 1 :
          alert('本期值整数部分不能高于12个字符');
          return false;
        case 3 :
          alert('本期值小数部分不能高于<%=precision%>个字符');
          return false;
      }
      switch (isDouble(document.all['plan' + i], 12, <%=precision%>)) {
        case 0 :
          alert('计划值必须为数字型');
          return false;
        case 1 :
          alert('计划值整数部分不能高于12个字符');
          return false;
        case 3 :
          alert('计划值小数部分不能高于<%=precision%>个字符');
          return false;
      }

      if (document.all['curr' + i].value == ''
        && document.all['plan' + i].value == '') {
        template.parameters[i].checked = false;
        continue;
      }

      var curr = document.all['curr' + i].value;
	  var plan = document.all['plan' + i].value;
      if (document.all['curr' + i].value == '') {
        curr = 'null';
      }
      if (document.all['plan' + i].value == '') {
        plan = 'null';
      }

      template.parameters[i].value =
				template.parameters[i].value +
        '|^|' + curr + '|^|' + plan;
      template.parameters[i].checked = true;
    }
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="child">
<form name="template" method="post" action="subjIndexConfig.jspviewhigh">

  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'><%=subjIndexName%></html:title>

  <%if (result != null) {%>
  <table>
    <tr>
      <td><button class="pageBtn" onclick="return save();">保存</button> 
      <!--<img src="images/save.gif" style="cursor:hand" onclick="return save();">--></td>
    </tr>
  </table>
  <%}%>

  <html:table clazz="result" divWidth="" divHeight="350">
    <html:tr clazz='label'>
      <td>项目指标</td>
	  <td>本期值</td>
	  <td>预算值</td>
    </html:tr>
    <%
      if (result != null) {
      for (int i = 0; i < result.length; i++) {
       if(result[i][7].length()==0)
      	result[i][7]="0";
        if(Integer.parseInt(result[i][7])==0){
                nf = new DecimalFormat("###0");
              }else{
                String zero = new String("0.");
                for(int j=0; j<Integer.parseInt(result[i][7]); j++){
                  zero += "0";
                }
                nf = new DecimalFormat("###"+zero);
              }
    %>

    <tr>
      <input type="checkbox" name="parameters" value="<%=result[i][0]%>" style="display:none">
      <td class="normalText"><%=result[i][1]%></td>
      <td class="normalText">
        <input type="text" class="textInputC" id="<%="curr" + i%>" value="<%=result[i][5].length()!=0?nf.format(Double.parseDouble(result[i][5])):""%>"
        >
      </td>
      <td class="normalText">
        <input type="text" class="textInputC" id="<%="plan" + i%>" value="<%=result[i][6].length()!=0?nf.format(Double.parseDouble(result[i][6])):""%>"
       >
      </td>
    </tr>
      <%}%>
    <%}%>
  </html:table>


  <input type="hidden" name="yearMonth"
    value="<%=request.getParameter("yearMonth")%>">
  <input type="hidden" name="idxCode"
    value="<%=request.getParameter("idxCode")%>">
  <input type="hidden" name="subFunction" value="config">
  <input type="hidden" name="subjIndexName" value="<%=subjIndexName%>">
  <input type="hidden" name="precision" value="<%=precision%>">
</form>
</html:html>
