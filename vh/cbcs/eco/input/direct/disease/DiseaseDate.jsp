<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/input/direct/disease/DiseaseDate.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Modtime:  $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page contentType="text/html; charset=GBK" errorPage="../../../error.jsp"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="java.text.*" %>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    for(i = 0; i < template.parameters.length; i++){
    if(document.all['value' + i].value.length!=0){
      switch(isDouble(document.all('value'+i),12,2)){
	        case 0 : alert('本期值必须为数字型'); return;
	        case 1 : alert('本期值整数部分不能高于12个字符'); return;
	        case 2 : alert('本期值没有整数部分'); return;
	        case 3 : alert('本期值小数部分不能高于2个字符'); return;
      	}
      }
      if(document.all['plan' + i].value.length!=0){
	      switch(isDouble(document.all('plan'+i),12,2)){
	        case 0 : alert('预测值必须为数字型'); return;
	        case 1 : alert('预测值整数部分不能高于12个字符'); return;
	        case 2 : alert('预测值没有整数部分'); return;
	        case 3 : alert('预测值小数部分不能高于2个字符'); return;
	      }
      }
    }
    for(i = 0; i < template.parameters.length; i++){
      if (document.all['value' + i].value == '') {
        document.all['value' + i].value = 'null';
      }
      if (document.all['plan' + i].value == '') {
        document.all['plan' + i].value = 'null';
      }
      template.parameters[i].value=template.parameters[i].value+'|^|'+document.all['value'+i].value+'|^|'+document.all['plan'+i].value;
      template.parameters[i].checked = true;
    }
    template.subFunction.value='store';
    show_wait();
    template.submit();
    return true;
  }

  function selectType(){
    show_wait();
    template.submit();
    return true;
  }

</Script>
<html:html clazz="child">
<form name="template" method="post" action="ecoInputDirectDisease.jspviewhigh">
	  <html:message/>
	  <html:table clazz="complex">
      <tr>
        <td class="normalText" nowrap="nowrap">就诊类型：</td>
        <td class="normalText" nowrap="nowrap"><input type="radio" name="type" value="I" onclick="selectType()" <%if(request.getParameter("type")==null || request.getParameter("type").equals("I")) out.print("checked");%>>住院</td>
        <td class="normalText" nowrap="nowrap"><input type="radio" name="type" value="O" onclick="selectType()" <%if(request.getParameter("type")!=null && request.getParameter("type").equals("O")) out.print("checked");%>>门诊</td>
      </tr>
      <html:title clazz='module'><%=request.getParameter("indexname")%></html:title>
      <%if(request.getAttribute("flag")==null || request.getAttribute("flag").equals("W")){%>
      <tr>
        <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button> 
         <!-- <img src="images/save.gif" style='cursor:hand' onclick="return save();" />-->
        </td>
      </tr>
      <%}%>
     <div style='overflow:auto; width:px; height:px'><table width='100%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
	      <tr class='resultLabel' noWrap='true'>
          <td nowrap="nowrap" class="resultLabel">病种名称</td>
          <td nowrap="nowrap" class="resultLabel">病种分型</td>
          <td nowrap="nowrap" class="resultLabel">本期值</td>
          <td nowrap="nowrap" class="resultLabel">预测值</td>
	      </tr>
          <%
            DecimalFormat nf ;
            String[][] result = (String[][]) request.getAttribute( "table_result" );
            if (result!=null) {
              for (int i=0; i<result.length; i++) {
                
				if(result[i][6].length()==0)
      				result[i][6]="0";

                if(Integer.parseInt(result[i][6])==0){
					nf = new DecimalFormat("###0");
                }else{
					String zero = new String("0.");
					for(int j=0; j<Integer.parseInt(result[i][6]); j++){
					  zero += "0";
					}
					nf = new DecimalFormat("###"+zero);
              }
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
          %>
        <tr CLASS="<%=rowColor%>">
          <input type="checkbox" name="parameters" value="<%=(result[i][0]+ "|^|" + result[i][2])%>" style="display:none">
          <td class="normalText" nowrap="nowrap"><%=result[i][1]%></td>
          <td class="normalText" nowrap="nowrap"><%=result[i][3]%></td>
          <td width="75%" class="normalText" nowrap="nowrap">
	          <input type=text id="<%="value"+i%>" value="<%=result[i][4].length()!=0?nf.format(Double.parseDouble(result[i][4])):""%>" class="textInputB" 
			  <%if(request.getAttribute("flag")!=null && request.getAttribute("flag").equals("R")) out.print("disabled");%>/>
		  </td>
          <td width="75%" class="normalText" nowrap="nowrap">
	          <input type=text id="<%="plan"+i%>" value="<%=result[i][5].length()!=0?nf.format(Double.parseDouble(result[i][5])):""%>" class="textInputB" 
	          <%if(request.getAttribute("flag")!=null && request.getAttribute("flag").equals("R")) out.print("disabled");%>/>
          </td>
        </tr>
          <%
              }
            }
          %>
	   </table>
	   </div>
	  </html:table>
  <input type=hidden name="subFunction" value="getDisease"/>
  <input type=hidden name="yearmonth" value="<%=request.getParameter("yearmonth")%>" />
  <input type=hidden name="indexcode" value="<%=request.getParameter("indexcode")%>"/>
  <input type=hidden name="indexname" value="<%=request.getParameter("indexname")%>"/>
</form>

</html:html>
