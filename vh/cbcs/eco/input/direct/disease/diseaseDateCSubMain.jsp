<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/input/direct/disease/diseaseDateCSubMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<html:html leftMargin="0" topMargin="0">
	<form name="template" method="post" action="ecoInputDirectDisease.viewhigh">
	  <html:message/>
		<table>
			<tr>
				<td><button class="pageBtn" onclick="parent.release();" >关闭</button> 
					<!--<img src="images/close.gif" style="cursor:hand" onclick="parent.release();"/>-->
				</td>
        <td rowspan="2" VLAIGN="top" >
          <iframe  VSPACE="1" HSPACE="1" FRAMEBORDER="0" WIDTH="615" SRC="" NAME="Disease_Iframe" HEIGHT="440" ></iframe>
        </td>				
			</tr>
			<tr>
				<td>
					<div style='overflow:auto; width:200px; height:410px'><table width='100%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
		        <tr class='resultLabel' noWrap='true'>
		          <td class="resultLabel">病种基本指标</td>
		          <td class="resultLabel">必填</td>
		          <td class="resultLabel">采集方式</td>
		        </tr>
		        <%
		          DecimalFormat nf = new DecimalFormat("#,##0.00");
		          String[][] result = (String[][]) request.getAttribute( "table_result" );
		          if (result!=null) {
		            for (int i=0; i<result.length; i++) {
		              String rowColor = "rowGray";
		              if (i/2*2==i) rowColor = "rowWhite";
		        %>
		        <tr CLASS="<%=rowColor%>">
		          <td class="normalText">
		            <a href="ecoInputDirectDisease.jspviewhigh?subFunction=getDisease&indexcode=<%=result[i][0]%>&indexname=<%=result[i][1]%>&yearmonth=<%=request.getParameter("year_month")%>"  target="Disease_Iframe">
		            <%=result[i][1]%></a>
		          </td>
		          <td><%=result[i][2]%>
		          </td>
		          <td><%=result[i][3]%>
		          </td>
		          
		        </tr>
		        <%
		            }
		          }
		        %>
		      </table>				
	      </td>
      </tr>
   </table>
   
   <input type=hidden name="year_month" value="<%=request.getParameter("year_month")%>" />
   <input type=hidden name="subFunction" value="subMain"/>
</form>

</html:html>
