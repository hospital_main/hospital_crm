<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/input/direct/disease/diseaseDateCMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script language="javascript">
  var flag = false; // true--正在添加
	
  function add() {
  	if (flag) {
  		alert("请先关闭正在添加中的页面");
  		return false;
  	}

    if (template.yearMonth.value == '') {
      alert('请选择年月!');
      return false;
    }

		wait();
    template.target="process";
    template.action='ecoInputDirectDisease.jspviewhigh?subFunction=subMain&year_month='+template.yearMonth.value    
    template.submit();
    
    flag = true;    
    document.getElementById("td_month").style.display="none";
    document.getElementById("td_month_read").style.display="";
    var str = template.yearMonth.value;
    str = str.substring(0, 4)+"年"+str.substring(4)+"月";
    document.getElementById("td_month_read").innerText = str;
    
    return false;
  }
  
  
  function release() {
  	flag = false;
  	document.getElementById("_wait").style.display='none';
		document.getElementById("_interval").style.display='none';
		document.getElementById("td_month").style.display="";
    document.getElementById("td_month_read").style.display="none";
  }
  
  
  var _1st_current=1;
  function wait() {
		document.getElementById("_wait").style.display='';
		document.getElementById("_interval").style.display='none';
	}
	
	function show() {
		document.getElementById("_wait").style.display='none';
		document.getElementById("_interval").style.display='';
	}	
</Script>

<html:html >
<form name="template" method="post" action="ecoInputDirectDisease.jspviewhigh">

  <html:message/>

  <html:title clazz='module'>病种基本指标</html:title>

  <html:table clazz="simple">
    <tr>
      <td class="normalText">
        核算月：
      </td>
      <td class="normalText" id="td_month_read" style="display:none"/ >
      <td class="normalText" id="td_month">
        <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("yearMonth", request.getParameter("yearMonth"))%>
      </td>
      <td class="normalText"><button class="pageBtn" onclick="return add();">添加</button>
            </td>
    </tr>
  </html:table>
 
 	<table width="100%" >
		<tr>
	 		<td id='_wait' style="display:none" align='center' width='50'><img id='_image' src='img/loading.gif'/ onclick="show()" style="cursor:hand"></td>
			<td id='_interval' width=1023 colspan="20" style="display:none">
				<IFrame id="body_iframe" name="process" width="830" height="470" frameborder="0" src="" ></IFrame>
			</td>
		</tr>
	</table>
</form>
</html:html>



