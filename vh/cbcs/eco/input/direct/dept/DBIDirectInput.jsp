<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/input/direct/dept/DBIDirectInput.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="java.text.*" %>
<%
  DecimalFormat nf ;
  String[][] result = (String[][])request.getAttribute("table_result");
%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>

<Script language="javascript">
  function save() {
  	if(document.all['curr' + 0].disabled==true){
  		alert("数据不能保存!");
  		return ;
  	}
  	
    for(i = 0; i < template.parameters.length; i++) {
		if(document.all['curr' + i]!=null){
		      switch (isDouble(document.all['curr' + i], 12, 6)) {
		        case 0 :
		          alert('金额必须为数字型');
		          return false;
		        case 1 :
		          alert('金额整数部分不能高于12个字符');
		          return false;
		        case 3 :
		          alert('金额小数部分不能高于6个字符');
		          return false;
		      }
		}
		if(document.all['plan' + i]!=null){
		      switch (isDouble(document.all['plan' + i], 12, 6)) {
		        case 0 :
		          alert('金额必须为数字型');
		          return false;
		        case 1 :
		          alert('金额整数部分不能高于12个字符');
		          return false;
		        case 3 :
		          alert('金额小数部分不能高于6个字符');
		          return false;
		      }
	      }
    }

    for(i = 0; i < template.parameters.length; i++) {
      if (document.all['curr' + i].value == '') {
        document.all['curr' + i].value = 'null';
      }
      if (document.all['plan' + i].value == '') {
        document.all['plan' + i].value = 'null';
      }
      var arr = template.parameters[i].value.split('|^|');
  	if(arr.length>2 && arr[2]!='N'){
      template.parameters[i].value =
				template.parameters[i].value +
        '|^|' + document.all['curr' + i].value + '|^|' +
        document.all['plan' + i].value;
      }else{
      	template.parameters[i].value =
				template.parameters[i].value +
        '|^|' + 0 + '|^|' +0;
      }
      template.parameters[i].checked = true;
    }
    show_wait();
    
    template.submit();
    return true;
  }
</Script>

<html:html leftMargin="0" topMargin="0">
<form name="template" method="post" action="DBIDirectInput.jspviewhigh">

  <html:message/>

  <html:title clazz='module'><%=request.getParameter("basicIndexName")%></html:title>

  <%if (result != null) {%>
  <table>
    <tr>
      <td>
      <button class="pageBtn" onclick="return save();">保存</button> 
      <!--<img src="images/save.gif" style="cursor:hand" onclick="return save();">--></td>
    </tr>
  </table>
  <%}%>

  <html:table clazz="result" divWidth="" divHeight="380">
    <html:tr clazz='label'>
      <td>科室</td>
      <td>本期值</td>
      <td>预算值</td>
    </html:tr>

    <%
      if (result != null) {
      for (int i = 0; i < result.length; i++) {
      if(result[i][10].length()==0)
      	result[i][10]="0";
        if(Integer.parseInt(result[i][10])==0){
                nf = new DecimalFormat("###0.00");
              }else{
                String zero = new String("0.");
                for(int j=0; j<Integer.parseInt(result[i][10]); j++){
                  zero += "0";
                }
                nf = new DecimalFormat("###"+zero);
              }
       //if(!result[i][4].equals("Y"))
       
    %>

    <tr>
      <input type="checkbox" name="parameters"
        value="<%=(result[i][0]+ "|^|" + result[i][2] + "|^|" + result[i][4])%>"
        style="display:none">
      <td class="normalText"><%=(result[i][0] + ":" + result[i][1])%></td>
      
      <td>
     <input  type="text" id="<%="curr" + i%>"
          value="<%=result[i][8].length()!=0?nf.format(Double.parseDouble(result[i][8])):""%>"
          <%if (!result[i][4].equals("Y")) {%> readonly <%} else {%>class="textInputC"<%}%>
        <%if(request.getAttribute("flag")!=null && request.getAttribute("flag").equals("R")) out.print("disabled");%>>
      </td>
      
	  <td>
        <input type="text" id="<%="plan" + i%>"
         value="<%=result[i][9].length()!=0?nf.format(Double.parseDouble(result[i][9])):""%>"
          <%if (!result[i][4].equals("Y")) {%> readonly <%}else {%>class="textInputC"<%}%>
        <%if(request.getAttribute("flag")!=null && request.getAttribute("flag").equals("R")) out.print("disabled");%>>
      </td>
    </tr>
      <%}%>
    <%}%>
  </html:table>


  <input type="hidden" name="yearMonth" value="<%=request.getParameter("yearMonth")%>">
  <input type="hidden" name="idxCode" value="<%=request.getParameter("idxCode")%>">
  <input type="hidden" name="subFunction" value="DBIConfig">
  <input type="hidden" name="basicIndexName" value="<%=request.getParameter("basicIndexName")%>"/>
</form>
</html:html>
