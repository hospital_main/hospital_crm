<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/input/direct/dept/BasicDeptIndexes.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
  String[][] result = (String[][])request.getAttribute("table_result");
%>

<html:html leftMargin="0" topMargin="0">
	<html:table clazz="result" divWidth="" divHeight=""> 
	  <html:tr clazz='label'>
	    <td>指标</td>
        <td>必填</td>
        <td>采集方式</td>
	  </html:tr>
	  <%
	    if (result != null) {
	      for (int i = 0; i < result.length; i++) {
	  %>
	  <tr>
	    <td>
	      <a href="DBIDirectInput.jspviewhigh?subFunction=DBIDirectInput&idxCode=<%=result[i][0]%>&basicIndexName=<%=result[i][1]%>&yearMonth=<%=request.getParameter("yearMonth")%>" target="Iframe_table">
	        <%=result[i][1]%>
	      </a>
	     </td>
	     <td>   
	        <%=result[i][3].equals("N")?"否":"是"%>
	      </td>
	      <td>  
	        <%=result[i][4].equals("1")?"自动":"录入"%>
	    </td>
	  </tr>
	  <%
	      }
	    }
	  %>
	</html:table>
</html:html>