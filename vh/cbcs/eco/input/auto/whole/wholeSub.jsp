<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/input/auto/whole/wholeSub.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Modtime: $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<html:html clazz="child">
<form name="template" method="post" action="ecoInputAutoWhole.jspviewhigh">
	<html:message/>

	<html:table clazz="complex">
    <tr>
      <td>
				<div style='overflow:auto; width:200px; height:410px'><table width='100%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
		        <tr class='resultLabel' noWrap='true'>
            <td class="resultLabel">全院基本指标</td>
	        </tr>
          <%
            DecimalFormat nf = new DecimalFormat("#,##0.00");
            String[][] result = (String[][]) request.getAttribute( "index" );
            if (result!=null) {
              for (int i=0; i<result.length; i++) {
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
          %>
          <tr CLASS="<%=rowColor%>">
            <td class="normalText">
              <a href="ecoInputAutoWhole.jspviewhigh?subFunction=getIndex&indexcode=<%=result[i][0]%>&indexname=<%=result[i][1]%>&type=<%=result[i][2]%>"  target="Index_Iframe">
              <%=result[i][1]%></a>
            </td>
          </tr>
          <%
              }
            }
          %>
	      </table>
      </td>
      <td>
	  		<html:table clazz="complex">
          <tr>
            <td COLSPAN="2" VLAIGN="top" >
              <iframe  VSPACE="1" HSPACE="1" FRAMEBORDER="0" WIDTH="660" SRC="" NAME="Index_Iframe" HEIGHT="435" ></iframe>
            </td>
          </tr>
	      </html:table>
      </td>
    </tr>
	</html:table>
  <input type=hidden name="subFunction" value="subMain"/>
</form>

</html:html>
