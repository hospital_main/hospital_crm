<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/input/auto/dept/setIndex.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="java.text.*" %>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    template.subFunction.value='store';
    show_wait();
    template.submit();
    return true;
  }

  function selectType(){
    show_wait();
    template.submit();
    return true;
  }

</Script>
<html:html clazz="child">
	<form name="template" method="post" action="ecoInputAutoDept.jspviewhigh">
	  <html:message/>
	  <html:title clazz='module'><%=request.getParameter("indexname")%></html:title>

 		<% String[][] result = (String[][]) request.getAttribute( "table_result" );%>
	  <html:table clazz="complex">
      <tr>
        <td class="normalText" nowrap="nowrap">项目类型：</td>
        <td class="normalText" nowrap="nowrap"><input type="radio" name="type" value="I" onclick="selectType()" <%if(request.getParameter("type")==null || !request.getParameter("type").equals("C")) out.print("checked");%>>收入项目<input type="radio" name="type" value="C" onclick="selectType()" <%if(request.getParameter("type")!=null && request.getParameter("type").equals("C")) out.print("checked");%>>成本项目</td>
        <td class="normalText" nowrap="nowrap">科室类型：</td>
        <td class="normalText" nowrap="nowrap">
          <select name="out_in" class="selectBg">
              <option value='O' <%if(result!=null && result[0][3]!=null && result[0][3].equals("O")) out.print("selected");%>>门诊</option>
              <option value='I' <%if(result[0][3]!=null && result[0][3].equals("I")) out.print("selected");%>>住院</option>
              <option value='T' <%if(result[0][3]!=null && result[0][3].equals("T")) out.print("selected");%>>医技</option>
              <option value='M' <%if(result[0][3]!=null && result[0][3].equals("M")) out.print("selected");%>>管理</option>
              <option value='A' <%if(result[0][3]!=null && result[0][3].equals("A")) out.print("selected");%>>医辅</option>
          </select>
        </td>
      </tr>
      <tr>
        <td colspan="2">
        <button class="pageBtn" onclick="return save();">保存</button> 
        <!-- <img src="images/save.gif" style='cursor:hand' onclick="return save();" />-->
        </td>
      </tr>
		</html:table>
    <html:table clazz="result" divWidth="" divHeight="315">
      <html:tr clazz='label'>
        <td nowrap="nowrap" class="resultLabel">选择</td>
        <td nowrap="nowrap" class="resultLabel">项目代码</td>
        <td nowrap="nowrap" class="resultLabel">项目名称</td>
      </html:tr>
        <%
          if (result!=null) {
            for (int i=0; i<result.length; i++) {
              String rowColor = "rowGray";
              if (i/2*2==i) rowColor = "rowWhite";
        %>
      <tr>
        <td align="center"><input type="checkbox" name="parameters" value="<%=result[i][0]%>" <%if(result[i][2]!=null && result[i][2].equals("B")) out.print("checked");%>></td>
        <td class="normalText" nowrap="nowrap"><%=result[i][0]%></td>
        <td class="normalText" nowrap="nowrap"><%=result[i][1]%></td>
      </tr>
        <%
            }
          }
        %>
    </html:table>

	  <input type=hidden name="subFunction" value="getIndex"/>
	  <input type=hidden name="indexcode" value="<%=request.getParameter("indexcode")%>"/>
	  <input type=hidden name="indexname" value="<%=request.getParameter("indexname")%>"/>
	</form>

</html:html>
