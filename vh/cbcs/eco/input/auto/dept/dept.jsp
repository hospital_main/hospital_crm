<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/input/auto/dept/dept.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Modtime:$
 $Revision: 1.1 $
 $NoKeywords:$
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script language="javascript">
  function collect() {
    if (template.yearMonth.value == '') {
      alert('请选择年月!');
      return false;
    }
    show_wait();
    template.target="";
    template.action="ecoInputAutoDept.jspviewhigh";    
    template.submit();
    return true;
  }
  
  var flag = false; // true--正在配置
  function set() {
  	if (flag) {
  		flag=false;
  		release();
  		return false;
  	}
  	
  	flag = true;
		wait();
    template.target="process";
    template.action="ecoInputAutoDept.jspviewhigh?subFunction=subMain";    
    template.submit();
  	
    return false;
  }

  function release() {
  	flag = false;
  	document.getElementById("_wait").style.display='none';
		document.getElementById("_interval").style.display='none';
  }
  
  var _1st_current=1;
  function wait() {
		document.getElementById("_wait").style.display='';
		document.getElementById("_interval").style.display='none';
	}
	
	function show() {
		document.getElementById("_wait").style.display='none';
		document.getElementById("_interval").style.display='';
	}	  
</Script>

<html:html clazz="main">
<form name="template" method="post" action="ecoInputAutoDept.jspviewhigh">

 	<html:message/>

  <html:title clazz='module'>科室指标自动采集</html:title>

  <html:table clazz="simple">
    <tr>
      <td class="normalText">
        核算月：
      </td>
      <td class="normalText">
        <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("yearMonth", request.getParameter("yearMonth"))%>
      </td>
      <td class="normalText">
        <!--img src="images/set.gif" style="cursor:hand" onclick="return set();">
        <img src="images/collect.gif" style="cursor:hand" onclick="return collect();"-->
        <button class="pageBtn" onclick="return set();">配置</button>
      	<button class="pageBtn" onclick="return collect();">采集</button>
      </td>
    </tr>
  </html:table>
	<table width="100%" >
		<tr>
	 		<td id='_wait' style="display:none" align='center' width='50'><img id='_image' src='img/loading.gif'/ onclick="show()" style="cursor:hand"></td>
			<td id='_interval' width=1023 colspan="20" style="display:none">
				<IFrame id="body_iframe" name="process" width="830" height="470" frameborder="0" src="" ></IFrame>
			</td>
		</tr>
	</table>
  <input type='hidden' name="subFunction" value='collect'>
</form>
</html:html>



