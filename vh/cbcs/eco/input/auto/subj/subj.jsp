<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/input/auto/subj/subj.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Modtime:$
 $Revision: 1.1 $
 $NoKeywords:$
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script language="javascript">
  function collect() {
    if (template.yearMonth.value == '') {
      alert('请选择年月!');
      return false;
    }
    show_wait();
	  template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="ecoInputAutoSubj.jspviewhigh">

  <!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>项目指标自动采集</html:title>

  <html:table clazz="simple">
    <tr>
      <td class="normalText">
        核算月：
      </td>
      <td class="normalText">
        <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("yearMonth", request.getParameter("yearMonth"))%>
      </td>
      <td class="normalText">
        <!--img src="images/collect.gif" style="cursor:hand" onclick="return collect();"-->
     	 <button class="pageBtn" onclick="return collect();">采集</button>
      </td>
    </tr>
  </html:table>
  <input type='hidden' name="subFunction" value='collect'>
</form>
</html:html>



