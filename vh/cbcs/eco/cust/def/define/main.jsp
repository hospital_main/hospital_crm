<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/cust/def/define/main.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: $
     $Revision: 1.1 $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../../error.jsp" %>

<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function create() {
        template.subFunction.value='preparedCreate';
        show_wait();
        template.submit();
        return true;
    }


  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }
  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }
  function find() {
    template.subFunction.value='find';
    template.submit();return true;
  }
  
  var id="" ;temp="";
  function clickIt(aid){
      if (aid.checked) temp =aid.value ;
      else  temp="";
      id= id+temp+" ";

        
  }  
  
  function accredit(){  
    if (id=="") {
	  alert("请选择模板！") ;	   
	  return(0);
	}	
    template.subFunction.value='accredit';
	window.open("define.jspviewhigh?subFunction="
	+template.subFunction.value+"&template="+id , 'child', 'width=400, height=250, top=250, left=350,  ');
  
	  return(0);		 
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="define.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>视角定义主页面</html:title>
  <!-- 查询信息 -->
    <%
      BaseRO  ro= (BaseRO)request.getAttribute("ro");

      String[][] result=null;
      if(ro!=null)
        {result=ro.getTableResult();
       }
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
      oper.addNeedButton("images/create.gif", "return create()");     //  添加
      oper.addNeedButton("images/shouquan.gif", "return accredit()");     //  添加      
    %>
  <html:table clazz="simple">
    <tr>

      <td nowrap class="signText">模板名称：</td>
      <td nowrap class="normalText">
      <input type="text" name="template_name" size="20" value='<%=request.getParameter("template_name")==null?"":request.getParameter("template_name")%>' /></td>
      <td nowrap class="signText">分析方法：</td>
      <td nowrap class="normalText">
     <input type="text" name="analysis_mtd" size="20" value='<%=request.getParameter("analysis_mtd")==null?"":request.getParameter("analysis_mtd")%>'/></td>
      <td>
      <button class="pageBtn" name=""  onclick="return find();" >查询</button>
      <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
 
</tr>

  </html:table>
  <html:title clazz='table'>视角定义</html:title>
  <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>
      <html:table clazz="result">
     <html:tr clazz='label'>
          <td >选择</td>
          <td >模板名称</td>
          <td >分析方法</td>

         </html:tr>

        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {

                String primaryKey = result[i][0];
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j]=="") result[i][j]="&nbsp;";
                }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
           <td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"   onclick="clickIt(this)"  ></td>
          <td class="normalText"><a href="define.jspviewhigh?subFunction=preparedupdate&primaryKey=<%=primaryKey%>"><%=result[i][1]%></a></td>
          <td class="normalText"><%=result[ i ][ 2]%></td>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>

   </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>

