<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/cust/def/define/create.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Modtime: 03-08-29 12:07 $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../../error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language='javascript' src='javascript/tag.js'></script>
<Script Language="JavaScript">
  function create() {
//-----------------------------------------------
    if(isEmpty(template.name)){
        alert('模板名称不能为空!');
        return false;
    }
    if(isEmpty(template.code)){
        alert('模板代码不能为空!');
        return false;
    }
    if(isTooLong(template.code,12)){
      alert('模板代码不能高于12个字符!');
      return;
    }
    if(isTooLong(template.name,100)){
      alert('模板名称不能高于100个字符!');
      return;
    }
//--------------------------------


     if(template.type[0].checked==true)
      {
        idx=false//判断是否选择了全院指标或科室指标
     for(var i=0;i<template.idx1.length;i++)
       {
       if(template.idx1.options[i].selected)
           idx=true;
        }
       if(idx==false)
       {  alert('指标不能为空');
          return false;
        }
      }

   //--------------------------------------------------------------
    var  dept_idx=0;
    var  dept_sign=0;
    if(template.type[1].checked==true)
     {
   for(var i=0;i<template.idx2.length;i++)
       {
       if(template.idx2.options[i].selected)
           dept_idx++;
        }
     for(var i=0;i<template.dept.length;i++)

       {
       if(template.dept.options[i].selected)
           dept_sign++;
        }
    if(dept_idx==0&&dept_sign==0)
      { alert('请选择科室指标代码和科室代码');
          return false;
       }
    if(dept_idx!=0&&dept_sign==0)
      { alert('科室代码不能为空');
          return false;
       }
     if(dept_idx==0&&dept_sign!=0)
      { alert('科室指标代码不能为空');
          return false;
       }
     if(dept_idx>1&&dept_sign>1)
      { alert('科室指标代码和科室代码不能同时为多选');
          return false;
       }
     }
     var  subj_idx=0;
    var  subj_sign=0;
    if(template.type[2].checked==true)
     {
   for(var i=0;i<template.idx3.length;i++)
       {
       if(template.idx3.options[i].selected)
           subj_idx++;
        }
     for(var i=0;i<template.subj.length;i++)
       {
       if(template.subj.options[i].selected)
           subj_sign++;
        }
    if(subj_idx==0&&subj_sign==0)
      { alert('请选择项目指标代码和项目代码');
          return false;
       }
    if(subj_idx!=0&&subj_sign==0)
      { alert('项目代码不能为空');
          return false;
       }
     if(subj_idx==0&&subj_sign!=0)
      { alert('项目指标代码不能为空');
          return false;
       }
     if(subj_idx>1&&subj_sign>1)
      { alert('项目指标代码和项目代码不能同时为多选');
          return false;
       }
     }
     var  disease_idx=0;
     var  disease=0;
     var  model=0;
     if(template.type[3].checked==true)
     {
   for(var i=0;i<template.idx4.length;i++)
       {
       if(template.idx4.options[i].selected)
           disease_idx=disease_idx+1;
        }
     for(var i=0;i<template.disease.length;i++)
       {
       if(template.disease.options[i].selected)
           disease=disease+1;
        }
      for(var i=0;i<template.model.length;i++)
       {
       if(template.model.options[i].selected)
           model=model+1;
        }
    if(disease_idx==0)
      { alert('请选择病种指标代码');
          return false;
       }
    if(disease==0)
      { alert('请选择病种代码');
          return false;
       }
    if(model==0)
      { alert('请选择病种分型代码');
          return false;
       }
    if(disease_idx>8){
      alert('最多只能选择八个病种指标')
      return
    }
    if(disease*model>8){
      alert('最多只能选择八个病种病型')
      return
    }
    if(disease_idx>1&&disease*model>1)
      { alert('指标集与病种病型集不能同时多选');
          return false;
       }
     }
    template.subFunction.value='create';
    show_wait();
    template.submit();
    return true;
  }
function show()
{
        yard_idx.style.display = "block"
        dept_idx.style.display = "none"
        dept.style.display = "none"
        subj_idx.style.display = "none"
        subj.style.display = "none"
        disease_idx.style.display = "none"
        disease.style.display = "none"
        model.style.display = "none"
        disease_type.style.display = "none"
 }
function show1()
{       yard_idx.style.display = "none"
        dept_idx.style.display = "block"
        dept.style.display = "block"
        subj_idx.style.display = "none"
        subj.style.display = "none"
        disease_idx.style.display = "none"
        disease.style.display = "none"
        model.style.display = "none"
        disease_type.style.display = "none"
 }
  function show2()
{       yard_idx.style.display = "none"
        dept_idx.style.display = "none"
        dept.style.display = "none"
        subj_idx.style.display = "block"
        subj.style.display = "block"
        disease_idx.style.display = "none"
        disease.style.display = "none"
        model.style.display = "none"
        disease_type.style.display = "none"
 }
 function show3()
{       yard_idx.style.display = "none"
        dept_idx.style.display = "none"
        dept.style.display = "none"
        subj_idx.style.display = "none"
        subj.style.display = "none"
        disease_idx.style.display = "block"
        disease.style.display = "block"
        model.style.display = "block"
        disease_type.style.display = "block"
 }

  // 返回
  function returnmain() {
    template.subFunction.value='main';
    show_wait();
    template.submit();
    return true;
  }
function diff1(){
   var sign=false;
   for(var i=0;i<template.method.options.length;i++){
      if(template.method.options[i].selected==true){
         if(template.method.options[i].value=="R"){
            sign=true;
         }
      }
   }

   if(sign==true){
      document.getElementById("radio1").style.display='none';
      document.getElementById("radio2").style.display='none';
      document.getElementById("radio3").style.display='none';
      show();
   }else{
     document.getElementById("radio1").style.display='block';
     document.getElementById("radio2").style.display='block';
     document.getElementById("radio3").style.display='block';
   }
 }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="define.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>
  <!--信息栏-->
  <html:title clazz='module'>视角定义添加页面</html:title>
  <% String[][] employee=(String[][])request.getAttribute("employee");
     String[][] dept=(String[][])request.getAttribute("dept");
     String[][] subj=(String[][])request.getAttribute("subj");
     String[][] disease=(String[][])request.getAttribute("disease");
     String[][] model=(String[][])request.getAttribute("model");
     String[][] idx1=(String[][])request.getAttribute("idx1");
     String[][] idx2=(String[][])request.getAttribute("idx2");
     String[][] idx3=(String[][])request.getAttribute("idx3");
     String[][] idx4=(String[][])request.getAttribute("idx4");

   %>
  <!-- 简单信息 -->
 <table width='60%' cellspacing='6' border='0' align="center" class='normalText'>
    <tr>
      <td class="signText" nowrap="nowrap">模板代码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="code" style="border:1px solid gray;" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">模板名称：</td>
      <td class="normalText" nowrap="nowrap" ><input type=text name="name" style="border:1px solid gray;" /></td>
    </tr>
   <tr>
      <td class="signText" nowrap="nowrap">分析方法：</td>
      <td  class="normalText" nowrap="nowrap" >
       <select name="method"  onchange="diff1()" >
         <option value='C'>比较</option>
         <option value='M'>构成</option>
         <option value='T'>趋势</option>
         <option value='R'>月报</option>
       </td>

    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">指标类别：</td>
      <td>
        <table>
          <tr>
            <td class="normalText">
              <input type="radio" name='type' value='A' onclick="show()" <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("A")) out.print("checked"); if(request.getParameter("type")==null) out.print("checked");%>  >全院指标
            </td>
            <td class="normalText" id="radio1">
              <input type="radio"  name='type' value='B' onclick="show1()" <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("B")) out.print("checked");%>>科室指标
            </td>
            <td class="normalText" id="radio2">
              <input type="radio"   name='type' value='C' onclick="show2()" <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("C")) out.print("checked");%>>项目指标
            </td>
            <td class="normalText" id="radio3">
              <input type="radio"  name='type' value='D' onclick="show3()" <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("D")) out.print("checked");%>>病种指标
            </td>
          </tr>
        </table>
    </td>
    </tr>
     <tr id='yard_idx' <%if(request.getParameter("type")==null || request.getParameter("type").equals("A")) out.print("style='display:block;'"); else out.print("style='display:none;'");%> >
      <td class="signText" nowrap="nowrap">全院指标名称：</td>
      <td  class="normalText" nowrap="nowrap">
         <html:select property="eco_cust_def_define_whole"  name='idx1' size="4" biSelect="true" showTip="true"/>
      </td>
    </tr>
   <tr id='dept_idx' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("B")) out.print("style='display:block;'");else out.print("style='display:none;'");%>>
       <td class="signText" nowrap="nowrap">科室指标名称：</td>
       <td  class="normalText" nowrap="nowrap" >
           <html:select property="eco_cust_def_define_dept_idx"  name='idx2' size="4" biSelect="true" showTip="true"/>
       </td>
   </tr>
    <tr id='dept' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("B")) out.print("style='display:block;'");else out.print("style='display:none;'");%>>
      <td class="signText" nowrap="nowrap">科室名称：</td>
      <td  class="normalText" nowrap="nowrap" >
         <html:select property="eco_cust_def_define_dept"  name='dept' size="4" biSelect="true" showTip="false"/>
      </td>
      </tr>
      <tr id='subj_idx' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("C")) out.print("style='display:block;'");else out.print("style='display:none;'");%>>
       <td class="signText" nowrap="nowrap">项目指标名称：</td>
       <td  class="normalText" nowrap="nowrap" >
          <html:select property="eco_cust_def_define_subj_idx"  name='idx3' size="4" biSelect="true" showTip="true"/>
       </td>
   </tr>
    <tr id='subj' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("C")) out.print("style='display:block;'");else out.print("style='display:none;'");%>>
      <td class="signText" nowrap="nowrap">项目名称：</td>
      <td  class="normalText" nowrap="nowrap" >
       <html:select property="eco_cust_def_define_subj"  name='subj' size="4" biSelect="true" showTip="false"/>
      </td>
      </tr>
      <tr id='disease_idx' <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("D")) out.print("style='display:block;'");else out.print("style='display:none;'");%>>
       <td class="signText" nowrap="nowrap">病种指标名称：</td>

      <td  class="normalText" nowrap="nowrap" >
       <html:select property="eco_cust_def_define_disease_idx"  name='idx4' size="4" biSelect="true" showTip="true"/>
      </td>
   </tr>
   <tr id='disease' <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("D")) out.print("style='display:block;'");else out.print("style='display:none;'");%>>
      <td class="signText" nowrap="nowrap">病种名称：</td>
      <td  class="normalText" nowrap="nowrap" >
         <html:select property="eco_cust_def_define_disease"  name='disease' size="4" biSelect="true" showTip="false"/>
      </td>
      </tr>
      <tr id='model' <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("D")) out.print("style='display:block;'");else out.print("style='display:none;'");%>>
      <td class="signText" nowrap="nowrap">病种分型名称：</td>
      <td  class="normalText" nowrap="nowrap" >
       <select name="model" size='4' multiple>
        <% if(model!=null) {
         for(int i=0;i<model.length;i++){%>
            <option value='<%=model[i][0]%>'><%=model[i][0]+":"+model[i][1]%></option>
       <%}}%>
       </td>
      </tr>
      <tr id='disease_type' <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("D")) out.print("style='display:block;'");else out.print("style='display:none;'");%>>
      <td class="signText" nowrap="nowrap">就诊类型：</td>
      <td  class="normalText" nowrap="nowrap" >
       <select name="disease_type" >
            <option value='I'>住院</option>
            <option value='O'>门诊</option>
       </td>
      </tr>
    <tr>
      <td colspan="2"> 
      <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return returnmain();">返回</button>  
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/return.gif" class="mouse" onclick="return returnmain();" />--></td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>
</html:html>

