<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/cust/def/analyse/monrpt.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.text.*,com.viewhigh.cbcs.base.mvc.view.MonthComponent,com.viewhigh.cbcs.cbcs.util.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<script language='javascript' src='javascript/chart.js'></script>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<%
   int count=6,dptname=5,money=8,havemoney=4,grade=2,total=3,hcur=9;
   DecimalFormat nfL,nfR ;
   String ss="",ssr="";

   String[][] result = (String[][])request.getAttribute("result");
   //String[][] ind = (String[][])request.getAttribute("ind");
   //String[][] init_title = (String[][])request.getAttribute("init_title");
%>
<Script Language="JavaScript">
  function analysis(){
    if(template.yearMonth.value=='' ){
      alert('请选择核算月')
      return
    }

    show_wait();
    template.submit()
  }
  function print(){
    alert("print?")}

function winclose()
{ window.close();
 }
function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }
</Script>
<html:html clazz="main" fixRows="1" isPrint="true">
<form name="template" method="post" action="analyse.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>
<html:title clazz='module'>全院经营指标一览表</html:title>
  <html:table clazz="complex">
    <tr bgcolor="#FFFFFF">
      <td >
        <html:table clazz="complex">
          <tr>
            <td class="signText" nowrap="nowrap">核算月：</td>
            <td class="normalText" nowrap="nowrap"><%=new MonthComponent("yearMonth",request.getParameter("yearMonth"))%></td>
              <td><button class="pageBtn" onclick="return analysis();">分析</button></td>
               <%if(result!=null){ %>
              <td><button class="pageBtn" onclick="return preparedPrint();">打印</button></td>
              <td><button class="pageBtn" onclick="return saveData();">保存历史</button>
              <!--<img src="images/bcls.png" class="mouse" onclick="return saveData();"/>--></td>
              <%} %>
              <td><button class="pageBtn" onClick="winclose();" >关闭</button></td>
           </tr>
         </html:table>
   </html:table>
   <p>
     <p>
 <%if(result!=null){
%>
<html:webprint clazz='new' >
        <colgroup id='tg'>
        	<col style = <%=DisplayWidth.NAME_WIDTH%>>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
       <%if(result.length>=2){%>
        	<col style = <%=DisplayWidth.NAME_WIDTH%>>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
       <%}%>
        </colgroup>
  		<html:tr clazz='label'>
   				<td class="resultLabel">指标名称</td>
				<td class="resultLabel">本期值</td>
				<td class="resultLabel">累计值</td>
           <%if(result.length>=2){%>
		   <td class="resultLabel">指标名称</td>
		   <td class="resultLabel">本期值</td>
		   <td class="resultLabel">累计值</td>
           <%}%>
     </html:tr>
    <%int leng=result.length;
    if(leng/2*2<leng){
      leng=leng/2+1;
      while(leng<result.length&&!result[leng][1].equals("TOP")){
        leng++;}
      }else{
      leng=leng/2;
      while(leng<result.length&&!result[leng][1].equals("TOP")){
        leng++;}
      }
    for(int i=0; i<leng; i++){
         if(Integer.parseInt(result[i][count])==0){
           nfL = new DecimalFormat("#,##0");
         }else{
           String zero = new String("0.");
           for(int j=0; j<Integer.parseInt(result[i][count]); j++){
             zero += "0";
           }
           nfL = new DecimalFormat("#,##"+zero);
         }
         if((leng+i)<result.length&&Integer.parseInt(result[leng+i][count])==0){
           nfR=new DecimalFormat("#,##0");
           }else{
             if(leng+i<result.length){
             String zero2=new String("0.");
             for(int j=0;j<Integer.parseInt(result[leng+i][count]);j++){
               zero2+="0";
               }
               nfR=new DecimalFormat("#,##"+zero2);
               }else{
               nfR=new DecimalFormat("#,##0");
               }
             }
        %>
     <tr>
           <%if(Integer.parseInt(result[i][grade])<=1&&result[i][4].equals("N")){%>
			<td style="color:#0000FF;" align="left" >
		   <%}else{%>
			<td class="normalText">
		   <%}%>
		   <%
			ss="";
			for(int j=1;j<Integer.parseInt(result[i][grade]);j++){
				ss+="&nbsp&nbsp";
			}
			if(result[i][total].equals("0")&&result[i][havemoney].equals("N")){
				if(result[i][dptname].trim().substring(0,2).equals("累计") || result[i][dptname].trim().substring(0,2).equals("本期")){
					out.print(ss+result[i][dptname].trim().substring(2));
				}else{
					out.print(ss+result[i][dptname]);
				}
			}else{
				if(result[i][dptname].trim().substring(0,2).equals("累计") || result[i][dptname].trim().substring(0,2).equals("本期")){
					out.print(ss+result[i][dptname].trim().substring(2));
				}else{
					out.println(ss+result[i][dptname].trim());
				}
			}
		   %>
            </td>
			
			<td align="right">
		   <%if((result[i][money].equals("0")&&result[i][havemoney].equals("N"))){out.print("");}
		   else if((result[i][hcur].equals("N"))){out.print("X");}
		   else{out.print(nfL.format(Double.parseDouble(result[i][money]))+(result[i][7].equals("百分比")? "%":result[i][7]));}%>
		    </td>


            <td align="right">
		   <%if(result[i][total].equals("0")&&result[i][havemoney].equals("N")){out.print("");}
		    else{out.print(nfL.format(Double.parseDouble(result[i][total]))+(result[i][7].equals("百分比")? "%":result[i][7]));}%>
		    </td>


		   <%if(result.length>=2&&(leng+i)<result.length){%>
           <%if(Integer.parseInt(result[i+leng][grade])<=1&&result[i+leng][4].equals("N")){%>
		    <td style="color:#0000FF;" align="left"  >
		   <%}else{%>
		    <td class="normalText">
		   <%}%>
		   <%
			 ssr="";
			 for(int j=1;j<Integer.parseInt(result[i+leng][grade]);j++) 
			 {ssr+="&nbsp&nbsp";}
			if(result[i+leng][total].equals("0")&&result[i+leng][havemoney].equals("N")){
				if(result[i+leng][dptname].trim().substring(0,2).equals("累计") || result[i+leng][dptname].trim().substring(0,2).equals("本期")){
					out.print(ssr+result[i+leng][dptname].trim().substring(2));
				}else{
					out.print(ssr+result[i+leng][dptname]);
				}
			}else{
				if(result[i+leng][dptname].trim().substring(0,2).equals("累计") || result[i+leng][dptname].trim().substring(0,2).equals("本期")){
					out.print(ssr+result[i+leng][dptname].trim().substring(2));
				}else{
					out.print(ssr+result[i+leng][dptname].trim());
				}
			}
			%>
			</td>
			
			<td align="right">
			<%if(result[i+leng][money].equals("0")&&result[i+leng][havemoney].equals("N")){out.print("");}
			else if ((result[i+leng][hcur].equals("N"))){out.print("X");}
			else{out.print(nfR.format(Double.parseDouble(result[i+leng][money]))+(result[i+leng][7].equals("百分比")? "%":result[i+leng][7]));}
			%>
			</td>


             <td align="right">
			 <%if(result[i+leng][total].equals("0")&&result[i+leng][havemoney].equals("N")){out.print("");}
			 else{out.print(nfR.format(Double.parseDouble(result[i+leng][total]))+(result[i+leng][7].equals("百分比")? "%":result[i+leng][7]));}
			 %>
			 </td>
             
<%}else if(result.length>=2){%><td></td><td></td><td></td><%}%>
     </tr>
    <%}%>
</html:webprint>
  <%}%>
  <input type="hidden" name="subFunction" value="monrpt">
  <input type="hidden" name="subFunction1" value="account">
  <input type="hidden" name="title" value="">
  <input type="hidden" name="template_code" value="<%=request.getParameter("template_code")%>">
</form>
</html:html>


