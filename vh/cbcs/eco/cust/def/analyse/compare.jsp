<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/cust/def/analyse/compare.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../../error.jsp" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<script language='javascript' src='javascript/chart.js'></script>
<%
   DecimalFormat nf;
   DecimalFormat percentFormat = new DecimalFormat("#0.00%");
   String[][] result = (String[][])request.getAttribute("result");
   String[][] init_title = (String[][])request.getAttribute("init_title");
%>
<Script Language="JavaScript">
  function choose()
  {
    if(template.unit.value=='1'){
      mon1.style.display = "block"
      mon2.style.display = "block"
      ji1.style.display = "none"
      ji2.style.display = "none"
      nian1.style.display = "none"
      nian2.style.display = "none"
      qijian1.style.display = "none"
      qijian2.style.display = "none"
    }
    else if(template.unit.value=='2'){
      mon1.style.display = "none"
      mon2.style.display = "none"
      ji1.style.display = "block"
      ji2.style.display = "block"
      nian1.style.display = "none"
      nian2.style.display = "none"
      qijian1.style.display = "none"
      qijian2.style.display = "none"
    }
    else if(template.unit.value=='3'){
      mon1.style.display = "none"
      mon2.style.display = "none"
      ji1.style.display = "none"
      ji2.style.display = "none"
      nian1.style.display = "block"
      nian2.style.display = "block"
      qijian1.style.display = "none"
      qijian2.style.display = "none"
    }
    else if(template.unit.value=='4'){
      mon1.style.display = "none"
      mon2.style.display = "none"
      ji1.style.display = "none"
      ji2.style.display = "none"
      nian1.style.display = "none"
      nian2.style.display = "none"
      qijian1.style.display = "block"
      qijian2.style.display = "block"
    }
  }
  function analysis(){

    if(template.unit.value=='1' && template.year_month1.value==''){
      alert('请选择报告期')
      return false;
    }
    else if(template.unit.value=='1' && template.year_month2.value==''){
      alert('请选择基期')
      return false;
    }
    else if(template.unit.value=='2' && template.year_quarter1.value==''){
      alert('请选择报告期')
      return false;
    }
    else if(template.unit.value=='2' && template.year_quarter2.value==''){
      alert('请选择基期')
      return false;
    }
    else if(template.unit.value=='3' && template.year1.value==''){
      alert('请选择报告期')
      return false;
    }
    else if(template.unit.value=='3' && template.year2.value==''){
      alert('请选择基期')
      return false;
    }
    else if(template.unit.value=='4' && (template.year_month_from1.value=='' || template.year_month_to1.value=='')){
      alert('请选择报告期')
      return false;
    }
    else if(template.unit.value=='4' && (template.year_month_from2.value=='' || template.year_month_to2.value=='')){
      alert('请选择基期')
      return false;
    }
    if(template.unit.value=='1' && template.year_month1.value<=template.year_month2.value){
      alert('报告期应大于基期!')
      return false;
    }
    if(template.unit.value=='2' && template.year_quarter1.value<=template.year_quarter2.value){
      alert('报告期应大于基期!')
      return false;
    }
    if(template.unit.value=='3' && template.year1.value<=template.year2.value){
      alert('报告期应大于基期!')
      return false;
    }
    if(template.unit.value=='4' && template.year_month_from1.value>=template.year_month_to1.value){
      alert('报告期开始时间应小于结束时间!')
      return false;
    }
    if(template.unit.value=='4' && template.year_month_from2.value>=template.year_month_to2.value){
      alert('基期开始时间应小于结束时间!')
      return false;
    }

    if(template.unit.value=='4' && template.year_month_to1.value<=template.year_month_to2.value){
      alert('报告期结束时间应大于基期结束时间!')
      return false;
    }

    show_wait();
    template.submit()
	}

	function winclose(){
  	window.close();
	}

<%if(result!=null){%>
	var cewolf1="";
<%}%>
</Script>

<html:html clazz="child">
	<form name="template" method="post" action="analyse.jspviewhigh">
	  <html:message/>

	  <table  width="100%" cellspacing="2" border="0" >
	    <tr bgcolor="#FFFFFF">
	      <td >
	        <table  width="100%" cellspacing="2" border="0" >
	          <tr>
	            <td class="signText" nowrap="nowrap">分析单位：</td>
	            <td class="normalText" nowrap="nowrap">
	              <select name="unit" onchange="choose()";>
	                <option value='1' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("1")) out.print("selected");%> >月</option>
	                <option value='2' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("2")) out.print("selected");%>>季</option>
	                <option value='3' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("3")) out.print("selected");%>>年</option>
	                <option value='4' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("4")) out.print("selected");%>>期间</option>
	              </select>
	            </td>
	      			<td align='center'><button class="pageBtn" onclick="return analysis();">分析</button></td>
	      			<td></td>
	      			<!-- <td align='center'><button class="pageBtn" onclick="return saveChart()">保存</button>
	      			<img src="images/saveChart.gif" style="cursor:hand" onclick="return saveChart()"/></td> -->
              <td align='center'><button class="pageBtn" onClick="window.close();" >关闭</button></td>

            </tr>
						<tr>
							<td class="signText" nowrap="nowrap">报告期：</td>
	            <td id="mon1" class="normalText" style='display:block;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month1", request.getParameter("year_month1"))%></td>
	            <td id="ji1" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.QuarterComponent("year_quarter1", request.getParameter("year_quarter1"))%></td>
	            <%String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};%>
	            <td id="nian1" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year1",request.getParameter("year1"),false,false)%>年</td>
	            <td id="qijian1" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from1", request.getParameter("year_month_from1"),"year_month_to1",request.getParameter("year_month_to1"))%></td>
	            <td class="signText" nowrap="nowrap">基期：</td>
	            <td id="mon2" class="normalText" style='display:block;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month2", request.getParameter("year_month2"))%></td>
	            <td id="ji2" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.QuarterComponent("year_quarter2", request.getParameter("year_quarter2"))%></td>
	            <td id="nian2" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year2",request.getParameter("year2"),false,false)%>年</td>
	            <td id="qijian2" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from2", request.getParameter("year_month_from2"),"year_month_to2",request.getParameter("year_month_to2"))%></td>
	          </tr>
	        </table>
<!--
					<table width="100%">
						<tr>
							<td align="left">

						 			<%
	if (result != null){
		ChartUtilities.createChartBar("cewolf1", (double[][])request.getAttribute("value"), (String[])request.getAttribute("rowKeys"),(String[])request.getAttribute("columnKeys"), pageContext);

  %>
		<cewolf:chart id="chart" title="<%=request.getParameter("title")%>" type="horizontalBar3D">
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="750" height="350" id="cewolf1" >
			<cewolf:map tooltipgeneratorid="cewolf1Tip"/>
		</cewolf:img>
    <%}%>
							<td>
						<tr>
					</table>
					-->
	<%
	    if(result!=null){
	%>
<%if(init_title!=null){%>
            <html:title clazz='table'><%=init_title[0][1]%></html:title>
<%}%>
	        <html:table clazz="result"  divWidth="750">
	          <tr>
	            <td class="resultLabel" rowspan=2>名称</td>
	            <td class="resultLabel" rowspan=2>报告期值</td>
	            <td class="resultLabel" rowspan=2>计量单位</td>
	            <td class="resultLabel" colspan="3">与基期比较</td>
	            <td class="resultLabel" colspan="3">与预算比较</td>
	          </tr>
	          <tr>
	            <td class="resultLabel">基期值</td>
	            <td class="resultLabel">差异</td>
	            <td class="resultLabel">差异率</td>
	            <td class="resultLabel">预算值</td>
	            <td class="resultLabel">差异</td>
	            <td class="resultLabel">差异率</td>
	          </tr>
	<%
	       for(int i=0; i<result.length; i++){
         if(Integer.parseInt(result[i][6])==0){
           nf = new DecimalFormat("#,##0");
         }else{
           String zero = new String("0.");
           for(int j=0; j<Integer.parseInt(result[i][6]); j++){
             zero += "0";
           }
           nf = new DecimalFormat("#,##"+zero);
         }
	         String rowColor = "rowGray";
	         if (i/2*2==i) rowColor = "rowWhite";
	%>
	          <tr CLASS="<%=rowColor%>">
	            <td class="normalText"><%=result[i][1]%></td>
	            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][2]))%></td>
	            <td class="normalText"><%=result[i][5]%></td>
	            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][3]))%></td>
	             <td class="numberText"><%=nf.format(Double.parseDouble(result[i][2])-Double.parseDouble(result[i][3]))%></td>
	<%
	  double rate;
	  if(Double.parseDouble(result[i][3])==0)
	    rate=0;
	  else
	    rate=(Double.parseDouble(result[i][2])-Double.parseDouble(result[i][3]))/Double.parseDouble(result[i][3]);
	%>
	            <td class="numberText"><%=percentFormat.format(rate)%></td>
	            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][4]))%></td>
	            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][2])-Double.parseDouble(result[i][4]))%></td>
	<%
	  if(Double.parseDouble(result[i][4])==0)
	    rate=0;
	  else
	    rate=(Double.parseDouble(result[i][2])-Double.parseDouble(result[i][4]))/Double.parseDouble(result[i][4]);
	%>
	            <td class="numberText"><%=percentFormat.format(rate)%></td>
	          </tr>
	<%}%>
	        </html:table>
	    <%}%>
	      </td>
	    </tr>
	  </table>
	  <input type="hidden" name="subFunction" value="compare">
	  <input type="hidden" name="subFunction1" value="account">
	   <input type="hidden" name="title" value="<%=request.getParameter("title")%>">
    <input type="hidden" name="template_code" value="<%=request.getParameter("template_code")%>">
	</form>

	<Script Language="JavaScript">
	 choose()
	</Script>

</html:html>
