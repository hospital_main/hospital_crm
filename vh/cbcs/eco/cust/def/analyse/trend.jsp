<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/cust/def/analyse/trend.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../../../error.jsp" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<script language='javascript' src='javascript/chart.js'></script>
<%
   DecimalFormat nf ;
   String[][] result = (String[][])request.getAttribute("result");
   String[][] ind = (String[][])request.getAttribute("ind");
   String[][] init_title = (String[][])request.getAttribute("init_title");
%>
<Script Language="JavaScript">
  function analysis(){
    if(template.year_month_from.value=='' || template.year_month_to.value=='' || template.year_month_from.value>template.year_month_to.value){
      alert('请选择起止年月')
      return
    }

    show_wait();
    template.submit()
  }
function winclose()
{ window.close();
 }

<%if(result!=null){%>
	var cewolf1="";
<%}%>
</Script>
<html:html clazz="child">
<form name="template" method="post" action="analyse.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <html:table clazz="complex">
    <tr bgcolor="#FFFFFF">
      <td >
        <html:table clazz="complex">
          <tr>
            <td class="signText" nowrap="nowrap">起止年月：</td>
            <td class="normalText" nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%></td>
              <td><button class="pageBtn" onclick="return analysis();">分析</button></td>
              <td align='center'><button class="pageBtn" onclick="return saveChart()">保存</button></td>
              <td><button class="pageBtn" onClick="winclose();" >关闭</button></td>
         </tr>
        </html:table>
<html:table clazz="complex">
<tr>
<td align="center">
<%
	if (result != null){
		ChartUtilities.createChartBar("cewolf1", (double[][])request.getAttribute("dataSet"), (String[])request.getAttribute("rowKey"),(String[])request.getAttribute("colKey"), pageContext);

  %>
		<cewolf:chart id="chart" title="<%=request.getParameter("title")%>" type="line">
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="750" height="350" id="cewolf1" >
			<cewolf:map tooltipgeneratorid="cewolf1Tip"/>
		</cewolf:img>
    <%}%>

 <td>
<tr>
</html:table>
<%
    if(result!=null){
%>
<%if(init_title!=null){%>
          <html:title clazz='table'><%=init_title[0][1]%><%if(init_title[0].length==4) out.print("("+init_title[0][2]+")");%></html:title>
<%}%>
        <html:table clazz="result" divWidth="750">
	        <html:tr clazz='label'>
            <td class="resultLabel" >年</td>
            <td class="resultLabel" >月</td>
<%
for(int i=0; i<ind.length; i++){
%>
            <td class="resultLabel" ><%=ind[i][1]%><%if(ind[i].length==5) out.print("("+ind[i][3]+")");%></td>
<%}%>
          </html:tr>
<%
       for(int j=0; j<result.length; j++){
         String rowColor = "rowGray";
         if (j/2*2==j) rowColor = "rowWhite";
%>
          <tr CLASS="<%=rowColor%>">
<%
if(j==0 || !result[j][0].equals(result[j-1][0])){
%>
            <td align="center" rowspan="<%=result[j][ind.length+2]%>"><%=result[j][0]%></td>
<%}%>
            <td class="normalText"><%=result[j][1]%></td>
<%
  for(int k=0; k<ind.length; k++){
    if(ind[0].length==5){
      if(Integer.parseInt(ind[0][4])==0){
           nf = new DecimalFormat("#,##0");
      }else{
        String zero = new String("0.");
        for(int t=0; t<Integer.parseInt(ind[k][4]); t++){
          zero += "0";
        }
        nf = new DecimalFormat("#,##"+zero);
      }
    }else{
      if(Integer.parseInt(init_title[0][3])==0){
           nf = new DecimalFormat("#,##0");
         }else{
           String zero = new String("0.");
           for(int t=0; t<Integer.parseInt(init_title[0][3]); t++){
             zero += "0";
           }
           nf = new DecimalFormat("#,##"+zero);
         }
    }
    if(result[j][k+2]==null)
      result[j][k+2] = "0";
%>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[j][k+2]))%></td>
<%}%>
          </tr>
<%}%>
        </html:table>
      </td>
    </tr>
    <%}%>
  </html:table>
  <input type="hidden" name="subFunction" value="trend">
  <input type="hidden" name="subFunction1" value="account">
  <input type="hidden" name="title" value="<%=request.getParameter("title")%>">
  <input type="hidden" name="template_code" value="<%=request.getParameter("template_code")%>">
</form>
</html:html>


