<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/cust/def/analyse/structure.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../../error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="java.text.*" %>
<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<script language='javascript' src='javascript/chart.js'></script>
<%
   DecimalFormat nf;
   String[][] result = (String[][])request.getAttribute("result");
   String[][] init_title = (String[][])request.getAttribute("init_title");
%>
<Script Language="JavaScript">
  function choose()
  {
    if(template.unit.value=='1'){
      mon.style.display = "block"
      ji.style.display = "none"
      nian.style.display = "none"
      qijian.style.display = "none"
    }
    else if(template.unit.value=='2'){
      mon.style.display = "none"
      ji.style.display = "block"
      nian.style.display = "none"
      qijian.style.display = "none"
    }
    else if(template.unit.value=='3'){
      mon.style.display = "none"
      ji.style.display = "none"
      nian.style.display = "block"
      qijian.style.display = "none"
    }
    else if(template.unit.value=='4'){
      mon.style.display = "none"
      ji.style.display = "none"
      nian.style.display = "none"
      qijian.style.display = "block"
    }
  }
  function analysis(){
    if(template.unit.value=='1' && template.year_month.value==''){
      alert('请选择时间点')
      return
    }
    else if(template.unit.value=='2' && template.year_quarter.value==''){
      alert('请选择时间点')
      return
    }
    else if(template.unit.value=='3' && template.year.value==''){
      alert('请选择时间点')
      return
    }
    else if(template.unit.value=='4' && (template.year_month_from.value=='' || template.year_month_to.value=='')){
      alert('请选择时间点')
      return
    }

    show_wait();
    template.submit()
  }
function winclose()
{ window.close();
 }

<%if(result!=null){%>
	var cewolf1="";
<%}%>
</Script>
<html:html clazz="child">
<form name="template" method="post" action="analyse.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <html:table clazz="complex">
    <tr bgcolor="#FFFFFF">
      <td >
        <html:table clazz="complex">
          <tr>
            <td class="signText" nowrap="nowrap">分析单位：</td>
            <td class="normalText" nowrap="nowrap">
              <select name="unit" onchange="choose()";>
                <option value='1' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("1")) out.print("selected");%> >月</option>
                <option value='2' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("2")) out.print("selected");%>>季</option>
                <option value='3' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("3")) out.print("selected");%>>年</option>
                <option value='4' <%if(request.getParameter("unit")!=null && request.getParameter("unit").equals("4")) out.print("selected");%>>期间</option>
              </select>
            </td>
            <td class="signText" nowrap="nowrap">时间点：</td>
            <td id="mon" class="normalText" style='display:block;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
            <td id="ji" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.QuarterComponent("year_quarter", request.getParameter("year_quarter"))%></td>
							<%String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};%>
            <td id="nian" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year",request.getParameter("year"),false,false)%>年</td>
            <td id="qijian" class="normalText" style='display:none;' nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%></td>
          </tr>
					<tr><td><br></td></tr>
          <tr>
            <td align='center' colspan='2'><button class="pageBtn" onclick="return analysis();">分析</button></td>
            <td align='center'><button class="pageBtn" onclick="return saveChart()">保存</button></td>
            <td align='center' colspan='2'><button class="pageBtn" onClick="winclose();" >关闭</button></td>
          </tr>
        </html:table>
<html:table clazz="complex">
<tr>
<td align="left">
  <%
	if (result != null){
		ChartUtilities.createChartPie("cewolf1",(String[])request.getAttribute("name"), (double[])request.getAttribute("value"),  pageContext);

  %>
		<cewolf:chart id="chart" title="<%=request.getParameter("title")%>" type="pie3D">
	    <cewolf:colorpaint color="#EEEEFF"/>
	    <cewolf:data>
	        <cewolf:producer id="cewolf1"/>
	    </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="750" height="350" id="cewolf1" >
			<cewolf:map tooltipgeneratorid="cewolf1Tip"/>
		</cewolf:img>
    <%}%>

 <td>
<tr>
</html:table>
<%
    if(result!=null){
%>
<%if(init_title!=null){%>
          <html:title clazz='table'><%=init_title[0][1]%></html:title>
<%}%>
        <html:table clazz="result" divWidth="750">
	        <html:tr clazz='label'>
            <td class="resultLabel" >名称</td>
            <td class="resultLabel" >本期值</td>
            <td class="resultLabel" >计量单位</td>
         </html:tr>
<%
       for(int i=0; i<result.length; i++){
         if(Integer.parseInt(result[i][4])==0){
           nf = new DecimalFormat("#,##0");
         }else{
           String zero = new String("0.");
           for(int j=0; j<Integer.parseInt(result[i][4]); j++){
             zero += "0";
           }
           nf = new DecimalFormat("#,##"+zero);
         }
         String rowColor = "rowGray";
         if (i/2*2==i) rowColor = "rowWhite";
%>
          <tr CLASS="<%=rowColor%>">
            <td class="normalText"><%=result[i][2]%></td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][1]))%></td>
            <td class="normalText"><%=result[i][3]%></td>
          </tr>
<%}%>
         </html:table>
<%}%>
      </td>
    </tr>

   </html:table>
  <input type="hidden" name="subFunction" value="structure">
  <input type="hidden" name="subFunction1" value="account">
    <input type="hidden" name="title" value="<%=request.getParameter("title")%>">
 <input type="hidden" name="template_code" value="<%=request.getParameter("template_code")%>">
</form>
</html:html>
<Script Language="JavaScript">

 choose()
</Script>


