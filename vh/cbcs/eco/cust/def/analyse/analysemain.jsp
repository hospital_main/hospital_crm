<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/cust/def/analyse/analysemain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:11 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../../error.jsp" %>

<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function create() {
        template.subFunction.value='preparedCreate';
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }
function directional(primaryKey,title)
{
  window.open("analyse.jspviewhigh?subFunction1=kind&title="+title+"&template_code="+primaryKey,'child',"toolbar=no, resizable=yes, scrollbars=yes, location=no, width=800, height=580, top=100, left=100")
 }
  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }
  function find() {
    template.subFunction1.value='find';
    template.submit();return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="analyse.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>管理分析主页面</html:title>
  <!-- 查询信息 -->
    <%
      BaseRO  ro= (BaseRO)request.getAttribute("ro");
      String[][] result=null;
      if(ro!=null)
       result=ro.getTableResult();
      TableMarge oper = new TableMarge(ro, "return find()");
    %>
  <html:table clazz="simple">
    <tr>

      <td nowrap class="signText">模板名称：</td>
      <td nowrap class="normalText"><input type="text" name="template_name" size="20" value='<%=request.getParameter("template_name")==null?"":request.getParameter("template_name")%>'/></td>
      <td nowrap class="signText">分析方法：</td>
      <td nowrap class="normalText"><input type="text" name="analysis_mtd" size="20" value='<%=request.getParameter("analysis_mtd")==null?"":request.getParameter("analysis_mtd")%>'/></td>
      <td>
      <button class="pageBtn" name=""  onclick="return find();" >查询</button>
      <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
		</tr>
  </html:table>

  <html:title clazz='table'>管理分析定义</html:title>
  <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>
      <html:table clazz="result">
     <html:tr clazz='label'>
          <td >模板名称</td>
          <td >分析方法</td>

        </html:tr>

        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {

                String primaryKey = result[i][0];
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j]=="") result[i][j]="&nbsp;";
                }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText"><a href="#" onclick="directional('<%=primaryKey%>','<%=result[i][1]%>')"><%=result[i][1]%></a></td>
          <td class="normalText"><%=result[ i ][ 2]%></td>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>

  </html:table>
  <input type=hidden name="subFunction1"/>
</form>
</html:html>


