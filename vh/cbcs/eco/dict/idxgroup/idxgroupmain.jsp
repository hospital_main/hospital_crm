<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/dict/idxgroup/idxgroupmain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                 com.viewhigh.cbcs.base.mvc.view.component.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
</Script>
<%
  String[][] group_grade={{"1","1级"},{"2","2级"},{"3","3级"},{"4","4级"},{"5","6级"}};
  String[][] stop_mark={{"N","否"},{"Y","是"}};
  String[][] last_level={{"N","否"},{"Y","是"}};
%>
<html:html clazz="main">
	<form name="template" method="post" action="ecoDixGroup.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>指标组主页面</html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
	    <tr>
	      <td nowrap class="signText">指标组代码：</td>
	        <% String idx_group_code = request.getParameter("idx_group_code");%>
	      <td class="normalText">
          <input type='text' name="idx_group_code" class="textInputC" <%if(idx_group_code != null){ out.println(" value=" + idx_group_code);}%>>
        </td>
	      <td nowrap class="signText">指标组名称：</td>
        <% String idx_group_name = request.getParameter("idx_group_name");%>
	      <td class="normalText">
          <input type='text' name="idx_group_name"  class="textInputC" <%if(idx_group_name != null){ out.println(" value=" + idx_group_name);}%>>
	      </td>
      <td nowrap class="signText" >指标组级别：</td>
      <td>
         <%=new SingleSelect(group_grade, "group_grade", request.getParameter("group_grade"), false, false)%>
      </td>
      </tr>
      <tr>
      <td nowrap class="signText" >是否停用：</td>
      <td>
         <%=new SingleSelect(stop_mark, "stop_mark", request.getParameter("stop_mark"), false, false)%>
      </td>
      <td nowrap class="signText" >是否末级：</td>
      <td>
         <%=new SingleSelect(last_level, "last_level", request.getParameter("last_level"), false, false)%>
      </td>
        <td>
        <button class="pageBtn" name=""   onclick="find()"  >查询</button>
        <!-- <img src="images/find.gif" class="mouse" onclick="find()" />--></td>
	    </tr>
	  </html:table>

	  <br>

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
		<html:title clazz='table'>指标组定义</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr>

		  <!-- 结果集 -->
		  <tr>
		    <td>
		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td>选择</td>
		          <td>指标组代码</td>
		          <td>指标组名称名称</td>
              <td>上级指标组代码</td>
              <td>指标组级别</td>
              <td>末级标志</td>
              <td>停用标志</td>
		        </html:tr>

		        <%if (ro!=null) {
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>

		        <tr CLASS="<%=rowColor%>">
		          <td><input type="checkbox" name='primaryKey' value="<%=primaryKey%>"></td>
		          <td class="normalText"><a href="ecoDixGroup.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>"><%=primaryKey%></a></td>
              <td class="normalText"><%=result[i][1]%></td>
              <td class="normalText"><%=result[i][2]%></td>
              <td class="normalText"><%=result[i][3]%></td>
              <td class="normalText"><%=result[i][4]%></td>
              <td class="normalText"><%=result[i][5]%></td>
            </tr>

		        <%
		              }
		            }
		        }
		        %>
		      </html:table>
		    </td>
		  </tr>

		  <!-- 操作 -->
	  </html:table>
	  <input type='hidden' name="subFunction"/>
	</form>
</html:html>


