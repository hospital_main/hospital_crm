<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/dict/idxgroup/idxgroupcreate.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Modtime: 03-09-02 10:08 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
    if(isEmpty(template.idx_group_code))
    {
      alert('指标组代码不能为空!');
      return;
    }
    if(isEmpty(template.idx_group_name))
    {
      alert('指标组名称不能为空!');
      return;
    }
    if(isTooLong(template.idx_group_code,6))
    {
      alert('指标组代码不能高于6个字符!');
      return;
    }
    if(isTooLong(template.idx_group_name,20))
    {
      alert('指标组名称不能高于20个字符!');
      return;
    }
    if(template.idx_group_code.value==template.supper_idex_group.value){
      alert('指标组代码不能与上级指标组代码相同!');
      return;
    }
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post"  action="ecoDixGroup.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>指标组添加页面</html:title>
  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText">指标组代码：</td>
      <td class="normalText">
          <input type='text' name="idx_group_code" class="textInputC">
      </td>
    </tr>
    <tr>
     <td nowrap class="signText">指标组名称：</td>
     <td class="normalText">
       <input type='text' name="idx_group_name"  class="textInputC">
     </td>
    </tr>
    <tr>
      <td nowrap class="signText">上级指标组代码：</td>
      <td class="normalText">
        <input type='text' name="supper_idex_group"  class="textInputC">
      </td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button><button class="pageBtn" onclick="return back(template);">返回</button>   
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </html:table>
  <input type='hidden' name="subFunction" value="create"/>
</form>
</html:html>
