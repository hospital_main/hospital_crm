<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/dict/dixgroupmap/dixGroupMapCreate.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Modtime: 03-09-02 10:08 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<script language='javascript' src='javascript/tag.js'></script>
<Script Language="JavaScript">
  function create()
  {
    if(template.idx_type[0].checked==true){
     if(template.idx1.length<=0){
        alert("全院指标不能为空");
        return false;
     }
   }else if(template.idx_type[1].checked==true){
     if(template.idx2.length<=0){
        alert("科室指标不能为空");
        return false;
     }
   }else if(template.idx_type[2].checked==true){
     if(template.idx3.length<=0){
        alert("项目指标不能为空");
        return false;
     }
   }else{
     if(template.idx4.length<=0){
        alert("病种指标不能为空");
        return false;
     }
   }
    template.submit();
    return true;
  }
  function show()
{       yard_idx.style.display = "block"
        dept_idx.style.display = "none"
        subj_idx.style.display = "none"
        disease_idx.style.display = "none"
 }
function show1()
{       yard_idx.style.display = "none"
        dept_idx.style.display = "block"
        subj_idx.style.display = "none"
        disease_idx.style.display = "none"
 }
  function show2()
{       yard_idx.style.display = "none"
        dept_idx.style.display = "none"
        subj_idx.style.display = "block"
        disease_idx.style.display = "none"
 }
 function show3()
{       yard_idx.style.display = "none"
        dept_idx.style.display = "none"
        subj_idx.style.display = "none"
        disease_idx.style.display = "block"
 }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
</Script>
<%
   String[][] idx_group_name=(String[][])request.getAttribute("idx_group_name");
    String[][] idx_type={{"A","全院指标"},{"B","科室指标"},{"C","项目指标"},{"D","病种指标"}};
%>
<html:html clazz="main">
<form name="template" method="post"  action="ecoDixGroupMap.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>
  <!-- 标题栏 -->
  <html:title clazz='module'>指标分组添加页面</html:title>
  <!-- 简单信息 -->
  <table width='100%' cellspacing='6' border='0' class='normalText'>
    <tr>
      <td class="signText" nowrap="nowrap">指标组代码：</td>
      <td nowrap="nowrap">
        <%=new SingleSelect(idx_group_name, "idx_group_name", request.getParameter("idx_group_name"), true, true)%>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">指标类别：</td>
      <td>
         <input type="radio" name='idx_type' value='A' onclick="show()" checked >全院指标
         <input type="radio" name='idx_type' value='B' onclick="show1()">科室指标
         <input type="radio" name='idx_type' value='C' onclick="show2()">项目指标
         <input type="radio" name='idx_type' value='D' onclick="show3()">病种指标
      </td>
    </tr>
    <tr id='yard_idx' style="display:block">
      <td class="signText" nowrap="nowrap">全院指标名称：</td>
      <td  class="normalText" nowrap="nowrap">
         <html:select property="eco_dict_dixgroupmap_whole"    showBefValue="false" name='idx1' size="10" biSelect="true" showTip="false"/>
      </td>
    </tr>
    <tr id='dept_idx' style="display:none">
       <td class="signText" nowrap="nowrap">科室指标名称：</td>
       <td  class="normalText" nowrap="nowrap" >
         <html:select property="eco_dict_dixgroupmap_dept"  showBefValue="false" name='idx2' size="10" biSelect="true" showTip="false"/>
       </td>
    </tr>
    <tr id='subj_idx' style="display:none">
      <td class="signText" nowrap="nowrap">项目指标名称：</td>
      <td  class="normalText" nowrap="nowrap" >
        <html:select property="eco_dict_dixgroupmap_subj" showBefValue="false"  name='idx3' size="10" biSelect="true" showTip="false"/>
      </td>
    </tr>
    <tr id='disease_idx' style="display:none">
      <td class="signText" nowrap="nowrap">病种指标名称：</td>
      <td  class="normalText" nowrap="nowrap" >
        <html:select property="eco_dict_dixgroupmap_disease" showBefValue="false" name='idx4' size="10" biSelect="true" showTip="false"/>
      </td>
     </tr>
    <tr>
      <td colspan="2"> 
      <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button><button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type='hidden' name="subFunction" value="create"/>
</form>
</html:html>
