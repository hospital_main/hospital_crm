<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/dict/idxudef/idxudefmain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Modtime:  $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                 com.viewhigh.cbcs.base.util.ExtendTool" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey' && template.elements[i].disabled=='false')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="ecoDixUDef.jspviewhigh">
	  <html:message/>
	  <html:title clazz='module'>指标定义</html:title>
	  <!-- 简单信息 -->
	  <html:table clazz="simple">
	    <tr>
	      <td nowrap class="normalText">指标代码：</td>
	        <% String idx_code = (String)request.getAttribute("idx_code");
	        //System.out.println(idx_code);
	        %>
	        
	      <td class="normalText">
          <input type='text' name="idx_code" class="textInputC" <%if(idx_code != null){ out.println(" value=" + idx_code);}%>>
        </td>
	      <td nowrap class="normalText">指标名称：</td>
          <% String idx_name = (String)request.getAttribute("idx_name");%>
	      <td class="normalText">
          <input type='text' name="idx_name"  class="textInputC" <%if(idx_name != null){ out.println(" value=" + idx_name);}%>>
	      </td>
      </tr>
      <tr>
        <td nowrap class="normalText">指标类别</td>
	        <% String idx_type = (String)request.getAttribute("idx_type");%>
	      <td class="normalText">
          <select name="idx_type">
                  <option value="A" <%if(idx_type.equals("A")){ out.println("selected");}%>>全院指标</option>
                  <option value="B" <%if(idx_type.equals("B")){ out.println("selected");}%>>科室指标</option>
                  <option value="C" <%if(idx_type.equals("C")){ out.println("selected");}%>>项目指标</option>
                  <option value="D" <%if(idx_type.equals("D")){ out.println("selected");}%>>病种指标</option>
          </select>
        </td>
	      <td nowrap class="normalText">指标类型</td>
          <% String idx_type_code = (String)request.getAttribute("idx_type_code");%>
	      <td class="normalText">
          <select name="idx_type_code">
                  <option value="B" <%if(idx_type_code.equals("B")){ out.println("selected");}%>>基本指标</option>
                  <option value="D" <%if(idx_type_code.equals("D")){ out.println("selected");}%>>派生指标</option>
          </select>
	      </td>
        <td> 
        <button class="pageBtn" name=""   onclick="find()"  >查询</button>
        <!--<img src="images/find.gif" class="mouse" onclick="find()" />--></td>
	    </tr>
	  </html:table>

	  <br>

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
		<html:title clazz='table'>指标自定义字典</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr>

		  <!-- 结果集 -->
		  <tr>
		    <td>
		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td>选择</td>
		          <td>指标编码</td>
		          <td>指标名称</td>
              <td>指标类别</td>
		          <td>计量单位</td>
              <td>精确度</td>
              <td>是否必填</td>
              <td>采集方式</td>
		        </html:tr>

		        <%if (ro!=null) {
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[i][0];
		              String  sysdef = result[i][5];		              
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>

		        <tr CLASS="<%=rowColor%>">
		          <td><input type="checkbox" name='primaryKey' value="<%=primaryKey%>" <%if(sysdef.equals("Y")) out.print("disabled");%> ></td>		          
		          <td class="normalText">
		          	<%if(sysdef.equals("Y")){
		          			out.print(result[i][0]);
		          		}else{
			          	%>
			          		<a href="ecoDixUDef.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>"><%=result[i][0]%></a>
			          	<%
			          	}
			          	%>
		          	
		          </td>
		          
              <td class="normalText"><%=result[i][1]%></td>
              <td class="normalText"><%=result[i][2]%></td>
              <td class="normalText"><%=result[i][3]%></td>
              <td class="normalText"><%=result[i][4]%></td>
              <td class="normalText"><%=result[i][6].equals("Y")?"是":"否" %></td>
              <td class="normalText"><%=result[i][7].equals("1")?"自动":"录入" %></td>
            </tr>

		        <%
		              }
		            }
		        }
		        %>
		      </html:table>
		    </td>
		  </tr>

		  <!-- 操作 -->
	  </html:table>
	  <input type="hidden" name="subFunction"/>
	</form>
</html:html>


