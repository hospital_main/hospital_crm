<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/dict/idxudef/idxudefsave.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Modtime: $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
   String[][] idxlist1 =(String[][])request.getAttribute("idx1");
   String[][] idxlist2 =(String[][])request.getAttribute("idx2");
   String[][] idxlist3 =(String[][])request.getAttribute("idx3");
   String[][] idxlist4 =(String[][])request.getAttribute("idx4");
   
   String[] idxitem =(String[])request.getAttribute("result");   
   
%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<script language='javascript' src='javascript/tag.js'></script>
<style type="text/css">
<!--
.list {
	font-size: 12px;
	width: 200px;
	height: 100px;
	border: thin none #0099CC;
}
-->
</style>
<Script Language="JavaScript">
	
  function create()
  {   	
  	//alert(checkformula());
  	if(template.idx_code.value == '')
  	{
  			alert("指标编号不能为空");
  			return false;
  	}
  	else if(template.idx_code.value.length <= 1){
  			alert("指标编号编号有误");
  			return false;  	
  	}
  	else if(template.formula.value == ''&& template.type_code.value =='D')
  	{
  			alert("公式不能为空");
  			return false;
  	}
  	else if(template.type_code.value =='D'){
	  	if(checkformula()=='Infinity'){
	  		alert("公式被 0 除错误！请检测！")
	  		return false;  	
	  	}  	
	  	else if(!checkformula()){
	  		alert("公式有误！请检测！")
	  		return false;
	  	}
	  	else
	  	{
		    template.submit();
		    show_wait();
		    return true;
	    }	  	
  	}
  	else
  	{
	    template.submit();
	    show_wait();
	    return true;
    }
  }
  
  function checkformula() //检测公式的正确性
  {
  	var formula = template.formula.value;
  	var dispobj;
  	var result;
  	if(template.idx_type.value=='A')
  		dispobj = eval("template.idxlist1");
  	else if(template.idx_type.value=='B')
  		dispobj = eval("template.idxlist2");
  	else if(template.idx_type.value=='C')
  		dispobj = eval("template.idxlist3");
  	else if(template.idx_type.value=='D')
  		dispobj = eval("template.idxlist4");  	
  		
		for(i=0;i<dispobj.length;i++)
		{
			var temp = '/('+dispobj.options(i).value+')/g';
			var temp2 = Math.round((Math.random()+1)*10);			
			formula = formula.replace(eval(temp),temp2);
		}
		//alert(formula);
		try{
			if((eval(formula))=='Infinity') //被0除
				return 'Infinity';
			else if(isNaN(eval(formula)))
				return false;
			else
				return true;
		}catch(exception){
				return false;
		}
					
  }
  
	function show(idn)
	{
	if(template.type_code.value =='D'){
		if(idn =='A')
		{     yard_idx.style.display = "block"
	        dept_idx.style.display = "none"
	        subj_idx.style.display = "none"
	        disease_idx.style.display = "none"
	  }else if(idn =='B')
		{     yard_idx.style.display = "none"
		      dept_idx.style.display = "block"
		      subj_idx.style.display = "none"
		      disease_idx.style.display = "none"
		}else if(idn =='C')
	  {     yard_idx.style.display = "none"
	        dept_idx.style.display = "none"
	        subj_idx.style.display = "block"
	        disease_idx.style.display = "none"
	  }else if (idn =='D')
	  {     yard_idx.style.display = "none"
	        dept_idx.style.display = "none"
	        subj_idx.style.display = "none"
	        disease_idx.style.display = "block"
	  }
	 }		
 }
 
	function show2(code)
	{
	if(code =='D'){
		//template.idx_code.value = 'D'
	 	var typev = template.idx_type.value;
	 	fm.style.display = "block";
		if(typev =='A')
		{     yard_idx.style.display = "block";
	        dept_idx.style.display = "none";
	        subj_idx.style.display = "none";
	        disease_idx.style.display = "none";
	  }else if(typev =='B')
		{     yard_idx.style.display = "none";
		      dept_idx.style.display = "block";
		      subj_idx.style.display = "none";
		      disease_idx.style.display = "none";
		}else if(typev =='C')
	  {     yard_idx.style.display = "none";
	        dept_idx.style.display = "none";
	        subj_idx.style.display = "block";
	        disease_idx.style.display = "none";
	  }else if (typev =='D')
	  {     yard_idx.style.display = "none";
	        dept_idx.style.display = "none";
	        subj_idx.style.display = "none";
	        disease_idx.style.display = "block";
	  }
	 }else{
	 			//template.idx_code.value = 'B';
	      yard_idx.style.display = "none";
	      dept_idx.style.display = "none";
	      subj_idx.style.display = "none";
	      disease_idx.style.display = "none";
	      fm.style.display = "none";
	 }		
 } 
 
 function getItem(select_obj)
 {
 	template.formula.value = template.formula.value + select_obj.value;
 }
 function oper(flag){
 	template.formula.value = template.formula.value + flag.value;
 } 
 
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
 

</Script>
<html:html clazz="main">
<form name="template" method="post"  action="ecoDixUDef.jspviewhigh">
<body>
  <!-- 信息提示栏 -->
  <html:message/>
  <!-- 标题栏 -->
  <html:title clazz='module'>指标修改</html:title>
  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td width=4>
      <td><table class="normaltext" cellspacing="2" width="100%" border="0" >          
            <tr>
              <td width="18%" noWrap><div align="right">指标类别：</div></td>
              <td width="27%"><select class="selectBg" name="idx_type" onchange="show(this.options[this.selectedIndex].value)">
                  <option value="A" <%if(idxitem[0].equals("A")){ out.println("selected");}%>>全院指标</option>
                  <option value="B" <%if(idxitem[0].equals("B")){ out.println("selected");}%>>科室指标</option>
                  <option value="C" <%if(idxitem[0].equals("C")){ out.println("selected");}%>>项目指标</option>
                  <option value="D" <%if(idxitem[0].equals("D")){ out.println("selected");}%>>病种指标</option>
              </SELECT></td>
              <td width="18%" noWrap><div align="right">计量单位：</div></td>
              <td width="37%">
              	<%=new SingleSelect(request.getAttribute("unit_code"), "unit_code", request.getParameter("unit_code"), true, true)%>
              </td>
            </TR>
            <tr>
              <td noWrap><div align="right">指标类型：</div></td>
              <td><select class="selectBg" name="type_code" onchange="show2(this.options[this.selectedIndex].value);">
              <%
              	if(idxitem[2].equals("D")){ 
              		out.println("<option value='D' selected>派生指标</option>");
              	}else{
              		out.println("<option value='B' selected>基本指标</option>");
              	}
              
              %>
              </select></td>
              <td noWrap><div align="right">小数点后保持几位：</div></td>
              <td><INPUT class="textInputC" name="prec" size="4"  maxlength="1" value='<%=idxitem[3]%>'></td>
            </TR>
            <tr>
              <td width="18%" noWrap class="normalText"><p align="right">指标编号：</p>
              </td>
              <td width="27%"><INPUT class="textInputC" name="idx_code1" maxlength="10" value='<%=idxitem[4]%>' disabled ></td>
              <td><div align="right">是否计算合计：</div></td>
              <td><select class="selectBg" name="is_sum_month">
                  <option value="Y" <%if(idxitem[5].equals("Y")){ out.println("selected");}%>>是</option>
                  <option value="N" <%if(idxitem[5].equals("N")){ out.println("selected");}%>>否</option>
              </select></td>
            </TR>
            <tr>
              <td class="normalText" noWrap><div align="right">指标名称：</div></td>
              <td><INPUT class="textInputC" name="idx_name" maxlength="40" value='<%=idxitem[6]%>'></td>
              <td class="normalText" noWrap>&nbsp;</td>
              <td>&nbsp;</td>
            </TR>
            <tr>
              <td width="18%" height="51" noWrap><div align="right">指标说明：</div></td>
              <td colspan="3"><textarea name="evaluation" cols="60" rows="3"><%=idxitem[7]%></textarea></td>
            </TR>
            <TR id="fm" style="display:none">
              <td noWrap><div align="right">计算公式：</div></td>
              <td colspan="3">
              <p>
                <textarea name="formula" cols="60" rows="3"><%=idxitem[8]%></textarea>
              </p></td>
            </TR>
            <tr>
              <td noWrap><div align="right">是否必填：</div></td>
              <td><select class="selectBg" name="if_must">
                  <option value="Y" <%System.out.println(idxitem.length);if(idxitem[9].equals("Y")){ out.println("selected");}%>>是</option>
                  <option value="N" <%if(idxitem[9].equals("N")){ out.println("selected");}%>>否</option>
              </select></td>
              <td noWrap><div align="right">采集方式：</div></td>
              <td><select class="selectBg" name="if_method">
                  <option value="1" <%if(idxitem[10].equals("1")){ out.println("selected");}%>>自动</option>
                  <option value="2" <%if(idxitem[10].equals("2")){ out.println("selected");}%>>录入</option>
              </select></td>

            </tr>            
				    <tr id='yard_idx' style="display:none">
				      <td align="right">全院指标名称：</td>
				      <td>
						  <select name="idxlist1" size="4" class="list" onDblClick="getItem(this)">
<%
              

if(idxlist1 != null && idxlist1.length>0)
for(int i =0;i<idxlist1.length;i++)
{
	out.print("<option value='"+idxlist1[i][1]+"'>"+idxlist1[i][1]+":"+idxlist1[i][2]+"</option>");
}
%>				         
				         </select>
				      </td>
				      <td colspan="2" valign="top">
								<table width="135" height="82" border="1">
								    <tr> 
								      <td width="45"><div align="center"> 
								          <input name="+" type="button" id="+" value=" + " onclick="oper(this)">
								        </div></td>
								      <td width="45"><div align="center"> 
								          <input name="-" type="button" id="-" value=" - " onclick="oper(this)">
								        </div></td>
								      <td width="45"><div align="center"> 
								          <input name="*" type="button" id="*" value=" * " onclick="oper(this)">
								        </div></td>
								    </tr>
								    <tr> 
								      <td><div align="center"> 
								          <input name="/" type="button" id="/" value=" / " onclick="oper(this)">
								        </div></td>
								      <td><div align="center"> 
								          <input name="(" type="button" id="(" value=" ( " onclick="oper(this)">
								        </div></td>
								      <td><div align="center"> 
								          <input name=")" type="button" id=")" value=" ) " onclick="oper(this)">
								        </div></td>
								    </tr>
								  </table>
  				      </td>
						</tr>
				    <tr id='dept_idx' style="display:none">
				       <td align="right">科室指标名称：</td>
				       <td>
						  <select name="idxlist2" size="4" class="list" onDblClick="getItem(this)">
<%
if(idxlist2 != null && idxlist1.length>0)
for(int i =0;i<idxlist2.length;i++)
{
	out.print("<option value='"+idxlist2[i][1]+"'>"+idxlist2[i][1]+":"+idxlist2[i][2]+"</option>");
}
%>				         
				         </select>
				       </td>
				      <td colspan="2" valign="top">
								<table width="135" height="82" border="1">
								    <tr> 
								      <td width="45"><div align="center"> 
								          <input name="+" type="button" id="+" value=" + " onclick="oper(this)">
								        </div></td>
								      <td width="45"><div align="center"> 
								          <input name="-" type="button" id="-" value=" - " onclick="oper(this)">
								        </div></td>
								      <td width="45"><div align="center"> 
								          <input name="*" type="button" id="*" value=" * " onclick="oper(this)">
								        </div></td>
								    </tr>
								    <tr> 
								      <td><div align="center"> 
								          <input name="/" type="button" id="/" value=" / " onclick="oper(this)">
								        </div></td>
								      <td><div align="center"> 
								          <input name="(" type="button" id="(" value=" ( " onclick="oper(this)">
								        </div></td>
								      <td><div align="center"> 
								          <input name=")" type="button" id=")" value=" ) " onclick="oper(this)">
								        </div></td>
								    </tr>
								  </table>
  				      </td>
				    </tr>
				    <tr id='subj_idx' style="display:none">
				      <td align="right">项目指标名称：</td>
				      <td>
						  <select name="idxlist3" size="4" class="list" onDblClick="getItem(this)">
<%
if(idxlist3 != null && idxlist1.length>0)
for(int i =0;i<idxlist3.length;i++)
{
	out.print("<option value='"+idxlist3[i][1]+"'>"+idxlist3[i][1]+":"+idxlist3[i][2]+"</option>");
}
%>				         
				         </select>
				      </td>
				      <td colspan="2" valign="top">
								<table width="135" height="82" border="1">
								    <tr> 
								      <td width="45"><div align="center"> 
								          <input name="+" type="button" id="+" value=" + " onclick="oper(this)">
								        </div></td>
								      <td width="45"><div align="center"> 
								          <input name="-" type="button" id="-" value=" - " onclick="oper(this)">
								        </div></td>
								      <td width="45"><div align="center"> 
								          <input name="*" type="button" id="*" value=" * " onclick="oper(this)">
								        </div></td>
								    </tr>
								    <tr> 
								      <td><div align="center"> 
								          <input name="/" type="button" id="/" value=" / " onclick="oper(this)">
								        </div></td>
								      <td><div align="center"> 
								          <input name="(" type="button" id="(" value=" ( " onclick="oper(this)">
								        </div></td>
								      <td><div align="center"> 
								          <input name=")" type="button" id=")" value=" ) " onclick="oper(this)">
								        </div></td>
								    </tr>
								  </table>
  				      </td>
				    </tr>
				    <tr id='disease_idx' style="display:none">
				      <td align="right">病种指标名称：</td>
				      <td>
						  <select name="idxlist4" size="4" class="list" onDblClick="getItem(this)">
<%
if(idxlist4 != null && idxlist1.length>0)
for(int i =0;i<idxlist4.length;i++)
{
	out.print("<option value='"+idxlist4[i][1]+"'>"+idxlist4[i][1]+":"+idxlist4[i][2]+"</option>");
}
%>				         
				         </select>
				      </td>
				      <td colspan="2" valign="top">
								<table width="135" height="82" border="1">
								    <tr> 
								      <td width="45"><div align="center"> 
								          <input name="+" type="button" id="+" value=" + " onclick="oper(this)">
								        </div></td>
								      <td width="45"><div align="center"> 
								          <input name="-" type="button" id="-" value=" - " onclick="oper(this)">
								        </div></td>
								      <td width="45"><div align="center"> 
								          <input name="*" type="button" id="*" value=" * " onclick="oper(this)">
								        </div></td>
								    </tr>
								    <tr> 
								      <td><div align="center"> 
								          <input name="/" type="button" id="/" value=" / " onclick="oper(this)">
								        </div></td>
								      <td><div align="center"> 
								          <input name="(" type="button" id="(" value=" ( " onclick="oper(this)">
								        </div></td>
								      <td><div align="center"> 
								          <input name=")" type="button" id=")" value=" ) " onclick="oper(this)">
								        </div></td>
								    </tr>
								  </table>
  				      </td>
				     </tr>            
            <tr>
              <td class="normalText">&nbsp;</td>
              <td class="normalText" colspan="3"><button class="pageBtn" onclick="return create();">保存</button>
              <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return back(template);">返回</button> 
              <!--<img src="images/save.gif" class="mouse"  onclick="return create();"/> 
              <img src="images/reset.gif" class="mouse" onclick="return reset();" />
              <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --> </td>
              <input type='hidden' name="subFunction" value="save"/></td>
              <input type='hidden' name="idx_code" value="<%=idxitem[4]%>"/></td>
            </TR>
            <tr>
			<td height="300"></td>
			</tr>
      </TABLE></td>
    </TR>  
  </html:table>
</body>
</html:html>

<Script Language="JavaScript">  	 
 	 var typev = template.idx_type.value;
 	 if(template.type_code.value =='D'){ 	 	
 	 		fm.style.display = "block";
			if(typev =='A')
			{     yard_idx.style.display = "block";
		        dept_idx.style.display = "none";
		        subj_idx.style.display = "none";
		        disease_idx.style.display = "none";
		  }else if(typev =='B')
			{     yard_idx.style.display = "none";
			      dept_idx.style.display = "block";
			      subj_idx.style.display = "none";
			      disease_idx.style.display = "none";
			}else if(typev =='C')
		  {     yard_idx.style.display = "none";
		        dept_idx.style.display = "none";
		        subj_idx.style.display = "block";
		        disease_idx.style.display = "none";
		  }else if (typev =='D')
		  {     yard_idx.style.display = "none";
		        dept_idx.style.display = "none";
		        subj_idx.style.display = "none";
		        disease_idx.style.display = "block";
		  }
	}else{			
      yard_idx.style.display = "none";
      dept_idx.style.display = "none";
      subj_idx.style.display = "none";
      disease_idx.style.display = "none";
      fm.style.display = "none";
	}
 </Script>