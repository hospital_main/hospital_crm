<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/itemanalysis/itemcost/itemcost.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:58:11 $
  $Modtime: 03-08-29 9:38 $
	$Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,
                com.viewhigh.cbcs.base.mvc.view.component.Select,
                com.viewhigh.cbcs.cbcs.itemcheckcount.itemcostcollect.base.ResultAndStringTransform" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">

function choose()
     {
      if(template.type.value=='1'){
        mon.style.display = "block"
        ji.style.display = "none"
        nian.style.display = "none"
        qijian.style.display = "none"
        }
        else if(template.type.value=='2'){
        mon.style.display = "none"
        ji.style.display = "block"
        nian.style.display = "none"
        qijian.style.display = "none"
        }
        else if(template.type.value=='3'){
        mon.style.display = "none"
        ji.style.display = "none"
        nian.style.display = "block"
        qijian.style.display = "none"
        }
        else if(template.type.value=='4'){
        mon.style.display = "none"
        ji.style.display = "none"
        nian.style.display = "none"
        qijian.style.display = "block"
        }
        else if(template.type.value=='0'){
        mon.style.display = "none"
        ji.style.display = "none"
        nian.style.display = "none"
        qijian.style.display = "none"
        }
      }
  function isEmptys()
   {
      if(template.type.value=='1'&&template.year_month.value=='')
        {  alert("请选择时间");
           return false;
          }
    else if(template.type.value=='2'&&template.year_quarter.value=='')
        {  alert("请选择时间");
           return false;
          }
    else if(template.type.value=='3'&& template.year.value=='')
        {  alert("请选择时间");
           return false;
        }
   else if(template.type.value=='4'&& template.year_month_from.value=='')
        {  alert("请选择时间");
           return false;
        }
  else if(template.type.value=='4'&&  template.year_month_to.value=='')
        {  alert("请选择时间");
           return false;
        }
   else return true;

}

function message()
{
 window.open("itemcost.jspviewhigh?subFunction=chart&level=1",null,"height=500,width=650,toolbar=no, location=no")
 return true;
   }

  function account() {
    if(isEmptys()) {
   		show_wait();
	    template.subFunction.value='account';
	    template.submit();
	    return true;
   	}
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="itemcost.jspviewhigh">
       <html:message/>
       <html:title clazz='module'>项目成本(医成本I3-02表)</html:title>
       <html:table clazz="simple">
                  <tr class="normalText" nowrap>
                   <td nowrap>分析单位:</td>
                    <td>
                      <select name="type" onchange="choose()" class="selectBg">
                         <option value='1' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("1")) out.print("selected");%> >月</option>
                         <option value='2' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("2")) out.print("selected");%>>季</option>
                         <option value='3' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("3")) out.print("selected");%>>年</option>
                         <option value='4' <%if(request.getParameter("type")!=null && request.getParameter("type").equals("4")) out.print("selected");%>>期间</option>
                      </select>
                     </td>
                     <td nowrap>本期点：</td>
                    <td id="mon" <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("1")) out.print(" style='display:block;' "); if(request.getParameter("type")!=null&&!request.getParameter("type").equals("1")) out.print(" style='display:none;' ");if(request.getParameter("type")==null) out.print(" style='display:block;' ");%>>
                     <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
                    <td id="ji"   <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("2")) out.print(" style='display:block;' "); if(request.getParameter("type")!=null&&!request.getParameter("type").equals("2")) out.print(" style='display:none;' "); if(request.getParameter("type")==null) out.print(" style='display:none;' ");%> nowrap>
                      <%=new com.viewhigh.cbcs.base.mvc.view.component.QuarterComponent("year_quarter", request.getParameter("year_quarter"))%> </td>
                   <%String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};%>
                    <td id="nian"  <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("3")) out.print(" style='display:block;' "); if(request.getParameter("type")!=null&&!request.getParameter("type").equals("3")) out.print(" style='display:none;' "); if(request.getParameter("type")==null) out.print(" style='display:none;' ");%> nowrap>
                     <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year",request.getParameter("year"),false,false)%>年</td>
                    <td id="qijian"  <%if(request.getParameter("type")!=null&&request.getParameter("type").equals("4")) out.print(" style='display:block;' "); if(request.getParameter("type")!=null&&!request.getParameter("type").equals("4")) out.print(" style='display:none;' "); if(request.getParameter("type")==null) out.print(" style='display:none;' ");%> nowrap>
                     <%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%></td>
                     </tr>


                   <tr>
                     <td class="normalText" nowrap>收入项目:</td>

                     <td>
                     <%
                  //******  开始生成javascript
                      String[][] seconditem=(String[][])request.getAttribute("seconditem");
                      String[] names = {"values","texts", "relations"};
                      out.println(com.viewhigh.cbcs.base.util.SimpleSearch.dynamicSelect(names, seconditem, "dynamicSelect", request.getParameter("second")));
                 //******  结束 生成javascript
                       String[][] firstitem=(String[][])request.getAttribute("firstitem");
                      Select super_code = new Select(firstitem, "first", request.getParameter("first"), false, false);
                        //******  开始调用javascript
                      super_code.setAttribute("onchange", "dynamicSelect(template.first, template.second)");
                //******  结束调用javascript
	              out.println(super_code);
                  %>
                    </td>
                   <td class="normalText" nowrap>收费类别:</td>
                   <td >
                 <%

                      Select seco_code = new Select("second");
	              out.println(seco_code);
                     %>
                      </td>
                      <td><button class="pageBtn" onclick="account();">分析</button></td>

                     </tr>
          </html:table>

      <br>
      <html:title clazz='table'>项目成本</html:title>

    <html:table clazz="result">
<%
	DecimalFormat nf = new DecimalFormat("#,##0.00");
	String[][] result = (String[][])request.getAttribute("result");
	if (result!=null) {
%>
      <html:tr clazz='label'>
          <td nowrap style='text-align:center;font-weight:bold' >项目名称</td>
          <td nowrap style='text-align:center;font-weight:bold' ><a href="#" onclick="message()">总成本(元)</a></td>
          <td nowrap style='text-align:center;font-weight:bold' >单位成本(元)</td>
          <td nowrap style='text-align:center;font-weight:bold' >工作量</td>
      </html:tr>

             <%
                  for (int i = 0; i < result.length; i++ )
                  {  String rowColor = "rowGray";
                      if (i/2*2==i) rowColor = "rowWhite";
              %>
              <tr CLASS="<%=rowColor%>">
                  <td nowrap class="normalText"><%=result[i][0]%></td>
                 <%for(int j=1;j<result[0].length-1;j++) {%>
                  <td class="numberText"><%=nf.format(Double.parseDouble(result[i][j]))%></td>
               <%}%>
              </tr>
              <%
                    }
   } else { %>
      <html:tr clazz='label'>
        <td nowrap style='text-align:center;font-weight:bold' >项目名称</td>
        <td nowrap style='text-align:center;font-weight:bold' >总成本(元)</td>
        <td nowrap style='text-align:center;font-weight:bold' >单位成本(元)</td>
        <td nowrap style='text-align:center;font-weight:bold' >工作量</td>
      </html:tr>
   <% }%>
    </html:table>
    <input type=hidden name="subFunction"/>

</form>
<!-- ******开始javascript初始化 -->
<script language="javascript">
dynamicSelect(template.first, template.second);
</script>
<!-- ******结束javascript初始化 -->


</html:html>


