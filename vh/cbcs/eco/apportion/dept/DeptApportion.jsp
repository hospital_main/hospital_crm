<!--
   $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/eco/apportion/dept/DeptApportion.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
   $Author: zhoulidong $
   $Date: 2012/03/12 01:58:11 $
   $Modtime:  $
   $Revision: 1.1 $
   $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript">
	function apportion(num) {
	  if (template.year_month.value=='') {
	    alert('请选择核算月');
	    return false;
	  }
          if(num==0)
            template.flag.value='no'
          else if(num==1)
            template.flag.value='complete'
	  show_wait();
	  template.submit();
	  return true;
	}
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="ecoDeptApportion.jspviewhigh?subFunction=count" >
      <!-- 返回信息栏 -->
	  <html:message/>

      <!-- 标题栏 -->
	  <html:title clazz='module'>科室指标</html:title>
<br>

      <!-- 简单信息 -->
	  <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap width="10%">核算月：</td>
          <td class="normalText"><%=new MonthComponent("year_month", request.getParameter("year_month"))%></td>
          <td><button class="pageBtn" onclick="return apportion(1)">完成</button>
          <!--<img src="images/complete.gif" class="mouse" onclick="return apportion(1)"/>--></td>
        </tr>
	  </html:table>
<input type='hidden' name="flag" value='complete'>
  	</form>

</html:html>
