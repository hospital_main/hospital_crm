<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/userset/BasicInfoSet.jsp,v 1.2 2012/10/24 04:57:25 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/10/24 04:57:25 $
	$Modtime: 03-09-02 15:40 $
	$Revision: 1.2 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" >
</Script>

<Script Language="JavaScript">

	function create() { 
		show_wait();
		template.subFunction.value='store';
		template.submit();
		return true;
	}
	
	function checkme(obj){
    switch(isDouble(obj,10,2))
     {
      case 0 : alert('住院床日/门诊人次必须为数字型'); return;;
     }
	
	}
function keyvalue() 
{ 
  if(event.keyCode=="13") 
  { 
    if(event.srcElement.type=="select" || event.srcElement.type=="text") 
    { 
      event.keyCode="9"; 
    }
  } 
}
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="basicInfoSet.jspviewhigh" onkeydown="keyvalue();">
		<html:message/>
		<html:title clazz='module'>
			单位基本信息设置页面
		</html:title>
		
		<html:table clazz="simple">
			<% String[] com_info=(String[])request.getAttribute("com_info"); %>
			<tr>
				<td width="25%" class="signText" nowrap="nowrap">核算月起始日：</td>
				<td width="25%"  class="normalText" nowrap="nowrap">
					<select type="select" name="start_day" class='selectBg' style="width:140px;"/>
						<%
						for(int i = 1; i<=28; i++) {
						%>
							<option value="<%=String.valueOf(i)%>" <%if(com_info!=null&&(String.valueOf(i)).equals(com_info[2])) out.print("selected");%>>
								<%=String.valueOf(i)%>
							</option>
						<%
						}
						%>
					</select>
				</td>
				<td class="signText" nowrap="nowrap">住院床日/门诊人次：</td>
				<td class="normalText" nowrap="nowrap">
					<input type="text" autocomplete="off" name="cost_scale" value='<%=(com_info==null? "0":com_info[6].substring(0, 1))%>'  onKeyUp="this.value=value.replace(/[^\d]/g,'');" size="8" maxlength='1' style="width:140px;"/>
				</td>
			</tr>
			<tr>
				<td class="signText" nowrap="nowrap">内部分配控制：</td>
				<td class="normalText" nowrap="nowrap">
					<select type="select" name="bonus_control" class='selectBg' style="width:200px;"/>
						<%
						for(int i = 0; i<=3; i++) {
						%>
							<option value="<%=String.valueOf(i)%>" <%if(com_info!=null&&(String.valueOf(i)).equals(com_info[7])) out.print("selected");%>>
								<%
									if(String.valueOf(i).equals("0")){
										out.print("不控制");
									}else if(String.valueOf(i).equals("1")){
										out.print("只提示不控制");
									}else if(String.valueOf(i).equals("2")){
										out.print("超过本年预算，不允许分配");
									}else if(String.valueOf(i).equals("3")){
										out.print("超过月预算，不允许分配");
									}
								%>
							</option>
						<%
						}
						%>
					</select>
				</td>

				<td class="signText" nowrap="nowrap">奖金调控继承：</td>
				<td class="normalText" nowrap="nowrap">
					<select type="select" name="bonus_inherit" class='selectBg' style="width:140px;"/>
						<%
						for(int i = 0; i<=1; i++) {
						%>
							<option value="<%=String.valueOf(i)%>" <%if(com_info!=null&&(String.valueOf(i)).equals(com_info[8])) out.print("selected");%>>
								<%
									if(String.valueOf(i).equals("0")){
										out.print("不继承");
									}else if(String.valueOf(i).equals("1")){
										out.print("继承");
									}
								%>
							</option>
						<%
						}
						%>
					</select>
				</td>

			</tr>

			<tr>
				<td colspan="4" align="center"> <button class="pageBtn" onclick="return create();" >保存</button>
				<button class="pageBtn" onclick="return reset();">重置</button>
				</td>
			</tr>
			<tr height="500">
				<td>&nbsp;</td>
			</tr>
		</html:table>
		<input type=hidden name="subFunction"/>
	</form>
	
</html:html>