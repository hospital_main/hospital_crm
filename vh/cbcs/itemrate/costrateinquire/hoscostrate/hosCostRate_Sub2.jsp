<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/costrateinquire/hoscostrate/hosCostRate_Sub2.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.util.Preference,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">

</Script>
<html:html clazz="main">
<form name="template" method="post" action="hosCostRate.jspviewhigh">
      <html:message/>
      <html:title clazz='module'>医疗项目人工单位成本  </html:title>
      <html:table clazz="simple">
      
			<%
			 String [][]note = (String[][])session.getAttribute("note");
			 if  (note!=null  ){
			%>
			     <tr> <td  nowrap class="signText">配置编码:<%=note[0][0]%> </td></tr> 
			     <tr> <td  nowrap class="signText">医疗项目:<%=note[1][1]%></td></tr>
			<%}%>

</html:table>
    <html:table clazz="simple"><tr><td><html:title clazz='table'>医疗项目人工单位成本</html:title></td></tr></html:table>
<%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro != null) {
      TableMarge oper = new TableMarge(ro, "return find()");
%>
<html:table clazz="simple"><tr><td><%=oper%></td></tr></html:table>
     <html:table clazz="complex">

<tr>
			<td>	
<html:table clazz="result">
	<html:tr clazz='label'> 
		<td   nowrap class="resultLabel" ><div align="center">作业名称</div></td>
		<td   nowrap class="resultLabel" ><div align="center">工作人员职称</div></td>
		<td   nowrap class="resultLabel" ><div align="center">工作时间(分钟)</div></td>
		<td   nowrap class="resultLabel" ><div align="center">工作人员数量</div></td>     
		<td   nowrap class="resultLabel" ><div align="center">合计</div></td>
		<td   nowrap class="resultLabel" ><div align="center">平均(每分钟)</div></td>   
		<td   nowrap class="resultLabel" ><div align="center">人工费用</div></td>       
	</html:tr>
        <%
          String[][] result = ro.getTableResult();
          DecimalFormat mf=new DecimalFormat("#,##0.00");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %> 
       <tr CLASS="<%=rowColor%>">
           <td nowrap class="normalText"><%=result[i][2]%>
           <td nowrap class="normalText"><%=result[i][4]%>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 5 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 6 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 9 ]))%></td> 
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 10 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 11 ]))%></td>  
          
          
          
        </tr>
        <%
              }
            }
        %>
       </html:table>
         </td>
         </tr>

      </html:table>
<%}%>
<input type="hidden" name="subFunction" >
</form>
</html:html> 