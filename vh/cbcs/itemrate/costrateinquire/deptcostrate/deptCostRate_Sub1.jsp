<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/costrateinquire/deptcostrate/deptCostRate_Sub1.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.util.Preference,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
	function find(){

		template.subFunction.value="findsub1";
		template.submit();
		return;
	}
</Script>
<html:html clazz="main">
<form name="template" method="post" action="deptCostRate.jspviewhigh">
      <html:message/>
      <html:title clazz='module'>科室医疗项目单位成本(成本分类) </html:title>
      <html:table clazz="simple">
      
			<%
			 String [][] note = (String[][])request.getSession().getAttribute("note");
			 if  (note!=null  ){
			%>
			     <tr> <td  nowrap class="signText">配置编码：<%=note[0][1]%> </td></tr>
			     <tr> <td  nowrap class="signText">科室:<%=note[1][1]%></td></tr>
			     <tr> <td  nowrap  class="signText">医疗项目:<%=note[2][1]%></td> </tr>
			<%}%>

</html:table>
 
    <html:table clazz="simple"><tr><td><html:title clazz='table'>科室医疗项目单位成本(成本分类)</html:title></td></tr></html:table>

     <html:table clazz="complex">

<tr>
        <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
		        
          <td nowrap   class="resultLabel" >成本分类</td>
          <td nowrap   class="resultLabel" >单位成本</td>
          <td nowrap   class="resultLabel" >直接归集</td>
          <td nowrap   class="resultLabel" >计算计入</td>          
        </html:tr> 
        <%

          String[][] result =(String[][]) request.getAttribute("result");
          DecimalFormat mf=new DecimalFormat("#,##0.00");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite"; 
            if ( result[i][7]==null|| result[i][7]=="0" ) continue;
            // Integer.parseInt(result[i][7])==0 
        %>

       <tr CLASS="<%=rowColor%>">           
          <td nowrap class="normalText"> <%=result[ i ][ 4 ]%></td>
          </a></td> 
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 5]))%></td>   
  				<%
  				 boolean IsCdirect= Preference.getIsCdirect();//单独采集
        	 boolean IsEcAcomp= Preference.getIsEcAcomp();//	单独计算
  				if ((result[i][8]=="1"&&IsCdirect )|| (result[i][8]=="2"&& IsEcAcomp ) ){
  				%>       
          <td nowrap class="numberText">
       		<a href="#" onclick="   var tim=new Date();  window.showModalDialog('deptCostRate.jspviewhigh?subFunction=findsub2&charge_ra_code=<%=result[ i ][ 0 ]%>&charge_detail_code1=<%=result[ i ][ 1 ]%>&flag=<%=result[ i ][ 8 ]%>&cost_class_code=<%=result[ i ][ 3 ]%>&time='+tim.toLocaleString(),  '','dialogLeft:120px;dialogTop:90px; status:0;dialogHeight: 650px; dialogWidth: 900px;');">
					<%=mf.format(Double.parseDouble(result[ i ][ 6]))%></a></td>   			
					<%
					}else {					
					%>						
					<td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 6]))%></td>   			
					<%
					}
					%>				      
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 7]))%></td>  
          
          
        </tr>
        <%  
              }
            }
        %>
       </html:table>
         </td>
         </tr>

      </html:table>

<input type="hidden" name="subFunction" >

<input type="hidden" name="charge_ra_code" value=<%=request.getParameter("charge_ra_code") %> >  
<input type="hidden" name="dept_code1" value=<%=request.getParameter("dept_code1") %> > 
<input type="hidden" name="charge_detail_code1" value=<%=request.getParameter("charge_detail_code1") %> > 
</form>
</html:html>
<script>
</script>