<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/costrateinquire/itemtaskcost/itemTaskCostMain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.util.Preference,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script language="javascript">
	function dischange(){
		template.changesign.value="dischange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function kindchange(){
		template.changesign.value="kindchange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function find(){
		if(trim(template.charge_ra_code.value)==""){
	     alert("请输入文件编号!");
	 	   return false;
    }
    if(trim(template.dept_code.value)==""){
	     alert("请选择科室!");
	 	   return false;
    }
    if(trim(template.dict_income_subj.value)==""){
	     alert("请选择收入项目!");
	 	   return false;
    }
    if(trim(template.b_dict_charge_detail_kind.value)==""){
	     alert("请选择收费类别!");
	 	   return false;
    }
    if(trim(template.dict_charge_detail.value)==""){
	     alert("请选择医疗项目!");
	 	   return false;
    }
		show_wait();
		template.subFunction.value="findAll";

		template.submit();
		return;
	}
</Script>
 
</Script>
<html:html clazz="main">
<form name="template" method="post" action="itemTaskCost.jspviewhigh">
      <html:message/>
      <html:title clazz='module'>医疗项目作业成本构成</html:title>
      <html:table clazz="simple">
      <tr>
			<td class="signText">文件编号：</td>
			<td class="normaltext">
		<%
			 Select charge_ra_code= new  Select(request.getAttribute("charge_ra_code"),"charge_ra_code",request.getParameter("charge_ra_code")==null?"":request.getParameter("charge_ra_code"),false,false);
		%>
		<%= charge_ra_code.toString()%>
			</td>	
			<td nowrap class="signText">科室：</td>
			<% String dept_code[][] = (String[][])request.getAttribute("dept_code");%>
			<td> <%=new Select(dept_code,"dept_code", request.getParameter("dept_code"), false, false)%></td> 	 

				
      </tr>
      <tr> 
				<td class="signText">收入项目：</td>
				<td class="normalText">
				<%
				Select dict_income_subj=new Select(request.getAttribute("dict_income_subj"),"dict_income_subj",request.getParameter("dict_income_subj")==null? "":request.getParameter("dict_income_subj"),false,false);
				dict_income_subj.setAttribute("onchange","dischange()");
				%>
				<%=dict_income_subj.toString() %></td>	
				<td class="signText">收费类别：</td>
				<td class="normalText">
				<%
				Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"b_dict_charge_detail_kind",request.getParameter("b_dict_charge_detail_kind")==null? "":request.getParameter("b_dict_charge_detail_kind"),false,false);
				b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
				%>
				<%=b_dict_charge_detail_kind.toString() %></td>
				<td class="signText">医疗项目: </td>
				<td class="normalText">
				<%
				Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"dict_charge_detail",request.getParameter("dict_charge_detail")==null? "":request.getParameter("dict_charge_detail"),false,false);
				%>
				<%=dict_charge_detail.toString() %></td>		
				
		 </tr>
		 <tr>
		 <td class="signText">说明:</td>
		 <td class="normalText">
		  <%=(String)request.getAttribute("dict_charge_set_con")==null? "":"数据源时间"+(String)request.getAttribute("dict_charge_set_con") %></td>
		 </tr>
		 <tr>
        <td id="p1"/><td id='p2'/><td id='p3'/>
        <td>
        <button class="pageBtn" name=""   onclick="find();"  >查询</button>
        <!--<img src="images/find.gif" class="mouse" onclick="find();">--></td>
      </tr>

</html:table>
    <html:table clazz="simple"><tr><td><html:title clazz='table'>医疗项目作业成本构成</html:title></td></tr></html:table>

	<html:table clazz="complex">
		<tr>
			<td>
      <html:table clazz="result">          
				<html:tr clazz='label'>   
				<td   nowrap class="resultLabel" ><div align="center">作业</div></td>
				<td   nowrap class="resultLabel" ><div align="center">成本类别</div></td>
				<td   nowrap class="resultLabel" ><div align="center">单位成本</div></td> 
				</html:tr>



        <%
          String[][] result =  (String[][])request.getAttribute("result") ;
          DecimalFormat mf=new DecimalFormat("#,##0.00");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
            //if (result[i][0].trim().equals("z")&& Double.parseDouble(result[ i ][ 5])==0) continue;
        %>

       <tr CLASS="<%=rowColor%>">
					<td nowrap class="normalText"><%=result[ i ][ 2 ]%></td>
					<td nowrap class="normalText"><%=result[ i ][ 4 ]%></td>
					<%
					if (result[i][0].trim().equals("z")||result[i][0].trim().equals("w")) {
					%>
					<td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 5]))%>
					<%
					}else {
					%>					
					<td nowrap class="numberText"><a href="#" onclick="var tim=new Date();   window.showModalDialog('itemTaskCost.jspviewhigh?subFunction=findsub1&charge_ra_code='+template.charge_ra_code.value+'&dept_code1='+template.dept_code.value+'&charge_detail_code1='+template.dict_charge_detail.value+'&work_code1=<%=result[i][1]%>&cost_class_code1=<%=result[i][3]%>&flag=<%=result[i][0]%>&time='+tim.toLocaleString(),window,'dialogLeft:120px;dialogTop:90px; status:0;dialogHeight: 650px; dialogWidth: 900px;');">
					<%=mf.format(Double.parseDouble(result[ i ][ 5]))%></a></td> 
					
					<%}%>
					 
       </tr>
        <%
              }
            }
        %>
       </html:table>
         </td>
         </tr>

      </html:table>
 
<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
<input type="hidden" name="subFunction" >
</form>
</html:html> 