<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/mCoopCost/mCoopCostUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  //添加
function save() {

		switch(isDouble(template.Consume_num, 10,6))
	    {
	      case 0 : alert('消耗数量必须为数字型'); return false;
	      case 1 : alert('消耗数量整数位不能超过10位'); return false;
	      case 3 : alert('消耗数量小数位不能超过6位'); return false;
	    }
	   switch(isDouble(template.Inv_price, 12,4))
	    {
	      case 0 : alert('单价必须为数字型'); return false;
	      case 1 : alert('单价整数位不能超过12位'); return false;
	      case 3 : alert('单价小数位不能超过4位'); return false;
	    }
	      template.subFunction.value='Update';
        show_wait();
        template.submit();
        return true;
    }
  
  function total()
	{
		if(template.Consume_num.value!="" && template.Inv_price.value != "")
		{
			template.Inv_mny.value = template.Consume_num.value * template.Inv_price.value;
		}
	}
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="mateCooperationCost.jspviewhigh">  
<%
	String []result =(String[]) request.getAttribute("result");
%>
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>材料消耗维护修改界面 </html:title>  
  <!-- 简单信息 -->    <!--配置编号 医疗项目 作业编号  材料类别 材料名称 消耗数量 单价 金额 -->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="normalText" nowrap="nowrap">配置编号:</td>
      <td nowrap class="normalText"><input type=text class="textInputC" value="<%=result[1]%>" disabled /></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">医疗项目:</td>
      <td class="normalText" nowrap="nowrap"><input type=text   class="textInputC" value="<%=result[3]%>" disabled /></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">作业编号:</td>
      <td class="normalText" nowrap="nowrap"><input type=text  class="textInputC" value="<%=result[5]%>" disabled /></td>
    </tr>   
    <tr>
      <td class="normalText" nowrap="nowrap">材料类别：</td>
      <td class="normalText" nowrap="nowrap"><input type=text  class="textInputC" value="<%=result[7]%>"  disabled /></td>
    </tr>    
      <td class="normalText" nowrap="nowrap">材料名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text    class="textInputC" value="<%=result[9]%>" disabled /></td>
    </tr>
    </tr>    
      <td class="normalText" nowrap="nowrap">消耗数量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text  maxlength="17"  name="Consume_num" onChange="total();" value="<%=result[10]%>" class="textInputC" /></td>
    </tr>
    </tr>    
      <td class="normalText" nowrap="nowrap">单价：</td>
      <td class="normalText" nowrap="nowrap"><input type=text  maxlength="17"  name="Inv_price" onChange="total();" value="<%=result[11]%>" class="textInputC" /></td>
    </tr>
    </tr>    
      <td class="normalText" nowrap="nowrap">金额：</td>
      <td class="normalText" nowrap="nowrap"><input type=text  maxlength="17"  name="Inv_mny"  value = "<%=result[12]%>" class="textInputC" readonly/></td>
    </tr>
    
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" />
 <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
  <input type=hidden name="set_cfg_code" class="textInputC" value="<%=result[0]%>"/></td>
  <input type=hidden name="charge_detail_code" value="<%=result[2]%>" /></td>
  <input type=hidden name="work_code" value="<%=result[4]%>" /></td>
  <input type=hidden name="Inv_type_code" value="<%=result[6]%>" /></td>
  <input type=hidden name="Inv_code" value="<%=result[8]%>"/></td>
</form>

</html:html>   