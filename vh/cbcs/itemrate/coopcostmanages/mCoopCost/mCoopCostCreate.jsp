<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/mCoopCost/mCoopCostCreate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
	function create() {
	
		 switch(isDouble(template.Consume_num, 10,6))
	    {
	      case 0 : alert('消耗数量必须为数字型'); return false;
	      case 1 : alert('消耗数量整数位不能超过10位'); return false;
	      case 3 : alert('消耗数量小数位不能超过6位'); return false;
	    }
	   switch(isDouble(template.Inv_price, 12,4))
	    {
	      case 0 : alert('单价必须为数字型'); return false;
	      case 1 : alert('单价整数位不能超过12位'); return false;
	      case 3 : alert('单价小数位不能超过4位'); return false;
	    }
	   
	if (template.set_cfg_code.value =="")
	{
		alert("请选择配置编码!");
		template.set_cfg_code.focus();
		return false;
	}
	
	if (template.work_code.value =="")
	{
		alert("请选择作业编号!");
		template.work_code.focus();
		return false;
	}
	if (template.Inv_type_code.value =="")
	{
		alert("请选择材料类别!");
		template.Inv_type_code.focus();
		return false;
	}
	if (template.Inv_code.value =="")
	{
		alert("请选择材料名称!");
		template.Inv_code.focus();
		return false;
	}
	if(template.Consume_num.value=="")
	{
		alert("请输入消耗数量!");
		template.Consume_num.focus();
		return false;
	}
	if(template.Inv_price.value == "")
	{
		alert("请输入单价!");
		template.Inv_price.focus();
		return false;
	}
	if (template.charge_detail_code.value =="")
	{
		alert("请输入医疗项目!");
		template.charge_detail_code.focus();
		return false;
	}
        template.subFunction.value='Create';
        show_wait();
        template.submit();
        return true;
    }
	function total()
	{
		if(template.Consume_num.value!="" && template.Inv_price.value != "")
		{
			template.Inv_mny.value = template.Consume_num.value * template.Inv_price.value;
		}
	}

  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="mateCooperationCost.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>
<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
	String [][]charge_detail_code = (String [][])request.getAttribute("dict_charge_detail");
	String [][]work_code = (String[][])request.getAttribute("dict_charge_work");
	String [][]Inv_type_code = (String[][])request.getAttribute("dict_inv_type");
	String [][]Inv_code = (String[][])request.getAttribute("dict_inv");
%>
  <!-- 标题栏 -->
	  <html:title clazz='module'>合作成本-材料合作成本添加界面 </html:title>  
  <!-- 简单信息 -->    <!-- 配置编号 医疗项目 作业编号  材料类别 材料名称 消耗数量 单价 金额     -->
  <table  width="100%" cellspacing="2" border="0">
     <tr>   
      <td class="normalText" nowrap="nowrap">配&nbsp;置&nbsp;编&nbsp;号&nbsp;：</td>
      <td class="normalText" nowrap="nowrap" ><%=new Select(set_cfg_code,"set_cfg_code","",false,false)%></td>
     </tr>
     
    <tr>
      <td class="normalText" nowrap="nowrap">作业名称：</td>
      <td nowrap class="normalText"><%=new Select(work_code,"work_code","",false,false)%></td>
    </tr>
    <tr>
			<td class="normalText" nowrap="nowrap">材料类别：</td>
			<td class="normalText" nowrap="nowrap"><%=new Select(Inv_type_code,"Inv_type_code","",false,false)%>  </td>
		</tr>
		<tr>
      <td class="normalText" nowrap="nowrap">材料名称：</td>
      <td class="normalText" nowrap="nowrap">  <%=new Select(Inv_code,"Inv_code","",false,false)%> </td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">消耗数量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="Consume_num"  maxlength="17"  class="textInputC" onChange = "total();"/></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">单价：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="Inv_price"  maxlength="17"  class="textInputC" onChange = "total();"/></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">金额：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="Inv_mny"  maxlength="17"  class="textInputC" readonly/></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">收&nbsp;费&nbsp;项&nbsp;目&nbsp;：</td>
      <td class="normalText" nowrap="nowrap" >
      	<?xml:namespace prefix="hzh"/>
				<hzh:QInput ID="nosNamea" name="charge_detail_code" value="" AdjustVal="95" previousObj="Inv_code" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="10" left="269" Lheight="5" xmlSource="dic/dict_charge_detail.xml" init="1"/>

      </td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   