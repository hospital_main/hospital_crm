<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/coopCostCac/coopCostCacMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
	function code_change(){
		if (template.set_cfg_code.value ==""){
				alert("请选择配置编码!");
				template.set_cfg_code.focus();
				return false;
		}
		 show_wait();
	   template.subFunction.value="Change";
	   template.submit();
	   return true;
	}
	function remove(){
		if (template.set_cfg_code.value =="")	{
			alert("请选择配置编码!");
			template.set_cfg_code.focus();
			return false;
		}
		netA.Gsend("cooperationCostCaculate.jspviewhigh?subFunction=check&set_cfg_code=" + template.set_cfg_code.value,copyAccess);
	}

	function copyAccess(){
		var num=new Number(netA.req.responseText);
			if(isNaN(num)){
				alert(netA.req.responseText);
			}
			else{
				if(netA.req.responseText>0)	{
					if(confirm("删除数据不可恢复,是否确实要删除高配置编码下的合作成本计算数据？")){
						doCopy();
					}
				}
				else{
					alert("该配置编码未曾计算过合作成本计算数据，因此不需要删除!");
					return false;
				}
			}
	}

	function doCopy(){
		template.subFunction.value="findAll";
		show_wait();
	  template.submit();
	  return true;
	}

	function Calculate(){
		if (template.set_cfg_code.value ==""){
			alert("请选择配置编码!");
			template.set_cfg_code.focus();
			return false;
		}
	 	netA.Gsend("cooperationCostCaculate.jspviewhigh?subFunction=check&set_cfg_code=" + template.set_cfg_code.value,copyAccessCalculate);
	}
	function copyAccessCalculate(){
		var num=new Number(netA.req.responseText);
		if(isNaN(num)){
			alert(netA.req.responseText);
		}
		else{
			if(netA.req.responseText>0)	{
				if(confirm("该配置编码已经计算过，是否重新计算？")){
					doCalculate();
				}
			}
			else{
				doCalculate();
			}
		}
	}
	function doCalculate(){
		template.subFunction.value="Calculate";
		show_wait();
	  template.submit();
	  return true;
	}
 
</Script>

<%
	//String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
	String ymf=(String)request.getAttribute("ymf");
	String ymt=(String)request.getAttribute("ymt");
	String  dateTitle="x年x月 到 x年x月";
	if(ymf!=null&&ymt!=null){
		dateTitle = ymf.substring(0,4)+"年"+ymf.substring(4,6)+"月 －"+ymt.substring(0,4)+"年"+ymt.substring(4,6)+"月";
		}
%>

<html:html clazz="main">
	<form name="template" method="post" action="cooperationCostCaculate.jspviewhigh">
  	
	  <!-- 信息提示栏 -->
	<html:message/>

	  <!-- 标题栏 -->
	<html:title clazz='module'>单位成本核算 </html:title>
	  <html:table clazz="simple">
	  	<tr>
			  <td nowrap class="normalText">配置编码:</td>
				<td nowrap class="normalText" colspan = 2>
					<%
					 	Select set_cfg_code=new Select(request.getAttribute("dict_charge_set_con"),"set_cfg_code",request.getParameter("set_cfg_code")==null? "":request.getParameter("set_cfg_code"),false,false);
					 	set_cfg_code.setAttribute("onchange","code_change()");
					%>
					<%=set_cfg_code.toString() %>
			</tr>
			<tr>
				<td class="normalText">数据源：</td>
			 	<td class="normalText">	 <%=dateTitle%> </td>
				<td><button class="pageBtn" onclick = "return Calculate();">计算</button>
				<button class="pageBtn" onclick="return remove();">删除</button>
				<!--<img src="images/delete.gif" class="mouse" onclick="return remove();">-->
				</td>
	 		</tr>
	 	</html:table>
		<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
		<input type="hidden" name="subFunction" value="perparedFind"/>
	</form>
</html:html>
