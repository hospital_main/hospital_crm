<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/eCoopCost/eCoopCostCreate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
	function create() {
		switch(isDouble(template.Depre_price, 12,4))
	    {
	      case 0 : alert('单位折旧成本必须为数字型'); return false;
	      case 1 : alert('单位折旧成本不能大于14个字符'); return false;
	      case 3 : alert('单位折旧成本有4位小数'); return false;
	    }
	 switch(isDouble(template.Use_time, 14,2))
	    {
	      case 0 : alert('时间必须为数字型'); return false;
	      case 1 : alert('时间不能大于14个字符'); return false;
	      case 3 : alert('时间有2位小数'); return false;
	    }
	
	if (template.set_cfg_code.value =="")
	{
		alert("请选择配置编码!");
		template.set_cfg_code.focus();
		return false;
	}
	if (template.inv_type_code.value =="")
	{
		alert("请选择成本项目!");
		template.inv_type_code.focus();
		return false;
	}
	if (template.equi_code.value =="")
	{
		alert("请选择设备!");
		template.equi_code.focus();
		return false;
	}
	if (template.Use_time.value =="")
	{
		alert("请输入时间!");
		template.Use_time.focus();
		return false;
	}
	if (template.Depre_price.value =="")
	{
		alert("请输入单位折旧成本!");
		template.Depre_price.focus();
		return false;
	}
	 if (template.charge_detail_code.value =="")
	{
		alert("请输入医疗项目!");
		template.charge_detail_code.focus();
		return false;
	}   
	    
        template.subFunction.value='Create';
        show_wait();
        template.submit();
        return true;
    }
   //计算折旧成本
    function Depre()
    {
    	if (template.Use_time.value != '' && template.Depre_price.value != '')
    	{
    		template.Depre_mny.value = template.Use_time.value  * template.Depre_price.value;
    	}
    }
    
    
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="equipCooperationCost.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>
<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
	String [][]inv_type_code = (String [][])request.getAttribute("dict_inv_type");
	String [][]dev_code = (String[][])request.getAttribute("dict_dev");
	
%>
  <!-- 标题栏 -->
	  <html:title clazz='module'>合作成本-设备折旧合作成本添加界面 </html:title>  
  <!-- 简单信息 -->    <!-- 配置编号 医疗项目 成本项目 设备 时间 单位折旧成本 折旧成本   -->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="normalText" nowrap="nowrap">配置编码：</td>
      <td class="normalText" nowrap="nowrap" ><%=new Select(set_cfg_code,"set_cfg_code","",false,false)%></td>
    </tr>
    <tr>
    	<td class="normalText" nowrap="nowrap">收&nbsp;费&nbsp;项&nbsp;目&nbsp;：</td>
      <td class="normalText" nowrap="nowrap" >
	      <?xml:namespace prefix="hzh"/>
	      <hzh:QInput ID="nosNamea" name="charge_detail_code" AdjustVal="50" previousObj="Use_time" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="35" left="268" Lheight="6" xmlSource="dic/dict_charge_detail_LVT.xml" init="1"/>
	    </td>
    </tr>
    <tr>     
      <td class="normalText" nowrap="nowrap">成本项目：</td>
      <td nowrap class="normalText">
	      <?xml:namespace prefix="hzh"/>
	      <hzh:QInput ID="nosNamea" name="inv_type_code" AdjustVal="145" codeCol='spell' valueCol="cost_subj_code" value="<%=request.getParameter("inv_type_code")==null? "":request.getParameter("inv_type_code")%>" textCol="cost_subj_name" width="200" top="65" left="430" Lheight="6" xmlSource="dic/dict_subj_cost_detail_L.xml" init="1"/>
			</td>
		</tr>
		<tr>
      <td class="normalText" nowrap="nowrap">设备：</td>
      <td class="normalText"nowrap="nowrap"><%=new Select(dev_code,"equi_code","",false,false)%></td>
    </tr>
    <tr>  
      <td class="normalText" nowrap="nowrap">时间（分钟）：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="Use_time"  maxlength="17"  class="textInputC" onChange="Depre();" /></td>
    </tr>
    <tr>  
      <td class="normalText" nowrap="nowrap">单位折旧成本（元/分钟）：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="Depre_price"  maxlength="17"  class="textInputC"  onChange="Depre();" /></td>
    </tr>
    <tr>  
      <td class="normalText" nowrap="nowrap">折旧金额：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="Depre_mny"  maxlength="17"  class="textInputC" /></td>
    </tr>
   
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   