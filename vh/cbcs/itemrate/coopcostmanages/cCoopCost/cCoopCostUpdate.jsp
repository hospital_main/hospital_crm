<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/cCoopCost/cCoopCostUpdate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
  <Script Language="JavaScript">
   function save() {
		 switch(isDouble(template.per_cost, 12,4)){
		      case 0 : alert('单位消耗必须为数字型'); return false;
		      case 1 : alert('单位消耗整数位不能超过12位'); return false;
		      case 3 : alert('单位消耗小数位不能超过4位'); return false;
		    }
        template.subFunction.value='Update';
        show_wait();
        template.submit();
        return true;
    }
// 返回
  function back( element ) {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
  <form name="template" method="post" action="cooperateCooperationCost.jspviewhigh">  
  <%
  	String []result =(String[]) request.getAttribute("result");
  %>
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>合作成本-其他合作成本修改界面 </html:title>  
  <!-- 简单信息 -->    <!-- 配置编码 医疗项目  作业编号 成本项目 单位消耗  -->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="normalText" nowrap="nowrap">配置编号:</td>
      <td nowrap class="normalText"><input type=text name = "set_cfg_code1" class="textInputC" value="<%=result[1]%>" disabled /></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">医疗项目:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name = "charge_detail_code1"  class="textInputC" value="<%=result[3]%>" disabled /></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">作业名称:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="inv_type_code" class="textInputC" value="<%=result[5]%>" disabled /></td>
    </tr>   
    <tr>
      <td class="normalText" nowrap="nowrap">成本项目：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="cost_subj_code1" value="<%=result[7]%>" class="textInputC" disabled /></td>
    </tr>   
    <tr>
      <td class="normalText" nowrap="nowrap">单位消耗：</td>
      <td class="normalText" nowrap="nowrap"><input type=text  maxlength="17"  name="per_cost" value="<%=result[8]%>" class="textInputC"  /></td>
    </tr>
  	<tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>   <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
   </table>
    <input type=hidden name="subFunction" value="create"/>
    <input type=hidden name="set_cfg_code" class="textInputC" value="<%=result[0]%>"/></td>
    <input type=hidden name="charge_detail_code" value="<%=result[2]%>" /></td>
    <input type=hidden name="work_code" value="<%=result[4]%>" /></td>
    <input type=hidden name="cost_subj_code" value="<%=result[6]%>" /></td>
  </form>
</html:html>   