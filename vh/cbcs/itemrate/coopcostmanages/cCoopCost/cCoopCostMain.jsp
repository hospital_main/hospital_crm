<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/cCoopCost/cCoopCostMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function dischange(){
			template.changesign.value="dischange";
			template.subFunction.value="change";
			show_wait();
			template.submit();
			return;
	}
	function kindchange(){
			template.changesign.value="kindchange";
			template.subFunction.value="change";
			show_wait();
			template.submit();
			return;
	}
	 	function find(){
	 		template.subFunction.value='findAll';
	 		show_wait();
	    template.submit();
	 	}
	 	function inherit(){
	 		template.subFunction.value ='praparedcopy';
	 		show_wait();
	 		template.submit();
	 	}
	  function create(){
	 	 		template.subFunction.value='preparedCreate';
	 	  	show_wait();
	 	 		template.submit();
	 	 		}

	  function remove() {
	    var flag = false;
	    for (var i=0; i<template.elements.length; i++) {
	          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
	            flag = true;
	      }

	     if( flag!=false) {
	        if (confirm('是否删除')) {
	          template.subFunction.value='remove';
	          template.submit();
	          return true;
	        } else
	            return false;
	    } else {
	      alert( "请先选择,再删除!");
	      show_wait();
	      return false;
	    }
	  }
	  function selectAll(){
	    for (var i=0; i<template.elements.length; i++) {
	        if (template.elements[i].name=='primaryKey')
	            template.elements[i].checked = true;
	    }
		}
	</Script>

<%
	//String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
	//String [][]dict_title = (String [][])request.getAttribute("dict_title");
%>

<html:html clazz="main">
<form name="template" method="post" action="cooperateCooperationCost.jspviewhigh">
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
<html:title clazz='module'>合作成本-其他合作成本维护</html:title>
  <html:table clazz="simple">
    <tr>
		  <td nowrap class="normalText">配置编号:</td>
			<td nowrap class="normalText">
		  	<%
			   	Select dict_charge_set_con=new Select(request.getAttribute("dict_charge_set_con"),"set_cfg_code",request.getParameter("set_cfg_code")==null? "":request.getParameter("set_cfg_code"),false,false);
				%>
				<%=dict_charge_set_con.toString() %></td>
			<td class="normalText" nowrap="nowrap">成本项目：</td>
	    <td nowrap class="normalText">
		    <?xml:namespace prefix="hzh"/>
	      <hzh:QInput ID="nosNamea" name="cost_subj_code" AdjustVal="145" codeCol='spell' valueCol="cost_subj_code" value="<%=request.getParameter("cost_subj_code")==null? "":request.getParameter("cost_subj_code")%>" textCol="cost_subj_name" width="200" top="65" left="430" Lheight="6" xmlSource="dic/dict_subj_cost_detail_L.xml" init="1"/>
			</td>
 		</tr>
    <tr>
  	  <td class="normalText">收入项目：</td>
			<td class="normalText">
				<%
			  	Select income_subj_code=new Select(request.getAttribute("dict_income_subj"),"income_subj_code",request.getParameter("income_subj_code")==null? "":request.getParameter("income_subj_code"),false,false);
				  income_subj_code.setAttribute("onchange","dischange()");
				%>
				<%=income_subj_code.toString() %></td>
			<td class="normalText">收费类别：</td>
			<td class="normalText">
				<%
  				Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"charge_kind_code",request.getParameter("charge_kind_code")==null? "":request.getParameter("charge_kind_code"),false,false);
	  			b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
				%>
				<%=b_dict_charge_detail_kind.toString() %></td>
		</tr>
		
 		<tr>
      <td class="normalText">医疗项目：</td>
		  <td >
				<%                                                                                                                                                                                                                                                   
					Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"charge_detail_code",request.getParameter("charge_detail_code")==null? "":request.getParameter("charge_detail_code"),false,false);                                   
			 	%>                                                                                                                                                                                                                                                   
				<%=dict_charge_detail.toString() %>

			</td>
   	 	<td colspan = 4 align = "center">
   	 	<button class="pageBtn" name="" onclick="find()" >查询</button>
    	 <!--<img src="images/find.gif" name="finds" class="mouse" onclick="find()" />--></td>
      </td>
    </tr>	
</html:table>
<html:table clazz="simple"><tr><td><html:title clazz='table'>合作成本-其他合作成本维护</html:title></td></tr></html:table>
  <%
    BaseRO ro = (BaseRO)request.getAttribute("baseRO");
  	if (ro != null) {
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
      oper.addNeedButton("images/create.gif", "return create()");     //  添加
      oper.addOptionButton("images/copy.gif", "return inherit()");     //  复制
  %>
<html:table clazz="simple"><tr><td colspan="2"><%=oper%></td></tr></html:table>
  <% } else { %>
<html:table clazz="simple"><tr><td></td></tr></html:table>
  <% }  %>
 <html:table clazz="complex">
	<tr>
    <td>
      <html:table clazz="result">
      	<html:tr clazz='label'>
      	<td nowrap class="resultLabel">选择</td>
      	<td nowrap class="resultLabel">配置编号</td>
      	<td nowrap class="resultLabel">医疗项目</td>
      	<td nowrap class="resultLabel">作业名称 </td>
      	<td nowrap class="resultLabel">成本项目</td>
      	<td nowrap class="resultLabel">单位消耗</td>
      	</html:tr>
     <%

 	 if (ro != null) {
        String[][] result = ro.getTableResult();
         DecimalFormat mf=new DecimalFormat("#,##0.0000");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[i][0]+"|^|"+result[i][2]+"|^|"+result[i][4]+"|^|"+result[i][6];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }
            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
       %>
      <tr CLASS="<%=rowColor%>">
       	<td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
        <td nowrap class="normalText"><%=result[i][1]%></td>
        <td nowrap class="normalText"><A href="cooperateCooperationCost.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>&charge_detail_name=<%=result[i][2]%>" onclick="show_wait()"><%=result[i][3]%></a></td>
				<td nowrap class="normalText"><%=result[i][5]%></td>
	      <td nowrap class="normalText"><%=result[i][7]%></td>
	      <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[i][8]))%></td>
     </tr>
  <%
  					}
 					}
	} %>
       </html:table>
        </td>
      </tr>
 	</html:table>
	<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	<input type="hidden" name="subFunction" value="perparedFind"/>
</form>
</html:html>                                                                                                                                                                                                                                                 