<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/cCoopCost/cCoopCostCreate.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
  function create() {
		switch(isDouble(template.per_cost,12,4))
	    {
	      case 0 : alert('单位消耗必须为数字型'); return false;
	      case 1 : alert('单位消耗整数位不能超过12位'); return false;
	      case 3 : alert('单位消耗小数位不能超过4位'); return false;
	    }
  	if (template.set_cfg_code.value =="")
  	{
  		alert("请选择配置编码!");
  		template.set_cfg_code.focus();
  		return false;
  	}
  	if (template.charge_detail_code.value =="")
  	{
  		alert("请输入医疗项目!");
  		template.set_cfg_code.focus();
  		return false;
  	}   
  	if (template.cost_subj_code.value =="")
  	{
  		alert("请选择成本项目!");
  		template.cost_subj_code.focus();
  		return false;
  	}
  	if (template.work_code.value =="")
  	{
  		alert("请选择作业名称!");
  		template.work_code.focus();
  		return false;
  	}
  	if(template.per_cost.value == "")
  	{
  	  alert("请输入单位消耗!");
  	  template.per_cost.focus();
  	  return false;
  	}
  	    template.subFunction.value='Create';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="cooperateCooperationCost.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>
<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
	String [][]cost_subj_code = (String [][])request.getAttribute("dict_cost_subj");
	String [][]work_code = (String [][])request.getAttribute("dict_charge_work");
%>
  <!-- 标题栏 -->
	  <html:title clazz='module'>合作成本-其他合作成本添加界面 </html:title>  
  <!-- 简单信息 -->    <!-- 配置编号 医疗项目 成本项目 作业名称 单位消耗   -->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="normalText" nowrap="nowrap">配置编码：</td>
      <td class="normalText" nowrap="nowrap" >
	      <%=new Select(set_cfg_code,"set_cfg_code",request.getParameter("set_cfg_code")==null?"":request.getParameter("set_cfg_code"),false,false)%>
      </td>
    </tr>
    <tr>
    	<td class="normalText" nowrap="nowrap">医疗项目：</td>
      <td class="normalText" nowrap="nowrap" >
	      <?xml:namespace prefix="hzh"/>
	      <hzh:QInput ID="nosNamea" name="charge_detail_code" value = <%=request.getParameter("charge_detail_code")==null?"":request.getParameter("charge_detail_code")%>  AdjustVal="50" previousObj="Use_time" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="35" left="268" Lheight="6" xmlSource="dic/dict_charge_detail_LVT.xml" init="1"/>
	    </td>
    </tr>
		<tr>
      <td class="normalText" nowrap="nowrap">作业名称：</td>
      <td class="normalText"nowrap="nowrap">
      <%=new Select(work_code,"work_code",request.getParameter("work_code")==null?"":request.getParameter("work_code"),false,false)%>
      </td>
    </tr>
    <tr>     
      <td class="normalText" nowrap="nowrap">成本项目：</td>
      <td nowrap class="normalText">
      <?xml:namespace prefix="hzh"/>
	      <hzh:QInput ID="nosNamea" name="cost_subj_code" AdjustVal="145" codeCol='spell' valueCol="cost_subj_code" value="<%=request.getParameter("cost_subj_code")==null? "":request.getParameter("cost_subj_code")%>" textCol="cost_subj_name" width="200" top="65" left="430" Lheight="6" xmlSource="dic/dict_subj_cost_detail_L.xml" init="1"/>
		</tr>
    <tr>  
      <td class="normalText" nowrap="nowrap">单位消耗：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="per_cost"  maxlength="17"  class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   