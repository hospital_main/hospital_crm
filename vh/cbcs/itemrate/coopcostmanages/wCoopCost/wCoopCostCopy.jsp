<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/wCoopCost/wCoopCostCopy.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                com.viewhigh.cbcs.base.mvc.view.BiDateComponent" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
		function confrim() {
	if (template.year_month.value =="")
	{
		alert("请选择数据源的年月!");
	//	template.year_month.focus();
		return false;
	}
	if (template.year_month1.value =="")
	{
		alert("请选择目标的年月!");
	//	template.year_month1.focus();
		return false;
	}
	/*
	二层三层校验
	*/
	if(template.dept_code.value =="" && template.dept_code1.value != "")
	{
		alert("请选择数据源的科室!");
	//	template.work_code1.focus();
		return false;
	}
	if(template.dept_code.value !="" && template.dept_code1.value == "")
	{
		alert("请选择目标的科室!");
	//	template.work_code.focus();
		return false;
	}
	if(template.charge_detail_code.value != "" && template.charge_detail_code1.value == "")
	{
		alert("请选择目标的医疗项目!");
	//	template.charge_detail_code1.focus();
		return false;
	}
	if(template.charge_detail_code.value == "" && template.charge_detail_code1.value != "")
	{
		alert("请选择数据源的医疗项目!");
	//	template.charge_detail_code.focus();
		return false;
	}
	
	if(template.charge_detail_code.value != "" && template.dept_code.value == "")
	{
		alert("请先选择科室!");
	//	template.dept_code.focus();
		return false;
	}
	if(template.year_month.value == template.year_month1.value 
			&& template.charge_detail_code.value == template.charge_detail_code1.value
			&& template.dept_code.value  == template.dept_code1.value)
	{
		alert("数据源的数据不能和目标数据完全相同!");
		//template.year_month.focus();
		return false;
	}
     netA.Gsend("workloadCooperationCost.jspviewhigh?subFunction=check&year_month1=" + template.year_month1.value + "&dept_code1="+template.dept_code1.value + "&charge_detail_code1="+template.charge_detail_code1.value,copyAccess);
        
    }
    	function copyAccess(){
		var num=new Number(netA.req.responseText);
		
		if(isNaN(num)){
			alert(netA.req.responseText);
		}else{	
			if(netA.req.responseText>0){
				if(confirm("拷贝目标存在数据，确认将会覆盖已存在数据，确认？")){
					doCopy();
				}
			}else{
				doCopy();
			}
		}
  }
  function doCopy(){
		template.subFunction.value="copy";
		show_wait();
    template.submit();
    return true;
  }
    
  // 返回
     
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="workloadCooperationCost.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>    

<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
	String [][]Cost_subj_code = (String[][])request.getAttribute("dict_cost_subj");
%>
  <!-- 标题栏 -->
	 <html:title clazz='module'>合作成本-对外协作工作量复制界面  </html:title>  
  <!-- 简单信息 --> 
  <table  width="100%" cellspacing="2" border="0">
  	<tr><td colspan=2 >数据源: </td> <td>目标: </td>
    </tr>
    <tr>   
      <td class="normalText" nowrap="nowrap"> 年月:</td>
      <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
       <td class="normalText" nowrap="nowrap">年月:</td>
       <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month1", request.getParameter("year_month1"))%></td>
		</tr>
		<tr>
    	<td class="normalText" nowrap="nowrap">科室:</td>
      <td nowrap class="normalText">
        <?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="dept_code" value="<%=request.getParameter("dept_code")==null? "":request.getParameter("dept_code")%>" AdjustVal="145" previousObj="amount"  codeCol='spell' valueCol="dept_code" textCol="dept_name" width="200" top="35" left="90" Lheight="5" xmlSource="dic/asst_dict_acct_dept_LVTD.xml" init="1"/>
      </td>
	     <td class="normalText" nowrap="nowrap">科室:</td>
      <td nowrap class="normalText">
        <?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="dept_code1" value="<%=request.getParameter("dept_code1")==null? "":request.getParameter("dept_code1")%>" AdjustVal="145" previousObj="amount"  codeCol='spell' valueCol="dept_code" textCol="dept_name" width="200" top="35" left="90" Lheight="5" xmlSource="dic/asst_dict_acct_dept_LVTD.xml" init="1"/>
      </td>
    </tr>
		<tr>
      <td class="normalText" nowrap="nowrap">医疗项目:</td>
      <td nowrap class="normalText">
      	<?xml:namespace prefix="hzh"/>
				<hzh:QInput ID="nosNamea" name="charge_detail_code" value="" AdjustVal="95" previousObj="set_cfg_code" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="35" left="133" Lheight="5" xmlSource="dic/dict_charge_detail.xml" init="1"/>
      </td>
	     <td class="normalText" nowrap="nowrap">医疗项目:</td>
      <td nowrap class="normalText">
      	<?xml:namespace prefix="hzh"/>
				<hzh:QInput ID="nosNamea" name="charge_detail_code1" value="" AdjustVal="95" previousObj="set_cfg_code1" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="35" left="550" Lheight="5" xmlSource="dic/dict_charge_detail.xml" init="1"/>
      </td>
    </tr>
    
    <tr><td><br><br></td></tr>
   	<tr align="center">
      <td colSpan="4"><button class="pageBtn" onclick="return confrim();">确定</button>  
       &nbsp;&nbsp;&nbsp;&nbsp;
<!--<img src="images/confirm.gif" class="mouse" onclick="return confrim();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />-->
</td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   