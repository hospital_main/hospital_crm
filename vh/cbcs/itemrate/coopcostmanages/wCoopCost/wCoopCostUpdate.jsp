<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/wCoopCost/wCoopCostUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
  <Script Language="JavaScript">
   function save() {
		 switch(isDouble(template.work_load, 16,0)){
		      case 0 : alert('对外协作工作量必须为数字型'); return false;
		      case 1 : alert('对外协作工作量整数位不能超过16位'); return false;
		      case 3 : alert('对外协作工作量没有小数位'); return false;
		    }
        template.subFunction.value='Update';
        show_wait();
        template.submit();
        return true;
    }
// 返回
  function back( element ) {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
  <form name="template" method="post" action="workloadCooperationCost.jspviewhigh">  
  <%
  	String []result =(String[]) request.getAttribute("result");
  %>
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>合作成本-对外协作工作量修改界面 </html:title>  
  <!-- 简单信息 -->    <!-- 年月 科室 医疗项目 对外协作工作量-->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="normalText" nowrap="nowrap">年月:</td>
      <td nowrap class="normalText"><input type=text name = "year_month" class="textInputC" value="<%=result[0]%>" disabled /></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">科室:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="dept_code1" class="textInputC" value="<%=result[2]%>" disabled /></td>
    </tr>   
    <tr>
      <td class="normalText" nowrap="nowrap">医疗项目:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name = "charge_detail_code1"  class="textInputC" value="<%=result[4]%>" disabled /></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">对外协作工作量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text  maxlength="16"  name="work_load" value="<%=result[5]%>" class="textInputC"  /></td>
    </tr>
  	<tr>
      <td colspan="2"> <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
   </table>
    <input type=hidden name="subFunction" value="create"/>
    <input type=hidden name="year_month"  value="<%=result[0]%>"/>
    <input type=hidden name="dept_code"  value="<%=result[1]%>"/>
    <input type=hidden name="charge_detail_code" value="<%=result[3]%>" />
  </form>
</html:html>   