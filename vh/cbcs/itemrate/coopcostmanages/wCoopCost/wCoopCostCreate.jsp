<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/wCoopCost/wCoopCostCreate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                com.viewhigh.cbcs.base.mvc.view.BiDateComponent" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
  function create() {
		switch(isDouble(template.work_load,16,0))
	    {
	      case 0 : alert('对外协作工作量必须为数字型'); return false;
	      case 1 : alert('对外协作工作量整数位不能超过16位'); return false;
	      case 3 : alert('对外协作工作量没有小数位'); return false;
	    }
  	if (template.year_month.value =="")
  	{
  		alert("请输入年月!");
  		template.year_month.focus();
  		return false;
  	}
  	if (template.dept_code.value =="")
  	{
  		alert("请选择科室!");
  		template.dept_code.focus();
  		return false;
  	}
  	if (template.charge_detail_code.value =="")
  	{
  		alert("请输入医疗项目!");
  		template.dept_code.focus();
  		return false;
  	}   
  	if (template.work_load.value =="")
  	{
  		alert("请输入对外协作工作量!");
  		template.work_load.focus();
  		return false;
  	}
  	    template.subFunction.value='Create';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="workloadCooperationCost.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>
<%
	String [][]dept_code = (String [][])request.getAttribute("dict_acct_dept");

%>
  <!-- 标题栏 -->
	  <html:title clazz='module'>合作成本-对外协作工作量添加界面 </html:title>  
  <!-- 简单信息 -->   
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="normalText" nowrap="nowrap">年月：</td>
      <td class="normalText" nowrap="nowrap" >
	      <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
      </td>
    </tr><tr>     
      <td class="normalText" nowrap="nowrap">科室：</td>
      <td nowrap class="normalText">
       <?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="dept_code" value="<%=request.getParameter("dept_code")==null? "":request.getParameter("dept_code")%>" AdjustVal="145" previousObj="amount"  codeCol='spell' valueCol="dept_code" textCol="dept_name" width="200" top="35" left="90" Lheight="5" xmlSource="dic/asst_dict_acct_dept_LVTD.xml" init="1"/>
		  </td>
		</tr>
    <tr>
    	<td class="normalText" nowrap="nowrap">医疗项目：</td>
      <td class="normalText" nowrap="nowrap" >
	      <?xml:namespace prefix="hzh"/>
	      <hzh:QInput ID="nosNamea" name="charge_detail_code" value = <%=request.getParameter("charge_detail_code")==null?"":request.getParameter("charge_detail_code")%>  AdjustVal="50" previousObj="Use_time" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="35" left="268" Lheight="6" xmlSource="dic/dict_charge_detail_LVT.xml" init="1"/>
	    </td>
    </tr>
		<tr>  
      <td class="normalText" nowrap="nowrap">对外协作工作量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="work_load"  maxlength="17"  class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   