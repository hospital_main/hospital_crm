<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/coopCostQuery/coopCostQueryDepre.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK"  %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
		function dischange(){
				template.changesign.value="dischange";
				template.subFunction.value="change";
				show_wait();
				template.submit();
				return;
		}
		function kindchange(){
				template.changesign.value="kindchange";
				template.subFunction.value="change";
				show_wait();
				template.submit();
				return;
		}
	 	function find(){
	 		template.subFunction.value='depre';
	 		show_wait();
	    template.submit();
	 	}
	 	function inherit(){
	 		template.subFunction.value ='inherit';
	 		show_wait();
	 		template.submit();
	 	}
	  function create(){
	 	 		template.subFunction.value='preparedCreate';
	 	  	show_wait();
	 	 		template.submit();
	 	}

	  function remove() {
	    var flag = false;
	    for (var i=0; i<template.elements.length; i++) {
	          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
	            flag = true;
	    }

     	if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
	    } else {
	      alert( "请先选择,再删除!");
	      show_wait();
	      return false;
	    }
	  }
	  function selectAll(){
	    for (var i=0; i<template.elements.length; i++) {
	        if (template.elements[i].name=='primaryKey')
	            template.elements[i].checked = true;
	    }
		}
	  function back( element )
	  {
	    for(var i=0;i<template.elements.length;i++)
	          template.elements[i].value="";
	    template.subFunction.value='findAll';
	    show_wait();
	    element.submit();
	  }		
	</Script>

<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
   DecimalFormat mf=new DecimalFormat("#,##0.00");
   DecimalFormat pf=new DecimalFormat("#,##0.00%");
%>

<html:html clazz="main">
<form name="template" method="post" action="cooperationCostQuery.jspviewhigh">
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
<html:title clazz='module'>合作成本详细信息--设备折旧成本 </html:title>
   <html:table clazz="simple">
  	<tr>
		  	<td nowrap class="normalText">配置编码:</td>
				<td nowrap class="normalText"><%=request.getAttribute("set_cfg_code")%></td>
				<td nowrap class="normalText">医疗项目：</td>
			 	<td nowrap class="normalText"><%=request.getAttribute("charge_detail_name")%></td>
			 	<td >
			 		<button class="pageBtn" onclick="return preparedPrint();">打印</button>
			 		<button class="pageBtn"  onclick="return back(template);">返回</button>
			 	</td>
 		</tr>
		
</html:table>

 <html:table clazz="complex">
	<tr>
    <td>
      <html:table clazz="result">
      	<html:tr clazz='label'>
      	<td nowrap class="resultLabel">成本项目</td>
      	<td nowrap class="resultLabel">设备</td>
      	<td nowrap class="resultLabel">时间(分钟) </td>
      	<td nowrap class="resultLabel">单位折旧成本(元/分钟)</td>
      	<td nowrap class="resultLabel">折旧成本</td>
      	<td nowrap class="resultLabel">成本所占比例</td>
      	</html:tr>
     <%
     String[][] result = (String[][])request.getAttribute("result");
 			double total_sum= 0;
 	 		if (result != null) {
       
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[i][0]+"|^|"+result[i][1];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }
            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
       %>
      <tr CLASS="<%=rowColor%>">

        <td nowrap class="normalText"><%=result[i][3]%></td>
        <td nowrap class="normalText"><%=result[i][5]%></td>
        <td nowrap class="numberText"><%=result[i][6]%></td>
        <td nowrap class="numberText"><%=mf.format(new Double(result[i][7]))%></td>
        <td nowrap class="numberText"><%=mf.format(new Double(result[i][8]))%></td>
        <td nowrap class="numberText"><%=pf.format(new Double(result[i][9]))%></td>
             </tr>
  <%
  ///	total_sum = total_sum + Double.parseDouble(result[i][8]);
  					}
 					}
	} %>
	  
   </html:table>
       </td>
     </tr>
 	</html:table>
	<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	<input type="hidden" name="subFunction" value="perparedFind"/>
</form>
</html:html>