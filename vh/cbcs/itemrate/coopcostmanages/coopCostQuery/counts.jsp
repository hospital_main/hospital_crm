<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/coopCostQuery/counts.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:28 $
     $Modtime: 03-09-02 16:24 $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

  <%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
  
  
<%@ page language="java" contentType="text/html;charset=GBK"  %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ page import="java.text.*" %>
<%
  DecimalFormat mf=new DecimalFormat("#,##0.00");
%>
<html:html clazz="main">
	<body>
  	<html:title clazz='module'>合作成本查询汇总</html:title>
      <html:table clazz="simple">
      	<tr>
        	<td nowrap class="normalText">人工合作成本:</td>
        	<td nowrap class="numberText"><%=request.getAttribute("person")%></td>
  			</tr>
  			<tr>
        	<td nowrap class="normalText">材料合作成本:</td>
        	<td nowrap class="numberText"><%=request.getAttribute("con")%></td>
  			</tr>
  			<tr>
        	<td nowrap class="normalText">设备折旧成本:</td>
        	<td nowrap class="numberText"><%=request.getAttribute("depre")%></td>
  			</tr>
  			<tr>
        	<td nowrap class="normalText">其他合作直接成本:</td>
        	<td nowrap class="numberText"><%=request.getAttribute("cooperate")%></td>
  			</tr>
  			<tr>
        	<td nowrap class="normalText">合计:</td>
        	<td nowrap class="numberText"><%=request.getAttribute("total_pcd")%></td>
  			</tr>
  			<tr>
  			<td></td>
  			<td><button class="pageBtn" onClick="window.close();" >关闭</button></td></tr>
      </html:table>
  </body>
</html:html>


