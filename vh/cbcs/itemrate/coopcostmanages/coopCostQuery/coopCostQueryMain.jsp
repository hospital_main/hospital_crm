<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/coopCostQuery/coopCostQueryMain.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function dischange(){
			template.changesign.value="dischange";
			template.subFunction.value="change";
			show_wait();
			template.submit();
			return;
	}
	function kindchange(){
			template.changesign.value="kindchange";
			template.subFunction.value="change";
			show_wait();
			template.submit();
			return;
	}
	 	function find(){
	 		template.subFunction.value='findAll';
	 		show_wait();
	    template.submit();
	 	}
	 	function inherit(){
	 		template.subFunction.value ='inherit';
	 		show_wait();
	 		template.submit();
	 	}
	  function create(){
	 	 		template.subFunction.value='preparedCreate';
	 	  	show_wait();
	 	 		template.submit();
	 	 		}

	  function remove() {
	    var flag = false;
	    for (var i=0; i<template.elements.length; i++) {
	          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
	            flag = true;
	      }

	     if( flag!=false) {
	        if (confirm('是否删除')) {
	          template.subFunction.value='remove';
	          template.submit();
	          return true;
	        } else
	            return false;
	    } else {
	      alert( "请先选择,再删除!");
	      show_wait();
	      return false;
	    }
	  }
	  function selectAll(){
	    for (var i=0; i<template.elements.length; i++) {
	        if (template.elements[i].name=='primaryKey')
	            template.elements[i].checked = true;
	    }
		}
	//汇总
	function accout() {
    window.showModalDialog("cooperationCostQuery.jspviewhigh?signs=collect&subFunction=findAll"
        +"&person=" +template.person.value
				+"&con=" +template.con.value 
        +"&depre="+template.depre.value
        +"&cooperate="+template.cooperate.value
        +"&total_pcd=" + template.total_pcd.value,window,"dialogHeight: 220px; dialogWidth: 500px;");
	}
	</Script>

<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
	DecimalFormat mf=new DecimalFormat("#,##0.00");
	DecimalFormat sf=new DecimalFormat("#,##0.0000");
%>

<html:html clazz="main">
<form name="template" method="post" action="cooperationCostQuery.jspviewhigh">
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
<html:title clazz='module'>合作成本查询 </html:title>
   <html:table clazz="simple">
  	<tr>
		  	<td nowrap class="normalText">配置编码:</td>
				<td nowrap class="normalText"><%=new Select(set_cfg_code,"set_cfg_code",request.getParameter("set_cfg_code")==null? "":request.getParameter("set_cfg_code"),false,false)%></td>
				<td nowrap class="normalText">医疗项目：</td>
			 	<td >
				 	<?xml:namespace prefix="hzh"/>
	        <hzh:QInput ID="nosNamea" name="charge_detail_code" value="" AdjustVal="95" previousObj="set_cfg_code" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="29" left="330" Lheight="5" xmlSource="dic/dict_charge_detail.xml" init="1"/>
				</td>
				<td align = right colspan = 4>
				<button class="pageBtn" name="" onclick="find();" >查询</button>
				<button class="pageBtn" name=""   onclick="accout()"  >汇总</button> 
				<button class="pageBtn" onclick="return preparedPrint();">打印</button>
				<!--<img src="images/find.gif" class="mouse" onclick="find();">
				<img src="images/huizong.GIF" class="mouse" onclick="accout();" />
				<img src="images/print.gif" class="mouse" onclick="return preparedPrint();" />-->
				</td>
	  </tr>
</html:table>
<html:table clazz="simple"><tr><td><html:title clazz='table'>合作成本查询 </html:title></td></tr></html:table>
<%
		double person = 0;//人工成本合计
		double con = 0;//材料成本合计
		double depre = 0;//设备成本合计
		double cooperate = 0;//其他合作直接成本合计
    double total_pcd = 0;//人工,材料,设备折旧的总计
%>
 <html:table clazz="complex">
	<tr>
    <td>
      <html:table clazz="result">
      	<html:tr clazz='label'>
      	<td nowrap class="resultLabel">配置编码</td>
      	<td nowrap class="resultLabel">医疗项目</td>
      	<td nowrap class="resultLabel">人工合作成本 </td>
      	<td nowrap class="resultLabel">材料合作成本</td>
      	<td nowrap class="resultLabel">设备折旧成本</td>
      	<td nowrap class="resultLabel">其他合作直接成本</td>
      	<td nowrap class="resultLabel">小计 </td>
      	</html:tr>
  <%
  	String[][] result = (String[][])request.getAttribute("result");
  	if (result != null) {
       if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[i][0]+"|^|"+result[i][2];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }
            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
   %>
      <tr CLASS="<%=rowColor%>">
       
        <td nowrap class="normalText"><%=result[i][1]%></td>
        <td nowrap class="normalText"><%=result[i][3]%></td>
        <td nowrap class="numberText"><A href="cooperationCostQuery.jspviewhigh?subFunction=detail&charge_detail_name=<%=result[i][3]%>&set_cfg_code=<%=result[i][0]%>&charge_detail_code=<%=result[i][2]%>" ><%=mf.format(new Double(result[i][4]))%></a></td>
	      <td nowrap class="numberText"><A href="cooperationCostQuery.jspviewhigh?subFunction=consume&charge_detail_name=<%=result[i][3]%>&set_cfg_code=<%=result[i][0]%>&charge_detail_code=<%=result[i][2]%>" ><%=mf.format(new Double(result[i][5]))%></a></td>
   			<td nowrap class="numberText"><A href="cooperationCostQuery.jspviewhigh?subFunction=depre&charge_detail_name=<%=result[i][3]%>&set_cfg_code=<%=result[i][0]%>&charge_detail_code=<%=result[i][2]%>" ><%=mf.format(new Double(result[i][6]))%></a></td>
   			<td nowrap class="numberText"><A href="cooperationCostQuery.jspviewhigh?subFunction=cooperate&charge_detail_name=<%=result[i][3]%>&set_cfg_code=<%=result[i][0]%>&charge_detail_code=<%=result[i][2]%>" ><%=mf.format(new Double(result[i][8]))%></a></td>
	      <td nowrap class="numberText"><%=mf.format(new Double(result[i][7]))%></td>
     </tr>
  <%
  	person = person + Double.parseDouble(result[i][4]);
  	con = con + Double.parseDouble(result[i][5]);
  	depre = depre + Double.parseDouble(result[i][6]);
  	cooperate =  cooperate + Double.parseDouble(result[i][8]);
 					}
 		total_pcd = person + con + depre + cooperate;
 	//	person = (new Double(new DecimalFormat(".0000").format(person))).doubleValue();
 	//	con = (new Double(new DecimalFormat(".0000").format(con))).doubleValue();
 	//	depre = (new Double(new DecimalFormat(".0000").format(depre))).doubleValue();
 	//	cooperate = (new Double(new DecimalFormat(".0000").format(cooperate))).doubleValue();
 	//	total_pcd = (new Double(new DecimalFormat(".0000").format(total_pcd))).doubleValue();
 		}
	} %>
       </html:table>
        </td>
      </tr>
 	</html:table>
	<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	<input type="hidden" name="subFunction" value="perparedFind">
	<input type="hidden" name="person" value=<%=sf.format(person)%>>
	<input type="hidden" name="con" value=<%=sf.format(con)%>>
	<input type="hidden" name="depre" value=<%=sf.format(depre)%>>
	<input type="hidden" name="cooperate" value=<%=sf.format(cooperate)%>>
	<input type="hidden" name="total_pcd" value=<%=sf.format(total_pcd)%>>
</form>
</html:html>                                                                                                                                                                                                                                                 