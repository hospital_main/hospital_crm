<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/coopCostQuery/coCostQueryM.jsp,v 1.1 2012/03/12 01:58:28 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:28 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function dischange(){
			template.changesign.value="dischange";
			template.subFunction.value="change";
			show_wait();
			template.submit();
			return;
	}
	function kindchange(){
			template.changesign.value="kindchange";
			template.subFunction.value="change";
			show_wait();
			template.submit();
			return;
	}
	 	function find(){
	 		template.subFunction.value='findAll';
	 		show_wait();
	    template.submit();
	 	}
	 	function inherit(){
	 		template.subFunction.value ='inherit';
	 		show_wait();
	 		template.submit();
	 	}
	  function create(){
	 	 		template.subFunction.value='preparedCreate';
	 	  	show_wait();
	 	 		template.submit();
	 	 		}

	  function remove() {
	    var flag = false;
	    for (var i=0; i<template.elements.length; i++) {
	          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
	            flag = true;
	      }

	     if( flag!=false) {
	        if (confirm('是否删除')) {
	          template.subFunction.value='remove';
	          template.submit();
	          return true;
	        } else
	            return false;
	    } else {
	      alert( "请先选择,再删除!");
	      show_wait();
	      return false;
	    }
	  }
	  function selectAll(){
	    for (var i=0; i<template.elements.length; i++) {
	        if (template.elements[i].name=='primaryKey')
	            template.elements[i].checked = true;
	    }
		}
	</Script>

<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
%>

<html:html clazz="main">
<form name="template" method="post" action="cooperationCostQuery.jspviewhigh">
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
<html:title clazz='module'>合作成本详细信息--材料成本</html:title>
   <html:table clazz="simple">
  	<tr>
		  	<td nowrap class="normalText">配置编码:</td>
				<td nowrap class="normalText"><%=new Select(set_cfg_code,"set_cfg_code",request.getParameter("set_cfg_code")==null? "":request.getParameter("set_cfg_code"),false,false)%></td>
				<td nowrap class="normalText">医疗项目：</td>
			 	<td >
				 	<?xml:namespace prefix="hzh"/>
	        <hzh:QInput ID="nosNamea" name="charge_detail_code" value="" AdjustVal="95" previousObj="set_cfg_code" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="39" left="330" Lheight="5" xmlSource="dic/dict_charge_detail.xml" init="1"/>
				</td>
				<td align = right colspan = 4>
				<button class="pageBtn" onclick="find();">打印</button>
				</td>
	  </tr>
</html:table>
<html:table clazz="simple"><tr><td><html:title clazz='table'>合作成本查询 </html:title></td></tr></html:table>
<%
  BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	if (ro != null) {

    TableMarge oper = new TableMarge(ro, "return find()");
   // oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
   // oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
  //  oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
  //  oper.addNeedButton("images/create.gif", "return create()");     //  添加
  //  oper.addOptionButton("images/copy.gif", "return inherit()");     //  复制


%>
 <html:table clazz="simple"><tr><td colspan="2"><%=oper%></td></tr></html:table>
<% } else { %>
 <html:table clazz="simple"><tr><td></td></tr></html:table>
<% }  %>
 <html:table clazz="complex">
	<tr>
    <td>
      <html:table clazz="result">
      	<html:tr clazz='label'>
      	<td nowrap class="resultLabel">材料类别</td>
      	<td nowrap class="resultLabel">材料名称</td>
      	<td nowrap class="resultLabel">数量</td>
      	<td nowrap class="resultLabel">单价</td>
      	<td nowrap class="resultLabel">成本金额</td>
      	<td nowrap class="resultLabel">成本所占比例</td>
      	</html:tr>
     <%

 	 if (ro != null) {
        String[][] result = ro.getTableResult();
         DecimalFormat mf=new DecimalFormat("#,##0.00");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[i][0]+"|^|"+result[i][2];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }
            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
       %>
      <tr CLASS="<%=rowColor%>">
       
        <td nowrap class="normalText"><%=result[i][1]%></td>
        <td nowrap class="normalText"><%=result[i][3]%></td>
        <td nowrap class="numberText"><A href="cooperationCostQuery.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>&charge_detail_name=<%=result[i][4]%>" onclick="show_wait()"><%=mf.format(Double.parseDouble(result[i][4]))%></a></td>
	      <td nowrap class="numberText"><A href="cooperationCostQuery.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>&charge_detail_name=<%=result[i][5]%>" onclick ="show_wait()"><%=mf.format(Double.parseDouble(result[i][5]))%></a></td>
   			<td nowrap class="numberText"><A href="cooperationCostQuery.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>&charge_detail_name=<%=result[i][6]%>" onclick ="show_wait()"><%=mf.format(Double.parseDouble(result[i][6]))%></a></td>
	      <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[i][7]))%></td>
	      
     </tr>
  <%
  					}
 					}
	} %>
       </html:table>
        </td>
      </tr>
 	</html:table>
	<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	<input type="hidden" name="subFunction" value="perparedFind"/>
</form>
</html:html>                                                                                                                                                                                                                                                 