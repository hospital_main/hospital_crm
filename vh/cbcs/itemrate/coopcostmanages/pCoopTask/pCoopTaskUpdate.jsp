<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/pCoopTask/pCoopTaskUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp"%>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  //添加
function save() {
		switch(isDouble(template.Worker_num, 4,0))
		{
			case 0 : alert('工作人员数量必须为数字型'); return false;
			case 1 : alert('工作人员数量不能大于4个字符'); return false;
			case 3 : alert('工作人员数量没有小数'); return false;
		}
				switch(isDouble(template.Work_time,4,2))
		{
			case 0 : alert('作业时间必须为数字型'); return false;
			case 1 : alert('作业时间不能大于4个字符'); return false;
			case 3 : alert('作业时间有两位小数'); return false;
		}
		
    template.subFunction.value='Update';
    show_wait();
    template.submit();
    return true;
}
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
  
<html:html clazz="main">
<form name="template" method="post" action="personCooperationTask.jspviewhigh">  
<%
	String []result =(String[]) request.getAttribute("result");
%>
  <!-- 信息提示栏 -->
<html:message/>
  <!-- 标题栏 -->
	  <html:title clazz='module'>合作成本-人工合作作业修改界面 </html:title>  
  <!-- 简单信息 -->    <!-- 配置编码 医疗项目 作业序号 作业名称 作业时间（分钟） 说明 111, 0004, 1, ADFSAF, 12, 12  -->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="normalText" nowrap="nowrap">配置编号:</td>
      <td nowrap class="normalText"><input type=text class="textInputC" value="<%=result[0]%>" disabled /></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">医疗项目:</td>
      <td class="normalText" nowrap="nowrap"><input type=text   class="textInputC" value="<%=result[8]%>" disabled /></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">作业名称:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="work_code" class="textInputC" value="<%=result[3]%>" disabled /></td>
    </tr>   
    <tr>
      <td class="normalText" nowrap="nowrap">职称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="title_code" value="<%=result[4]%>" class="textInputC" disabled /></td>
    </tr>
      <td class="normalText" nowrap="nowrap">工作人员数量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text  maxlength="16"  name="Worker_num" value="<%=result[5]%>" class="textInputC" /></td>
    </tr>
    <tr>
    	<td class="normalText" nowrap="nowrap">单位作业时间：</td>
      <td class="normalText" nowrap="nowrap"><input type=text  maxlength="17"  name="Work_time" value="<%=result[6]%>" class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return back(template);">返回</button>  
     <!-- <img src="images/save.gif" class="mouse" onclick="return save();" /> 
     <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
<img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="Create"/>
  <input type=hidden name="set_cfg_code" class="textInputC" value="<%=result[0]%>"/></td>
  <input type=hidden name="charge_detail_code" value="<%=result[1]%>" /></td>
  <input type=hidden name="work_code" value="<%=result[2]%>" /></td>
  <input type=hidden name="title_code" value="<%=result[7]%>" /></td>
  
</form>

</html:html>  
