<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/coopcostmanages/pCoopTask/pCoopTaskCreate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
	function create() {
	
	switch(isDouble(template.amount, 16,0))
	    {
	      case 0 : alert('工作人员数量必须为数字型'); return false;
	      case 1 : alert('工作人员数量不能大于16个字符'); return false;
	      case 3 : alert('工作人员数量没有小数');return false;
	    }
	if (template.set_cfg_code.value =="")
	{
		alert("请选择配置编码!");
		template.set_cfg_code.focus();
		return false;
	}
	
	if (template.work_code.value =="")
	{
		alert("请选择作业名称!");
		template.work_code.focus();
		return false;
	}
	if (template.title_code.value =="")
	{
		alert("请选择工作人员职称!");
		template.title_code.focus();
		return false;
	}
	if(template.amount.value =="")
	{
		alert("请输入工作人员数量!");
		template.amount.focus();
		return false;
	}
	if(template.time.value =="")
	{
		alert("请输入单位作业时间!");
		template.time.focus();
		return false;
	}
	
 switch(isDouble(template.time, 14,2))
	    {
	      case 0 : alert('作业时间必须为数字型'); return false;
	      case 1 : alert('作业时间不能大于14个字符'); return false;
	      case 3 : alert('作业时间有两位小数'); return false;
	    }
if (template.charge_detail_code.value =="")
	{
		alert("请输入医疗项目!");
		template.charge_detail_code.focus();
		return false;
	}
        template.subFunction.value='Create';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="personCooperationTask.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>
<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
	String [][]charge_detail_code = (String [][])request.getAttribute("dict_income_subj");
	String [][]work_code = (String [][])request.getAttribute("b_dict_charge_detail_kind");
	String [][]title_code = (String[][])request.getAttribute("title_code");
%>
  <!-- 标题栏 -->
	  <html:title clazz='module'>合作成本-人工合作作业维护添加界面 </html:title>  
  <!-- 简单信息 -->    <!-- 配置编号 医疗项目 作业编号 职称 工作人员数量 单位作业时间     -->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="normalText" nowrap="nowrap">配&nbsp;置&nbsp;编&nbsp;号&nbsp;：</td>
      <td class="normalText" nowrap="nowrap" ><%=new Select(set_cfg_code,"set_cfg_code","",false,false)%></td>
    </tr>
     <tr>
      <td class="normalText" nowrap="nowrap">收&nbsp;费&nbsp;项&nbsp;目&nbsp;：</td>
      <td class="normalText" nowrap="nowrap" >
      		<?xml:namespace prefix="hzh"/>
	        <hzh:QInput ID="nosNamea" name="charge_detail_code" value="" AdjustVal="95" previousObj="title_code" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="12" left="268" Lheight="5" xmlSource="dic/dict_charge_detail.xml" init="1"/>
      	
      </td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">作业名称：</td>
      <td nowrap class="normalText">
	      <%=new Select(work_code,"work_code","",false,false)%></td>
		</tr>
		<tr>
		
			<td class="normalText" nowrap="nowrap">职称：</td><td class="normalText"nowrap="nowrap">
       <%=new Select(title_code,"title_code","",false,false)%>
      </td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">工作人员数量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="amount"  maxlength="16"  class="textInputC" /></td>
    </tr>
    <tr>
       <td class="normalText" nowrap="nowrap">单位作业时间：</td>
       <td class="normalText" nowrap="nowrap"><input type=text name="time"  maxlength="17"  class="textInputC" /></td>
    </tr>
   
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   