<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/datacollection/rateincomemaintenance/rateIncomeMaintenanceUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Revision: 1.1 $
-->


<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,
                com.viewhigh.cbcs.base.mvc.view.BiDateComponent" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function save() {

    switch(isDouble(template.amount,10,2))
    {
      case 0 : alert('金额必须为数字型'); return;
      case 1 : alert('金额整数部分不能高于10个字符'); return;
      case 2 : alert('金额没有整数部分'); return;
      case 3 : alert('金额小数部分不能高于2个字符'); return;
    }
        switch(isDouble(template.workload,10,0))
    {
      case 0 : alert('工作量必须为数字型'); return;
      case 1 : alert('工作量整数部分不能高于10个字符'); return;
      case 2 : alert('工作量没有整数部分'); return;
      case 3 : alert('工作量小数部分不能高于4个字符'); return;
    }

		template.subFunction.value='save';
		show_wait();
    template.submit();
		return true;
	}

function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='preparedfindAll';
    window.opener.template.subFunction.value='preparedfindAll'
    window.opener.document.template.submit()
    self.close();
  }
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="itemmaintenance.jspviewhigh">
     <!-- 信息提示栏 -->
	  <html:message/>

     <!--信息栏-->
	  <html:title clazz='module'>项目收入修改页面</html:title>

     <!-- 简单信息 -->
	  <html:table clazz="simple">
     <%String[][] result=(String[][])request.getAttribute("result");%>
      <tr>
       <td nowrap class="signText">统计年月：</td>
       <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0][0]%>" disabled />
        <input type=hidden name="year_month" value="<%=result[0][0]%>">
      </td>
      </tr>
     <tr>
      <td nowrap class="signText">医疗项目名称：</td>
       <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text   value="<%=result[0][2]%>" disabled />
        <input type=hidden  name="charge_detail_code" value="<%=result[0][1]%>">
      </td>
      </tr>
      <tr>
        <td nowrap class="signText">开单科室：</td>
       <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text  value="<%=result[0][4]%>" disabled />
         <input type=hidden  name="order_by" value="<%=result[0][3]%>">
      </td>
       </tr>
      <tr>
      <td nowrap class="signText">执行科室：</td>
       <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text  value="<%=result[0][6]%>" disabled />
        <input type=hidden  name="perform_by" value="<%=result[0][5]%>">
      </td>
      </tr>
      <tr>
        <td nowrap class="signText">金额：</td>
        <td nowrap class="normalText"><input type="text" name="amount" size="30" value="<%=result[0][7]%>" /></td>
        </tr>
              <tr>
        <td nowrap class="signText">工作量：</td>
        <td nowrap class="normalText"><input type="text" name="workload" size="30" maxlength="10" value="<%=result[0][8]%>" /></td>
        </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return self.close();" >关闭</button> 
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" /> <img src="images/close.gif" class="mouse" onclick="return self.close();" />--></td>
    </tr>
	  </html:table>
      <br>
      <input type=hidden name="subFunction"/>
  	</form>


</html:html>
