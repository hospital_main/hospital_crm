<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/datacollection/rateincomemaintenance/rateIncomeMaintenanceCreate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Revision: 1.1 $ 
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,
                com.viewhigh.cbcs.base.mvc.view.BiDateComponent" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function create() {
    if(template.year_month.value=="")  {
	      alert('统计年月不能为空!');
	      return;
	    }
    if(template.charge_detail_code.value=="")  {
	      alert('医疗项目不能为空!');
	      return;
	    }
     if(template.order_by.value=="")  {
      alert('开单科室不能为空!');  
      return;
    }
    if(template.perform_by.value=="")   {
      alert('执行科室不能为空!');
      return;
    }
    switch(isDouble(template.amount,10,2))
    {
      case 0 : alert('金额必须为数字型'); return;
      case 1 : alert('金额整数部分不能高于10个字符'); return;
      case 2 : alert('金额没有整数部分'); return;
      case 3 : alert('金额小数部分不能高于2个字符'); return;
    }
        switch(isDouble(template.workload,10,0))
    {
      case 0 : alert('工作量必须为数字型'); return;
      case 1 : alert('工作量整数部分不能高于10个字符'); return;
      case 2 : alert('工作量没有整数部分'); return;
      case 3 : alert('工作量小数部分不能高于4个字符'); return;
    }

		template.subFunction.value='create';
		show_wait();
    template.submit();
		return true;
	}

	function back( element ) {
		for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='preparedfindAll';
    show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="rateIncomeMaintenance.jspviewhigh">
     <!-- 信息提示栏 -->
	  <html:message/>

     <!--信息栏-->
	  <html:title clazz='module'>项目收入添加页面</html:title>

     <!-- 简单信息 -->
	  <html:table clazz="simple">
      <tr>
       <td nowrap class="signText">统计年月：</td>
       <td class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
      </tr>
     <tr>
     <td nowrap class="signText">医疗项目名称：</td>
        <td nowrap class="normalText">
        <?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea" name="charge_detail_code"  AdjustVal="95" previousObj="year_month" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="13" left="255" Lheight="5" xmlSource="dic/dict_charge_detail_LVT.xml" init="1"/>
       </tr>

      <tr>
        <td nowrap class="signText">开单科室：</td>
         <td nowrap class="normalText">
          <hzh:QInput ID="nosNameb" name="order_by"  AdjustVal="105" previousObj="year_month" codeCol='spell' valueCol="dept_code" textCol="dept_name" Lwidth="100"  top="-13" left="255" Lheight="5" width="200" xmlSource="dic/dict_acct_dept_LVT.xml" init="1" />
       </tr>
       <tr>
       <td nowrap class="signText">执行科室：</td>
        <td nowrap class="normalText">
         <hzh:QInput ID="nosNamec" name="perform_by"  AdjustVal="120" previousObj="year_month" codeCol='spell' valueCol="dept_code" textCol="dept_name" Lwidth="100"  top="-40" left="255" width="200" Lheight="5" xmlSource="dic/asst_dict_acct_dept_LVTD.xml" init="1" />
      </tr>
      <tr>
        <td nowrap class="signText">金额：</td>
        <td nowrap class="normalText"><input type="text" name="amount" size="30" value="<%=request.getParameter("amount")==null? "0.00":request.getParameter("amount")%>" /></td>
        </tr>
      <tr>
        <td nowrap class="signText">工作量：</td>
        <td nowrap class="normalText"><input type="text" name="workload" size="30" maxlength="10" value="<%=request.getParameter("workload")==null? "0":request.getParameter("workload")%>" /></td>
       </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
	  </html:table>
      <br>
      <input type=hidden name="subFunction"/>
  	</form>

</html:html>



