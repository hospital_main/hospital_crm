<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/costratecaculate/deptRate/deptRateMain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
com.viewhigh.cbcs.cbcs.util.DisplayWidth,
com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script language="javascript">

	function Release(){
	if(template.dept_code.value==""){
		alert('请选择科室');
		return;
	}	
	if(template.charge_ra_code.value==""){
		alert('请选择配置文件');
		return;
		}
  if(template.year_month.value==""){
		alert('请选择启用年月');
		return;
		} 

	if(confirm("如果继续将覆盖掉以前发布，是否继续？" )){
    show_wait();
    template.subFunction.value="Release";
    template.submit();
    return;
 	}
	}
</Script> 

<html:html clazz="main" isPrint="true" fixRows="2">
<form name="template" method="post" action="deptRate.jspviewhigh">
<html:message/>
<html:title clazz='module'>单位成本发布(按科室)</html:title>
<table  width="100%" cellspacing="2" border="0" >
		<tr>	   
			<td class="signText">文件编号：</td>
			<td class="normaltext">
		<%
			 Select charge_ra_code= new  Select(request.getAttribute("charge_ra_code"),"charge_ra_code",request.getParameter("charge_ra_code")==null?"":request.getParameter("charge_ra_code"),false,false);
		%>
		<%= charge_ra_code.toString()%>
			</td>		 

		<tr>
			<td class="signText">科室:</td>
			<td class="normalText"><%
			 Select dept_code= new  Select(request.getAttribute("dept_code"),"dept_code",request.getParameter("dept_code")==null?"":request.getParameter("dept_code"),false,false);
		 %>
		 <%= dept_code.toString()%>
			</td>
		</tr>
		<tr>
			<td class="signText">发布应用启始年月：</td>
			<td class="normalText"><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td>
			<td><img src="images/send.gif" class="mouse" onclick="return Release();"/></td>			
    	</tr>
    </table>
				<input type="hidden" name="subFunction" value="findAll">
	</form>
</html:html>