<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/costratecaculate/costratecacul/costRateCaculMain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
com.viewhigh.cbcs.cbcs.util.DisplayWidth,java.text.*,
com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script language="javascript">
function code_change(){

		if(template.set_cfg_code.value==""){
			alert('配置编码不能为空')
			return;
		}
	  show_wait();
    template.subFunction.value="Change";
    template.submit();
    return;
}

function Cacul(){
		if(template.set_cfg_code.value==""){
			alert('配置编码不能为空')
			return;
		} 
		if(template.charge_ra_code.value==""){
			alert('文件编号不能为空')
			return;
		}     
    show_wait();
    template.subFunction.value="Cacul";
    template.submit();
    return;
}
</Script> 
<%
	String [][]result = (String[][])request.getAttribute("result");
	String ymf=(String)request.getAttribute("ymf");
	String ymt=(String)request.getAttribute("ymt");
	 	String  dateTitle="x年x月 到 x年x月";
	if(ymf!=null&&ymt!=null){
		dateTitle = ymf.substring(0,4)+"年"+ymf.substring(4,6)+"月 －"+ymt.substring(0,4)+"年"+ymt.substring(4,6)+"月";
	}
%>
<html:html clazz="main" isPrint="true" fixRows="2">
<form name="template" method="post" action="costRateCacul.jspviewhigh">

<html:message/>
<html:title clazz='module'>单位成本核算</html:title>

<table  width="100%" cellspacing="2" border="0" >
		<tr>	   
			<td class="signText">配置编码:</td>
			<td class="normalText">
		<%                                                                                                                                                                                                                                                 
			 	Select set_cfg_code=new Select(request.getAttribute("dict_charge_set_con"),"set_cfg_code",request.getParameter("set_cfg_code")==null? "":request.getParameter("set_cfg_code"),false,false);                                           
			 	set_cfg_code.setAttribute("onchange","code_change()");                                                                                                                                                                                           
		%>                                                                                                                                                                                                                                                 
			 	<%=set_cfg_code.toString() %></td>   </tr>
		<tr>		
			<td class="signText">数据源：</td>
			<td class="normalText"> <%=dateTitle%> </td>
		</tr>
		<tr>
			<td class="signText">文件编号：</td>
			<td><input type="text" name = "charge_ra_code" value="<%=request.getParameter("charge_ra_code")==null?"":request.getParameter("charge_ra_code")%>"></td>
			<td><button class="pageBtn" onclick="return Cacul();">计算</button></td>			
    	</tr>

    </table>
    
	  <!-- 复杂信息 -->
  <%
      if(result!=null){
  %>
     <!-- 复杂信息 -->
  <html:table clazz="complex">
   <!-- 操作 -->
   <tr>
 			<td>
 	    <html:table clazz="result" divHeight="280">
 	      <html:tr clazz='label'>
 	         <td nowrap="nowrap" class="resultLabel">序号</td>
 	         <td nowrap="nowrap" class="resultLabel">问题描述</td>
 	      </html:tr>
    <%
       for (int i = 0;i < result.length; i++ ) {
    %>
 	    <tr >
 	       <td class="normalText" nowrap="nowrap" ><%=i+1%></td>
 	       <td class="normalText" nowrap="nowrap" ><%=result[ i ][1]%></td>
 	     </tr>
 	   <% } %> 
     </html:table>
     </td>
   </tr>
 </html:table>
   <%
     }
   %>
	<input type="hidden" name="subFunction" value="findAll">				
	</form>
</html:html>