<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/costratecaculate/deptInnerChargeItem/deptInnerChargeItemMain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
com.viewhigh.cbcs.cbcs.util.DisplayWidth,
com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script language="javascript">
	function dischange(){
		template.changesign.value="dischange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function kindchange(){
		template.changesign.value="kindchange";
		template.subFunction.value="change";
		show_wait();
		template.submit();
		return;
	}
	function Release(){
	
	if(template.dept_code.value==""){
		alert('请选择科室');
		return;
	}
	if(template.year_month.value==""){
		alert('请选择启用年月');
		return;
		}
	if(template.charge_ra_code.value==""){
		alert('请选择配置文件');
		return;
		}
	if(template.dict_income_subj.value==""){
			alert('请选择收入项目'); 
			return;
		}
		if(template.b_dict_charge_detail_kind.value==""){
			alert('选择收费类别');
			return;
		}
		if(template.dict_charge_detail.value==""){
			alert('医疗项目不能为空');
			return;
		}
	if(confirm("如果继续将覆盖掉以前发布，是否继续？" )){
    show_wait();
    template.subFunction.value="Release";
    template.submit();
    return;
 	}
}
</Script> 

<html:html clazz="main" isPrint="true" fixRows="2">
<form name="template" method="post" action="deptInnerChargeItem.jspviewhigh">
<html:message/>
<html:title clazz='module'>单位成本发布(科室内项目)</html:title>
<table  width="100%" cellspacing="2" border="0" >
		<tr>
			<td class="signText">科室:</td>
			<td class="normalText"><%
			 Select dept_code= new  Select(request.getAttribute("dept_code"),"dept_code",request.getParameter("dept_code")==null?"":request.getParameter("dept_code"),false,false);
		 %>
		 <%= dept_code.toString()%>
			</td>
			<td class="signText">收入项目：</td>
			<td class="normalText">
		<%
		Select dict_income_subj=new Select(request.getAttribute("dict_income_subj"),"dict_income_subj",request.getParameter("dict_income_subj")==null? "":request.getParameter("dict_income_subj"),false,false);
		dict_income_subj.setAttribute("onchange","dischange()");
		%>
		<%=dict_income_subj.toString() %></td>

		</tr>

		<tr>	   
			<td class="signText">文件编号：</td>
			<td class="normaltext">
		<%
			 Select charge_ra_code= new  Select(request.getAttribute("charge_ra_code"),"charge_ra_code",request.getParameter("charge_ra_code")==null?"":request.getParameter("charge_ra_code"),false,false);
			 charge_ra_code.setAttribute("MULTIPLE","true");
		%>
		<%= charge_ra_code.toString()%>
			</td>		 
			<td class="signText">收费类别：</td>
			<td class="normalText">
		<%
		Select b_dict_charge_detail_kind=new Select(request.getAttribute("b_dict_charge_detail_kind"),"b_dict_charge_detail_kind",request.getParameter("b_dict_charge_detail_kind")==null? "":request.getParameter("b_dict_charge_detail_kind"),false,false);
		b_dict_charge_detail_kind.setAttribute("onchange","kindchange()");
		%>
		<%=b_dict_charge_detail_kind.toString() %></td>
		<tr>		
			<td class="signText">发布应用时间范围：启始年月</td>
			<td class="normalText"><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td>
			<td class="signText">医疗项目: </td>
			<td class="normalText">
		<%
		Select dict_charge_detail=new Select(request.getAttribute("dict_charge_detail"),"dict_charge_detail",request.getParameter("dict_charge_detail")==null? "":request.getParameter("dict_charge_detail"),false,false);
		dict_charge_detail.setAttribute("MULTIPLE","true");
		%>
		<%=dict_charge_detail.toString() %>
			</td>

			<td><img src="images/send.gif" class="mouse" onclick="return Release();"/></td>			
    	</tr>
    </table>
				<input type="hidden" name="subFunction" value="Release">
				<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	</form>
</html:html>