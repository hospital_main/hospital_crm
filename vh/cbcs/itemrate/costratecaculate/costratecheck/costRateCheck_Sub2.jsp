<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/costratecaculate/costratecheck/costRateCheck_Sub2.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.util.Preference,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DisplayWidth"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
	var itemsumHand2;
</Script>
<html:html clazz="main">
<form name="template" method="post" action="costRateCheck.jspviewhigh">
      <html:message/>
      <html:title clazz='module'> 科室医疗项目单位成本(成本分类) </html:title> 
      <html:table clazz="simple">
 			     <tr> <td  nowrap class="signText">作业套：  <%= request.getParameter("charge_ra_code")%> </td></tr>
			     <tr> <td  nowrap class="signText">科室： <%= request.getParameter("dept_name")%></td> </tr>
			     <tr> <td  nowrap class="signText">医疗项目： <%= request.getParameter("charge_detail_name")%></td> </tr>
</html:table>
	
   <vh:vhFixTable fixRow=1 fixCol=0>
      <table width = '100%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt'  >
        <colgroup id=tg>
        <col style = <%=DisplayWidth.NAME_WIDTH%> >

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
				<col style = <%=DisplayWidth.MONEY_WIDTH%>>       
      </colgroup>
			<tr clazz='label'>		    
				<td   nowrap class="resultLabel" ><div align="center">成本分类</div></td>
				<td   nowrap class="resultLabel" ><div align="center">单位成本</div></td>
				<td   nowrap class="resultLabel" ><div align="center">直接归集</div></td>
				<td   nowrap class="resultLabel" ><div align="center">计算计入</div></td>     
				<td   nowrap class="resultLabel" ><div align="center">合作成本</div></td>   
			</tr>
        <%
        	String isEcAcomp=request.getAttribute("isEcAcomp").toString().trim();//单独计算
	        String IsCdirect=request.getAttribute("IsCdirect").toString().trim();//单独采集

          String[][] result = (String[][])request.getAttribute("result");
          DecimalFormat mf=new DecimalFormat("#,##0.0000");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }
						if(!(result[i][2].equals("0.0000") && result[i][3].equals("0") && result[i][4].equals("0")))
						{
							String rowColor = "rowGray";
	            if (i/2*2==i) rowColor = "rowWhite";
        %>

       <tr CLASS="<%=rowColor%>">
           <td nowrap class="normalText"><%=result[i][0]%>
      <% //如果此记录行不等于合计，或者是成本分类为人力成本或者是直接材料的，并且相应的是单独计算或者单独采集的，则增加连接
      	if(!result[i][0].trim().equals("合计")&&result[i][7].trim().equals("Y")&&
      		(result[i][5].equals("1")||result[i][5].equals("2")||result[i][5].equals("5"))
      		&&(IsCdirect.equals("true")||isEcAcomp.equals("true"))
      		)
      { %>      
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 1 ]))%></td>
            <td nowrap class="numberText">
                <a href="#" onclick="   var tim=new Date(); itemsumHand2 = window.open('costRateCheck.jspviewhigh?subFunction=findsub3&charge_ra_code=<%=request.getParameter("charge_ra_code")%>&ymf=<%=request.getParameter("ymf")%>&ymt=<%=request.getParameter("ymt")%>&charge_detail_name=<%=request.getParameter("charge_detail_name")%>&charge_detail_code=<%=request.getParameter("charge_detail_code")%>&dept_code=<%=request.getParameter("dept_code")%>&dept_name= <%= request.getParameter("dept_name")%>&cost_class_code=<%=result[i][6]%>&cost_class_name=<%=result[i][0]%>&is_direct=<%=result[i][5]%>&time='+tim.toLocaleString(),  'itemsum2','Left=90px,Top=90px,status=0,resizable=no,depended=yes,Height=550px,Width=900px');if(itemsumHand2 !=undefined){itemsumHand2.focus();}">
										<%=mf.format(Double.parseDouble(result[ i ][ 2 ]))%> 
								</a>
						</td>
            
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 3 ]))%></td>            
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 4 ]))%></td>
       <% } else { %>				
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 1 ]))%></td>          
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 2 ]))%></td> 
					<td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 3 ]))%></td> 
					<td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 4 ]))%></td> 
        </tr>
        <%
              }
            }
						}
          }
        %>
         </table>
       </vh:vhFixTable>

<input type="hidden" name="subFunction" >
<input type="hidden" name="charge_ra_code" value=<%=request.getParameter("charge_ra_code") %> >
<input type="hidden" name="ymf" value=<%=request.getParameter("ymf")%> >
<input type="hidden" name="ymf" value=<%=request.getParameter("ymt") %> >
<input type="hidden" name="dept_code" value=<%=request.getParameter("charge_detail_name") %> >
<input type="hidden" name="dept_code" value=<%=request.getParameter("dept_code") %> >
<input type="hidden" name="charge_detail_code" value=<%=request.getParameter("charge_detail_code") %> 

</form>
</html:html> 
