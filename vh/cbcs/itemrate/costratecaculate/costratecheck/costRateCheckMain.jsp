<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/costratecaculate/costratecheck/costRateCheckMain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
		com.viewhigh.cbcs.cbcs.util.DisplayWidth,
		com.viewhigh.cbcs.base.sql.BaseRO, 
		com.viewhigh.cbcs.base.mvc.view.TableMarge,
		com.viewhigh.cbcs.base.mvc.view.component.Select"
%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script language="javascript">
var itemsumHand;
	function find(){
	 	if(template.charge_ra_code.value==""){
			alert('请选择配置文件');
			return;
			}   
	    show_wait();
	    template.subFunction.value="findAll";
	    template.submit();
	    return;
	}
	function ra_change(){
	    template.subFunction.value="ra_change";	
	    template.submit();
	    return;
  	}
  	var tcode='';
  	var tname='';
  	function check(){
  		tcode=template.charge_ra_code.options[template.charge_ra_code.selectedIndex].value;
  		tname=template.charge_ra_code.options[template.charge_ra_code.selectedIndex].text;
  		if(tcode==''){
  			alert("请选择文件编号");
	  		return;
  		}
  		netA.Gsend("costRateCheck.jspviewhigh?subFunction=check&charge_ra_code="+tcode,del);
  	}
  	function del(){
  		if(netA.req.responseText==1){
  			if(confirm("文件"+tname+":"+tcode+"已经发布过，是否删除？")){
  				netA.Gsend("costRateCheck.jspviewhigh?subFunction=del&charge_ra_code="+tcode,showMessage);
  			}
  		}else{
  			if(confirm("是否删除文件"+tname+":"+tcode+"？")){
  				netA.Gsend("costRateCheck.jspviewhigh?subFunction=del&charge_ra_code="+tcode,showMessage);
  			}
  		}
  	}
</Script> 

<html:html clazz="main" isPrint="true" fixRows="2">
<form name="template" method="post" action="costRateCheck.jspviewhigh">
<html:message/>
<html:title clazz='module'>医疗项目单位成本验算</html:title>
<%
 	String dateTitle="数据源为 配置文件";
 	String [][]result = null;
 	if(request.getAttribute("ymf")!=null)
 	dateTitle = "数据源为"+(String)request.getAttribute("ymf")+" 到 "+(String)request.getAttribute("ymt")+"配置文件";
 	String date = (String)request.getAttribute("ymf")+" 到 "+(String)request.getAttribute("ymt");
  BaseRO ro = (BaseRO)request.getAttribute("baseRO");
 %>
<table  width="100%" cellspacing="2" border="0" >

		<tr>	   
			<td class="signText">文件编号：</td>
			<td class="normaltext">
		<%
			 Select charge_ra_code= new  Select(request.getAttribute("charge_ra_code"),"charge_ra_code",request.getParameter("charge_ra_code")==null?"":request.getParameter("charge_ra_code"),false,false);
			 charge_ra_code.setAttribute("onchange","ra_change()");
		%>
		<%= charge_ra_code.toString()%>
			</td>		 
		</tr>
		<tr>		
			<td class="signText">科室：</td>
			<td class="normalText">
		<%
			 Select dept_code= new Select(request.getAttribute("dept_code"),"dept_code","",false,false);
			 dept_code.setAttribute("MULTIPLE","true");
		%>
		<%= dept_code.toString()%>

			</td>
		</tr>
		<tr>
			<td class="signText">说明</td><td class="normalText"><%=dateTitle%></td>
		</tr>
		<tr>
			<td colspan='2' align="right">
			<button class="pageBtn" name="" onclick="return find();" >查询</button>
			<!--<img src="images/find.gif" class="mouse" onclick="return find();"/>-->
			&nbsp;&nbsp;&nbsp;&nbsp;
			<button class="pageBtn" onclick="return check();">删除</button></td>
    	</tr>
</table>
  
		<html:title clazz='table'>医疗项目单位成本验算</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
  <%    if (ro!=null){
				TableMarge oper = new TableMarge(ro, "return find()");
  %>
	    <tr><td><%=oper%></td></tr>
	    <% } %>
	  </html:table>  
	    	<html:table clazz="result">
			  	<html:tr clazz='label'>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>年月</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>科室</td>
				 	  <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>项目总成本</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>医疗总成本</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>对外合作成本</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>被提供合作成本</td>
						<td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>差额</td>
				  </html:tr>
				<%

					java.text.DecimalFormat mf=new java.text.DecimalFormat("#,#00.0000");

				 	if(ro!=null)result= ro.getTableResult();
					if(result !=null){
						for(int i=0;i<result.length;i++){
					%>
				<tr>
					<td nowrap  class="normalText" ><%=date%></td>
					<td nowrap class="normalText" ><%=result[i][0]%></td>
					
					<td nowrap class="numberText" >	
						<a href="#" onclick="var tim=new Date();  itemsumHand=window.open('costRateCheck.jspviewhigh?subFunction=findsub1&charge_ra_code=<%=request.getParameter("charge_ra_code")%>&dept_name=<%=result[i][0]%>&dept_code=<%=result[ i ][ 5 ]%>&ymf=<%=request.getAttribute("ymf")%>&ymt=<%=request.getAttribute("ymt")%>&time='+tim.toLocaleString(), 'itemsum','Left=90px,Top=90px,status=0,resizable=no,depended=yes,Height=550px,Width=900px');if(itemsumHand!=undefined){itemsumHand.focus();}"><%=mf.format(Double.valueOf(result[i][2]))%></a>
					</td>
					<td nowrap class="numberText"><%=mf.format(Double.valueOf(result[i][1]))%></td>
					<td nowrap class="numberText" >	
						<a href="#" onclick="var tim=new Date();  itemsumHand=window.open('costRateCheck.jspviewhigh?subFunction=findsubcoo&charge_ra_code=<%=request.getParameter("charge_ra_code")%>&dept_name=<%=result[i][0]%>&dept_code=<%=result[ i ][ 5 ]%>&ymf=<%=request.getAttribute("ymf")%>&ymt=<%=request.getAttribute("ymt")%>&time='+tim.toLocaleString(), 'item1','Left=90px,Top=90px,status=0,resizable=no,depended=yes,Height=550px,Width=900px');if(itemsumHand!=undefined){itemsumHand.focus();}"><%=mf.format(Double.valueOf(result[i][3]))%></a>
					</td>
					<td nowrap class="numberText" >	
						<a href="#" onclick="var tim=new Date();  itemsumHand=window.open('costRateCheck.jspviewhigh?subFunction=findsuboffer&charge_ra_code=<%=request.getParameter("charge_ra_code")%>&dept_name=<%=result[i][0]%>&dept_code=<%=result[ i ][ 5 ]%>&ymf=<%=request.getAttribute("ymf")%>&ymt=<%=request.getAttribute("ymt")%>&time='+tim.toLocaleString(), 'item2','Left=90px,Top=90px,status=0,resizable=no,depended=yes,Height=550px,Width=900px');if(itemsumHand!=undefined){itemsumHand.focus();}"><%=mf.format(Double.valueOf(result[i][6]))%></a>
					</td>
					<td nowrap class="numberText" ><%=mf.format(Double.valueOf(result[i][4]))%></td>
				</tr>
				<%	}
					}%>
		   </html:table>
		<input type="hidden" name="subFunction" value="findAll">
		<input type="hidden" name="ymf" value="<%= request.getAttribute("ymf")==null?"":request.getAttribute("ymf")%>">
		<input type="hidden" name="ymt" value="<%= request.getAttribute("ymt")==null?"":request.getAttribute("ymt")%>">
	</form>
</html:html>

