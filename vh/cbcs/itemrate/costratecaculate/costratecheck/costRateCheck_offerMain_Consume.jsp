<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/costratecaculate/costratecheck/costRateCheck_offerMain_Consume.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK"  %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DisplayWidth"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

	</Script>

<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
  DecimalFormat mf=new DecimalFormat("#,##0.0000");
  DecimalFormat pf=new DecimalFormat("#,##0.00%");
%>

<html:html clazz="main">
<form name="template" method="post" action="costRateCheck.jspviewhigh">
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
<html:title clazz='module'>被提供合作成本验算(明细)--材料成本 </html:title>
   <html:table clazz="simple">
   	<tr> <td  nowrap class="signText">验算年月： <%=request.getParameter("ymf")%> 到 <%=request.getParameter("ymt")%> </td></tr>
	  <tr> <td  nowrap class="signText">作业套： <%= request.getParameter("charge_ra_code")%> </td></tr>
	  <tr> <td  nowrap class="signText">科室： <%= request.getParameter("dept_name")%></td> 
	  		 	<td>
			 		<button class="pageBtn" onclick="return preparedPrint();">打印</button>
			 		<img style='cursor:hand' src='images/priClose.gif' class='noprint' onclick='window.close()'>
			 	</td>
 		</tr>
		
</html:table>
 <vh:vhFixTable fixRow=1 fixCol=0>
      <table width = '100%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt'  >
        <colgroup id=tg>
        <col style = <%=DisplayWidth.NAME_WIDTH%> >

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
 				<col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
      
      </colgroup>
      <tr clazz='label'>
      	<td nowrap class="resultLabel">材料类别</td>
      	<td nowrap class="resultLabel">材料名称</td>
      	<td nowrap class="resultLabel">数量 </td>
      	<td nowrap class="resultLabel">单价</td>
      	<td nowrap class="resultLabel">成本金额</td>
      	<td nowrap class="resultLabel">成本所占比例</td>
      </tr>
     <%
     String[][] result = (String[][])request.getAttribute("result");
 			double total_sum= 0;
 	 		if (result != null) {
         if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[i][0]+"|^|"+result[i][1];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }
            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
       %>
      <tr CLASS="<%=rowColor%>">

        <td nowrap class="normalText"><%=result[i][3]%></td>
        <td nowrap class="normalText"><%=result[i][5]%></td>
        <td nowrap class="numberText"><%=result[i][6]%></td>
        <td nowrap class="numberText"><%=mf.format(new Double(result[i][7]))%></td>
        <td nowrap class="numberText"><%=mf.format(new Double(result[i][8]))%></td>
        <td nowrap class="numberText"><%=pf.format(new Double(result[i][9]))%></td>
     </tr>
  <%
  	//total_sum = total_sum + Double.parseDouble(result[i][8]);
  					}
 					}
	} %>
	   
    </table>
  </vh:vhFixTable>
	<input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	<input type="hidden" name="subFunction" value="perparedFind"/>
</form>
</html:html>
