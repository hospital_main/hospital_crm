<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/costratecaculate/costratecheck/costRateCheck_Sub3_Equipt.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.util.Preference,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DisplayWidth"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">

</Script>
<html:html clazz="main">
<form name="template" method="post" action="costRateCheck.jspviewhigh">
      <html:message/>
      <html:title clazz='module'> 医疗项目设备折旧成本 </html:title> 
      <html:table clazz="simple">
 			   <tr> 
 			     <td  nowrap class="signText">作业套： <%= request.getParameter("charge_ra_code")%> </td>
 			     <td  nowrap class="signText">科室： <%= request.getParameter("dept_name")%></td> 
 			   </tr>
			   <tr> 
			   		<td  nowrap class="signText">医疗项目： <%= request.getParameter("charge_detail_name")%></td> 
			   		<td  nowrap class="signText">成本类别： <%= request.getParameter("cost_class_name")%></td>
			   </tr>
</html:table>
   <vh:vhFixTable fixRow=1 fixCol=0>
      <table width = '100%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt'  >
        <colgroup id=tg>
        <col style = <%=DisplayWidth.NAME_WIDTH%> >

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
				<col style = <%=DisplayWidth.MONEY_WIDTH%>>       
				<col style = <%=DisplayWidth.MONEY_WIDTH%>> 
      </colgroup> 	
			<tr clazz='label'>		    
				<td   nowrap class="resultLabel" ><div align="center" >成本项目</div></td>
				<td   nowrap class="resultLabel" ><div align="center"  >科室</div></td>
				<td   nowrap class="resultLabel" ><div align="center"  >设备 </div></td>
				<td   nowrap class="resultLabel" ><div align="center"  >时间</div></td>
				<td   nowrap class="resultLabel" ><div align="center"  >单位折旧成本</div></td>
				<td   nowrap class="resultLabel" ><div align="center"  >折旧成本</div></td>
			</tr>	
			
        <%
          String[][] result = (String[][])request.getAttribute("result");
          DecimalFormat mf=new DecimalFormat("#,##0.0000");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

       <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText"><%=result[i][0]%>
          <td nowrap class="normalText"><%=result[i][1]%>
          <td nowrap class="normalText"><%= result[ i ][ 2 ] %></td>
          <td nowrap class="numberText"><%=result[ i ][ 3 ]%></td>
          <td nowrap class="normalText"><%=mf.format(Double.parseDouble(result[ i ][ 4 ]))%></td>
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 5 ]))%></td>             
       </tr>
        <%
              }
            }
        %>
      </table>
    </vh:vhFixTable>

<input type="hidden" name="subFunction" >
</form>
</html:html> 