<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/costratecaculate/costratecheck/costRateCheck_Sub1.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.util.Preference,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DisplayWidth"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
	var itemsumHand1;
</Script>
<html:html clazz="main">
<form name="template" method="post" action="costRateCheck.jspviewhigh">
      <html:message/>
      <html:title clazz='module'>医疗项目单位成本验算(明细)  </html:title>
      <html:table clazz="simple">
           <tr> <td  nowrap class="signText">验算年月： <%=request.getParameter("ymf")%> 到 <%=request.getParameter("ymt")%> </td></tr>
			     <tr> <td  nowrap class="signText">作业套： <%= request.getParameter("charge_ra_code")%> </td></tr>
			     <tr> <td  nowrap class="signText">科室： <%= request.getParameter("dept_name")%></td> 
			     			<td colspan="2" align="right"></td>
			     </tr>

		</html:table>
    <html:table clazz="simple"><tr><td><html:title clazz='table'>医疗项目单位成本验算(明细)</html:title></td></tr></html:table>

   <vh:vhFixTable fixRow=1 fixCol=0>
      <table width = '100%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt'  >
        <colgroup id=tg>
        <col style = <%=DisplayWidth.NAME_WIDTH%> >

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
      
      </colgroup>
		    <tr clazz='label'>
		      <!--医疗项目 项目总成本 工作量 项目单位成本  -->  
          <td nowrap   class="resultLabel" >医疗项目</td>
          <td nowrap   class="resultLabel" >项目总成本</td>
          <td nowrap   class="resultLabel" >工作量</td>
          <td nowrap   class="resultLabel" >项目单位成本</td>          
        </tr> 
        <%
   				 String[][] result =(String[][]) request.getAttribute("result");
          DecimalFormat mf=new DecimalFormat("#,##0.0000");
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              if(result[i][0].trim().equals("合计")){
              result[i][3]="&nbsp;";
              result[i][2]="&nbsp;";
              }
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

       <tr CLASS="<%=rowColor%>">  
      
          <td nowrap class="normalText"> 
          	<%
          		if(result[i][0].equals("&nbsp;")){
          	%>
          		<%=result[ i ][ 4 ]%>
          	<%}else{%>
          		<%=result[i][0]%>
          	<%}%>         
          </td>
       <% if(!result[i][0].trim().equals("合计")){ %>  
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 1 ]))%></td>          
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 2]))%></td>   					      
         <td nowrap class="numberText">

          <a href="#" onclick="   var tim=new Date();  itemsumHand1=window.open('costRateCheck.jspviewhigh?subFunction=findsub2&charge_ra_code=<%=request.getParameter("charge_ra_code")%>&ymf=<%=request.getParameter("ymf")%>&ymt=<%=request.getParameter("ymt")%>&charge_detail_name=<%=result[ i ][ 0 ]%>&charge_detail_code=<%=result[i][4]%>&dept_code=<%=result[i][5]%>&dept_name= <%= request.getParameter("dept_name")%>&time='+tim.toLocaleString(),  'itemsum1','Left=90px,Top=90px,status=0,resizable=no,depended=yes,Height=550px,Width=900px');if(itemsumHand1!=undefined){itemsumHand1.focus();}"><%=mf.format(Double.parseDouble(result[ i ][ 3]))%></a>
				<% } else { %>				
          <td nowrap class="numberText"><%=mf.format(Double.parseDouble(result[ i ][ 1 ]))%></td>          
          <td nowrap class="numberText"><%=result[ i ][ 2 ]%></td> 
					<td nowrap class="numberText"><%=result[ i ][ 3 ]%></td>
				<% } %>
			</td>  
        </tr>
        <%
              }
            }
        %>
       </table>
       </vh:vhFixTable>
<%//}%>
 
<input type="hidden" name="subFunction" >
<input type="hidden" name="charge_ra_code" value=<%=request.getParameter("charge_ra_code") %> >
<input type="hidden" name="ymf" value=<%=request.getParameter("ymf")%> >
<input type="hidden" name="ymf" value=<%=request.getParameter("ymt") %> >
<input type="hidden" name="charge_detail_code" value=<%=request.getParameter("charge_detail_code") %> >

</form>
</html:html>
<script>
</script>