<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/itemtaskmanages/personnelsalarymainten/personnelSalaryMaintenUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
function save() {
 
 	if( trim(template.m_avg_salary.value)=="")
    {   alert('请输入月平均工资');
        return false;
    }  
	if(!isNumber(template.m_avg_salary))
    {   alert('月平均工资必须为数字型');
    		template.m_avg_salary.focus();
	    	template.m_avg_salary.select();
        return false;
    }
       template.subFunction.value='Update';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main"> 
<form name="template" method="post" action="personnelSalaryMainten.jspviewhigh">  
<%
	String []result =(String[]) request.getAttribute("result");
%>
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>人员工资计算（按职称分类）修改界面 </html:title>  
  <!-- 简单信息 -->    
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="signText" nowrap="nowrap">配置编码：</td>
      <td nowrap class="normalText"><input type=text class="textInputC" value="<%=result[0]%>" disabled /></td>
    </tr>
		<tr>
	    <td class="signText" nowrap="nowrap">职称：</td>
      <td nowrap class="normalText"><input type=text class="textInputC" value="<%=result[1]%>" disabled /> </td> 
    </tr>    
    <tr>
      <td class="signText" nowrap="nowrap">成本项目:</td>
      <td nowrap class="normalText"><input type=text  value="<%=result[2]%>" class="textInputC" disabled /></td>
    </tr>    
    <tr>
      <td class="signText" nowrap="nowrap">月平均工资：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="m_avg_salary" value="<%=result[3]%>" class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!-- <img src="images/save.gif" class="mouse" onclick="return save();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" />
 <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
  <input type=hidden name="set_cfg_code" value="<%=result[0]%>"/></td>
  <input type=hidden name="title_code" value="<%=result[4]%>" /></td>
  <input type=hidden name="cost_subj_code" value="<%=result[5]%>" /></td>
  
</form>

</html:html>   