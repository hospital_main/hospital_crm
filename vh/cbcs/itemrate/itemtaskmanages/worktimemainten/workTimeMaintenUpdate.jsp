<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/itemtaskmanages/worktimemainten/workTimeMaintenUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  //添加
function save() {

	if(!isNumber(template.m_avg_wdays))
    {   alert('每月工作日时间必须为数字型');
  			  template.m_avg_wdays.focus();
	    		template.m_avg_wdays.select();
       return false;
    }
  else 
   {
  		if(eval(template.m_avg_wdays.value)>31)
  			{ alert('每月工作日不能超过31天');
  			  template.m_avg_wdays.focus();
	    		template.m_avg_wdays.select();
	    		return false;
	  			}
   }
   
	if(!isNumber(template.d_avg_whours))
    {    alert('日平均工作时间时间必须为数字型');
   			  template.d_avg_whours.focus();
	    		template.d_avg_whours.select();   
        return false;
    }
  else 
   {
  		if(eval(template.d_avg_whours.value)>24)
  			{ alert('日平均工作时间不能超过24小时');
   			  template.d_avg_whours.focus();
	    		template.d_avg_whours.select();   			
  			  return false;
  			}
   } 
        template.subFunction.value='Update';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="workTimeMainten.jspviewhigh">  
<%
	String []result =(String[]) request.getAttribute("result");
%>
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>工作时间维护修改界面 </html:title>  
  <!-- 简单信息 -->    
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="signText" nowrap="nowrap">配置编码：</td>
      <td nowrap class="normalText"><input type=text class="textInputC" value="<%=result[0]%>" disabled /></td>
    </tr>
		<tr>
	    <td class="signText" nowrap="nowrap">职称：</td>
      <td nowrap class="normalText"><input type=text class="textInputC" value="<%=result[4]%>" disabled /> </td> 
    </tr>    
    <tr>
      <td class="signText" nowrap="nowrap">每月工作日：</td>
      <td nowrap class="normalText"><input type=text name="m_avg_wdays"  value="<%=result[2]%>" class="textInputC" /></td>
    </tr>    
    <tr>
      <td class="signText" nowrap="nowrap">每天工作时间(小时):</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="d_avg_whours" value="<%=result[3]%>" class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
       <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
 <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
  <input type=hidden name="set_cfg_code" value="<%=result[0]%>"/></td>
  <input type=hidden name="title_code" value="<%=result[1]%>" /></td>
</form>

</html:html>   