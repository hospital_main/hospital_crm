<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/itemtaskmanages/worktimemainten/workTimeMaintenCopy.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
	function confrim() {
		if (template.set_cfg_code.value =="")
		{
			alert("请选择数据源的配置编码!");
			return;
		}
		if (template.set_cfg_code1.value =="")
		{
			alert("请选择目标的配置编码!");
			return;
		}
		if (template.set_cfg_code.value ==template.set_cfg_code1.value&&''==template.title_code.value)
		{
			alert("请选择数据源的职称!");
			return;
		}	
			if (template.set_cfg_code.value!=template.set_cfg_code1.value&&''==template.title_code.value&&''!=template.title_code1.value)
		{
			alert("请选择数据源的职称!");
			return;
		}	
		if (template.set_cfg_code.value ==template.set_cfg_code1.value&&template.title_code1.value ==template.title_code.value)
		{
			alert("当前设置无法进行复制!");
			return;
		}
		netA.Gsend("workTimeMainten.jspviewhigh?subFunction=check&set_cfg_code1="+template.set_cfg_code1.value+"&title_code1="+template.title_code1.value,copyAccess);
	}
	function copyAccess(){
		var num=new Number(netA.req.responseText);
		
		if(isNaN(num)){
			alert(netA.req.responseText);
		}else{	
			if(netA.req.responseText>0){
				if(confirm("拷贝目标存在数据，确认将会覆盖已存在数据，确认？")){
					doCopy();
				}
			}else{
				doCopy();
			}
		}
  }
  function doCopy(){
		template.subFunction.value="copy";
		show_wait();
    template.submit();
    return true;
  }
     
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="workTimeMainten.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>    

<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
	String [][]dict_title = (String [][])request.getAttribute("dict_title");
%>
  <!-- 标题栏 -->
	 <html:title clazz='module'>工作时间维护复制界面  </html:title>  
  <!-- 简单信息 -->   
  <table  width="100%" cellspacing="2" border="0">
  	<tr><td colspan=2 >数据源: </td> <td>目标: </td>
    </tr>
    <tr>   
      <td class="normalText" nowrap="nowrap">配置编码:</td>
      <td nowrap class="normalText"><%=new Select(set_cfg_code,"set_cfg_code",request.getParameter("set_cfg_code"),false,false)%></td>
      <td class="normalText" nowrap="nowrap">配置编码:</td>
      <td align="left"><%=new Select(set_cfg_code,"set_cfg_code1",request.getParameter("set_cfg_code1"),false,false)%></td>
		</tr>
		<tr>
     <td class="normalText" nowrap="nowrap">职称：</td>
     <td nowrap class="normalText"><%=new Select(dict_title,"title_code",request.getParameter("title_code"),false,false)%></td>
     <td class="normalText" nowrap="nowrap">职称：</td>
     <td nowrap class="normalText"><%=new Select(dict_title,"title_code1",request.getParameter("title_code1"),false,false)%></td>
    </tr>
    <tr><td><br><br></td></tr>
   	<tr align="center">
      <td colSpan="4"><button class="pageBtn" onclick="return confrim();">确定</button>   
     &nbsp;&nbsp;&nbsp;&nbsp;
<button class="pageBtn" onclick="return back(template);">返回</button>  
<!-- <img src="images/confirm.gif" class="mouse" onclick="return confrim();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />-->
</td>
    </tr>
    
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   