<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/itemtaskmanages/itemMateUsed/itemMateUsedCreate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
	function create() {
	
	switch(isDouble(template.amount, 10,6))
	    {
	      case 0 : alert('数量必须为数字型'); return false;
	      case 1 : alert('数量整数位不能超过10位'); return false;
	      case 3 : alert('数量小数位不能超过6位'); return false;
	    }
	if (template.set_cfg_code.value =="")
	{
		alert("请选择配置编码!");
	}
	if (template.dict_income_subj.value =="")
	{
		alert("请输入医疗项目!");
	}
	if (template.dict_title.value =="")
	{
		alert("请选择作业序号!");
	}
	if (template.inv_code.value =="")
	{
		alert("请选择消耗材料!");
	}
  else if(eval(template.amount.value)>9999999999999999)
    {
    	alert('数量必须小于9999999999999999');
        template.amount.focus();
    		template.amount.select();    	
    	return;
    }
        template.subFunction.value='Create';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemMateUsed.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>
<%
	String [][]set_cfg_code = (String [][])request.getAttribute("set_cfg_code");
	String [][]dict_income_subj = (String [][])request.getAttribute("dict_income_subj");
	String [][]dict_title = (String[][])request.getAttribute("dict_title");
	String [][]inv_code = (String[][])request.getAttribute("inv_code");
%>
  <!-- 标题栏 -->
	  <html:title clazz='module'>作业材料消耗添加界面 </html:title>  
  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="normalText" nowrap="nowrap">配&nbsp;置&nbsp;编&nbsp;码&nbsp;：</td>
      <td class="normalText" nowrap="nowrap" ><%=new Select(set_cfg_code,"set_cfg_code",request.getParameter("set_cfg_code"),false,false)%></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">收&nbsp;费&nbsp;项&nbsp;目&nbsp;：</td>
      <td class="normalText" nowrap="nowrap" >
      <?xml:namespace prefix="hzh"/> 
      <hzh:QInput ID="nosNamec" name="dict_income_subj" value="" AdjustVal="10" previousObj="male" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" Lwidth="100"  top="158" left="365" width="200" Lheight="5" xmlSource="dic/dict_charge_detail_LVT.xml" init="1" />
      </td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">作业序号：</td>
      <td nowrap class="normalText"><%=new Select(dict_title,"dict_title",request.getParameter("dict_title"),false,false)%></td>
		</tr>
		<tr>
      <td class="normalText" nowrap="nowrap">消耗材料：</td>
      <td class="normalText"nowrap="nowrap"><%=new Select(inv_code,"inv_code","",false,false)%></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">数量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="amount"  maxlength="17"  class="textInputC" /></td>
    </tr>
    
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   