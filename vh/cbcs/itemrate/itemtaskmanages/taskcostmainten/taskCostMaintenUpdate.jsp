<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/itemtaskmanages/taskcostmainten/taskCostMaintenUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
                        com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    if(!isNumber(template.per_cost))
    {
      alert('单位消耗需输入数字类型!');
        template.per_cost.focus();
    		template.per_cost.select();      
      return false;
    }
    template.subFunction.value='Update';
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="taskCostMainten.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module"> 其它直接成本</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息  year_month, charge_detail_code, cost_class_code, cvalue, stop_month -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">配置描述：</td>
      <td width="75%" class="normalText" nowrap="nowrap"><input type="text"   value=<%=result[1] %> class="textInputC" disabled /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">医疗项目：</td>
      <td class="normalText" nowrap="nowrap"><input type="text"   value=<%=result[3] %> class="textInputC" disabled /></td>
    </tr> 
    <tr>
      <td class="signText" nowrap="nowrap">作业:</td>
      <td class="normalText" nowrap="nowrap"><input type="text"  value=<%=result[5] %> class="textInputC" disabled /></td>
    </tr>   
    <tr>    
    <tr>
      <td class="signText" nowrap="nowrap">成本项目编码:</td>
      <td class="normalText" nowrap="nowrap"><input type="text"  value=<%=result[7] %> class="textInputC" disabled /></td>
    </tr>   
    <tr>
      <td class="signText" nowrap="nowrap">单位消耗：</td>
      <td class="normalText" nowrap="nowrap"><input type="text"  name="per_cost" value=<%=result[8] %> class="textInputC" /></td>
    </tr>

     <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="set_cfg_code1" value="<%=result[0]%>">
  <input type="hidden" name="charge_detail_code1" value="<%=result[2]%>">
  <input type="hidden" name="work_code1" value="<%=result[4]%>">
  <input type="hidden" name="cost_subj_code1" value="<%=result[6]%>">
  
  <%}%>
</form>


</html:html>
