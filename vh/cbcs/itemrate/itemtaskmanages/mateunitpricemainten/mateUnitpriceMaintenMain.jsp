<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/itemtaskmanages/mateunitpricemainten/mateUnitpriceMaintenMain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent,
								 com.viewhigh.cbcs.cbcs.util.SimpleSearch" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
 

   function save() {
    template.subFunction.value='Update';
    show_wait();
    template.submit();
    return true;
  }
 
 
  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="mateUnitpriceMainten.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>直接材料成本管理</html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
	    <tr>
  		  <td nowrap class="signText">材料类别：</td>
	      <% String inv_type_code = request.getParameter("inv_type_code");%>
          <td> <%=new Select(SimpleSearch.find("  select inv_type_code,inv_type_name from  dict_inv_type  "), "inv_type_code", "", false, false)%></td> 
	       
	      <td nowrap class="signText">材料名称：</td>
	      <% String inv_name = request.getParameter("inv_name");%>
	      <td><input type=text name="inv_name" class="textInputC" <%if(inv_name!= null){ out.println(" value=" + inv_name);}%>></td>

	      <td nowrap class="signText">材料编码：</td>
	      <% String inv_code = request.getParameter("inv_code");%>
	      <td><input type=text name="inv_code" class="textInputC" <%if(inv_code!= null){ out.println(" value=" + inv_code);}%>></td>
	    </tr>
	    <tr>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      <td class="normalText">
	      <button class="pageBtn" name=""  onclick="return find();" >查询</button>
	      <!--<img src="images/find.gif" class="mouse" onclick="return find();" />--></td>
	    </tr>	
	    </tr>

	  </html:table>

	  <br>

	  <%
	     // BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	     // if (ro!=null) {
	     // TableMarge oper = new TableMarge(ro, "return find()"); 
	      //oper.addOptionButton("images/reset.gif", "return reset()");     //  重置 
	     // oper.addNeedButton("images/save.gif", "return save()");     //  添加
	  %>
	<button class="pageBtn" onclick="return save();">保存</button> 
	<button class="pageBtn" onclick="return reset();" >重置</button>   
   <!-- <img src="images/save.gif" class="mouse" onclick="return save();" /> 
    <img src="images/reset.gif" class="mouse" onclick="return reset();" />-->
   
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	     </html:table>
		    	<html:table clazz="result">
		        <html:tr clazz='label'> 
		          <td nowrap="nowrap" class="resultLabel" >材料编码</td>
		          <td nowrap="nowrap" class="resultLabel" >材料名称</td>
		          <td nowrap="nowrap" class="resultLabel" >计量单位</td>
		          <td nowrap="nowrap" class="resultLabel" >材料单价</td>
		        </html:tr>

		        <%
		         // String[][] result = ro.getTableResult();
		          String[][] result = (String[][])request.getAttribute("baseRO");
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )		            
		            {
		              String primaryKey = result[ i ][ 0 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>
 
		        <tr CLASS="<%=rowColor%>">

		          <td nowrap="nowrap" class="normalText">
		          <input type="hidden" name ="key" value=<%=primaryKey%> readonly><%=primaryKey%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
		                
		          <td nowrap="nowrap" width='200' align="right">
		          <input type="text" name="inv_price" class="textInputC" text-align="right" value=<%=result[ i ][ 3 ]%>  ></td> 

		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		<% // }%>
	  <input type=hidden name="subFunction"/>
	</form>
</html:html>

