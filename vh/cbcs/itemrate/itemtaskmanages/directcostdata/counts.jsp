<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/itemtaskmanages/directcostdata/counts.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:44 $
     $Modtime: 03-09-02 16:24 $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
  com.viewhigh.cbcs.base.mvc.view.TableMarge, 
  com.viewhigh.cbcs.base.sql.BaseRO,java.text.*,com.viewhigh.cbcs.cbcs.util.*" %>
  <%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<html:html clazz="main">
    <body>
      	  	  <html:title clazz='module'>直接材料成本汇总</html:title>

            <%
            DecimalFormat nf = new DecimalFormat("#,##0.00");
						Object obj=request.getSession(false).getAttribute("collect");
            String a="";
            if(obj!=null){
                String[][] result= ((String[][])obj);
                if(result!=null)
              		a=result[0][0];
             
            %>
   	  <html:table clazz="simple">
        <tr>
          <td nowrap class="signText">直接材料成本总金额:</td><td nowrap class="normalText"><%=nf.format(Double.valueOf(a))%>元</td>
  			</tr>
  			<tr>
  			<td></td>
  			<td></td>
  			<td><button class="pageBtn" onClick="window.close();" >关闭</button></td></tr>
      </html:table><%}%>
    </body>
</html:html>


