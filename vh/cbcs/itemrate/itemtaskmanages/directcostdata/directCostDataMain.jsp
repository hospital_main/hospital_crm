<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/itemtaskmanages/directcostdata/directCostDataMain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent,
								 com.viewhigh.cbcs.cbcs.util.SimpleSearch" %>
								 
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
		function dischange(){
			template.changesign.value="dischange";
			template.subFunction.value="change";
			show_wait();
			template.submit();
			return;
		}
		function kindchange(){
			template.changesign.value="kindchange";
			template.subFunction.value="change";
			show_wait();
			template.submit();
			return;
		}
		
	function accout() { 
	  var date=new Date();	  
		window.showModalDialog("directCostData.jspviewhigh?subFunction=Collect" +"& date= " +date.toString(), window,"status:yes;dialogHeight: 140px; dialogWidth: 500px;");			
	}

   function create() {
        template.subFunction.value='preparedCreate';
            show_wait();
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
  var seed=1
  function showDistill(){
  	seed++;
		window.showModalDialog("directCostData.jspviewhigh?subFunction=Frames&seed="+seed,window,"status:no;dialogHeight: 250px; dialogWidth: 400px;resizable:no");
  }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="directCostData.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>直接材料成本管理</html:title>

	  <!-- 简单信息 -->
	  
	  <%
	  
	  %>
	  <html:table clazz="simple"> 
		<tr>	    
	      <td nowrap class="signText">年月：</td>
	     <td nowrap class="normalText"  ><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td> 
	     
	     <td nowrap class="signText">收入项目：</td>
		 	<td class="normalText">
			 	<%
			 	Select income_subj_code=new Select(request.getAttribute("income_subj_code"),"income_subj_code",request.getParameter("income_subj_code")==null? "":request.getParameter("income_subj_code"),false,false);
			 	income_subj_code.setAttribute("onchange","dischange()");
			 	%>
			 	<%=income_subj_code.toString() %></td>
			<td nowrap class="signText">材料类别：</td>
			<% String inv_type_code = request.getParameter("inv_type_code");%>
			<td> <%=new Select(SimpleSearch.find("  select inv_type_code,inv_type_name from  dict_inv_type  "), "inv_type_code", inv_type_code, false, false)%></td> 

		 </tr>
		 <tr>
			<td nowrap class="signText">科室：</td>
			<% String dept_code = request.getParameter("dept_code");%>
			<td> <%=new Select(SimpleSearch.find("SELECT dept_code,dept_name FROM 	dict_acct_dept WHERE EXISTS ( select dept_code from dict_charge_dept_ratio where dept_code = dict_acct_dept.dept_code) ORDER BY dept_code "),"dept_code", dept_code, false, false)%></td> 	 

		 <td nowrap class="signText">收费类别：</td>
		 	<td class="normalText">
			 	<%
			 	Select charge_kind_code=new Select(request.getAttribute("charge_kind_code"),"charge_kind_code",request.getParameter("charge_kind_code")==null? "":request.getParameter("charge_kind_code"),false,false);
			 	charge_kind_code.setAttribute("onchange","kindchange()");
			 	%>
			 	<%=charge_kind_code.toString() %></td>
	      <td nowrap class="signText">材料名称：</td>
	      <% String inv_name = request.getParameter("inv_name");%>
	      <td>
	      	<input type=text name="inv_name" class="textInputC" <%if(inv_name!= null){ out.println(" value=" + inv_name);}%>>
	      </td>
	     
	    </tr>
	    <tr>
	    	<td></td><td></td>
			 	<td nowrap class="signText">医疗项目: </td>
		 		<td class="normalText">
			 	<%
			 	Select charge_detail_code=new Select(request.getAttribute("charge_detail_code"),"charge_detail_code",request.getParameter("charge_detail_code")==null? "":request.getParameter("charge_detail_code"),false,false);
			 	%>
			 	<%=charge_detail_code.toString() %></td>
			 	<td></td>
	      <td class="normalText">
	      <button class="pageBtn" name=""  onclick="return find();" >查询</button>
	      <button class="pageBtn" name=""   onclick="accout()"  >汇总</button> 
	      <!--<img src="images/find.gif" class="mouse" onclick="return find();" />
          <img src="images/huizong.GIF" class="mouse" onclick="accout()" />-->
          <img src="images/sjtq.gif" class="mouse" onclick="showDistill()" />
          </td>
	    </tr>	
	    </tr>
	  </html:table>

	  <br>

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
	  
  
		<html:title clazz='table'>材料信息定义</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>

		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td nowrap="nowrap" class="resultLabel" >选择</td>		          
		          <td nowrap="nowrap" class="resultLabel" >年月</td>
		          <td nowrap="nowrap" class="resultLabel" >科室</td>
		          <td nowrap="nowrap" class="resultLabel" >时间</td>
		          <td nowrap="nowrap" class="resultLabel" >病历号</td>
		          <td nowrap="nowrap" class="resultLabel" >姓名</td>
		          <td nowrap="nowrap" class="resultLabel" >年龄</td>
		          <td nowrap="nowrap" class="resultLabel" >医疗项目</td>
		          <td nowrap="nowrap" class="resultLabel" >作业名称</td>
		          <td nowrap="nowrap" class="resultLabel" >材料名称</td>
		          <td nowrap="nowrap" class="resultLabel" >规格型号</td>
		          <td nowrap="nowrap" class="resultLabel" >数量</td>
		          <td nowrap="nowrap" class="resultLabel" >计量单位</td>
		          <td nowrap="nowrap" class="resultLabel" >材料单价</td>
		          <td nowrap="nowrap" class="resultLabel" >金额</td> 
		        </html:tr>

		        <%
		        
              DecimalFormat nf = new DecimalFormat("#,##0.00");
              DecimalFormat nfs = new DecimalFormat("#,##0.000000");
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>
 
		        <tr CLASS="<%=rowColor%>">
		          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
		          <td nowrap="nowrap" class="normalText">
		          <a href="directCostData.jspviewhigh?subFunction=preparedSave&primaryKey=<%=result[ i ][ 0 ]%>"> <%=result[ i ][ 1 ]%></a> </td>
		          
 		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 14 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td> 
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 4 ]%></td>
		          <td nowrap="nowrap" class="numberText"><%=result[ i ][ 5 ]%></td> 
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 6 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 15 ]%></td>		          
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 18 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 16 ]%></td> 
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 9 ]%></td>
              <td nowrap="nowrap" class="numberText"><%=nfs.format(Double.parseDouble(result[ i ][ 10 ]))%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 11]%></td> 		           
              <td nowrap="nowrap" class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 12 ]))%></td>
              <td nowrap="nowrap" class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ 13 ]))%></td>
		          
 
		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		<%}%>
	  <input type='hidden' name="subFunction"/>
	  <input type="hidden" name="changesign" value=<%=request.getParameter("changesign")%>>
	</form>
</html:html>

