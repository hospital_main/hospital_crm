<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/itemtaskmanages/eCoopCost/eCoopCostUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  //添加
function save() {

		switch(isDouble(template.Use_time, 14,2))
		    {
		      case 0 : alert('时间必须为数字型'); return false;
		      case 1 : alert('时间不能大于14个字符'); return false;
		      case 3 : alert('时间有2位小数'); return false;
		    }
		 switch(isDouble(template.Depre_price, 12,4))
		    {
		      case 0 : alert('单位折旧成本必须为数字型'); return false;
		      case 1 : alert('单位折旧成本不能大于12个字符'); return false;
		      case 3 : alert('单位折旧成本有4位小数'); return false;
		    }
        template.subFunction.value='Update';
        show_wait();
        template.submit();
        return true;
    }
	function total()
	{
		if(template.Use_time.value != '' && template.Depre_price.value != '')
		{
			template.Depre_mny.value = template.Use_time.value * template.Depre_price.value;
		}
	}

  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="depreCooperationCost.jspviewhigh">  
<%
	String []result =(String[]) request.getAttribute("result");
	String [][]dept_code = (String[][])request.getAttribute("dict_acct_dept");
%>
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>项目作业管理-设备折旧成本修改界面 </html:title>  
  <!-- 简单信息 -->    <!-- 配置编码 医疗项目 作业序号 作业名称 作业时间（分钟） 说明 111, 0004, 1, ADFSAF, 12, 12  -->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="normalText" nowrap="nowrap">配置编号:</td>
      <td nowrap class="normalText"><input type=text name = "set_cfg_code" class="textInputC" value="<%=result[1]%>" disabled /></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">医疗项目:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name = "charge_detail_code"  class="textInputC" value="<%=result[3]%>" disabled /></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">成本项目:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="inv_type_code" class="textInputC" value="<%=result[5]%>" disabled /></td>
    </tr>   
    <tr>
      <td class="normalText" nowrap="nowrap">设备：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="equi_code" value="<%=result[7]%>" class="textInputC" disabled /></td>
    </tr>  
    <tr>
      <td class="normalText" nowrap="nowrap">科室：</td>
      <td class="normalText" nowrap="nowrap"><%=new Select(dept_code,"dept_code",result[12],false,false)%></td>
    </tr>  
    <tr>  
      <td class="normalText" nowrap="nowrap">时间：</td>
      <td class="normalText" nowrap="nowrap"><input type=text  maxlength="17"  name="Use_time" value="<%=result[8]%>" class="textInputC" onChange = "total();" /></td>
    </tr>
  	<tr>    
      <td class="normalText" nowrap="nowrap">单位折旧成本：</td>
      <td class="normalText" nowrap="nowrap"><input type=text  maxlength="17"  name="Depre_price" value="<%=result[9]%>" class="textInputC" onChange = "total();" /></td>
    </tr>
		<tr>    
      <td class="normalText" nowrap="nowrap">折旧成本：</td>
      <td class="normalText" nowrap="nowrap"><input type=text  maxlength="17"  name="Depre_mny" value="<%=result[10]%>" class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
       <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
  <input type=hidden name="set_cfg_code" class="textInputC" value="<%=result[0]%>"/></td>
  <input type=hidden name="charge_detail_code" value="<%=result[2]%>" /></td>
  <input type=hidden name="inv_type_code" value="<%=result[4]%>" /></td>
  <input type=hidden name="equi_code" value="<%=result[6]%>" /></td>

</form>

</html:html>   
