<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/itemtaskmanages/manpowertaskmanage/manpowerTaskManageCreate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
	function create() {
	
	switch(isDouble(template.amount, 4,0))
	    {
	      case 0 : alert('工作人员数量必须为数字型'); return false;
	      case 1 : alert('工作人员数量不能大于4个字符'); return false;
	      case 3 : alert('工作人员数量有小数'); return false;
	    }
	if (template.set_cfg_code.value =="")
	{
		alert("请选择配置编码!");
	}
	if (template.charge_detail_code.value =="")
	{
		alert("请输入医疗项目!");
	}
	if (template.work_code.value =="")
	{
		alert("请选择作业序号!");
	}
	if (template.title_code.value =="")
	{
		alert("请选择工作人员职称!");
	}
  else if(eval(template.amount.value)>255)
    {
    	alert('工作人员数量必须小于255');
        template.amount.focus();
    		template.amount.select();    	
    	return;
    }
        template.subFunction.value='create';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="manpowerTaskManage.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>
<%
	String [][]set_cfg_code = (String [][])request.getAttribute("dict_charge_set_con");
	String [][]work_code = (String [][])request.getAttribute("work_code");
	String [][]title_code = (String[][])request.getAttribute("dict_title");
%>
  <!-- 标题栏 -->
	  <html:title clazz='module'>人工作业管理添加界面 </html:title>  
  <!-- 简单信息 -->    <!-- 配置编码 医疗项目 作业序号  作业名称  工作人员职称 工作人员数量     -->
  <table  width="100%" cellspacing="2" border="0">
    <tr>   
      <td class="signText" nowrap="nowrap">配&nbsp;置&nbsp;编&nbsp;码&nbsp;：</td>
      <td class="normalText" nowrap="nowrap" ><%=new Select(set_cfg_code,"set_cfg_code","",false,false)%></td>
      <td class="signText" nowrap="nowrap">收&nbsp;费&nbsp;项&nbsp;目&nbsp;：</td>
      <td class="normalText" nowrap="nowrap" >
	      <?xml:namespace prefix="hzh"/>
	      <hzh:QInput ID="nosNamea" name="charge_detail_code" AdjustVal="50" previousObj="set_cfg_code" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="35" left="380" Lheight="6" xmlSource="dic/dict_charge_detail_LVT.xml" init="1"/>
	    </td>
      <td class="signText" nowrap="nowrap">作业序号：</td>
      <td nowrap class="normalText"><%=new Select(work_code,"work_code","",false,false)%></td>
		</tr>
		<tr>
      <td class="signText" nowrap="nowrap">工作人员职称：</td><td class="normalText"nowrap="nowrap"><%=new Select(title_code,"title_code","",false,false)%></td>
      <td class="signText" nowrap="nowrap">工作人员数量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="amount"  maxlength="4"  class="textInputC" /></td>
    </tr>
    
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   