<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/dict/countdeptdef/countDeptDefCreate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
        template.subFunction.value='Create';
        
        show_wait();
        template.submit();
 
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="countDeptDef.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>参与单位成本计算科室添加页面</html:title>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" > 
           
        <td class="signText">科室：</td> 
        <td>
              <select  class="selectBg" multiple="multiple" name="dept" size="15" >
              <%String[][] dept=(String[][])request.getAttribute("dept");
              String[] dval=request.getParameterValues("dept");
              %>
              <option value="" <%out.print("selected");%>>--不限--</option >
              <%
              if(dept!=null){
              for(int i=0;i<dept.length;i++){%>
              <option value="<%=dept[i][0]%>"
              <%if(dval!=null){for(int j=0;j<dval.length;j++){
                if(dept[i][0].equals(dval[j])){
                out.print("selected");
                }}}%>>
                <%=dept[i][1]%></option>
              <%}}%>
              </select>  
         </td> 
    
    
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="Create"/>
</form>

</html:html>   