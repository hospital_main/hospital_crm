<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/dict/rateCostType/rateCostTypeCreate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ 
 --> 
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
 <%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  //添加
    function create() {
    	if(trim(template.cost_class_code.value)==""){
    		alert("成本分类编号不能为空");
    		return false;
    	}
      if(trim(template.cost_class_name.value)==""){
    		alert("成本分类名称不能为空");
    		return false;
    	}
    	if(trim(template.spell.value) ==""){
    	  alert("拼音码不能为空");
    	  return false;
    	}
    		template.subFunction.value='create';
        show_wait();
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="rateCostType.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>成本分类定义 </html:title>  
	  <%
	 		String[][] flag ={{"0","其他归集"},{"1","材料归集"},{"2","人工归集"},{"3","奖金归集"},{"4","离退休归集"},{"5","折旧归集"}};  
	 		String[][]supp_item_code = (String [][])request.getAttribute("dict_cost_class");
	  %>

  <!-- 简单信息 -->  
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">成本分类编号：</td>
      <td width="75%" class="normalText" nowrap="nowrap"><input type=text name="cost_class_code" class="textInputC" maxlength="20"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">成本分类名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="cost_class_name" class="textInputC"  maxlength="20"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">成本性质定义:</td>
      <td nowrap class="normalText"> <%=new Select(flag,"is_direct",request.getParameter("is_direct"),false,true)%></td>
    </tr>   
      <tr>
      <td class="signText" nowrap="nowrap">上级代码：</td>
      <td class="normalText" nowrap="nowrap">
       <%=new Select(supp_item_code,"supp_item_code",request.getParameter("supp_item_code")==null?"":request.getParameter("supp_item_code"),false,true)%>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">是否停用：</td>
      <td class="normalText" nowrap="nowrap">
		    是<input type="radio" name="stop_mark" value="Y" />
        否<input type="radio" name="stop_mark" value="N" checked />
		  </td>
	  </tr>
	  <tr>
      <td class="signText" nowrap="nowrap">拼音码:</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="spell" value="<%=request.getParameter("spell")==null? "":request.getParameter("spell")%>" class="textInputC"  maxlength="10"/></td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
  <input type=hidden name="last_level" value="Y"/>
</form>

</html:html>   