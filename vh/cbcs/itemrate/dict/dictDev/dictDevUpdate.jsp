<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/dict/dictDev/dictDevUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
    com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save(){    
    if(template.dev_code.value =="") {
      alert('设备编码不能为空!');
      return;
    }
    if(template.dev_name.value =="") {
      alert('设备名称不能为空!');
      return;
    }
    if(template.dev_name.value =="") {
      alert('设备原值不能为空!');
      return false;
    }
    if(template.start_day.value == ""){
			alert('请选择开始折旧时间!');
			return false;
		}
		if(template.dep_amount.value =="") {
      alert('折旧率不能为空!');
      return;
    }    
    if(template.dep_years.value =="") {
      alert('折旧年数不能为空!');
      return;
    }
    template.subFunction.value='Update';
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictDev.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module"> 设备维护修改页面</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息 dev_code,dev_name,value,start_day,dep_amount,dep_years-->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">设备编码： </td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text name="dev_code" value="<%=result[0]%>" class="textInputC" disabled >
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">设备名称：</td>
	    <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text name="dev_name" value="<%=result[1]%>" class="textInputC" disabled>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">设备原值：</td>
	    <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text name="dev_value" value="<%=result[2]%>" class="textInputC">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">开始折旧时间：</td>
	    <td nowrap class="normalText"  ><%=new com.viewhigh.cbcs.base.mvc.view.DateComponent("start_day",result[3].substring(0,10))%></td> 
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">折旧率：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="dep_amount" value="<%=result[4]%>" class="textInputC" maxlength="30">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">折旧年数：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="dep_years" value="<%=result[5]%>" class="textInputC" maxlength="30">
      </td>
    </tr>
     <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type=hidden name="dev_code" value="<%=result[0]%>" />
  <input type=hidden name="dev_name" value="<%=result[1]%>" />
  <input type=hidden name="dev_value" value="<%=result[2]%>" />
  <%}%>
</form>


</html:html>
