<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/dict/dictDev/dictDevCreate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
    com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
    if(trim(template.dev_code.value) =="") {
      alert('设备编码不能为空!');
      return;
    }
    if(trim(template.dev_name.value) =="") {
      alert('设备名称不能为空!');
      return;
    }
    if(trim(template.dev_value.value) =="") {
      alert('设备原值不能为空!');
      return false;
    }
    if(template.start_day.value == ""){
	
			alert('请选择开始折旧时间!');
			return false;
		}
	  if(template.dep_amount.value == ""){
			alert('折旧率不能为空!');
			return false;
		}
		if(template.dep_years.value == ""){
			alert('折旧年数不能为空!');
			return false;
		}
    template.subFunction.value='Create';
    template.submit();
    return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="dictDev.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>设备维护添加页面</html:title>

  <!-- 简单信息  -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">设备编码：</td>
      <td width="75%" class="normalText" nowrap="nowrap"><input type=text name="dev_code" class="textInputC" maxlength="20"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap" >设备名称：</td>	      
	    <td nowrap class="normalText"  ><input type=text name="dev_name" class="textInputC" maxlength="40"/></td> 
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap" >设备原值：</td>	      
	    <td nowrap class="normalText"  ><input type=text name="dev_value" class="textInputC" maxlength="16"/></td> 
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">开始折旧时间：</td>	 
	     <td nowrap class="normalText"  ><%=new com.viewhigh.cbcs.base.mvc.view.DateComponent("start_day",request.getParameter("start_day"))%></td> 
    <tr>
      <td class="signText" nowrap="nowrap">折旧率：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="dep_amount" class="textInputC" maxlength='30'/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">折旧年数：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="dep_years" class="textInputC" maxlength='30'/></td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="Create"/>
</form>

</html:html>   