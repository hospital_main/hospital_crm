<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/dict/confgcodemanage/confgCodeManageCreate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
    com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
    if(trim(template.set_cfg_code.value) =="") {
      alert('配置编码不能为空!');
      return;
    }
    		if(template.b_y_month.value == ""){
			alert('请选择起始年月!');
			return false;
		}
		if(template.e_y_month.value == ""){
	
			alert('请选择核算结束年月!');
			return false;
		}
		if(template.b_y_month.value>template.e_y_month.value){
			alert('起始年月应小于结束年月!');
			return false;
		}
    template.subFunction.value='Create';
    template.submit();
    return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="confgCodeManage.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>基本配置编码添加页面</html:title>

  <!-- 简单信息  -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
    
      <td class="signText" nowrap="nowrap">配置编码：</td>
      <td width="75%" class="normalText" nowrap="nowrap"><input type=text name="set_cfg_code" class="textInputC" maxlength="10"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap" >起始年月：</td>	      
	     <td nowrap class="normalText"  ><%=new MonthComponent("b_y_month",request.getParameter("b_y_month"))%></td> 

    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">终止年月：</td>	 
	     <td nowrap class="normalText"  ><%=new MonthComponent("e_y_month",request.getParameter("e_y_month"))%></td> 

    <tr>
      <td class="signText" nowrap="nowrap">配置编码说明：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="set_cfg_desc" class="textInputC" maxlength='30'/></td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="Create"/>
</form>

</html:html>   