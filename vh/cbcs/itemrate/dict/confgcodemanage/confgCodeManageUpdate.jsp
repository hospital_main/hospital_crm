<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/dict/confgcodemanage/confgCodeManageUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
    com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save(){    
    if(template.set_cfg_code.value =="") {
      alert('配置编码不能为空!');
      return;
    }
    		if(template.b_y_month.value == ""){
			alert('请选择起始年月!');
			return false;
		}
		if(template.e_y_month.value == ""){
	
			alert('请选择核算结束年月!');
			return false;
		}
		if(template.b_y_month.value>template.e_y_month.value){
			alert('起始年月应小于结束年月!');
			return false;
		}
    
    template.subFunction.value='Update';
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="confgCodeManage.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module"> 基本配置编码修改页面</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息 set_cfg_code, set_cfg_desc, b_y_month, e_y_month-->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">配置编码： </td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text name="set_cfg_code1" value="<%=result[0]%>" class="textInputC" readonly>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">起始年月：</td>
	    <td nowrap class="normalText"  ><%=new MonthComponent("b_y_month",result[2])%></td> 
    </tr>
    
    <tr>
      <td class="signText" nowrap="nowrap">终止年月：</td>
	    <td nowrap class="normalText"  ><%=new MonthComponent("e_y_month",result[3])%></td> 
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">配置编码说明：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="set_cfg_desc" value="<%=result[1]%>" class="textInputC" maxlength="30">
      </td>
    </tr>
     <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="set_cfg_code" value="<%=result[0]%>">
  <%}%>
</form>


</html:html>
