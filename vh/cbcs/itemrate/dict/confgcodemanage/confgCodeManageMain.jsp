<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/dict/confgcodemanage/confgCodeManageMain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="confgCodeManage.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>基本配置编码定义管理</html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
	    <tr>
	      <td nowrap class="signText">配置编码：</td>
	        <% String set_cfg_code = request.getParameter("set_cfg_code");%>
	      <td><input type=text name="set_cfg_code" class="textInputC" <%if(set_cfg_code != null){ out.println(" value=" + set_cfg_code);}%>></td>
	      <td nowrap class="signText">说明：</td>
	      <% String set_cfg_desc = request.getParameter("set_cfg_desc");%>
	      <td><input type=text name="set_cfg_desc" class="textInputC" <%if(set_cfg_desc!= null){ out.println(" value=" + set_cfg_desc);}%>></td>
	    </tr>
	    <tr>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      
	      	    <tr>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      <td class="normalText">
	      <button class="pageBtn" name="" onclick="return find();"  >查询</button>
	      <!--<img src="images/find.gif" class="mouse" onclick="return find();" />--></td>
	    </tr>	
	    </tr>

	  </html:table>

	  <br>

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
	  
  
		<html:title clazz='table'>基本配置编码定义管理</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>

		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		        
		            
		          <td nowrap="nowrap" class="resultLabel" >选择</td>
		          <td nowrap="nowrap" class="resultLabel" >配置编码</td>
		          <td nowrap="nowrap" class="resultLabel" >起始年月</td>
		          <td nowrap="nowrap" class="resultLabel" >终止年月</td>
		          <td nowrap="nowrap" class="resultLabel" >配置编码说明</td>
		        </html:tr>

		        <%
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>
 
		        <tr CLASS="<%=rowColor%>">
		          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
		          <td nowrap="nowrap" class="normalText"><a href="confgCodeManage.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>"><%=primaryKey%></a></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td> 

		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		<%}%>
	  <input type=hidden name="subFunction"/>
	</form>
</html:html>

