<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/dict/rateMateInfo/rateMateInfoUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
   if(trim(template.inv_code.value)==""){
			alert("材料编码不能为空！");
			return false;
		}
		if(trim(template.inv_name.value)==""){
			alert("材料名称不能为空！");
			return false;
		}
		if(trim(template.price.value)==""){
			alert("单价不能为空！");
			return false;
		}
    switch(isDouble(template.price, 8, 4))
    {
      case 0 : alert('单价必须为数字型'); return false;
      case 1 : alert('单价整数部分不能高于8个字符'); return false;
      case 3 : alert('单价小数部分不能高于4个字符'); return false;
    }
    template.subFunction.value='Update';
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="rateMateInfo.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module"> 材料信息修改</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
  
    <tr>
      <td class="signText" nowrap="nowrap">材料编码：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text name="inv_code" readonly value="<%=result[0]%>" class="textInputC">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">物资分类编码：</td>
      <td> <%=new Select(request.getAttribute("invTypeList"), "inv_type_code", result[1], true, true)%></td> 
      </td>
    </tr>
    
    <tr>
      <td class="signText" nowrap="nowrap">材料名称：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text name="inv_name" value="<%=result[2]%>" class="textInputC" maxlength="30"/>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">拼音码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="spell" value="<%=result[3]%>" class="textInputC" maxlength="6"/>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">规格型号：</td>
      <td class="normalText" nowrap="nowrap">
      <input type=text name="inv_model" value="<%=result[4]%>"  class="textInputC" maxlength="20"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">计量单位：</td>
      <td> <%=new Select(request.getAttribute("dictUnitList"), "unit_code",  result[5] , true, true)%></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">单价：</td>
      <td class="normalText" nowrap="nowrap">
      <input type=text name="price" value="<%=result[6]%>"  class="textInputC" /></td>
    </tr>
    
    
     <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="inv_type_code" value="<%=result[0]%>">
  <%}%>
</form>


</html:html>
