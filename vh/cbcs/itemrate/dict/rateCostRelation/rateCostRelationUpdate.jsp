<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/dict/rateCostRelation/rateCostRelationUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    if(trim(template.cost_subj_code.value)=="")
    {
      alert('直接成本项目不能为空!');
      return;
    }

    template.subFunction.value='Update';
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='preparedfindAll';
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="rateCostRelation.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module"> 物资分类修改</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">物资类别名称：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text name="inv_type_codes" value="<%=result[2]%>" class="textInputC" readonly>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">直接成本项目：</td>
      <% String cost_subj_code = request.getParameter("cost_subj_code")==null ? "":request.getParameter("cost_subj_code");%>
	      <td><%=new Select(request.getAttribute("cost_subj"),"cost_subj_code",result[1],true,false)%></td>
    </tr>
     <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
 <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="inv_type_code" value="<%=result[0]%>">
  <%}%>
</form>


</html:html>
