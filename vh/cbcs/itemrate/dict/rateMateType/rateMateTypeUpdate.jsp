<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/dict/rateMateType/rateMateTypeUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
								com.viewhigh.cbcs.base.mvc.view.component.Select" %> 

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
  	  	if(isCheckSysmodconform()=="1"){
    		alert('和物流或者科室成本系统联用,不能进行该操作！');
    		return;
    	} 
		if(trim(template.inv_type_code.value)==""){
			alert("物资分类编码不能为空！");
			return false;
		}
		if(trim(template.inv_type_name.value)==""){
			alert("物资分类名称不能为空！");
			return false;
		}
		if(trim(template.supper_code.value)==""){
			alert("上级编码不能为空！");
			return false;
		}
    		
    template.subFunction.value='Update';
    show_wait();
    template.submit();
    return true;
  }
function changeD(){
	if(template.is_last.value==0){
		Direct.style.display="none";
	}
	if(template.is_last.value==1){
		Direct.style.display="block";
	}
}
  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();    
    element.submit();
  }
  //判断项目核算系统与其他系统是否连用
   function isCheckSysmodconform(){
	   var p=getValuePairBySql("cbcsRate_isSysmodconform_dict","<sql_flag>1</sql_flag><mod_code1>dept</mod_code1><mod_code2>mate</mod_code2>");
	   return p[0];
	}
</Script>
<html:html clazz="main">
<form name="template" method="post" action="rateMateType.jspviewhigh">
 <%
 	 String[][] flag ={{"0"," 否"},{"1"," 是"}}; 
 %>
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module"> 物资分类修改</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">物资类别编号：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text name="inv_type_codes" readonly value="<%=result[0]%>" class="textInputC"/>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">物资类别名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="inv_type_name" value="<%=result[1]%>" class="textInputC" maxlength="10"/>
      </td>
    </tr>
    <tr id="Direct">
	    <td nowrap class="signText">直接成本项目：</td>
	    <td>
	    <%
	    Select sel= new Select(request.getAttribute("cost_subj"),"cost_subj_code",result[2],true,false);
		out.print(sel);
	    %>
	    </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">上级代码：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text name="supper_code" value="<%=result[3]%>" class="textInputC" maxlength="20"/>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">是否为末级：</td>
      <td class="normalText" nowrap="nowrap">
      <%Select sel2=new Select(flag,"is_last",(result[4].equals("true")? "1":(result[4].equals("false")? "0":result[4])),false,true);
      	sel2.setAttribute("onchange","changeD()");
      	out.print(sel2);
      %>
      </td>
    </tr>
     <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="inv_type_code" value="<%=result[0]%>">
  <%}%>
</form>


</html:html>
<script>
changeD();
</script>