<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/dict/rateCostBelType/rateCostBelTypeMain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:44 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
     $NoKeywords: $ 
 --> 
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
        show_wait();
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="rateCostBelType.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>成本所属分类定义</html:title>
      <%
	 		  String[][]  cost_class_code = (String [][])request.getAttribute("dict_cost_class_code");
	 		  String[][]  cost_class_name = (String [][])request.getAttribute("dict_cost_class_name");
	 		  String[][]  cost_subj_code = (String [][])request.getAttribute("dict_cost_subj_code");
	 		  String[][]  cost_subj_name = (String [][])request.getAttribute("dict_cost_subj_name");
	    %>
	  <!-- 简单信息 -->
	  <html:table clazz="simple"> 
	    <tr>
	      <td nowrap class="signText">成本分类编号：</td>
	      <td>
	        <%=new Select(cost_class_code,"cost_class_code",request.getParameter("cost_class_code")==null?"":request.getParameter("cost_class_code"),false,false)%>
	      </td>
	      <td nowrap class="signText">成本分类名称：</td>
	      <td>
	        <%=new Select(cost_class_name,"cost_class_name",request.getParameter("cost_class_name")==null?"":request.getParameter("cost_class_name"),false,false)%>
	      </td>
	    </tr>
	   	<tr>
	      <td nowrap class="signText">成本项目编号：</td>
	      <td>
	        <%=new Select(cost_subj_code,"cost_subj_code",request.getParameter("cost_subj_code")==null?"":request.getParameter("cost_subj_code"),false,false)%>
	      </td>
	      <td nowrap class="signText">成本项目名称：</td>
	      <td>
	        <%=new Select(cost_subj_name,"cost_subj_name",request.getParameter("cost_subj_name")==null?"":request.getParameter("cost_subj_name"),false,false)%>
	      </td>
	    </tr> 
	    <tr>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	    <tr>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      <td class="normalText">
	      <button class="pageBtn" name=""  onclick="return find();" >查询</button>
	      <!--<img src="images/find.gif" class="mouse" onclick="return find();" />--></td>
	    </tr>	
	    </tr>
	  </html:table>

	  <br>

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
	  
  
		<html:title clazz='table'>成本所属分类定义</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>
 		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td nowrap="nowrap" class="resultLabel" >选择</td>
		          <td nowrap="nowrap" class="resultLabel" >成本项目编码</td>
		          <td nowrap="nowrap" class="resultLabel" >成本项目名称</td>
		          <td nowrap="nowrap" class="resultLabel" >成本分类编号</td>
		          <td nowrap="nowrap" class="resultLabel" >成本分类名称</td>		          
		        </html:tr>

		        <%
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>
 
		        <tr CLASS="<%=rowColor%>">
		          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
		          <td nowrap="nowrap" class="normalText"><a href="rateCostBelType.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>" onclick="show_wait()"><%=primaryKey%></a></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 1 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td>							
		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		<%}%>
	  <input type=hidden name="subFunction"/>
	</form>
</html:html>

