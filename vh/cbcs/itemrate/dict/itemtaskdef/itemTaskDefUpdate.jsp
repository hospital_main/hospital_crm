<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/dict/itemtaskdef/itemTaskDefUpdate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:44 $
     $Modtime: 03-09-10 14:01 $
     $Revision: 1.1 $
     $NoKeywords: $ 
-->  
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
	  if(trim(template.work_name.value)=="")
	    {
	      alert('作业名称不能为空!');
	      return;
	    }    
	
    template.subFunction.value='Update';
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="itemTaskDef.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz="module">工资项目作业定义修改页面</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>     
      <td class="signText" nowrap="nowrap">作业编码：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0]%>"  disabled />
       </td>
  	</tr>
  	<tr>
      <td class="signText" nowrap="nowrap">作业名称：</td>
      <td><input type=text name="work_name" class="textInputC" value="<%=result[1]%>" maxlength='20'></td> 
    </tr>
     <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "Update"/>
  <input type="hidden" name="work_code" value="<%=result[0]%>">
  <%}%>
</form>

</html:html>
