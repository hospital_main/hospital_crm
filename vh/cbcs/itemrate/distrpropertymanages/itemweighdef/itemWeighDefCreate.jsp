<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/distrpropertymanages/itemweighdef/itemWeighDefCreate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
                        com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
    	if (template.set_cfg_code.value =="")
	{
		alert("请选择配置描述!");
		return false;
	}
	if (template.charge_detail_code.value =="")
	{
		alert("请选择医疗项目!");
		return false;
	}
	if (template.cost_class_code.value =="")
	{
		alert("请选择成本分类");
		return false;
	}


   	switch(isDouble(template.right,10,4))
     {
      case 0 : alert('权重需输入数字类型!');
      return;
     }

       template.subFunction.value='Create';
        template.submit();
        return true;
    }
  // 返回
	function changeitems(){
		var now=new Date();
		netA.Gsend("itemWeighDef.jspviewhigh?subFunction=select&set_cfg_code="+template.set_cfg_code.value+"&stamp="+now.getTime(),todochange);
		document.body.style.cursor="wait";
	}

	function todochange(){
        jhtcSetHtml(cost_type, '<select class="selectBg" name="cost_class_code" >'+netA.req.responseText+'</select>');
		document.body.style.cursor="default";
		template.cost_class_code.value='<%=request.getParameter("cost_class_code")%>';
	}
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemWeighDef.jspviewhigh">
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>添加医疗项目权重</html:title>
  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">配置描述：</td>
      <td  class="normalText" nowrap="nowrap">
      <%
      	Select setcfgcode=new Select(request.getAttribute("set_cfg_code"),"set_cfg_code",request.getParameter("set_cfg_code")==null? "":request.getParameter("set_cfg_code"),false,false);
      	setcfgcode.setAttribute("onchange","changeitems()");
      %><%=setcfgcode.toString()%></td>
       <td nowrap class="signText">医疗项目：</td>
        <td class="normalText" nowrap="nowrap">
        <?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea" name="charge_detail_code" value="<%=request.getParameter("charge_detail_code")==null? "":request.getParameter("charge_detail_code")%>" AdjustVal="95" previousObj="set_cfg_code" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="39" left="468" Lheight="5" xmlSource="dic/dict_charge_detail_LVT.xml" init="1"/>
        </td>
    <tr>
      <td class="signText" nowrap="nowrap">成本分类：</td>
      <td id="cost_type" class="normalText" nowrap="nowrap"><%=new Select(request.getAttribute("cost_class_code"),"cost_class_code","" ,false,false) %></td>
       <td class="signText" nowrap="nowrap">权重：</td>
			<td class="normaltext"><input type="text"  name="right"  class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />-->
			</td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="Create"/>
</form>

</html:html>
<script>
template.set_cfg_code.onchange();
</script>