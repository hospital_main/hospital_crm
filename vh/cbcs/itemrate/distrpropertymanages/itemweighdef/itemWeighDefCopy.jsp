<!--/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/distrpropertymanages/itemweighdef/itemWeighDefCopy.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:58:44 $
 * $Modtime: $
 * $Revision: 1.1 $
 */ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
  //添加
		function confrim() {
			if (template.set_cfg_code_s.value =="")
			{
				alert("请选择数据源的配置编码!");
				return;
			}
			if (template.set_cfg_code_d.value =="")
			{
				alert("请选择目标的配置编码!");
				return;
			}
			if (template.set_cfg_code_s.value ==template.set_cfg_code_d.value&&''==template.charge_detail_code.value)
			{
				alert("请选择数据源的医疗项目!");
				return;
			}	
				if (template.set_cfg_code_s.value!=template.set_cfg_code_d.value&&''==template.charge_detail_code.value&&''!=template.charge_detail_code1.value)
			{
				alert("请选择数据源的医疗项目!");
				return;
			}	
			if (template.set_cfg_code_s.value ==template.set_cfg_code_d.value&&template.charge_detail_code1.value ==template.charge_detail_code.value)
			{
				alert("当前设置无法进行复制!");
				return;
			}
			netA.Gsend("itemWeighDef.jspviewhigh?subFunction=check&set_cfg_code_s="+template.set_cfg_code_s.value+"&charge_detail_code1="+template.charge_detail_code1.value,copyAccess);
		}
	function copyAccess(){
		var num=new Number(netA.req.responseText);
		
		if(isNaN(num)){
			alert(netA.req.responseText);
		}else{	
			if(netA.req.responseText>0){
				if(confirm("拷贝目标存在数据，确认将会覆盖已存在数据，确认？")){
					doCopy();
				}
			}else{
				doCopy();
			}
		}
  }		
	function doCopy(){
			template.subFunction.value="copy";
			show_wait();
	    template.submit();
	    return true;
   }
     
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="itemWeighDef.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>    


  <!-- 标题栏 -->
	 <html:title clazz='module'>医疗项目权重复制界面</html:title>  
  <!-- 简单信息 -->   
  <table  width="100%" cellspacing="2" border="0">
  	<tr><td colspan=2 >数据源: </td> <td>目标: </td>
    </tr>
    <tr>   
      <td class="signText" nowrap="nowrap">配置编码:</td>
      <td nowrap class="normalText"><%=new Select(request.getAttribute("set_cfg_code"),"set_cfg_code_s",request.getParameter("set_cfg_code_s"),false,false)%></td>
      <td class="signText" nowrap="nowrap">配置编码:</td>
      <td align="left"><%=new Select(request.getAttribute("set_cfg_code"),"set_cfg_code_d",request.getParameter("set_cfg_code_d"),false,false)%></td>
		</tr>
		<tr>
     <td class="signText" nowrap="nowrap">医疗项目：</td>
     <td class="normalText" nowrap="nowrap">
        <?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea" name="charge_detail_code" value="<%=request.getParameter("charge_detail_code")==null? "":request.getParameter("charge_detail_code")%>" AdjustVal="95" previousObj="set_cfg_code" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="39" left="468" Lheight="5" xmlSource="dic/dict_charge_detail_LVT.xml" init="1"/>
     </td>
     <td class="signText" nowrap="nowrap">医疗项目：</td>
     <td class="normalText" nowrap="nowrap">
        <?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea" name="charge_detail_code" value="<%=request.getParameter("charge_detail_code")==null? "":request.getParameter("charge_detail_code")%>" AdjustVal="95" previousObj="set_cfg_code" codeCol='spell' valueCol="charge_detail_code" textCol="charge_detail_name" width="200" top="39" left="468" Lheight="5" xmlSource="dic/dict_charge_detail_LVT.xml" init="1"/>
     </td>
    </tr>
    <tr><td><br><br></td></tr>
   	<tr align="center">
      <td colSpan="4"><button class="pageBtn" onclick="return confrim();">确定</button>   
      &nbsp;&nbsp;&nbsp;&nbsp;
<button class="pageBtn" onclick="return back(template);">返回</button>  
<!--<img src="images/confirm.gif" class="mouse" onclick="return confrim();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />-->
</td>
    </tr>
    
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   