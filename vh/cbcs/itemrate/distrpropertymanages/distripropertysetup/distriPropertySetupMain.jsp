<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/itemrate/distrpropertymanages/distripropertysetup/distriPropertySetupMain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent,
								 com.viewhigh.cbcs.cbcs.util.SimpleSearch" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript"> 
	function save(){ 
		template.subFunction.value="save";
		template._current_page.value='<%=request.getParameter("_current_page")==null? "0":request.getParameter("_current_page")%>';
		show_wait();
		template.submit();
		return;
	}	
  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="distriPropertySetup.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'> 分配参数定义 </html:title>



	  <!-- 简单信息 -->  
	  <html:table clazz="simple"> 
		<tr>	 
	   <td nowrap class="signText">配置编码： </td>
     <td class="normalText" nowrap="nowrap"><%=new Select(request.getAttribute("set_cfg_code"),"set_cfg_code",request.getParameter("set_cfg_code")==null? "":request.getParameter("set_cfg_code") ,false,false)%></td>
     <td class="normalText">
     <button class="pageBtn" name=""  onclick="return find();" >查询</button>
     <!--<img src="images/find.gif" class="mouse" onclick="return find();" />--></td>
	  </tr>		      
	  </html:table>
	  

	  <%
 
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/save.gif", "return save()");   // 全选
	  %>
	  
  
		<html:title clazz='table'> 分配参数定义 </html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>

		    	<html:table clazz="result">
		        <html:tr clazz='label'>		            
		          <td nowrap="nowrap" class="resultLabel" >配置编码</td>
		          <td nowrap="nowrap" class="resultLabel" >成本分类</td>
		          <td nowrap="nowrap" class="resultLabel" >分配参数</td>
		          <td nowrap="nowrap" class="resultLabel" >是否加权</td> 
		        </html:tr>

		        <%
		        
		          java.text.DecimalFormat mf=new java.text.DecimalFormat("#,##0.00");
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey =  result[ i ][ 0 ]+"_"+result[ i ][ 2 ];
		                for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>
 
		        <tr CLASS="<%=rowColor%>">  
		          <td nowrap="nowrap" class="normalText"> <input  type=hidden name ="primaryKey" value="<%=primaryKey%>" readonly>
		                                                 <%=result[ i ][ 0 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 3 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=new Select(request.getAttribute("para_value_list"),"para_value",result[i][4] ,false,false)%></td>
		          <td nowrap="nowrap" class="normalText"><%=new Select(request.getAttribute("is_a_right_list"),"is_a_right",result[i][5] ,false,false)%></td>  
		        </tr>
<input type="hidden" name ="dddd" value=<%=result[i][5]%> 
		        <%
		 
		              }
		            } 
		        %>
		      </html:table>
		<%}%> 
		
	  <input type='hidden' name="subFunction"/>

	</form>
</html:html>

