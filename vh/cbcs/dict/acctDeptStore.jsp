<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/acctDeptStore.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<html:html clazz="main">

<%
  String[] result = (String[])request.getAttribute("result");
%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
<%
  String message = request.getAttribute("message")==null?"":(String)request.getAttribute("message");
  if(message.equals("添加成功"))
    out.println("parent.frames[0].spring.click()");
%>
  function store()
  {
    if(isEmpty(template.dept_name))
    {
      alert('科室名称不能为空!');
      return;
    }
    if (isEmpty(template.app_level)) {
      alert('分摊级别不能为空!');
      return;
    }
    if (isTooLong(template.dept_name,40)) {
      alert('科室名称不能高于40个字符!');
      return;
    }
    if (!isNumber(template.app_level)) {
      alert('分摊级别必须为数字类型!');
      return;
    }
    template.subFunction.value='store';
		show_wait();
    template.submit();
    return true;
  }
  function create() {
    if(template.last_level.value=='Y'){
      alert('<%if(result!=null) out.print(result[1]);%>'+'已经为末级科室')
      return
    }
    template.subFunction.value='preparedCreate';
		show_wait();
    template.submit();
    return true;
  }
</Script>
<%
  String flag = (String)request.getParameter("flag");
%>
<form name="template" method="post" action="dictacctdept.jspviewhigh">
  <!-- 信息提示栏 -->
<%  if(flag==null){%>
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>

  <%}
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <table id ='table1' width="100%" cellspacing="2" border="0" style='visibility:visible' >
    <tr>
      <td class="signText" nowrap="nowrap">科室代码：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0]%>" readonly disabled />
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">科室名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="dept_name" value="<%=result[1]%>" class="textInputC">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">上级科室代码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="supper_dept" readonly disabled value="<%=result[2]%>"/>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">科室类别：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(request.getAttribute("init_depttype"), "app_dept_type_code", result[3], false, true)%>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">分配类别：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(request.getAttribute("subj_kind"), "subj_kind_code", result[10], false, true)%>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">科室级别：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="dept_grade" id="grade" readonly disabled value="<%=result[4]%>" class="textInputC">
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">参与成本分摊标志：</td>
      <td class="normalText" nowrap="nowrap">
         是<input type="radio" name="cost_app_ind" value="Y" <% if(result[5].equals("Y")){out.print(" checked ");}%>/>
         否<input type="radio" name="cost_app_ind" value="N" <% if(result[5].equals("N")){out.print(" checked ");}%>/>
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">分摊级别：</td>
      <td class="normalText" nowrap="nowrap">
         <input type=text name="app_level" value="<%=result[6]%>"/>
      </td>
    </tr>

    <tr>
      <td class="normalText" nowrap="nowrap">末级标志：</td>
      <td class="normalText" nowrap="nowrap">
         是<input type="radio" name="dept_last_level" value="Y" <% if(result[7].equals("Y")){out.print(" checked ");}%>/>
         否<input type="radio" name="dept_last_level" value="N" <% if(result[7].equals("N")){out.print(" checked ");}%>/>
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">门诊住院标识：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(request.getAttribute("init_outorin"), "out_or_in", result[8], false, false)%>
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">停用标志：</td>
      <td class="normalText" nowrap="nowrap">
       是<input type="radio" name="stop_mark" value="Y" <% if(result[9].equals("Y")){out.print(" checked ");}%>/>
       否<input type="radio" name="stop_mark" value="N" <% if(result[9].equals("N")){out.print(" checked ");}%>/>
      </td>
    </tr>

    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return store();" >保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return store();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/create.gif" class="mouse" onclick="return create();" /> --> </td>
    </tr>
  </table>
  <input type="hidden" name="supper_dept" value = "<%=result[2]%>"/>
  <%}%>
  <input type="hidden" name="subFunction" value = "store"/>
  <input type="hidden" name="dept_code" <%if(result==null) out.print("value=\'TOP\'"); else out.print("value=\'"+result[0]+"\'");%>>
  <input type="hidden" name="dept_grade" <%if(result==null) out.print("value=\'0\'"); else out.print("value=\'"+result[4]+"\'");%>>
  <input type="hidden" name="last_level" <%if(result==null) out.print("value=\'0\'"); else out.print("value=\'"+result[7]+"\'");%>>
  <td colspan="2"><img id='but' src="images/create.gif" class="mouse" onclick="return create();"  style='display:none;' /> </td>
</form>
<Script Language="JavaScript">
<%
  if(flag!=null && flag.equals(""))
    out.println("template.but.style.display=\'block\'");
%>
</Script>

</html:html>
