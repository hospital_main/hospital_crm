<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictEmployeeSave.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:57:47 $
  $Modtime: 03-08-26 23:13 $
	$Revision: 1.1 $
	$NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save() {
    if(isEmpty(template.emp_name)) {
      alert('职工姓名不能为空!');
      return;
    }
    if(isTooLong(template.emp_name,20)) {
      alert('职工姓名不能高于12个字符!');
      return;
    }
    show_wait();
    template.submit();
    return true;
	}


  function back() {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
		show_wait();
  	template.submit();
  	return true;
  }

</Script>
<html:html clazz="main">
    <form name="template" method="post" action="dictEmployee.jspviewhigh">
     <!-- 信息提示栏 -->
       <html:message/>
      <!--信息栏-->
      <html:title clazz='module'>职工信息修改页面</html:title>

      <%
       String[] result = (String[]) request.getAttribute("result");
         if(result!=null){
      %>
      <!-- 简单信息 -->
      <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap="nowrap">职工编号：</td>
          <td width="75%" class="normalText" nowrap="nowrap">
            <input type=text value="<%=result[0]%>" disabled />
            <input type=hidden name="emp_id" value="<%=result[0]%>" class="textInputC" maxlength="20"/>
          </td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">职工姓名：</td>
          <td class="normalText" nowrap="nowrap">
            <input type=text name="emp_name" value="<%=result[1]%>" class="textInputC" maxlength="40"/>
          </td>
        </tr>
         <tr>

      <td class="signText" nowrap="nowrap">性别：</td>
      <td class="normalText" nowrap="nowrap">
		    <%=new SingleSelect(request.getAttribute("init_sex"), "sex", result[2], false, true)%>
		  </td>
     </tr>
       <tr>
      <td class="signText" nowrap="nowrap">所在科室：</td>
      <td class="normalText" nowrap="nowrap">
		    <%=new SingleSelect(request.getAttribute("init_dept_code"), "dept_code", result[3], true, true)%>
		  </td>
     </tr>
     <tr>
      <td class="signText" nowrap="nowrap">职务：</td>
      <td class="normalText" nowrap="nowrap">
		    <%=new SingleSelect(request.getAttribute("init_duty_code"), "duty_code", result[4], false, false)%>
		  </td>
     </tr>
      <tr>
      <td class="signText" nowrap="nowrap">人员类别：</td>
      <td class="normalText" nowrap="nowrap">
		    <%=new SingleSelect(request.getAttribute("init_kind_code"), "kind_code", result[5], false, false)%>
		  </td>
     </tr>
     <tr>
      <td class="signText" nowrap="nowrap">职称：</td>
      <td class="normalText" nowrap="nowrap">
		    <%=new SingleSelect(request.getAttribute("init_title_code"), "title_code", result[6], false, false)%>
		  </td>
     </tr>
     <tr>
      <td class="signText" nowrap="nowrap">学历：</td>
      <td class="normalText" nowrap="nowrap">
		    <%=new SingleSelect(request.getAttribute("init_edu_code"), "edu_code", result[7], false, false)%>
		  </td>
     </tr>
      <tr>
      <td class="signText" nowrap="nowrap">停用标志：</td>
      <td class="normalText" nowrap="nowrap">
		     是<input type="radio" name="stop_mark" value="Y"   <%if(result[8].equals("Y")) out.println("checked");%>/>
         否<input type="radio" name="stop_mark" value="N"  <%if(result[8].equals("N")) out.println("checked");%>/>
		  </td>
    </tr>
    <tr>
        <tr>
          <td colspan="2">
          <button class="pageBtn" onclick="return save();">保存</button> 
         <button class="pageBtn" onclick="return reset();" >重置</button><button class="pageBtn" onclick="return back();">返回</button>  
          <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
          <img src="images/reset.gif" class="mouse" onclick="return reset();" /> <img src="images/return.gif" class="mouse" onclick="return back();" />--></td>
        </tr>
      </html:table>
      <input type="hidden" name="initsub" value="sub"/>
      <input type=hidden name="subFunction" value = "store"/>
      <input type=hidden name="anly_indx_code" value="<%=result[0]%>">
      <%}%>
  	</form>


</html:html>
