<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictFixAssetDeprTypeSave.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 21:41 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    if(isEmpty(template.fix_asset_depr_type_name))
    {
      alert('固定资产折旧类型名称不能为空!');
      return;
    }
    if(isTooLong(template.fix_asset_depr_type_name,20))
    {
      alert('固定资产折旧类型名称不能大于20个字符!');
      return;
    }
    //template.subFunction.value='save';
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    template._current_page.value=<%=request.getParameter("_current_page")%>
		show_wait();
    element.submit();
  }
</Script>

<html:html clazz="main">  <form name="template" method="post" action="fixassetdeprtype.jspviewhigh">
  <!-- 信息提示栏 -->
 <html:message/>

  <!-- 标题栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("固定资产折旧类型修改页面")%>

  <%
    String[] result = (String[])request.getAttribute( "result" );
    if ( result != null )
    {
  %>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" class="simpleQuery">
    <tr>
      <td class="signText" nowrap="nowrap">固定资产折旧类型代码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0]%>" disabled />
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">固定资产折旧类型名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="fix_asset_depr_type_name" value="<%=result[1]%>" class="textInputC">
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">停用标志：</td>
      <td class="normalText" nowrap="nowrap">
        是<input type="radio" name="stop_mark" value="Y" <% if(result[2].equals("Y")){out.println(" checked ");}%>/>
        否<input type="radio" name="stop_mark" value="N" <% if(result[2].equals("N")){out.println(" checked ");}%>/>
      </td>
    </tr>


    <tr>
      <td colspan="2" align="center">
      <button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" /> <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="initsub" value="sub"/>
  <input type="hidden" name="fix_asset_depr_type_code" value="<%=result[0]%>">
  <input type=hidden name="_current_page" value = "<%=request.getParameter("_current_page")%>" />
  <%}%>
</form>


</html:html>
