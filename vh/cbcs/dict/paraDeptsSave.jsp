<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/paraDeptsSave.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-13 9:46 $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(101, request)%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    switch(isDouble(template.value,16,2))
    {
      case 0 : alert('数量必须为数字型'); return;
      case 1 : alert('数量整数部分不能高于10个字符'); return;
      case 2 : alert('数量没有整数部分'); return;
      case 3 : alert('数量小数部分不能高于2个字符'); return;
    }

    //template.subFunction.value='save';
    template.subFunction.value='save';
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
		show_wait();
    element.submit();
  }
</Script>

<form name="template" method="post" action="paraDepts.jspviewhigh">

  <!-- 返回信息栏 -->
  <table width="100%" border="0">
    <tr>
      <td>
        <table width="100%" border="0" >
        <%
          String message = (String)request.getAttribute("message");
          if (message!=null && !message.trim().equals(""))
          {
        %>
          <tr>
            <td class="errorText" align="center"><%=message%></td>
            <td align="center"><button class="pageBtn" onclick="history.go(-1);">返回</button> 
            <!--<img src="images/return.gif" onclick="history.go(-1);" style="cursor:hand">--></td>
          </tr>
        <%} else if (request.getParameter("subFunction").equals("save")) { %>
          <tr>
            <td class="successText" align="center">修改成功</td>
          </tr>
        <%}%>
      </table>
    <td>
  <tr>
  </table>

  <!-- 标题栏 -->
  <table width="100%" border="0">
    <tr>
      <td>
        <table width="100%" border="0" >
          <tr>
            <td  class="moduleTitle" nowrap="nowrap">科室基本情况表修改页面：</td>
          </tr>
        </table>
      <td>
    <tr>
  </table>


  <%
    String[] result = (String[])request.getAttribute( "result" );
    if ( result != null )
    {
  %>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">科室代码：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0]%>" disabled />
        <input type=hidden name="dept_code" value="<%=result[0]%>" class="textInputC" maxlength="20"/>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">分摊参数代码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[1]%>" disabled />
        <input type=hidden name="app_para_code" value="<%=result[1]%>" class="textInputC" maxlength="10"/>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">数量：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="value" value="<%=result[2]%>" class="textInputC">
      </td>
    </tr>

     <%
      String[][] paraDeptsInfo =
        (String[][])request.getAttribute("paraDeptsInfo");

    %>
    <tr>
      <td colspan="2">
      <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" /><img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" />
  <%}%>
</form>


<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>
