<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/costSubjTree.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime:  $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<html:html clazz="child">
  <script src="javascript/tree.js"></script>
<%
  String script = (String)request.getAttribute("script");
%>
<input type = "hidden" name = "selectednode" value = "ok">
<script language="javascript">
  <%=script%>
  function create(){
    var str = cost_subj_name.value
    if(last_level.value=='Y'){
      aux1 = insDoc(indexOfEntries[selectednode.value], gLnk(2,0,str,"dictcostsubj.jspviewhigh?subFunction=find&cost_subj_code="+cost_subj_code.value))
      addNode(aux1)
    }
    else{
      aux1 = insFld(indexOfEntries[selectednode.value], gFld(str,"dictcostsubj.jspviewhigh?subFunction=find&cost_subj_code="+cost_subj_code.value))
      addNode(aux1)
    }
  }
  initializeDocument()
</script>
<input type = "button" name = "spring" onclick = "create()" style='display:none;'>
<input type = "hidden" name = "cost_subj_code">
<input type = "hidden" name = "cost_subj_name">
<input type = "hidden" name = "last_level">
</html:html>
