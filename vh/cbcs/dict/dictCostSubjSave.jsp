<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictCostSubjSave.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-29 11:15 $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    if(isEmpty(template.cost_subj_name))
    {
      alert('成本项目名称不能为空!');
      return;
    }
    if(isEmpty(template.supp_item_code))
    {
      alert('上级代码不能为空!');
      return;
	  }
    if(isEmpty(template.treat_or_med)) {
      alert('医疗/药品标识不能为空!');
      return;
	  }
	  if(isTooLong(template.cost_subj_code,20)) {
      alert('成本项目代码不能大于20个字符!');
      return;
    }
    if(isTooLong(template.cost_subj_name,40)) {
      alert('成本项目名称不能大于40个字符!');
      return;
	  }
    if(isTooLong(template.supp_item_code,20)) {
      alert('上级代码不能大于20个字符!');
      return;
    }
    if(isTooLong(template.spell,10))
    {
      alert('拼音码不能大于10个字符!');
      return;
    }
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    template._current_page.value=<%=request.getParameter("_current_page")%>
		show_wait();
    element.submit();
  }
  
   function makeSpell ( obj)
  {
  		var p=getValuePairBySql("cbcs_sys_get_name_pym","<name>"+obj.value+"</name>");
  		if(p!=null && p[1]!='')
		template.spell.value=p[1];

  }
</Script>

<html:html clazz="main">  <form name="template" method="post" action="costsubj.jspviewhigh">
	<!-- 信息提示栏 -->
 <html:message/>

  <!-- 标题栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("成本项目修改页面")%>

  <%
    String[] result = (String[])request.getAttribute( "result" );
    if ( result != null )
    {
  %>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" class="simpleQuery" >
    <tr>
      <td class="signText" nowrap="nowrap">成本项目代码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0]%>" disabled />
      </td>
      <td class="signText" nowrap="nowrap">成本项目名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="cost_subj_name" value="<%=result[1]%>" class="textInputC" onblur="makeSpell(this)">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap" >上级代码：</td>
      <td class="normalText" nowrap="nowrap">
        <input  disabled="disabled" type=text value="<%=result[2]%>"/>
          <input type="hidden" name="supp_item_code" value="<%=result[2]%>">
      </td>
      <td class="signText" nowrap="nowrap">医疗/药品标识：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("init_treat_or_med"), "treat_or_med", result[3], false, true)%>
		  <input type="hidden" name="last_level" value="<%=result[5]%>">
        </td>
    </tr>

    <tr>
      <td class="signText"  nowrap="nowrap">是否末级：</td>
      <td class="normalText" nowrap="nowrap" >
        是<input type="radio" disabled="disabled" name="last_levely" value="Y" <% if(result[5].equals("Y")){out.println(" checked ");}%>/>
        否<input type="radio" disabled="disabled" name="last_leveln" value="N" <% if(result[5].equals("N")){out.println(" checked ");}%>/>
      </td>
      <td class="signText" nowrap="nowrap">拼音码：</td>
      <td class="normalText" nowrap="nowrap">  
         <input type=text name="spell"  value="<%=result[7]%>"/>
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">是否停用：</td>
      <td class="normalText" nowrap="nowrap">
        是<input type="radio" name="stop_mark" value="Y" <% if(result[4].equals("Y")){out.println(" checked ");}%>/>
        否<input type="radio" name="stop_mark" value="N" <% if(result[4].equals("N")){out.println(" checked ");}%>/>
      </td>
	  <td></td>
    </tr>
    <tr>
      <td colspan="4" align="center">
      <button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="initsub" value="sub"/>
  <input type="hidden" name="cost_subj_code" value="<%=result[0]%>">
  <input type=hidden name="_current_page" value = "<%=request.getParameter("_current_page")%>" />
  <%}%>
</form>


</html:html>
