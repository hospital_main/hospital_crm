<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictAnlyIndxSave.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:57:47 $
  $Modtime: 03-08-29 11:15 $
	$Revision: 1.1 $
	$NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(101, request)%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save() {
   if(isTooLong(template.anly_indx_name,100)) {
      alert('分析指标名称不能高于100个字符!');
      return;
    }
		show_wait();
    template.submit();
    return true;
	}

  function back() {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
		show_wait();
  	template.submit();
  	return true;
  }

</Script>

    <form name="template" method="post" action="dictAnlyIndx.jspviewhigh">
     <!-- 信息提示栏 -->
       <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>
      <!--信息栏-->
       <%= new com.viewhigh.cbcs.base.mvc.view.MessageTitle("分析指标字典修改页面")%>
      <%
       String[] result = (String[]) request.getAttribute("result");
         if(result!=null){
      %>
      <!-- 简单信息 -->
      <table  width="100%" cellspacing="2" border="0" >
        <tr>
          <td class="signText" nowrap="nowrap">分析指标代码：</td>
          <td width="75%" class="normalText" nowrap="nowrap">
            <input type=text value="<%=result[0]%>" disabled />
            <input type=hidden name="anly_indx_code" value="<%=result[0]%>" class="textInputC" maxlength="20"/>
          </td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">分析指标名称：</td>
          <td class="normalText" nowrap="nowrap">
            <input type=text name="anly_indx_name" value="<%=result[1]%>" class="textInputC" maxlength="40"/>
          </td>
        </tr>
        <tr>
          <td colspan="2">
          <button class="pageBtn" onclick="return save();">保存</button> 
          <button class="pageBtn" onclick="return reset();" >重置</button>
          <button class="pageBtn" onclick="return back();">返回</button>  
          <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
           <img src="images/reset.gif" class="mouse" onclick="return reset();" />
           <img src="images/return.gif" class="mouse" onclick="return back();" />--> </td>
        </tr>
      </table>
      <input type=hidden name="subFunction" value = "store"/>
      <input type=hidden name="anly_indx_code" value="<%=result[0]%>">
      <%}%>
  	</form>


<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>