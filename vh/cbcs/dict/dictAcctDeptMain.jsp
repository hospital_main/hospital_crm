<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictAcctDeptMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
	Calendar thisMonth=Calendar.getInstance(); 
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;
%>
<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
				show_wait();
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
					show_wait();
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    var num=document.getElementById('num').value;
    var number = new Number(num);
    if(isNaN(number)){
      alert('科室级别应为数字');
      return;
    }
    if(!checkLegality(template.dept_code)){
      alert('科室代码含有非法字符！')
      return true;
    }
    if(!checkLegality(template.dept_name)){
      alert('科室名称含有非法字符！')
      return true;
    }
    if(!checkLegality(template.app_level)){
      alert('分摊级别含有非法字符！')
      return true;
    }
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }

	var child;
  function updateCode(){
	 	//window.open("cbcs/dict/codeChange.jsp", "","status:no;dialogHeight: 250px; dialogWidth: 500px;");
		if(child!=null&&typeof(child.template)!='undefined'){
			child.focus();
			return;
		}
		child=window.open("cbcs/dict/codeChange.html",'child', 'width=500, height=250, top=390, left=300, scrollbars = 1, resizable = no');
		//window.showModalDialog("cbcs/dict/codeChange.html", window,"status:no;dialogHeight: 250px; dialogWidth: 500px;");
  }
  
  function initpage(){
    
  }

</Script>

<html:html clazz="main">
	<form name="template" method="post" action="acctdept.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>核算科室主页面</html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
	    <tr>
	      <td nowrap class="signText">科室代码：</td>
	        <% String dept_code = request.getParameter("dept_code");%>
	      <td><input type=text name="dept_code" class="textInputC" <%if(dept_code != null){ out.println(" value=" + dept_code);}%>></td>
	     <td class="signText">科室级别：</td>
	      <td class="normalText">
	      	        <% String dept_level = request.getParameter("dept_level");%>
        <input type=text name="dept_level" id='num' style="width:140px" class="textInputC" <%if(dept_level != null){ out.println(" value=" + dept_level);}%> >
	      </td>
	      <td colspan="2">科室属性：
	      	          <select class="selectBg" name="dept_propety" style="width:140px">
	          <%String[][] OUTORIN=(String[][])request.getAttribute("OUTORIN");
	          %>
	              <option value=''></option>
	              <%String dept_propety=request.getParameter("dept_propety");%>
	              <%for(int j=0;j<OUTORIN.length;j++){%>
	              <option value="<%=OUTORIN[j][0]%>" <%if(dept_propety!=null&&!dept_propety.equals("")&&dept_propety.equals(OUTORIN[j][0])) out.println(" selected ");%> ><%=OUTORIN[j][1]%></option>
	              <%}%>	          </select>
	      </td>
	    </tr>
	    <tr>
	    <td nowrap class="signText">科室名称：</td>
	      <% String dept_name = request.getParameter("dept_name");%>
	      <td><input type=text name="dept_name" class="textInputC" <%if(dept_name!= null){ out.println(" value=" + dept_name);}%> ></td>

	     <td class="signText">分配类别：</td>
	     <td>

	         <%=new Select(request.getAttribute("propertys"), "propertys", request.getParameter("propertys"), false, false)%>
	      </td>


        <td colspan="2">科室类别：
	      

	         <%=new Select(request.getAttribute("init_depttype"), "app_dept_type_code", request.getParameter("app_dept_type_code"), false, false)%>
	      </td>
	    </tr>
	    <tr>
	      <td class="signText">分摊级别：</td>
	      <td class="normalText"><input type=text name="app_level" class="textInputC" value="<%if (request.getParameter("app_level")!=null) out.print(request.getParameter("app_level"));%>" style="width:140px;"></td>
	      <td class="signText">是否停用：</td>
	      <td class="normalText">
	           <select class="selectBg" name="stop_mark" style="width:140px;">
	              <option value=''></option>
	              <%String stop_mark=request.getParameter("stop_mark");%>
	              <option value="Y" <%if(stop_mark!=null&&!stop_mark.equals("")&&stop_mark.equals("Y")) out.println(" selected ");%> >是</option>
	              <option value="N" <%if(stop_mark!=null&&!stop_mark.equals("")&&stop_mark.equals("N"))   out.println(" selected ");%> >否</option>
	          </select></td>
		<td colspan="2">是否末级： <select class="selectBg" name="last_if" style="width:140px">
	              <option value=''></option>
	              <%String last_if=request.getParameter("last_if"); %>
	              <option value="Y" <%if(last_if!=null&&!last_if.equals("")&&last_if.equals("Y")){ out.println(" selected ");}%> >是</option>
	              <option value="N" <%if(last_if!=null&&!last_if.equals("")&&last_if.equals("N")) {  out.println(" selected ");}%> >否</option>
	          </select></td>
	      </tr>

	      <tr>
        <td style="display:none" class="normalText" nowrap ><%=new MonthComponent("year_month", (request.getParameter("year_month")==null?(String.valueOf(thisMonth.get(Calendar.YEAR))+curMonth):(request.getParameter("year_month")))) %></td>
			<td class="signText">参与分摊：</td>
	        <td><select class="selectBg" name="cost_app" style="width:140px">
	              <option value=''></option>
	              <%String cost_app=request.getParameter("cost_app");%>
	              <option value="Y" <%if(cost_app!=null&&!cost_app.equals("")&&cost_app.equals("Y")){ out.print(" selected ");}%> >是</option>
	              <option value="N" <%if(cost_app!=null&&!cost_app.equals("")&&cost_app.equals("N")) {  out.print(" selected ");}%> >否</option>
	          </select>
	        </td>

	      <td class="signText">拼音码：</td>
	      <td class="normalText"><input type=text name="spell" class="textInputC" value="<%if (request.getParameter("spell")!=null) out.print(request.getParameter("spell"));%>" style="width:140px;"></td>
	      <td colspan="2" align="right">
		  <button class="pageBtn" onclick="return find();" >查询</button>
		  <!--button class="pageBtn" onclick="return updateCode();" >修改</button-->
		  <button class="pageBtn" onclick="return preparedPrint();">打印</button>
	      <!--<img src="images/xiugai.png" class="mouse" onclick="return updateCode();" />
	      <img src="images/print.gif" class="mouse" onclick="return preparedPrint();" />--></td>


	    </tr>
	    
	    
	  </html:table>

	  <br>

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      //oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      //oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
		<html:title clazz='table'>核算科室字典</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr>

		  <!-- 结果集 -->
		  <tr>
		    <td>
		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td noWrap="true">选择</td>
		          <td noWrap="true">科室代码</td>
		          <td noWrap="true">科室名称</td>
		          <td noWrap="true">上级科室代码</td>
		          <td noWrap="true">科室类别</td>
				  <td noWrap="true">科室级别</td>
				  <td noWrap="true">参与分摊</td>
				  <td noWrap="true">分摊级别</td>
				  <td noWrap="true">末级标志</td>
		          <td noWrap="true">科室属性</td>
		          <td noWrap="true">分配类别</td>
		          <td noWrap="true">停用标志</td>
		          <td noWrap="true">启用日期</td>
		          <td noWrap="true">停用日期</td>
		          <td noWrap="true">拼音码</td>
		        </html:tr>

		        <%
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String primaryKey = result[ i ][ 0 ];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>

		        <tr CLASS="<%=rowColor%>">
		          <td><input type="checkbox"  <%if (result[i][7].trim().equals("否")){ out.print("disabled=\"disabled\" name=\"primaryKeyd\"");}else{out.print(" name=\"primaryKey\"");}%> value="<%=primaryKey+" "+result[i][2]%>"></td>
		         <td class="normalText"><a href="acctdept.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>&_current_page=<%=request.getParameter("_current_page")==null? "0":request.getParameter("_current_page")%>"><%=primaryKey%></a></td>
		          <td class="normalText" noWrap="true"><%=result[ i ][ 1 ]%></td>
		          <td class="normalText" noWrap="true"><%=result[ i ][ 2 ]%></td>
		          <td class="normalText" noWrap="true"><%=result[ i ][ 3 ]%></td>
		          <td class="normalText" noWrap="true"><%=result[ i ][ 4 ]%></td>
		          <td class="normalText" noWrap="true"><%=result[ i ][ 5 ]%></td>
		          <td class="normalText" noWrap="true"><%=result[ i ][ 6 ]%></td>
		          <td class="normalText" noWrap="true"><%=result[ i ][ 7 ]%></td>
		          <td class="normalText" noWrap="true"><%=result[ i ][ 8 ]%></td>
		          <td class="normalText" noWrap="true"><%=result[ i ][ 9 ]%></td>
		          <td class="normalText" noWrap="true"><%=result[ i ][10]%></td>
		          <td class="normalText" noWrap="true"><%=result[i][11].length()==6?"":result[i][11].substring(0,10) %></td>
		          <td class="normalText" noWrap="true"><%=result[i][12].length()==6?"":result[i][12].substring(0,10) %></td>
		          <td class="normalText" noWrap="true"><%=result[ i ][13]%></td>
		          

		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		    </td>
		  </tr>

		  <!-- 操作 -->
	  </html:table>
		<%}%>
	  <input type="hidden" name="subFunction" />
	  <input type="hidden" name="initsub" value="sub"/>
	</form>
</html:html>
<script>
initpage()
document.all.propertys.style.width=140
document.all.app_dept_type_code.style.width=140

</script>