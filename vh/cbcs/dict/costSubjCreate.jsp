<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/costSubjCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime:  $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(102, request)%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
    if(isEmpty(template.cost_subj_code))
    {
      alert('成本项目代码不能为空!');
      return;
    }
    if(isEmpty(template.cost_subj_name))
    {
      alert('成本项目名称不能为空!');
      return;
    }
    if(!isRadioChecked(template.last_level))
    {
      alert('末级标志必须选!');
      return;
    }
    if(!isRadioChecked(template.stop_mark))
    {
      alert('停用标志必须选!');
      return;
    }
    if(isTooLong(template.cost_subj_code,20))
    {
      alert('成本项目代码不能大于20个字符!');
      return;
    }
    if(isTooLong(template.cost_subj_name,40))
    {
      alert('成本项目名称不能大于40个字符!');
      return;
    }
    parent.frames[0].cost_subj_name.value = template.cost_subj_name.value
    parent.frames[0].cost_subj_code.value = template.cost_subj_code.value
    if(template.last_level[0].checked)
      parent.frames[0].last_level.value = template.last_level[0].value
    else parent.frames[0].last_level.value = template.last_level[1].value
		show_wait();
    template.submit();
    return true;
  }
  function back(){
    history.back()
  }
</Script>

<form name="template" method="post" action="dictcostsubj.jspviewhigh">
  <!-- 信息提示栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>
  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">成本项目代码：</td>
      <td width="75%" class="normalText" nowrap="nowrap"><input type=text name="cost_subj_code" class="textInputC" maxlength="20"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">成本项目名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="cost_subj_name" class="textInputC" maxlength="40"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">上级代码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="supp_item_code" class="textInputC"  readonly disabled maxlength="10" <%if(request.getParameter("cost_subj_code")!=null) out.print("value='"+request.getParameter("cost_subj_code")+"'");else out.print("value='TOP'");%>/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">医疗/药品标识：</td>
      <td class="normalText" nowrap="nowrap">
		    <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("init_treat_or_med"), "treat_or_med", null, false, true)%>
		  </td>
    </tr>
		<tr>
      <td class="signText" nowrap="nowrap">是否末级：</td>
      <td class="normalText" nowrap="nowrap">
		    是<input type="radio" name="last_level" value="Y" />
        否<input type="radio" name="last_level" value="N" />
		  </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">是否停用：</td>
      <td class="normalText" nowrap="nowrap">
		    是<input type="radio" name="stop_mark" value="Y" />
        否<input type="radio" name="stop_mark" value="N" checked />
		  </td>
    </tr>
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="return back();">返回</button> 
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back();" />--></td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
  <input type="hidden" name="supp_item_code" <%if(request.getParameter("cost_subj_code")!=null) out.print("value='"+request.getParameter("cost_subj_code")+"'");else out.print("value='TOP'");%>>
  <input type="hidden" name="subj_grade" <%if(request.getParameter("subj_grade")!=null) out.print("value='"+(Integer.parseInt(request.getParameter("subj_grade"))+1)+"'"); else out.print("value='1'");%>>
</form>

<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(102)%>
