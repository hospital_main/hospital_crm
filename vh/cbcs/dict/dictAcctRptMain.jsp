<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictAcctRptMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:57:47 $
  $Modtime: 03-09-02 10:42 $
	$Revision: 1.1 $
	$NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(101, request)%>



<Script Language="JavaScript">
    function create() {
        template.subFunction.value='prepareCreate';
				show_wait();
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
					show_wait();
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }
</Script>


    <form name="template" method="post" action="costsubj.jspviewhigh">
        <table width="97%" border="0" cellpadding="0" cellspacing="0">
            <tr bgcolor="#FFFFFF">
                <td valign="top"><img src="../../images/left.gif" width="5" height="5"></td>
                <td align="right" valign="top"><img src="../../images/right.gif" width="5" height="5"></td>
            </tr>

                <%
                    String message = ( String )request.getAttribute( "message" );
                    if ( message != null && !message.trim().equals( "" ) ) {
                %>

            <tr bgcolor="#FFFFFF" class="a12">
                <td height="26" colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;<font color="#FF0000"><%=message%></font></td>
            </tr>

                <%}else{%>

            <tr bgcolor="#FFFFFF">
                <td colspan="2">
                    <table width="96%" border="0" align="center" cellspacing="1" bgcolor="#999999">
                        <tr bgcolor="#CCCCCC" class="a12" align="center">
                            <td width="8%" height="24">删除标志</td>
                            <td width="10%">会计报表代码</td>
                            <td width="10%">会计报表名称</td>
                            <td width="10%">停用标志</td>
                        </tr>
              <%

                    String[][] result = (String[][])request.getAttribute( "table_result" );
                    if ( result != null ) {
                      for (int i = 0; i < result.length; i++ ) {
                          String primaryKey = result[ i ][ 0 ];
                             for (int j=0; j<result[i].length; j++)
                       {
                         if (result[i][j]!=null&&result[i][j].trim().equals(""))
                           {
                             result[i][j]="&nbsp;";
                           }
                     }
              %>

                        <tr bgcolor="#FFFFFF" class="a12" align="center">
                            <td width="8%"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
                            <td width="10%"><a href="acctrpt.jspviewhigh?subFunction=save&primaryKey=<%=primaryKey%>"><%=primaryKey%></a></td>
                            <td width="28%"><%=result[ i ][ 1 ]%></td>
                            <td width="10%"><%=result[ i ][ 2 ]%></td>
                        </tr>
              <%
                    }
                  }

              %>
                    </table>
                </td>
            </tr>

            <tr bgcolor="#FFFFFF" class="a12" align="center">
                <td height="60" valign="middle" colspan="6">
                    <input type="button" value="全 选" onclick="return selectAll();" >&nbsp;
                    <input type="reset"  value="重 置" >&nbsp;
                    <input type="button" value="删 除" onclick="return remove();" >&nbsp;
                    <input type="button" value="增 加" onclick="return create();" >
                    <input type="hidden" name="subFunction" >
                </td>
            </tr>
        <%}%>
        </table>
    </form>


<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>

