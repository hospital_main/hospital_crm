<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/paraDeptsFixAssetMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:57:47 $
	$Revision: 1.1 $
	$NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(101, request)%>




<Script Language="JavaScript">
	function create() {
		template.subFunction.value='preparedCreate';
		show_wait();
    template.submit();
		return true;
	}

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
  		if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
  		  flag = true;
  	}

    if( flag!=false) {
    	if (confirm('是否删除')) {
	      template.subFunction.value='remove';
				show_wait();
	      template.submit();
	      return true;
	    } else
	    	return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectedAll(){
    for (var i=0; i<template.elements.length; i++) {
    	if (template.elements[i].name=='primaryKey')
    		template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
		show_wait();    
		template.submit();
		return true;
  }
</Script>
    <form name="template" method="post" action="paraDeptsFixAsset.jspviewhigh">
     <!-- 信息提示栏 -->
       <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>

     <!--信息栏-->
       <%= new com.viewhigh.cbcs.base.mvc.view.MessageTitle("科室固定资产基本情况主页面")%>

      <!-- 简单信息 -->
      <table  width="100%" cellspacing="2" border="0" >
        <tr>
            <td class="signText">科室</td>
          <td class="normalText">
          <select type="select" name="dept_code">
              <%String dept_code=request.getParameter("dept_code");%>
              <option value="" <%if(dept_code==null||dept_code.equals("")) out.println(" selected ");%>>请选择</option>
              <%
                  String table_result[][] =
                      (String[][])request.getAttribute( "table_result" );
                  if(table_result!=null)
                  {
                      for( int i = 0; i < table_result.length; i++ )
                      {  for (int j=0; j<table_result[i].length; j++)
                       {
                         if (table_result[i][j]!=null&&table_result[i][j].trim().equals(""))
                           {
                             table_result[i][j]="&nbsp;";
                           }
                     }
              %>
              <option value="<%=table_result[i][0]%>" <%if(dept_code!=null&&!dept_code.equals("")&&dept_code.equals(table_result[i][0])) out.println(" selected ");%>><%=table_result[i][0]+":"+table_result[i][1]%></option>
              <%
                      }
                  }
              %>
          </select>
      </td>
         <td class="signText">固定资产类型</td>
          <td class="normalText">
          <select type="select" name="fix_asset_depr_type_code">
              <%String fix_asset_depr_type_code=request.getParameter("fix_asset_depr_type_code");%>
              <option value="" <%if(fix_asset_depr_type_code==null||fix_asset_depr_type_code.equals("")) out.println(" selected ");%>>请选择</option>
              <%
                  String table_result1[][] =
                      (String[][])request.getAttribute( "table_result1" );
                  if(table_result1!=null)
                  {
                      for( int i = 0; i < table_result1.length; i++ )
                      {
              %>
              <option value="<%=table_result1[i][0]%>" <%if(fix_asset_depr_type_code!=null&&!fix_asset_depr_type_code.equals("")&&fix_asset_depr_type_code.equals(table_result1[i][0])) out.println(" selected ");%>><%=table_result1[i][1]%></option>
              <%
                      }
                  }
              %>
          </select>
      </td>
          <td><button class="pageBtn" name=""  onclick="return find()" >查询</button>
          <!--<img src="images/find.gif" class="mouse" onclick="return find()" />--> </td>
        </tr>
      </table>

      <br>

      <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	    TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectedAll()");   // 全选
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
      oper.addNeedButton("images/create.gif", "return create()");     //  添加
      %>

      <!-- 复杂信息 -->
      <table width="100%">
        <!-- 操作 -->
        <tr><td><%=oper%></td></tr>
        <tr>
          <td>
            <table  BORDERCOLOR="#214597" border="1" cellspacing="1" width="100%" >
              <tr>
                <td colspan="100" class="resultTitle" align="center">科室固定资产基本情况</td>
              </tr>
              <tr>
                <td class="resultLabel">选择</td>
                <td class="resultLabel">科室名称</td>
	              <td class="resultLabel">固定资产折旧类型</td>
	              <td class="resultLabel">固定资产金额</td>
              </tr>

              <%
                if (ro!=null) {
              	String[][] result = ro.getTableResult();
              	if (result!=null) {

                  for (int i = 0; i < result.length; i++ )
                 {
                String[] temp = {result[i][0], result[i][1]};
                String primaryKey = ExtendTool.arrayToString(temp);

                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
              %>
              <tr CLASS="<%=rowColor%>">
                  <td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
                  <td class="normalText"><a href="paraDeptsFixAsset.jspviewhigh?subFunction=load&primaryKey=<%=primaryKey%>"><%=result[i][3]%></a></td>
                  <td class="normalText"><%=result[i][4]%></td>
                  <td nowrap class=numberText class="normalText"><%=result[i][2]%></td>
              </tr>
              <%
                    }
                  }
                }

              %>
            </table>
          </td>
        </tr>

        <!-- 操作 -->

      </table>
      <input type=hidden name="subFunction"/>
  	</form>


<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>

