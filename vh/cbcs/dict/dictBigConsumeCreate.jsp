<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictBigConsumeCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
String[][] cost_subj = (String[][])request.getAttribute("initial_cost_subj");
String[][] dept = (String[][])request.getAttribute("initial_dept");

%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create() {
    //template.subFunction.value='create';
    
    if(template.cost_subj.value=="" || template.cost_subj.value==null)
    {
      alert('成本项目不能为空!');
      return;
    }
    if(template.dept.value=="" || template.dept.value==null)
    {
      alert('科室不能为空!');
      return;
    }
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element ) {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
		show_wait();
    element.submit();
  }
</Script>

<html:html clazz="main">  <form name="template" method="post" action="dictBigConsume.jspviewhigh">
  <!-- 信息提示栏 -->
	  <html:message/>

  <!--信息栏-->
  <html:title clazz="module">大用户添加页面</html:title>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0"  class="simpleQuery">
    <tr>
      <td class="signText" nowrap="nowrap">成本项目：</td>
      <td class="normalText" nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(cost_subj,"cost_subj",null,true,false, false, "selectBgW")%></td>
       																			
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">科室：</td>
      <td class="normalText" nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(dept,"dept",null,true,false, false, "selectBgW")%></td>
    </tr>
    <tr>
      <td colspan="2" align="left">　　　　　　　　　　　　　　　　　　
      <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>   
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr>
    <td></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>
