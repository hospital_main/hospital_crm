<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictLogisticServeSave.jsp,v 1.2 2014/03/13 01:25:48 yuchengying Exp $
 $Author: yuchengying $
 $Date: 2014/03/13 01:25:48 $
 $Modtime: 03-08-28 22:55 $
 $Revision: 1.2 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    if(isTooLong(template.logistic_serve_name,100))
    {
      alert('院内服务项目名称不能高于100个字符!');
      return;
    }
    if(isTooLong(template.unit,10))
    {
      alert('单位不能高于10个字符!');
      return;
    }
    switch(isDouble(template.unit_price,7,2))
    {
      case 0 : alert('参考单价必须为数字型'); return;
      case 1 : alert('参考单价整数部分不能高于7个字符'); return;
      case 2 : alert('参考单价没有整数部分'); return;
      case 3 : alert('参考单价小数部分不能高于2个字符'); return;
    }
    //template.subFunction.value='save';
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    template._current_page.value=<%=request.getParameter("_current_page")%>
		show_wait();
    element.submit();
  }
</Script>

<html:html clazz="main">  <form name="template" method="post" action="dictLogisticServe.jspviewhigh">
  <!-- 信息提示栏 -->
 <html:message/>

  <!-- 标题栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("院内服务项目修改页面")%>

  <%
    String[] result = (String[])request.getAttribute( "result" );
    if ( result != null )
    {
  %>

  <!-- 简单信息 -->
  <table  width="100%" class="simpleQuery">
    <tr>
      <td class="signText" nowrap="nowrap">院内服务项目代码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0]%>" disabled style="width:140px;"/>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">院内服务项目名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="logistic_serve_name" value="<%=result[1]%>" class="textInputC">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">单位：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="unit" value="<%=result[2]%>" class="textInputC">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">参考单价：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="unit_price" value="<%=result[3]%>" class="textInputC">
      </td>
    </tr>
     <%
      String[][] logisticServeInfo =
        (String[][])request.getAttribute("logisticServeInfo");

    %>
    <tr>
      <td colspan="2" align="left">　　　　　　　　　　　　　　　　　　　　　　
      <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>   
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
      <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="logistic_serve_code" value="<%=result[0]%>">
  <input type=hidden name="_current_page" value = "<%=request.getParameter("_current_page")%>" />
  <%}%>
</form>


</html:html>
