<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictAcctDeptSave.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-09-01 9:52 $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
  	var str_date = template.begin_date.value
//    	var r = str_date.match(/^2(\d{3})(-|\/)(\d{1,2})(-|\/)(\d{1,2})$/);
	var r = str_date.match(/^(\d{4})(-|\/)(\d{1,2})(-|\/)(\d{1,2})$/);
    	if(r==null){
//    	alert('请输入正确的日期格式，并且日期要在2000年以后!');
		alert('请输入正确的日期格式!');
      	return;
    	}
    	str_date = template.stop_date.value
//    	r = str_date.match(/^2(\d{3})(-|\/)(\d{1,2})(-|\/)(\d{1,2})$/);
	r = str_date.match(/^(\d{4})(-|\/)(\d{1,2})(-|\/)(\d{1,2})$/);	
    	if(r==null){
//    	alert('请输入正确的日期格式，并且日期要在2000年以后!');
		alert('请输入正确的日期格式!');
      	return;
    	}

     if(template.begin_date.value>template.stop_date.value)
    {
      alert('启用日期不能大于停用日期!');
      return;
    }
    if(isEmpty(template.dept_name))
    {
      alert('科室名称不能为空!');
      return;
    }
    if(isEmpty(template.supper_dept))
    {
      alert('上级科室代码不能为空!');
      return;
    }
    if(template.supper_dept.value==template.deptcode.value){
      alert('上级科室代码不能与本科室相同!');
      return;}
    if (isEmpty(template.dept_grade)) {
      alert('科室级别不能为空!');
      return;
    }
    if (isTooLong(template.dept_name,40)) {
      alert('科室名称不能高于40个字符!');
      return;
    }
    if (isTooLong(template.supper_dept,20)) {
      alert('上级科室代码不能高于10个字符!');
      return;
    }
     if(isTooLong(template.app_level,1))
    {
      alert('分摊级别不能高于1个字符!');
      return;
    }
    if (!isNumber(template.dept_grade)) {
      alert('科室级别必须为数字类型!');
      return;
    }
     if(isTooLong(template.dept_grade,1))
    {
      alert('科室级别不能高于1个字符!');
      return;
    }
    template.subFunction.value='save';
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    template._current_page.value=<%=request.getParameter("_current_page")%>
		show_wait();
    element.submit();
  }
  
  function makeSpell()
  {
		template.subFunction.value="save_getSpellCode";
		show_wait();
    template.submit();
  }
  
  
</Script>
<html:html clazz="main">
<form name="template" method="post" action="acctdept.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>核算科室修改页面</html:title>

  <%

    String[] result = (String[])request.getAttribute("result");
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">科室代码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="deptcode" value="<%=result[0]%>" disabled  style="width:140px;"/>
      </td>
      <td class="signText" nowrap="nowrap">科室名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="dept_name" value="<%=result[1]%>" class="textInputC" onblur="" style="width:140px;" onchange="getSpellCode(this);"/>
      </td>
   </tr>

    <tr>
      <td class="signText" nowrap="nowrap">上级科室代码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text  disabled="disabled"  value="<%=result[2]%>"  style="width:140px;"/>
          <input type="hidden" name="supper_dept" value="<%=result[2]%>">
      </td>
      <td class="signText" nowrap="nowrap">科室类别：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(request.getAttribute("init_depttype"), "app_dept_type_code", result[3], false, true)%>
      </td>
   </tr>

    <tr>
      <td class="signText" nowrap="nowrap">分配类别：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(request.getAttribute("subj_kind"), "subj_kind_code", result[10], false, false)%>
      </td>
      <td class="signText" nowrap="nowrap">科室级别：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="dept_grade" value="<%=result[4]%>" class="textInputC"  style="width:140px;">
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">参与成本分摊标志：</td>
      <td class="normalText" nowrap="nowrap">
         是<input type="radio" style= "border:0pt; " name="cost_app_ind" value="Y" <% if(result[5].equals("Y")){out.print(" checked ");}%>/>
         否<input type="radio" style= "border:0pt; " name="cost_app_ind" value="N" <% if(result[5].equals("N")){out.print(" checked ");}%>/>
      </td>
      <td class="signText" nowrap="nowrap">分摊级别：</td>
      <td class="normalText" nowrap="nowrap">
         <input type=text name="app_level" value="<%=result[6]%>"  style="width:140px;"/> </td>
   </tr>
    <tr>
      <td class="signText" nowrap="nowrap" style="visibility:hidden ">末级标志：</td>
      <td class="normalText" nowrap="nowrap" style=" visibility:hidden" >
         是<input type="radio"  style= "border:0pt; " name="dept_last_level" value="Y" <% if(result[7].equals("Y")){out.print(" checked ");}%>/>
         否<input type="radio" style= "border:0pt; " name="dept_last_level" value="N" <% if(result[7].equals("N")){out.print(" checked ");}%>/>
      </td>
      <td class="signText" nowrap="nowrap">科室属性：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(request.getAttribute("init_outorin"), "out_or_in", result[8], false, false)%>
      </td>
   </tr>

    <tr>
      <td class="signText" nowrap="nowrap">拼音码：</td>
      <td class="normalText" nowrap="nowrap">
         <input type=text name="spell"  value="<%=result[13]==null?request.getAttribute("spell"):result[13]%>"  style="width:140px;"/>
      </td>
      <td class="signText" nowrap="nowrap">停用标志：</td>
      <td class="normalText" nowrap="nowrap">
       是<input type="radio" style= "border:0pt; " name="stop_mark" value="Y" <% if(result[9].equals("Y")){out.print(" checked ");}%>/>
       否<input type="radio" style= "border:0pt; " name="stop_mark" value="N" <% if(result[9].equals("N")){out.print(" checked ");}%>/>
      </td>
    </tr>
	<tr>
      <td class="signText" nowrap="nowrap">启用日期：</td>
      <td class="normalText" nowrap="nowrap">
		     <input type='text' name='begin_date' class='commonCalendar' value='<%=result[11].trim().length()==0?"":result[11].substring(0,10) %>'/>
		  </td>
      <td class="signText" nowrap="nowrap">停用日期：</td>
      <td class="normalText" nowrap="nowrap">
		     <input type='text' name='stop_date' class='commonCalendar' value='<%=result[12].trim().length()==0?"":result[12].substring(0,10) %>'/>
		  </td>
    </tr>
    <tr>
      <td colspan="4" align="center">
      <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>   
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </html:table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="_current_page" value = "<%=request.getParameter("_current_page")%>" />
  <input type="hidden" name="dept_code" value="<%=result[0]%>">
  <%}%>
</form>


</html:html>
<script>
function getSpellCode(source){
var name = source.value ;
name = "<a>"+name+"</a>"
var p=getValuePairBySql("ccbsGetSpellCode_query",name);
	var ec,en;
	if(p!=null){
		ec=p[0];
		en=p[1];	
	}else{
		ec="";
		en="";
	}
document.getElementById("spell").value = ec;
  
}
 

</script>
