<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictChargeDetailMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
                com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
				show_wait();
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
					show_wait();
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
		show_wait();
    template.submit();return true;
  }
  
  function importData(obj,v){
		   		
				var fileName=fileUploader.choose(1);
				if(fileName.length<4&&fileName.indexOf(".")<0)
					return false;
				var fileType=fileName.substr(fileName.lastIndexOf(".")+1);
				fileUploader.submit(function(m,f){
						if( m != ''){
							alert(m);
							return false;
						}
						for(var k in f){
							fileType+="="+k;
							break;
						}
						var msg = "" ;
						var queryParams = "<comp_file>"+fileType+"</comp_file>" ;
						confirmImportStyleDialog(function(v){
								if(v==0)
									return;

					        window.xmlhttp.post("hosDictsUnitinfoDictchargedetail_import",queryParams+"<q_flag>"+v+"</q_flag>");
					        
					         var responseText = window.xmlhttp._object.responseText ;
					          msg = window.doMsg(responseText,"show")
						  if(msg==true) {
							template.subFunction.value='findAll';
							show_wait();
					    		template.submit();
					    		return true;
						  }
						});
					},obj
				)	
			}
</Script>
<%String[][] income_type={{"T","医疗收入"},{"M","药品收入"}};%>
<html:html clazz="main">
<form name="template" method="post" action="dictChargeDetail.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>医疗项目主页面</html:title>

  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText">医疗项目代码：</td>
      <% String charge_detail_code = request.getParameter("charge_detail_code");%>
      <td><input type=text name="charge_detail_code" class="textInputC" style="width:140" <%if(charge_detail_code != null){ out.println(" value=" + charge_detail_code);}%>></td>
       <td nowrap class="signText">医疗项目名称：</td>
      <% String charge_detail_name = request.getParameter("charge_detail_name");%>
      <td><input type=text name="charge_detail_name" class="textInputC" style="width:140" <%if(charge_detail_name != null){ out.println(" value=" + charge_detail_name);}%>></td>
    	 <td class="signText" nowrap="nowrap">收入类型：</td>
      <td nowrap="nowrap">
        <%=new SingleSelect(income_type, "income_type", request.getParameter("income_type"), false, false)%>
      </td>
    </tr>
   <tr>
     
      <td align="right" colspan="6">
      <button class="pageBtn" name=""   onclick="template.subFunction.value='findAll';show_wait();template.submit();return true;"  >查询</button>
    	<button class="pageBtn" onclick="return preparedPrint();">打印</button>
    	<button class="pageBtn" name="hosDictsUnitinfoDictchargedetail_import"
						onclick="importData(this,1)" >导入</button> 
      <!--<img src="images/find.gif" class="mouse" onclick="template.subFunction.value='findAll';show_wait();template.submit();return true;" />
    	<img src="images/transfers.gif" class="mouse" name="hosDictsUnitinfoDictchargedetail_import"
						onclick="importData(this,1)" />--></td>
    </tr>
  </html:table>

  <br>

  <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro!=null) {
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
      oper.addNeedButton("images/create.gif", "return create()");     //  添加
  %>

  <html:title clazz='table'>医疗项目</html:title>
  <!-- 复杂信息 -->
  <html:table clazz="complex">
    <!-- 操作 -->
    <tr><td><%=oper%></td></tr>

  <!-- 结果集 -->
  <tr>
    <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td class="resultLabel">选择</td>
          <td class="resultLabel">医疗项目代码</td>
          <td class="resultLabel">医疗项目名称</td>
          <td class="resultLabel">收入类型</td>
          <td class="resultLabel">停用标志</td>
          <td class="resultLabel">拼音码</td>
        </html:tr>

        <%
          String[][] result = ro.getTableResult();
          if ( result != null )
          {
            for (int i = 0; i < result.length; i++ )
            {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++)
              {
                if (result[i][j]!=null && result[i][j]=="") result[i][j]="&nbsp;";
              }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
          <td class="normalText"><a href="dictChargeDetail.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>&_current_page=<%=request.getParameter("_current_page")==null? "0":request.getParameter("_current_page")%>"><%=primaryKey%></a></td>
          <td class="normalText"><%=result[ i ][ 1 ]%></td>
          <td class="normalText"><%=result[ i ][ 2 ]%></td>
          <td class="normalText"><%=result[ i ][ 3 ]%></td>
          <td class="normalText"><%=result[ i ][ 4 ]%></td>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>

  <!-- 操作 -->

  </html:table>
	<%}%>
  <input type=hidden name="subFunction"/>
  <input type="hidden" name="initsub" value="sub"/>
</form>
<div id="upload_file_div" style="position:absolute;display:none;">
	    <iframe src="uploadFileD.cmd" id="uploadFrame"></iframe>
	</div>
	<script Language="JavaScript" >
		_____VHFileCreatFromTimerss=window.setInterval(createVhFile,10);
		  function createVhFile(){
		    if(typeof(uploadFrame)=="undefined")
		      return ;
		    window.clearInterval(_____VHFileCreatFromTimerss);
		    fileUploader=new VhFileUploader(uploadFrame);
		  }
	</script>
</html:html>
