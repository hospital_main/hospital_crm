<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictServeStuffCreate.jsp,v 1.2 2014/03/14 00:43:07 yuchengying Exp $
 $Author: yuchengying $
 $Date: 2014/03/14 00:43:07 $
 $Modtime: 03-09-03 13:25 $
 $Revision: 1.2 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
  template.serve_stuff_code.value = getCode(template.serve_stuff_code.value);
    if(isEmpty(template.serve_stuff_code))
    {
      alert('服务材料代码不能为空!');
      return;
    }
    if(isEmpty(template.serve_stuff_name))
    {
      alert('服务材料名称不能为空!');
      return;
    }

    if(isEmpty(template.unit))
    {
      alert('单位不能为空!');
      return;
    }

    if(isEmpty(template.unit_price))
    {
      alert('参考单价不能为空!');
      return;
    }
    if(template.cost_subj_code.value=="")
    {
      alert('对应成本项目不能为空!');
      template.cost_subj_code.focus();
      return;
    }
    if(isTooLong(template.serve_stuff_code,10))
    {
      alert('服务材料代码不能大于10个字符!');
      return;
    }
    if(isTooLong(template.serve_stuff_name,100))
    {
      alert('服务材料名称不能大于100个字符!');
      return;
    }
    if(isTooLong(template.unit,10))
    {
      alert('单位不能大于10个字符!');
      return;
    }
    switch(isDouble(template.unit_price, 7, 2))
    {
      case 0 : alert('参考单价必须为数字型'); return false;
      case 1 : alert('参考单价整数部分不能高于7个字符'); return false;
      case 3 : alert('参考单价小数部分不能高于2个字符'); return false;
    }
    //template.subFunction.value='create';
		show_wait();
    template.submit();
    return true;
  }

    // 返回
    function back( element ){
       for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
       template.subFunction.value='findAll';
       template.initsub.value="sub";
			 show_wait();
       element.submit();
    }
</Script>


<html:html clazz="main">  <form name="template" method="post" action="dictServeStuff.jspviewhigh">
	<!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("服务材料添加页面")%>

  <!-- 简单信息 -->
  <table  width="100%" class="simpleQuery">
    <tr>
      <td class="signText" nowrap="nowrap">服务材料代码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="serve_stuff_code" class="textInputC" /></td>
      <td class="signText" nowrap="nowrap">服务材料名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="serve_stuff_name" class="textInputC" /></td>
    </tr>

     <tr>
      <td class="signText" nowrap="nowrap">单位：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="unit" class="textInputC" /></td>
      <td class="signText" nowrap="nowrap">参考单价：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="unit_price" class="textInputC" ></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">对应成本项目：</td>
      <td>
        <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("init_cost_subj"), "cost_subj_code", null, true, false, false, "selectBgW")%>
      </td>
      <td class="signText" nowrap="nowrap">停用标志：</td>
         <td>
         是<input type="radio" name="stop_mark" value="Y"/>
         否<input type="radio" name="stop_mark" value="N" checked/>
         </td>
    </tr>
    <tr>
      <td colspan="4" align="center"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>
