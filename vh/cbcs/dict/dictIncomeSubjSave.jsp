<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictIncomeSubjSave.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 13:24 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    //template.subFunction.value='save';
    if(isEmpty(template.income_subj_name))
    {
      alert('收入项目名称不能为空!');
      return;
    }
    if(isTooLong(template.income_subj_name, 40))
    {
      alert('收入项目名称不能大于40个字符!');
      return;
    }
    var stopMark = template.stop_mark;
    
    if(template.stop_mark[0].checked==false && template.stop_mark[1].checked==false)
    {
      alert('请选择停用标志!');
      return;
    }
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    template._current_page.value=<%=request.getParameter("_current_page")%>
		show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="incomesubj.jspviewhigh">
<!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>收入项目修改页面</html:title>


  <%
    String[] result = (String[])request.getAttribute( "result" );
    if ( result != null )
    {
  %>

  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">收入项目代码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0]%>" disabled />
      </td>
      <td class="signText" nowrap="nowrap">收入项目名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="income_subj_name" value="<%=result[1]%>" class="textInputC">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">停用标志：</td>
      <td class="normalText" nowrap="nowrap">
        是<input type="radio" style= "border:0pt; " name="stop_mark" value="Y" <% if(result[2].equals("Y")){out.println(" checked ");}%>/>
        否<input type="radio" style= "border:0pt; " name="stop_mark" value="N" <% if(result[2].equals("N")){out.println(" checked ");}%>/>
      </td>
	  <td></td>
	  <td></td>
    </tr>


    <tr>
      <td colspan="4" align="center">
      <button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button><button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </html:table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="initsub" value="sub"/>
  <input type="hidden" name="income_subj_code" value="<%=result[0]%>">
  <input type=hidden name="_current_page" value = "<%=request.getParameter("_current_page")%>" />
  <%}%>
</form>


</html:html>
