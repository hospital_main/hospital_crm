<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictAcctDeptCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-09-02 10:08 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function check(){
return true;
  }
  function getCode(str){
	  var _str ;
	  _str = str.replace(/(^\s+)|\s+$/g, "") ;
	  return _str.replace(/[\+%&]/g, "");
}

  function create()
  {
  template.dept_code.value = getCode(template.dept_code.value);
    //template.subFunction.value='create';
    if(isEmpty(template.dept_code))
    {
      alert('科室代码不能为空!');
      return;
    }
    if(isEmpty(template.dept_name))
    {
      alert('科室名称不能为空!');
      return;
    }
    if(isEmpty(template.supper_dept))
    {
      alert('上级科室代码不能为空!');
      return;
    }
    if(template.supper_dept.value==template.dept_code.value){
      alert('上级科室代码不能与本科室相同!');
      return;}
    if(isEmpty(template.dept_grade))
    {
      alert('科室级别不能为空!');
      return;
    }
    if(template.app_dept_type_code.value=="")
    {
      alert('请选择科室类别!');
      return;
    }
    if(template.subj_kind_code.value=="")
    {
      alert('请选择分配类别!');
      return;
    }

    if(!isRadioChecked(template.stop_mark))
    {
      alert('停用标志必须选!');
      return;
    }
    if(!isRadioChecked(template.cost_app_ind))
    {
      alert('成本分摊标志必须选!');
      return;
    }
    if(isTooLong(template.dept_code,20))
    {
      alert('科室代码不能高于20个字符!');
      return;
    }
    if(isTooLong(template.dept_name,40))
    {
      alert('科室名称不能高于40个字符!');
      return;
    }
    if(isTooLong(template.supper_dept,20))
    {
      alert('上级科室代码不能高于20个字符!');
      return;
    }
     if(isTooLong(template.app_level,1))
    {
      alert('分摊级别不能高于1个字符!');
      return;
    }
    if(!isNumber(template.dept_grade))
    {
      alert('科室级别为数字类型!');
      return;
    }
     if(isTooLong(template.dept_grade,1))
    {
      alert('科室级别不能高于1个字符!');
      return;
    }
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
		show_wait();
    element.submit();
  }
  
  function makeSpell ( )
  {
		template.subFunction.value='create_getSpellCode';
		show_wait();
    template.submit();
  }
  
</Script>
<html:html clazz="main">
<form name="template" method="post" onsubmit="check()" action="acctdept.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>核算科室添加页面</html:title>


  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">科室代码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="dept_code" class="textInputC" value="<%=request.getParameter("dept_code")==null? "":request.getParameter("dept_code")%>"/></td>
  
      <td class="signText" nowrap="nowrap">科室名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="dept_name" class="textInputC" value="<%=request.getParameter("dept_name")==null? "":request.getParameter("dept_name")%>" onblur="makeSpell()"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">上级科室代码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="supper_dept" class="textInputC" value="<%=request.getParameter("supper_dept")==null? "":request.getParameter("supper_dept")%>"/></td>
   
      <td class="signText" nowrap="nowrap">科室类别：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(request.getAttribute("init_depttype"), "app_dept_type_code", request.getParameter("app_dept_type_code")==null? "":request.getParameter("app_dept_type_code"), false, false)%>
		  </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">分配类别：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(request.getAttribute("subj_kind"), "subj_kind_code", request.getParameter("subj_kind_code")==null? "":request.getParameter("subj_kind_code"), false, false)%>
		  </td>
   
      <td class="signText" nowrap="nowrap">科室级别：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="dept_grade" class="textInputC"  value="<%=request.getParameter("dept_grade")==null? "":request.getParameter("dept_grade")%>"/></td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">参与成本分摊标志：</td>
      <td class="normalText" nowrap="nowrap">

		    是<input type="radio" name="cost_app_ind" value="Y" <%if (request.getParameter("cost_app_ind")!=null&&request.getParameter("cost_app_ind").trim().equals("Y")) out.print("checked='checked'");%> />
        否<input type="radio" name="cost_app_ind" value="N" <%if (request.getParameter("cost_app_ind")!=null&&request.getParameter("cost_app_ind").trim().equals("N")) out.print("checked='checked'");%>/>
		  </td>
   
      <td class="signText" nowrap="nowrap">分摊级别：</td>
      <td class="normalText" nowrap="nowrap">
         <input type=text name="app_level"  value="<%=request.getParameter("app_level")==null? "":request.getParameter("app_level")%>" style="width:140px;"/>
      </td>
    </tr>
     <tr>
      <td class="signText" nowrap="nowrap">科室属性：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(request.getAttribute("init_outorin"), "out_or_in",  request.getParameter("out_or_in")==null? "":request.getParameter("out_or_in"), false, false)%>
      </td>
   
      <td class="signText" nowrap="nowrap">拼音码：</td>
      <td class="normalText" nowrap="nowrap">
         <input type=text name="spell"  value="<%=request.getAttribute("spell")==null? "":request.getAttribute("spell")%>" style="width:140px;"/>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">停用标志：</td>
      <td class="normalText" nowrap="nowrap">
		     是<input type="radio" name="stop_mark" value="Y" <%if (request.getParameter("stop_mark")!=null&&request.getParameter("stop_mark").trim().equals("Y")) out.print("checked='checked'");%>/>
         否<input type="radio" name="stop_mark" value="N" <%if (request.getParameter("stop_mark")!=null&&request.getParameter("stop_mark").trim().equals("N")) {out.print("checked='checked'");}else if(request.getParameter("stop_mark")==null||request.getParameter("stop_mark").trim().equals("")){%>checked <%}%>/>
		  </td>
   
      <td class="signText" nowrap="nowrap">启用日期：</td>
      <td class="normalText" nowrap="nowrap">
		     <input type='text' name='begin_date' class='commonCalendar' value=''/>
		  </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">停用日期：</td>
      <td class="normalText" nowrap="nowrap">
		     <input type='text' name='stop_date' class='commonCalendar' value='2099-12-31'/>
		  </td>
    </tr>
    <tr>
      <td colspan="4" align="center"> <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
   <tr height="500">
    <td/>
    </tr>
  </html:table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>
