<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/clinicDeptMapCreate.jsp,v 1.2 2013/08/13 03:51:35 liyan Exp $
 $Author: liyan $
 $Date: 2013/08/13 03:51:35 $
 $Modtime: 03-09-02 10:08 $
 $Revision: 1.2 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  { 
  
    if(isEmpty(template.clinic_code))
    {
      alert('自定义科室代码不能为空!');
      return;
    }
   // alert(template.dept_code.value);
    if( template.dept_code.value=="" ){//.value ==''){
   		 alert('没有选择添加的科室');
   		 return ;
    }
  
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    for(var i=0;i<template.dept_code.options.length;i++){
      template.dept_code.options[i].selected=false;
    }
    template.subFunction.value='findAll';
    template.initsub.value="sub";
		show_wait();
    element.submit();
  }
</Script>
<%
   String[][] clinic_code=(String[][])request.getAttribute("clinic_code");
   String[][] dept_code=(String[][])request.getAttribute("dept_code");
%>
<html:html clazz="main">
<form name="template" method="post"  action="dictClinicDeptMap.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>自定义科室定义添加页面</html:title>
 	<!--
 	<html:title clazz='table'>自定义科室的属性必须与核算科室的属性相同,不相同则不保存</html:title>
 	-->

  <!-- 简单信息 -->
  <html:table clazz="simple" >
    <tr>
      <td class="signText" nowrap="nowrap">自定义科室：</td>
      <td nowrap="nowrap">
        <%=new SingleSelect(clinic_code, "clinic_code", request.getParameter("clinic_code"), true, true)%>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap" valign="top">科室：</td>
      <td  nowrap="nowrap">
        <select name="dept_code" multiple size="10" style="width:140px;">
          <%if(null!=dept_code){
          for(int i=0;i<dept_code.length;i++){
          %>
           <option value="<%=dept_code[i][0]%>" ><%=dept_code[i][0]+":"+dept_code[i][1]%></option>
          <%}
          }
          %>
        </select>

      </td>
    </tr>

    <tr>
      <td colspan="2" align="left">　　　　　　　　　　　　　　　　　
      <button class="pageBtn" onclick="return create();"  window.parent.template.create(); >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/create.gif" class="mouse" onclick="return create();"  window.parent.template.create(); /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </html:table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type='hidden' name="subFunction" value="create"/>
</form>
</html:html>
