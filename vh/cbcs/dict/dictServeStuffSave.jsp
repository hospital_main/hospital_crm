<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictServeStuffSave.jsp,v 1.2 2014/03/14 00:43:07 yuchengying Exp $
 $Author: yuchengying $
 $Date: 2014/03/14 00:43:07 $
 $Modtime: 03-09-03 13:25 $
 $Revision: 1.2 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    if(isEmpty(template.serve_stuff_name))
    {
      alert('服务材料名称不能为空!');
      return;
    }

    if(isEmpty(template.unit))
    {
      alert('单位不能为空!');
      return;
    }

    if(isEmpty(template.unit_price))
    {
      alert('参考单价不能为空!');
      return;
    }
     if(isEmpty(template.cost_subj_code))
    {
      alert('对应成本项目不能为空!');
      return;
    }
    if(isTooLong(template.serve_stuff_name,100))
    {
      alert('服务材料名称不能大于100个字符!');
      return;
    }
    if(isTooLong(template.unit,10))
    {
      alert('单位不能大于10个字符!');
      return;
    }
    switch(isDouble(template.unit_price, 7, 2))
    {
      case 0 : alert('参考单价必须为数字型'); return false;
      case 1 : alert('参考单价整数部分不能高于7个字符'); return false;
      case 3 : alert('参考单价小数部分不能高于2个字符'); return false;
    }
    //template.subFunction.value='save';
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    template._current_page.value=<%=request.getParameter("_current_page")%>
		show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictServeStuff.jspviewhigh">
	<!-- 信息提示栏 -->
 <html:message/>

  <!-- 标题栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("服务材料修改页面")%>

  <%
    String[] result = (String[])request.getAttribute( "result" );
    if ( result != null )
    {
  %>

  <!-- 简单信息 -->
  <table  width="100%" class="simpleQuery">
    <tr>
      <td class="signText" nowrap="nowrap">服务材料代码： </td>
      <td class="normalText" nowrap="nowrap"><input type=text value="<%=result[0]%>" disabled /></td>
      <td class="signText" nowrap="nowrap">服务材料名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="serve_stuff_name" value="<%=result[1]%>" class="textInputC"></td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">单位：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="unit" value="<%=result[2]%>" class="textInputC"></td>
      <td class="signText" nowrap="nowrap">参考单价：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="unit_price" value="<%=result[3]%>" class="textInputC"></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">对应成本项目：</td>
      <td><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("init_cost_subj"), "cost_subj_code", result[4], true, false)%>      </td>
	<td class="signText" nowrap="nowrap">停用标志：</td>
    <td>是<input type="radio" name="stop_mark" value="Y" <%if(result[5].equals("Y")) out.print("checked");%>/>
         否<input type="radio" name="stop_mark" value="N" <%if(result[5].equals("N")) out.print("checked");%>/></td>
   </tr>
    <tr>
      <td colspan="4" align="center">
      <button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" /> <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="initsub" value="sub"/>
  <input type="hidden" name="serve_stuff_code" value="<%=result[0]%>">
  <input type=hidden name="_current_page" value = "<%=request.getParameter("_current_page")%>" />
  <%}%>
</form>


</html:html>
