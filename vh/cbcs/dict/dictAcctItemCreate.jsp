<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictAcctItemCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(101, request)%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
    //template.subFunction.value='create';
    if(isEmpty(template.item_code))
    {
      alert('财务项目代码不能为空!');
      return;
    }
    if(isEmpty(template.item_name))
    {
      alert('财务项目名称不能为空!');
      return;
    }
    if(isTooLong(template.item_code,10))
    {
      alert('财务项目代码不能高于10个字符!');
      return;
    }
    if(isTooLong(template.item_name,40))
    {
      alert('财务项目名称不能高于40个字符!');
      return;
    }
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
		show_wait();
    element.submit();
  }
</Script>

<form name="template" method="post" action="acctitem.jspviewhigh">

  <!-- 返回信息栏 -->
  <table width="100%" border="0">
    <tr>
      <td>
        <table width="100%" border="0" >
        <%
          String message = (String)request.getAttribute("message");
          if (message!=null && !message.trim().equals(""))
          {
        %>
          <tr>
            <td class="errorText" align="center"><%=message%></td>
            <td align="center"><button class="pageBtn" onclick="history.go(-1);">返回</button> 
            <!--<img src="images/return.gif" onclick="history.go(-1);" style="cursor:hand">--></td>
          </tr>
        <%} else if (request.getParameter("subFunction").equals("create")) { %>
          <tr>
            <td class="successText" align="center">添加成功</td>
          </tr>
        <%}%>
        </table>
      <td>
    <tr>
  </table>

  <!-- 标题栏 -->
  <table width="100%" border="0">
    <tr>
      <td>
        <table width="100%" border="0" >
          <tr>
            <td  class="moduleTitle" nowrap="nowrap">核算财务项目字典添加页面：</td>
          </tr>
        </table>
      <td>
    <tr>
  </table>



  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">财务项目代码：</td>
      <td width="75%" class="normalText" nowrap="nowrap"><input type=text name="item_code" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">财务项目名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="item_name" class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return create();" >添加</button>      
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>
