<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/deptcategory/deptDefCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
    function back(){
      if(!isEmpty(template.subj_kind_code)&&
           !isEmpty(template.subj_kind_name)&&
             !(template.subj_kind_code.value==template.subj_kind_codec.value)){
          if(confirm('数据已经更改，是否需要创建')){
               checkcreate();
               return;
           }
        }
		 	for(var i=0;i<template.elements.length;i++){
 		 	template.elements[i].value="";}
 		 	template.subFunction.value='findAll';
      template.submit();
  		return;
		}
  function checkcreate(){
    if(isEmpty(template.subj_kind_code)){
      alert('科室类编码不能为空');
      return;
    }
    if(!checkLegality(template.subj_kind_code)){
      alert('科室类编码含有非法字符！')
      return true;
    }
    if(isEmpty(template.subj_kind_name)){
      alert('科室类名称不能为空');
      return;
    }
    if(!checkLegality(template.subj_kind_name)){
      alert('科室类名称含有非法字符！')
      return true;
    }
    if((template.subj_kind_code.value==template.subj_kind_codec.value)){
           if(confirm("此次提交的类编码与刚才提交相同！")){}else{return;}}
   template.subFunction.value='Create';
   template.submit();
   return true;

  }

</Script>


<html:html clazz='main'>
<form name="template" method="post" action="deptdef.jspviewhigh">
      <html:message/>
	    <html:title clazz='module'>科室分类创建页面</html:title>

<html:table clazz="simple">
   <tr>
      <td nowrap class="signText">科室类编码:</td>
      <% String subj_kind_code = (String)request.getAttribute("subj_kind_code");
       if(subj_kind_code==null) {subj_kind_code="";}%>
      <td class="normalText"><input type=text name="subj_kind_code" size="10" maxlength="10"></td>
    </tr>
    <tr>
      <td nowrap class="signText">科室类名称:</td>
      <% String subj_kind_name = (String)request.getAttribute("subj_kind_name");
       if(subj_kind_name==null) {subj_kind_name="";} %>
      <td class="normalText"><input type=text name="subj_kind_name" size="20" maxlength="20"></td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="checkcreate();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="back();">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="checkcreate();"/><img src="images/reset.gif" class="mouse" onclick="return reset();"  /> 
      <img src="images/return.gif" class="mouse" onclick="back();" />--></td>
   	 </tr>

  </html:table>
  <input type=hidden name="subj_kind_namec" value="<%=request.getAttribute("subj_kind_name")%>"/>
  <input type=hidden name="subj_kind_codec" value="<%=request.getAttribute("subj_kind_code")%>"/>
  <input type=hidden name="subFunction"/>


</form>
</html:html>





