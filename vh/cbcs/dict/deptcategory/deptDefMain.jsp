<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/deptcategory/deptDefMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
        show_wait();
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除？')) {
          template.subj_kind_code.value="";
          template.subj_kind_name.value="";
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    show_wait();
    template.submit();return true;
  }
  function check(){
    if(!checkLegality(template.subj_kind_code)){
      alert('科室类编码含有非法字符！')
      return true;
    }
    if(!checkLegality(template.subj_kind_name)){
      alert('科室类名称含有非法字符！')
      return true;
    }
   template.subFunction.value='findAll';
   template.submit();
   return true;

  }

</Script>


<html:html clazz='main'>
<form name="template" method="post" action="deptdef.jspviewhigh">
      <html:message/>
	    <html:title clazz='module'>科室分类主页面</html:title>

<html:table clazz="simple">
   <tr>
      <td nowrap class="signText">科室类编码:</td>
      <% String subj_kind_code = (String)request.getAttribute("subj_kind_code");
if(subj_kind_code==null) subj_kind_code="";%>
      <td class="normalText"><input type=text name="subj_kind_code" value="<%=subj_kind_code%>" size="10" maxlength="10"></td>
    </tr>
    <tr>
      <td nowrap class="signText">科室类名称:</td>
      <% String subj_kind_name = request.getParameter("subj_kind_name");
if(subj_kind_name==null) subj_kind_name="";%>
      <td class="normalText"><input type=text name="subj_kind_name" value="<%=subj_kind_name%>" size="20" maxlength="20"></td>
      <td>
      <button class="pageBtn" name=""   onclick="check();"  >查询</button>
       <!--<img src="images/find.gif" class="mouse" onclick="check();" />--></td>
   	 </tr>
</html:table>

<%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro != null) {
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   //
      oper.addOptionButton("images/reset.gif", "return reset()");     //
      oper.addOptionButton("images/remove.gif", "return remove()");   // shanchu
      oper.addNeedButton("images/create.gif", "return create()");     //   tainjia

%>
  <html:title clazz='table'>科室分类信息</html:title>

  <html:table clazz="complex">

       <tr><td><%=oper%></td></tr>

  <tr>
    <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td class="resultLabel">选择</td>
          <td class="resultLabel">科室类编码</td>
          <td class="resultLabel">科室类名称 </td>
        </html:tr>

<%
          String[][] result = ro.getTableResult();
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
%>
          <tr CLASS="<%=rowColor%>">
          <td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
          <td class="normalText"><a onclick="show_wait()" href="deptdef.jspviewhigh?subFunction=preparedSave&pk=<%=primaryKey%>&name=<%=result[ i ][ 1]%>&_current_page=<%=request.getParameter("_current_page")==null? "0":request.getParameter("_current_page")%>"><%=primaryKey%></a></td>
          <td class="normalText"><%=result[ i ][ 1]%></td>
          </tr>
 <%
              }
            }
 %>
      </html:table>
    </td>
  </tr>
  </html:table>
	<%}%>
  <input type=hidden name="subFunction"/>
</form>
</html:html>
