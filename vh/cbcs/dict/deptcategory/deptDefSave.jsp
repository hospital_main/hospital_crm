<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/deptcategory/deptDefSave.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
    function back(){
      if(template.copyofsubj_kind_name.value!=template.subj_kind_name.value){
         if(confirm("数据已经改动,需要提交么?")){
           checksave();
          return;}
       }
		 	for(var i=0;i<template.elements.length;i++){
 		 	template.elements[i].value="";}
 		 	template.subFunction.value='findAll';
 		 	template._current_page.value=<%=request.getParameter("_current_page")%>
      template.submit();
  		return;
		}

  function checksave(){
    if(isEmpty(template.subj_kind_name)){
      alert('科室类名称不能为空');
      return true;
    }
    if(!checkLegality(template.subj_kind_name)){
      alert('科室类名称含有非法字符！')
      return true;
    }
    if(template.copyofsubj_kind_name.value==template.subj_kind_name.value){
         if(confirm('此次提交与上次一样,继续提交么?')){}
      else{return;}}
      template.copyofsubj_kind_name.value=template.subj_kind_name.value;
      template.subFunction.value='save';
      template.submit();
      return true;

  }

</Script>


<html:html clazz='main'>
<form name="template" method="post" action="deptdef.jspviewhigh">
      <html:message/>
	    <html:title clazz='module'>科室分类修改页面</html:title>

<html:table clazz="simple">
   <tr>
      <td nowrap class="signText">科室类编码:</td>
      <%
       String subj_kind_code = (String)request.getAttribute("subj_kind_code");
       if(subj_kind_code==null) {subj_kind_code="";}
       %>
      <td class="normalText"><input type=text disabled="true" name="subj_kind_code" value="<%=subj_kind_code%>" size="10" maxlength="10"></td>
    </tr>
    <tr>
      <td nowrap class="signText">科室类名称:</td>
      <%
      String subj_kind_name = (String) request.getAttribute("subj_kind_name");
      if(subj_kind_name==null) {subj_kind_name="";}
      %>
      <td class="normalText"><input type=text name="subj_kind_name" value="<%=subj_kind_name%>" size="20" maxlength="20"></td>
    </tr>
    <tr>
     <td>
     <button class="pageBtn" onclick="checksave();">保存</button>
     <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="back();">返回</button>   
     <!--<img src="images/save.gif" class="mouse" onclick="checksave();" />
      <img src="images/reset.gif" class="mouse" onclick=" return  reset();" />
      <img src="images/return.gif" class="mouse" onclick="back();" />--> 
    </tr>
</html:table>

  <input type=hidden name="copyofsubj_kind_name" value="<%=request.getAttribute("subj_kind_name")%>">
  <input type=hidden name="subFunction"/>
  <input type=hidden name="_current_page" value = "<%=request.getParameter("_current_page")%>" />
  <input type=hidden name="subj_kind_code" value="<%=request.getAttribute("subj_kind_code")%>">
</form>
</html:html>





