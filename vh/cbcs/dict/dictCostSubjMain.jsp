<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictCostSubjMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 19:48 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
		show_wait();
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true){
           var tds=template.elements[i].parentNode.parentNode.getElementsByTagName("td");
           if(tds[6].innerText=="否"){
           	alert("非末级节点不能删除!");
          		return;
          }
            flag = true;
          }
      }

    if( flag!=false) {
		if (confirm('是否删除')) {
			template.subFunction.value='remove';
			show_wait();
			template.submit();
			return true;
		} else{
			return false;
		}    
    } else {
      alert( "请选择数据");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
		show_wait();
    template.submit();
		return true;
  }
  
  var child;
  function updateCode(){
	 	//window.open("cbcs/dict/costcodeChange.jsp", "","status:no;dialogHeight: 250px; dialogWidth: 500px;");
		if(child!=null&&typeof(child.template)!='undefined'){
			child.focus();
			return;
		}
		child=window.open("cbcs/dict/costCodeChange.html",'child', 'width=500, height=250, top=280, left=300, scrollbars = 1, resizable = no');
		//window.showModalDialog("cbcs/dict/costCodeChange.html", window,"status:no;dialogHeight: 250px; dialogWidth: 500px;");
  }
  function importAcctSubj(){//导入会计科目
  	   template.subFunction.value='acctImport';
			 show_wait();
    	 template.submit();
			 return true;
  }
  function importData(obj,v){
		   		
				var fileName=fileUploader.choose(1);
				if(fileName.length<4&&fileName.indexOf(".")<0)
					return false;
				var fileType=fileName.substr(fileName.lastIndexOf(".")+1);
				fileUploader.submit(function(m,f){
						if( m != ''){
							alert(m);
							return false;
						}
						for(var k in f){
							fileType+="="+k;
							break;
						}
						var msg = "" ;
						var queryParams = "<comp_file>"+fileType+"</comp_file>" ;
						confirmImportStyleDialog(function(v){
								if(v==0)
									return;

					        window.xmlhttp.post("hosDictsUnitinfoDictcostsubj_import",queryParams+"<q_flag>"+v+"</q_flag>");
					        
					         var responseText = window.xmlhttp._object.responseText ;
					          msg = window.doMsg(responseText,"show")
						  if(msg==true) {
							template.subFunction.value='findAll';
							show_wait();
					    		template.submit();
					    		return true;
						  }
						   });
					},obj
				)	
			}
</Script>

<html:html clazz="main">
<form name="template" method="post" action="costsubj.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>成本项目主页面</html:title>

  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText">成本项目代码：</td>
        <% String cost_subj_code = request.getParameter("cost_subj_code");%>
      <td><input type=text name="cost_subj_code" class="textInputC" style="width:140" <%if(cost_subj_code != null){ out.println(" value=" + cost_subj_code);}%>></td>
      <td nowrap class="signText">成本项目名称：</td>
            <% String cost_subj_name = request.getParameter("cost_subj_name");%>
      <td><input type=text name="cost_subj_name" class="textInputC" style="width:140" <%if(cost_subj_name != null){ out.println(" value=" +  cost_subj_name);}%>></td>
       <td class="signText">级别：</td>
      <td class="normalText">
          <select type="select" name="level" style="width:140">
              <option value=''>请选择</option>
              <%String level=request.getParameter("level");%>
              <option value="TOP" <%if(level!=null&&!level.equals("")&&level.equals("TOP")) out.println(" selected ");%> >1级</option>
              <option value="Y" <%if(level!=null&&!level.equals("")&&level.equals("Y"))   out.println(" selected ");%> >末级</option>
          </select>
      </td>
     </tr>
    <tr>
     
      <td class="signText">拼音码：</td>
      <td class="normalText"><input type=text name="spell" class="textInputC" style="width:140" value="<%if (request.getParameter("spell")!=null) out.print(request.getParameter("spell"));%>"></td>

      <td colspan="4" align="right">
      <button class="pageBtn" name="" onclick="template.subFunction.value='findAll';show_wait();template.submit();return true;"  >查询</button>
    	<button class="pageBtn" onclick="return preparedPrint();">打印</button>
    	
      	<button class="pageBtn" name="hosDictsUnitinfoDictcostsubj_import"
						onclick="importData(this,1)" >导入</button> 
				<button class="pageBtn" name="hosDictsUnitinfoDictacctsubj_import"
						onclick="importAcctSubj()" >导入会计科目</button> 
      <!--<img src="images/find.gif" class="mouse" onclick="template.subFunction.value='findAll';show_wait();template.submit();return true;" />
      	<img src="images/transfers.gif" class="mouse" name="hosDictsUnitinfoDictcostsubj_import"
						onclick="importData(this,1)" />-->
      </td>
    </tr>
  </html:table>
  <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro!=null) {
      TableMarge oper = new TableMarge(ro, "return find()");
      
    	oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
     //oper.addOptionButton("images/update1.gif", "return updateCode()");   // 修改--newlic
      oper.addNeedButton("images/create.gif", "return create()");     //  添加
    
  %>
  <html:title clazz='table'>成本项目</html:title>
  <!-- 复杂信息 -->
  <html:table clazz="complex">
    <!-- 操作 -->
    <!--
    <tr>
    	<td>
    		<button class='pageBtn' onclick='return selectAll();' >全选</button>
    		<button class='pageBtn' onclick='return reset();' >重置</button>
    		<button class='pageBtn' onclick='return remove();' >删除</button>
    		<button class='pageBtn' onclick='return updateCode();' >修改</button>
    		<button class='pageBtn' onclick='return create();' >添加</button>
    	</td>
    </tr>
    -->
	<!--增加分页显示 开始	-->
	<!-- 操作 -->
	    <tr><td><%=oper%></td></tr>
	<!--增加分页显示 结束	-->
  <!-- 结果集 -->
  <tr>
    <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td class="resultLabel">选择</td>
          <td class="resultLabel">成本项目代码</td>
          <td class="resultLabel">成本项目名称</td>
          <td class="resultLabel">上级代码</td>
          <td class="resultLabel">医疗/药品标识</td>
		  <td class="resultLabel">是否停用</td>
		  <td class="resultLabel">是否末级</td>
		  <td class="resultLabel">拼音码</td>
        </html:tr>

        <%
          String[][] result = ro.getTableResult();
          if ( result != null )
          {
            for (int i = 0; i < result.length; i++ )
            {
              String primaryKey = result[ i ][ 0 ];
               for (int j=0; j<result[i].length; j++)
                       {
                         if (result[i][j]!=null&&result[i][j].trim().equals(""))
                           {
                             result[i][j]="&nbsp;";
                           }
                     }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td><input type="checkbox"   <%out.print("name=\"primaryKey\"");%>  value="<%=primaryKey+" "+result[ i ][ 2 ]%>"></td>
          <td class="normalText"><a href="costsubj.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>&_current_page=<%=request.getParameter("_current_page")==null? "0":request.getParameter("_current_page")%>"><%=primaryKey%></a></td>
          <td class="normalText"><%=result[ i ][ 1 ]%></td>
          <td class="normalText"><%=result[ i ][ 2 ]%></td>
          <td class="normalText"><%=result[ i ][ 3 ]%></td>
          <td class="normalText"><%=result[ i ][ 4 ]%></td>
          <td class="normalText"><%=result[ i ][ 5 ]%></td>
           <td class="normalText"><%=result[ i ][ 6 ]%></td>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>

  <!-- 操作 -->


  </html:table>
	<%}%>
  <input type=hidden name="subFunction"/>
  <input type="hidden" name="initsub" value="sub"/>
</form>
<div id="upload_file_div" style="position:absolute;display:none;">
	    <iframe src="uploadFileD.cmd" id="uploadFrame"></iframe>
	</div>
	<script Language="JavaScript" >
		_____VHFileCreatFromTimerss=window.setInterval(createVhFile,10);
		  function createVhFile(){
		    if(typeof(uploadFrame)=="undefined")
		      return ;
		    window.clearInterval(_____VHFileCreatFromTimerss);
		    fileUploader=new VhFileUploader(uploadFrame);
		  }
	</script>
</html:html>
