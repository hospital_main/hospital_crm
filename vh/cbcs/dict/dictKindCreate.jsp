<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictKindCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-29 12:07 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript">
  function create() {
    if (isEmpty(template.kind_code)) {
      alert('人员类别代码不能为空');
      return false;
    }
    if(isTooLong(template.kind_code, 10)) {
      alert('人员类别代码不能高于10个字符!');
      return false;
    }
    if (isTooLong(template.kind_name, 20)) {
      alert('人员类别名称不能高于20个字符!');
      return false;
    }
    if (isEmpty(template.kind_name)) {
      alert('人员类别名称不能为空');
      return false;
    }

    template.subFunction.value='create';
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back() {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictKind.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!--信息栏-->
  <html:title clazz='module'>人员类别添加页面</html:title>

  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">人员类别代码：</td>
      <td width="75%" class="normalText" nowrap="nowrap"><input type=text name="kind_code" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">人员类别名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="kind_name" class="textInputC" /></td>
    </tr>

    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button><button class="pageBtn" onclick="return back();">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back();" />--></td>
    </tr>
  </html:table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>
