<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictCostBelongTypeMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
	$Author: zhoulidong $
  $Modtime: $
	$Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
  function store(){
  	if(typeof(template._current_page)=="undefined" || typeof(template._old_current_page)=="undefined"){
  		alert("没有数据需要保存!");
  		return;
  	}
  	
    template.subFunction.value='store';
   	if (template!=null && template._old_current_page!=null) {
      template._current_page.value = template._old_current_page.value
    }
	
    show_wait();
    template.submit();
    return
  }
  function find(i) {
    if(i==2){
      if(template.flag.value=="true") {
        alert('请先保存已修改信息')
        return false
      }
    }
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return
  }
  function change(){
    template.flag.value=true
  }
</Script>

<html:html clazz="main">
    <form name="template" method="post" action="dictCostBelongType.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>成本所属类型主页面</html:title>

      <!-- 简单信息 -->
    <html:table clazz="simple">
        <tr>
          <td nowrap class="signText">成本代码：</td>
          <% String cost_code = (String)request.getParameter("cost_code");%>
          <td><input  type=text name="cost_code" class="textInputC" <%if(cost_code != null){ out.println(" value=" + cost_code);}%>></td>
           <td nowrap class="signText">成本名称：</td>
          <% String cost_name = (String)request.getParameter("cost_name");%>
          <td><input type=text name="cost_name" class="textInputC" <%if(cost_name != null){ out.println(" value=" + cost_name);}%>></td>
          <td class="signText">成本类别：</td>
          <td class="normalText">
            <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("init_cost_type"), "cost_type", request.getParameter("cost_type"), false, false)%>
          </td>
        </tr>
      	<tr>
          
          <td colspan="6" align="right"><button class="pageBtn" onclick="return find(1)" >查询</button> </td>
      	</tr>
      </html:table>

      <br>

      <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro!=null) {
      TableMarge oper = new TableMarge(ro, "return find(2)");
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addNeedButton("images/save.gif", "return store()");     //  保存
      %>

  <html:title clazz='table'>成本所属类型字典</html:title>
      <!-- 复杂信息 -->
    <html:table clazz="complex">
        <!-- 操作 -->
        <tr><td><%=oper%></td></tr>
<%
          String[][] result = ro.getTableResult();
          if (result!=null) {
%>
        <!-- 结果集 -->
        <tr>
          <td>
			      <html:table clazz="result">
					    <html:tr clazz='label'>
                <td class="resultLabel">成本项目名称</td>
<%
            for(int i=2; i<result[0].length; i++){
%>
                <td class="resultLabel"><%=result[1][i]%></td>
<%
              for(int j=2; j<result.length; j++)
                if(result[j][i]==null)
                  result[j][i] = "N";
            }
%>
              </html:tr>

<%
            for(int m=2; m<result.length; m++){
              String rowColor = "rowGray";
              if (m/2*2==m) rowColor = "rowWhite";
%>
              <tr CLASS="<%=rowColor%>">
                  <td class="normalText"><%=result[m][1]%></td>
                  <input type=hidden name="subj" value="<%=result[m][0]%>"/>
<%
              for(int n=2; n<result[0].length; n++){
                String[] temp = {result[m][0], result[0][n]};
                String primaryKey = ExtendTool.arrayToString(temp);
%>
                  <td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>" <%if(result[m][n].equals("Y")) out.print("checked");%> onchange="return change()"></td>
            <%}%>
              </tr>
<%            }%>
            </html:table>

<%
	        }
%>

          </td>
        </tr>

        <!-- 操作 -->

      </html:table>
<%
   	}
%>
      <input type=hidden name="subFunction"/>
      <input type=hidden name="flag" value="false"/>
</form>
</html:html>
