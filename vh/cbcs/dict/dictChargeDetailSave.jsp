<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictChargeDetailSave.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-09-02 10:41 $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
  String[][] chargeKinds = (String[][])request.getAttribute("charge_detail_kind");
  String[][] chargeKindsSelect = new String[chargeKinds.length][2];
	for (int i = 0; i < chargeKinds.length; i++) {
    chargeKindsSelect[i][0] = chargeKinds[i][0];
    chargeKindsSelect[i][1] = chargeKinds[i][1];
  }
%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    //template.subFunction.value='save';
    if(isEmpty(template.charge_detail_name))
    {
      alert('医疗项目名称不能为空!');
      return;
    }
    if(isTooLong(template.charge_detail_name,40))
    {
      alert('医疗项目名称不能大于40个字符!');
      return;
    }
    if(isTooLong(template.spell,10))
    {
      alert('拼音码不能大于10个字符!');
      return;
    }
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    template._current_page.value = <%=request.getParameter("_current_page")%>
		show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictChargeDetail.jspviewhigh">
	<!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>医疗项目修改页面</html:title>

  <%
    String[] result = (String[])request.getAttribute("result");
    if ( result != null )
    {
  %>

  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">医疗项目代码： </td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0]%>" disabled />
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">医疗项目名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="charge_detail_name" value="<%=result[1]%>" class="textInputC" onchange="getSpellCode(this);">
      </td>
    </tr>

     <tr>
      <td class="signText" nowrap="nowrap">医疗项目类别：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(chargeKindsSelect, "charge_kind_code", result[2], true, true)%>
      </td>
     </tr>
     
     <tr>
      <td class="signText" nowrap="nowrap">拼音码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="spell" value="<%=result[5]%>" class="textInputC">
      </td>
    </tr>
    
    <tr>
      <td class="signText" nowrap="nowrap">停用标志：</td>
      <td class="normalText" nowrap="nowrap">
        是<input type="radio" style= "border:0pt; " name="stop_mark" value="Y" <% if(result[4].equals("Y")){out.println(" checked ");}%>/>
        否<input type="radio" style= "border:0pt; " name="stop_mark" value="N" <% if(result[4].equals("N")){out.println(" checked ");}%>/>
      </td>
    </tr>


    <tr>
      <td colspan="2" align="left">　　　　　　　　　　
      <button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </html:table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="initsub" value="sub"/>
  <input type="hidden" name="charge_detail_code" value="<%=result[0]%>">
  <input type=hidden name="_current_page" value = "<%=request.getParameter("_current_page")%>" />
  <%}%>
</form>

</html:html>
<script>
function getSpellCode(source){
var name = source.value ;
name = "<a>"+name+"</a>"
var p=getValuePairBySql("ccbsGetSpellCode_query",name);
	var ec,en;
	if(p!=null){
		ec=p[0];
		en=p[1];	
	}else{
		ec="";
		en="";
	}
document.getElementById("spell").value = ec;
  
}

</script>
