<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictLogisticServeCreate.jsp,v 1.2 2014/03/13 01:25:48 yuchengying Exp $
 $Author: yuchengying $
 $Date: 2014/03/13 01:25:48 $
 $Modtime: 03-08-28 22:55 $
 $Revision: 1.2 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
  template.logistic_serve_code.value = getCode(template.logistic_serve_code.value);
    //template.subFunction.value='create';
    if(isEmpty(template.logistic_serve_code))
    {
      alert('院内服务项目代码不能为空!');
      return;
    }
    if(isTooLong(template.logistic_serve_code,10))
    {
      alert('院内服务项目代码不能高于10个字符!');
      return;
    }
    if(isTooLong(template.logistic_serve_name,100))
    {
      alert('院内服务项目名称不能高于100个字符!');
      return;
    }
    if(isTooLong(template.unit,10))
    {
      alert('单位不能高于10个字符!');
      return;
    }
    switch(isDouble(template.unit_price,7,2))
    {
      case 0 : alert('参考单价必须为数字型'); return;
      case 1 : alert('参考单价整数部分不能高于7个字符'); return;
      case 2 : alert('参考单价没有整数部分'); return;
      case 3 : alert('参考单价小数部分不能高于2个字符'); return;
    }
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
		show_wait();
    element.submit();
  }
</Script>

<html:html clazz="main">  <form name="template" method="post" action="dictLogisticServe.jspviewhigh">
  <!-- 信息提示栏 -->
 <html:message/>

  <!-- 标题栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("院内服务项目添加页面")%>

  <!-- 简单信息 -->
  <table  width="100%" class="simpleQuery" >
    <tr>
      <td class="signText" nowrap="nowrap">院内服务项目代码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="logistic_serve_code" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">院内服务项目名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="logistic_serve_name" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">单位：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="unit" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">参考单价：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="unit_price" class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2" align="left">　　　　　　　　　　　　　　　　　　　　　　
      <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr>
    <td></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>
