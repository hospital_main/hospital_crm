<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictEmployeeCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:57:47 $
  $Modtime: 03-09-01 23:56 $
	$Revision: 1.1 $
	$NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {

if(isEmpty(template.emp_id))
    {
      alert('职工编号不能为空!');
      return;
    }
if(isEmpty(template.emp_name))
    {
      alert('职工姓名不能为空!');
      return;
    }
if(isEmpty(template.sex))
    {
      alert('性别不能为空!');
      return;
    }
if(isEmpty(template.dept_code))
    {
      alert('所在科室不能为空!');
      return;
    }
    if(isTooLong(template.emp_id,12))
    {
      alert('职工编号不能高于12个字符!');
      return;
    }
    if(isEmpty(template.emp_name))
    {
      alert('职工姓名不能为空!');
      return;
    }
    if(isTooLong(template.emp_name,20))
    {
      alert('职工姓名不能高于12个字符!');
      return;
    }
		show_wait();
    template.submit();
    return true;
  }

  function back() {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
		show_wait();
  	template.submit();
  	return true;
  }

</Script>

<html:html clazz="main">
    <form name="template" method="post" action="dictEmployee.jspviewhigh">
     <!-- 信息提示栏 -->
       <html:message/>
      <!--信息栏-->
      <html:title clazz='module'>职工信息添加页面</html:title>


      <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap="nowrap">职工编号：</td>
          <td width="75%" class="normalText" nowrap="nowrap"><input type=text name="emp_id" class="textInputC" maxlength="20"/></td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">职工姓名：</td>
          <td class="normalText" nowrap="nowrap"><input type=text name="emp_name" class="textInputC" maxlength="40"/></td>
        </tr>

        <tr>
          <td class="signText" nowrap="nowrap">性别：</td>
          <td class="normalText" nowrap="nowrap">
            <%=new SingleSelect(request.getAttribute("init_sex"), "sex", request.getParameter("sex"), false, true)%>
  		    </td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">所在科室：</td>
          <td class="normalText" nowrap="nowrap">
    		    <%=new SingleSelect(request.getAttribute("init_dept_code"), "dept_code", request.getParameter("dept_code"), true, true)%>
    		  </td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">职务：</td>
          <td class="normalText" nowrap="nowrap">
            <%=new SingleSelect(request.getAttribute("init_duty_code"), "duty_code", request.getParameter("duty_code"), false, false)%>
    		  </td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">人员类别：</td>
          <td class="normalText" nowrap="nowrap">
            <%=new SingleSelect(request.getAttribute("init_kind_code"), "kind_code", request.getParameter("kind_code"), false, false)%>
    		  </td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">职称：</td>
          <td class="normalText" nowrap="nowrap">
            <%=new SingleSelect(request.getAttribute("init_title_code"), "title_code", request.getParameter("title_code"), false, false)%>
    		  </td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">学历：</td>
          <td class="normalText" nowrap="nowrap">
            <%=new SingleSelect(request.getAttribute("init_edu_code"), "edu_code", request.getParameter("edu_code"), false, false)%>
    		  </td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">停用标志：</td>
          <td class="normalText" nowrap="nowrap">
    		     是<input type="radio" name="stop_mark" value="Y"/>
             否<input type="radio" name="stop_mark" value="N" checked/>
    		  </td>
        </tr>
        <tr>
          <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
          <button class="pageBtn" onclick="return reset();" >重置</button>
          <button class="pageBtn" onclick="return back();">返回</button>   
          <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
          <img src="images/return.gif" class="mouse" onclick="return back();" />--></td>
        </tr>
      </html:table>
      <input type="hidden" name="initsub" value="sub"/>
      <input type=hidden name="subFunction" value="create"/>
  	</form>


</html:html>
