<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictChargeTypeSave.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 23:27 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript">
    function save() {
        template.subFunction.value='save';
				show_wait();
        template.submit();
        return true;
    }

    // 返回
    function back( element ){
       for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
       template.subFunction.value='findAll';
			 show_wait();
       element.submit();
    }
</Script>

<html:html clazz="main">  <form name="template" method="post" action="dictChargeType.jspviewhigh">
    <table width="97%" border="0" cellpadding="0" cellspacing="0">

        <tr bgcolor="#FFFFFF">
            <td valign="top"><img src="../../images/left.gif" width="5" height="5"></td>
            <td align="right" valign="top"><img src="../../images/right.gif" width="5" height="5"></td>
        </tr>

        <%
            String message = ( String )request.getAttribute( "message" );
            if ( message != null && !message.trim().equals( "" ) ) {
        %>

            <tr bgcolor="#FFFFFF" class="a12">
                <td height="26" colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;<font color="#FF0000"><%=message%></font></td>
            </tr>

        <%}else{

            String[] result = (String[])request.getAttribute( "result" );
                if ( result != null ) {

              %>
        <tr bgcolor="#FFFFFF">
            <td colspan="2">
                <table width="60%" border="0" align="center" cellspacing="1" bgcolor="#999999">
                     <tr bgcolor="#FFFFFF" class="a12" align="center">
                            <td align="right" bgcolor="#CCCCCC">费别代码：</td>
                            <td align="left" bgcolor="#CCCCCC">
                              <input type="text" name="charge_type_code" value="<%=result[0]%>" disabled="true"/>
                            </td>
                    </tr>
                    <tr bgcolor="#FFFFFF" class="a12" align="center">
                        <td align="right" bgcolor="#CCCCCC">费别名称：</td>
                        <td align="left" bgcolor="#CCCCCC">
                          <input type="text" name="charge_type_name" value="<%=result[1]%>"/>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF" class="a12" align="center">
                        <td align="right" bgcolor="#CCCCCC">是否停用：</td>
                        <td align="left" bgcolor="#CCCCCC">
                          是<input type="radio" name="stop_mark" value="Y" <% if(result[2].equals("Y")){out.println(" checked ");}%>/>
                          否<input type="radio" name="stop_mark" value="N" <% if(result[2].equals("N")){out.println(" checked ");}%>/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr bgcolor="#FFFFFF" class="a12" align="center">
            <td height="60" valign="middle" colspan="6">
                <input type="button" value="修改" onClick="save();"> &nbsp;&nbsp;
                <input type="reset"  value="重 置" >&nbsp;&nbsp;
                <input type="button" value="返 回" onclick="return back( template );" >
                <input type="hidden" name="subFunction" >
                <input type="hidden" name="charge_type_code" value="<%=result[0]%>">
            </td>
        </tr>

    <%
        }
    }
    %>
    </table>
</form>
</html:html>
