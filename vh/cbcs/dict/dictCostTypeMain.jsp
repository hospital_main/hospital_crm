<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictCostTypeMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">


  function find() {
    template.subFunction.value='findAll';
		show_wait();
    template.submit();
		return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="dictCostType.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>成本类型主页面</html:title>

  <!-- 简单信息 -->
  <html:table clazz="simple1">
  </html:table>

  <br>
  <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro!=null) {
      %>

  <html:title clazz='table'>成本类型</html:title>
  <!-- 复杂信息 -->
  <html:table clazz="complex">
  <tr>
    <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td class="resultLabel">成本类型代码</td>
          <td class="resultLabel">成本类型名称</td>
        </html:tr>

        <%
            String[][] result = ro.getTableResult();
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                String primaryKey = result[ i ][ 0 ];
                for (int j=0; j<result[i].length; j++)
                       {
                         if (result[i][j]!=null&&result[i][j].trim().equals(""))
                           {
                             result[i][j]="&nbsp;";
                           }
                     }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText"><%=primaryKey%></td>
          <td class="normalText"><%=result[ i ][ 1 ]%></td>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>

  <!-- 操作 -->


  </html:table>
<%
   }
%>
  <input type=hidden name="subFunction"/>
</form>
</html:html>
