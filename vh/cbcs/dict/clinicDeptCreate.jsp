<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/clinicDeptCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-09-02 10:08 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
  template.clin_code.value = getCode(template.clin_code.value);
    if(isEmpty(template.clin_code))
    {
      alert('自定义科室代码不能为空!');
      return;
    }
    if(isEmpty(template.clin_name))
    {
      alert('自定义科室名称不能为空!');
      return;
    }
    if(isTooLong(template.clin_code,20))
    {
      alert('自定义科室代码不能高于20个字符!');
      return;
    }
    if(isTooLong(template.clin_name,40))
    {
      alert('自定义科室名称不能高于40个字符!');
      return;
    }
    if(template.clin_name.value=='临床科室' ||template.clin_name.value=='医技科室' ||template.clin_name.value=='医辅科室' ||template.clin_name.value=='管理科室' || template.clin_name.value=='自定义科室' || template.clin_name.value=='住院合计' || template.clin_name.value=='医技合计' || template.clin_name.value=='医辅合计' || template.clin_name.value=='门诊合计')
    {
      alert('不能用该科室名称,请重新定义!');
      return;
    }
    if(template.clin_name.value=='住院' || template.clin_name.value=='门诊' || template.clin_name.value=='医技' || template.clin_name.value=='每职工')
    {
      alert('不能用该科室名称,请重新定义!');
      return;
    }
    
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
		show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post"  action="dictClinicDept.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>自定义科室添加页面</html:title>


  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">自定义科室代码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type='text' name="clin_code" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">自定义科室名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type='text' name="clin_name" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">是否统计合计：</td>
      <td class="normalText" nowrap="nowrap">
         <input name="btot" type="radio" value='0'  checked  style="border:0">否
         <input name="btot" type="radio" value='1'  style="border:0">是
      </td>
    </tr>
    <tr>
    	<td class="signText" nowrap="nowrap">科室类型：</td>
    	<td>
      <select name="dept_type" size="1" style="width:140px;">
           <option value="1" >管理科室</option>
           <option value="2" >医辅科室</option>
           <option value="3" >医技科室</option>
           <option value="4" >临床科室</option>
      </select>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="left">　　　　　　　　　　　　　　　　　　　　
      <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </html:table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type='hidden' name="subFunction" value="create"/>
</form>
</html:html>
