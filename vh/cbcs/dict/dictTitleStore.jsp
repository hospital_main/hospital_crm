<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictTitleStore.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-29 12:57 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript">
  function store() {
    if(isTooLong(template.title_code, 10)) {
      alert('职称代码不能高于10个字符!');
      return false;
    }
    if (isTooLong(template.title_name, 20)) {
      alert('职称名称不能高于20个字符!');
      return false;
    }
    if (isEmpty(template.title_name)) {
      alert('职称名称不能为空');
      return false;
    }
    template.subFunction.value='store';
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back() {
    for(var i=0;i<template.elements.length;i++)
      template.elements[i].value="";
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return;
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictTitle.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!--信息栏-->
  <html:title clazz='module'>职称修改页面</html:title>

  <%
    String[] result = (String[])request.getAttribute( "result" );
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">职称代码：</td>
      <td width="75%" class="normalText" nowrap="nowrap" class="textInputC">
        <input type=text value="<%=result[0]%>" disabled />
        <input type=hidden name="title_code" value="<%=result[0]%>">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">职称名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="title_name" value="<%=result[1]%>" class="textInputC">
      </td>
    </tr>
    <tr>
      <td colspan="2">
      <button class="pageBtn" onclick="return store();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back();">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return store();" />
      <img src="images/reset.gif" class="mouse" onclick="return reset();" /> <img src="images/return.gif" class="mouse" onclick="return back();" />--> </td>
    </tr>
  </html:table>
  <input type=hidden name="subFunction" value = "save"/>
  <%}%>
</form>

</html:html>
