<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictFixAssetDeprTypeCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 21:41 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
  	template.fix_asset_depr_type_code.value = getCode(template.fix_asset_depr_type_code.value);
    if(isEmpty(template.fix_asset_depr_type_code))
    {
      alert('固定资产折旧类型代码不能为空!');
      return;
    }
    if(isEmpty(template.fix_asset_depr_type_name))
    {
      alert('固定资产折旧类型名称不能为空!');
      return;
    }
    if(!isRadioChecked(template.stop_mark))
    {
      alert('停用标志必须选!');
      return;
    }
    if(isTooLong(template.fix_asset_depr_type_code,2))
    {
      alert('固定资产折旧类型代码必须小于2个字符!');
      return;
    }
    if(isTooLong(template.fix_asset_depr_type_name,20))
    {
      alert('固定资产折旧类型名称不能大于20个字符!');
      return;
    }
    //template.subFunction.value='create';
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
		show_wait();
    element.submit();
  }
</Script>


<html:html clazz="main">  <form name="template" method="post" action="fixassetdeprtype.jspviewhigh">
  <!-- 信息提示栏 -->
 <html:message/>

  <!-- 标题栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("固定资产折旧类型添加页面")%>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" class="simpleQuery">
    <tr>
      <td class="signText" nowrap="nowrap">固定资产折旧类型代码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="fix_asset_depr_type_code" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">固定资产折旧类型名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="fix_asset_depr_type_name" class="textInputC"/></td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">停用标志：</td>
      <td class="normalText" nowrap="nowrap">
		    是<input type="radio" name="stop_mark" value="Y" />
        否<input type="radio" name="stop_mark" value="N" checked />
		  </td>
    </tr>

    <tr>
      <td colspan="4" align="center"> <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>
