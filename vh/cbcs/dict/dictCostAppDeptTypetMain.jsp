<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictCostAppDeptTypetMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,
                java.util.HashMap,
		java.util.Map
                " %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
				show_wait();
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
					show_wait();
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }
function find() {
    template.subFunction.value='findAll';
    template.submit();return true;
  }
  function saveUse(){
  	var alloc={"A":"","B":"","C":""};
  	var ips=deptClassTable.getElementsByTagName("input");
  	var str="";
  	for(var i=0;i<ips.length;i++){
  		if(ips[i].checked==true){
  			if(ips[i].kind=="use")
  				str+=ips[i].name;
  			else if(ips[i].kind=="alloc"){
  				if(typeof(alloc[ips[i].name])!="undefined"){
  					alloc[ips[i].name]=alloc[ips[i].name]+ips[i].code;
  				}
  			}
  		}
  	}
  	xmlhttp.post("cbcsDictAppoDeptModel_use","<use>"+str+"</use><a>"+alloc.A+"</a><b>"+alloc.B+"</b><c>"+alloc.C+"</c>","");
  	doMsg(xmlhttp._object.responseText);
  }
  function changeUsage(code,isUse){
  	var ips=deptClassTable.getElementsByTagName("input");
  	for(var i=0;i<ips.length;i++){
  		if(ips[i].kind=="alloc"&&ips[i].code==code){
			if(isUse==true){
				ips[i].disabled=false;
				ips[i].checked=true;
			}else{
				ips[i].disabled=true;
				ips[i].checked=false;
			}
		}
  	}
  	
  }
  
   function changeUsageNew(obj,code,isUse){
  	var ips=deptClassTable.getElementsByTagName("input");
  	for(var i=0;i<ips.length;i++){
  		if(ips[i].kind=="alloc"&&ips[i].code==code){
			if(isUse==true){
				ips[i].disabled=false;
				ips[i].checked=true;
			}else{
				ips[i].disabled=true;
				ips[i].checked=false;
			}
		}
  	}
  	
  	var currTd = obj.parentNode.nextSibling;
  	var checks=currTd.getElementsByTagName("input");
  	for(var i=0;i<checks.length;i++){
			if(isUse==true){
				checks[i].disabled=false;
				checks[i].checked=true;
			}else{
				checks[i].disabled=true;
				checks[i].checked=false;
			}
  	}
  }
  function changeParent(obj,code,isUse){

  	var currTd = obj.parentNode.previousSibling;
  	var checks=currTd.getElementsByTagName("input");
  	for(var i=0;i<checks.length;i++){
			if(isUse==true){
				checks[i].checked=true;
				changeUsage(checks[i].name,true)
			}else{
				//checks[i].checked=false;
			}
  	}
  	
  	var ips=deptClassTable.getElementsByTagName("input");
  	
  	for(var i=0;i<ips.length;i++){
  		if(ips[i].kind=="use"&&ips[i].name==code){
  			
			if(isUse==true){
				ips[i].checked=true;
				changeUsage(ips[i].name,true)
			}else{
				//ips[i].checked=false;
			}
		}
  	}
  	
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="dictAppoDept.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>科室类别主页面</html:title>

  <!-- 简单信息 -->
  <html:table clazz="simple"><button class="pageBtn" onclick="return saveUse();" style="text-align:center">保存</button>
  </html:table>

  <br>
  <%
  	StringBuffer checkList=new StringBuffer();
  	Map alloc=new HashMap();
  	//alloc.put("A",new String[]{"B","医疗辅助科室","C","医疗技术科室","D","直接医疗科室","S","科研科室","E","教学科室"});
  	alloc.put("A",new String[]{"B","医疗辅助科室","C","医疗技术科室","D","直接医疗科室"});
  //	alloc.put("B",new String[]{"C","医疗技术科室","D","直接医疗科室","S","科研科室","E","教学科室"});
  	alloc.put("B",new String[]{"C","医疗技术科室","D","直接医疗科室"});
  	alloc.put("C",new String[]{"D","直接医疗科室"});
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro!=null) {
      %>

  <html:title clazz='table'>科室类别</html:title>
  <!-- 复杂信息 -->
  <html:table clazz="complex">
  <tr>
    <td id="deptClassTable">
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td class="resultLabel" noWrap='true'>科室类别代码</td>
          <td class="resultLabel" noWrap='true'>科室类别名称</td>
          <td class="resultLabel" noWrap='true'>是否启用</td>
          <td class="resultLabel" noWrap='true'>是否分摊</td>
        </html:tr>

        <%
        	
        	
            String[][] result = ro.getTableResult();
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                String primaryKey = result[ i ][ 0 ];
                 for (int j=0; j<result[i].length; j++)
                       {
                         if (result[i][j]!=null&&result[i][j].trim().equals(""))
                           {
                             result[i][j]="&nbsp;";
                           }
                     }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText" noWrap='true'><%=primaryKey%></td>
          <td class="normalText" noWrap='true'><%=result[ i ][ 1 ]%></td>
          <td class="normalText" noWrap='true'>
          <%if("0BESU".indexOf(result[ i ][ 0 ])>=0){%>
          	<input type="checkbox" kind="use" name="<%=result[ i ][ 0 ]%>" <%="true".equals(result[ i ][ 2 ])?"checked":""%> onclick="changeUsageNew(this,this.name,this.checked)" />
          <%}%>
          </td>
          <td align="left" noWrap='true'>
         	<%if(alloc.containsKey(result[ i ][ 0 ])){
         		String[] items=(String[])alloc.get(result[ i ][ 0 ]);
         		for(int is=0;is<items.length;is+=2){
         	%>
          		<input type="checkbox" kind="alloc" code="<%=items[is]%>" onclick="changeParent(this,this.code,this.checked)" name="<%=result[ i ][ 0 ]%>" <%=(result[ i ][ 3 ].indexOf(items[is])>=0)?"checked":""%> /><%=items[is+1]%>&nbsp;
          	<%}}%>
          </td>
        </tr>

        <%
                }
              }
        %>
      </html:table>
    </td>
  </tr>

  <!-- 操作 -->

  </html:table>
  <script Language="JavaScript">
	
</script>
<%
   }
%>

  <input type=hidden name="subFunction"/>
</form>
</html:html>
