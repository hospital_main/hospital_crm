<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/paraDeptsMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-12 17:21 $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(101, request)%>

<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool"%>
<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
				show_wait();
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
					show_wait();
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }
  function find() {
    template.subFunction.value='findAll';
		show_wait();
    template.submit();
		return true;
  }
</Script>

<form name="template" method="post" action="paraDepts.jspviewhigh">
	<!-- 返回信息栏 -->
  <table width="100%" border="0">
    <tr>
      <td>
        <table width="100%" border="0" >

          <%
            String message = (String)request.getAttribute("message");
            if (message!=null && !message.trim().equals("")) {
          %>

          <tr>
            <td class="successText" align="center"><%=message%></td>
          </tr>

          <%}
            String appError = (String)request.getAttribute("appError");
            if (appError!=null && !appError.trim().equals("")) {
          %>

          <tr>
            <td class="errorText" align="center"><%=appError%></td>
          </tr>

          <%}%>
        </table>
      <td>
    <tr>
  </table>

  <!-- 标题栏 -->
  <table width="100%" border="0">
    <tr>
      <td>
        <table width="100%" border="0" >
          <tr>
            <td  class="moduleTitle" nowrap="nowrap">科室基本情况表主页面：</td>
          </tr>
        </table>
      <td>
    <tr>
  </table>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
        <td class="signText">科室</td>
       <td class="normalText">
          <select type="select" name="dept_code">
              <%String dept_code=request.getParameter("dept_code");%>
              <option value="" <%if(dept_code==null||dept_code.equals("")) out.println(" selected ");%>>请选择</option>
              <%
                  String table_result[][] =
                      (String[][])request.getAttribute( "table_result" );
                  if(table_result!=null)
                  {
                      for( int i = 0; i < table_result.length; i++ )
                      {  for (int j=0; j<table_result[i].length; j++)
                       {
                         if (table_result[i][j]!=null&&table_result[i][j].trim().equals(""))
                           {
                             table_result[i][j]="&nbsp;";
                           }
                     }
              %>
              <option value="<%=table_result[i][0]%>" <%if(dept_code!=null&&!dept_code.equals("")&&dept_code.equals(table_result[i][0])) out.println(" selected ");%>><%=table_result[i][0]+":"+table_result[i][1]%></option>
              <%
                      }
                  }
              %>
          </select>
      </td>
       <td class="signText">成本分摊参数</td>
       <td class="normalText">
          <select type="select" name="app_para_code">
              <%String app_para_code=request.getParameter("app_para_code");%>
              <option value="" <%if(app_para_code==null||app_para_code.equals("")) out.println(" selected ");%>>请选择</option>
              <%
                  String table_result1[][] =
                      (String[][])request.getAttribute( "table_result1" );
                  if(table_result1!=null)
                  {
                      for( int i = 0; i <table_result1.length; i++ )
                      {
              %>
              <option value="<%=table_result1[i][0]%>" <%if(app_para_code!=null&&!app_para_code.equals("")&&app_para_code.equals(table_result1[i][0])) out.println(" selected ");%>><%=table_result1[i][1]%></option>
              <%
                      }
                  }
              %>
          </select>
      </td>
      <td>
  <button class="pageBtn" name=""  onclick="template.subFunction.value='findAll';show_wait();template.submit();return true;"  >查询</button>    
    <!-- <img src="images/find.gif" class="mouse" onclick="template.subFunction.value='findAll';show_wait();template.submit();return true;" />-->
    </tr>
  </table>

  <br>
  <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
      oper.addNeedButton("images/create.gif", "return create()");     //  添加
      %>
  <!-- 复杂信息 -->
  <table width="100%">
    <!-- 操作 -->
    <tr><td><%=oper%></td></tr>

  <!-- 结果集 -->
  <tr>
    <td>
      <table  BORDERCOLOR="#214597" border="1" cellspacing="1" width="100%" >
        <tr>
          <td colspan="100" class="resultTitle" align="center">科室基本情况表</td>
        </tr>
        <tr>
          <td class="resultLabel">选择</td>
          <td class="resultLabel">科室名称</td>
          <td class="resultLabel">分摊参数</td>
          <td class="resultLabel">数量</td>
        </tr>

        <%
          String[][] result = ro.getTableResult();
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                String[] temp = {result[i][0], result[i][1]};
                String primaryKey = ExtendTool.arrayToString(temp);
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
          <td class="normalText"><a href="paraDepts.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>"><%=result[i][3]%></a></td>
          <td class="normalText"><%=result[i][4]%></td>
          <td nowrap class=numberText><%=result[i][2]%></td>
        </tr>

        <%
              }
            }
        %>
      </table>
    </td>
  </tr>

  <!-- 操作 -->

  </table>

  <input type=hidden name="subFunction"/>
</form>


<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>


