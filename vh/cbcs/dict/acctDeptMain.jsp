<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/acctDeptMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<html:html clazz="main">
<form name="template" method="post" action="dictacctdept.jspviewhigh">
  <!-- 信息提示栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>

  <!-- 标题栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("核算科室主页面")%>

  <!-- 复杂信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td>
        <table  width="50%" cellspacing="2" border="0" >
          <tr>
            <td>
              <iframe  VSPACE="1" HSPACE="1" FRAMEBORDER="0" WIDTH="250" SRC="dictacctdept.jspviewhigh?subFunction=findAll" NAME="Dept_tree" HEIGHT="500" ></iframe>
            </td>
          </tr>
        </table>
      </td>
      <td >
        <table  width="50%" cellspacing="2" border="0" >
          <tr>
            <td>
              <iframe  VSPACE="1" HSPACE="1" FRAMEBORDER="0" WIDTH="550" SRC="" NAME="Iframe_table" HEIGHT="500" ></iframe>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <input type=hidden name="subFunction"/>
</form>
</html:html>


