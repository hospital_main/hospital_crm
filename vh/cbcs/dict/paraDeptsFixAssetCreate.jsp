<!--
 	 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/paraDeptsFixAssetCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 	 $Author: zhoulidong $
 	 $Date: 2012/03/12 01:57:47 $
	 $Modtime: 03-08-13 9:46 $
	 $Revision: 1.1 $
	 $NoKeywords: $
 -->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(101, request)%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
    //template.subFunction.value='create';
     var str=template.dept_code.value
    if(str.charAt(str.length-1)!='Y')
     alert("必须选择末级科室")
   else{
   switch(isDouble(template.fix_asset_value,10,2))
    {
      case 0 : alert('数量必须为数字型'); return;
      case 1 : alert('数量整数部分不能高于10个字符'); return;
      case 2 : alert('数量没有整数部分'); return;
      case 3 : alert('数量小数部分不能高于2个字符'); return;
    }
		show_wait();
    template.submit();
    return true;
   }
  }

  function back() {
   for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
		show_wait();
  	template.submit();
  	return true;
  }

</Script>


    <form name="template" method="post" action="paraDeptsFixAsset.jspviewhigh">
     <!-- 信息提示栏 -->
       <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>

     <!--信息栏-->
       <%= new com.viewhigh.cbcs.base.mvc.view.MessageTitle("科室固定资产基本情况添加页面")%>


      <!-- 简单信息 -->
      <table  width="100%" cellspacing="2" border="0" >
        <tr>
       <%
        String[][] ParaDeptsFixAsset =
          (String[][])request.getAttribute("ParaDeptsFixAsset");

       %>
      <td class="signText" nowrap="nowrap">科室代码：</td>
      <td class="normalText" nowrap="nowrap">
		    <select type="select" name="dept_code"/>
          <%if(ParaDeptsFixAsset!=null)
          for(int i = 0; i < ParaDeptsFixAsset.length; i++)
          {
          %>

          <option value="<%=ParaDeptsFixAsset[i][0]+" "+ParaDeptsFixAsset[i][2]%>">
              <%=ParaDeptsFixAsset[i][0]%> <%=ParaDeptsFixAsset[i][1]%>
          </option>
          <%
          }
          %>
      </select>
		  </td>
     </tr>
     <tr>
       <%
        String[][] FixAssetDeprTypeCode =
          (String[][])request.getAttribute("FixAssetDeprTypeCode");

       %>
      <td class="signText" nowrap="nowrap">固定资产折旧类型：</td>
      <td class="normalText" nowrap="nowrap">
		    <select type="select" name="fix_asset_depr_type_code"/>
          <%if(FixAssetDeprTypeCode!=null)
          for(int i = 0; i < FixAssetDeprTypeCode.length; i++)
          {
          %>

          <option value="<%=FixAssetDeprTypeCode[i][0]%>">
           <%=FixAssetDeprTypeCode[i][1]%>
          </option>e
          <%
          }
          %>
          </select>
        </td>
       </tr>
       <tr>
          <td class="signText" nowrap="nowrap">固定资产金额：</td>
          <td class="normalText" nowrap="nowrap"><input type=text name="fix_asset_value" class="textInputC" maxlength="10"/></td>
        </tr>
       <tr>
          <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
          <button class="pageBtn" onclick="return reset();" >重置</button><button class="pageBtn" onclick="return back();">返回</button>  
          <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
          <img src="images/return.gif" class="mouse" onclick="return back();" /> --></td>
        </tr>
      </table>
      <input type=hidden name="subFunction" value="create"/>


  	</form>


<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>
