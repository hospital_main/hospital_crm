<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/clinicDeptMapMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
	$Author: zhoulidong $
	$Date: 2012/03/12 01:57:47 $
	$Modtime: 03-09-02 11:44 $
	$Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">

	function autoRun(){
		if(template.needAlert.value=='true'){
			if(confirm('已经存在自定义科室定义，是否重新定义，如果重新定义，将引起以前的分析结果不正确？')){
				template.subFunction.value='autoDo';
				//show_wait();
				template.submit();
				return true;
			}
		}
	}
	function create() {
		template.subFunction.value='preparedCreate';
		show_wait();
		template.submit();
		return true;
	}

	function auto() {
		if(confirm('确认自定义科室定义与核算科室相同？')){
			template.subFunction.value='auto';
			show_wait();
			template.submit();
			return true;
		}else{
			return null;
		}
	}


	function remove() {
		var flag = false;
		for (var i=0; i<template.elements.length; i++) {
			if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
				flag = true;
		}

		if( flag!=false) {
			if (confirm('是否删除')) {
				template.subFunction.value='remove';
				show_wait();
				template.submit();
				return true;
			} else
			return false;
		} else {
		alert( "请先选择,再删除!");
		return false;
		}
	}

	function selectAll(){
		for (var i=0; i<template.elements.length; i++) {
			if (template.elements[i].name=='primaryKey')
				template.elements[i].checked = true;
		}
	}

	function find() {
		template.subFunction.value='findAll';
		show_wait();
		template.submit();
		return true;
	}
	
	function importData(obj,v){
		   		
				var fileName=fileUploader.choose(1);
				if(fileName.length<4&&fileName.indexOf(".")<0)
					return false;
				var fileType=fileName.substr(fileName.lastIndexOf(".")+1);
				fileUploader.submit(function(m,f){
						if( m != ''){
							alert(m);
							return false;
						}
						for(var k in f){
							fileType+="="+k;
							break;
						}
						var msg = "" ;
						var queryParams = "<comp_file>"+fileType+"</comp_file>" ;
						confirmImportStyleDialog(function(v){
								if(v==0)
									return;

					        window.xmlhttp.post("hosDictsUnitinfoDictclinicdeptmap_import",queryParams+"<q_flag>"+v+"</q_flag>");
					        
					         var responseText = window.xmlhttp._object.responseText ;
					          msg = window.doMsg(responseText,"show")
						  if(msg==true) {
							template.subFunction.value='findAll';
							show_wait();
					    		template.submit();
					    		return true;
						  }
						  });
					},obj
				)	
			}
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="dictClinicDeptMap.jspviewhigh">

		<html:message/>

		<html:title clazz='module'>自定义科室定义主页面</html:title>

		<!-- 简单信息 -->
		<html:table clazz="simple">
			<tr>
				<td nowrap class="signText">自定义科室代码：</td>
				<% String clin_code = request.getParameter("clin_code");%>
				<td class="normalText">
					<input type='text' name="clin_code" class="textInputC" <%if(clin_code != null){ out.println(" value=" + clin_code);}%>>
				</td>
				<td nowrap class="signText">自定义科室名称：</td>
				<% String clin_name = request.getParameter("clin_name");%>
				<td class="normalText">
					<input type='text' name="clin_name"  class="textInputC" <%if(clin_name != null){ out.println(" value=" + clin_name);}%>>
				</td>
				<td nowrap class="signText">科室代码：</td>
					<% String dept_code = request.getParameter("dept_code");%>
				<td class="normalText">
					<input type='text' name="dept_code" class="textInputC" <%if(dept_code != null){ out.println(" value=" + dept_code);}%>>
				</td>
			</tr>
			<tr>
				
				<td nowrap class="signText">科室名称：</td>
					<% String dept_name = request.getParameter("dept_name");%>
				<td class="normalText">
					<input type='text' name="dept_name"  class="textInputC" <%if(dept_name != null){ out.println(" value=" + dept_name);}%>>
				</td>
				<td colspan="4" align="right">
				<button class="pageBtn" name=""   onclick="find()"  >查询</button>
				<button class="pageBtn" name="hosDictsUnitinfoDictclinicdeptmap_import"
						onclick="importData(this,1)" >导入</button> 
				 <!--<img src="images/find.gif" class="mouse" onclick="find()" />
				<img src="images/transfers.gif" class="mouse" name="hosDictsUnitinfoDictclinicdeptmap_import"
						onclick="importData(this,1)" />--></td>
			</tr>
		</html:table>

		<br>

		<%
			BaseRO ro = (BaseRO)request.getAttribute("baseRO");
			TableMarge oper = new TableMarge(ro, "return find()");
			oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
			oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
			oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
			oper.addNeedButton("images/create.gif", "return create()");     //  添加
			oper.addNeedButton("images/auto.gif", "return auto()");     //  自动
		%>
		<html:title clazz='table'>自定义科室定义字典</html:title>
		<!-- 复杂信息 -->
		<html:table clazz="complex">
		<!-- 操作 -->
			<tr><td><%=oper%></td></tr>

			<!-- 结果集 -->
			<tr>
				<td>
					<html:table clazz="result">
						<html:tr clazz='label'>
							<td>选择</td>
							<td>自定义科室代码</td>
							<td>自定义科室名称</td>
							<td>科室代码</td>
							<td>科室名称</td>
						</html:tr>

						<%
							if (ro!=null) {
								String[][] result = ro.getTableResult();
								if ( result != null ){
									for (int i = 0; i < result.length; i++ ){
										String primaryKey = result[ i ][ 2 ];
										for (int j=0; j<result[i].length; j++) {
											if (result[i][j]!=null && result[i][j].equals(""))
												result[i][j]="&nbsp;";
										}
										String rowColor = "rowGray";
										if (i/2*2==i) rowColor = "rowWhite";
						%>
						<tr CLASS="<%=rowColor%>">
							<td><input type="checkbox" name='primaryKey' value="<%=primaryKey%>"></td>
							<td class="normalText"><%=result[i][0]%></td>
							<td class="normalText"><%=result[i][1]%></td>
							<td class="normalText"><%=result[i][2]%></td>
							<td class="normalText"><%=result[i][3]%></td>
						</tr>

					<%
								}
							}
						}
					%>
					</html:table>
				</td>
			</tr>

		<!-- 操作 -->
		</html:table>
		<input type=hidden name="subFunction"/>
		<input type=hidden name="needAlert" value='<%=request.getAttribute("needAlert")%>'/>
		<input type="hidden" name="initsub" value="sub"/>
		<Script>
			autoRun();
		</Script>
	</form>
	<div id="upload_file_div" style="position:absolute;display:none;">
	    <iframe src="uploadFileD.cmd" id="uploadFrame"></iframe>
	</div>
	<script Language="JavaScript" >
		_____VHFileCreatFromTimerss=window.setInterval(createVhFile,10);
		  function createVhFile(){
		    if(typeof(uploadFrame)=="undefined")
		      return ;
		    window.clearInterval(_____VHFileCreatFromTimerss);
		    fileUploader=new VhFileUploader(uploadFrame);
		  }
	</script>
</html:html>


