<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/paraDeptsCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-15 8:37 $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(101, request)%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
    //template.subFunction.value='create';
    if(isEmpty(template.dept_code))
    {
      alert('科室代码不能为空!');
      return;
    }
    if(isEmpty(template.app_para_code))
    {
      alert('分摊参数代码不能为空!');
      return;
    }
    if(isTooLong(template.dept_code,20))
    {
      alert('科室代码不能高于20个字符!');
      return;
    }
    if(isTooLong(template.app_para_code,2))
    {
      alert('分摊参数代码不能高于2个字符!');
      return;
    }
    switch(isDouble(template.value,16,2))
    {
      case 0 : alert('数量必须为数字型'); return;
      case 1 : alert('数量整数部分不能高于10个字符'); return;
      case 2 : alert('数量没有整数部分'); return;
      case 3 : alert('数量小数部分不能高于2个字符'); return;
    }
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
		show_wait();
    element.submit();
  }
</Script>

<form name="template" method="post" action="paraDepts.jspviewhigh">

  <!-- 返回信息栏 -->
  <table width="100%" border="0">
    <tr>
      <td>
        <table width="100%" border="0" >
        <%
          String message = (String)request.getAttribute("message");
          if (message!=null && !message.trim().equals(""))
          {
        %>
          <tr>
            <td class="errorText" align="center"><%=message%></td>
            <td align="center"><button class="pageBtn" onclick="history.go(-1);">返回</button> 
            <!--<img src="images/return.gif" onclick="history.go(-1);" style="cursor:hand">--></td>
          </tr>
        <%} else if (request.getParameter("subFunction").equals("create")) { %>
          <tr>
            <td class="successText" align="center">添加成功</td>
          </tr>
        <%}%>
        </table>
      <td>
    <tr>
  </table>

  <!-- 标题栏 -->
  <table width="100%" border="0">
    <tr>
      <td>
        <table width="100%" border="0" >
          <tr>
            <td  class="moduleTitle" nowrap="nowrap">科室基本情况表添加页面：</td>
          </tr>
        </table>
      <td>
    <tr>
  </table>



  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
     <tr>
        <td class="signText">科室</td>
       <td class="normalText">
          <select type="select" name="dept_code">
              <%String dept_code=request.getParameter("dept_code");%>
              <option value="" >--------------------</option>
              <%
                  String table_result[][] =
                      (String[][])request.getAttribute( "table_result" );
                  if(table_result!=null)
                  {
                      for( int i = 0; i < table_result.length; i++ )
                      {

              %>
              <option value="<%=table_result[i][0]%>" <%if(dept_code!=null&&!dept_code.equals("")&&dept_code.equals(table_result[i][0])) out.println(" selected ");%>><%=table_result[i][0]+":"+table_result[i][1]%></option>
              <%

                  }
              }
              %>
          </select>
      </td>
   <tr>
       <%
        String[][] table_result1 =
          (String[][])request.getAttribute("table_result1");

       %>
      <td class="signText" nowrap="nowrap">成本分摊科室类别：</td>
      <td class="normalText" nowrap="nowrap">
		    <select type="select" name="app_para_code"/>
           <option value="">-----------</option>
          <%
          if(table_result1!=null)
          {
          for(int i = 0; i < table_result1.length; i++)
          {
          %>

          <option value="<%=table_result1[i][0]%>">
              <%=table_result1[i][0]+":"+table_result1[i][1]%>
          </option>
          <%
          }
          }
          %>
      </select>
		  </td>
    </tr>
      <td class="signText" nowrap="nowrap">数量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="value" class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>   
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
</form>

<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>
