<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/paraDeptsFixAssetSave.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:57:47 $
	$Modtime: 03-08-13 9:46 $
	$Revision: 1.1 $
	$NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(101, request)%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save() {
   switch(isDouble(template.fix_asset_value,10,2))
    {
      case 0 : alert('数量必须为数字型'); return;
      case 1 : alert('数量整数部分不能高于10个字符'); return;
      case 2 : alert('数量没有整数部分'); return;
      case 3 : alert('数量小数部分不能高于2个字符'); return;
    }
		show_wait();
    template.submit();
    return true;
	}

  function back() {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
		show_wait();
  	template.submit();
  	return true;
  }

</Script>

    <form name="template" method="post" action="paraDeptsFixAsset.jspviewhigh">

      <!-- 信息提示栏 -->
       <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>

     <!--信息栏-->
       <%= new com.viewhigh.cbcs.base.mvc.view.MessageTitle("科室固定资产基本情况修改页面")%>
     <%
				  String[] result = (String[]) request.getAttribute("result");
            if(result!=null){
      %>
      <!-- 简单信息 -->
      <table  width="100%" cellspacing="2" border="0" >
        <tr>
       <tr>
          <td class="signText" nowrap="nowrap">科室名称：</td>
          <td width="75%" class="normalText" nowrap="nowrap">
            <input type=text value="<%=result[3]%>" disabled />
            <input type=hidden name="dept_code" value="<%=result[0]%>" class="textInputC" maxlength="20"/>
          </td>
        </tr>
        <td class="signText" nowrap="nowrap">固定资产折旧类型：</td>
          <td class="normalText" nowrap="nowrap">
            <input type=text value="<%=result[4]%>" disabled />
            <input type=hidden name="fix_asset_depr_type_code" value="<%=result[1]%>" class="textInputC" maxlength="10"/>

          </td>
        </tr>
          <td class="signText" nowrap="nowrap">固定资产金额：</td>
          <td class="normalText" nowrap="nowrap">
            <input type=text name="fix_asset_value" value="<%=result[2]%>" class="textInputC" maxlength="40"/>
          </td>
        </tr>
        <tr>
          <td colspan="2">
          <button class="pageBtn" onclick="return save();">保存</button>
          <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back();">返回</button>  
          <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
          <img src="images/reset.gif" class="mouse" onclick="return reset();" /> <img src="images/return.gif" class="mouse" onclick="return back();" />--></td>
        </tr>
      </table>
      <input type=hidden name="subFunction" value = "store"/>
      <input type=hidden name="dept_code" value="<%=result[0]%>">
      <input type=hidden name="fix_asset_depr_type_code" value="<%=result[1]%>">
      <%}%>
  	</form>


<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>
