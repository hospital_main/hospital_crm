<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>收入项目编码</th>
        <th nowrap='true'>收入项目名称</th>
        <th nowrap='true' style="display:none">标准收入项目编码</th>
        <th nowrap='true'>标准收入项目名称</th>
      </tr>
    </thead>
    <tbody>
    	<xsl:for-each select="/root/tbody/tr">
    		<xsl:variable name="pos" select="position()"/>
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
								<td align='left'><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=2">
								<td align='left'><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=4">
								<td>
								<input type="text" name='stIncomeCode' class='inputSelect' listRows="20" load='cbcsIncomesDataBasic' defaultValue="true"  extent='175'>	
									<xsl:attribute name="id">TRTR<xsl:value-of select="$pos"/>Id<xsl:value-of select="position()"/></xsl:attribute>
									<xsl:attribute name="initValue"><xsl:value-of select="../td[3]"/></xsl:attribute>
									<xsl:attribute name="incomeSubjCode"><xsl:value-of select="../pk/incomeSubjCode"/></xsl:attribute>
									<xsl:attribute name="income_subj_code"><xsl:value-of select="../td[1]"/></xsl:attribute>
									<xsl:attribute name="income_subj_name"><xsl:value-of select="../td[2]"/></xsl:attribute>
									<xsl:attribute name="standard_income_subj_code"><xsl:value-of select="../td[3]"/></xsl:attribute>
									<xsl:attribute name="standard_income_subj_name"><xsl:value-of select="../td[4]"/></xsl:attribute>
									<xsl:attribute name="trIndex"><xsl:value-of select="$pos"/></xsl:attribute>
									<xsl:attribute name="tdIndex"><xsl:value-of select="position()"/></xsl:attribute>
								</input>	
								</td>
						</xsl:when>
          	</xsl:choose>
  			  </xsl:for-each>
  			  </tr>
   		</xsl:for-each>       
    </tbody>
  </xsl:template>
</xsl:stylesheet>

