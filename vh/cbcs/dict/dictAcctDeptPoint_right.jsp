<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictAcctDeptPoint_right.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
  String[][] result = (String[][])request.getAttribute("table_result");
%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script language="javascript">
  function save() {
		
    for(i = 0; i < template.parameters.length; i++) {
      template.parameters.value =
				template.parameters[i].value +
        '|^|' + document.all['finance' + i].value + '|^|' +
        document.all['his' + i].value;
      template.parameters[i].checked = true;
    }
    show_wait();
    template.submit();
    return true;
  }

  function restore() {
    template.subFunction.value = 'init';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="dictAcctDeptPoint.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>科室代码映射</html:title>
<br>

  <%if (result != null) {%>
  <table>
    <tr>
      <td>
      <button class="pageBtn" onclick="return save()">保存</button> 
      <!--<img src="images/save.gif" class='mouse' onclick="return save()">--></td>
      <td><button class="pageBtn" onclick="return restore();">重置</button> 
      <!--<img src="images/reset.gif" class="mouse" onclick="return restore();">--></td>
    </tr>
  </table>
  <%}%>
  <br>
  <html:table clazz="result">
		  <html:tr clazz='label'>
      <td class="resultLabel">科室代码</td>
      <td class="resultLabel">科室名称</td>
      <td class="resultLabel">财务科室代码</td>
      <td class="resultLabel">His科室代码</td>
    </html:tr>

    <%
      if (result != null) {

      for (int i = 0; i < result.length; i++) {
    %>

    <tr>
      <input type="checkbox" name="parameters" value="<%=result[i][0]%>"
        style="display:none">
      <td class="normalText"><%=result[i][0]%></td>
      <td class="normalText"><%=result[i][1]%></td>
      <td>
        <input class="normalText" type="text" id="<%="finance" + i%>" name="<%="finance" + i%>"
          <%if (!result[i][5].equals("")) {%> value="<%=result[i][5]%>" <%}%>>
      </td>
      <td>
        <input class="normalText" type="text" id="<%="his" + i%>" name="<%="his" + i%>"
          <%if (!result[i][6].equals("")) {%> value="<%=result[i][6]%>" <%}%>>
      </td>
    </tr>
      <%}%>
    <%}%>
  </html:table>



  <input type="hidden" name="subFunction" value="config">
</form>
</html:html>
