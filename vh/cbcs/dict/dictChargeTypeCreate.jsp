<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictChargeTypeCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 23:27 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%@ page import="java.util.ArrayList" %>
<%
    ArrayList user_author = (ArrayList)session.getAttribute("user_author");
    user_author.add( "cbcsDictChargeTypeModel_create" );

%>
<Script Language="JavaScript">
    function create() {
        template.subFunction.value='create';
				show_wait();
        template.submit();
        return true;
    }

    // 返回
    function back( element ){
       for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
       template.subFunction.value='findAll';
			 show_wait();
       element.submit();
    }
</Script>

<html:html clazz="main">  <form name="template" method="post" action="dictChargeType.jspviewhigh">

    <table width="97%" border="0" cellpadding="0" cellspacing="0">

        <tr bgcolor="#FFFFFF">
            <td valign="top"><img src="../../images/left.gif" width="5" height="5"></td>
            <td align="right" valign="top"><img src="../../images/right.gif" width="5" height="5"></td>
        </tr>

        <tr bgcolor="#FFFFFF">
            <td colspan="2">
                <table width="60%" border="0" align="center" cellspacing="1" bgcolor="#999999">
                    <tr bgcolor="#FFFFFF" class="a12" align="center">
                            <td align="right" bgcolor="#CCCCCC">费别代码：</td>
                            <td align="left" bgcolor="#CCCCCC">
                              <input type="text" name="charge_type_code"/>
                            </td>
                    </tr>
                    <tr bgcolor="#FFFFFF" class="a12" align="center">
                        <td align="right" bgcolor="#CCCCCC">费别名称：</td>
                        <td align="left" bgcolor="#CCCCCC">
                          <input type="text" name="charge_type_name" />
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF" class="a12" align="center">
                        <td align="right" bgcolor="#CCCCCC">是否停用：</td>
                        <td align="left" bgcolor="#CCCCCC">
                          是<input type="radio" name="stop_mark" value="Y" />
                          否<input type="radio" name="stop_mark" value="N" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr bgcolor="#FFFFFF" class="a12" align="center">
            <td height="60" valign="middle" colspan="6">
                <input type="button" value="增 加" onClick="create();"> &nbsp;&nbsp;
                <input type="reset"  value="重 置" >&nbsp;&nbsp;
                <input type="button" value="返 回" onclick="return back( template );" >
                <input type="hidden" name="subFunction" >
            </td>
        </tr>

    </table>
</form>
</html:html>
