<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/costSubjStore.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime:  $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(102, request)%>
<%
  String[] result = (String[])request.getAttribute("result");
%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
<%
  String message = request.getAttribute("message")==null?"":(String)request.getAttribute("message");
  if(message.equals("添加成功"))
    out.println("parent.frames[0].spring.click()");
%>
  function store()
  {
    if(isEmpty(template.cost_subj_name))
    {
      alert('成本项目名称不能为空!');
      return;
    }
    if(isEmpty(template.treat_or_med)) {
      alert('医疗/药品标识不能为空!');
      return;
    }
    if(isTooLong(template.cost_subj_name,40)) {
      alert('成本项目名称不能大于40个字符!');
      return;
    }
    template.subFunction.value='store';
		show_wait();
    template.submit();
    return true;
  }
  function create() {
    if(template.subj_last_level.value=='Y'){
      alert('<%if(result!=null) out.print(result[1]);%>'+'已经为末级项目')
      return
    }
    template.subFunction.value='preparedCreate';
		show_wait();
    template.submit();
    return true;
  }
</Script>
<%
  String flag = (String)request.getParameter("flag");
%>
<form name="template" method="post" action="dictcostsubj.jspviewhigh">
  <!-- 信息提示栏 -->
<%  if(flag==null){%>
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>

  <%}
    if ( result != null ) {
  %>

  <!-- 简单信息 -->
  <table id ='table1' width="100%" cellspacing="2" border="0" style='visibility:visible' >
    <tr>
      <td class="signText" nowrap="nowrap">成本项目代码：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0]%>" disabled readonly/>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">成本项目名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="cost_subj_name" value="<%=result[1]%>" class="textInputC">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">上级代码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="supp_item_code" disabled readonly value="<%=result[2]%>"/>
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">医疗/药品标识：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("init_treat_or_med"), "treat_or_med", result[3], false, true)%>
		  </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">是否末级：</td>
      <td class="normalText" nowrap="nowrap">
        是<input type="radio" name="last_level" value="Y" <% if(result[5].equals("Y")){out.println(" checked ");}%>/>
        否<input type="radio" name="last_level" value="N" <% if(result[5].equals("N")){out.println(" checked ");}%>/>
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">是否停用：</td>
      <td class="normalText" nowrap="nowrap">
        是<input type="radio" name="stop_mark" value="Y" <% if(result[4].equals("Y")){out.println(" checked ");}%>/>
        否<input type="radio" name="stop_mark" value="N" <% if(result[4].equals("N")){out.println(" checked ");}%>/>
      </td>
    </tr>

    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return store();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <!--<img src="images/save.gif" class="mouse" onclick="return store();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/create.gif" class="mouse" onclick="return create();" /> --> </td>
    </tr>
  </table>
  <input type="hidden" name="supp_item_code" value = "<%=result[2]%>"/>
  <%}%>
  <input type="hidden" name="subFunction" value = "store"/>
  <input type="hidden" name="cost_subj_code" <%if(result==null) out.print("value=\'TOP\'"); else out.print("value=\'"+result[0]+"\'");%>>
  <input type="hidden" name="subj_last_level" <%if(result==null) out.print("value=\'0\'"); else out.print("value=\'"+result[5]+"\'");%>>
  <input type="hidden" name="subj_grade" <%if(result==null) out.print("value=\'0\'"); else out.print("value=\'"+result[6]+"\'");%>>
  <td colspan="2"><img id='but' src="images/create.gif" class="mouse" onclick="return create();"  style='display:none;' /> </td>
</form>
<Script Language="JavaScript">
<%
  if(flag!=null && flag.equals(""))
    out.println("template.but.style.display=\'block\'");
%>
</Script>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(102)%>
