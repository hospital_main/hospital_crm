<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictChargeDetailCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-09-02 11:06 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
	String[][] chargeKinds = (String[][])request.getAttribute("charge_detail_kind");
  String[][] chargeKindsSelect = new String[chargeKinds.length][2];
	for (int i = 0; i < chargeKinds.length; i++) {
    chargeKindsSelect[i][0] = chargeKinds[i][0];
    chargeKindsSelect[i][1] = chargeKinds[i][1];
  }
%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
 function getCode(str){
	  var _str ;
	  _str = str.replace(/(^\s+)|\s+$/g, "") ;
	  return _str.replace(/[\+%&]/g, "");
}
  function create()
  {
  template.charge_detail_code.value = getCode(template.charge_detail_code.value);
    //template.subFunction.value='create';
     if(isEmpty(template.charge_kind_code))
    {
      alert('医疗项目类别不能为空!');
      return;
    }
    if(isEmpty(template.charge_detail_code))
    {
      alert('医疗项目代码不能为空!');
      return;
    }
    if(isEmpty(template.charge_detail_name))
    {
      alert('医疗项目名称不能为空!');
      return;
    }
    if(!isRadioChecked(template.stop_mark))
    {
      alert('停用标志必须选!');
      return;
    }
    if(isTooLong(template.charge_detail_code,20))
    {
      alert('医疗项目代码不能大于20个字符!');
      return;
    }
    if(isTooLong(template.charge_detail_name,40))
    {
      alert('医疗项目名称不能大于40个字符!');
      return;
    }
    if(isTooLong(template.spell,10))
    {
      alert('拼音码不能大于10个字符!');
      return;
    }
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
		show_wait();
    element.submit();
  }
  
  function makeSpell ( )
  {
		template.subFunction.value='create_getSpellCode';
		show_wait();
    template.submit();
  }
  
  function myReset()
  {
  		template.charge_kind_code.selectedIndex=0;
  		template.charge_detail_code.value='';
  		template.charge_detail_name.value='';
  		template.stop_mark[0].checked=false;
  		template.stop_mark[1].checked=true;
  		template.spell.value='';
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="dictChargeDetail.jspviewhigh">
	<!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>医疗项目添加页面</html:title>

  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">医疗项目代码：</td>
      <td width="75%" class="normalText" nowrap="nowrap"><input type=text name="charge_detail_code" class="textInputC" value="<%=request.getParameter("charge_detail_code")==null? "":request.getParameter("charge_detail_code")%>"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">医疗项目名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="charge_detail_name" class="textInputC" onblur="makeSpell()" value="<%=request.getParameter("charge_detail_name")==null? "":request.getParameter("charge_detail_name")%>"/></td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">医疗项目类别：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <%=new SingleSelect(chargeKindsSelect, "charge_kind_code", request.getParameter("charge_kind_code")==null? "":request.getParameter("charge_kind_code"), true, true)%>
      </td>
      
    </tr>
       <td class="signText" nowrap="nowrap">拼音码：</td>
        <td width="75%" class="normalText" nowrap="nowrap"><input type=text name="spell" class="textInputC" value="<%=request.getAttribute("spell")==null? "":request.getAttribute("spell")%>"/></td>
        </td>
    <tr>
    
    </tr>
    
    <tr>
      <td class="signText" nowrap="nowrap">停用标志：</td>
      <td class="normalText" nowrap="nowrap">
		    是<input type="radio" style= "border:0pt; " name="stop_mark" value="Y" <%if (request.getParameter("stop_mark")!=null&&request.getParameter("stop_mark").trim().equals("Y")) out.print("checked='checked'");%>/>
        否<input type="radio" style= "border:0pt; " name="stop_mark" value="N" <%if (request.getParameter("stop_mark")!=null&&request.getParameter("stop_mark").trim().equals("N")) {out.print("checked='checked'");}else if(request.getParameter("stop_mark")==null||request.getParameter("stop_mark").trim().equals("")){%>checked <%}%> />
		  </td>
    </tr>

    <tr>
      <td colspan="2" align="left">　　　　　　　　　　
      <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="myReset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>   
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </html:table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>
