<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictCostSubjCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-29 11:15 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
    function create()
    {
    	template.cost_subj_code.value = getCode(template.cost_subj_code.value);
    	
	    if(isEmpty(template.cost_subj_code))
      {
        alert('成本项目代码不能为空!');
        return;
      }
      if(isEmpty(template.cost_subj_name))
	    {
        alert('成本项目名称不能为空!');
        return;
	    }
      if(isEmpty(template.supp_item_code))
      {
        alert('上级代码不能为空!');
        return;
	    }
      if(template.supp_item_code.value==template.cost_subj_code.value){
        alert('上级代码不能与成本项目代码相同');
        return;}
      if(isEmpty(template.supp_item_code))
      {
        alert('上级代码不能为空!');
        return;
	    }
      if(!isRadioChecked(template.stop_mark))
	    {
        alert('停用标志必须选!');
        return;
	    }
      if(isTooLong(template.cost_subj_code,20))
      {
        alert('成本项目代码不能大于20个字符!');
        return;
      }
      if(isTooLong(template.cost_subj_name,40))
	    {
        alert('成本项目名称不能大于40个字符!');
        return;
	    }
      if(isTooLong(template.supp_item_code,20))
      {
        alert('上级代码不能大于20个字符!');
        return;
      }
     if(isTooLong(template.spell,10))
     {
      alert('拼音码不能大于10个字符!');
      return;
     }
     /*if(isEmpty(template.pk_cost_subj))
     {
       alert('成本项目分类不能为空!');
        return;
     }*/
      //template.subFunction.value='create';
			show_wait();
      template.submit();
      return true;
    }

    // 返回
    function back( element )
    { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
      template.subFunction.value='findAll';
      template.initsub.value="sub";
			show_wait();
      element.submit();
    }
    
  function makeSpell ( )
  {
		template.subFunction.value='create_getSpellCode';
		show_wait();
    template.submit();
  }
  
  function myReset()
  {
  	template.cost_subj_code.value='';
  	template.cost_subj_name.value='';
  	template.supp_item_code.value='';
  	template.treat_or_med.selectedIndex=0;
  	template.spell.value='';
  	template.stop_mark[0].checked=false;
  	template.stop_mark[1].checked=true;
  }
      
</Script>

<html:html clazz="main">  <form name="template" method="post" action="costsubj.jspviewhigh">
	<!-- 信息提示栏 -->
 <html:message/>

  <!-- 标题栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("成本项目添加页面")%>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" class="simpleQuery" >
    <tr>
      <td class="signText" nowrap="nowrap">成本项目代码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="cost_subj_code"  value="<%=request.getParameter("cost_subj_code")%>" class="textInputC" maxlength="20"/></td>
      <td class="signText" nowrap="nowrap">成本项目名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="cost_subj_name" value="<%=request.getParameter("cost_subj_name")%>" class="textInputC" maxlength="40" onblur="makeSpell()"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">上级代码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="supp_item_code" value="<%=request.getParameter("supp_item_code")==null? "":request.getParameter("supp_item_code")%>" class="textInputC"  maxlength="30"/></td>
      <td class="signText" nowrap="nowrap">医疗/药品标识：</td>
      <td class="normalText" nowrap="nowrap">
		    <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("init_treat_or_med"), "treat_or_med", request.getParameter("treat_or_med")==null? "":request.getParameter("treat_or_med"), false, true)%>
		  </td>
    </tr>
    	
	 <tr>
      <td class="signText" nowrap="nowrap">拼音码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="spell" value="<%=request.getAttribute("spell")==null? "":request.getAttribute("spell")%>" class="textInputC"  maxlength="10"/></td>
      <td class="signText" nowrap="nowrap">是否停用：</td>
      <td class="normalText" nowrap="nowrap">
		    是<input type="radio" name="stop_mark" value="Y" <%if (request.getParameter("stop_mark")!=null&&request.getParameter("stop_mark").trim().equals("Y")) out.print("checked='checked'");%>/>
        否<input type="radio" name="stop_mark" value="N" <%if (request.getParameter("stop_mark")!=null&&request.getParameter("stop_mark").trim().equals("N")) {out.print("checked='checked'");}else if(request.getParameter("stop_mark")==null||request.getParameter("stop_mark").trim().equals("")){%>checked <%}%>/>
		  </td>
	</tr>
	<!--tr>
		  <td class="signText" nowrap="nowrap">成本项目分类：</td>
		  <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea0" name="pk_cost_subj" value="<%=request.getParameter("cost_subj_code")%>"  AdjustVal="97" previousObj="costNo"  codeCol='cost_subj_code' indexCodeSequence="pk_cost_subj|cost_subj_code|cost_subj_name|spell" textCol="cost_subj_name"  width="140" top="12" left="97" Lheight="5" xmlSource="dic/st_standard_cost_subj.xml" init="1"/>
      </td>
	</tr-->

    <tr>
      <td colspan="4" align="center"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="myReset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>
