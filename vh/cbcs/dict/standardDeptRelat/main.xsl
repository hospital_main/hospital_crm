<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>����</th>
        <th nowrap='true'>��׼��������</th>
        <th nowrap='true' style="display:none">���ұ���</th>
        <th nowrap='true'>��������</th>
      </tr>
    </thead>
    <tbody>
    	<xsl:for-each select="/root/tbody/tr">
    		<xsl:variable name="pos" select="position()"/>
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
								<td align='left'><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=2">
								<td align='left'><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=4">
								<td>
								<input type="text" name='stDeptCode' class='inputSelect' listRows="20" load='dict_acct_standard_select' defaultValue="true"  extent='175'>	
									<xsl:attribute name="id">TRTR<xsl:value-of select="$pos"/>Id<xsl:value-of select="position()"/></xsl:attribute>
									<xsl:attribute name="initValue"><xsl:value-of select="../td[3]"/></xsl:attribute>
									<xsl:attribute name="year"><xsl:value-of select="../pk/year"/></xsl:attribute>
									<xsl:attribute name="deptCode"><xsl:value-of select="../pk/deptCode"/></xsl:attribute>
									<xsl:attribute name="dept_code"><xsl:value-of select="../td[1]"/></xsl:attribute>
									<xsl:attribute name="dept_name"><xsl:value-of select="../td[2]"/></xsl:attribute>
									<xsl:attribute name="st_dept_code"><xsl:value-of select="../td[3]"/></xsl:attribute>
									<xsl:attribute name="st_dept_name"><xsl:value-of select="../td[4]"/></xsl:attribute>
									<xsl:attribute name="trIndex"><xsl:value-of select="$pos"/></xsl:attribute>
									<xsl:attribute name="tdIndex"><xsl:value-of select="position()"/></xsl:attribute>
								</input>	
								</td>
						</xsl:when>
          	</xsl:choose>
  			  </xsl:for-each>
  			  </tr>
   		</xsl:for-each>       
    </tbody>
  </xsl:template>
</xsl:stylesheet>

