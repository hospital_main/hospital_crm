<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/dictAnlyIndxMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:57:47 $
  $Modtime: 03-08-29 9:38 $
  $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
	function create() {
		template.subFunction.value='preparedCreate';
		show_wait();
    template.submit();
		return true;
	}

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
  		if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
  		  flag = true;
  	}

    if( flag!=false) {
    	if (confirm('是否删除')) {
	      template.subFunction.value='remove';
				show_wait();
	      template.submit();
	      return true;
	    } else
	    	return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectedAll(){
    for (var i=0; i<template.elements.length; i++) {
    	if (template.elements[i].name=='primaryKey')
    		template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
		show_wait();
    template.submit();return true;
  }
</Script>

<html:html clazz="main">
    <form name="template" method="post" action="dictAnlyIndx.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>分析指标主页面</html:title>

      <!-- 简单信息 -->
      <html:table clazz="simple">
        <tr>
      <td nowrap class="signText">分析指标代码</td>
      <% String anly_indx_code = request.getParameter("anly_indx_code");%>
      <td><input type=text name="anly_indx_code" class="textInputC" <%if(anly_indx_code != null){ out.println(" value=" + anly_indx_code);}%>></td>
       <td nowrap class="signText">分析指标名称</td>
      <% String anly_indx_name = request.getParameter("anly_indx_name");%>
      <td><input type=text name="anly_indx_name" class="textInputC" <%if(anly_indx_name != null){ out.println(" value=" + anly_indx_name);}%>></td>
      <td><button class="pageBtn" onclick="return find()" >查询</button> </td>
        </tr>
      </html:table>

      <br>

      <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro!=null) {
	    TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectedAll()");   // 全选
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
      oper.addNeedButton("images/create.gif", "return create()");     //  添加
      %>

  <html:title clazz='table'>分析指标</html:title>
  <!-- 复杂信息 -->
  <html:table clazz="complex">
        <!-- 操作 -->
        <tr><td><%=oper%></td></tr>

        <!-- 结果集 -->
        <tr>
          <td>
            <html:table clazz="result">
		          <html:tr clazz='label'>
                <td class="resultLabel">选择</td>
                <td class="resultLabel">分析指标代码</td>
	              <td class="resultLabel">分析指标名称</td>
              </html:tr>

              <%
              	String[][] result = ro.getTableResult();
              	if (result!=null) {
                  for (int i = 0; i < result.length; i++ )
                  {
                     String primaryKey = result[ i ][ 0 ];
                      for (int j=0; j<result[i].length; j++)
                       {
                         if (result[i][j]!=null&&result[i][j].trim().equals(""))
                           {
                             result[i][j]="&nbsp;";
                           }
                     }


                      String rowColor = "rowGray";
                      if (i/2*2==i) rowColor = "rowWhite";
              %>
              <tr CLASS="<%=rowColor%>">
                  <td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
                  <td class="normalText"><a href="dictAnlyIndx.jspviewhigh?subFunction=load&primaryKey=<%=primaryKey%>"><%=result[i][0]%></a></td>
                  <td class="normalText"><%=result[i][1]%></td>
              </tr>
              <%
                    }
                  }

              %>
            </html:table>
          </td>
        </tr>

        <!-- 操作 -->
      </html:table>
<%
     }
%>
      <input type=hidden name="subFunction"/>
  	</form>
</html:html>
