<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/acctDeptCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<html:html clazz="main">


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
    //template.subFunction.value='create';
    if(isEmpty(template.dept_code))
    {
      alert('科室代码不能为空!');
      return;
    }
    if(isEmpty(template.dept_name))
    {
      alert('科室名称不能为空!');
      return;
    }
    if(!isRadioChecked(template.stop_mark))
    {
      alert('停用标志必须选!');
      return;
    }
    if(!isRadioChecked(template.cost_app_ind))
    {
      alert('成本分摊标志必须选!');
      return;
    }
    if (isEmpty(template.app_level)) {
      alert('分摊级别不能为空!');
      return;
    }
    if(!isRadioChecked(template.dept_last_level))
    {
      alert('末级标志必须选!');
      return;
    }
    if(isTooLong(template.dept_code,20))
    {
      alert('科室代码不能高于20个字符!');
      return;
    }
    if(isTooLong(template.dept_name,40))
    {
      alert('科室名称不能高于40个字符!');
      return;
    }
    if (!isNumber(template.app_level)) {
      alert('分摊级别必须为数字类型!');
      return;
    }
     if(isTooLong(template.app_level,1))
    {
      alert('分摊级别不能高于1个字符!');
      return;
    }
    parent.frames[0].dept_name.value = template.dept_name.value
    parent.frames[0].dept_code.value = template.dept_code.value
    if(template.dept_last_level[0].checked)
      parent.frames[0].dept_last_level.value = template.dept_last_level[0].value
    else parent.frames[0].dept_last_level.value = template.dept_last_level[1].value
		show_wait();    
		template.submit();
    return true;
  }
  function back(){
    history.back()
  }
</Script>

<form name="template" method="post" action="dictacctdept.jspviewhigh">
  <!-- 信息提示栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>
  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">科室代码：</td>
      <td width="75%" class="normalText" nowrap="nowrap"><input type=text name="dept_code" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">科室名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="dept_name" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">上级科室代码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="supper_dept" readonly disabled class="textInputC" <%if(request.getParameter("dept_code")!=null) out.print("value='"+request.getParameter("dept_code")+"'");else out.print("value='TOP'");%>/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">科室类别：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(request.getAttribute("init_depttype"), "app_dept_type_code", null, false, true)%>
		  </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">分配类别：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(request.getAttribute("subj_kind"), "subj_kind_code", null, false, true)%>
		  </td>
    </tr>

		<tr>
      <td class="signText" nowrap="nowrap">科室级别：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="dept_grade" readonly disabled class="textInputC" <%if(request.getParameter("dept_grade")!=null) out.print("value='"+(Integer.parseInt(request.getParameter("dept_grade"))+1)+"'"); else out.print("value='1'");%>/></td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">参与成本分摊标志：</td>
      <td class="normalText" nowrap="nowrap">
		    是<input type="radio" name="cost_app_ind" value="Y"/>
        否<input type="radio" name="cost_app_ind" value="N"/>
		  </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">分摊级别：</td>
      <td class="normalText" nowrap="nowrap">
         <input type=text name="app_level" />
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">末级标志：</td>
      <td class="normalText" nowrap="nowrap">
         是<input type="radio" name="dept_last_level" value="Y" />
         否<input type="radio" name="dept_last_level" value="N" />
      </td>
    </tr>

     <tr>
      <td class="signText" nowrap="nowrap">门诊住院标识：</td>
      <td class="normalText" nowrap="nowrap">
        <%=new SingleSelect(request.getAttribute("init_outorin"), "out_or_in", null, false, false)%>
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">停用标志：</td>
      <td class="normalText" nowrap="nowrap">
		     是<input type="radio" name="stop_mark" value="Y"/>
         否<input type="radio" name="stop_mark" value="N" checked />
		  </td>
    </tr>
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back();">返回</button>  
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back();" />--></td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value="create"/>
  <input type="hidden" name="supper_dept" <%if(request.getParameter("dept_code")!=null) out.print("value='"+request.getParameter("dept_code")+"'");else out.print("value='TOP'");%>>
  <input type="hidden" name="dept_grade" <%if(request.getParameter("dept_grade")!=null) out.print("value='"+(Integer.parseInt(request.getParameter("dept_grade"))+1)+"'"); else out.print("value='1'");%>>
</form>
</html:html>