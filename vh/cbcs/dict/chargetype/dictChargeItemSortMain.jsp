<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/chargetype/dictChargeItemSortMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    template.submit();return true;
  }
  
  function importData(obj,v){
		   		
				var fileName=fileUploader.choose(1);
				if(fileName.length<4&&fileName.indexOf(".")<0)
					return false;
				var fileType=fileName.substr(fileName.lastIndexOf(".")+1);
				fileUploader.submit(function(m,f){
						if( m != ''){
							alert(m);
							return false;
						}
						for(var k in f){
							fileType+="="+k;
							break;
						}
						var msg = "" ;
						var queryParams = "<comp_file>"+fileType+"</comp_file>" ;
						confirmImportStyleDialog(function(v){
							if(v==0)
								return;
						        window.xmlhttp.post("hosDictsUnitinfoBdictchargedetailkind_import",queryParams+"<q_flag>"+v+"</q_flag>");
						        
						         var responseText = window.xmlhttp._object.responseText ;
						          msg = window.doMsg(responseText,"show")
							  if(msg==true) {
								template.subFunction.value='findAll';
								show_wait();
						    		template.submit();
						    		return true;
						 	 }
						  });
					},obj
				)	
			}

</Script>
<%String[][] income_type={{"T","医疗收入"},{"M","药品收入"}};%>
<html:html clazz="main">
<form name="template" method="post" action="chargeitemsort.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>收费类别主页面</html:title>


  <!-- \u7B80\u5355\u4FE1\u606F -->
  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText">收费类别编码：</td>
      <% String income_subj_code = request.getParameter("income_subj_code");%>
      <td><input type=text name="income_subj_code" style="width:140" class="textInputC" <%if(income_subj_code!= null){ out.println(" value=" + income_subj_code);}%>></td>
      <td nowrap class="signText">收费类别名称：</td>
      <% String income_subj_name = request.getParameter("income_subj_name");%>
      <td><input type=text name="income_subj_name" class="textInputC" style="width:140"  <%if(income_subj_name!= null){ out.println(" value=" + income_subj_name);}%>></td>
      <td class="signText" nowrap="nowrap">收入类型：</td>
      <td nowrap="nowrap">
        <%=new SingleSelect(income_type, "income_type", request.getParameter("income_type"), false, false)%>
      </td>
    </tr>
    <tr>
      
      <td class="signText">拼音码：</td>
      <td class="normalText"><input type=text name="spell" class="textInputC" style="width:140" value="<%if (request.getParameter("spell")!=null) out.print(request.getParameter("spell"));%>"></td>
      <td colspan="4" align="right">
      <button class="pageBtn" name="" onclick="template.subFunction.value='findAll';template.submit();return true;" >查询</button>
      	<button class="pageBtn" name="hosDictsUnitinfoBdictchargedetailkind_import"
						onclick="importData(this,1)"  >导入</button>  
      <!--<img src="images/find.gif" class="mouse" onclick="template.subFunction.value='findAll';template.submit();return true;" />
       <td> <img src="images/transfers.gif" class="mouse" name="hosDictsUnitinfoBdictchargedetailkind_import"
						onclick="importData(this,1)" />--></td>
    </tr>
  </html:table>

  <br>

   <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if (ro != null) {
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // \u5168\u9009
      oper.addOptionButton("images/reset.gif", "return reset()");     //  \u91CD\u7F6E
      oper.addOptionButton("images/remove.gif", "return remove()");   //  \u5220\u9664
      oper.addNeedButton("images/create.gif", "return create()");     //  \u6DFB\u52A0
  %>

  <html:title clazz='table'>收费类别</html:title>

  <html:table clazz="complex">

    <tr><td><%=oper%></td></tr>


  <tr>
    <td>
      <html:table clazz="result">
		    <html:tr clazz='label'>
          <td class="resultLabel">选择</td>
          <td class="resultLabel">收费类别编码</td>
          <td class="resultLabel">收费类别名称</td>
          <td class="resultLabel">收入项目名称</td>
           <td class="resultLabel">收入类型</td>
           <td class="resultLabel">拼音码</td>
        </html:tr>

        <%
          String[][] result = ro.getTableResult();
          if ( result != null ) {
            for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null&&result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            String rowColor = "rowGray";
            if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
          <td class="normalText"><a href="chargeitemsort.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>&_current_page=<%=request.getParameter("_current_page")==null? "0":request.getParameter("_current_page")%>"><%=primaryKey%></a></td>
          <td class="normalText"><%=result[ i ][ 1 ]%></td>
          <td class="normalText"><%=result[ i ][ 2 ]%></td>
          <td class="normalText"><%=result[ i ][ 3 ]%></td>
          <td class="normalText"><%=result[ i ][ 4 ]%></td>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>

  <!-- \u64CD\u4F5C -->
  </html:table>
	<%}%>
  <input type=hidden name="subFunction"/>
  <input type="hidden" name="initsub" value="sub"/>
</form>
<div id="upload_file_div" style="position:absolute;display:none;">
	    <iframe src="uploadFileD.cmd" id="uploadFrame"></iframe>
	</div>
	<script Language="JavaScript" >
		_____VHFileCreatFromTimerss=window.setInterval(createVhFile,10);
		  function createVhFile(){
		    if(typeof(uploadFrame)=="undefined")
		      return ;
		    window.clearInterval(_____VHFileCreatFromTimerss);
		    fileUploader=new VhFileUploader(uploadFrame);
		  }
	</script>
</html:html>
