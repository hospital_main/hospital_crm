<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/chargetype/dictChargeItemSortSave.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
                com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    //template.subFunction.value='save';
    if(isEmpty(template.charge_kind_name))
    {
      alert('请输入收费类名称');
      return;
    }
    if(isEmpty(template.income_subj_code))
    {
      alert('请选择收入项目名称');
      return;
    }
    template.submit();
    return true;
  }

  // \u8FD4\u56DE
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    template._current_page.value=<%=request.getParameter("_current_page")%>
    element.submit();
  }
  
  function myReset(){
  		template.charge_kind_code.value='';
  		template.charge_kind_name.value='';
  		template.spell.value='';
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="chargeitemsort.jspviewhigh">
<!-- \u4FE1\u606F\u63D0\u793A\u680F -->
  <html:message/>

  <!-- \u6807\u9898\u680F -->
  <html:title clazz='module'>收费类别修改页面</html:title>

 <%String[][] income_type={{"T","医疗收入"},{"M","药品收入"}};%>
  <%
    String[] result = (String[])request.getAttribute( "result" );
  
    if ( result != null )
    {
  %>

  <!-- \u7B80\u5355\u4FE1\u606F -->
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">收费类编码：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="charge_kind_code" value="<%=result[0]%>" disabled />
      </td>
      <td class="signText" nowrap="nowrap">收费类名称：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="charge_kind_name" value="<%=result[1]%>" class="textInputC" onchange="getSpellCode(this);">
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">收入项目名称：</td>
     <td class="normalText">
        <%=new SingleSelect(request.getAttribute("init_income_subj"), "income_subj_code", result[2], true, false)%>
      </td>
      <td class="signText" nowrap="nowrap">收入类型：</td>
      <td nowrap="nowrap">
        <%=new SingleSelect(income_type, "income_type", result[3], false, true)%>
      </td>
    </tr>
		<tr>
      <td class="signText" nowrap="nowrap">拼音码：</td>
      <td class="normalText" nowrap="nowrap">
         <input type=text name="spell"  value="<%=result[4]%>"/>
      </td>
	  <td></td>
	  <td></td>
    </tr>
    <tr>
      <td colspan="4" align="center">
      <button class="pageBtn" onclick="return savea();">保存</button>
      <button class="pageBtn" onclick="return myReset();" >重置</button> <!--reset()-->
      <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
    
      <tr height="500">
    <td/>
    </t
   </html:table>
  <input type="hidden" name="subFunction" value = "save"/>
  <input type="hidden" name="initsub" value="sub"/>
  <input type="hidden" name="charge_kind_code" value="<%=result[0]%>">
  <input type="hidden" name="_current_page" value = "<%=request.getParameter("_current_page")%>" />
  <%}%>
</form>

</html:html>
<script>
function getSpellCode(source){
var name = source.value ;
name = "<a>"+name+"</a>"
var p=getValuePairBySql("ccbsGetSpellCode_query",name);
	var ec,en;
	if(p!=null){
		ec=p[0];
		en=p[1];	
	}else{
		ec="";
		en="";
	}
document.getElementById("spell").value = ec;
  
}
function savea(){
	var income_subj_code1 = document.getElementsByName("income_subj_code")[0].value;
  if(income_subj_code1==''){
  		 alert("收入项目名称不能为空！") ;
  		 return;	
  }
   save();
	}

</script>

