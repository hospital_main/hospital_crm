<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dict/chargetype/dictChargeItemSortCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
                com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

function getCode(str){
	  var _str ;
	  _str = str.replace(/(^\s+)|\s+$/g, "") ;
	  return _str.replace(/[\+%&#]/g, "");
}
 function create()
 {
  template.charge_kind_code.value = getCode(template.charge_kind_code.value);
    //template.subFunction.value='create';
    if(isEmpty(template.charge_kind_code))
    {
        alert('请输入收费类编码');
        return;
    }

    if(isEmpty(template.charge_kind_name))
    {
        alert('请输入收费类名称');
        return;
    }
//    if(!isRadioChecked(template.income_subj_code.value))
//    {
//        alert('请选择收入项目');
//        return;
//    }
    if(isTooLong(template.charge_kind_code,20))
    {
        alert('收费类编码不要超过20位');
        return;
    }
    if(isTooLong(template.charge_kind_name,40))
    {
        alert('收费类名称太长了');
        return;
    }
    if(template.spell.value==""){
    		alert('请输入拼音码！');
        return;
    	}
    template.submit();
    return true;
  }

  // \u8FD4\u56DE
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";

    element.submit();
  }
  
  function makeSpell ( )
  {
		template.subFunction.value='create_getSpellCode';
		show_wait();
    template.submit();
  }
  function myReset(){
  		template.charge_kind_code.value='';
  		template.charge_kind_name.value='';
  		template.spell.value='';
  }
</Script>
<%String[][] income_type={{"T","医疗收入"},{"M","药品收入"}};%>
<html:html clazz="main">
<form name="template" method="post" action="chargeitemsort.jspviewhigh">
  <!-- \u4FE1\u606F\u63D0\u793A\u680F -->
  <html:message/>

  <!-- \u6807\u9898\u680F -->
  <html:title clazz='module'>收费类别添加页面</html:title>

  <!-- \u7B80\u5355\u4FE1\u606F -->
  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">收费类别编码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="charge_kind_code" class="textInputC" maxlength="20" value="<%=request.getParameter("charge_kind_code")==null? "":request.getParameter("charge_kind_code")%>"/></td>
      <td class="signText" nowrap="nowrap">收费类别名称：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="charge_kind_name" class="textInputC" maxlength="40" onblur="makeSpell()" value="<%=request.getParameter("charge_kind_name")==null? "":request.getParameter("charge_kind_name")%>"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">收入项目名称：</td>
          <td class="normalText">
        <%=new SingleSelect(request.getAttribute("init_income_subj"), "income_subj_name", request.getParameter("income_subj_name")==null? "":request.getParameter("income_subj_name"), true, true)%>
      </td>
      <td class="signText" nowrap="nowrap">收入类型：</td>
      <td nowrap="nowrap">
        <%=new SingleSelect(income_type, "income_type", request.getParameter("income_type")==null? "":request.getParameter("income_type"), false, true)%>
      </td>
    </tr>
		<tr>
      <td class="signText" nowrap="nowrap">拼音码：</td>
      <td class="normalText" nowrap="nowrap">
         <input type=text name="spell"  value="<%=request.getAttribute("spell")==null? "":request.getAttribute("spell")%>" style=" width:140px;"/>
      </td>
 	  <td></td>
	  <td></td>
   </tr>

    <tr>
      <td colspan="4" align="center"><button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return myReset();" >重置</button><button class="pageBtn" onclick="return back(template);">返回</button>   
      <!-- <img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
  </html:table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>
