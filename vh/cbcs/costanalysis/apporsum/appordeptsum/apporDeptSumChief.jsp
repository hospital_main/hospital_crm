<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/apporsum/appordeptsum/apporDeptSumChief.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import = "com.viewhigh.cbcs.base.mvc.view.component.Select,
  com.viewhigh.cbcs.base.mvc.view.margin.Header,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth,
  com.viewhigh.cbcs.cbcs.util.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>



<Script language="javascript">

  function analyze() {

    if (template.syear.value == '' || template.fmonth.value == '' || template.tmonth.value == '') {
      alert('请选择查询月份!');
      return false;
    }
    if(template.syear.value == "")
    {
      alert('请选择核算年份!');
      return false;
    }
    if(template.fmonth.value == "")
    {
      alert('请选择核算开始月份!');
      return false;
    }
    if(template.tmonth.value == "")
    {
      alert('请选择核算结束月份!');
      return false;
    }
    if(template.fmonth.value > template.tmonth.value ) {
      alert('开始月份不能晚于结束月份!');
      return false;
    }
    show_wait();
    setCBCS_Year1(template.syear.value);
    setCBCS_Month1(template.fmonth.value);
    setCBCS_Month2(template.tmonth.value);
    template.submit();
    return;
  }
  function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }
  function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();

  	year_1  ? template.syear.value=year_1 : "";
    month_1 ? template.fmonth.value=month_1 : "";
    month_2 ? template.tmonth.value=month_2: "";
  }
</Script>


<html:html clazz="main" isPrint="true" fixCols="2">
  <form name="template" method="post" action="apporDeptSumChief.jspviewhigh" />
		<html:message/>
		<html:title clazz='module'>科室成本分摊汇总总表(医成本C5-01表)</html:title>
<%
	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
	String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};

	Calendar thisMonth=Calendar.getInstance();
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;
%>

    <table  width="100%" cellspacing="2" border="0" >
      <tr>
        <td class="signText" nowrap width="2%">核算月：</td>
        <td class="signText"><%=new Select(year,"syear",request.getParameter("syear")==null? String.valueOf(thisMonth.get(Calendar.YEAR)):request.getParameter("syear"),false,true)%>年
	 <%=new Select(month,"fmonth",request.getParameter("fmonth")==null? curMonth:request.getParameter("fmonth"),false,true)%>月 到
	 <%=new Select(month,"tmonth",request.getParameter("tmonth")==null? curMonth:request.getParameter("tmonth"),false,true)%>月
        </td>
        <td><button class="pageBtn" onclick="return analyze();">计算</button></td>
      <%
        String[][] result = (String[][])request.getAttribute("table_result");
        if (result != null && result.length > 1){
      %>
        <td><button class="pageBtn" onclick="return preparedPrintWithIndent();">打印</button></td>
        <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
      <%
        }
      %>
      </tr>
       <tr >
	  <td colspan="2" ><br> <input type="text" class="findVhTabCtn"/></td>
	  </tr>
    </table>

    <br>

		<html:title clazz='table'>科室成本分摊汇总总表</html:title>
    <vh:vhFixTable fixRow=2 fixCol=1>
		<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
			<colgroup id=tg>
				<col style = <%=DisplayWidth.NAME_WIDTH%>>
				<col style = <%=DisplayWidth.MONEY_WIDTH%>>
				<col style = <%=DisplayWidth.MONEY_WIDTH%>>
				<col style = <%=DisplayWidth.PERCENT_WIDTH%>>
				<col style = <%=DisplayWidth.MONEY_WIDTH%>>
				<col style = <%=DisplayWidth.PERCENT_WIDTH%>>
				<col style = <%=DisplayWidth.MONEY_WIDTH%>>
				<col style = <%=DisplayWidth.PERCENT_WIDTH%>>
				<col style = <%=DisplayWidth.MONEY_WIDTH%>>
				<col style = <%=DisplayWidth.PERCENT_WIDTH%>>
				<col style = <%=DisplayWidth.MONEY_WIDTH%>>
				<col style = <%=DisplayWidth.PERCENT_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
				<col style = <%=DisplayWidth.PERCENT_WIDTH%>>
			</colgroup>

      <tr>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">科室</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">科室成本</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">直接计入</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">直接成本计算计入</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">分摊公用成本</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">分摊管理成本</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">分摊医疗辅助成本</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">分摊医疗技术成本</td>
      </tr>

      <tr>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
      </tr>

		<%
		  if (result != null && result.length > 1) {
			DecimalFormat percentFormat = new DecimalFormat("#0.00%");
		  DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
		  String[] total = null;

		  for (int i = 0; i < result.length; i++ ) {
		    /** 去掉&nbsp;*/
		    StringBuffer name = new StringBuffer(result[i][3]);
		    while(name.indexOf("&nbsp;") != -1) {
		      name.delete(name.indexOf("&nbsp;"), name.indexOf("&nbsp;") + 6);
		    }
		    for (int j = 7; j < result[i].length; j++) {
					if ((j % 2 == 0) || (j == 7)) {
		        result[i][j] =
		          moneyFormat.format(Double.parseDouble(result[i][j]));
		      } else {
		        result[i][j] =
		          percentFormat.format(Double.parseDouble(result[i][j]));
		      }
				}

		    if (result[i][3].equals("类别小计")) {
		      int rowSpan = 1;
		      int j = i+1;
		      for( ; j < result.length; j++ ) {
		        if (!result[j][0].equals(result[i][0]))
		          break;
		        rowSpan++;
		      }
		%>
	    <tr bgcolor="#E1E1FB" height="22" >
	      <td class="normalText" nowrap>
	          <a href = "javascript:var win = window.open('apporDeptSumDetail.jspviewhigh?subFunction=apporDeptSumDetail&sumByWhat=appDeptType&value=<%=result[i][0]%>&year_month=<%=request.getParameter("syear")+request.getParameter("fmonth")%>&year_month2=<%=request.getParameter("syear")+request.getParameter("tmonth")%>&name=<%=result[i][1].toString()%>', 'child', 'width=800, height=580, top=100, left=100, scrollbars = 1, resizable = 1');">
	          <%=result[i][1]%>小计
	          </a>
	      </td>
	      <td class="numberText" style='text-align:right'><%=result[i][7]%></td>
	      <td class="numberText" style='text-align:right'><%=result[i][8]%></td>
	      <td class="numberText" style='text-align:right'><%=result[i][9]%></td>
	      <td class="numberText" style='text-align:right'><%=result[i][10]%></td>
	      <td class="numberText" style='text-align:right'><%=result[i][11]%></td>
	      <td class="numberText" style='text-align:right'><%=result[i][12]%></td>
	      <td class="numberText" style='text-align:right'><%=result[i][13]%></td>

          <td class="numberText" style='text-align:right'><%=result[i][14]%></td>
	      <td class="numberText" style='text-align:right'><%=result[i][15]%></td>

          <td class="numberText" style='text-align:right'><%=result[i][16]%></td>
	      <td class="numberText" style='text-align:right'><%=result[i][17]%></td>
          <td class="numberText" style='text-align:right'><%=result[i][18]%></td>
	      <td class="numberText" style='text-align:right'><%=result[i][19]%></td>
	    </tr>
	    <%
	} else if ( result[i][1].equals("全院")) {
	              total = result[i];
	            } else {
	    %>
	    <tr height="22" bgcolor="#FFFFFF"  >
	      <td class="normalText" nowrap>
	        <a href = "javascript:var win = window.open('apporDeptSumDetail.jspviewhigh?subFunction=apporDeptSumDetail&sumByWhat=dept&value=<%=result[i][2]%>&year_month=<%=request.getParameter("syear")+request.getParameter("fmonth")%>&year_month2=<%=request.getParameter("syear")+request.getParameter("tmonth")%>&name=<%=name.toString()%>', 'child', 'width=800, height =580, top=100, left=100, scrollbars = 1, resizable = 1');">
	          <%=result[i][3]%>
			</a>
	      </td>
	      <td class="numberText" style='text-align:right' nowrap><%=result[i][7]%></td>
	      <td class="numberText" style='text-align:right' nowrap>
     		<% if ( result[i][6].equals("Y") ) { %>
			<a href = "javascript:var win = window.open('apporDeptSumDetail.jspviewhigh?subFunction=direct&sumByWhat=dept&value=<%=result[i][2]%>&year_month=<%=request.getParameter("syear")+request.getParameter("fmonth")%>&year_month2=<%=request.getParameter("syear")+request.getParameter("tmonth")%>&name=<%=name.toString()%>', 'child', 'width=800, height =580, top=100, left=100, scrollbars = 1, resizable = 1');">
			<%=result[i][8]%>
			</a>
        <%}else{ out.println(result[i][8] ); }%>
	      </td>
	      <td class="numberText" style='text-align:right' nowrap><%=result[i][9]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=result[i][10]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=result[i][11]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=result[i][12]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=result[i][13]%></td>
        <td class="numberText" style='text-align:right' nowrap>
          		<% if ( result[i][6].equals("Y") ) { %>
			<a href = "javascript:var win = window.open('apporDeptSumDetail.jspviewhigh?subFunction=manage&sumByWhat=dept&value=<%=result[i][2]%>&year_month=<%=request.getParameter("syear")+request.getParameter("fmonth")%>&year_month2=<%=request.getParameter("syear")+request.getParameter("tmonth")%>&name=<%=name.toString()%>', 'child', 'width=800, height =580, top=100, left=100, scrollbars = 1, resizable = 1');">
			<%=result[i][14]%>
			</a>
              <%}else{ out.println(result[i][14] ); }%>
          </td>
          <td class="numberText" style='text-align:right' nowrap><%=result[i][15]%></td>
          <td class="numberText" style='text-align:right' nowrap>
          		<% if ( result[i][6].equals("Y") ) { %>
              <a href = "javascript:var win = window.open('apporDeptSumDetail.jspviewhigh?subFunction=service&sumByWhat=dept&value=<%=result[i][2]%>&year_month=<%=request.getParameter("syear")+request.getParameter("fmonth")%>&year_month2=<%=request.getParameter("syear")+request.getParameter("tmonth")%>&name=<%=name.toString()%>', 'child', 'width=800, height =580, top=100, left=100, scrollbars = 1, resizable = 1');">
              <%=result[i][16]%>
              </a>
              <%}else{ out.println(result[i][16] ); }%>
          </td>
          <td class="numberText" style='text-align:right' nowrap><%=result[i][17]%></td>
          <td class="numberText" style='text-align:right' nowrap>
          		<% if ( result[i][6].equals("Y") ) { %>
              <a href = "javascript:var win = window.open('apporDeptSumDetail.jspviewhigh?subFunction=techfindAll&sum=chargetype&value=<%=result[i][2]%>&year_month=<%=request.getParameter("syear")+request.getParameter("fmonth")%>&year_month2=<%=request.getParameter("syear")+request.getParameter("tmonth")%>&name=<%=name.toString()%>', 'child', 'width=800, height =580, top=100, left=100, scrollbars = 1, resizable = 1');">
              <%=result[i][18]%>
              </a>
              <%}else{ out.println(result[i][18] ); }%>
          </td>
          <td class="numberText" style='text-align:right' nowrap><%=result[i][19]%></td>
	    </tr>
	        <%
	            }
	        }
	        if (total != null) {
	        %>

	    <tr height="22" bgcolor="#FFFFFF" >
	      <td class="normalText" nowrap>
	        <a href = "javascript:var win = window.open('apporDeptSumDetail.jspviewhigh?subFunction=apporDeptSumDetail&sumByWhat=totalSum&&year_month=<%=request.getParameter("syear")+request.getParameter("fmonth")%>&year_month2=<%=request.getParameter("syear")+request.getParameter("tmonth")%>', 'child', 'width=800, height=580, top=100, left=100, scrollbars = 1, resizable = 1');" >
	          <%=total[1]%><%=total[3]%>
					</a>
	      </td>
	      <td class="numberText" style='text-align:right' nowrap>--</td>
	      <td class="numberText" style='text-align:right' nowrap><%=total[8]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=total[9]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=total[10]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=total[11]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=total[12]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=total[13]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=total[14]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=total[15]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=total[16]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=total[17]%></td>
          <td class="numberText" style='text-align:right' nowrap><%=total[18]%></td>
	      <td class="numberText" style='text-align:right' nowrap><%=total[19]%></td>
	    </tr>
	    <%}
	    }%>
	  </table>
	  </vh:vhFixTable>
		<input type="hidden" name="subFunction" value="apporDeptSumChief">
	</form>
</html:html>


