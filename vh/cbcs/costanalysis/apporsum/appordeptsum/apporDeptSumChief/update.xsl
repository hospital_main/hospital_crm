<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	
	<thead>
	<xsl:variable name="gy" select="/root/tbody/tr[1]/td[last()-1]"/>
	<xsl:variable name="yf" select="/root/tbody/tr[1]/td[last()]"/>
	<tr noWrap="true" class="mainHead">
		<td nowrap="true" rowspan="2">成本项目</td>
		<td nowrap="true" colspan="2">合计</td>		
		<td nowrap="true" colspan="4">直接成本</td>
		
		<xsl:choose>
			<xsl:when test="$gy=1 and $yf=1">
				<td nowrap="true"  colspan="4">间接成本</td>
			</xsl:when>
			
			<xsl:when test="$gy=0 and $yf=0">
				<td nowrap="true"  colspan="2">间接成本</td>
			</xsl:when>
			
			<xsl:otherwise>
				<td nowrap="true"  colspan="3">间接成本</td>
			</xsl:otherwise>
		
		</xsl:choose>
		
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  >金额</td>
		<td noWrap="true"  >比例</td>
		<td noWrap="true"  >直接计入</td>
		<td noWrap="true"  >计算计入</td>
		<td noWrap="true"  >小计</td>
		<td noWrap="true"  >比例</td>
		<xsl:if test="$gy=1">
			<td noWrap="true"  >公用成本</td>
		</xsl:if>
		<td noWrap="true"  >管理科室</td>
		<xsl:if test="$yf=1">
			<td noWrap="true"  >医疗辅助</td>
		</xsl:if>
		<td noWrap="true"  >医疗技术</td>
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			 	<tr>
			 		<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=last()-1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                	<td noWrap="true">
		                   		<xsl:value-of select="."/>
		                  	</td>
		                </xsl:when>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=6">
		                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
		                <xsl:when test="position()=10">
		                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
