<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>


	<tr noWrap="true" class="mainHead">
		<td noWrap="true" >科室名称</td>
		<td noWrap="true" >分摊成本</td>
		<td noWrap="true" >比例</td>
		
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			 	<tr>
			 		<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                	<td noWrap="true">
		                   		<xsl:choose>
				 					<xsl:when test=". !='合计'">
			                			<a href="#" ><xsl:attribute name="onclick">win_ftyf_detail("<xsl:value-of select="../td[4]"/>")</xsl:attribute><xsl:value-of select="."/></a>
				 					</xsl:when>
				 					<xsl:otherwise>
				 						<xsl:value-of select="."/>
				 					</xsl:otherwise>
				 				</xsl:choose>
		                  	</td>
		                </xsl:when>
		                <xsl:when test="position()=2">
		                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=3">
		                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
		                <xsl:otherwise>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
