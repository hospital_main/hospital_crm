<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	
	<thead>
	<xsl:variable name="gy" select="/root/tbody/tr[1]/td[last()-1]"/>
	<xsl:variable name="yf" select="/root/tbody/tr[1]/td[last()]"/>	
	<tr noWrap="true" class="mainHead">
		<td nowrap="true"  rowspan="2">科室</td>
		<td nowrap="true"  rowspan="2">科室成本</td>
		<td nowrap="true"  colspan="2">直接计入</td>
		<td nowrap="true"  colspan="2">直接成本计算计入</td>
		<xsl:if test="$gy=1">
			<td nowrap="true"  colspan="2">分摊公用成本</td>
		</xsl:if>
		<td nowrap="true"  colspan="2">分摊管理成本</td>
		<xsl:if test="$yf=1">
			<td nowrap="true"  colspan="2">分摊医疗辅助成本</td>
		</xsl:if>
		<td nowrap="true"  colspan="2">分摊医疗技术成本</td>
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  >金额</td>
		<td noWrap="true"  >比例</td>
		<td noWrap="true"  >金额</td>
		<td noWrap="true"  >比例</td>
		<td noWrap="true"  >金额</td>
		<td noWrap="true"  >比例</td>
		<td noWrap="true"  >金额</td>
		<td noWrap="true"  >比例</td>
		<xsl:if test="$gy=1">
			<td noWrap="true"  >金额</td>
			<td noWrap="true"  >比例</td>
		</xsl:if>
		<xsl:if test="$yf=1">
			<td noWrap="true"  >金额</td>
			<td noWrap="true"  >比例</td>
		</xsl:if>
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="v1" select="td[4]"/>
				<xsl:variable name="is_last" select="td[6]"/>
			 	<xsl:choose>
			 	<xsl:when test="$v1='1' or $v1='2'">
					<tr bgcolor="#E1E1FB">
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()-1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                </xsl:when>
		                <xsl:when test="position()=3">
		                	<td noWrap="true">
		                		<xsl:if test="../td[2]!=''">
		                	<a href="#" >
		                		<xsl:attribute name="onclick">win("<xsl:value-of select="../td[2]"/>","<xsl:value-of select="."/>")</xsl:attribute><xsl:value-of select="."/>
		                	</a>
		                	</xsl:if>
		                	<xsl:if test="../td[2]=''">
		                			<xsl:value-of select="."/>
		                	</xsl:if>
		                	</td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()=6">
		                </xsl:when>
						<xsl:when test="position() &gt; 7 and position() mod 2 =1">
							<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
		                <xsl:otherwise>
		                   <xsl:choose>
		                		<xsl:when test="position()=7 and (../td[3]='全院合计')">
		                			
		                			<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                		</xsl:when>
		                		<xsl:otherwise>
		                			<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                		</xsl:otherwise>
		                	</xsl:choose>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			 	</xsl:when>
			 	<xsl:otherwise>
			 		<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=last()-1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                </xsl:when>
		                <xsl:when test="position()=3">
		                	<td noWrap="true">
		                	<a href="#" ><xsl:attribute name="onclick">win("<xsl:value-of select="../td[2]"/>","<xsl:value-of select="."/>")</xsl:attribute><xsl:value-of select="."/></a>
		                	</td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()=6">
		                </xsl:when>
		                <xsl:when test="position()=8">
		                	<td noWrap="true" class="numberText">
			                	<xsl:choose>
				 					<xsl:when test="$is_last='Y'">
			                			<a href="#" ><xsl:attribute name="onclick">win_zjjr("<xsl:value-of select="../td[2]"/>","<xsl:value-of select="../td[3]"/>","直接成本明细")</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/></a>
				 					</xsl:when>
				 					<xsl:otherwise>
				 						<xsl:value-of select="format-number(.,'#,##0.00')"/>
				 					</xsl:otherwise>
				 				</xsl:choose>	
			 				</td>
		                </xsl:when>
		                
		                <xsl:when test="position()=12">
		                	<td noWrap="true" class="numberText">
			                	<xsl:choose>
			                		<xsl:when test="$gy='0'">
				                		<xsl:choose>
				                			
				 										<xsl:when test="$is_last='Y'">
			                			<a href="#" ><xsl:attribute name="onclick">win_ftgl("<xsl:value-of select="../td[2]"/>","<xsl:value-of select="../td[3]"/>","接受管理科室分摊成本")</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/></a>
				 										
				 										</xsl:when>
				 										
										 				<xsl:otherwise>
										 						<xsl:value-of select="format-number(.,'#,##0.00')"/>
										 					</xsl:otherwise>
									 					</xsl:choose>
									 					
				 					</xsl:when>
				 					<xsl:otherwise>
				 						<xsl:value-of select="format-number(.,'#,##0.00')"/>
				 					</xsl:otherwise>
				 				</xsl:choose>	
			 				</td>
		                </xsl:when>
		                
		                <xsl:when test="position()=14">
		                	<td noWrap="true" class="numberText">
			                	<xsl:choose>
				 								<xsl:when test="$is_last='Y'">
				 									<xsl:if test="$gy='1'">
							               	<a href="#" ><xsl:attribute name="onclick">win_ftgl("<xsl:value-of select="../td[2]"/>","<xsl:value-of select="../td[3]"/>","接受管理科室分摊成本")</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/></a>
							            </xsl:if>
							            <xsl:if test="$gy='0'">
							              <xsl:if test="$yf='1'">
			                				<a href="#" ><xsl:attribute name="onclick">win_ftyf("<xsl:value-of select="../td[2]"/>","<xsl:value-of select="../td[3]"/>","接受医疗辅助科室分摊成本")</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/></a>
				 										</xsl:if>
							               	<xsl:if test="$yf='0'">
					                			<a href="#" ><xsl:attribute name="onclick">win_ftyj("<xsl:value-of select="../td[2]"/>","<xsl:value-of select="../td[3]"/>","接受医疗技术科室分摊成本")</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/></a>
							               	</xsl:if>
							               </xsl:if>
				 					</xsl:when>
				 					<xsl:otherwise>
				 						<xsl:value-of select="format-number(.,'#,##0.00')"/>
				 					</xsl:otherwise>
				 				</xsl:choose>	
			 				</td>
		                </xsl:when>
		                
		                <xsl:when test="position()=16">
		                	<td noWrap="true" class="numberText">
		                		<xsl:choose>
								 					<xsl:when test="$is_last='Y'">
								 						<xsl:if test="$gy='1' and $yf='1'">
				                			<a href="#" ><xsl:attribute name="onclick">win_ftyf("<xsl:value-of select="../td[2]"/>","<xsl:value-of select="../td[3]"/>","接受医疗辅助科室分摊成本")</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/></a>
			                			</xsl:if>
								 						<xsl:if test="$gy='0' or $yf='0'">
				                			<a href="#" ><xsl:attribute name="onclick">win_ftyj("<xsl:value-of select="../td[2]"/>","<xsl:value-of select="../td[3]"/>","接受医疗技术科室分摊成本")</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/></a>
			                			</xsl:if>
								 					</xsl:when>
								 					<xsl:otherwise>
								 						<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 					</xsl:otherwise>
								 				</xsl:choose>	
							 				</td>
		                </xsl:when>
		                
		                <xsl:when test="position()=18">
		                	<td align="right" noWrap="true" class="numberText">
			                	<xsl:choose>
				 					<xsl:when test="$is_last='Y'">
			                			<a href="#" ><xsl:attribute name="onclick">win_ftyj("<xsl:value-of select="../td[2]"/>","<xsl:value-of select="../td[3]"/>","接受医疗技术科室分摊成本")</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/></a>
				 					</xsl:when>
				 					<xsl:otherwise>
				 						<xsl:value-of select="format-number(.,'#,##0.00')"/>
				 					</xsl:otherwise>
				 				</xsl:choose>	
			 				</td>
		                </xsl:when>
		                
						<xsl:when test="position() &gt; 7 and position() mod 2 =1">
							<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			 	</xsl:otherwise>
			</xsl:choose>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
