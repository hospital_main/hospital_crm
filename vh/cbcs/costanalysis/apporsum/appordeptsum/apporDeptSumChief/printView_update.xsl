<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="11"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="11" ></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="11" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>		
		</tr>
		<tr>
			<td noWrap="true" colspan="11" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>		
		</tr>
		<tr noWrap="true" class="mainHead">
			<td nowrap="true"  rowspan="2">成本项目</td>
			<td nowrap="true"  colspan="2">合计</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="4">直接成本</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="4">间接成本</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>

		 <tr noWrap="true" class="mainHead">
		 	<td style="display:none"></td>
		 	<td noWrap="true" >金额</td>
			<td noWrap="true" >比例</td>
			<td noWrap="true" >直接计入</td>
			<td noWrap="true" >计算计入</td>
			<td noWrap="true" >小计</td>
			<td noWrap="true" >比例</td>
			<td noWrap="true" >公用成本</td>
			<td noWrap="true" >管理科室</td>
			<td noWrap="true" >医疗辅助</td>
			<td noWrap="true" >医疗技术</td>
		  </tr>
		</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			 	<tr>
			 		<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                	<td noWrap="true">
		                   		<xsl:value-of select="."/>
		                  	</td>
		                </xsl:when>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=6">
		                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
		                <xsl:when test="position()=10">
		                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
		<td noWrap="true" colspan="11" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
	</tr>
	<tr>
		<td noWrap="true" colspan="11" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
	</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
