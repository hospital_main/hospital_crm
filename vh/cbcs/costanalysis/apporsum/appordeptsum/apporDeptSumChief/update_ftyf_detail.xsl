<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<colgroup id="tg">
          <col style = 'width:45mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
     </colgroup>

	<tr>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>项目名称</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>分摊成本</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
		
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			 	<tr>
			 		<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                	<td noWrap="true">
		                   		<xsl:value-of select="."/>
		                  	</td>
		                </xsl:when>
		                <xsl:when test="position()=2">
		                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=3">
		                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
		                <xsl:otherwise>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
