<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
		<xsl:variable name="gy" select="/root/tbody/tr[1]/td[last()-1]"/>
		<xsl:variable name="yf" select="/root/tbody/tr[1]/td[last()]"/>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="colcount"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" ></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr noWrap="true" class="mainHead">
			<td nowrap="true"  rowspan="2">科室</td>
			<td nowrap="true"  rowspan="2">科室成本</td>
			<td nowrap="true"  colspan="2">直接计入</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">直接成本计算计入</td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td nowrap="true"  colspan="2">分摊公用成本</td>
				<td style="display:none"></td>
			</xsl:if>
			<td nowrap="true"  colspan="2">分摊管理成本</td>
			<td style="display:none"></td>
			<xsl:if test="$yf=1">
				<td nowrap="true"  colspan="2">分摊医疗辅助成本</td>
				<td style="display:none"></td>
			</xsl:if>
			<td nowrap="true"  colspan="2">分摊医疗技术成本</td>
			<td style="display:none"></td>
		</tr>
		 <tr noWrap="true" class="mainHead">
		 	<td style="display:none"></td>
		 	<td style="display:none"></td>
			<td noWrap="true" >金额</td>
			<td noWrap="true" >比例</td>
			<td noWrap="true" >金额</td>
			<td noWrap="true" >比例</td>
			<td noWrap="true" >金额</td>
			<td noWrap="true" >比例</td>
			<td noWrap="true" >金额</td>
			<td noWrap="true" >比例</td>
			<xsl:if test="$gy=1">
				<td noWrap="true" >金额</td>
				<td noWrap="true" >比例</td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td noWrap="true" >金额</td>
				<td noWrap="true" >比例</td>
			</xsl:if>
		  </tr>
		</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=last()-1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                </xsl:when>
		                <xsl:when test="position()=3">
		                	<td noWrap="true"><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()=6">
		                </xsl:when>
						<xsl:when test="position() &gt; 7 and position() mod 2 =1">
							<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
		                <xsl:otherwise>
		                   <xsl:choose>
		                		<xsl:when test="position()=7 and (../td[3]='全院合计')">
		                			<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                		</xsl:when>
		                		<xsl:otherwise>
		                			<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                		</xsl:otherwise>
		                	</xsl:choose>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
			<td noWrap="true" colspan="colcount" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
				<td style="display:none"></td>
			</xsl:if>
		</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
