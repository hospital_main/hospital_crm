<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>


	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  >成本单号</td>
		<td noWrap="true"  >支付日期</td>
		<td noWrap="true"  >摘要</td>
		<td noWrap="true"  >成本项目名称</td>
		<td noWrap="true"  >金额</td>
		<td noWrap="true"  >录入人</td>
		<td noWrap="true"  >是否已分摊</td>
		<td noWrap="true"  >数据来源</td>
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			 	<tr>
			 		<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                	<xsl:choose>
		                		<xsl:when test=". = 0">
		                			<td noWrap="true">合计:</td>
		                		</xsl:when>
		                		<xsl:otherwise>
		                			<td noWrap="true">
		                   				<xsl:value-of select="."/>
		                  			</td>
		                		</xsl:otherwise>
		                	</xsl:choose>
		                </xsl:when>
		                <xsl:when test="position()=5">
		                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                
		                <xsl:otherwise>
		                   <td noWrap="true">
		                   		<xsl:value-of select="."/>
		                  	</td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
