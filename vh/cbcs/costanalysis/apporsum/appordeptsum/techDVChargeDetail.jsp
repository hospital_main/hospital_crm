<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/apporsum/appordeptsum/techDVChargeDetail.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.margin.Header,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script>
	function find(){
	 template.subFunction.value="techfindAll"
	 show_wait();
	 template.submit();
	 return true;
	}

    function choose(){
        var sp = document.getElementById("deptType");
        if(document.getElementById("sum2").checked){
            var sele = "&nbsp;&nbsp;分摊类型：<select name='fType'><option value=''></option><option value='T103'>工作量</option><option value='T102'>服务量</option><option value='T101'>收支配比</option></select>"
            sp.innerHTML = sele;
        }else{
            while(sp.hasChildNodes()){
                sp.removeChild(sp.firstChild);
            }
        }
    }
</script>
   <%
        String name = request.getParameter("name");//new String(request.getParameter("name").getBytes("ISO8859_1"), "GBK");
        String yearMonth = request.getParameter("year_month");
        String yearMonth2 = request.getParameter("year_month2");
        String sum = request.getParameter("sum");

        if(sum==null){
        	sum="chargetype";
        }

        String yearMonthStr = yearMonth.substring(0,4) + "年" + Integer.parseInt(yearMonth.substring(4)) + "月";
        if(!yearMonth.substring(4,6).equals(yearMonth2.substring(4,6)))
            yearMonthStr  =  yearMonthStr + "到"+ Integer.parseInt(yearMonth2.substring(4))+ "月";
    %>

<html:html clazz="child" isPrint="true">
<form name="template" method="post" action="apporDeptSumDetail.jspviewhigh" />
<html:message/>

	<html:title clazz='module'>成本分摊汇总表-<%=name%>接受医疗技术科室分摊成本（<%=yearMonthStr%>）</html:title>

  <table  width="100%" cellspacing="2" border="0" >
    <tr>
		<td class="signText" >
			<input type="radio" id="sum1" name="sum" value="chargetype" <%if(sum.equals("chargetype")){out.print("checked");}%> onclick="choose()">按收费类别统计
			<input type="radio" id="sum2" name="sum" value="dept" <%if(sum.equals("dept")){out.print("checked");}%> onclick="choose()">按科室统计
            <span id="deptType"><span>
		</td>
		<td>
      		<button class="pageBtn"  onclick="find();">查询</button>
      	</td>
      	<td><button class="pageBtn" onClick="window.close()" >关闭</button>
      		<!--<img src="images/priClose.gif" class="mouse"  onclick="window.close();"/>-->
      	</td>
    </tr>
 	</table>
 	<p></p>
    <vh:vhFixTable fixRow=1 fixCol=1>
		<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
		  <colgroup id=tg>
			<col style = <%=DisplayWidth.NAME_WIDTH%>>
			<col style = <%=DisplayWidth.NAME_WIDTH%>>
			<col style = <%=DisplayWidth.MONEY_WIDTH%>>
	    </colgroup>

    <tr>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>科室</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>医疗项目</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>分摊成本</td>
	</tr>
	 <%
	 String[][] result = (String[][])request.getAttribute("results");
	 if (result != null) {
	 	DecimalFormat percentFormat = new DecimalFormat("#0.00%");
	    DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");

		//System.out.println(result.length);
	    for (int i = 0; i < result.length; i++) {
	    /*
	    System.out.println("m1");
	    System.out.println(result[i][0]);
	    System.out.println("  dd");
	    System.out.println(result[i][1]);
	    System.out.println("  ff");
	    System.out.println(result[i][2]);
	    */
	%>
    <tr height="22" >
      <td class="normalText" nowrap><%=result[i][0]%></td>
      <td class="numberText" style='text-align:right' nowrap><%=result[i][1]%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][2]))%></td>
	</tr>
	<%
		}
	}%>
	</table>
    </vh:vhFixTable>
    <input type="hidden" name="subFunction">
	<input type="hidden" name="value" value="<%=request.getParameter("value")%>">
    <input type="hidden" name="year_month" value="<%=yearMonth%>">
    <input type="hidden" name="year_month2" value="<%=yearMonth2%>">
    <input type="hidden" name="name" value="<%=name%>">

</form>
</html:html>
