<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/apporsum/appordeptsum/directDetail.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.margin.Header,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
   <%
        String name = request.getParameter("name");//new String(request.getParameter("name").getBytes("ISO8859_1"), "GBK");
        String yearMonth = request.getParameter("year_month");
        String yearMonth2 = request.getParameter("year_month2");

        String yearMonthStr = yearMonth.substring(0,4) + "年" + Integer.parseInt(yearMonth.substring(4)) + "月";
        if(!yearMonth.substring(4,6).equals(yearMonth2.substring(4,6)))
            yearMonthStr  =  yearMonthStr + "到"+ Integer.parseInt(yearMonth2.substring(4))+ "月";
    %>

<html:html clazz="child" isPrint="true">
<html:message/>

	<html:title clazz='module'>成本分摊汇总表-<%=name%>直接成本明细（<%=yearMonthStr%>）</html:title>

  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      	<td><button class="pageBtn" onClick="window.close()" >关闭</button>
      	<!--	<img src="images/priClose.gif" class="mouse"  onclick="window.close();"/>-->
      	</td>
    </tr>
 	</table>
 	<p></p>
    <vh:vhFixTable fixRow=1 fixCol=1>
		<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
		  <colgroup id=tg>
			<col style = <%=DisplayWidth.NAME_WIDTH%>>
			<col style = <%=DisplayWidth.NAME_WIDTH%>>
			<col style = <%=DisplayWidth.NAME_WIDTH%>>
			<col style = <%=DisplayWidth.NAME_WIDTH%>>
			<col style = <%=DisplayWidth.MONEY_WIDTH%>>
			<col style = <%=DisplayWidth.NAME_WIDTH%>>
			<col style = <%=DisplayWidth.NAME_WIDTH%>>
			<col style = <%=DisplayWidth.NAME_WIDTH%>>
	    </colgroup>

    <tr>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>成本单号</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>支付日期</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>摘要</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>成本项目名称</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>金额</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>录入人</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>是否已分摊</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>数据来源</td>
	</tr>
	 <%
	 double totalResult=0.0;
	 String[][] result = (String[][])request.getAttribute("results");
	 DecimalFormat percentFormat = new DecimalFormat("#0.00%");
	    DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
	 if (result != null) {


	    for (int i = 0; i < result.length; i++) {
	%>
    <tr height="22" >
      <td class="normalText" nowrap><%=result[i][0]%></td>
      <td class="normalText" nowrap><%=result[i][1]%></td>
      <td class="normalText" nowrap><%=result[i][2]%></td>
      <td class="normalText" nowrap><%=result[i][3]%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][4]))%></td>
      <td class="normalText" nowrap><%=result[i][5]%></td>
      <td class="normalText" nowrap><%=result[i][6]%></td>
      <td class="normalText" nowrap><%=result[i][7]%></td>
    	<%
    		totalResult+= Double.parseDouble(result[i][4]);
    	%>
	</tr>

	<%
		}
	}%>
	<tr>
			<td class="normalText" nowrap>合计：</td>
      <td class="normalText" nowrap></td>
      <td class="normalText" nowrap></td>
      <td class="normalText" nowrap></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(new Double(totalResult))%></td>
      <td class="normalText" nowrap></td>
      <td class="normalText" nowrap></td>
      <td class="normalText" nowrap></td>
		</tr>
	</table>
    </vh:vhFixTable>
</html:html>
