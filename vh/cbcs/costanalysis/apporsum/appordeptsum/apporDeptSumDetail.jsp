<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/apporsum/appordeptsum/apporDeptSumDetail.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.margin.Header,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

   <%
        String sumByWhat = request.getParameter("sumByWhat");
        String name = request.getParameter("name");//new String(request.getParameter("name").getBytes("ISO8859_1"), "GBK");
        String yearMonth = request.getParameter("year_month");
        String yearMonth2 = request.getParameter("year_month2");
        String yearMonthStr = "核算月: " + yearMonth.substring(0,4) + "年" + Integer.parseInt(yearMonth.substring(4)) + "月";
        if(!yearMonth.substring(4,6).equals(yearMonth2.substring(4,6)))
            yearMonthStr  =  yearMonthStr + "到"+ Integer.parseInt(yearMonth2.substring(4))+ "月";
        String moduleTitle = "";

        if(sumByWhat != null && sumByWhat.equals("appDeptType") && name != null) {
            moduleTitle = "类别: " + name + "   " + yearMonthStr;
        }
        else if(sumByWhat != null && sumByWhat.equals("dept") && name != null) {
          moduleTitle = "科室:" + name + "   " + yearMonthStr;
        }
        else if(sumByWhat != null && sumByWhat.equals("totalSum")) {
          moduleTitle = "全院:" + "   " + yearMonthStr;
        }
    %>
<script type="text/javascript">
  function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav&alertwindow=aw","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }
  function init()
  {
	  window.prefix = opener.window.prefix;
  }
</script>
<html:html clazz="child" isPrint="true">

  <html:message/>

	<html:title clazz='module'>科室成本分摊汇总明细表(医成本C5-02表) <%=moduleTitle%></html:title>

  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <%
      String[][] result = (String[][])request.getAttribute("table_result");
      if (result != null && result.length > 1){
      %>
      <td align='center'><button class="pageBtn" onclick="init();preparedPrintWithIndent();">打印</button></td>
      <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
      <%
      }
      %>
      <td align='center'><button class="pageBtn" onClick="window.close()" >关闭</button>
      <!--<img src="images/priClose.gif" class="mouse"  onclick="window.close();"/>--></td>
    </tr>
 	</table>
 	<p></p>

	<html:title clazz='table'>科室成本分摊汇总明细表</html:title>

    <vh:vhFixTable fixRow=2 fixCol=1>
		<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
		  <colgroup id=tg>
	      <col style = <%=DisplayWidth.NAME_WIDTH%>>
	      <col style = <%=DisplayWidth.MONEY_WIDTH%>>
       <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
	      <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
	      <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
	      <col style = <%=DisplayWidth.MONEY_WIDTH%>>
	      <col style = <%=DisplayWidth.MONEY_WIDTH%>>
	    </colgroup>

    <tr>
     <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">成本项目</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">合计</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="4">直接成本</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="4">间接成本</td>
    </tr>

    <tr>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >直接计入</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >计算计入</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >小计</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >公用成本</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >管理科室</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >医疗辅助</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >医疗技术</td>
    </tr>

		<%
		  if (result != null && result.length > 1) {
    DecimalFormat percentFormat = new DecimalFormat("#0.00%");
    DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
    String[] total = null;
    for (int i = 0; i < result.length; i++) {
    if (result[i][1].equals("合计")) {
        total = result[i];
    } else if(result[i][4]!=null){
		%>

    <tr height="22" >
      <td class="normalText" nowrap><%=result[i][1]%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][4]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(result[i][5]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][6]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][7]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][8]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(result[i][4])!=0?Double.parseDouble(result[i][8])/Double.parseDouble(result[i][4]):0)%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][10]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][11]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][12]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][13]))%></td>
    </tr>
		<%
		  }
		}
		  if (total!=null) {
		%>
		<tr height="22" >
		 	<td class="normalText" nowrap><%=total[1]%></td>
      <td class="numberText" style='text-align:right' nowrap><%if(sumByWhat.equals("totalSum")){out.print("--");}else{out.print(moneyFormat.format(Double.parseDouble(total[4])));}%></td>
      <td class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(total[5]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[6]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[7]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[8]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(total[4])!=0?Double.parseDouble(total[8])/Double.parseDouble(total[4]):0)%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[10]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[11]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[12]))%></td>
      <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[13]))%></td>
		</tr>
		<%}
		   }%>
		  </table>
    </vh:vhFixTable>

</html:html>
