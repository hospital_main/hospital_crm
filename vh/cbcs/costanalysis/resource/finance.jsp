<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/resource/finance.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:57:34 $
     $Modtime: 03-09-15 10:37 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.margin.Header" %>
<%
  Header header = new Header(101, request);
  header.setPrintStatus(true);
  out.print(header);
%>

<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
                 com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>
<Script language="javascript">
  function preparedPrin(){
    if(template.year_month.value=="")
    {
      alert("请先选择月份");
      return;
    }
    // 报表名称
    grid.prn.title1='财力资源配置指标';
    // 年月
    var temp=template.year_month.value;
    grid.prn.title2=temp.substring(0,4)+'年'+temp.substring(4,6)+'月';
    // 表头行数
    grid.prn.tabHead=2;
    // 打印
    grid.print();
  }
</Script>
<form name="template" method="post" action="resourceFinance.jspviewhigh">
 <!-- 标题栏 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td  style="moduleTitle" nowrap="nowrap">财力资源配置指标</td>
    </tr>
  </table>
  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td style="normalText" ><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td>
      <td><button class="pageBtn" onclick="show_wait();template.submit();">计算</button></td>
      <td><button class="pageBtn" onclick="return preparedPrin();">打印</button></td>
      <td colspan=180></td>
    </tr>
  </table>
     <br>
  <!-- 结果集 -->
<fc:webprint id="grid">
  <table BORDERCOLOR="#214597" border="1" cellspacing="1" width="100%" >
                 <colgroup id=tg>
                         <col style=<%=DisplayWidth.NAME_WIDTH%>>
                         <col style=<%=DisplayWidth.PERCENT_WIDTH%>>
                         <col style=<%=DisplayWidth.PERCENT_WIDTH%>>
                         <col style=<%=DisplayWidth.PERCENT_WIDTH%>>
                         <col style=<%=DisplayWidth.PERCENT_WIDTH%>>
                         <col style=<%=DisplayWidth.PERCENT_WIDTH%>>
                         <col style=<%=DisplayWidth.PERCENT_WIDTH%>>
                         <col style=<%=DisplayWidth.PERCENT_WIDTH%>>
                     </colgroup>

    <tr bordercolor="#214597" bgcolor="#FFFFFF" style="resultLabel">
      <td width="16%" style='text-align:center;font-weight:bold' rowspan="2"> <div align="center">指标<strong> </strong></div></td>
      <td width="13%" style='text-align:center;font-weight:bold' rowspan="2" bordercolor="#214597"> <div align="center">本期值</div></td>
      <td height="38" style='text-align:center;font-weight:bold' colspan="2"> <div align="center">与上期比较</div></td>
      <td style='text-align:center;font-weight:bold' colspan="2"> <div align="center">与去年同期比较</div></td>
      <td style='text-align:center;font-weight:bold' colspan="2"> <div align="center">与平均比较</div></td>
      <div style='text-align:center;font-weight:bold' align="center"></div></td>
    </tr>
    <tr>
      <td style='text-align:center;font-weight:bold' nowrap style="resultLabel"> <div align="center">上期值</div></td>
      <td nowrap style='text-align:center;font-weight:bold' style="resultLabel"> <div align="center">差异</div></td>
      <td nowrap style="resultLabel" style='text-align:center;font-weight:bold'> <div align="center">去年同期值</div></td>
      <td nowrap style="resultLabel" style='text-align:center;font-weight:bold'> <div align="center">差异</div></td>
      <td nowrap style="resultLabel" style='text-align:center;font-weight:bold'> <div align="center">平均值</div></td>
      <td nowrap style="resultLabel" style='text-align:center;font-weight:bold'> <div align="center">差异</div></td>
    </tr>
    <%
      DecimalFormat df = new DecimalFormat("0.00");
      String[][] result = (String[][])request.getAttribute( "table_result" );
      if ( result != null )
      {
        for(int i=0;i<4;i++){
          for(int j=0;j<7;j++){
            if( result[i][j] != null && !result[i][j].equals(""))
              result[i][j]=df.format(Double.parseDouble(result[i][j])*100) + "%";
            else result[i][j]="&nbsp";
          }
        }

        String rowColor = "rowWhite";
    %>

    <tr style="<%=rowColor%>">
      <td ><div align="left">资产负债率</div></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[0][0]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[0][1]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[0][2]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[0][3]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[0][4]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[0][5]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[0][6]%></td>
    </tr>
    <tr style="<%=rowColor%>">
      <td ><div align="left">流动比率</div></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[1][0]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[1][1]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[1][2]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[1][3]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[1][4]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[1][5]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[1][6]%></td>
    </tr>
    <tr style="<%=rowColor%>">
      <td ><div align="left">速动比率</div></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[2][0]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[2][1]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[2][2]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[2][3]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[2][4]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[2][5]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[2][6]%></td>
    </tr>
    <tr style="<%=rowColor%>">
      <td ><div align="left">现金比率</div></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[3][0]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[3][1]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[3][2]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[3][3]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[3][4]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[3][5]%></td>
      <td height="22" style="numberText" style='text-align:right'><%=result[3][6]%></td>
    </tr>
     <%
      }
     %>
  </table>
</fc:webprint>
<input type=hidden name="subFunction" value="sub"/>
<input type=hidden name="preparedPrint"/>
</form>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>