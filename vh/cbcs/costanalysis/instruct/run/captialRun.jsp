<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/instruct/run/captialRun.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime: 03-09-16 16:59 $
 $Revision: 1.1 $
 $NoKeywords: $
-->


<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp"  %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.margin.Header,
                  com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>

<%
  Header header = new Header(101, request);
  header.setPrintStatus(true);
  out.print(header);
%>
<Script Language="JavaScript">
  function find(){
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
  function preparedPrin(){
    if(template.year_month.value=="")
    {
      alert("请先选择月份");
      return;
    }
    // 报表名称
    grid.prn.title1='资金运营能力指标';
    // 年月
    var temp=template.year_month.value;
    grid.prn.title2=temp.substring(0,4)+'年'+temp.substring(4,6)+'月';
    // 表头行数
    grid.prn.tabHead=2;
    // 打印
    grid.print();
  }
</Script>
  <form name="template" method="post" action="instructRun.jspviewhigh">
    <!-- 信息提示栏 -->
       <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>
     <!--信息栏-->
       <%= new com.viewhigh.cbcs.base.mvc.view.MessageTitle("资金运营能力指标")%>
      <!-- 简单信息 -->
      <table  width="100%" cellspacing="2" border="0" >
        <tr>
          <td class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
          <td><button class="pageBtn" onclick="return find();" >计算</button></td>
          <td><button class="pageBtn" onclick="return preparedPrin();">打印</button></td>
          <td colspan=180></td>
        </tr>
      </table>
         <br>
        <fc:webprint id="grid">
              <table BORDERCOLOR="#214597" width="100%" border="1" align="center" cellspacing="1" >
                    <colgroup id=tg>
                         <col style = <%=DisplayWidth.NAME_WIDTH%>>

                         <col style = <%=DisplayWidth.MONEY_WIDTH%> >
                         <col style = <%=DisplayWidth.MONEY_WIDTH%> >
                         <col style = <%=DisplayWidth.MONEY_WIDTH%> >
                         <col style = <%=DisplayWidth.PERCENT_WIDTH%>>

                         <col style = <%=DisplayWidth.MONEY_WIDTH%> >
                         <col style = <%=DisplayWidth.MONEY_WIDTH%> >
                         <col style = <%=DisplayWidth.PERCENT_WIDTH%>>

                         <col style = <%=DisplayWidth.MONEY_WIDTH%> >
                         <col style = <%=DisplayWidth.MONEY_WIDTH%> >
                         <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
                     </colgroup>
                <tr >
                  <td  class="resultLabel" style='text-align:center;font-weight:bold' rowspan="2" > 指标</td>
                  <td  class="resultLabel" style='text-align:center;font-weight:bold' rowspan="2"  bordercolor="#214597"> 本期值</td>
                  <td  class="resultLabel" style='text-align:center;font-weight:bold' colspan="3"> 与上期比较</td>
                  <td  class="resultLabel" style='text-align:center;font-weight:bold' colspan="3">与去年同期比较</td>
                  <td  class="resultLabel" style='text-align:center;font-weight:bold' colspan="3"> 与年平均比较</td>
                </tr>
                <tr >
                  <td  class="resultLabel" style='text-align:center;font-weight:bold'> 上期值</td>
                  <td  class="resultLabel" style='text-align:center;font-weight:bold'>差异</td>
                  <td  class="resultLabel" style='text-align:center;font-weight:bold'> <div align="center">差异率</div></td>
                  <td  class="resultLabel" style='text-align:center;font-weight:bold'>去年同期值</td>
                  <td  class="resultLabel" style='text-align:center;font-weight:bold'>差异</td>
                  <td  class="resultLabel" style='text-align:center;font-weight:bold'> 差异率</td>
                  <td  class="resultLabel" style='text-align:center;font-weight:bold'> 平均值</td>
                  <td  class="resultLabel" style='text-align:center;font-weight:bold'>差异</td>
                  <td  class="resultLabel" style='text-align:center;font-weight:bold'> 差异率</td>
                </tr>
                <%if(request.getAttribute("storeThisMonth")!=null&&request.getAttribute("storeDayThisMonth")!=null&&request.getAttribute("drugThisMonth")!=null&&
              request.getAttribute("drugDayThisMonth")!=null&&request.getAttribute("fThisMonth")!=null&&request.getAttribute("fDayThisMonth")!=null&&
              !request.getAttribute("storeThisMonth").equals("?")&&!request.getAttribute("storeDayThisMonth").equals("?")&&!request.getAttribute("drugThisMonth").equals("?")&&
              !request.getAttribute("drugDayThisMonth").equals("?")&&!request.getAttribute("fThisMonth").equals("?")&&!request.getAttribute("fDayThisMonth").equals("?"))
               {%>
                <tr >
                  <td nowrap class="normalText" >(1)库存物资周转率</td>
                  <td >&nbsp;</td>
                  <td >&nbsp;</td>
                  <td >&nbsp;</td>
                  <td >&nbsp;</td>
                  <td >&nbsp;</td>
                  <td >&nbsp;</td>
                  <td >&nbsp;</td>
                  <td >&nbsp;</td>
                  <td >&nbsp;</td>
                  <td >&nbsp;</td>
                </tr>
                <tr >
                  <td nowrap class="normalText" >库存物资周转次</td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeThisMonth")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeUpMonthValue")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("stroeMinThisUpMon")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeMinRateThisUpMon")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeUpYearValue")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeMinThisUpYear")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeMinRateThisUpYear")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeThisYearAve")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeMinThisYearAve")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeMinRateThisAveyear")%></td>
                </tr>
                <tr >
                  <td nowrap class="normalText">库存物资周转天</td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeDayThisMonth")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeDayUpMonthValue")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("stroeDayMinThisUpMon")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeDayMinRateThisUpMon")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeDayUpYearValue")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeDayMinThisUpYear")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeDayMinRateThisUpYear")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeDayThisYearAve")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeDayMinThisYearAve")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("storeDayMinRateThisAveyear")%></td>
                </tr>
                <tr >
                  <td nowrap class="normalText">(2)药品周转率</td>
                  <td nowrap  >&nbsp;</td>
                  <td nowrap  >&nbsp;</td>
                  <td nowrap  >&nbsp;</td>
                  <td nowrap  >&nbsp;</td>
                  <td nowrap  >&nbsp;</td>
                  <td nowrap  >&nbsp;</td>
                  <td nowrap  >&nbsp;</td>
                  <td nowrap  >&nbsp;</td>
                  <td nowrap  >&nbsp;</td>
                  <td nowrap >&nbsp;</td>
                </tr>
                <tr >
                  <td nowrap  class="normalText">药品周转次</td>
                  <td nowrap  class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugThisMonth")%></td>
                  <td nowrap  class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugUpMonthValue")%></td>
                  <td nowrap  class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugMinThisUpMon")%></td>
                  <td nowrap  class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugMinRateThisUpMon")%></td>
                  <td nowrap  class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugUpYearValue")%></td>
                  <td nowrap  class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugMinThisUpYear")%></td>
                  <td nowrap  class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugMinRateThisUpYear")%></td>
                  <td nowrap  class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugThisYearAve")%></td>
                  <td nowrap  class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugMinThisYearAve")%></td>
                  <td nowrap  class="numberText"  style='text-align:right'  ><%=(String)request.getAttribute("drugMinRateThisAveyear")%></td>
                </tr>
                <tr >
                  <td nowrap class="normalText">药品周转天</td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugDayThisMonth")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugDayUpMonthValue")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugDayMinThisUpMon")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugDayMinRateThisUpMon")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugDayUpYearValue")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugDayMinThisUpYear")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugDayMinRateThisUpYear")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugDayThisYearAve")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugDayMinThisYearAve")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("drugDayMinRateThisAveyear")%></td>
                </tr>
                <tr >
                  <td nowrap class="normalText">(3)其它应收帐款周转率</td>
                  <td nowrap >&nbsp;</td>
                  <td nowrap >&nbsp;</td>
                  <td nowrap >&nbsp;</td>
                  <td nowrap >&nbsp;</td>
                  <td nowrap >&nbsp;</td>
                  <td nowrap >&nbsp;</td>
                  <td nowrap >&nbsp;</td>
                  <td nowrap >&nbsp;</td>
                  <td nowrap >&nbsp;</td>
                  <td nowrap >&nbsp;</td>
                </tr>
                <tr >
                  <td nowrap class="normalText">应收帐款周转次</td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fThisMonth")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fUpMonthValue")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fMinThisUpMon")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fMinRateThisUpMon")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fUpYearValue")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fMinThisUpYear")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fMinRateThisUpYear")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fThisYearAve")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fMinThisYearAve")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fMinRateThisAveyear")%></td>
                </tr>
                <tr >
                  <td nowrap class="normalText">应收帐款周转天</td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fDayThisMonth")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fDayUpMonthValue")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fDayMinThisUpMon")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fDayMinRateThisUpMon")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fDayUpYearValue")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fDayMinThisUpYear")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fDayMinRateThisUpYear")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fDayThisYearAve")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fDayMinThisYearAve")%></td>
                  <td nowrap class="numberText" style='text-align:right' ><%=(String)request.getAttribute("fDayMinRateThisAveyear")%></td>
                </tr>
              </table>
</fc:webprint>
        <%}%>
    <input type=hidden name="subFunction"/>
    </form>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>
