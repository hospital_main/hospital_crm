<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<xsl:variable name="wnr" select="/root/tbody/tr[1]/td[last()-1]"/>
	<thead>
		<colgroup id="tg">
          <col style = 'width:45mm' />
          <col style = 'width:35mm' />
     </colgroup>
	<tr>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' >收入项目名称</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
	</tr>

	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			 	<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                	<td noWrap="true"><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
