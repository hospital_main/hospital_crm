<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="7"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="7" ></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="7" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="7" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr noWrap="true" class="mainHead">
			<td nowrap="true"  rowspan="2">科室名称</td>
			<td nowrap="true"  colspan="2">收入</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">成本</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">收益</td>
			<td style="display:none"></td>
		</tr>
		 <tr noWrap="true" class="mainHead">
		 	<td style="display:none"></td>
		 	<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
		  </tr>
		  
		</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="v1" select="td[5]"/>
			 	<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                </xsl:when>
		                <xsl:when test="position()=3">
		                	<td noWrap="true">
		                		<xsl:value-of select="."/>
		                	</td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()=6">
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
			<td noWrap="true" colspan="7" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="7" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
