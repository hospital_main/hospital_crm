<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	
	<thead>
	<xsl:variable name="wnr" select="/root/tbody/tr[1]/td[last()-1]"/>	
	<tr noWrap="true" class="mainHead">
		<td nowrap="true"   rowspan="2">科室名称</td>
		<td nowrap="true"   colspan="2">收入</td>
		<td nowrap="true"   colspan="2">成本</td>
		<td nowrap="true"   colspan="2">收益</td>
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true" dataIndex="6"  >本期</td>
		<td noWrap="true" dataIndex="7"  >累计</td>
		<td noWrap="true" dataIndex="8"  >本期</td>
		<td noWrap="true" dataIndex="9"  >累计</td>
		<td noWrap="true" dataIndex="10"  >本期</td>
		<td noWrap="true" dataIndex="11"  >累计</td>
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="v1" select="td[5]"/>
			 	<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                </xsl:when>
		                <xsl:when test="position()=3">
		                	<td noWrap="true">
		                		<xsl:value-of select="."/>
		                	</td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()=6">
		                </xsl:when>
		                <xsl:when test="position()=7">
		                	<td noWrap="true" align="right">
		                		<a href="#" ><xsl:attribute name="onclick">win_sr("<xsl:value-of select="../td[1]"/>","<xsl:value-of select="../td[3]"/>")</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/></a>
		                	</td>
		                </xsl:when>
		                <xsl:when test="position()=9">
		                	<td noWrap="true" align="right">
		                		<a href="#" ><xsl:attribute name="onclick">win_cb("<xsl:value-of select="../td[1]"/>","<xsl:value-of select="../td[3]"/>")</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/></a>
		                	</td>
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
