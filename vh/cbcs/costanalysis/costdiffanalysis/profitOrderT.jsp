
<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costdiffanalysis/profitOrderT.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:57:34 $
  $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DisplayWidth,com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.*,com.viewhigh.cbcs.base.mvc.view.MonthComponent,java.text.DecimalFormat" %>

<%@ page import="java.util.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%DecimalFormat df=new DecimalFormat("###,##0.00");%>
<Script Language="JavaScript">

function account(){
if (template.syear.value == '' || template.fmonth.value == '' || template.tmonth.value == '') {
      alert('请选择查询月份!');
      return false;
    }
    if(template.syear.value == "")
    {
      alert('请选择核算年份!');
      return false;
    }
    if(template.fmonth.value == "")
    {
      alert('请选择核算开始月份!');
      return false;
    }
    if(template.tmonth.value == "")
    {
      alert('请选择核算结束月份!');
      return false;
    }
    if(template.fmonth.value > template.tmonth.value ) {
      alert('开始月份不能晚于结束月份!');
      return false;
    }
  template.subFunction.value='count'
  show_wait()
  setCBCS_Year1(template.syear.value);
    setCBCS_Month1(template.fmonth.value);
    setCBCS_Month2(template.tmonth.value);
  template.submit()
}
function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }
  function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();
  	
  	year_1  ? template.syear.value=year_1 : "";
    month_1 ? template.fmonth.value=month_1 : "";
    month_2 ? template.tmonth.value=month_2: "";
  }
function printData()
{
	account();
	return printForSort(eval('sortTable').sortColIndex, eval('sortTable').sortFlag);
}
function doKeyDown()
{
	if(event.keyCode==13)
	{
		return false;		
	}
}
</Script>
<script language='javascript' src='javascript/taxis.js'></script>

	  <%
	      String[][] result = (String[][])request.getAttribute("result");
	      String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
	      String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
	Calendar thisMonth=Calendar.getInstance(); 
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;
   	  %>
<html:html clazz="main">
	<form name="template" method="post" action="profitOrderTec.jspviewhigh">
<script language='javascript' src='javascript/taxis.js'></script>
	  <html:message/>

	  <html:title clazz='module'>医技科室收益排名表(医成本C6-03表)</html:title>

	  <!-- 简单信息 -->
	<html:table clazz="simple">
		<tr> 
			<td>核算月：</td>
			<td> 
			<%=new Select(year,"syear",request.getParameter("syear")==null? String.valueOf(thisMonth.get(Calendar.YEAR)):request.getParameter("syear"),false,true)%>年
			<%=new Select(month,"fmonth",request.getParameter("fmonth")==null? curMonth:request.getParameter("fmonth"),false,true)%>月 到 
			<%=new Select(month,"tmonth",request.getParameter("tmonth")==null? curMonth:request.getParameter("tmonth"),false,true)%>月
			</td>
			<td>科室级别:</td>
			<td>
			<%=new SingleSelect(request.getAttribute("deptgrade"),"deptgrade",request.getParameter("deptgrade"),false,true)%>
			</td>
    	</tr>
    	<tr>
    		<td colspan="2">
				<input type="text" class="findVhTabCtn" onkeydown="return doKeyDown();"/>
    		</td>
			<td colspan="2">
			<button class="pageBtn" onclick="return account();">计算</button>
			<%if(result!=null&&result[0][0]!=null){%>
				<button class="pageBtn" onclick="return printData();">打印</button>
				<!--<img src="images/print.gif" class="mouse" onclick="return printForSort(eval('sortTable').sortColIndex, eval('sortTable').sortFlag);"/>-->
				<button class="pageBtn" onclick="return saveData();">保存历史</button>
			<%}%>
			</td>
		</tr>
	</html:table>

	<html:title clazz='table'>医技科室收益排名表</html:title>

	  <!-- 复杂信息 -->


    <vh:vhFixTable id='sortTable' fixRow=2 fixCol=1 colOffset=1>
			<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
        <colgroup id=tg>
            <col style = <%=DisplayWidth.NAME_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
          </colgroup>
           <tr >
        <td nowrap class="resultLabel" rowspan="2" >科室名称</td>
        <td nowrap  colspan="2" class="resultLabel" >收入</td>
        <td nowrap colspan="2" class="resultLabel" >成本</td>
        <td nowrap colspan="2" class="resultLabel" >收益</td>
     </tr>
     <tr>
          <td nowrap class="resultLabel" >本期</td>
          <td nowrap class="resultLabel" >累计</td>
          <td nowrap class="resultLabel" >本期</td>
          <td nowrap class="resultLabel" >累计</td>
          <td nowrap class="resultLabel" >本期</td>
          <td nowrap class="resultLabel" >累计</td>
     </tr>
     <%if(result!=null){%>
        <%
           for (int j=0;j<result.length;j++){
            if(result[j][0]==null){break;}%>
            <tr>
              <td nowrap class="normalText">
            <%
            if(result[j][1]!=null){
              if(result[j][2].equals("TOP")){
                out.print(result[j][1]);
                }else{
                  out.print(result[j][0]);
                }
                 }%>
                </td>
            <%if(result[j][1]!=null){
              if(result[j][2].equals("TOP")){%>
            <%

              //String openwin="<a href = \"javascript:var win = window.open('apporDeptSumDetail.jspviewhigh?subFunction=apporDeptSumDetail&sumByWhat=dept&value="+result[j][0]+"&OorI="+request.getParameter("deptproper")+"&year_month="+request.getParameter("syear")+request.getParameter("fmonth")+"&name="+result[j][1].toString()+"', 'child', 'width=800, height=580, top=100, left=100, scrollbars = 1, resizable = 1');\">";
               String openwin="<a href = \"javascript:var win = window.open('apporDeptSumDetail.jspviewhigh?subFunction=apporDeptSumDetail&appDeptTypeCode=C&sumByWhat=dept&value="+result[j][0]+"&OorI="+request.getParameter("deptproper")+"&year_month="+request.getParameter("syear")+request.getParameter("fmonth")+"&year_month2="+request.getParameter("syear")+request.getParameter("tmonth")+"&name="+result[j][1].toString()+"', 'child', 'width=800, height=580, top=100, left=100, scrollbars = 1, resizable = 1');\">";
              String openwin1="<a href = \"javascript:var win = window.open('profitOrder.jspviewhigh?subFunction=subIncoming&sumByWhat=dept&deptcode="+result[j][0]+"&OorI="+request.getParameter("deptproper")+"&depttype="+request.getParameter("depttype")+"&syear="+request.getParameter("syear")+"&fmonth="+request.getParameter("fmonth")+"&tmonth="+request.getParameter("tmonth")+"&amounts="+df.format(Double.parseDouble(result[j][4]))+"&level="+request.getParameter("deptgrade")+"&name="+result[j][1].toString()+"', 'child', 'width=800, height=580, top=100, left=100, scrollbars = 1, resizable = 1');\">";
            for(int k=4;k<10;k++){%>
          <td class="result" nowrap  style='text-align:right' ><%if(k==4){%><%=openwin1+df.format(Double.parseDouble(result[j][k]))+"</a>"%><%}else if(k==6){%><%=openwin+df.format(Double.parseDouble(result[j][k]))+"</a>"%><%}else{%><%=df.format(Double.parseDouble(result[j][k]))%><%}%></td>
          <%}%>
          
           <%} else if(!result[j][2].equals("TOP") && result[j].length>8){%>
	          <% 
	          //科室级别不是一级和末级时执行
			String openwin="<a href = \"javascript:var win = window.open('apporDeptSumDetail.jspviewhigh?subFunction=apporDeptSumDetail&appDeptTypeCode=C&sumByWhat=dept&value="+result[j][7]+"&OorI="+request.getParameter("deptproper")+"&year_month="+request.getParameter("syear")+request.getParameter("fmonth")+"&year_month2="+request.getParameter("syear")+request.getParameter("tmonth")+"&name="+result[j][0].toString()+"', 'child', 'width=800, height=580, top=100, left=100, scrollbars = 1, resizable = 1');\">";
			String openwin1="<a href = \"javascript:var win = window.open('profitOrder.jspviewhigh?subFunction=subIncoming&sumByWhat=dept&deptcode="+result[j][7]+"&OorI="+request.getParameter("deptproper")+"&depttype="+request.getParameter("depttype")+"&syear="+request.getParameter("syear")+"&fmonth="+request.getParameter("fmonth")+"&tmonth="+request.getParameter("tmonth")+"&level="+request.getParameter("deptgrade")+"&amounts="+df.format(Double.parseDouble(result[j][1]))+"&name="+result[j][0].toString()+"&dept_grade="+result[j][8]+"', 'child', 'width=800, height=580, top=100, left=100, scrollbars = 1, resizable = 1');\">";
			for(int k=1;k<result[0].length-2;k++){//显示同末级,比末级多一列级别,不显示后边的两列%>
			<td class="result" nowrap  style='text-align:right' ><%if(k==3){%>
			<%=openwin+df.format(Double.parseDouble(result[j][k]))+"</a>"%><%}else if(k==1){%><%=openwin1+df.format(Double.parseDouble(result[j][k]))+"</a>"%><%}else{%><%=df.format(Double.parseDouble(result[j][k]))%><%}%></td>
			<%}%>
			
            <%
              }else{
              String openwin="<a href = \"javascript:var win = window.open('apporDeptSumDetail.jspviewhigh?subFunction=apporDeptSumDetail&appDeptTypeCode=C&sumByWhat=dept&value="+result[j][7]+"&OorI="+request.getParameter("deptproper")+"&year_month="+request.getParameter("syear")+request.getParameter("fmonth")+"&year_month2="+request.getParameter("syear")+request.getParameter("tmonth")+"&name="+result[j][0].toString()+"', 'child', 'width=800, height=580, top=100, left=100, scrollbars = 1, resizable = 1');\">";
              String openwin1="<a href = \"javascript:var win = window.open('profitOrder.jspviewhigh?subFunction=subIncoming&sumByWhat=dept&deptcode="+result[j][7]+"&OorI="+request.getParameter("deptproper")+"&depttype="+request.getParameter("depttype")+"&syear="+request.getParameter("syear")+"&fmonth="+request.getParameter("fmonth")+"&tmonth="+request.getParameter("tmonth")+"&level="+request.getParameter("deptgrade")+"&amounts="+df.format(Double.parseDouble(result[j][1]))+"&name="+result[j][0].toString()+"', 'child', 'width=800, height=580, top=100, left=100, scrollbars = 1, resizable = 1');\">";
              for(int k=1;k<result[0].length-1;k++){%>
          <td class="result" nowrap  style='text-align:right' ><%if(k==3){%><%=openwin+df.format(Double.parseDouble(result[j][k]))+"</a>"%><%}else if(k==1){%><%=openwin1+df.format(Double.parseDouble(result[j][k]))+"</a>"%><%}else{%><%=df.format(Double.parseDouble(result[j][k]))%><%}%></td>
          <%}
              }
                }%>
             </tr>
          <%}
          }%>
     </table>
     </vh:vhFixTable>
	  <input type=hidden name="subFunction"/>	  
	  <input type=hidden name='depttype' value='C'>
	</form>
</html:html>
