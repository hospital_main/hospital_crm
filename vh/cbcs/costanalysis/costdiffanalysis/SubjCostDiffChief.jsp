<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costdiffanalysis/SubjCostDiffChief.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime:$
 $Revision: 1.1 $
-->



<%@ page language = "java" contentType = "text/html;charset=GBK" errorPage = "../../../error.jsp" %>
<%@ page import = "java.util.*, java.text.*, com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
	Calendar thisMonth=Calendar.getInstance();
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;
%>
<%!
  public String getColor(String value) {

      int index = value.indexOf("-");

      if(index > -1) {
        return " style=\"color:red\" ";
      } else {
        return "";
      }

  }

%>



<script language="javascript">

    function analyze() {

      if(template.year_month.value == '')
      {
        alert('请选择查询年月!');
        return false;
      }
      else
      {
        show_wait();
        setCBCS_Year1(template.year_month.value.substr(0,4));
		    setCBCS_Month1(template.year_month.value.substr(4,2));
		    setCBCS_Month2(template.year_month.value.substr(4,2));
        return template.submit();


      }
    }

    function preparedPrin() {
    var win = window.open("subjCostDiffChief.jspviewhigh?subFunction=preparedPrint&year_month=<%=request.getParameter("year_month")%>","","width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1")
    }
function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }
  function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();

  	if(year_1 && month_1 && month_2){
    	template.year_month.setValue(year_1+""+month_1)
    }
  }

	function getRsultXml(){//将显示结果拼成XML格式的字串
	  var xmlStr = "<?xml version='1.0' encoding='GBK'?><root>";
	  var tbody = document.getElementById("result_data");//表体
	  var thead = document.getElementById("result_head");//表头
	  var tbodyStr = "<tbody>"; //表体字串
	  var theadStr = "<thead>"; //表头字串
	  var trs = tbody.getElementsByTagName("tr");
	  var table_width = trs[0].childNodes.length;
	  for(var i = 0; i < trs.length; i++){
		tbodyStr += "<tr>";
		var tds = trs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
            if(j == 0 && tds[j].childNodes[0].nodeType != 3) //有超链接
                tbodyStr += "<td>"+tds[j].childNodes[0].innerHTML+"</td>";
            else
			    tbodyStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		tbodyStr += "</tr>";
	  }
	  tbodyStr += "</tbody>";

	  var htrs = thead.getElementsByTagName("tr");
	  for(var i = 0; i < htrs.length; i++){
		theadStr += "<tr>";
		var tds = htrs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
			theadStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		theadStr += "</tr>";
	  }
	  theadStr += "</thead>";
	  xmlStr += theadStr + tbodyStr + "</root>";
      return xmlStr.replace(/\&nbsp;&nbsp;/g, "　").replace(/\&nbsp;/g, "");
	}

	function datefromat (){
        var myDate=new Date();
    	var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
    	var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
    	var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
    	year='0000'+year;year=year.substring(year.length-4);
    	month='00'+month;month=month.substring(month.length-2);
    	day='00'+day; day=day.substring(day.length-2);

		return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString();
    }

  	function print(){

      	var printTitle="成本项目比较分析表(医成本C4-03表)";
		var info = "制作日期: "+datefromat();
        var ym = document.all["year_month"].value;
      	var data=new Object();
      	data["thead_1_1"]= printTitle;
      	data["thead_2_1"]= ym.substring(0,4)+"年"+ym.substring(4)+"月";
      	data["thead_3_1"]= "金额单位：元";

      	data["tfoot_1_1"]= info;
      	data["tfoot_2_1"]="北京望海康信科技有限公司  制作";

      	printXmlToCellByXsltFile(getRsultXml(), 'cbcs/costanalysis/costdiffanalysis/DeptCostDiffChiefPrintView.xsl', data, false, false);
	}
</script>

<html:html clazz="main" isPrint="true" fixCols="2">
	<form name="template" method="post" action="subjCostDiffChief.jspviewhigh" />
		<html:message/>

		<html:title clazz='module'>成本项目比较分析表(医成本C4-03表)</html:title>

    <table  width="100%" cellspacing="2" border="0" >
      <tr>
        <td class="signText" nowrap width="2%" >核算月：</td>
        <td class="signText"><%=new MonthComponent("year_month", (request.getParameter("year_month")==null?(String.valueOf(thisMonth.get(Calendar.YEAR))+curMonth):(request.getParameter("year_month")))) %></td>
        <td><button class="pageBtn" onclick="return analyze();">计算</button></td>
        <%
        String[][] result = (String[][])request.getAttribute("table_result");
        if (result != null && result.length > 1) {%>
        
        <td><button class="pageBtn" onclick="return print()//preparedPrin();">打印</button></td>
        <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
        <%}%>
      </tr>
      <tr >
	  <td colspan="2" ><br> <input type="text" class="findVhTabCtn"/></td>
	  </tr>
    </table>

    <br>

    <html:title clazz='table'>成本项目比较分析汇总表</html:title>
<vh:vhFixTable fixRow=2 fixCol=1>
     <table border='1' bgColor=white borderColor=black style='border:2px solid #000000;BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
      <colgroup id=tg>
				<col style = <%=DisplayWidth.NAME_WIDTH%> >
				<col style = <%=DisplayWidth.MONEY_WIDTH%> >
				<col style = <%=DisplayWidth.MONEY_WIDTH%> >
				<col style = <%=DisplayWidth.MONEY_WIDTH%> >
				<col style = <%=DisplayWidth.PERCENT_WIDTH%> >
				<col style = <%=DisplayWidth.MONEY_WIDTH%> >
				<col style = <%=DisplayWidth.MONEY_WIDTH%> >
				<col style = <%=DisplayWidth.PERCENT_WIDTH%> >
				<col style = <%=DisplayWidth.MONEY_WIDTH%> >
				<col style = <%=DisplayWidth.MONEY_WIDTH%> >
				<col style = <%=DisplayWidth.PERCENT_WIDTH%> >
				<col style = <%=DisplayWidth.MONEY_WIDTH%> >
				<col style = <%=DisplayWidth.MONEY_WIDTH%> >
				<col style = <%=DisplayWidth.PERCENT_WIDTH%> >
      </colgroup>
<thead id="result_head">
      <tr>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">成本项目</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">本期数值</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与同期比较</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与上期比较</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与预算比较</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与平均比较</td>
      </tr>

      <tr>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >同期数值</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >上期数值</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >预算数值</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >平均数值</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
      </tr>
</thead>
<tbody id="result_data">
<%

             if (result != null && result.length > 1){
              DecimalFormat percentFormat = new DecimalFormat("#0.00%");
              DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
              String[] total = null;
              for (int i = 0; i < result.length; i++ )
              {

                /** reformat */
                for( int j = 3; j <= 15; j++)
                {
                  if(result[i][j] == null || result[i][j].trim().equals(""))
                  {
                    result[i][j] = "&nbsp;&nbsp;&nbsp;";
                    continue;
                  }

		  		/** percent format */
                  if(j == 6 || j == 9 || j == 12 ||j == 15)
                  {
                    result[i][j] =
                      percentFormat.format(Double.parseDouble(result[i][j]));
                    continue;
                  }

                  /** money format */
                  result[i][j] =   moneyFormat.format(Double.parseDouble(result[i][j]));

                }

                if(result[i][1].equals("合计"))
                {
                    total = result[i];
                }
                else
                {

%>

	<tr valign="middle"  bordercolor="#000000" bgcolor="#FFFFFF" class="a12">
    <td   class="normalText" nowrap>
      <a href = "javascript:var win = window.open('subjCostDiffDetail.jspviewhigh?subFunction=diffAnalysis&sumByWhat=costSubj&year_month=<%=request.getParameter("year_month")%>&value=<%=result[i][0]%>&name=<%=result[i][1]%>', 'child', 'width=800, height=580, left=100, top=100, scrollbars = 1, resizable = 1');"><%for(int k = 0; k < Integer.parseInt(result[i][2]) - 1; k++) out.print("&nbsp;");%><%=result[i][1]%></a>
    </td>

    <td   class="numberText" style='text-align:right' nowrap><%=result[i][3]%></td>
    <td   <%out.print(getColor(result[i][4]));%> class="numberText" style='text-align:right' nowrap><%=result[i][4]%></td>
    <td   <%out.print(getColor(result[i][5]));%> class="numberText" style='text-align:right' nowrap><%=result[i][5]%></td>
    <td   <%out.print(getColor(result[i][6]));%> class="numberText" style='text-align:right' nowrap><%=result[i][6]%></td>

    <td   <%out.print(getColor(result[i][7]));%> class="numberText"  nowrap><%=result[i][7]%></td>
    <td   <%out.print(getColor(result[i][8]));%> class="numberText"  nowrap><%=result[i][8]%></td>
    <td   <%out.print(getColor(result[i][9]));%> class="numberText"  nowrap><%=result[i][9]%></td>

    <td   <%out.print(getColor(result[i][10]));%> class="numberText"  nowrap><%=result[i][10]%></td>
    <td   <%out.print(getColor(result[i][11]));%> class="numberText"  nowrap><%=result[i][11]%></td>
    <td   <%out.print(getColor(result[i][12]));%> class="numberText"  nowrap><%=result[i][12]%></td>

    <td   <%out.print(getColor(result[i][13]));%> class="numberText"  nowrap><%=result[i][13]%></td>
    <td   <%out.print(getColor(result[i][14]));%> class="numberText"  nowrap><%=result[i][14]%></td>
    <td   <%out.print(getColor(result[i][15]));%> class="numberText"  nowrap><%=result[i][15]%></td>


	</tr>
<%
                    }
                }
}

%>
</tbody>
		 </table>
    </vh:vhFixTable>
		<input type="hidden" name="subFunction" value="diffAnalysis">
	</form>
</html:html>
