<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="2"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="2" ></td>
			<td style="display:none"></td>
			
		</tr>
		<tr>
			<td noWrap="true" colspan="2" align="left"></td>
			<td style="display:none"></td>
			
		</tr>
		<tr>
			<td noWrap="true" colspan="2" align="left"></td>
			<td style="display:none"></td>
		
		</tr>
		<tr noWrap="true" class="mainHead">
			<td nowrap="true"  >收入项目名称</td>
			<td nowrap="true"  >金额</td>
		</tr>
		 
		</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			 	<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                	<td noWrap="true"><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" align="right" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
			<td noWrap="true" colspan="2" align="right"></td>
			<td style="display:none"></td>
			
		</tr>
		<tr>
			<td noWrap="true" colspan="2" align="right"></td>
			<td style="display:none"></td>
			
		</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
