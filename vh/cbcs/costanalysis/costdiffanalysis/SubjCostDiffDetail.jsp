<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costdiffanalysis/SubjCostDiffDetail.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime:$
 $Revision: 1.1 $
-->


<%@ page language = "java" contentType = "text/html;charset=GBK" errorPage = "../../../../error.jsp" %>
<%@ page import = "java.util.*" %>
<%@ page import = "java.text.*" %>
<%@ page import = "com.viewhigh.cbcs.base.mvc.view.MonthComponent ,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<%!
  public String getColor(String value) {

      int index = value.indexOf("-");

      if(index == -1) {

        if(value.equals("0.00") || value.equals("&nbsp;&nbsp;&nbsp;") || value.equals("0.00%")) {
          return "";
        }
        return " style=\"color:red\" ";
      } else {
        return "";
      }

  }

%>
    <%
        String sumByWhat = request.getParameter("sumByWhat");
        String name = request.getParameter("name");//new String(request.getParameter("name").getBytes("ISO8859_1"), "GBK");
        String yearMonth = request.getParameter("year_month");
        String yearMonthStr = "核算月: " + yearMonth.substring(0,4) + "年" + Integer.parseInt(yearMonth.substring(4)) + "月";
        String moduleTitle = "";

        if(sumByWhat != null && sumByWhat.equals("costSubj") && name != null) {
            moduleTitle = "成本项目: " + name + "   " + yearMonthStr;
        }
        else if(sumByWhat != null && sumByWhat.equals("totalSum")) {
          moduleTitle = "全院: " + "   " + yearMonthStr;
        }
        String nn = moduleTitle.substring(0,moduleTitle.length()-yearMonthStr.length());
    %>
<Script language="javascript">
  function preparedPrin() {
    var win = window.open("subjCostDiffDetail.jspviewhigh?subFunction=preparedPrint&year_month=<%=request.getParameter("year_month")%>&name=<%=nn%>&sumByWhat=<%=request.getParameter("sumByWhat")%>&value=<%=request.getParameter("value")%>","","width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1")
  }
  function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav&alertwindow=aw","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }

	function getRsultXml(){//将显示结果拼成XML格式的字串
	  var xmlStr = "<?xml version='1.0' encoding='GBK'?><root>";
	  var tbody = document.getElementById("result_data");//表体
	  var thead = document.getElementById("result_head");//表头
	  var tbodyStr = "<tbody>"; //表体字串
	  var theadStr = "<thead>"; //表头字串
	  var trs = tbody.getElementsByTagName("tr");
	  var table_width = trs[0].childNodes.length;
	  for(var i = 0; i < trs.length; i++){
		tbodyStr += "<tr>";
		var tds = trs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
            if(j == 0 && tds[j].childNodes[0].nodeType != 3) //有超链接
                tbodyStr += "<td>"+tds[j].childNodes[0].innerHTML+"</td>";
            else
			    tbodyStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		tbodyStr += "</tr>";
	  }
	  tbodyStr += "</tbody>";

	  var htrs = thead.getElementsByTagName("tr");
	  for(var i = 0; i < htrs.length; i++){
		theadStr += "<tr>";
		var tds = htrs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
			theadStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		theadStr += "</tr>";
	  }
	  theadStr += "</thead>";
	  xmlStr += theadStr + tbodyStr + "</root>";
      return xmlStr.replace(/\&nbsp;&nbsp;/g, "　").replace(/\&nbsp;/g, "");
	}

	function datefromat (){
        var myDate=new Date();
    	var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
    	var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
    	var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
    	year='0000'+year;year=year.substring(year.length-4);
    	month='00'+month;month=month.substring(month.length-2);
    	day='00'+day; day=day.substring(day.length-2);

		return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString();
    }

  	function print(){

      	var printTitle="成本项目比较分析明细表(医成本C4-04表)";
		var info = "制作日期: "+datefromat();
      	var data=new Object();
      	data["thead_1_1"]= printTitle;
      	data["thead_2_1"]= "<%=moduleTitle%>";
      	data["thead_3_1"]= "金额单位：元";

      	data["tfoot_1_1"]= info;
      	data["tfoot_2_1"]="北京望海康信科技有限公司  制作";

      	printXmlToCellByXsltFile(getRsultXml(), 'cbcs/costanalysis/costdiffanalysis/DeptCostDiffChiefPrintView.xsl', data, false, false);
	}
</Script>

<html:html clazz="child" isPrint="true" fixCols="2">
  <html:message/>

  <html:title clazz='module'>成本项目比较分析明细表(医成本C4-04表) <%=moduleTitle%></html:title>

  <table  width="100%" cellspacing="2" border="0" >
    <tr>
        <%
        String[][] result = (String[][])request.getAttribute("table_result");
        if (result != null && result.length > 1) {%>
        <td><button class="pageBtn" onclick="return print() // preparedPrin();">打印</button></td>
        <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
        <%}%>
      <td align='center'><button class="pageBtn" onClick="window.close();" >关闭</button></td>
    </tr>
 	</table>
  <br>

  <html:title clazz='table'>成本项目比较分析明细表</html:title>

<vh:vhFixTable fixRow=2 fixCol=1>
     <table border='1' bgColor=white borderColor=black style='border:2px solid #000000;BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
        <colgroup id=tg>
      <col style = <%=DisplayWidth.NAME_WIDTH%> >
      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
       <col style = <%=DisplayWidth.MONEY_WIDTH%> >
      <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
      <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
      <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
      <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
    </colgroup>
    <thead id="result_head">
       <tr>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">科室</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">本期成本</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与同期比较</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与上期比较</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与预算比较</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与平均比较</td>
    </tr>

    <tr>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >同期成本</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>

      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >上期成本</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >预算成本</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >平均成本</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
    </tr>
    </thead>
    <tbody id="result_data">
        <%

        if (result != null){

              DecimalFormat percentFormat = new DecimalFormat("#0.00%");
              DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
              String[] total = null;
              for(int i = 0; i < result.length; i++ )
              {
                /** reformat */
                for( int j = 6; j <= 18; j++)
                {
                  if(result[i][j] == null || result[i][j].trim().equals("") || result[i][j].trim().equals("0"))
                  {
                    result[i][j] = "&nbsp;&nbsp;&nbsp;";
                    continue;
                  }

                  /** percent format */
                  if(j == 9 || j == 12 || j == 18)
                  {
                    result[i][j] =
                      percentFormat.format(Double.parseDouble(result[i][j]));
                    continue;
                  }
                  /** money format */
                  result[i][j] =
                    moneyFormat.format(Double.parseDouble(result[i][j]));
                }


                if(result[i][3].equals("类别小计"))
                {
                  int rowSpan = 1;
                  int j = i+1;
                  for( ; j < result.length; j++ )
                  {
                      if( !result[j][0].equals(result[i][0]))
                          break;
                      rowSpan++;
                  }


        %>

                          <tr bgcolor="#E1E1FB" >
                            <td    nowrap><%=result[i][1]%>小计</td>

                            <td   class="numberText" style='text-align:right' nowrap><%=result[i][6]%></td>

                            <td    class="numberText" style='text-align:right' nowrap><%=result[i][13]%></td>

                            <td    <%out.print(getColor(result[i][14]));%> class="numberText" style='text-align:right' nowrap><%=result[i][14]%></td>

                            <td    <%out.print(getColor(result[i][15]));%> class="numberText" style='text-align:right' nowrap><%=result[i][15]%></td>

                            <td    class="numberText" style='text-align:right' nowrap><%=result[i][16]%></td>

                            <td    <%out.print(getColor(result[i][17]));%> class="numberText" style='text-align:right' nowrap><%=result[i][17]%></td>

                            <td    <%out.print(getColor(result[i][18]));%> class="numberText" style='text-align:right' nowrap><%=result[i][18]%></td>

                            <td    class="numberText" style='text-align:right' nowrap><%=result[i][7]%></td>

                            <td    <%out.print(getColor(result[i][8]));%> class="numberText" style='text-align:right' nowrap><%=result[i][8]%></td>

                            <td    <%out.print(getColor(result[i][9]));%> class="numberText" style='text-align:right' nowrap><%=result[i][9]%></td>

                            <td   class="numberText" style='text-align:right' nowrap><%=result[i][10]%></td>

                            <td   <%out.print(getColor(result[i][11]));%> class="numberText" style='text-align:right' nowrap><%=result[i][11]%></td>

                            <td  <%out.print(getColor(result[i][12]));%> class="numberText" style='text-align:right' nowrap><%=result[i][12]%></td>
                          </tr>
                    <%
			}
                        else
                        {
                    %>
                          <tr >
                            <td class="normalText" nowrap><%for(int k = 0; k < Integer.parseInt(result[i][4]) - 1; k++) out.print("&nbsp;");%><%=result[i][3]%></td>
                            <td class="numberText" style='text-align:right' nowrap><%=result[i][6]%></td>
                            <td class="numberText" style='text-align:right' nowrap><%=result[i][13]%></td>
                            <td class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][14]));%>><%=result[i][14]%></td>
                            <td class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][15]));%>><%=result[i][15]%></td>
                            <td class="numberText" style='text-align:right' nowrap><%=result[i][16]%></td>
                            <td class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][14]));%>><%=result[i][17]%></td>
                            <td class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][15]));%>><%=result[i][18]%></td>
                            <td class="numberText" style='text-align:right' nowrap><%=result[i][7]%></td>
                            <td class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][8]));%>><%=result[i][8]%></td>
                            <td class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][9]));%>><%=result[i][9]%></td>
                            <td class="numberText" style='text-align:right' nowrap><%=result[i][10]%></td>
                            <td class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][11]));%>><%=result[i][11]%></td>
                            <td class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][12]));%>><%=result[i][12]%></td>
                          </tr>
                    <%
                        }
                    }
}

                    %>
           </tbody>
 </table>
    </vh:vhFixTable>
</html:html>
