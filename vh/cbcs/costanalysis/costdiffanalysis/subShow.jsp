<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costdiffanalysis/subShow.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,com.viewhigh.cbcs.cbcs.util.DisplayWidth,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO,java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function group() {
    if (template.year_month.value == "") {
      alert("请选择年月！");
      return false
    }
	  template.subFunction.value = "calculateAssignDeptBenefit";
    show_wait();
    template.submit();
    return true;
	}
  function init()
  {
	  window.prefix = opener.window.prefix;
  }
</script>
<html:html clazz="main">
<br>
<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<% String titl="";
 if(request.getParameter("fmonth").equals(request.getParameter("tmonth")))
      titl="项目收入明细表 "+"科室:"+request.getParameter("name")+" 核算月:"+request.getParameter("syear")+"年"+request.getParameter("fmonth")+"月";
 else titl="项目收入明细表 "+"科室:"+request.getParameter("name")+" 核算月:"+request.getParameter("syear")+"年"+request.getParameter("fmonth")+"月到"+request.getParameter("tmonth")+"月";
%>
<%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle(titl)%>
<%
  String[][] result = (String[][])request.getAttribute("result");
  DecimalFormat mf=new DecimalFormat("#,##0.00");
%>
<form name="template" method="post" action="assignDeptBenefit.jspviewhigh">

	<table width=100%>
	  <tr>
	    <td  align="center"><button class="pageBtn" onclick="init();preparedPrint(1);">打印</button></td>
	    <td align="center"><button class="pageBtn" onClick="window.close();" >关闭</button></td>
	  </tr>
  </table>

<!-- 简单信息 -->
		<html:title clazz='table'>项目收入明细表</html:title>

<!-- 复杂信息 -->
    <vh:vhFixTable fixRow=1 fixCol=1>
			<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
          <colgroup id=tg>
            <col style = <%=DisplayWidth.NAME_WIDTH%> >
            <col style = <%=DisplayWidth.NAME_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
          </colgroup>
				  <tr>
				    <td  nowrap  class="resultLabel" style='text-align:center;font-weight:bold' >收入项目名称</td>
				    <td  nowrap  class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
				  </tr>
				  <%
					  if (result!=null) {
					    for (int i=0; i<result.length; i++) {
					      String primaryKey = result[ i ][ 0 ];
					      for(int j=0;j<result[i].length;j++){
					        if(result[i][j]==null||result[i][j].equals(""))
					          result[i][j]="&nbsp;";
					      }

					      String rowColor = "rowGray";
          		  if (i/2*2==i) rowColor = "rowWhite";

				  %>
				  <tr class="<%=rowColor%>">
				    <td class="normalText"><%= result[i][0]%></td>
				    <td class="result" align="right"><%= mf.format(Double.parseDouble(result[i][1]))%></td>
				  </tr>
				  <%

					  }
				  %>
				  <tr class="rowWhite">
				    <td class="normalText">合 计</td>
				    <td class="result" align="right"><%=request.getParameter("amounts")%></td>
				  </tr>
				<%
				  }
				%>
				</table>
</vh:vhFixTable>
<input type="hidden" name="subFunction">
</form>
</html:html>



