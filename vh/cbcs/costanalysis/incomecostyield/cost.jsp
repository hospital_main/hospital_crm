<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/incomecostyield/cost.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:57:34 $
     $Modtime: 03-09-16 16:57 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.margin.Header,
                 com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>
<%
  Header header = new Header(101, request);
  header.setPrintStatus(true);
  out.print(header);
%>

<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<Script language="javascript">
  function preparedPrin(){
    if(template.year_month.value=="")
    {
      alert("请先选择月份");
      return;
    }
    // 报表名称
    grid.prn.title1='成 本 指 标';
    // 年月
    var temp=template.year_month.value;
    grid.prn.title2=temp.substring(0,4)+'年'+temp.substring(4,6)+'月';
    // 表头行数
    grid.prn.tabHead=2;
    // 打印
    grid.print();
  }
</Script>
<form name="template" method="post" action="costindx.jspviewhigh">

 <!-- 标题栏 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td  class="moduleTitle" nowrap="nowrap">成本指标：</td>
    </tr>
  </table>
<%String[][] acct_dept=(String[][])request.getAttribute("initial_acct_dept");%>
  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class='signText'>核算月：</td>
      <td class="signText" ><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td>
      <td><button class="pageBtn" onclick="show_wait();template.submit();" >计算</button></td>
      <td><button class="pageBtn" onclick="return preparedPrin();">打印</button></td>
      <td colspan=180></td>
    </tr>
  </table>
  <!-- 结果集 -->
<fc:webprint id="grid">
  <table BORDERCOLOR="#214597" border="1" cellspacing="1" width="100%" >
                   <colgroup id=tg>
                         <col style = <%=DisplayWidth.NAME_WIDTH%>>
                         <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
                         <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
                         <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
                         <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
                         <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
                         <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
                         <col style = <%=DisplayWidth.PERCENT_WIDTH%>>

                     </colgroup>
    <tr bordercolor="#214597" bgcolor="#FFFFFF" class="resultLabel">
      <td nowrap rowspan="2" style='text-align:center;font-weight:bold'> <div align="center">指标<strong> </strong></div></td>
      <td nowrap rowspan="2" style='text-align:center;font-weight:bold' bordercolor="#214597"> <div align="center">本期值</div></td>
      <td nowrap colspan="2" style='text-align:center;font-weight:bold'> <div align="center">与上期比较</div></td>
      <td nowrap colspan="2" style='text-align:center;font-weight:bold'> <div align="center">与去年同期比较</div></td>
      <td nowrap colspan="2" style='text-align:center;font-weight:bold'> <div align="center">与平均比较</div></td>
      <div align="center"></div></td>
    </tr>
    <tr class="a12">
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'> <div align="center">上期值</div></td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'> <div align="center">差异</div></td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'> <div align="center">去年同期值</div></td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'> <div align="center">差异</div></td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'> <div align="center">平均值</div></td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'> <div align="center">差异</div></td>
    </tr>
    <%
      DecimalFormat df = new DecimalFormat("0.00");
      String[][] result = (String[][])request.getAttribute( "table_result" );
      if ( result != null )
      {
        for(int i=0;i<9;i++){
          for(int j=0;j<10;j++){
            if( result[i][j] != null && !result[i][j].equals(""))
              result[i][j]=df.format(Double.parseDouble(result[i][j])*100) + "%";
            else result[i][j]="&nbsp";
          }
        }

        String rowColor = "rowWhite";
    %>

    <tr CLASS="<%=rowColor%>">
      <td nowrap class="normalText" align="left">直接成本率</td>
      <td height="22" class="numberText" style='text-align:right'><%=result[0][0]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[0][1]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[0][2]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[0][4]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[0][5]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[0][7]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[0][8]%></td>
    </tr>
    <tr CLASS="<%=rowColor%>">
      <td width="16%" nowrap class="normalText" align="left">间接成本率</td>
      <td height="22" class="numberText" style='text-align:right'><%=result[1][0]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[1][1]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[1][2]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[1][4]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[1][5]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[1][7]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[1][8]%></td>
    </tr>
    <tr CLASS="<%=rowColor%>">
      <td width="16%" nowrap class="normalText" align="left">管理费用成本率</td>
      <td height="22" class="numberText" style='text-align:right'><%=result[2][0]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[2][1]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[2][2]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[2][4]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[2][5]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[2][7]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[2][8]%></td>
    </tr>
    <tr CLASS="<%=rowColor%>">
      <td nowrap class="normalText" align="left">变动成本率</td>
      <td height="22" class="numberText" style='text-align:right'><%=result[3][0]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[3][1]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[3][2]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[3][4]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[3][5]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[3][7]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[3][8]%></td>
    </tr>
    <tr CLASS="<%=rowColor%>">
      <td nowrap class="normalText" align="left">固定成本率</td>
      <td height="22" class="numberText" style='text-align:right'><%=result[4][0]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[4][1]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[4][2]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[4][4]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[4][5]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[4][7]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[4][8]%></td>
    </tr>
    <tr CLASS="<%=rowColor%>">
      <td nowrap class="normalText" align="left">折旧成本率</td>
      <td height="22" class="numberText" style='text-align:right'><%=result[5][0]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[5][1]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[5][2]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[5][4]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[5][5]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[5][7]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[5][8]%></td>
    </tr>
    <tr CLASS="<%=rowColor%>">
      <td nowrap class="normalText" align="left">工资成本率</td>
      <td height="22" class="numberText" style='text-align:right'><%=result[6][0]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[6][1]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[6][2]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[6][4]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[6][5]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[6][7]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[6][8]%></td>
    </tr>
    <tr CLASS="<%=rowColor%>">
      <td nowrap class="normalText" align="left">可控成本率</td>
      <td height="22" class="numberText" style='text-align:right'><%=result[7][0]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[7][1]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[7][2]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[7][4]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[7][5]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[7][7]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[7][8]%></td>
    </tr>
    <tr CLASS="<%=rowColor%>">
      <td nowrap class="normalText" align="left">不可控成本率</td>
      <td height="22" class="numberText" style='text-align:right'><%=result[8][0]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[8][1]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[8][2]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[8][4]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[8][5]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[8][7]%></td>
      <td height="22" class="numberText" style='text-align:right'><%=result[8][8]%></td>
    </tr>
     <%
      }
     %>
  </table>
</fc:webprint>
<input type=hidden name="subFunction" value="sub"/>
</form>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>
