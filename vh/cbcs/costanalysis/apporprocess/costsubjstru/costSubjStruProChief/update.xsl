<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<colgroup id="tg">
          <col style = 'width:45mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
     </colgroup>
	<tr>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' rowspan="2">科室</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' rowspan="2">科室成本</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' colspan="2">直接计入</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' colspan="2">直接成本计算计入</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' colspan="2">分摊公用成本</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' colspan="2">分摊管理成本</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' colspan="2">分摊医疗辅助成本</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' colspan="2">分摊医疗技术成本</td>
	</tr>

	<tr>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>金额</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>金额</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>金额</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>金额</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>金额</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>金额</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="v1" select="td[4]"/>
				<xsl:variable name="is_last" select="td[6]"/>
			 	<xsl:choose>
			 	<xsl:when test="$v1='1' or $v1='2'">
					<tr bgcolor="#E1E1FB">
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                </xsl:when>
		                <xsl:when test="position()=3">
		                	<td noWrap="true">
		                	<xsl:value-of select="."/>
		                	</td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()=6">
		                </xsl:when>
						<xsl:when test="position() &gt; 7 and position() mod 2 =1">
							<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
		                <xsl:otherwise>
		                	<xsl:choose>
		                		<xsl:when test="position()=7 and (../td[3]='全院合计')">
		                			<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                		</xsl:when>
		                		<xsl:otherwise>
		                			<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                		</xsl:otherwise>
		                	</xsl:choose>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			 	</xsl:when>
			 	<xsl:otherwise>
			 		<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                </xsl:when>
		                <xsl:when test="position()=3">
		                	<td noWrap="true">
		                	<xsl:value-of select="."/>
		                	</td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()=6">
		                </xsl:when>
		                
						<xsl:when test="position() &gt; 7 and position() mod 2 =1">
							<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			 	</xsl:otherwise>
			</xsl:choose>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
