<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	
	<thead>
	<xsl:variable name="gy" select="/root/tbody/tr[1]/td[last()-1]"/>
	<xsl:variable name="yf" select="/root/tbody/tr[1]/td[last()]"/>	

	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  >成本项目</td>
		<td noWrap="true"  >直接计入</td>
		<xsl:if test="$gy=1">
			<td noWrap="true"  >公用成本</td>
		</xsl:if>
		<td noWrap="true"  >管理科室成本</td>
		<xsl:if test="$yf=1">
			<td noWrap="true"  >医疗辅助成本</td>
		</xsl:if>
		<td noWrap="true"  >医疗技术成本</td>
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="v1" select="td[3]"/>
			 	<xsl:choose>
			 	<xsl:when test="$v1='1'">
					<tr bgcolor="#E1E1FB">
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()-1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()">
		                </xsl:when>
		                
		                <xsl:when test="position()=2">
		                	<td noWrap="true">
		                		<a href="#" ><xsl:attribute name="onclick">win("<xsl:value-of select="../td[1]"/>","<xsl:value-of select="."/>")</xsl:attribute><xsl:value-of select="."/></a>
		                	</td>
		                </xsl:when>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>

		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			 	</xsl:when>
			 	<xsl:otherwise>
			 		<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()-1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()">
		                </xsl:when>
		                
		                <xsl:when test="position()=2">
		                	<td noWrap="true">
		                		<a href="#" ><xsl:attribute name="onclick">win("<xsl:value-of select="../td[1]"/>","<xsl:value-of select="."/>")</xsl:attribute><xsl:value-of select="."/></a>
		                	</td>
		                </xsl:when>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>

		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			 	</xsl:otherwise>
			</xsl:choose>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
