<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<xsl:variable name="gy" select="/root/tbody/tr[1]/td[last()-1]"/>
	<xsl:variable name="yf" select="/root/tbody/tr[1]/td[last()]"/>

		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="colcount"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" ></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
		<td noWrap="true" >成本项目</td>
		<td noWrap="true" >直接计入</td>
		<xsl:if test="$gy=1">
			<td noWrap="true" >公用成本</td>
		</xsl:if>
		<td noWrap="true" >管理科室成本</td>
		<xsl:if test="$yf=1">
			<td noWrap="true" >医疗辅助成本</td>
		</xsl:if>
		<td noWrap="true" >医疗技术成本</td>
	</tr>
		 
		</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()-1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()">
		                </xsl:when>
		                
		                <xsl:when test="position()=2">
		                	<td noWrap="true">
		                		<a href="#" ><xsl:attribute name="onclick">win("<xsl:value-of select="../td[2]"/>","<xsl:value-of select="."/>")</xsl:attribute><xsl:value-of select="."/></a>
		                	</td>
		                </xsl:when>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>

		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
			<td noWrap="true" colspan="colcount" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
			</xsl:if>
		</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
