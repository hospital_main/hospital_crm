<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/apporprocess/costsubjstru/CostSubjStruProChief.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime: 03-09-16 18:05 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import = "java.util.*, java.text.*, com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth,com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
	String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
	Calendar thisMonth=Calendar.getInstance();
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;
%>

<Script language="javascript">

  function analyze() {
    if (template.syear.value == '' || template.fmonth.value == '' || template.tmonth.value == '') {
      alert('请选择查询月份!');
      return false;
    }
    if(template.syear.value == "")
    {
      alert('请选择核算年份!');
      return false;
    }
    if(template.fmonth.value == "")
    {
      alert('请选择核算开始月份!');
      return false;
    }
    if(template.tmonth.value == "")
    {
      alert('请选择核算结束月份!');
      return false;
    }
    if(template.fmonth.value > template.tmonth.value ) {
      alert('开始月份不能晚于结束月份!');
      return false;
    }
    show_wait();
    setCBCS_Year1(template.syear.value);
    setCBCS_Month1(template.fmonth.value);
    setCBCS_Month2(template.tmonth.value);
    template.submit();
    return;
  }

  function preparedPrin() {
    var win = window.open("costSubjStruProChief.jspviewhigh?subFunction=preparedPrint&year_month=<%=request.getParameter("syear")+request.getParameter("fmonth")%>&year_month2=<%=request.getParameter("syear")+request.getParameter("tmonth")%>","","width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1")
  }
  function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }
  function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();

  	year_1  ? template.syear.value=year_1 : "";
    month_1 ? template.fmonth.value=month_1 : "";
    month_2 ? template.tmonth.value=month_2: "";
  }


	function getRsultXml(){//将显示结果拼成XML格式的字串
	  var xmlStr = "<?xml version='1.0' encoding='GBK'?><root>";
	  var tbody = document.getElementById("result_data");//表体
	  var thead = document.getElementById("result_head");//表头
	  var tbodyStr = "<tbody>"; //表体字串
	  var theadStr = "<thead>"; //表头字串
	  var trs = tbody.getElementsByTagName("tr");
	  var table_width = trs[0].childNodes.length;
	  for(var i = 0; i < trs.length; i++){
		tbodyStr += "<tr>";
		var tds = trs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
            if(j == 0 && tds[j].childNodes[0].nodeType != 3) //有超链接
                tbodyStr += "<td>"+tds[j].childNodes[0].innerHTML+"</td>";
            else
			    tbodyStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		tbodyStr += "</tr>";
	  }
	  tbodyStr += "</tbody>";

	  var htrs = thead.getElementsByTagName("tr");
	  for(var i = 0; i < htrs.length; i++){
		theadStr += "<tr>";
		var tds = htrs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
			theadStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		theadStr += "</tr>";
	  }
	  theadStr += "</thead>";
	  xmlStr += theadStr + tbodyStr + "</root>";
      return xmlStr.replace(/\&nbsp;&nbsp;/g, "　").replace(/\&nbsp;/g, "");
	}

	function datefromat (){
        var myDate=new Date();
    	var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
    	var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
    	var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
    	year='0000'+year;year=year.substring(year.length-4);
    	month='00'+month;month=month.substring(month.length-2);
    	day='00'+day; day=day.substring(day.length-2);

		return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString();
    }

  	function print(){

      	var printTitle='成本项目分摊汇总总表(医成本C5-03表)';
		var info = "制作日期: "+datefromat();
       // var ym = document.all["year_month"].value;
      	var data=new Object();
      	data["thead_1_1"]= printTitle;
      	data["thead_2_1"]= document.all['syear'].value+'年'+document.all['fmonth'].value+'月到'+document.all['tmonth'].value+'月';;
      	data["thead_3_1"]= "金额单位：元";

      	data["tfoot_1_1"]= info;
      	data["tfoot_2_1"]="北京望海康信科技有限公司  制作";

      	printXmlToCellByXsltFile(getRsultXml(), 'cbcs/costanalysis/apporprocess/costsubjstru/CostSubjStruProChiefPrintView.xsl', data, false, false);
	}
</Script>


<html:html clazz="main" isPrint="true">
  <form name="template" mehtod="post" action="costSubjStruProChief.jspviewhigh" />

  	<html:message/>

  	<html:title clazz='module'>成本项目分摊汇总总表(医成本C5-03表)</html:title>

    <table  width="100%" cellspacing="2" border="0" >
      <tr>
        <td class="signText" nowrap width="2%">核算月：</td>
        <td class="signText"><%=new Select(year,"syear",request.getParameter("syear")==null? String.valueOf(thisMonth.get(Calendar.YEAR)):request.getParameter("syear"),false,true)%>年
	 <%=new Select(month,"fmonth",request.getParameter("fmonth")==null? curMonth:request.getParameter("fmonth"),false,true)%>月 到
	 <%=new Select(month,"tmonth",request.getParameter("tmonth")==null? curMonth:request.getParameter("tmonth"),false,true)%>月</td>
        <td><button class="pageBtn" onclick="return analyze();">计算</button></td>
        <%
        String[][] result = (String[][])request.getAttribute("table_result");
        if (result != null && result.length > 1) {%>
        <td><button class="pageBtn" onclick="return print();">打印</button></td>
        <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
        <%}%>
     </tr>
      <tr >
	  <td colspan="2" ><br> <input type="text" class="findVhTabCtn"/></td>
	  </tr>
    </table>

    <br>

		<html:title clazz='table'>成本项目分摊汇总总表</html:title>

  <html:table clazz="result">
      <thead id="result_head">
      <tr>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>成本项目</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>直接计入</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >公用成本</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>管理科室成本</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >医疗辅助成本</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >医疗技术成本</td>
      </tr>
      </thead>
      <tbody id="result_data">
          <%
            if (result != null && result.length > 1) {
            %>
            <%
              DecimalFormat percentFormat = new DecimalFormat("#0.00%");
              DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
              String[] total = null;
              for (int i = 0; i < result.length; i++ ) {
                if(result[i][1].equals("合计")) {
                    total = result[i];
                } else if(result[i][7]!=null) {
          %>

              <tr bgcolor="#FFFFFF" height="22"  >
                  <td   class="normalText" nowrap>
                    <a href = "javascript:var win = window.open('costSubjStruProDetail.jspviewhigh?subFunction=costSumDetail&sumByWhat=<%=result[i][4]%>&year_month=<%=request.getParameter("syear")+request.getParameter("fmonth")%>&year_month2=<%=request.getParameter("syear")+request.getParameter("tmonth")%>&value=<%=result[i][0]%>&name=<%=result[i][1].substring(result[i][1].lastIndexOf(";")+1,result[i][1].length())%>', 'child', 'width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1');"><%=result[i][1]%></a>
                  </td>
                  <td  class="numberText" style='text-align:right' nowrap>
                    <%=moneyFormat.format(Double.parseDouble(result[i][7]))%>
                  </td>
                  <td    class="numberText" style='text-align:right' nowrap>
                    <%=moneyFormat.format(Double.parseDouble(result[i][8])+Double.parseDouble(result[i][11]))%>
                  </td>
                  <td   class="numberText" style='text-align:right' nowrap>
                    <%=moneyFormat.format(Double.parseDouble(result[i][12]))%>
                  </td>
                  <td   class="numberText" style='text-align:right' nowrap>
                    <%=moneyFormat.format(Double.parseDouble(result[i][13]))%>
                  </td>
                  <td   class="numberText" style='text-align:right' nowrap>
                    <%=moneyFormat.format(Double.parseDouble(result[i][14]))%>
                  </td>
              </tr>
            <%
                    }
                }
            %>

            <%
                if(total!=null)
                {
            %>

              <tr bgcolor="#CCCCFF"  height="22"  >
                <td ><a href="javascript:var win = window.open('costSubjStruProDetail.jspviewhigh?subFunction=costSumDetail&sumByWhat=<%=total[4]%>&year_month=<%=request.getParameter("syear")+request.getParameter("fmonth")%>&year_month2=<%=request.getParameter("syear")+request.getParameter("tmonth")%>', 'child', 'width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1');" ><%=total[1]%></a></td>
                <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[7]))%></td>
                <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[8])+Double.parseDouble(total[11]))%></td>
                <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[12]))%></td>
                <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[13]))%></td>
                <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[14]))%></td>
              </tr>
            <%
                }
            %>


	           <%
                }
            %>	  </tbody> 
             </html:table>
  <input type="hidden" name="subFunction" value="costSum">
	</form>
</html:html>

