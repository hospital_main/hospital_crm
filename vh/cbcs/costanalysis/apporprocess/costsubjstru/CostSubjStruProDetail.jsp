<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/apporprocess/costsubjstru/CostSubjStruProDetail.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime: 03-09-16 18:05 $
 $Revision: 1.1 $
-->


<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
   <%
        String sumByWhat = request.getParameter("sumByWhat");
        String name = request.getParameter("name");//new String(request.getParameter("name").getBytes("ISO8859_1"), "GBK");
        String yearMonth = request.getParameter("year_month");
        String yearMonth2 = request.getParameter("year_month2");
        String yearMonthStr = "核算月: " + yearMonth.substring(0,4) + "年" + Integer.parseInt(yearMonth.substring(4)) + "月";
        if(!yearMonth.substring(4,6).equals(yearMonth2.substring(4,6)))
            yearMonthStr  =  yearMonthStr + "到"+ Integer.parseInt(yearMonth2.substring(4))+ "月";
         String moduleTitle = "";

        if(sumByWhat != null && sumByWhat.equals("costSubj") && name != null) {
            moduleTitle = "成本项目: " + name + "   " + yearMonthStr;
        }
        else if(sumByWhat != null && sumByWhat.equals("totalSum")) {
          moduleTitle = "全院: " + "   " + yearMonthStr;
        }
    %>
<script type="text/javascript">
  function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav&alertwindow=aw","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }

	function getRsultXml(){//将显示结果拼成XML格式的字串
	  var xmlStr = "<?xml version='1.0' encoding='GBK'?><root>";
	  var tbody = document.getElementById("result_data");//表体
	  var thead = document.getElementById("result_head");//表头
	  var tbodyStr = "<tbody>"; //表体字串
	  var theadStr = "<thead>"; //表头字串
	  var trs = tbody.getElementsByTagName("tr");
	  var table_width = trs[0].childNodes.length;
	  for(var i = 0; i < trs.length; i++){
		tbodyStr += "<tr>";
		var tds = trs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
            if(j == 0 && tds[j].childNodes[0].nodeType != 3) //有超链接
                tbodyStr += "<td>"+tds[j].childNodes[0].innerHTML+"</td>";
            else
			    tbodyStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		tbodyStr += "</tr>";
	  }
	  tbodyStr += "</tbody>";

	  var htrs = thead.getElementsByTagName("tr");
	  for(var i = 0; i < htrs.length; i++){
		theadStr += "<tr>";
		var tds = htrs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
			theadStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		theadStr += "</tr>";
	  }
	  theadStr += "</thead>";
	  xmlStr += theadStr + tbodyStr + "</root>";
      return xmlStr.replace(/\&nbsp;&nbsp;/g, "　").replace(/\&nbsp;/g, "");
	}

	function datefromat (){
        var myDate=new Date();
    	var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
    	var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
    	var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
    	year='0000'+year;year=year.substring(year.length-4);
    	month='00'+month;month=month.substring(month.length-2);
    	day='00'+day; day=day.substring(day.length-2);

		return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString();
    }

  	function print(){

      	var printTitle='成本项目分摊汇总总表(医成本C5-03表)';
		var info = "制作日期: "+datefromat();
       // var ym = document.all["year_month"].value;
      	var data=new Object();
      	data["thead_1_1"]= printTitle;
      	data["thead_2_1"]= "<%=moduleTitle%>";
      	data["thead_3_1"]= "金额单位：元";

      	data["tfoot_1_1"]= info;
      	data["tfoot_2_1"]="北京望海康信科技有限公司  制作";

      	printXmlToCellByXsltFile(getRsultXml(), 'cbcs/costanalysis/apporprocess/costsubjstru/CostSubjStruProDetailPrintView.xsl', data, false, false);
	}
</script>
<html:html clazz="child" isPrint="true" fixCols="2">

	<html:message/>

	<html:title clazz='module'>成本项目分摊汇总明细表(医成本C5-04表) <%=moduleTitle%></html:title>


  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <%
      String[][] result = (String[][])request.getAttribute("table_result");
      if (result != null && result.length > 1){
      %>
      <td align='center'><button class="pageBtn" onclick="return print();">打印</button></td>
      <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
      <%
      }
      %>
      <td align='center'><button class="pageBtn" onClick="window.close()" >关闭</button>
      <!--<img src="images/priClose.gif" class="mouse"  onclick="window.close();"/>--></td>
    </tr>
 	</table>
 	<br>
        <%

        %>
     <html:table clazz="result">
    <thead id="result_head">
    <tr>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">科室</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">科室成本</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">直接计入</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">直接成本计算计入</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">分摊公用成本</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">分摊管理成本</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">分摊医疗辅助成本</td>
      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'colspan="2">分摊医疗技术成本</td>
    </tr>

    <tr>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>金额</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>金额</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>金额</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
    </tr>
    </thead>
    <tbody id="result_data">
            <%
            if (result != null && result.length > 1){
              DecimalFormat percentFormat = new DecimalFormat("#0.00%");
              DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
              String[] total = null;
              for (int i = 0; i < result.length; i++ )
              {
                if(result[i][1].equals("类别小计") && result[i][6]!=null)
                {
        %>

      <tr bgcolor="#E1E1FB"   >
        <td height="22"   class="normalText" nowrap>
          <%=result[i][5]%>小计
        </td>
        <td height="22"  class="numberText" style='text-align:right' nowrap>
          <%=moneyFormat.format(Double.parseDouble(result[i][6]))%>
				</td>
        <td height="22"   class="numberText" style='text-align:right' nowrap>
          <%=moneyFormat.format(Double.parseDouble(result[i][7]))%>
				</td>
        <td height="22"   class="numberText" style='text-align:right' nowrap>
          <%=percentFormat.format(Double.parseDouble(result[i][8]))%>
				</td>
        <td height="22"   class="numberText" style='text-align:right' nowrap>
          <%=moneyFormat.format(Double.parseDouble(result[i][9]))%>
				</td>
        <td height="22"   class="numberText" style='text-align:right' nowrap>
          <%=percentFormat.format(Double.parseDouble(result[i][10]))%>
				</td>
        <td height="22"   class="numberText" style='text-align:right' nowrap>
          <%=moneyFormat.format(Double.parseDouble(result[i][11]))%>
        </td>
        <td height="22"   class="numberText" style='text-align:right' nowrap>
          <%=percentFormat.format(Double.parseDouble(result[i][12]))%>
        </td>
        <td height="22"   class="numberText" style='text-align:right' nowrap>
          <%=moneyFormat.format(Double.parseDouble(result[i][13]))%>
        </td>
        <td  height="22"   class="numberText" style='text-align:right' nowrap>
          <%=percentFormat.format(Double.parseDouble(result[i][14]))%>
				</td>
        <td height="22"   class="numberText" style='text-align:right' nowrap>
          <%=moneyFormat.format(Double.parseDouble(result[i][15]))%>
				</td>
        <td height="22"   class="numberText" style='text-align:right' nowrap>
          <%=percentFormat.format(Double.parseDouble(result[i][16]))%>
				</td>
        <td height="22"   class="numberText" style='text-align:right' nowrap>
          <%=moneyFormat.format(Double.parseDouble(result[i][17]))%>
				</td>
        <td height="22"   class="numberText" style='text-align:right' nowrap>
          <%=percentFormat.format(Double.parseDouble(result[i][18]))%>
        </td>
      </tr>
                    <%
			}
                        else if( result[i][5].equals("全院"))
                        {
                          total = result[i];
                        }
                        else if(result[i][6]!=null)
                        {
                    %>
      <tr >
	      <td height="22"   class="normalText" nowrap>
	        <%=result[i][1]%>
	      </td>
	      <td height="22"   class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][6]))%></td>
	      <td height="22"   class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][7]))%></td>
	      <td height="22"   class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(result[i][8]))%></td>
        <td height="22"   class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][9]))%></td>
	      <td height="22"   class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(result[i][10]))%></td>
	      <td height="22"   class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][11]))%></td>
	      <td height="22"   class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(result[i][12]))%></td>
	      <td height="22"   class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][13]))%></td>
	      <td height="22"   class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(result[i][14]))%></td>
	      <td height="22"   class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][15]))%></td>
	      <td height="22"   class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(result[i][16]))%></td>
	      <td height="22"   class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][17]))%></td>
	      <td height="22"   class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(result[i][18]))%></td>
	    </tr>
                    <%
                        }
                    }

                    if(total != null)
                    {
                    %>

      <tr >
        <td bordercolor="#000000" class="normalText" nowrap><%=total[5]%><%=total[1]%></td>
        <td class="numberText" style='text-align:right' nowrap>--</td>
        <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[7]))%></td>
        <td class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(total[8]))%></td>
        <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[9]))%></td>
        <td class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(total[10]))%></td>
        <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[11]))%></td>
        <td class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(total[12]))%></td>
        <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[13]))%></td>
        <td class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(total[14]))%></td>
        <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[15]))%></td>
        <td class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(total[16]))%></td>
        <td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(total[17]))%></td>
        <td class="numberText" style='text-align:right' nowrap><%=percentFormat.format(Double.parseDouble(total[18]))%></td>
      </tr>
                <%
                   } }%></tbody>
     </html:table>



</html:html>
