<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/apporprocess/deptstru/DeptStruProChief.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime: 03-09-15 9:49 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import = "com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.base.mvc.view.margin.Header,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>
<%
  Header header = new Header(101, request);
  header.setPrintStatus(true);
  out.print(header);
%>

<Script language="javascript">

  function analyze() {

    if (template.year_month.value == '') {
      alert('请选择查询月份!');
      return false;
    }
    show_wait();
    template.submit();
    return;
  }

  function preparedPrin() {

    // 报表名称
    grid.prn.title1='科室成本总表';
    // 年月
    var temp=template.year_month.value;
        grid.prn.top23='医成本10表';
    grid.prn.title2=temp.substring(0,4)+'年'+temp.substring(4,6)+'月';
    // 表头行数
    grid.prn.tabHead=2;
    // 打印
    grid.print();
  }

</Script>


  <form name="template" method="post" action="deptStruProChief.jspviewhigh" />

     <!-- 信息提示栏 -->
       <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>
      <!--信息栏-->
      <%= new com.viewhigh.cbcs.base.mvc.view.MessageTitle("科室成本总表(医成本10表)")%>


      <table  width="100%" cellspacing="2" border="0" >
        <tr>
          <td class="signText" nowrap width="10%">核算月：</td>
          <td class="normalText"><%=new MonthComponent("year_month", request.getParameter("year_month"))%></td>
          <td><button class="pageBtn" onclick="return analyze();">计算</button></td>
          <td><button class="pageBtn" onclick="return preparedPrin();">打印</button></td>
          <td colspan=60></td>
        </tr>
      </table>

      <br>
    <vh:vhFixTable fixRow=2 fixCol=1>
            <table  BORDERCOLOR="#214597" border="1" cellspacing="1" width="100%" >
              <colgroup id=tg>
                <col style = <%=DisplayWidth.NAME_WIDTH%>>
                <col style = <%=DisplayWidth.NAME_WIDTH%>>
                <col style = <%=DisplayWidth.MONEY_WIDTH%>>
                <col style = <%=DisplayWidth.MONEY_WIDTH%>>
                <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
                <col style = <%=DisplayWidth.MONEY_WIDTH%> >
                <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
                <col style = <%=DisplayWidth.MONEY_WIDTH%> >
                <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
                <col style = <%=DisplayWidth.MONEY_WIDTH%> >
                <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
                <col style = <%=DisplayWidth.MONEY_WIDTH%> >
                <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
              </colgroup>

              <tr>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">类别</td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">科室</td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">科室成本</td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">直接计入</td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">分摊公用成本</td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">分摊管理成本</td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">分摊后勤保障成本</td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">分摊辅助医疗成本</td>
              </tr>

              <tr>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
              </tr>

        <%

            String[][] result = (String[][])request.getAttribute("table_result");

            if (result != null && result.length > 1)
            {
              for(int i = 0; i < result.length - 1; i++)
              {
                for(int j = (i+1); j < result.length; j++)
                {
                  if(result[i][0] != null && result[i][0].equals(result[j][0])&& result[i][2] != null && result[i][2].equals(result[j][2]))
                  {
                    for(int k = 6; k <= 16; k++)
                    {
                      result[i][k] =
                        (Double.parseDouble(result[i][k]) +
                         Double.parseDouble(result[j][k])) + "";
                    }
                    String[][] resultTemp = new String[result.length - 1][];
                    for(int index = 0; index < j; index++)
                    {
                      resultTemp[index] = result[index];
                    }
                    for(int index = j; index < resultTemp.length; index++)
                    {
                      resultTemp[index] = result[index+1];
                    }

                    result = resultTemp;
                  }
                  else
                  {
                    break;
                  }
                }
              }

              DecimalFormat percentFormat = new DecimalFormat("#0.00%");
              DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
              String[] total = null;
              for (int i = 0; i < result.length; i++ )
              {
                result[i][6] =
                  moneyFormat.format(Double.parseDouble(result[i][6]));
                result[i][7] =
                  moneyFormat.format(Double.parseDouble(result[i][7]));
                result[i][9] =
                  moneyFormat.format(Double.parseDouble(result[i][9]));
                result[i][11] =
                  moneyFormat.format(Double.parseDouble(result[i][11]));
                result[i][13] =
                  moneyFormat.format(Double.parseDouble(result[i][13]));
                result[i][15] =
                  moneyFormat.format(Double.parseDouble(result[i][15]));

                result[i][8] =
                  percentFormat.format(Double.parseDouble(result[i][8]));
                result[i][10] =
                  percentFormat.format(Double.parseDouble(result[i][10]));
                result[i][12] =
                  percentFormat.format(Double.parseDouble(result[i][12]));
                result[i][14] =
                  percentFormat.format(Double.parseDouble(result[i][14]));
                result[i][16] =
                  percentFormat.format(Double.parseDouble(result[i][16]));

                if(result[i][3].equals("类别小计"))
                {
                  int rowSpan = 1;
                  int j = i+1;
                  for( ; j < result.length; j++ )
                  {
                      if( !result[j][0].equals(result[i][0]))
                          break;
                      rowSpan++;
                  }


        %>

                          <tr bordercolor="#000000" bgcolor="#FFFFFF" class="a12">
                            <td width="8%" class="normalText" rowspan="<%=rowSpan%>">
                              <div align="left"><%=result[i][1]%></div>
                            </td>
                            <td width="9%" class="normalText" height="22" bgcolor="#E1E1FB" nowrap>
                              <div align="left">
                                <a href = "javascript:var win = window.open('deptStruProDetail.jspviewhigh?subFunction=costSumDetail&sumByWhat=<%=result[i][5]%>&value=<%=result[i][0]%>&year_month=<%=request.getParameter("year_month")%>&name=<%=result[i][1]%>', '', 'width = 800, height = 500, scrollbars = 1, resizable = 1');">
                                  <%=result[i][3]%>
                                </a>
                              </div>
                            </td>
                            <td width="7%" class="numberText" style='text-align:right' height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][6]%></div>
                            </td>
                            <td width="8%" class="numberText" style='text-align:right' height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][7]%></div>
                            </td>
                            <td width="7%" class="numberText" style='text-align:right' height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][8]%></div>
                            </td>
                            <td width="8%" class="numberText" style='text-align:right' height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][9]%></div>
                            </td>
                            <td width="7%" class="numberText" style='text-align:right' height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][10]%></div>
                            </td>
                            <td width="8%" class="numberText" style='text-align:right' height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][11]%></div>
                            </td>
                            <td width="8%" class="numberText" style='text-align:right' height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][12]%></div>
                            </td>
                            <td height="22" class="numberText" style='text-align:right' bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][13]%></div>
                            </td>
                            <td height="22" class="numberText" style='text-align:right' bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][14]%></div>
                            </td>
                            <td height="22" class="numberText" style='text-align:right' bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][15]%></div>
                            </td>
                            <td height="22" class="numberText" style='text-align:right' bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][16]%></div>
                            </td>
                          </tr>
                    <%
			} else if ( result[i][1].equals("全院")) {
                          total = result[i];
                        } else {
                    %>
                          <tr class="a12">
                            <td height="22" class="normalText" bgcolor="#FFFFFF" nowrap>
                              <div align="left">
                                <a href = "javascript:var win = window.open('deptStruProDetail.jspviewhigh?subFunction=costSumDetail&sumByWhat=<%=result[i][5]%>&value=<%=result[i][2]%>&year_month=<%=request.getParameter("year_month")%>&name=<%=result[i][3]%>', '', 'width = 800, height = 500, scrollbars = 1, resizable = 1');">
                                  <%for(int k = 0; k < Integer.parseInt(result[i][4]) - 1; k++) out.println("&nbsp;");%><%=result[i][3]%>
                                </a>
                              </div>
                            </td>
                            <td height="22" bgcolor="#FFFFFF" class="numberText" style='text-align:right' nowrap><%=result[i][6]%></td>
                            <td height="22" bgcolor="#FFFFFF" class="numberText" style='text-align:right' nowrap><%=result[i][7]%></td>
                            <td height="22" bgcolor="#FFFFFF" class="numberText" style='text-align:right' nowrap><%=result[i][8]%></td>
                            <td height="22" bgcolor="#FFFFFF" class="numberText" style='text-align:right' nowrap><%=result[i][9]%></td>
                            <td height="22" bgcolor="#FFFFFF" class="numberText" style='text-align:right' nowrap><%=result[i][10]%></td>
                            <td height="22" bgcolor="#FFFFFF" class="numberText" style='text-align:right' nowrap><%=result[i][11]%></td>
                            <td height="22" bgcolor="#FFFFFF" class="numberText" style='text-align:right' nowrap><%=result[i][12]%></td>
                            <td height="22" bgcolor="#FFFFFF" class="numberText" style='text-align:right' nowrap><%=result[i][13]%></td>
                            <td height="22" bgcolor="#FFFFFF" class="numberText" style='text-align:right' nowrap><%=result[i][14]%></td>
                            <td height="22" bgcolor="#FFFFFF" class="numberText" style='text-align:right' nowrap><%=result[i][15]%></td>
                            <td height="22" bgcolor="#FFFFFF" class="numberText" style='text-align:right' nowrap><%=result[i][16]%></td>
                          </tr>
                    <%
                        }
                    }

                    if(total != null)
                    {
                    %>

                  <tr bordercolor="#000000" bgcolor="#CCCCFF" class="a12">
                    <td height="26" class="normalText" nowrap>
                      <div align="left"><%=total[1]%></div>
                    </td>
                    <td bordercolor="#000000" class="normalText" nowrap>
                      <a href = "javascript:var win = window.open('deptStruProDetail.jspviewhigh?subFunction=costSumDetail&sumByWhat=<%=total[5]%>&year_month=<%=request.getParameter("year_month")%>', '', 'width = 800, height = 500, scrollbars = 1, resizable = 1');" >
                        <%=total[3]%>
                      </a>
                    </td>
                    <td class="numberText" style='text-align:right' nowrap>--</td>
                    <td class="numberText" style='text-align:right' nowrap><%=total[7]%></td>
                    <td class="numberText" style='text-align:right' nowrap><%=total[8]%></td>
                    <td class="numberText" style='text-align:right' nowrap><%=total[9]%></td>
                    <td class="numberText" style='text-align:right' nowrap><%=total[10]%></td>
                    <td class="numberText" style='text-align:right' nowrap><%=total[11]%></td>
                    <td class="numberText" style='text-align:right' nowrap><%=total[12]%></td>
                    <td class="numberText" style='text-align:right' nowrap><%=total[13]%></td>
                    <td class="numberText" style='text-align:right' nowrap><%=total[14]%></td>
                    <td class="numberText" style='text-align:right' nowrap><%=total[15]%></td>
                    <td class="numberText" style='text-align:right' nowrap><%=total[16]%></td>
                  </tr>
                <%
                    }
                }
                %>
    </table>
    <vh:vhFixTable fixRow=2 fixCol=1>
    <input type="hidden" name="subFunction" value="costSum">
</form>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>
