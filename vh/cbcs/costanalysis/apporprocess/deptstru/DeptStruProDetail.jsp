<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/apporprocess/deptstru/DeptStruProDetail.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime: 03-09-16 17:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->


<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.margin.Header,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>

<%
  Header header = new Header(102, request);
  header.setPrintStatus(true);
  out.print(header);
%>

<Script language="javascript">
  function preparedPrin() {

   // 报表名称
    grid.prn.title1='科室成本明细表';
   //科室
   grid.prn.top31="科室：<%=request.getParameter("name")%>";
    // 年月
    var temp='<%=request.getParameter("year_month")%>';
    grid.prn.title2=temp.substring(0,4)+'年'+temp.substring(4,6)+'月';
    // 表头行数
    grid.prn.tabHead=2;
    // 打印
    grid.print();
  }
</Script>


   <%
        String sumByWhat = request.getParameter("sumByWhat");
        String name = request.getParameter("name");
        String yearMonth = request.getParameter("year_month");
        String yearMonthStr = "核算月: " + yearMonth.substring(0,4) + "年" + Integer.parseInt(yearMonth.substring(4)) + "月";
        String moduleTitle = "";

        if(sumByWhat != null && sumByWhat.equals("appDeptType") && name != null) {
            moduleTitle = "类别: " + name + "   " + yearMonthStr;
        }
        else if(sumByWhat != null && sumByWhat.equals("dept") && name != null) {
          moduleTitle = "科室: " + name + "   " + yearMonthStr;
        }
        else if(sumByWhat != null && sumByWhat.equals("totalSum")) {
          moduleTitle = "全院: " + "   " + yearMonthStr;
        }
    %>

     	<!-- 信息提示栏 -->
      <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>

			<!--标题栏-->
      <%= new com.viewhigh.cbcs.base.mvc.view.MessageTitle("科室成本明细表"+"  "+moduleTitle)%>

      <table  width="100%" cellspacing="2" border="0" >
        <tr>
          <td><button class="pageBtn" onclick="return preparedPrin();">打印</button></td>
          <td colspan=150></td>
        </tr>
   		</table>
    <vh:vhFixTable fixRow=2 fixCol=1>

            <table  BORDERCOLOR="#214597" border="1" cellspacing="1" width="100%" >

              <colgroup id=tg>
                <col style = <%=DisplayWidth.NAME_WIDTH%>>
                <col style = <%=DisplayWidth.MONEY_WIDTH%>>
                <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
                <col style = <%=DisplayWidth.MONEY_WIDTH%>>
                <col style = <%=DisplayWidth.PERCENT_WIDTH%>>
                <col style = <%=DisplayWidth.MONEY_WIDTH%>>
                <col style = <%=DisplayWidth.MONEY_WIDTH%>>
                <col style = <%=DisplayWidth.MONEY_WIDTH%>>
                <col style = <%=DisplayWidth.MONEY_WIDTH%>>
              </colgroup>

              <tr>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">类别</td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">总成本</td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">直接费用</td>
                <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="4">分摊间接费用</td>
              </tr>

              <tr>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >金额</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >比例</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >公用成本</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >管理科室</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >后勤保障</td>
                <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >辅助医疗</td>
              </tr>





<%

            String[][] result = (String[][])request.getAttribute("table_result");

            if (result != null && result.length > 1)
            {

              /** 合并相同的节点 */
              for(int i = 0; i < result.length - 1; i++)
              {
		for(int j = (i+1); j < result.length; j++)
		{
                  if(result[i][0].equals(result[j][0]))
                  {
                    for(int k = 3; k <= 10; k++)
                    {
                      result[i][k] =
                        (Double.parseDouble(result[i][k]) +
                         Double.parseDouble(result[j][k])) + "";
                    }
                    String[][] resultTemp = new String[result.length - 1][];
                    for(int index = 0; index < j; index++)
                    {
                      resultTemp[index] = result[index];
                    }
                    for(int index = j; index < resultTemp.length; index++)
                    {
                      resultTemp[index] = result[index+1];
                    }

                    result = resultTemp;
                  }
                  else
                  {
                    break;
                  }
		}
              }

              DecimalFormat percentFormat = new DecimalFormat("#0.00%");
              DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
              String[] total = null;
              for (int i = 0; i < result.length; i++ ) {
                result[i][3] =
		  moneyFormat.format(Double.parseDouble(result[i][3]));
                result[i][5] =
                  moneyFormat.format(Double.parseDouble(result[i][5]));
                result[i][7] =
                  moneyFormat.format(Double.parseDouble(result[i][7]));
                result[i][8] =
                  moneyFormat.format(Double.parseDouble(result[i][8]));
                result[i][9] =
                  moneyFormat.format(Double.parseDouble(result[i][9]));
                result[i][10] =
                  moneyFormat.format(Double.parseDouble(result[i][10]));

                result[i][4] =
                  percentFormat.format(Double.parseDouble(result[i][4]));
                result[i][6] =
                  percentFormat.format(Double.parseDouble(result[i][6]));

                if (result[i][1].equals("合计")) {
                    total = result[i];
                } else {
%>

    <tr valign="middle" bordercolor="#000000" bgcolor="#FFFFFF" class="a12">
        <td width="13%" height="22" class="normalText" nowrap>
          <%for(int k = 0; k < Integer.parseInt(result[i][2]) - 1; k++) out.println("&nbsp;");%><%=result[i][1]%>
        </td>
        <td width="11%" height="22" class="numberText" style='text-align:right' nowrap>
          <%=result[i][3]%>
        </td>
        <td width="8%" height="22" class="numberText" style='text-align:right' nowrap>
          <%=result[i][4]%>
        </td>
        <td width="11%" height="22" class="numberText" style='text-align:right' nowrap>
          <%=result[i][5]%>
        </td>
        <td width="11%" height="22" class="numberText" style='text-align:right' nowrap>
          <%=result[i][6]%>
        </td>
        <td width="11%" height="22" class="numberText" style='text-align:right' nowrap>
          <%=result[i][7]%>
        </td>
        <td width="11%" height="22" class="numberText" style='text-align:right' nowrap>
          <%=result[i][8]%>
        </td>
        <td height="22" class="numberText" style='text-align:right' nowrap>
          <%=result[i][9]%>
        </td>
        <td height="22" class="numberText" style='text-align:right' nowrap>
          <%=result[i][10]%>
        </td>
    </tr>
<%
                    }
                }

%>

<%
    if (total!=null) {
%>
  <tr valign="middle" bordercolor="#000000" bgcolor="#CCCCFF" class="a12">
    <td height="26" class="normalText" nowrap> <div align="left"><%=total[1]%></div></td>
    <td class="numberText" style='text-align:right' nowrap>
      <%if(sumByWhat.equals("totalSum")){out.print("--");}else{out.print(total[3]);}%>
    </td>
    <td class="numberText" style='text-align:right' nowrap><%=total[4]%></td>
    <td class="numberText" style='text-align:right' nowrap><%=total[5]%></td>
    <td class="numberText" style='text-align:right' nowrap><%=total[6]%></td>
    <td class="numberText" style='text-align:right' nowrap><%=total[7]%></td>
    <td class="numberText" style='text-align:right' nowrap><%=total[8]%></td>
    <td class="numberText" style='text-align:right' nowrap><%=total[9]%></td>
    <td class="numberText" style='text-align:right' nowrap><%=total[10]%></td>
  </tr>
<%
    }
}
%>
</table>
    </vh:vhFixTable>

<br>
<table>
  <tr>
    <td><button class="pageBtn" onclick="window.close();" >关闭</button>  
    <!--<img src="images/close.gif" class="mouse" onclick="window.close();"/>--></td>

  </tr>
</table>

<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(102)%>
