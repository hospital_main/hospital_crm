<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/apporresult/costsubjstru/CostSubjStruDetail.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime: 03-08-05 10:05 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(102, request)%>


   <%
        String sumByWhat = request.getParameter("sumByWhat");
        String name = request.getParameter("name");
        String moduleTitle = "";

        if(sumByWhat != null && sumByWhat.equals("costSubj") && name != null) {
            moduleTitle = "成本项目: " + name;
        }
        else if(sumByWhat != null && sumByWhat.equals("totalSum")) {
          moduleTitle = "全院: ";
        }
    %>

    <!-- 返回信息栏 -->
      <table width="100%" border="0">
        <tr>
          <td>
            <table width="100%" border="0" >
              <%
                String message = (String)request.getAttribute("message");
                if (message!=null && !message.trim().equals("")) {
              %>
              <tr>
                <td class="successText" align="center"><%=message%></td>
              </tr>
              <%}
                String appError = (String)request.getAttribute("appError");
                if (appError!=null && !appError.trim().equals("")) {
              %>
              <tr>
                <td class="errorText" align="center"><%=appError%></td>
              </tr>
              <%}%>
            </table>
      	  <td>
      	<tr>
      </table>

      <!-- 标题栏 -->
      <table width="100%" border="0">
        <tr>
          <td>
            <table width="100%" border="0" >
              <tr>
                <td  class="moduleTitle" nowrap="nowrap"><%=moduleTitle%></td>
              </tr>
            </table>
      	  <td>
      	<tr>
      </table>

      <table width="100%">
        <!-- 结果集 -->
        <tr>
          <td>
            <table  BORDERCOLOR="#214597" border="1" cellspacing="1" width="100%" >

              <tr>
                <td colspan="100" class="resultTitle" align="center">科室分摊结果表</td>
              </tr>

              <tr>
                <td class="resultLabel" rowspan="2">类别</td>
                <td class="resultLabel" rowspan="2">科室</td>
                <td class="resultLabel" rowspan="2">最终成本</td>
                <td class="resultLabel" colspan="2">直接计入</td>
                <td class="resultLabel" colspan="2">分摊公用成本</td>
                <td class="resultLabel" colspan="2">分摊管理成本</td>
                <td class="resultLabel" colspan="2">分摊后勤保障成本</td>
                <td class="resultLabel" colspan="2">分摊辅助医疗成本</td>
              </tr>

              <tr>
                <td class="resultLabel">金额</td>
                <td class="resultLabel">比例</td>
                <td class="resultLabel">金额</td>
                <td class="resultLabel">比例</td>
                <td class="resultLabel">金额</td>
                <td class="resultLabel">比例</td>
                <td class="resultLabel">金额</td>
                <td class="resultLabel">比例</td>
                <td class="resultLabel">金额</td>
                <td class="resultLabel">比例</td>
              </tr>

        <%

            String[][] result = (String[][])request.getAttribute("table_result");

            if (result != null && result.length > 1)
            {
              for(int i = 0; i < result.length - 1; i++)
              {
                for(int j = (i+1); j < result.length; j++)
                {
                  if(result[i][0] != null && result[i][0].equals(result[j][0])&& result[i][2] != null && result[i][2].equals(result[j][2]))
                  {
                    for(int k = 5; k <= 15; k++)
                    {
                      result[i][k] =
                        (Double.parseDouble(result[i][k]) +
                         Double.parseDouble(result[j][k])) + "";
                    }
                    String[][] resultTemp = new String[result.length - 1][];
                    for(int index = 0; index < j; index++)
                    {
                      resultTemp[index] = result[index];
                    }
                    for(int index = j; index < resultTemp.length; index++)
                    {
                      resultTemp[index] = result[index+1];
                    }

                    result = resultTemp;
                  }
                  else
                  {
                    break;
                  }
                }
              }

              DecimalFormat percentFormat = new DecimalFormat("#0.00%");
              DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
              String[] total = null;
              for (int i = 0; i < result.length; i++ )
              {
                result[i][5] =
                  moneyFormat.format(Double.parseDouble(result[i][5]));
                result[i][6] =
                  moneyFormat.format(Double.parseDouble(result[i][6]));
                result[i][8] =
                  moneyFormat.format(Double.parseDouble(result[i][8]));
                result[i][10] =
                  moneyFormat.format(Double.parseDouble(result[i][10]));
                result[i][12] =
                  moneyFormat.format(Double.parseDouble(result[i][12]));
                result[i][14] =
                  moneyFormat.format(Double.parseDouble(result[i][14]));

                result[i][7] =
                  percentFormat.format(Double.parseDouble(result[i][7]));
                result[i][9] =
                  percentFormat.format(Double.parseDouble(result[i][9]));
                result[i][11] =
                  percentFormat.format(Double.parseDouble(result[i][11]));
                result[i][13] =
                  percentFormat.format(Double.parseDouble(result[i][13]));
                result[i][15] =
                  percentFormat.format(Double.parseDouble(result[i][15]));

                if(result[i][3].equals("类别小计"))
                {
                  int rowSpan = 1;
                  int j = i+1;
                  for( ; j < result.length; j++ )
                  {
                      if( !result[j][0].equals(result[i][0]))
                          break;
                      rowSpan++;
                  }


        %>

                          <tr bordercolor="#000000" bgcolor="#FFFFFF" class="a12">
                            <td width="8%" rowspan="<%=rowSpan%>">
                              <div align="left"><%=result[i][1]%></div></td>
                            <td width="9%" height="22" bgcolor="#E1E1FB">
                              <div align="left"><%=result[i][3]%></div></td>
                            <td width="7%" height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][5]%>
                              </div></td>
                            <td width="8%" height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][6]%>
                              </div></td>
                            <td width="7%" height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][7]%></div></td>
                            <td width="8%" height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][8]%>
                              </div></td>
                            <td width="7%" height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][9]%></div></td>
                            <td width="8%" height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][10]%> </div></td>
                            <td width="8%" height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][11]%></div></td>
                            <td height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][12]%></div></td>
                            <td height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][13]%></div></td>
                            <td height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][14]%> </div></td>
                            <td height="22" bgcolor="#E1E1FB">
                              <div align="right"><%=result[i][15]%></div></td>
                          </tr>
                    <%
			}
                        else if( result[i][1].equals("全院"))
                        {
                          total = result[i];
                        }
                        else
                        {
                    %>
                          <tr class="a12">
                            <td height="22" bgcolor="#FFFFFF" nowrap>
                              <div align="left"><%for(int k = 0; k < Integer.parseInt(result[i][4]) - 1; k++) out.println("&nbsp;");%><%=result[i][3]%></div></td>
                            <td height="22" bgcolor="#FFFFFF" align="right" nowrap><%=result[i][5]%></td>
                            <td height="22" bgcolor="#FFFFFF" align="right" nowrap><%=result[i][6]%></td>
                            <td height="22" bgcolor="#FFFFFF" align="right" nowrap><%=result[i][7]%></td>
                            <td height="22" bgcolor="#FFFFFF" align="right" nowrap><%=result[i][8]%></td>
                            <td height="22" bgcolor="#FFFFFF" align="right" nowrap><%=result[i][9]%></td>
                            <td height="22" bgcolor="#FFFFFF" align="right" nowrap><%=result[i][10]%></td>
                            <td height="22" bgcolor="#FFFFFF" align="right" nowrap><%=result[i][11]%></td>
                            <td height="22" bgcolor="#FFFFFF" align="right" nowrap><%=result[i][12]%></td>
                            <td height="22" bgcolor="#FFFFFF" align="right" nowrap><%=result[i][13]%></td>
                            <td height="22" bgcolor="#FFFFFF" align="right" nowrap><%=result[i][14]%></td>
                            <td height="22" bgcolor="#FFFFFF" align="right" nowrap><%=result[i][15]%></td>
                          </tr>
                    <%
                        }
                    }

                    if(total != null)
                    {
                    %>

                  <tr bordercolor="#000000" bgcolor="#CCCCFF" class="a12">
                    <td height="26">
                      <div align="left"><%=total[1]%></div></td>
                    <td bordercolor="#000000"><%=total[3]%></td>
                    <td align="right"><%=total[5]%></td>
                    <td align="right"><%=total[6]%></td>
                    <td align="right"><%=total[7]%></td>
                    <td align="right"><%=total[8]%></td>
                    <td align="right"><%=total[9]%></td>
                    <td align="right"><%=total[10]%></td>
                    <td align="right"><%=total[11]%></td>
                    <td align="right"><%=total[12]%></td>
                    <td align="right"><%=total[13]%></td>
                    <td align="right"><%=total[14]%></td>
                    <td align="right"><%=total[15]%></td>
                  </tr>
                <%
                    }
                }
                %>
    </table>

  </td>
</tr>
</table>
<br>
<table>
  <tr>
    <td><button class="pageBtn" onclick="window.close();" >关闭</button> 
    <!--<img src="images/close.gif" class="mouse" onclick="window.close()"/>--></td>
    <td><button class="pageBtn" >打印</button></td>
  </tr>
</table>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(102)%>

