<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/apporresult/costsubjstru/CostSubjStruChief.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime: 03-08-05 10:02 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="java.util.*, java.text.*, com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(101, request)%>


  <form name="template" mehtod="post" action="costSubjStruChief.jspviewhigh" />


    <!-- 返回信息栏 -->
      <table width="100%" border="0">
        <tr>
          <td>
            <table width="100%" border="0" >
              <%
                String message = (String)request.getAttribute("message");
                if (message!=null && !message.trim().equals("")) {
              %>
              <tr>
                <td class="successText" align="center"><%=message%></td>
              </tr>
              <%}
                String appError = (String)request.getAttribute("appError");
                if (appError!=null && !appError.trim().equals("")) {
              %>
              <tr>
                      <td class="errorText" align="center"><%=appError%></td>
              </tr>
              <%}%>
            </table>
      	  </td>
      	</tr>
      </table>


      <!-- 标题栏 -->
      <table width="100%" border="0">
        <tr>
          <td>
            <table width="100%" border="0" >
              <tr>
                <td  class="moduleTitle" nowrap="nowrap">分摊汇总：</td>
              </tr>
            </table>
      	  </td>
      	</tr>
      </table>


      <table  width="100%" cellspacing="2" border="0" >
        <tr>
          <td class="signText" nowrap width="10%">核算月：</td>
          <td class="normalText"><%=new MonthComponent("year_month", request.getParameter("year_month"))%></td>
          <td><button class="pageBtn" onclick="template.submit();">计算</button></td>
          <td><button class="pageBtn" >打印</button></td>
        </tr>
      </table>

      <br>

      <table width="100%">
        <!-- 结果集 -->
        <tr>
          <td>
            <table  BORDERCOLOR="#214597" border="1" cellspacing="1" width="100%" >
              <tr>
                <td colspan="100" class="resultTitle" align="center">成本项目构成结果汇总</td>
              </tr>

              <tr>
                <td class="resultLabel" rowspan="2">类别</td>
                <td class="resultLabel" colspan="2">总成本</td>
                <td class="resultLabel" colspan="2">直接费用</td>
                <td class="resultLabel" colspan="4">分摊间接费用</td>
              </tr>

              <tr>
                <td class="resultLabel">金额</td>
                <td class="resultLabel">比例</td>
                <td class="resultLabel">金额</td>
                <td class="resultLabel">比例</td>
                <td class="resultLabel">公用成本</td>
                <td class="resultLabel">管理科室</td>
                <td class="resultLabel">后勤保障</td>
                <td class="resultLabel">辅助医疗</td>
              </tr>

          <%

            String[][] result = (String[][])request.getAttribute("table_result");

            if (result != null && result.length > 1)
            {

              /** 合并相同的节点 */
              for(int i = 0; i < result.length - 1; i++)
              {
		for(int j = (i+1); j < result.length; j++)
		{
                  if(result[i][0].equals(result[j][0]))
                  {
                    for(int k = 4; k <= 11; k++)
                    {
                      result[i][k] =
                        (Double.parseDouble(result[i][k]) +
                         Double.parseDouble(result[j][k])) + "";
                    }
                    String[][] resultTemp = new String[result.length - 1][];
                    for(int index = 0; index < j; index++)
                    {
                      resultTemp[index] = result[index];
                    }
                    for(int index = j; index < resultTemp.length; index++)
                    {
                      resultTemp[index] = result[index+1];
                    }

                    result = resultTemp;
                  }
                  else
                  {
                    break;
                  }
		}
              }

              DecimalFormat percentFormat = new DecimalFormat("#0.00%");
              DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
              String[] total = null;
              for (int i = 0; i < result.length; i++ )
              {
                result[i][4] =
		  moneyFormat.format(Double.parseDouble(result[i][4]));
                result[i][6] =
                  moneyFormat.format(Double.parseDouble(result[i][6]));
                result[i][8] =
                  moneyFormat.format(Double.parseDouble(result[i][8]));
                result[i][9] =
                  moneyFormat.format(Double.parseDouble(result[i][9]));
                result[i][10] =
                  moneyFormat.format(Double.parseDouble(result[i][10]));
                result[i][11] =
                  moneyFormat.format(Double.parseDouble(result[i][11]));

                result[i][5] =
                  percentFormat.format(Double.parseDouble(result[i][5]));
                result[i][7] =
                  percentFormat.format(Double.parseDouble(result[i][7]));

                if(result[i][1].equals("合计"))
                {
                    total = result[i];
                }
                else
                {

          %>

              <tr valign="middle" bordercolor="#000000" bgcolor="#FFFFFF" class="a12">
                  <td width="13%" height="22">
                    <div align="left"><a href="costSubjStruDetail.jspviewhigh?subFunction=costSumDetail&sumByWhat=<%=result[i][3]%>&year_month=<%=request.getParameter("year_month")%>&value=<%=result[i][0]%>&name=<%=result[i][1]%>" target="win1"><%for(int k = 0; k < Integer.parseInt(result[i][2]) - 1; k++) out.println("&nbsp;");%><%=result[i][1]%></a></div>
                  </td>
                  <td width="11%" height="22">
                      <div align="right"> <%=result[i][4]%></div>
                  </td>
                  <td width="8%" height="22">
                      <div align="right"><%=result[i][5]%></div>
                  </td>
                  <td width="11%" height="22">
                      <div align="right"><%=result[i][6]%></div>
                  </td>
                  <td width="11%" height="22">
                      <div align="right"><%=result[i][7]%></div>
                  </td>
                  <td width="11%" height="22">
                      <div align="right"><%=result[i][8]%></div>
                  </td>
                  <td width="11%" height="22">
                      <div align="right"><%=result[i][9]%></div>
                  </td>
                  <td height="22">
                      <div align="right"><%=result[i][10]%></div>
                  </td>
                  <td height="22">
                      <div align="right"><%=result[i][11]%></div>
                  </td>
              </tr>
            <%
                    }
                }

            %>

            <%
                if(total!=null)
                {
            %>
              <tr valign="middle" bordercolor="#000000" bgcolor="#CCCCFF" class="a12">
                <td height="26"><div align="left"><a href="costSubjStruDetail.jspviewhigh?subFunction=costSumDetail&sumByWhat=<%=total[3]%>&year_month=<%=request.getParameter("year_month")%>" target="win1"> <%=total[1]%> </a></div></td>
                <td align="right"><%=total[4]%></td>
                <td align="right"><%=total[5]%></td>
                <td align="right"><%=total[6]%></td>
                <td align="right"><%=total[7]%></td>
                <td align="right"><%=total[8]%></td>
                <td align="right"><%=total[9]%></td>
                <td align="right"><%=total[10]%></td>
                <td align="right"><%=total[11]%></td>
              </tr>
            <%
                }
            }
            %>
            </table>
          </td>
        </tr>
      </table>
    <input type="hidden" name="subFunction" value="costSum">
</form>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>
