<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costanalysis/deptAllCostAlan/printView_child.xsl,v 1.2 2012/04/17 06:02:46 yangqing Exp $
 $Author: yangqing $
 $Date: 2012/04/17 06:02:46 $
 $Revision: 1.2 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">   
  <xsl:variable name="count" select="count(//root/tbody/tr[1]/td)"/>
  <root>
	  <colgroup>		
		  <xsl:for-each select="/root/tbody/tr[1]">
				<xsl:for-each select='td'>
					<xsl:choose>
						<xsl:when test="position()=1">
							<col style = 'width:200mm'/>	
						</xsl:when>
						<xsl:when test="position() > 1 and position() &lt; ($count - 13) ">
							<col style = 'width:100mm'/>
							<col style = 'width:100mm'/>
						</xsl:when>
						<xsl:otherwise>
							<col style = 'width:100mm'/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:for-each>
		</colgroup>
  	<thead>
		  <xsl:for-each select="/root/tbody/tr[1]">
		  	<tr noWrap='true'>
					<xsl:for-each select='td'>
						<xsl:choose>
							<xsl:when test="position()=1">
								<td style='fontsize:maintitle;colspan:colcount'></td>
							</xsl:when>
							<xsl:when test="position() > 1 and position() &lt; ($count - 13) ">
								<td style="display:none"></td>
								<td style="display:none"></td>
							</xsl:when>
							<xsl:otherwise>
								<td style="display:none"></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>   
			
			<xsl:for-each select="/root/tbody/tr[1]">
		  	<tr noWrap='true'>
					<xsl:for-each select='td'>
						<xsl:choose>
							<xsl:when test="position()=1">
							  <td noWrap='true' style='fontsize:subtitle;colspan:colcount;align:right'>
							 		<xsl:attribute name='colspan'>
							 			<xsl:value-of select=" ($count - 5)*2 + 5 "/>
							 		</xsl:attribute>
							 		<xsl:value-of select="成本医03-1表"/>
							 	</td>
							</xsl:when>
							<xsl:when test="position() > 1 and position() &lt; ($count - 13) ">
								<td style="display:none"></td>
								<td style="display:none"></td>
							</xsl:when>
							<xsl:otherwise>
								<td style="display:none"></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>   
			<xsl:for-each select="/root/tbody/tr[1]">
		  	<tr noWrap='true'>
					<xsl:for-each select='td'>
						<xsl:choose>
							<xsl:when test="position()=1">
							  <td noWrap='true' style='fontsize:subtitle;colspan:colcount;align:left'>
							 		<xsl:attribute name='colspan'>
							 			<xsl:value-of select="($count - 5)*2 + 5 "/>
							 		</xsl:attribute>
							 	</td>
							</xsl:when>
							<xsl:otherwise>
								<td style="display:none"></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:choose>
					<xsl:when test="position() = 1">
						<tr noWrap="true" class="mainHead">
							<xsl:for-each select="td">
								<xsl:variable name="endcol" select="last()"/>
								<xsl:choose>
								<xsl:when test="position() = 1">
									<td align="center">
										<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:attribute name="rowspan">2</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position() > 1 and position() &lt; last()-13 ">
									<td align="center">
										<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:attribute name="colspan">2</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
									<td style="display:none" ></td>
								</xsl:when>
								<xsl:otherwise>
									<td align="center" >
										<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:attribute name="rowspan">2</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</tr>
						<tr noWrap="true" class="mainHead">
							<xsl:for-each select="td">
								<xsl:variable name="endcol" select="last()"/>
								<xsl:choose>
								<xsl:when test="position() = 1">
									<td>
										<xsl:attribute name="style">display:none</xsl:attribute>
									</td>
								</xsl:when>
								<xsl:when test="position() > 1 and position() &lt; last()-13 ">
									<td align="center" >金额</td>
									<td align="center" >%</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:attribute name="style">display:none</xsl:attribute>
									</td>
								</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</tr>
					</xsl:when>
					<xsl:otherwise>
						<tr>
							<xsl:for-each select="td">   
								<xsl:choose>
									<xsl:when test="position() = 1">
										<td>
											<xsl:attribute name="noWrap">true</xsl:attribute>
											<xsl:value-of select="."/>
										</td>
									</xsl:when>
									<xsl:when test="position() > 1 and position() &lt; last()-14 ">
										<td>
											<xsl:attribute name="align">right</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</td>
										<td>
											<xsl:attribute name="align">right</xsl:attribute>
											<xsl:value-of select=" format-number((. div ../td[last()-14])* 100 ,'0.00')"/>
										</td>
									</xsl:when>
									<xsl:when test="position() = last()-14 ">
										<td>
											<xsl:attribute name="align">right</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</td>
										<td>
											<xsl:attribute name="align">right</xsl:attribute>100.00</td>
									</xsl:when>
									<xsl:otherwise>
										<td>
											<xsl:attribute name="align">right</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</tr>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tbody>
		<tfoot>
			<xsl:for-each select="/root/tbody/tr[1]">
		  	<tr noWrap='true'>
					<xsl:for-each select='td'>
						<xsl:choose>
							<xsl:when test="position()=1">
						 		<td noWrap='true' style='fontsize:subtitle;colspan:colcount;align:left'>
						 			<xsl:attribute name='colspan'>
						 				<xsl:value-of select="($count - 5)*2 + 5 "/>
						 			</xsl:attribute>
						 		</td>
							</xsl:when>
							<xsl:when test="position() > 1 and position() &lt; ($count - 13) ">
								<td style="display:none"></td>
								<td style="display:none"></td>
							</xsl:when>
							<xsl:otherwise>
								<td style="display:none"></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>   
	  </tfoot>
  </root>
	</xsl:template>
</xsl:stylesheet>
