<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='0.00'/>
	<xsl:template match="/">
	<thead>
		
	<tr noWrap="true" class="mainHead">
		<td nowrap="true"  style='text-align:center;font-weight:bold' rowspan="2">项目</td>
		<xsl:for-each select="/root/tbody/tr[1]/td">
			<xsl:choose>
		        <xsl:when test="position()&lt;13">
		        </xsl:when>
		        <xsl:when test="position()mod 5 =3">
		        	<td nowrap="true"  style='text-align:center;font-weight:bold' colspan="4">
		        	 <xsl:value-of select="."/>
		        	</td>
		        </xsl:when>
		        <xsl:otherwise>
		        </xsl:otherwise>
	     	</xsl:choose>
		</xsl:for-each>
	</tr>

	<tr noWrap="true" class="mainHead">
		<xsl:for-each select="/root/tbody/tr[1]/td">
			<xsl:choose>
		        <xsl:when test="position()&lt;13">
		        </xsl:when>
		        <xsl:when test="position()mod 5 =3">
		        	<td noWrap="true"  style='text-align:center;font-weight:bold'>本期</td>
		        	<td noWrap="true"  style='text-align:center;font-weight:bold'>比例</td>
					<td noWrap="true"  style='text-align:center;font-weight:bold'>累计</td>
					<td noWrap="true"  style='text-align:center;font-weight:bold'>比例</td>
		        </xsl:when>
		        <xsl:otherwise>
		        </xsl:otherwise>
	     	</xsl:choose>
		</xsl:for-each>
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                	<td>
		                	 <xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:when test="position()&gt;2 and position()&lt;9">
		                </xsl:when>
		                <xsl:when test="position()mod 5 =3">
		                </xsl:when>
		                <xsl:otherwise>
		                	<xsl:choose>
		                		<xsl:when test="(position()&gt;8) and (position()mod 5 =0 or position()mod 5 =2)">
		                			<td noWrap="true"  align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                		</xsl:when>
		                		<xsl:otherwise>
		                			<td noWrap="true"  align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                		</xsl:otherwise>
		                	</xsl:choose>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
