<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	
	<thead>
	<xsl:variable name="gy" select="/root/tbody/tr[1]/td[last()-1]"/>
	<xsl:variable name="yf" select="/root/tbody/tr[1]/td[last()]"/>	
	<tr noWrap="true" class="mainHead">
		<td nowrap="true"  rowspan="3">自定义科室</td>
		<td nowrap="true"  colspan="6">收入</td>
		
		<xsl:choose>
			<xsl:when test="$gy=0 and $yf=0">
				<td nowrap="true"  colspan="5">成本</td>
			</xsl:when>
			<xsl:when test="$gy=1 and $yf=1">
				<td nowrap="true"  colspan="7">成本</td>
			</xsl:when>
						
			<xsl:otherwise>
				<td nowrap="true"  colspan="6">成本</td>
			</xsl:otherwise>
		</xsl:choose>
	
		<td nowrap="true"  colspan="2">收益</td>
	</tr>
	
	<tr noWrap="true" class="mainHead">
		<td nowrap="true"  colspan="2">合计</td>
		<td nowrap="true"  colspan="2">医疗（不含药品）</td>
		<td nowrap="true"  colspan="2">药品</td>
		
		<xsl:choose>
			<xsl:when test="$gy=0 and $yf=0">
				<td nowrap="true"  colspan="4">本期</td>
			</xsl:when>
			<xsl:when test="$gy=1 and $yf=1">
				<td nowrap="true"  colspan="6">本期</td>
			</xsl:when>
						
			<xsl:otherwise>
				<td nowrap="true"  colspan="5">本期</td>
			</xsl:otherwise>
		</xsl:choose>
		
		<td nowrap="true"  rowspan="2">累计</td>
		
		<td nowrap="true"  rowspan="2">本期</td>
		<td nowrap="true"  rowspan="2">累计</td>
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		
		<td noWrap="true"  >合计</td>
		<td noWrap="true"  >直接计入</td>
		<xsl:if test="$gy=1">
			<td noWrap="true"  >公用成本</td>
		</xsl:if>
		<td noWrap="true"  >管理成本</td>
		<xsl:if test="$yf=1">
			<td noWrap="true"  >医疗辅助</td>
		</xsl:if>
		<td noWrap="true"  >医疗技术</td>

	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="v1" select="td[5]"/>
			 	<xsl:choose>
			 	<xsl:when test="$v1='1'">
					<tr bgcolor="#E1E1FB">
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=last()-1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                </xsl:when>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position()=4">
		                	<td noWrap="true">
		                	<xsl:value-of select="."/>
		                	</td>
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		               
		                <xsl:otherwise>
		                   <td align="right" noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			 	</xsl:when>
			 	<xsl:otherwise>
			 		<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=last()-1">
		                </xsl:when>
		                
		                <xsl:when test="position()=last()">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                </xsl:when>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position()=4">
		                	<td noWrap="true">
		                		<xsl:choose>
		                			<xsl:when test="$v1='3'">
		                				<a href="#" ><xsl:attribute name="onclick">win("<xsl:value-of select="../td[1]"/>","<xsl:value-of select="../td[2]"/>","<xsl:value-of select="../td[3]"/>","<xsl:value-of select="../td[4]"/>")</xsl:attribute><xsl:value-of select="."/></a>
		                			</xsl:when>
		                			<xsl:otherwise>
		                				<xsl:value-of select="."/>
		                			</xsl:otherwise>
		                		</xsl:choose>
		                	</td>
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                
		                <xsl:otherwise>
		                   <td align="right" noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			 	</xsl:otherwise>
			</xsl:choose>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
