<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<xsl:variable name="wnr" select="/root/tbody/tr[1]/td[last()-1]"/>
	<thead>
		<colgroup id="tg">
          <col style = 'width:45mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
          <col style = 'width:35mm' />
     </colgroup>
	<tr>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' rowspan="3">科室名称</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' colspan="2">收入</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' colspan="7">成本</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' colspan="2">收益</td>
	</tr>
	
	<tr>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' rowspan="2">本期</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' rowspan="2">累计</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' colspan="6">本期</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' rowspan="2">累计</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' rowspan="2">本期</td>
		<td nowrap="true" class="resultLabel" style='text-align:center;font-weight:bold' rowspan="2">累计</td>
	</tr>

	<tr>
		
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>合计</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>直接计入</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>公用成本</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>管理成本</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>医疗辅助</td>
		<td noWrap="true" class="resultLabel" style='text-align:center;font-weight:bold'>医疗技术</td>

	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			 	<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                	<td noWrap="true"><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
