<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costanalysis/deptAllImmeCost/printView.xsl,v 1.6 2012/04/23 07:33:20 liyan Exp $
 $Author: liyan $
 $Date: 2012/04/23 07:33:20 $
 $Revision: 1.6 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  		<xsl:variable name="wnr" select="/root/tbody/tr[1]/td[last()-1]"/>
  		<xsl:variable name="count" select="count(//root/tbody/tr[1]/td)"/>
  		
  <root>
	  <colgroup>		
	  <xsl:for-each select="/root/tbody/tr[1]">
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							<col style = 'width:200mm'/>	
					</xsl:when>
					<xsl:otherwise>
						<col style = 'width:100mm'/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
	
		</xsl:for-each>   
		</colgroup>
  	<thead>
  <xsl:for-each select="/root/tbody/tr[1]">
  	<tr noWrap='true'>
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							 <td style='fontsize:maintitle;colspan:colcount'></td>
					</xsl:when>
					<xsl:otherwise>
						<td style="display:none"></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>	
	 <xsl:for-each select="/root/tbody/tr[1]">
  	<tr noWrap='true'>
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							 <td noWrap='true' style='fontsize:subtitle;colspan:colcount;align:right' >
							 		<xsl:attribute name='colspan'>
							 				<xsl:value-of select="$count"/>
							 		</xsl:attribute>
							 		<xsl:value-of select="�ɱ�ҽ01��"/>
							 	</td>
					</xsl:when>
					<xsl:otherwise>
							<td style="display:none"></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>   
	  	
	 <xsl:for-each select="/root/tbody/tr[1]">
  	<tr noWrap='true'>
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							 <td noWrap='true' style='fontsize:subtitle;colspan:colcount;align:center'>
							 		<xsl:attribute name='colspan'>
							 				<xsl:value-of select="$count"/>
							 		</xsl:attribute>
							 	
							 	</td>
					</xsl:when>
					<xsl:otherwise>
							<td style="display:none"></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>   
<xsl:for-each select="/root/tbody/tr[1]">
  	<tr noWrap="true" class="mainHead">
							<xsl:for-each select="td">   
								<td>
									<xsl:attribute name="align">center</xsl:attribute>
									<xsl:attribute name="noWrap">true</xsl:attribute>
									<xsl:value-of select="."/>
								</td>
							</xsl:for-each>
						</tr>
	</xsl:for-each>
		
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:choose>
					<xsl:when test="position() = 1">
					</xsl:when>
					<xsl:otherwise>
						<tr>
							<xsl:for-each select="td">   
								<xsl:choose>
									<xsl:when test="position() = 1">
									<td>
										<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
									</xsl:when>
									<xsl:otherwise>
										<td>
											<xsl:attribute name="align">right</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
											
										</td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</tr>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>	
		</tbody>
		<tfoot>
			
		<xsl:for-each select="/root/tbody/tr[1]">
  	<tr noWrap='true'>
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							 <td noWrap='true' style='fontsize:maintitle;colspan:colcount;align:left'>
							 		<xsl:attribute name='colspan'>
							 				<xsl:value-of select="$count"/>
							 			</xsl:attribute>
							 	</td>
					</xsl:when>
					<xsl:otherwise>
							<td style="display:none"></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>   

	</tfoot>
  </root>
	</xsl:template>
</xsl:stylesheet>
