<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
			<thead>
			<xsl:for-each select="/root/tbody/tr[position() = 1]">
					<tr noWrap="true" class="mainHead">
						<xsl:for-each select="td">   
							<td>
								<xsl:attribute name="noWrap">true</xsl:attribute>
								<xsl:value-of select="."/>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
		</thead>
		<tbody>
		<xsl:for-each select="/root/tbody/tr[position() &gt; 1]">
						<tr>
							<xsl:for-each select="td">   
								<xsl:choose>
									<xsl:when test="position() = 1">
									<td>
										<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
									</xsl:when>
									<xsl:otherwise>
										<td>
											<xsl:attribute name="class">numberText</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
