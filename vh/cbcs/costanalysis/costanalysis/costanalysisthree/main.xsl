<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		
	<tr noWrap="true" class="mainHead">
		<td nowrap="true"  style='text-align:center;font-weight:bold' rowspan="3">科室</td>
		<td nowrap="true"  style='text-align:center;font-weight:bold' colspan="6">医疗</td>
		<td nowrap="true"  style='text-align:center;font-weight:bold' colspan="6">药品</td>
		<td nowrap="true"  style='text-align:center;font-weight:bold' colspan="6">合计</td>
		<td nowrap="true"  style='text-align:center;font-weight:bold' rowspan="2" colspan="2">未纳入成本项</td>
		<td nowrap="true"  style='text-align:center;font-weight:bold' rowspan="2" colspan="2">收支结余</td>
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  style='text-align:center;font-weight:bold' colspan="2">收入</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold' colspan="2">成本</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold' colspan="2">收益</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold' colspan="2">收入</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold' colspan="2">成本</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold' colspan="2">收益</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold' colspan="2">收入</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold' colspan="2">成本</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold' colspan="2">收益</td>
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  style='text-align:center;font-weight:bold'>本期</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>累计</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>本期</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>累计</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>本期</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>累计</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>本期</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>累计</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>本期</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>累计</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>本期</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>累计</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>本期</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>累计</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>本期</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>累计</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>本期</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>累计</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>本期</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>累计</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>本期</td>
		<td noWrap="true"  style='text-align:center;font-weight:bold'>累计</td>
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
        <tr>
					 <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                 <xsl:when test="position()=3">
                </xsl:when>
                <xsl:otherwise>
                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
