<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costanalysis/costanalysisthree/printView.xsl,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
			<col style = 'width:125mm'/>
			<col style = 'width:125mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:135mm'/>
			<col style = 'width:135mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:135mm'/>
			<col style = 'width:125mm'/>
			<col style = 'width:135mm'/>
			<col style = 'width:135mm'/>
			<col style = 'width:135mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:135mm'/>
			<col style = 'width:135mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:135mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:135mm'/>
			<col style = 'width:135mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:135mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:135mm'/>
			</colgroup>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="23"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="23" ></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="23" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="23" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr noWrap="true" class="mainHead">
			<td noWrap="true" rowspan="3">科室</td>
			<td noWrap="true" colspan="6">医疗</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="6">药品</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="6">合计</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2" rowspan="2">未纳入成本项</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2" rowspan="2">收支结余</td>
			<td style="display:none"></td>
		</tr>

		<tr noWrap="true" class="mainHead">
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">收入</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">收益</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">收入</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">收益</td>
			<td style="display:none"></td>
  			<td noWrap="true" colspan="2">收入</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">收益</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>

		</tr>

		 <tr noWrap="true" class="mainHead">
			<td style="display:none"></td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
		  </tr>


		</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
        <tr>
					 <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                 <xsl:when test="position()=3">
                </xsl:when>
                <xsl:otherwise>
                   <td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
		<td noWrap="true" colspan="23" align="right"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
	</tr>
	<tr>
		<td noWrap="true" colspan="23" align="right"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
	</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
