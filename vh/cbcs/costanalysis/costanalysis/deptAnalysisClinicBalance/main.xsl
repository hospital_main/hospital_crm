<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	
	<thead>
	
		<xsl:for-each select="/root/tbody/tr[position()&lt;4]">
        	<tr noWrap="true" class="mainHead">
        	<xsl:variable name="r" select="position()"/>
		    <xsl:for-each select="td[position()&gt;2]">
                	<xsl:variable name="td" select="."/>	 	
                	<xsl:choose>
		                	<xsl:when test="position()&lt;3">
	                		 	<xsl:if test="$r = 1">
		                		 	<td nowrap="true"  rowspan="3">
		                			   <xsl:value-of select="."/>
		                			</td>
	                			</xsl:if>
	                			<xsl:if test="$r != 1">
		                		 	<td style='display:none' >
		                			   <xsl:value-of select="."/>
		                			</td>
	                			</xsl:if>
	                		</xsl:when>
	                		
	                		<xsl:when test="$r = 1">
	                		 	<xsl:if test=". !=''">
		                		 	<td nowrap="true"  >
		                		 	  <xsl:attribute name="colspan" ><xsl:value-of select="substring-after(.,'||')"/></xsl:attribute>
		                			   <xsl:value-of select="substring-before(.,'||')"/>
		                			</td>
	                			</xsl:if>
	                			
	                			
	                		</xsl:when>
	                		
	                		<xsl:when test="$r = 2">
	                		 	<xsl:if test="position() mod 3 = 0">
		                		 	<td nowrap="true"  colspan="3">
		                			   <xsl:value-of select="."/>
		                			</td>
	                			</xsl:if>
	                			<xsl:if test="position() mod 3 != 0">
		                		 	<td style='display:none' >
		                			   <xsl:value-of select="."/>
		                			</td>
	                			</xsl:if>
	                			
	                		</xsl:when>
	                		

                			<xsl:otherwise>
                   				<td nowrap="true"  style='text-align:center;font-weight:bold'>
	                			   <xsl:value-of select="."/>
	                			</td>
                			</xsl:otherwise>
                	</xsl:choose>	
	           </xsl:for-each>
		</tr>
		</xsl:for-each>
	
	</thead>
	
	<tbody>
		<xsl:for-each select="/root/tbody/tr[position()&gt;3]">
        	<tr>
			 <xsl:for-each select="td[position()&gt;2]">
	                	<xsl:choose>
                		<xsl:when test="position()=1">
                		 	<td nowrap="true" height="22">
                			   <xsl:value-of select="."/>
                			</td>
                		</xsl:when>
                		
                		<xsl:otherwise>
                   			<td noWrap="true" class="numberText">
	                   			<xsl:if test=". != '' and . !=0">
	                   			<xsl:value-of select="format-number(.,'#,##0.00')"/>
	                   			</xsl:if>
                   			</td>
                		</xsl:otherwise>
                	</xsl:choose>
	                		
	          	</xsl:for-each>
		</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
