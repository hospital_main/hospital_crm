<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costanalysis/HosCostSum.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime:$
 $Revision: 1.1 $
-->


<%@ page language = "java" contentType = "text/html;charset=GBK" errorPage = "error.jsp" %>
<%@ page import = "com.viewhigh.cbcs.cbcs.util.DisplayWidth, com.viewhigh.cbcs.base.mvc.view.component.Select,
  java.text.*, java.util.*"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>



<%
  DecimalFormat nf = new DecimalFormat("#,##0.00");
  DecimalFormat df = new DecimalFormat("#,##0.00%");
  String[][] result = (String[][])request.getAttribute("table_result");



  double[] uncount = (double[])request.getAttribute("uncount");
  String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
  String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
	Calendar thisMonth=Calendar.getInstance();
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;

%>

<Script language="javascript">
  function analyze(){
  if (template.syear.value == '' || template.fmonth.value == '' || template.tmonth.value == '') {
      alert('请选择查询月份!');
      return false;
    }
    if(template.syear.value == "")
    {
      alert('请选择核算年份!');
      return false;
    }
    if(template.fmonth.value == "")
    {
      alert('请选择核算开始月份!');
      return false;
    }
    if(template.tmonth.value == "")
    {
      alert('请选择核算结束月份!');
      return false;
    }
    if(template.fmonth.value > template.tmonth.value ) {
      alert('开始月份不能晚于结束月份!');
      return false;
    }
    show_wait();

    setCBCS_Year1(template.syear.value);
    setCBCS_Month1(template.fmonth.value);
    setCBCS_Month2(template.tmonth.value);

    template.submit();
    return;
  }

function getRsultXml(){

  var tbody = document.getElementById("result_data");
  var xmlStr = "<?xml version='1.0' encoding='GBK'?><root><tbody>";
  var trs = tbody.getElementsByTagName("tr");
  for(var i = 0; i < trs.length; i++){
	xmlStr += "<tr>";
	var tds = trs[i].getElementsByTagName("td");
	for(var j = 0; j < tds.length; j++){
		xmlStr += "<td>"+tds[j].innerHTML+"</td>";
	}
	xmlStr += "</tr>";
  }
  xmlStr += "</tbody></root>"

  return xmlStr.replace(/\&nbsp;&nbsp;/g, "　").replace(/\&nbsp;/g, "");
}

	function datefromat (){
        var myDate=new Date();
    	var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
    	var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
    	var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
    	year='0000'+year;year=year.substring(year.length-4);
    	month='00'+month;month=month.substring(month.length-2);
    	day='00'+day; day=day.substring(day.length-2);

		return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString();
    }

  	function print(){

      	var printTitle="医院成本构成表(医成本C2-01表)";
		var info = "制作日期: "+datefromat();
      	var data=new Object();
      	data["thead_1_1"]=printTitle;
      	data["thead_2_1"]=document.all['syear'].value+'年'+document.all['fmonth'].value+'月到'+document.all['tmonth'].value+'月';
      	data["thead_3_1"]="金额单位：元";

      	data["tfoot_1_1"]= info;
      	data["tfoot_2_1"]="北京望海康信科技有限公司  制作";

      	printXmlToCellByXsltFile(getRsultXml(), 'cbcs/costanalysis/costanalysis/hosCostSumPrintView.xsl', data, false, false);
	}

  function win(flag){
   if (template.syear.value == '' || template.fmonth.value == '' || template.tmonth.value == '')
   {
     alert('请选择核算月！');
      return;
   }

    //  var win=window.open("hosCostSum.jspviewhigh?subFunction=hosCostSumSub&year_month="+template.year_month.value+"&flag="+flag,"","width=800,height=550, scrollbars=1, resizable=yes");
     var win=window.open("hosCostSum.jspviewhigh?subFunction=hosCostSumSub&year_month="+template.syear.value+template.fmonth.value+"&yearMonth2="+template.syear.value+template.tmonth.value+"&flag="+flag,"","width=800,height=550, scrollbars=1, resizable=yes");
  }
  function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }

  function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();

  	year_1  ? template.syear.value=year_1 : "";
    month_1 ? template.fmonth.value=month_1 : "";
    month_2 ? template.tmonth.value=month_2: "";
  }
</Script>

<html:html clazz="main" isPrint="true" fixRows="2">
	<form name="template" method="post" action="hosCostSum.jspviewhigh">
  	<html:message/>
  	<html:title clazz='module'>医院成本构成表(医成本C2-01表)</html:title>
<div id="hhhh" style="font-size:10pt">
</div>
	  <table width=100% cellspacing="2" border="0" >
	    <tr>
	     <td class="signText">核算月：
	     <%=new Select(year,"syear",request.getParameter("syear")==null? String.valueOf(thisMonth.get(Calendar.YEAR)):request.getParameter("syear"),false,true)%>年
	    <%=new Select(month,"fmonth",request.getParameter("fmonth")==null? curMonth:request.getParameter("fmonth"),false,true)%>月 到
	    <%=new Select(month,"tmonth",request.getParameter("tmonth")==null? curMonth:request.getParameter("tmonth"),false,true)%>月</td>

        <td><button class="pageBtn" onclick="return analyze();">计算</button></td>

        <%
        if (result != null && result.length > 1) {%>
        <td><button class="pageBtn"  onclick="return print();">打印</button></td>
        <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
        <%}%>
	    </tr>
	  <tr>
       <td colspan="4"> <br><input type="text" class="findVhTabCtn"/></td>
       <tr>
	  </table>

		<br>

		<html:title clazz='table'>医院成本构成表</html:title>
<vh:vhFixTable fixRow=2 fixCol=1>
			<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
        <colgroup id=tg>
            <col style = <%=DisplayWidth.NAME_WIDTH%>  >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
          </colgroup>
		  <thead>
             <html:tr clazz='white'>
            <td nowrap class="resultLabel" rowspan="2" style='text-align:center;font-weight:bold'>项目</td>
            <td nowrap class="resultLabel" colspan="4" style='text-align:center;font-weight:bold' ><%if(result!=null) out.print("<a href=\"javascript:win('O')\">");%>门诊合计<%if(result!=null) out.print("</a>");%></td>
            <td nowrap class="resultLabel" colspan="4" style='text-align:center;font-weight:bold'><%if(result!=null) out.print("<a href=\"javascript:win('I')\">");%>住院合计<%if(result!=null) out.print("</a>");%></td>
            <td nowrap class="resultLabel" colspan="4" style='text-align:center;font-weight:bold'><%if(result!=null) out.print("<a href=\"javascript:win('S')\">");%>科研教学合计<%if(result!=null) out.print("</a>");%></td>
            <td nowrap class="resultLabel" colspan="4" style='text-align:center;font-weight:bold'><%if(result!=null) out.print("<a href=\"javascript:win('A')\">");%>全院总成本<%if(result!=null) out.print("</a>");%></td>
          </html:tr>

	<!-- second row -->
	      <html:tr clazz='white'>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
      </html:tr>
	  </thead>
      <%  if(result != null){%>
	  <tbody id="result_data">
        <tr  >
          <td nowrap class="normalText">总计</td>
<%
        /** 总计开始 */
          double currOut = 0.0;
          double totalOut = 0.0;
          double currIn = 0.0;
          double totalIn = 0.0;
          double currSci = 0.0;
          double totalSci = 0.0;

          /** 门诊 */

          for(int i = 0; i < result.length; i++)
          {
            if(result[i][4]!=null && result[i][3].equals("Y")){
              currOut = currOut + Double.parseDouble(result[i][4]);
              totalOut = totalOut + Double.parseDouble(result[i][5]);

              currIn = currIn + Double.parseDouble(result[i][6]);
              totalIn = totalIn + Double.parseDouble(result[i][7]);
              currSci = currSci + Double.parseDouble(result[i][8]);
              totalSci = totalSci + Double.parseDouble(result[i][9]);
            }
          }

          //currSci+=(uncount[0]);
          //totalSci+=(uncount[1]);



%>
          <td nowrap class='numberText' style='text-align:right'><%=nf.format(currOut)%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(1)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(totalOut)%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(1)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(currIn)%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(1)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(totalIn)%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(1)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(currSci)%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(1)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(totalSci)%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(1)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(currIn + currOut + currSci)%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(1)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(totalIn + totalOut + totalSci)%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(1)%></td>
    </tr>
<%
  for(int i=0;i<result.length;i++){
    if(result[i][4]!=null){

%>
     <tr >
        <td nowrap class='normalText'><%=result[i][1]%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(Double.parseDouble(result[i][4]))%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(currOut==0?0:Double.parseDouble(result[i][4])/currOut)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(Double.parseDouble(result[i][5]))%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(totalOut==0?0:Double.parseDouble(result[i][5])/totalOut)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(Double.parseDouble(result[i][6]))%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(currIn==0?0:Double.parseDouble(result[i][6])/currIn)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(Double.parseDouble(result[i][7]))%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(totalIn==0?0:Double.parseDouble(result[i][7])/totalIn)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(Double.parseDouble(result[i][8]))%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(currSci==0?0:Double.parseDouble(result[i][8])/currSci)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(Double.parseDouble(result[i][9]))%></td><td nowrap class='numberText' style='text-align:right'><%=df.format(totalSci==0?0:Double.parseDouble(result[i][9])/totalSci)%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(Double.parseDouble(result[i][10]))%></td><td nowrap class='numberText' style='text-align:right'><%=df.format((currIn + currOut + currSci)==0?0:Double.parseDouble(result[i][10])/(currIn + currOut + currSci))%></td><td nowrap class='numberText' style='text-align:right'><%=nf.format(Double.parseDouble(result[i][11]))%></td><td nowrap class='numberText' style='text-align:right'><%=df.format((totalIn + totalOut + totalSci)==0?0:Double.parseDouble(result[i][11])/(totalIn + totalOut + totalSci))%></td>
     </tr>
<%
    }
  }
  }
%>
</tbody>
    	</table>
</vh:vhFixTable>

  	<input type="hidden" name="subFunction" value="hosCostSum">
	</form>
</html:html>
