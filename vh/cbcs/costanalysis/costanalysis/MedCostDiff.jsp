<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costanalysis/MedCostDiff.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime: 03-09-15 9:49 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="java.util.*, java.text.*, com.viewhigh.cbcs.base.mvc.view.MonthComponent,
								com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
	Calendar thisMonth=Calendar.getInstance();
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;
%>
<%!
  public String getColor(String value) {

      int index = value.indexOf("-");

      if(index == -1) {

        if(value.equals("0.00") || value.equals("0.00%")) {
          return "";
        }
        return " style=\"color:red\" ";
      } else {
        return "";
      }

  }

%>

<script language="javascript">

    function analyze() {

      if (template.year_month.value == '') {
        alert('请选择查询月份!');
        return false;
      } else {
        show_wait();
        setCBCS_Year1(template.year_month.value.substr(0,4));
		    setCBCS_Month1(template.year_month.value.substr(4,2));
		    setCBCS_Month2(template.year_month.value.substr(4,2));
        return template.submit();
      }
    }
  function preparedPrin() {
    var win = window.open("medCostDiff.jspviewhigh?subFunction=preparedPrint&year_month=<%=request.getParameter("year_month")%>","","width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1")
  }
function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }
  function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();

  	if(year_1 && month_1 && month_2){
    	template.year_month.setValue(year_1+""+month_1)
    }
  }


	function getRsultXml(){//将显示结果拼成XML格式的字串
	  var xmlStr = "<?xml version='1.0' encoding='GBK'?><root>";
	  var tbody = document.getElementById("result_data");//表体
	  var thead = document.getElementById("result_head");//表头
	  var tbodyStr = "<tbody>"; //表体字串
	  var theadStr = "<thead>"; //表头字串
	  var trs = tbody.getElementsByTagName("tr");
	  var table_width = trs[0].childNodes.length;
	  for(var i = 0; i < trs.length; i++){
		tbodyStr += "<tr>";
		var tds = trs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
            if(j == 0 && tds[j].childNodes[0].nodeType != 3) //有超链接
                tbodyStr += "<td>"+tds[j].childNodes[0].innerHTML+"</td>";
            else
			    tbodyStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		tbodyStr += "</tr>";
	  }
	  tbodyStr += "</tbody>";

	  var htrs = thead.getElementsByTagName("tr");
	  for(var i = 0; i < htrs.length; i++){
		theadStr += "<tr>";
		var tds = htrs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
			theadStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		theadStr += "</tr>";
	  }
	  theadStr += "</thead>";
	  xmlStr += theadStr + tbodyStr + "</root>";
      return xmlStr.replace(/\&nbsp;&nbsp;/g, "　").replace(/\&nbsp;/g, "");
	}

	function datefromat (){
        var myDate=new Date();
    	var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
    	var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
    	var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
    	year='0000'+year;year=year.substring(year.length-4);
    	month='00'+month;month=month.substring(month.length-2);
    	day='00'+day; day=day.substring(day.length-2);

		return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString();
    }

  	function print(){

      	var printTitle="医疗成本比较分析表(医成本C4-05表)";
		var info = "制作日期: "+datefromat();
        var ym = document.all["year_month"].value;
      	var data=new Object();
      	data["thead_1_1"]= printTitle;
      	data["thead_2_1"]= ym.substring(0,4)+"年"+ym.substring(4)+"月";
      	data["thead_3_1"]= "金额单位：元";

      	data["tfoot_1_1"]= info;
      	data["tfoot_2_1"]="北京望海康信科技有限公司  制作";

      	printXmlToCellByXsltFile(getRsultXml(), 'cbcs/costanalysis/costanalysis/MedCostDiffPrintView.xsl', data, false, false);
	}
</script>


<html:html clazz="main" isPrint="true" fixRows="3" fixCols="1">
  <form name="template" method="post" action="medCostDiff.jspviewhigh" />
		<html:message/>
		<html:title clazz='module'>医疗成本比较分析表(医成本C4-05表)</html:title>

	  <table  width="100%" cellspacing="2" border="0" >
	    <tr>
	      <td class="signText" nowrap width="2%">核算月：</td>
	      <td class="normalText"><%=new MonthComponent("year_month", (request.getParameter("year_month")==null?(String.valueOf(thisMonth.get(Calendar.YEAR))+curMonth):(request.getParameter("year_month")))) %></td>
	      <td><button class="pageBtn" onclick="return analyze();">计算</button></td>
        <%
        String[][] result = (String[][])request.getAttribute("table_result");
	      if (result != null && result.length > 1) {%>
        <td><button class="pageBtn" onclick="return print()//preparedPrin();">打印</button></td>
        <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
        <%}%>
	    </tr>
	     <tr >
	  <td colspan="2" ><br> <input type="text" class="findVhTabCtn"/></td>
	  </tr>
	  </table>

	  <br>

		<html:title clazz='table'>医疗成本比较分析表</html:title>
<vh:vhFixTable fixRow=3 fixCol=1>
     <table border='1' bgColor=white borderColor=black style='border:2px solid #000000;BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
          <colgroup id=tg>
                <col style = <%=DisplayWidth.NAME_WIDTH%> >
                <%for (int i = 0; i < 37; i++) {%>
                <col style = <%=DisplayWidth.MONEY_WIDTH%> >
                <%}%>
              </colgroup>
            <thead id="result_head">
              <tr>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' rowspan="3">科室</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="4">实际成本</td>
				<td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="12">与预算成本比</td>
				<td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="12">与去年同期比</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="9">综合比较</td>
              </tr>

              <tr>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">总成本</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">单位成本</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">总成本</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">差异</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">差异率</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">单位成本</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">差异</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">差异率</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">总成本</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">差异</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">差异率</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">单位成本</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">差异</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="2">差异率</td>
		        <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="6">与上期比较</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold' colspan="3">与平均比较</td>
              </tr>

              <tr>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>本期</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>累计</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>总成本</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>差异</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>差异率</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>单位成本</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>差异</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>差异率</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>总成本</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>差异</td>
                <td class="resultLabel" nowrap style='text-align:center;font-weight:bold'>差异率</td>
              </tr>
              </thead>
              <tbody id="result_data">
        <%
           if (result != null)
             {

             DecimalFormat percentFormat = new DecimalFormat("#0.00%");
             DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
//              String[] total = null;


              for (int i = 0; i < result.length; i++ )
              {
                /** reformat */
                for( int j = 5; j <= 35; j++)
                {

                  /** percent format */
                  if(j == 13 || j == 14 ||
                     j == 19 || j == 20 || j == 25 || j == 26 || j == 29 || j == 32 || j == 35)
                  {
                    result[i][j] =
                      percentFormat.format(Double.parseDouble(result[i][j]));
                    continue;
                  }
                  /** money format */
                  result[i][j] =
                    moneyFormat.format(Double.parseDouble(result[i][j]));
                }

                if(result[i][3].equals("类别小计"))
                {
                  int rowSpan = 1;
                  int j = i+1;
                  for( ; j < result.length; j++ )
                  {
                      if( !result[j][0].equals(result[i][0]))
                          break;
                      rowSpan++;
                  }


        %>

                        <tr bgcolor="#E1E1FB" >

                          <td width="9%" height="22"  class="normalText" nowrap><%=result[i][1]%>小计</td>
                          <td width="7%" height="22"  class="numberText" style='text-align:right' nowrap ><%=result[i][5]%></td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap ><%=result[i][6]%></td>
                          <td width="7%" height="22"  class="numberText" style='text-align:right' nowrap >--</td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap >--</td>
                          <td width="7%" height="22"  class="numberText" style='text-align:right' nowrap ><%=result[i][9]%></td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap ><%=result[i][10]%></td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][11]));%>><%=result[i][11]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][12]));%>><%=result[i][12]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][13]));%>><%=result[i][13]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][14]));%>><%=result[i][14]%></td>
                          <td width="7%" height="22"  class="numberText" style='text-align:right' nowrap >--</td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap >--</td>
                          <td width="7%" height="22"  class="numberText" style='text-align:right' nowrap >--</td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap >--</td>
                          <td width="7%" height="22"  class="numberText" style='text-align:right' nowrap >--</td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap >--</td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap ><%=result[i][15]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap ><%=result[i][16]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][17]));%>><%=result[i][17]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][18]));%>><%=result[i][18]%></td>
                          <td width="7%" height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][19]));%>><%=result[i][19]%></td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][20]));%>><%=result[i][20]%></td>
                          <td width="7%" height="22"  class="numberText" style='text-align:right' nowrap >--</td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap >--</td>
                          <td width="7%" height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][23]));%>><%=result[i][23]%></td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][24]));%>><%=result[i][24]%></td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][25]));%>><%=result[i][25]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][26]));%>><%=result[i][26]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap ><%=result[i][27]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][28]));%>><%=result[i][28]%></td>
                          <td width="7%" height="22" b class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][29]));%>><%=result[i][29]%></td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap >--</td>
                          <td width="7%" height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][31]));%>>--</td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][32]));%>>--</td>
                          <td width="7%" height="22"  class="numberText" style='text-align:right' nowrap ><%=result[i][33]%></td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][34]));%>><%=result[i][34]%></td>
                          <td width="8%" height="22"  class="numberText" style='text-align:right' nowrap  <%out.print(getColor(result[i][35]));%>><%=result[i][35]%></td>
                        </tr>
                    <%
			}
                        else
                        {
                    %>
                        <tr >
                          <td height="22"  class="normalText" nowrap><%for(int k = 0; k < Integer.parseInt(result[i][4]) - 1; k++) out.print("&nbsp;");%><%=result[i][3]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][5]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][6]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][36].equals("Y")?result[i][7]:"--"%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][36].equals("Y")?result[i][8]:"--"%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][9]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][10]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][11]));%>><%=result[i][11]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][12]));%>><%=result[i][12]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][13]));%>><%=result[i][13]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][14]));%>><%=result[i][14]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap>--</td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap>--</td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap>--</td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap>--</td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap>--</td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap>--</td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][15]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][16]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][17]));%>><%=result[i][17]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][18]));%>><%=result[i][18]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][19]));%>><%=result[i][19]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][20]));%>><%=result[i][20]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][36].equals("Y")?result[i][21]:"--"%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][36].equals("Y")?result[i][22]:"--"%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][23]));%>><%=result[i][23]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][24]));%>><%=result[i][24]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][25]));%>><%=result[i][25]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][26]));%>><%=result[i][26]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][27]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][28]));%>><%=result[i][28]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][29]));%>><%=result[i][29]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][36].equals("Y")?result[i][30]:"--"%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][31]));%>><%=result[i][36].equals("Y")?result[i][31]:"--"%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][32]));%>><%=result[i][36].equals("Y")?result[i][32]:"--"%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap><%=result[i][33]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][34]));%>><%=result[i][34]%></td>
                          <td height="22"  class="numberText" style='text-align:right' nowrap <%out.print(getColor(result[i][35]));%>><%=result[i][35]%></td>
                        </tr>
                    <%
                        }
                    }
               }

                    %>
                    </tbody>
  	 </table>
    </vh:vhFixTable>

    <input type="hidden" name="subFunction" value="medCostDiff">
	</form>
</html:html>
