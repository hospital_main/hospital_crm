<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:variable name="wnr" select="/root/tbody/tr[1]/td[last()]"/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<xsl:if test="$wnr =1">
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="23"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			
			<xsl:if test="$wnr =0">
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="21"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<xsl:if test="$wnr =1">
			<td noWrap="true" colspan="23" ></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$wnr =0">
			<td noWrap="true" colspan="21" ></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<xsl:if test="$wnr =1">
			<td noWrap="true" colspan="23" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			
			</xsl:if>
			<xsl:if test="$wnr =0">
			<td noWrap="true" colspan="21" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<xsl:if test="$wnr =1">
			<td noWrap="true" colspan="23" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			
			</xsl:if>
			<xsl:if test="$wnr =0">
			<td noWrap="true" colspan="21" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
				</xsl:if>
		</tr>
		<tr noWrap="true" class="mainHead">
			<td noWrap="true" rowspan="3">科室</td>
			<td noWrap="true" colspan="6">合计</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="6">医疗（不含药品）</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="6">其中药品</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>

			<xsl:if test="$wnr =1">
			<td noWrap="true" colspan="2" rowspan="2">未纳入成本项</td>
			<td style="display:none"></td>
			</xsl:if>
			<td noWrap="true" colspan="2" rowspan="2">收支结余</td>
			<td style="display:none"></td>
		</tr>

		<tr noWrap="true" class="mainHead">
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">收入</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">收益</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">收入</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">收益</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">收入</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">收益</td>
			<td style="display:none"></td>
  			
			<xsl:if test="$wnr =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<td style="display:none"></td>
			<td style="display:none"></td>

		</tr>

		 <tr noWrap="true" class="mainHead">
			<td style="display:none"></td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			
			<xsl:if test="$wnr =1">
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			</xsl:if>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
		  </tr>


		</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<xsl:variable name="c" select="position()"/>
			<xsl:variable name="dd" select="last()"/>
        <tr>
			<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                 <xsl:when test="position()=3">
                </xsl:when>
                <xsl:when test="position()=last()">
                </xsl:when>
                <xsl:when test="$wnr=1 and ($c &lt;$dd) and ($c &gt;1) and  (position()=22 or position()=23)">
                	<td></td>
                </xsl:when>
             
                 <xsl:when test="$wnr=1 and ($c=$dd) and  (position()=2 or position()=3 or position()=4 or position()=5 or position()=6
		                 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12
		                  or position()=13 or position()=14 or position()=15 or position()=24 or position()=25)">
		                	<td align="center"></td>
		            </xsl:when>
                <xsl:otherwise>
                   <td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
		<xsl:if test="$wnr =1">
		<td noWrap="true" colspan="23" align="right"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		</xsl:if>
		<xsl:if test="$wnr =0">
		<td noWrap="true" colspan="21" align="right"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		
		</xsl:if>
	</tr>
	<tr>
		<xsl:if test="$wnr =1">
		<td noWrap="true" colspan="23" align="right"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		</xsl:if>
		<xsl:if test="$wnr =0">
		<td noWrap="true" colspan="21" align="right"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		</xsl:if>
	</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
