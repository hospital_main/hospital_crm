<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	
	<thead>
	<xsl:variable name="wnr" select="/root/tbody/tr[1]/td[last()]"/>	
	<tr noWrap="true" class="mainHead">
		<td nowrap="true"   rowspan="3">科室</td>
		<td nowrap="true"   colspan="6">合计</td>
		<td nowrap="true"   colspan="6">药品</td>
		<td nowrap="true"   colspan="6">医疗（不含药品）</td>
		
		<xsl:if test="$wnr =1">
		<td nowrap="true"   rowspan="2" colspan="2">未纳入成本项</td>
		</xsl:if>
		<td nowrap="true"   rowspan="2" colspan="2">收支结余</td>
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true"   colspan="2">收入</td>
		<td noWrap="true"   colspan="2">成本</td>
		<td noWrap="true"   colspan="2">收益</td>
		<td noWrap="true"   colspan="2">收入</td>
		<td noWrap="true"   colspan="2">成本</td>
		<td noWrap="true"   colspan="2">收益</td>
		<td noWrap="true"   colspan="2">收入</td>
		<td noWrap="true"   colspan="2">成本</td>
		<td noWrap="true"   colspan="2">收益</td>
		
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		
		<xsl:if test="$wnr =1">
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		</xsl:if>
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			 <xsl:variable name="v1" select="td[1]"/>
			 <xsl:variable name="v2" select="td[3]"/>
			 <xsl:variable name="c" select="position()"/>
			 <xsl:variable name="dd" select="last()"/>
			 <xsl:choose>
			 	<xsl:when test="$v2='1' or $v2='2'">
			 <tr bgcolor="#E1E1FB">
					 <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                  <td noWrap="true">
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                 <xsl:when test="position()=3">
                </xsl:when>
                <xsl:when test="position()=last()">
                </xsl:when>
                <xsl:when test="$wnr=1 and ($c &lt;$dd) and ($c &gt;1) and  (position()=22 or position()=23)">
                	<td></td>
                </xsl:when>
               
                 <xsl:when test="$wnr=1 and ($c=$dd) and  (position()=2 or position()=3 or position()=4 or position()=5 or position()=6
		                 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12
		                 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17 or position()=18 
		                 or position()=19 or position()=20 or position()=21 or position()=24 or position()=25)">
		                	<td align="center"></td>
		        </xsl:when>
                <xsl:otherwise>
                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
                </xsl:when>
                <xsl:otherwise>
                <tr>
			<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                  <td noWrap="true">
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                 <xsl:when test="position()=3">
                </xsl:when>
                 <xsl:when test="position()=last()">
                </xsl:when>
                <xsl:when test="$wnr=1 and ($c &lt;$dd) and ($c &gt;1) and  (position()=22 or position()=23)">
                	<td></td>
                </xsl:when>
              
                <xsl:when test="$wnr=1 and ($c=$dd) and  (position()=2 or position()=3 or position()=4 or position()=5 or position()=6
		                 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12
		                 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17 or position()=18 
		                 or position()=19 or position()=20 or position()=21 or position()=24 or position()=25)">
		                	<td align="center"></td>
		        </xsl:when>
                <xsl:otherwise>
                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
                </xsl:otherwise>
			 </xsl:choose>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
