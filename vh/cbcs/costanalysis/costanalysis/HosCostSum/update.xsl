<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='0.00'/>
	<xsl:template match="/">
	<thead>
	<tr class="mainHead">
		<td nowrap="true" rowspan="2">项目</td>
		<xsl:for-each select="/root/tbody/tr[1]/td">
			<xsl:choose>
		        <xsl:when test="position()&lt;8">
		        </xsl:when>
		        <xsl:when test="position()mod 3 =0">
		        	<td nowrap="true" colspan="2">
		        	 <xsl:value-of select="."/>
		        	</td>
		        </xsl:when>
		        <xsl:otherwise>
		        </xsl:otherwise>
	     	</xsl:choose>
		</xsl:for-each>
	</tr>

	<tr class="mainHead">
		<xsl:for-each select="/root/tbody/tr[1]/td">
			<xsl:choose>
		        <xsl:when test="position()&lt;8">
		        </xsl:when>
		        <xsl:when test="position()mod 3 =0">
		        	<td noWrap="true">本期</td>
					<td noWrap="true">累计</td>
		        </xsl:when>
		        <xsl:otherwise>
		        </xsl:otherwise>
	     	</xsl:choose>
		</xsl:for-each>
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                	<td>
		                	 <xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()mod 3 =0">
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
