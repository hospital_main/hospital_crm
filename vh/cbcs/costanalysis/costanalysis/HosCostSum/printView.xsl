<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='0.00'/>
	<xsl:variable name="ky" select="/root/tbody/tr[1]/td[last()-3]"/>
	<xsl:variable name="jx" select="/root/tbody/tr[1]/td[last()-2]"/>
	<xsl:variable name="gy" select="/root/tbody/tr[1]/td[last()-1]"/>
	<xsl:variable name="fz" select="/root/tbody/tr[1]/td[last()]"/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="colcount"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			
			<xsl:if test="$ky =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$jx =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$gy =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$fz =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" ></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			
			<xsl:if test="$ky =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$jx =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$gy =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$fz =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			
			<xsl:if test="$ky =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$jx =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$gy =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$fz =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			
			<xsl:if test="$ky =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$jx =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$gy =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$fz =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
		<td nowrap="true"  rowspan="2">项目</td>
		<td nowrap="true"  colspan="4">门诊合计</td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td nowrap="true"  colspan="4">住院合计</td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<xsl:if test="$ky =1">
		<td nowrap="true"  colspan="4">科研合计</td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		</xsl:if>
		<xsl:if test="$jx =1">
		<td nowrap="true"  colspan="4">教学合计</td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		</xsl:if>
		<xsl:if test="$gy =1">
		<td nowrap="true"  colspan="4">公用成本合计</td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		</xsl:if>
		<xsl:if test="$fz =1">
		<td nowrap="true"  colspan="4">医辅成本合计</td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		</xsl:if>
		<td nowrap="true"  colspan="4">全院总成本</td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
	</tr>

		 <tr noWrap="true" class="mainHead">
		<td style="display:none"></td>
		<td noWrap="true" >本期</td>
		<td noWrap="true" >比例</td>
		<td noWrap="true" >累计</td>
		<td noWrap="true" >比例</td>
		<td noWrap="true" >本期</td>
		<td noWrap="true" >比例</td>
		<td noWrap="true" >累计</td>
		<td noWrap="true" >比例</td>
		
		<xsl:if test="$ky =1">
		<td noWrap="true" >本期</td>
		<td noWrap="true" >比例</td>
		<td noWrap="true" >累计</td>
		<td noWrap="true" >比例</td>
		</xsl:if>
		<xsl:if test="$jx =1">
		<td noWrap="true" >本期</td>
		<td noWrap="true" >比例</td>
		<td noWrap="true" >累计</td>
		<td noWrap="true" >比例</td>
		</xsl:if>
		<xsl:if test="$gy =1">
		<td noWrap="true" >本期</td>
		<td noWrap="true" >比例</td>
		<td noWrap="true" >累计</td>
		<td noWrap="true" >比例</td>
		</xsl:if>
		<xsl:if test="$fz =1">
		<td noWrap="true" >本期</td>
		<td noWrap="true" >比例</td>
		<td noWrap="true" >累计</td>
		<td noWrap="true" >比例</td>
		</xsl:if>
		<td noWrap="true" >本期</td>
		<td noWrap="true" >比例</td>
		<td noWrap="true" >累计</td>
		<td noWrap="true" >比例</td>
		  </tr>


		</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                	<td>
		                	 <xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="(position()mod 2=0) and (position() &lt; last()-3) and (position() &gt; 4)">
		                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
						<xsl:when test="(position()=last()) or (position()=last()-1) or (position()=last()-2) or (position()=last()-3)">
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
			<td noWrap="true" colspan="colcount" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			
			<xsl:if test="$ky =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$jx =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$gy =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$fz =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
	<tr>
			<td noWrap="true" colspan="colcount" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			
			<xsl:if test="$ky =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$jx =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$gy =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$fz =1">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</xsl:if>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
