<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='0.00'/>
	<xsl:template match="/">
	
	<thead>
	<xsl:variable name="ky" select="/root/tbody/tr[1]/td[last()-3]"/>
	<xsl:variable name="jx" select="/root/tbody/tr[1]/td[last()-2]"/>
	<xsl:variable name="gy" select="/root/tbody/tr[1]/td[last()-1]"/>
	<xsl:variable name="fz" select="/root/tbody/tr[1]/td[last()]"/>	
	<tr noWrap="true" class="mainHead">
		<td nowrap="true"  rowspan="2">项目</td>
		<td nowrap="true"  colspan="4">
		<a href="#" onclick="win('mz')">门诊合计</a></td>
		<td nowrap="true"  colspan="4">
		<a href="#" onclick="win('zy')">住院合计</a></td>
		<xsl:if test="$ky =1">
		<td nowrap="true"  colspan="4">
		<a href="#" onclick="win('ky')">科研合计</a></td>
		</xsl:if>
		<xsl:if test="$jx =1">
		<td nowrap="true"  colspan="4">
		<a href="#" onclick="win('jx')">教学合计</a></td>
		</xsl:if>
		<xsl:if test="$gy =1">
		<td nowrap="true"  colspan="4">
		<a href="#" onclick="win('gy')">公用成本合计</a></td>
		</xsl:if>
		<xsl:if test="$fz =1">
		<td nowrap="true"  colspan="4">
		<a href="#" onclick="win('fz')">医辅成本合计</a></td>
		</xsl:if>
		<td nowrap="true"  colspan="4">
		<a href="#" onclick="win('sum')">临床科室总成本</a></td>
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >比例</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >比例</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >比例</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >比例</td>
		
		<xsl:if test="$ky =1">
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >比例</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >比例</td>
		</xsl:if>
		<xsl:if test="$jx =1">
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >比例</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >比例</td>
		</xsl:if>
		<xsl:if test="$gy =1">
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >比例</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >比例</td>
		</xsl:if>
		<xsl:if test="$fz =1">
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >比例</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >比例</td>
		</xsl:if>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >比例</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >比例</td>
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                	<td>
		                	 <xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="(position()mod 2=0) and (position() &lt; last()-3) and (position() &gt; 4)">
		                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                </xsl:when>
						<xsl:when test="(position()=last()) or (position()=last()-1) or (position()=last()-2) or (position()=last()-3)">
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
