<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='0.00'/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="colcount"></td>
			<xsl:for-each select="/root/tbody/tr[1]/td">
				<xsl:choose>
			        <xsl:when test="position()&lt;8">
			        </xsl:when>
			        <xsl:when test="position()mod 3 =0">
			        	<td style="display:none"></td>
						<td style="display:none"></td>
			        </xsl:when>
			        <xsl:otherwise>
			        </xsl:otherwise>
		     	</xsl:choose>
			</xsl:for-each>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" ></td>
			<xsl:for-each select="/root/tbody/tr[1]/td">
				<xsl:choose>
			        <xsl:when test="position()&lt;8">
			        </xsl:when>
			        <xsl:when test="position()mod 3 =0">
			        	<td style="display:none"></td>
						<td style="display:none"></td>
			        </xsl:when>
			        <xsl:otherwise>
			        </xsl:otherwise>
		     	</xsl:choose>
			</xsl:for-each>
			
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="left"></td>
			<xsl:for-each select="/root/tbody/tr[1]/td">
				<xsl:choose>
			        <xsl:when test="position()&lt;8">
			        </xsl:when>
			        <xsl:when test="position()mod 3 =0">
			        	<td style="display:none"></td>
						<td style="display:none"></td>
			        </xsl:when>
			        <xsl:otherwise>
			        </xsl:otherwise>
		     	</xsl:choose>
			</xsl:for-each>
			
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="left"></td>
			<xsl:for-each select="/root/tbody/tr[1]/td">
				<xsl:choose>
			        <xsl:when test="position()&lt;8">
			        </xsl:when>
			        <xsl:when test="position()mod 3 =0">
			        	<td style="display:none"></td>
						<td style="display:none"></td>
			        </xsl:when>
			        <xsl:otherwise>
			        </xsl:otherwise>
		     	</xsl:choose>
			</xsl:for-each>
		</tr>
		<tr>
		<td nowrap="true"  rowspan="2">项目</td>
		<xsl:for-each select="/root/tbody/tr[1]/td">
			<xsl:choose>
		        <xsl:when test="position()&lt;8">
		        </xsl:when>
		        <xsl:when test="position()mod 3 =0">
		        	<td colspan="2"><xsl:value-of select="."/></td>
		        	<td style="display:none"></td>
		        </xsl:when>
		        <xsl:otherwise>
		        </xsl:otherwise>
	     	</xsl:choose>
		</xsl:for-each>
	</tr>

		 <tr noWrap="true" class="mainHead">
			<td style="display:none"></td>
			<xsl:for-each select="/root/tbody/tr[1]/td">
				<xsl:choose>
			        <xsl:when test="position()&lt;8">
			        </xsl:when>
			        <xsl:when test="position()mod 3 =0">
			        	<td noWrap="true">本期</td>
						<td noWrap="true">累计</td>
			        </xsl:when>
			        <xsl:otherwise>
			        </xsl:otherwise>
		     	</xsl:choose>
			</xsl:for-each>
		 </tr>
   </thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=2">
		                	<td>
		                	 <xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()mod 3 =0">
		                </xsl:when>
		                <xsl:otherwise>
		                   <td noWrap="true" align="right" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>
		          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
			<td noWrap="true" colspan="colcount" align="right"></td>
			<xsl:for-each select="/root/tbody/tr[1]/td">
				<xsl:choose>
			        <xsl:when test="position()&lt;8">
			        </xsl:when>
			        <xsl:when test="position()mod 3 =0">
			        	<td style="display:none"></td>
						<td style="display:none"></td>
			        </xsl:when>
			        <xsl:otherwise>
			        </xsl:otherwise>
		     	</xsl:choose>
			</xsl:for-each>
		</tr>
	<tr>
			<td noWrap="true" colspan="colcount" align="right"></td>
			<xsl:for-each select="/root/tbody/tr[1]/td">
				<xsl:choose>
			        <xsl:when test="position()&lt;8">
			        </xsl:when>
			        <xsl:when test="position()mod 3 =0">
			        	<td style="display:none"></td>
						<td style="display:none"></td>
			        </xsl:when>
			        <xsl:otherwise>
			        </xsl:otherwise>
		     	</xsl:choose>
			</xsl:for-each>
		</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
