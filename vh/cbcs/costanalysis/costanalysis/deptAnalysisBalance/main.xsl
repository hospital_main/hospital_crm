<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
	<tr noWrap="true" class="mainHead">
		<td nowrap="true"  rowspan="2">项目</td>
		<td nowrap="true"  rowspan="2">合计</td>
		<td nowrap="true"  colspan="3">门诊</td>
		<td nowrap="true"  colspan="5">住院</td>
	</tr>
	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  >门诊费用</td>
		<td noWrap="true"  >门诊人次</td>
		<td noWrap="true"  >诊次费用</td>	
		<td noWrap="true"  >住院费用</td>	
		<td noWrap="true"  >住院床日</td>	
		<td noWrap="true"  >床日费用</td>	
		<td noWrap="true"  >出院人数</td>
		<td noWrap="true"  >出院人均费用</td>	
	</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
        	<tr>
		 <xsl:for-each select="td">
                	<xsl:choose>
                		<xsl:when test="position()=1">
                		 	<td height="22">
                			   <xsl:value-of select="."/>
                			</td>
                		</xsl:when>
                		<xsl:otherwise>
                			<td noWrap="true" class="numberText">
	                   			<xsl:if test=". != '' and . !=0">
	                   			<xsl:value-of select="format-number(.,'#,##0.00')"/>
	                   			</xsl:if>
                   			</td>
                		</xsl:otherwise>
                	</xsl:choose>
                
          	</xsl:for-each>
		</tr>
		</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
