<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costanalysis/hospitalFour.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime:$
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DisplayWidth"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
	String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
	Calendar thisMonth=Calendar.getInstance();
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;
%>

<script language="javascript">

  function analyze() {

    if(template.syear.value == '' || template.fmonth.value == '' || template.tmonth.value == ''){
    	alert('请选择月份!');
      return false;
    }
    if(template.fmonth.value > template.tmonth.value ) {
      alert('开始月份不能晚于结束月份!');
      return false;
    }

    setCBCS_Year1(template.syear.value);
    setCBCS_Month1(template.fmonth.value);
    setCBCS_Month2(template.tmonth.value);
    template.submit()
    show_wait();
    return ;
  }

  function preparedPrin(){
  grid.print();
  }
function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }
  function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();

  	year_1  ? template.syear.value=year_1 : "";
    month_1 ? template.fmonth.value=month_1 : "";
    month_2 ? template.tmonth.value=month_2: "";
  }

	function getRsultXml(){//将显示结果拼成XML格式的字串
	  var xmlStr = "<?xml version='1.0' encoding='GBK'?><root>";
	  var tbody = document.getElementById("result_data");//表体
	  var thead = document.getElementById("result_head");//表头
	  var tbodyStr = "<tbody>"; //表体字串
	  var theadStr = "<thead>"; //表头字串
	  var trs = tbody.getElementsByTagName("tr");
	  var table_width = trs[0].childNodes.length;
	  for(var i = 0; i < trs.length; i++){
		tbodyStr += "<tr>";
		var tds = trs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
			tbodyStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		tbodyStr += "</tr>";
	  }
	  tbodyStr += "</tbody>";

	  var htrs = thead.getElementsByTagName("tr");
	  for(var i = 0; i < htrs.length; i++){
		theadStr += "<tr>";
		var tds = htrs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
			theadStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		theadStr += "</tr>";
	  }
	  theadStr += "</thead>";
	  xmlStr += theadStr + tbodyStr + "</root>";
      return xmlStr.replace(/\&nbsp;&nbsp;/g, "　").replace(/\&nbsp;/g, "");
	}

	function datefromat (){
        var myDate=new Date();
    	var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
    	var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
    	var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
    	year='0000'+year;year=year.substring(year.length-4);
    	month='00'+month;month=month.substring(month.length-2);
    	day='00'+day; day=day.substring(day.length-2);

		return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString();
    }

  	function print(){

      	var printTitle="成本分类分析表四[按项目构成划分](医成本C3-04表)";
		var info = "制作日期: "+datefromat();
      	var data=new Object();
      	data["thead_1_1"]= printTitle;
      	data["thead_2_1"]= document.all['syear'].value+'年'+document.all['fmonth'].value+'月到'+document.all['tmonth'].value+'月';
      	data["thead_3_1"]= "金额单位：元";

      	data["tfoot_1_1"]= info;
      	data["tfoot_2_1"]="北京望海康信科技有限公司  制作";
      	printXmlToCellByXsltFile(getRsultXml(), 'cbcs/costanalysis/costanalysis/hospitalFourPrintView.xsl', data, false, false);
	}
</script>


<html:html clazz="main" isPrint="true" fixRows="3">
  <form name="template" method="post" action="hospital.jspviewhigh" />
   	<html:message/>
   	<html:title clazz='module'>成本分类分析表四[按项目构成划分](医成本C3-04表)</html:title>

     <table  width="100%" cellspacing="2" border="0" >
      <tr>
        <td class="signText" nowrap width="10%">核算月：</td>
        <td class="signText"><%=new Select(year,"syear",request.getParameter("syear")==null? String.valueOf(thisMonth.get(Calendar.YEAR)):request.getParameter("syear"),false,true)%>年
	 <%=new Select(month,"fmonth",request.getParameter("fmonth")==null? curMonth:request.getParameter("fmonth"),false,true)%>月 到
	 <%=new Select(month,"tmonth",request.getParameter("tmonth")==null? curMonth:request.getParameter("tmonth"),false,true)%>月</td>
        <td><button class="pageBtn" onclick="return analyze();">计算</button></td>
        <%
	      if (request.getAttribute("result")!=null) {%>
        <td><button class="pageBtn"  onclick="return print()// preparedPrintWithIndent();">打印</button></td>
        <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
        <%}%>
      </tr>
    </table>
    <br>
    <html:title clazz='table'>成本分类分析表四[按项目构成划分]</html:title>

<vh:vhFixTable fixRow=3 fixCol=1>
     <table border='1' bgColor=white borderColor=black style='border:2px solid #000000;BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
      		      <colgroup id=tg>
        <col style = <%=DisplayWidth.NAME_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>
        <col style = <%=DisplayWidth.MONEY_WIDTH%>>



      </colgroup>
<thead id="result_head">
      <html:tr clazz='white'>
        <td nowrap class="resultLabel" rowspan="3" style='text-align:center;font-weight:bold'>科室</td>
        <td nowrap class="resultLabel" colspan="2" rowspan="2" style='text-align:center;font-weight:bold'>实际成本</td>
        <td nowrap class="resultLabel" colspan="4" style='text-align:center;font-weight:bold'>人力成本</td>
        <td nowrap class="resultLabel" colspan="4" style='text-align:center;font-weight:bold'>离退休人员成本</td>
        <td nowrap class="resultLabel" colspan="4" style='text-align:center;font-weight:bold'>材料成本</td>
        <td nowrap class="resultLabel" colspan="4" style='text-align:center;font-weight:bold'>净药品成本</td>
        <td nowrap class="resultLabel" colspan="4" style='text-align:center;font-weight:bold'>折旧成本</td>
        <td nowrap class="resultLabel" colspan="4" style='text-align:center;font-weight:bold'>其它成本</td>

      </html:tr>
      <html:tr clazz='white'>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>成本额</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>成本比例</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>成本额</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>成本比例</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>成本额</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>成本比例</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>成本额</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>成本比例</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>成本额</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>成本比例</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>成本额</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>成本比例</td>

      </html:tr>

      <html:tr clazz='white'>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
		    <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>


      </html:tr>
</thead>
<tbody id="result_data">
<%
    if(request.getAttribute("result")!=null){
      String[][] result=(String[][])request.getAttribute("result");
      if(result!=null && result.length > 0){
        for(int i=0;i<result.length;i++) {
		  if(result[i][0].endsWith("小计")){
%>
    	  <tr bgcolor="#E1E1FB" >
<%
		  } else {
%>
		  <tr>
<%
      }
		  for(int j = 0;j<result[i].length;j++){
		    if(j==0){//显示科室名称
%>
			  <td  nowrap class="numberText" style='text-align:left'><%=result[i][j]%></td>
<%
			  } else{
%>
              <td  nowrap class="numberText" style='text-align:right'><%=result[i][j]%></td>
<%
		    }
		  }
%>
         </tr>
<%
      }
  	}
	}

%>
</tbody>
     </table>
    </vh:vhFixTable>
		<input type=hidden name="subFunction" value="find"/>
		        <input type=hidden name="sqlId" value="4"/>

	</form>
</html:html>
