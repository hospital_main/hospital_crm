<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		
	<tr noWrap="true" class="mainHead">
		<td nowrap="true"  rowspan="3">科室</td>
		<td nowrap="true"  colspan="4">实际成本</td>
		<td nowrap="true"  colspan="12">与预算成本比</td>
		<td nowrap="true"  colspan="12">与去年同期比</td>
		<td nowrap="true"  colspan="9">综合比较</td>
	</tr>
	<tr noWrap="true" class="mainHead">
		<td nowrap="true"  colspan="2">总成本</td>
		<td nowrap="true"  colspan="2">单位成本</td>
		<td nowrap="true"  colspan="2">总成本</td>
		<td nowrap="true"  colspan="2">差异</td>
		<td nowrap="true"  colspan="2">差异率</td>
		<td nowrap="true"  colspan="2">单位成本</td>
		<td nowrap="true"  colspan="2">差异</td>
		<td nowrap="true"  colspan="2">差异率</td>
		
		<td nowrap="true"  colspan="2">总成本</td>
		<td nowrap="true"  colspan="2">差异</td>
		<td nowrap="true"  colspan="2">差异率</td>
		<td nowrap="true"  colspan="2">单位成本</td>
		<td nowrap="true"  colspan="2">差异</td>
		<td nowrap="true"  colspan="2">差异率</td>
		
		<td nowrap="true"  colspan="6">与上期比较</td>
		<td nowrap="true"  colspan="3">与平均比较</td>
	
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td nowrap="true"  >总成本</td>
		<td nowrap="true"  >差异</td>
		<td nowrap="true"  >差异率</td>
		<td nowrap="true"  >单位成本</td>
		<td nowrap="true"  >差异</td>
		<td nowrap="true"  >差异率</td>
		<td nowrap="true"  >总成本</td>
		<td nowrap="true"  >差异</td>
		<td nowrap="true"  >差异率</td>
			
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			 <xsl:variable name="v2" select="td[4]"/>
			 <xsl:variable name="vv" select="td[2]"/>
			 <xsl:variable name="d_code" select="td[1]"/>
			 <xsl:variable name="dept_last_level" select="td[6]"/>
			 <xsl:choose>
			 	<xsl:when test="$v2='1'">
			 <tr bgcolor="#E1E1FB">
			<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                </xsl:when>
                <xsl:when test="position()=3">
                 	<td noWrap="true">
                    <xsl:value-of select="."/>
                  	</td>
                </xsl:when>
                <xsl:when test="position()=4">
                </xsl:when>
                <xsl:when test="position()=5">
                </xsl:when>
                <xsl:when test="position()=6">
                </xsl:when>
                <xsl:when test="position()=13 or position()=14 or position()=25 or position()=26 or position()=31 or position()=32 or position()=36 or position()=39 or position()=42">
                	<xsl:choose>
                		<xsl:when test=". &gt; 0">
                			<td style="color:red" noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                		</xsl:when>
                		<xsl:otherwise>
                			<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                		</xsl:otherwise>
                	</xsl:choose>
                </xsl:when>
                <xsl:when test="position()=15 or position()=16 or position()=27 or position()=28 or position()=33 or position()=34 or position()=37 or position()=40 or position()=43">
                	<xsl:choose>
                		<xsl:when test=". &gt; 0">
                			<td style="color:red" noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                		</xsl:when>
                		<xsl:otherwise>
                			<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                		</xsl:otherwise>
                	</xsl:choose>
                </xsl:when>
                <xsl:when test="position()=17 or position()=18 or position()=19 or position()=20 or position()=21 or position()=22">
                	<td align="right">--</td>
                </xsl:when>
                <xsl:when test="(position()=9 or position()=10) and $d_code='00D'">
               		<td noWrap="true" class="numberText"></td>
                </xsl:when>
                <xsl:otherwise>
               		<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
                </xsl:when>
                <xsl:otherwise>
                <tr>
			<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                </xsl:when>
                <xsl:when test="position()=3">
                 	<td noWrap="true">
                    <xsl:value-of select="."/>
                  	</td>
                </xsl:when>
                <xsl:when test="position()=4">
                </xsl:when>
                <xsl:when test="position()=5">
                </xsl:when>
                <xsl:when test="position()=6">
                </xsl:when>
                <xsl:when test="position()=13 or position()=14 or position()=25 or position()=26 or position()=31 or position()=32 or position()=36 or position()=39 or position()=42">
                	<xsl:choose>
                		<xsl:when test=". &gt; 0">
                			<td style="color:red" noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                		</xsl:when>
                		<xsl:otherwise>
                			<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                		</xsl:otherwise>
                	</xsl:choose>
                </xsl:when>
                <xsl:when test="position()=15 or position()=16 or position()=27 or position()=28 or position()=33 or position()=34 or position()=37 or position()=40 or position()=43">
                	<xsl:choose>
                		<xsl:when test=". &gt; 0">
                			<td style="color:red" noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                		</xsl:when>
                		<xsl:otherwise>
                			<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                		</xsl:otherwise>
                	</xsl:choose>
                </xsl:when>
                <xsl:when test="position()=17 or position()=18 or position()=19 or position()=20 or position()=21 or position()=22">
                	<td align="right">--</td>
                </xsl:when>
                <xsl:when test="(position()=9 or position()=10) and starts-with($vv, '00D') and $dept_last_level!='Y'">
               		<td noWrap="true" class="numberText"></td>
                </xsl:when>
                <xsl:otherwise>
                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
                </xsl:otherwise>
			 </xsl:choose>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
