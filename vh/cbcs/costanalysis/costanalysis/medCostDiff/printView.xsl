<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="38"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="38" ></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="38" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>		
		</tr>
		<tr>
			<td noWrap="true" colspan="38" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr noWrap="true" class="mainHead">
			<td nowrap="true"  rowspan="3">科室</td>
			<td nowrap="true"  colspan="4">实际成本</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>	
			<td nowrap="true"  colspan="12">与预算成本比</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>	
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>	
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>	
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="12">与去年同期比</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>	
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>	
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>	
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="9">综合比较</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>	
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>	
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		
		<tr>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">总成本</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">单位成本</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">总成本</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">差异</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">差异率</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">单位成本</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">差异</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">差异率</td>
			<td style="display:none"></td>
			
			<td nowrap="true"  colspan="2">总成本</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">差异</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">差异率</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">单位成本</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">差异</td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="2">差异率</td>
			<td style="display:none"></td>
			
			<td nowrap="true"  colspan="6">与上期比较</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="3">与平均比较</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
	
		</tr>

		 <tr noWrap="true" class="mainHead">
		 	<td style="display:none"></td>
		 	<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td noWrap="true" >本期</td>
			<td noWrap="true" >累计</td>
			<td nowrap="true"  >总成本</td>
			<td nowrap="true"  >差异</td>
			<td nowrap="true"  >差异率</td>
			<td nowrap="true"  >单位成本</td>
			<td nowrap="true"  >差异</td>
			<td nowrap="true"  >差异率</td>
			<td nowrap="true"  >总成本</td>
			<td nowrap="true"  >差异</td>
			<td nowrap="true"  >差异率</td>
		  </tr>
		</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
                <tr>
			<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                </xsl:when>
                <xsl:when test="position()=3">
                 	<td noWrap="true">
                    <xsl:value-of select="."/>
                  	</td>
                </xsl:when>
                <xsl:when test="position()=4">
                </xsl:when>
                <xsl:when test="position()=5">
                </xsl:when>
                <xsl:when test="position()=6">
                </xsl:when>
                <xsl:when test="position()=13 or position()=14 or position()=25 or position()=26 or position()=31 or position()=32 or position()=36 or position()=39 or position()=42">
                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:when test="position()=15 or position()=16 or position()=27 or position()=28 or position()=33 or position()=34 or position()=37 or position()=40 or position()=43">
                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=17 or position()=18 or position()=19 or position()=20 or position()=21 or position()=22">
                	<td align="right">--</td>
                </xsl:when>
                <xsl:otherwise>
                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
		<td noWrap="true" colspan="38" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
	</tr>
	<tr>
		<td noWrap="true" colspan="38" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
	</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
