<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costanalysis/HosCostSumSub.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime:$
 $Revision: 1.1 $
-->
<%@ page language = "java" contentType = "text/html;charset=GBK" errorPage = "error.jsp" %>
<%@ page import = "com.viewhigh.cbcs.cbcs.util.DisplayWidth,java.text.*, java.util.*"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language='javascript' src='javascript/tag.js'></script>
<%
  DecimalFormat nf = new DecimalFormat("#,##0.00");
  String[][] result = (String[][])request.getAttribute("table_result");
  String[][] init_dept = (String[][])request.getAttribute("init_dept");
  String flag = request.getParameter("flag");
  String isPrint="false";
  String dept;
  if(result!=null)
    isPrint="true";
  if(flag!=null && flag.equals("O"))
    dept="dept_O";
  else if(flag!=null && flag.equals("I"))
    dept="dept_I";
  else if(flag!=null && flag.equals("S"))
    dept="dept_S";
  else
    dept="dept_A";
  String[] select_dept = request.getParameterValues("dept");
  String array = "(";
  if(select_dept!=null){
    for (int i = 0; i < select_dept.length - 1; i++) {
      array = array + ("'" + select_dept[i] + "',");
    }
    array = array + "'" + select_dept[select_dept.length - 1] + "'";
  }
  array = array + ")";
%>

<%!
	public String getTitle(HttpServletRequest rq){
	//取得核算日期并拼成标题字串 如：2009年01月到02月
		String title = "";

		if (rq.getParameter("year_month").substring(4,6).equals(rq.getParameter("yearMonth2").substring(4,6)))
			title = rq.getParameter("year_month").substring(0,4)+"年"+rq.getParameter("year_month").substring(4,6)+"月";
		else
			title = rq.getParameter("year_month").substring(0,4)+"年"+rq.getParameter("year_month").substring(4,6)+"月到"+rq.getParameter("yearMonth2").substring(4,6)+"月";

		return title;
	}
%>


<Script language="javascript">
  function analyze(){
    if(template.year_month.value == "")
    {
      alert('请选择查询月份!');
      return;
    }
    if(template.dept.length==0){
      alert('请选择科室')
      return
    }
    if(template.dept.length>9){
      alert('最多只能选择九个科室')
      return
    }
    show_wait();
    template.submit();
    return;
  }

  function preparedPrin(){
    var win = window.open("hosCostSum.jspviewhigh?subFunction=preparedPrintSub&year_month=<%=request.getParameter("year_month")%>&yearMonth2=<%=request.getParameter("yearMonth2")%>&dept=<%=array%>","","width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1")
  }

	function getRsultXml(){//将显示结果拼成XML格式的字串
	  var xmlStr = "<?xml version='1.0' encoding='GBK'?><root>";
	  var tbody = document.getElementById("result_data");//表体
	  var thead = document.getElementById("result_head");//表头
	  var tbodyStr = "<tbody>"; //表体字串
	  var theadStr = "<thead>"; //表头字串
	  var trs = tbody.getElementsByTagName("tr");
	  var table_width = trs[0].childNodes.length;
	  for(var i = 0; i < trs.length; i++){
		tbodyStr += "<tr>";
		var tds = trs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
			tbodyStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		tbodyStr += "</tr>";
	  }
	  tbodyStr += "</tbody>";

	  var htrs = thead.getElementsByTagName("tr");
	  for(var i = 0; i < htrs.length; i++){
		theadStr += "<tr>";
		var tds = htrs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
			theadStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		theadStr += "</tr>";
	  }
	  theadStr += "</thead>";
	  xmlStr += theadStr + tbodyStr + "</root>";
	  return xmlStr.replace(/\&nbsp;&nbsp;/g, "　").replace(/\&nbsp;/g, "");
	}

	function datefromat (){
        var myDate=new Date();
    	var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
    	var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
    	var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
    	year='0000'+year;year=year.substring(year.length-4);
    	month='00'+month;month=month.substring(month.length-2);
    	day='00'+day; day=day.substring(day.length-2);

		return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString();
    }

  	function print(){

      	var printTitle="医院成本构成总表明细（"+document.getElementById("sub_head").innerText.replace("：", "")+"）";
		var info = "制作日期: "+datefromat();
      	var data=new Object();
      	data["thead_1_1"]= printTitle;
      	data["thead_2_1"]= "<%=getTitle(request)%>";
      	data["thead_3_1"]= "金额单位：元";

      	data["tfoot_1_1"]= info;
      	data["tfoot_2_1"]="北京望海康信科技有限公司  制作";
      	printXmlToCellByXsltFile(getRsultXml(), 'cbcs/costanalysis/costanalysis/hosCostSumSubPrintView.xsl', data, false, false);
	}
</Script>
<html:html clazz='child' isPrint="<%=isPrint%>" fixRows='2'>

	<form name="template" method="post" action="hosCostSum.jspviewhigh">
  	<html:message/>

 	<html:title clazz='module'>医院成本构成总表明细（核算月：<%=getTitle(request)%>）</html:title>

	  <table width="100%" cellspacing="2" border="0" >
	    <tr>
	      <td class="signText" nowrap id="sub_head"><%if(flag.equals("O")) out.print("门诊");else if(flag.equals("I")) out.print("住院");else if(flag.equals("S")) out.print("科研教学");else out.print("全院");%>科室：</td>
	      <td class="signText"><html:select property="<%=dept%>" name='dept' size='4' biSelect='true' showTip='false'/></td>
	      <td><button class="pageBtn" onclick="return analyze();">计算</button></td>
	      <%if(result!=null){%><td>
	      <button class="pageBtn" onclick="return print();">打印</button></td><%}%>
	    </tr>
	  </table>
		<br>
<%
   if(result!=null){
%>
    <html:title clazz='table'>医院成本构成总表明细</html:title>
<vh:vhFixTable fixRow=2 fixCol=1>
	<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
    <colgroup id=tg>
            <col style = <%=DisplayWidth.NAME_WIDTH%> >
<%
       for(int i=0;i<=init_dept.length;i++){
%>
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
<%
       }
%>
          </colgroup>
<thead id="result_head">
          <!-- first row -->
          <tr>
            <td nowrap class="resultLabel" rowspan="2" style='text-align:center;font-weight:bold'>项目</td>
<%
        for(int i=0;i<init_dept.length;i++){
%>
            <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold' ><%=init_dept[i][1]%></td>
<%}%>
            <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold' >合计</td>
          </tr>

	<!-- second row -->
	      <tr>
<%
       for(int i=0;i<=init_dept.length;i++){
%>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
<%}%>
      </tr>
</thead>
<tbody id="result_data">
    <tr CLASS="rowWhite">
          <td nowrap class="normalText">总计</td>

        <!-- 总计 -->
<%
        /** 总计开始 */
          double[] curr = new double[init_dept.length];
          double[] total = new double[init_dept.length];
          double cur=0;
          double tot=0;

          /** 门诊 */
          for(int i = 0; i < result.length; i++)
          {
            if(result[i][4]!=null && result[i][3].equals("Y")){
              for(int j=0;j<init_dept.length;j++){
                curr[j] = curr[j] + Double.parseDouble(result[i][j*2+4]);
                total[j] = total[j] + Double.parseDouble(result[i][j*2+5]);
              }
            }
          }
          for(int j=0;j<init_dept.length;j++){
            cur = cur + curr[j];
            tot = tot + total[j];
%>
            <!-- 合计 -->
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(curr[j])%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(total[j])%></td>
<%}%>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(cur)%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(tot)%></td>
    </tr>
<%
  for(int i=0;i<result.length;i++){
    if(result[i][4]!=null){
      cur=0;
      tot=0;
%>
     <tr>
        <td nowrap class="normalText"><%=result[i][1]%></td>
<%
     for(int j=4;j<result[i].length;j+=2){
       cur = cur + Double.parseDouble(result[i][j]);
       tot = tot + Double.parseDouble(result[i][j+1]);
%>
        <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(result[i][j]))%></td>
        <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(result[i][j+1]))%></td>
<%}%>
        <td nowrap class="numberText" style='text-align:right'><%=nf.format(cur)%></td>
        <td nowrap class="numberText" style='text-align:right'><%=nf.format(tot)%></td>
     </tr>
<%
    }
  }
%>
</tbody>
</table>
		</vh:vhFixTable>
<%}%>
  	<input type="hidden" name="subFunction" value="detail">
    <input type="hidden" name="year_month" value="<%=request.getParameter("year_month")%>">
    <input type="hidden" name="yearMonth2" value="<%=request.getParameter("yearMonth2")%>">
    <input type="hidden" name="flag" value="<%=request.getParameter("flag")%>">
	</form>
</html:html>
