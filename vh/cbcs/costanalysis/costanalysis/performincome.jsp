<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costanalysis/performincome.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DisplayWidth,
   com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<script language="javascript">

  function analyze() {
    if(template.syear.value == "")
    {
      alert('请选择核算年份!');
      return false;
    }
    if(template.fmonth.value == "")
    {
      alert('请选择核算开始月份!');
      return false;
    }
    if(template.tmonth.value == "")
    {
      alert('请选择核算结束月份!');
      return false;
    }
    if(template.fmonth.value > template.tmonth.value ) {
      alert('开始月份不能晚于结束月份!');
      return false;
    }

    setCBCS_Year1(template.syear.value);
    setCBCS_Month1(template.fmonth.value);
    setCBCS_Month2(template.tmonth.value);

    template.submit()
    show_wait();
    return ;
  }
function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }
function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();

  	year_1  ? template.syear.value=year_1 : "";
    month_1 ? template.fmonth.value=month_1 : "";
    month_2 ? template.tmonth.value=month_2: "";
  }

</script>


<html:html clazz="main" isPrint="true" fixRows="2">
  <form name="template" method="post" action="performIncome.jspviewhigh" />
   	<html:message/>
   	<html:title clazz='module'>医技科室收入成本收益明细表(医成本C1-05表)</html:title>

     <table  width="100%" cellspacing="2" border="0" >
<%
	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
	String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
	Calendar thisMonth=Calendar.getInstance();
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;
%>
   <tr>
	 <td class="signText">核算月:
	 <%=new Select(year,"syear",request.getParameter("syear")==null? String.valueOf(thisMonth.get(Calendar.YEAR)):request.getParameter("syear"),false,true)%>年
	 <%=new Select(month,"fmonth",request.getParameter("fmonth")==null? curMonth:request.getParameter("fmonth"),false,true)%>月 到
	 <%=new Select(month,"tmonth",request.getParameter("tmonth")==null? curMonth:request.getParameter("tmonth"),false,true)%>月</td>

     <td><button class="pageBtn" onclick="return analyze();">计算</button></td>
        <%
        String[][] result=(String[][])request.getAttribute("result");
        if (result != null && result.length > 1) {%>
        <td><button class="pageBtn" onclick="return preparedPrintWithIndent();">打印</button></td>
        <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
        <%}%>
      </tr>
      <tr>
       <td colspan="4"> <br><input type="text" class="findVhTabCtn"/></td>
       <tr>
    </table>
    <br>
    <html:title clazz='table'>医技科室收入成本收益明细表</html:title>

    <%
    DecimalFormat nf = new DecimalFormat("#,##0.00");

    %>
    <vh:vhFixTable fixRow=2 fixCol=1>
		<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
      <colgroup>
        <col style = <%=DisplayWidth.NAME_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
      </colgroup>
      <tr align="center">
        <td rowspan="2" nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>科室名称</td>
        <td colspan="2" nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>收入</td>
        <td colspan="2" nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>成本</td>
        <td colspan="2" nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>收益</td>
      </tr>
      <tr align="center">
        <td  nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>本期</td>
        <td  nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>累计</td>
        <td  nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>本期</td>
        <td  nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>累计</td>
        <td  nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>本期</td>
        <td  nowrap class="resultLabel"  style='text-align:center;font-weight:bold'>累计</td>
      </tr>
    	<%
    	    if (result!=null) {
			for (int i = 0; i < result.length; i++) {
				for (int j=0; j<result[i].length; j++) {
          if (result[i][j]!=null&&result[i][j].trim().equals("")) {
           	result[i][j]="&nbsp;";
         	}
      	}
      %>
       <tr height="22" >
        <td class="normalText" nowrap><%=result[i][0]%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=nf.format(Double.parseDouble(result[i][1]))%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=nf.format(Double.parseDouble(result[i][2]))%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=nf.format(Double.parseDouble(result[i][3]))%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=nf.format(Double.parseDouble(result[i][4]))%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=nf.format(Double.parseDouble(result[i][5]))%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=nf.format(Double.parseDouble(result[i][6]))%></td>
      </tr>
      <%}
      }%>
	  </table>
	</vh:vhFixTable>
		<input type=hidden name="subFunction" value="count"/>
	</form>
</html:html>
