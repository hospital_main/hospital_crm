<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costanalysis/assMedCost.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:57:34 $
     $Modtime: 03-09-22 11:07 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DisplayWidth,com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language='javascript' src='javascript/tag.js'></script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<%
  DecimalFormat nf = new DecimalFormat("#,##0.00");
  DecimalFormat df = new DecimalFormat("#,##0.00%");
  String[][] result = (String[][])request.getAttribute("table_result");
  String[][] init_dept = (String[][])request.getAttribute("init_dept");

	Calendar thisMonth=Calendar.getInstance();
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;

  String isPrint="false";
  if(result!=null)
    isPrint="true";
  String[] select_dept = request.getParameterValues("dept");
  String array = "(";
  if(select_dept!=null){
    for (int i = 0; i < select_dept.length - 1; i++) {
      array = array + ("'" + select_dept[i] + "',");
    }
    array = array + "'" + select_dept[select_dept.length - 1] + "'";
  }
  array = array + ")";
%>

<Script language="javascript">
  function find(){
    if (template.syear.value == '' || template.fmonth.value == '' || template.tmonth.value == '') {
      alert('请选择查询月份!');
      return false;
    }
    if(template.syear.value == "")
    {
      alert('请选择核算年份!');
      return false;
    }
    if(template.fmonth.value == "")
    {
      alert('请选择核算开始月份!');
      return false;
    }
    if(template.tmonth.value == "")
    {
      alert('请选择核算结束月份!');
      return false;
    }
    if(template.fmonth.value > template.tmonth.value ) {
      alert('开始月份不能晚于结束月份!');
      return false;
    }
    if(template.dept.length==0){
      alert('请选择科室')
      return;
    }
    if(template.dept.length>9){
      alert('最多只能选择九个科室')
      return;
    }
    if(template.dept.length==0){
      alert('请选择科室')
      return;
    }
    if(template.dept.length>9){
      alert('最多只能选择九个科室')
      return;
    }
    show_wait();

    setCBCS_Year1(template.syear.value);
    setCBCS_Month1(template.fmonth.value);
    setCBCS_Month2(template.tmonth.value);

    template.submit();
    return;
  }
  function preparedPrin(){
    var win = window.open("assMedCost.jspviewhigh?subFunction=preparedPrint&year_month=<%=request.getParameter("syear")+request.getParameter("fmonth")%>&yearMonth2=<%=request.getParameter("syear")+request.getParameter("tmonth")%>&dept=<%=array%>","","width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1")
  }
  function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }
  function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();

  	year_1  ? template.syear.value=year_1 : "";
    month_1 ? template.fmonth.value=month_1 : "";
    month_2 ? template.tmonth.value=month_2: "";
  }

	function getRsultXml(){//将显示结果拼成XML格式的字串
	  var xmlStr = "<?xml version='1.0' encoding='GBK'?><root>";
	  var tbody = document.getElementById("result_data");//表体
	  var thead = document.getElementById("result_head");//表头
	  var tbodyStr = "<tbody>"; //表体字串
	  var theadStr = "<thead>"; //表头字串
	  var trs = tbody.getElementsByTagName("tr");
	  var table_width = trs[0].childNodes.length;
	  for(var i = 0; i < trs.length; i++){
		tbodyStr += "<tr>";
		var tds = trs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
			tbodyStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		tbodyStr += "</tr>";
	  }
	  tbodyStr += "</tbody>";

	  var htrs = thead.getElementsByTagName("tr");
	  for(var i = 0; i < htrs.length; i++){
		theadStr += "<tr>";
		var tds = htrs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
			theadStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		theadStr += "</tr>";
	  }
	  theadStr += "</thead>";
	  xmlStr += theadStr + tbodyStr + "</root>";
	  return xmlStr.replace(/\&nbsp;&nbsp;/g, "　").replace(/\&nbsp;/g, "");
	}

	function datefromat (){
        var myDate=new Date();
    	var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
    	var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
    	var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
    	year='0000'+year;year=year.substring(year.length-4);
    	month='00'+month;month=month.substring(month.length-2);
    	day='00'+day; day=day.substring(day.length-2);

		return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString();
    }

  	function print(){

      	var printTitle="医技科室成本构成表(医成本C2-03表)";
		var info = "制作日期: "+datefromat();
      	var data=new Object();
      	data["thead_1_1"]= printTitle;
      	data["thead_2_1"]= document.all['syear'].value+'年'+document.all['fmonth'].value+'月到'+document.all['tmonth'].value+'月';
      	data["thead_3_1"]= "金额单位：元";

      	data["tfoot_1_1"]= info;
      	data["tfoot_2_1"]="北京望海康信科技有限公司  制作";
      	printXmlToCellByXsltFile(getRsultXml(), 'cbcs/costanalysis/costanalysis/directHosCostSumPrintView.xsl', data, false, false);
	}
</Script>

<html:html clazz="main" isPrint="<%=isPrint%>">
	<form name="template" method="post" action="assMedCost.jspviewhigh">

  	<html:message/>
  	<%
  	 String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
     String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
    %>


  	<html:title clazz='module'>医技科室成本构成表(医成本C2-03表)</html:title>

	  <table  width="100%" cellspacing="2" border="0" >
	    <tr>
	     <td class="signText">核算月：</td>
	    <td class="signText"> <%=new Select(year,"syear",request.getParameter("syear")==null? String.valueOf(thisMonth.get(Calendar.YEAR)):request.getParameter("syear"),false,true)%>年
	    <%=new Select(month,"fmonth",request.getParameter("fmonth")==null? curMonth:request.getParameter("fmonth"),false,true)%>月 到
	    <%=new Select(month,"tmonth",request.getParameter("tmonth")==null? curMonth:request.getParameter("tmonth"),false,true)%>月</td>
      <td class="signText"> 医技科室：</td>
        <td class="normalText"><html:select property="dept_yj" name='dept' size='4' biSelect='true' showTip='false'/></td>
	    </tr>
	    <tr>
	  <td colspan="2" > <input type="text" class="findVhTabCtn"/></td>
	   <td></td>
	  <td><button class="pageBtn" onclick="return find();">计算</button>
	      <%if(result!=null){%>
	      <button class="pageBtn" onclick="return print();//preparedPrin();">打印</button>
	      <button class="pageBtn" onclick="return saveData();">保存历史</button>
	      <%}%></td>
	  </tr>

	  </table>

	  <br>
<%
   if(result!=null){
%>
	  <html:title clazz='table'>医技科室成本构成表</html:title>
<vh:vhFixTable fixRow=2 fixCol=1>
	<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
       <colgroup id=tg>
            <col style = <%=DisplayWidth.NAME_WIDTH%> >
<%
       for(int i=0;i<=init_dept.length;i++){
%>
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
            <col style = <%=DisplayWidth.MONEY_WIDTH%> >
            <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
<%
       }
%>
          </colgroup>
<thead id="result_head">
          <!-- first row -->
          <html:tr clazz='white'>
            <td nowrap class="resultLabel" rowspan="2" style='text-align:center;font-weight:bold'>项目</td>
<%
        for(int i=0;i<init_dept.length;i++){
%>
            <td nowrap class="resultLabel" colspan="4" style='text-align:center;font-weight:bold' ><%=init_dept[i][1]%></td>
<%}%>
            <td nowrap class="resultLabel" colspan="4" style='text-align:center;font-weight:bold' >合计</td>
          </html:tr>

	<!-- second row -->
	      <html:tr clazz='white'>
<%
       for(int i=0;i<=init_dept.length;i++){
%>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>比例</td>
<%}%>
      </html:tr>
</thead>
<tbody id="result_data">
    <tr CLASS="rowWhite">
          <td nowrap class="normalText">总计</td>

        <!-- 总计 -->
<%
        /** 总计开始 */
          double[] curr = new double[init_dept.length];
          double[] total = new double[init_dept.length];
          double cur=0;
          double tot=0;
          double curSubj=0;
          double totSubj=0;

          /** 门诊 */
          for(int i = 0; i < result.length; i++)
          {
            if(result[i][4]!=null && result[i][3].equals("Y")){
              for(int j=0;j<init_dept.length;j++){
                curr[j] = curr[j] + Double.parseDouble(result[i][j*2+4]);
                total[j] = total[j] + Double.parseDouble(result[i][j*2+5]);
              }
            }
          }
          for(int j=0;j<init_dept.length;j++){
            cur = cur + curr[j];
            tot = tot + total[j];
%>
            <!-- 合计 -->
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(curr[j])%></td>
          <td nowrap class="numberText" style='text-align:right'><%=df.format(1)%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(total[j])%></td>
          <td nowrap class="numberText" style='text-align:right'><%=df.format(1)%></td>
<%}%>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(cur)%></td>
          <td nowrap class="numberText" style='text-align:right'><%=df.format(1)%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(tot)%></td>
          <td nowrap class="numberText" style='text-align:right'><%=df.format(1)%></td>
    </tr>
<%
  for(int i=0;i<result.length;i++){
    if(result[i][4]!=null){
      curSubj=0;
      totSubj=0;
%>
     <tr>
        <td nowrap class="normalText"><%=result[i][1]%></td>
<%
     for(int j=4;j<result[i].length;j+=2){
       curSubj = curSubj + Double.parseDouble(result[i][j]);
       totSubj = totSubj + Double.parseDouble(result[i][j+1]);
%>
        <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(result[i][j]))%></td>
        <td nowrap class="numberText" style='text-align:right'><%=df.format(curr[(j-4)/2]==0?0:Double.parseDouble(result[i][j])/curr[(j-4)/2])%></td>
        <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(result[i][j+1]))%></td>
        <td nowrap class="numberText" style='text-align:right'><%=df.format(total[(j-4)/2]==0?0:Double.parseDouble(result[i][j+1])/total[(j-4)/2])%></td>
<%}%>
        <td nowrap class="numberText" style='text-align:right'><%=nf.format(curSubj)%></td>
        <td nowrap class="numberText" style='text-align:right'><%=df.format(cur==0?0:curSubj/cur)%></td>
        <td nowrap class="numberText" style='text-align:right'><%=nf.format(totSubj)%></td>
        <td nowrap class="numberText" style='text-align:right'><%=df.format(tot==0?0:totSubj/tot)%></td>
     </tr>
<%
    }
  }
 }
%>
</tbody>
</table>
		</vh:vhFixTable>
  	<input type="hidden" name="subFunction" value="MedCost">
	</form>
</html:html>
