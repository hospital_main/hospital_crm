<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:variable name="gy" select="/root/tbody/tr[1]/td[last()-1]"/>
	<xsl:variable name="yf" select="/root/tbody/tr[1]/td[last()]"/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="colcount"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" ></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr>
			<td noWrap="true" colspan="colcount" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
			</xsl:if>
		</tr>
		<tr noWrap="true" class="mainHead">
			<td nowrap="true"  rowspan="2">科室</td>
			<td nowrap="true"  rowspan="2">收入</td>
			
			
			<xsl:choose>
				<xsl:when test="$gy=1 and $yf=1">
					<td nowrap="true"  colspan="5">成本</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</xsl:when>
				<xsl:when test="$gy=0 and $yf=0">
					<td nowrap="true"  colspan="3">成本</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</xsl:when>
				<xsl:otherwise>
					<td nowrap="true"  colspan="4">成本</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</xsl:otherwise>
			</xsl:choose>
			
			<td nowrap="true"  rowspan="2">收益</td>
		</tr>

		<tr noWrap="true" class="mainHead">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" >直接计入</td>
			<td noWrap="true" >直接成本计算计入</td>
			<xsl:if test="$gy=1">
				<td noWrap="true" >公用成本</td>
			</xsl:if>
			<td noWrap="true" >管理成本</td>
			<xsl:if test="$yf=1">
				<td noWrap="true" >医辅成本</td>
			</xsl:if>
			<td style="display:none"></td>

		</tr>

		</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
        <tr>
		<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=last()-1">
                </xsl:when>
                
                <xsl:when test="position()=last()">
                </xsl:when>
                <xsl:when test="position()=2">
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                 <xsl:when test="position()=3">
                </xsl:when>
                <xsl:when test="position()=4">
                </xsl:when>
                <xsl:otherwise>
                   <td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
		<td noWrap="true" colspan="colcount" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
			</xsl:if>
	</tr>
	<tr>
		<td noWrap="true" colspan="colcount" align="right"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<xsl:if test="$gy=1">
				<td style="display:none"></td>
			</xsl:if>
			<xsl:if test="$yf=1">
				<td style="display:none"></td>
			</xsl:if>
	</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
