<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costanalysis/gatherAnalysisDetail.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DisplayWidth"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<script language="javascript">

  function analyze() {
/*
    if(template.year_month.value == ''){
      alert("请选择核算月")
      return false;
    }
    */
    template.submit()
    show_wait();
    return ;
  }

</script>
<%
   String[][] result=(String[][])request.getAttribute("result");
%>

<html:html clazz="main" isPrint="true" fixRows="2">
  <form name="template" method="post" action="gatherAnalysis.jspviewhigh" />
   	<html:message/>
   	<html:title clazz='module'><%= request.getAttribute("title")%></html:title>

     <table  width="100%" cellspacing="2" border="0" >
      <tr>
        <td align='center'><button class="pageBtn"  onclick="return preparedPrint(1);">打印</button></td>
      <td align='center'><button class="pageBtn" onClick="window.close()" >关闭</button></td>
      </tr>
    </table>
    <br>
    <html:title clazz='table'>科室收益表</html:title>
<vh:vhFixTable fixRow=3 fixCol=1>
		<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
      <colgroup>
        <col style = <%=DisplayWidth.NAME_WIDTH%> >

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >

        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
        <col style = <%=DisplayWidth.MONEY_WIDTH%> >
      </colgroup>
      <tr>
        <td nowrap class="resultLabel" rowspan="3" style='text-align:center;font-weight:bold'>科室名称</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>收入</td>
        <td nowrap class="resultLabel" colspan="7" style='text-align:center;font-weight:bold'>成本</td>
        <td nowrap class="resultLabel" colspan="2" style='text-align:center;font-weight:bold'>收益</td>
      </tr>
      <tr>
        <td nowrap class="resultLabel" rowspan="2" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" rowspan="2" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" colspan="6" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" rowspan="2" style='text-align:center;font-weight:bold'>累计</td>
        <td nowrap class="resultLabel" rowspan="2" style='text-align:center;font-weight:bold'>本期</td>
        <td nowrap class="resultLabel" rowspan="2" style='text-align:center;font-weight:bold'>累计</td>
      </tr>

      <tr>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>合计</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>直接计入</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>公用成本</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>管理成本</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>医疗辅助</td>
        <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>医疗技术</td>
      </tr>

      <%
       if (result!=null) {
        for (int i = 0; i < result.length; i++ )
        {
      %>
      <tr height="22" >
        <td class="numberText" style='text-align:left;' nowrap><%=result[i][0]%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=changeFormat(result[i][1])%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=changeFormat(result[i][2])%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=changeFormat(result[i][3])%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=changeFormat(result[i][4])%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=changeFormat(result[i][5])%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=changeFormat(result[i][6])%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=changeFormat(result[i][7])%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=changeFormat(result[i][8])%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=changeFormat(result[i][9])%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=changeFormat(result[i][10])%></td>
        <td class="numberText" style='text-align:right;' nowrap><%=changeFormat(result[i][11])%></td>
			</tr>
    	<%
				 } }
      %>
	  </table>
	</vh:vhFixTable>
		<input type=hidden name="subFunction" value="count"/>
	</form>
</html:html>
