<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr>
			<td noWrap="true" class="mainHead" >
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr>
			<td noWrap="true" class="mainHead" align="left">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<xsl:for-each select="/root/thead/tr[1]">
		<tr noWrap="true" class="mainHead">
			<xsl:for-each select="td">
			<xsl:choose>
				<xsl:when test="position() = 1">
                    <td noWrap="true" rowspan="2"><xsl:value-of select='.'/></td>
                </xsl:when>
                <xsl:when test="position() &gt; 1">
                    <td noWrap="true" colspan="2"><xsl:value-of select='.'/></td>
			        <td style="display:none"></td>
                </xsl:when>
            </xsl:choose>
            </xsl:for-each>
        </tr>
		</xsl:for-each>
        <xsl:for-each select="/root/thead/tr[2]">
		<tr noWrap="true" class="mainHead">
            <td style="display:none"></td>
			<xsl:for-each select="td">
                    <td noWrap="true"><xsl:value-of select='.'/></td>
            </xsl:for-each>
        </tr>
		</xsl:for-each>
		</thead>
	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
	        <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                   <td class="numberText" align="right"><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
	</tr>
	</xsl:for-each>
	</tbody>
	<tfoot>
	<tr>
        <td noWrap="true" class="mainHead" align="right">
            <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
        </td>
        <xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
        <td style="display:none"></td>
        </xsl:for-each>
	</tr>
	<tr>
        <td noWrap="true" class="mainHead" align="right">
            <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
        </td>
        <xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
        <td style="display:none"></td>
        </xsl:for-each>
	</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
