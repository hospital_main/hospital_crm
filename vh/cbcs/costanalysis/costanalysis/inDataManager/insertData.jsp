<%@ page language="java" contentType="text/html;charset=GBK"%>
<%@ page import = "com.viewhigh.cbcs.cbcs.historydata.model.InsertHistorydataManager" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script type="text/javascript">

function save(){
	if(template.dataname.value=="" || template.dataname.value.length==0){
		alert("记录表名不可为空！");
		template.dataname.focus();
		return false;
	}
	template.subFunction.value="save";
	template.submit()
}

</Script>
<html:html clazz="main">
<%InsertHistorydataManager ihdm = (InsertHistorydataManager)request.getSession().getAttribute("Historydata");%>
<form name="template" method="post" action="insertHistoryData.jspviewhigh">
<html:message/>
<html:title clazz='module'>入库界面</html:title>
	<table nowrap='nowrap'>
		<tr>
			<td class="signText" colspan="2" align="center"><%=request.getAttribute("stu")==null?"":request.getAttribute("stu") %></td>
		</tr>
		<tr>
			<td class="signText">入库记录表名：</td>
			<td><input type="text" name="dataname" value="<%=request.getParameter("alertwindow")==null?ihdm.getReportClassName()+"绩效":ihdm.getReportClassName()+"成本"%>" size="50"></td>
		</tr>
		<tr>
			<td><button class="commonButton" accessKey="S" onclick="save()">保存</button></td>
			<td><button class="commonButton" onclick="window.close()">关闭</button></td>
		</tr>
	</table>
	<input type="hidden" name="subFunction" value=""/>
	<input type="hidden" name="aw" value="<%=request.getAttribute("alertwindow") %>"/>
</form>
</html:html>