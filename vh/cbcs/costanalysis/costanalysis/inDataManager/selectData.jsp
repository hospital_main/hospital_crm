<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.Select"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
	String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
	Calendar thisMonth=Calendar.getInstance(); 
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;
%>
<script language="javascript">

  function analyze() {

    if(template.syear.value == '' || template.fmonth.value == '' || template.tmonth.value == ''){
    	alert('请选择月份!');
      return false;
    }
    if(template.fmonth.value > template.tmonth.value ) {
      alert('开始月份不能晚于结束月份!');
      return false;
    }
    
    template.submit()
    show_wait();
    
    setCBCS_Year1(template.syear.value);
    setCBCS_Month1(template.fmonth.value);
    setCBCS_Month2(template.tmonth.value);
    
    return ;
  }
  
  function checkboxCount(obj_box) 
	{
		check = 0;
	
		if (obj_box + "." != "undefined.") 
		{
			if (obj_box.length + "." != "undefined.") 
			{
				for (i = 0; i < obj_box.length; i++) 
				{
					if (obj_box[i].checked) 
					{
						check = check + 1;
					}
				}
			} 
			else 
			{
				if (obj_box.checked) 
				{
					check = 1;
				}
			}
		}
	
		return check;
	}
function deleteData()
{
	if(checkboxCount(template.check)==0)
	{
		alert("请选择一个删除对象");
		return false;
	}
	if(confirm("你确实要删除吗?")){
		template.action = "insertHistoryData.jspviewhigh?subFunction=delete"; 	
		template.submit()
    	show_wait();
    	return ;
	}
}
function showData(id){
	window.open("insertHistoryData.jspviewhigh?subFunction=show&id="+id,"","width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1");
}
function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();
  	
  	year_1  ? template.syear.value=year_1 : "";
    month_1 ? template.fmonth.value=month_1 : "";
    month_2 ? template.tmonth.value=month_2: "";
  }
</script>

<html:html scrollCtl="yes" clazz="main" isPrint="true" fixRows="3">
<form name="template" method="post" action="insertHistoryData.jspviewhigh" />
   	<html:message/>
   	<html:title clazz='module'>成本分析历史记录</html:title>
   	<table  width="100%" cellspacing="2" border="0" class="simpleQuery">
   		<tr>
			<td colspan="4" class="successText" align="center"><%=request.getAttribute("stu")==null?"":request.getAttribute("stu") %></td>
		</tr>
	  <tr>
	  	<td class="signText" nowrap width="10%">报表大类：</td>
	  	<td nowrap><input type="text" name="reportType" value="<%=request.getAttribute("reportType")==null?"":request.getAttribute("reportType") %>"  style="width:140px;"/></td>
	  	<td class="signText" nowrap>报表类型代码：</td>
	  	<td nowrap><input type="text" name="reportClass" value="<%=request.getAttribute("reportClass")==null?"":request.getAttribute("reportClass") %>"  style="width:140px; margin-left:3px;"/></td>
	  	<td class="signText" nowrap>报表类型名称：</td>
      		<td nowrap><input type="text" name="reportClassName" value="<%=request.getAttribute("reportClassName")==null?"":request.getAttribute("reportClassName") %>" style="width:140px;"/></td>
	  </tr>
      <tr>
      	<td class="signText" nowrap>报表名称：</td>
      	<td nowrap><input type="text" name="reportName" value="<%=request.getAttribute("reportName")==null?"":request.getAttribute("reportName") %>"  style="width:140px;"/></td>
        
     
		<td nowrap colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保存时间：<%=new Select(year,"syear",request.getParameter("syear")==null? String.valueOf(thisMonth.get(Calendar.YEAR)):request.getParameter("syear"),false,true)%> 年
	 	<%=new Select(month,"fmonth",request.getParameter("fmonth")==null? curMonth:request.getParameter("fmonth"),false,true)%> 月 到 
	 	<%=new Select(month,"tmonth",request.getParameter("tmonth")==null? curMonth:request.getParameter("tmonth"),false,true)%> 月</td>
	 </tr>
      <tr>	
        <td align="right" colspan="6"><button class="pageBtn" onclick="return analyze();">查询</button></td>
      </tr>
    </table>
    
    <br>
    <html:title clazz='table'>成本分析记录查询表</html:title>
    <%if(request.getAttribute("result")!=null){ %>
    <table>
    	<tr>
    		<td><button class="pageBtn" onclick="return deleteData();">删除</button></td>
    	</tr>
    </table>
    <%} %>
     <table width="100%" class="resultSetTable">
      <html:tr clazz='white'>
        <th nowrap class="resultLabel" style='width:5%'>选择</th>
        <th nowrap class="resultLabel" style='width:10%'>报表大类</th>
        <th nowrap class="resultLabel" style='width:10%'>报表类型代码</th>
        <th nowrap class="resultLabel" style='width:25%'>报表类型名称</th>
        <th nowrap class="resultLabel" style='width:25%'>报表名称</th>
        <th nowrap class="resultLabel" style='width:10%'>保存人</th>
        <th nowrap class="resultLabel" >保存时间</th>
      </html:tr>
   	
   	<%
   		if(request.getAttribute("result")!=null){
	      	String[][] result=(String[][])request.getAttribute("result");
		      	if(result!=null && result.length > 0){     	
		        	for(int i=0;i<result.length;i++) {
   	%>
   	<tr>
   	<%
   		 for(int j = 0;j<result[i].length;j++){
   		 	if(j==0){
   		 		%>
   		 			<td nowrap class="numberText" style='text-align:center'><input type="checkbox" name="check" value="<%=result[i][j] %>"></td>	
   		 		<%
   		 	}else if(j==6){
   		 		%>
   		 			<td nowrap class="numberText" style='text-align:center'><%=result[i][j].substring(0,19) %></td>
   		 		<%
   		 	}else if(j==4){
	   		 	%>
	   		 		<td nowrap class="numberText" style='text-align:left'><a href="#" onclick="showData('<%=result[i][0] %>')"><%=result[i][j]%></a></td>
	   		 	<% 
   		 	}else{
   		 		%>
   		 			<td nowrap class="numberText" style='text-align:left'><%=result[i][j]%></td>
   		 		<%
   		 	}
   		 }
   	%>
   	<tr>
   	<%
   				}
   			}
   		}
   	%>
   	</table>
    	<input type="hidden" name="roo" value="findsub"/>
   		<input type=hidden name="subFunction" value="find"/>
	</form>
</html:html>