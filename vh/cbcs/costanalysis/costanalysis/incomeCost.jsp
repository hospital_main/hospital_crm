<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costanalysis/incomeCost.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth,
  com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<%
   DecimalFormat nf = new DecimalFormat("#,##0.00");
   String[] outpatientsum=(String[])request.getAttribute("outpatientsum");
   String[][] outpatient=(String[][])request.getAttribute("outpatient");
   String[] beinhospitalsum=(String[])request.getAttribute("beinhospitalsum");
   String[][] beinhospital=(String[][])request.getAttribute("beinhospital");
   String[] scientificsum=(String[])request.getAttribute("scientificsum");
   String[][] scientific=(String[][])request.getAttribute("scientific");
   String[][] outercost=(String[][])request.getAttribute("outercost");
%>


<Script language="javascript">
  function find(){
    if(template.syear.value == "")
    {
      alert('请选择核算年份!');
      return false;
    }
    if(template.fmonth.value == "")
    {
      alert('请选择核算开始月份!');
      return false;
    }
    if(template.tmonth.value == "")
    {
      alert('请选择核算结束月份!');
      return false;
    }
    if(template.fmonth.value > template.tmonth.value ) {
      alert('开始月份不能晚于结束月份!');
      return false;
    }
    show_wait();

    setCBCS_Year1(template.syear.value);
    setCBCS_Month1(template.fmonth.value);
    setCBCS_Month2(template.tmonth.value);

    template.submit();
    return;
  }

  function preparedPrin(){
    var win = window.open("costAnalysisIncomeCost.jspviewhigh?subFunction=preparedPrint&syear=<%=request.getParameter("syear")%>&fmonth=<%=request.getParameter("fmonth")%>&tmonth=<%=request.getParameter("tmonth")%>","","width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1")
  }

  function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }
  function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();

  	year_1  ? template.syear.value=year_1 : "";
    month_1 ? template.fmonth.value=month_1 : "";
    month_2 ? template.tmonth.value=month_2: "";
  }
</Script>

<html:html clazz="main" isPrint="true" fixRows="3">
	<form name="template" method="post" action="costAnalysisIncomeCost.jspviewhigh">
		<html:message/>
		<html:title clazz='module'>收入成本收益总表附表[不含财政收入](医成本C1—06表)</html:title>

	  <table  width="100%" cellspacing="2" border="0" >
<%
	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
	String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
	Calendar thisMonth=Calendar.getInstance();
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;
%>
   <tr>

	 <td class="signText">核算月:
   <%=new Select(year,"syear",request.getParameter("syear")==null? String.valueOf(thisMonth.get(Calendar.YEAR)):request.getParameter("syear"),false,true)%>年
	 <%=new Select(month,"fmonth",request.getParameter("fmonth")==null? curMonth:request.getParameter("fmonth"),false,true)%>月 到
	 <%=new Select(month,"tmonth",request.getParameter("tmonth")==null? curMonth:request.getParameter("tmonth"),false,true)%>月</td>



	      <td><button class="pageBtn" onclick="return find();">计算</button></td>
	      <%
        if (outpatient!=null ||  beinhospital!=null || scientific!=null || outercost!=null) {%>
        <td><button class="pageBtn" onclick="return preparedPrintWithIndent()">打印</button></td>
        <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
        <%}%>
	   </tr>
	    <tr>
       <td colspan="4"> <br><input type="text" class="findVhTabCtn"/></td>
       <tr>
	  </table>

	  <br>
	  <html:title clazz='table'>收入成本收益总表附表[不含财政收入]</html:title>


    <vh:vhFixTable fixRow=3 fixCol=1>
			<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
		    <colgroup id=tg>
          <col style = <%=DisplayWidth.NAME_WIDTH%> >
          <%for (int i = 0; i < 22; i++) {%>
          <col style = <%=DisplayWidth.MONEY_WIDTH%> >
          <%}%>
        </colgroup>

        <tr>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="3">科室</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="6">医疗</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="6">药品</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="6">合计</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2" colspan="2">未纳入成本项</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2" colspan="2">收支结余</td>
        </tr>

        <tr>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">收入</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">成本</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold'  colspan="2">收益</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">收入</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">成本</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">收益</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">收入</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">成本</td>
          <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">收益</td>
        </tr>

        <tr>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >累计</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold'>本期</td>
          <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >累计</td>
        </tr>
            <%
      if(outpatient!=null ||  beinhospital!=null || scientific!=null || outercost!=null){
    %>
        <tr bgcolor="#E1E1FB" height="22" >
          <td nowrap class="normalText" style='text-align:left'>总计</td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[1])+Double.parseDouble(beinhospitalsum[1])+Double.parseDouble(scientificsum[1]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[2])+Double.parseDouble(beinhospitalsum[2])+Double.parseDouble(scientificsum[2]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[3])+Double.parseDouble(beinhospitalsum[3])+Double.parseDouble(scientificsum[3]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[4])+Double.parseDouble(beinhospitalsum[4])+Double.parseDouble(scientificsum[4]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[1])-Double.parseDouble(outpatientsum[3])+Double.parseDouble(beinhospitalsum[1])-Double.parseDouble(beinhospitalsum[3])+Double.parseDouble(scientificsum[1])-Double.parseDouble(scientificsum[3]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[2])-Double.parseDouble(outpatientsum[4])+Double.parseDouble(beinhospitalsum[2])-Double.parseDouble(beinhospitalsum[4])+Double.parseDouble(scientificsum[2])-Double.parseDouble(scientificsum[4]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[5])+Double.parseDouble(beinhospitalsum[5])+Double.parseDouble(scientificsum[5]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[6])+Double.parseDouble(beinhospitalsum[6])+Double.parseDouble(scientificsum[6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[7])+Double.parseDouble(beinhospitalsum[7])+Double.parseDouble(scientificsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[8])+Double.parseDouble(beinhospitalsum[8])+Double.parseDouble(scientificsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[5])-Double.parseDouble(outpatientsum[7])+Double.parseDouble(beinhospitalsum[5])-Double.parseDouble(beinhospitalsum[7])+Double.parseDouble(scientificsum[5])-Double.parseDouble(scientificsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[6])-Double.parseDouble(outpatientsum[8])+Double.parseDouble(beinhospitalsum[6])-Double.parseDouble(beinhospitalsum[8])+Double.parseDouble(scientificsum[6])-Double.parseDouble(scientificsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[1])+Double.parseDouble(outpatientsum[5])+Double.parseDouble(beinhospitalsum[1])+Double.parseDouble(beinhospitalsum[5])+Double.parseDouble(scientificsum[1])+Double.parseDouble(scientificsum[5]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[2])+Double.parseDouble(outpatientsum[6])+Double.parseDouble(beinhospitalsum[2])+Double.parseDouble(beinhospitalsum[6])+Double.parseDouble(scientificsum[2])+Double.parseDouble(scientificsum[6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[3])+Double.parseDouble(outpatientsum[7])+Double.parseDouble(beinhospitalsum[3])+Double.parseDouble(beinhospitalsum[7])+Double.parseDouble(scientificsum[3])+Double.parseDouble(scientificsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[4])+Double.parseDouble(outpatientsum[8])+Double.parseDouble(beinhospitalsum[4])+Double.parseDouble(beinhospitalsum[8])+Double.parseDouble(scientificsum[4])+Double.parseDouble(scientificsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[1])+Double.parseDouble(outpatientsum[5])-Double.parseDouble(outpatientsum[3])-Double.parseDouble(outpatientsum[7])+Double.parseDouble(beinhospitalsum[1])+Double.parseDouble(beinhospitalsum[5])-Double.parseDouble(beinhospitalsum[3])-Double.parseDouble(beinhospitalsum[7])+Double.parseDouble(scientificsum[1])+Double.parseDouble(scientificsum[5])-Double.parseDouble(scientificsum[3])-Double.parseDouble(scientificsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[2])+Double.parseDouble(outpatientsum[6])-Double.parseDouble(outpatientsum[4])-Double.parseDouble(outpatientsum[8])+Double.parseDouble(beinhospitalsum[2])+Double.parseDouble(beinhospitalsum[6])-Double.parseDouble(beinhospitalsum[4])-Double.parseDouble(beinhospitalsum[8])+Double.parseDouble(scientificsum[2])+Double.parseDouble(scientificsum[6])-Double.parseDouble(scientificsum[4])-Double.parseDouble(scientificsum[8]))%></td>
<%if(outercost==null){%>
          <td nowrap class="numberText" style='text-align:right'><%=0.00%></td>
          <td nowrap class="numberText" style='text-align:right'><%=0.00%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[1])+Double.parseDouble(outpatientsum[5])-Double.parseDouble(outpatientsum[3])-Double.parseDouble(outpatientsum[7])+Double.parseDouble(beinhospitalsum[1])+Double.parseDouble(beinhospitalsum[5])-Double.parseDouble(beinhospitalsum[3])-Double.parseDouble(beinhospitalsum[7])+Double.parseDouble(scientificsum[1])+Double.parseDouble(scientificsum[5])-Double.parseDouble(scientificsum[3])-Double.parseDouble(scientificsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[2])+Double.parseDouble(outpatientsum[6])-Double.parseDouble(outpatientsum[4])-Double.parseDouble(outpatientsum[8])+Double.parseDouble(beinhospitalsum[2])+Double.parseDouble(beinhospitalsum[6])-Double.parseDouble(beinhospitalsum[4])-Double.parseDouble(beinhospitalsum[8])+Double.parseDouble(scientificsum[2])+Double.parseDouble(scientificsum[6])-Double.parseDouble(scientificsum[4])-Double.parseDouble(scientificsum[8]))%></td>
<%}else{%>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outercost[0][2]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outercost[0][3]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[1])+Double.parseDouble(outpatientsum[5])-Double.parseDouble(outpatientsum[3])-Double.parseDouble(outpatientsum[7])+Double.parseDouble(beinhospitalsum[1])+Double.parseDouble(beinhospitalsum[5])-Double.parseDouble(beinhospitalsum[3])-Double.parseDouble(beinhospitalsum[7])+Double.parseDouble(scientificsum[1])+Double.parseDouble(scientificsum[5])-Double.parseDouble(scientificsum[3])-Double.parseDouble(scientificsum[7])-Double.parseDouble(outercost[0][2]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[2])+Double.parseDouble(outpatientsum[6])-Double.parseDouble(outpatientsum[4])-Double.parseDouble(outpatientsum[8])+Double.parseDouble(beinhospitalsum[2])+Double.parseDouble(beinhospitalsum[6])-Double.parseDouble(beinhospitalsum[4])-Double.parseDouble(beinhospitalsum[8])+Double.parseDouble(scientificsum[2])+Double.parseDouble(scientificsum[6])-Double.parseDouble(scientificsum[4])-Double.parseDouble(scientificsum[8])-Double.parseDouble(outercost[0][3]))%></td>
<%}%>
        </tr>


        <%
            }
            if ( outpatient != null)
            {

        %>
        <tr bgcolor="#E1E1FB" height="22" >
          <td nowrap class="normalText" style='text-align:left'><%=outpatientsum[0]%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[1]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[2]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[3]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[4]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[1])-Double.parseDouble(outpatientsum[3]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[2])-Double.parseDouble(outpatientsum[4]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[5]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[5])-Double.parseDouble(outpatientsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[6])-Double.parseDouble(outpatientsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[1])+Double.parseDouble(outpatientsum[5]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[2])+Double.parseDouble(outpatientsum[6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[3])+Double.parseDouble(outpatientsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[4])+Double.parseDouble(outpatientsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[1])+Double.parseDouble(outpatientsum[5])-Double.parseDouble(outpatientsum[3])-Double.parseDouble(outpatientsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[2])+Double.parseDouble(outpatientsum[6])-Double.parseDouble(outpatientsum[4])-Double.parseDouble(outpatientsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'>&nbsp;</td>
          <td nowrap class="numberText" style='text-align:right'>&nbsp;</td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[1])+Double.parseDouble(outpatientsum[5])-Double.parseDouble(outpatientsum[3])-Double.parseDouble(outpatientsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatientsum[2])+Double.parseDouble(outpatientsum[6])-Double.parseDouble(outpatientsum[4])-Double.parseDouble(outpatientsum[8]))%></td>
        </tr>
        <%
              for (int i = 0; i < outpatient.length; i++ )
              {
                if(outpatient[i][4]!=null){
        %>

        <tr height="22" >
          <td nowrap class="normalText" style='text-align:left'><%=outpatient[i][1]%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][4]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][5]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][4])-Double.parseDouble(outpatient[i][6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][5])-Double.parseDouble(outpatient[i][7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][9]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][11]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][8])-Double.parseDouble(outpatient[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][9])-Double.parseDouble(outpatient[i][11]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][4])+Double.parseDouble(outpatient[i][8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][5])+Double.parseDouble(outpatient[i][9]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][6])+Double.parseDouble(outpatient[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][7])+Double.parseDouble(outpatient[i][11]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][4])+Double.parseDouble(outpatient[i][8])-Double.parseDouble(outpatient[i][6])-Double.parseDouble(outpatient[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][5])+Double.parseDouble(outpatient[i][9])-Double.parseDouble(outpatient[i][7])-Double.parseDouble(outpatient[i][11]))%></td>
          <td nowrap class="numberText" style='text-align:right'>&nbsp;</td>
          <td nowrap class="numberText" style='text-align:right'>&nbsp;</td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][4])+Double.parseDouble(outpatient[i][8])-Double.parseDouble(outpatient[i][6])-Double.parseDouble(outpatient[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(outpatient[i][5])+Double.parseDouble(outpatient[i][9])-Double.parseDouble(outpatient[i][7])-Double.parseDouble(outpatient[i][11]))%></td>
        </tr>

        <%
                }
              }
            }
            if ( beinhospital != null)
            {
        %>
        <tr bgcolor="#E1E1FB" height="22" >
          <td nowrap class="normalText" style='text-align:left'><%=beinhospitalsum[0]%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[1]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[2]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[3]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[4]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[1])-Double.parseDouble(beinhospitalsum[3]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[2])-Double.parseDouble(beinhospitalsum[4]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[5]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[5])-Double.parseDouble(beinhospitalsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[6])-Double.parseDouble(beinhospitalsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[1])+Double.parseDouble(beinhospitalsum[5]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[2])+Double.parseDouble(beinhospitalsum[6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[3])+Double.parseDouble(beinhospitalsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[4])+Double.parseDouble(beinhospitalsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[1])+Double.parseDouble(beinhospitalsum[5])-Double.parseDouble(beinhospitalsum[3])-Double.parseDouble(beinhospitalsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospitalsum[2])+Double.parseDouble(beinhospitalsum[6])-Double.parseDouble(beinhospitalsum[4])-Double.parseDouble(beinhospitalsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'>&nbsp;</td>
          <td nowrap class="numberText">&nbsp;</td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(beinhospitalsum[1])+Double.parseDouble(beinhospitalsum[5])-Double.parseDouble(beinhospitalsum[3])-Double.parseDouble(beinhospitalsum[7]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(beinhospitalsum[2])+Double.parseDouble(beinhospitalsum[6])-Double.parseDouble(beinhospitalsum[4])-Double.parseDouble(beinhospitalsum[8]))%></td>
        </tr>

        <%
              for (int i = 0; i < beinhospital.length; i++ )
              {
                if(beinhospital[i][4]!=null){
        %>
        <tr height="22" >
          <td nowrap class="normalText" style='text-align:left'><%=beinhospital[i][1]%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][4]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][5]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][4])-Double.parseDouble(beinhospital[i][6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][5])-Double.parseDouble(beinhospital[i][7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][9]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][11]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][8])-Double.parseDouble(beinhospital[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][9])-Double.parseDouble(beinhospital[i][11]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][4])+Double.parseDouble(beinhospital[i][8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][5])+Double.parseDouble(beinhospital[i][9]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][6])+Double.parseDouble(beinhospital[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][7])+Double.parseDouble(beinhospital[i][11]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][4])+Double.parseDouble(beinhospital[i][8])-Double.parseDouble(beinhospital[i][6])-Double.parseDouble(beinhospital[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][5])+Double.parseDouble(beinhospital[i][9])-Double.parseDouble(beinhospital[i][7])-Double.parseDouble(beinhospital[i][11]))%></td>
          <td nowrap class="numberText" style='text-align:right'>&nbsp;</td>
          <td nowrap class="numberText" style='text-align:right'>&nbsp;</td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][4])+Double.parseDouble(beinhospital[i][8])-Double.parseDouble(beinhospital[i][6])-Double.parseDouble(beinhospital[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(beinhospital[i][5])+Double.parseDouble(beinhospital[i][9])-Double.parseDouble(beinhospital[i][7])-Double.parseDouble(beinhospital[i][11]))%></td>
        </tr>

        <%
                }
              }
            }
            if ( scientific != null)
            {
        %>
        <tr bgcolor="#E1E1FB" height="22" >
          <td nowrap class="normalText" style='text-align:left'><%=scientificsum[0]%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[1]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[2]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[3]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[4]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[1])-Double.parseDouble(scientificsum[3]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[2])-Double.parseDouble(scientificsum[4]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[5]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[5])-Double.parseDouble(scientificsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[6])-Double.parseDouble(scientificsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[1])+Double.parseDouble(scientificsum[5]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[2])+Double.parseDouble(scientificsum[6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[3])+Double.parseDouble(scientificsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[4])+Double.parseDouble(scientificsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[1])+Double.parseDouble(scientificsum[5])-Double.parseDouble(scientificsum[3])-Double.parseDouble(scientificsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[2])+Double.parseDouble(scientificsum[6])-Double.parseDouble(scientificsum[4])-Double.parseDouble(scientificsum[8]))%></td>
          <td nowrap class="numberText" style='text-align:right'>&nbsp;</td>
          <td nowrap class="numberText" style='text-align:right'>&nbsp;</td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[1])+Double.parseDouble(scientificsum[5])-Double.parseDouble(scientificsum[3])-Double.parseDouble(scientificsum[7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientificsum[2])+Double.parseDouble(scientificsum[6])-Double.parseDouble(scientificsum[4])-Double.parseDouble(scientificsum[8]))%></td>
        </tr>

        <%
              for (int i = 0; i < scientific.length; i++ )
              {
                if(scientific[i][4]!=null){
        %>

        <tr height="22" >
          <td nowrap class="normalText" style='text-align:left'><%=scientific[i][1]%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][4]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][5]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][4])-Double.parseDouble(scientific[i][6]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][5])-Double.parseDouble(scientific[i][7]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][9]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][11]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][8])-Double.parseDouble(scientific[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][9])-Double.parseDouble(scientific[i][11]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][4])+Double.parseDouble(scientific[i][8]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][5])+Double.parseDouble(scientific[i][9]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][6])+Double.parseDouble(scientific[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][7])+Double.parseDouble(scientific[i][11]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][4])+Double.parseDouble(scientific[i][8])-Double.parseDouble(scientific[i][6])-Double.parseDouble(scientific[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][5])+Double.parseDouble(scientific[i][9])-Double.parseDouble(scientific[i][7])-Double.parseDouble(scientific[i][11]))%></td>
          <td nowrap class="numberText" style='text-align:right'>&nbsp;</td>
          <td nowrap class="numberText" style='text-align:right'>&nbsp;</td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][4])+Double.parseDouble(scientific[i][8])-Double.parseDouble(scientific[i][6])-Double.parseDouble(scientific[i][10]))%></td>
          <td nowrap class="numberText" style='text-align:right'><%=nf.format(Double.parseDouble(scientific[i][5])+Double.parseDouble(scientific[i][9])-Double.parseDouble(scientific[i][7])-Double.parseDouble(scientific[i][11]))%></td>
        </tr>
        <%
                }
              }
            }
            if(outercost!=null){
        %>

        <tr bgcolor="#E1E1FB" height="22" >
          <td nowrap class="normalText" style='text-align:left'><%=outercost[0][1]+"成本"%></td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center" >--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(outercost[0][2]))%></td>
          <td nowrap class="numberText"><%=nf.format(Double.parseDouble(outercost[0][3]))%></td>
          <td nowrap align="center">--</td>
          <td nowrap align="center">--</td>
        </tr>
        <%}%>
  	</table>
    </vh:vhFixTable>


  	<input type="hidden" name="subFunction" value="IncomeCost">
	</form>
</html:html>

