<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/costanalysis/incomeCostSubj.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page language="java" import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth,
   com.viewhigh.cbcs.base.mvc.view.component.Select"%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<%
   String[][] result=(String[][])request.getAttribute("table_result");
%>
<Script language="javascript">

  function find(){
    if(template.syear.value == "")
    {
      alert('请选择核算年份!');
      return false;
    }
    if(template.fmonth.value == "")
    {
      alert('请选择核算开始月份!');
      return false;
    }
    if(template.tmonth.value == "")
    {
      alert('请选择核算结束月份!');
      return false;
    }
    if(template.fmonth.value > template.tmonth.value ) {
      alert('开始月份不能晚于结束月份!');
      return false;
    }
    show_wait();

    setCBCS_Year1(template.syear.value);
    setCBCS_Month1(template.fmonth.value);
    setCBCS_Month2(template.tmonth.value);

    template.submit();
    return;
  }
function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  }

  function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();

  	year_1  ? template.syear.value=year_1 : "";
    month_1 ? template.fmonth.value=month_1 : "";
    month_2 ? template.tmonth.value=month_2: "";
  }
</Script>

<html:html clazz="main" isPrint="true" fixRows="2">
	<body onload="init()">
	<form name="template" method="post" action="costAnalysisIncomeCostSubj.jspviewhigh">
		<html:message/>
		<html:title clazz='module'>收入成本收益总表[不含财政收入](医成本C1-01表)</html:title>

	  <table  width="100%" cellspacing="2" border="0" >
	    <tr>

	    <%
	String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
	String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
	Calendar thisMonth=Calendar.getInstance();
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;
%>



   <tr>
	 <td class="signText">核算月:</td>
	 <td class="signText"> <%=new Select(year,"syear",request.getParameter("syear")==null? String.valueOf(thisMonth.get(Calendar.YEAR)):request.getParameter("syear"),false,true)%>年
	 <%=new Select(month,"fmonth",request.getParameter("fmonth")==null? curMonth:request.getParameter("fmonth"),false,true)%>月 到
	 <%=new Select(month,"tmonth",request.getParameter("tmonth")==null? curMonth:request.getParameter("tmonth"),false,true)%>月</td>
	 <td><button class="commonButton" accessKey="C" onclick="return find();">计算</button></td>
	      <%
        if (result!=null) {%>
        <td><button class="commonButton" accessKey="P" onclick="return preparedPrintWithIndent();">打印</button></td>
        <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
        <%}%>
	   </tr>
	  </table>

	  <br>
	  <html:title clazz='table'>收入成本收益总表[不含财政收入]</html:title>

<vh:vhFixTable fixRow=2 fixCol=1>
			<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt'>
		    <colgroup id=tg>
          <col style = <%=DisplayWidth.NAME_WIDTH%> >
          <%for (int i = 0; i < 10; i++) {%>
          <col style = <%=DisplayWidth.MONEY_WIDTH%> >
          <%}%>
        </colgroup>
         <tr>
          <td nowrap   class="resultLabel"  rowspan="2">项目</td>
          <td nowrap   class="resultLabel"  colspan="2">收入</td>
          <td nowrap   class="resultLabel"  colspan="2">成本</td>
          <td nowrap   class="resultLabel"  colspan="2">收益</td>
          <td nowrap   class="resultLabel" colspan="2">未纳入成本项</td>
          <td nowrap   class="resultLabel" colspan="2">收支结余</td>
       </tr>
        <tr>
          <td nowrap   class="resultLabel"  >本期</td>
          <td nowrap   class="resultLabel"  >累计</td>
          <td nowrap   class="resultLabel"  >本期</td>
          <td nowrap   class="resultLabel"  >累计</td>
          <td nowrap   class="resultLabel" >本期</td>
          <td nowrap   class="resultLabel"  >累计</td>
          <td nowrap   class="resultLabel"  >本期</td>
          <td nowrap   class="resultLabel"  >累计</td>
          <td nowrap   class="resultLabel"  >本期</td>
          <td nowrap   class="resultLabel" >累计</td>
        </tr>
		<%
		  if(result!=null){
		   for(int i=0;i<result.length;i++){
		%>
		        <tr height="22" >
		          <td nowrap class="resultTableTdText"><%=result[i][0]%></td>
		          <%
		          for(int j=1;j<result[i].length;j++){
		          %>
		          <td nowrap class="resultTableTdNum"><%=result[i][j]%></td>
		          <%}%>
		        </tr>
		 <%}}%>
			  	</table>
</vh:vhFixTable>

  	<input type="hidden" name="subFunction" value="IncomeCost">
	</form>
	</body>
</html:html>

