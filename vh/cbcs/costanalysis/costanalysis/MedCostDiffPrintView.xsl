<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr>
			<td noWrap="true" class="mainHead" >
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr>
			<td noWrap="true" class="mainHead" align="left">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
        <tr class="mainHead" noWrap="true">
            <td rowspan="3">科室</td>
            <td colspan="4">实际成本</td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td colspan="12">与预算成本比</td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td colspan="12">与去年同期比</td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td colspan="9">综合比较</td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
        </tr>
        <tr class="mainHead" noWrap="true">
            <td style="display:none"></td>
            <td colspan="2">总成本</td>
            <td style="display:none"></td>
            <td colspan="2">单位成本</td>
            <td style="display:none"></td>
            <td colspan="2">总成本</td>
            <td style="display:none"></td>
            <td colspan="2">差异</td>
            <td style="display:none"></td>
            <td colspan="2">差异率</td>
            <td style="display:none"></td>
            <td colspan="2">单位成本</td>
            <td style="display:none"></td>
            <td colspan="2">差异</td>
            <td style="display:none"></td>
            <td colspan="2">差异率</td>
            <td style="display:none"></td>
            <td colspan="2">总成本</td>
            <td style="display:none"></td>
            <td colspan="2">差异</td>
            <td style="display:none"></td>
            <td colspan="2">差异率</td>
            <td style="display:none"></td>
            <td colspan="2">单位成本</td>
            <td style="display:none"></td>
            <td colspan="2">差异</td>
            <td style="display:none"></td>
            <td colspan="2">差异率</td>
            <td style="display:none"></td>
            <td colspan="6">与上期比较</td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td colspan="3">与平均比较</td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
        </tr>
        <tr class="mainHead" noWrap="true">
            <td style="display:none"></td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >本期</td>
            <td >累计</td>
            <td >总成本</td>
            <td >差异</td>
            <td >差异率</td>
            <td >单位成本</td>
            <td >差异</td>
            <td >差异率</td>
            <td >总成本</td>
            <td >差异</td>
            <td >差异率</td>
        </tr>
		</thead>
	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
	        <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                   <td class="numberText" align="right"><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
	</tr>
	</xsl:for-each>
	</tbody>
	<tfoot>
	<tr>
        <td noWrap="true" class="mainHead" align="right">
            <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
        </td>
        <xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
        <td style="display:none"></td>
        </xsl:for-each>
	</tr>
	<tr>
        <td noWrap="true" class="mainHead" align="right">
            <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
        </td>
        <xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
        <td style="display:none"></td>
        </xsl:for-each>
	</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
