<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr>
			<td noWrap="true" class="mainHead" >
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr>
			<td noWrap="true" class="mainHead" align="left">
                <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
            </td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
			<td style="display:none"></td>
			</xsl:for-each>
		</tr>
        <tr>
            <td noWrap="true" class="mainHead" rowspan="3">科室</td>
            <td noWrap="true" class="mainHead" rowspan="2" colspan="2">实际成本</td>
            <td style="display:none"></td>
            <td noWrap="true" class="mainHead" colspan="4">直接成本</td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td noWrap="true" class="mainHead" colspan="4">间接成本</td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
        </tr>
        <tr>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td style="display:none"></td>
            <td noWrap="true" class="mainHead" colspan="2">成本额</td>
            <td style="display:none"></td>
            <td noWrap="true" class="mainHead" colspan="2">成本比例</td>
            <td style="display:none"></td>
            <td noWrap="true" class="mainHead" colspan="2">成本额</td>
            <td style="display:none"></td>
            <td noWrap="true" class="mainHead" colspan="2">成本比例</td>
            <td style="display:none"></td>
        </tr>
        <tr>
            <td style="display:none"></td>
            <td noWrap="true" class="mainHead">本期</td>
            <td noWrap="true" class="mainHead">累计</td>
            <td noWrap="true" class="mainHead">本期</td>
            <td noWrap="true" class="mainHead">累计</td>
            <td noWrap="true" class="mainHead">本期</td>
            <td noWrap="true" class="mainHead">累计</td>
            <td noWrap="true" class="mainHead">本期</td>
            <td noWrap="true" class="mainHead">累计</td>
            <td noWrap="true" class="mainHead">本期</td>
            <td noWrap="true" class="mainHead">累计</td>
        </tr>
		</thead>
	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
	        <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                   <td class="numberText" align="right"><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
	</tr>
	</xsl:for-each>
	</tbody>
	<tfoot>
	<tr>
        <td noWrap="true" class="mainHead" align="right">
            <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
        </td>
        <xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
        <td style="display:none"></td>
        </xsl:for-each>
	</tr>
	<tr>
        <td noWrap="true" class="mainHead" align="right">
            <xsl:attribute name="colspan"><xsl:value-of select='count(/root/tbody/tr[1]/td)'/></xsl:attribute>
        </td>
        <xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
        <td style="display:none"></td>
        </xsl:for-each>
	</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
