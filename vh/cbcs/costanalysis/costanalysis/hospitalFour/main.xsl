<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		
	<tr noWrap="true" class="mainHead">
		<td nowrap="true"  rowspan="3">科室</td>
		<td nowrap="true"  rowspan="2" colspan="2">实际成本</td>
		<td nowrap="true"  colspan="4">人力成本</td>
		<td nowrap="true"  colspan="4">离退休人员成本</td>
		<td nowrap="true"  colspan="4">材料成本</td>
		<td nowrap="true"  colspan="4">净药品成本</td>
		<td nowrap="true"  colspan="4">折旧成本</td>
		<td nowrap="true"  colspan="4">其他成本</td>
		<td nowrap="true"  colspan="4">无形资产摊销成本</td>
		<td nowrap="true"  colspan="4">提取医疗风险基金</td>
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  colspan="2">成本额</td>
		<td noWrap="true"  colspan="2">成本比例</td>
		<td noWrap="true"  colspan="2">成本额</td>
		<td noWrap="true"  colspan="2">成本比例</td>
		<td noWrap="true"  colspan="2">成本额</td>
		<td noWrap="true"  colspan="2">成本比例</td>
		<td noWrap="true"  colspan="2">成本额</td>
		<td noWrap="true"  colspan="2">成本比例</td>
		<td noWrap="true"  colspan="2">成本额</td>
		<td noWrap="true"  colspan="2">成本比例</td>
		<td noWrap="true"  colspan="2">成本额</td>
		<td noWrap="true"  colspan="2">成本比例</td>
		<td noWrap="true"  colspan="2">成本额</td>
		<td noWrap="true"  colspan="2">成本比例</td>
		<td noWrap="true"  colspan="2">成本额</td>
		<td noWrap="true"  colspan="2">成本比例</td>
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
		<td noWrap="true"  >本期</td>
		<td noWrap="true"  >累计</td>
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			 <xsl:variable name="v2" select="td[4]"/>
			 <xsl:choose>
			 	<xsl:when test="$v2='1'">
			 <tr bgcolor="#E1E1FB">
			<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                </xsl:when>
                 <xsl:when test="position()=3">
                 	<td noWrap="true">
                    <xsl:value-of select="."/>
                  	</td>
                </xsl:when>
                <xsl:when test="position()=4">
                </xsl:when>
                <xsl:when test="position()=5">
                </xsl:when>
                <xsl:when test="position()=10">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=11">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=14">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=15">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=18">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=19">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                 <xsl:when test="position()=22">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=23">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                 <xsl:when test="position()=26">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=27">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                 <xsl:when test="position()=30">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=31">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                 <xsl:when test="position()=34">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=35">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                 <xsl:when test="position()=38">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=39">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:otherwise>
                   <td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
                </xsl:when>
                <xsl:otherwise>
                <tr>
			<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                </xsl:when>
                 <xsl:when test="position()=3">
                 	<td noWrap="true">
                    <xsl:value-of select="."/>
                  	</td>
                </xsl:when>
                <xsl:when test="position()=4">
                </xsl:when>
                <xsl:when test="position()=5">
                </xsl:when>
                <xsl:when test="position()=10">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=11">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=14">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=15">        
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                 <xsl:when test="position()=18">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=19">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                 <xsl:when test="position()=22">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=23">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                 <xsl:when test="position()=26">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=27">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                 <xsl:when test="position()=30">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=31">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                 <xsl:when test="position()=34">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=35">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=38">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:when test="position()=39">
                	<td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:otherwise>
                   <td noWrap="true" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
                
              </xsl:choose>
          </xsl:for-each>
				</tr>
                </xsl:otherwise>
			 </xsl:choose>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
