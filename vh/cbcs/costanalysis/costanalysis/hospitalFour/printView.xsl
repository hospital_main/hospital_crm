<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="35"></td>
			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="34"/>    
	     </xsl:call-template>
		</tr>
		<tr>
			<td noWrap="true" colspan="35" ></td>
			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="34"/>    
	        	</xsl:call-template>
		</tr>
		<tr>
			<td noWrap="true" colspan="35" align="left"></td>
			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="34"/>    
	        	</xsl:call-template>	
		</tr>
		<tr>
			<td noWrap="true" colspan="35" align="left"></td>
			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="34"/>    
	     </xsl:call-template>		
		</tr>
		<tr noWrap="true" class="mainHead">
			<td noWrap="true" rowspan="3">科室</td>
			<td noWrap="true" rowspan="2" colspan="2">实际成本</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="4">人力成本</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="4">离退休人员成本</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="4">材料成本</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="4">净药品成本</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="4">折旧成本</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="4">其他成本</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="4">无形资产摊销成本</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="4">提取医疗风险基金</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>

		<tr noWrap="true" class="mainHead">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本额</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本比例</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本额</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本比例</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本额</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本比例</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本额</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本比例</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本额</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本比例</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本额</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本比例</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本额</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本比例</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本额</td>
			<td style="display:none"></td>
			<td noWrap="true" colspan="2">成本比例</td>
			<td style="display:none"></td>
			
		</tr>

		 <tr noWrap="true" class="mainHead">
			<td style="display:none"></td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
			<td noWrap="true" colspan="1">本期</td>
			<td noWrap="true" colspan="1">累计</td>
		  </tr>
		</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
            <tr>
			<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                </xsl:when>
                 <xsl:when test="position()=3">
                 	<td>
                    <xsl:value-of select="."/>
                  	</td>
                </xsl:when>
                <xsl:when test="position()=4">
                </xsl:when>
                <xsl:when test="position()=5">
                </xsl:when>
                
                <xsl:when test="(position() mod 4 = 2 or position() mod 4 = 3) and position() &gt; 9">
                	<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>                
                <xsl:otherwise>
                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
		</tr>       
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
		<td noWrap="true" colspan="35" align="right"></td>
		<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="34"/>    
	        	</xsl:call-template>
	</tr>
	<tr>
		<td noWrap="true" colspan="35" align="right"></td>
		<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="34"/>    
	        	</xsl:call-template>
	</tr>
	</tfoot>
		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
