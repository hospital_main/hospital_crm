<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	
	<thead>
	<xsl:variable name="ky" select="/root/tbody/tr[1]/td[last()-2]"/>
	<xsl:variable name="wnr" select="/root/tbody/tr[1]/td[last()-1]"/>
	<tr noWrap="true" class="mainHead">
		<th nowrap="true"  rowspan="2">项目</th>
		<th nowrap="true"  colspan="2">收入</th>
		<th nowrap="true"  colspan="2">成本</th>
		<th nowrap="true"  colspan="2">收益</th>
		<xsl:if test="$wnr =1">
		<th nowrap="true"  colspan="2">未纳入成本项</th>
		</xsl:if>
		<th nowrap="true"  colspan="2">收支结余</th>
	</tr>

	<tr noWrap="true" class="mainHead">
		<th noWrap="true"  >本期</th>
		<th noWrap="true"  >累计</th>
		<th noWrap="true" >本期</th>
		<th noWrap="true"  >累计</th>
		<th noWrap="true"  >本期</th>
		<th noWrap="true"  >累计</th>
		<th noWrap="true"  >本期</th>
		<th noWrap="true"  >累计</th>
		<xsl:if test="$wnr =1">
		<th noWrap="true"  >本期</th>
		<th noWrap="true" >累计</th>
		</xsl:if>
		
	</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:variable name="c" select="position()"/>
			<xsl:variable name="dd" select="last()"/>
        <tr>
		 <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="$wnr =1">
                	<xsl:choose>
                		<xsl:when test="position()=1">
                			<td noWrap="true">
                			<xsl:value-of select="."/>
                			</td>
                		</xsl:when>
                		<xsl:when test="$dd=$c and (position()=2 or position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=10 or position()=11  )" >
                			<td></td>
                		</xsl:when>
               			 <xsl:when test="(position()=last()) or (position()=last()-1) or (position()=last()-2)">
                		</xsl:when>
                		<xsl:when test="$c &lt; $dd and $c &gt;1 and  (position()=8 or position()=9)">
                			<td></td>
                		</xsl:when>
                		
                		<xsl:otherwise>
                   			<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                		</xsl:otherwise>
                	</xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                	<xsl:choose>
                		<xsl:when test="position()=1">
                			<td noWrap="true">
                			<xsl:value-of select="."/>
                			</td>
                		</xsl:when>
						<xsl:when test="(position()=last()) or (position()=last()-1) or (position()=last()-2)">
                		</xsl:when>
                		<xsl:otherwise>
                   			<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                		</xsl:otherwise>
                	</xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
