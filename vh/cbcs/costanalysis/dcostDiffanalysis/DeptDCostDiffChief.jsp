<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costanalysis/dcostDiffanalysis/DeptDCostDiffChief.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,com.viewhigh.cbcs.base.sql.BaseRO,
  com.viewhigh.cbcs.cbcs.util.DisplayWidth" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
	Calendar thisMonth=Calendar.getInstance();
	String curMonth = String.valueOf(thisMonth.get(Calendar.MONTH) +1);
	if (curMonth.length()==1) curMonth="0" + curMonth;
%>
<%!
  public String getColor(String value) {
      int index = value.indexOf("-");
      if(index == -1) {
        if(value.equals("0.00") || value.equals("&nbsp;&nbsp;&nbsp;") || value.equals("0.00%")) {
          return "";
        }
        return " style=\"color:red\" ";
      } else {
        return "";
      }
  }
%>
  <script language="javascript">
     function analyze() {
      if(template.year_month.value == '')
      {
        alert('请选择查询年月!');
        return false;
      }
      else
      {
        show_wait();
        setCBCS_Year1(template.year_month.value.substr(0,4));
		    setCBCS_Month1(template.year_month.value.substr(4,2));
		    setCBCS_Month2(template.year_month.value.substr(4,2));
        template.submit();
        return;
      }
    }

    function preparedPrin() {
    var win = window.open("deptDCostDiffChief.jspviewhigh?subFunction=chiefpreparedPrint&year_month=<%=request.getParameter("year_month")%>","","width = 800, height=580, top=100, left=100, scrollbars = 1, resizable = 1")
    }
    function saveData(){
  	window.open("insertHistoryData.jspviewhigh?subFunction=nav","","width = 500, height=500, top=100, left=100, scrollbars = 1, resizable = 1");
  	}
  	function window_onload2(){
  	var year_1 = getCBCS_Year1();
  	var month_1 = getCBCS_Month1();
  	var month_2 = getCBCS_Month2();

  	if(year_1 && month_1 && month_2){
    	template.year_month.setValue(year_1+""+month_1)
    }
  }

	function getRsultXml(){//将显示结果拼成XML格式的字串
	  var xmlStr = "<?xml version='1.0' encoding='GBK'?><root>";
	  var tbody = document.getElementById("result_data");//表体
	  var thead = document.getElementById("result_head");//表头
	  var tbodyStr = "<tbody>"; //表体字串
	  var theadStr = "<thead>"; //表头字串
	  var trs = tbody.getElementsByTagName("tr");
	  var table_width = trs[0].childNodes.length;
	  for(var i = 0; i < trs.length; i++){
		tbodyStr += "<tr>";
		var tds = trs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
            if(j == 0 && tds[j].childNodes[0].nodeType != 3) //有超链接
                tbodyStr += "<td>"+tds[j].childNodes[0].innerHTML+"</td>";
            else
			    tbodyStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		tbodyStr += "</tr>";
	  }
	  tbodyStr += "</tbody>";

	  var htrs = thead.getElementsByTagName("tr");
	  for(var i = 0; i < htrs.length; i++){
		theadStr += "<tr>";
		var tds = htrs[i].getElementsByTagName("td");
		for(var j = 0; j < tds.length; j++){
			theadStr += "<td>"+tds[j].innerHTML+"</td>";
		}
		theadStr += "</tr>";
	  }
	  theadStr += "</thead>";
	  xmlStr += theadStr + tbodyStr + "</root>";
      return xmlStr.replace(/\&nbsp;&nbsp;/g, "　").replace(/\&nbsp;/g, "");
	}

	function datefromat (){
        var myDate=new Date();
    	var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
    	var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
    	var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
    	year='0000'+year;year=year.substring(year.length-4);
    	month='00'+month;month=month.substring(month.length-2);
    	day='00'+day; day=day.substring(day.length-2);

		return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString();
    }

  	function print(){

      	var printTitle="医疗成本比较分析表(医成本C4-05表)";
		var info = "制作日期: "+datefromat();
        var ym = document.all["year_month"].value;
      	var data=new Object();
      	data["thead_1_1"]= printTitle;
      	data["thead_2_1"]= ym.substring(0,4)+"年"+ym.substring(4)+"月";
      	data["thead_3_1"]= "金额单位：元";

      	data["tfoot_1_1"]= info;
      	data["tfoot_2_1"]="北京望海康信科技有限公司  制作";

      	printXmlToCellByXsltFile(getRsultXml(), 'cbcs/costanalysis/dcostDiffanalysis/DeptDCostDiffChiefPrintView.xsl', data, false, false);
	}
</script>

<html:html clazz="main" isPrint="true" fixCols="2">
  <form name="template" method="post" action="deptDCostDiffChief.jspviewhigh" />
    <html:message/>

    <html:title clazz='module'>科室直接成本比较分析表(医成本C4-06表)</html:title>

    <table  width="100%" cellspacing="2" border="0" >
      <tr>
        <td class="signText" nowrap width="2%">核算月：</td>
        <td class="signText"><%=new MonthComponent("year_month", (request.getParameter("year_month")==null?(String.valueOf(thisMonth.get(Calendar.YEAR))+curMonth):(request.getParameter("year_month")))) %></td>
        <td><button class="pageBtn" onclick="return analyze();">计算</button></td>

        <%
         String[][] result = (String[][])request.getAttribute("table_result");
          if (result != null && result.length > 1)
        {%>
        <td><button class="pageBtn" onclick="return print();// preparedPrin();">打印</button></td>
        <td><button class="pageBtn" onclick="return saveData();">保存历史</button></td>
        <%}
        %>
        </tr>
             <tr >
	  <td colspan="2" ><br> <input type="text" class="findVhTabCtn"/></td>
	  </tr>
    </table>

    <br>

    <html:title clazz='table'>科室直接成本比较分析汇总表</html:title>
<vh:vhFixTable fixRow=2 fixCol=1>
		<table border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
	    <colgroup id=tg>
	      <col style = <%=DisplayWidth.NAME_WIDTH%> >
	      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	      <col style = <%=DisplayWidth.PERCENT_WIDTH%> >

	      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	      <col style = <%=DisplayWidth.PERCENT_WIDTH%> >

	      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	      <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
	      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	      <col style = <%=DisplayWidth.MONEY_WIDTH%> >
	      <col style = <%=DisplayWidth.PERCENT_WIDTH%> >
	    </colgroup>
        <thead id="result_head">
	    <tr>
	      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">科室</td>
	      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">本期直接成本</td>
	      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与上期比较</td>
	 	  <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与同期比较</td>
	      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与预算比较</td>
	      <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="3">与平均比较</td>
	    </tr>
	    <tr>
	      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >上期成本</td>
	      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
	      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>

	     <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >同期成本</td>
	      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
	      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>

	      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >预算成本</td>
	      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
	      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>

	      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >平均成本</td>
	      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异</td>
	      <td nowrap class="resultLabel" style='text-align:center;font-weight:bold' >差异率</td>
	    </tr>
        </thead>
        <tbody id="result_data">
    <%

    if (result != null) {
      DecimalFormat percentFormat = new DecimalFormat("#,##0.00%");
      DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
      String[] total = null;
      for (int i = 0; i < result.length; i++ )  {
        /** reformat  */

          if(Double.parseDouble(result[i][2])==0) result[i][4] = "&nbsp;&nbsp;&nbsp;";
             else  result[i][4] = percentFormat.format(Double.parseDouble(result[i][4]));
          if(Double.parseDouble(result[i][5])==0) result[i][7] = "&nbsp;&nbsp;&nbsp;";
             else  result[i][7] = percentFormat.format(Double.parseDouble(result[i][7]));
          if(Double.parseDouble(result[i][8])==0) result[i][10] = "&nbsp;&nbsp;&nbsp;";
              else  result[i][10] = percentFormat.format(Double.parseDouble(result[i][10]));
        if(Double.parseDouble(result[i][11])==0) result[i][13] = "&nbsp;&nbsp;&nbsp;";
              else  result[i][13] = percentFormat.format(Double.parseDouble(result[i][13]));

      %>
		<tr bgcolor="white" height="22" >
		       <td><a href = "javascript:var win = window.open('deptDCostDiffChief.jspviewhigh?subFunction=detaildiffAnalysis&year_month=<%=request.getParameter("year_month")%>&dept_code=<%=result[i][14]%>&dept_name=<%=result[i][0]%>', 'child', 'width=800, height=580, top=100, left=100, scrollbars = 1, resizable = 1');"><%=result[i][0]%></a></td>
				<td class="numberText" nowrap><%= moneyFormat.format(Double.parseDouble(result[i][1]))%></td>
 				<td class="numberText" nowrap><%= moneyFormat.format(Double.parseDouble(result[i][2]))%></td>
				<td <%out.print(getColor(result[i][3]));%> class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][3]))%></td>
				<td <%out.print(getColor(result[i][4]));%> class="percentText" nowrap><%=result[i][4]%></td>

 				<td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][5]))%></td>
 				<td <%out.print(getColor(result[i][6]));%> class="numberText" nowrap><%=moneyFormat.format(Double.parseDouble(result[i][6]))%></td>
 				<td <%out.print(getColor(result[i][7]));%> class="numberText" nowrap><%=result[i][7]%></td>
 				<td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][8]))%></td>
 				<td <%out.print(getColor(result[i][9]));%> class="numberText" nowrap><%=moneyFormat.format(Double.parseDouble(result[i][9]))%></td>
 				<td <%out.print(getColor(result[i][10]));%> class="numberText" nowrap><%=result[i][10]%></td>
 				<!--  --->
				<td class="numberText" style='text-align:right' nowrap><%=moneyFormat.format(Double.parseDouble(result[i][11]))%></td>
 				<td <%out.print(getColor(result[i][12]));%> class="numberText" nowrap><%=moneyFormat.format(Double.parseDouble(result[i][12]))%></td>
 				<td <%out.print(getColor(result[i][13]));%> class="numberText" nowrap><%=result[i][13]%></td>
 				<!--  -->
				</tr>
    <%
       }
      }
    %>
    </tbody>
  	</table>
</vh:vhFixTable>
  	<input type="hidden" name="subFunction" value="diffAnalysis">
	</form>
</html:html>
