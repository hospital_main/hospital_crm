<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
		<tr>
			<td noWrap="true" class="mainHead" style="fontsize:maintitle;" colspan="14"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="14" ></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td noWrap="true" colspan="14" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>			
		</tr>
		<tr>
			<td noWrap="true" colspan="14" align="left"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>			
		</tr>
		<tr noWrap="true" class="mainHead">
			<td nowrap="true"  rowspan="2">科室</td>
			<td nowrap="true"  rowspan="2">本期直接成本</td>
			<td nowrap="true"  colspan="3">与同期比较</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="3">与上期比较</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="3">与预算比较</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td nowrap="true"  colspan="3">与平均比较</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>

		 <tr noWrap="true" class="mainHead">
		 	<td style="display:none"></td>
			<td style="display:none"></td>
			<td noWrap="true" >同期数值</td>
			<td noWrap="true" >差异</td>
			<td noWrap="true" >差异率</td>
			<td noWrap="true" >上期成本</td>
			<td noWrap="true" >差异</td>
			<td noWrap="true" >差异率</td>
			<td noWrap="true" >预算成本</td>
			<td noWrap="true" >差异</td>
			<td noWrap="true" >差异率</td>
			<td noWrap="true" >平均成本</td>
			<td noWrap="true" >差异</td>
			<td noWrap="true" >差异率</td>
		  </tr>
		</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
                <tr>
			<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                </xsl:when>
                 <xsl:when test="position()=3">
                 	<td noWrap="true">
                    <xsl:value-of select="."/>
                  	</td>
                </xsl:when>
                <xsl:when test="position()=4">
                </xsl:when>
                <xsl:when test="position()=5">
                </xsl:when>
                <xsl:when test="position() &gt; 7 and (position()mod 3=0 or position()mod 3=2)">
                	<xsl:if test="position()mod 3=0">
                	<td style="color:red" noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                	</xsl:if>
                	<xsl:if test="position()mod 3=2">
                	<td style="color:red" noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                	</xsl:if>
                </xsl:when>
                <xsl:otherwise>
                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	<tfoot>
	<tr>
		<td noWrap="true" colspan="14" align="right"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
	</tr>
	<tr>
		<td noWrap="true" colspan="14" align="right"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
		<td style="display:none"></td>
	</tr>
	</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
