<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
	<tr noWrap="true" class="mainHead">
		<td nowrap="true" rowspan="2">成本项目</td>
		<td nowrap="true" rowspan="2">本期成本</td>
		<td nowrap="true" colspan="3">与同期比较</td>
		<td nowrap="true" colspan="3">与上期比较</td>
		<td nowrap="true" colspan="3">与预算比较</td>
		<td nowrap="true" colspan="3">与平均比较</td>
	</tr>

	<tr noWrap="true" class="mainHead">
		<td noWrap="true" >同期成本</td>
		<td noWrap="true" >差异</td>
		<td noWrap="true" >差异率</td>
		<td noWrap="true" >上期成本</td>
		<td noWrap="true" >差异</td>
		<td noWrap="true" >差异率</td>
		<td noWrap="true" >预算成本</td>
		<td noWrap="true" >差异</td>
		<td noWrap="true" >差异率</td>
		<td noWrap="true" >平均成本</td>
		<td noWrap="true" >差异</td>
		<td noWrap="true" >差异率</td>
		
	</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr">
			 <xsl:variable name="v2" select="td[4]"/>
			 <xsl:variable name="vv" select="td[2]"/>
			 <xsl:choose>
			 	<xsl:when test="$v2='1'">
			 <tr bgcolor="#E1E1FB">
			<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                </xsl:when>
                <xsl:when test="position()=3">
                 	<td noWrap="true">
                   		<xsl:value-of select="."/>
                  	</td>
                </xsl:when>
                <xsl:when test="position()=4">
                </xsl:when>
                <xsl:when test="position()=5">
                </xsl:when>
                <xsl:when test="position() &gt; 7 and (position()mod 3=0 or position()mod 3=2)">
                	<xsl:choose>
                		<xsl:when test=". &gt; 0">
                			<xsl:if test="position()mod 3=0">
		                		<td style="color:red" noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                	</xsl:if>
		                	<xsl:if test="position()mod 3=2">
		                		<td style="color:red" noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                	</xsl:if>
                		</xsl:when>
                		<xsl:otherwise>
                			<xsl:if test="position()mod 3=0">
		                		<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                	</xsl:if>
		                	<xsl:if test="position()mod 3=2">
		                		<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                	</xsl:if>
                		</xsl:otherwise>
                	</xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
                </xsl:when>
                <xsl:otherwise>
                <tr>
			<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
                </xsl:when>
                <xsl:when test="position()=3">
                 	<td noWrap="true">
                   		<xsl:value-of select="."/>
                  	</td>
                </xsl:when>
                <xsl:when test="position()=4">
                </xsl:when>
                <xsl:when test="position()=5">
                </xsl:when>
                <xsl:when test="position() &gt; 7 and (position()mod 3=0 or position()mod 3=2)">
                	<xsl:choose>
                		<xsl:when test=". &gt; 0">
                			<xsl:if test="position()mod 3=0">
		                		<td style="color:red" noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                	</xsl:if>
		                	<xsl:if test="position()mod 3=2">
		                		<td style="color:red" noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                	</xsl:if>
                		</xsl:when>
                		<xsl:otherwise>
                			<xsl:if test="position()mod 3=0">
		                		<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
		                	</xsl:if>
		                	<xsl:if test="position()mod 3=2">
		                		<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                	</xsl:if>
                		</xsl:otherwise>
                	</xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                   <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
                </xsl:otherwise>
			 </xsl:choose>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
