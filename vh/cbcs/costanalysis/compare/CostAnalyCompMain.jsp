<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../error.jsp" %>

<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(101, request)%>

<%@ page import="java.util.ArrayList" %>
<%!
	double[] n = {1,0,0,0,0,0,0,0,0,0};
%>
<%
    ArrayList user_author = (ArrayList)session.getAttribute("user_author");
%>
<%

%>
<Script Language="JavaScript">
function check() {
      template.subFunction.value='findall';
       template.submit();
       return true;
   }

</Script>


    <form name="template" method="post" action="costAnalyComp.jspviewhigh" onsubmit="return check();">
        <table width="97%" border="0" cellpadding="0" cellspacing="0">
            <tr bgcolor="#FFFFFF">
                <td valign="top"><img src="../../images/left.gif" width="5" height="5"></td>
                <td align="right" valign="top"><img src="../../images/right.gif" width="5" height="5"></td>
            </tr>

                <%
                    String message = ( String )request.getAttribute( "message" );
                    if ( message != null && !message.trim().equals( "" ) ) {
                %>

            <tr bgcolor="#FFFFFF" class="a12">
                <td height="26" colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;<font color="#FF0000"><%=message%></font></td>
            </tr>

                <%}%>


	   <%
//		String[][] result1 = (String[][])request.getAttribute("SERSUB");

                double[] part1 = (double[])request.getAttribute("GL");
		double[] part2 = (double[])request.getAttribute("LASTGL");
		double[] part3 = (double[])request.getAttribute("DIFFGL");
		if(part1==null)part1= n;
		if(part2==null)part2= n;
		if(part3==null)part3= n;


          %>
            <tr bgcolor="#FFFFFF">
                <td colspan="2">

<table border="0" align="center" cellspacing="1" bgcolor="#999999" width="96%">
  <tr bgcolor="#CCCCCC">
    <%  String newdate = (String)request.getAttribute("date"); if(newdate==null)newdate="";%>
    <td  colspan="1" align="center">日期</td>
    <td colspan="6" align="left"><input type="text" name="DATE" size="20" value = "<%=newdate%>"></td>


  </tr>
  <tr bgcolor="#CCCCCC">
    <td width="20%" rowspan="2" align="center">指标</td>
    <td width="12%" rowspan="2" align="center">本期值</td>
    <td width="24%" colspan="2" align="center">与上期比较</td>
    <td width="24%" colspan="2" align="center">与平均比较</td>
    <td width="20%" rowspan="2" align="center">备注</td>
  </tr>
  <tr bgcolor="#CCCCCC" >
    <td width="12%" align="center" >上期值</td>
    <td width="12%" align="center">差异</td>
    <td width="12%" align="center">平均值</td>
    <td width="12%" align="center">差异</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td width="20%" align="center" height="35">直接成本率</td>
    <td width="12%" align="center" height="35"><%=part1[1]%>%</td>
    <td width="12%" align="center" height="35"><%=part2[1]%>%</td>
    <td width="12%" align="center" height="35"><%=part3[1]%>%</td>
    <td width="12%" align="center" height="35"></td>
    <td width="12%" align="center" height="35"></td>
    <td width="20%" align="center" height="35" height="35"></td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td width="20%" align="center" height="35">间接成本率</td>
    <td width="12%" align="center" height="35"><%=part1[2]%>%</td>
    <td width="12%" align="center" height="35"><%=part2[2]%>%</td>
    <td width="12%" align="center" height="35"><%=part3[2]%>%</td>
    <td width="12%" align="center" height="35"></td>
    <td width="12%" align="center" height="35"></td>
    <td width="20%" align="center" height="35"></td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td width="20%" align="center" height="35">管理费用成本率</td>
    <td width="12%" align="center" height="35"><%=part1[3]%>%</td>
    <td width="12%" align="center" height="35"><%=part2[3]%>%</td>
    <td width="12%" align="center" height="35"><%=part3[3]%>%</td>
    <td width="12%" align="center" height="35"></td>
    <td width="12%" align="center" height="35"></td>
    <td width="20%" align="center" height="35"></td>
  </tr>

  <tr bgcolor="#FFFFFF">
    <td width="20%" align="center" height="35">固定成本率</td>
    <td width="12%" align="center" height="35"><%=part1[4]%>%</td>
    <td width="12%" align="center" height="35"><%=part2[4]%>%</td>
    <td width="12%" align="center" height="35"><%=part3[4]%>%</td>
    <td width="12%" align="center" height="35"></td>
    <td width="12%" align="center" height="35"></td>
    <td width="20%" align="center" height="35"></td>
  </tr>

  <tr bgcolor="#FFFFFF">
    <td width="20%" align="center" height="35">折旧成本率</td>
    <td width="12%" align="center" height="35"><%=part1[5]%>%</td>
    <td width="12%" align="center" height="35"><%=part2[5]%>%</td>
    <td width="12%" align="center" height="35"><%=part3[5]%>%</td>
    <td width="12%" align="center" height="35"></td>
    <td width="12%" align="center" height="35"></td>
    <td width="20%" align="center" height="35"></td>
  </tr>

  <tr bgcolor="#FFFFFF">
    <td width="20%" align="center" height="35">工资成本率</td>
    <td width="12%" align="center" height="35"><%=part1[6]%>%</td>
    <td width="12%" align="center" height="35"><%=part2[6]%>%</td>
    <td width="12%" align="center" height="35"><%=part3[6]%>%</td>
    <td width="12%" align="center" height="35"></td>
    <td width="12%" align="center" height="35"></td>
    <td width="20%" align="center" height="35"></td>
  </tr>

  <tr bgcolor="#FFFFFF">
    <td width="20%" align="center" height="35">变动成本率</td>
    <td width="12%" align="center" height="35"><%=part1[7]%>%</td>
    <td width="12%" align="center" height="35"><%=part2[7]%>%</td>
    <td width="12%" align="center" height="35"><%=part3[7]%>%</td>
    <td width="12%" align="center" height="35"></td>
    <td width="12%" align="center" height="35"></td>
    <td width="20%" align="center" height="35"></td>
  </tr>

  <tr bgcolor="#FFFFFF">
    <td width="20%" align="center" height="35">可控成本率</td>
    <td width="12%" align="center" height="35"><%=part1[8]%>%</td>
    <td width="12%" align="center" height="35"><%=part2[8]%>%</td>
    <td width="12%" align="center" height="35"><%=part3[8]%>%</td>
    <td width="12%" align="center" height="35"></td>
    <td width="12%" align="center" height="35"></td>
    <td width="20%" align="center" height="35"></td>
  </tr>

  <tr bgcolor="#FFFFFF">
    <td width="20%" align="center" height="35">不可控成本率</td>
    <td width="12%" align="center" height="35"><%=part1[9]%>%</td>
    <td width="12%" align="center" height="35"><%=part2[9]%>%</td>
    <td width="12%" align="center" height="35"><%=part3[9]%>%</td>
    <td width="12%" align="center" height="35"></td>
    <td width="12%" align="center" height="35"></td>
    <td width="20%" align="center" height="35"></td>
  </tr>



</table>

                </td>
            </tr>

            <tr bgcolor="#FFFFFF" class="a12" align="center">
                <td height="60" valign="middle" colspan="6">

                    <input type="button" value="查 看" onclick="return check();" >
        <!--            <input type="button" value="提 交" onclick="return create();" >   -->
                    <input type="hidden" name="subFunction" >
                    <input type="hidden" name="newdate" value = "<%=newdate%>">
                </td>
            </tr>
        </table>
    </form>


<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>

