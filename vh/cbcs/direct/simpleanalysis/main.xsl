<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
  				<th noWrap="true" rowspan ='2' >科室</th>
				<th noWrap="true" colspan ='2' >收入</th>
				<th noWrap="true" colspan ='4' >成本</th>
				<th noWrap="true" colspan ='2' >收益</th>
				<th noWrap="true" colspan ='2' >成本收益率</th>
  		</tr>
  		 <tr class="mainHead">
				<th noWrap="true" style ='display:none'></th>
				<th noWrap="true" >本期</th>
				<th noWrap="true" >累计</th>
				<th noWrap="true" >本期可控成本</th>
				<th noWrap="true" >累计可控成本</th>
				<th noWrap="true" >本期直接成本</th>
				<th noWrap="true" >累计直接成本</th>
				<th noWrap="true" >本期</th>
				<th noWrap="true" >累计</th>
				<th noWrap="true" >本期</th>
				<th noWrap="true" >累计</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
		<xsl:variable name="times" select="td[position()=1]"/>
          <xsl:for-each select="td">
              <xsl:choose>
               <xsl:when test="position()=1 ">
			<td align="left"><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:when test="position()=2 ">
                  <td align = 'right'>
			<a href="#">
				<xsl:attribute name="onclick">
					openDialog('income.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;curmonth&gt;<xsl:value-of select="."/>&lt;/curmonth&gt;&lt;times&gt;<xsl:value-of select='$times'/>&lt;/times&gt;&lt;type&gt;<xsl:value-of select='0'/>&lt;/type&gt;', 'dialogWidth:1024px;dialogHeight:768px')
				</xsl:attribute>
			<xsl:value-of select="format-number(.,'#,##0.00')"/>
                   	</a>
                  </td>
                </xsl:when>
                <xsl:when test="position()=3 ">
                  <td align = 'right'>
			<a href="#">
				<xsl:attribute name="onclick">
					openDialog('income.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;curmonth&gt;<xsl:value-of select="."/>&lt;/curmonth&gt;&lt;times&gt;<xsl:value-of select='$times'/>&lt;/times&gt;&lt;type&gt;<xsl:value-of select='1'/>&lt;/type&gt;', 'dialogWidth:1024px;dialogHeight:768px')
				</xsl:attribute>
			<xsl:value-of select="format-number(.,'#,##0.00')"/>
                   	</a>
                  </td>
                </xsl:when>
                <xsl:when test="position()=4 ">
                  <td align = 'right'>
			<a href="#">
				<xsl:attribute name="onclick">
					openDialog('dir.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;curmonth&gt;<xsl:value-of select="."/>&lt;/curmonth&gt;&lt;times&gt;<xsl:value-of select='$times'/>&lt;/times&gt;&lt;type&gt;<xsl:value-of select='0'/>&lt;/type&gt;', 'dialogWidth:1024px;dialogHeight:768px')
				</xsl:attribute>
			<xsl:value-of select="format-number(.,'#,##0.00')"/>
                   	</a>
                  </td>
                </xsl:when>
                <xsl:when test="position()=5 ">
                  <td align = 'right'>
			<a href="#">
				<xsl:attribute name="onclick">
					openDialog('dir.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;curmonth&gt;<xsl:value-of select="."/>&lt;/curmonth&gt;&lt;times&gt;<xsl:value-of select='$times'/>&lt;/times&gt;&lt;type&gt;<xsl:value-of select='1'/>&lt;/type&gt;', 'dialogWidth:1024px;dialogHeight:768px')
				</xsl:attribute>
			<xsl:value-of select="format-number(.,'#,##0.00')"/>
                   	</a>
                  </td>
                </xsl:when>
		<xsl:when test="position()=10 or position()=11 ">
                    <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:otherwise>
                  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  </xsl:template>
</xsl:stylesheet>
