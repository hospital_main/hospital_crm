<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
  	<thead>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
		  <tr  noWrap="true"  class="mainHead">
  				<td noWrap="true" rowspan ='2'>科室</td>
				<td noWrap="true" colspan = '2'>收入</td>
				<td style="display:none"></td>
				<td noWrap="true"  colspan = '4'>成本</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td noWrap="true"  colspan = '2'>收益</td>
				<td style="display:none"></td>
				<td noWrap="true"  colspan = '2'>成本收益率</td>
				<td style="display:none"></td>
  		</tr>
		<tr  noWrap="true" class="mainHead">
				<td noWrap="true" style ='display:none'></td>
				<td noWrap="true">本期</td>
				<td noWrap="true">累计</td>
				<td noWrap="true">本期可控成本</td>
				<td noWrap="true">累计可控成本</td>
				<td noWrap="true">本期直接成本</td>
				<td noWrap="true">累计直接成本</td>
				<td noWrap="true">本期</td>
				<td noWrap="true">累计</td>
				<td noWrap="true" >本期</td>
				<td noWrap="true" >累计</td>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
		<xsl:variable name="times" select="td[position()=1]"/>
          <xsl:for-each select="td">
              <xsl:choose>
               <xsl:when test="position()=1 ">
			<td align="left"><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:when test="position()=2 ">
                  <td align = 'right'>
			<xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:when>
		<xsl:when test="position()=10 or position()=11 ">
                    <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
                </xsl:when>
                <xsl:otherwise>
                  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>
