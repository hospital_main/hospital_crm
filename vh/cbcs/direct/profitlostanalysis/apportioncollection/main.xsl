<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>    
		<xsl:variable name="gy" select="/root/tbody/tr[1]/td[last()-1]"/>
		<xsl:variable name="yf" select="/root/tbody/tr[1]/td[last()]"/>
			<tr noWrap="true" class="mainHead">    		  
				<td noWrap="true" rowspan="3">科室</td>    
				<td noWrap="true" rowspan="3">总成本</td>
				<td noWrap="true" colspan="4">直接成本</td>
				<td noWrap="true" colspan="8">间接成本</td>
		  </tr>
		  <tr noWrap="true" class="mainHead">    
		    <td noWrap="true" colspan="2">直接计入</td>
				<td noWrap="true" colspan="2">计算计入</td>
				<xsl:if test="$gy=1">
					<td noWrap="true" colspan="2">分摊公用成本</td>
				</xsl:if>
				<td noWrap="true" colspan="2">分摊管理成本</td>
				<xsl:if test="$yf=1">
					<td noWrap="true" colspan="2">分摊医辅成本</td>
				</xsl:if>
				<td noWrap="true" colspan="2">分摊医技成本</td>
		  </tr>
		  <tr noWrap="true" class="mainHead">    
				<td noWrap="true">金额</td>
				<td noWrap="true">比例</td>
				<td noWrap="true">金额</td>
				<td noWrap="true">比例</td>
				<xsl:if test="$gy=1">
					<td noWrap="true">金额</td>
					<td noWrap="true">比例</td>
				</xsl:if>
				<td noWrap="true">金额</td>
				<td noWrap="true">比例</td>
				<xsl:if test="$yf=1">
					<td noWrap="true">金额</td>
					<td noWrap="true">比例</td>
				</xsl:if>
				<td noWrap="true">金额</td>
				<td noWrap="true">比例</td>
			</tr>                                                                                     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>  
					<xsl:for-each select="td">
						<xsl:choose> 
							<xsl:when test="position()=1 or position()=2">
								<td style="display:none"><xsl:value-of select="."/></td>
							</xsl:when>
							
							<xsl:when test="position()=last()">
								
							</xsl:when>
							
							<xsl:when test="position()=last()-1">
								
							</xsl:when>
							
							<xsl:when test="position()=3">
							  <xsl:if test="../td[2]!='A'">
                  <td noWrap="true">
                    <a href="#">
  										<xsl:attribute name="onclick">
  											openTooMuchLink(pageMap,<xsl:value-of select="position()"/>,'<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
  										</xsl:attribute>
  										<xsl:value-of select="."/>
									  </a>
                  </td>
                </xsl:if>
                <xsl:if test="../td[2]='A'">
                  <td noWrap="true"><xsl:value-of select="."/></td>
                </xsl:if>
							</xsl:when>
							<xsl:when test="position()=4">
								<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							
							
							
							<xsl:when test="$gy=1 and $yf=1 and (position()=5 or position()=11 or position()=13 or position()=15)">
								<xsl:if test="../td[3]!='合计'">
  								<td noWrap="true" class="numberText">
  								  <a href="#">
  										<xsl:attribute name="onclick">
  											openTooMuchLink(pageMap,<xsl:value-of select="position()"/>,'<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
  										</xsl:attribute>
  										<xsl:value-of select="format-number(.,'#,##0.00')"/>
  									</a>
  								</td>
								</xsl:if>
								<xsl:if test="../td[3]='合计'">
								  <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:if>
							</xsl:when>
							
							<xsl:when test="$gy!=1 and $yf=1 and (position()=5 or position()=9 or position()=11 or position()=13)">
								<xsl:if test="../td[3]!='合计'">
  								<td noWrap="true" class="numberText">
  								  <a href="#">
  										<xsl:attribute name="onclick">
  											openTooMuchLink(pageMap2,<xsl:value-of select="position()"/>,'<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
  										</xsl:attribute>
  										<xsl:value-of select="format-number(.,'#,##0.00')"/>
  									</a>
  								</td>
								</xsl:if>
								<xsl:if test="../td[3]='合计'">
								  <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:if>
							</xsl:when>
							
							<xsl:when test="$gy!=1 and $yf!=1 and (position()=5 or position()=9 or position()=11)">
								<xsl:if test="../td[3]!='合计'">
  								<td noWrap="true" class="numberText">
  								  <a href="#">
  										<xsl:attribute name="onclick">
  											openTooMuchLink(pageMap3,<xsl:value-of select="position()"/>,'<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
  										</xsl:attribute>
  										<xsl:value-of select="format-number(.,'#,##0.00')"/>
  									</a>
  								</td>
								</xsl:if>
								<xsl:if test="../td[3]='合计'">
								  <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:if>
							</xsl:when>
							
							<xsl:when test="$gy=1 and $yf!=1 and (position()=5 or position()=11 or position()=13)">
								<xsl:if test="../td[3]!='合计'">
  								<td noWrap="true" class="numberText">
  								  <a href="#">
  										<xsl:attribute name="onclick">
  											openTooMuchLink(pageMap4,<xsl:value-of select="position()"/>,'<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
  										</xsl:attribute>
  										<xsl:value-of select="format-number(.,'#,##0.00')"/>
  									</a>
  								</td>
								</xsl:if>
								<xsl:if test="../td[3]='合计'">
								  <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:if>
							</xsl:when>
							
							<xsl:when test="$gy=1 and $yf=1 and (position()=7 or position()=9)">
							  <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							
							<xsl:when test="$gy!=1 and $yf=1 and position()=7">
							  <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							
							<xsl:when test="$gy!=1 and $yf!=1 and position()=7">
							  <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							
							<xsl:when test="$gy=1 and $yf!=1 and position()=9">
							  <td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							
							<xsl:otherwise>
  							<td noWrap="true" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
  						</xsl:otherwise>
  						
  					
  						
  					
  						
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
