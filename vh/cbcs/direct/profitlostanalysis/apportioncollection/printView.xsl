<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
  	<thead>     
  	<xsl:variable name="gy" select="/root/tbody/tr[1]/td[last()-1]"/>
	<xsl:variable name="yf" select="/root/tbody/tr[1]/td[last()]"/>
  	<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr> 
			<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" rowspan="3">科室</td>    
				<td noWrap="true" rowspan="3">总成本</td>
				<td noWrap="true" colspan="4">直接成本</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td noWrap="true" colspan="8">间接成本</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
		    <td style="display:none"></td>
		    <td style="display:none"></td>
		    <td noWrap="true" colspan="2">直接计入</td>
		    <td style="display:none"></td>
				<td noWrap="true" colspan="2">间接计入</td>
				<td style="display:none"></td>
				<xsl:if test="$gy=1">
					<td noWrap="true" colspan="2">分摊公用成本</td>
					<td style="display:none"></td>
				</xsl:if>
				<td noWrap="true" colspan="2">分摊管理成本</td>
				<td style="display:none"></td>
				<xsl:if test="$yf=1">
					<td noWrap="true" colspan="2">分摊医辅成本</td>
					<td style="display:none"></td>
				</xsl:if>
				<td noWrap="true" colspan="2">分摊医技成本</td>
				<td style="display:none"></td>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
		    <td style="display:none"></td>
		    <td style="display:none"></td>
				<td noWrap="true">金额</td>
				<td noWrap="true">比例</td>
				<td noWrap="true">金额</td>
				<td noWrap="true">比例</td>
				<xsl:if test="$gy=1">
					<td noWrap="true">金额</td>
					<td noWrap="true">比例</td>
				</xsl:if>
				<td noWrap="true">金额</td>
				<td noWrap="true">比例</td>
				<xsl:if test="$yf=1">
					<td noWrap="true">金额</td>
					<td noWrap="true">比例</td>
				</xsl:if>
				<td noWrap="true">金额</td>
				<td noWrap="true">比例</td>
			</tr>
		</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>  
					<xsl:for-each select="td[position()!=1 and position()!=2]">
						<xsl:choose> 
							<xsl:when test="position()=1">
                <td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=last()">
                
							</xsl:when>
							
							<xsl:when test="position()=last()-1">
                
							</xsl:when>
							
							<xsl:when test="position()=2 or (position() mod 2)=1">
								<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
  							<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
  						</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>
