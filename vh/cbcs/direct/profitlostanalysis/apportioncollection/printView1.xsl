<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
	  <colgroup>
			<col style = 'width:225mm'/>
			<col style = 'width:225mm'/>
			<col style = 'width:225mm'/>
		</colgroup>
  	<thead>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
	    <tr noWrap="true" class="mainHead">
	      <xsl:for-each select="/root/tbody/tr[td[1]='yes']/td[position()!=1 and position()!=2]">
				  <xsl:choose>
            <xsl:when test="position()=1">
			    		<td noWrap="true"><xsl:value-of select="."/></td>
			    	</xsl:when>
				    <xsl:otherwise>
				      <td noWrap="true"><xsl:value-of select="."/></td>
				    </xsl:otherwise>
				  </xsl:choose>
				</xsl:for-each>
			</tr>
		</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr[td[1]='no' and (td[2]='2' or td[2]='3')]">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1 or position()=2">
              </xsl:when>
              <xsl:when test="position()=5">
  			    		   <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:otherwise>
  				      <td><xsl:value-of select="."/></td>
  				    </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
        
       <xsl:for-each select="/root/tbody/tr[td[1]='no' and (td[2]='5' or td[2]='6')]">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1 or position()=2">
              </xsl:when>
              <xsl:when test="position()=3">
  			    		  <xsl:if test="../td[2]='5'">
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:if>
                <xsl:if test="../td[2]!='5'">
                  <td><xsl:value-of select="."/></td>
                </xsl:if>
              </xsl:when>
              <xsl:when test="position()=4">
  			    		   <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:when test="position()=5">
  			    		   <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
              </xsl:when>
              <xsl:otherwise>
  				      <td><xsl:value-of select="."/></td>
  				    </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>
