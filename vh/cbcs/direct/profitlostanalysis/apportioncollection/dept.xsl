<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	
		<thead>        
				<xsl:variable name="gy" select="/root/annex/is_gy"/>
				<xsl:variable name="yf" select="/root/annex/is_yf"/>  
				  
				<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true" rowspan="2">成本项目</th>    
				<th noWrap="true" colspan="2">总成本</th>
				<th style="display:none"></th>
				<th noWrap="true" colspan="2">直接成本</th>
				<th style="display:none"></th>
				<xsl:if test="$gy=1">
					<th noWrap="true" colspan="2">公用成本</th>
					<th style="display:none"></th>
				</xsl:if>
				<th noWrap="true" colspan="2">管理分摊成本</th>
				<th style="display:none"></th>
				<xsl:if test="$yf=1">
					<th noWrap="true" colspan="2">医辅分摊成本</th>
					<th style="display:none"></th>
				</xsl:if>
				<th noWrap="true" colspan="2">医技分摊成本</th>
				<th style="display:none"></th>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
		    <th style="display:none"></th>
		    <th noWrap="true">成本</th>
		    <th noWrap="true">比例</th>
			<th noWrap="true">成本</th>
		    <th noWrap="true">比例</th>
		    <xsl:if test="$gy=1">
			    <th noWrap="true">成本</th>
			    <th noWrap="true">比例</th>
		    </xsl:if>
		    <th noWrap="true">成本</th>
		    <th noWrap="true">比例</th>
		    <xsl:if test="$yf=1">
			    <th noWrap="true">成本</th>
			    <th noWrap="true">比例</th>
		    </xsl:if>
		    <th noWrap="true">成本</th>
		    <th noWrap="true">比例</th>
		  </tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>  
					<xsl:for-each select="td">
						<xsl:choose> 
							<xsl:when test="position()=1">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							
							<xsl:when test="position()=6">
							  <xsl:if test="$gy=1">
							  	<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							  </xsl:if>
							  <xsl:if test="$gy=0">
							  </xsl:if>
							</xsl:when>
							
							<xsl:when test="position()=7">
							  <xsl:if test="$gy=1">
							  	<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
							  </xsl:if>
							  <xsl:if test="$gy=0">
							  </xsl:if>
							</xsl:when>
							
							
							<xsl:when test="position()=10">
							  <xsl:if test="$yf=1">
							  	<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							  </xsl:if>
							  <xsl:if test="$yf=0">
							  </xsl:if>
							</xsl:when>
							
							<xsl:when test="position()=11">
							  <xsl:if test="$yf=1">
							  	<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
							  </xsl:if>
							  <xsl:if test="$yf=0">
							  </xsl:if>
							</xsl:when>
							
							
							<xsl:when test="(position() mod 2)=0">
							  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
  							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
  						</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
