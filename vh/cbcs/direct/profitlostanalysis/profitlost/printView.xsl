<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
  	<thead>
  	<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true" rowspan="3">科室</td>
				<td noWrap="true" colspan="9">收益</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
		  </tr>
		  <tr noWrap="true" class="mainHead">
		    <td noWrap="true" style="display:none"></td>
		    <td noWrap="true" colspan="3">收入</td>
		    <td noWrap="true" style="display:none"></td>
		    <td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">成本</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="2">收益</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" rowspan="2">工作量</td>
		  </tr>
		  <tr noWrap="true" class="mainHead">
		    <td noWrap="true" style="display:none"></td>
				<td noWrap="true">合计</td>
				<td noWrap="true">直接</td>
				<td noWrap="true">间接</td>
				<td noWrap="true">合计</td>
				<td noWrap="true">直接</td>
				<td noWrap="true">间接</td>
				<td noWrap="true">收益</td>
				<td noWrap="true">成本收益率</td>
				<td noWrap="true" style="display:none"></td>
			</tr>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:if test="pk/isImport=1">
						<xsl:attribute name="style">
							color:red
						</xsl:attribute>
					</xsl:if>
					<xsl:for-each select="td">
						<td>
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:when test="position()=9">
								  <xsl:attribute name='align'>right</xsl:attribute>
								  <xsl:value-of select="format-number(.,'#,##0.00%')"/>
								</xsl:when>
								<xsl:otherwise>
								  <xsl:attribute name='align'>right</xsl:attribute>
								  <xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>
