<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">  		  
			<th noWrap="true">职工编号</th>
			<th noWrap="true">职工姓名</th>
			<th noWrap="true">职称</th>
			<th noWrap="true">工资合计</th>
			<th noWrap="true">比例</th>
			<xsl:variable name="flag" select="count(/root/tbody/tr)" />
				<xsl:if test="$flag > 0">
        				<th noWrap="true">@active_1</th>
      				</xsl:if>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>          
				<xsl:for-each select="td">
					<xsl:choose> 
						<xsl:when test="position()=1 or position()=2 or position()=3">
							<td><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:when test="position()=5">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="../td[1]!='合计' or position()=4">
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:if>
							<xsl:if test="../td[1]='合计' and position()!=4">
								<td noWrap="true"></td>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>