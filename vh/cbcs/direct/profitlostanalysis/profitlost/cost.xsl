<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
			<tr noWrap="true" class="mainHead">  		  
				<th noWrap="true" rowspan="3">成本归集分类</th>
				<th noWrap="true" rowspan="2" colspan="2">总成本</th>
				<th style="display:none"></th>
				<th noWrap="true" colspan="4">直接成本</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th noWrap="true" colspan="8">间接成本</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th noWrap="true" rowspan="3">跟踪分析</th>
			</tr>
			<tr noWrap="true" class="mainHead">  
			  <th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th noWrap="true" colspan="2">直接计入</th>
				<th style="display:none"></th>
				<th noWrap="true" colspan="2">计算计入</th>
				<th style="display:none"></th>
				<th noWrap="true" colspan="2">公用成本</th>
				<th style="display:none"></th>
				<th noWrap="true" colspan="2">管理分摊</th>
				<th style="display:none"></th>
				<th noWrap="true" colspan="2">医辅分摊</th>
				<th style="display:none"></th>
				<th noWrap="true" colspan="2">医技分摊</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
			</tr>
			<tr noWrap="true" class="mainHead">  
			  <th style="display:none"></th>
				<th noWrap="true">金额</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">成本</th>
				<th noWrap="true">比例</th>
				<th noWrap="true">成本</th>
				<th noWrap="true">比例</th>
				<th style="display:none"></th>
			</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>          
				<xsl:for-each select="td">
				  
					<xsl:choose> 
						<xsl:when test="position()=1">
							<td><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:when test="position()=4">
						  <td class="numberText">
  							<a href="#">
  								<xsl:attribute name="onclick">
  									openTooMuchLink(pageMap,'<xsl:value-of select="../pk/costClassCode"/>_<xsl:value-of select="position()"/>','<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
  								</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
  							</a>
							</td>
						</xsl:when>
						<xsl:when test="position()=2 or (position() mod 2)=0">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:when test="(position() mod 2)=1">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
				<td align="center">
  				<xsl:if test="td[1]!='合计'">
  					<a href="#">
  						<xsl:attribute name="onclick">
  							openTooMuchLink(pageMap,16,'<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
  					</xsl:attribute><IMG src="../../../../images/yingkui/genzong.png" border="0"/></a>
  				</xsl:if>	
				</td>
			</tr>
		</xsl:for-each>  	
	</tbody>
	</xsl:template>
</xsl:stylesheet>
