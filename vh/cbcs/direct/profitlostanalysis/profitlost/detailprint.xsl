<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true">收费分类</td>
				<td noWrap="true">合计</td>
				<td noWrap="true">直接收入</td>
				<td noWrap="true">间接收入</td>
  		</tr>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise>
								<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:3;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
  </root>
	</xsl:template>
</xsl:stylesheet>
