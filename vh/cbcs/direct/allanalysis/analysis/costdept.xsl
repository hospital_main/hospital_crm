<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" >科室名称</th>
			<th noWrap="true" >分摊成本</th>
			<th noWrap="true" >比例</th>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>          
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td>
  							<xsl:if test="../td[1]!='合计'">
  							  <a href="#">
  							    <xsl:attribute name="onclick">
  							      openDialog('costdept_item.html?load=&lt;dateYear&gt;<xsl:value-of select="../pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="../pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="../pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;deptCode&gt;<xsl:value-of select="../pk/deptCode"/>&lt;/deptCode&gt;&lt;costType&gt;<xsl:value-of select="../pk/costType"/>&lt;/costType&gt;','dialogWidth:850px;dialogHeight:540px',result)
  							    </xsl:attribute>
  							    <xsl:value-of select="."/>
  							  </a>
  							</xsl:if>
  							<xsl:if test="../td[1]='合计'">
  							  <xsl:value-of select="."/>
  							</xsl:if>  
							</td>
						</xsl:when>
						<xsl:when test="position()=2">
							<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:when test="position()=3">
							<td align="right"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
