<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true">住院科室</td>
				<td noWrap="true">实际床日数</td>
				<td noWrap="true">单位收入</td>
				<td noWrap="true">单位变动成本</td>
				<td noWrap="true">单位收益</td>
				<td noWrap="true">固定成本</td>
				<td noWrap="true">变动成本</td>
				<td noWrap="true">保本床日数</td>
				<td noWrap="true">保本收入</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position()!=10]">
						<xsl:variable name="ppp" select="position()"/>
						<xsl:choose>
							<xsl:when test="position()=1 ">
								<td>
									<xsl:if test="../td[4]&lt;../td[3]">
										<a href="#">
											<xsl:attribute name="onclick" >
												openDialog('chart.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;v1&gt;<xsl:value-of select="../td[8]"/>&lt;/v1&gt;&lt;v2&gt;<xsl:value-of select="../td[9]"/>&lt;/v2&gt;&lt;v3&gt;<xsl:value-of select="../td[6]"/>&lt;/v3&gt;', 'dialogWidth:850px;dialogHeight:540px', result)
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="../td[4]&gt;=../td[3]">
										<xsl:value-of select="."/>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()=6 or position()=7">
								<td align="right">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<a href="#">
										<xsl:if test="../td[2]&lt;../td[8]">
				        						<xsl:attribute name="style">color:red</xsl:attribute>
				        					</xsl:if>
										<xsl:attribute name="onclick" >
											openDialog('on_<xsl:value-of select="$ppp"/>.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:540px', result)
										</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</a>
								</td>
							</xsl:when>
							<xsl:when test="position()=8 or position()=9">
								<td align="right">
									<xsl:if test="../td[4]&lt;../td[3]">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:if test="../td[2]&lt;../td[8]">
											<xsl:attribute name="style">color:red</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
									<xsl:if test="../td[4]&gt;=../td[3]">
										<xsl:attribute name="class">numberText</xsl:attribute>
										-
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td align="right">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:if test="../td[2]&lt;../td[8]">
										<xsl:attribute name="style">color:red</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:8;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
  </root>
	</xsl:template>
</xsl:stylesheet>
