<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<!--
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			-->
	  		</thead>
		  	<tbody>
				<!--收益状况分析-->
				<!--<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td ></td>
					<td ></td>
					<td ></td>
					<td ></td>
					<td ></td>
				</tr>-->
				<tr>
					<td colspan="9" id="syntdesisinfanalyTd1">收益状况分析</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap="true">
					<td noWrap="true" rowspan="2" class="mainBody" align="center">项目</td>
					<td noWrap="true" colspan="4" class="mainBody" align="center">全院</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td noWrap="true" rowspan="2" class="mainBody" align="center">药占比</td>
					<td noWrap="true" colspan="3" class="mainBody" align="center">每职工</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap="true" class="mainHead">
					<td noWrap="true" style="display:none"></td>
					<td class="mainBody" align="center">收入</td>
					<td class="mainBody" align="center">成本</td>
					<td class="mainBody" align="center">收益</td>
					<td noWrap="true" class="mainBody" align="center">成本收益率</td>
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" class="mainBody" align="center">收入</td>
					<td noWrap="true" class="mainBody" align="center">成本</td>
					<td noWrap="true" class="mainBody" align="center">收益</td>
				</tr>
				
				<xsl:for-each select="/root/tbody/tr[td[1]=11]">
					<tr>          
						<xsl:for-each select="td[position()!=1]">
							<xsl:choose> 
								<xsl:when test="position()=1">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												openDialog('detail11.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidtd:850px;dialogHeight:540px', result)
											</xsl:attribute>
										<xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=5 or position()=6">
									<td class="numberText" align='right'><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td class="numberText" align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>  	
				
				<tr noWrap="true" >
					<td noWrap="true" rowspan="2" class="mainBody" align="center">项目</td>
					<td noWrap="true" colspan="4" class="mainBody" align="center">医疗（不含药品）</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td noWrap="true" colspan="4" class="mainBody" align="center">药品</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap="true" >
					<td noWrap="true" style="display:none"></td>
					<td noWrap="true" class="mainBody" align="center">收入</td>
					<td noWrap="true" class="mainBody" align="center">成本</td>
					<td noWrap="true" class="mainBody" align="center">收益</td>
					<td noWrap="true" class="mainBody" align="center">成本收益率</td>
					<td noWrap="true" class="mainBody" align="center">收入</td>
					<td noWrap="true" class="mainBody" align="center">成本</td>
					<td noWrap="true" class="mainBody" align="center">收益</td>
					<td noWrap="true" class="mainBody" align="center">成本收益率</td>
				</tr>
				
				<xsl:for-each select="/root/tbody/tr[td[1]=12]">
					<tr>          
						<xsl:for-each select="td[position()!=1]">
							<xsl:choose> 
								<xsl:when test="position()=1">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												openDialog('detail12.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidtd:850px;dialogHeight:540px', result)
											</xsl:attribute>
										<xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=5 or position()=9">
									<td class="numberText" align='right'><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td class="numberText" align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>  
				
				<tr>
					<td colspan="9" id="syntdesisinfanalyTd2">成本状况分析</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>        
				<tr noWrap="true" >
					<td noWrap="true" class="mainBody" align="center">分类方法</td>
					<td noWrap="true" class="mainBody" align="center">成本分类</td>
					<td noWrap="true" class="mainBody" align="center">金额</td>
					<td noWrap="true" class="mainBody" align="center">百分比</td>
					<td noWrap="true" colspan="2" class="mainBody" align="center">分类方法</td>
					<td style="display:none"></td>
					<td noWrap="true" class="mainBody" align="center">成本分类</td>
					<td noWrap="true" class="mainBody" align="center">金额</td>
					<td noWrap="true" class="mainBody" align="center">百分比</td>
				</tr>
				
				<xsl:for-each select="/root/tbody/tr[td[1]=2]">
					<xsl:variable name="pos" select="position()"/>
					<tr>          
						<xsl:for-each select="td[position()!=1]">
							<xsl:choose> 
								<xsl:when test="position()=1">
									<xsl:if test="1=$pos">
										<td colspan='1' rowspan='5' align="center">
											科室类别
										</td> 
									</xsl:if>
									<xsl:if test="2=$pos">
									</xsl:if>
									
									<xsl:if test="6=$pos">
										<td colspan='1' rowspan='3' align="center">
											 可控性
										</td>
									</xsl:if>
									<xsl:if test="1!=$pos and 6!=$pos">
										<td style="display:none"></td>
									</xsl:if>									
								</xsl:when>
							 
								
								<xsl:when test="position()=6">
									<xsl:if test="1=$pos">
										
										<td colspan='2' rowspan='2' align="center">
											成本习性
										</td> 
									</xsl:if>
									<xsl:if test="2=$pos">
									</xsl:if>
									
									<xsl:if test="3=$pos">
										<td colspan='2' rowspan='6' align="center">
											 项目构成
										</td>
									</xsl:if>
									<td>
									 <xsl:if test="2 &lt; $pos and 9 &gt; $pos">
									</xsl:if>
										</td> 
									<xsl:if test="1!=$pos and 3!=$pos">
										<td style="display:none"></td>
									</xsl:if>
								</xsl:when>
								
								
								<xsl:when test="position()=2">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												openDialog('detail21.html?load=&lt;dateYear&gt;<xsl:value-of select="../pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="../pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="../pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;itemCode&gt;<xsl:value-of select="../td[2]"/>&lt;/itemCode&gt;&lt;itemName&gt;<xsl:value-of select="../td[3]"/>&lt;/itemName&gt;', 'dialogWidth:850px;dialogHeight:540px', result)
											</xsl:attribute>
										<xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=7">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=3 ">
									<xsl:if test=". !=0">
									<td class="numberText"  align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:if>
									<xsl:if test=". =0">
									<td ></td>
									</xsl:if>
								</xsl:when>
								<xsl:when test=" position()=8">
									<td class="numberText" align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
								<xsl:when test="position()=4 ">
									<xsl:if test=". !=0">
									<td class="numberText"  align='right'><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
									</xsl:if>
									<xsl:if test=". =0">
									<td ></td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=9">
									<td class="numberText" align='right'><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
								</xsl:when>
								<xsl:when test="position()=5">
								</xsl:when>
								<xsl:otherwise>
									<td class="numberText" align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
				
				<tr>
					<td colspan="9" id="syntdesisinfanalyTd3">本量利分析</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>               
				<tr noWrap="true" >
					<td noWrap="true" class="mainBody" align="center">项目</td>
					<td noWrap="true" class="mainBody" align="center">固定成本</td>
					<td noWrap="true" class="mainBody" align="center">变动成本</td>
					<td noWrap="true" class="mainBody" align="center">工作量</td>
					<td noWrap="true" class="mainBody" align="center">单位收入</td>
					<td noWrap="true" class="mainBody" align="center">单位变动成本</td>
					<td noWrap="true" class="mainBody" align="center">单位收益</td>
					<td noWrap="true" class="mainBody" align="center">保本工作量</td>
					<td noWrap="true" class="mainBody" align="center">保本收入</td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[1]=3]">
					<tr>          
						<xsl:for-each select="td[position()!=1]">
							<xsl:choose> 
								<xsl:when test="position()=1">
									<td>
										<a href="#" onclick="alert('未定');openDialog('未知.html','dialogWidtd:850px;dialogHeight:540px',result)"><xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=8">
									<td class="numberText" align='right'>
										<a href="#">
											<xsl:attribute name="onclick" >
												openChart('cbcs/decide/costpoint/queryCostpointChart.jsp?dept=<xsl:value-of select="../td[2]"/>&amp;type=O&amp;dateType=montd&amp;dateValue=<xsl:value-of select="../pk/dateYear"/><xsl:value-of select="../pk/dateFromMontd"/>&amp;perIncome=<xsl:value-of select="../td[6]"/>&amp;perChangeCost=<xsl:value-of select="../td[7]"/>&amp;fixedCost=<xsl:value-of select="../td[3]"/>');
											</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/></a>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td class="numberText" align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each> 
				<!--
				<tr>
					<td colspan="9" id="synthesisinfanalyTd10" >科室排名分析</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>      
				<tr noWrap="true"  id="syntdesisinfanalyTd4">
					<td noWrap="true" colspan="4" class="tdHead" align="center">直接医疗科室盈余前5名</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td noWrap="true" colspan="5" class="tdHead" align="center">直接医疗亏损科室前5名</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					
				</tr>
				<tr noWrap="true" > 
					<td noWrap="true" class="mainBody" align="center">排名</td>
					<td noWrap="true" class="mainBody" align="center">科室</td>
					<td noWrap="true" class="mainBody" align="center">收益</td>
					<td noWrap="true" class="mainBody" align="center">收益率</td>
					<td noWrap="true" class="mainBody" align="center">排名</td>
					<td noWrap="true" class="mainBody" align="center">科室</td>
					<td noWrap="true" class="mainBody" align="center">收益</td>
					<td noWrap="true" class="mainBody" align="center">收益率</td>
					<td noWrap="true" class="mainBody" align="center"></td>
					
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[1]=4]">
				<xsl:variable name="pos" select="position()"/>
				<tr>
					<xsl:for-each select="td[position()!=1]">
						<xsl:choose> 
							<xsl:when test="position()=1 or position()=5">
								<td align="center"><xsl:value-of select="$pos"/></td>
							</xsl:when>
							<xsl:when test="position()=2 or position()=6">
								<xsl:variable name="codepos" select="position()"/>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()=3 or position()=8">
								<td class="numberText" align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=4 or position()=9">
								<td class="numberText"  align='right'><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
					<td></td>
				</tr>
			</xsl:for-each>  -->	
		</tbody>
		<!--
		<tfoot>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:8;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>		
				<td noWrap="true" style="fontsize:foot;colspan:1;align:right"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:foot;colspan:colcount;align:right"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
		</tfoot>
		-->
	</root>
	</xsl:template>
</xsl:stylesheet>

