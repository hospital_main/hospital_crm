<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" rowspan="2" class="mainHead" >科室</th>
			<th noWrap="true" colspan="4" class="mainHead" >医疗</th>
			<th style="display:none"></th>
			<th style="display:none"></th>
			<th style="display:none"></th>
			<th noWrap="true" colspan="4" class="mainHead" >药品</th>
			<th style="display:none"></th>
			<th style="display:none"></th>
			<th style="display:none"></th>
		</tr>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" style="display:none"></th>
			<th noWrap="true" class="mainHead" >收入</th>
			<th noWrap="true" class="mainHead" >成本</th>
			<th noWrap="true" class="mainHead" >收益</th>
			<th noWrap="true" class="mainHead" >成本收益率</th>
			<th noWrap="true" class="mainHead" >收入</th>
			<th noWrap="true" class="mainHead" >成本</th>
			<th noWrap="true" class="mainHead" >收益</th>
			<th noWrap="true" class="mainHead" >成本收益率</th>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>          
				<xsl:for-each select="td">
					<xsl:choose> 
					  <xsl:when test="position()=1">
							<td style="display:none"><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:when test="position()=2">
							<td><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:when test="position()=6 or position()=10">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>  	
	</tbody>
	</xsl:template>
</xsl:stylesheet>
