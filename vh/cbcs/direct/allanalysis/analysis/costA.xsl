<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">
			<th noWrap="true" >成本分类</th>
			<th noWrap="true" >金额</th>
			<th noWrap="true" >百分比</th>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>          
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td>
							  <!--
								<a href="#">
								<xsl:attribute name="onclick">
									<xsl:if test=".='直接成本'">
										openDialog('costdirect.html?load=&lt;dateYear&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;deptCode&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/deptCode"/>&lt;/deptCode&gt;&lt;costType&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/costType"/>&lt;/costType&gt;','dialogWidth:850px;dialogHeight:540px',result)
									</xsl:if>
									<xsl:if test=".='公用成本'">
										openDialog('costitem.html?load=&lt;dateYear&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;deptCode&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/deptCode"/>&lt;/deptCode&gt;&lt;costType&gt;<xsl:value-of select="/root/tbody/tr[2]/pk/costType"/>&lt;/costType&gt;','dialogWidth:850px;dialogHeight:540px',result)
									</xsl:if>
									<xsl:if test=".='管理分摊成本'">
										openDialog('costdept.html?load=&lt;dateYear&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;deptCode&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/deptCode"/>&lt;/deptCode&gt;&lt;costType&gt;<xsl:value-of select="/root/tbody/tr[3]/pk/costType"/>&lt;/costType&gt;','dialogWidth:850px;dialogHeight:540px',result)
									</xsl:if>
									<xsl:if test=".='医辅分摊成本'">
										openDialog('costdept.html?load=&lt;dateYear&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;deptCode&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/deptCode"/>&lt;/deptCode&gt;&lt;costType&gt;<xsl:value-of select="/root/tbody/tr[4]/pk/costType"/>&lt;/costType&gt;','dialogWidth:850px;dialogHeight:540px',result)
									</xsl:if>
									<xsl:if test=".='医技分摊成本'">
										openDialog('costtec.html?load=&lt;dateYear&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateYear"/>&lt;/dateYear&gt;&lt;dateFromMonth&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateFromMonth"/>&lt;/dateFromMonth&gt;&lt;dateToMonth&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/dateToMonth"/>&lt;/dateToMonth&gt;&lt;deptCode&gt;<xsl:value-of select="/root/tbody/tr[1]/pk/deptCode"/>&lt;/deptCode&gt;&lt;costType&gt;<xsl:value-of select="/root/tbody/tr[5]/pk/costType"/>&lt;/costType&gt;','dialogWidth:850px;dialogHeight:540px',result)
									</xsl:if>
								</xsl:attribute>
								<xsl:value-of select="."/>
								</a>
								-->
								<xsl:value-of select="."/>
							</td>
						</xsl:when>
						<xsl:when test="position()=2">
							<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:when test="position()=3">
							<td align="right"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
