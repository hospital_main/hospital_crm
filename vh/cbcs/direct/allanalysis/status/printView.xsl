<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
  	<thead>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:coltitle">项目</td>
				<td noWrap="true" style="fontsize:coltitle">收入</td>
				<td noWrap="true" style="fontsize:coltitle">成本</td>
				<td noWrap="true" style="fontsize:coltitle">收益</td>
				<td noWrap="true" style="fontsize:coltitle">门诊工作量</td>
				<td noWrap="true" style="fontsize:coltitle">住院工作量</td>
				<td noWrap="true" style="fontsize:coltitle">医技工作量</td>
  		</tr>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				<xsl:if test="position()=1">
					<xsl:for-each select="td">
						<td>
							<xsl:choose> 
								<xsl:when test="position()=1">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="style">align:right</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</xsl:if>
				<xsl:if test="position()=2">
					<xsl:for-each select="td">
						<td>
							<xsl:choose> 
								<xsl:when test="position()=1">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="style">align:right</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</xsl:if>
				<xsl:if test="position()=3">
					<xsl:for-each select="td">
						<td>
							<xsl:choose> 
								<xsl:when test="position()=1">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="style">align:right</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</xsl:if>
				<xsl:if test="position()=4">
					<xsl:for-each select="td">
						<td>
							<xsl:choose> 
								<xsl:when test="position()=1">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00%')"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</xsl:if>
				</tr>
			</xsl:for-each>  	
		</tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>
