<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/direct/allanalysis/multiorientation/printView.xsl,v 1.1 2012/03/12 01:57:58 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:58 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		
			<thead>
			<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
				<tr noWrap="true" class="mainHead">  		  
					<td noWrap="true" rowspan="2">科室</td>
					<td noWrap="true" rowspan="2">收入</td>       
					<td noWrap="true" colspan="2">全成本</td>
					<td style="display:none"></td>
					<td noWrap="true" colspan="2">可控成本</td>
					<td style="display:none"></td>
					<td noWrap="true" colspan="2">直接成本</td>
					<td style="display:none"></td>
					<td noWrap="true" colspan="2">不含管理成本</td>
					<td style="display:none"></td>
					<td noWrap="true" colspan="2">变动成本</td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap="true" class="mainHead">  		  
					<td style="display:none"></td>
					<td style="display:none"></td>      
					<td noWrap="true">成本</td>
					<td noWrap="true">收益</td>
					<td noWrap="true">成本</td>
					<td noWrap="true">收益</td>
					<td noWrap="true">成本</td>
					<td noWrap="true">收益</td>
					<td noWrap="true">成本</td>
					<td noWrap="true">收益</td>
					<td noWrap="true">成本</td>
					<td noWrap="true">收益</td>
				</tr>
			</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td">
						<xsl:choose> 
							<xsl:when test="position()!=1">
								<td class="numberText" style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
