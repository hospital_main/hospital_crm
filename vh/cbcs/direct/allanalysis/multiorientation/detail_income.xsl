<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead> 
		<tr noWrap="true" class="mainHead">
			<th noWrap="true">收费类别</th>
			<th noWrap="true">收入</th>
			<th noWrap="true">比率</th>
			<th noWrap="true">自己开单自己执行</th>
			<th noWrap="true">比率</th>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>          
				<xsl:for-each select="td">
					<xsl:choose> 
						<xsl:when test="position()=1">
						
						<xsl:if test="../td[1] != '合计'">
							<td>
								<a href="#">
										<xsl:attribute name="onclick">
											openTooMuchLink(pageMap,<xsl:value-of select="position()"/>,'<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
										</xsl:attribute><xsl:value-of select="."/>
								</a>
							</td>
						</xsl:if>	
						<xsl:if test="../td[1] = '合计'">
						<td>
						<xsl:value-of select="."/>
						</td>
						</xsl:if>	
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>  	
	</tbody>
	</xsl:template>
</xsl:stylesheet>
