<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>

  	<thead>
  	<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" rowspan="2">住院科室</td>
				<td noWrap="true" colspan="5">实际</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td noWrap="true" colspan="2">保本</td>
				<td style="display:none"></td>
		  </tr>
		  <tr noWrap="true" class="mainHead">
		  	<td style="display:none"></td>
				<td style="fontsize:coltitle">总收入</td>
				<td noWrap="true">总成本</td>
				<td noWrap="true">总收益</td>
				<td noWrap="true">床日数</td>
				<td noWrap="true">床位使用率</td>
				<td noWrap="true">保本床日</td>
				<td noWrap="true">保本总收入</td>
			</tr>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:if test="floor(td[5])&lt;floor(td[7]) or string(number(td[7]))='NaN'">
						<xsl:attribute name="style">
							color:red
						</xsl:attribute>
					</xsl:if>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
								<td>
									<xsl:value-of select="."/>
								</td>
								</xsl:when>
								<xsl:when test="position()&gt;=2 and position()&lt;=8">
								<td>
									<xsl:attribute name="align">right</xsl:attribute>
									<xsl:if test="position()=6">
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</xsl:if>
									<xsl:if test="position()!=6 and position()!=7 and position()!=8">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
									<xsl:if test="position()=8">
									<xsl:attribute name="align">right</xsl:attribute>
										<xsl:if test="string(number(.))='NaN'">
											-
										</xsl:if>
										<xsl:if test="string(number(.))!='NaN'">
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</xsl:if>
									</xsl:if>
									<xsl:if test="position()=7">
									<xsl:attribute name="align">right</xsl:attribute>
										<xsl:if test="string(number(.))='NaN'">
											-
										</xsl:if>
										<xsl:if test="string(number(.))!='NaN'">
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</xsl:if>
									</xsl:if>
									</td>
								</xsl:when>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>
