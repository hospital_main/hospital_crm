<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead> 
		<tr noWrap="true" class="mainHead">
			<th noWrap="true">医疗项目</th>
			<th noWrap="true">收入</th>
			<th noWrap="true">数量</th>
			<th noWrap="true">比率</th>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>          
				<xsl:for-each select="td">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<td><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:when test="position()=2">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:when test="position()=3">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,###')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>  	
			<tr>
				<td align="center">合计</td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[2]),'#,##0.00')"/></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),'#,###')"/></td> 
				<td/>
			</tr>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
