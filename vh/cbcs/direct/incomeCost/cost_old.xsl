<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">  		  
			<th noWrap="true">固定资产编码</th>
			<th noWrap="true">固定资产名称</th>
			<th noWrap="true">原值</th>
			<th noWrap="true">折旧额</th>
			<th noWrap="true">启用时间</th>
			<th noWrap="true">折旧年限</th>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>          
				<xsl:for-each select="td">
					<xsl:choose> 
						<xsl:when test="position()=1">
							<td><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:when test="position()=3 or position()=4 or position()=6">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>