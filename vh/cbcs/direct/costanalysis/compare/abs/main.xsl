<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
		     <th noWrap="true">ѡ��</th>
				<th noWrap="true">��Ŀ</th>
				<xsl:variable name="flag" select="count(/root/tbody/tr)" />
				<xsl:if test="$flag > 0">
        				<th noWrap="true">@active_1</th>
      				</xsl:if>
			</tr>
		  </thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:block'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
            <td>
                  <xsl:value-of select="."/>
            </td>
                </xsl:when>
                
                <xsl:otherwise>
            <td align='right'>
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

