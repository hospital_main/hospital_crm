<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/direct/costanalysis/compare/abs/printView.xsl,v 1.1 2012/03/12 01:57:58 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:58 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
	<!--
		<colgroup>		       
	  	   <col style = 'width:100m'/>
	  		<xsl:for-each select="/root/activeHead/cbcsDirectCostAnalysisCompareAbs_Select_head_1/th">
	      <col style = 'width:100m'/>
	      </xsl:for-each>
		</colgroup>
		-->
		<thead>
		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				
				<xsl:for-each select="/root/activeHead/cbcsDirectCostAnalysisCompareAbs_Select_head_1/th">
	      <td style="display:none"></td>
	      </xsl:for-each>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<xsl:for-each select="/root/activeHead/cbcsDirectCostAnalysisCompareAbs_Select_head_1/th">
	      <td style="display:none"></td>
	      </xsl:for-each>
  		</tr>
			<tr noWrap="true" class="mainHead">  
			<td noWrap="true">��Ŀ</td>
		 		<xsl:for-each select="/root/activeHead/cbcsDirectCostAnalysisCompareAbs_Select_head_1/th">
	            <td noWrap="true"><xsl:value-of select="."/></td>
	         </xsl:for-each>
	    </tr>
	    </thead>
	    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td[position()=2]">
						<td><xsl:value-of select="."/>
						</td>
					</xsl:for-each>
					<xsl:for-each select="td[position()>2]">
						<td>
							<xsl:attribute name='class'>numberText</xsl:attribute>
							<xsl:attribute name="style">align:right</xsl:attribute>
							<xsl:value-of select="format-number(.,'#,##0.00')"/>
						</td>	
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
