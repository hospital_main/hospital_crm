<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/direct/costanalysis/subj/compare/printView.xsl,v 1.1 2012/03/12 01:57:58 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:58 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
	<!--
		<colgroup>		       
			<col style = 'width:125mm'/>
			<col style = 'width:95mm'/>	
			<col style = 'width:95mm'/>
			<col style = 'width:95mm'/>	
			<col style = 'width:65mm'/>
			<col style = 'width:95mm'/>
			<col style = 'width:90mm'/>	
			<col style = 'width:65mm'/>
			<col style = 'width:95mm'/>	
			<col style = 'width:95mm'/>
			<col style = 'width:65mm'/>	
			<col style = 'width:95mm'/>
			<col style = 'width:95mm'/>
	    <col style = 'width:65mm'/>
		</colgroup>
		-->
		<thead>
		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
			<tr noWrap="true" class="mainHead">  
				<td noWrap="true" rowspan="2">成本项目</td>
				<td noWrap="true" colspan="1">本期</td>
				<td noWrap="true" colspan="3">预算</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td noWrap="true" colspan="3">上期</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td noWrap="true" colspan="3">同期</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td noWrap="true" colspan="3">平均</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr noWrap="true" class="mainHead">  
				<td style="display:none"></td>
				<td noWrap="true" colspan="1">成本额</td>
				<td noWrap="true" colspan="1">成本额</td>
				<td noWrap="true" colspan="1">差异</td>
				<td noWrap="true" colspan="1">差异率</td>
				<td noWrap="true" colspan="1">成本额</td>
				<td noWrap="true" colspan="1">差异</td>
				<td noWrap="true" colspan="1">差异率</td>
				<td noWrap="true" colspan="1">成本额</td>
				<td noWrap="true" colspan="1">差异</td>
				<td noWrap="true" colspan="1">差异率</td>
				<td noWrap="true" colspan="1">成本额</td>
				<td noWrap="true" colspan="1">差异</td>
				<td noWrap="true" colspan="1">差异率</td>
			</tr>
		</thead>
	  <tbody>
			<xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td[position()!=15]">
	        	<xsl:choose>
							<xsl:when test="position()>=2 and position()!=15">
	            	<td>
									<xsl:attribute name="style">align:right</xsl:attribute>
									<xsl:if test="position() = 2 or ((position()-2) mod 3) != 0">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
									<xsl:if test="position() != 2 and ((position()-2) mod 3) = 0">
										<xsl:if test=". &lt; ../td[15]">
											<xsl:attribute name="style">font-weight:bold;color:orange;align:right</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()>=15">
	        			<td style="display:none">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
	        			</td>
							</xsl:when>
	            <xsl:otherwise>
	            	<td>
	              	<xsl:value-of select="."/>
	            	</td>
	            </xsl:otherwise>
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
	  </tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
