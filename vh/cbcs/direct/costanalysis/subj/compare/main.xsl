<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/direct/costanalysis/subj/compare/main.xsl,v 1.1 2012/03/12 01:57:58 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:58 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap="true" class="mainHead">  
			<th noWrap="true" rowspan="2">选择</th>
			<th noWrap="true" rowspan="2">成本项目</th>
			<th noWrap="true" colspan="1">本期</th>
			<th noWrap="true" colspan="3">预算</th>
			<th noWrap="true" colspan="3">上期</th>
			<th noWrap="true" colspan="3">同期</th>
			<th noWrap="true" colspan="3">平均</th>
		</tr>
		<tr noWrap="true" class="mainHead">  
			<th noWrap="true" colspan="1">成本额</th>
			<th noWrap="true" colspan="1">成本额</th>
			<th noWrap="true" colspan="1">差异</th>
			<th noWrap="true" colspan="1">差异率</th>
			<th noWrap="true" colspan="1">成本额</th>
			<th noWrap="true" colspan="1">差异</th>
			<th noWrap="true" colspan="1">差异率</th>
			<th noWrap="true" colspan="1">成本额</th>
			<th noWrap="true" colspan="1">差异</th>
			<th noWrap="true" colspan="1">差异率</th>
			<th noWrap="true" colspan="1">成本额</th>
			<th noWrap="true" colspan="1">差异</th>
			<th noWrap="true" colspan="1">差异率</th>
		</tr>
	</thead>
	<tbody>
	<xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center' style='display:block'> 
          	 <xsl:if test="position()!=last()">
              <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
                <xsl:attribute name="value" >
                  <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
                </xsl:attribute>
              </input>
             </xsl:if>
       		</td>

          <xsl:for-each select="td[position()!=15]">
              <xsl:choose>
                <xsl:when test="position()=1">
	          		<td>
	  	            	<xsl:value-of select="."/>
            		</td>
                </xsl:when>
                
				<xsl:when test="position()>=2 and position()!=15">
            		<td>
						
							<xsl:attribute name="class">numberText</xsl:attribute>
							<xsl:if test="position() = 2 or ((position()-2) mod 3) != 0">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</xsl:if>
							<xsl:if test="position() != 2 and ((position()-2) mod 3) = 0">
								<xsl:if test=". &gt; ../td[15]">
									<xsl:attribute name="style">font-weight:bold;color:orange</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="format-number(.,'#,##0.00%')"/>
							</xsl:if>
						
            		</td>
				</xsl:when>
				<xsl:when test="position()>=15">
        			<td style="display:none">
						<xsl:value-of select="format-number(.,'#,##0.00')"/>
        			</td>
				</xsl:when>
                <xsl:otherwise>
            		<td>
                  		<xsl:value-of select="."/>
            		</td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>