<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/direct/costanalysis/class/compare/printView.xsl,v 1.1 2012/03/12 01:57:58 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:58 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		
		<thead>
		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
			<tr noWrap="true" class="mainHead">  
				<td noWrap="true" rowspan="2">项目</td>
				<td noWrap="true">本期</td>
				<td noWrap="true" colspan="3">预算</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
		
				<td noWrap="true" colspan="3">上期</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>

				<td noWrap="true" colspan="3">同期</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
		
				<td noWrap="true" colspan="3">平均</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
	

		    </tr>
		    <tr noWrap="true" class="mainHead">  
				<td style="display:none"/>
				<td noWrap="true">成本</td>
			   <td noWrap="true">成本</td>
				<td noWrap="true">差异</td>
		      <td noWrap="true">差异率</td>
			   <td noWrap="true">成本</td>
				<td noWrap="true">差异</td>
		      <td noWrap="true">差异率</td>
			   <td noWrap="true">成本</td>
				<td noWrap="true">差异</td>
				 <td noWrap="true">差异率</td>
             <td noWrap="true">成本</td>
				<td noWrap="true">差异</td>
		      <td noWrap="true">差异率</td>
			</tr>
		</thead>
		  <tbody>
	
	<xsl:variable name="gy" select="/root/annex/is_gy"/>
	<xsl:variable name="yf" select="/root/annex/is_yf"/>
	
	
	<xsl:for-each select="/root/tbody/tr[1]/td">
	<xsl:variable name="td_now" select="position() -2"/>

	<xsl:if test="$td_now>0">
	<xsl:choose>
		<xsl:when test="position()=4 and $gy =0">
		
		</xsl:when>
		
		<xsl:when test="position()=6 and $yf =0">
		
		</xsl:when>
		
		<xsl:otherwise>
			<tr noWrap="true">
		<xsl:choose>
			
			<xsl:when test="$td_now=1">
				<td align='center' bgcolor='yellow'>科室成本分类</td>
			</xsl:when>
			<xsl:when test="$td_now=2">
				
				<td>公用分摊成本</td>
			</xsl:when>
			<xsl:when test="$td_now=3">
				
				<td>管理分摊成本</td>
			</xsl:when>
			<xsl:when test="$td_now=4">
				
				<td>医辅分摊成本</td>
			</xsl:when>
			<xsl:when test="$td_now=5">
				
				<td>医技分摊成本</td>
			</xsl:when>
			<xsl:when test="$td_now=6">
				
				<td align='center' bgcolor='yellow'>成本习性</td>
			</xsl:when>
			<xsl:when test="$td_now=7">
				
				<td>固定成本</td>
			</xsl:when>
			<xsl:when test="$td_now=8">
				
				<td>变动成本</td>
			</xsl:when>
			<xsl:when test="$td_now=9">
				
				<td align='center' bgcolor='yellow'>可控性</td>
			</xsl:when>
			<xsl:when test="$td_now=10">
				
				<td>可控成本</td>
			</xsl:when>
			<xsl:when test="$td_now=11">
				
				<td>不可控成本</td>
			</xsl:when>
			<xsl:when test="$td_now=12">
				
				<td align='center' bgcolor='yellow'>计入方式</td>
			</xsl:when>
			<xsl:when test="$td_now=13">
				
				<td>直接成本</td>
			</xsl:when>
			<xsl:when test="$td_now=14">
				
				<td>间接成本</td>
			</xsl:when>
			<xsl:when test="$td_now=15">
			
				<td align='center' bgcolor='yellow'>成本分类</td>
			</xsl:when>
			<xsl:when test="$td_now=16">
				
				<td>人力成本</td>
			</xsl:when>
			<xsl:when test="$td_now=17">
				
				<td>离退休人员成本</td>
			</xsl:when>
			<xsl:when test="$td_now=18">
				
				<td>折旧成本</td>
			</xsl:when>
			<xsl:when test="$td_now=19">
				
				<td>材料成本</td>
			</xsl:when>
			<xsl:when test="$td_now=20">
				
				<td>药品成本</td>
			</xsl:when>
			<xsl:when test="$td_now=21">
				
				<td>其他成本</td>
			</xsl:when>
			<xsl:when test="$td_now=22">
			
				<td>无形资产摊销</td>
			</xsl:when>
			<xsl:when test="$td_now=23">
			
				<td>提取医疗风险基金</td>
			</xsl:when>
			<xsl:otherwise>
				<td><xsl:value-of select="$td_now"/></td>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:variable name="less_than" select="td[2]"/>
			<td class="numberText"><xsl:value-of select="format-number(td[$td_now+2],'#,##0.00')"/></td>
			<xsl:if test="position()>1">
				<xsl:variable name="td_compare" select="td[$td_now+2] - ../tr[1]/td[$td_now+2]"/>
				<xsl:variable name="td_compare2" select="$td_compare div td[$td_now+2]"/>
				<td class="numberText"><xsl:value-of select="format-number($td_compare,'#,##0.00')"/></td>
				<xsl:element name="td">
					<xsl:if test="td[$td_now+2] != 0">
						<xsl:attribute name="class">numberText<xsl:if test="$less_than &lt; $td_compare2">Warnning</xsl:if></xsl:attribute>
						<xsl:value-of select="format-number($td_compare2,'#,##0.00%')"/>
					</xsl:if>
					<xsl:if test="td[$td_now+2] = 0">
						<xsl:attribute name="class">numberText<xsl:if test="$less_than &lt; 0">Warnning</xsl:if></xsl:attribute>
						<xsl:value-of select="format-number(0,'#,##0.00%')"/>
					</xsl:if>
				</xsl:element>
			</xsl:if>    
		</xsl:for-each>      
	</tr>                    
		</xsl:otherwise>
	</xsl:choose>
	
	</xsl:if>
	</xsl:for-each>

    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>