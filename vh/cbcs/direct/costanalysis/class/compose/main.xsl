<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
		    <xsl:for-each select="/root/tbody/tr[td[1]='']/td">  
					<xsl:choose>
				    <xsl:when test="position()=1 or position()=3">
					    <th style="display:none"><xsl:value-of select="."/></th>
					  </xsl:when>
					  <xsl:when test="position()=2">
					    <th noWrap="true"><xsl:value-of select="."/></th>
					  </xsl:when>
					</xsl:choose>  
			  </xsl:for-each>
				<th noWrap="true">�ɱ�</th>
				<th noWrap="true">����</th>
				<th noWrap="true">����ͼ</th>
			</tr>	
		</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[td[1]!='']">
        <tr>
          <td align='center' style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
              	<xsl:when test="position()=1">
	  	            <xsl:value-of select="."/>
	                </xsl:when>
                <xsl:when test="position()=2">
	                <xsl:attribute name='class'>numberText</xsl:attribute>
	  	            <xsl:value-of select="format-number(.,'##0.00')"/>
	                </xsl:when>
                <xsl:when test="position()=3">
	                <xsl:attribute name='class'>numberText</xsl:attribute>
	  	            <xsl:value-of select="format-number(.,'##0.00%')"/>
	                </xsl:when>
                <xsl:otherwise>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
          <td>
          	<table border="0" height="12"  cellpadding='0' cellspacing='0' >
            	<xsl:attribute name="width"><xsl:value-of select="td[3]*100"/>%</xsl:attribute>
            	<tr><td bgcolor="blue"></td></tr>
            </table> 
          </td>
        </tr>
      </xsl:for-each>
    </tbody>
		</xsl:template>
</xsl:stylesheet>
