<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/direct/targetforecast/experiment/experimentTargetMain.jsp,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,com.viewhigh.cbcs.base.mvc.view.component.Select" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<%String[][] result = (String[][])request.getAttribute("result");%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function account(){
    if(judgeDate()==false){
     return false;
      }
    template.subFunction.value = "account";
    show_wait();
    template.submit();
  }
  function judgeDate(){
     if(template.type.value=="month"){
      if(template.yearMonth.value==""){
        alert("请选择核算时间");
        return false;
      }
    }
    else if(template.type.value=="quarter"){
      if(template.yearQuarter.value==""){
        alert("请选择核算时间");
        return false;
      }
    }
    else{
      if(template.year.value==""){
        alert("请选择核算时间");
        return false;
      }
    }
    return true;
  }
</Script>

<html:html clazz="main">
  <form name="template" method="post" action="directTargetExperiment.jspviewhigh">
    <html:message/>
    <html:title clazz='module'>收益敏感实验</html:title>
    <html:table clazz="simple">
      <tr>
         <td class="normalText"><%String[][] s1={{"month","月"},
                        {"quarter","季"},
                        {"year","年"}
                       };%>
          分析单位：<input:vanish name="type" options="<%=s1%>" cssclass="selectBg">
        </td >
        <td nowrap class="signText" valign="middle">核算时间：</td>
        <td valign="middle"><input:vanishEle >
          <input:yearM name="yearMonth"></input:yearM>
          </input:vanishEle>
          <input:vanishEle >
            <input:yearQ name="yearQuarter" ></input:yearQ>
          </input:vanishEle>
          <input:vanishEle >
            <input:year name="year" ></input:year>
          </input:vanishEle>
          </input:vanish>
        </td>
        <td class="signText" nowrap="nowrap">科室：<%=new Select(request.getAttribute("depts"), "depts", request.getParameter("depts")==null? "":request.getParameter("depts"), false, true)%></td>
		<td colspan="3" nowrap class="signText" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td colspan="4" nowrap class="signText" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		
      </tr>
	  <tr>
	  <td colspan="10" nowrap  align="right"><button class="pageBtn" onclick="return account();">计算</button></td>
	  <td nowrap class="signText"></td>
		
	   
	  </tr>
    </html:table>
    <br>
    <html:title clazz='table'>收益敏感实验</html:title>
    <html:table clazz="complex">
      <!-- 结果集 -->
      <tr>
        <td><html:table clazz="result" id="table">
          <html:tr clazz='label'>
        <td colspan="2">影响收益因素</td>
        <td rowspan="2">变动程度%</td>
        <td colspan="2">影响范围</td>
        <td rowspan="2">增加收入</td>
        <td rowspan="2">降低成本</td>
        <td rowspan="2">增加收益</td>
        <td rowspan="2">影响程度</td>
        </html:tr>
        <html:tr clazz='label'>
          <td class="resultLabelTT" >因素</td>
          <td class="resultLabelTT" >基础数据</td>
          <td class="resultLabelTT" >业务收入</td>
          <td class="resultLabelTT" >医疗成本</td>
        </html:tr>
        </html:table>
        </td>
      </tr>
    </html:table>
    <input type='hidden' name="subFunction" value="findByCondition"/>
  </form>
</html:html>
