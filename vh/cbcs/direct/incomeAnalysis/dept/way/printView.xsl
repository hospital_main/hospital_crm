<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<!--
<colgroup>		 
	  	  <xsl:for-each select="/root/tbody/tr[td[1]='科室']/td">
	  	    <col style = 'width:180mm'/>
	      </xsl:for-each>
		</colgroup>
		-->
		<thead>
		
		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				
				<xsl:for-each select="/root/tbody/tr[td[1]='科室']/td[position()>=2]">
	  	    <td style="display:none"></td>
	      </xsl:for-each>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<xsl:for-each select="/root/tbody/tr[td[1]='科室']/td[position()>=2]">
	  	    <td style="display:none"></td>
	      </xsl:for-each>
  		</tr>
			<tr noWrap="true" class="mainHead"> 
				<xsl:for-each select="/root/tbody/tr[td[1]='科室']/td">
					<xsl:choose>
                <xsl:when test="position()=1">
					    		<td noWrap="true" rowspan="2" width="82"><xsl:value-of select="."/></td>
					    	</xsl:when>
					    	<xsl:when test="position()=2">
					    		<td noWrap="true"><xsl:value-of select="format-number(.,'###0')"/></td>
					    		
					    	</xsl:when>
					    	<xsl:when test="(position() mod 2)=1">
					    		<td noWrap="true" colspan="2"><xsl:value-of select="format-number(.,'###0')"/></td>
					    		<td noWrap="true" style="display:none"><xsl:value-of select="format-number(.,'###0')"/></td>
					    	</xsl:when>              	
              		</xsl:choose>
	    	</xsl:for-each>
	    </tr>
	    <tr noWrap="true" class="mainHead">  
				<xsl:for-each select="/root/tbody/tr[td[1]='科室']/td">
						<xsl:choose>		
                			<xsl:when test="position()=1">
					    			<td noWrap="true"></td>
					    	</xsl:when>
					    	<xsl:when test="position()=2">
					    		<td noWrap="true">收入</td>
					    	</xsl:when>   
					    	<xsl:when test="(position() mod 2)=1">
					    		<td noWrap="true">收入</td>
					    		<td noWrap="true">比值</td>
					    	</xsl:when>           	
              </xsl:choose>
	    	</xsl:for-each>
	    </tr>
		</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr[td[1]!='科室']">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td>
  	                <xsl:value-of select="."/>
  	              </td>
                </xsl:when>
                <xsl:when test="position()=2">
                  <td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:when test="(position() mod 2)=1">
                  <td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                 </xsl:when>
                <xsl:otherwise>
                  <td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>

    </tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
