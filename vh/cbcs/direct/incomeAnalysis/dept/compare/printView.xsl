<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/direct/incomeAnalysis/dept/compare/printView.xsl,v 1.1 2012/03/12 01:57:58 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:58 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		
		<thead>
		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
			<tr noWrap="true" class="mainHead">  
				<td noWrap="true" rowspan="2">科室</td>
				<td noWrap="true">本期</td>
				<td noWrap="true" colspan="3">预算</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
		
				<td noWrap="true" colspan="3">上期</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>

				<td noWrap="true" colspan="3">同期</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
		
				<td noWrap="true" colspan="3">平均</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
	

		    </tr>
		    <tr noWrap="true" class="mainHead">  
				<td style="display:none"/>
				<td noWrap="true">收入</td>
			   <td noWrap="true">收入</td>
				<td noWrap="true">差异</td>
		      <td noWrap="true">差异率</td>
			   <td noWrap="true">收入</td>
				<td noWrap="true">差异</td>
		      <td noWrap="true">差异率</td>
			   <td noWrap="true">收入</td>
				<td noWrap="true">差异</td>
				 <td noWrap="true">差异率</td>
             <td noWrap="true">收入</td>
				<td noWrap="true">差异</td>
		      <td noWrap="true">差异率</td>
			</tr>
		</thead>
		  <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
          <xsl:for-each select="td[position()!=15]">
            <td>
              <xsl:choose>
                <xsl:when test="position()=1">
                  <a tabindex='-1'>
  	            <xsl:value-of select="."/></a>
                </xsl:when>
                
 								<xsl:when test="(position()>1 and ((position()-2) mod 3)!=0 ) or position()=2 ">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
 								<xsl:when test="position()>3 and ((position()-2) mod 3)=0">
									<xsl:attribute name="class">numberText<xsl:if test=". &lt; ../td[15]">Warnning</xsl:if></xsl:attribute>
								<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									
								</xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>

	</root>
	</xsl:template>
</xsl:stylesheet>
