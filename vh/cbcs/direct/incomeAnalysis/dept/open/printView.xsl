<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/direct/incomeAnalysis/dept/open/printView.xsl,v 1.1 2012/03/12 01:57:58 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:58 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<thead>
		<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true">开单科室</td>
				<td noWrap="true">执行科室</td>
				<td noWrap="true">收入</td>
				<td noWrap="true">收入比(%)</td>
	    </tr>
		</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position()=1 or position()=2">
	                </xsl:when>
	                <xsl:when test="position()=5">
	               <td class="numberText" align="right"><xsl:value-of select="."/></td>
	                </xsl:when>
	                 <xsl:when test="position()=6">
	               <td class="numberText" align="right"><xsl:value-of select="format-number(.'#,##0.00')"/>%</td>
	                </xsl:when>
                <xsl:otherwise>
                   <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>

	</root>
	</xsl:template>
</xsl:stylesheet>
