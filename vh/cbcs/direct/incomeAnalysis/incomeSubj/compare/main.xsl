<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
		    <th noWrap="true" rowspan="2">选择</th>
				<xsl:for-each select="/root/tbody/tr[td[1]='']/td">  
					<xsl:choose>
					  <xsl:when test="position()=2">
					    <th noWrap="true" rowspan="2"><xsl:value-of select="."/></th>
					  </xsl:when>
					  <xsl:otherwise>
					    <th style="display:none" rowspan="2"><xsl:value-of select="."/></th>
					  </xsl:otherwise>
				  </xsl:choose>  
			  </xsl:for-each>
				<th noWrap="true" colspan="1">本期</th>
				<th noWrap="true" colspan="3">预算</th>
				<th noWrap="true" colspan="3">上期</th>
				<th noWrap="true" colspan="3">同期</th>
				<th noWrap="true" colspan="3">平均</th>
			</tr>
		  <tr noWrap="true" class="mainHead">  
				<th noWrap="true" colspan="1">收入</th>
			  <th noWrap="true" colspan="1">收入</th>
				<th noWrap="true" colspan="1">差异</th>
		    <th noWrap="true" colspan="1">差异率</th>
			  <th noWrap="true" colspan="1">收入</th>
				<th noWrap="true" colspan="1">差异</th>
		    <th noWrap="true" colspan="1">差异率</th>
			  <th noWrap="true" colspan="1">收入</th>
				<th noWrap="true" colspan="1">差异</th>
				<th noWrap="true" colspan="1">差异率</th>
        <th noWrap="true" colspan="1">收入</th>
				<th noWrap="true" colspan="1">差异</th>
		    <th noWrap="true" colspan="1">差异率</th>
			</tr>
		</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr[td[1]!='']">
        <tr>
          <td align='center'  style='display:block'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td[position()!=1 and position()!=16]">
            <td>
              <xsl:choose>
                <xsl:when test="position()=1">
                  <a tabindex='-1'>
  	            <xsl:value-of select="."/></a>
                </xsl:when>
                
 								<xsl:when test="(position()>1 and ((position()-2) mod 3)!=0 ) or position()=2 ">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
 								<xsl:when test="position()>3 and ((position()-2) mod 3)=0">
									<xsl:attribute name="class">numberText<xsl:if test=". &lt; ../td[16]">Warnning</xsl:if></xsl:attribute>
						  	<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									
								</xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

