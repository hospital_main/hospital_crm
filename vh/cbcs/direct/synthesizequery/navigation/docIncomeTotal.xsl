<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
				<th noWrap="true" rowspan ='2'>收入项目</th>
				<th noWrap="true" colspan = '4'>总收入</th>
				<th noWrap="true"  colspan = '2'>单位收入</th>

  		</tr>
  		 <tr class="mainHead">
				<th noWrap="true">合计</th>
				<th noWrap="true">占比</th>
				<th noWrap="true">开单收入</th>
				<th noWrap="true">执行收入</th>
				<th noWrap="true">诊次收入</th>
				<th noWrap="true">床日收入</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position()=1 ">
              		 <td>
			<a href="#">
				<xsl:attribute name="onclick">
					openDialog('docIncomedetail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:1024px;dialogHeight:768px')
				</xsl:attribute>
				<xsl:value-of select="."/>
                   	</a>
                  </td>
              	</xsl:when>
              	
              	<xsl:when test="position()=3 ">
              		 <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
              	</xsl:when>
                <xsl:otherwise>
                  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  </xsl:template>
</xsl:stylesheet>
