<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
				<th noWrap="true" >收费项目</th>
				<th noWrap="true" > 开单科室</th>
				<th noWrap="true" >  执行科室</th>
				<th noWrap="true" >  项目数量</th>
				<th noWrap="true" > 收入金额</th>
				<th noWrap="true" >  占比(%)</th>
  		</tr>
  		 
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
				<xsl:variable name="trnum" select="position()" />
				<xsl:variable name="charge_detail_name" select="td[position()=1]"/>
          <xsl:for-each select="td">
              <xsl:choose>
              	 <xsl:when test="position()=4 or position()=5">
                  <td align="right">
                  	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                 </xsl:when>
                  <xsl:when test="position()=6">
                  <td align="right">
                  	<xsl:value-of select="format-number(.,'#,##0.00')"/>%
                  </td>
                 </xsl:when>
                <xsl:when test="position()=1">
                  <td>
										<a href="#">
											<xsl:attribute name="onclick">
												   openDialog('doc_detail_main.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;charge_detail_name&gt;<xsl:value-of select='$charge_detail_name'/>&lt;/charge_detail_name&gt;&lt;flag&gt;aa&lt;/flag&gt;', 'dialogWidth:1024px;dialogHeight:768px')
											</xsl:attribute>
										<xsl:value-of select="."/>
                   	</a>
                  </td>
                </xsl:when>
                
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  </xsl:template>
</xsl:stylesheet>
