<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead> 
		<tr noWrap="true" class="mainHead">
			<th noWrap="true">年月</th>
			<th noWrap="true">开单时间</th>
			<th noWrap="true">收费项目</th>
			<th noWrap="true">开单科室</th>
			<th noWrap="true">开单医生</th>
			<th noWrap="true">执行科室</th> 
			<th noWrap="true">执行医生</th>
			<th noWrap="true">数量</th>
			<th noWrap="true">单价</th>
			<th noWrap="true">收入金额</th>
			<th noWrap="true">病人姓名</th>  
			<th noWrap="true">病历/住院号</th>  
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>          
				<xsl:for-each select="td">
					<xsl:choose> 

						<xsl:when test="position()=8 or position()=9 or position()=10 ">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>  	
	</tbody>
	</xsl:template>
</xsl:stylesheet>
