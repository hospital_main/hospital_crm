<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
				<th noWrap="true" rowspan ='2'>医生姓名</th>
				<th noWrap="true" rowspan ='2'>职称</th>
				<th noWrap="true" rowspan ='2'>总计</th>
				<th noWrap="true" colspan = '3'>开单收入</th>
				<th noWrap="true"  colspan = '3'>执行收入</th>
				<th noWrap="true"  colspan = '2'>诊次收入</th>
				<th noWrap="true"  colspan = '2'>床日收入</th>
  		</tr>
  		 <tr class="mainHead">
				<th noWrap="true" style ='display:none'></th>
				<th noWrap="true" style ='display:none'></th>
				<th noWrap="true" style ='display:none'></th>
				<th noWrap="true">合计</th>
				<th noWrap="true">门诊</th>
				<th noWrap="true">住院</th>
				<th noWrap="true">合计</th>
				<th noWrap="true">门诊</th>
				<th noWrap="true">住院</th>
				<th noWrap="true">门诊人次</th>
				<th noWrap="true">诊次收入</th>
				<th noWrap="true">床日数</th>
				<th noWrap="true">床日收入</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
				<xsl:variable name="trnum" select="position()" />
				<xsl:variable name="doc_name" select="td[position()=1]"/>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position()=1 or position()=2">
              		 <td><xsl:value-of select="."/></td>
              	</xsl:when>
              	<xsl:when test="position()=3">
              	</xsl:when>
              	<xsl:when test="position()=4 and $trnum=1">
                  <td  align="right">
			<a href="#">
				<xsl:attribute name="onclick">
					openDialog('docIncomeTotal.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;doc_name&gt;<xsl:value-of select='$doc_name'/>&lt;/doc_name&gt;', 'dialogWidth:1024px;dialogHeight:768px')
				</xsl:attribute>
			<xsl:value-of select="format-number(.,'#,##0.00')"/>
	 		</a>
                  </td>
                </xsl:when>
                <xsl:when test="position()=4 and $trnum!=1">
                  <td  align="right">
										<a href="#">
											<xsl:attribute name="onclick">
												  openDialog('docIncomeSubj.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;doc_name&gt;<xsl:value-of select='$doc_name'/>&lt;/doc_name&gt;', 'dialogWidth:1024px;dialogHeight:768px')
											</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
                   	</a>
                  </td>
                </xsl:when>
                <xsl:when test="position()=15 or position()=16 or position()=17 or position()=18 or position()=19 or position()=20 or position()=21">
                	<td style="display:none"><xsl:value-of select="."/></td>
              	</xsl:when>
                <xsl:otherwise>
                  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  </xsl:template>
</xsl:stylesheet>
