<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/direct/unitanalysis/average/compare/main.xsl,v 1.1 2012/03/12 01:57:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:59 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
		     <th noWrap="true" rowspan="2">选择</th>
				<th noWrap="true" rowspan="2">项目</th>
				<th noWrap="true" colspan="1">本期</th>
				<th noWrap="true" colspan="3">预算</th>
				<th noWrap="true" colspan="3">上期</th>
				<th noWrap="true" colspan="3">同期</th>
				<th noWrap="true" colspan="3">平均</th>

		    </tr>
		    <tr noWrap="true" class="mainHead">  
				<th noWrap="true" colspan="1">数值</th>
			   <th noWrap="true" colspan="1">数值</th>
				<th noWrap="true" colspan="1">差异</th>
		      <th noWrap="true" colspan="1">差异率</th>
			   <th noWrap="true" colspan="1">数值</th>
				<th noWrap="true" colspan="1">差异</th>
		      <th noWrap="true" colspan="1">差异率</th>
			   <th noWrap="true" colspan="1">数值</th>
				<th noWrap="true" colspan="1">差异</th>
				 <th noWrap="true" colspan="1">差异率</th>
             <th noWrap="true" colspan="1">数值</th>
				<th noWrap="true" colspan="1">差异</th>
		      <th noWrap="true" colspan="1">差异率</th>
			</tr>
		</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:block'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td[position()!=15]">
            <td>
              <xsl:choose>
                <xsl:when test="position()=1">
									<xsl:if test="../td[1]='住院' or ../td[1]='门诊' or ../td[1]='医技' or ../td[1]='每职工'">
                  	<xsl:attribute name="bgColor">yellow</xsl:attribute>
									</xsl:if>
	  	            <xsl:value-of select="."/>
                </xsl:when>
                
								<xsl:when test="position()=2">
									<xsl:if test="../td[1]!='住院' and ../td[1]!='门诊' and ../td[1]!='医技' and ../td[1]!='每职工'">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>

 								<xsl:when test="(position() &lt; 15 and ((position()-2) mod 3)!=0)">
									<xsl:if test="../td[1]!='住院' and ../td[1]!='门诊' and ../td[1]!='医技' and ../td[1]!='每职工'">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
								
 								<xsl:when test="(position()>14 and ((position()-2) mod 3)!=0)">
									<xsl:if test="../td[1]!='住院' and ../td[1]!='门诊' and ../td[1]!='医技' and ../td[1]!='每职工' and ../td[1]!='诊次数量' and ../td[1]!='床日数量' and ../td[1]!='单位数量' and ../td[1]!='职工人数'">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
								
 								<xsl:when test="position()>14 and ((position()-2) mod 3)=0">
									<xsl:if test="../td[1]!='住院' and ../td[1]!='门诊' and ../td[1]!='医技' and ../td[1]!='每职工' and ../td[1]!='诊次数量' and ../td[1]!='床日数量' and ../td[1]!='单位数量' and ../td[1]!='职工人数'">
										<xsl:attribute name="class">numberText<xsl:if test=". &lt; ../td[15]">Warnning</xsl:if></xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</xsl:if>
								</xsl:when>

 								<xsl:when test="position()>3 and ((position()-2) mod 3)=0 and position() &lt; 15">
									<xsl:if test="../td[1]!='住院' and ../td[1]!='门诊' and ../td[1]!='医技' and ../td[1]!='每职工'">
										<xsl:attribute name="class">numberText<xsl:if test=". &lt; ../td[15]">Warnning</xsl:if></xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</xsl:if>
								</xsl:when>
 								
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

