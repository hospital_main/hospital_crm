<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
		<xsl:variable name="BtrValue" select="/root/tbody/tr[td[3]='成本收益率']/td[1]"/>
			<tr noWrap="true" class="mainHead">  
				<th rowspan="2">选择</th>
				<xsl:for-each select="/root/tbody/tr[td[3]='项目']/td">
						<xsl:choose>
						  <xsl:when test="position()=1 or position()=2">
						    <th style="display:none" rowspan="2"><xsl:value-of select="."/></th>
						  </xsl:when>
						  <xsl:when test="position()=3">
				    		<th noWrap="true" rowspan="2"><xsl:value-of select="."/></th>
				    	</xsl:when>
				    	<xsl:when test="position()=4">
				    		<th noWrap="true"><xsl:value-of select="format-number(.,'###0')"/></th>
				    	</xsl:when>
				    	<xsl:when test="(position() mod 2)=1">
				    		<th noWrap="true" colspan="2"><xsl:value-of select="format-number(.,'###0')"/></th>
				    		<th noWrap="true" style="display:none"><xsl:value-of select="."/></th>
				    	</xsl:when>              	
            </xsl:choose>
	    	</xsl:for-each>
	    </tr>
	    <tr noWrap="true" class="mainHead">  
	    	<th style="display:none"></th>
				<xsl:for-each select="/root/tbody/tr[td[3]='项目']/td">
						<xsl:choose>
						  <xsl:when test="position()=1 or position()=2 or position()=3">
				    		<th noWrap="true" style="display:none"></th>
				    	</xsl:when>
				    	<xsl:when test="position()=4">
				    		<th noWrap="true">数值</th>
				    	</xsl:when>   
				    	<xsl:when test="(position() mod 2)=1">
				    		<th noWrap="true">数值</th>
				    		<th noWrap="true">百分比</th>
				    	</xsl:when>           	
            </xsl:choose>
	    	</xsl:for-each>
	    </tr>
		</thead>
		<tbody>
	
      <xsl:for-each select="/root/tbody/tr[td[3]!='项目']">
        <tr>
          <xsl:if test="td[2]='yes'">
            <td align='center' style='display:block'>    
            </td>
          </xsl:if>
          <xsl:if test="td[2]='no'">
            <td align='center' style='display:block'>
              <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
                <xsl:attribute name="value" >
                  <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
                </xsl:attribute>
              </input>
            </td>
          </xsl:if>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1 or position()=2">
                <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position()=3">
                <xsl:if test="../td[2]='yes'">
                  <td align="center">
                    	<xsl:attribute name="bgColor">yellow</xsl:attribute>
											<xsl:value-of select="."/>
                  </td>
                </xsl:if>
                <xsl:if test="../td[2]='no'">
                  <td><xsl:value-of select="."/></td>
                </xsl:if>
              </xsl:when>
              <xsl:when test="position()=4">
               <xsl:if test="../td[2]='no'">    
	               	<xsl:if test="../td[3]='药占比'">
	                 <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
	                </xsl:if>
	                <xsl:if test="starts-with(../td[1],$BtrValue)">
	                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
					</xsl:if>
					<xsl:if test="../td[3]!='药占比' and not(starts-with(../td[1],$BtrValue))">
	                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
					</xsl:if>
                </xsl:if>
                <xsl:if test="../td[2]='yes'">
                  <td></td>
                </xsl:if>
              </xsl:when>
              <xsl:when test="(position() mod 2)=1">
               <xsl:if test="../td[2]='no'">
               	<xsl:if test="../td[3]='药占比'">
                 <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
                </xsl:if>
                <xsl:if test="starts-with(../td[1],$BtrValue)">
	              <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
				</xsl:if>
				<xsl:if test="../td[3]!='药占比' and not(starts-with(../td[1],$BtrValue))">
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				</xsl:if>
                </xsl:if>
                <xsl:if test="../td[2]='yes'">
                  <td></td>
                </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <xsl:if test="../td[2]='no'">
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
                </xsl:if>
                <xsl:if test="../td[2]='yes'">
                  <td></td>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

