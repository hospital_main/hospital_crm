<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/direct/profitanalysis/direct/compose/printView.xsl,v 1.1 2012/03/12 01:57:58 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:58 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	
	<root>
	<!--
		<colgroup>		       
			<col style = 'width:100mm'/>	
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>		
		</colgroup>
		-->
		<thead>
		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
			<tr noWrap="true" class="mainHead">  
				<td noWrap="true"><xsl:value-of select="/root/activeHead/cbcsDirectProfitAnalysisDirectCompose_Select_head_1/th"/></td>
				<td noWrap="true">����</td>
				<td noWrap="true">����</td>
			</tr>
		</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
              	<xsl:when test="position()=1">
	  	            <xsl:value-of select="."/>
	                </xsl:when>
                <xsl:when test="position()=2">
	                <xsl:attribute name='class'>numberText</xsl:attribute>
	                <xsl:attribute name="style">align:right</xsl:attribute>
	  	            <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </xsl:when>
                <xsl:when test="position()=3">
	                <xsl:attribute name='class'>numberText</xsl:attribute>
	                <xsl:attribute name="style">align:right</xsl:attribute>
	  	            <xsl:value-of select="format-number(.,'##0.00%')"/>
	                </xsl:when>
                <xsl:otherwise>
                 <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
		
	</root>
	</xsl:template>
</xsl:stylesheet>
