<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
		    <th noWrap="true" rowspan="2">选择</th>
				<th noWrap="true" rowspan="2">项目</th>
				<th noWrap="true" >本期</th>
				<th noWrap="true" colspan="3">预算</th>
				<th style='display:none'></th>
				<th style='display:none'></th>
				<th noWrap="true" colspan="3">上期</th>
				<th style='display:none'></th>
				<th style='display:none'></th>
				<th noWrap="true" colspan="3">同期</th>
				<th style='display:none'></th>
				<th style='display:none'></th>
				<th noWrap="true" colspan="3">平均</th>
				<th style='display:none'></th>
				<th style='display:none'></th>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
		    <th style='display:none'></th>
		    <th style='display:none'></th>
				<th noWrap="true" colspan="1">数值</th>
			  <th noWrap="true" colspan="1">数值</th>
				<th noWrap="true" colspan="1">差异</th>
		    <th noWrap="true" colspan="1">差异率</th>
			  <th noWrap="true" colspan="1">数值</th>
				<th noWrap="true" colspan="1">差异</th>
		    <th noWrap="true" colspan="1">差异率</th>
			  <th noWrap="true" colspan="1">数值</th>
				<th noWrap="true" colspan="1">差异</th>
				<th noWrap="true" colspan="1">差异率</th>
        <th noWrap="true" colspan="1">数值</th>
				<th noWrap="true" colspan="1">差异</th>
		    <th noWrap="true" colspan="1">差异率</th>
			</tr>
		</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:if test="td[2]='yes'">
            <td align='center' style='display:block'></td>
          </xsl:if>
          <xsl:if test="td[2]='no'">
            <td align='center' style='display:block'>
              <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
                <xsl:attribute name="value" >
                  <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
                </xsl:attribute>
              </input>
            </td>
          </xsl:if>
          <xsl:for-each select="td[position()!=17]">
            <xsl:choose>
              <xsl:when test="position()=1 or position()=2">
                <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position()=3">
                <xsl:if test="../td[2]='yes'">
                  <td align="center"><xsl:attribute name="bgColor">yellow</xsl:attribute><xsl:value-of select="."/></td>
                </xsl:if>
                <xsl:if test="../td[2]='no'">
                  <td><xsl:value-of select="."/></td>
                </xsl:if>
              </xsl:when>
              <xsl:when test="position()=7 or position()=10 or position()=13 or position()=16">
                <xsl:choose>
                  <xsl:when test="../td[2]='yes'">
                    <td></td>
                  </xsl:when>
                  <xsl:when test="../td[2]='no' and . &lt; ../td[17]">
                    <td class="numberText">
                      <xsl:attribute name="style">font-weight:bold;color:orange</xsl:attribute>
                      <xsl:value-of select="format-number(.,'#,##0.00%')"/>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td class="numberText">
                      <xsl:value-of select="format-number(.,'#,##0.00%')"/>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:if test="../td[2]='no'">
                 <xsl:if test="../td[3]='药占比'">
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
                 </xsl:if>
                 <xsl:if test="contains(../td[3],'率')">
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
                 </xsl:if>
                 <xsl:if test="../td[3]!='药占比' and not(contains(../td[3],'率'))">
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                 </xsl:if>
                </xsl:if>
                <xsl:if test="../td[2]='yes'">
                  <td></td>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
