<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
	<!--
		<colgroup>		       
			<col style = 'width:160mm'/>
			<col style = 'width:95mm'/>	
			<col style = 'width:95mm'/>
			<col style = 'width:95mm'/>	
			<col style = 'width:65mm'/>
			<col style = 'width:95mm'/>
			<col style = 'width:90mm'/>	
			<col style = 'width:65mm'/>
			<col style = 'width:95mm'/>	
			<col style = 'width:95mm'/>
			<col style = 'width:65mm'/>	
			<col style = 'width:95mm'/>
			<col style = 'width:95mm'/>
	    <col style = 'width:65mm'/>
		</colgroup>
		-->
		<thead>
		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
			<tr noWrap="true" class="mainHead">  
				<td noWrap="true" rowspan="2">项目</td>
				<td noWrap="true">本期</td>
				<td noWrap="true" colspan="3">预算</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">上期</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">同期</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" colspan="3">平均</td>
				<td noWrap="true" style="display:none"></td>
				<td noWrap="true" style="display:none"></td>
		  </tr>
		  <tr noWrap="true" class="mainHead">  
				<td style="display:none"/>
				<td noWrap="true">数值</td>
			  <td noWrap="true">数值</td>
				<td noWrap="true">差异</td>
		    <td noWrap="true">差异率</td>
			  <td noWrap="true">数值</td>
				<td noWrap="true">差异</td>
		    <td noWrap="true">差异率</td>
			  <td noWrap="true">数值</td>
				<td noWrap="true">差异</td>
				<td noWrap="true">差异率</td>
        <td noWrap="true">数值</td>
				<td noWrap="true">差异</td>
		    <td noWrap="true">差异率</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
					<xsl:for-each select="td[position()!=1 and position()!=2 and position()!=3 and position()!=18]">
            <xsl:choose>
              <xsl:when test="position()=1">
                <xsl:if test="../td[3]='yes'">
                  <td align="center"><xsl:value-of select="."/></td>
                </xsl:if>
                <xsl:if test="../td[3]='no'">
                  <td><xsl:value-of select="."/></td>
                </xsl:if>
              </xsl:when>
              <xsl:when test="position()=5 or position()=8 or position()=11 or position()=14 or position()=17">
                <xsl:choose>
                  <xsl:when test="../td[3]='yes'">
                    <td></td>
                  </xsl:when>
                  <xsl:when test="../td[3]='no' and . &lt; ../td[18]">
                    <td class="numberText">
                      <xsl:attribute name="style">font-weight:bold;color:orange</xsl:attribute>
                      <xsl:attribute name="style">align:right</xsl:attribute>
                      <xsl:value-of select="format-number(.,'#,##0.00%')"/>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td class="numberText" align="right">
                      <xsl:value-of select="format-number(.,'#,##0.00%')"/>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:if test="../td[3]='no'">
                  <xsl:if test="../td[1] &lt;= 3">
                  <td class="numberText"  align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                  </xsl:if>
                  <xsl:if test="../td[1] &gt; 3">
                  <td class="numberText"  align="right"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
                  </xsl:if>
                </xsl:if>
                <xsl:if test="../td[3]='yes'">
                  <td></td>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
