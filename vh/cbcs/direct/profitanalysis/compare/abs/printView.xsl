<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/direct/profitanalysis/compare/abs/printView.xsl,v 1.2 2015/04/25 11:12:26 yaoqingsong Exp $
 $Author: yaoqingsong $
 $Date: 2015/04/25 11:12:26 $
 $Revision: 1.2 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
	<!--
		<colgroup>		       
	  	   <col style = 'width:100m'/>
	  		<xsl:for-each select="/root/activeHead/cbcsDirectProfitAnalysisCompareAbs_Select_head_1/th">
	      <col style = 'width:100m'/>
	      </xsl:for-each>
		</colgroup>
		-->
		<thead>
		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"></td>
				<xsl:for-each select="/root/activeHead/cbcsDirectProfitAnalysisCompareAbs_Select_head_1/th">
	      <td style="display:none"></td>
	      </xsl:for-each>
  		</tr>
  		<tr noWrap="true" class="mainHead">  		  
				<td noWrap="true" style="fontsize:subtitle;colspan:colcount"></td>
				<xsl:for-each select="/root/activeHead/cbcsDirectProfitAnalysisCompareAbs_Select_head_1/th">
	      <td style="display:none"></td>
	      </xsl:for-each>
  		</tr>
			<tr noWrap="true" class="mainHead">  
			<td noWrap="true">��Ŀ</td>
		 		<xsl:for-each select="/root/activeHead/cbcsDirectProfitAnalysisCompareAbs_Select_head_1/th">
	            <td noWrap="true"><xsl:value-of select="."/></td>
	         </xsl:for-each>
	    </tr>
	    </thead>
	    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>          
          	<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=2">
	                <td>
		                <xsl:value-of select="."/>
	                </td>
                </xsl:when>
                <xsl:when test="../td[1] &gt; '05'">
									<td class="numberText" style="align:right">
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</td>
								</xsl:when>
                <xsl:otherwise>
			            <td>
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:attribute name="style">align:right</xsl:attribute>
      	            <xsl:value-of select="format-number(.,'#,##0.00')"/>
        			    </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
