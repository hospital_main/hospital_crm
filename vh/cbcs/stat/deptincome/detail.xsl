<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th nowrap='true'>序号</th>
				<th nowrap='true'>医疗项目</th>
				<th nowrap='true'>收入</th>
				<th nowrap='true'>划入收入</th>
				<th nowrap='true'>序号</th>
				<th nowrap='true'>成本项目</th>
				<th nowrap='true'>可控成本</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
         

          <xsl:for-each select="td">
            <xsl:choose>
              
              <xsl:when test="position() = 4">
 		          	<td align='center'>
 		          		  <input type='text' name='para_value1' class='inputTextA' >
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
									</input>
 		          	</td>
              </xsl:when>
             

						<xsl:when test="position() = 7">
 		          	<td align='center'><div>
 		          		  <input type='text' name='para_value3' class='inputTextA' >
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
									</input>
 		          	</div></td>
              </xsl:when>
					
              <xsl:otherwise>
                <td  width="102"><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

