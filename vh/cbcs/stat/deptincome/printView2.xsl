<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<root>
		<colgroup>		       
	  	   	<col style = 'width:150m'/>
	  	   	<col style = 'width:120m'/>
	  	   	<col style = 'width:120m'/>
	  	   	<col style = 'width:120m'/>
	  	   	<col style = 'width:120m'/>
	  	   	<col style = 'width:120m'/>
	  	   	<col style = 'width:120m'/>
		</colgroup>
		<thead>       
					<tr noWrap='true'>
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
				
				</tr>
					<tr noWrap='true'>
					<td style="colspan:colcount;fontsize:subtitle"></td>
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
				
				</tr>
		  <tr noWrap="true" class="mainHead"> 
				<td noWrap="true" >序号</td>
				<td noWrap="true">医疗项目</td>
				<td noWrap="true">收入</td>
				<td noWrap="true">划入收入</td>
				<td noWrap="true">序号</td>
				<td noWrap="true">成本项目</td>
				<td noWrap="true">可控成本</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test="position()=1">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()=2">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test=" position()=3">
								<td align="right">
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
								</xsl:when>
								<xsl:when test="position()=4">
								<td align="right">
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
								</td>	
								</xsl:when>
								<xsl:when test="position()=5">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()=6">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()=7">
								<td align="right">
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>	
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
				
			</xsl:for-each> 
			 	<!--<tr>
					<td>合计</td>
					<td></td>
					<td><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),'#,##0.00')"/></td>
					<td><xsl:value-of select="format-number(sum(/root/tbody/tr/td[4]),'#,##0.00')"/></td>
					<td></td>
					<td></td>
					<td><xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/></td>
				</tr>-->
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
