<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
		<tr noWrap='true' class='mainHead'>
		<th nowrap='true' width='10px'><span><input type='checkbox' id="first">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span>
		</th>
		<th noWrap="true">收费类别编码</th>
		<th noWrap="true">收费类别名称</th>
		<th nowrap='true'>参与计算</th>
		<th noWrap="true">本科开单本科执行比例</th>
		<th noWrap="true">本科开单他科执行比例</th>
		<th noWrap="true">本科执行他科开单比例</th>
     </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <td align='center'>
            <div><input type='checkbox'>
	            <xsl:attribute name="id">first_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;					</xsl:for-each>
        	    </xsl:attribute>
     			  </input></div>
     			</td>

          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
              	<td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position() = 4">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">iscalc_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					             <xsl:attribute name="onclick">readClick("<xsl:value-of select='../td[1]'/>",this,"4")</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 5">
 		          	<td align='center'>
 		          		  <input type='text' name='para_value1' class='inputTextA' >
 		          		  
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>
									</input>
 		          	</td>
              </xsl:when>
              <xsl:when test="position() = 6">
 		          	<td align='center'><div>
 		          		  <input type='text' name='para_value2' class='inputTextA' >
 		          		 
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>
									</input>
 		          	</div></td>
              </xsl:when>

						<xsl:when test="position() = 7">
 		          	<td align='center'><div>
 		          		  <input type='text' name='para_value3' class='inputTextA' >
 		          		
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>
									</input>
 		          	</div></td>
              </xsl:when>
					
              <xsl:otherwise>
                <td  width="110"><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</xsl:template>
</xsl:stylesheet>


