<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="lzk_hc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<tr>
			<th style="background-color:#FCB724;font-weight: bold;font-family:'宋体'"  align="center" rowspan="2" class="tabLTC">成本分类</th>
			<th style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center">
				<xsl:attribute name="colspan"><xsl:value-of select="$lzk_hc "/></xsl:attribute>资源动因</th>
			<xsl:for-each select="/root/tbody/tr[position() &lt; $lzk_hc]">
				<th style="display:none"></th>
			</xsl:for-each>
		</tr>
		<tr>
			<th align="center" style="display:none">科室</th>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $lzk_hc]">
				<th style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center">
					<xsl:value-of select="td[2]"/>
				</th>
			</xsl:for-each>
		</tr>
	</thead>
	<tbody>
		<xsl:variable name="lzk_bc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<xsl:variable name="lzk_opt" select="/root/tbody/tr[1]/td[5]"/>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:if test="td[8]='Y'">
				<xsl:if test="( position() mod $lzk_bc )=1 or $lzk_bc = 1">
					<xsl:text disable-output-escaping="yes">
						&lt;tr&gt;
					</xsl:text>
					<td style="background-color:#FCB724;font-family:'宋体'" class="tabL">
						<xsl:value-of select="td[1]"/>
					</td>
				</xsl:if>
				<td align="center">
					<select style="z-index: -1; position: relative ; width:95%;">
						<!--onchange="change_res_code(this)"-->
						<xsl:attribute name="set_cfg_code"><xsl:value-of select="td[3]"/></xsl:attribute>
						<xsl:attribute name="dept_code"><xsl:value-of select="td[4]"/></xsl:attribute>
						<xsl:attribute name="cost_class_code"><xsl:value-of select="td[5]"/></xsl:attribute>
						<xsl:value-of select="td[7]" disable-output-escaping="yes"/>
					</select>
				</td>
				<xsl:if test="(position() mod $lzk_bc)=0 or $lzk_bc = 1">
					<xsl:text disable-output-escaping="yes">
						&lt;/tr&gt;
					</xsl:text>
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

