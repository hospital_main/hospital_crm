<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<colgroup>		         	
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>	  	   	
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>	  	   	
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
	  	   	<col style = 'width:85'/>
		</colgroup>
		<thead>
			<tr noWrap="true" class="mainHead">
				<th noWrap="true" colspan="1" rowspan="3" >成本分类</th>
				<th noWrap="true" colspan="4" rowspan="2" >合计</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="7"  >直接成本</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="9" >分摊成本</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
			</tr>  
			<tr noWrap="true" class="mainHead">
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="2" >科室医疗成本</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="1" >收费材料</th>
				<th noWrap="true" colspan="3">作业直接</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="1" rowspan="2" >差额</th>
				<th noWrap="true" colspan="3" >公用成本</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="3" >管理成本</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" colspan="3" >医辅成本</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
			</tr> 
		  <tr noWrap="true" class="mainHead">
				<th noWrap="true" style="display:none">成本分类</th>
				<th noWrap="true">科室医疗成本</th>
				<th noWrap="true">收费材料</th>
				<th noWrap="true">作业成本</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">直接归集</th>
				<th noWrap="true">直接计算计入</th>
				<th noWrap="true">直接归集</th>
				<th noWrap="true">直接归集</th>
				<th noWrap="true">科内直接</th>
				<th noWrap="true">直接计算计入</th>
				<th noWrap="true" style="display:none">差额</th>
				<th noWrap="true">科室</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">科室</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">科室</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>

			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test="position()=1 or position()=2 or position()=3 or position()=4 or position()=5 or position()=6 or position()=28">

								</xsl:when>

								<xsl:when test="position()=7 and text() !='合计' ">
									<td>
										<a href="#">
											<xsl:attribute name="onclick">
													openDialog('workcostcheck_by_costsubjcode.html?load=&lt;set_cfg_code&gt;<xsl:value-of select="../td[1]"/>&lt;/set_cfg_code&gt;&lt;set_cfg_desc&gt;<xsl:value-of select="../td[2]"/>&lt;/set_cfg_desc&gt;&lt;year_month&gt;<xsl:value-of select="../td[3]"/>&lt;/year_month&gt;&lt;dept_code&gt;<xsl:value-of select="../td[4]"/>&lt;/dept_code&gt;&lt;dept_name&gt;<xsl:value-of select="../td[5]"/>&lt;/dept_name&gt;&lt;cost_class_code&gt;<xsl:value-of select="../td[6]"/>&lt;/cost_class_code&gt;&lt;cost_class_name&gt;<xsl:value-of select="../td[7]"/>&lt;/cost_class_name&gt;', 'dialogWidth:1100px;dialogHeight:700px')
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>							
									</td>	
								</xsl:when>
								
								<xsl:when test="position()=7 and text()='合计'">
									<td>
										<xsl:value-of select="."/>				
									</td>	
								</xsl:when>
																
								<xsl:when test="position()=8 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17 or position()=18 or position()=19 or position()=20 or position()=21 or position()=22 or position()=23 or position()=24 or position()=25 or position()=26 or position()=27">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>	
								</xsl:when>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
