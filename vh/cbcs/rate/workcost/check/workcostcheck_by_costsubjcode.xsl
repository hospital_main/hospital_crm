<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th noWrap="true"  rowspan="3" class="mainHead">成本项目</th>
				<th noWrap="true" colspan="4" rowspan="2"  class="mainHead">合计</th>
				<th noWrap="true" colspan="7"  class="mainHead">直接成本</th>
				<th noWrap="true" colspan="9" class="mainHead">分摊成本</th>
			</tr>  
				<tr noWrap="true" class="mainHead">
				<!--th noWrap="true" colspan="1" class="mainHead"></th-->
				<!--th noWrap="true" colspan="4" class="mainHead"></th-->
				<th noWrap="true" colspan="2" class="mainHead">科室医疗成本</th>
				<th noWrap="true" colspan="1" class="mainHead">收费材料</th>
				<th noWrap="true" colspan="3" class="mainHead">作业直接</th>
				<th noWrap="true" colspan="1" rowspan="2" class="mainHead">差额</th>
				<th noWrap="true" colspan="3" class="mainHead">公用成本</th>
				<th noWrap="true" colspan="3" class="mainHead">管理成本</th>
				<th noWrap="true" colspan="3" class="mainHead">医辅成本</th>
			</tr> 
		  <tr noWrap="true" class="mainHead">
		    <th noWrap="true" style="display:none">set_cfg_code  1</th>
				<th noWrap="true" style="display:none">set_cfg_desc  2</th>
				<th noWrap="true" style="display:none">year_month  3</th>
				<th noWrap="true" style="display:none">dept_code  4</th>             <!--设置属性，隐藏列-->
				<th noWrap="true" style="display:none">dept_name  5</th>
				<th noWrap="true" style="display:none">cost_class_code  6</th>
				<th noWrap="true" style="display:none">cost_class_name 7</th>
				<th noWrap="true" style="display:none">cost_subj_code  8</th>
				<th noWrap="true" style="display:none">is_direct  9</th>
				<!--th noWrap="true"></th-->
				<th noWrap="true">科室医疗成本</th>
				<th noWrap="true">收费材料</th>
				<th noWrap="true">作业成本</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">直接归集</th>
				<th noWrap="true">直接计算计入</th>
				<th noWrap="true">直接归集</th>
				<th noWrap="true">直接归集</th>
				<th noWrap="true">科内直接</th>
				<th noWrap="true">直接计算计入</th>
				<!--th noWrap="true">差额</th-->
				<th noWrap="true">科室</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">科室</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">科室</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true" style="display:none">order_no 31</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test="position()=1 or position()=2 or position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=31 " >
								<td>
									<xsl:attribute name="style">display:none</xsl:attribute>   <!--设置属性，隐藏列-->
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()=10">
								<td>
										<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()=18 and ../td[10]!='合计' and (../td[9]='1' or ../td[9]='2' or ../td[9]='5') ">
								<td>
								<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<a href="#">
										<xsl:attribute name="onclick">
												openDialog('workcostcheck_work_dev_inv_other.html?load=&lt;set_cfg_code&gt;<xsl:value-of select="../td[1]"/>&lt;/set_cfg_code&gt;&lt;set_cfg_desc&gt;<xsl:value-of select="../td[2]"/>&lt;/set_cfg_desc&gt;&lt;year_month&gt;<xsl:value-of select="../td[3]"/>&lt;/year_month&gt;&lt;dept_code&gt;<xsl:value-of select="../td[4]"/>&lt;/dept_code&gt;&lt;dept_name&gt;<xsl:value-of select="../td[5]"/>&lt;/dept_name&gt;&lt;cost_class_code&gt;<xsl:value-of select="../td[6]"/>&lt;/cost_class_code&gt;&lt;cost_class_name&gt;<xsl:value-of select="../td[7]"/>&lt;/cost_class_name&gt;&lt;cost_subj_code&gt;<xsl:value-of select="../td[8]"/>&lt;/cost_subj_code&gt;&lt;is_direct&gt;<xsl:value-of select="../td[9]"/>&lt;/is_direct&gt;&lt;cost_subj_name&gt;<xsl:value-of select="../td[10]"/>&lt;/cost_subj_name&gt;', 'dialogWidth:1100px;dialogHeight:700px')	
										</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</a>							
								</td>	
								</xsl:when>
								<xsl:when test="position()=18 and ../td[10]!='合计' and (../td[9]!='1' and ../td[9]!='2' and ../td[9]!='5') ">
								<td>
								<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>								
								</td>	
								</xsl:when>
								<xsl:when test="position()=18 and ../td[10]='合计' ">
								<td>
										<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>						
								</td>	
								</xsl:when>
								<xsl:when test="position()=11 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17 or position()=19 or position()=20 or position()=21 or position()=22 or position()=23 or position()=24 or position()=25 or position()=26 or position()=27 or position()=28 or position()=29 or position()=30">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>	
								</xsl:when>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
