<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<colgroup>		         	
	  	   	<col style = 'width:120mm'/>
	  	   	<col style = 'width:120mm'/>
	  	   	<col style = 'width:120mm'/>
		</colgroup>
		<thead>
		  <tr noWrap="true" class="mainHead">
				<th noWrap="true">工作人员职称</th>
				<th noWrap="true">工作人员数量</th>
				<th noWrap="true">人工费用</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test=" position()=1 or position()=5 " >
								</xsl:when>
								<xsl:when test="position()=2">
								<td>
									<xsl:attribute name="style">text-align:left;</xsl:attribute>
										<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()=3 or position()=4">
								<td>
										<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>	
								</xsl:when>
								
								
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
