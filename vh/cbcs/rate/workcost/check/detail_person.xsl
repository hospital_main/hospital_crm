<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
		  <tr noWrap="true" class="mainHead">
				<th noWrap="true" style="display:none">detail_code</th>
				<th noWrap="true">工作人员职称</th>
				<th noWrap="true">工作人员数量</th>
				<th noWrap="true">人工费用</th>
				<th noWrap="true" style="display:none">order_no</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test=" position()=1 or position()=5 " >
								<td>
									<xsl:attribute name="style">display:none</xsl:attribute>   <!--设置属性，隐藏列-->
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()=2">
								<td>
									<xsl:attribute name="style">text-align:left;</xsl:attribute>
										<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()=3 or position()=4">
								<td>
										<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>	
								</xsl:when>
								
								
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
