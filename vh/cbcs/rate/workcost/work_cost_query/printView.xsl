<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/rate/workcost/work_cost_query/printView.xsl,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:03 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<colgroup>		       
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
		</colgroup>
		<thead>
		  <tr noWrap="true" class="mainHead">  
				<th noWrap="true" rowspan="2">科室名称</th>            
				<th noWrap="true" rowspan="2">作业</th>
				<th noWrap="true" rowspan="2">作业成本</th>
				<th noWrap="true" colspan="4">直接成本</th>
				<th noWrap="true" colspan="4">分摊成本</th>
			</tr>
			<tr noWrap="true" class="mainHead">  
				<th noWrap="true" >人</th>
				<th noWrap="true" >材</th>
				<th noWrap="true" >设备</th>
				<th noWrap="true" >其它</th>
				<th noWrap="true" >科内直接成</th>
				<th noWrap="true" >分摊公用</th>
				<th noWrap="true" >分摊管理</th>
				<th noWrap="true" >分摊医辅</th>									
			</tr>
	  </thead>
	   <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test="position()=1 or position()=2 or position()=3 or position()=5  or position()=8 or position()=9 ">
								</xsl:when>
								
								<xsl:when test="position()=4 or position()=6 ">
									<td>
										<xsl:value-of select="."/>				
									</td>	
								</xsl:when>
	
								<xsl:when test="position()=7 and ../td[6]!='小计'">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<a href="#">
											<xsl:attribute name="onclick">
													openDialog('work_cost_query_costclasscode.html?load=&lt;set_cfg_code&gt;<xsl:value-of select="../td[1]"/>&lt;/set_cfg_code&gt;&lt;year_month&gt;<xsl:value-of select="../td[2]"/>&lt;/year_month&gt;&lt;set_cfg_desc&gt;<xsl:value-of select="../td[9]"/>&lt;/set_cfg_desc&gt;&lt;dept_name&gt;<xsl:value-of select="../td[4]"/>&lt;/dept_name&gt;&lt;dept_code&gt;<xsl:value-of select="../td[3]"/>&lt;/dept_code&gt;&lt;work_code&gt;<xsl:value-of select="../td[5]"/>&lt;/work_code&gt;&lt;work_name&gt;<xsl:value-of select="../td[6]"/>&lt;/work_name&gt;', 'dialogWidth:1100px;dialogHeight:700px')
											</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</a>							
								</td>	
								</xsl:when>
									<xsl:when test="position()=7 and ../td[6]='小计'">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>	
								</xsl:when>
								<xsl:otherwise>
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>	
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
