<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/rate/workcost/work_cost_query/printViewcostclass.xsl,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:03 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<colgroup>		       
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:150m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>
	  	   	<col style = 'width:100m'/>	  	   	
	  	   	<col style = 'width:100m'/>

		</colgroup>
		<thead>
		<tr noWrap="true" class="mainHead">
		  
		  		<th noWrap="true" rowspan="2">科室名称</th>
		  		<th noWrap="true" rowspan="2">作业</th>
		  		<th noWrap="true" rowspan="2">成本分类</th>
		  		<th noWrap="true" rowspan="2">合计</th>
		  		<th noWrap="true" rowspan="2">直接成本</th>
		  		<th noWrap="true" colspan="5">分摊成本</th>
		  </tr>  
		  
		  <tr noWrap="true" class="mainHead">  
		    <th noWrap="true" style="display:none">set_cfg_code 1</th>
				<th noWrap="true" style="display:none">year_month  2</th>
				<th noWrap="true" style="display:none">set_code_desc  3</th>
				<th noWrap="true" style="display:none">dept_code  4</th>   <!--设置属性，隐藏列-->   
				<th noWrap="true" style="display:none">work_code  6</th>

				<th noWrap="true">科内直接成本</th>
				<th noWrap="true">直接计算成本</th>
				<th noWrap="true">公用成本</th>
				<th noWrap="true">管理成本</th>
				<th noWrap="true">医辅成本</th>
	    </tr>
	  </thead>
	   <tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test="position()=1 or position()=2 or position()=3 or position()=4  or position()=6 or position()=8 or position()=17 ">
								</xsl:when>
								
								<xsl:when test="position()=5 or position()=7">
									<td>
										<xsl:value-of select="."/>				
									</td>	
								</xsl:when>
	
								<xsl:when test="position()=9  and text()!='合计'">
								<td>
									<xsl:attribute name="style">text-align:left;</xsl:attribute>
										<a href="#">
											<xsl:attribute name="onclick">

													openDialog('work_cost_query_costsubjcode3.html?load= &lt;set_cfg_code&gt;<xsl:value-of select="../td[1]"/>&lt;/set_cfg_code&gt;&lt;year_month&gt;<xsl:value-of select="../td[2]"/>&lt;/year_month&gt;&lt;set_cfg_desc&gt;<xsl:value-of select="../td[3]"/>&lt;/set_cfg_desc&gt;&lt;dept_name&gt;<xsl:value-of select="../td[5]"/>&lt;/dept_name&gt;&lt;cost_class_name&gt;<xsl:value-of select="../td[9]"/>&lt;/cost_class_name&gt;&lt;dept_code&gt;<xsl:value-of select="../td[4]"/>&lt;/dept_code&gt;&lt;work_code&gt;<xsl:value-of select="../td[6]"/>&lt;/work_code&gt;&lt;work_name&gt;<xsl:value-of select="../td[7]"/>&lt;/work_name&gt;&lt;cost_class_code&gt;<xsl:value-of select="../td[8]"/>&lt;/cost_class_code&gt;', 'dialogWidth:1100px;dialogHeight:700px')
											</xsl:attribute>
											<xsl:value-of select="."/>		
										</a>							
								</td>	
								</xsl:when>
									<xsl:when test="position()=9 and text()='合计'">
								<td>
									<xsl:attribute name="style">text-align:left;</xsl:attribute>
									<xsl:value-of select="."/>			
								</td>	
								</xsl:when>
								
								<xsl:when test="position()=10 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>	
								</xsl:when>
								
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
