<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
			</colgroup>
			<thead>            
			  <tr noWrap="true" class="mainHead">  
			    <th noWrap="true">科室</th>
				<th noWrap="true">作业排序号</th>            
				<th noWrap="true">作业</th>
				<th noWrap="true">是否外协</th>
				<th noWrap="true">体现人工作业</th>
				<th noWrap="true">体现设备作业</th>
				<th noWrap="true">体现材料作业</th>
				<th noWrap="true">作业属性</th>
				<th noWrap="true">是否按设备分类分配</th>
				<th noWrap="true">是否按材料分类分配</th>
				<th noWrap="true">说明</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>     
						<xsl:for-each select="td">
							<td>
								<xsl:value-of select="."/>	
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>  	
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
