<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
			</colgroup>
			<thead>            
			  <tr noWrap="true" class="mainHead">  
			    <th noWrap="true">作业编码</th>
				<th noWrap="true">作业名称</th>            
				<th noWrap="true">作业分类</th>
				<th noWrap="true">作业说明</th>
				<th noWrap="true">作业计量单位</th>
				<th noWrap="true">拼音简写</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>     
						<xsl:for-each select="td">
							<td>
								<xsl:value-of select="."/>	
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>  	
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
