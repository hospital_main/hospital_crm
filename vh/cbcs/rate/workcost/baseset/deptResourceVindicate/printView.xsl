<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
			</colgroup>
			<thead>            
			  <tr noWrap="true" class="mainHead">  
			    <th noWrap="true">科室</th>
				<th noWrap="true">作业</th>            
				<th noWrap="true">成本分类</th>
				<th noWrap="true">说明</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>     
						<xsl:for-each select="td">
							<td>
								<xsl:value-of select="."/>	
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>  	
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
