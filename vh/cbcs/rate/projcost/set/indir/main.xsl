<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr>
			<td  class="mainHead" align="center" style="width:100px" rowspan="2">作业编号</td>
			<td  class="mainHead" align="center" rowspan="2">作业名称</td>
			<td  class="mainHead" align="center" colspan="2">公用成本</td>
			<td style="display:none"></td>
			<td  class="mainHead" align="center" colspan="2">管理成本</td>
			<td style="display:none"></td>
			<td  class="mainHead" align="center" colspan="2">医辅成本</td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td  class="mainHead" align="center" style="display:none"></td>
			<td  class="mainHead" align="center" style="display:none"></td>
			<td  class="mainHead" align="center">作业动因</td>
			<td  class="mainHead" align="center" nowrap="true">是否考<br/>虑时间</td>
			<td  class="mainHead" align="center">作业动因</td>
			<td  class="mainHead" align="center" nowrap="true">是否考<br/>虑时间</td>
			<td  class="mainHead" align="center">作业动因</td>
			<td  class="mainHead" align="center" nowrap="true">是否考<br/>虑时间</td>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:variable name="tr_pos" select="position()"/>
			<tr>
			<xsl:for-each select="td">
				<xsl:variable name="lzk_opt" select="position()"/>
				<xsl:choose> 
					<xsl:when test=" position()=1 or position()=2">
						<td style="width:100px"><xsl:value-of select="."/></td>
					</xsl:when>
					<xsl:when test=" position()=3 or position()=8 or position()=13">
						<td><table width="0" border="0" cellspacing="0" cellpadding="1">
							<tr>
							<td style="display:none">
								<input type="radio" name="radiobutton" value="radiobutton" onclick="selRadioValue(this)">
									<xsl:attribute name="sid">sel<xsl:value-of select="position()"/></xsl:attribute>
								</input>
							</td>
							<td>
							<select onchange="doSetEditStatus(this)">
								<xsl:attribute name="id">sel<xsl:value-of select="position()"/>_<xsl:value-of select="$tr_pos"/></xsl:attribute>
								<xsl:attribute name="wid"><xsl:value-of select="../td[18]"/>+<xsl:value-of select="../td[$lzk_opt + 3]"/></xsl:attribute>
								<xsl:attribute name="posid"><xsl:value-of select="position()"/>_<xsl:value-of select="$tr_pos"/></xsl:attribute>
								<xsl:attribute name="oldValue"><xsl:value-of select="."/></xsl:attribute>
								<xsl:attribute name="costName"><xsl:value-of select="position()"/></xsl:attribute>
								<xsl:attribute name="workName"><xsl:value-of select="../td[2]"/></xsl:attribute>
								<xsl:value-of select="../td[$lzk_opt + 2]" disable-output-escaping="yes"/>
							</select>
							</td>
							<td>
								<button onclick="doEditNum(this)">
									<xsl:attribute name="id">btn<xsl:value-of select="position()"/>_<xsl:value-of select="$tr_pos"/></xsl:attribute>
									<xsl:attribute name="sid">sel<xsl:value-of select="position()"/>_<xsl:value-of select="$tr_pos"/></xsl:attribute>
									<xsl:if test=".='001' or .='002'"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
									编辑
								</button>
							</td>
							</tr></table>
						</td>
						<td align="center">
							<input type="checkbox">
								<xsl:attribute name="id">chk<xsl:value-of select="position()"/>_<xsl:value-of select="$tr_pos"/></xsl:attribute>
								<xsl:attribute name="oldValue"><xsl:value-of select="../td[$lzk_opt + 1]"/></xsl:attribute>
								<xsl:if test="../td[$lzk_opt + 1]='Y'"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
							</input>
						</td>
					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

