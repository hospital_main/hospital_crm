
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<xsl:variable name="lzk_cc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
				<col style = 'width:100mm'/>
				<col style = 'width:200mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:50mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:50mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:50mm'/>
			</colgroup>
			<thead>
				<tr>
					<th bgcolor="#FFDDA6" align="center" rowspan="2">作业编号</th>
					<th bgcolor="#FFDDA6" align="center" rowspan="2">作业名称</th>
					<th bgcolor="#FFDDA6" align="center" colspan="2">公用成本</th>
					<th style="display:none"></th>
					<th bgcolor="#FFDDA6" align="center" colspan="2">管理成本</th>
					<th style="display:none"></th>
					<th bgcolor="#FFDDA6" align="center" colspan="2">医辅成本</th>
					<th style="display:none"></th>
				</tr>
				<tr>
					<th bgcolor="#FFDDA6" align="center" style="display:none"></th>
					<th bgcolor="#FFDDA6" align="center" style="display:none"></th>
					<th bgcolor="#FFDDA6" align="center">作业动因</th>
					<th bgcolor="#FFDDA6" align="center">是否考虑时间</th>
					<th bgcolor="#FFDDA6" align="center">作业动因</th>
					<th bgcolor="#FFDDA6" align="center">是否考虑时间</th>
					<th bgcolor="#FFDDA6" align="center">作业动因</th>
					<th bgcolor="#FFDDA6" align="center">是否考虑时间</th>
				</tr>
			</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<xsl:variable name="tr_pos" select="position()"/>
			<tr>
			<xsl:for-each select="td">
				<xsl:variable name="lzk_opt" select="position()"/>
				<xsl:choose> 
					<xsl:when test=" position()=1 or position()=2">
						<td><xsl:value-of select="."/></td>
					</xsl:when>
					<xsl:when test=" position()=3 or position()=8 or position()=13">
						<td><xsl:value-of select="../td[$lzk_opt + 4]"/></td>
						<td align="center">
							<xsl:if test="../td[$lzk_opt + 1]='Y'">是</xsl:if>
							<xsl:if test="../td[$lzk_opt + 1]!='Y'">否</xsl:if>
						</td>
					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			</tr>
		</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
