<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="lzk_hc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<tr>
			<td  class="mainHead" align="center" rowspan="2">�ɱ�����</td>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $lzk_hc]">
				<td  class="mainHead" align="center" nowrap="true">
					<xsl:value-of select="td[2]"/>
				</td>
			</xsl:for-each>
		</tr>
	</thead>
	<tbody>
		<xsl:variable name="lzk_bc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<xsl:variable name="lzk_opt" select="/root/tbody/tr[1]/td[5]"/>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:if test="( position() mod $lzk_bc )=1">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;
				</xsl:text>
				<td class="mainHeadB" style="align:left" align="left">
					<xsl:value-of select="td[1]"/>
				</td>
			</xsl:if>
			<xsl:if test="td[5]!=''">
				<td align="center">
					<input type="checkbox">
						<xsl:attribute name="id">chk<xsl:value-of select="position()"/></xsl:attribute>
						<xsl:attribute name="wid"><xsl:value-of select="td[5]"/></xsl:attribute>
						<xsl:if test="td[8]=1"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
					</input>
				</td>
			</xsl:if>
			<xsl:if test="td[5]=''">
				<td></td>
			</xsl:if>
			<xsl:if test="(position() mod $lzk_bc)=0">
				<xsl:text disable-output-escaping="yes">
					&lt;/tr&gt;
				</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

