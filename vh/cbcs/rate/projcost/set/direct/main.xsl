<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="lzk_hc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<tr>
			<th  class="tabLTC" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="2">成本分类</th>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $lzk_hc]">
				<th  style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" colspan="2">
					<xsl:value-of select="td[2]"/>
				</th>
				<th style="display:none"></th>
			</xsl:for-each>
		</tr>
		<tr>
			<th  class="mainHead" align="center" style="display:none">成本项目</th>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $lzk_hc]">
				<th   align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">
					作业动因
				</th>
				<th  style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" nowrap="true">
					是否考<br/>虑时间
				</th>
			</xsl:for-each>
		</tr>
	</thead>
	<tbody>
		<xsl:variable name="lzk_bc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<xsl:variable name="lzk_opt" select="/root/tbody/tr[1]/td[5]"/>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:if test="( position() mod $lzk_bc )=1 or $lzk_bc = 1">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;
				</xsl:text>
				<td class="tabL"  align="left" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">
					<xsl:value-of select="td[1]"/>
				</td>
			</xsl:if>
			<xsl:if test="td[5]!=''">
				<td><table width="0" border="0" cellspacing="0" cellpadding="1">
					<tr>
					<td>
						<input type="radio" name="radiobutton" value="radiobutton" onclick="selRadioValue(this)">
							<xsl:attribute name="sid">sel<xsl:value-of select="position()"/></xsl:attribute>
						</input>
					</td>
					<td>
					<select onchange="doSetEditStatus(this)">
						<xsl:attribute name="id">sel<xsl:value-of select="position()"/></xsl:attribute>
						<xsl:attribute name="wid"><xsl:value-of select="td[5]"/></xsl:attribute>
						<xsl:attribute name="posid"><xsl:value-of select="position()"/></xsl:attribute>
						<xsl:attribute name="oldValue"><xsl:value-of select="td[3]"/></xsl:attribute>
						<xsl:attribute name="costName"><xsl:value-of select="td[1]"/></xsl:attribute>
						<xsl:attribute name="workName"><xsl:value-of select="td[2]"/></xsl:attribute>
						<xsl:value-of select="td[6]" disable-output-escaping="yes"/>
					</select>
					</td>
					<td>
						<button onclick="doEditNum(this)">
							<xsl:attribute name="id">btn<xsl:value-of select="position()"/></xsl:attribute>
							<xsl:attribute name="sid">sel<xsl:value-of select="position()"/></xsl:attribute>
							<xsl:if test="td[3]='001' or td[3]='002'"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
							编辑
						</button>
					</td>
					</tr></table>
				</td>
				<td align="center">
					<input type="checkbox">
						<xsl:attribute name="id">chk<xsl:value-of select="position()"/></xsl:attribute>
						<xsl:attribute name="oldValue"><xsl:value-of select="td[4]"/></xsl:attribute>
						<xsl:if test="td[4]='Y'"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
					</input>
				</td>
			</xsl:if>
			<xsl:if test="td[5]=''">
				<td></td><td></td>
			</xsl:if>
			<xsl:if test="(position() mod $lzk_bc)=0 or $lzk_bc = 1">
				<xsl:text disable-output-escaping="yes">
					&lt;/tr&gt;
				</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

