
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<xsl:variable name="lzk_cc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
				<col style = 'width:200mm'/>
				<xsl:for-each select="/root/tbody/tr[position() &lt;= $lzk_cc]">
					<col style = 'width:100mm'/>
				</xsl:for-each>
			</colgroup>
			<thead>
				<xsl:variable name="lzk_hc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
				<tr>
					<th bgcolor="#FFDDA6" align="center" rowspan="2">���Ϸ���</th>
					<xsl:for-each select="/root/tbody/tr[position() &lt;= $lzk_hc]">
						<th bgcolor="#FFDDA6" align="center">
							<xsl:value-of select="td[2]"/>
						</th>
					</xsl:for-each>
				</tr>
			</thead>
			<tbody>
				<xsl:variable name="lzk_bc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
				<xsl:variable name="lzk_opt" select="/root/tbody/tr[1]/td[5]"/>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:if test="( position() mod $lzk_bc )=1 or $lzk_bc = 1">
						<xsl:text disable-output-escaping="yes">
							&lt;tr&gt;
						</xsl:text>
						<td bgcolor="#FFDDA6">
							<xsl:value-of select="td[1]"/>
						</td>
					</xsl:if>
					<xsl:if test="td[5]!=''">
						<td><xsl:value-of select="td[7]"/></td>
					</xsl:if>
					<xsl:if test="td[5]=''">
						<td></td>
					</xsl:if>
					<xsl:if test="(position() mod $lzk_bc)=0 or $lzk_bc = 1">
						<xsl:text disable-output-escaping="yes">
							&lt;/tr&gt;
						</xsl:text>
					</xsl:if>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
