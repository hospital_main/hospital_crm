<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
		  <tr noWrap="true" class="mainHead">  
		    		<th noWrap="true">科室</th>
				<th noWrap="true">项目总数</th>
				<th noWrap="true">盈利数量</th>
				<th noWrap="true">亏损数量</th>          
				<th noWrap="true">图形</th>

			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     <xsl:variable name='CurTrPos' select='position()'/>
					<xsl:for-each select="td">
							<xsl:choose> 
								<xsl:when test="position()=1  ">
								<td>
									<xsl:value-of select="."/>
								</td>
								</xsl:when>	
								<xsl:when test="position()=2 or position()=3 or position()=4  ">
								<td align = 'right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
								</xsl:when>
								<xsl:when test=" position()=5  ">
									<td align='center'  noWrap='true'>
								            	<button class="graphButton">
								            	 <xsl:attribute name="index"><xsl:value-of select='$CurTrPos'/></xsl:attribute>
								            	 <xsl:attribute name="onclick">openSwf(this)</xsl:attribute>
								            	 ___
								    		</button>
							         	 </td>
								</xsl:when>	
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
