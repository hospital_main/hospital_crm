
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
		  <colgroup>		       
				<col style = 'width:305mm'/>
				<col style = 'width:305mm'/>
				<col style = 'width:305mm'/>
				<col style = 'width:305mm'/>
				
		
			</colgroup>
	  	<thead>            
		  <tr noWrap="true" class="mainHead">  
		    		<th noWrap="true">科室</th>
				<th noWrap="true">项目总数</th>
				<th noWrap="true">盈利金额</th>
				<th noWrap="true">亏损金额</th>          
			

			</tr>
		</thead>
	  	<tbody>
				<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
								<xsl:when test="position()=1 ">
								<td>
									<xsl:value-of select="."/>
								</td>
								</xsl:when>	
								<xsl:when test="position()=2 or position()=3 or position()=4  ">
								<td align = 'right'>
									<xsl:value-of select="."/>
								</td>
								</xsl:when>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  		
			</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>
