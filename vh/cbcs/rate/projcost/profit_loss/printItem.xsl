<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/rate/projcost/profit_loss/printItem.xsl,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
	  <colgroup>		       
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
		</colgroup>
  	<thead>
		  <tr noWrap="true" class="mainHead">
				<th noWrap="true">项目总数</th>
				<th noWrap="true">盈利数量</th>
				<th noWrap="true">亏损数量</th>				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0')"/></td>	
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>
