<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
		  <tr noWrap="true" class="mainHead">
		    <th noWrap="true" style='display:none'></th>
				<th noWrap="true">项目总数</th>
				<th noWrap="true">盈利数量</th>
				<th noWrap="true">亏损数量</th>				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr id="dataTr">     
					<xsl:for-each select="td">
							<td class="numberText"><xsl:value-of select="format-number(.,'#,##0')"/></td>	
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
