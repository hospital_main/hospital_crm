<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
		
		<tr noWrap="true" class="mainHead">  
		  	
				<th noWrap="true" rowspan="2" >作业序号</th>
				<th noWrap="true" rowspan="2" >作业名称</th>
				
				<th noWrap="true" colspan="2">单位成本</th>
				<th noWrap="true" colspan="3">直接成本</th>
				<th noWrap="true" colspan="4">间接成本</th>
			
				
			</tr>
		
		
		  <tr noWrap="true" class="mainHead">  
		  	<!--th noWrap="true" >作业序号 1</th-->
		  	<th noWrap="true" style="display:none">work_code 2</th>
		  	<!--th noWrap="true" >作业名称 3</th-->
		    <th noWrap="true" style="display:none">set_cfg_code 4</th>
				<th noWrap="true" style="display:none">dept_code 5</th>
				<th noWrap="true" style="display:none">dept_name 6</th>
				<th noWrap="true" style="display:none">charge_detail_code 7</th>             <!--设置属性，隐藏列-->
				<th noWrap="true" style="display:none">detail_charge_name 8</th>
				
				<th noWrap="true" >成本</th>
				<th noWrap="true" style="display:none">总成本 隐藏10</th>
				<th noWrap="true" >成本构成</th>
				
				<th noWrap="true" >合计</th>
				<th noWrap="true" >直接成本归集</th>
				<th noWrap="true" >计算计入</th>
				
				<th noWrap="true" >合计</th>
				<th noWrap="true" >公用成本</th>
				<th noWrap="true" >管理成本</th>
				<th noWrap="true" >医辅成本</th>
				
				<th noWrap="true" style="display:none">year_month 19</th>
				<th noWrap="true" style="display:none">order_no 20</th>
				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test="position()=2 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=10 or position()=19 or position()=20  ">
								<td>
									<xsl:attribute name="style">display:none</xsl:attribute>   <!--设置属性，隐藏列-->
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								
								<xsl:when test="position()=11 ">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								
								<xsl:when test="position()=1  ">
								<td>
									
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								
								<xsl:when test=" position()=9 or position()=11  or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17 or position()=18 ">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.000000')"/>
								</td>	
								</xsl:when>
								
								<xsl:when test="position()=3 and text()!='合计' ">
								<td>
									<!--xsl:attribute name="style">text-align:center</xsl:attribute-->
									<a href="#">
											<xsl:attribute name="onclick">
													openDialog('proj_cost_query_chargedetail_costclasscode2.html?load=&lt;set_cfg_code&gt;<xsl:value-of select="../td[4]"/>&lt;/set_cfg_code&gt;&lt;year_month&gt;<xsl:value-of select="../td[19]"/>&lt;/year_month&gt;&lt;dept_code&gt;<xsl:value-of select="../td[5]"/>&lt;/dept_code&gt;&lt;dept_name&gt;<xsl:value-of select="../td[6]"/>&lt;/dept_name&gt;&lt;charge_detail_code&gt;<xsl:value-of select="../td[7]"/>&lt;/charge_detail_code&gt;&lt;charge_detail_name&gt;<xsl:value-of select="../td[8]"/>&lt;/charge_detail_name&gt;&lt;work_code&gt;<xsl:value-of select="../td[2]"/>&lt;/work_code&gt;&lt;work_name&gt;<xsl:value-of select="../td[3]"/>&lt;/work_name&gt;', 'dialogWidth:1100px;dialogHeight:700px')
											</xsl:attribute>
									<xsl:value-of select="."/>
									</a>	
								</td>	
								</xsl:when>
								<xsl:when test="position()=3 and text()='合计' ">
								<td>
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
