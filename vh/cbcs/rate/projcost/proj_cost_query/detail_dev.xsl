<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
		  <tr noWrap="true" class="mainHead">
				<th noWrap="true" style="display:none">dev_type_code</th>
				<th noWrap="true" style="display:none">dev_type_name</th>
				<th noWrap="true">设备编号</th>
				<th noWrap="true">设备名称</th>
				<th noWrap="true">作业时间</th>
				<th noWrap="true">设备原值</th>
				<th noWrap="true">设备折值</th>				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test="position()=1 or position()=2 " >
								<td>
									<xsl:attribute name="style">display:none</xsl:attribute>   <!--设置属性，隐藏列-->
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()=3  or position()=4 ">
								<td>
									<xsl:attribute name="style">text-align:left;</xsl:attribute>
										<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()=5  or position()=6 or position()=7 ">
								<td>
										<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.000000')"/>
								</td>	
								</xsl:when>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
