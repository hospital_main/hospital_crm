<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
		  <tr noWrap="true" class="mainHead">  
		    <th noWrap="true" style="display:none">配置编码  1</th>
				<th noWrap="true" style="display:none">charge_detail_code 2</th>             <!--设置属性，隐藏列-->
				<th noWrap="true">医疗项目</th>
				<th noWrap="true">单位成本</th>
				<th noWrap="true">成本明细</th>
				<th noWrap="true" style="display:none">year_month6</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test="position()=1 or position()=2 or position()=7 ">
								<td>
									<xsl:attribute name="style">display:none</xsl:attribute>   <!--设置属性，隐藏列-->
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()=3">
								<td>
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								
								<xsl:when test=" position()=4 ">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.000000')"/>
								</td>	
								</xsl:when>							
									<xsl:when test="position()=5 ">
								<td>
									<xsl:attribute name="style">text-align:center</xsl:attribute>
									<a href="#">
											<xsl:attribute name="onclick">
											openDialog('proj_cost_query_costclasscode1_avg.html?load=&lt;set_cfg_code&gt;<xsl:value-of select="../td[1]"/>&lt;/set_cfg_code&gt;&lt;charge_detail_code&gt;<xsl:value-of select="../td[2]"/>&lt;/charge_detail_code&gt;&lt;charge_detail_name&gt;<xsl:value-of select="../td[3]"/>&lt;/charge_detail_name&gt;&lt;year_month&gt;<xsl:value-of select="../td[6]"/>&lt;/year_month&gt;', 'dialogWidth:1100px;dialogHeight:700px')
											</xsl:attribute>
									<xsl:value-of select="."/>
									</a>	
								</td>	
								</xsl:when>								
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
