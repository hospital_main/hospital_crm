<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
		
		<tr noWrap="true" class="mainHead">  
		  	
				<th noWrap="true" rowspan="2" >成本分类</th>
				<th noWrap="true" colspan="2" >单位成本</th>
				<th noWrap="true" colspan="3">直接成本</th>
				<th noWrap="true" colspan="4">间接成本</th>
			
				
			</tr>
		
		
		  <tr noWrap="true" class="mainHead">  
		  
		  	<th noWrap="true" style="display:none">cost_class_code 1</th>
		  	
				<!--th noWrap="true">成本分类 2</th-->
				
				<th noWrap="true" >成本</th>
				<th noWrap="true" >成本构成</th>
				
				<th noWrap="true" >合计</th>
				<th noWrap="true" >直接成本归集</th>
				<th noWrap="true" >计算计入</th>
				
				<th noWrap="true" >合计</th>
				<th noWrap="true" >公用成本</th>
				<th noWrap="true" >管理成本</th>
				<th noWrap="true" >医辅成本</th>
				
				<th noWrap="true" style="display:none">order_no 12</th>
				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test="position()=1 or position()=12 ">
								<td>
									<xsl:attribute name="style">display:none</xsl:attribute>   <!--设置属性，隐藏列-->
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								
								<xsl:when test="position()=2 ">
								<td>
									
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								
								<xsl:when test=" position()=4 ">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								
								<xsl:when test=" position()=3 or position()=5  or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 ">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.000000')"/>
								</td>	
								</xsl:when>

							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
