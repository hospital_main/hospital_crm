<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
		
		<tr noWrap="true" class="mainHead">  	
				<th noWrap="true" rowspan="2" >成本分类</th>	
				<th noWrap="true" colspan="2">单位成本</th>
				<th noWrap="true" colspan="3">直接成本</th>
				<th noWrap="true" colspan="4">间接成本</th>
				<!--th noWrap="true" colspan="3">flag</th-->
			</tr>
			
		  <tr noWrap="true" class="mainHead">  
		  	<th noWrap="true" style="display:none">work_code 1</th>
		  	<th noWrap="true" style="display:none">work_name 2</th>
		  	<th noWrap="true" style="display:none">set_cfg_code 3</th>
				<th noWrap="true" style="display:none">dept_code 4</th>
				<th noWrap="true" style="display:none">dept_name 5</th>
				<th noWrap="true" style="display:none">charge_detail_code 6</th>             <!--设置属性，隐藏列-->
				<th noWrap="true" style="display:none">charge_detail_code 7</th>
				<th noWrap="true" style="display:none">cost_class_code 8</th>
				<!--th noWrap="true" >成本分类 9</th-->
				<th noWrap="true" >成本</th>
				<th noWrap="true" >成本构成</th>
				
				<th noWrap="true" >合计</th>
				<th noWrap="true" >直接成本归集</th>
				<th noWrap="true" >计算计入</th>
				
				<th noWrap="true" >合计</th>
				<th noWrap="true" >公用成本</th>
				<th noWrap="true" >管理成本</th>
				<th noWrap="true" >医辅成本</th>
				
				<th noWrap="true" style="display:none">is_direct 19</th>
				<th noWrap="true" style="display:none">year_month 20</th>
				<th noWrap="true" style="display:none">order_no 21</th>
				<th noWrap="true" style="display:none">is_emp_work 22</th>
				<th noWrap="true" style="display:none">is_equip_work 23</th>
				<th noWrap="true" style="display:none">is_meta_work 24</th>
				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test="position()=1 or position()=2 or position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=19 or position()=19 or position()=20 or position()=21  or position()=22 or position()=23 or position()=24 ">
								<td>
									<xsl:attribute name="style">display:none</xsl:attribute>   <!--设置属性，隐藏列-->
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								
								<xsl:when test="position()=11 ">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								
								<xsl:when test=" position()=10 or position()=12  or position()=14 or position()=15 or position()=16 or position()=17 or position()=18 ">
								<td>
									<xsl:attribute name="style">text-align:right;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.000000')"/>
								</td>	
								</xsl:when>
								
								<xsl:when test="position()=13 and text()!= '合计' and (../td[19]='1' or ../td[19]='2' or ../td[19]='5' ) and (../td[22]='1' or ../td[23]='1' or ../td[24]='1' )">
								<td>
									
									<a href="#">
											<xsl:attribute name="onclick">
													openDialog('workcostcheck_work_dev_inv_other.html?load=&lt;set_cfg_code&gt;<xsl:value-of select="../td[3]"/>&lt;/set_cfg_code&gt;&lt;year_month&gt;<xsl:value-of select="../td[20]"/>&lt;/year_month&gt;&lt;dept_code&gt;<xsl:value-of select="../td[4]"/>&lt;/dept_code&gt;&lt;dept_name&gt;<xsl:value-of select="../td[5]"/>&lt;/dept_name&gt;&lt;charge_detail_code&gt;<xsl:value-of select="../td[6]"/>&lt;/charge_detail_code&gt;&lt;charge_detail_name&gt;<xsl:value-of select="../td[7]"/>&lt;/charge_detail_name&gt;&lt;work_code&gt;<xsl:value-of select="../td[1]"/>&lt;/work_code&gt;&lt;work_name&gt;<xsl:value-of select="../td[2]"/>&lt;/work_name&gt;&lt;cost_class_code&gt;<xsl:value-of select="../td[8]"/>&lt;/cost_class_code&gt;&lt;cost_class_name&gt;<xsl:value-of select="../td[9]"/>&lt;/cost_class_name&gt;&lt;is_direct&gt;<xsl:value-of select="../td[19]"/>&lt;/is_direct&gt;&lt;is_emp_work&gt;<xsl:value-of select="../td[22]"/>&lt;/is_emp_work&gt;&lt;is_equip_work&gt;<xsl:value-of select="../td[23]"/>&lt;/is_equip_work&gt;&lt;is_meta_work&gt;<xsl:value-of select="../td[24]"/>&lt;/is_meta_work&gt;', 'dialogWidth:1100px;dialogHeight:700px')
											</xsl:attribute>
									<xsl:value-of select="."/>
									</a>	
								</td>	
								</xsl:when>
									
									
									
									<xsl:when test="position()=9 or text()= '合计' or (../td[19]!='1' and ../td[19]!='2' and ../td[19]!='5' ) or (../td[22]!='1' and ../td[23]!='1' and ../td[24]!='1' ) ">
								<td>
								
									<xsl:value-of select="."/>
								
								</td>	
								</xsl:when>
								
								
								
								
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
