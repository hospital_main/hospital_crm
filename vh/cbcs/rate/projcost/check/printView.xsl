<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<root>
		<colgroup>		       
	  	   	<col style = 'width:150m'/>
	  	   	<col style = 'width:120m'/>
	  	   	<col style = 'width:120m'/>
	  	   	<col style = 'width:120m'/>
	  	   	<col style = 'width:120m'/>
	  	   	<col style = 'width:120m'/>
	  	   	<col style = 'width:120m'/>
	  	   	<col style = 'width:120m'/>
		</colgroup>
		<thead>          
		  <tr noWrap="true" class="mainHead">  
		    <th noWrap="true" >配置编号</th>
				<th noWrap="true">科室名称</th>            
				<th noWrap="true" >项目总成本</th>
				<th noWrap="true">科室医疗总成本</th>
				<th noWrap="true">对外协作成本</th>
				<th noWrap="true">接受协作成本</th>
				<th noWrap="true">单收费材料成本</th>
				<th noWrap="true">差额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test=" position()=2 ">
								</xsl:when>
								
								<xsl:when test="position()=1 ">
									<td>
										<xsl:value-of select="."/>				
									</td>	
								</xsl:when>
	
								<xsl:when test="position()=3 ">
								<td>
											<xsl:value-of select="."/>		
								</td>	
								</xsl:when>
								
								
								<xsl:when test="position()=4 or position()=5  or position()=6 or position()=7 or position()=8 or position()=9 ">
									<td>
										<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>	
									</td>	
								</xsl:when>
								
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
