<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
		  <tr noWrap="true" class="mainHead">  
		    <th noWrap="true" >配置编号</th>
				<th noWrap="true" style="display:none">dept_code  2</th>   <!--设置属性，隐藏列-->
				<th noWrap="true">科室名称</th>            
				<th noWrap="true" >项目总成本</th>
				<th noWrap="true">科室医疗总成本</th>
				<th noWrap="true">对外协作成本</th>
				<th noWrap="true">接受协作成本</th>
				<th noWrap="true">单收费材料成本</th>
				<th noWrap="true">差额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test=" position()=2 ">
								<td>
									<xsl:attribute name="style">display:none</xsl:attribute>   <!--设置属性，隐藏列-->
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								
								<xsl:when test="position()=1 ">
									<td>
										<xsl:value-of select="."/>				
									</td>	
								</xsl:when>
	
								<xsl:when test="position()=3 ">
								<td>
										<a href="#">
											<xsl:attribute name="onclick">
													openDialog('projcostcheck_by_deptcode.html?load=&lt;set_cfg_code&gt;<xsl:value-of select="../td[1]"/>&lt;/set_cfg_code&gt;&lt;dept_code&gt;<xsl:value-of select="../td[2]"/>&lt;/dept_code&gt;&lt;dept_name&gt;<xsl:value-of select="../td[3]"/>&lt;/dept_name&gt;', 'dialogWidth:1100px;dialogHeight:700px')
											</xsl:attribute>
											<xsl:value-of select="."/>		
										</a>							
								</td>	
								</xsl:when>
								
								
								<xsl:when test="position()=4 or position()=5  or position()=6 or position()=7 or position()=8 or position()=9 ">
									<td>
										<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>	
									</td>	
								</xsl:when>
								
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
