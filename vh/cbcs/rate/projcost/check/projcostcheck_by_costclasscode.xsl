<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
			<tr noWrap="true" class="mainHead">  
			<th noWrap="true" rowspan="2" >成本分类</th>
			<th noWrap="true" colspan="3">总成本</th>
			<th noWrap="true" colspan="3">直接成本核算</th>
			<th noWrap="true" colspan="3">公用成本</th>
			<th noWrap="true" colspan="3">管理成本</th>
			<th noWrap="true" colspan="3">医辅成本</th>
			</tr>
		  <tr noWrap="true" class="mainHead">  
		   
				<th noWrap="true" style="display:none">set_cfg_code  1</th>   <!--设置属性，隐藏列-->
				<th noWrap="true"  style="display:none">work_code 2</th>
				<th noWrap="true" style="display:none">work_name 3</th>
				<th noWrap="true" style="display:none">cost_class_code 4</th>            
				<!--th noWrap="true">成本分类 cost_class_name  5</th-->
				<th noWrap="true" style="display:none">dept_code 6</th>            
				<th noWrap="true" style="display:none">dept_name  7</th>
				<th noWrap="true">项目</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">项目</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">项目</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">项目</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">项目</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true" style="display:none">isout_flag  23</th>   <!--设置属性，隐藏列-->
				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test=" position()=5 ">
								<td>
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
							<xsl:when test=" position()=1 or  position()=2 or  position()=3 or  position()=4 or  position()=6 or position()=7 or  position()=23 ">
								<td>
									<xsl:attribute name="style">display:none</xsl:attribute>   <!--设置属性，隐藏列-->
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>

								<!--xsl:when test="position()=3 ">
								<td>
										<a href="#">
											<xsl:attribute name="onclick">
													openDialog('projcostcheck_by_deptcode.html?load=&lt;set_cfg_code&gt;<xsl:value-of select="../td[1]"/>&lt;/set_cfg_code&gt;&lt;dept_code&gt;<xsl:value-of select="../td[2]"/>&lt;/dept_code&gt;&lt;dept_name&gt;<xsl:value-of select="../td[3]"/>&lt;/dept_name&gt;', 'dialogWidth:1100px;dialogHeight:700px')
											</xsl:attribute>
											<xsl:value-of select="."/>		
										</a>							
								</td>	
								</xsl:when -->
								
								<xsl:when test="position()&gt;7 and position()&lt;23 ">
									<td>
										<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>	
									</td>	
								</xsl:when>
								
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
