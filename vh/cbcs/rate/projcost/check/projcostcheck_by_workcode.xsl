<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
			<tr noWrap="true" class="mainHead">  
			<th noWrap="true" rowspan="2" >作业</th>
			<th noWrap="true" colspan="3">总成本</th>
			<th noWrap="true" colspan="3">直接成本核算</th>
			<th noWrap="true" colspan="3">公用成本</th>
			<th noWrap="true" colspan="3">管理成本</th>
			<th noWrap="true" colspan="3">医辅成本</th>
			</tr>
		  <tr noWrap="true" class="mainHead">  
		   
				<th noWrap="true" style="display:none">set_cfg_code  1</th>   <!--设置属性，隐藏列-->
				<th noWrap="true"  style="display:none">work_code 2</th>
				<!--th noWrap="true" >作业 3</th-->
				<th noWrap="true" style="display:none">dept_code 4</th>            
				<th noWrap="true" style="display:none">dept_name  5</th>
				<th noWrap="true">项目</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">项目</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">项目</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">项目</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true">项目</th>
				<th noWrap="true">作业</th>
				<th noWrap="true">差额</th>
				<th noWrap="true" style="display:none">isout_flag  21</th>   <!--设置属性，隐藏列-->
				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<!--xsl:when test=" position()=3 ">
								<td>
									<xsl:value-of select="."/>
								</td>	
								</xsl:when-->
							<xsl:when test=" position()=2 or  position()=1 or  position()=4 or position()=5 or  position()=21 ">
								<td>
									<xsl:attribute name="style">display:none</xsl:attribute>   <!--设置属性，隐藏列-->
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>

								<xsl:when test="position()=3 and text()!='合计' ">
								<td>
										<a href="#">
											<xsl:attribute name="onclick">
													openDialog('projcostcheck_by_costclasscode.html?load=&lt;set_cfg_code&gt;<xsl:value-of select="../td[1]"/>&lt;/set_cfg_code&gt;&lt;work_code&gt;<xsl:value-of select="../td[2]"/>&lt;/work_code&gt;&lt;work_name&gt;<xsl:value-of select="../td[3]"/>&lt;/work_name&gt;&lt;dept_code&gt;<xsl:value-of select="../td[4]"/>&lt;/dept_code&gt;&lt;dept_name&gt;<xsl:value-of select="../td[5]"/>&lt;/dept_name&gt;&lt;isout_flag&gt;<xsl:value-of select="../td[21]"/>&lt;/isout_flag&gt;', 'dialogWidth:1100px;dialogHeight:700px')
											</xsl:attribute>
											<xsl:value-of select="."/>		
										</a>							
								</td>	
								</xsl:when>
								
								<xsl:when test="position()=3 and text()='合计' ">
								<td>
										<xsl:value-of select="."/>			
								</td>	
								</xsl:when>
								
								<xsl:when test="position()&gt;5 and position()&lt;21 ">
									<td>
										<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>	
									</td>	
								</xsl:when>
								
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
