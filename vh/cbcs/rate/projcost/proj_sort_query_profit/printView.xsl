<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/rate/projcost/proj_sort_query_profit/printView.xsl,v 1.1 2012/03/12 01:58:45 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:45 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
		  <colgroup>		       
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
			</colgroup>
	  	<thead>
				<tr noWrap="true" class="mainHead">
			    		<th noWrap="true">科室</th>
					<th noWrap="true">项目代码</th>
					<th noWrap="true">项目名称</th>
					<th noWrap="true">单位收入</th>             
					<th noWrap="true">工作量</th>
					<th noWrap="true">单位成本</th>
					<th noWrap="true">收益</th>
					<th noWrap="true">总收益</th>
					<th noWrap="true">占收益比重</th>		
				</tr>
	  	</thead>
	  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
								<xsl:when test="position()=1 or position()=2 or position()=3  ">
								<td>
									<xsl:value-of select="."/>
								</td>
								</xsl:when>	
								<xsl:when test="position()=9 ">
								<td class="numberText" align = 'right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>%
								</td>
								</xsl:when>	
								<xsl:otherwise>
					                		<td class="numberText" align = 'right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>	
					              		</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  		
		</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>
