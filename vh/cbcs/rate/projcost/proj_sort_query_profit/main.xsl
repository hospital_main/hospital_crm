<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
		  <tr noWrap="true" class="mainHead">  
		    		<th noWrap="true">科室</th>
				<th noWrap="true">项目代码</th>
				<th noWrap="true">项目名称</th>
				<th noWrap="true">单位收入</th>            
				<th noWrap="true">工作量</th>
				<th noWrap="true">单位成本</th>
				<th noWrap="true">收益</th>
				<th noWrap="true">总收益</th>
				<th noWrap="true">占收益比重</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
								<xsl:when test="position()=1 or position()=2 or position()=3 ">
								<td>
									<xsl:value-of select="."/> 
								</td>
								</xsl:when>
								<xsl:when test="position()=9">
								
									<td align = 'right'><xsl:value-of select="format-number(.,'#,##0.000000')"/>%</td>
							
								</xsl:when>
								<xsl:otherwise>
					                		<td align = 'right'><xsl:value-of select="format-number(.,'#,##0.000000')"/></td>	
					              		</xsl:otherwise>	
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
