<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
		  <tr noWrap="true" class="mainHead">
		    <th noWrap="true">项目代码</th>
				<th noWrap="true">项目名称</th>
				<th noWrap="true">单位收入</th>
				<th noWrap="true">单位成本</th>
				<th noWrap="true">单位收益</th>
				<th noWrap="true">成本收益率</th>
				<th noWrap="true">工作量</th>				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1 or position()=2">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=8 or position()=9">
							</xsl:when>
							<xsl:when test="position()=4">
								<td class="numberText">
									<xsl:attribute name="style">text-align:right</xsl:attribute>
									<a href="#">
										<xsl:attribute name="onclick"> 
											javascript:openDialog('../../../projcost/proj_cost_query/proj_cost_query_costclasscode1_avg.html?load=&lt;set_cfg_code&gt;<xsl:value-of select="../td[8]"/>&lt;/set_cfg_code&gt;&lt;charge_detail_code&gt;<xsl:value-of select="../td[1]"/>&lt;/charge_detail_code&gt;&lt;charge_detail_name&gt;<xsl:value-of select="../td[2]"/>&lt;/charge_detail_name&gt;&lt;year_month&gt;<xsl:value-of select="../td[9]"/>&lt;/year_month&gt;', 'dialogWidth:1100px;dialogHeight:700px')
										</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.0000')"/>
									</a>
								</td>	
							</xsl:when>
							<xsl:when test="position()=6">
								<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>	
							</xsl:when>
							<xsl:when test="position()=7">
								<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
                <td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.0000')"/></td>	
              </xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>