<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
		  <tr noWrap="true" class="mainHead">
		    <th noWrap="true">科室代码</th>
				<th noWrap="true">科室名称</th>
				<th noWrap="true">医疗代码</th>
				<th noWrap="true">医疗项目</th>
				<th noWrap="true">单位收入</th>
				<th noWrap="true">单位成本</th>
				<th noWrap="true">单位收益</th>
				<th noWrap="true">成本收益率</th>
				<th noWrap="true">工作量</th>				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
						<xsl:choose> 
							<xsl:when test="position()=1 or position()=2 or position()=3 or position()=4">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=10 or position()=11">
							</xsl:when>
							<xsl:when test="position()=6">
								<td class="numberText">
									<xsl:attribute name="style">text-align:right</xsl:attribute>
										<a href="#">
											<xsl:attribute name="onclick">
												javascript:openDialog('../../../projcost/proj_cost_query/proj_cost_query_costclasscode1.html?load=&lt;set_cfg_code&gt;<xsl:value-of select="../td[10]"/>&lt;/set_cfg_code&gt;&lt;charge_detail_code&gt;<xsl:value-of select="../td[3]"/>&lt;/charge_detail_code&gt;&lt;charge_detail_name&gt;<xsl:value-of select="../td[4]"/>&lt;/charge_detail_name&gt;&lt;dept_code&gt;<xsl:value-of select="../td[1]"/>&lt;/dept_code&gt;&lt;dept_name&gt;<xsl:value-of select="../td[2]"/>&lt;/dept_name&gt;&lt;year_month&gt;<xsl:value-of select="../td[11]"/>&lt;/year_month&gt;', 'dialogWidth:1100px;dialogHeight:700px')
											</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.0000')"/>
										</a>
									</td>	
							</xsl:when>
							<xsl:when test="position()=8">
								<td class="numberText"  align="right"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>	
							</xsl:when>
							<xsl:when test="position()=9">
								<td class="numberText"  align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>	
							</xsl:when>
							<xsl:otherwise>
                <td class="numberText"  align="right"><xsl:value-of select="format-number(.,'#,##0.0000')"/></td>	
              </xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
