<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/rate/projanaly/proj_income_analy/dept/printView.xsl,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:44 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
		  <colgroup>		       
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
				<col style = 'width:105mm'/>
			</colgroup>
	  	<thead>
				<tr noWrap="true" class="mainHead">
					<th noWrap="true">科室代码</th>
					<th noWrap="true">科室名称</th>
					<th noWrap="true">医疗代码</th>
					<th noWrap="true">医疗项目</th>
					<th noWrap="true">单位收入</th>
					<th noWrap="true">单位成本</th>
					<th noWrap="true">单位收益</th>
					<th noWrap="true">成本收益率</th>
					<th noWrap="true">工作量</th>			
				</tr>
	  	</thead>
	  	<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>     
						<xsl:for-each select="td">
							<xsl:choose> 
								<xsl:when test="position()=1 or position()=2 or position()=3 or position()=4">
									<td><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:when test="position()=10 or position()=11">
								</xsl:when>
								<xsl:when test="position()=6">
									<td class="numberText">
										<xsl:attribute name="style">text-align:center</xsl:attribute>
										<xsl:value-of select="."/>
									</td>	
								</xsl:when>
								<xsl:when test="position()=8">
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>	
								</xsl:when>
								<xsl:when test="position()=9">
									<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>	
								</xsl:when>
								<xsl:otherwise>
	                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.000000')"/></td>	
	              </xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>  	
			</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>
