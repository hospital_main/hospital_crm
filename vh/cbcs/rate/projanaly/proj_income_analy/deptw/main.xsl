<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
		  <tr noWrap="true" class="mainHead">
		    <th noWrap="true">科室代码</th>
				<th noWrap="true">科室名称</th>
				<th noWrap="true">医疗代码</th>
				<th noWrap="true">医疗项目</th> 
				<th noWrap="true">单位成本</th>  
				 			
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
						<xsl:choose> 
							<xsl:when test="position()=1 or position()=2 or position()=3 or position()=4">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
 
							<xsl:when test="position()=6 or position()=7 or position()=8">
							</xsl:when>								
							<xsl:otherwise>
                <td class="numberText"  align="right"><xsl:value-of select="format-number(.,'#,##0.0000')"/></td>	
              </xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
