<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
		 <colgroup>		       
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>
			<col style = 'width:105mm'/>

		</colgroup>
		<thead>            
		  <tr noWrap="true" class="mainHead">
					<th noWrap="true" rowspan="3" valign="middle">科室名称</th>
					<th noWrap="true" rowspan="3">项目</th>
					<th noWrap="true" rowspan="3">单位收入</th>
					
					<th noWrap="true" colspan="8">单位成本</th>
					<th noWrap="true" style="display:none"/>
					<th noWrap="true" style="display:none"/>
					<th noWrap="true" style="display:none"/>
					<th noWrap="true" style="display:none"/>
					<th noWrap="true" style="display:none"/>
					<th noWrap="true" style="display:none"/>
					<th noWrap="true" style="display:none"/>
					
					<th noWrap="true" rowspan="3">单位收益</th>
					<th noWrap="true" colspan="2">业务量</th>
					<th noWrap="true" style="display:none"/>
				</tr>
				<tr noWrap="true" class="mainHead">
					<th noWrap="true" style="display:none">科室名称</th>
					<th noWrap="true" style="display:none">项目</th>
					<th noWrap="true" style="display:none">单位收入</th>
					
					<th noWrap="true" rowspan="2">单位成本</th>
					<th noWrap="true" colspan="2">直接成本</th>
					<th noWrap="true" style="display:none"></th>
					<th noWrap="true" colspan="5">间接成本</th>
					<th noWrap="true" style="display:none"></th>
					<th noWrap="true" style="display:none"></th>
					<th noWrap="true" style="display:none"></th>
					<th noWrap="true" style="display:none"></th>
					
					<th noWrap="true" style="display:none"></th>
					<th noWrap="true" rowspan="2">业务量</th>
					<th noWrap="true" rowspan="2">占比</th>
				</tr>
				<tr noWrap="true" class="mainHead">
					<th noWrap="true" style="display:none">科室名称</th>
					<th noWrap="true" style="display:none">项目</th>
					<th noWrap="true" style="display:none">单位收入</th>
					
					<th noWrap="true"  style="display:none">单位成本</th>
					<th noWrap="true">成本</th>
					<th noWrap="true">占比</th>
					<th noWrap="true">合计</th>
					<th noWrap="true">占比</th>
					<th noWrap="true">分摊公用</th>
					<th noWrap="true">分摊管理</th>
					<th noWrap="true">分摊医辅</th>
					
					<th noWrap="true" style="display:none">cc</th>
					<th noWrap="true"  style="display:none">dd</th>
					<th noWrap="true"  style="display:none">dd</th>
				</tr>

			 

		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
 <xsl:choose>
								<xsl:when test="position()=1 or position()=3 ">
									 
								</xsl:when>
								<xsl:when test="position()=2 or position()=4 ">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select='format-number(.," #,##0.00 ")'/>
									</td>
								</xsl:otherwise>

</xsl:choose>
								
						 

	 
							 
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
