<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>            
		  <tr noWrap="true" class="mainHead">  
				<th noWrap="true" style="display:none">dept_code  1</th>   <!--设置属性，隐藏列-->
				<th noWrap="true" rowspan="3"  valign="middle">科室名称</th>  
				<th noWrap="true" style="display:none">charge_detail_code  2</th>          
				<th noWrap="true" rowspan="3"  valign="middle">项目</th>
				<th noWrap="true" rowspan="3"  valign="middle">单位收入</th>
				<th noWrap="true" colspan="8"  valign="middle">单位成本</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
				
				<th noWrap="true" rowspan="4"  valign="middle">单位收益</th>
				<th noWrap="true" colspan="2"  valign="middle">业务量</th>
				<th noWrap="true" style="display:none"></th>
				<th noWrap="true" style="display:none"></th>
			</tr>
			<tr noWrap="true" class="mainHead">  
				<th noWrap="true" style="display:none">dept_code  1</th>   <!--设置属性，隐藏列-->
				<th noWrap="true" style="display:none"  rowspan="2">科室名称</th>            
				<th noWrap="true" style="display:none">charge_detail_code  2</th>   
				<th noWrap="true" style="display:none">项目</th>
				<th noWrap="true" style="display:none">单位收入</th>

				<th noWrap="true" rowspan="2">单位成本</th>
				<th noWrap="true" colspan="2">直接成本</th>
				<th noWrap="true" style="display:none">成本</th>
				<th noWrap="true" style="display:none">占比</th>
				<th noWrap="true" colspan="5">间接成本</th>
				<th noWrap="true" style="display:none">成本</th>
				<th noWrap="true" style="display:none">占比</th>
				<th noWrap="true" rowspan="2"  valign="middle">业务量</th>
				<th noWrap="true" rowspan="2"  valign="middle">占比</th>
			</tr>
			<tr noWrap="true" class="mainHead">  
				<th noWrap="true" style="display:none">dept_code  1</th>   <!--设置属性，隐藏列-->
				<th noWrap="true" style="display:none">科室名称</th>            
				<th noWrap="true" style="display:none">charge_detail_code  2</th>   
				<th noWrap="true" style="display:none">项目</th>
				<th noWrap="true" style="display:none">单位收入</th>

				<th noWrap="true">成本</th>
				<th noWrap="true">占比</th>
								
				<th noWrap="true">合计</th>
				<th noWrap="true">占比</th>
				<th noWrap="true" >分摊公用</th>
				<th noWrap="true" >分摊管理</th>
				<th noWrap="true" >分摊医辅</th>
									
			</tr>

		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>     
					<xsl:for-each select="td">
							<xsl:choose> 
							<xsl:when test="position()=1 or position()=3 ">
								<td>
									<xsl:attribute name="style">display:none</xsl:attribute>   <!--设置属性，隐藏列-->
									<xsl:value-of select="."/>
								</td>	
								</xsl:when>
								<xsl:when test="position()=8 or position()=10 or position()=16">
								<td align='right'>
										<xsl:value-of select="format-number(.,' #,##0.00%')"/>
									</td>
								</xsl:when>	
									
								<xsl:when test="position()=2 or position()=4 ">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td align='right'>
										<xsl:value-of select='format-number(.," #,##0.00")'/>
									</td>
								</xsl:otherwise>

	 
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>  	
		</tbody>
	</xsl:template>
</xsl:stylesheet>
