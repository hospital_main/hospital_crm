<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="lzk_hc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<tr>
			<td style="background-color:#FCB724;color:#472300;font-weight: bold;font-family:'宋体'" align="center" rowspan="2">成本分类</td>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $lzk_hc]">
				<td style="background-color:#FCB724;color:#472300;font-weight: bold;font-family:'宋体'" align="center" colspan="2">
					<xsl:value-of select="td[2]"/>
				</td>
				<td style="display:none"></td>
			</xsl:for-each>
		</tr>
		<tr>
			<td bgcolor="#FFDDA6" align="center" style="display:none">成本项目</td>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $lzk_hc]">
				<td style="background-color:#FCB724;color:#472300;font-weight: bold;font-family:'宋体'" align="center">
					单位成本
				</td>
				<td style="background-color:#FCB724;color:#472300;font-weight: bold;font-family:'宋体'" align="center">
					成本构成
				</td>
			</xsl:for-each>
		</tr>
	</thead>
	<tbody>
		<xsl:variable name="lzk_bc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<xsl:variable name="lzk_opt" select="/root/tbody/tr[1]/td[5]"/>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:if test="( position() mod $lzk_bc )=1 or $lzk_bc = 1">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;
				</xsl:text>
				<td>
					<xsl:value-of select="td[1]"/>
				</td>
			</xsl:if>
				<td align="right">
				<xsl:value-of select="format-number(td[7],'#,##0.000000')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[8],'#,##0.00%')"/>
				</td>
			<xsl:if test="(position() mod $lzk_bc)=0 or $lzk_bc = 1">
				<xsl:text disable-output-escaping="yes">
					&lt;/tr&gt;
				</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

