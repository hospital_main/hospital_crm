<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costing/plancostinput/planCostInputSub.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

 <%
            String before_dept_name = (String)request.getParameter("dept_name");
            String dept_name = before_dept_name;//new String(before_dept_name.getBytes("GBK"),"utf-8");

          %>
<html:html clazz="child">
<form name="template" method="post"  action="planCostInput.jspviewhigh">
	  <html:message/>
      <tr><input type=text name="dept_name" readonly class="textInputC" value="<%=dept_name%>" style='border:none' /> </tr>
      <html:title clazz='module'>预算成本明细</html:title>

	<html:table clazz="complex"  divWidth="750">
     <tr>
      <td valign="top">
           <html:table  clazz="result" divWidth="700">
	        <html:tr  clazz='label'>
            <td class="resultLabel">项目代码</td>
            <td class="resultLabel">项目名称</td>
	        <td class="resultLabel">全成本预算</td>
	        <td class="resultLabel">直接成本预算</td>
	        </html:tr>
          <%
            DecimalFormat nf = new DecimalFormat("#,##0.00");
            String[][] result = (String[][]) request.getAttribute( "table_result" );
            if (result!=null) {
              for (int i=0; i<result.length; i++) {
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
          %>
          <tr CLASS="<%=rowColor%>">
            <td class="normalText">
          <%    if(result[i][2].equals("Y") ){ %>
              <a onclick="changeAddButton()" href="planCostInput.jspviewhigh?subFunction=findAll&subjcode=<%=result[i][0]%>&yearmonth=<%=request.getParameter("year")%>&deptcode=<%=request.getParameter("dept_code")%>"  target="Year_Iframe"> <%}%>

              <%=result[i][0]%><% if(result[i][2].equals("Y") ){%></a><%}%></td>
            <td class="normalText"><%=result[i][1]%></td>
            <td class="numberText" align="right"><%=nf.format(Double.parseDouble(result[i][4]))%></td>
            <td class="numberText" align="right"><%=nf.format(Double.parseDouble(result[i][5]))%></td>
          </tr>
          <%
              }
            }
          %>
	      </html:table>
      </td>
      <td>&nbsp;</td>
      <td valign="top">
	      <html:table clazz="complex">
          <tr>
            <td COLSPAN="2" VLAIGN="top" >
              <iframe  VSPACE="1" HSPACE="1" FRAMEBORDER="0" WIDTH="520" SRC="" NAME="Year_Iframe" HEIGHT="445" ></iframe>
            </td>
          </tr>
	      </html:table>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center"><button class="pageBtn" id="addButton" onclick="Year_Iframe.save();changeAddButtonToDisabled();" disabled = true>添加</button>
      <button class="pageBtn" onclick="window.close();" >关闭</button> 
        <!--<img src="images/create.gif" style='cursor:hand' onclick="opener.template.submit();window.close();" />
        <img src="images/close.gif" class="mouse" onclick="window.close();" />-->
      </td>
    </tr>
	</html:table>

  <input type=hidden name="subFunction" value='subFindAll'/>
  <input type=hidden name="year" value="<%=request.getParameter("year")%>"/>
  <input type=hidden name="dept_code" value="<%=request.getParameter("dept_code")%>"/>
  <input type=hidden name="subjcode" />

</form>
<Script Language="JavaScript">
	function changeAddButton(){
		document.getElementById("addButton").disabled = false;
		}
		function changeAddButtonToDisabled(){
			document.getElementById("addButton").disabled = true;
			}
	</Script>
</html:html>