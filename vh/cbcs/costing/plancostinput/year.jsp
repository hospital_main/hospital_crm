<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costing/plancostinput/year.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="java.text.*" %>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    switch(isDouble(template.Jan,12,2))
    {
      case 0 : alert('一月金额必须为数字型'); return;
      case 1 : alert('一月金额整数部分不能高于12个字符'); return;
      case 2 : alert('一月金额没有整数部分'); return;
      case 3 : alert('一月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Feb,12,2))
    {
      case 0 : alert('二月金额必须为数字型'); return;
      case 1 : alert('二月金额整数部分不能高于12个字符'); return;
      case 2 : alert('二月金额没有整数部分'); return;
      case 3 : alert('二月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Mar,12,2))
    {
      case 0 : alert('三月金额必须为数字型'); return;
      case 1 : alert('三月金额整数部分不能高于12个字符'); return;
      case 2 : alert('三月金额没有整数部分'); return;
      case 3 : alert('三月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Apr,12,2))
    {
      case 0 : alert('四月金额必须为数字型'); return;
      case 1 : alert('四月金额整数部分不能高于12个字符'); return;
      case 2 : alert('四月金额没有整数部分'); return;
      case 3 : alert('四月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.May,12,2))
    {
      case 0 : alert('五月金额必须为数字型'); return;
      case 1 : alert('五月金额整数部分不能高于12个字符'); return;
      case 2 : alert('五月金额没有整数部分'); return;
      case 3 : alert('五月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Jun,12,2))
    {
      case 0 : alert('六月金额必须为数字型'); return;
      case 1 : alert('六月金额整数部分不能高于12个字符'); return;
      case 2 : alert('六月金额没有整数部分'); return;
      case 3 : alert('六月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Jul,12,2))
    {
      case 0 : alert('七月金额必须为数字型'); return;
      case 1 : alert('七月金额整数部分不能高于12个字符'); return;
      case 2 : alert('七月金额没有整数部分'); return;
      case 3 : alert('七月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Aug,12,2))
    {
      case 0 : alert('八月金额必须为数字型'); return;
      case 1 : alert('八月金额整数部分不能高于12个字符'); return;
      case 2 : alert('八月金额没有整数部分'); return;
      case 3 : alert('八月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Sep,12,2))
    {
      case 0 : alert('九月金额必须为数字型'); return;
      case 1 : alert('九月金额整数部分不能高于12个字符'); return;
      case 2 : alert('九月金额没有整数部分'); return;
      case 3 : alert('九月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Oct,12,2))
    {
      case 0 : alert('十月金额必须为数字型'); return;
      case 1 : alert('十月金额整数部分不能高于12个字符'); return;
      case 2 : alert('十月金额没有整数部分'); return;
      case 3 : alert('十月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Nov,12,2))
    {
      case 0 : alert('十一月金额必须为数字型'); return;
      case 1 : alert('十一月金额整数部分不能高于12个字符'); return;
      case 2 : alert('十一月金额没有整数部分'); return;
      case 3 : alert('十一月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Dec,12,2))
    {
      case 0 : alert('十二月金额必须为数字型'); return;
      case 1 : alert('十二月金额整数部分不能高于12个字符'); return;
      case 2 : alert('十二月金额没有整数部分'); return;
      case 3 : alert('十二月金额小数部分不能高于2个字符'); return;
    }
    var Jan=new Number(template.Jan.value);
    var Feb=new Number(template.Feb.value);
    var May=new Number(template.May.value);
    var Apr=new Number(template.Apr.value);
    var Mar=new Number(template.Mar.value);
    var Jun=new Number(template.Jun.value);
    var Jul=new Number(template.Jul.value);
    var Aug=new Number(template.Aug.value);
    var Sep=new Number(template.Sep.value);
    var Oct=new Number(template.Oct.value);
    var Nov=new Number(template.Nov.value);
    var Dec=new Number(template.Dec.value);
    var temp;
    template.Sum.value=Jan+Feb+Mar+Apr+May+Jun+Jul+Aug+Sep+Oct+Nov+Dec;
    template.subFunction.value='save';
    template.yearmonth.value=parent.template.year.value;
    template.deptcode.value=parent.template.dept_code.value;
    
    switch(isDouble(template.Jan1,12,2))
    {
      case 0 : alert('一月金额必须为数字型'); return;
      case 1 : alert('一月金额整数部分不能高于12个字符'); return;
      case 2 : alert('一月金额没有整数部分'); return;
      case 3 : alert('一月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Feb1,12,2))
    {
      case 0 : alert('二月金额必须为数字型'); return;
      case 1 : alert('二月金额整数部分不能高于12个字符'); return;
      case 2 : alert('二月金额没有整数部分'); return;
      case 3 : alert('二月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Mar1,12,2))
    {
      case 0 : alert('三月金额必须为数字型'); return;
      case 1 : alert('三月金额整数部分不能高于12个字符'); return;
      case 2 : alert('三月金额没有整数部分'); return;
      case 3 : alert('三月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Apr1,12,2))
    {
      case 0 : alert('四月金额必须为数字型'); return;
      case 1 : alert('四月金额整数部分不能高于12个字符'); return;
      case 2 : alert('四月金额没有整数部分'); return;
      case 3 : alert('四月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.May1,12,2))
    {
      case 0 : alert('五月金额必须为数字型'); return;
      case 1 : alert('五月金额整数部分不能高于12个字符'); return;
      case 2 : alert('五月金额没有整数部分'); return;
      case 3 : alert('五月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Jun1,12,2))
    {
      case 0 : alert('六月金额必须为数字型'); return;
      case 1 : alert('六月金额整数部分不能高于12个字符'); return;
      case 2 : alert('六月金额没有整数部分'); return;
      case 3 : alert('六月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Jul1,12,2))
    {
      case 0 : alert('七月金额必须为数字型'); return;
      case 1 : alert('七月金额整数部分不能高于12个字符'); return;
      case 2 : alert('七月金额没有整数部分'); return;
      case 3 : alert('七月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Aug1,12,2))
    {
      case 0 : alert('八月金额必须为数字型'); return;
      case 1 : alert('八月金额整数部分不能高于12个字符'); return;
      case 2 : alert('八月金额没有整数部分'); return;
      case 3 : alert('八月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Sep1,12,2))
    {
      case 0 : alert('九月金额必须为数字型'); return;
      case 1 : alert('九月金额整数部分不能高于12个字符'); return;
      case 2 : alert('九月金额没有整数部分'); return;
      case 3 : alert('九月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Oct1,12,2))
    {
      case 0 : alert('十月金额必须为数字型'); return;
      case 1 : alert('十月金额整数部分不能高于12个字符'); return;
      case 2 : alert('十月金额没有整数部分'); return;
      case 3 : alert('十月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Nov1,12,2))
    {
      case 0 : alert('十一月金额必须为数字型'); return;
      case 1 : alert('十一月金额整数部分不能高于12个字符'); return;
      case 2 : alert('十一月金额没有整数部分'); return;
      case 3 : alert('十一月金额小数部分不能高于2个字符'); return;
    }
    switch(isDouble(template.Dec1,12,2))
    {
      case 0 : alert('十二月金额必须为数字型'); return;
      case 1 : alert('十二月金额整数部分不能高于12个字符'); return;
      case 2 : alert('十二月金额没有整数部分'); return;
      case 3 : alert('十二月金额小数部分不能高于2个字符'); return;
    }
    var Jan1=new Number(template.Jan1.value);
    var Feb1=new Number(template.Feb1.value);
    var May1=new Number(template.May1.value);
    var Apr1=new Number(template.Apr1.value);
    var Mar1=new Number(template.Mar1.value);
    var Jun1=new Number(template.Jun1.value);
    var Jul1=new Number(template.Jul1.value);
    var Aug1=new Number(template.Aug1.value);
    var Sep1=new Number(template.Sep1.value);
    var Oct1=new Number(template.Oct1.value);
    var Nov1=new Number(template.Nov1.value);
    var Dec1=new Number(template.Dec1.value);
    var temp1;
    template.Sum1.value=Jan1+Feb1+Mar1+Apr1+May1+Jun1+Jul1+Aug1+Sep1+Oct1+Nov1+Dec1;
    
    template.subFunction.value='save';
    template.yearmonth.value=parent.template.year.value;
    template.deptcode.value=parent.template.dept_code.value;
    
    show_wait();
    template.submit();
    return true;
  }
  function apportion()
  {
   if(template.Sum.value != "")
    {
	    switch(isDouble(template.Sum,16,2))
	    {
	      case 0 : alert('合计金额必须为数字型'); return;
	      case 1 : alert('合计金额整数部分不能高于16个字符'); return;
	      case 2 : alert('合计金额没有整数部分'); return;
	      case 3 : alert('合计金额小数部分不能高于2个字符'); return;
	    }
	    var number = new Number(template.Sum.value);
	    var temp=roundOff(number/12, 2);
	    template.Jan.value=temp;
	    template.Feb.value=temp;
	    template.Mar.value=temp;
	    template.Apr.value=temp;
	    template.May.value=temp;
	    template.Jun.value=temp;
	    template.Jul.value=temp;
	    template.Aug.value=temp;
	    template.Sep.value=temp;
	    template.Oct.value=temp;
	    template.Nov.value=temp;
	    template.Dec.value=roundOff(number-temp*11, 2);
	 }
  
    
    if(template.Sum1.value != "")
     {
	    switch(isDouble(template.Sum1,16,2))
	    {
	      case 0 : alert('合计金额必须为数字型'); return;
	      case 1 : alert('合计金额整数部分不能高于16个字符'); return;
	      case 2 : alert('合计金额没有整数部分'); return;
	      case 3 : alert('合计金额小数部分不能高于2个字符'); return;
	    }
	    var number1 = new Number(template.Sum1.value);
	    var temp1=roundOff(number1/12, 2);
	    template.Jan1.value=temp1;
	    template.Feb1.value=temp1;
	    template.Mar1.value=temp1;
	    template.Apr1.value=temp1;
	    template.May1.value=temp1;
	    template.Jun1.value=temp1;
	    template.Jul1.value=temp1;
	    template.Aug1.value=temp1;
	    template.Sep1.value=temp1;
	    template.Oct1.value=temp1;
	    template.Nov1.value=temp1;
	    template.Dec1.value=roundOff(number1-temp1*11, 2);
     }
    return true;
  }

</Script>
<html:html clazz="child">
<form name="template" method="post" action="planCostInput.jspviewhigh">
	 <html:message/>
	  <html:table clazz="complex" >
    <%
      DecimalFormat nf = new DecimalFormat("##0.00");
      String[][] temp=(String[][])request.getAttribute( "table" );
      String[][] result = null;
      if(temp==null){
        String[][] resultTemp={{"","","","0"},{"","","","0"},{"","","","0"},{"","","","0"},{"","","","0"},{"","","","0"},{"","","","0"},{"","","","0"},{"","","","0"},{"","","","0"},{"","","","0"},{"","","","0"}};
        result = resultTemp;
      }
      else
        result = (String[][])request.getAttribute( "table" );
    %>
    <!-- 
      <tr><input type=text name="subj_name" readonly class="textInputC"  <% if(result!=null) out.println("value="+result[0][3]);%> style='border:none'/> </tr>
    <tr> <td><input type=text name="subj_name" readonly class="textInputC"  <%= request.getAttribute("subjname") %> </td></tr>  
     -->
  
 <tr>
 <td>
 <html:table clazz="result" divWidth="500" divHeight="353">
	  <html:tr clazz='label'>
      <td class="resultLabel" style="border-left:1px solid gray">月份 </td>
      <td class="resultLabel">全成本预算</td>
      <td class="resultLabel">直接成本预算</td>
	        </html:tr> 
    <tr>
      <td class="normalText" width="50px" style="border-left:1px solid gray" nowrap="nowrap">1</td>
      <td width="40%" class="numberlText" nowrap="nowrap"><input type=text  name="Jan" value="<%=nf.format(Double.parseDouble(result[0][3]))%>" class="textInputC"/></td>
     <td width="40%" class="numberlText" nowrap="nowrap"><input type=text width="40%"name="Jan1" value="<%=nf.format(Double.parseDouble(result[0][5]))%>" class="textInputC"/></td>
    
  </tr>
    <tr>
      <td class="normalText" style="border-left:1px solid gray" nowrap="nowrap">2</td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Feb" value="<%=nf.format(Double.parseDouble(result[1][3]))%>" class="textInputC"/></td>
       <td class="numberlText" nowrap="nowrap"><input type=text name="Feb1" value="<%=nf.format(Double.parseDouble(result[1][5]))%>" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="normalText" style="border-left:1px solid gray" nowrap="nowrap">3</td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Mar" value="<%=nf.format(Double.parseDouble(result[2][3]))%>" class="textInputC"/></td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Mar1" value="<%=nf.format(Double.parseDouble(result[2][5]))%>" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="normalText" style="border-left:1px solid gray" nowrap="nowrap">4</td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Apr" value="<%=nf.format(Double.parseDouble(result[3][3]))%>" class="textInputC"/></td>
     <td class="numberlText" nowrap="nowrap"><input type=text name="Apr1" value="<%=nf.format(Double.parseDouble(result[3][5]))%>" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="normalText"  style="border-left:1px solid gray" nowrap="nowrap">5</td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="May" value="<%=nf.format(Double.parseDouble(result[4][3]))%>" class="textInputC"/></td>
     <td class="numberlText" nowrap="nowrap"><input type=text name="May1" value="<%=nf.format(Double.parseDouble(result[4][5]))%>" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="normalText" style="border-left:1px solid gray" nowrap="nowrap">6</td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Jun" value="<%=nf.format(Double.parseDouble(result[5][3]))%>" class="textInputC"/></td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Jun1" value="<%=nf.format(Double.parseDouble(result[5][5]))%>" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="normalText"  style="border-left:1px solid gray" nowrap="nowrap">7</td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Jul" value="<%=nf.format(Double.parseDouble(result[6][3]))%>" class="textInputC"/></td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Jul1" value="<%=nf.format(Double.parseDouble(result[6][5]))%>" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="normalText"  style="border-left:1px solid gray" nowrap="nowrap">8</td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Aug" value="<%=nf.format(Double.parseDouble(result[7][3]))%>" class="textInputC"/></td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Aug1" value="<%=nf.format(Double.parseDouble(result[7][5]))%>" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="normalText" style="border-left:1px solid gray" nowrap="nowrap">9</td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Sep" value="<%=nf.format(Double.parseDouble(result[8][3]))%>" class="textInputC"/></td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Sep1" value="<%=nf.format(Double.parseDouble(result[8][5]))%>" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="normalText" style="border-left:1px solid gray" nowrap="nowrap">10</td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Oct" value="<%=nf.format(Double.parseDouble(result[9][3]))%>" class="textInputC"/></td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Oct1" value="<%=nf.format(Double.parseDouble(result[9][5]))%>" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="normalText" style="border-left:1px solid gray" nowrap="nowrap">11</td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Nov" value="<%=nf.format(Double.parseDouble(result[10][3]))%>" class="textInputC"/></td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Nov1" value="<%=nf.format(Double.parseDouble(result[10][5]))%>" class="textInputC"/></td>
    </tr>
    <tr>
      <td class="normalText" style="border-left:1px solid gray;" nowrap="nowrap">12</td>
      <td class="numberlText" nowrap="nowrap"><input type=text  name="Dec" value="<%=nf.format(Double.parseDouble(result[11][3]))%>" class="textInputC"/></td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Dec1" value="<%=nf.format(Double.parseDouble(result[11][5]))%>" class="textInputC"/></td>
    </tr>
     </html:table>
    <tr style="display:none">
      <td class="normalText" nowrap="nowrap">合计</td>
      <td class="numberlText" nowrap="nowrap"><input type=text name="Sum" class="textInputC"/></td>
       <td class="numberlText" nowrap="nowrap"><input type=text name="Sum1" class="textInputC"/></td>
    </tr>
	   
	      <td>
	      </tr>
  <tr>
    <td colspan="2">
    <button class="pageBtn" onclick="return save();">保存</button>
	<button class="pageBtn" onclick="return apportion();">评分</button>  
    <!--  <img src="images/save.gif" style='cursor:hand' onclick="return save();" />
      <img src="images/split.gif" style='cursor:hand' onclick="return apportion();" />-->
    </td>
  </tr>
	      </html:table>
  <input type=hidden name="subFunction" value='findAll'/>
  <input type=hidden name="yearmonth" value="<%=request.getParameter("yearmonth")%>" />
  <input type=hidden name="deptcode" value="<%=request.getParameter("deptcode")%>"/>
  <input type=hidden name="subjcode" value="<%=request.getParameter("subjcode")%>" />
</form>

<script language='javascript'>
  parent.template.subjcode.value='<%=request.getParameter("subjcode")%>';
  <%
  if (request.getParameter("subFunction")!=null && request.getParameter("subFunction").trim().equals("save"))
    out.println("parent.document.template.submit();");
  %>
</script>
</html:html>