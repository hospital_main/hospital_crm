<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costing/plancostinput/planCostInput.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:57:35 $
     $Modtime: 03-09-02 16:14 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.base.mvc.view.component.*,com.viewhigh.cbcs.cbcs.util.DisplayWidth,
  com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>
<%
     DecimalFormat nf = new DecimalFormat("#,##0.00");
     String[][] result = (String[][])request.getAttribute( "table_result" );
     String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function find() {
    if(template.year_find.value=="")
    {
      alert('请先选择会计年度!');
      return;
    }

    template.subFunction.value='supFindAll';
    template.submit();
    show_wait();
    return true;
  }
  function win(deptcode,deptname){
  var win=window.open("planCostInput.jspviewhigh?subFunction=subFindAll&year="+template.YEAR.value+"&dept_code="+deptcode+"&dept_name="+deptname,"","width=800,height=580,  top=100, left=100, scrollbars=1, resizable=0");
  }
</Script>
<html:html clazz="main" fixRows="1">
<form name="template" method="post" action="planCostInput.jspviewhigh">
  <!-- 返回信息栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>成本预算</html:title>
  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText">会计年度：</td>
      <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year_find",request.getParameter("year_find"),false,false)%></td>
      <td nowrap class="signText">科室代码：</td>
      <% String findCondition = request.getParameter("findCon");%>
      <td><input type=text name="findCon" class="textInputC" <%if(findCondition != null){ out.println(" value=" + findCondition);}%>></td>
      <td colspan="2" align="right"> <button class="pageBtn" onclick="return find()" >查询</button></td>
    </tr>
	  </html:table>

  <br>
          <html:title clazz='table'>成本预算</html:title>
	   <vh:vhFixTable fixRow=1 fixCol=0>
	      <table  width="100%" class="resultSetTable">
		    <colgroup id=tg>
          
           <col style = 'width:54mm' >
           
          <col style = 'width:54mm' >
          
          <col style = 'width:55mm' >
          
          <col style = 'width:55mm' >
          
        </colgroup>	            
	        <tr class="resultLabel">
            <td nowrap class="resultLabel">科室代码</td>
            <td nowrap class="resultLabel">科室名称</td>
            <td nowrap class="resultLabel">全成本预算</td>
            <td nowrap class="resultLabel">直接成本预算</td>
	        </tr>
	        
          <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]!=null && result[i][j]=="") result[i][j]="&nbsp;";
                }
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
          %>
          <tr CLASS="<%=rowColor%>">
            <td class="normalText">
              <%    if(result[i][2].equals("Y") ){%>
              <a href="javascript:win('<%=result[i][0]%>','<%=result[i][1]%>');"> <%}%>
              <%=result[i][0]%><% if(result[i][2].equals("Y") ){%></a><%}%></td>
            <td class="normalText"><%=result[i][1]%></td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][4]))%></td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][5]))%></td>
          </tr>
          <%
              }
            }
          %>
			     </table>
</vh:vhFixTable>

<input type=hidden name="YEAR" value="<%=request.getParameter("year_find")%>"/>
<input type=hidden name="subFunction" value="supFindAll"/>
</form>
</html:html>
