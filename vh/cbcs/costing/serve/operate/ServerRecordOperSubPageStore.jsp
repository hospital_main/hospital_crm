<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costing/serve/operate/ServerRecordOperSubPageStore.jsp,v 1.2 2014/03/14 00:42:29 yuchengying Exp $
 $Author: yuchengying $
 $Date: 2014/03/14 00:42:29 $
 $Revision: 1.2 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<%
  boolean ischeckout=Boolean.valueOf(request.getParameter("checkout")).booleanValue();
  String[] result = (String[])request.getAttribute("result");
%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function getAll2(){
    var num1 = template.stuffunitprice.value;
    var num2 = template.use.value;
    document.template.usetotal.value = roundOff(num1*num2,2);
  }

  function store(){
    if(trim(template.use.value)==""){
      alert('使用量不能为空');
      return;
    }
    switch(isDouble(template.use,7,2))
    {
      case 0 : alert('使用量必须为数字型'); return;
      case 1 : alert('使用量整数部分不能高于7个字符'); return;
      case 2 : alert('使用量没有整数部分'); return;
      case 3 : alert('使用量小数部分不能高于2个字符'); return;
    }
    template.date.value = window.opener.template.date.value;
    template.benefit_dept.value = window.opener.template.benefit_dept.value;
    template.serve_no.value = window.opener.template.serve_no.value;
    template.subFunction.value='subPageStore';    
    show_wait()
    template.submit();
    window.close();
    return true;
    
  }

  <%
  String serve_no = request.getAttribute("serve_no")==null?"":(String)request.getAttribute("serve_no");
  String appError = request.getAttribute("appError")==null?"":(String)request.getAttribute("appError");
  if (serve_no!=null && !serve_no.trim().equals("") && (appError==null || appError.trim().equals(""))) {
    out.println("window.opener.template.serve_no.value='"+serve_no+"';");
    out.println("window.opener.template.submit();");
    out.println("window.close();");
  }
  %>
</Script>
<html:html clazz="child">
<form name="template" method="post" action="costingRecord.jspviewhigh">

  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>材料明细修改页面</html:title>

	  <html:table clazz="complex">
    <tr>
      <td>

	  <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap="nowrap">使用材料：</td>
          <td class="normalText" nowrap="nowrap"><input class='textInputB1' type="text" name="stuff" readonly  style='border:none' <%if( result!= null){ out.println(" value='" + result[1]+"'");}%>></td>
            <input type="hidden" name="stuffcode" <%if( result!= null){ out.println(" value='" + result[0]+"'");}%> >
          <td class="signText" nowrap="nowrap" id='stuff1'>单位：</td>
          <td class="normalText" nowrap="nowrap" id='stuff2'><input class='textInputB1' type="text" name="stuffunit" readonly  style='border:none' <%if( result!= null){ out.println(" value='" + result[2]+"'");}%>></td>
          <td class="signText" nowrap="nowrap" id='stuff3'>单价：</td>
          <td class="normalText" nowrap="nowrap" id='stuff4'><input class='textInputB1' type="text" name="stuffunitprice" <%if( result!= null){ out.println(" value='" + result[3]+"'");}%> onkeyup="getAll2()"></td>
        </tr>
        <tr>
          <td class='signText' nowrap id='stuff5'>使用量：</td>
          <td class='normalText' nowrap id='stuff6'><input class='textInputB1' type="text" name="use" <%if( result!= null){ out.println(" value='" + result[4]+"'");}%> onkeyup="getAll2()"></td>
          <td class='signText' nowrap id='stuff7'>材料费合计：</td>
          <td class='normalText' nowrap id='stuff8'><input class='textInputB1' type="text" name="usetotal" readonly <%if( result!= null){ out.println(" value='" + result[5]+"'");}%> style='border:none'></td>
          <td class='signText' nowrap id='stuff9'>材料费对应科目：</td>
          <td class='normalText' nowrap id='stuff10'>
            <input class='textInputB1' type="text" readonly <%if( result!= null){ out.println(" value='" + result[7]+"'");}%> style='border:none'>
            <input type="hidden" name="subj" <%if( result!= null){ out.println(" value='" + result[6]+"'");}%> >
          </td>
        </tr>
         <tr>
      <td colspan="6" align="left">　　　　     　　　　　　　　　　　　　　　　　　  
        <%if(!ischeckout){%>
        <button class="pageBtn" onclick="return store();">保存</button> 
        <!--<img src="images/save.gif" style='cursor:hand' onclick="return store();">-->
        <%}%>
         <button class="pageBtn" onclick="window.close();">关闭</button></td>
    </tr>
    <tr height="300">
    <td></td>
    </tr>
	  </html:table>
      </td>
    </tr>
    
	  </html:table>
  <input type=hidden name="subFunction"/>
  <input type=hidden name="serve_no" value=""/>
  <input type=hidden name="date" value="<%=request.getParameter("date")%>"/>
  <input type=hidden name="serve_dept" value="<%=request.getParameter("serve_dept")%>"/>
  <input type=hidden name="benefit_dept" value="<%=request.getParameter("benefit_dept")%>"/>
  <input type=hidden name="serSubj" value="<%=request.getParameter("serSubj")%>"/>
  <input type=hidden name="seramt" value="<%=request.getParameter("seramt")%>"/>
  <input type=hidden name="seramttotal" value="<%=request.getParameter("seramttotal")%>"/>
  <input type=hidden name="abstrac" value="<%=request.getParameter("abstrac")%>"/>
  <input type="hidden" name="writerid" value="<%=request.getParameter("writerid")%>"/>
</form>
</html:html>
