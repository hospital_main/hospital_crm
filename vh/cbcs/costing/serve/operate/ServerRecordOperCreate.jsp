<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costing/serve/operate/ServerRecordOperCreate.jsp,v 1.3 2014/03/13 04:01:56 yuchengying Exp $
 $Author: yuchengying $
 $Date: 2014/03/13 04:01:56 $
 $Revision: 1.3 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect"%>
<!--
  未做完
    1. "使用量" 和 "服务量" 的数字校验
    2. "服务科目" 和 "使用材料" 的改变 触发是否显示

-->

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="java.util.ArrayList" %>

<%
    String script="";
    String[][] table = (String[][])request.getAttribute("table_result");
    String[][] result = (String[][])request.getAttribute("sub_table");
    String[][] serDept = (String[][])request.getAttribute("initial_serve_dept");
    String[][] benDept = (String[][])request.getAttribute("initial_benefit_dept");
    String[][] serSubj = new String[table.length][2];
for(int i=0;i<table.length;i++)
{
  serSubj[i][0] = table[i][0];
  serSubj[i][1] = table[i][1];
  script+="subcat["+i+"]=new Array(\""+table[i][0]+"\",\""+table[i][2]+"\",\""+table[i][3]+"\");";
}
%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  subcat = new Array();
  <%=script%>
  function changelocation(locationid,count){
    var locationid=locationid;
    var i;
    if(locationid=="") hide('serve');
    else
    for (i=0;i <count; i++){
      if (subcat[i][0] == locationid){
        template.serunit.value=subcat[i][1];
        template.serunitprice.value=subcat[i][2];
      }
    }
    getAll1();
    if (document.template.serSubj.value != "")
      show('serve')
  }

  function hide(str) {
    for (var i=0; i<100; i++) {
      if (document.all[str+i]!=null)
        document.all[str+i].style.visibility='hidden';
    }
  }

  function show(str) {
    for (var i=0; i<100; i++) {
      if (document.all[str+i]!=null)
        document.all[str+i].style.visibility='visible';
    }
  }

  function subCreate() {
   	var redDate = /^\d{4}-\d{1,2}-\d{1,2}$/

    if(template.date.value==""){
      alert('请选择日期');
     
      return;
    }
    if(!redDate.test(template.date.value)){
	alert("请输入正确的日期格式，如:2010-01-01") ;
	return;
    }
    if(template.serve_dept.value==""){
      alert('请选择服务科室');
      return;
    }
    if(template.benefit_dept.value==""){
      alert('请选择受益科室');
      return;
    }
    if(template.serSubj.value==""){
      alert('请选择服务项目');
      return;
    }
    if(isEmpty(template.seramt)){
      alert('服务量不能为空');
      return;
    }
    switch(isDouble(template.seramt,7,2))
    {
      case 0 : alert('服务量必须为数字型'); return;
      case 1 : alert('服务量整数部分不能高于7个字符'); return;
      case 2 : alert('服务量没有整数部分'); return;
      case 3 : alert('服务量小数部分不能高于2个字符'); return;
    }
   	
   	if(isTooLong(template.abstrac,64)){
    	alert('摘要长度不能超过64个字符！')
      return
		}
   
   	var win;
		if (template.serve_no.value==null || template.serve_no.value=="") // create页面上的辅表添加按钮
   		win=window.open("costingRecord.jspviewhigh?subFunction=preparedSubPageCreate&sorc=create&date="+template.date.value+"&serve_dept="+template.serve_dept.value+"&benefit_dept="+template.benefit_dept.value+"&serSubj="+template.serSubj.value+"&seramt="+template.seramt.value+"&seramttotal="+template.seramttotal.value+"&abstrac="+template.abstrac.value,"sub","left=180, top=500, width=800,height=200");
		else
    	win=window.open("costingRecord.jspviewhigh?subFunction=preparedSubPageCreate&sorc=create&serve_no="+template.serve_no.value+"&date="+template.date.value+"&serve_dept="+template.serve_dept.value+"&benefit_dept="+template.benefit_dept.value+"&serSubj="+template.serSubj.value+"&seramt="+template.seramt.value+"&seramttotal="+template.seramttotal.value+"&abstrac="+template.abstrac.value,"sub","left=180, top=500, width=800,height=200");
    	
    win.focus();
  }

  function subPageLoad(code){
    var win=window.open("costingRecord.jspviewhigh?subFunction=subPageLoad&serve_no="+template.serve_no.value+"&serve_stuff_code="+code,"sub","left=180, top=500, width=800,height=200");
    win.focus();
  }

  function subRemove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
      if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
      flag = true;
    }
    if( flag!=false) {
      if (confirm('是否删除')) {
        template.subFunction.value='createRemove';
        show_wait()
        template.submit();
        return true;
      } else
        return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }


  function subSelectedAll(){
    for (var i=0; i<template.elements.length; i++) {
      if (template.elements[i].name=='primaryKey')
        template.elements[i].checked = true;
      }
  }

  function subReset() {
    for (var i=0; i<template.elements.length; i++) {
      if (template.elements[i].name=='primaryKey')
        template.elements[i].checked = false;
      }
  }


  function getAll1(){
   var num1 = document.template.serunitprice.value;
   var num2 = document.template.seramt.value;
   document.template.seramttotal.value = roundOff(num1*num2,2);
  }

  function back() {
    template.abstrac.value='';
    template.serve_dept.value='';
    template.benefit_dept.value='';
    template.serve_no.value='';
    template.action='costingRecord.jspviewhigh';
    template.subFunction.value='find';
    template.signs.value="find";
    template.submit();
  }


  function create(){
    if(template.date.value==""){
      alert('请选择日期');
      return;
    }
    if(template.serve_dept.value==""){
      alert('请选择服务科室');
      return;
    }
    if(template.benefit_dept.value==""){
      alert('请选择受益科室');
      return;
    }
    if(template.serSubj.value==""){
      alert('请选择服务项目');
      return;
    }
    if(isEmpty(template.seramt)){
      alert('服务量不能为空');
      return;
    }
    switch(isDouble(template.seramt,7,2))
    {
      case 0 : alert('服务量必须为数字型'); return;
      case 1 : alert('服务量整数部分不能高于7个字符'); return;
      case 2 : alert('服务量没有整数部分'); return;
      case 3 : alert('服务量小数部分不能高于2个字符'); return;
    }
   if(isTooLong(template.abstrac,64)){
     alert('摘要长度不能超过64个字符！')
      return
   }
    template.subFunction.value='create';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="costingRecord.jspviewhigh">

  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>内部服务添加页面</html:title>

  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">日期：</td>
      <td class="normalText" nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.DateComponent("date", request.getParameter("date"))%></td>
      
      <td nowrap class="signText">服务科室：</td>
        <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="serve_dept" value="<%=request.getParameter("serve_dept")==null? "":request.getParameter("serve_dept")%>" AdjustVal="147" previousObj="date" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" width="200" top="42" left="358" Lheight="5" xmlSource="dic/asst_dict_acct_dept_LV.xml" init="1"/>
        </td>
         <td nowrap class="signText">受益科室：</td>
        <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="benefit_dept" value="<%=request.getParameter("benefit_dept")==null? "":request.getParameter("benefit_dept")%>" AdjustVal="148" previousObj="date" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" width="200" top="42" left="640" Lheight="5" xmlSource="dic/dict_acct_dept_LV.xml" init="1"/>
        </td>
      <!--
      <td class="signText" nowrap="nowrap">服务科室：</td>
      <td class="normalText" nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(serDept, "serve_dept", request.getParameter("serve_dept"), false, false)%></td>
      <td class="signText" nowrap="nowrap">受益科室：</td>
      <td class="normalText" nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(benDept,"benefit_dept",(String)request.getAttribute("benefit_dept"),true,false)%></td>
   -->
    </tr>

		<tr>
      <td class="signText" nowrap="nowrap">服务项目：</td>
<%
SingleSelect select_sercode=new SingleSelect(serSubj,"serSubj",(String)request.getParameter("serSubj"),false,false);
select_sercode.setAttribute("onchange", "changelocation(template.serSubj.value,"+table.length+")");
%>
      <td class="normalText" nowrap="nowrap"><%=select_sercode%></td>
      <td class="signText" nowrap="nowrap" id='serve1' >服务单位：</td>
      <td class="normalText" nowrap="nowrap" id='serve2' ><input class=textInputB1 type="text" name="serunit" readonly style='border:none' <%if(request.getParameter("serunit") != null){ out.println(" value=" + request.getParameter("serunit"));}%>/></td>
      <td class="signText" nowrap="nowrap" id='serve3' >单位服务量：</td>
      <td class="normalText" nowrap="nowrap" id='serve4' ><input class=textInputB1 type="text" name="serunitprice" readonly style='border:none' <%if(request.getParameter("serunitprice") != null){ out.println(" value=" + request.getParameter("serunitprice"));}%>></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap" id='serve5' >服务量：</td>
      <td class="normalText" nowrap="nowrap" id='serve6' ><input class=textInputB1 type="text" name="seramt" onkeyup="getAll1()" <%if(request.getParameter("seramt") != null){ out.println(" value=" + request.getParameter("seramt"));}%>/></td>
      <td class="signText" nowrap="nowrap" id='serve7' >服务当量合计：</td>
      <td class="normalText" nowrap="nowrap" id='serve8' ><input class=textInputB1 type="text" name="seramttotal" readonly value='<%=request.getAttribute("seramttotal")==null?"":request.getAttribute("seramttotal")%>' style='border:none'></td>
    </tr>
	  </html:table>

	  <html:table clazz="simple">
    <tr>
      <td class='signText' nowrap >摘要：</td>
      <td class='normalText' nowrap colspan=5 >
        <textarea name='abstrac' class='textareaBg' rows='2' cols='40'  maxlength='64' ><%if (request.getParameter("abstrac") != null) out.print(request.getParameter("abstrac"));%></textarea>
      </td>
      <td class='signText' nowrap >服务单号：</td>
      <td class="normalText" nowrap="nowrap" ><input class=textInputB1 type="text" readonly name="serve_no" style='border:none' <%if(request.getParameter("serve_no")!= null && !request.getParameter("subFunction").trim().equals("preparedCreate")){ out.println(" value='" + request.getParameter("serve_no")+"'");}%>/></td>
    </tr>
	  </html:table>

          <html:title clazz='table'></html:title>
          <html:title clazz='table'></html:title>
		
	  <html:table clazz="complex">
    <tr>
      <td align="right">
       	<button class="pageBtn" onclick="return create();" >保存</button>
       	<button class="pageBtn" onclick="return back(template)">返回</button> 
        <input type="hidden" name="subFunction" value="createLoad">
      </td>
    </tr>
	      </html:table>

          <html:title clazz='table'>材 料</html:title>


  	<!-- 子表信息 -->
		<html:table clazz="complex">
		<tr><td align='center'>
		<table>
    <tr>
      <td  nowrap="nowrap">
        <!--<img src="images/selectedAll.gif" style='cursor:hand' onclick="return subSelectedAll();" />-->
        <button class="pageBtn" onclick="return subSelectedAll();" >全选</button>    
	<button class="pageBtn" onclick="return subCreate()" >添加</button>        
	<button class="pageBtn" onclick="return subReset();"  >重置</button> 
	<button class="pageBtn" onclick="return subRemove();"  >删除</button> 
	
       <!--<img src="images/reset.gif" style='cursor:hand' onclick="return subReset();" />
        <img src="images/create.gif" style='cursor:hand' onclick="return subCreate()" />-->
      </td>
    </tr>
	  <tr>
	  	<td>    
	      <html:table clazz="result" divHeight="300" divWidth='800'>
	        <html:tr clazz='label'>
            <td class="resultLabel">选择</td>
            <td class="resultLabel">使用材料</td>
            <td class="resultLabel">单位</td>
	          <td class="resultLabel">单价</td>
            <td class="resultLabel">使用量</td>
            <td class="resultLabel">材料费合计</td>
            <td class="resultLabel">材料费对应项目</td>
	        </html:tr>
        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]!=null && result[i][j]=="") result[i][j]="&nbsp;";
                }
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
        %>

          <tr CLASS="<%=rowColor%>">
            <td><input type="checkbox" name="primaryKey" value="<%=result[i][0]%>"></td>
            <td class="normalText"><a href="javascript:subPageLoad('<%=result[i][0]%>');"><%=result[i][1]%></a></td>
            <td class="normalText"><%=result[i][2]%></td>
            <td class="normalText"><%=result[i][3]%></td>
            <td class="normalText"><%=result[i][4]%></td>
            <td class="normalText"><%=result[i][5]%></td>
            <td class="normalText"><%=result[i][6]%></td>
          </tr>

        <%
              }
            }
        %>
        </html:table>
      	</td>
      </tr>
		</table>
		</td></tr>
		</html:table>


<input type="hidden" name="signs" value="find"/>
</form>

<script language='javascript'>
  hide('serve')
 changelocation(template.serSubj.value,<%=table.length%>)
<%
  if(((String)request.getAttribute("serve_dept"))!=null&&!((String)request.getAttribute("serve_dept")).equals(""))
    out.println("show('serve')");
%>
template.date.readOnly="true" ;
</script>

</html:html>

