<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costing/serve/operate/ServerRecordOperSubPageCreate.jsp,v 1.2 2014/03/14 00:42:29 yuchengying Exp $
 $Author: yuchengying $
 $Date: 2014/03/14 00:42:29 $
 $Modtime: 03-09-03 10:41 $
 $Revision: 1.2 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect"%>
<%@ page import="java.util.ArrayList" %>

<%
  String script="";
  String[][] table = (String[][])request.getAttribute("table_result");
  String[] result = (String[])request.getAttribute("result");
  String[][] stuff = null;
if(table!=null){
  stuff = new String[table.length][2];
  for(int i=0;i<table.length;i++) {
    stuff[i][0] = table[i][0];
    stuff[i][1] = table[i][1];
    script+="subcat["+i+"]=new Array(\""+table[i][0]+"\",\""+table[i][2]+"\",\""+table[i][3]+"\",\""+table[i][4]+"\",\""+table[i][5]+"\");";
  }
}
%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
    subcat = new Array();
    <%=script%>
  function changelocation1(locationid,count){
    var locationid=locationid;
    var i;
    if(locationid=="") hide('stuff');
    else
    for (i=0;i <count; i++){
      if (subcat[i][0] == locationid){
        document.template.stuffunit.value=subcat[i][1];
        document.template.stuffunitprice.value=subcat[i][2];
        document.template.subj_name.value=subcat[i][3];
        document.template.subj.value=subcat[i][4];
      }
    }
   getAll2();
   if (document.template.stuff.selectedIndex>0)
        show('stuff')
  }
  function hide(str) {
      for (var i=0; i<100; i++) {
        if (document.all[str+i]!=null)
          document.all[str+i].style.visibility='hidden';
      }
    }

  function show(str) {
    for (var i=0; i<100; i++) {
      if (document.all[str+i]!=null)
        document.all[str+i].style.visibility='visible';
    }
  }

  function getAll2(){
    var num1 = template.stuffunitprice.value;
    var num2 = template.use.value;
    document.template.usetotal.value = roundOff(num1*num2,2);
  }

  function create(){
    if(template.stuff.value==""){
      alert('请选择使用材料');
      return;
    }
    if(isEmpty(template.use)){
      alert('使用量不能为空');
      return;
    }
    switch(isDouble(template.use,7,2))
    {
      case 0 : alert('使用量必须为数字型'); return;
      case 1 : alert('使用量整数部分不能高于7个字符'); return;
      case 2 : alert('使用量没有整数部分'); return;
      case 3 : alert('使用量小数部分不能高于2个字符'); return;
    }
    template.serve_no.value = window.opener.template.serve_no.value;
    template.subFunction.value='subPageCreate';
    show_wait()
    template.submit();
    return true;
  }

  <%
  String serve_no = request.getAttribute("serve_no")==null?"":(String)request.getAttribute("serve_no");
  String appError = request.getAttribute("appError")==null?"":(String)request.getAttribute("appError");
  if (serve_no!=null && !serve_no.trim().equals("") && (appError==null || appError.trim().equals(""))) {
    out.println("window.opener.template.serve_no.value='"+serve_no+"';");
    out.println("window.opener.template.submit();");
    out.println("window.close();");
  }
  %>
</Script>
<html:html clazz="child">
<form name="template" method="post" action="costingRecord.jspviewhigh">

  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>材料明细添加页面</html:title>

	  <html:table clazz="complex">
    <tr>
      <td>
<%if(table!=null){%>
	  <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap="nowrap">使用材料：</td>
<%
SingleSelect select_stucode=new SingleSelect(stuff,"stuff",null,false,false);
select_stucode.setAttribute("onchange", "changelocation1(document.template.stuff.options[document.template.stuff.selectedIndex].value,"+table.length+")");
%>
          <td class="normalText" nowrap="nowrap"><%=select_stucode%></td>
          <td class="signText" nowrap="nowrap" id='stuff1'>单位：</td>
          <td class="normalText" nowrap="nowrap" id='stuff2'><input class='textInputB1' type="text" name="stuffunit" readonly <%if( result!= null){ out.println(" value=" + result[1]);}%> style='border:none'></td>
          <td class="signText" nowrap="nowrap" id='stuff3'>单价：</td>
          <td class="normalText" nowrap="nowrap" id='stuff4'><input class='textInputB1' type="text" name="stuffunitprice" <%if( result!= null){ out.println(" value=" + result[2]);}%> onkeyup="getAll2()"></td>
        </tr>
        <tr>
          <td class='signText' nowrap id='stuff5'>使用量：</td>
          <td class='normalText' nowrap id='stuff6'><input class='textInputB1' type="text" name="use" <%if( result!= null){ out.println(" value=" + result[3]);}%> onkeyup="getAll2()"></td>
          <td class='signText' nowrap id='stuff7'>材料费合计：</td>
          <td class='normalText' nowrap id='stuff8'><input class='textInputB1' type="text" name="usetotal" readonly <%if( result!= null){ out.println(" value=" + result[4]);}%> style='border:none'></td>
          <td class='signText' nowrap id='stuff9'>材料费对应科目：</td>
          <td class='normalText' nowrap id='stuff10'>
            <input class='textInputB1' type="text" name="subj_name" readonly style='border:none'>
            <input type="hidden" name="subj"  >
          </td>
        </tr>
         <tr>
     <td colspan="2">
	<button class="pageBtn" onclick="return create();" >添加</button>
	<button class="pageBtn" onclick="window.close();" >关闭</button>
    </td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
	  </html:table>
      </td>
    </tr>
	<%}%>
</html:table>

  <input type=hidden name="subFunction"/>
  <input type=hidden name="serve_no" value=""/>
  <input type=hidden name="date" value="<%=request.getParameter("date")%>"/>
  <input type=hidden name="serve_dept" value="<%=request.getParameter("serve_dept")%>"/>
  <input type=hidden name="benefit_dept" value="<%=request.getParameter("benefit_dept")%>"/>
  <input type=hidden name="serSubj" value="<%=request.getParameter("serSubj")%>"/>
  <input type=hidden name="seramt" value="<%=request.getParameter("seramt")%>"/>
  <input type=hidden name="seramttotal" value="<%=request.getParameter("seramttotal")%>"/>
  <input type=hidden name="abstrac" value="<%=request.getParameter("abstrac")%>"/>
  <input type="hidden" name="writerid" value="<%=request.getParameter("writerid")%>"/>
</form>
<%if( result== null){%>
<script language='javascript'>
  hide('stuff')
</script>

<%}%>

</html:html>