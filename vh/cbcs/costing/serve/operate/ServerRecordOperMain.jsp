<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costing/serve/operate/ServerRecordOperMain.jsp,v 1.5 2014/02/27 04:47:29 yuchengying Exp $
  $Author: yuchengying $
  $Date: 2014/02/27 04:47:29 $
  $Revision: 1.5 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
  com.viewhigh.cbcs.base.mvc.view.TableMarge,
  com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.cbcs.util.*" %>
  <%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.base.mvc.view.component.*,com.viewhigh.cbcs.cbcs.util.DisplayWidth,
  com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
String[][] serSubj = (String[][])request.getAttribute("initial_logistic_serve_code");
String[][] benDept = (String[][])request.getAttribute("initial_benefit_dept");
String[][] serDept = (String[][])request.getAttribute("initial_serve_dept");
String[][] create_type={{"",""},{"1","录入"},{"2","汇总"}};
%>


 <script>
   var dd='<?xml version="1.0" encoding="GBK"?><root><msg/><error/><record><total/></record><activeHead/><thead/><tbody>';

</script>
<html:html clazz="main" fixRows="1">
  <form name="template" method="post" action="costingRecord.jspviewhigh">
    <!-- 信息提示栏 -->
	  <html:message/>

    <!-- 标题栏 -->
	  <html:title clazz='module'>数据维护</html:title>

    <!-- 简单信息 -->
	  <html:table clazz="simple">
      <tr>
        <td nowrap class="signText">服务时间：</td> 
        <td nowrap class="normalText" ><%=new BiDateComponent("start", request.getParameter("start"), "end", request.getParameter("end"))%></td>
        <td nowrap class="signText">录入人：</td>
        <td nowrap class="normalText"><input type="text" name="operator" class="textInputC" value='<%=request.getParameter("operator")==null?"":request.getParameter("operator")%>' style="width:140px"/></td>
     	<td nowrap class="signText">院内服务项目：</td>
        <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(serSubj,"logistic_serve_no",request.getParameter("logistic_serve_no"),false,false)%></td>
      </tr>
      <tr>
         <td nowrap class="signText">服务单号：</td>
        <td nowrap class="normalText"><input type="text" name="serve_no" class="textInputC" value='<%=request.getParameter("serve_no")==null?"":request.getParameter("serve_no")%>' style="width:140px"/></td>
      <td nowrap class="signText">受益科室：</td>
        <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="benefit_dept"  value="<%=request.getParameter("benefit_dept")%>"  AdjustVal="147" previousObj="serve_no" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" width="140" top="42" left="116" Lheight="5" xmlSource="dic/dict_acct_dept_L.xml" init="1"/>
        </td>
         <td nowrap class="signText">服务科室：</td>
        <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="serve_dept"  value="<%=request.getParameter("serve_dept")%>"  AdjustVal="148" previousObj="serve_no" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" width="140" top="42" left="608" Lheight="5" xmlSource="dic/asst_dict_acct_dept_L.xml" init="1"/>
        </td>
      </tr>
      <tr>
        <td nowrap class="signText">摘要：</td>
        <td nowrap class="normalText"> 
          <input type="text" name="abstrac" class="textInputD" style="width:140px;"/></td>  
        <td class="signText" nowrap="nowrap">录入方式：</td>
         <td nowrap="nowrap">
          <%=new SingleSelect(create_type, "create_type", request.getParameter("create_type")==null? "":request.getParameter("create_type"), false, true)%>
         </td>
        <!--input type="text" name="abstrac"   value='<%=request.getParameter("abstrac")==null?"":request.getParameter("abstrac")%>'/>     
         常用摘要:<%=new SingleSelect(request.getAttribute("dictsummary"), "dictsummary", request.getParameter("dictsummary"), false, false)%>
				</td-->
        <td colspan='4' align="right"><button class="pageBtn" onclick="find()" >查询</button> 
        <button class="pageBtn" name=""  onclick="collect()" >汇总</button> 
        <button class="pageBtn" name="hosDictsUnitinfoStlogisticservedetail_import"
						onclick="importData(this,1)" >导入</button> 
				<!-- 添加打印功能 -->		
				<button class="pageBtn" accessKey="P" onClick="prints()"/>打印</button>		 
        <!--<img src="images/huizong.GIF" class="mouse" onclick="collect()" />
        <img src="images/transfers.gif" class="mouse" name="hosDictsUnitinfoStlogisticservedetail_import"
						onclick="importData(this,1)" /> --></td>
      </tr>
	  </html:table>

    <br>

    <html:title clazz='table'>内部服务记录</html:title>
    
    <%
    BaseRO ro = (BaseRO)request.getAttribute("baseRO");
    if(ro!=null){
      TableMarge oper = new TableMarge(ro, "find();");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
      oper.addNeedButton("images/create.gif", "return create()");     //  添加
    %>
   
      <!-- 操作 -->
      <tr><td><%=oper%></td></tr>
      
	  <vh:vhFixTable fixRow=1 fixCol=0>
	      <table  width="100%" id="tablea1" class="resultSetTable" >
		    <colgroup id=tg>
          
          <col style = 'width:20mm' >
          
          <col style = 'width:50mm' >
          
          <col style = 'width:30mm' >
            
          <col style = 'width:30mm' >
          
          <col style = 'width:30mm' >
          
          <col style = 'width:30mm' >
            
          <col style = 'width:30mm' >
          
          <col style = 'width:30mm' >
          
          <col style = 'width:30mm' >
          
          <col style = 'width:30mm' >
        </colgroup>	        

          <tr class="resultLabel">
            <td nowrap="nowrap" class="resultLabel">选择</td>
            <td nowrap="nowrap" class="resultLabel">服务单号</td>
            <td nowrap="nowrap" class="resultLabel">服务时间</td>
            <td nowrap="nowrap" class="resultLabel">院内服务项目</td>
            <td nowrap="nowrap" class="resultLabel">服务量</td>
            <td nowrap="nowrap" class="resultLabel">服务当量</td>
            <td nowrap="nowrap" class="resultLabel">受益科室</td>
            <td nowrap="nowrap" class="resultLabel">服务科室</td>
            <td nowrap="nowrap" class="resultLabel">录入人</td>
			      <td nowrap="nowrap" class="resultLabel">录入时间</td>
			      <td nowrap="nowrap" class="resultLabel">录入方式</td>
	        </tr>

            <%
              if (ro!=null) {
                String[][] result = ro.getTableResult();
              	
              	if (result!=null) {
              	  for (int i=0; i<result.length; i++) {
              	     %>
				              	  <script>
				              	  dd+="<tr>";
				              	  </script>
                     <%
              	     boolean isCheckout =
                      Boolean.valueOf(result[i][9]).booleanValue();
              	  	String primaryKey = result[ i ][ 0 ];
              	  	String is_create= result[ i ][ 10 ];
                    for(int j=0;j<result[i].length;j++){
                      if(result[i][j]==null||result[i][j].equals(""))
                        result[i][j]="&nbsp";
                    }
                    String rowColor = "rowGray";
                    if (i/2*2==i) rowColor = "rowWhite";
            %>
            <tr CLASS="<%=rowColor%>">
                <td><input id="t0" type="checkbox" name="primaryKey" value="<%=primaryKey%>" <%if(isCheckout){out.print(" disabled ");}%>></td>
                <td class="normalText">
                   <% if (is_create.equals("1")){%> 
                    <a href='#' onclick="return openSave('costingRecord.jspviewhigh?subFunction=load&serve_no=<%=primaryKey%>&checkout=<%=isCheckout%>');">
                     <%=primaryKey%>
                    </a>
                   <%}else{%>
                    <%=primaryKey%>
                   <%}%>
                </td>
                <td class="normalText" nowrap="nowrap" id="t1"><%=result[i][1].substring(0,10)%></td>
                <td class="normalText" nowrap="nowrap" id="t2"><%=result[i][2]%></td>
                <td class="numberText" nowrap="nowrap" id="t3"><%=result[i][7]%></td>
                <td class="numberText" nowrap="nowrap" id="t4"><%=result[i][8]%></td>
                <td class="normalText" nowrap="nowrap" id="t5"><%=result[i][3]%></td>
                <td class="normalText" nowrap="nowrap" id="t6"><%=result[i][4]%></td>
                <td class="normalText" nowrap="nowrap" id="t7"><%=result[i][6]%></td>
				        <td class="normalText" nowrap="nowrap" id="t8"><%=result[i][9]%></td>
				        <td class="normalText" nowrap="nowrap" id="t9"><%=result[i][11]%></td>
            </tr>
            
            <script>
            				dd+="<td>"+document.getElementById("t0").value+"</td>";
              	  	dd+="<td>"+document.getElementById("t1").innerHTML+"</td>";
              	  	dd+="<td>"+document.getElementById("t2").innerHTML+"</td>";
              	  	dd+="<td>"+document.getElementById("t3").innerHTML+"</td>";
              	  	dd+="<td>"+document.getElementById("t4").innerHTML+"</td>";
              	  	dd+="<td>"+document.getElementById("t5").innerHTML+"</td>";
              	  	dd+="<td>"+document.getElementById("t6").innerHTML+"</td>";
              	  	dd+="<td>"+document.getElementById("t7").innerHTML+"</td>";
              	  	dd+="<td>"+document.getElementById("t8").innerHTML+"</td>";
              	  	dd+="<td>"+document.getElementById("t9").innerHTML+"</td>";
              	  	
            </script>
            <%
             %>
              	  <script>
              	  	dd+="</tr>";	
              	  </script>
             <%
            				}
              %>
              	  <script>
              	  	dd+="</tbody></root>";
              	  	//alert(dd)
              	  </script>
             <%     
                }
              }
            %>
	      	     </table>
</vh:vhFixTable>

<%}%>
    <input type="hidden" name="subFunction" >
    <input type="hidden" name="signs" >
    <input type="hidden" name="flag" value="NOT">
  </form>

<%
  if(request.getParameter("clear_flag")!=null&&request.getParameter("clear_flag").equals("true")){    
    out.println("<Script Language='JavaScript'>");
    out.println("template.abstrac.value='';");
    out.println("template.serve_dept.value='';");
    out.println("template.benefit_dept.value='';");
    out.println("</Script>");
  }
%>
<script>
//  document.all.dictsummary.onchange=function(){
//  	document.all.abstrac.value=this.options[this.selectedIndex].text;	
//  }
document.all.logistic_serve_no.style.width=140
function create() {
     template.subFunction.value='preparedCreate';
     show_wait();
     template.submit();
     return true;
  }

  function remove() {
   var flag = false;
   for (var i=0; i<template.elements.length; i++) {
         if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
           flag = true;
     }

   if( flag!=false) {
       if (confirm('是否删除')) {
         template.subFunction.value='remove';
         show_wait()
         template.submit();
         return true;
       } else
           return false;
   } else {
     alert( "请先选择,再删除!");
     return false;
   }
  }
  
  
  function collect(){
    var d=new Date()
    var s=d.toString()
    template.signs.value="collect"
    template.subFunction.value='find';
    window.showModalDialog("costingRecord.jspviewhigh?signs="+template.signs.value+"&subFunction="+template.subFunction.value+"&start="+template.start.value+"&end="+template.end.value+"&abstrac="+template.abstrac.value+"&logistic_serve_no="+template.logistic_serve_no.value+"&serve_no="+template.serve_no.value+"&benefit_dept="+template.benefit_dept.value+"&serve_dept="+template.serve_dept.value+"&create_type="+template.create_type.value+"&operator="+template.operator.value+"&s="+s, window,"dialogHeight: 160px; dialogWidth: 500px;");
    
  }
  
   function selectedAll(){
    for (var i=0; i<template.elements.length; i++) {
    	if (template.elements[i].name=='primaryKey')
    		template.elements[i].checked = true;
    }
  }
  function selectAll(){
  	var inputs=document.getElementsByName('primaryKey');
   for (var i=0; i<inputs.length/2; i++) {
    if (inputs[i].name == 'primaryKey'
      && inputs[i].disabled == false) {
      inputs[i].checked = true;
    }
   }
  }

  function find() {
    template.signs.value="find"
    template.subFunction.value='find';
    show_wait()
    template.submit();
    return true;
  }

  function openSave(url) {
    var win = window.open(url, 'save', 'left=160, top=100, width=840, height=600');
    win.focus();
    return false;
  }
  
   function importData(obj,v){
		   		
				var fileName=fileUploader.choose(1);
				if(fileName.length<4&&fileName.indexOf(".")<0)
					return false;
				var fileType=fileName.substr(fileName.lastIndexOf(".")+1);
				fileUploader.submit(function(m,f){
						if( m != ''){
							alert(m);
							return false;
						}
						for(var k in f){
							fileType+="="+k;
							break;
						}
						var msg = "" ;
						var queryParams = "<comp_file>"+fileType+"</comp_file>" ;
						confirmImportStyleDialog(function(v){
								if(v==0)
									return;

					        window.xmlhttp.post("hosDictsUnitinfoStlogisticservedetail_import",queryParams+"<q_flag>"+v+"</q_flag>");
					        
					         var responseText = window.xmlhttp._object.responseText ;
					          msg = window.doMsg(responseText,"show")
						  if(msg==true) {
							 template.signs.value="find"
    							 template.subFunction.value='find';
							show_wait();
					    		template.submit();
					    		return true;
						  }
						  },1);
					},obj
				)	
			}
  
  function prints(){
  	var xml='<?xml version="1.0" encoding="GBK"?><root><msg/><error/><record><total/></record><activeHead/><thead/><tbody>';
  	var a = document.getElementById("tablea1");
  	var trs = a.getElementsByTagName("tr");
  	for(var i=1;i<trs.length;i++){
  		 var tds= trs[i].getElementsByTagName('td');
  		 xml=xml+"<tr>"
  		 for(var j=0; j<tds.length;j++){
  		 	    var b =tds[j].innerText
  		 	   if(b!=''){
  		 	   	  xml = xml + '<td>'+b+'</td>';
  		 	   	}		 	    
  		 	}
  		  xml=xml+"</tr>" 
  		}
  		xml=xml+"</tbody></root>";  
		  var strXml = new ActiveXObject("Microsoft.XMLDOM");
		  strXml.async=false;		
		  strXml.loadXML(xml);		  
			//var tempd=window.xmlhttp._object.responseXml;
			var temptrs=strXml.getElementsByTagName("tr");
			//alert(strXml.xml)
	  	if(temptrs==null || temptrs.length==0) {
	  	     alert("没有需要打印的数据！");
				return;
			}
			var data=new Object();
			data["thead_1_1"]="数据维护统计表";
			printXmlToCellByXsltFile(strXml.xml,"cbcs/costing/serve/operate/printView.xsl",data,false,false);  
		}
		
		
  </script>
  
  <div id="upload_file_div" style="position:absolute;display:none;">
	    <iframe src="uploadFileD.cmd" id="uploadFrame"></iframe>
	</div>
	<script Language="JavaScript" >
		_____VHFileCreatFromTimerss=window.setInterval(createVhFile,10);
		  function createVhFile(){
		    if(typeof(uploadFrame)=="undefined")
		      return ;
		    window.clearInterval(_____VHFileCreatFromTimerss);
		    fileUploader=new VhFileUploader(uploadFrame);
		  }
	</script>
</html:html>

