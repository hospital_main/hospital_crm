<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costing/serve/operate/diag.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:57:35 $
     $Modtime: 03-09-02 16:24 $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
  com.viewhigh.cbcs.base.mvc.view.TableMarge,
  com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.cbcs.util.*,java.text.*" %>
  <%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<html:html clazz="main">
    <body>
      	  	  <html:title clazz='module'>内部服务量汇总</html:title>
           
            <%
            DecimalFormat nf=new DecimalFormat();
            BaseRO ro = (BaseRO)request.getAttribute("baseRO");
            if(ro!=null){
            String[][] result=ro.getTableResult();
            String a="";
            String b="";
            if(result!=null){
              a=result[0][0];
               b=result[0][1];
            }
            %>
   	  <html:table clazz="simpleForm">
        <tr>
          <td nowrap class="signText">服务量：</td><td nowrap class="normalText"><%=nf.format(Double.parseDouble(a))%></td>
        </tr>
        <tr>
          <td nowrap class="signText">服务当量：</td><td nowrap class="normalText"><%=nf.format(Double.parseDouble(b))%></td>
  			</tr>
  			<tr>
  			<td></td>
  			<td></td>
  			<td><button class="pageBtn" onClick="window.close()" >关闭</button></td></tr>
      </html:table><%}%>
    </body>
</html:html>


