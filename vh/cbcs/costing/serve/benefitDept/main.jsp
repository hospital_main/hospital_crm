<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costing/serve/benefitDept/main.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Modtime: 03-09-22 13:32 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.base.mvc.view.component.*,com.viewhigh.cbcs.cbcs.util.DisplayWidth,
  com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
    function group()
    {
    	show_wait();
        template.submit();
    }
</Script>
<html:html clazz="main" fixRows="1">
 <form name="template" method="post" action="benefitDept.jspviewhigh" >
     <!-- 信息提示栏 -->
	  <html:message/>
     <!--信息栏-->
	  <html:title clazz='module'>受益科室汇总</html:title>
        <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText" >起止日期：</td>
      <td nowrap class="normalText"><%=new BiDateComponent("start", request.getParameter("start"),"end", request.getParameter("end"))%></td>
   
      <td nowrap class="signText" >受益科室代码：</td>
      <td nowrap class="normalText" ><input type="text" name="ben_dept_code" <%if(request.getParameter("ben_dept_code")!=null) out.println(" value=\""+request.getParameter("ben_dept_code")+"\"");%>  style="width:140px"/></td>
      <td nowrap class="signText" >受益科室名称：</td>
      <td nowrap class="normalText" ><input type="text" name="ben_dept_name" <%if(request.getParameter("ben_dept_name")!=null) out.println(" value=\""+request.getParameter("ben_dept_name")+"\"");%>  style="width:140px"/></td>
    </tr>
   <tr>
      <td nowrap class="signText" >服务科室代码：</td>
      <td nowrap class="normalText" ><input type="text" name="ser_dept_code" style="width:140px;" <%if(request.getParameter("ser_dept_code")!=null) out.println(" value=\""+request.getParameter("ser_dept_code")+"\"");%>/></td>
      <td nowrap class="signText" >服务科室名称：</td>
      <td nowrap class="normalText" ><input type="text" name="ser_dept_name" <%if(request.getParameter("ser_dept_name")!=null) out.println(" value=\""+request.getParameter("ser_dept_name")+"\"");%>  style="width:140px"/></td>
   	<td></td>
   	<td></td>
    </tr>
    <tr>
      <td align="right" colspan="6"><button class="pageBtn" onclick="group();" >查询</button> </td>
    </tr>
	  </html:table>

  <br>
          <html:title clazz='table'>受益科室汇总</html:title>
          
  <vh:vhFixTable fixRow=1 fixCol=0>
	      <table  width="100%" class="resultSetTable">
		    <colgroup id=tg>
          
          <col style = 'width:54mm' >
          
          <col style = 'width:55mm' >
          
          <col style = 'width:55mm' >
          
        </colgroup>	            
	        <tr class="resultLabel">
            <td nowrap class="resultLabel">受益科室</td>
            <td nowrap class="resultLabel">服务科室</td>
            <td nowrap class="resultLabel">金额</td>
	        </tr>
       

        <%
        DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
        String[][] result = (String[][])request.getAttribute("result");
        if ( result!=null )  {
					for (int i=0; i<result.length; i++) {
						for (int j=0; j<result[i].length; j++) {
							if (result[i][j]==null || result[i][j].trim().length()==0) result[i][j] = "&nbsp;";
						}
					}


        String total [] = null;
        for (int i=0; i<result.length; i++)
        {
          for (int j=0; j<result[i].length; j++)
          {
            if (result[i][j]!=null && result[i][j]=="") result[i][j]="&nbsp;";
          }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
          String dept = result[i][0];

          // 判断是否为总计,若是,则放在total中,最后取出
          if( result[i][0].equals( "合计" ))
          {
              total = result[i];
              continue;
          }

          // 判断是否为子项,如果是,则不显示科室名称
          if( i != 0 )
          {
              if( result[i][0].equals( result[i-1][0] ))
              {
                  dept = "&nbsp;";
              }

          }



        %>
              <tr CLASS="<%=rowColor%>">
                <td class="normalText"><%=dept%></td>
                <td class="normalText"><%=result[ i ][ 1 ]%></td>
                <td class="numberText"><%=moneyFormat.format(Double.parseDouble(result[ i ][ 2 ]))%></td>
              </tr>
              <%
                    }
              %>
                 <tr CLASS="rowWhite">
                  <td class="normalText"><%=total[0]%></td>
                  <td class="normalText"><%=total[1]%></td>
                  <td class="numberText"><%=moneyFormat.format(Double.parseDouble(total[2]))%></td>
                 </tr>
          <%
              }

        %>
		     </table>
</vh:vhFixTable>

  <input type="hidden" name="subFunction" value="findAll" />
</form>


</html:html>


