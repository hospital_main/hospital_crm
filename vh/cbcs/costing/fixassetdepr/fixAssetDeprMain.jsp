<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costing/fixassetdepr/fixAssetDeprMain.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Modtime: 03-09-03 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiMonthComponent,
  com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
      if (template.elements[i].name == 'primaryKey'
        && template.elements[i].disabled == false) {
        template.elements[i].checked = true;
    	}
    }
  }

    function find()
    {
        switch(isDouble(template.depr_value, 8, 2))
        {
          case 0 : alert('折旧金额必须为数字型'); return false;
          case 1 : alert('折旧金额整数部分不能高于8个字符'); return false;
          case 3 : alert('折旧金额小数部分不能高于2个字符'); return false;
        }

        if(template.operator.value!=null&&template.operator.value!='')
        {
            if(isTooLong(template.operator,10))
            {
                alert('操作员ID不能大于10个字符!');
                return;
            }
        }

        template.subFunction.value = "findByCondition";
        template.submit();
    }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="fixAssetDepr.jspviewhigh">



	<!-- 返回信息栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>固定资产折旧</html:title>

  <!-- 获取历史信息 -->
  <%
    String fix_asset_depr_type_code = request.getParameter("fix_asset_depr_type_code");
    String depr_value = request.getParameter("depr_value");
    String operator = request.getParameter("operator");
  %>

  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText" >起止年月： </td>
      <td nowrap class="normalText" colspan='3'><%=new BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%></td>
      <td class='signText'>固定资产折旧类型：
        <select type="select" name="fix_asset_depr_type_code">
            <option value="" <%if(fix_asset_depr_type_code == null || fix_asset_depr_type_code.equals("")) out.println(" selected ");%>>请选择</option>
            <%
                String fixResult[][] =
                    (String[][])request.getAttribute( "fix_asset_depr_type" );
                if(fixResult!=null)
                {
                    for( int i = 0; i < fixResult.length; i++ )
                    {
            %>
            <option value="<%=fixResult[i][0]%>" <%if(fix_asset_depr_type_code!=null&&fix_asset_depr_type_code.equals(fixResult[i][0])) out.println(" selected ");%>><%=fixResult[i][1]%></option>
            <%
                    }
                }
            %>
        </select>
			</td>
    </tr>
    <tr>
      <td class='signText'>折旧金额：</td>
      <td class='normalText'><input type="text" name="depr_value" <%if(depr_value!=null&&!depr_value.equals(""))out.println(" value=\""+depr_value+"\"");%>/></td>
      <td class='signText'>录入人：</td>
      <td class='normalText'><input type="text" name="operator" size=12 <%if(operator!=null&&!operator.equals(""))out.println(" value=\""+operator+"\"");%>/></td>
      <td><button class="pageBtn" onclick="find();" >查询</button><td>
      <td></td>
    </tr>
	  </html:table>

  <br>
          <html:title clazz='table'>固定资产折旧</html:title>
   <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if(ro!=null){
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
      oper.addNeedButton("images/create.gif", "return create()");     //  添加
  %>

  <!-- 复杂信息 -->
	  <html:table clazz="complex">
    <!-- 操作 -->
    <tr><td><%=oper%></td></tr>

  <!-- 结果集 -->
  <tr>
    <td>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
          <td class="resultLabel">选择</td>
          <td class="resultLabel">年月</td>
          <td class="resultLabel">固定资产折旧类型</td>
          <td class="resultLabel">折旧金额</td>
          <td class="resultLabel">录入人</td>
	        </html:tr>

        <%
          DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
          String[][] result = ro.getTableResult();
          if (result != null) {
            for (int i = 0; i < result.length; i++ ) {
              boolean isCheckout =
                Boolean.valueOf(result[i][5]).booleanValue();
              String primaryKey = result[ i ][ 0 ];
              for (int j=0; j<result[i].length; j++)
              {
                if (result[i][j]!=null && result[i][j]=="") result[i][j]="&nbsp;";
              }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td><input type="checkbox" name="primaryKey" value="<%=(result[i][0] + "|^|" + result[i][1])%>" <%if(isCheckout){out.print(" disabled ");}%>></td>
          <td class="normalText">
            <%if(!isCheckout) {%>
            <a href="fixAssetDepr.jspviewhigh?subFunction=preparedSave&primaryKey=<%=(result[i][0] + "|^|" + result[i][1])%>">
            <%}%>
              <%=result[i][0].substring(0,4) + "年" + Integer.parseInt(result[i][0].substring(4)) + "月"%>
            <%if(!isCheckout) {%>
            </a>
            <%}%>
          </td>
          <td class="normalText"><%=result[ i ][ 2 ]%></td>
          <td class="numberText"><%=moneyFormat.format(Double.parseDouble(result[ i ][ 3 ]))%></td>
          <td class="normalText"><%=result[ i ][ 4 ]%></td>
        </tr>

        <%
              }
            }
        %>
	      </html:table>
    </td>
  </tr>

	      </html:table>
<%}%>
  <input type=hidden name="subFunction"/>
</form>


</html:html>


