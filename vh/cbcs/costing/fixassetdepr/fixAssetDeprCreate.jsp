<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costing/fixassetdepr/fixAssetDeprCreate.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Modtime: 03-09-03 11:51 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
    if(template.year_monthc.value == '')
    {
      alert('年月不能为空!');
      return false;
    }

    switch(isDouble(template.depr_valuec,8,2))
    {
      case 0 : alert('折旧金额必须为数字型'); return false;
      case 1 : alert('折旧金额整数部分不能高于8个字符'); return false;
      case 3 : alert('折旧金额小数部分不能高于2个字符'); return false;
    }

    //template.subFunction.value='create';
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  {
    template.subFunction.value='findByCondition';
    element.submit();
  }
</Script>


<html:html clazz="main">
<form name="template" method="post" action="fixAssetDepr.jspviewhigh">

  <!-- 返回信息栏 -->
	  <html:message/>

  <!-- 标题栏 -->

	  <html:title clazz='module'>固定资产折旧情况添加页面</html:title>

  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
     <td nowrap class="signText" >核算月： </td>
      <td nowrap class="normalText" colspan='3'><%=new MonthComponent("year_monthc")%></td>
      <td class="signText" nowrap="nowrap">固定资产折旧类型：</td>
      <td class="normalText" nowrap="nowrap">
        <select type="select" name="fix_asset_depr_type_codec">
        <%
        String fixResult[][] =
          (String[][])request.getAttribute( "fix_asset_depr_type" );
        if(fixResult!=null)
        {
					for( int i = 0; i < fixResult.length; i++ )
          {
        %>
        <option value="<%=fixResult[i][0]%>"><%=fixResult[i][1]%></option>
        <%
          }
        }
				%>
        </select>
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">折旧金额：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="depr_valuec" class="textInputC" /></td>
    </tr>

    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> 
      <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
	  </html:table>
  <input type=hidden name="subFunction" value="create"/>
</form>
</html:html>
