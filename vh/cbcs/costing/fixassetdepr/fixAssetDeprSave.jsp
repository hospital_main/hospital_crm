<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/costing/fixassetdepr/fixAssetDeprSave.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Modtime: 03-09-03 11:54 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  {
    switch(isDouble(template.depr_valuex, 8, 2))
    {
      case 0 : alert('折旧金额必须为数字型'); return false;
      case 1 : alert('折旧金额整数部分不能高于8个字符'); return false;
      case 3 : alert('折旧金额小数部分不能高于2个字符'); return false;
    }

    template.submit();
    return true;
  }

  // 返回
  function back( element )
  {
    template.subFunction.value='findByCondition';
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="fixAssetDepr.jspviewhigh">

  <!-- 返回信息栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>固定资产折旧情况修改页面</html:title>

  <%
    String[] result = (String[])request.getAttribute( "result" );
    if ( result != null )
    {
  %>

  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap="nowrap">核算月： </td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0].substring(0,4) + "年" + Integer.parseInt(result[0].substring(4)) + "月"%>" disabled />
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">固定资产折旧类型：</td>
      <td class="normalText" nowrap="nowrap">
        <select type="select" name="fix_asset_depr_type_codex" disabled="true">
        <%
            String fixResult[][] =
                (String[][])request.getAttribute( "fix_asset_depr_type" );
            if(fixResult!=null)
            {
                for( int i = 0; i < fixResult.length; i++ )
                {
        %>
        <option value="<%=fixResult[i][0]%>" <%if(fixResult[i][0].equals(result[1]))out.println(" selected ");%>><%=fixResult[i][1]%></option>
        <%
                }
            }
        %>
        </select>
      </td>
    </tr>

    <tr>
      <td class="signText" nowrap="nowrap">折旧金额： </td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text name="depr_valuex" value="<%=result[2]%>" />
      </td>
    </tr>

    <tr>
      <td colspan="2">
      <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>   
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
	  </html:table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="year_monthx" value="<%=result[0]%>">
  <input type="hidden" name="fix_asset_depr_type_codex" value="<%=result[1]%>">

  <%}%>
</form>

</html:html>
