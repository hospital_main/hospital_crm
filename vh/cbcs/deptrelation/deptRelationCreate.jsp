<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/deptrelation/deptRelationCreate.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
								 com.viewhigh.cbcs.base.util.DataUtil,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%> 


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  //添加
    function create() {
      if(trim(template.cbcs_year.value)=="")
    	{
    		alert("年度不能为空");
    		return;
    	}
      if(trim(template.cbcs_month.value)=="")
    	{
    		alert("月份不能为空");
    		return;
    	}
    	
    	template.year_month.value = template.cbcs_year.value + template.cbcs_month.value;
    	
    	if(template.dept_code.value=="")
    	{
    		alert("服务科室不能为空");
    		return;
    	
    	}
    	if(template.benefit_dept_code.value=="")
    	{
    		alert("受益科室不能为空");
    		return;
    	} 
    	
    	if(template.dept_code.value==template.benefit_dept_code.value)
    	{
    		alert("受益科室定向不能与服务科室相同");
    		return;
    	} 
    	
    	var a=getValuePairBySql('deptRelationCreate_check','<td>'+template.year_month.value+'</td><td>'+template.dept_code.value+'</td><td>'+template.benefit_dept_code.value+'</td>');
    	if(a[0]!==""){
	    		alert(a[0])
	    		return false;
    		}
        template.subFunction.value='create';
        template.submit();
        show_wait();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    template.initsub.value="sub";
    show_wait();
    element.submit();
  }
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="apporDeptRelation.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>科室服务关系添加页面</html:title>

  <!-- 简单信息 -->
  <table  width="100%" class="simpleQuery" border="0">
		<tr>
			<td style="display:none"><input type="text" name="year_month" size="20"></td>
			<td class="signText" nowrap="nowrap" width="45%">年度：</td>
 			<td nowrap class="normalText"><%=new SingleSelect(request.getAttribute("cbcs_year"),"cbcs_year",(String)request.getAttribute("cur_year"),false,false)%></td>
      </tr>
		<tr>
			<td class="signText" nowrap="nowrap" width="45%">月份：</td>
 			<td nowrap class="normalText"><%=new SingleSelect(request.getAttribute("cbcs_month"),"cbcs_month",(String)request.getAttribute("cur_month"),false,false)%></td>
      </tr>
  	<tr>
			<td class="signText" nowrap>服务科室：</td>			
            <td><%= new Select(request.getAttribute("benefit_dept_code"),"dept_code","",false,false)%></td>
		</tr>
		<tr>
			<td class="signText" nowrap valign="top">受益科室：</td>	
			
	     <td >
	     	<select name="benefit_dept_code" multiple size="10" style="width:140px;">
          <%
   
          String[][] benefit_dept_code=(String[][])request.getAttribute("benefit_dept_code");
          if(null!=benefit_dept_code){
          for(int i=0;i<benefit_dept_code.length;i++){
          %>
           <option value="<%=benefit_dept_code[i][0]%>" ><%=benefit_dept_code[i][0]+":"+benefit_dept_code[i][1]%></option>
          <%}
          }
          %>
        </select>
	     </td>
		</tr>    
    
    <tr>
      <td colspan="2" align="center"> <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
     <tr height="500">
    <td>
    </td>
    </tr>
  </table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>   