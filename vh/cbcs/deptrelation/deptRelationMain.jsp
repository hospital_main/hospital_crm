<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/deptrelation/deptRelationMain.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.util.DataUtil,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>    

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
 
<Script Language="JavaScript">
    function create() {

        template.subFunction.value='preparedCreate';
        template.submit();
	    show_wait();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }

  function find() {
    template.subFunction.value='findAll';
    template.submit();
    show_wait();
    return true;
  }
  function inherit(){
    template.subFunction.value='preparedInherit';
    template.submit();
    show_wait();
    return true;      
  }

  function group() {
    if (template.year_month.value == "") {
      alert("请选择年月！");
      return false;
    }
    }
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="apporDeptRelation.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>科室服务关系定义</html:title>

	  <!-- 简单信息 -->
	  <html:table clazz="simple">
	    <tr>
              <TD noWrap class="signText">年月：</TD>	 
              <td nowrap class="normalText"><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td>
              <td nowrap class="signText">服务科室：</td>              
	     <!--<td ><%= new Select(request.getAttribute("dept_code"),"dept_code","",false,false)%></td>-->
	     <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="dept_code"  value="<%=request.getParameter("dept_code")%>"  AdjustVal="97" previousObj="costNo" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" width="140" top="12" left="370" Lheight="5" xmlSource="dic/dept_code_server.xml" init="1"/></td>
       </td>
		
	      <td nowrap class="signText">受益科室：</td>			
	     <!--<td ><%= new Select(request.getAttribute("benefit_dept_code"),"benefit_dept_code","",false,false)%> </td>-->
	      <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="benefit_dept_code"  value="<%=request.getParameter("benefit_dept_code")%>"  AdjustVal="97" previousObj="costNo" codeCol='dept_code' indexCodeSequence="dept_code|dept_name|spell" textCol="dept_name" width="140" top="12" left="370" Lheight="5" xmlSource="dic/dept_code_income.xml" init="1"/></td>
	       </tr>
	       <tr>
	      <td class="normalText" style="text-align:right"colspan="6"><button class="pageBtn" onclick="return find();" >查询</button><button class="pageBtn" onclick="return inherit();" >继承</button></td>
	    </tr>	
	   

	  </html:table>

	  <br>

	  <%
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      if (ro!=null) {
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
	  
  
		<html:title clazz='table'> 科室服务关系定义</html:title>
	  <!-- 复杂信息 -->
	  <html:table clazz="complex">
	    <!-- 操作 -->
	    <tr><td><%=oper%></td></tr></html:table>

		    	<html:table clazz="result">
		        <html:tr clazz='label'>
		          <td nowrap="nowrap" class="resultLabel" >选择</td>
		          <td nowrap="nowrap" class="resultLabel" >年月</td>
		          <td nowrap="nowrap" class="resultLabel" >服务科室</td>
		          <td nowrap="nowrap" class="resultLabel" >受益科室</td> 
		        </html:tr>

		        <%
		          String[][] result = ro.getTableResult();
		          if ( result != null )
		          {
		            for (int i = 0; i < result.length; i++ )
		            {
		              String  primaryKey = result[ i ][ 0 ]+" "+result[i][1]+" "+result[i][3];
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }

		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
		        %>
 
		        <tr CLASS="<%=rowColor%>">
		          <td nowrap="nowrap"><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 0 ]%> </td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 2 ]%></td>
		          <td nowrap="nowrap" class="normalText"><%=result[ i ][ 4 ]%></td>

		        </tr>

		        <%
		              }
		            }
		        %>
		      </html:table>
		<%}%>
	  <input type=hidden name="subFunction"/>
	  <input type="hidden" name="initsub" value="sub"/>
	</form>
</html:html>

