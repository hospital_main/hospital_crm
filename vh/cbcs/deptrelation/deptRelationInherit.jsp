<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/deptrelation/deptRelationInherit.jsp,v 1.1 2012/03/12 01:57:47 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:47 $
 $Modtime: 03-08-28 13:40 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select,
								 com.viewhigh.cbcs.base.util.DataUtil,
								 com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
//继承

 function create(){
     if( template.sdyear.value>=template.ddyear.value )
    {
      alert('目标年度必须大于源年度!');
      return;
    }
        if (!confirm('继承覆盖['+template.ddyear.value+']年数据 ，请确认')) {
         return true;
        }     
        template.subFunction.value='inherit';
        template.submit();
        show_wait();
        return true;
 }
   // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    element.submit();
  }
</Script>
    
    
<html:html clazz="main">
<form name="template" method="post" action="apporDeptRelation.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>


  <!-- 标题栏 -->
	  <html:title clazz='module'>科室服务关系定义复制页面</html:title>
  <table  width="100%" class="simpleQuery" >
  <tr>  
      <TD class="signText" nowrap="nowrap">源年月：</TD> 
      <td nowrap class="normalText"><%=new MonthComponent("sdyear",request.getParameter("year_month"))%></td>
      <TD noWrap class="signText">目标年月：</TD>	 
      <td nowrap class="normalText"><%=new MonthComponent("ddyear",request.getParameter("year_month"))%></td>
    </tr>

  <tr>                      
	<td class="normalText" nowrap="nowrap" colspan="4" style="text-align:center">
	<button class="pageBtn" onclick="return create()">确定</button> 
	<button class="pageBtn" onclick="return back(template);">返回</button> 
		<!--<IMG src="images/confirm.gif" width="44" height="21" border="0" class=mouse onclick="return create()"><IMG src="images/return.gif" width="44" height="21" border="0" class=mouse onclick="return back(template)"> -->
	</td>
  </tr>
    <input type=hidden name="subFunction" value="create"/>
  </form>
  </table>
</html:html>  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  