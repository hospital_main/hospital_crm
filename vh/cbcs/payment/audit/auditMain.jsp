<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/payment/audit/auditMain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:44 $
     $Modtime: 03-09-16 14:11 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%
    String[][] dept=(String[][])request.getAttribute("init_dept");
    String[][] payout_subj=(String[][])request.getAttribute("init_payout_subj");
    String[][] cost_subj=(String[][])request.getAttribute("init_cost_subj");
%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }
  function find() {
    template.subFunction.value='findAll';
    template.submit();return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="audit.jspviewhigh">
  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>请选择需要稽核的单据：</html:title>

  <!-- 查询信息 -->
    <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      String[][] result = ro.getTableResult();
      TableMarge oper = new TableMarge(ro, "return find()");
    %>
	  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText">申请时间</td>
      <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.BiDateComponent("start", request.getParameter("start"), "end", request.getParameter("end"))%></td>
      <td nowrap class="normalText">&nbsp;</td>
      <td nowrap class="normalText">&nbsp;</td>
      <td nowrap class="signText">申请单号</td>
      <td nowrap class="normalText"><input type="text" name="reqno" size="20" value='<%=request.getParameter("req_no")==null?"":request.getParameter("req_no")%>'/></td>
    </tr>
    <tr>
      <td nowrap class="signText">科室</td>
      <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(dept,"dept_code",request.getParameter("dept_code"),true,false)%></td>
      <td nowrap class="signText">支出项目</td>
      <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(payout_subj,"pay_item_code",request.getParameter("pay_item_code"),false,false)%></td>
      <td nowrap class="signText">成本项目</td>
      <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(cost_subj,"cost_subj_code",request.getParameter("cost_subj_code"),false,false)%></td>
    </tr>
	  </html:table>
  <table  width="80%" cellspacing="2" border="0" >
    <tr>
      <td class="normalText" nowrap="nowrap"><input type="radio" name="pay" value="0" <%if(request.getParameter("pay")!=null && request.getParameter("pay").equals("0")) out.print("checked");%>>未稽核  <input type="radio" name="pay" value="1" <%if(request.getParameter("pay")!=null && request.getParameter("pay").equals("1")) out.print("checked");%>>已稽核  <input type="radio" name="pay" value="2" <%if(request.getParameter("pay")!=null && request.getParameter("pay").equals("2")) out.print("checked");%>>作废
          <input type="radio" name="pay" value="3" <%if(request.getParameter("pay")==null || request.getParameter("pay").equals("3")) out.print("checked");%>>全选</td>
      <td class="normalText" nowrap="nowrap">&nbsp</td>
      <td>
      <button class="pageBtn" name=""  onclick="return find();" >查询</button>
      <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
    </tr>
  </table>
          <html:title clazz='table'>财务稽核</html:title>
	  <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
          <td class="resultLabel">申请单号</td>
          <td class="resultLabel">申请日期</td>
          <td class="resultLabel">申请总金额</td>
          <td class="resultLabel">录入人</td>
          <td class="resultLabel">审核人</td>
          <td class="resultLabel">审核日期</td>
          <td class="resultLabel">稽核金额</td>
	        </html:tr>

        <%
          DecimalFormat nf = new DecimalFormat("#,##0.00");
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                String primaryKey = result[ i ][ 0 ];
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j]=="") result[i][j]="&nbsp;";
                }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText"><a href="audit.jspviewhigh?subFunction=preparedCheck&req_no=<%=primaryKey%>"><%=primaryKey%></a></td>
          <td class="normalText"><%=result[ i ][ 1 ]%></td>
          <td class="numberText" align="right"><%=nf.format(Double.parseDouble(result[ i ][ 2 ]))%></td>
          <td class="normalText"><%=result[ i ][ 3 ]%></td>
          <td class="normalText"><%=result[ i ][ 4 ]%></td>
          <td class="normalText"><%=result[ i ][ 5 ]%></td>
          <td class="normalText"><%=nf.format(Double.parseDouble(result[ i ][ 6 ]))%></td>
        </tr>

        <%
              }
            }
        %>
	      </html:table>
    </td>
  </tr>
  <!-- 操作 -->

	      </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>
