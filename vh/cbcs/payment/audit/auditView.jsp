<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/payment/audit/auditView.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:44 $
     $Modtime: 03-09-16 14:16 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool" %>
<%@ page import="java.util.ArrayList" %>
<%
    String[][] result = (String[][])request.getAttribute("table_result");
    double sum=0;
    double checksum=0;
    int num=0;
    DecimalFormat nf = new DecimalFormat("#,##0.00");
    String script="";
%>
<Script Language="JavaScript">
  function backtomain(){
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value="findAll";
    template.submit();
  }
</Script>

<Script Language="JavaScript">
  function viewProcess(){
    var win=window.open("checkresult.jspviewhigh?subFunction=view&req_no="+template.req_no.value,"","");
  }

</Script>

<html:html clazz="main">

<form name="template" method="post" action="audit.jspviewhigh">
  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>审核支出申请表</html:title>

	  <html:table clazz="simple">
    <tr>
      <td><a href="javascript:viewProcess();">查看办理过程</a></td>
     <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
     <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </tr>
	  </html:table>

  <table cellspacing="2" border="1" width="80%" align="center">
    <tr>
    <table cellspacing="2" border="0" width="70%" align="center">
      <tr>
          <td nowrap class="signText">申请单号</td>
          <td nowrap class="normalText"><input type="text" name="req_no" size="20" value='<%=result==null?"":result[0][0]%>' readonly style='border:none'/></td>
          <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td class="signText" nowrap="nowrap" style="text-align:right">申请日期：</td>
          <td class="normalText" nowrap="nowrap" style="text-align:right"><input type="text" name="req_date" size="20" value='<%=result==null?"":result[0][1]%>' readonly style='border:none'/></td>
      </tr>
    </table>
    </tr>
    <tr>
        <table cellspacing="2" width="70%" align="center">
<tr><td>
<br>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
            <td class="resultLabel">序号</td>
            <td class="resultLabel">摘要</td>
            <td class="resultLabel">科室</td>
            <td class="resultLabel">支出项目</td>
            <td class="resultLabel">成本项目</td>
            <td class="resultLabel">申请金额</td>
            <td class="resultLabel">&nbsp;</td>
            <td class="resultLabel">稽核金额</td>
	        </html:tr>
        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j].trim().equals("")) result[i][j]="&nbsp;";
                }
                if(result[i][5].equals("&nbsp;"))
                  result[i][5]="无";
                sum=sum+Double.parseDouble(result[i][9]);
                if(result[i][13].equals("&nbsp;"))
                  checksum=checksum+Double.parseDouble(result[i][9]);
                else checksum=checksum+Double.parseDouble(result[i][13]);
                num=i+1;
                String rowColor = "rowWhite";
        %>

          <tr CLASS="<%=rowColor%>">
            <td class="normalText"><%=num%></td>
            <td class="normalText"><%=result[i][5]%></td>
            <td class="normalText"><%=result[i][6]%></td>
            <td class="normalText"><%=result[i][7]%></td>
            <td class="normalText"><%=result[i][8]%></td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][9]))%></td>
            <td><img src='images/view.gif' class='mouse' onclick="alert('会计期间:<%=request.getAttribute("exam_date")==null?"":request.getAttribute("exam_date")%>\n核算科室:<%=result[i][6]%>\n成本项目:<%=result[i][8]%>\n预算成本:<%=nf.format(Double.parseDouble(result[i][10]))%>\n预算余额:<%=nf.format(Double.parseDouble(result[i][11]))%>\n');" ></td>
            <td class="numberText"><%if(result[i][13].equals("&nbsp;")) out.println(nf.format(Double.parseDouble(result[i][9])));else out.println(nf.format(Double.parseDouble(result[i][13])));%></td>
            <input type=hidden name="serial_no" value='<%=result[i][4]%>'/>
          </tr>
        <%
              }
            }
            num=num+1;
        %>
          <tr>
            <td class="normalText">&nbsp;</td>
            <td class="normalText">&nbsp;</td>
            <td class="normalText">&nbsp;</td>
            <td class="normalText">&nbsp;</td>
            <td align="right" class="signText">合计</td>
            <td class="numberText" align="right"><%=nf.format(sum)%></td>
            <td class="normalText">&nbsp;</td>
            <td class="numberText" align="right"><%=nf.format(checksum)%></td>
          </tr>
	      </html:table>
</td></tr>
        </table>
    </tr>
    <tr>
      <table cellspacing="2" border="0" width="70%" align="center">
        <tr>
          <td nowrap class="signText">录入人</td>
          <td nowrap class="normalText"><input type="text" name="person" value='<%=result==null?"":result[0][2]%>' readonly style='border:none'/></td>
          <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td class="signText" nowrap="nowrap" style="text-align:right">录入日期：</td>
          <td class="normalText" nowrap="nowrap" style="text-align:right"><input type="text" name="putin_date" size="20" value='<%=result==null?"":result[0][3]%>' readonly style='border:none'/></td>
        </tr>
      </table>
    </tr>
  </table>
	  <html:table clazz="simple">
    <tr>
	  <html:table clazz="simple">
        <tr>
          <td colspan="100" class="rowGray" style="text-align:left">稽核：</td>
        </tr>
	  </html:table>
    </tr>
    <tr>
      <table  cellspacing="2" border="1" width="70%" align="center">
        <tr>
        <table  cellspacing="2" border="0" width="70%" align="center">
        <tr>
          <td class="signText" nowrap="nowrap">备注：</td>
          <td nowrap class="normalText"><textarea name="content" readonly cols="60"></textarea></td>
        </tr>
      </table>
      </tr>
      <tr>
      <table  cellspacing="2" border="0" width="70%" align="center">
        <tr>
          <td nowrap class="signText">稽核人</td>
          <td nowrap class="normalText"><input type="text" name="person" value='<%=request.getAttribute("exam_person")==null?"":request.getAttribute("exam_person")%>' readonly style='border:none'/></td>
          <td class="signText" nowrap="nowrap" style="text-align:right">稽核时间：</td>
          <td class="normalText" nowrap="nowrap" style="text-align:right"><input type="text" name="putin_date" size="20" value='<%=request.getAttribute("exam_date")==null?"":request.getAttribute("exam_date")%>' readonly style='border:none'/></td>
        </tr>
      </table>
       </tr>
    </table>
    </tr>
    <tr>
	  <html:table clazz="simple">
        <tr>
          <td class="normalText" nowrap="nowrap"><input type="radio" name="check" value="0">稽核通过  <input type="radio" name="check" value="1">稽核不通过  </td>
          <td class="normalText" nowrap="nowrap">&nbsp</td>
        </tr>
	  </html:table>
    </tr>
    <tr>
	  <html:table clazz="simple">
       <tr>
        <td><button class="pageBtn" onclick="return backtomain();">返回</button> <!--
<img src="images/return.gif" style='cursor:hand' onclick="return backtomain();" > --></td>
       </tr>
	  </html:table>
    </tr>
	  </html:table>
     <input type=hidden name="subFunction" value="checkload"/>
     <input type=hidden name="nextpage" value="<%=result[0][0]%>"/>
     <input type=hidden name="serial" value="<%=result[0][12]%>"/>
</form>
</html:html>