<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/payment/pay/payView.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:44 $
     $Modtime: 03-09-08 18:54 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="java.text.*" %>
<%
    String[][] result = (String[][])request.getAttribute("table_result");
    String person = (String)request.getAttribute("person");
    double sum=0;
    double checksum=0;
    int num=0;
    DecimalFormat nf = new DecimalFormat("#,##0.00");
%>

<Script Language="JavaScript">
  function viewProcess(){
    var win=window.open("checkresult.jspviewhigh?subFunction=view&req_no="+template.req_no.value,"","");
  }
  function backtomain(){
   for(var i=0;i<template.elements.length;i++)
    template.elements[i].value="";
    template.subFunction.value="findAll";
    template.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="pay.jspviewhigh">
  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>查看申请单</html:title>

	  <html:table clazz="simple">
    <tr>
      <td><a href="javascript:viewProcess();">查看办理过程</a></td>
    </tr>
	  </html:table>
  <table cellspacing="2" border="1" width="80%" align="center">
    <tr>
      <table cellspacing="2" border="0" width="70%" align="center">
        <tr>
          <td nowrap class="signText">申请单号:</td>
          <td nowrap class="normalText"><input type="text" name="req_no" size="20" value='<%=request.getAttribute("req_no")==null?"":request.getAttribute("req_no")%>' readonly style='border:none'/></td>
          <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td class="signText" nowrap="nowrap" style="text-align:right">申请日期：</td>
          <td class="normalText" nowrap="nowrap" style="text-align:right"><input type="text" name="req_date" size="20" value='<%=request.getAttribute("req_date")==null?"":((String)request.getAttribute("req_date")).substring(0,10)%>' readonly style='border:none'/></td>
        </tr>
      </table>
    </tr>
    <tr>
        <table cellspacing="2" width="70%" align="center">
<tr><td>
<br>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
            <td class="resultLabel">序号</td>
            <td class="resultLabel">摘要</td>
            <td class="resultLabel">科室</td>
            <td class="resultLabel">支出项目</td>
            <td class="resultLabel">成本项目</td>
            <td class="resultLabel">申请金额</td>
            <td class="resultLabel">审批金额</td>
	        </html:tr>
        <%

            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j].trim().equals("")) result[i][j]="&nbsp;";
                }
                if(result[i][1].equals("&nbsp;"))
                  result[i][1]="无";
                sum=sum+Double.parseDouble(result[i][9]);
               if(result[i][10].equals("&nbsp;"))
                  checksum=checksum+Double.parseDouble(result[i][9]);
                else checksum=checksum+Double.parseDouble(result[i][10]);
                num=i+1;
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";

        %>

            <tr CLASS="<%=rowColor%>">
            <td class="normalText"><%=num%></td>
            <td class="normalText"><%=result[i][5]%></td>
            <td class="normalText"><%=result[i][6]%></td>
            <td class="normalText"><%=result[i][7]%></td>
            <td class="normalText"><%=result[i][8]%></td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][9]))%></td>
            <td class="numberText"><%if(result[i][10].equals("&nbsp;")) out.println(nf.format(Double.parseDouble(result[i][9])));else out.println(nf.format(Double.parseDouble(result[i][10])));%></td>
          </tr>
        <%
              }

            }
        %>
          <tr>
            <td class="normalText">&nbsp;</td>
            <td class="normalText">&nbsp;</td>
            <td class="normalText">&nbsp;</td>
            <td class="normalText">&nbsp;</td>
            <td align="right" class="numberText">合计</td>
            <td class="numberText" align="right"><%=nf.format(sum)%></td>
            <td class="numberText" align="right"><%=nf.format(checksum)%></td>
          </tr>
	      </html:table>
</td></tr>
        </table>
    </tr>
    <tr>
      <table cellspacing="2" border="0" width="70%" align="center">
        <tr>
          <td nowrap class="signText">录入人:</td>
          <td nowrap class="normalText"><input type="text" name="input_person" value='<%=request.getAttribute("input_person")==null?"":request.getAttribute("input_person")%>' readonly style='border:none'/></td>
          <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td class="signText" nowrap="nowrap" style="text-align:right">录入日期：</td>
          <td class="normalText" nowrap="nowrap" style="text-align:right"><input type="text" name="input_date" size="20" value='<%=request.getAttribute("input_date")==null?"":request.getAttribute("input_date").toString().substring(0,10)%>' readonly style='border:none'/></td>
        </tr>
      </table>
    </tr>
  </table>

	  <html:table clazz="simple">
    <!--
    <tr>
      <table  cellspacing="2" border="0" width="100%" >
        <tr>
          <td colspan="100" class="rowGray" style="text-align:left">财务支付：</td>
        </tr>
      </table>
    </tr>
    <tr>
      <table  cellspacing="2" border="1" width="70%" align="center">
        <tr>
         <table  cellspacing="2" border="0" width="70%" align="center">
           <tr>
            <td nowrap class="normalText" nowrap="nowrap">支付金额：</td>
            <td nowrap class="numberText">
            <input type="text" name="pay_amount" style="text-align:right" value='<%=nf.format(sum)%>'</td>
           </tr>
          </table>
       </tr>
        <tr>
      <table  cellspacing="2" border="0" width="70%" align="center">
        <tr>
          <td class="normalText" nowrap="nowrap">备注：</td>
          <td nowrap class="normalText"><textarea name="content" readonly cols="60"></textarea></td>
        </tr>
      </table>
      </tr>
      <tr>
      <table  cellspacing="2" border="0" width="70%" align="center">
        <tr>
          <td nowrap class="normalText"  >支付人</td>
          <td nowrap class="normalText"><input type="text" name="person" value='<%=request.getAttribute("person")==null?"":request.getAttribute("person")%>' readonly style='border:none'/></td>
          <td class="normalText" nowrap="nowrap" style="text-align:right">支付时间：</td>
          <td class="normalText" nowrap="nowrap" style="text-align:right"><input type="text" name="pay_date" size="20" value='<%=request.getAttribute("pay_date")==null?"":request.getAttribute("pay_date")%>' readonly style='border:none'/></td>
        </tr>
      </table>
       </tr>

    </table>

    </tr>
-->
    <tr>
	  <html:table clazz="simple">
       <tr>
        <td><button class="pageBtn"onclick="return backtomain();">返回</button> </td>
       </tr>
	  </html:table>
    </tr>
	  </html:table>
    <input type=hidden name="subFunction">
</form>
</html:html>
