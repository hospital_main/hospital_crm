<!--
	$Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/payment/pay/paymain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
	$Author: zhoulidong $Date: 2012/03/12 01:58:44 $
	$Modtime: 03-09-08 12:00 $
	$Revision: 1.1 $
	$NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,
                com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
  function find() {
    template.subFunction.value='findAll';
    template.submit();return true;
  }
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="pay.jspviewhigh">
     <!-- 信息提示栏 -->
	  <html:message/>

     <!--信息栏-->
	  <html:title clazz='module'>支付主页面</html:title>

     <!-- 简单信息 -->
	  <html:table clazz="simple">
      <tr>
        <td nowrap class="signText">起始日期</td>
        <td nowrap class="normalText" colspan='1'><%=new BiMonthComponent("start", request.getParameter("start"),"end",request.getParameter("end"))%></td>
        <td nowrap class="signText">申请单号</td>
        <td nowrap class="normalText"><input type="text" name="req_no" size="30" value='<%=request.getParameter("req_no")==null?"":request.getParameter("req_no")%>'/></td>
    </tr>
      <tr>

        <td nowrap class="signText">科室</td>
        <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("dept_code"),"dept_code",request.getParameter("dept_code"),false,false)%></td>
        <td nowrap class="signText">支出项目</td>
        <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("pay_item_code"),"pay_item_code",request.getParameter("pay_item_code"),false,false)%></td>
        <td nowrap class="signText">成本项目</td>
        <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(request.getAttribute("cost_subj_code"),"cost_subj_code",request.getParameter("cost_subj_code"),false,false)%></td>
      </tr>
      <tr>
        <td nowrap class="signText">金额</td>
        <td nowrap class="normalText"><input type="text" name="amount" size="30" value='<%=request.getParameter("amount")==null?"":request.getParameter("amount")%>'/></td>
        <td nowrap class="signText">录入人</td>
        <td nowrap class="normalText"><input type="text" name="operator" size="30" value='<%=request.getParameter("operator")==null?"":request.getParameter("operator")%>'/></td>
      </tr>
         <td nowrap class="signText">摘要</td>
        <td nowrap class="normalText"><input type="text" name="abstrac" size="30" value='<%=request.getParameter("abstrac")==null?"":request.getParameter("abstrac")%>'/></td>
      <tr>
      </tr >
	  </html:table>
	  <html:table clazz="simple">
      <tr>
      <td class="normalText" nowrap="nowrap">
          <input type="radio" name="condition" value="0" <%if(request.getParameter("condition")!=null && request.getParameter("condition").equals("0")) out.print("checked");%>>待支付
          <input type="radio" name="condition" value="1" <%if(request.getParameter("condition")!=null && request.getParameter("condition").equals("1")) out.print("checked");%>>已支付
          <input type="radio" name="condition" value="2" <%if(request.getParameter("condition")!=null && request.getParameter("condition").equals("2")) out.print("checked");%>>作废
          <input type="radio" name="condition" value="3" <%if(request.getParameter("condition")==null || request.getParameter("condition").equals("3")) out.print("checked");%>>所有
       </td>
      <td class="normalText" nowrap="nowrap">&nbsp</td>
      <td  colspan='2'>
      <button class="pageBtn" name=""  onclick="find()" >查询</button>
      <!--<img src="images/find.gif" class="mouse" onclick="find()" />--> </td>

      </tr>
	  </html:table>


          <html:title clazz='table'>支付</html:title>
      <%
      DecimalFormat nf = new DecimalFormat("#,##0.00");
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	    TableMarge oper = new TableMarge(ro, "return find()");
      %>

      <!-- 复杂信息 -->
	  <html:table clazz="complex">
        <!-- 操作 -->
        <tr><td><%=oper%></td></tr>

        <!-- 结果集 -->
        <tr>
          <td>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
                <td class="resultLabel">申请单号</td>
                <td class="resultLabel">申请日期</td>
                 <td class="resultLabel">申请金额</td>
                <td class="resultLabel">录入人</td>
	              <td class="resultLabel">批准日期</td>
	              <td class="resultLabel">批准人</td>
                <td class="resultLabel">支付日期</td>
                <td class="resultLabel">支付人</td>
	        </html:tr>

              <%
                if (ro!=null) {
              	String[][] result = ro.getTableResult();
              	if (result!=null) {

                  for (int i = 0; i < result.length; i++ )
                 {
                String primaryKey = result[ i ][ 0 ];
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j]=="") result[i][j]="&nbsp;";
                }

                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
              %>
              <tr CLASS="<%=rowColor%>">
                  <td class="normalText"><a href="pay.jspviewhigh?subFunction=view&primaryKey=<%=primaryKey%>"><%=result[i][0]%></a></td>
                  <td class="normalText"><%=result[i][1]%></td>
                  <td nowrap class=numberText class="normalText"><%=nf.format(Double.parseDouble(result[i][2]))%></td>
                  <td class="normalText"><%=result[i][3]%></td>
                  <td class="normalText"><%=result[i][4]%></td>
                  <td class="normalText"><%=result[i][5]%></td>
                  <td class="normalText"><%=result[i][6]%></td>
                  <td class="normalText"><%=result[i][7]%></td>
              </tr>
              <%
                    }
              	  }
                }

              %>
	      </html:table>
          </td>
        </tr>

        <!-- 操作 -->

	      </html:table>
      <input type=hidden name="subFunction"/>
  	</form>

</html:html>



