<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/payment/req/paymentSubPageStore.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:44 $
     $Modtime: 03-09-03 10:08 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect"%>
<%@ page import="java.util.ArrayList" %>
<%
    String script="";
    String[][] dept = (String[][])request.getAttribute("init_dept");
    String[][] table = (String[][])request.getAttribute("result");
    String[][] result = (String[][])request.getAttribute("table_result");
    String[][] payout_subj = new String[table.length][2];
    for(int i=0;i<table.length;i++) {
      payout_subj[i][0] = table[i][0];
      payout_subj[i][1] = table[i][1];
      script+="subcat["+i+"]=new Array(\""+table[i][0]+"\",\""+table[i][2]+"\",\""+table[i][3]+"\");";
  }

%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
    subcat = new Array();
    <%=script%>
  function changelocation1(locationid,count){
    var locationid=locationid;
    var i;
    if(locationid=="") hide('cost');
    else
    for (i=0;i <count; i++){
      if (subcat[i][0] == locationid){
        document.template.cost_subj.value=subcat[i][2];
        document.template.cost_subj_code.value=subcat[i][1];
      }
    }
   if (document.template.payout_subj.selectedIndex>0)
        show('cost')
  }
  function hide(str) {
      for (var i=0; i<100; i++) {
        if (document.all[str+i]!=null)
          document.all[str+i].style.visibility='hidden';
      }
    }

  function show(str) {
    for (var i=0; i<100; i++) {
      if (document.all[str+i]!=null)
        document.all[str+i].style.visibility='visible';
    }
  }
  function store(){
    if(isTooLong(template.abstrac,100)){
      alert('摘要过长不能超过100个字符');
      return;
    }
    if(template.dept.value==""){
      alert('请选择科室');
      return;
    }
    if(template.payout_subj.value){
      alert('请选择支出项目');
      return;
    }
    if(isEmpty(template.money)){
      alert('金额不能为空');
      return;
    }
    switch(isDouble(template.money,10,2))
    {
      case 0 : alert('金额必须为数字型'); return;
      case 1 : alert('金额整数部分不能高于10个字符'); return;
      case 2 : alert('金额没有整数部分'); return;
      case 3 : alert('金额小数部分不能高于2个字符'); return;
    }
    template.subFunction.value='subPageStore';
    template.submit();
    return true;
  }
  function remove(){
    if (confirm('是否删除')) {
          template.subFunction.value='detailremove';
          template.submit();
          return true;
        } else
            return false;
  }
  <%
  String req_no = request.getAttribute("req_no")==null?"":(String)request.getAttribute("req_no");
  String appError = request.getAttribute("appError")==null?"":(String)request.getAttribute("appError");
  String del = request.getAttribute("removemain")==null?"":(String)request.getAttribute("removemain");
  if (req_no!=null && !req_no.trim().equals("") && (appError==null || appError.trim().equals(""))) {
    if(del.equals("remove"))
      req_no = "";
    out.println("window.opener.template.req_no.value='"+req_no+"';");
    out.println("window.opener.template.submit();");
    out.println("window.close();");
  }
%>
</Script>
<html:html clazz="child">
<form name="template" method="post" action="payRequest.jspviewhigh">

  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>修改支出项目</html:title>

	  <html:table clazz="simple">
    <tr>
      <td>
	  <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap="nowrap">序号：</td>
          <td class='normalText' nowrap="nowrap"><input class='textInputB1' type="text" name="num" value='<%=request.getParameter("num")%>' readonly style='border:none'></td>
        </tr>
        <tr>
          <td nowrap class="signText">摘要</td>
          <td nowrap class="normalText"><input type="text" name="abstrac" size="40" <%if(result!=null && !result[0][0].trim().equals("")) out.println("value="+result[0][0]);%>/></td>
        </tr>
        <tr>
          <td nowrap class="signText">科室</td>
          <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(dept,"dept",result[0][2],true,false)%></td>
        </tr>
        <tr>
          <td nowrap class="signText">支出项目</td>
<%
SingleSelect select_payout=new SingleSelect(payout_subj,"payout_subj",result[0][3],false,false);
select_payout.setAttribute("onchange", "changelocation1(document.template.payout_subj.options[document.template.payout_subj.selectedIndex].value,"+table.length+")");
%>
          <td class="normalText" nowrap="nowrap"><%=select_payout%></td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap" id='cost1'>成本项目：</td>
          <td class="normalText" nowrap="nowrap" id='cost2'><input class='textInputB1' type="text" name="cost_subj" <%if(result!=null) out.println("value="+result[0][5]);%> readonly style='border:none'></td>
          <input type=hidden name="cost_subj_code" <%if(result!=null) out.println("value="+result[0][4]);%>/>
        </tr>
        <tr>
          <td nowrap class="signText">金额</td>
          <td nowrap class="normalText"><input type="text" name="money" size="40" <%if(result!=null) out.println("value="+result[0][1]);%>/></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><button class="pageBtn"  onclick="return store();">保存</button>
          <button class="pageBtn" onclick="return reset();" >重置</button>  
          <button class="pageBtn" onclick="window.close();" >关闭</button> 
           <img src="images/remove.gif" style='cursor:hand' onclick="return remove();" >        
           <!-- <img src="images/save.gif" style='cursor:hand' onclick="return store();" >
          <img src="images/reset.gif" style='cursor:hand' onclick="return reset();" >
<img src="images/close.gif" style='cursor:hand' onclick="window.close();" >--></td>
        </tr>
	  </html:table>
      </td>
    </tr>
	  </html:table>
  <input type=hidden name="req_no" value='<%=request.getParameter("req_no")%>'/>
  <input type=hidden name="serial" value='<%=request.getParameter("serial")%>'/>
  <input type=hidden name="subFunction"/>
</form>
</html:html>
