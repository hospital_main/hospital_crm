<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/payment/req/paymentCreate.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:44 $
     $Modtime: 03-09-03 10:06 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="java.text.*" %>
<%
    String[][] result = (String[][])request.getAttribute("table_result");
    String person = (String)request.getAttribute("person");
    double sum=0;
    int num=0;
    DecimalFormat nf = new DecimalFormat("#,##0.00");
%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function subCreate(num){
    if(template.req_date.value==""){
      alert('请选择日期');
      return false;
    }
    if (template.req_no.value==null || template.req_no.value=="") // create页面上的辅表添加按钮
      var win=window.open("payRequest.jspviewhigh?subFunction=preparedSubPageCreate&date="+template.req_date.value+"&num="+num,"","width=900,height=500");
    else
      win=window.open("payRequest.jspviewhigh?subFunction=preparedSubPageCreate&req_no="+template.req_no.value+"&date="+template.req_date.value+"&num="+num,"","width=900,height=500");
  }
  function subPageLoad(serial,num){
    var win=window.open("payRequest.jspviewhigh?subFunction=subPageLoad&req_no="+template.req_no.value+"&serial="+serial+"&num="+num,"","width=900,height=500");
  }
  function refer(){
    if(isEmpty(template.req_no)){
      alert('请先填写申请表');
      return false;
    }
    var win=window.open("payRequest.jspviewhigh?subFunction=preparedRefer&req_no="+template.req_no.value,"","width=900,height=500");
  }
  function backtomain(){
    if(isEmpty(template.req_no))
      template.subFunction.value="findAll";
    else
      template.subFunction.value="Store";
    template.submit();
  }
  function remove(){
    if (confirm('是否删除')) {
          template.subFunction.value='allremove';
          template.submit();
          return true;
        } else
            return false;
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="payRequest.jspviewhigh">
  <!-- 信息提示栏 -->
	  <html:message/>
  <!-- 标题栏 -->
	  <html:title clazz='module'>支出申请表</html:title>
<br>
  <table cellspacing="2" border="1" width="80%" align="center">
    <tr>
    <table cellspacing="2" border="0" width="70%" align="center">
      <tr>
        <td nowrap class="signText">申请单号</td>
        <td nowrap class="normalText"><input type="text" name="req_no" size="20" value='<%=request.getAttribute("req_no")==null?"":request.getAttribute("req_no")%>' readonly style='border:none'/></td>
        <td class="signText" nowrap="nowrap">申请日期：</td>
        <td class="normalText" nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.DateComponent("req_date", (String)request.getParameter("req_date"))%></td>
      </tr>
    </table>
    </tr>
    <tr>
        <table cellspacing="2" width="70%" align="center">
<tr><td>
<br>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
            <td class="resultLabel">序号</td>
            <td class="resultLabel">摘要</td>
            <td class="resultLabel">科室</td>
            <td class="resultLabel">支出项目</td>
            <td class="resultLabel">成本项目</td>
            <td class="resultLabel">申请金额</td>
	        </html:tr>
        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j].trim().equals("")) result[i][j]="&nbsp;";
                }
                if(result[i][1].equals("&nbsp;"))
                  result[i][1]="无";
                sum=sum+Double.parseDouble(result[i][5]);
                num=i+1;
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
        %>

          <tr CLASS="<%=rowColor%>">
            <td class="normalText"><%=num%></td>
            <td class="normalText"><a href="javascript:subPageLoad('<%=result[i][0]%>','<%=num%>');"><%=result[i][1]%></a></td>
            <td class="normalText"><%=result[i][2]%></td>
            <td class="normalText"><%=result[i][3]%></td>
            <td class="normalText"><%=result[i][4]%></td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][5]))%></td>
          </tr>
        <%
              }
            }
            num=num+1;
        %>
          <tr>
            <td class="normalText">&nbsp;</td>
            <td class="normalText">&nbsp;</td>
            <td class="normalText">&nbsp;</td>
            <td class="normalText">&nbsp;</td>
            <td align="right" class="numberText">合计</td>
            <td class="numberText" align="right"><%=nf.format(sum)%></td>
          </tr>
	      </html:table>
</td></tr>
        </table>
    </tr>
    <tr>
      <table cellspacing="2" border="0" width="70%" align="center">
        <tr>
          <td><img src='images/create.gif' class='mouse' onclick="return subCreate('<%=num%>');" ></td>
        </tr>
        <tr>
          <td nowrap class="signText">录入人</td>
          <td nowrap class="normalText"><input type="text" name="person" value='<%=request.getAttribute("person")==null?"":request.getAttribute("person")%>' readonly style='border:none'/></td>
          <td><img src='images/remove.gif' class='mouse' onclick="return remove();" ><img src='images/refer.gif' class='mouse' onclick="return refer();" ></td>
        </tr>
      </table>
    </tr>
  </table>
  <table cellspacing="2" border="0" width="75%" align="center">
  <tr>
    <td><img src='images/return.gif' class='mouse' onclick="return backtomain();" ></td>
  </tr>
  </table>
  <input type="hidden" name="subFunction" value="createLoad">
</form>
</html:html>