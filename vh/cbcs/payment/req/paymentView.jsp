<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/payment/req/paymentView.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:44 $
     $Modtime: 03-08-28 14:44 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="java.text.*" %>
<%
    String[][] result = (String[][])request.getAttribute("table_result");
    String person = (String)request.getAttribute("person");
    double sum=0;
    int num=0;
    DecimalFormat nf = new DecimalFormat("#,##0.00");
%>
<Script Language="JavaScript">
  function viewProcess(){
    var win=window.open("checkresult.jspviewhigh?subFunction=view&req_no="+template.req_no.value,"","");
  }
  function backtomain(){
    template.subFunction.value="findAll";
    template.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="payRequest.jspviewhigh">
  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>查看申请单</html:title>

  <table cellspacing="2" border="0" width="90%" align="center">
    <tr>
      <td><a href="javascript:viewProcess();">查看办理过程</a></td>
    </tr>
  </table>
  <table cellspacing="2" border="1" width="80%" align="center">
    <tr>
      <table cellspacing="2" border="0" width="70%" align="center">
        <tr>
          <td nowrap class="signText">申请单号</td>
          <td nowrap class="normalText"><input type="text" name="req_no" size="20" value='<%=request.getParameter("req_no")==null?"":request.getParameter("req_no")%>' readonly style='border:none'/></td>
          <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td class="signText" nowrap="nowrap" style="text-align:right">申请日期：</td>
          <td class="normalText" nowrap="nowrap" style="text-align:right"><input type="text" name="req_date" size="20" value='<%=request.getAttribute("req_date")==null?"":request.getAttribute("req_date")%>' readonly style='border:none'/></td>
        </tr>
      </table>
    </tr>
    <tr>
        <table cellspacing="2" width="70%" align="center">
<tr><td>
<br>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
            <td class="resultLabel">序号</td>
            <td class="resultLabel">摘要</td>
            <td class="resultLabel">科室</td>
            <td class="resultLabel">支出项目</td>
            <td class="resultLabel">成本项目</td>
            <td class="resultLabel">申请金额</td>
	        </html:tr>
        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j].trim().equals("")) result[i][j]="&nbsp;";
                }
                if(result[i][1].equals("&nbsp;"))
                  result[i][1]="无";
                sum=sum+Double.parseDouble(result[i][5]);
                num=i+1;
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
        %>

          <tr CLASS="<%=rowColor%>">
            <td class="normalText"><%=num%></td>
            <td class="normalText"><%=result[i][1]%></td>
            <td class="normalText"><%=result[i][2]%></td>
            <td class="normalText"><%=result[i][3]%></td>
            <td class="normalText"><%=result[i][4]%></td>
            <td class="numberText"><%=nf.format(Double.parseDouble(result[i][5]))%></td>
          </tr>
        <%
              }
            }
        %>
          <tr>
            <td class="normalText">&nbsp;</td>
            <td class="normalText">&nbsp;</td>
            <td class="normalText">&nbsp;</td>
            <td class="normalText">&nbsp;</td>
            <td align="right" class="numberText">合计</td>
            <td class="numberText" align="right"><%=nf.format(sum)%></td>
          </tr>
	      </html:table>
</td></tr>
        </table>
    </tr>
    <tr>
      <table cellspacing="2" border="0" width="70%" align="center">
        <tr>
          <td nowrap class="signText">录入人</td>
          <td nowrap class="normalText"><input type="text" name="person" value='<%=request.getAttribute("person")==null?"":request.getAttribute("person")%>' readonly style='border:none'/></td>
          <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td class="normalText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td class="signText" nowrap="nowrap" style="text-align:right">录入日期：</td>
          <td class="normalText" nowrap="nowrap" style="text-align:right"><input type="text" name="putin_date" size="20" value='<%=request.getAttribute("putin_date")==null?"":request.getAttribute("putin_date")%>' readonly style='border:none'/></td>
        </tr>
      </table>
    </tr>
  </table>
  <table cellspacing="2" border="0" width="75%" align="center">
  <tr>
    <td><img src='images/return.gif' class='mouse' onclick="return backtomain();" ></td>
  </tr>
  </table>

  <input type="hidden" name="subFunction" value="createLoad">
</form>
</html:html>