<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/payment/req/paymentRefer.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:44 $
     $Modtime: 03-09-17 9:20 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
     String[][] exam_person = (String[][])request.getAttribute("init_exam_person");
%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function refer(){
    if(template.exam_person.value==""){
      alert('请选择审批人');
      return;
    }
   if(template.exam_person.value=="<%=com.viewhigh.cbcs.cbcs.util.SysPara.getEmp_id(request)%>"){
      alert('不能提交给自己');
      return;
   }
    if(isEmpty(template.rtitile)){
      alert('标题不能为空');
      return;
    }
    if(isTooLong(template.content,200)){
      alert('摘要过长不能超过200个字符');
      return;
    }
    template.subFunction.value='refer';
    template.submit();
    return true;
  }
  <%
  String messge = request.getAttribute("messge")==null?"":(String)request.getAttribute("messge");
  String appError = request.getAttribute("appError")==null?"":(String)request.getAttribute("appError");
  if (messge!=null && messge.equals("提交成功") && (appError==null || appError.trim().equals(""))) {
    out.println("window.opener.template.submit();");
    out.println("window.close();");
  }
%>
</Script>
<html:html clazz="child">
<form name="template" method="post" action="payRequest.jspviewhigh">
  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>提交给：</html:title>

	  <html:table clazz="simple">
    <tr>
      <td>
	  <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap="nowrap">审批人：</td>
          <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(exam_person,"exam_person",null,false,false)%></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="100" class="rowGray" style="text-align:left">附言：</td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;标题：</td>
          <td nowrap class="normalText"><input type="text" name="rtitile" size="40" value="请审批申请单"/></td>
        </tr>
        <tr>
          <td class="signText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;内容：</td>
          <td nowrap class="normalText"><textarea name="content" cols="60"></textarea></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><img src="images/refer.gif" style='cursor:hand' onclick="return refer();" >
<button class="pageBtn"  onclick="window.close();">返回</button>  </td>
        </tr>
	  </html:table>
      </td>
    </tr>
	  </html:table>
  <input type=hidden name="req_no" value="<%=request.getParameter("req_no")%>"/>
  <input type=hidden name="subFunction"/>
</form>
</html:html>
