<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/payment/req/paymentMain.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:44 $
     $Modtime: 03-08-28 14:43 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%
    String[][] payout_subj=(String[][])request.getAttribute("init_payout_subj");
    String[][] cost_subj=(String[][])request.getAttribute("init_cost_subj");
%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }
  function find() {
    if(!isNumber(template.reqsum)){
      alert('申请金额必须为数字型');
      return;
    }
    template.subFunction.value='findAll';
    template.submit();return true;
  }
  function create() {
     template.subFunction.value='preparedCreate';
     template.submit();
     return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="payRequest.jspviewhigh">
  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>支出申请记录查询</html:title>

  <!-- 查询信息 -->
    <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      System.out.println("*****************="+ro);
      String[][] result = ro.getTableResult();
      TableMarge oper = new TableMarge(ro, "return find()");
      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
      oper.addNeedButton("images/create.gif", "return create()");     //  添加
      %>
	  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText">申请时间</td>
      <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.BiDateComponent("datestart", request.getParameter("datestart"), "dateend", request.getParameter("dateend"))%></td>
      <td nowrap class="signText">申请单号</td>
      <td nowrap class="normalText"><input type="text" name="reqno" size="20" value='<%=request.getParameter("reqno")==null?"":request.getParameter("reqno")%>'/></td>
    </tr>
    <tr>
      <td nowrap class="signText">支出项目</td>
      <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(payout_subj,"payout_subj",request.getParameter("payout_subj"),false,false)%></td>
      <td nowrap class="signText">成本项目</td>
      <td nowrap class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(cost_subj,"cost_subj",request.getParameter("cost_subj"),false,false)%></td>
      <td nowrap class="signText">申请金额</td>
      <td nowrap class="normalText"><input type="text" name="reqsum" size="20" value='<%=request.getParameter("reqsum")==null?"":request.getParameter("reqsum")%>'/></td>
    </tr>
    <tr>
      <td nowrap class="signText">摘要</td>
      <td nowrap class="normalText"><input type="text" name="abstrac" size="40" value='<%=request.getParameter("abstrac")==null?"":request.getParameter("abstrac")%>'/></td>

    </tr>
	  </html:table>
	  <html:table clazz="simple">
    <tr>
      <td class="normalText" nowrap="nowrap"><input type="radio" name="pay" value="0" <%if(request.getParameter("pay")!=null && request.getParameter("pay").equals("0")) out.print("checked");%>>未批准  <input type="radio" name="pay" value="1" <%if(request.getParameter("pay")!=null && request.getParameter("pay").equals("1")) out.print("checked");%>>已批准，待支付
        <input type="radio" name="pay" value="2" <%if(request.getParameter("pay")!=null && request.getParameter("pay").equals("2")) out.print("checked");%>>已支付  <input type="radio" name="pay" value="3" <%if(request.getParameter("pay")!=null && request.getParameter("pay").equals("3")) out.print("checked");%>>作废  <input type="radio" name="pay" value="4" <%if(request.getParameter("pay")==null || request.getParameter("pay").equals("4")) out.print("checked");%>>所有
      </td>
      <td class="normalText" nowrap="nowrap">&nbsp</td>
      <td>
      <button class="pageBtn" name=""  onclick="return find();" >查询</button>
      <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
    </tr>
	  </html:table>
          <html:title clazz='table'>支出申请</html:title>
	  <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
          <td class="resultLabel">选择</td>
          <td class="resultLabel">申请单号</td>
          <td class="resultLabel">申请日期</td>
          <td class="resultLabel">申请总金额</td>
          <td class="resultLabel">录入人</td>
          <td class="resultLabel">批准日期</td>
          <td class="resultLabel">批准人</td>
          <td class="resultLabel">支付日期</td>
          <td class="resultLabel">支付人</td>
	        </html:tr>

        <%
          DecimalFormat nf = new DecimalFormat("#,##0.00");
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                String primaryKey = result[ i ][ 0 ];
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]==null || result[i][j]=="") result[i][j]="&nbsp;";
                }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
          <td class="normalText"><a href="payRequest.jspviewhigh?subFunction=load&req_no=<%=primaryKey%>"><%=primaryKey%></a></td>
          <td class="normalText"><%=result[ i ][ 1 ]%></td>
          <td class="numberText" align="right"><%=nf.format(Double.parseDouble(result[ i ][ 2 ]))%></td>
          <td class="normalText"><%=result[ i ][ 3 ]%></td>
          <td class="normalText"><%=result[ i ][ 4 ]%></td>
          <td class="normalText"><%=result[ i ][ 5 ]%></td>
          <td class="normalText"><%=result[ i ][ 6 ]%></td>
          <td class="normalText"><%=result[ i ][ 7 ]%></td>
        </tr>

        <%
              }
            }
        %>
	      </html:table>
    </td>
  </tr>
  <!-- 操作 -->

	      </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>
