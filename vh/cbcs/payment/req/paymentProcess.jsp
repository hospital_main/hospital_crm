<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/payment/req/paymentProcess.jsp,v 1.1 2012/03/12 01:58:44 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:58:44 $
     $Modtime: 03-08-26 11:22 $
     $Revision: 1.1 $
     $NoKeywords: $ -->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Header(101, request)%>
<%
  String[][] result = (String[][])request.getAttribute("table_result");
%>
<Script Language="JavaScript">

</Script>

<form name="template" method="post" action="payRequest.jspviewhigh">
  <!-- 信息提示栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTip(request)%>

  <!-- 标题栏 -->
  <%=new com.viewhigh.cbcs.base.mvc.view.MessageTitle("查看办理过程：")%>
<table width="100%" border="0">
  <tr>
    <table BORDERCOLOR="#214597" width="100%" cellspacing="1" border="1">
       <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j]!=null && result[i][j]=="") result[i][j]="&nbsp;";
                }
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
       %>

       <tr CLASS="<%=rowColor%>">
         <td class="normalText"><a href="javascript:viewprocess('<%=i%>');"><%=i+1%>.</a></td>
         <td class="normalText"><%=result[i][1]%></td>
         <td class="normalText"><%=result[i][2]%></td>
         <td class="normalText"><%=result[i][3]%></td>
       </tr>
       <%
              }
            }
       %>

    </table>
  </tr>
  <tr>
    <td><button class="pageBtn"onclick="return backto();" >返回</button> </td>
  </tr>
  <tr>
    <table width="100%" border="0">
      <tr>
        <td COLSPAN="2" VLAIGN="top" >
          <iframe  VSPACE="1" HSPACE="1" FRAMEBORDER="0" WIDTH="420" SRC="" NAME="view_process" HEIGHT="500" ></iframe>
        </td>
      </tr>
    </table>
  </tr>
</table>
</form>
<%=new com.viewhigh.cbcs.base.mvc.view.margin.Footer(101)%>