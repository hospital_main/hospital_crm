<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/dept/deptAddtwo.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<%
	   String year_month = request.getParameter("year_month");
	   String dept_clin_code = request.getParameter("dept_clin_code");
	   String dept_status = (String)request.getAttribute("dept_status");
	   String module = (String)request.getAttribute("module");
	   request.setAttribute("module",module);
     int i = 0;
%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function return1() {
    template.subFunction.value = "go<%= module%>DeptMain";
    show_wait();
    template.submit();
    return true;
  }
  function save() {
    var table=document.getElementById('table');
		var tbody=table.firstChild;
		for(var i=2;i<tbody.childNodes.length;i++){
		  input=tbody.childNodes[i].childNodes[1].firstChild;
		  if(trim(input.value) == '')
		  {
		    alert("元素名称不能为空，请添加完整!");
		    input.focus();
		    return false;
		  }
		  input=tbody.childNodes[i].childNodes[2].firstChild;
		  if(trim(input.value) == '')
		  {
		    alert("元素值不能为空，请添加完整!");
		    input.focus();
		    return false;
		  }
		}
    template.subFunction.value = "go<%= module%>DeptAddtwo";
    show_wait();
    template.submit();
    return true;
  }

  function check(o){
    var c = o.value;
    if(!checkMoney(o)){
      o.select();
      return false;
    }
    if(c<-10000000){
      alert("元素值不应小于-10000000!");
      o.select();
      return false;
    }
    if(c>=10000000){
      alert("元素值应小于10000000!");
      o.select();
      return false;
    }
    var amount_sum = 0;
    var table=document.getElementById('table');
		var tbody=table.firstChild;
		for(var i=2;i<tbody.childNodes.length;i++){
		  input=tbody.childNodes[i].childNodes[2].firstChild;
		  if(input.value != '')
		  {
		    amount_sum = parseFloat(amount_sum) + parseFloat(input.value);
		  }
		}
		eval('sum').innerText = amount_sum;
	  template.sum_amount.value = amount_sum;
    return true;
  }

	function createTable(tableId,id){
	  var text;
	  var input;
	  var td;
	  var table=document.getElementById(tableId);
		var tbody=table.firstChild;
		var length=tbody.childNodes.length-1;
		var tr=document.createElement("tr");
		tbody.appendChild(tr);
		//添加一个为多选按钮的列
		td=document.createElement("<td align='center'/>");
		tr.appendChild(td);
		var check=document.createElement("<input type='checkbox' name='primaryKey'/>");
		td.appendChild(check);
	  //添加一个输入域的列
	  td=document.createElement("<td class='normalText' width='40%'/>");
		tr.appendChild(td);
		input=document.createElement("<input id='element_name1"+length+"' type='text'  class='textInputE' maxlength='14'  />");
		input.name='element_name'+length;
		td.appendChild(input);
		//添加一个输入域的列
	  td=document.createElement("<td class='normalText' width='40%'/>");
		tr.appendChild(td);
		input=document.createElement("<input id='element_value1'"+length+" type='text' class='textInputE' maxlength='14' onKeyPress='return isFloat(event)' onblur='return check(this)'/>");
		input.name='element_value'+length;
		td.appendChild(input);
	  //给隐藏域赋值
	  var hidden=document.getElementById(id);
	  hidden.value=tbody.childNodes.length-2;
	 }
	function deleteTable(tableId,id){
		var j=0;
	  var table=document.getElementById(tableId);
		var tbody=table.firstChild;

	  var flag=false;
		for(var i=2;i<tbody.childNodes.length;i++){
		  var td=tbody.childNodes[i].firstChild;
		  var box=td.firstChild;
		  if(box.checked==true){
		    flag = true;
		    break;
		  }
		}
		if(!flag)
		{
			alert( "请先选择,再删除!");
		}
	  else if (confirm('是否删除')) {
			for(var i=2;i<tbody.childNodes.length;i++){
			  var td=tbody.childNodes[i].firstChild;
			  var box=td.firstChild;
			  if(box.checked==true){
			  tbody.removeChild(tbody.childNodes[i]);
			  i--;
			  }
			}
			tbody=table.firstChild;
			for(var i=2;i<tbody.childNodes.length;i++){
			  var text=tbody.childNodes[i].childNodes[0].firstChild;
			  text.data=""+i;
			  var input;
			  input=tbody.childNodes[i].childNodes[1].firstChild;
			  input.name='element_name'+(i-1);
			  input=tbody.childNodes[i].childNodes[2].firstChild;
			  input.name='element_value'+(i-1);
			}
			var hidden=document.getElementById(id);
		  hidden.value=tbody.childNodes.length-2;
	    return true;
	  } else
	      return false;
  }

  function init()
  {
    var amount_sum = 0;
    var table=document.getElementById('table');
		var tbody=table.firstChild;
		for(var i=2;i<tbody.childNodes.length;i++){
		  input=tbody.childNodes[i].childNodes[2].firstChild;
		  if(input.value != '')
		  {
		    amount_sum = parseFloat(amount_sum) + parseFloat(input.value);
		  }
		}
		eval('sum').innerText = amount_sum;
		template.sum_amount.value = amount_sum;
  }
</script>
<html:html clazz="main">
<form name="template" method="post" action="dist<%= module%>Dept.jspviewhigh">
	<!-- 信息提示栏 -->
	<html:message/>

	<!-- 标题栏 -->
	<html:title clazz='module'>加减项2维护</html:title>

	<!-- 简单信息 -->
	<html:table clazz="simple">
    <tr>
      <td class='tableTitle' nowrap='nowrap'>
<%
		   if(year_month!=null){
		      out.print(year_month.substring(0,4)+"年");
		      out.print(year_month.substring(4,6)+"月");
		   }
		   out.print(CommonData.getDeptName(dept_clin_code));
%>
       加减项2
      </td>
    </tr>
  </html:table>
	<!-- 复杂信息 -->
	<html:table clazz="complex">
	  <!-- 操作 -->
    <tr>
      <td>
        <table width='100%'>
          <tr>
            <td  nowrap='nowrap'>
<%
     if(!"Y".equals(dept_status)){
%>
              <button class="pageBtn" onclick="return createTable('table','hidden');" >添加</button>
              <!--<img src="images/create.gif" class="mouse" onclick="return createTable('table','hidden');"/>-->
              <button src="images/remove.gif" class="pageBtn" onclick="return deleteTable('table','hidden');">删除</button>
              <button src="images/save.gif" class="pageBtn" onclick="return save();">保存</button>
              <button src="images/reset.gif" class="pageBtn" onclick="return reset();">重置</button>
              <button class="pageBtn" onclick="return return1();">返回</button> 
              <!--<img src="images/return.gif" class="mouse" onclick="return return1();"/>-->
 <%
     } else {
 %>
              <button class="pageBtn" onclick="history.back()">返回</button> 
             <!-- <img src="images/return.gif" class="mouse" onclick="history.back()"/>-->
 <%
     }
 %>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <!-- 结果集 -->
    <tr>
    	<td>
        <div style='overflow:auto; width:830px; height:390px'>
          <table width='100%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' id="table" >
            <tr>
              <td class="resultLabel">选择</td>
              <td class="resultLabel">元素名称</td>
              <td class="resultLabel">元素值</td>
             </tr>
            <tr class="rowGray">
              <td class="normalText">&nbsp;</td>
              <td class="normalText">加减项2 总计</td>
              <td class="numberText" id="sum"></td>
            </tr>
<%
    String[][] result = (String[][])request.getAttribute("result");
    if (result!=null) {
      for (i=0; i<result.length; i++) {
         for(int j=0;j<result[i].length;j++){
          if(result[i][j]==null||result[i][j].equals(""))
            result[i][j]="";
         }
        String rowColor = "rowGray";
        if (i/2*2==i) rowColor = "rowWhite";
        if(!"Y".equals(dept_status)){//如果没有完成.则显示输入框
 %>
		      <tr class="<%=rowColor%>">
		        <td align='center' nowrap>
		          <input name="primaryKey" type="checkbox" value="<%=result[i][0]%>" >
		        </td>
		        <td class="normalText" width="40%" nowrap>
		          <input type=text name='<%="element_name"+(i+1)%>' class="textInputE" maxlength="14" value="<%=result[i][0]%>">
		        </td>
		        <td class="normalText" width="40%" nowrap>
              <input type=text name='<%="element_value"+(i+1)%>' class='textInputE' maxlength='14' value="<%=changeFormat(result[i][1],"##0.00")%>" onKeyPress='return isFloat(event)' onblur='return check(this)'/>
		        </td>
		      </tr>
<%
        } else {//分配完成,显示一般文本.
%>
		      <tr class="<%=rowColor%>">
		        <td align='center'>&nbsp;</td>
		        <td class="normalText" width="40%" nowrap>
		          <%=result[i][0]%>
		        </td>
		        <td class="numberText" width="40%" nowrap>
		          <input type=hidden name='<%="element_value"+(i+1)%>' value="<%=changeFormat(result[i][1],"##0.00")%>"/>
		          <%=changeFormat(result[i][1])%>
		        </td>
		      </tr>
<%
        }
      }
    }
%>
        </table>
      </div>
    </td>
  </tr>
</html:table>
<input type='hidden' name="sum_amount" value="0"/>
<input type="hidden" name="subFunction">
<input type='hidden' name="length" value="<%= i%>" id="hidden"/>
<input type='hidden' name="year_month" value="<%= request.getParameter("year_month")%>"/>
<input type='hidden' name="dept_clin_code" value="<%= request.getParameter("dept_clin_code")%>"/>
<input type='hidden' name="dept_status" value="<%= request.getParameter("dept_status")%>"/>
</form>
<Script Language="JavaScript">
  init();
</Script>
</html:html>
