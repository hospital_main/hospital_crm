<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/dept/deptCost.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
  String module = (String)request.getAttribute("module");
%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function return1() {
	  template.subFunction.value='go<%= module%>DeptMain';
	  show_wait();
    template.submit();
    return true;
  }
  
	function find() {
	  template.subFunction.value='go<%= module%>DeptCost';
	  show_wait();
    template.submit();
    return true;
	}  
</script>
<%
  String year_month =request.getParameter("year_month");
  String dept_clin_code =request.getParameter("dept_clin_code");
  BaseRO ro = (BaseRO)request.getAttribute("result");
  TableMarge oper = new TableMarge(ro, "find()");
  oper.addNeedButton("images/return.gif", "return1()");   // back
%>
<html:html clazz="main">
<form name="template" method="post" action="dist<%= module%>Dept.jspviewhigh">
	<!-- 信息提示栏 -->
	<html:message/>

	<!-- 标题栏 -->
	<html:title clazz='module'>成本明细表</html:title>

	<!-- 简单信息 -->
	<html:table clazz="simple">
    <tr>
      <td class='tableTitle' nowrap='nowrap'>
        <%
          if(year_month!=null){
            out.print(year_month.substring(0,4)+"年");
            out.print(year_month.substring(4,6)+"月");
           }
          out.print(CommonData.getDeptName(dept_clin_code));
       %>
       成本明细表
      </td>
    </tr>
  </html:table>
	<!-- 复杂信息 -->
	<html:table clazz="complex">
	  <!-- 操作 -->
    	<button class="pageBtn" onclick="history.back()">返回</button> 
    <!-- 结果集 -->
    <tr>
    	<td>
        <html:table clazz="result">
					<tr>
						<td class="resultLabel">科室编码</td>
						<td class="resultLabel">科室名称</td>
						<td class="resultLabel">成本编码</td>
						<td class="resultLabel">成本名称</td>
						<td class="resultLabel">直接成本</td>
						<td class="resultLabel">公用成本</td>
						<td class="resultLabel">管理成本</td>
						<td class="resultLabel">医疗辅助成本</td>
						<td class="resultLabel">医疗技术成本</td>
					</tr>
<%
  if (ro!=null) {
    String[][] result = ro.getTableResult();
    if (result!=null) {
      for (int i=0; i<result.length; i++) {
         for(int j=0;j<result[i].length;j++){
          if(result[i][j]==null||result[i][j].equals(""))
            result[i][j]="&nbsp;";
         }

        String rowColor = "rowGray";
        if (i/2*2==i) rowColor = "rowWhite";
%>
          <tr class="<%=rowColor%>">
<%
          for(int j=0;j<4;j++){
%>
            <td class="normalText" nowrap><%= result[i][j]%></td>
<%
          }
          for(int j=4;j<result[i].length;j++){
%>
            <td class="numberText" nowrap><%= changeFormat(result[i][j])%></td>
<%
          }
%>
          </tr>
<%
      }
    }
  }
%>
    </html:table>
   </td>
 </tr>
</html:table>
<input type="hidden" name="subFunction">
<input type='hidden' name="year_month" value="<%= request.getParameter("year_month")%>"/>
<input type='hidden' name="dept_clin_code" value="<%= request.getParameter("dept_clin_code")%>"/>
<input type='hidden' name="dept_status" value="<%= request.getParameter("dept_status")%>"/>
</form>
</html:html>
