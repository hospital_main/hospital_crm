<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/dept/status/otherBonusMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>

<%
  String year_month = (String)request.getParameter("year_month");
  String bonus_code = (String)request.getParameter("bonus_code");
  String title = "设置表";
  if (year_month!=null && year_month.length()==6) {
    title = year_month.substring(0, 4)+"年"+year_month.substring(4)+"月"+CommonData.getBonusName(bonus_code)+title;
  }
  String[][] result = (String[][])request.getAttribute("result");
%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>

<html:html clazz="main">
<form name="template" method="post" action="distOtherBonus.jspviewhigh">
	<html:message/>

	<!-- 标题栏 -->
	<html:title clazz='module'>其他奖金分配</html:title>

	<!-- 简单信息 -->
	<html:table clazz="simple">
		<tr>
			<td nowrap width="40"></td>
	 	<%if (result!=null) {%>
	 	  <td> <button class="pageBtn" onclick="return preparedPrint();">打印</button></td>
	 	<%}%>
      <td><button class="pageBtn" onclick="return history.back()" >返回</button></td>
		</tr>
  </html:table>


  <br>
  <html:title clazz='table'><%=title%></html:title>

  <html:table clazz="complex">
<%if (result==null || result.length<2) {%>
    <tr>
      <td>
    		<html:table clazz="result">
				  <tr id='label'>
				    <td nowrap class="resultLabel">分配科室名称</td>
				    <td nowrap class="resultLabel">人数</td>
				    <td nowrap class="resultLabel">实发金额</td>
				    <td nowrap class="resultLabel">人均金额</td>
				  </tr>
				</html:table>
		  </td>
		</tr>
<%} else {%>
    <tr>
      <td>
				<html:table clazz="result">
				  <tr id='label'>
				    <td nowrap class="resultLabel">分配科室名称</td>
				    <td nowrap class="resultLabel">人数</td>
				    <td nowrap class="resultLabel">实发金额</td>
				    <td nowrap class="resultLabel">人均金额</td>
				  </tr>
        <%for (int i=0; i<result.length; i++) {
            String rowColor = "rowGray";
            if (i/2*2==i)
              rowColor = "rowWhite";%>
          <tr class="<%=rowColor%>">
            <td class='normalText'><%=result[i][0]%></td>
            <td class='numberText'><%=result[i][1]%></td>
            <td class='numberText'><%=result[i][2]%></td>
            <td class='numberText'><%=result[i][3]%></td>
          </tr>
        <%}%>
        </html:table>
      </td>
    </tr>
<%}%>
  </html:table>
  <input type="hidden" name="subFunction">
</form>
</html:html>

