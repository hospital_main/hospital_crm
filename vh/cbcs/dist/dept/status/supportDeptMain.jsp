<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/dept/status/supportDeptMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
  String[][] result = (String[][])request.getAttribute("result");
  String[][] title = (String[][])request.getAttribute("title");
  String year_month = request.getParameter("year_month")==null?null:(String)request.getParameter("year_month");
%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function back() {
    history.back();
  }

	function print() {
	  template.subFunction.value = "goSupportDeptPrint";
	  show_wait();
	  template.submit();
    return true;
	}
</script>
<html:html clazz="main">
<form name="template" method="post" action="distDeptStatus.jspviewhigh">
	<!-- 信息提示栏 -->
	<html:message/>

	<!-- 标题栏 -->
	<html:title clazz='module'>服务科室奖金分配</html:title>

	<!-- 简单信息 -->
	<html:table clazz="simple">
		<tr>
	    <td nowrap width="40"></td>
			<td width="100"> <button class="pageBtn" onclick="preparedPrint();">打印</button></td>
	    <td width="100"><button class="pageBtn" onclick="return back();" >返回</button></td>
	    <td> </td>
			<td class="normalText" width="40"> </td>
	  </tr>
  </html:table>
	<table width='100%' border='0'>
    <tr>
      <td class='tableTitle' nowrap='nowrap'>
        <%
          if(year_month!=null){
             out.print(year_month.substring(0,4)+"年");
             out.print(year_month.substring(4,6)+"月");
          }
        %>
        服务科室奖金分配表
      </td>
    </tr>
	</table>
	<!-- 复杂信息 -->
	<html:table clazz="complex">
 	 	<!-- 操作 -->
		<!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
   			  <tr>
          <%
  			    if(title != null) {
  			    	for (int i=0; i<title[0].length; i++) {
				  %>
				    <td class="resultLabel" nowrap><%= title[0][i]%></td>
				  <%
				      }
				    }
				  %>
				  </tr>
				  <%
				    if (result!=null) {
				      int rowLength = result.length;
				      int colLength = result[0].length;
				    	for (int i=0; i<result.length; i++) {
				      	for(int j=0;j<result[i].length;j++){
				        	if(result[i][j]==null||result[i][j].equals(""))
				          	result[i][j]="&nbsp";
				        }
				        String rowColor = "rowGray";
        				if (i/2*2==i)
                   rowColor = "rowWhite";
				  %>
				  <tr class="<%=rowColor%>">
            <td class="normalText"><%= result[i][0]%></td>
            <td class="numberText"><%= result[i][1]%></td>
            <td class="numberText"><%= result[i][2]%></td>
            <td class="numberText"><%= result[i][3]%></td>
            <td class="numberText"><%= result[i][4]%></td>
            <td class="numberText"><%= result[i][5]%></td>
            <td class="numberText"><%= result[i][6]%></td>
            <td class="numberText"><%= result[i][7]%></td>
            <td class="numberText"><%= result[i][8]%></td>
            <td class="numberText"><%= result[i][9]%></td>
            <td class="numberText"><%= result[i][10]%></td>
				  </tr>
				  <%
			  		  }
			      }
				  %>
				</html:table>
    	</td>
    </tr>
		<!-- 操作 -->
	</html:table>
  <input type="hidden" name="subFunction">
</form>
</html:html>
