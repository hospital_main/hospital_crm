<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/dept/manageDeptMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<%
  String control_id = (String)request.getAttribute("control_id");
  String control_warning = (String)request.getAttribute("control_warning");
  String dept_status = (String)request.getAttribute("dept_status")==null?"0":(String)request.getAttribute("dept_status");
  String[][] result = (String[][])request.getAttribute("result");
  String[][] title = (String[][])request.getAttribute("title");
  String year_month = request.getParameter("year_month")==null?null:(String)request.getParameter("year_month");  
%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

  function find() {
    if (template.year_month.value == "") {
      alert("请选择年月！");
      return false
    }
	  template.subFunction.value = "goManageDeptMain";
    show_wait();
    template.submit();
    return true;
	}
	
  function initial(){
    if (template.year_month.value == "") {
      alert("请选择年月！");
      return false
    }
    if (template.year_month.value != <%= year_month%>) {
      alert("核算月已经改变，请重新查询！");
      return false
    }
    template.subFunction.value = "goManageDeptInitial";
  <%
    if("O".equals(dept_status)){//如果本月经营科室奖金分配没有完成
  %>
    alert('本月经营科室奖金分配没有完成！');
    return false;
  <%
    }
    if("P".equals(dept_status)){//如果奖金分配正在进行中
  %>
    if (confirm('执行此操作会丢失本月数据，是否继续?')) {
      show_wait();
      template.submit();
      return true;
    } else
      return false;
  <%
    }
  %>
    show_wait();
    template.submit();
    return true;
  }
  
	function finish() {
    if (template.year_month.value != <%= year_month%>) {
      alert("核算月已经改变，请重新查询！");
      return false
    }	
	  template.subFunction.value = "goManageDeptClose";
	  show_wait();
	  template.submit();
    return true;
	}
</script>
<html:html clazz="main">
<form name="template" method="post" action="distManageDept.jspviewhigh">
  <%
    if(control_warning == null) {
  %>
	<!-- 信息提示栏 -->
	<html:message/>
	<%
    }
  %>

	<!-- 标题栏 -->
	<html:title clazz='module'>管理科室奖金分配</html:title>

	<!-- 简单信息 -->
	<html:table clazz="simple">
		<tr>
	    <td nowrap width="40"></td>
	  	<td nowrap class="signText" width="72">核算月:</td>
	    <td nowrap class="normalText" colspan='3'>
	    	<!-- 年月组件 -->
	      <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
	    </td>
	    <td width="100">
	    <button class="pageBtn" name=""   onclick="find();"  >查询</button>
	    <!--<img src="images/find.gif" class="mouse" onclick="find();" />--></td>
			<%
			  if("Y".equals(dept_status)){
			%>
			<td width="100"> <button class="pageBtn"  onclick="preparedPrint();" >打印</button></td>
			<%
			  }
			%>
			<td class="normalText" width="40"> </td>
	  </tr>
  </html:table>
	<table width='100%' border='0'>
    <tr>
      <td class='tableTitle' nowrap='nowrap'>
        <%
          if(year_month!=null){
             out.print(year_month.substring(0,4)+"年");
             out.print(year_month.substring(4,6)+"月");
          }
        %>
        管理科室奖金分配表
      </td>
    </tr>
	</table>
	<!-- 复杂信息 -->
	<html:table clazz="complex">
 	 	<!-- 操作 -->
    <tr>
      <td>
        <table width='100%' border='0'>
          <tr>
      			<%
      			  if("N".equals(dept_status)||"O".equals(dept_status)){
      			%>
      			<td width="100"><button src='images/initial.gif' class='pageBtn' onclick='return initial()'>初始化</button></td>
      			<%
      			  } else if("P".equals(dept_status)){
      			%>
            <td  nowrap='nowrap'>
							<button src='images/initial.gif' class='pageBtn' onclick='return initial()'>初始化</button>
							<button src='images/finish.gif' class='pageBtn' onclick='return finish()'>完成</button>
            </td>
            <%
              }
            %>
          </tr>
        </table>
      </td>
    </tr>
		<!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
   			  <tr>
					<%
  			    if(title != null) {
  			    	for (int i=0; i<title[0].length; i++) {
				  %>
				    <td class="resultLabel" nowrap><%= title[0][i]%></td>
				  <%
				      }
				    }
				  %>
				  </tr>
				  <%
				    if (result!=null) {
				      int rowLength = result.length;
				      int colLength = result[0].length;
				    	for (int i=0; i<result.length; i++) {
				      	for(int j=0;j<result[i].length;j++){
				        	if(result[i][j]==null||result[i][j].equals(""))
				          	result[i][j]="";
				        }
				        String rowColor = "rowGray";
        				if (i/2*2==i)
                   rowColor = "rowWhite";
				  %>
				  <tr class="<%=rowColor%>">
					  <td class="normalText" nowrap><input type="hidden" name="dept_clin_code" value="<%= result[i][0]%>"><%= result[i][1]%></td>
					  <td class="numberText" nowrap><%= result[i][2]%></td>
					  <td class="numberText" nowrap><%= result[i][3]%></td>
					  <td class="numberText" nowrap><%= result[i][4]%></td>
					  <!--考核成绩-->
					  <td class="numberText" nowrap>
					    <a href="distManageDept.jspviewhigh?subFunction=goManageDeptPreScore&dept_clin_code=<%= result[i][0]%>&year_month=<%= request.getParameter("year_month")%>"><%= result[i][5]%></a>
					  </td>
					  <!--成本-->
					  <td class="numberText" nowrap>
					    <a href="distManageDept.jspviewhigh?subFunction=goManageDeptCost&dept_clin_code=<%= result[i][0]%>&year_month=<%= request.getParameter("year_month")%>"><%= result[i][6]%></a>
					  </td>
					  <td class="numberText" nowrap><%= result[i][7]%></td>
					  <td class="numberText" nowrap><%= result[i][8]%></td>
					  <td class="numberText" nowrap><%= result[i][9]%></td>
					  <td class="numberText" nowrap>
					    <a href="distManageDept.jspviewhigh?subFunction=goManageDeptPreMultiply&dept_clin_code=<%= result[i][0]%>&year_month=<%= request.getParameter("year_month")%>"><%= result[i][10]%></a>
					  </td>
					  <td class="numberText" nowrap>
					    <a href="distManageDept.jspviewhigh?subFunction=goManageDeptPreAddtwo&dept_clin_code=<%= result[i][0]%>&year_month=<%= request.getParameter("year_month")%>"><%= result[i][11]%></a>
					  </td>
					  <td class="numberText" nowrap><%=result[i][12]%></td>
					  <td class="numberText" nowrap><%=result[i][13]%></td>
				  </tr>
				  <%
			  		  }
			      }
				  %>
				</html:table>
    	</td>
    </tr>
		<!-- 操作 -->
	</html:table>
  <input type="hidden" name="control_id" value="0">	
  <input type="hidden" name="subFunction">
  <input type="hidden" name="last_year_month" value="<%=request.getParameter("year_month")%>">
</form>
</html:html>
<Script Language="JavaScript">
<%
  if("1".equals(control_id)) {
%>
  if (confirm('<%= control_warning%>')) {
    template.control_id.value = 1;
	  template.subFunction.value = "goManageDeptClose";
	  show_wait();
	  template.submit();
  }
<%  
  } else if("0".equals(control_id)) {
%>  
  template.control_id.value = 2;
  alert('<%= control_warning%>');
<%
  }
%>  
</Script>