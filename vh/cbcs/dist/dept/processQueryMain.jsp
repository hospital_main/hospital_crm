<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/dept/processQueryMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
function find() {
	if(template.year.value==""){
	  alert("请选择年份!");
	  return false
	}
	template.subFunction.value='goProcessQueryMain';
	show_wait();
	template.submit();return true;
}
</Script>
<html:html clazz="main">
<form name="template" method="post" action="distProcessQuery.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>奖金定额查询</html:title>
  <!-- 查询信息 -->
  
  <html:table clazz="simple">
    <tr>
      <td class="normalText" width="40">　</td>
      <td nowrap class="signText">期间：
        <%
          java.util.Date d = new java.util.Date();
          String nowYear =String.valueOf(d.getYear()+1900);
        %>
        <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getYears(),"year",request.getParameter("year")==null?nowYear:request.getParameter("year"),false,true)%>年
      </td>
      <td width="100">
      <button class="pageBtn" name="" onclick="return find();" >查询</button>
      <!-- <img src="images/find.gif" style="cursor:hand" onclick="return find();"/>-->
        </td>
      <td class="normalText" width="40"> </td>
    </tr>
  </html:table>
	<table width='100%' border='0'>
		<tr>
		  <td class='tableTitle' nowrap='nowrap'><%=request.getParameter("year")==null?"":request.getParameter("year")+"年"%>奖金定额表</td>
		</tr>
	</table>   
	<%
	  String[][] result =(String[][])request.getAttribute("result");
	  if ( result != null ){
	%>
  <html:table clazz="complex">
  <!-- 操作 -->
  
  <!-- 结果集 -->
  <tr>
    <td>
      <html:table clazz="result">
        <html:tr clazz='label'>
          <td colspan=2 nowrap>核算类型</td>
          <td nowrap><%=request.getParameter("year")==null?"":request.getParameter("year")%>年
          </td>
          <td nowrap>1月</td>
          <td nowrap>2月</td>
          <td nowrap>3月</td>
          <td nowrap>4月</td>
          <td nowrap>5月</td>
          <td nowrap>6月</td>
          <td nowrap>7月</td>
          <td nowrap>8月</td>
          <td nowrap>9月</td>
          <td nowrap>10月</td>
          <td nowrap>11月</td>
          <td nowrap>12月</td>
        </html:tr>
        <%
          for (int i = 0; i < result.length; i++ ){
            for (int j=0; j<result[i].length; j++){
              if (result[i][j]==null || result[i][j]=="")
                 result[i][j]="";
              }
              String rowColor = "rowGray";
              if (i/2*2==i)
                rowColor = "rowWhite";
         %>
          <tr CLASS="<%=rowColor%>" >
          <%
            if(i%2==0){
          %>
              <td class="normalText" rowspan=2 nowrap>
          <%
							switch(i){
							 case 0:out.print("全院");break;
							 case 2:out.print("经营科室");break;
							 case 4:out.print("管理科室");break;
							 case 6:out.print("服务科室");break;
							 case 8:out.print("其他奖金");break;
							}
				  %>
              </td>
          <%
            }
            if(i%2==0) {
          %>
              <td class="normalText" nowrap>定额</td>
          <%
					  } else {
				  %>
							<td class="normalText" nowrap>实际金额</td>
				  <%
					  }
							
						for (int j=0; j<result[i].length; j++){
						  if(i%2==1 && result[i-1][j]!=null && !"".equals(result[i][j]) && !"".equals(result[i-1][j]) && (Double.parseDouble(result[i][j].trim())>Double.parseDouble(result[i-1][j].trim()))){
				  %>
							<td class="numberText" style="color:red" nowrap>
				  <%
							 } else {
				  %>
							<td class="numberText" nowrap>
				  <%
							 }
				  %> 
							<%=("".equals(result[i][j]))?"":changeFormat(result[i][j])%>
							</td>
					<%
					  }
					%>
        </tr>
        <%
          }
        %>
       </html:table>
    </td>
  </tr>
  <!-- 操作 -->

  </html:table>
  <%
    }
  %>  
  <input type=hidden name="subFunction"/>
</form>
</html:html>


