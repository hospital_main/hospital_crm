<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/dept/deptStatusMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function find(){
    if(template.year.value==""){
      alert("请选择年");
      return false
    }
    template.subFunction.value='goDeptStatusMain';
    template.submit();
    show_wait();
    return true;
  }
</Script>
<%
  String[][] result = (String[][])request.getAttribute("result");
  java.util.Date d = new java.util.Date();
  String nowYear =String.valueOf(d.getYear()+1900);
%>
<html:html clazz="main">
<form name="template" method="post" action="distDeptStatus.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>
  <!-- 标题栏 -->
  <html:title clazz='module'>分配状态</html:title>
    <br>
  <!-- 查询信息 -->
  <html:table clazz="simple">
    <tr>
      <td class="normalText" width="40"> </td>
      <td nowrap class="signText">期间：
        <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getYears(),"year",request.getParameter("year")==null?nowYear:request.getParameter("year"),false,true)%>年
      </td>
      <td width="100">
      <button class="pageBtn" name=""  onclick="return find();" >查询</button>
      <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
      <td class="normalText" width="40"> </td>
    </tr>
  </html:table>
  <table width='100%' border='0'>
    <tr>
      <td class='tableTitle' nowrap='nowrap'>
        <%
         String year_month = request.getParameter("year")==null?null:(String)request.getParameter("year");
         if(year_month!=null){
           out.print(year_month+"年");
         }
       %>
       分配科室设置表
      </td>
    </tr>
  </table>
  <html:table clazz="complex">
  <!-- 操作 -->
  <!-- 结果集 -->
  <tr>
    <td>
      <html:table clazz="result">
        <html:tr clazz='label'>
          <td>核算类型</td>
          <td>1月</td>
          <td>2月</td>
          <td>3月</td>
          <td>4月</td>
          <td>5月</td>
          <td>6月</td>
          <td>7月</td>
          <td>8月</td>
          <td>9月</td>
          <td>10月</td>
          <td>11月</td>
          <td>12月</td>
        </html:tr>
        <%
          if ( result != null ){
            for (int i = 0; i < result.length; i++ ){
              String primaryKey = result[i][0];
              for (int j=0; j<result[i].length; j++){
                if (result[i][j]==null || result[i][j]=="") result[i][j]="&nbsp;";
              }
              String rowColor = "rowGray";
              if (i/2*2==i) rowColor = "rowWhite";
        %>
        <tr CLASS="<%=rowColor%>">
        <td class="normalText" nowrap><%=CommonData.getWorkItemName(result[i][1])%></td>
        <%
         String work_item = "";
         String action = "";
         String bonus_code = "";
         for(int j=2;j<result[i].length;j++){
           if(("完成".equals(result[i][j])) && (!"0101".equals(result[i][1]))){
             if("0201".equals(result[i][1])) {
               action = "distBenefitDept";
               work_item = "goStatusBenefitMain";
             } else if ("0202".equals(result[i][1])) {
               action = "distManageDept";
               work_item = "goStatusManageMain";
             } else if ("0203".equals(result[i][1])) {
               action = "distSupportDept";
               work_item = "goStatusSupportMain";
             } else if (result[i][1].startsWith("9")) {
               action = "distOtherBonus";
               work_item = "goStatusOtherBonusMain";
               bonus_code = "&bonus_code=" + result[i][1];
             }
       %>
        <td class="normalText" nowrap>
          <a href="<%= action%>.jspviewhigh?subFunction=<%= work_item%>&year_month=<%= result[i][0] + ((j<11)?"0"+(j-1):""+(j-1))%><%= bonus_code%>"><%=result[i][j]%></a>
        </td>
        <% 
          } else {
        %>
        <td class="normalText" nowrap><%=result[i][j]%></td>
        <%
          }
        }
        %>
        </tr>
     <%
        }
     }
     %>
      </html:table>
    </td>
  </tr>
  <!-- 操作 -->
  </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>
