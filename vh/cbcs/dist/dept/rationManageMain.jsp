<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/dept/rationManageMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function find() {
	  template.subFunction.value='goRationManageMain';
	  show_wait();
	  template.submit();
	  return true;
  }
  function save(){
    if (template.year.value != <%= request.getParameter("year")%>) {
      alert("核算月已经改变，请重新查询！");
      return false
    }
		template.subFunction.value='goRationManageSet';
		show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="distRationManage.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>奖金定额维护</html:title>
  <!-- 查询信息 -->
  
  <html:table clazz="simple">
    <tr>
      <td class="normalText" width="40">　</td>
      <td nowrap class="signText">期间：
        <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getYears(),"year",request.getParameter("year"),false,true)%>年
      </td>
      <td width="100">
      <button class="pageBtn" name="" onclick="return find();">查询</button>
      <!--<img src="images/find.gif" style="cursor:hand" onclick="return find();"/>-->
      </td>
      <td class="normalText" width="40"> </td>
    </tr>
	</html:table>
	<table width='100%' border='0'>
		<tr>
		  <td class='tableTitle' nowrap='nowrap'><%=request.getParameter("year")==null?"":request.getParameter("year")+"年"%>奖金定额维护表</td>
		</tr>
	</table>
	<%
		String[][] result = (String[][])request.getAttribute("result");
		String[][] status = (String[][])request.getAttribute("status");
		if( result != null) {
	%>
  <!-- 操作 -->
  <html:table clazz="complex">
  <tr>
    <td>
      <button src='images/reset.gif' class='pageBtn' onclick='return reset()'>重置</button>
      <button src='images/save.gif' class='pageBtn' onclick='return save()'>保存</button>
    </td>
  </tr>
  <!-- 结果集 -->
  <tr>
    <td>
    <html:table clazz="result">
			<html:tr clazz='label'>
				<td nowrap>核算类型</td>
				<td nowrap><%=request.getParameter("year")==null?"":request.getParameter("year")%>年</td>
				<td nowrap>1月</td>
				<td nowrap>2月</td>
				<td nowrap>3月</td>
				<td nowrap>4月</td>
				<td nowrap>5月</td>
				<td nowrap>6月</td>
				<td nowrap>7月</td>
				<td nowrap>8月</td>
				<td nowrap>9月</td>
				<td nowrap>10月</td>
				<td nowrap>11月</td>
				<td nowrap>12月</td>
			</html:tr>
      <%
				for (int i = 0; i < result.length; i++ )
				{
					for (int j=0; j<result[i].length; j++)
					{
					  if (result[i][j]==null || result[i][j]=="")
					    result[i][j]="&nbsp;";
					}
					String rowColor = "rowGray";
					if (i/2*2==i)
					  rowColor = "rowWhite";
      %>
			<tr CLASS="<%=rowColor%>">
			  <td class="normalText" nowrap>
        <%
					switch(i){
					 case 0:out.print("全院");break;
					 case 1:out.print("经营科室");break;
					 case 2:out.print("管理科室");break;
					 case 3:out.print("服务科室");break;
					 case 4:out.print("其他奖金");break;
					}
        %>
        </td>
        <td class="numberText" nowrap><%= changeFormat(result[i][3])%></td>
        <%
          for(int j=4; j<result[i].length; j++) {
        %>
        <td class="numberText" nowrap><%if(i == 0) { out.print(changeFormat(result[i][j])); } else {%>
          <%
            if ("Y".equals(status[i-1][j-1])) {
          %>
            <input type=hidden name="<%=("ctrl_amount"+i+(j-3))%>" value="<%=changeFormat(result[i][j],"#0.00")%>"/>
          <%
					  }
				  %>
          <input:text name='<%=("ctrl_amount"+i+(j-3))%>' unabled='<%="Y".equals(status[i-1][j-1])?"true":"false"%>' dVal='<%=changeFormat(result[i][j],"#0.00")%>' type="number" numType="float"  cssclass="textInputE" maxlength="10">
          </input:text><%}%>
        </td>
        <%
					}
				%>
      </tr>
			<%
        } 
      %>
      </html:table>
    </td>
  </tr>
  <!-- 操作 -->
  </html:table>
  <%
   }
  %>
<input type=hidden name="subFunction"/>
<input type=hidden name="select_year" value="<%= request.getParameter("year")%>"/>
</form>
</html:html>
