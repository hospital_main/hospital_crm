<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/dept/deptProject.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
  String year_month =request.getParameter("year_month");
  String dept_clin_code =request.getParameter("dept_clin_code");
  String bonus_code =request.getParameter("bonus_code");
%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function return1() {
    window.history.back();
  }
</script>
<html:html clazz="main">
<form name="template" method="post" action="distBenefitDept.jspviewhigh">
	<!-- 信息提示栏 -->
	<html:message/>

	<!-- 标题栏 -->
	<html:title clazz='module'>项目奖金明细表</html:title>

	<!-- 简单信息 -->
  <table width='100%' border='0'>
    <tr>
      <td class='tableTitle' nowrap='nowrap'>
        <%
          if(year_month!=null){
            out.print(year_month.substring(0,4)+"年");
            out.print(year_month.substring(4,6)+"月");
          }
          out.print(CommonData.getDeptName(dept_clin_code)+" "); 
          out.print(CommonData.getWorkItemName(bonus_code));
        %>
        奖金项目明细表
      </td>
   </tr>
  </table>
  <!-- 复杂信息 -->
  <html:table clazz="complex">
    <!-- 操作 -->
    <tr>
      <td>
        <table width='100%'>
          <tr>
            <td  nowrap='nowrap'>
            <button class="pageBtn" onclick="return return1();">返回</button> 
            <!-- <img src="images/return.gif" class="mouse" onclick="return return1();"/>-->
           </td>
          </tr>
        </table>
      </td>
    </tr>
    <!-- 结果集 -->
    <tr>
    	<td>
        <html:table clazz="result">
          <tr>
            <td nowrap class="resultLabel">收费类别编码</td>
            <td nowrap class="resultLabel">收费类别名称</td>
            <td nowrap class="resultLabel">收入项目</td>
            <td nowrap class="resultLabel">收入金额</td>
            <td nowrap class="resultLabel">提取比例</td>
            <td nowrap class="resultLabel">考核分数</td>
            <td nowrap class="resultLabel">应发奖金</td>
          </tr>
          <%
            String[][] result = (String[][])request.getAttribute("result");
            if (result!=null) {
              for (int i=0; i<result.length; i++) {
                 for(int j=0;j<result[i].length;j++){
                  if(result[i][j]==null||result[i][j].equals(""))
                    result[i][j]="&nbsp;";
                 }
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
          %>
          <tr class="<%=rowColor%>">
            <td class="normalText" nowrap><%=result[i][0]%></td>
            <td class="normalText" nowrap><%=result[i][1]%></td>
            <td class="normalText" nowrap><%=result[i][2]%></td>
            <td class="numberText" nowrap><%=changeFormat(result[i][3])%></td>
            <td class="numberText" nowrap><%=changeFormat(result[i][4],"##0.00%")%></td>
            <td class="numberText" nowrap><%=changeFormat(result[i][5],"##0.00%")%></td>
            <td class="numberText" nowrap><%=changeFormat(result[i][6])%></td>
          </tr>
          <%
                }
              }
          %>
        </html:table>
      </td>
    </tr>
  </html:table>
  <input type="hidden" name="subFunction">
</form>
</html:html>



