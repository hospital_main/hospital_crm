<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/dept/otherBonusFinish.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>

<%
  String year_month = (String)request.getParameter("year_month");
  String bonus_code = (String)request.getParameter("bonus_code");
  String title = "设置表";
  if (year_month!=null && year_month.length()==6) {
    title = year_month.substring(0, 4)+"年"+year_month.substring(4)+"月"+CommonData.getBonusName(bonus_code)+title;
  }
  String[][] result = (String[][])request.getAttribute("result");
%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function find() {
    if (template.year_month.value == "") {
      alert("请选择核算月！");
      return false
    }
    if (template.bonus_code.value=='') {
      alert("请选择其他奖金项目！");
      return false
    }
    template.subFunction.value = "goOtherBonusMain";
    show_wait();
    template.submit();
    return true;
  }
</script>

<html:html clazz="main">
<form name="template" method="post" action="distOtherBonus.jspviewhigh">
	<html:message/>

	<!-- 标题栏 -->
	<html:title clazz='module'>其他奖金分配</html:title>

	<!-- 简单信息 -->
	<html:table clazz="simple">
		<tr>
			<td nowrap width="40"></td>
			<td nowrap class="signText" width="82">核算月:</td>
			<td nowrap class="normalText" >
				<%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
			</td>
			<td nowrap class="signText"  width="100">其他奖金项目:</td>
			<td nowrap class="normalText" >
			  <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getOtherBonusItem(), "bonus_code", request.getParameter("bonus_code"), true, false)%>
			</td>
	 	  <td>
	 	  <button class="pageBtn" name="" onclick="return find();" >查询</button>
	 	  <!--<img src="images/find.gif" class="mouse" onclick="return find();" />--></td>
	 	<%if (result!=null) {%>
	 	  <td> <button class="pageBtn"  onclick="return preparedPrint();" >打印</button></td>
	 	<%}%>
		</tr>
  </html:table>


  <br>
  <html:title clazz='table'><%=title%></html:title>

  <html:table clazz="complex">
<%if (result==null || result.length<2) {%>
    <tr>
      <td>
    		<html:table clazz="result">
				  <tr id='label'>
				    <td nowrap class="resultLabel">分配科室名称</td>
				    <td nowrap class="resultLabel">人数</td>
				    <td nowrap class="resultLabel">实发金额</td>
				    <td nowrap class="resultLabel">人均金额</td>
				  </tr>
				</html:table>
		  </td>
		</tr>
<%} else {%>
    <tr>
      <td>
				<html:table clazz="result">
				  <tr id='label'>
				    <td nowrap class="resultLabel">分配科室名称</td>
				    <td nowrap class="resultLabel">人数</td>
				    <td nowrap class="resultLabel">实发金额</td>
				    <td nowrap class="resultLabel">人均金额</td>
				  </tr>
        <%for (int i=0; i<result.length; i++) {
            String rowColor = "rowGray";
            if (i/2*2==i)
              rowColor = "rowWhite";%>
          <tr class="<%=rowColor%>">
            <td class='normalText' nowrap><%=result[i][0]%></td>
            <td class='numberText' nowrap><%=result[i][1]%></td>
            <td class='numberText' nowrap><%=result[i][2]%></td>
            <td class='numberText' nowrap><%=result[i][3]%></td>
          </tr>
        <%}%>
        </html:table>
      </td>
    </tr>
<%}%>
  </html:table>
  <input type="hidden" name="subFunction">
</form>
</html:html>

