<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/dept/otherBonusMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>

<%
  String year_month = (String)request.getParameter("year_month");
  String bonus_code = (String)request.getParameter("bonus_code");
  String title = "设置表";
  if (year_month!=null && year_month.length()==6) {
    title = year_month.substring(0, 4)+"年"+year_month.substring(4)+"月"+CommonData.getBonusName(bonus_code)+title;
  }
  String[][] result = (String[][])request.getAttribute("result");

  String control_id = (String)request.getAttribute("control_id");
  String control_warning = (String)request.getAttribute("control_warning");
%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function find() {
    if (template.year_month.value == "") {
      alert("请选择核算月！");
      return false
    }
    if (template.bonus_code.value=='') {
      alert("请选择其他奖金项目！");
      return false
    }
    template.subFunction.value = "goOtherBonusMain";
    show_wait();
    template.submit();
    return true;
  }

  function init() {
    if (template.year_month.value != '<%=year_month%>') {
      alert("核算月已经改变，请重新查询！");
      return false
    }
    if (template.bonus_code.value != '<%=bonus_code%>') {
      alert("其他奖金项目已经改变，请重新查询！");
      return false
    }
    if (!confirm('执行此操作会丢失本月数据，是否继续?')) {
      return false;
    }
    template.subFunction.value = "goOtherBonusInit";
    show_wait();
    template.submit();
    return true;
  }

  function save() {
    if (template.year_month.value != '<%=year_month%>') {
      alert("核算月已经改变，请重新查询！");
      return false
    }
    if (template.bonus_code.value != '<%=bonus_code%>') {
      alert("其他奖金项目已经改变，请重新查询！");
      return false
    }
    template.subFunction.value = "goOtherBonusSave";
    show_wait();
    document.getElementById('inputTotal').click();
    template.submit();
    return true;
  }

  function finish() {
    if (template.year_month.value != '<%=year_month%>') {
      alert("核算月已经改变，请重新查询！");
      return false
    }
    if (template.bonus_code.value != '<%=bonus_code%>') {
      alert("其他奖金项目已经改变，请重新查询！");
      return false
    }
    template.subFunction.value = "goOtherBonusFinish";
    show_wait();
    document.getElementById('inputTotal').click();
    template.submit();
    return true;
  }

  function inputControl() {
    var length = eval('label').parentNode.childNodes.length - 1;
    if (length > 1) {
      document.getElementById('total0').disabled=true
      document.getElementById('average0').disabled=true
    }

    if (template.input_method[0].checked) {//实发
      for (var i=1; i<length; i++) {
        document.getElementById('total'+i).disabled=false
        document.getElementById('average'+i).disabled=true
      }
    }
    if (template.input_method[1].checked) {//人均
      for (var i=1; i<length; i++) {
        document.getElementById('total'+i).disabled=true
        document.getElementById('average'+i).disabled=false
      }
    }
  }

  function adjustData(comp) {
    if (!checkMoney(comp)) {
      return false;
    }
    var length = eval('label').parentNode.childNodes.length - 1;

    var total0 = 0;
    if (template.input_method[0].checked) {//实发
      for (var i=1; i<length; i++) {
        document.getElementById('average'+i).value=roundOff(parseFloat(document.getElementById('total'+i).value)/parseFloat(document.getElementById('person'+i).innerText), 2)
        total0=total0+parseFloat(document.getElementById('total'+i).value)
      }
    }
    if (template.input_method[1].checked) {//人均
      for (var i=1; i<length; i++) {
        document.getElementById('total'+i).value=roundOff(parseFloat(document.getElementById('average'+i).value)*parseFloat(document.getElementById('person'+i).innerText), 2)
        total0=total0+parseFloat(document.getElementById('total'+i).value)
      }
    }
    document.getElementById('total0').value = roundOff(total0, 2)
    document.getElementById('average0').value = roundOff(total0/parseFloat(document.getElementById('person0').innerText), 2)
  }

</script>

<html:html clazz="main">
<form name="template" method="post" action="distOtherBonus.jspviewhigh">
	<html:message/>

	<!-- 标题栏 -->
	<html:title clazz='module'>其他奖金分配</html:title>

	<!-- 简单信息 -->
	<html:table clazz="simple">
		<tr>
			<td nowrap width="40"></td>
			<td nowrap class="signText" width="82">核算月:</td>
			<td nowrap class="normalText" >
				<%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
			</td>
			<td nowrap class="signText"  width="100">其他奖金项目:</td>
			<td nowrap class="normalText" >
			  <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getOtherBonusItem(), "bonus_code", request.getParameter("bonus_code"), true, false)%>
			</td>
	  </tr>
	  <tr>
			<td nowrap ></td>
			<td class="signText" >输入方式:</td>
			<td class="normalText" nowrap>
				<input type="radio" name='input_method'  value='1' checked onclick='inputControl()' id='inputTotal'/>实发金额
				<input type="radio" name='input_method'  value='2' onclick='inputControl()'/>人均金额
			</td>
			<td>
			<button class="pageBtn" name=""  onclick=" return find();" >查询</button>
			<!--<img src="images/find.gif" class="mouse" onclick=" return find();" />--></td>
		</tr>
  </html:table>


  <br>
  <html:title clazz='table'><%=title%></html:title>

  <html:table clazz="complex">
<%if (result==null || result.length<2) {%>
    <tr>
      <td>
    <%if (year_month != null) {%>
        <button src='images/initial.gif' class='pageBtn' onclick='return init()'>初始化</button>
    <%}%>
      </td>
    </tr>
    <tr>
      <td>
    		<html:table clazz="result">
				  <tr id='label'>
				    <td nowrap class="resultLabel">分配科室名称</td>
				    <td nowrap class="resultLabel">人数</td>
				    <td nowrap class="resultLabel">实发金额</td>
				    <td nowrap class="resultLabel">人均金额</td>
				  </tr>
				</html:table>
		  </td>
		</tr>
<%} else {%>
    <tr>
      <td>
				<button src='images/initial.gif' class='pageBtn' onclick='return init()'>初始化</button>
	      <button src='images/reset.gif' class='pageBtn' onclick='return reset()'>重置</button>
	      <button src='images/save.gif' class='pageBtn' onclick='return save()'>保存</button>
				<button src='images/finish.gif' class='pageBtn' onclick='return finish()'>完成</button>
      </td>
    </tr>
    <tr>
      <td>
				<html:table clazz="result">
				  <tr id='label'>
				    <td nowrap class="resultLabel">分配科室名称</td>
				    <td nowrap class="resultLabel">人数</td>
				    <td nowrap class="resultLabel">实发金额</td>
				    <td nowrap class="resultLabel">人均金额</td>
				  </tr>
        <%for (int i=0; i<result.length; i++) {
            String rowColor = "rowGray";
            if (i/2*2==i)
              rowColor = "rowWhite";%>
          <tr class="<%=rowColor%>">
            <td class='normalText' nowrap>
              <input type=hidden name='dept_clin_code' value='<%=result[i][0]%>' <%=i==0?"disabled":""%>>
              <%=result[i][1]%>
            </td>
            <td class='numberText' id='person<%=i%>' nowrap><%=result[i][2]%></td>
            <td class='numberText' nowrap>
              <input id='total<%=i%>' name="total_amount" onblur='return adjustData(this)' type="text" value="<%=result[i][3]%>" class='textInputE'>
            </td>
            <td class='numberText' nowrap>
              <input id='average<%=i%>' onblur='return adjustData(this)' type="text" value="<%=result[i][4]%>" class='textInputE'>
            </td>
          </tr>
        <%}%>
        </html:table>
      </td>
    </tr>
<%}%>
  </html:table>
  <input type="hidden" name="subFunction">
  <input type="hidden" name="control_id" value="0">
</form>
</html:html>


<script language='javascript'>
inputControl();
<%
  if("1".equals(control_id)) {
%>
  if (confirm('<%= control_warning%>')) {
    template.control_id.value = 1;
	  template.subFunction.value = "goOtherBonusFinish";
	  show_wait();
	  template.submit();
  }
<%
  }
%>

</script>