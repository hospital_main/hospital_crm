<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/dept/deptScore.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<%
	   String year_month = request.getParameter("year_month");
	   String dept_clin_code = request.getParameter("dept_clin_code");
	   String dept_status = (String)request.getAttribute("dept_status");
     String module = (String)request.getAttribute("module");
     request.setAttribute("module",module);
%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function return1() {
    template.subFunction.value = "go<%= module%>DeptMain";
    show_wait();
    template.submit();
    return true;
  }
  
  function save() {
    template.subFunction.value = "go<%= module%>DeptScore";
    show_wait();
    template.submit();
    return true;
  }
  
  function check(o){
    var c = o.value;
    if(!checkMoney(o)){
      o.select();
      return false;
    }
    if(c<0){
      alert("元素值不应小于0!");
      o.select();
      return false;
    }
    var amount_sum = 0;
    var table=document.getElementById('table');
		var tbody=table.firstChild; 
		for(var i=2;i<tbody.childNodes.length;i++){
		  input=tbody.childNodes[i].childNodes[3].firstChild;
		  check_power=tbody.childNodes[i].childNodes[2].firstChild;
		  if(input.value != '') 
		  {
		    amount_sum = parseFloat(amount_sum) + parseFloat(input.value)*parseFloat(check_power.value)/100;
		  }
		}  
		eval('sum').innerText = roundOff(amount_sum,2);
		template.sum_amount.value = roundOff(parseFloat(amount_sum)/100,4);
    return true;
  }  
  
  function init()
  {
    var amount_sum = 0;
    var table=document.getElementById('table');
		var tbody=table.firstChild; 
		for(var i=2;i<tbody.childNodes.length;i++){
		  input=tbody.childNodes[i].childNodes[3].firstChild;
		  check_power=tbody.childNodes[i].childNodes[2].firstChild;
		  if(input.value != '') 
		  {
		    amount_sum = parseFloat(amount_sum) + parseFloat(input.value)*parseFloat(check_power.value)/100;
		  }
		}   
		eval('sum').innerText = roundOff(amount_sum,2);
		template.sum_amount.value = roundOff(parseFloat(amount_sum)/100,4);
  }  
</script>
<html:html clazz="main">
<form name="template" method="post" action="dist<%= module%>Dept.jspviewhigh">
	<!-- 信息提示栏 -->
	<html:message/>

	<!-- 标题栏 -->
	<html:title clazz='module'>科室考核分数</html:title>

	<!-- 简单信息 -->
	<html:table clazz="simple">
    <tr>
      <td class='tableTitle' nowrap='nowrap'>
<%
		   if(year_month!=null){
		      out.print(year_month.substring(0,4)+"年");
		      out.print(year_month.substring(4,6)+"月");
		   }
		   out.print(CommonData.getDeptName(dept_clin_code));
%>
       考核分数表
      </td>
    </tr>
  </html:table>
	<!-- 复杂信息 -->
	<html:table clazz="complex">
	  <!-- 操作 -->
    <tr>
      <td>
        <table width='100%'>
          <tr>
            <td  nowrap='nowrap'>
<%
     if(!"Y".equals(dept_status)){
%>
              <button src="images/save.gif" class="pageBtn" onclick="return save();">保存</button>
              <button src="images/reset.gif" class="pageBtn" onclick="return reset();">重置</button>
              <button class="pageBtn" onclick="return return1();">返回</button> 
              <!--<img src="images/return.gif" class="mouse" onclick="return return1();"/>-->
 <%
     } else {
 %>     
              <button class="pageBtn" onclick="history.back()">返回</button> 
             <!-- <img src="images/return.gif" class="mouse" onclick="history.back()"/>-->
 <%
     }
 %>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <!-- 结果集 -->
    <tr>
    	<td>
        <table width='100%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' id="table" >
          <tr>
            <td class="resultLabel">指标编码</td>
            <td class="resultLabel">指标名称</td>
            <td class="resultLabel">权重(%)</td>
            <td class="resultLabel">考核成绩(%)</td>
           </tr>

          <tr class="rowGray">
            <td class="normalText">总计</td>
            <td class="normalText">&nbsp;</td>
            <td class="numberText">&nbsp;</td>
            <td class="numberText" id="sum"></td>
          </tr>
<%
    String[][] result = (String[][])request.getAttribute("result");
    if (result!=null) {
      for (int i=0; i<result.length; i++) {
         for(int j=0;j<result[i].length;j++){
          if(result[i][j]==null||result[i][j].equals(""))
            result[i][j]="&nbsp;";
         }
        String rowColor = "rowGray";
        if (i/2*2==i) rowColor = "rowWhite";
 %>
		      <tr class="<%=rowColor%>">
		        <td class="normalText" nowrap>
		        <%=result[i][0]%>
		        <input type='hidden' name="check_id" value="<%=result[i][0]%>"/>
		        </td>
		        <td class="normalText" width="30%">
		        <%=result[i][1]%>
		        </td>
		        <td class="numberText" width="30%" nowrap>
  		        <input type=hidden name='check_power' value="<%=changeFormat(result[i][2])%>"/>
  		        <%=changeFormat(result[i][2])%>
		        </td>
		      <%
		        if(!"Y".equals(dept_status)){
		      %>
		        <td class="normalText" width="30%" nowrap>
		          <input type=text id='check_mark' name='check_mark' class='textInputE' maxlength='14' value="<%=changeFormat(result[i][3],"#0.00")%>" onKeyPress='return isFloat(event)' onblur='return check(this)'/>
		        </td>
		      <%
		        }
		        else{
		      %>
		        <td class="numberText" width="30%" nowrap>
		          <input type=hidden name='check_mark' value="<%=changeFormat(result[i][3])%>"/><%= changeFormat(result[i][3])%>
		        </td>
		       <%
		        }
		       %>
		      </tr>
<%
      }
    }
%>
        </table>
      </div>
    </td>
  </tr>
</html:table>
<input type='hidden' name="sum_amount" value="0"/>
<input type="hidden" name="subFunction">
<input type='hidden' name="length" value="0" id="hidden"/>
<input type='hidden' name="year_month" value="<%= request.getParameter("year_month")%>"/>
<input type='hidden' name="dept_clin_code" value="<%= request.getParameter("dept_clin_code")%>"/>
<input type='hidden' name="dept_status" value="<%= request.getParameter("dept_status")%>"/>
</form>
<Script Language="JavaScript">
  init();
</Script>
</html:html>
