<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/collect/deptAverageContrastMain.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
function find() {
   if(template.year_month_from.value=='')
     {alert("请选择年月")
      return false
     }
    if(template.year_month_to.value=='')
     {alert("请选择年月")
      return false
     }
    if(template.year_month_from.value>template.year_month_to.value)
    {  alert("起始年月不能大于终止年月")
        return false
     }
    template.subFunction.value='goDeptAverageContrastMain';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="distDeptAverageContrast.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>人均金额分析</html:title>
  <!-- 查询信息 -->
    <%
      DecimalFormat nf = new DecimalFormat("#,##0.00");
      BaseRO  ro= (BaseRO)request.getAttribute("ro");
      String[] allYearMonth=(String[])request.getAttribute("allYearMonth");
      String[][] result;
      if(ro!=null)
       result =ro.getTableResult();
      else   result=null;
     TableMarge oper = new TableMarge(ro, "return find()");
     String[][] type = {{"1","经营科室"},{"2","管理科室"},{"3","服务科室"}};
    %>
  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText">时间范围：</td>
      <td nowrap>
      <%=new com.viewhigh.cbcs.base.mvc.view.BiMonthComponent("year_month_from", request.getParameter("year_month_from"),"year_month_to",request.getParameter("year_month_to"))%>
     </td>
     <td nowrap class="signText" width="72">科室类型：</td>
     <td nowrap class="normalText" width="150">
        <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(type, "dept_type", request.getParameter("dept_type"), false, true)%>
     </td>
      <td>
      <button class="pageBtn" name="" onclick="return find();" >查询</button>
      <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
</tr>

   </html:table>
<br>
<html:title clazz='table'><%if(request.getParameter("dept_type")!=null && request.getParameter("dept_type").equals("1")) out.print("经营");else if(request.getParameter("dept_type")!=null && request.getParameter("dept_type").equals("2")) out.print("管理");else if(request.getParameter("dept_type")!=null && request.getParameter("dept_type").equals("3")) out.print("服务");%>科室奖金人均金额分析</html:title>
   <html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>
      	<html:table clazz="result">
		        <html:tr clazz='label'>
     <% if(allYearMonth!=null&&result!=null){%>
          <td >分配科室名称</td>

          <% for(int i=0;i<allYearMonth.length;i++)
               {
          %>
          <td ><%=allYearMonth[i]%></td>

        <%}}%>
        </html:tr>

        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText"><%=result[ i ][ 0]%></td>
         <%for(int j=1;j<result[0].length;j++)
            {
          %>
           <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[ i ][ j]))%></td>
          <%}%>
        </tr>

        <%
              }
            }
        %>
        </html:table>
    </td>
  </tr>

 </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>



