<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/collect/empTypeContrastMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
									com.viewhigh.cbcs.base.util.ExtendTool,
									com.viewhigh.cbcs.base.mvc.view.TableMarge,
									com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
									com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
	function find() {
		if(template.year_month.value==''){
			alert("请选择年月")
			return false
		}
    if(template.kind.value==''){
			alert("请选择对比类别")
			return false
		}
		template.subFunction.value='goEmpTypeContrastMain';
		template.submit();
		return true;
	}
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="distEmpTypeContrast.jspviewhigh">
		<!-- 信息提示栏 -->
		<html:message/>

		<!-- 标题栏 -->
		<html:title clazz='module'>人员类别分析
</html:title>
		<!-- 查询信息 -->

<%//DecimalFormat nf = new DecimalFormat("#,##0.00%");
	//DecimalFormat nf2 = new DecimalFormat("#,##0.00");
	String[][] result = (String[][])request.getAttribute("result");
  String[] title =(String[])request.getAttribute("title");
%>
  <html:table clazz="simple">

			<tr>
				<td nowrap class="signText" >核算期间： </td>
				<td nowrap class="normalText" colspan='3'>
					<!-- 年月组件 -->
					<%= new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
				</td>
				<td>对比类别：</td>
				<td>
					<%String[][] kind = {{"1","职称对比"},{"2","职务对比"},{"3","类别对比"},{"4","学历对比"}};%>
					<%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(kind, "kind", request.getParameter("kind"), false, false)%>
				</td>
				<td>
				<button class="pageBtn" name=""  onclick="return find();"  >查询</button>
				<!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
			</tr>

		</html:table>
		<br>
<%if(title!=null){%>
		<html:title clazz='table'>
			<%=request.getParameter("year_month").substring(0,4)%>年<%=request.getParameter("year_month").substring(4,6)%>月人员平均奖金按
      <%
       String kindStr= request.getParameter("kind")==null?"1":request.getParameter("kind");  
       int kindInt =Integer.parseInt(kindStr);
       switch(kindInt){
          case 1:out.print("职称对比");break;
          case 2:out.print("职务对比");break;
          case 3:out.print("类别对比");break;
          case 4:out.print("学历对比");break;
          default:out.print("职称对比");
       }
      %>表
		</html:title>
		<html:table clazz="complex">
			<!-- 操作 -->
			
			<!-- 结果集 -->
			<tr>
				<td>
				<html:table clazz="result">
				<html:tr clazz='label'>
			<%//显示列头
       if(title!=null){
         for(int i=0;i<title.length;i++){
      %>
         <td nowrap><%=title[i]%></td>
      <%   
         }
       }   
      %>
				</html:tr>

<%	if(result != null){
			for (int i = 0; i < result.length; i++ ){
     		String rowColor = "rowGray";
     		if (i/2*2==i) rowColor = "rowWhite";
%>
			<tr CLASS="<%=rowColor%>">
<%			for(int j=0;j<result[i].length;j++){
           if(j==0){
           %>
           <td nowrap class="normalText">
              <%if(result[i][j].equals(""))
                  out.print("平均值");
                else 
                  out.print(result[i][j]);
              %></td>
           <%
           }else{
           %>
           <td nowrap class="numberText"><%=result[i][j]%></td>
           <%
           }
      	}%>
			</tr>
<%		}
		}%>
				</html:table>
			</td>
			</tr>
		</html:table>
<%}%>
		<input type=hidden name="subFunction"/>
	</form>
</html:html>