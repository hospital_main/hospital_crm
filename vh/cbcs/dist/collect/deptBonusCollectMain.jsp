<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/collect/deptBonusCollectMain.jsp,v 1.1 2012/03/12 01:58:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:00 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
  String[][] result = (String[][])request.getAttribute("result");
  String[][] title = (String[][])request.getAttribute("title");
  String year_month = request.getParameter("year_month")==null?null:(String)request.getParameter("year_month");
%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function find() {
    if(template.year_month.value=='')
    {
      alert("请选择年月");
      return false;
    }
    if(template.bonus_kind.value=='')
    {
      alert("请选择奖金项目");
      return false;
    }
    template.subFunction.value='goDeptBonusCollectMain';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="distDeptBonusCollect.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>科室奖金汇总</html:title>

  <!-- 简单信息 -->
  <html:table clazz="simple">
    <tr>
      <td width="40"> </td>
      <td nowrap class="signText" width="72">核算月：</td>
      <td nowrap>
        <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
      </td>
      <td nowrap class="signText" width="72">奖金项目：</td>
      <td>
        <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getBaseOtherBonusItem(), "bonus_kind", request.getParameter("bonus_kind"), true, true)%>
      </td>
      <td>
      <button class="pageBtn" name="" onclick="return find();" >查询</button>
      <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
      <td width="40"> </td>
    </tr>

  </html:table>
  <br>
  <html:title clazz='table'>
    <%
      if(year_month!=null){
        out.print(year_month.substring(0,4)+"年");
        out.print(year_month.substring(4,6)+"月");
      }
      out.println(CommonData.getWorkItemName(request.getParameter("bonus_kind")));
    %>
    科室奖金汇总表
  </html:title>
	<!-- 复杂信息 -->
	<html:table clazz="complex">
		<!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
   			  <tr>
				  <%
  			    if(title != null) {
  			    	for (int i=0; i<title[0].length; i++) {
				  %>
				    <td class="resultLabel" nowrap><%= title[0][i]%></td>
				  <%
				      }
				    }
				  %>
				  </tr>
				  <%
				    if (result!=null) {
				      int rowLength = result.length;
				      int colLength = result[0].length;
				    	for (int i=0; i<result.length; i++) {
				        String rowColor = "rowGray";
        				if (i/2*2==i)
                   rowColor = "rowWhite";
				  %>
				  <tr class="<%=rowColor%>">
				    <td class="normalText" nowrap><%= result[i][0]%></td>
				    <td class="numberText" nowrap><%= result[i][1]%></td>
				  <%
				        for(int k=2;k<result[i].length;k++){
				  %>
            <td class="numberText" nowrap><%= result[i][k]%></td>
          <%
			  		    }
				  %>
				  </tr>
				  <%
			  		  }
			      }
				  %>
				</html:table>
    	</td>
    </tr>
		<!-- 操作 -->
	</html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>


