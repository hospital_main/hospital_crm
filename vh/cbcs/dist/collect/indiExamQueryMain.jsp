<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/collect/indiExamQueryMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
									com.viewhigh.cbcs.base.util.ExtendTool,
									com.viewhigh.cbcs.base.mvc.view.TableMarge,
									com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
									com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
	function find() {
		if(template.year_month.value==''){
			alert("请选择年月")
			return false
		}
		template.subFunction.value='findAll';
		template.submit();
		return true;
	}
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="distIndiExamQuery.jspviewhigh">
		<!-- 信息提示栏 -->
		<html:message/>

		<!-- 标题栏 -->
		<html:title clazz='module'>个人考核成绩</html:title>
		<!-- 查询信息 -->
		
<%DecimalFormat nf = new DecimalFormat("#,##0.00%");
	DecimalFormat nf2 = new DecimalFormat("#,##0.00");
	BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	TableMarge oper = new TableMarge(ro, "return find()");
	String[][] result;
	if(ro!=null){
		result =ro.getTableResult();
	}	else   
		result=null;%>
		<html:table clazz="simple">

			<tr>
				<td nowrap class="signText" >核算月： </td>
				<td nowrap class="normalText" colspan='3'>
					<!-- 年月组件 -->
					<%= new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%>
				</td>
				<td class="signText" nowrap="nowrap">分配科室：</td>
				<td class="normalText" nowrap="nowrap">
					<%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getBigDept(), "dept_clin_code", request.getParameter("dept_clin_code"), true, false)%>
				</td>
				<td>
				<button class="pageBtn" name="" onclick="return find();">查询</button>
				<!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
			</tr>

		</html:table>
		<br>
		<html:title clazz='table'>
			<%=request.getParameter("year_month")==null?"":request.getParameter("year_month").substring(0,4)+"年"+request.getParameter("year_month").substring(4,6)+"月"%>考核成绩表
		</html:title>
		<html:table clazz="complex">
			<!-- 操作 -->
			<tr><td><%=oper%></td></tr>
			<!-- 结果集 -->
			<tr>
				<td>
				<html:table clazz="result">
				<html:tr clazz='label'>
					<td >姓名</td>
					<td >考核项目</td>  
					<td >权重</td>
					<td >考核分</td>
				</html:tr>

<%	if(result != null){
			for (int i = 0; i < result.length; i++ ){
				String rowColor = "";
				if(!result[i][0].equals("")){
					rowColor = "rowGray";
				}else
					rowColor = "rowWhite";%>

			<tr CLASS="<%=rowColor%>">
				<td nowrap class="normalText"><%=result[ i ][ 0]%></td>
				<td nowrap class="normalText"><%=result[ i ][ 1]%></td>
<%			for(int j=2;j<result[0].length;j++){%>
				<td nowrap class="numberText">
<%				if(j==2 && result[ i ][ j].equals("0") && result[ i ][ 1].equals("")){
						;
					}else{%>
<%					if(j==2){%>
							<%=nf.format(Double.parseDouble(result[ i ][ j]))%>
<%					}else{%>
							<%=nf2.format(Double.parseDouble(result[ i ][ j]))%>
<%					}%>
<%				}%>				
				</td>

<%			}%>
			</tr>

<%		}
		}%>
				</html:table>
			</td>
			</tr>
		</html:table>
		<input type=hidden name="subFunction"/>
	</form>
</html:html>