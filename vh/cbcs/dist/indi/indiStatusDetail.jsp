<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/indi/indiStatusDetail.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.*,  com.viewhigh.cbcs.base.mvc.view.*,com.viewhigh.cbcs.cbcs.dist.base.*" %>
<%@ page import="java.text.*,com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
  String[][] totbonus =(String[][])request.getAttribute("totbonus");
  String[] title = (String[])request.getAttribute("title");
  String[][] result = (String[][])request.getAttribute("result");
%>

<Script language="javascript">

  function find() {
    if (template.yearMonth.value == '') {
      alert('请选择核算年月!');
      return false;
    }
   //template.subFunction.value="prepareBonusAllot&yearMonth=" +
   //     template.yearMonth.value + "&deptCode=" + template.deptCode.value,
   template.subFunction.value="goIndiBonusMain";
   template.submit();

    return true;
  }
  function print(){
   template.subFunction.value="prePrint";
   template.submit();
   return true;
  }
</Script>
<!--弹出处的JS-->




<html:html clazz="main">
<form name="template" method="post" action="distIndiStatus.jspviewhigh">

  <!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>奖金分配</html:title>
	<br>
  <html:table clazz="simple">
  <!-- 简单信息 -->

	<tr>
     <td  nowrap width="40">
     <td>  <button class="pageBtn" onclick="preparedPrint();">打印</button></td>
     <td><button class="pageBtn" onclick="javascript:window.history.back()"  >返回</button></td>
  </tr>
  <tr>
     <td  nowrap width="40">
     <td class='tableTitle' nowrap='nowrap'>
          <%
           String year_month =request.getParameter("year_month");
           String dept_name =request.getParameter("dept_name");
           String bonus_code =request.getParameter("bonus_code");
           if(year_month!=null){
              out.print(year_month.substring(0,4)+"年");
              if(year_month.length()==5)
                 out.print(year_month.substring(4,5)+"月");
              else
                 out.print(year_month.substring(4,6)+"月");
              }
           out.print(dept_name);
				String bonus_name=(bonus_code==null ? "":(bonus_code.equals("1")? "效益和项目奖金":(CommonData.getBonusName(bonus_code))));
				out.print(bonus_name);
         %>分配表
     </td>
   </tr>
  </html:table>


  <br>
  <html:table clazz="result">      
  	 <html:tr clazz='label'>
        <td nowrap class="resultLabel"   rowspan="2">姓名</td>
      
        <%
        if(totbonus!=null)
          for (int i = 0; i < title.length; i++) {
        %>
        <td nowrap class="resultLabel"   colspan="2">
          <%=totbonus[i][2]%>
          <input type="hidden" name="bonusname<%=i%>" value="<%=totbonus[i][0]%>"><!--奖金--> 
        </td>
        <%
          }
        %>
        <td nowrap class="resultLabel"   rowspan="2">
          考核成绩
        </td>
        <td nowrap class="resultLabel"   rowspan="2">
          应发金额
        </td>
        <td nowrap class="resultLabel"   rowspan="2">
          加减项2
        </td>
        <td nowrap class="resultLabel"   rowspan="2">
          实发金额
        </td>
      </html:tr>
      
      <html:tr clazz='label'>
        <%
        if(title!=null)
          for (int i = 0; i < title.length; i++) {
        %>
        <td nowrap   class="resultLabel">
          工作量
        </td>
        <td nowrap   class="resultLabel">
          金额
        </td>
        <%
          }
        %>
      </html:tr>
      <tr >
        <td nowrap   >总计</td>
      
        <%
        if(totbonus!=null)
          for (int i = 0; i < totbonus.length; i++) {
        %>
        <td nowrap></td>
        <td nowrap   class=numberText>
          <%=changeFormat(totbonus[i][1])%>
        </td>
        <%
          }
        %>
        <td nowrap   >
          
        </td>
        <td nowrap  >
        </td>
        <td nowrap  >
          
        </td>
        <td nowrap   >
        </td>
      </tr>

      <%
        
        if(result!=null){
        for (int i = 0; i < result.length; i++) {
      %>
      <input type="checkbox" name="parameters"
        value="<%=result[i][0]%>"
        style="display:none">
      <tr>
        <td nowrap>
          <%=result[i][0]%><!--姓名-->
        </td>
      <% //处理动态的列
        int n = title.length;  
        n=n*2;
        for(int j=1;j<=n;j++){
        	if(j%2!=1){
          %>
        <td nowrap  class=numberText>
          <%=changeFormat(result[i][j])%>
        </td>
       <%
        }else{%>
         <td nowrap  class=numberText>
          <%=result[i][j]%>
        </td>
        <%}
        }
       %>
       <td nowrap  class=numberText>
        <a href="distIndiStatus.jspviewhigh?subFunction=goIndiStatusSubMain&emp_id=<%=result[i][result[0].length-6]%>&employee_name=<%= result[i][0]%>&year_month=<%= request.getParameter("year_month")%>&dept_name=<%= result[i][1]%>&dept_clin_code=<%=request.getParameter("dept_clin_code")%>&bonus_code=<%=request.getParameter("bonus_code")%>"><%= changeFormat(result[i][result[0].length-4])%>%</a>
         <!--考核成绩-->
       </td>
       <td nowrap  class=numberText>
          <%=changeFormat(result[i][result[0].length-3])%><!--应发金额-->
       </td>
       <td nowrap  class=numberText>
          <%=changeFormat(result[i][result[0].length-2])%><!--加减项2-->
       </td>
       <td nowrap  class=numberText>
          <%=changeFormat(result[i][result[0].length-1])%><!--实发金额-->
       </td>
      </tr>
      <%}%>

  <%
    }
  %>
      </html:table>
  <input type="hidden" name="subFunction" value="save">
  <input type="hidden" name="yearMonth" value="<%=request.getParameter("year_month")%>">
  <input type="hidden" name="deptCode" value="<%=request.getParameter("deptCode")%>">
  <input type="hidden" name="total">
</form>

</html:html>








