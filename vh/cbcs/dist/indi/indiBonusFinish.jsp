<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/indi/indiBonusFinish.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.*,
  com.viewhigh.cbcs.base.mvc.view.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
  String[][] bonusItems = (String[][])request.getAttribute("bonus_items");
  String[][] result = (String[][])request.getAttribute("table_result");
  String flag = (String)request.getAttribute("isAllot");
  DecimalFormat nf = new DecimalFormat("##0.00");
  DecimalFormat nff = new DecimalFormat("#,##0.00");
  double sum=0;
  double allsum=0;
  boolean isAllot = false;
  if (flag != null && flag.equals("true")) {
    isAllot = true;
  }
%>

<Script language="javascript">
  <%
    if (result != null && bonusItems != null) {
  %>
  function save(isCommit) {

    var total = new Array(<%=bonusItems.length%>);
    var plan = new Array(<%=bonusItems.length%>);

    for (j = 0; j < <%=bonusItems.length%>; j++) {
      total[j] = 0;
      plan[j] = 0.00;
    }



    <%
      for (int i = 0; i < bonusItems.length; i++) {
    %>
    plan[<%=i%>] = <%=bonusItems[i][2]%>;
    <%
      }
    %>

    var sum=0;
    for (var k=0; k<template.elements.length; k++) {
          if (template.elements[k].name=='parameters')
            sum=sum+1;
      }

    for (i = 0; i < sum; i++) {

      for(j = 0; j < <%=bonusItems.length%>; j++) {
        switch(isDouble(document.all[i + 'value' + j],4,0))
        {
          case 0 : alert('工作量必须为数字型'); return false;
          case 1 : alert('工作量长度不能高于4个数字'); return false;
          case 3 : alert('工作量必须为整数');return false;
        }

				if (!checkMark(document.all['checkMark' + i])) return false;

        switch(isDouble(document.all['controlAmount' + i], 12, 4)) {
          case 0 : alert('调控必须为数字型'); return;
          case 1 : alert('调控整数部分不能高于12个字符'); return;
          case 2 : alert('调控没有整数部分'); return;
          case 3 : alert('调控小数部分不能高于4个字符'); return;
        }
      }
    }

if(sum>1)
    for (i = 0; i < template.parameters.length; i++) {
      for(j = 0; j < <%=bonusItems.length%>; j++) {
        if (document.all[i + 'value' + j].value == '') {
          document.all[i + 'value' + j].value = '0';
        }
        template.parameters[i].value =
          template.parameters[i].value +
          '|^|' + document.all[i + 'value' + j].value;
        total[j] += parseInt(document.all[i + 'value' + j].value);
      }
      if (document.all['checkMark' + i].value == '') {
        document.all['checkMark' + i].value = '0';
      }
      if (document.all['controlAmount' + i].value == '') {
        document.all['controlAmount' + i].value = '0';
      }
      template.parameters[i].value =
          template.parameters[i].value +
          '|^|' + document.all['checkMark' + i].value;
      template.parameters[i].value =
          template.parameters[i].value +
          '|^|' + document.all['controlAmount' + i].value;
      template.parameters[i].checked = true;
    }
else
    for (i = 0; i < sum; i++) {
      for(j = 0; j < <%=bonusItems.length%>; j++) {
        if (document.all[i + 'value' + j].value == '') {
          document.all[i + 'value' + j].value = '0';
        }
        template.parameters.value =
          template.parameters.value +
          '|^|' + document.all[i + 'value' + j].value;
        total[j] += parseInt(document.all[i + 'value' + j].value);
      }
      if (document.all['checkMark' + i].value == '') {
        document.all['checkMark' + i].value = '0';
      }
      if (document.all['controlAmount' + i].value == '') {
        document.all['controlAmount' + i].value = '0';
      }
      template.parameters.value =
          template.parameters.value +
          '|^|' + document.all['checkMark' + i].value;
      template.parameters.value =
          template.parameters.value +
          '|^|' + document.all['controlAmount' + i].value;
      template.parameters.checked = true;
    }

    if (isCommit) {
      template.subFunction.value = 'commit';
    }

    /* check */
    var totalBonus = 0.00;
    for(i = 0; i < sum; i++) {
      var individualBonus = 0.00;
      for(j = 0; j < <%=bonusItems.length%>; j++) {
       if(total[j]!=0)
        individualBonus +=
          parseInt(document.all[i + 'value' + j].value) * plan[j] / total[j] * parseFloat(document.all['checkMark' + i].value)/100 ;
      }
      individualBonus += parseFloat(document.all['controlAmount' + i].value);
      totalBonus += individualBonus;
    }

    var planBonus = 0.00;
    for(j = 0; j < <%=bonusItems.length%>; j++) {
      planBonus += plan[j];
    }
    var diff = planBonus - totalBonus;
    if (roundOff(diff,2) != 0) {
      if(confirm('科室总奖金与个人实发奖金之和不等, 其差值为:' + roundOff(diff,2) + ',您确认修改吗?' )) {
        for(j = 0; j < <%=bonusItems.length%>; j++) {
          if (j == 0) {
            template.total.value = total[j];
          } else {
            template.total.value =
              template.total.value + '|^|' + total[j] ;
          }
        }
        show_wait();
        template.submit();
        return true;
      } else {
        return false;
      }
    }else{
      for(j = 0; j < <%=bonusItems.length%>; j++) {
          if (j == 0) {
            template.total.value = total[j];
          } else {
            template.total.value =
              template.total.value + '|^|' + total[j] ;
          }
        }
        show_wait();
        template.submit();
        return true;
      }

  }

  <%
    }
  %>
</Script>

<html:html clazz="child">
<form name="template" method="post" action="distIndiBonus.jspviewhigh">

  <!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>奖金分配&nbsp;<%=request.getAttribute("dept")%><%if(request.getParameter("yearMonth")!=null) out.print(request.getParameter("yearMonth").substring(0,4)+"年"+request.getParameter("yearMonth").substring(4,6)+"月");%></html:title>

  <br>
  <%
    if (result != null && bonusItems != null) {
  %>
    <%
      if (!isAllot) {
    %>
    <table>
      <tr>
        <td><button class="pageBtn" onclick="return save(false);">保存</button> 
        <!--<img src="images/save.gif" class="mouse"  onclick="return save(false);">--></td>
        <td><button src="images/finish.gif" class="pageBtn" onclick="return save(true);" >完成</button>
        <td><button class="pageBtn" onclick="window.close();" >关闭</button> 
        <!--<img src="images/close.gif" class="mouse" class="mouse" onclick="window.close();">--></td>
      </tr>
    </table>
    <%
      }
    %>
    <table width='100%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"??"; font-size:10pt' >
      <html:tr clazz='label'>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">姓名</td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">性别</td>
        <%
        if(bonusItems!=null)
          for (int i = 0; i < bonusItems.length; i++) {
        %>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' colspan="2">
          <%=bonusItems[i][1]%>
        </td>
        <%
          }
        %>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">
          考核成绩
        </td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">
          应发
        </td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">
          调控
        </td>
        <td nowrap class="resultLabel"  style='text-align:center;font-weight:bold' rowspan="2">
          实发金额
        </td>
      </html:tr>

      <html:tr clazz='label'>
        <%
        if(bonusItems!=null)
          for (int i = 0; i < bonusItems.length; i++) {
        %>
        <td nowrap   style='text-align:center;font-weight:bold'>
          工作量
        </td>
        <td nowrap   style='text-align:center;font-weight:bold'>
          金额
        </td>
        <%
          }
        %>
      </html:tr>

      <html:tr clazz='label'>
        <td nowrap   style='text-align:center;font-weight:bold'>
          总计
        </td>
        <td nowrap   style='text-align:center;font-weight:bold'>
          &nbsp;&nbsp;
        </td>
        <%
        if(bonusItems!=null)
          for (int i = 0; i < bonusItems.length; i++) {
        %>
        <td nowrap   style='text-align:center;font-weight:bold'>
          &nbsp;&nbsp;
        </td>
        <td nowrap   style='text-align:center;font-weight:bold'>
          <%=nff.format(Double.parseDouble(bonusItems[i][2]))%>
        </td>
        <%
          }
        %>
        <td nowrap   style='text-align:center;font-weight:bold'>
          &nbsp;&nbsp;
        </td>
        <td nowrap   style='text-align:center;font-weight:bold'>
          &nbsp;&nbsp;
        </td>
        <td nowrap   style='text-align:center;font-weight:bold'>
          &nbsp;&nbsp;
        </td>
        <td nowrap  style='text-align:center;font-weight:bold'>
          &nbsp;&nbsp;
        </td>
      </html:tr>

      <%
        int index = 0;
        if(result!=null){
        for (int i = 0; i < result.length; i += bonusItems.length) {
      %>
      <input type="checkbox" name="parameters"
        value="<%=result[i][0]%>"
        style="display:none">
      <tr>
        <td nowrap>
          <%=result[i][1]%>
        </td>
        <td nowrap>
          <%if (result[i][2].equals("M")) {%>
          男
          <%} else {%>
          女
          <%}%>
        </td>
        <%
          sum=0;
          for (int j = 0; j < bonusItems.length; j++) {
        %>
        <td nowrap>
          <input id="<%=(index + "value" + j)%>" type="text" <% if(isAllot) out.print("disabled='true'");%> class="textInputA" <%if ((i+j)<result.length) {%> value="<%=result[i + j][4]%>" <%}else out.print("value='0'");%>>
        </td>
        <td nowrap>
          <%if((i+j)<result.length) out.print(nff.format(Double.parseDouble(result[i + j][7]))); else out.print("0.00");%>
        </td>
        <%
           if((i+j)<result.length)
            sum=sum+Double.parseDouble(result[i+j][7]);
          }
        %>
        <td nowrap>
          <input type="text"  class="textInputA" id="<%=("checkMark" + index)%>"  value="<%=nff.format(Double.parseDouble(result[i][5]))%>"
            <%if (isAllot) {%> disabled="true" <%}%>><%="&nbsp;%"%>
        </td>
<%
          sum=sum*Double.parseDouble(result[i][5])/100;
%>
        <td nowrap>
          <%=nff.format(sum)%>
        </td>
        <td nowrap>
          <input type="text"  class="textInputA" id="<%=("controlAmount" + index)%>"  value="<%=nf.format(Double.parseDouble(result[i][6]))%>"
            <%if (isAllot) {%> disabled="true" <%}%>>
        </td>
<%
           sum=sum+Double.parseDouble(result[i][6]);
%>
        <td nowrap>
          <%=nff.format(sum)%>
        </td>
      </tr>
      <%
          allsum = allsum + Double.parseDouble(nf.format(sum));
          index++;
        }
      %>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <%
        if(bonusItems!=null)
          for (int i = 0; i < bonusItems.length; i++) {
        %>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <%}%>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><%=nff.format(allsum)%></td>
      </tr>
      <%}%>
    </table>
  <%
    }
  %>
  <input type="hidden" name="subFunction" value="save">
  <input type="hidden" name="yearMonth" value="<%=request.getParameter("yearMonth")%>">
  <input type="hidden" name="deptCode" value="<%=request.getParameter("deptCode")%>">
  <input type="hidden" name="total">
</form>

</html:html>




