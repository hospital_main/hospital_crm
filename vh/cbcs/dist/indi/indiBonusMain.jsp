<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/indi/indiBonusMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<!--
说明:

1.bonus_flag:奖金分配标志.0:没有分配;1:正在分配;2:分配完毕.
2.initial_flag:初始化标志(是否点击初始化按钮).0:未点击;1:已点击.
3.result:二维数组.列数=按DEMO要求的列数
4.title:一维数组.存放动态的项目名称.可以为空或者多条
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.*,  com.viewhigh.cbcs.base.mvc.view.*,com.viewhigh.cbcs.cbcs.dist.base.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<%
  String status=(String)request.getAttribute("status");
  if(status==null){
    status="Y";
  }
  String[][] totalamount=(String [][])request.getAttribute("totalamount");
  if(totalamount!=null){
  	totalamount[0][0]=changeFormat(totalamount[0][0]);
  }
  String[] title = (String[])request.getAttribute("title");
  String [][] totbonus=(String[][])request.getAttribute("totbonus");
  String[][] result = (String[][])request.getAttribute("result");
  String dept_acount_flag = (String)request.getAttribute("dept_acount_flag")==null? "0":(String)request.getAttribute("dept_acount_flag");
%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script language="javascript">

	function find() {
		if (template.yearMonth.value == "") {
			alert('请选择核算年月!');
			return false;
		}
		template.subFunction.value="goIndiBonusMain";
		show_wait()
		template.submit();
		return true;
  	}
	function initial(){
		if (template.yearMonth.value == "") {
			alert("请选择年月！");
			return false
		}
		if(before()){
			if(confirm('确认进行初始化？')){
				template.subFunction.value="goIndiBonusInit";
				show_wait();
				template.submit();
				return true;
			}
		}
	}
	function finish() {
		if(check(2)){
			template.subFunction.value = "goIndiBonusEnd";
			show_wait();
			template.submit();
			return true;
		}
	}
	function save() {
		if(before()){
			if(check(1)){
				template.subFunction.value = "goIndiBonusSave";
				show_wait()
				template.submit();
				return true;
			}
		}
	}
	function before(){
		if (template.yearMonth.value != template.nyear_month.value) {
			alert("核算月已经改变，请重新查询！");
			return false
		}
		if (template.bonus_code.value != template.nbonus_code.value) {
			alert("奖金类别已经改变，请重新查询！");
			return false
		}
		if (template.dept_clin_code.value != template.ndept_clin_code.value) {
			alert("分配科室已经改变，请重新查询！");
			return false
		}
		return true
	}
	<%if(result!=null&&totbonus!=null&&(request.getParameter("bonus_code")!=null&&Integer.parseInt(request.getParameter("bonus_code"))<9000)){%>
function check(m){
  var worklength=<%=totbonus.length%>;
  var reslength=<%=result.length%>;
  var totloads=new Array(worklength);
  var workloads=new Array(worklength);
  var totbonus=new Array(worklength)
	var checkmark=document.all['checkmark'];
	var addtwo=document.all['Addtwo'];
  <%
  double tempbonus=0;
  for(int j=0;j<totbonus.length;j++){
     tempbonus+=Double.parseDouble(totbonus[j][1]);
  %>
      totbonus[<%=j%>]=<%=totbonus[j][1]%>

  <%}%>
  var sumbonus=<%=tempbonus%>
  for(j=0;j<worklength;j++){
    workloads[j] =document.all['workload'+j];
  }
  if (reslength==1) {
    workloads[0][0] = workloads[0];
    checkmark[0] = checkmark;
  }

  for(k=0;k<worklength;k++){
  	var temp=0;
  	for(j=0;j<reslength;j++){
   		temp+=parseInt(workloads[k][j].value=='' ? '0':workloads[k][j].value);
  	}
  	totloads[k]=temp+'';
  }
  var temps=0;
  for(j=0;j<reslength;j++){
  	for( k=0;k<worklength;k++){
  		temps=parseFloat(temps)+parseFloat(totbonus[k])*parseInt(workloads[k][j].value=='' ? '0':workloads[k][j].value)*parseFloat(checkmark[j].value)/(parseInt(totloads[k]==0? '1':totloads[k])*100)
  	}
  }
  var sumaddtwo=0;
  for(j=0;j<addtwo.length;j++)
  	sumaddtwo=parseFloat(sumaddtwo)+parseFloat(addtwo[j].value==''? '0':addtwo[j].value);
  if((roundOff(parseFloat(sumbonus),2)-roundOff((parseFloat(temps)+parseFloat(sumaddtwo)),2))==0){
  return true;
  }else{
  	var cc=''
  	if(m==1){
  	  cc='保存'
  	  }else if(m==2){
  	  cc='完成'
  	  }
  	test=confirm('科室总奖金为'+roundOff(parseFloat(sumbonus),2)+'，个人奖金之和为'+roundOff((parseFloat(temps)+parseFloat(sumaddtwo)),2)+'，确认'+cc+'当前科室的分配吗？')
    return test
  }
}
<%}else{%>
function check(){
	return true;
}
<%}%>
</Script>
<!--弹出处的JS-->




<html:html clazz="main">
<form name="template" method="post" action="distIndiBonus.jspviewhigh">

  <!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>奖金分配</html:title>
	<br>
  <html:table clazz="simple">
    <tr>

      <td>
        核算月:
      </td>
      <td>
        <%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("yearMonth", request.getParameter("yearMonth"))%>
      </td>
      <td>
        分配科室:
      </td>
      <td>
        <%=new SingleSelect(request.getAttribute("dept_clin_code"), "dept_clin_code", request.getParameter("dept_clin_code"), true, true)
        %>
      </td>
      <td>
        奖金类别:
      </td>
      <td>
        <%=new SingleSelect(
          CommonData.getSpecBonusItem (), "bonus_code", request.getParameter("bonus_code"), true, true)
        %>
      </td>
      <td>
      <button class="pageBtn" name=""  onclick="return find();" >查询</button>
      <!-- <img src="images/find.gif" style="cursor:hand" onclick="return find();">-->
      </td>
      <%
        if(status.equals("Y")&&result!=null){
     %>
     <td>  <button class="pageBtn" onclick="preparedPrint();">打印</button></td>
     <%
       }
     %>
    </tr>
  </html:table>


  <br>
  <html:table clazz="simple">
      <tr>
  <%
  if(!"Y".equals(status)&&!(request.getParameter("yearMonth")==null)){
  %>
        <td><button src='images/initial.gif' class='pageBtn' onclick='return initial()'>初始化</button>
  <%
      }
    if(request.getParameter("yearMonth")!=null&&!status.equals("Y")&&result!=null){//点击了初始化按钮并且分配没完成

  %>
  <button class="pageBtn" onclick='return save()'>保存</button> 
          <!--<img src="images/save.gif" class="mouse"  onclick='return save()'>-->
	<button src="images/reset.gif" class="pageBtn" onclick="reset()">重置</button>
	<button src='images/finish.gif' class='pageBtn' onclick='return finish()'>完成</button>
        </td>
<%
    }
    if(result!=null&&result[0].length<5){
%>
				<td nowrap class=numberText>
				奖金总计：<%=totbonus==null? "0":changeFormat(totbonus[0][0]) %>
          <input type="hidden" name="atotbonus" value="<%=totbonus==null? "0":totbonus[0][0]%>">

				</td>
<%}%>
      </tr>
	</html:table>
<%if (request.getParameter("bonus_code")!=null&&Integer.parseInt(request.getParameter("bonus_code"))>=9000){%>
      <html:table clazz="result">
      <html:tr clazz='label'>
        <td nowrap class="resultLabel">
          姓名
        </td>
        <td nowrap class="resultLabel">
          工作量
        </td>
        <td nowrap class="resultLabel">
          实发金额
        </td>
      </html:tr>

      <%if(result!=null){%>
      <tr>
        <td nowrap >
          总计
        </td>
        <td nowrap class=numberText>
        </td>
        <td nowrap class=numberText>
          <%=totalamount==null? "":totalamount[0][0]%>
        </td>
      </tr>
      <%
      for(int j=1;j<result.length;j++){%>
         <tr>
         <td nowrap>
          <%=result[j][1]%>
          <input type="hidden" name="emp_id" value="<%=result[j][0]%>">
         </td>
         <td nowrap >
           <input:text name='workload' type="number" numType="int" unabled="<%=status.equals("Y")? "true":"false"%>" dVal="<%=result[j][2]%>" cssclass="textInputE" maxlength="14" >
             <input:textError errorCode="min" value="0">工作量不能小于0</input:textError>
           </input:text>
        </td>
         <td nowrap class=numberText><%=changeFormat(result[j][3])%></td>
         </tr>
       <%}}%>
       </html:table>
<%}else{%>
  	<html:table clazz="result">
      <html:tr clazz='label'>
        <td nowrap class="resultLabel"   rowspan="2">姓名</td>

        <%
        if(totbonus!=null)
          for (int i = 0; i < title.length; i++) {
        %>
        <td nowrap class="resultLabel"   colspan="2">
          <%=totbonus[i][2]%>
          <input type="hidden" name="bonusname<%=i%>" value="<%=totbonus[i][0]%>"><!--奖金-->
        </td>
        <%
          }
        %>
        <td nowrap class="resultLabel"   rowspan="2">
          考核成绩
        </td>
        <td nowrap class="resultLabel"   rowspan="2">
          应发金额
        </td>
        <td nowrap class="resultLabel"   rowspan="2">
          加减项2
        </td>
        <td nowrap class="resultLabel"   rowspan="2">
          实发金额
        </td>
      </html:tr>
      <html:tr clazz='label'>
        <%
        if(totbonus!=null)
          for (int i = 0; i < totbonus.length; i++) {

        %>
        <td nowrap  class="resultLabel" >
          工作量
        </td>
        <td nowrap  class="resultLabel" >
          金额
        </td>
        <%
          }
        %>
      </html:tr>
   <%
        if(result!=null){
   %>
   		<tr >
        <td nowrap >总计</td>

        <%
        if(totbonus!=null)
          for (int j = 0; j < totbonus.length; j++) {
        %>
        <td nowrap class=numberText></td>
        <td nowrap  class=numberText>
          <%=changeFormat(totbonus[j][1])%>
          <input type="hidden" name="bonus<%=j%>" value="<%=totbonus[j][1]%>"><!--奖金-->
        </td>
        <%
          }
        %>
        <td nowrap   >

        </td>
        <td nowrap  >

        </td>
        <td nowrap  >

        </td>
        <td nowrap class=numberText >        
				<%=totalamount==null? "":totalamount[0][0]%>
        </td>
      </tr>

   <%
        for (int i = 0; i < result.length; i++) {
      %>

      <tr >
        <td nowrap>
          <%=result[i][0]%><!--人员姓名-->
          <input type="hidden" name="emp_id" value="<%=result[i][result[0].length-6]%>"><!--人员代码-->
        </td>
      <% //处理动态的列
        int n = title.length;
        n=n*2;
        int kk=0;
        for(int j=1;j<=n;j++){
          if(j%2==1&&(!status.equals("Y"))){//是工作量并且没有分配完成时,显示输入框

      %>
        <td class=numberText>
           <input:text name='<%="workload"+kk%>' type="number" numType="int" unabled="<%=status.equals("Y")? "true":"false"%>" dVal="<%=result[i][j]%>" cssclass="textInputE" maxlength="14" >
             <input:textError errorCode="min" value="0">工作量不能小于0</input:textError>
           </input:text>
        </td>
       <%
          kk++;
          }else if(j%2==1&&(status.equals("Y"))){%>
        <td nowrap class=numberText>
          <%=result[i][j]%>
        </td>
          <%          
          }else {

       %>
        <td nowrap class=numberText>
          <%=changeFormat(result[i][j])%>
        </td>
       <%
          }
        }
       %>
       <td nowrap class=numberText>
        <a href="distIndiBonus.jspviewhigh?subFunction=goIndiBonusScore&emp_id=<%=result[i][result[0].length-6]%>&employee_name=<%= result[i][0]%>&year_month=<%= request.getParameter("yearMonth")%>&status=<%=status%>&dept_clin_code=<%=request.getParameter("dept_clin_code") %>&bonus_code=<%=request.getParameter("bonus_code")%>"><%= changeFormat(result[i][result[i].length-4])%>%</a>
         <input type="hidden" name="checkmark" value="<%= result[i][result[i].length-4]%>"> <!--考核成绩-->
       </td>
       <td nowrap class=numberText>
          <%=changeFormat(result[i][result[0].length-3])%><!--应发金额-->
       </td>
       <td nowrap class=numberText>

      <%if(!status.equals("Y")){%>
           <input:text name='Addtwo' type="number" numType="float" unabled="<%=status.equals("Y")? "true":"false"%>" dVal="<%=changeFormat(result[i][result[0].length-2],"##0.00")%>" cssclass="textInputE" maxlength="14" >
           </input:text>
       <%}else{
         out.print(changeFormat(result[i][result[0].length-2]));
       }%>
       </td>
       <td nowrap class=numberText>
          <%=changeFormat(result[i][result[0].length-1])%><!--实发金额-->
       </td>
     </tr>
      <%}
       }
      %>
    </html:table>
    <%}%>
    <%if((!"Y".equals(status))&&result!=null&&result[0].length>5){%>
    <br/>
    <span class='normalText'>在修改考核成绩之前请先保存(以防刚刚改动的数据丢失!)</span>
    <%}%>
 <input type="hidden" name="subFunction">

 <input type="hidden" name="status"  value=<%=status%>>
  <input type="hidden" name="reslength" value="<%=result==null ? 0 : result.length%>">
  <input type="hidden" name="titlength" value="<%=title==null ? 0 : title.length%>">
  <input type="hidden" name="nbonus_code" value="<%=request.getParameter("bonus_code")%>">
  <input type="hidden" name="nyear_month" value="<%=request.getParameter("yearMonth")%>">
  <input type="hidden" name="ndept_clin_code" value="<%=request.getParameter("dept_clin_code")%>">
 </form>
</html:html>








