<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/indi/indiStatusSubMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 com.viewhigh.cbcs.base.sql.BaseRO,java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function return1() {
   window.history.back();
  }
</script>
<html:html clazz="main">
<br>
<!-- 信息提示栏 -->
<html:message/>
<!-- 标题栏 -->
<html:title clazz="module">分配科室个人考核分数 
</html:title>
<form name="template" method="post" action="distBenefitDept.jspviewhigh">
<!-- 简单信息 -->
<table width='100%' border='0'>
    <tr>
        <td class='tableTitle' nowrap='nowrap'>
          <%
           String year_month =request.getParameter("year_month");
           String dept_clin_code =request.getParameter("dept_clin_code");
           String total_mount =request.getParameter("total_mount");
           String employee_name =request.getParameter("employee_name");
           if(year_month!=null){
              out.print(year_month.substring(0,4)+"年");
              if(year_month.length()==5)
                 out.print(year_month.substring(4,5)+"月");
              else
                 out.print(year_month.substring(4,6)+"月");
              }
           out.print(com.viewhigh.cbcs.cbcs.dist.base.CommonData.getClinicDeptName(dept_clin_code)+"科室");
           out.print(" "+employee_name);
         %>
         考核分数表
       </td>
   </tr>
</table>
<!-- 复杂信息 -->
<html:table clazz="complex">
 <!-- 操作 -->
    <tr>
      <td>
         <table width='100%'>
           <tr>
              <td  nowrap='nowrap'>
               <button class="pageBtn" onclick="return return1();">返回</button> 
           </td>
          </tr>
        </table>
      </td>
    </tr>
  <!-- 结果集 -->
    <tr><%
    
    String[][] result = (String[][])request.getAttribute("result");
    double tot=0;
    if (result!=null) {

    for(int j=0;j<result.length;j++){
    tot+=Double.parseDouble("".equals(result[j][5]) ? "0":result[j][5])*Double.parseDouble("".equals(result[j][6]) ? "0":result[j][6]);
    }
    }
    %>
    	<td>
        <html:table clazz="result">
          <tr>
            <td class="resultLabel">指标编码</td>
            <td class="resultLabel">指标名称</td>
            <td class="resultLabel">权重(%)</td>
            <td class="resultLabel">考核成绩（％）</td>
           </tr>

          <tr class="rowGray">
            <td class="normalText">总计</td>
            <td class="normalText">&nbsp;</td>
            <td class="numberText">&nbsp;</td>
            <td class="numberText"><%=changeFormat(""+tot*100)%></td>
          </tr>
<%

    if (result!=null) {
      for (int i=0; i<result.length; i++) {
         for(int j=0;j<result[i].length;j++){
          if(result[i][j]==null||result[i][j].equals(""))
            result[i][j]="&nbsp;";
         }
        String rowColor = "rowGray";
        if (i/2*2==i) rowColor = "rowWhite";
 %>
      <tr class="<%=rowColor%>">
        <td class="normalText">
        <%=result[i][4]%>
        </td>
        <td class="normalText" width="30%">
        <%=result[i][3]%>
        </td>
        <td class="numberText" width="30%">
        <%=changeFormat(""+Double.parseDouble(result[i][5])*100)%>
        </td>
        <td class="numberText" width="30%">
        <%=changeFormat(""+Double.parseDouble(result[i][6])*100)%>
        </td>
      </tr>
<%
      }
    }
%>
</html:table>
    </td>
  </tr>
</html:table>
<input type="hidden" name="subFunction">
</form>
</html:html>



