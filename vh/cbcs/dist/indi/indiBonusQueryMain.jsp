<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/indi/indiBonusQueryMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="java.util.*,java.text.*, com.viewhigh.cbcs.base.mvc.view.*,  com.viewhigh.cbcs.base.mvc.view.component.*,com.viewhigh.cbcs.cbcs.dist.base.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
 // String[][] depts = (String[][]) request.getAttribute("depts");
  String[][] result = (String[][]) request.getAttribute("result");
  String queryType = request.getParameter("queryType");
  String flag = "";
%>

<Script language="javascript">
  function query() {
    
    if (template.yearMonth.value == '') {
      alert('请选择核算年月!');
      return false;
    }   
    template.subFunction.value="goIndiBonusQueryMain";
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="distIndiBonusQuery.jspviewhigh">

  <!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>奖金查询</html:title>

  <!-- 简单信息 -->
  <html:table clazz="simple">
     <tr>
      <td nowrap class="signText" >核算月: </td>
      <td nowrap class="normalText" ><%=new MonthComponent("yearMonth",request.getParameter("yearMonth"))%>
      </td>
      
      <td nowrap class="signText" >
        分配科室:
      </td>
      <td>
         <%=new SingleSelect(
          request.getAttribute("dept_clin_code"), "dept_clin_code", request.getParameter("dept_clin_code"), true, false)
        %>
      </td>
      
      <td nowrap class="signText" >
        职工名字:
      </td>
      <td nowrap class="normalText" >
        <input type="text" name="emp_name"
          <%if (request.getParameter("emp_name") != null) {%>
            value=<%=request.getParameter("emp_name")%>
          <%}%>>
      </td>
       
    </tr>
    <tr>
      <td>奖金类别:</td>
       <td>
         <%=new SingleSelect(
          CommonData.getSpecBonusItem(), "bonus_code", request.getParameter("bonus_code"), true, true)
         %>
         
      </td>
      <td class="normalText" >　</td>
      <td>
      <button class="pageBtn" name="" onclick="return query();">查询</button>
      <!-- <img src="images/find.gif" class="mouse" onclick="return query();" />-->
      </td>
      <%if(result!=null){%>
      <td>  <button class="pageBtn" onclick="return preparedPrint();">打印</button>
      </td>
      <%}%>	 
    </tr>
  </html:table>

  <br>
   <table width='100%' border='0'>
      <tr>
        <td class='tableTitle' nowrap='nowrap'> <%=request.getParameter("yearMonth")==null ? "":((String)request.getParameter("yearMonth").substring(0,4))%>年<%=request.getParameter("yearMonth")==null ? "":((String)request.getParameter("yearMonth").substring(4))%>月分配科室<%=request.getParameter("dept_clin_code")==null ? "":com.viewhigh.cbcs.cbcs.dist.base.CommonData.getDeptName(request.getParameter("dept_clin_code"))%><%=request.getParameter("bonus_code")==null ? "":(request.getParameter("bonus_code").equals("0301")? "月度奖金":com.viewhigh.cbcs.cbcs.dist.base.CommonData.getBonusName(request.getParameter("bonus_code")))%>奖金表</td>
      </tr>
      
   </table>
  <!-- 复杂信息 -->
  <html:table clazz="complex">

  <!-- 结果集 -->
  <tr>
    <td>
      <html:table clazz="result">

        <html:tr clazz='label'>
          <td>分配科室名称</td>
          <td>姓名</td>
          <td>金额</td>
        </html:tr>

        <%
          DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");

          if (result != null) {
            for (int i = 0; i < result.length; i++) {
              String rowColor = "rowGray";
              for (int j = 0; j < result[i].length; j++) {
                if (result[i][j] == null || result[i][j].trim().equals("")) {
                  result[i][j] = "&nbsp;";
                }
              }

              if (!result[i][1].equals("&nbsp;")) {
                result[i][0] = "&nbsp;";
                rowColor = "rowWhite";
              }
        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText"><%=result[i][0]%></td>
          <td class="normalText"><%=result[i][1]%></td>
          <td class="numberText"><%=result[i][2]%></td>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>
  </html:table>

 <input type="hidden" name="subFunction" />
</form>
</html:html>


