<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/indi/indiStatusMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript" src="javascript/check.js"></script>
<Script Language="JavaScript">
  function find(){
    if(template.year.value==""){
      alert("请选择年");
      return false
    }
    template.subFunction.value='goIndiStatusMain';
    template.submit();
    show_wait();
    return true;
  }
 function changepage(){
   var c = document.all["selectA"].value
   if(c=="1"||c=="2"||c=="3")
      template.subFunction.value='goDeptStatusNormal';
   else
     if(c=="4")
       template.subFunction.value='goDeptStatusOther';
     else
       template.subFunction.value='goDeptStatusNormal';
   template.submit();
   show_wait();
 }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="distIndiStatus.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>
  <!-- 标题栏 -->
  <html:title clazz='module'>分配状态</html:title>
    <br>
  <!-- 查询信息 -->
    <%BaseRO  ro= (BaseRO)request.getAttribute("result");
      TableMarge oper = new TableMarge(ro, "return find()");
      String[][] result = null;
      if(ro!=null)
        result =ro.getTableResult();
      else
        result=null;
      String[] title = (String[])request.getAttribute("title");
     	Date d = new Date();
      String nowYear =String.valueOf(d.getYear()+1900);%>
  <html:table clazz="simple">
    <tr>
      <td nowrap width="40"></td>
      <td nowrap class="signText">期间：
      <%String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};%>
      <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(year,"year",request.getParameter("year")==null?nowYear:request.getParameter("year"),false,false)%>
      </td>
      <td nowrap class="signText">奖金项目：
         <%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(com.viewhigh.cbcs.cbcs.dist.base.CommonData.getBaseOtherBonusItem(),"bonus_code",request.getParameter("bonus_code"),true,true)%>
      </td>
      <td>
      <button class="pageBtn" name=""  onclick="return find();" >查询</button>
      <!--<img src="images/find.gif" style='cursor:hand' onclick="return find();" >--></td>
    </tr>
  </html:table>
    <br>
    <table width='100%' border='0'>
      <tr>
        <td class='tableTitle' nowrap='nowrap'>
          <%
           String year_month = request.getParameter("year")==null?nowYear:(String)request.getParameter("year");
           if(year_month!=null)
             out.print(year_month+"年");
         %>
         <%String name=com.viewhigh.cbcs.cbcs.dist.base.CommonData.getBonusName(request.getParameter("bonus_code")==null ? "":request.getParameter("bonus_code"));%>
         <%=name%>
         分配状态表
        </td>
     </tr>
  </table>
  <html:table clazz="complex">
  <!-- 操作 -->
   <tr>
      <td>
         <%=oper%>
      </td>
   </tr>
  <!-- 结果集 -->
  <tr>
    <td>
      <html:table clazz="result">
        <html:tr clazz='label'>
        <%if(title!=null){
            for(int i=0;i<title.length;i++){%>
          <td><%=title[i]%></td>
          <%}
          }%>
        </html:tr>
        <%if ( result != null ){
            for (int i = 0; i < result.length; i++ ){
              String rowColor = "rowGray";
              if (i/2*2==i)
                rowColor = "rowWhite";%>
          <tr CLASS="<%=rowColor%>">
    <%
         for(int j=0;j<result[i].length-1;j++){
             if(result[i][j].equals("Y")){
    %>
          <td class="normalText">
          <%String codes=request.getParameter("bonus_code");
          if(codes==null){
          codes="0";}
          if(Integer.parseInt(codes)<9000){
          %>
            <a href="distIndiStatus.jspviewhigh?subFunction=goIndiStatusDetail&bonus_code=<%=request.getParameter("bonus_code")%>&dept_name=<%=result[i][0]%>&dept_clin_code=<%=result[i][result[i].length-1]%>&year_month=<%=year_month%><%=j<10 ? "0"+j:j+""%>">已经完成</a>
            <%}else{%>
            已经完成
            <%}%>
          </td>
         <%
           } else {
          %>
          <td class="normalText">
          <%
            if(result[i][j].toString().equals("P")){
              out.print("进行中");
              }else if(result[i][j].equalsIgnoreCase("N")){
              out.print("未开始");
              }else{
              out.print(result[i][j]);
              }
             
            %>
          </td>
            <%
               }
           }
           %>
        </tr>
     <%
        }
     }
     %>
      </html:table>
    </td>
  </tr>
  <!-- 操作 -->
  </html:table>
  <input type=hidden name="subFunction"/>
</form>
</html:html>
