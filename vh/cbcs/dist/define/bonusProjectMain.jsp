<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/define/bonusProjectMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function find() {
	  template.subFunction.value='goBonusProjectMain';
		show_wait();
    template.submit();
    return true;
	}
  function create(){
    template.subFunction.value='goBonusProjectPreCreate';
		show_wait();
    template.submit();
    return true;
  }
  function remove(){
    var flag = false;
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey' && template.elements[i].checked == true)
        flag = true;
    }
    if( flag != false){
      if (confirm('是否停用')){
        template.subFunction.value='goBonusProjectStop';
				show_wait();
        template.submit();
        return true;
      }else{
        return false;
      }
    }else{
      alert( "请先选择,再停用!");
      return false;
    }
  }
  function use(){
    var flag = false;
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey' && template.elements[i].checked == true)
        flag = true;
    }
    if( flag != false){
      if (confirm('是否启用')){
        template.subFunction.value='goBonusProjectStart';
				show_wait();
        template.submit();
        return true;
      }else{
        return false;
      }
    }else{
      alert( "请先选择,再启用!");
      return false;
    }
  }
  function selectAll(){
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey'){
        if(template.elements[i].disabled == false){
          template.elements[i].checked = true;
        }
      }
    }
  }
</script>

<html:html clazz="main">
  <form name="template" method="post" action="distBonusProject.jspviewhigh">
    <!-- 信息提示栏 -->
    <html:message/>
    <!-- 标题栏 -->
    <html:title clazz="module">奖金项目</html:title>
    <br/>
    <!-- 复杂信息 -->
    <html:table clazz="complex">
    <!-- 操作 -->
      <tr>
        <td>
          <table width='100%' border='0'>
            <tr>
              <td  nowrap='nowrap'>
                <button src='images/selectedAll.gif' class='pageBtn' onclick='return selectAll()'>全选</button>
                <button src='images/reset.gif' class='pageBtn' onclick='return reset()'>重置</button>
                <button src='images/stop.gif' class='pageBtn' onclick='return remove()'>停用</button>
                <button src='img/qiyong.gif' class='pageBtn' onclick='return use()'>启用</button>
                <button src='images/create.gif' class='pageBtn' onclick='return create()'>添加</button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </html:table>
    <!-- 结果集 -->
    
    <html:table clazz="result">
      <html:tr clazz="label">
        <td nowrap>选择</td>
        <td nowrap>奖金项目代码</td>
        <td nowrap>奖金项目名称</td>
        <td nowrap>公 式</td>
        <td nowrap>医疗项目类</td>
        <td nowrap>状态</td>
        <td nowrap>说 明</td>
      </html:tr>
<%String[][] result = (String[][])request.getAttribute("result");
	if (result!=null) {
		for (int i=0; i<result.length; i++) {
			String primaryKey = result[ i ][ 0 ];
				for(int j=0;j<result[i].length;j++){
					if(result[i][j]==null||result[i][j].equals(""))
							result[i][j]="&nbsp";
				}
			String rowColor = "rowGray";
			if (i/2*2==i) 
				rowColor = "rowWhite";%>

				  
      <tr class="<%=rowColor%>">
        <td>
<%    if (Integer.parseInt(primaryKey)>=4){%>
          <input name="primaryKey" type="checkbox" value="<%=primaryKey%>"/>
<%		}%>
        </td>
        <td class="normalText">
          <%= primaryKey%>
        </td>
        <td class="normalText">
          <%= result[i][2]%>
        </td>
        <td class="normalText">
          <%= result[i][3]%>
        </td>
        <td class="normalText">
          <%= result[i][1]%>
        </td>
        <td class="normalText">
          <%= result[i][5]%>
        </td>
        <td class="normalText">
          <%= result[i][4]%>
        </td>
      </tr>
<% 	}
	}%>
    </html:table>
    <input type="hidden" name="subFunction"/>
  </form>
</html:html>
