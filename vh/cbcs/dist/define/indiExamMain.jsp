<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/define/indiExamMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function find() {
	  template.subFunction.value='goIndiExamMain';
		show_wait();
    template.submit();
    return true;
	}

  function store() {
    if (template.dept_clin_code.value == '') {
      alert('请选择分配科室');
      return false;
    }

		if (template.dept_clin_code.value != '<%=request.getParameter("dept_clin_code")%>') {
			alert("分配科室已经改变，请重新查询！");
			return false
		}
    if(total()==false)
      return false;
    template.subFunction.value='goIndiExamStore';
		show_wait();
    template.submit();
    return true;
  }

  function total(){
    var total=0;
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'check_power')
       total = total + parseFloat(template.elements[i].value);
    }
    if(total!=100){
      alert("权重比例之和应为100!现在是:"+total);
      return false;
    }
    return true;
  }
</script>

<html:html clazz="main">
  <form name="template" method="post" action="distIndiExam.jspviewhigh">
    <!-- 信息提示栏 -->
    <html:message/>
    <!-- 标题栏 -->
    <html:title clazz="module">个人考核定义</html:title>
    <!-- 简单信息 -->
    <html:table clazz="simple">
      <tr>
      	<td class="normalText" width="40">　</td>
        <td nowrap class="signText" width="72">分配科室：</td>
        <td nowrap class="normalText">
          <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getBigDept(), "dept_clin_code", request.getParameter("dept_clin_code"), true, true)%>
        </td>
        <td width="100">
        <button class="pageBtn" name="" onclick="template.subFunction.value='goIndiExamMain';show_wait();template.submit();">查询</button>
        <!--<img src="images/find.gif" style="cursor:hand" onclick="template.subFunction.value='goIndiExamMain';show_wait();template.submit();"/>-->
        </td>
      	<td class="normalText" width="40">　</td>
      </tr>
    </html:table>
    <table width="100%" border="0">
      <tr>
        <td class="tableTitle" nowrap="nowrap">
<%=request.getParameter("dept_clin_code")==null?(CommonData.getBigDept()==null?"":CommonData.getBigDept()[0][1]):CommonData.getDeptName(request.getParameter("dept_clin_code"))%>个人考核设置表</td>

      </tr>
    </table>
    <br/>
<%String[][] result = (String[][])request.getAttribute("result");
	if (result!=null) {%>
    <!-- 复杂信息 -->
    <html:table clazz="complex">
    <!-- 操作 -->
      <tr>
        <td>
          <table width='100%' border='0'>
            <tr>
              <td  nowrap='nowrap'>
                <button src='images/save.gif' class='pageBtn' onclick='return store()'>保存</button>
                <button src='images/reset.gif' class='pageBtn' onclick='return reset()'>重置</button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </html:table>

    <html:table clazz="result">
      <html:tr clazz="label">
        <td width="60%">指标名称</td>
        <td width="40%">权重（%）</td>
      </html:tr>
<%	for (int i=0; i<result.length; i++) {
			String primaryKey = result[ i ][ 0 ];
			for(int j=0;j<result[i].length;j++){
				if(result[i][j]==null||result[i][j].equals(""))
					result[i][j]="&nbsp";
			}
		String rowColor = "rowGray";
		if (i/2*2==i)
			rowColor = "rowWhite";%>
      <tr class="<%=rowColor%>">
        <td class="normalText" width="50%">
          <%=  result[i][1]%>
          <input type="hidden" name="check_id" value="<%= result[i][0]%>"/>
        </td>
        <td class="normalText" width="50%">
          <input:text name='check_power' type="number" numType="float"  dVal="<%=result[i][2]%>" cssclass="textInputE" maxlength="14">
            <input:textError errorCode="min" value="0">权重比例不应小于0</input:textError>
            <input:textError errorCode="max" value="100">权重比例不应大于100</input:textError>
          </input:text>

        </td>
      </tr>
<% 	}%>
    </html:table>
<%}%>
    <input type="hidden" name="subFunction"/>
  </form>
</html:html>