<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/define/checkIndexCreate.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create(){
    template.subFunction.value='goCheckIndexCreate';
    if (template.check_formula.value.length > 255) {
      alert("数据太长了！");
      return false;
    }
		if(template.check_name.value==""){ 
			alert("考核指标名称不能为空");
			return;
		}

		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back(element) {
    template.subFunction.value='goCheckIndexMain';
    element.submit();
  }
</script>

<html:html clazz="main">
<form name="template" method="post" action="distCheckIndex.jspviewhigh">
<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<html:title clazz='module'>添加考核指标</html:title>
<br>
<!-- 简单信息 -->
<html:table clazz="simple">
  <tr>
    <td class="signText" nowrap="nowrap">考核指标名称:</td>
    <td class="normalText" nowrap="nowrap"><input type="text" name="check_name" size="20" maxlength="20"></td>
  </tr>
  <tr>
    <td class="signText" nowrap="nowrap">考核指标类别:</td>
    <td class="normalText" nowrap="nowrap">
    		<input type="radio" name="check_kind" value="1" checked>科室
    		<input type="radio" name="check_kind" value="2">个人
    </td>
  </tr>
  <tr>
    <td class="signText" nowrap="nowrap">考核指标公式:</td>
    <td class="normalText" nowrap="nowrap">
      <textarea name='check_formula' rows='2' cols='40'></textarea>
    </td>
  </tr>
  <tr>
    <td colspan="2">
    	<button class="pageBtn" onclick="return create();" >添加</button>
    	<button class="pageBtn" onclick="return reset();" >重置</button>
    	<button class="pageBtn" onclick="return back(template);">返回</button>  
    	<!--<img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" />
    	<img src="images/return.gif" class="mouse" onclick="return back(template);" />-->
    </td>
  </tr>
</html:table>
<input type="hidden" name="subFunction">
</form>
</html:html>
