<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/define/bonusSchemeStore.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>


<%
  String[][] result = (String[][])request.getAttribute("result");
  String title = CommonData.getDeptName(request.getParameter("dept_clin_code"));
%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript" src="javascript/common.js" ></Script>
<Script Language="JavaScript">
	function store() {
    //必须有一个被选中

	  template.subFunction.value='goBonusSchemeStore';
	  show_wait();
	  template.submit();
	  return true;
	}

	// 返回
	function back() {
	  template.subFunction.value='goBonusSchemeReturnMain';
		template.dept_clin_code.value='';
		for(var i=0;i<template.elements.length; i++){
      if (template.elements[i].name == 'bonus_code'){
				template.elements[i].value	= '';
			}
		}
		show_wait();
	  template.submit();
	}

  function search(){
		if(template.dept_clin_code.value==''){
			alert("分配科室不能为空");
			return;
		}
    template.subFunction.value='goBonusSchemeFind';
    show_wait();
    template.submit();
    return true;
  }

  function selectBonus(comp) {
    var length = document.getElementById('bonus_code0').parentNode.parentNode.parentNode.childNodes.length;
    if (comp.value=='1') {
      if (comp.checked) {
        for (var i=1; i<length-1; i++) {
          var checkbox = document.getElementById('checkbox'+i);
          if (checkbox.value != '2' && checkbox.value != '3')
            checkbox.style.display=''
          else
            checkbox.style.display='none'
        }

        var bonus_scale = document.getElementById('bonus_scale0')
        bonus_scale.style.display = ''
        bonus_scale.disabled = false

        document.getElementById('bonus_code0').disabled = false
        document.getElementById('isUpdate0').disabled = false
      } else {
        for (var i=1; i<length-1;i++) {
          var checkbox = document.getElementById('checkbox'+i);
          if (checkbox.value == '2' || checkbox.value == '3')
            checkbox.style.display=''
          else
            checkbox.style.display='none'
        }

        var bonus_scale = document.getElementById('bonus_scale0')
        bonus_scale.style.display = 'none'
        bonus_scale.disabled = true

        document.getElementById('bonus_code0').disabled = true
        document.getElementById('isUpdate0').disabled = true
      }
    } else if (comp.value=='2') {
      if (comp.checked) {
        document.getElementById('checkbox0').style.display = 'none'
        document.getElementById('checkbox2').style.display = 'none'

        var bonus_scale = document.getElementById('bonus_scale1')
        bonus_scale.style.display = ''
        bonus_scale.disabled = false

        var cost_control = document.getElementById('cost_control1')
        cost_control.style.display=''
        cost_control.disabled=false

        var cost_scale = document.getElementById('cost_scale1')
        cost_scale.style.display=''
        cost_scale.disabled=false

        document.getElementById('bonus_code1').disabled = false
        document.getElementById('isUpdate1').disabled = false
      } else {
        document.getElementById('checkbox0').style.display = ''
        document.getElementById('checkbox2').style.display = ''

        var bonus_scale = document.getElementById('bonus_scale1')
        bonus_scale.style.display = 'none'
        bonus_scale.disabled = true

        var cost_control = document.getElementById('cost_control1')
        cost_control.style.display='none'
        cost_control.disabled=true

        var cost_scale = document.getElementById('cost_scale1')
        cost_scale.style.display='none'
        cost_scale.disabled=true

        document.getElementById('bonus_code1').disabled = true
        document.getElementById('isUpdate1').disabled = true
      }
    } else if (comp.value=='3') {
      if (comp.checked) {
        document.getElementById('checkbox0').style.display = 'none'
        document.getElementById('checkbox1').style.display = 'none'

        var bonus_scale = document.getElementById('bonus_scale2')
        bonus_scale.style.display = ''
        bonus_scale.disabled = false

        document.getElementById('bonus_code2').disabled = false
        document.getElementById('isUpdate2').disabled = false
      } else {
        document.getElementById('checkbox0').style.display = ''
        document.getElementById('checkbox1').style.display = ''

        var bonus_scale = document.getElementById('bonus_scale2')
        bonus_scale.style.display = 'none'
        bonus_scale.disabled = true

        document.getElementById('bonus_code2').disabled = true
        document.getElementById('isUpdate2').disabled = true
      }
    } else { //单项奖金
      if (comp.checked) {
        for (var i=0; i<length-1; i++) {
          if (comp==document.getElementById('checkbox'+i)) {
            var bonus_scale = document.getElementById('bonus_scale'+i);
            bonus_scale.style.display = ''
            bonus_scale.disabled = false

            document.getElementById('bonus_code'+i).disabled = false
            document.getElementById('isUpdate'+i).disabled = false
          }
        }

        document.getElementById('checkbox0').disabled = true
      } else {
        var flag = false; //检查是否有单项奖金被选中

        for (var i=3; i<length-1; i++) {
          var checkbox = document.getElementById('checkbox'+i);
          if (comp==checkbox) {
            var bonus_scale = document.getElementById('bonus_scale'+i);
            bonus_scale.style.display = 'none'
            bonus_scale.disabled = true

            document.getElementById('bonus_code'+i).disabled = true
            document.getElementById('isUpdate'+i).disabled = true
          } else if (checkbox.checked) {
            flag = true;
          }
        }

        if (!flag) {
          document.getElementById('checkbox0').disabled = false
        }
      }
    }
  }

	function init() {
    if (document.getElementById('bonus_code0') != null) {
      var length = document.getElementById('bonus_code0').parentNode.parentNode.parentNode.childNodes.length;
      var flag = false;
      for (var i=0; i<length-1; i++) {
        var bonus_code = document.getElementById('bonus_code'+i);
        if (bonus_code.value != '') {
          var tick = document.createElement("<B></B>")
          tick.innerText = "√";
          bonus_code.parentNode.appendChild(tick);
          bonus_code.disabled = false

          var isUpdate = document.getElementById('isUpdate'+i);
          isUpdate.disabled = false;
          isUpdate.value = "true";

          var bonus_scale = document.getElementById('bonus_scale'+i);
          bonus_scale.style.display=''
          bonus_scale.disabled=false

          if (bonus_code.value == "2") {
            var cost_control = document.getElementById('cost_control'+i);
            cost_control.style.display=''
            cost_control.disabled=false

            var cost_scale = document.getElementById('cost_scale'+i);
            cost_scale.style.display=''
            cost_scale.disabled=false
          } else if (bonus_code.value == "1") {
            for (i++; i<length-1; i++) {
              var checkbox = document.getElementById('checkbox'+i);
              if (checkbox.value != '2' && checkbox.value != '3') {
                bonus_code = document.getElementById('bonus_code'+i);
                if (bonus_code.value != '') {
                  var tick = document.createElement("<B></B>")
                  tick.innerText = "√";
                  bonus_code.parentNode.appendChild(tick);
                  bonus_code.disabled = false;

                  var isUpdate = document.getElementById('isUpdate'+i);
                  isUpdate.disabled = false;
                  isUpdate.value = "true";

                  var bonus_scale = document.getElementById('bonus_scale'+i);
                  bonus_scale.style.display=''
                  bonus_scale.disabled=false
                } else {
                  checkbox.style.display=''
                  bonus_code.value = document.getElementById('checkbox'+i).value;
                }
              }
            }
          }

          flag = true;
        } else {
          bonus_code.value = document.getElementById('checkbox'+i).value;
        }
      }

      if (!flag) { // 此科室没有任何奖金项目被设置
        for (var i=0; i<length-1; i++) {
          var checkbox = document.getElementById('checkbox'+i);
          if (checkbox.value=='1' || checkbox.value=='2' || checkbox.value=='3')
            checkbox.style.display=''
        }
      }
    }
	}
</script>

<html:html clazz="main">
<form name="template" method="post" action="distBonusScheme.jspviewhigh">

<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<html:title clazz='module'>科室奖金分配方案</html:title>
<br>

<!-- 简单信息 -->
<html:table clazz="simple">
  <tr>
    <td class="normalText" width="40">　</td>
    <td class="signText" nowrap="nowrap" width="72">分配科室：</td>
    <td class="normalText" nowrap="nowrap">
      <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getBigDept(), "dept_clin_code", request.getParameter("dept_clin_code"), true, false)%>
    </td>
    <td width="100">
    <button class="pageBtn" name=""  onclick="return search()" >查询</button>
    <!--<img src="images/find.gif" class="mouse" onclick="return search()"/>--></td>
    <td class="normalText" width="40">　</td>
  </tr>
</html:table>

<html:title clazz='table'><%=title%> 奖金分配表</html:title>

<!-- 复杂信息 -->
<html:table clazz="complex">
<!-- 操作 -->
  <tr>
    <td colspan="5">
    <%if (result!=null) {%>
    <button class="pageBtn" onclick="return store();" >保存</button> 
	   <!-- <img src="images/save.gif" class="mouse" onclick="return store();" />-->
	  <%}%>
	  <button class="pageBtn" onclick="return back();">返回</button> 
      <!--<img src="images/return.gif" class="mouse" onclick="return back();" />-->
    </td>
  </tr>
  <!-- 结果集 -->
	<tr>
  	<td>
			<html:table clazz="result">
			  <html:tr clazz='label'>
			    <td width='30'>选择</td>
			    <td>奖金项目编码</td>
			    <td>奖金项目名称</td>
			    <td>提取比例(%)</td>
			    <td >成本调控额</td>
			    <td >成本比例(%)</td>
			  </html:tr>
      <%if (result!=null) {
		      for (int i=0; i<result.length; i++) {
			      String rowColor = "rowGray";
			      if (i/2*2==i)
				      rowColor = "rowWhite";%>
			  <tr class="<%=rowColor%>">
			    <td>
			      <input id='bonus_code<%=i%>' name="bonus_code" class='textInputE' style='display:none' disabled value='<%=result[i][0]%>'>
            <input id='checkbox<%=i%>' style='display:none;' type="checkbox" value='<%=result[i][1]%>' onclick="return selectBonus(this)">
            <input id='isUpdate<%=i%>' name='isUpdate' type='hidden' disabled >
			    </td>
			    <td height='24' class="normalText"><%=result[i][1]%></td>
			    <td class="normalText"><%=result[i][2]%></td>
			    <td class="normalText"><input id='bonus_scale<%=i%>' name='bonus_scale' class='textInputE' style='display:none' disabled value='<%=result[i][3]%>' onblur="return checkScale(this)"></td>
			    <td class="normalText"><input id='cost_control<%=i%>' name='cost_control' class='textInputE' style='display:none' disabled value='<%=result[i][4]%>' onblur="return checkMoney(this)"></td>
			    <td class="normalText"><input id='cost_scale<%=i%>' name='cost_scale' class='textInputE' style='display:none' disabled value='<%=result[i][5]%>' onblur="return checkMark(this)"></td>
			  </tr>
      <%	}
	    }%>
			</html:table>
  	</td>
  </tr>
</html:table>
<input type="hidden" name="subFunction">
</form>
<Script Language="JavaScript">
  init();
</Script>
</html:html>