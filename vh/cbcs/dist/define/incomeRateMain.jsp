<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/define/incomeRateMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.*,
								 com.viewhigh.cbcs.base.mvc.view.component.*,
								 com.viewhigh.cbcs.base.sql.BaseRO,
								 java.text.*,
								 com.viewhigh.cbcs.cbcs.dist.base.*"%>
<%@ page import="com.viewhigh.cbcs.cbcs.util.DisplayWidth,com.viewhigh.cbcs.cbcs.dist.base.CommonData"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function find() {
	  template.subFunction.value='goIncomeRateMain';
		show_wait();
    template.submit();
    return true;
	}
  function create() {
    template.subFunction.value='goIncomeRateCreate';
		show_wait();
    template.submit();
    return true;
  }
  function update(){

    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'income_scale') {
        if (!checkScale(template.elements[i]))
        	return false;
      }
    }
    if (template!=null && template._old_current_page!=null) {
      template._current_page.value = template._old_current_page.value;
    }
   	template.subFunction.value='goIncomeRateUpdate';
		show_wait();
    template.submit();
    return true;
  }

</script>

<html:html clazz="main">
  <form name="template" method="post" action="distIncomeRate.jspviewhigh">
    <!-- 信息提示栏 -->
    <html:message/>
    <!-- 标题栏 -->
    <html:title clazz="module">收入比例</html:title>
    <!-- 简单信息 -->
    <html:table clazz="simple">
      <tr>
        <td class="normalText" width="40">　</td>
        <td nowrap class="signText" width="72">科室类别：</td>
        <td nowrap class="normalText" width="150">
          <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getDeptType(), "dept_clin_code", request.getParameter("dept_clin_code"), true, false)%>
        </td>
        <td nowrap class="signText" width="82">医疗项目类：</td>
        <td nowrap class="normalText">
          <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getChargeType(), "charge_clin_code", request.getParameter("charge_clin_code"), true, false)%>
        </td>
        <td width="100">
        <button class="pageBtn" name="" onclick="template.subFunction.value='goIncomeRateMain';show_wait();template.submit();">查询</button>
        <!--<img src="images/find.gif" style="cursor:hand" onclick="template.subFunction.value='goIncomeRateMain';show_wait();template.submit();"/>-->
        </td>
        <td class="normalText" width="40">　</td>
      </tr>
    </html:table>
    <table width="100%" border="0">
      <tr>
        <td class="tableTitle" nowrap="nowrap">收入比例设置表</td>
      </tr>
    </table>

<%BaseRO ro = (BaseRO)request.getAttribute("result");
	TableMarge oper = new TableMarge(ro, "find();");
	oper.addOptionButton("images/save.gif", "return update()");     //  保存
	oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	oper.addNeedButton("images/create.gif", "return create()");     //  添加	%>
    <!-- 复杂信息 -->
    <html:table clazz="complex">
      <!-- 操作 -->
      <tr>
        <td>
          <%= oper%>
        </td>
      </tr>
      <!-- 结果集 -->
      <tr>
        <td>
          <html:table clazz="result" divHeight='425'>
            <html:tr clazz="label">
              <td nowrap width=120>医疗项目类型编码</td>
              <td nowrap width=120>医疗项目类型名称</td>
              <td nowrap width=70>收入项目</td>
              <td nowrap width=70>科室类别</td>
              <td nowrap width=90>下限比例(%)</td>
				      <td nowrap width=90>金额限制</td>
				      <td nowrap width=90>上限比例(%)</td>
              <td nowrap width=100>备注</td>
            </html:tr>
<%if (ro!=null) {
	String[][] result = ro.getTableResult();
 	if (result!=null) {
		for (int i=0; i<result.length; i++) {
  		String primaryKey = result[ i ][ 0 ];
    	for(int j=0;j<result[i].length;j++){
    	if(result[i][j]==null||result[i][j].equals(""))
      	result[i][j]="&nbsp";
    }
    DecimalFormat percentFormat = new DecimalFormat("#0.00");
    String rowColor = "rowGray";
		if (i/2*2==i)
			rowColor = "rowWhite";%>
            <tr class="<%=rowColor%>">
              <td class="normalText">
                <%=  result[i][0]%>
                <input type="hidden" name="charge_kind_code" value="<%= result[i][0]%>"/>
              </td>
              <td class="normalText">
                <%=  result[i][1]%>
              </td>
              <td class="normalText">
                <%=  result[i][2]%>
              </td>
              <td class="normalText">
                <%=  result[i][4]%>
                <input type="hidden" name="dept_kind_code" value="<%= result[i][3]%>"/>
              </td>
              <td class="normalText">
							<input:text name='income_scale_down' type="number" numType="float"  dVal="<%= percentFormat.format(new Double(result[i][6]))%>" cssclass="textInputE" maxlength="14">
							</input:text>

              </td>
              <td class="normalText">
							<input:text name='income_limit' type="number" numType="float"  dVal="<%= percentFormat.format(new Double(result[i][5]))%>" cssclass="textInputE" maxlength="14">
								<input:textError errorCode="min" value="0">金额限制不应小于0</input:textError>
							</input:text>

              </td>
              <td class="normalText">
							<input:text name='income_scale_up' type="number" numType="float"  dVal="<%= percentFormat.format(new Double(result[i][7]))%>" cssclass="textInputE" maxlength="14">
							</input:text>
              </td>
              <td class="normalText">
                <input name="remark" type="text" maxlength="200" class="textInputC" value="<%= result[i][8]%>"/>
              </td>
            </tr>
<%	}
	}
}%>
          </html:table>
        </td>
      </tr>
    </html:table>
    <input type="hidden" name="subFunction"/>
  </form>
</html:html>
