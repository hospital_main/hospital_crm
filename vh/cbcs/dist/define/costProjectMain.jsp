<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/define/costProjectMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.cbcs.dist.base.*,
								 com.viewhigh.cbcs.base.sql.BaseRO"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function find() {
	  template.subFunction.value='goCostProjectMain';
		show_wait();
    template.submit();
    return true;
	}

  function create(){
		if (template.dept_kind_code.value == '') {
			alert("科室类别不能为空");
			return false
		}
    if (template.dept_kind_code.value != '<%=request.getParameter("dept_kind_code")%>') {
			alert("科室类别已经改变，请重新查询！");
			return false
		}
		template.subFunction.value='goCostProjectCreate';
		show_wait();
    template.submit();
    return true;
  }

  function update(){
		if (template.dept_kind_code.value != '<%=request.getParameter("dept_kind_code")%>') {
			alert("科室类别已经改变，请重新查询！");
			return false
		}
		if (template!=null && template._old_current_page!=null) {
			template._current_page.value = template._old_current_page.value;
		}
		template.subFunction.value='goCostProjectUpdate';
		show_wait();
    template.submit();
    return true;
  }

	function change(obj) {
		var names = obj.name.substring(0, obj.name.lastIndexOf("_"));
	
		var count = obj.name.substring(obj.name.lastIndexOf("_") + 1);
	
		var newObj = eval("template." +names + "[" + count + "]");
	
		if (obj.value == 1) {
			newObj.value = 0;
			obj.value = 0;
		}else {
			newObj.value = 1;
			obj.value = 1;
		}
	}
	function selectAll(){
		
    for (var i=0; i<template.elements.length; i++) {
    		//alert(template.elements[i].name);
        if (template.elements[i].name.indexOf('prime_cost_mark')!=-1){
            template.elements[i].checked = true;
            template.elements[i].value=1;
        }
         if (template.elements[i].name.indexOf('adm_cost_mark')!=-1){
            template.elements[i].checked = true;
            template.elements[i].value=1;
        }
         if (template.elements[i].name.indexOf('cost_s_mark')!=-1){
            template.elements[i].checked = true;
            template.elements[i].value=1;
        }
         if (template.elements[i].name.indexOf('cost_t_mark')!=-1){
            template.elements[i].checked = true;
            template.elements[i].value=1;
        }
         if (template.elements[i].name.indexOf('cost_f_mark')!=-1){
            template.elements[i].checked = true;
            template.elements[i].value=1;
        }
    }
    
  }

</script>

<%BaseRO ro = (BaseRO)request.getAttribute("result");
  TableMarge oper = new TableMarge(ro, "find();");
  oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
  oper.addOptionButton("images/save.gif", "return update()");     //  重置
  oper.addNeedButton("images/create.gif", "return create()");     //  添加
  oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
  %>

<html:html clazz="main">
  <form name="template" method="post" action="distCostProject.jspviewhigh">
    <!-- 信息提示栏 -->
    <html:message/>
    <!-- 标题栏 -->
    <html:title clazz="module">成本项目</html:title>
    <!-- 简单信息 -->
    <html:table clazz="simple">
      <tr>
        <td class="normalText" width="40">　</td>
        <td nowrap class="signText" width="72">科室类别：</td>
        <td nowrap class="normalText" >
          <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getDeptType(), "dept_kind_code", request.getParameter("dept_kind_code"), true, true)%>
        </td>
        <td nowrap width="100">
        <button class="pageBtn" name=""   onclick="find();"  >查询</button>
        <!-- <img src="images/find.gif" class="mouse" onclick="find();"/>-->
        </td>
        <td class="normalText" width="40">　</td>
      </tr>
    </html:table>
    <table width="100%" border="0">
      <tr>
        <td class="tableTitle" nowrap="nowrap">
<%=request.getParameter("dept_kind_code")==null?(CommonData.getDeptType()==null?"":CommonData.getDeptType()[0][1]):CommonData.getDeptTypeName(request.getParameter("dept_kind_code"))%>成本项目设置表</td>
      </tr>
    </table>
    <!-- 复杂信息 -->
    <html:table clazz="complex">
      <!-- 操作 -->
      <tr>
        <td>
          <%= oper%>
        </td>
      </tr>
      <!-- 结果集 -->
      <tr>
        <td>
          <html:table clazz="result">
            <html:tr clazz="label">
              <td>项目编号</td>
              <td>项目名称</td>
              <td>直接成本</td>
              <td>分摊公用成本</td>
              <td>分摊管理成本</td>
              <td>分摊医疗辅助成本</td>
              <td>分摊医疗技术成本</td>
            </html:tr>
            <input type="hidden" name="cost_subj_code" value=""/>
            <input type="hidden" name="prime_cost_mark" value=""/>
            <input type="hidden" name="adm_cost_mark" value=""/>
            <input type="hidden" name="cost_s_mark" value=""/>
            <input type="hidden" name="cost_t_mark" value=""/>
            <input type="hidden" name="cost_f_mark" value=""/>
<%if (ro!=null) {
	String[][] result = ro.getTableResult();
	if (result!=null) {
		for (int i=0; i<result.length; i++) {
			String primaryKey = result[ i ][ 0 ];
			for(int j=0;j<result[i].length;j++){
				if(result[i][j]==null||result[i][j].equals(""))
					result[i][j]="&nbsp";
			}

			String rowColor = "rowGray";
			if (i/2*2==i)
				rowColor = "rowWhite";%>
            <tr class="<%=rowColor%>">
              <td class="normalText">
                <%=  result[i][1]%>
                <input type="hidden" name="cost_subj_code" value="<%= result[i][1]%>"/>
              </td>
              <td class="normalText">
                <%=  result[i][2]%>
              </td>
              <td class="normalText">
                <input name="prime_cost_mark_<%= i+1%>" type="checkbox" value="<%=(result[i][3].equals("true")?"1":"0")%>" <%=(result[i][3].equals("true")?"checked":"")%> onClick="change(this)"/>
                <input type="hidden" name="prime_cost_mark" value="<%=(result[i][3].equals("true")?"1":"0")%>"/>
              </td>
              <td class="normalText">
                <input name="adm_cost_mark_<%= i+1%>" type="checkbox" value="<%=(result[i][4].equals("true")?"1":"0")%>"  <%=(result[i][4].equals("true")?"checked":"")%> onClick="change(this)"/>
                <input type="hidden" name="adm_cost_mark" value="<%=(result[i][4].equals("true")?"1":"0")%>"/>
              </td>
              <td class="normalText">
                <input name="cost_s_mark_<%= i+1%>" type="checkbox" value="<%=(result[i][5].equals("true")?"1":"0")%>"  <%=(result[i][5].equals("true")?"checked":"")%> onClick="change(this)"/>
                <input type="hidden" name="cost_s_mark" value="<%=(result[i][5].equals("true")?"1":"0")%>"/>
              </td>
              <td class="normalText">
                <input name="cost_t_mark_<%= i+1%>" type="checkbox" value="<%=(result[i][6].equals("true")?"1":"0")%>"  <%=(result[i][6].equals("true")?"checked":"")%> onClick="change(this)"/>
                <input type="hidden" name="cost_t_mark" value="<%=(result[i][6].equals("true")?"1":"0")%>"/>
              </td>
              <td class="normalText">
                <input name="cost_f_mark_<%= i+1%>" type="checkbox" value="<%=(result[i][7].equals("true")?"1":"0")%>" <%=(result[i][7].equals("true")?"checked":"")%> onClick="change(this)"/>
                <input type="hidden" name="cost_f_mark" value="<%=(result[i][7].equals("true")?"1":"0")%>"/>
              </td>
            </tr>
<% 		}
		}
	}
%>
          </html:table>
        </td>
      </tr>
    </html:table>
    <input type="hidden" name="subFunction"/>
  </form>
</html:html>
