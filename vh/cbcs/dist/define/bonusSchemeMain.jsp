<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/define/bonusSchemeMain.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 java.text.*,
                 com.viewhigh.cbcs.cbcs.dist.base.*,
                 com.viewhigh.cbcs.base.sql.BaseRO" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function find() {
	  template.subFunction.value='goBonusSchemeMain';
		show_wait();
    template.submit();
    return true;
	}
  function selectAll(){
    for (var i = 0; i < template.elements.length; i++){
      if (template.elements[i].name == 'primaryKey'){
        if(template.elements[i].disabled == false){
          template.elements[i].checked = true;
        }
      }
    }
  }
	function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='goBonusSchemeRemove';
					show_wait();
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function set(){
    template.subFunction.value='goBonusSchemeSet';
		show_wait();
    template.submit();
    return true;
  }
</script>
<%
	BaseRO ro = (BaseRO)request.getAttribute("result");
  TableMarge oper = new TableMarge(ro, "find();");
  oper.addNeedButton("images/selectedAll.gif", "return selectAll()");     //  全选
  oper.addNeedButton("images/reset.gif", "return reset()");     //  重置
  oper.addNeedButton("images/remove.gif", "return remove()");     //  删除
  oper.addNeedButton("images/set.gif", "return set()");     //  配置
%>
<html:html clazz="main">
<form name="template" method="post" action="distBonusScheme.jspviewhigh">
<!-- 信息提示栏 -->
<html:message/>

<!-- 标题栏 -->
<html:title clazz='module'>科室奖金分配方案</html:title>
<!-- 简单信息 -->
    <html:table clazz="simple">
      <tr>
      	<td class="normalText" width="40">　</td>
        <td nowrap class="signText" width="72">分配科室：</td>
        <td nowrap class="normalText">
          <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getBigDept(), "dept_clin_code", request.getParameter("dept_clin_code"), true, false)%>
        </td>
        <td nowrap class="signText" width="72">奖金项目：</td>
        <td nowrap class="normalText">
          <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(CommonData.getBonusItem(), "bonus_code", request.getParameter("bonus_code"), true, false)%>
        </td>
        <td width="100">
        <button class="pageBtn" name=""  onclick="template.subFunction.value='goBonusSchemeMain';show_wait();template.submit();">查询</button>
        <!--<img src="images/find.gif" style="cursor:hand" onclick="template.subFunction.value='goBonusSchemeMain';show_wait();template.submit();"/>-->
        </td>
      	<td class="normalText" width="40">　</td>
      </tr>
    </html:table>

  <!-- 复杂信息 -->
  <html:table clazz="complex">

	 	<!-- 操作 -->
   	<tr><td><%=oper%></td></tr>

		<!-- 结果集 -->
		<tr>
    	<td>
				<html:table clazz="result">
				  <tr>
			      <td nowrap class="resultLabel">选择</td>
				    <td nowrap class="resultLabel">分配科室编码</td>
				    <td nowrap class="resultLabel">分配科室名称</td>
				    <td nowrap class="resultLabel">奖金项目编码</td>
				    <td nowrap class="resultLabel">奖金项目名称</td>
				    <td nowrap class="resultLabel">提取比例</td>
				    <td nowrap class="resultLabel">成本调控额</td>
				    <td nowrap class="resultLabel">成本比例</td>
				  </tr>
<%if (ro!=null) {
		String[][] result = ro.getTableResult();
		if (result!=null) {
			for (int i=0; i<result.length; i++) {
				String primaryKey = result[ i ][ 0 ];
				String rowColor = "rowGray";
				if (i/2*2==i)
					rowColor = "rowWhite";%>
				  <tr class="<%=rowColor%>">
						<td>
		  				<input name="primaryKey" type="checkbox" value="<%=result[i][0]+"&"+result[i][2]%>"/>
						</td>
				    <td class="normalText"><A HREF="distBonusScheme.jspviewhigh?subFunction=goBonusSchemeFind&dept_clin_code=<%=result[i][0]%>"><%= result[i][0]%></A></td>
				    <td class="normalText"><%= result[i][1]%></td>
				    <td class="normalText"><%= result[i][2]%></td>
				    <td class="normalText"><%= result[i][3]%></td>
				    <td class="numberText"><%= result[i][4]%></td>
				    <td class="numberText"><%= result[i][5]%></td>
				    <td class="numberText"><%= result[i][6]%></td>
				  </tr>
<%		}
		}
	}%>
				</html:table>
    	</td>
    </tr>
  </html:table>
<input type="hidden" name="subFunction">
</form>
</html:html>