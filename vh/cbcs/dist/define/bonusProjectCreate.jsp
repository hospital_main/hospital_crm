<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/dist/define/bonusProjectCreate.jsp,v 1.1 2012/03/12 01:58:11 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:58:11 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create(){
    template.subFunction.value='goBonusProjectCreate';
    if(template.bonus_name.value==""){ 
			alert("项目名称不能为空");
			return;
		}
    if(isTooLong(template.bonus_formula,255)){
      alert('公式太长!');
      return false;
    }
    if(isTooLong(template.remark,255)){
      alert('说明太长!');
      return false;
    }
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back(element) {
    template.subFunction.value='goBonusProjectMain';
		show_wait();
    element.submit();
  }

  function change() {
 	  if (template.bonus_type.options[template.bonus_type.selectedIndex].value=='9') {
	    eval('hide11').style.display='none'
	    eval('hide12').style.display='none'
	  } else if (template.bonus_type.options[template.bonus_type.selectedIndex].value=='4') {
	    eval('hide11').style.display=''
	    eval('hide12').style.display=''
    }
  }
</script>
<!-- 信息提示栏 -->

<html:html clazz="main">

<form name="template" method="post" action="distBonusProject.jspviewhigh">
<html:message/>

  <!-- 标题栏 -->
  <html:title clazz='module'>奖金项目</html:title>

<br>

<!-- 简单信息 -->
<html:table clazz="simple">
  <tr>
    <td class="signText" nowrap="nowrap">奖金项目名称：</td>
    <td class="normalText" nowrap="nowrap"><input type="text" name="bonus_name" size="20" maxlength="20"></td>
  </tr>
  <tr>
 		<td class="signText" nowrap="nowrap">奖金项目类别：</td>
    <td width="75%" class="normalText" nowrap="nowrap">
      <select type="select" name="bonus_type" onchange="change();">
        <option value="4">项目奖金</option>
        <option value="9">其他奖金</option>
      </select>
    </td>
  </tr>
  <tr>
    <td class="signText" nowrap="nowrap" id='hide11'>医疗项目类：</td>
    <td class="normalText" nowrap="nowrap" id='hide12'>
      <%= new com.viewhigh.cbcs.base.mvc.view.component.Select(CommonData.getChargeType(), "charge_kind_code", request.getParameter("charge_kind_code"), true, true)%>
    </td>
  </tr>
  <tr>
    <td class="signText" nowrap="nowrap">公 式：</td>
    <td class="normalText" nowrap="nowrap">
    	<textarea name='bonus_formula' rows='2' cols='40' maxlength="20"></textarea>
    </td>
  </tr>
  <tr>
    <td class="signText" nowrap="nowrap">说 明：</td>
    <td class="normalText" nowrap="nowrap">
    	<textarea name='remark' rows='2' cols='40' maxlength="20"></textarea>
    </td>
  </tr>
  <tr>
    <td colspan="2"><button class="pageBtn" onclick="return create();" >添加</button>    	
    	<button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button> 
    	<!--<img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" />
    	<img src="images/return.gif" class="mouse" onclick="return back(template);" />-->
    </td>
  </tr>
</html:table>
<input type="hidden" name="subFunction" >
<input type="hidden" name="income_subj_code_copy" >
</form>
</html:html>
