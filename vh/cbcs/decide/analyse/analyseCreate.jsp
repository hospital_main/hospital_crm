<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/decide/analyse/analyseCreate.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
   function account() {
    if(isEmpty(template.invest_code))
    {
      alert('方案代码不能为空!');
      template.invest_code.focus();
      return;
    }
    if(isEmpty(template.invest_name))
    {
      alert('方案名称不能为空!');
      template.invest_name.focus();
      return;
    }
    if(isEmpty(template.length)||template.length.value==0)
    {
      alert('请添加数据细则!');
      return;
    }
    if(!(template.length.value >= Math.ceil(template.plan_recycle.value)))
    {
      alert('添加年度小于计划回收期，请补充完整!');
      return;
    }    
    template.subFunction.value='goAnalyseAccount';
    show_wait();
    template.submit();
    return true;
  }
  function find(){
    template.subFunction.value = "goAnalyseMain";
    show_wait();
    template.submit();
  }
</Script>
<%String date=getDate();%>
<html:html clazz="main" scrollCtl='true'>
<form name="template" method="post" action="decideAnalyse.jspviewhigh">
	<!-- 返回信息栏 -->
  <html:message/>
 <!-- 标题栏 -->
	<html:title clazz='module'>投资决策方案添加 </html:title>

  <!-- 查询项 -->
  <html:table clazz="simple">
  	<tr>
  		<td class="normalText" width="40">　</td>
    	<td class="signText" >方案代码:</td>
    	<td class="normalText">
      	<input:text name="invest_code" getLastValue="false" maxlength="20"></input:text>
    	</td>
    	<td class="signText" >初始投资(元)：</td>
    	<td class="normalText">
      	<input:text name="start_amount" getLastValue="false" type="number" numType="float" maxlength="14">
      	  <input:textError errorCode="min" value="-100000000000">初始投资不应小于-100000000000</input:textError>
		      <input:textError errorCode="max" value="100000000000">初始投资不应大于100000000000</input:textError>
      	</input:text>
    	</td>
  	</tr>
  	<tr>
  		<td class="normalText" width="40">　</td>
    	<td class="signText" >方案名称:</td>
    	<td class="normalText">
      	<input:text name="invest_name" getLastValue="false" maxlength="20"></input:text>
    	</td>
    	<td class="signText" > 计划回收期(年)：</td>
    	<td class="normalText">
      	<input:text name="plan_recycle" getLastValue="false" type="number" numType="float" maxlength="7"></input:text>
    	</td>
    	<td class="normalText" width="20">　</td>
  	</tr>
   	<tr>
  		<td class="normalText" width="40">　</td>
    	<td class="signText" >制定日期：</td>
    	<td class="normalText">
       <%=date%><input type='hidden' name="opdate" value="<%=date%>"/>
    	</td>
    	<td class="signText" > 制定人：</td>
    	<td class="normalText">
      <%=getUser_name(request)%><input type='hidden' name="framer" value="<%=getUser_name(request)%>"/>
    	</td>
    	<td class="normalText" width="20">　</td>
  	</tr>
  </html:table>

  <br>
  <html:table clazz="complex" >
    <tr>
      <td align="left">
				<button class="pageBtn" onclick="return createTable('table','hidden');" >添加</button>
       	<!-- <img src="images/create.gif" class="mouse" onclick="return createTable('table','hidden');"/>
        <img src="images/remove.gif" class="mouse" onclick="return deleteTable('table','hidden');"/>-->
      	<button class="pageBtn" onclick="return deleteTable('table','hidden');" >删除</button>
      </td>
      <td align="right"><button class="pageBtn" onclick="return account();">计算</button>
      </td>
    <tr>
  </html:table>
 	<table  width='100%' border='1' bgColor='white' borderColor='black' style='BORDER-COLLAPSE:collapse;font-family:\"宋体\"; font-size:10pt' id="table" >
  	<html:tr clazz='label'>
    	<td class="resultLabel">选择</td>
      <td class="resultLabel">年度</td>
      <td class="resultLabel">业务收入(元)</td>
      <td class="resultLabel">成本(元)</td>
      <td class="resultLabel">折旧(元)</td>
      <td class="resultLabel">净现金流量(元)</td>
      <td class="resultLabel">现值系数</td>
      <td class="resultLabel">尚未收回的投资额(元)</td>
      <td class="resultLabel">现值(元)</td>
    </html:tr>
	</table>
  <br>
  <br>
 <table width='100%' border='1' bgColor='white' borderColor='black' style='BORDER-COLLAPSE:collapse;font-family:\"宋体\"; font-size:10pt' >
 		<tr char="rowWhite">
  		<td class="normalText">未来报酬总数:</td>
    	<td class="numberText">元</td>
    	<td class="normalText">初始投资:</td>
    	<td class="numberText" width="100">元</td>
    	<td class="normalText">计划回收期：</td>
    	<td class="numberText">年</td>
  	</tr>
  	<tr char="rowWhite">
  		<td class="normalText" rowspan="3">分析结果:</td>
    	<td class="normalText">净现值</td>
    	<td class="numberText">元</td>
    	<td class="normalText"></td>
    	<td class="normalText" colspan="2">原则：净现值＞0可以采纳</td>
  	</tr>
  	<tr char="rowWhite">
  		<td class="normalText">回收期</td>
    	<td class="numberText">年</td>
    	<td class="normalText"></td>
    	<td class="normalText" colspan="2">原则：回收期≤计划回收期，可以采纳；反之，不可以采纳</td>
  	</tr>
  	<tr char="rowWhite">
  		<td class="normalText">现值指数</td>
    	<td class="numberText"></td>
    	<td class="normalText"></td>
    	<td class="normalText" colspan="2">原则：现值指数＞1可以采纳</td>
  	</tr>
	</table>
  <br>
  <br>
  <html:table clazz="complex">
    <tr>
      <td align="right"><button class="pageBtn" onclick="find()">返回</button>
        <!--<img src="images/return1.gif" class="mouse" onclick="find()"/>-->
       </td>
    <tr>
  </html:table>
   <input type='hidden' name="subFunction"/>
   <input type='hidden' name="length" value="0" id="hidden"/>
</form>
</html:html>
