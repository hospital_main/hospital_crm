<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/decide/analyse/analyseMain.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create() {
    template.subFunction.value='goAnalysePreparedCreate';
    show_wait();
    template.submit();
    return true;
  }
  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
      if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
        flag = true;
    }
    if( flag!=false) {
      if (confirm('是否删除')) {
        template.subFunction.value='goAnalyseRemove';
        show_wait();
        template.submit();
        return true;
      } else
          return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }
  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
      if (template.elements[i].name == 'primaryKey'
        && template.elements[i].disabled == false) {
        template.elements[i].checked = true;
    	}
    }
  }
  function find(){
    template.subFunction.value = "goAnalyseMain";
    show_wait();
    template.submit();
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="decideAnalyse.jspviewhigh">
	<!-- 返回信息栏 -->
	<html:message/>

  <!-- 标题栏 -->
	<html:title clazz='module'>投资决策分析</html:title>
  <!-- 查询项 -->
	<html:table clazz="simple">
		<tr>
   		<td class="normalText" width="40">　</td>
   		<td nowrap class="signText" width="72">时间：</td>
   		<td><input:date name="date" ></input:date></td>
   		<td nowrap class="signText" width="72">排序：</td>
   		<td>
       	<input type="checkbox" name="taxis1" value="1" <%if(request.getParameter("taxis1")!=null&&request.getParameter("taxis1").equals("1")) out.print("checked");%>>回收期
       	<input type="checkbox" name="taxis2" value="2" <%if(request.getParameter("taxis2")!=null&&request.getParameter("taxis2").equals("2")) out.print("checked");%>>净现值
       	<input type="checkbox" name="taxis3" value="3" <%if(request.getParameter("taxis3")!=null&&request.getParameter("taxis3").equals("3")) out.print("checked");%>>现值指数
   		</td>
      <td class="normalText" width="20">　</td>
    </tr>
		<tr>
    	<td class="normalText" width="40">　</td>
	    <td nowrap class="signText" width="72">方案代码:</td>
  	  <td><input:text name="code" maxlength="20"></input:text></td>
    	<td nowrap class="signText" width="72">方案名称:</td>
	    <td><input:text name="name" maxlength="20"></input:text></td>
	    <td><button class="pageBtn" onclick="return find();">查询</button></td>
      <td class="normalText" width="20">　</td>
    </tr>
  </html:table>

  <br>

  <!-- 查询结果 -->
  <html:title clazz='table'>投资方案</html:title>
	<%
	  BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	  if(ro!=null){
	  TableMarge oper = new TableMarge(ro, "return find()");
	  oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
	  oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	  oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	  oper.addNeedButton("images/create.gif", "return create()");     //  添加
	%>

	<html:table clazz="complex">
  <!-- 操作 -->
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  	<tr>
    	<td>
      	<html:table clazz="result">
      		<html:tr clazz='label'>
        		<td class="resultLabel">选择</td>
        		<td class="resultLabel">制定时间</td>
        		<td class="resultLabel">方案代码</td>
        		<td class="resultLabel">方案名称</td>
        		<td class="resultLabel">制定人</td>
        		<td class="resultLabel">初始投资(元)</td>
        		<td class="resultLabel">计划回收期(年)</td>
        		<td class="resultLabel">预测回收期(年)</td>
        		<td class="resultLabel">净现值(元)</td>
        		<td class="resultLabel">现值指数</td>
        		<td class="resultLabel">是否采纳</td>
        	</html:tr>
        <%
          String[][] result = ro.getTableResult();
          if (result != null) {
            for (int i = 0; i < result.length; i++ ) {
		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
        %>
	      	<tr CLASS="<%=rowColor%>">
          	<td><input type="checkbox" name="primaryKey" value="<%=result[i][1]%>"></td>
          	<td class="normalText"><%=changeDateFormat(result[i][0])%></td>
          	<td class="normalText"><a href="decideAnalyse.jspviewhigh?subFunction=goAnalysePreparedSave&&primaryKey=<%=result[i][1]%>"><%=result[i][1]%></a></td>
          	<td class="normalText"><%=result[i][2]%></td>
          	<td class="normalText"><%=result[i][3]%></td>
          	<td class="numberText"><%=changeFormat(result[i][4])%></td>
          	<td class="numberText"><%=changeFormat(result[i][5])%></td>
          	<td class="numberText"><%=changeFormat(result[i][6])%></td>
          	<td class="numberText"><%=changeFormat(result[i][7])%></td>
          	<td class="numberText"><%=changeFormat(result[i][8],"##0.00")%></td>
          	<td class="normalText"><%=result[i][9]%></td>
	       </tr>
		    <%
		          }
		        }
		    %>
      </html:table>
     </td>
   </tr>
	</html:table>
<%}%>
  <input type=hidden name="subFunction"/>
</form>
</html:html>
