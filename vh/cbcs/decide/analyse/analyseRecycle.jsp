<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/decide/analyse/analyseRecycle.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript">
  function find(){
    history.back();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="decideAnalyse.jspviewhigh">
	<!-- 返回信息栏 -->
  <html:message/>
 <!-- 标题栏 -->
	<center><html:title clazz='table'>回收期法投资决策分析 </html:title></center>
   <%
			String[] result2 = (String[])request.getSession().getAttribute("result2");
    %>
  <!-- 查询项 -->
  <html:table clazz="simple">
  	<tr>
  		<td class="normalText" width="40">　</td>
    	<td class="signText" >方案代码:</td>
    	<td class="normalText">
      	<%if(result2!=null&&result2[0]!=null) out.print(result2[0]);%>
    	</td>
    	<td class="signText" >方案名称:</td>
    	<td class="normalText">
    	  <%
    	    String invest_name = ""; 
    	    if(request.getParameter("invest_name") == null) {
	    	    if(result2!=null&&result2[1]!=null) 
	    	      invest_name = result2[1];
	    	  } else {
	    	    invest_name = request.getParameter("invest_name");
	    	  }
    	  %>
      	<%= invest_name%>
    	</td>
  	</tr>
   	<tr>
  		<td class="normalText" width="40">　</td>
    	<td class="signText" >制定日期：</td>
    	<td class="normalText">
        <%
    	    String opdate = ""; 
    	    if(result2!=null&&result2[2]!=null) 
    	      opdate = changeDateFormat(result2[2]);
    	  %>
        <%= opdate%>
    	</td>
    	<td class="signText" > 制定人：</td>
    	<td class="normalText">
        <%
    	    String framer = ""; 
    	    if(result2!=null&&result2[3]!=null) 
    	      framer = result2[3];
    	  %>
        <%= framer%>
    	</td>
    	<td class="normalText" width="20">　</td>
  	</tr>
  </html:table>
  <table width='100%' border='1' borderColor='white'>
    <tr>
      <td class="normalText">        
    	  <%
    	    String start_amount = ""; 
    	    if(result2!=null&&result2[4]!=null) 
    	      start_amount = changeFormat(result2[4],"##0.00");
    	  %>初始投资额：<%= start_amount%>元</td>
    </tr>
	</table>  
 	<table  width='100%' border='1' bgColor='white' borderColor='black' style='BORDER-COLLAPSE:collapse;font-family:\"宋体\"; font-size:10pt' id="table" >
    <html:tr clazz='label'>
      <td class="resultLabel">年度</td>
      <td class="resultLabel">每年净现值流量(元)</td>
      <td class="resultLabel">年末尚未收回投资额(元)</td>
    </html:tr>
    <%
      String[][] result = (String[][])request.getSession().getAttribute("result"); 
      if(result!=null){
        for (int i = 0; i < result.length; i++ ) {
          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
    %>
     <tr CLASS="<%=rowColor%>">
       <td class="titleText"><%=result[i][0]%></td>
       <td class="numberText"><%=changeFormat(result[i][4])%></td>
       <td class="numberText"><%=changeFormat(result[i][6])%></td>
     </tr>
    <%
          }
    }
    %>
    <%
			String[] result1 = (String[])request.getSession().getAttribute("result1");
    %>
    <tr>
      <td colspan="2" class="normalText">计划回收期(年)</td>
      <td class="numberText"><%if(result1!=null&&result1[2]!=null) out.print(changeFormat(result1[2]));%></td>
    <tr>
    <tr>
      <td colspan="2" class="normalText">预测回收期(年)</td>
      <td class="numberText"><%if(result1!=null&&result1[3]!=null) out.print(changeFormat(result1[3]));%></td>
    <tr>
	</table>
  <br>
  <html:table clazz="complex">
    <tr>
      <td align="right"><button class="pageBtn" onclick="find()">返回</button>
       </td>
    <tr>
  </html:table>
   <input type='hidden' name="subFunction"/>
   <input type='hidden' name="length" value="0" id="hidden"/>
</form>
</html:html>

