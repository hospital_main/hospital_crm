<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/decide/analyse/analysePrint.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>
<%@ page import="com.viewhigh.cbcs.base.util.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function preparedPrin() {
     // 报表名称
    grid.prn.title1='投资决策方案';
    // 年月
    grid.prn.title2="";
    // 表头行数
    grid.prn.tabHead=0;
    // 打印
    grid.print();
    history.back(-1);
  }
  function find(){
    template.subFunction.value = "goAnalyseMain";
    show_wait();
    template.submit();
    return true;
  }
</Script>
<html xmlns:fc>
<head>
  <title>登录用户：<%=getUser_name(request)%></title>
  <meta http-equiv='Content-Type' content='text/html; charset=GBK'>
  <link rel='stylesheet' href='vh.css' type='text/css'>
  <link rel='stylesheet' href='newVh.css' type='text/css'>
  <style>fc\:webprint {behavior: url(webprint/webgrid.htc);}</style>
  <script language='javascript' src='javascript/common.js'></script>
  <script language='javascript1.2'>
    function window_onload() {
      grid.width=840
      grid.height=448
      grid.FixRows=2
      grid.FixCols=3
      grid.prn.printFixCols=1

      //调整行高和列高
      grid.AdjustRowHeight=false;
      grid.AdjustColWidth=false;

      grid.ReadOnly=true;

      //设置每次请求页面时刷新缓存
      grid.prn.changeUserDate=true;

      //表头行数
      grid.prn.tabHead=1;

      //是否套打
      grid.prn.coverPrint= false;

      //纸张大小 428（W）*299（H）单位：毫米(mm)
      grid.prn.repWidth='<%=Preference.getWidth()%>mm';
      grid.prn.repHeight='<%=Preference.getHeight()%>mm';
      //上下左边距
      grid.prn.tabTop='<%=Preference.getTop()%>';
      grid.prn.tabBottom='<%=Preference.getBottom()%>';
      grid.prn.tabLeft='<%=Preference.getLeft()%>';
      grid.prn.tabRight='<%=Preference.getRight()%>';
      //是否横向打印
      grid.prn.level='<%=Preference.getTransverse()%>';
      grid.prn.portrait='<%=!Preference.getTransverse()%>';
      //是否自动转行
      grid.prn.AutoturnRow=false;
      //宽度超宽时压缩到一页上
      //grid.prn.compressWidth=1;
      //表格超宽超高压缩时压缩表格字体
      //grid.prn.compressFont=0;
      //inprint值为0时表示打印时先进行预览
      grid.prn.inPrint=0;
      //设置标题与附加信息
      grid.prn.top21='编制单位：'+'<%=Preference.getCom_name()%>';
      grid.prn.top22='';
      grid.prn.top13='页号：(p)/(P)';
      grid.prn.top33='金额单位：元';
      grid.prn.bottom13='.';
      grid.prn.bottom23='打印日期：'+'<%=ExtendTool.getTime().toString().substring(0, 19)%>'+'      制表人：'+'<%=LoginUser.getUser_name(request)%>';
      grid.prn.bottom33='北京东软望海科技有限公司 制作';
      grid.prn.fontTop='30';
      grid.prn.fontBottom='12';
      if (parent._1st_current!=null) parent.show();
    }
  </script>
</head>
<body bgcolor='#FFFFFF' rightMargin='0' onload='window_onload();preparedPrin();'>
<img id='_image' src='img/loading.gif' style='display:none'/>
<div id='_process'>
<form name="template" method="post" action="decideAnalyse.jspviewhigh">

	<!-- 返回信息栏 -->
  <html:message/>
 <!-- 标题栏 -->
	<html:title clazz='module'>投资决策方案</html:title>
   <%
			String[] result2 = (String[])request.getSession().getAttribute("result2");
    %>
  <fc:webprint id="grid" >
  <html:webprint clazz='new' >
 	  <colgroup id='tg'>
      <col style = 'width:103'>
      <col style = 'width:103'>
      <col style = 'width:103'>
      <col style = 'width:103'>
      <col style = 'width:103'>
      <col style = 'width:103'>
      <col style = 'width:103'>
      <col style = 'width:103'>
    </colgroup>
  	<tr>
    	<td style="text-align:center;height:30;border-color:#FFFFFF" >方案代码:</td>
    	<td style="text-align:right;height:30;border-color:#FFFFFF" > </td>
      <td style="text-align:right;height:30;border-color:#FFFFFF"><%if(result2!=null&&result2[0]!=null) out.print(result2[0]);%></td>
      <td style="text-align:right;height:30;border-color:#FFFFFF" > </td>
      <td align="left" bordercolor="#FFFFFF" height="30">初始投资额(元):</td>
      <td style="text-align:left;height:30;border-color:#FFFFFF" > </td>
      <td style="text-align:right;height:30;border-color:#FFFFFF">
      <%
  	    String start_amount = "";
  	    if(result2!=null&&result2[4]!=null)
  	      start_amount = changeFormat(result2[4]);
  	  %><%= start_amount%></td>
      <td style="text-align:left;height:30;border-color:#FFFFFF" > </td>
    </tr>
  	<tr>
    	<td style="text-align:center;height:30;border-color:#FFFFFF">方案名称:</td>
      <td style="text-align:right;height:30;border-color:#FFFFFF" > </td>
      <td style="text-align:right;height:30;border-color:#FFFFFF">
      <%
  	    String invest_name = "";
  	    if(result2!=null&&result2[1]!=null)
    	      invest_name = result2[1];
  	  %><%= invest_name%></td>
      <td style="text-align:right;height:30;border-color:#FFFFFF" > </td>
      <td align="left" bordercolor="#FFFFFF" height="30">计划回收期(年):</td>
      <td style="text-align:left;height:30;border-color:#FFFFFF"> </td>
      <td style="text-align:right;height:30;border-color:#FFFFFF">
      <%
  	    String plan_recycle = "";
  	    if(result2!=null&&result2[5]!=null)
  	      plan_recycle = changeFormat(result2[5]);
  	  %><%= plan_recycle%></td>
      <td style="text-align:left;height:30" bordercolor="#FFFFFF"></td>
    </tr>
    <tr>
    	<td align="center" bordercolor="#FFFFFF" height="30">制定日期:</td>
      <td style="text-align:right;height:30;border-color:#FFFFFF" > </td>
      <td align="right" bordercolor="#FFFFFF" height="30">
      <%
  	    String opdate = "";
  	    if(result2!=null&&result2[2]!=null)
  	      opdate = changeDateFormat(result2[2]);
  	  %><%= opdate%></td>
      <td style="text-align:right;height:30;border-color:#FFFFFF" > </td>
      <td align="left" bordercolor="#FFFFFF" height="30">制订人:</td>
      <td align="left" bordercolor="#FFFFFF" height="30"> </td>
      <td style="text-align:right;height:30" bordercolor="#FFFFFF">
      <%
  	    String framer = "";
  	    if(result2!=null&&result2[3]!=null)
  	      framer = result2[3];
  	  %><%= framer%></td>
      <td style="text-align:left;height:30" bordercolor="#FFFFFF"> </td>
    </tr>
  	<tr>
    	<td colspan="8" align="center"></td>
    </tr>
  	<tr style='font-family:"宋体";  font-size:14px; text-decoration:none; color:#000000; background:#F6D7AB; text-align:center; vertical-align:bottom;'>
    	<td style="text-align:center;height:30">年度</td>
      <td style="text-align:center;height:30">业务收入(元)</td>
      <td style="text-align:center;height:30">成本(元)</td>
      <td style="text-align:center;height:30">折旧(元)</td>
      <td style="text-align:center;height:30">净现金流量(元)</td>
      <td style="text-align:center;height:30">现值系数</td>
      <td style="text-align:center;height:30">尚未收回的投资额(元)</td>
      <td style="text-align:center;height:30">现值(元)</td>
    </tr>
    <%String[][] result = (String[][])request.getSession().getAttribute("result");
      if(result!=null){
        for (int i = 0; i < result.length; i++ ) {
    %>
    <tr>
    	<td align="center" height="30"><%=result[i][0]%></td>
      <td style="text-align:right;height:30"><%=changeFormat(result[i][1])%></td>
      <td style="text-align:right;height:30"><%=changeFormat(result[i][2])%></td>
      <td style="text-align:right;height:30"><%=changeFormat(result[i][3])%></td>
      <td style="text-align:right;height:30"><%=changeFormat(result[i][4])%></td>
      <td style="text-align:right;height:30"><%=changeFormat(result[i][5])%></td>
      <td style="text-align:right;height:30"><%=changeFormat(result[i][6])%></td>
      <td style="text-align:right;height:30"><%=changeFormat(result[i][7])%></td>
    </tr>
    <%}
    }%>
    <%
			String[] result1 = (String[])request.getSession().getAttribute("result1");
    %>
    <tr>
    	<td align="center" colspan="8" height="30"> </td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align:center;height:30">未来报酬总数:</td>
      <td align="center" colspan="6" height="30"><%if(result1!=null&&result1[0]!=null) out.print(changeFormat(result1[0]));%>元</td>
    </tr>
    <tr>
    	<td align="center" rowspan="3" height="30">分析结果:</td>
      <td align="center" height="30">净现值</td>
      <td style="text-align:right;height:30"><%if(result1!=null&&result1[4]!=null) out.print(changeFormat(result1[4]));%>元</td>
    	<td colspan="2" align="center" height="30"><%if(result1!=null&&result1[4]!=null){out.print((Float.parseFloat(result1[4])>0)?"可以采纳":"不可采纳");}%></td>
      <td align="left" colspan="3" height="30">原则：净现值＞0可以采纳</td>
      </tr>
    <tr>
      <td align="center" height="30">回收期</td>
    	<td class="numberText"><%if(result1!=null&&result1[3]!=null&&"9999999".equals(result1[3])){ out.print("大于"+ result.length); } else { out.print(changeFormat(result1[3]));}%>年</td>
    	<td colspan="2" align="center" height="30"><%if(result1!=null&&result1[3]!=null){out.print((Float.parseFloat(result1[3])<=Float.parseFloat(result1[2]))?"可以采纳":"不可采纳");}%></td>
      <td align="left" colspan="3" height="30">原则：回收期≤计划回收期，可以采纳；反之，不可以采纳</td>
    </tr>
    <tr>
      <td align="center" height="30">现值指数</td>
      <td style="text-align:right;height:30"><%if(result1!=null&&result1[5]!=null) out.print(changeFormat(result1[5]));%></td>
    	<td colspan="2" align="center" height="30"><%if(result1!=null&&result1[5]!=null){out.print((Float.parseFloat(result1[5])>1)?"可以采纳":"不可采纳");}%></td>
      <td align="left" colspan="3" height="30">原则：现值指数＞1可以采纳</td>
      </tr>
    <tr>
    	<td align="center" colspan="2" height="30">是否采纳:</td>
      <td align="center" colspan="6" height="30"><%=(request.getParameter("accept")!=null)?"是":"否"%></td>
    </tr>
  </html:webprint>
  </fc:webprint>
</form>
</div>
</body>
</html>