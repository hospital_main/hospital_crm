<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/decide/analyse/analyseSave.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create() {
    if(isEmpty(template.invest_code))
    {
      alert('方案代码不能为空!');
      template.invest_code.focus();
      return;
    }
    if(isEmpty(template.invest_name))
    {
      alert('方案名称不能为空!');
      template.invest_name.focus();
      return;
    }
    if (confirm('修改数据必须计算后才能生效，确认保存吗？')) {
	    template.subFunction.value = "goAnalyseSave";
	    show_wait();
	    template.submit();
      return true;
    } else
        return false;
  }
  function account() {
    if(isEmpty(template.invest_code))
    {
      alert('方案代码不能为空!');
      template.invest_code.focus();
      return;
    }
    if(isEmpty(template.invest_name))
    {
      alert('方案名称不能为空!');
      template.invest_name.focus();
      return;
    }
    if(isEmpty(template.length)||template.length.value==0)
    {
      alert('请添加数据细则!');
      return;
    }
    if(!(template.length.value >= Math.ceil(template.plan_recycle.value)))
    {
      alert('添加年度小于计划回收期，请补充完整!');
      return;
    }    
    template.subFunction.value='goAnalyseSaveAccount';
    show_wait();
    template.submit();
    return true;
  }
  function find(){
    template.subFunction.value = "goAnalyseMain";
    show_wait();
    template.submit();
    return true;
  }
  function print(){
    template.subFunction.value = "goAnalysePrint";
    show_wait();
    template.submit();
    return true;
  }
</Script>
<%String date=getDate();%>
<html:html clazz="main" scrollCtl='true'>
<form name="template" method="post" action="decideAnalyse.jspviewhigh">
	<!-- 返回信息栏 -->
  <html:message/>
 <!-- 标题栏 -->
	<html:title clazz='module'>投资决策方案修改 </html:title>
   <%
			String[] result2 = (String[])request.getAttribute("result2");
			request.getSession().setAttribute("result2",result2);
    %>
  <!-- 查询项 -->
  <html:table clazz="simple">
  	<tr>
  		<td class="normalText" width="40">　</td>
    	<td class="signText" >方案代码:</td>
    	<td class="normalText">
      	<%if(result2!=null&&result2[0]!=null) out.print(result2[0]);%><input type='hidden' name="invest_code" value="<%if(result2!=null&&result2[0]!=null) out.print(result2[0]);%>"/>
    	</td>
    	<td class="signText" >初始投资(元)：</td>
    	<td class="normalText">
    	  <%
    	    String start_amount = "";
    	    if(result2!=null&&result2[4]!=null)
    	      start_amount = changeFormat(result2[4],"##0.00");
    	  %>
      	<input:text name="start_amount" dVal='<%= start_amount%>' type="number" numType="float" maxlength="14">
      	  <input:textError errorCode="min" value="-100000000000">初始投资不应小于-100000000000</input:textError>
		      <input:textError errorCode="max" value="100000000000">初始投资不应大于100000000000</input:textError>
      	</input:text>
    	</td>
  	</tr>
  	<tr>
  		<td class="normalText" width="40">　</td>
    	<td class="signText" >方案名称:</td>
    	<td class="normalText">
    	  <%
    	    String invest_name = "";
    	    if(request.getParameter("invest_name") == null) {
	    	    if(result2!=null&&result2[1]!=null)
	    	      invest_name = result2[1];
	    	  } else {
	    	    invest_name = request.getParameter("invest_name");
	    	  }
    	  %>
      	<input:text name="invest_name" dVal="<%= invest_name%>" maxlength="20"></input:text>
    	</td>
    	<td class="signText" > 计划回收期(年)：</td>
    	<td class="normalText">
    	  <%
    	    String plan_recycle = "";
    	    if(result2!=null&&result2[5]!=null)
    	      plan_recycle = changeFormat(result2[5],"##0.00");
    	  %>
      	<input:text name="plan_recycle" dVal='<%= plan_recycle%>' type="number" numType="float" maxlength="7"></input:text>
    	</td>
    	<td class="normalText" width="20">　</td>
  	</tr>
   	<tr>
  		<td class="normalText" width="40">　</td>
    	<td class="signText" >制定日期：</td>
    	<td class="normalText">
        <%
    	    String opdate = "";
    	    if(result2!=null&&result2[2]!=null)
    	      opdate = changeDateFormat(result2[2]);
    	  %>
        <%= opdate%><input type='hidden' name="opdate" value="<%= opdate%>"/>
    	</td>
    	<td class="signText" > 制定人：</td>
    	<td class="normalText">
        <%
    	    String framer = "";
    	    if(result2!=null&&result2[3]!=null)
    	      framer = result2[3];
    	  %>
        <%= framer%><input type='hidden' name="framer" value="<%= framer%>"/>
    	</td>
    	<td class="normalText" width="20">　</td>
  	</tr>
  </html:table>

  <br>
  <html:table clazz="complex" >
    <tr>
      <td align="left"><button class="pageBtn" onclick="return createTable('table','hidden');" >添加</button>
       <!-- <img src="images/create.gif" class="mouse" onclick="return createTable('table','hidden');"/>
        <img src="images/remove.gif" class="mouse" onclick="return deleteTable('table','hidden');"/>-->
      	<button src="images/remove.gif" class="pageBtn" onclick="return deleteTable('table','hidden');" >删除</button>
      </td>
      <td align="right"><button class="pageBtn" onclick="return account();">计算</button>
        <button class="pageBtn" onclick="return print();">打印</button>
      </td>
    <tr>
  </html:table>
 	<table  width='100%' border='1' bgColor='white' borderColor='black' style='BORDER-COLLAPSE:collapse;font-family:\"宋体\"; font-size:10pt' id="table" >
  	<html:tr clazz='label'>
    	<td class="resultLabel">选择</td>
      <td class="resultLabel">年度</td>
      <td class="resultLabel">业务收入(元)</td>
      <td class="resultLabel">成本(元)</td>
      <td class="resultLabel">折旧(元)</td>
      <td class="resultLabel">净现金流量(元)</td>
      <td class="resultLabel">现值系数</td>
      <td class="resultLabel">尚未收回的投资额(元)</td>
      <td class="resultLabel">现值(元)</td>
    </html:tr>
    <%String[][] result = (String[][])request.getAttribute("result");
      if(result!=null){
			  request.getSession().setAttribute("result",result);
        for (int i = 0; i < result.length; i++ ) {
    %>
    <tr>
    	<td align="center"><input type="checkbox"></td>
      <td class="numberlText" align="center"><%=result[i][0]%></td>
      <td class="numberlText" align="center">
      	<input:text name='<%="income"+(i+1)%>'  dVal='<%=changeFormat(result[i][1],"##0.00")%>' type="number" numType="float"  cssclass="textInputA" maxlength="12">
        </input:text><div style="display:none"><%=changeFormat(result[i][1],"##0.00")%></div>
      </td>
      <td class="numberlText" align="center">
      	<input:text name='<%="cost"+(i+1)%>'  dVal='<%=changeFormat(result[i][2],"##0.00")%>' type="number" numType="float"  cssclass="textInputA" maxlength="12">
      </input:text><div style="display:none"><%=changeFormat(result[i][2],"##0.00")%></div>
      </td>
      <td class="numberlText" align="center">
      	<input:text name='<%="deprec"+(i+1)%>'  dVal='<%=changeFormat(result[i][3],"##0.00")%>' type="number" numType="float"  cssclass="textInputA" maxlength="12">
        </input:text><div style="display:none"><%=changeFormat(result[i][3],"##0.00")%></div>
      </td>
      <td class="numberlText" align="right"><%=changeFormat(result[i][4])%></td>
      <td class="numberlText" align="center">
      	<input:text name='<%="quot"+(i+1)%>'  dVal='<%=changeFormat(result[i][5],"##0.00")%>' type="number" numType="float"  cssclass="textInputA" maxlength="6">
          <input:textError errorCode="min" value="0">现值系数不应小于0</input:textError>
	        <input:textError errorCode="max" value="100">现值系数不应大于100</input:textError>
        </input:text><div style="display:none"><%=changeFormat(result[i][5],"##0.00")%></div>
      </td>
      <td class="numberlText" align="right"><%=changeFormat(result[i][6])%></td>
      <td class="numberlText" align="right"><%=changeFormat(result[i][7])%></td>
    </tr>
   <%}
   }%>
	</table>
  <br>
  <br>
 <table width='100%' border='1' bgColor='white' borderColor='black' style='BORDER-COLLAPSE:collapse;font-family:\"宋体\"; font-size:10pt' >
   <%
			String[] result1 = (String[])request.getAttribute("result1");
			request.getSession().setAttribute("result1",result1);
    %>
 		<tr char="rowWhite">
  		<td class="normalText">未来报酬总数:</td>
    	<td class="numberText"><%if(result1!=null&&result1[0]!=null) out.print(changeFormat(result1[0]));%>元</td>
    	<td class="normalText">初始投资:</td>
    	<td class="numberText" width="100"><%if(result1!=null&&result1[1]!=null) out.print(changeFormat(result1[1]));%>元</td>
    	<td class="normalText">计划回收期：</td>
    	<td class="numberText"><%if(result1!=null&&result1[2]!=null) out.print(changeFormat(result1[2]));%>年</td>
  	</tr>
  	<tr char="rowWhite">
  		<td class="normalText" rowspan="3">分析结果:</td>
    	<td class="normalText"><a href="decideAnalyse.jspviewhigh?subFunction=goAnalyseShowValue">净现值</a></td>
    	<td class="numberText"><%if(result1!=null&&result1[4]!=null) out.print(changeFormat(result1[4]));%>元</td>
    	<td class="numberText"><%if(result1!=null&&result1[4]!=null){out.print((Float.parseFloat(result1[4])>0)?"可以采纳":"不可采纳");}%></td>
    	<td class="normalText" colspan="2">原则：净现值＞0可以采纳</td>
  	</tr>
  	<tr char="rowWhite">
  		<td class="normalText"><a href="decideAnalyse.jspviewhigh?subFunction=goAnalyseShowRecycle">回收期</a></td>
    	<td class="numberText"><%if(result1!=null&&result1[3]!=null&&"9999999".equals(result1[3])){ out.print("大于"+ result.length); } else { out.print(changeFormat(result1[3]));}%>年</td>
    	<td class="numberText"><%if(result1!=null&&result1[3]!=null){out.print((Float.parseFloat(result1[3])<=Float.parseFloat(result1[2]))?"可以采纳":"不可采纳");}%></td>
    	<td class="normalText" colspan="2">原则：回收期≤计划回收期，可以采纳；反之，不可以采纳</td>
  	</tr>
  	<tr char="rowWhite">
  		<td class="normalText"><a href="decideAnalyse.jspviewhigh?subFunction=goAnalyseShowRate">现值指数</a></td>
    	<td class="numberText"><%if(result1!=null&&result1[5]!=null) out.print(changeFormat(result1[5]));%></td>
    	<td class="numberText"><%if(result1!=null&&result1[5]!=null){out.print((Float.parseFloat(result1[5])>1)?"可以采纳":"不可采纳");}%></td>
    	<td class="normalText" colspan="2">原则：现值指数＞1可以采纳</td>
  	</tr>
  	<tr char="rowWhite">
  		<td class="normalText">是否采纳:</td>
  		<%
    	    String check = "";
    	    if(request.getParameter("flag")==null) {
	    	    if(result2!=null&&result2[9]!=null) {
	    	      if(result2[9].equalsIgnoreCase("Y")) {
	    	        check = "checked";
	    	      }
	    	    }
	    	  } else {
	    	    if(request.getParameter("accept") != null) {
	    	    	check = "checked";
	    	    }
	    	  }
    	  %>
    	<td class="normalText" colspan="5"><input type="checkbox" name="accept" value="Y" <%= check%> ></td>
  	</tr>
	</table>
  <br>
  <br>
  <html:table clazz="complex">
    <tr>
      <td align="right">
        <button class="pageBtn" onclick="return create();">保存</button>
        <button class="pageBtn" onclick="find()">返回</button>
       </td>
    <tr>
  </html:table>
   <input type='hidden' name="subFunction"/>
   <input type='hidden' name="flag" value="1"/>
   <input type='hidden' name="length" id="hidden" value="<%if(result!=null) out.print(result.length);%>"/>
</form>
</html:html>