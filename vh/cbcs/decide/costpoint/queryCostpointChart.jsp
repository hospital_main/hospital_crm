<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/decide/costpoint/queryCostpointChart.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK"%>

<%@taglib uri='/WEB-INF/taglib/cewolf.tld' prefix='cewolf' %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@page import="com.viewhigh.cbcs.base.chart.*"%>
<%
	request.setCharacterEncoding("GBK");
  String dept = request.getParameter("dept");
  String dateType = request.getParameter("dateType");
  String dateValue = request.getParameter("dateValue");
 
  
  String perIncome = request.getParameter("perIncome");
  String perChangeCost = request.getParameter("perChangeCost");
  String fixedCost = request.getParameter("fixedCost");
	
	if ("month".equals(dateType)) {
	  dateValue = dateValue.substring(0,4)+"年"+dateValue.substring(4)+"月";
	} else if ("quarter".equals(dateType)) {
	  dateValue = dateValue.substring(0,4)+"年第"+dateValue.substring(4)+"季";
	} else if ("year".equals(dateType)) {
	  dateValue = dateValue.substring(0,4)+"年";
	}
%>
<script language='javascript' src='/vh/javascript/chart.js'></script>
<Script Language="JavaScript">
	var cewolf1;
  cewolf1 = "";
</Script>
<html>

<table>

<tr>
	<td align="center" >
	<%
  	double[] pointer = ChartUtilities.createChartLine("cewolf1", new String[]{"业务收入", "总成本", "固定成本"}, new String[] {perIncome, perChangeCost, fixedCost}, pageContext);  	
  	String temp = dept+dateValue+"本量利分析图";
	%>
		<cewolf:chart id="chart" title="<%=temp%>" type="xy" xaxislabel="<%=dept%>" yaxislabel="元" labelXY="<%=pointer%>">
		  <cewolf:colorpaint color="#EEEEFF"/>
		  <cewolf:data>
		     <cewolf:producer id="cewolf1"/>
		  </cewolf:data>
		</cewolf:chart>
		<cewolf:img chartid="chart" renderer="/cewolf" width="710" height="490" id="cewolf1" />
	</td>
</tr>		
</html>