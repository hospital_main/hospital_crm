<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/decide/costpoint/queryCostpointDeptMain.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
 function judgeDate(){
    if(template.type.value=="month"){
      if(template.yearMonth.value==""){
        alert("请选择核算时间");
        return false;
      }
    }
    else if(template.type.value=="quarter"){
      if(template.yearQuarter.value==""){
        alert("请选择核算时间");
        return false;
      }
    }
    else{
      if(template.year.value==""){
        alert("请选择核算时间");
        return false;
      }
    }
 }
  function find(){
   if(judgeDate()==false){
     return false;
   }
    template.subFunction.value = "goDeptQueryCostpointMain";
    show_wait();
    template.submit();
  }
  function chart(url) {
		window.open(url,'','width=750,height=570, top=100, left=100, scrollbars = 1, resizable = 1');
  }

</Script>
<%
	  String[][] result = (String[][])request.getAttribute("result");

	%>
<html:html clazz="main" fixRows="1" isPrint="true">
<form name="template" method="post" action="decideCostpointDeptQuery.jspviewhigh">
	<!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
	<html:title clazz='module'>住院科室保本点预测(医成本V1-01表)</html:title>

  <!-- 查询项 -->
  <html:table clazz="simple">
  	<tr>
    	<td class="normalText" width="40">　</td>
    	<td class="signText" width="72">分析单位:</td>
    	<td class="normalText">
       <%String[][] s1={{"month","月"},
                        {"quarter","季"},
                        {"year","年"}
                       };%>
       <input:vanish name="type" options="<%=s1%>" cssclass="selectBg">
    	</td >
    	<td nowrap class="signText" width="72">核算时间:</td>
    	<td>
      	<input:vanishEle >
        	<input:yearM name="yearMonth"></input:yearM>
        </input:vanishEle>
        <input:vanishEle >
        	<input:yearQ name="yearQuarter" ></input:yearQ>
        </input:vanishEle>
        <input:vanishEle >
        	<input:year name="year" ></input:year>
        </input:vanishEle>
       </input:vanish>
    	</td>
			<td nowrap width="100"><button class="pageBtn" onclick="return find();">计算</button></td>
    	<%if(result!=null){%>
      <td nowrap width="100"><button class="pageBtn" onclick="return preparedPrint();">打印</button></td>
      <%}%>
    	<td class="normalText" width="20">　</td>
  	</tr>
  </html:table>

  <br>

  <!-- 查询结果 -->
  <html:title clazz='table'>住院科室床日成本保本点</html:title>

      <html:webprint clazz='new' >
        <colgroup id='tg'>
        	<col style = <%=DisplayWidth.NAME_WIDTH%>>
        	<col style = 'width:120px'>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
        	<col style = 'width:120px'>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
        	<col style = 'width:120px'>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
        </colgroup>
        <html:tr clazz='label'>
        	<td class="resultLabel" nowrap>住院科室</td>
        	<td class="resultLabel" nowrap>实际床日数<br>(床日)</td>
        	<td class="resultLabel" nowrap>总收入<br>(元)</td>
        	<td class="resultLabel" nowrap>单位收入<br>(元)</td>
        	<td class="resultLabel" nowrap>总成本<br>(元)</td>
        	<td class="resultLabel" nowrap>单位变动成本<br>(元)</td>
        	<td class="resultLabel" nowrap>固定成本<br>(元)</td>
        	<td class="resultLabel" nowrap>保本床日数<br>(床日)</td>
        	<td class="resultLabel" nowrap>保本收入<br>(元)</td>
        </html:tr>
        </html:webprint>
        <% if(result!=null){%>
          <div style='border:1px solid #000000;overflow:auto;height:378px'>
            <table border='1' bgColor=white borderColor=black style='border:2px solid #000000;BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
             <colgroup id='tg'>
        	<col style = <%=DisplayWidth.NAME_WIDTH%>>
        	<col style = 'width:120px'>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
        	<col style = 'width:120px'>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
        	<col style = 'width:120px'>
        	<col style = <%=DisplayWidth.MONEY_WIDTH%>>
        </colgroup>
         <%
            for (int i = 0; i < result.length; i++ ) {
		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
              boolean sign=false;
              if(Integer.parseInt(result[i][1])<Integer.parseInt(result[i][7]))
                   sign=true;
              else sign=false;
              boolean flag=false;
              if(Double.parseDouble(result[i][3])<Double.parseDouble(result[i][5]))
                   flag=true;
              else flag=false;
        %>
	      <tr  class='<%=rowColor%>'>
        	<td class="normalText" nowrap style="text-align:left"><a href="#" onclick=chart("cbcs/decide/costpoint/queryCostpointChart.jsp?dept=<%= result[i][0]%>&type=O&dateType=<%=request.getAttribute("type")%>&dateValue=<%=request.getAttribute("dateValue")%>&perIncome=<%=result[i][3]%>&perChangeCost=<%= result[i][5]%>&fixedCost=<%= result[i][6]%>")><%if(sign==true||flag==true ) out.print("<span style='color: red'>");%><%=result[i][0]%><%if(sign==true||flag==true ) out.print("</span>");%></td>
          <td nowrap class="numberText"><%if(sign==true||flag==true ) out.print("<span style='color: red'>");%><%=result[i][1]%><%if(sign==true||flag==true ) out.print("</span>");%></td>
          <td nowrap class="numberText"><%if(sign==true||flag==true ) out.print("<span style='color: red'>");%><%=changeFormat(result[i][2])%><%if(sign==true||flag==true ) out.print("</span>");%></td>
          <td nowrap class="numberText"><%if(sign==true||flag==true ) out.print("<span style='color: red'>");%><%=changeFormat(result[i][3])%><%if(sign==true||flag==true ) out.print("</span>");%></td>
          <td nowrap class="numberText"><%if(sign==true||flag==true ) out.print("<span style='color: red'>");%><%=changeFormat(result[i][4])%><%if(sign==true||flag==true ) out.print("</span>");%></td>
          <td nowrap class="numberText"><%if(sign==true||flag==true ) out.print("<span style='color: red'>");%><%=changeFormat(result[i][5])%><%if(sign==true||flag==true ) out.print("</span>");%></td>
          <td nowrap class="numberText"><%if(sign==true||flag==true ) out.print("<span style='color: red'>");%><%=changeFormat(result[i][6])%><%if(sign==true||flag==true ) out.print("</span>");%></td>
          <td nowrap class="numberText"><%if(sign==true||flag==true ) out.print("<span style='color: red'>");if(flag==true){%>单位变动成本过高<%} else {%><%="0".equals(result[i][7])?"0":changeFormat(result[i][7],"#,##0")%><%}if(sign==true||flag==true ) out.print("</span>");%></td>
          <td nowrap class="numberText"><%if(sign==true||flag==true ) out.print("<span style='color: red'>");if(flag==true){%>&nbsp;<%} else {%><%=changeFormat(result[i][8])%><%}if(sign==true||flag==true ) out.print("</span>");%></td>
	      </tr>
		    <%
		          }
        %>
         </table>
        </div>
        <%
          }
		    %>

  <input type='hidden' name="subFunction"/>

</form>
</html:html>
