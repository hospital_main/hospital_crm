<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/decide/query/diagnoseQueryMain.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
function find(){
    if(template.yearM.value==""){
      alert("请选择核算月");
      return false;
    }
    template.subFunction.value = "goQueryDiagnoseMain";
    show_wait();
    template.submit();
  }
</Script>
<%
	    String[][] result = (String[][])request.getAttribute("result");
%>
<html:html clazz="main" isPrint="true" fixRows="1">
<form name="template" method="post" action="decideQueryDiagnose.jspviewhigh">
	<!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
	<html:title clazz='module'>门诊科室诊次成本查询</html:title>
  <!-- 查询项 -->
  <html:table clazz="simple">
  	<tr>
    	<td class="normalText" width="40">　</td>
    	<td class="signText" width="72">核算月：</td>
    	<td class="normalText">
      	<input:yearM name="yearM" ></input:yearM>
    	</td >
    	<td nowrap width="100"><button class="pageBtn" onclick="return find();">计算</button></td>
    	<%if(result!=null) {%>
      <td nowrap width="100"><button class="pageBtn" onclick="return preparedPrint();">打印</button></td>
      <%}%>
    	<td class="normalText" width="40">　</td>
  	</tr>
  </html:table>

  <br>

  <!-- 查询结果 -->
  <html:title clazz='table'>门诊科室诊次成本表</html:title>

      <html:webprint clazz='new'>
        <colgroup id='tg'>
      		<col style ='width:215px'>
      		<col style ='width:200px'>
      		<col style ='width:200px'>
      		<col style ='width:200px'>
    		</colgroup>
        <html:tr clazz='label'>
        	<td class="resultLabel" nowrap>门诊科室</td>
        	<td class="resultLabel" nowrap>总成本<br>(元)</td>
        	<td class="resultLabel" nowrap>门急诊人次<br>(诊次)</td>
       	  <td class="resultLabel" nowrap>平均诊次成本<br>(元)</td>
        </html:tr>
       </html:webprint>
        <% if(result!=null){%>
        <div style='border:1px solid #000000;overflow:auto;height:378px'>
          <table border='1' bgColor=white borderColor=black style='border:2px solid #000000;BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
            <colgroup id='tg'>
      		    <col style ='width:215px'>
      		    <col style ='width:200px'>
      		    <col style ='width:200px'>
      		    <col style ='width:200px'>
    		    </colgroup>
        <%   for (int i = 0; i < result.length; i++ ) {
		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
        %>
	      <tr CLASS="<%=rowColor%>">
        	<td class="normalText" nowrap style="text-align:left"><%=result[i][0]%></td>
          <td nowrap="nowrap" class="numberText"><%=changeFormat(result[i][1])%></td>
          <td nowrap="nowrap" class="numberText"><%=result[i][2]%></td>
          <td nowrap="nowrap" class="numberText"><%=changeFormat(result[i][3])%></td>
	      </tr>
		    <%
		          }
        %>
        </table>
      </div>
        <%
        }
		    %>

		<input type='hidden' name="subFunction"/>
	</form>
</html:html>
