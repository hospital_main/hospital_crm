<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/decide/collect/collectquery.jsp,v 1.2 2013/03/04 03:45:10 yuchengying Exp $
 $Author: yuchengying $
 $Date: 2013/03/04 03:45:10 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.2 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
function back() {
  	template.subFunction.value='return';
    show_wait();
  	template.submit();
  	return true;
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="decideCollect.jspviewhigh">
  <%
    String yearmonth=request.getParameter("yearMonth");
    String type=request.getParameter("kind");
  %>
	<html:title clazz='module'><%=yearmonth.substring(0,4)%>年<%=String.valueOf(Integer.parseInt(yearmonth.substring(4,6)))%>月<%if(type.trim().equalsIgnoreCase("I")){%>住院<%}else{%>门诊<%}%>科室计划数据查询 </html:title>
  <html:table clazz="complex">
  	<tr>
    	<td align="left"><button class="pageBtn" onclick="return back()">返回</button> 
      	<!--<img src="images/return.gif" class="mouse" onclick="return back()"/>-->
      </td>
      <td class="normalText" width="20"></td>
    <tr>
  </html:table>
 <%
	  String[][] result = (String[][])request.getAttribute("result");

	%>
  <!-- 结果集 -->
  <html:table clazz="complex">
  	<tr>
    	<td>
      	<html:table clazz="result">
        	<html:tr clazz='label'>
          	<td class="resultLabel" >科室名称</td>
          	<td class="resultLabel" >固定成本<br>(元)</td>
          	<td class="resultLabel" >工作量<br>(<%if(type.trim().equalsIgnoreCase("I")){%>床日<%}else{%>诊次<%}%>)</td>
            <td class="resultLabel" >变动成本<br>(元)</td>
          	<td class="resultLabel" >总收入<br>(元)</td>
            <td class="resultLabel" >单位变动成本<br>(元)</td>
            <td class="resultLabel" >单位收入<br>(元)</td>
            <td class="resultLabel" >总收益<br>(元)</td>
        	</html:tr>
        <% if(result!=null){
            for (int i = 0; i < result.length; i++ ) {
		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
        %>
	      	<tr CLASS="<%=rowColor%>">
          	<td class="normalText"><%=result[i][0]%></td>
           	<td class="numberText">
              <%=changeFormat(result[i][1])%>
            </td>
            <td class="numberText">
           		<%=changeFormat(result[i][2])%>
            </td>
            <td class="numberText">
           		<%=changeFormat(result[i][3])%>
            </td>
            <td class="numberText">
              <%=changeFormat(result[i][4])%>
            </td>
            <td class="numberText">
              <%=changeFormat(result[i][5])%>
            </td>
            <td class="numberText">
              <%=changeFormat(result[i][6])%>
            </td>
            <td class="numberText">
              <%=changeFormat(result[i][7])%>
            </td>
	        </tr>
		    <%
		          }
        }
		    %>
       </html:table>
     </td>
   </tr>
	</html:table>
<input type='hidden' name="kind" value="<%=request.getParameter("kind")%>"/>
<input type='hidden' name="yearMonth" value="<%=yearmonth%>"/>
<input type='hidden' name="subFunction"/>
</form>
</html:html>
