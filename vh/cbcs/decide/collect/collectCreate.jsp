<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/decide/collect/collectCreate.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create() {
    for(var i=0;i<template.elements.length;i++)
    template.elements[i].value="";
    template.subFunction.value='create';
    show_wait();
    template.submit();
    return true;
  }
function back() {
for(var i=0;i<template.elements.length;i++)
    template.elements[i].value="";
  	template.subFunction.value='return';
    show_wait();
  	template.submit();
  	return true;
  }
</Script>
<html:html clazz="main">
  <form name="template" method="post" action="decideCollect.jspviewhigh">
  <!-- 返回信息栏 -->
  <html:message/>
  <!-- 标题栏 -->
  <%String yearmonth=request.getParameter("yearMonth");
  String type=request.getParameter("kind");%>
	<html:title clazz='module'><%if(yearmonth!=null) out.print(yearmonth.substring(0,4));%>年<%if(yearmonth!=null)out.print(String.valueOf(Integer.parseInt(yearmonth.substring(4,6))));%>月<%if(type.trim().equalsIgnoreCase("I")){%>住院<%}else{%>门诊<%}%>科室计划数据添加</html:title>
  <html:table clazz="complex">
  	<tr>
    	<td align="left">
    	<button class="pageBtn" onclick="return create()">保存</button>
    	<button class="pageBtn" onclick="return back();">返回</button>  
      	<!--<img src="images/save.gif" class="mouse" onclick="return create()"/>
        <img src="images/return.gif" class="mouse" onclick="return back()"/>-->
      </td>
      <td class="normalText" width="20"></td>
    <tr>
  </html:table>
 <%
	  String[][] result = (String[][])request.getAttribute("result");

	%>

	<html:table clazz="complex">
  <!-- 结果集 -->
  	<tr>
    	<td>
      	<html:table clazz="result">
        	<html:tr clazz='label'>
          	<td class="resultLabel" >科室名称</td>
          	<td class="resultLabel" >固定成本<br>(元)</td>
          	<td class="resultLabel" >变动成本<br>(元)</td>
          	<td class="resultLabel" >工作量<br>(<%if(type.trim().equalsIgnoreCase("I")){%>床日<%}else{%>诊次<%}%>)</td>
          	<td class="resultLabel" >总收入<br>(元)</td>
        	</html:tr>
        <%if(result!=null){
            for (int i = 0; i < result.length; i++ ) {
		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
        %>
	      	<tr CLASS="<%=rowColor%>">
          	<td nowrap="nowrap" class="normalText"><%=result[i][0]%></td>
            <td align='center'>
           		<input:text name='<%="root"+i%>' type="number" numType="float"  dVal="<%=result[i][1]%>" cssclass="textInputA" maxlength="14">
	              <input:textError errorCode="min" value="-100000000000">固定成本不应小于-100000000000</input:textError>
		            <input:textError errorCode="max" value="100000000000">固定成本不应大于100000000000</input:textError>
              </input:text>
            </td>
           	<td align='center'>
           		<input:text name='<%="change"+i%>' type="number" numType="float"  dVal="<%=result[i][2]%>" cssclass="textInputA" maxlength="14">
                <input:textError errorCode="min" value="-100000000000">变动成本不应小于-100000000000</input:textError>
		            <input:textError errorCode="max" value="100000000000">变动成本不应大于100000000000</input:textError>
              </input:text>
            </td>
            <td align='center'>
            	<input:text name='<%="workload"+i%>' type="number" numType="int"  dVal="<%=result[i][3]%>" cssclass="textInputA" maxlength="9">
              </input:text>
            </td>
            <td align='center'>
            	<input:text name='<%="income"+i%>' type="number" numType="float"  dVal="<%=result[i][4]%>" cssclass="textInputA" maxlength="14">
                <input:textError errorCode="min" value="-100000000000">总收入不应小于-100000000000</input:textError>
		            <input:textError errorCode="max" value="100000000000">总收入不应大于100000000000</input:textError>
              </input:text>
            </td>
	        </tr>
		    <%
		          }
        }
		    %>
      </html:table>
    </td>
  </tr>
</html:table>
  <%if(result!=null){
            for (int i = 0; i < result.length; i++ ) {
  %>
    <input type='hidden' name='<%="code"+i%>' value="<%=result[i][5]%>"/>
    <input type='hidden' name='<%="name"+i%>' value="<%=result[i][0]%>"/>
  <%}}%>
  <input type='hidden' name="kind" value="<%=request.getParameter("kind")%>"/>
  <input type='hidden' name="yearMonth" value="<%=yearmonth%>"/>
  <input type='hidden' name="length" value='<%if(result!=null) out.print(result.length);%>'/>
  <input type='hidden' name="subFunction"/>
</form>
</html:html>
