<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/decide/collect/collectMain.jsp,v 1.1 2012/03/12 01:57:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:35 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create() {
    if(template.yearMonth.value==""){
      alert("请选择计划时间");
      return false;
    }
    template.subFunction.value='preparedcreate';
    show_wait();
    template.submit();
    return true;
  }
  function save() {
    if(template.yearMonth.value==""){
      alert("请选择计划时间");
      return false;
    }
    template.subFunction.value='preparedsave';
    show_wait();
    template.submit();
    return true;
  }
  function remove() {
    if(template.yearMonth.value==""){
      alert("请选择计划时间");
      return false;
    }
    if (confirm('是否删除')) {
	    template.subFunction.value = "remove";
	    show_wait();
	    template.submit();
      return true;
    } else
        return false;
  }
function find(){
    if(template.yearMonth.value==""){
      alert("请选择计划时间");
      return false;
    }
    template.subFunction.value = "findByCondition";
    show_wait();
    template.submit();
  }
</Script>

<html:html clazz="main">
<form name="template" method="post" action="decideCollect.jspviewhigh">
	<!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
	<html:title clazz='module'> 收入成本收益计划维护 </html:title>
  <!-- 查询项 -->
  <html:table clazz="simple">
  	<tr>
    	
    	<td class="signText" width="240" align="right">计划时间：</td>
   	  <td class="normalText" align="left" ><input:yearM name="yearMonth"></input:yearM></td>
    	<td nowrap class="signText" width="72">科室属性：</td>
    	<td>
      	<input type="radio" style= "border:0pt; " name="kind" value="I" <%if(request.getParameter("kind")==null||(request.getParameter("kind")!=null&&request.getParameter("kind").equals("I"))) out.print("checked");%> >住院
        <input type="radio" style= "border:0pt; " name="kind" value="O" <%if(request.getParameter("kind")!=null&&request.getParameter("kind").equals("O")) out.print("checked");%>>门诊
      </td>
      <!--td>
    	<button class="pageBtn" onclick="return find();">查询</button>
      	<button class="pageBtn" onclick="return create();">添加</button>
       	<button class="pageBtn" onclick="return save();">修改</button>
   	<!--	<img src="images/add.gif" class="mouse" onclick="return create();"/>
      	<img src="images/update1.gif" class="mouse" onclick="return save();"/>>
      	<button class="pageBtn" onclick="return remove();">删除</button>
    	</td-->
    	<td class="normalText" width="20">　</td>
    </tr>
    <tr>
	    <td>
	    <button class="pageBtn" onclick="return create();">添加</button>
       	<button class="pageBtn" onclick="return save();">修改</button>
       	<button class="pageBtn" onclick="return remove();">删除</button>
	    </td>
	    <td align="right" colspan="4"><button class="pageBtn" onclick="return find();" >查询</button></td>
    </tr>
   <input type='hidden' name="subFunction"/>
  </html:table>
</form>
</html:html>
