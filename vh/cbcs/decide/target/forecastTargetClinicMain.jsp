<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/decide/target/forecastTargetClinicMain.jsp,v 1.1 2012/03/12 01:57:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:46 $
 $Modtime: 01-22-04 11:58 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge" %>
<%@ page import="java.util.*" %>
<%@ page import="com.viewhigh.cbcs.cbcs.util.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-input.tld" prefix="input"%>
<Script Language="JavaScript" src="javascript/input.js" ></Script>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
 function judgeDate(){
     if(template.type.value=="month"){
      if(template.beginYearM.value==""){
        alert("请选择基期");
        return false;
      }
      if(template.endYearM.value==""){
        alert("请选择计划期");
        return false;
      }
    }
    else if(template.type.value=="quarter"){
      if(template.beginYearQ.value==""){
        alert("请选择基期");
        return false;
      }
      if(template.endYearQ.value==""){
        alert("请选择计划期");
        return false;
      }
    }
    else{
      if(template.beginYear.value==""){
        alert("请选择基期");
        return false;
      }
      if(template.endYear.value==""){
        alert("请选择计划期");
        return false;
      }
    }
  }
 function find(){
   if(judgeDate()==false){
      return false;
    }
    template.subFunction.value = "goClinicForecastTargetMain";
    template.submit();
  }
  function preparedPrint() {
    // 报表名称
    grid.prn.title1='目标预测分析';
    // 年月
    grid.prn.title2 ='<%=judgeTitlePeriod()%>';
    // 表头行数
    grid.prn.tabHead = 2;
    // 打印
    grid.print();
  }
</Script>
<%
	  String[][] result = (String[][])request.getAttribute("result");
	%>
<html:html clazz="main" isPrint="true" fixRows="2" scrollCtl='true'>
<form name="template" method="post" action="decideTargetClinicForecast.jspviewhigh">
	<!-- 返回信息栏 -->
  <html:message/>

  <!-- 标题栏 -->
	<html:title clazz='module'>门诊科室目标预测分析(医成本V2-02表)</html:title>
  <!-- 查询项 -->
  <html:table clazz="simple">
    <tr>
      <td class="normalText" width="40">　</td>
      <td nowrap class="signText" width="72">分析单位：</td>
      <td>
           <%String[][] s={{"month","月"},
                           {"quarter","季"},
                           {"year","年"}
                           };%>

        <input:vanish name="type" options="<%=s%>" cssclass="selectBg">
       </td>
       <td colspan="2" style="text-align:center">
         <input:vanishEle >
           <table>
             <tr>
               <td nowrap class="signText">基期:</td><td><input:yearM name="beginYearM"></input:yearM></td>
               <td>&nbsp;&nbsp;</td>
               <td nowrap class="signText">计划期:</td><td><input:yearM name="endYearM"></input:yearM></td>
             </tr>
           </table>
          </input:vanishEle>
          <input:vanishEle >
            <table>
              <tr>
                <td nowrap class="signText">基期:</td><td><input:yearQ name="beginYearQ"></input:yearQ></td>
                <td>&nbsp;&nbsp;</td>
                <td nowrap class="signText">计划期:</td><td><input:yearQ name="endYearQ"></input:yearQ></td>
              </tr>
            </table>
          </input:vanishEle>
          <input:vanishEle >
            <table>
              <tr>
                <td nowrap class="signText">基期:</td><td><input:year name="beginYear" ></input:year></td>
                <td>&nbsp;&nbsp;</td>
                <td nowrap class="signText">计划期:</td><td><input:year name="endYear"></input:year></td>
              </tr>
            </table>
          </input:vanishEle>
          </input:vanish>
        </td>
        <td class="normalText" width="20">　</td>
      </tr>
      <tr>
        <td class="normalText" width="40">　</td>
        <td nowrap class="signText" width="72">变动因素:</td>
       <%String[][] factor={{"1","工作量"},{"2","单位收入"},
                            {"3","单位变动成本"},{"4","固定成本"}};%>
        <td><input:select name="factor" options="<%=factor%>" blankoption="y"></input:select></td>
        <td nowrap align="center"><button class="pageBtn" onclick="return find();">计算</button>
          <%if(result!=null){%>
          <button class="pageBtn" onclick="return preparedPrint();">打印</button>
          <%}%>
        </td>
        <td class="normalText" width="20">　</td>
      </tr>
    </html:table>
  <!-- 查询结果 -->
  <html:title clazz='table'>目标预测分析</html:title>
<%if(result!=null){
        %>
            <table border='1' bgColor=white borderColor=black style='border:2px solid #000000;BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
             <colgroup id='tg'>
            <col style = 'width:120px'>
            <col style = 'width:120px'>
            <col style = 'width:120px'>
            <col style = 'width:120px'>
            <col style = 'width:120px'>
            <col style = 'width:110px'>
            <col style = 'width:110px'>
          </colgroup>
          <tr class="resultLabel">
            <td class="resultLabel" nowrap rowspan="2">门诊科室</td>
            <td class="resultLabel" nowrap colspan="4">基期</td>
            <td class="resultLabel" nowrap rowspan="2">计划收益<br>(元)</td>
            <td class="resultLabel" nowrap rowspan="2" width="100">
              <%if(request.getParameter("factor")==null||request.getParameter("factor").equals("")) out.print("预测变动项目");
                if(request.getParameter("factor")!=null&&!request.getParameter("factor").equals("")){
                   if(request.getParameter("factor").equals("1"))
                       out.print("工作量<br>(诊次)");
                   if(request.getParameter("factor").equals("2"))
                       out.print("单位收入<br>(元)");
                   if(request.getParameter("factor").equals("3"))
                       out.print("单位变动成本<br>(元)");
                   if(request.getParameter("factor").equals("4"))
                       out.print("固定成本<br>(元)");
                  }%>
            </td>
          </tr>

          <tr class="resultLabel">
            <td class="resultLabel" nowrap>工作量<br>(诊次)</td>
            <td class="resultLabel" nowrap>单位收入<br>(元)</td>
            <td class="resultLabel" nowrap>单位变动成本<br>(元)</td>
            <td class="resultLabel" nowrap>固定成本<br>(元) </td>
          </tr>
        
        <%
            boolean sign=false;
            if(result[0].length==6) sign=true;
            for (int i = 0; i < result.length; i++ ) {
		          String rowColor = "rowGray";
		          if (i/2*2==i) rowColor = "rowWhite";
        %>
	        <tr CLASS="<%=rowColor%>">
            <td class="normalText" nowrap style="text-align:left"><%=result[i][0]%></td>
            <td nowrap class="numberText"><%=result[i][1]%></td>
            <td nowrap class="numberText"><%=changeFormat(result[i][2])%></td>
            <td nowrap class="numberText"><%=changeFormat(result[i][3])%></td>
            <td nowrap class="numberText"><%=changeFormat(result[i][4])%></td>
            <td nowrap class="numberText"><%=changeFormat(result[i][5])%></td>
            <td nowrap class='numberText'>
             <% if(sign!=true){
                if(request.getParameter("factor")!=null&&
                   request.getParameter("factor").equals("1"))
                      out.print(result[i][6]);
                if(request.getParameter("factor")!=null&&
                   !request.getParameter("factor").equals("1"))
               		    out.print( changeFormat(result[i][6]));
             }
             %>
            </td>
	        </tr>
		    <%
		          }
        %>
          </table>
        <%
         }
		    %>


  <input type='hidden' name="subFunction"/>
</form>
</html:html>
