<!--
   $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/apportion/costApportion.jsp,v 1.1 2012/03/12 01:57:33 zhoulidong Exp $
   $Author: zhoulidong $
   $Date: 2012/03/12 01:57:33 $
   $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,com.viewhigh.cbcs.base.mvc.view.MonthComponent,com.viewhigh.cbcs.base.mvc.view.TableMarge" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript">
        function back(){
	  template.subFunction.value='show'
	  template.submit();
          }
	function find(num) {
	   if (template.year_month.value=='') {
	    alert('请选择核算月');
	    return false;
	    }
          if(num==2)
            template.subFunction.value='check'

	  show_wait();
	  template.submit();
	  return true;
	 }
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="costApportion.jspviewhigh" >
      <!-- 返回信息栏 -->
	  <html:message/>

      <!-- 标题栏 -->
	  <html:title clazz='module'>成本分摊</html:title><!-- 简单信息 -->
	  <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap width="10%">核算月：</td>
          <td class="normalText"><%=new MonthComponent("year_month", request.getParameter("year_month"))%></td>
          <td align="right"><button class="pageBtn" onclick="return find(2)" >检查</button>
          <!--<img src="images/check.gif" class="mouse" onclick="return find(2)">--></td>
        </tr>
	  </html:table>
  <%
      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
      if(ro!=null){
      TableMarge oper = new TableMarge(ro, "return find(2)");
  %>
     <!-- 复杂信息 -->
	  <html:table clazz="complex">
    <!-- 操作 -->
    <tr><td><%=oper%></td></tr>
    <tr>
   		<td>
	      <html:table clazz="result" divHeight="280">
	        <html:tr clazz='label'>
            <td nowrap="nowrap" class="resultLabel">序号</td>
            <td nowrap="nowrap" class="resultLabel">问题描述</td>
	        </html:tr>

        <%
          String[][] result = ro.getTableResult();
            if ( result != null ) {
              for (int i = 0; i < result.length; i++ ) {
        %>

        <tr >
          <td class="normalText" nowrap="nowrap" ><%=i+1%></td>
          <td class="normalText" nowrap="nowrap" ><%=result[ i ][0]%></td>
        </tr>
          <%
            }
           }
          %>
          </html:table>
      </td>
    </tr>
  </html:table>
<%}%>
		<input type='hidden' name="flag" value='complete'>	
		<input type="hidden" name="subFunction" >
  	</form>

</html:html>
