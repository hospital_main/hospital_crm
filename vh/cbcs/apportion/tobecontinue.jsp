<!--
   $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/apportion/tobecontinue.jsp,v 1.1 2012/03/12 01:57:33 zhoulidong Exp $
   $Author: zhoulidong $
   $Date: 2012/03/12 01:57:33 $
   $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,com.viewhigh.cbcs.base.mvc.view.MonthComponent,com.viewhigh.cbcs.base.mvc.view.TableMarge" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript">
function back()
     {
 	template.subFunction.value='show'
 	template.submit();
     }
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="costApportion.jspviewhigh" >
      <!-- 返回信息栏 -->
       <html:message/>

      <!-- 标题栏 -->
       <html:title clazz='module'>成本分摊</html:title>
      <!-- 简单信息 -->
       <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap width="10%">核算月：</td>
          <td class="normalText"><%=request.getParameter("year_month")%></td>
          <td align="right">
		   <button class="pageBtn" onclick="show_wait();submit()">继续</button>
		  <button class="pageBtn" onclick="back()">返回</button>
          <!--<img src="images/go-on.GIF" class="mouse" onclick="show_wait();submit()"/><img src="images/return1.gif" class="mouse" onclick="back()"/>--></td>          
          <td></td>
          <td></td>
        </tr>
	  </html:table>
  <%
      String[] result = (String[])request.getAttribute("results");
      if(result!=null){
  %>
     <!-- 复杂信息 -->
      <html:table clazz="complex">
    <!-- 操作 -->
	<tr>
	<tr>
	<td height="30"></td>
	</tr>
	<td>
	   <html:table clazz="result" divHeight="280">
	    <html:tr clazz='label'>
	       <td nowrap="nowrap" class="resultLabel">序号</td>
	       <td nowrap="nowrap" class="resultLabel">问题描述</td>
	    </html:tr>

        <%
            if ( result != null ) {
              for (int i = 0; i < result.length; i++ ) {
        %>

	    <tr >
	       <td class="normalText" nowrap="nowrap" ><%=i+1%></td>
	       <td class="normalText" nowrap="nowrap" ><%=result[ i ]%></td>
	    </tr>
          <%
            }
           }
          %>
          </html:table>
        </td>
        </tr>
     </html:table>
<%}%>
		<input type='hidden' name="flag" value='<%=request.getParameter("flag")%>'>	
		<input type="hidden" name="subFunction" value='gotoapp' >
		<input type='hidden' name='year_month' value='<%=request.getParameter("year_month")%>'>
    </form>

</html:html>
