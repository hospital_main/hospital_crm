<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/apportion/config/ApporParaConfigDetailPerson.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime: 03-08-19 17:51 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

</Script>
<html:html clazz="child">
<form name="template" method="post" action="apporParaConfig.jspviewhigh">
<!-- 信息提示栏 -->
	  <html:message/>
<%
  String[][] result =(String[][]) request.getAttribute( "result" );
  String year_month=request.getParameter("year_month");
  String dept_name=request.getParameter("dept_name");
  String dept_code = request.getParameter("dept_code");
  if(year_month==null){
  	year_month="";
  }
  if(dept_name==null){
  	dept_name="";
  }
  if(dept_code==null){
  	dept_code="";
  }
  
%>
    <html:title clazz='module'><%=(year_month+dept_name)%>人员考勤</html:title>
      <html:table clazz="simple">
    <tr>
      <% String work_num = request.getParameter("work_num");%>
      <% String emp_name = request.getParameter("emp_name");%>
      <td style="display:none"><input type=text name="dept_code" class="textInputC" <%out.println(" value=" + dept_code);%>></td>
      <td style="display:none"><input type=text name="year_month" class="textInputC" <%out.println(" value=" + year_month);%>></td>
      <td style="display:none"><input type=text name="dept_name" class="textInputC" <%out.println(" value=" + dept_name);%>></td>
      <td class="signText" >工号：</td>
      <td><input type=text name="work_num" class="textInputC" <%if(work_num!= null){ out.println(" value=" + work_num);}%>></td>      
      <td class="signText" >姓名：</td>
      <td><input type=text name="emp_name" class="textInputC" <%if(emp_name!= null){ out.println(" value=" + emp_name);}%>></td>    
      
      <td> <button class="pageBtn" onclick="template.submit();return true;" >查询</button>
    </tr>
  </html:table>
    
    
    
	  <html:table clazz="complex">
<p>

 
<table width='100%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
    <html:tr clazz='label'>
	    <td class="resultLabel" noWrap="true" rowspan="2">人员类别</td>
	    <td class="resultLabel" noWrap="true" rowspan="2">职别</td>
	    <td class="resultLabel" noWrap="true" rowspan="2">工号</td>
	    <td class="resultLabel" noWrap="true" rowspan="2">姓名</td>
	    <td class="resultLabel" noWrap="true" rowspan="2">实际出勤天数</td>
	    <td class="resultLabel" colspan='31'>出勤状况</td>
    </html:tr>
    <html:tr clazz='label'>
    <%for(int j=0;j<31;j++){%>
    <td class="resultLabel"><%=j+1%></td>
    <%}%>
		</html:tr>
<%
  if (result!=null)
  {
    for (int i = 0; i < result.length; i++)
    {
%>
  <tr>
  <%for(int k=0;k<result[0].length;k++){%>
    <td class="normalText" noWrap="true"><%=result[i][k]%></td>
  <%}%>
  </tr>
<%
    }
  }
%>
	</table>
</html:table>
  <input type="hidden" name="subFunction" value="apporParaConfigDetailPerson_find">

</form>
</html:html>
