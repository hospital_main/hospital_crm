<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/apportion/config/ApporParaConfigDetailEquip.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime: 03-08-19 17:51 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">

</Script>
<html:html clazz="child">
<form name="template" method="post" action="apporParaConfig.jspviewhigh">
<!-- 信息提示栏 -->
	  <html:message/>
<%
  String[][] result =(String[][]) request.getAttribute("result");
  String  year_month =request.getParameter("year_month");
  String 	dept_name =request.getParameter("dept_name");
%>

<html:title clazz='module'><%=year_month+dept_name%>参与折旧的设备清单</html:title>
<html:table clazz="complex">
<p>
	  <table width='100%' border='1' bgColor=white borderColor=black style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt' >
	    <html:tr clazz='label'>
	    		<td class="resultLabel">设备编码</td>
		    <td class="resultLabel">设备名称</td>
		    <td class="resultLabel">原值</td>
		  </html:tr>
				<%
				  if (result!=null)
				  {
				    for (int i = 0; i < result.length; i++)
				    {
				%>
				  <tr>
				  	<td class="normalText"><%=result[i][2]%></td>
				    <td class="normalText"><%=result[i][0]%></td>
				    <td class="normalText"><%=result[i][1]%></td>
				  </tr>
				<%
				    }
				   }
				%>
			</table>
	</html:table>

</form>
</html:html>
