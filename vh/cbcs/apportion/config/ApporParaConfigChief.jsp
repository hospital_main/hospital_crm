<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/apportion/config/ApporParaConfigChief.jsp,v 1.2 2015/03/25 03:37:44 zhubingrui Exp $
 $Author: zhubingrui $
 $Date: 2015/03/25 03:37:44 $
 $Revision: 1.2 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
com.viewhigh.cbcs.base.mvc.view.component.Select,com.viewhigh.cbcs.base.util.Preference,java.text.* ,
com.viewhigh.cbcs.base.util.Preference,com.viewhigh.cbcs.cbcs.util.DictCache,com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
 

<html:html clazz="main">
<form name="template" method="post" action="apporParaConfig.jspviewhigh">
    <html:message/>
    <html:title clazz='module'>参数配置</html:title>
    <%	  BaseRO ro = (BaseRO)request.getAttribute("baseRO");
   
    	TableMarge oper = new TableMarge(ro, "return find()");%> 
    <html:table clazz="simple">
	      <tr>	     
	          <td  class="signText">核算月：</td> 
	          <td  class="normalText"><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td>
              <td  class="normalText" style="text-align:right"><button class="pageBtn" onclick="return find()">查询</button> </td>  
      	  </tr>      
	  </html:table> 
  
  <html:title clazz='table'><%=request.getParameter("year_month")==null? "X":request.getParameter("year_month").substring(0,4)%>年<%=request.getParameter("year_month")==null? "X":request.getParameter("year_month").substring(4)%>月参数配置</html:title>
  <html:table clazz="complex">
  	<tr><td><%=oper%></td></tr>
  	<tr>
	    <td>
	    	<html:table clazz="result">
	        <html:tr clazz='label'>
          	<td nowrap class="resultLabel" >类型</td>
						<td nowrap class="resultLabel" >配置标志</td>	
	        </html:tr>
	        <%
         if(ro!=null){
          String[][] result = ro.getTableResult(); 
          if ( result != null ) {
	          String [] isConfig= new String [result.length];
	          for (int i = 0; i < result.length; i++ ) {
              String primaryKey = result[ i ][ 0 ];
              isConfig[i] = result[i][2];
              for (int j=0; j<result[i].length; j++) {
                if (result[i][j]!=null && result[i][j].trim().equals("")) {
                  result[i][j]="&nbsp;";
                }
              }

            	String rowColor = "rowGray";
            	if (i % 2 == 0) rowColor = "rowWhite";
        %>
	       <tr CLASS="<%=rowColor%>">
          <td nowrap class="normalText">
          <%
         // boolean PersonConfig=com.viewhigh.cbcs.base.util.Preference.getPersonConfig().equals("true")||Preference.getPersonConfig().equals("TRUE"); 
          %>
            <a href='#' onclick="window.open('apporParaConfig.jspviewhigh?subFunction=apporParaConfigInit&year_month='+template.year_month.value+' &app_para_code=<%=result[ i ][ 0 ]%>&app_para_name=<%=result[ i ][ 1 ]%>&sign=<%=result[i][3].equals("已核算")? 0:1%>'
             +'&date='+tim.toLocaleString(),null,'top =100, left=100, height=600,width=800,status=no,toolbar=no,menubar=no,location=no,scrollbars=1')"> <%=result[ i ][ 1 ]%>  </a>  
          </td>
          <td nowrap class="normalText"><%=result[ i ][ 3 ]%></td>
	 				</tr>
	   
	           <%
              }
            }
           }
        %>
	       </html:table> 
	    </td>
  	</tr>
  </html:table>	
		
<input type="hidden" name="subFunction" >		
</form>


<Script Language="JavaScript" src="javascript/check.js" ></Script>

<script>
function find(){ 
	if(template.year_month.value==""){ alert("请选择日期!");	return false; 
	} 
     template.subFunction.value ='find';
      show_wait();
      template.submit();
      return true;
  }
  
  function showmodul(src,name,style){
   var tim=new Date();
   window.showModalDialog(src+'&time='+tim.toLocaleString(),name,style)
   if (template!=null && template._old_current_page!=null) {
      template._current_page.value = template._old_current_page.value
   }

   find();
  }
var tim=new Date();
</script>

</html:html>
