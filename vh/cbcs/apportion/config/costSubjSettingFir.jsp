<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/apportion/config/costSubjSettingFir.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect, com.viewhigh.cbcs.cbcs.util.Constant"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.base.mvc.view.component.*,com.viewhigh.cbcs.cbcs.util.DisplayWidth,
  com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>
<%
  String script="";
  String[][] checked = (String[][])request.getAttribute("checked");
if(checked!=null)
  for(int i=0;i<checked.length;i++)
    script+="subcat["+i+"]=\""+checked[i][0]+"\";";

%>
<script language="JavaScript">
function next(){
  var flag = false;
  for (var i=0; i<template.elements.length; i++) {
    if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
      flag = true;
  }
  if( flag==false){
    alert( "请先选择!");
    return false;
  } else{
		template.subFunction.value='preparedSubjSetting';
		show_wait();
		template.submit();
		return true;
  }
}

</script>
<html:html clazz="main" fixRows="1">
<form name="template" method="post" action="costSubjSetting.jspviewhigh">
  <!-- 信息提示栏 -->
	  <html:message/>
  <!-- 标题栏 -->
	<html:title clazz='module'>分摊设置2－1:公共部门 成本项目选择</html:title>
  <%
    String[][] result = (String[][])request.getAttribute("table_result");
  %>
  <table width="100%" height="10%">
  <tr>
  	<td align="left"><button class="pageBtn"  onclick="next()" >下一步</button>
	<!--<img src="images/next-1.gif" class="mouse" onclick="next()"/>--></td>
  </tr>
  <tr><td>
  <!-- 复杂信息 -->
	 <html:table clazz="result">	            
	        <tr class="resultLabel">
            <td nowrap class="resultLabel">选择</td>
            <td nowrap class="resultLabel">成本项目代码</td>
            <td nowrap class="resultLabel">成本项目名称</td>
	        </tr>

        <%
            if ( result != null )
            {
              for (int i = 0; i < result.length; i++ )
              {
                String primaryKey = result[i][0];
                 for (int j=0; j<result[i].length; j++)
                       {
                         if (result[i][j]!=null&&result[i][j].trim().equals(""))
                           {
                             result[i][j]="&nbsp;";
                           }
                     }
                String rowColor = "rowGray";
                if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td><input type="checkbox" name="primaryKey" checked="true" value="<%=primaryKey%>"></td>
          <td class="normalText"><%=result[i][0]%></td>
          <td class="normalText"><%=result[i][1]%></td>
        </tr>

        <%
              }
            }
        %>
 </html:table>
</td>
</tr>
</table>
<input type=hidden name="subFunction"/>
</form>
<%if(checked!=null){%>
<script language="JavaScript">
subcat = new Array()
<%=script%>
     for (var i=0; i<template.elements.length; i++){
       if (template.elements[i].name=='primaryKey'){
         for(var j=0; j<subcat.length; j++){
           if(template.elements[i].value==subcat[j]){
             template.elements[i].checked=true;
           }
         }
       }
     }
</script>
<%}%>
</html:html>
