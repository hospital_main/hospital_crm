<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/apportion/config/ApporParaConfigDetail.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Modtime: 03-08-19 17:51 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function calculate(){
		window.location.href='apporParaConfig.jspviewhigh?subFunction=apporParaConfigInit&year_month=<%=request.getAttribute("year_month")%>&app_para_code=<%=request.getAttribute("app_para_code")%>&app_para_name=<%=request.getAttribute("app_para_name")%>&&isCalculate=YES&date='+new Date().toLocaleString();
	}
	
	function inherit(){
		template.subFunction.value ='inherit';
		if (!confirm('继承覆盖本月份数据:请确认')) {
			return false;
		} 
		show_wait();
		template.submit();
		return true;
	}
  
	function check(){
		for(i = 0; i < template.apporParaConfigs.length; i++){
			if(isEmpty(document.all['value' + i])){
				alert('数量不能为空!');
				return false;
			}else if(isNaN(document.all['value' + i].value.replace(',',''))){
				alert(document.all['value' + i].value+'不是数值!');
				return false;
			}else if(!isNaN(document.all['value' + i].value.replace(',',''))){
				document.all['value' + i].value = document.all['value' + i].value.replace(',','');
			}
			
			if(document.all['value' + i].value<0){
				alert('数量必须输入大于0的数值!');	
				return false;				
			}
			
			/*
			else if(!isNumber(document.all['value' + i]))
			{
			//alert('数量必须输入大于0的数值!');
			alert(document.all['value' + i].value+'不是数值!');
			return false;
			}
			*/
		}
		for(i = 0; i < template.apporParaConfigs.length; i++){
			template.apporParaConfigs[i].value =
			template.apporParaConfigs[i].value
			+ '|^|' + document.all['value' + i].value;
			template.apporParaConfigs[i].checked = true;
		}
		show_wait();
		template.submit();
		return true;
	}

</Script>
<html:html clazz="child">
<form name="template" method="post" action="apporParaConfig.jspviewhigh">
<!-- 信息提示栏 -->
	  <html:message/>


<%
  String[][] result =(String[][]) request.getAttribute( "table_result" );
  String app_para_name = (String) request.getAttribute( "app_para_name" );
  if (result!=null)
  {
    String appParaCode = request.getParameter("app_para_code")==null ? "":request.getParameter("app_para_code").trim();
    
		%><html:title clazz='module'><%=app_para_name%></html:title>
	  <html:table clazz="complex">
<p>

<%

 String PersonConfig=com.viewhigh.cbcs.base.util.Preference.getPersonConfig(); 
//out.print(request.getParameter("sign")+"aaa:::"+request.getAttribute("sign"));
 if((request.getParameter("sign")!=null&&request.getParameter("sign").equals("1"))
 	|| (request.getAttribute("sign")!=null&&request.getAttribute("sign").equals("1"))){  //未核算
   
    if( "01".equals(appParaCode) ||  "02".equals(appParaCode) || "03".equals(appParaCode) || "04".equals(appParaCode) ){
    
%>  
<button class="pageBtn" onclick="return check();"  >保存</button>
<button class="pageBtn" onclick="return reset();">重置</button>
<button class="pageBtn" onclick="return inherit();" >继承</button>
<button class="pageBtn" onclick="return calculate();" >计算</button>

 <!--  <img src="images/b_save.gif" class="mouse" onclick="return check();" /> 
    <img src="images/b_reset.gif" class="mouse" onclick="return reset();" />
    <img src="images/inherit.gif" class="mouse" onclick="return inherit();" />-->
</p>
<%}else{
%>
<button class="pageBtn" onclick="return check();"  >保存</button>
<button class="pageBtn" onclick="return reset();">重置</button>
<button class="pageBtn" onclick="return inherit();" >继承</button>
<!--<button class="pageBtn" onclick="return calculate();" >计算</button>

    <img src="images/b_save.gif" class="mouse" onclick="return check();" /> 
    <img src="images/b_reset.gif" class="mouse" onclick="return reset();" />
    <img src="images/inherit.gif" class="mouse" onclick="return inherit();" />-->
</p>
<%}
}%>
  <table width='100%' class='resultSetTable'  >
    <html:tr clazz='label'>
    <td class="resultLabel">科室代码</td>
    <td class="resultLabel">科室名称</td>
    <td class="resultLabel">数量</td>
	        </html:tr>
<%
    int j = 0;
    for (int i = 0; i < result.length; i++)
    {
%>

  <tr>
    <td class="normalText"><%=result[i][0]%></td>
    <td class="normalText">
    <%if(("01".equals(appParaCode)&&(!result[i][2].equals("N"))) && "3".equals(PersonConfig)) {
    	out.print("<a href='#' onclick=\"window.open('apporParaConfig.jspviewhigh?subFunction=apporParaConfigDetailPerson&year_month="+result[i][4]+"&dept_code="+result[i][0]+"&dept_name="+result[i][1].replaceAll("　","")+"','_blank','top =100, left=100, height=600,width=800,status=no,toolbar=no,menubar=no,location=no,scrollbars=1')\">");
    }
    else if(((!result[i][2].equals("N"))&&"03".equals(appParaCode))){
    	out.print("<a href='#' onclick=\"window.open('apporParaConfig.jspviewhigh?subFunction=apporParaConfigDetailEquipment&year_month="+result[i][4]+"&dept_code="+result[i][0]+"&dept_name="+result[i][1].replaceAll("　","")+"','_blank','top =100, left=100, height=600,width=800,status=no,toolbar=no,menubar=no,location=no,scrollbars=1')\">");
    }
    	%>
    <%=result[i][1]%>
    <%if((("01".equals(appParaCode)&&(!result[i][2].equals("N"))) && "3".equals(PersonConfig)) ||(((!result[i][2].equals("N"))&&"03".equals(appParaCode))))  {
    	out.print("</a>");
    }%>
    </td>
    <td class="numberText">
      <%if(result[i][2].equals("N")){%><%=new DecimalFormat("##0.00").format(Double.parseDouble(result[i][3]))%><%}else{%><!--new DecimalFormat("#,##0.00")-->
      <p style="display:none"><%=result[i][3]%></p>
      <input type="checkbox" name="apporParaConfigs" value="<%=result[i][0]%>" style="display:none">
      <input type="text" style="text-align:right;border:1px solid gray;" id=<%="value" + j%> name=<%="value" + j%> <%if(!result[i][3].equals("0")){out.println(" value=" + new DecimalFormat("##0.00").format(Double.parseDouble(result[i][3])));}%>><% j++;}%>
      &nbsp;
    </td>
  </tr>

<%
    }
%>

	      </table>

	      </html:table>
  <input type="hidden" name="subFunction" value="apporParaConfig">
  <input type="hidden" name="app_para_code" value="<%=request.getAttribute("app_para_code")%>">
  <input type="hidden" name="sign" value="<%=request.getParameter("sign")%>">
  <input type="hidden" name="app_para_name" value="<%=request.getAttribute("app_para_name")%>">
<%

  }
%>
</form>
</html:html>
