<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/apportion/config/costSubjSetting.jsp,v 1.1 2012/03/12 01:57:34 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:34 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelectInDiv, com.viewhigh.cbcs.cbcs.util.Constant"%>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%

     String[][] cost_subj = (String[][])request.getAttribute("table_result");
     String[][] person_cost = (String[][])request.getAttribute("other_cost");
     String[][] cost = (String[][])request.getAttribute("cost");
     String[][] traffic = (String[][])request.getAttribute("init_traffic");
     String[][] house_depr_argu = {{Constant.AREA_QTY_CODE, Constant.AREA_QTY_NAME}};
     String[][] house_depr_fix = (String[][])request.getAttribute("fix_asset_depr_type");
     String[][] equipment_argu = {{Constant.CAP_QTY_CODE, Constant.CAP_QTY_NAME}};
     String[][] equipment_fix = (String[][])request.getAttribute("fix_asset_depr_type");
     String[][] house_repair = (String[][])request.getAttribute("house_repair");
%>
<Script Language="JavaScript">

  function sychronize(name, index)
  {
    var a = new Array(
      document.all['person_cost' + index],
      document.all['cost' + index],
      document.all['traffic' + index],
      document.all['house_depr_argu' + index],
      document.all['house_depr_fix' + index],
      document.all['equipment_argu' + index],
      document.all['equipment_fix' + index],
      document.all['house_repair' + index]
    );

    if(document.all[name + index].value == '')
    {
      return true;
    }
    else
    {
      if(name.search('house_depr') != -1)
      {
        for(i = 0; i < a.length; i++)
        {
          if(a[i].name.search('house_depr') == -1)
          {
            a[i].value = '';
          }
        }
      }
      else if(name.search('equipment') != -1)
      {
        for(i = 0; i < a.length; i++)
        {
          if(a[i].name.search('equipment') == -1)
          {
            a[i].value = '';
          }
        }
      }
      else
      {
        for(i = 0; i < a.length; i++)
        {
          if(a[i].name != (name + index))
          {
            a[i].value = '';
          }
        }
      }
      return true;
    }

  }

  function packParameters()
  {
    var checkedNumber = 0;
    if(template.costSubjSettings == null)
    {
      alert('没有配置数据!');
      return false;
    }
  if(template.costSubjSettings.length!=null){
    for(i = 0; i < template.costSubjSettings.length; i++)
    {
      template.costSubjSettings[i].value = ''
      var a = new Array(
        document.all['person_cost' + i],
        document.all['cost' + i],
        document.all['traffic' + i],
        document.all['house_depr_argu' + i],
        document.all['house_depr_fix' + i],
        document.all['equipment_argu' + i],
        document.all['equipment_fix' + i],
        document.all['house_repair' + i]
      );

      var name = '';
      for(j = 0; j < a.length; j++)
      {
        if(a[j].value != '')
        {
          name = a[j].name;
          break;
        }
      }

      if(name == '')
      {
        template.costSubjSettings[i].checked = false;
      }
      else
      {
        if(name == ('person_cost' + i))
        {
          template.costSubjSettings[i].value =
            'A|^|' + template.subjCode[i].value + '|^|' +
            document.all['person_cost' + i].value;
          template.costSubjSettings[i].checked = true;
          checkedNumber++;
        }
        if(name == ('cost' + i))
        {
          template.costSubjSettings[i].value =
            'B|^|' + template.subjCode[i].value + '|^|' +
            document.all['cost' + i].value;
          template.costSubjSettings[i].checked = true;
          checkedNumber++;
        }
        if(name == ('traffic' + i))
        {
          template.costSubjSettings[i].value =
            'C|^|' + template.subjCode[i].value + '|^|' +
            document.all['traffic' + i].value;
          template.costSubjSettings[i].checked = true;
          checkedNumber++;
        }
        if(name.search('house_depr') != -1)
        {
          if(document.all['house_depr_argu' + i].value == '' ||
            document.all['house_depr_fix' + i].value == '')
          {
            alert('请将'+template.subjName[i].value+'的参数完整设定!');
            return false;
          }
          else
          {
            template.costSubjSettings[i].value =
            'D|^|' + template.subjCode[i].value + '|^|' +
            document.all['house_depr_argu' + i].value + '|^|' +
            document.all['house_depr_fix' + i].value;
            template.costSubjSettings[i].checked = true;
            checkedNumber++;
          }
        }
        if(name.search('equipment') != -1)
        {
          if(document.all['equipment_argu' + i].value == '' ||
            document.all['equipment_fix' + i].value == '')
          {
            alert('请将'+template.subjName[i].value+'的参数完整设定!');
            return false;
          }
          else
          {
            template.costSubjSettings[i].value =
            'E|^|' + template.subjCode[i].value + '|^|' +
            document.all['equipment_argu' + i].value + '|^|' +
            document.all['equipment_fix' + i].value;
            template.costSubjSettings[i].checked = true;
            checkedNumber++;
          }
        }
        if(name == ('house_repair' + i))
        {
          template.costSubjSettings[i].value =
            'F|^|' + template.subjCode[i].value + '|^|' +
            document.all['house_repair' + i].value;
          template.costSubjSettings[i].checked = true;
          checkedNumber++;
        }
      }
    }
  }
  else{
    template.costSubjSettings.value = ''
    var a = new Array(
        document.all['person_cost0'],
        document.all['cost0'],
        document.all['traffic0'],
        document.all['house_depr_argu0'],
        document.all['house_depr_fix0'],
        document.all['equipment_argu0'],
        document.all['equipment_fix0'],
        document.all['house_repair0']
      );

      var name = '';
      for(j = 0; j < a.length; j++)
      {
        if(a[j].value != '')
        {
          name = a[j].name;
          break;
        }
      }

      if(name == '')
      {
        template.costSubjSettings.checked = false;
      }
      else
      {
        if(name == ('person_cost0'))
        {
          template.costSubjSettings.value =
            'A|^|' + template.subjCode.value + '|^|' +
            document.all['person_cost0'].value;
          template.costSubjSettings.checked = true;
          checkedNumber++;
        }
        if(name == ('cost0'))
        {
          template.costSubjSettings.value =
            'B|^|' + template.subjCode.value + '|^|' +
            document.all['cost0'].value;
          template.costSubjSettings.checked = true;
          checkedNumber++;
        }
        if(name == ('traffic0'))
        {
          template.costSubjSettings.value =
            'C|^|' + template.subjCode.value + '|^|' +
            document.all['traffic0'].value;
          template.costSubjSettings.checked = true;
          checkedNumber++;
        }
        if(name.search('house_depr') != -1)
        {
          if(document.all['house_depr_argu0'].value == '' ||
            document.all['house_depr_fix0'].value == '')
          {
            alert('请将'+template.subjName[i].value+'的参数完整设定!');
            return false;
          }
          else
          {
            template.costSubjSettings.value =
            'D|^|' + template.subjCode.value + '|^|' +
            document.all['house_depr_argu0'].value + '|^|' +
            document.all['house_depr_fix0'].value;
            template.costSubjSettings.checked = true;
            checkedNumber++;
          }
        }
        if(name.search('equipment') != -1)
        {
          if(document.all['equipment_argu0'].value == '' ||
            document.all['equipment_fix0'].value == '')
          {
            alert();
            return false;
          }
          else
          {
            template.costSubjSettings.value =
            'E|^|' + template.subjCode.value + '|^|' +
            document.all['equipment_argu0'].value + '|^|' +
            document.all['equipment_fix0'].value;
            template.costSubjSettings.checked = true;
            checkedNumber++;
          }
        }
        if(name == ('house_repair0'))
        {
          template.costSubjSettings.value =
            'F|^|' + template.subjCode.value + '|^|' +
            document.all['house_repair0'].value;
          template.costSubjSettings.checked = true;
          checkedNumber++;
        }
      }
  	}
    if(checkedNumber == 0) {
      alert('没有配置数据!');
      return false;
    }
    show_wait();
    template.submit();
    return true;
  }
  
  function back(){
    template.subFunction.value='preparedSubjSelect';
    show_wait();
    template.submit();
    return true;
  }
</Script>

<html:html clazz="main" scrollCtl="yes">
<form name="template" method="post" action="costSubjSetting.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>分摊设置2－2:公共部门 成本项目设置</html:title>

<table>
	<tr>
		<td align="right"><button class="pageBtn"  onclick="return back();" >上一步</button>
		<button class="pageBtn"   onclick="packParameters()"  >完成</button>
		<!--<img src="images/pre-1.gif" class="mouse" onclick="return back();" > &nbsp;
			<img src="images/complete-1.gif" class="mouse" onclick="packParameters()" />--></td>
  </tr>
</table>
  <div  style='overflow-y:visible; width:100%;'>
   	<table  class='resultSetTable' width="100%">
   		
   
	  <tr>
      <td rowspan="3" class="resultLabel" nowrap width="80">成本项目代码</td>
      <td rowspan="3" class="resultLabel" nowrap width="70">成本项目名称</td>
      <td height="21" colspan="8" class="resultLabel" nowrap >分摊类别</td>
	   </tr>
	        <tr>
      <td rowspan="2" class="resultLabel" nowrap width="150">其他成本</td>
      <td rowspan="2" class="resultLabel" nowrap width="85">煤电水费</td>
      <td rowspan="2" class="resultLabel" nowrap width="155">交通工具消耗</td>
      <td colspan="2" class="resultLabel" nowrap >房屋折旧</td>
      <td colspan="2" class="resultLabel" nowrap >设备折旧</td>
      <td rowspan="2" class="resultLabel"  >房屋修缮,零星工程</td>
	        </tr>
	        <tr>
      <td class="resultLabel" nowrap width="70">参数</td>
      <td class="resultLabel" nowrap width="80">固定资产折旧类型</td>
      <td class="resultLabel" nowrap width="105">参数</td>
      <td class="resultLabel" nowrap width="80">固定资产折旧类型</td>
	        </tr>
</table>
</div>

	    <div  style='overflow-y:auto; width:100%; height:375px'>
	    	<table  class='resultSetTable' width="100%">
	      
    <%
      for(int i = 0; i < cost_subj.length; i++)
      {
    %>
    <tr>
      <td class="normalText" nowrap width="80"><%=cost_subj[i][0]%></td>
      <td class="normalText" nowrap width="70"><%=cost_subj[i][1]%></td>
<%
SingleSelectInDiv select_person_cost = new SingleSelectInDiv(person_cost,"person_cost" + i,cost_subj[i][2],false,false,"");
SingleSelectInDiv select_cost = new SingleSelectInDiv(cost,"cost" + i,cost_subj[i][3],false,false,"");
SingleSelectInDiv select_traffic = new SingleSelectInDiv(traffic,"traffic" + i,cost_subj[i][4],false,false,"");
SingleSelectInDiv select_house_depr_argu = new SingleSelectInDiv(house_depr_argu,"house_depr_argu" + i,cost_subj[i][5],false,false,"");
SingleSelectInDiv select_house_depr_fix = new SingleSelectInDiv(house_depr_fix,"house_depr_fix" + i,cost_subj[i][6],false,false,"");
SingleSelectInDiv select_equipment_argu = new SingleSelectInDiv(equipment_argu,"equipment_argu" + i,cost_subj[i][7],false,false,"");
SingleSelectInDiv select_equipment_fix = new SingleSelectInDiv(equipment_fix,"equipment_fix" + i,cost_subj[i][8],false,false,"");
SingleSelectInDiv select_house_repair=new SingleSelectInDiv(house_repair,"house_repair" + i,cost_subj[i][9],false,false,"");

select_person_cost.setAttribute("id", "person_cost" + i);
select_cost.setAttribute("id", "cost" + i);
select_traffic.setAttribute("id", "traffic" + i);
select_house_depr_argu.setAttribute("id", "house_depr_argu" + i);
select_house_depr_fix.setAttribute("id", "house_depr_fix" + i);
select_equipment_argu.setAttribute("id", "equipment_argu" + i);
select_equipment_fix.setAttribute("id", "equipment_fix" + i);
select_house_repair.setAttribute("id", "house_repair" + i);

select_person_cost.setAttribute("onchange", "sychronize('person_cost'," + i + ")");
select_cost.setAttribute("onchange", "sychronize('cost'," + i + ")");
select_traffic.setAttribute("onchange", "sychronize('traffic'," + i + ")");
select_house_depr_argu.setAttribute("onchange", "sychronize('house_depr_argu'," + i + ")");
select_house_depr_fix.setAttribute("onchange", "sychronize('house_depr_fix'," + i + ")");
select_equipment_argu.setAttribute("onchange", "sychronize('equipment_argu'," + i + ")");
select_equipment_fix.setAttribute("onchange", "sychronize('equipment_fix'," + i + ")");
select_house_repair.setAttribute("onchange", "sychronize('house_repair'," + i + ")");

%>
      <input type="checkbox" name="costSubjSettings" value="" style="display:none">
      <input type="hidden" name="subjCode" value="<%=cost_subj[i][0]%>" />
      <input type="hidden" name="subjName" value="<%=cost_subj[i][1]%>" />
      <td class="normalText" nowrap="nowrap" width="150"><%=select_person_cost%></td>
      <td class="normalText" nowrap="nowrap" nowrap width="85"><%=select_cost%></td>
      <td class="normalText" nowrap="nowrap" width="155"><%=select_traffic%></td>
      
      <td class="normalText" nowrap="nowrap" width="70"><%=select_house_depr_argu%></td>
      <td class="normalText" nowrap="nowrap" width="80"><%=select_house_depr_fix%></td>
      <td class="normalText" nowrap="nowrap" width="105"><%=select_equipment_argu%></td>
      <td class="normalText" nowrap="nowrap" width="80"><%=select_equipment_fix%></td>
      <td class="normalText" nowrap="nowrap"><%=select_house_repair%></td>
    </tr>
  <%
      }
  %>
	</table></div>
    <input type="hidden" name="subFunction" value="subjSetting" />
<%
   String array = (String)request.getAttribute("array");
%>
  <input type="hidden" name="array" value=<%=array%> />
</form>
</html:html>

