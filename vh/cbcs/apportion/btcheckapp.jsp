<!--
   $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/apportion/btcheckapp.jsp,v 1.1 2012/03/12 01:57:33 zhoulidong Exp $
   $Author: zhoulidong $
   $Date: 2012/03/12 01:57:33 $
   $Revision: 1.1 $
-->


<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,com.viewhigh.cbcs.base.mvc.view.MonthComponent,com.viewhigh.cbcs.base.mvc.view.TableMarge" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript">
    function back(){
	 template.subFunction.value='show'
	 template.submit();
      }
    function apportion(num) {
    	if(num==0){
    	    template.flag.value='no'
      	    template.subFunction.value='apportion'
   	  } else if(num==1) {
            template.flag.value='complete'
            template.subFunction.value='apportion'
          }
	 show_wait();
	 template.submit();
	 return true;
	}
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="costApportion.jspviewhigh" >
      <!-- 返回信息栏 -->
	  <html:message/>

      <!-- 标题栏 -->
	  <html:title clazz='module'>成本分摊</html:title>
	  <!-- 简单信息 -->
	  <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap width="10%">核算月：</td>
          <td class="normalText"><%=request.getParameter("year_month")%></td>
          <td align="right">
		  <button class="pageBtn" onclick="return apportion(0)">预结</button>
		  <button class="pageBtn" onclick="return apportion(1)">完成</button>
		  <button class="pageBtn" onclick="back()">返回</button>
		  <!--<img src="images/preparedBalance.gif" class="mouse" onclick="return apportion(0)"/>
		  <img src="images/complete.gif" class="mouse" onclick="return apportion(1)"/>
		  <img src="images/return1.gif" class="mouse" onclick="back()"/>--></td>
        </tr>
	  </html:table>
  
		<input type='hidden' name="flag" value='complete'>	
		<input type="hidden" name="subFunction" >
		<input type='hidden' name='year_month' value='<%=request.getParameter("year_month")%>'>
  	</form>

</html:html>
