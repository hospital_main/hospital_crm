<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/save/cbcsTransSaveMain.jsp,v 1.1 2012/03/12 01:59:04 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:59:04 $
  $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript">
	function trans() {
	  if (CheckInput()) {
	    show_wait();
  	  template.submit();
  	  return true;
  	} else {
  	  return false;
  	}
  }

  function CheckInput() {
     if(document.template.FILE.length==0) {
         alert('You must provide a name!');
         document.template.FILE.focus();
         return false;
     }
     FName=document.template.FILE.value;
     if (navigator.appVersion.indexOf('Win')>0) {
     FName=FName.substring(FName.lastIndexOf('\\')+1,FName.length);
     }else{
     FName=FName.substring(FName.lastIndexOf('/')+1,FName.length);
     }
     if ((navigator.appVersion.lastIndexOf('Win'))!=-1) {
         if((FName.indexOf('\\')>=0) || (FName.indexOf('/')>=0) || (FName.indexOf(':')>=0) || (FName.indexOf('*')>=0) || (FName.indexOf('?')>=0) || (FName.indexOf('"')>=0)|| (FName.indexOf('<')>=0) || (FName.indexOf('>')>=0) || (FName.indexOf('|')>=0)) {
             alert('请输入文件');
             document.template.FILE.focus();
             return false;
         };
     }else{
         if((FName.indexOf('\\')>=0) || (FName.indexOf(':')>=0) || (FName.indexOf('*')>=0) || (FName.indexOf('?')>=0) || (FName.indexOf('"')>=0)|| (FName.indexOf('<')>=0) || (FName.indexOf('>')>=0) || (FName.indexOf('|')>=0)) {
             alert('请输入文件');
             document.template.FILE.focus();
             return false;
         }
     }
     MyStr=document.template.FILE.value;
     i=0;
     while (MyStr.charAt(i)==' ') { i ++ }
     if (i==MyStr.length) {
         alert('请输入文件');
         document.template.FILE.focus();
         return false;
         }
     return true;
  }

</Script>
<html:html clazz="main">
    <form name="template" method="post" action="cbcsTransSave.jspviewhigh?subFunction=save" enctype='multipart/form-data'>
      <!-- 返回信息栏 -->
	  <html:message/>

      <!-- 标题栏 -->
	  <html:title clazz='module'>数据导入页面</html:title>
     <br>

      <!-- 简单信息 -->
	  <html:table clazz="simple">
        <tr>
          <td><input type=file name="FILE"/></td>
          <td>
          <button class="pageBtn" onclick="return trans()">导入</button> 
          <!--<img src="images/transfers.gif" class="mouse" onclick="return trans()"/>--></td>
        </tr>
	  </html:table>
  	</form>


</html:html>
