<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@taglib uri="/WEB-INF/taglib/viewhigh-html.tld"prefix="html"%>


<Script Language="JavaScript">
	function trans() {
	  if (CheckInput()) {
	    ShowWaitPost();
          template.action =
            'rptIncomeLeechdomTrans.jspviewhigh?subFunction=save&year_month=' + template.year_month.value;
  	  template.submit();
  	  return true;
  	} else {
  	  return false;
  	}
  }

  function remove()
  {if(template.year_month.value == '')
      {
        alert('请选择年月!');
        return false;
      }
    if(confirm('您确定要删除数据吗?'))
    {

      template.action =
        'rptIncomeLeechdomTrans.jspviewhigh?subFunction=delete&year_month=' + template.year_month.value;
      template.submit();
    }
    else
    {
      return false;
    }
  }

  function win(){
      if(template.year_month.value==""){
        alert('请选择年月')
        return
       }
    var win=window.open("rptIncomeLeechdomTrans.jspviewhigh?subFunction=findAll&year_month=" + template.year_month.value);
  }

  function CheckInput() {
     if(document.template.FILE.length==0) {
         alert('You must provide a name!');
         document.template.FILE.focus();
         return false;
     }
     FName=document.template.FILE.value;
     if (navigator.appVersion.indexOf('Win')>0) {
     FName=FName.substring(FName.lastIndexOf('\\')+1,FName.length);
     }else{
     FName=FName.substring(FName.lastIndexOf('/')+1,FName.length);
     }
     if ((navigator.appVersion.lastIndexOf('Win'))!=-1) {
         if((FName.indexOf('\\')>=0) || (FName.indexOf('/')>=0) || (FName.indexOf(':')>=0) || (FName.indexOf('*')>=0) || (FName.indexOf('?')>=0) || (FName.indexOf('"')>=0)|| (FName.indexOf('<')>=0) || (FName.indexOf('>')>=0) || (FName.indexOf('|')>=0)) {
             alert('请输入文件');
             document.template.FILE.focus();
             return false;
         };
     }else{
         if((FName.indexOf('\\')>=0) || (FName.indexOf(':')>=0) || (FName.indexOf('*')>=0) || (FName.indexOf('?')>=0) || (FName.indexOf('"')>=0)|| (FName.indexOf('<')>=0) || (FName.indexOf('>')>=0) || (FName.indexOf('|')>=0)) {
             alert('请输入文件');
             document.template.FILE.focus();
             return false;
         }
     }
     MyStr=document.template.FILE.value;
     i=0;
     while (MyStr.charAt(i)==' ') { i ++ }
     if (i==MyStr.length) {
         alert('请输入文件');
         document.template.FILE.focus();
         return false;
         }
     return true;
  }


  function ShowWaitPost(){
    var strAgent=navigator.userAgent;
    var xCoord;var yCoord;
    if (strAgent.indexOf('Mozilla/3')==-1 && strAgent.indexOf('MSIE 3')==-1){
       xCoord=screen.width/2 - 150 ;
       yCoord=(screen.height/2) - 30;
    }
    waitWin=open('','waitWin','top='+ yCoord +',left=' + xCoord +',directories=no,menubar=no,scrollbars=no,resizable=no,location=no,status=no,toolbar=no,width=300,height=60');
    waitWin.document.open();
    waitWin.document.write('<html><body bgcolor=#FFFFFF><center>');
    waitWin.document.write('<table cellpadding=10 cellspacing=0 border=0><tr>');
    waitWin.document.write('<td><font face=helv,helvetica size=2 color=#000000>上传文件...</font></td></tr></table>');
    waitWin.document.write('</center></body></html>');
    waitWin.document.close();
  }

  function WaitPost(){
    var strAgent=navigator.userAgent;
    var xCoord;var yCoord;
    if (strAgent.indexOf('Mozilla/3')==-1 && strAgent.indexOf('MSIE 3')==-1){
       xCoord=screen.width/2 - 150 ;
       yCoord=(screen.height/2) - 30;
    }
    waitWin=open('','waitWin','top='+ yCoord +',left=' + xCoord +',directories=no,menubar=no,scrollbars=no,resizable=no,location=no,status=no,toolbar=no,width=300,height=60');
    waitWin.document.open();
    waitWin.document.write('<html><body bgcolor=#FFFFFF><center>');
    waitWin.document.write('<table cellpadding=10 cellspacing=0 border=0><tr>');
    waitWin.document.write('<td><font face=helv,helvetica size=2 color=#000000>上传文件...</font></td></tr></table>');
    waitWin.document.write('</center></body></html>');
    waitWin.document.close();
    waitWin.close();
  }

  <%
  if (request.getParameter("subFunction")!=null && request.getParameter("subFunction").equals("save")) {
  %>
  WaitPost();
  <%  }  %>


</Script>
<html:html clazz="main">
    <form name="template" method="post" action="rptIncomeLeechdomTrans.jspviewhigh?subFunction=save" enctype='multipart/form-data'>
      <!-- 返回信息栏 -->
      <html:message/>
      <!-- 标题栏 -->
       <html:title clazz='module'>药品收支明细表导入</html:title>

      <!-- 简单信息 -->
   <html:table clazz="simple">
        <tr>
          <td nowrap class="signText" >核算月： </td>
          <td nowrap class="normalText" align=right><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td>
					<td colspan=3></td>
        <tr>
				<br>
        <tr>
					<td nowrap class="signText">文件：</td>
          <td><input type=file name="FILE"/></td>
	<td>
	<button class="pageBtn" onclick="return trans()" >导入</button> 
	<!--<img src="images/transfers.gif" class="mouse" onclick="return trans()"/>--></td>
          <td>
          <button class="pageBtn" name=""  onclick="return win()" >查询</button> 
          <!--<img src="images/find.gif" class="mouse" onclick="return win()" />--></td>
					<td><button class="pageBtn" onclick="return remove();">删除</button></td>
        </tr>
	  </html:table>


    </form>

</html:html>

