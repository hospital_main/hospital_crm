<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/save/FinanceVoucherTransMain.jsp,v 1.1 2012/03/12 01:59:04 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:04 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%@taglib uri="/WEB-INF/taglib/viewhigh-html.tld"prefix="html"%>


<Script Language="JavaScript">
  function trans() {
  	if (template.year_month.value=="") {
  		alert("请输入日期");
  		return false;
 		}
    if (CheckInput()) {
      show_wait();
      template.action =
        'FinanceVoucherTrans.jspviewhigh?subFunction=save&year_month='+template.year_month.value+'&clean='+template.clean.value+'&force='+template.force.value;
      template.submit();
      return true;
    } else {
      return false;
    }
  }
	
	function check() {
		if (template.year_month.value=="") {
  		alert("请输入日期");
  		return false;
 		}
    if (CheckInput()) {
      show_wait();
      template.action =
        'FinanceVoucherTrans.jspviewhigh?subFunction=check&year_month='+template.year_month.value+'&clean='+template.clean.value+'&force='+template.force.value;
      template.submit();
      return true;
    } else {
      return false;
    }
	}


  function remove()
  {
    if(confirm('您确定要删除数据吗?'))
    {
      if(template.year_month.value == '')
      {
        alert('请选择年月!');
        return false;
      }
      template.action = 'FinanceVoucherTrans.jspviewhigh?subFunction=delete&year_month=' + template.year_month.value;
      template.submit();
    }
    else
    {
      return false;
    }
  }

  function win(){
    var win=window.open("FinanceVoucherTrans.jspviewhigh?subFunction=findAll&year_month=" + template.year_month.value, null, "top=180, left=100, height=500,width=850, toolbar=no, location=no");
  }

  function CheckInput() {
     if(document.template.FILE.length==0) {
         alert('You must provide a name!');
         document.template.FILE.focus();
         return false;
     }
     FName=document.template.FILE.value;
     if (navigator.appVersion.indexOf('Win')>0) {
     FName=FName.substring(FName.lastIndexOf('\\')+1,FName.length);
     }else{
     FName=FName.substring(FName.lastIndexOf('/')+1,FName.length);
     }
     if ((navigator.appVersion.lastIndexOf('Win'))!=-1) {
         if((FName.indexOf('\\')>=0) || (FName.indexOf('/')>=0) || (FName.indexOf(':')>=0) || (FName.indexOf('*')>=0) || (FName.indexOf('?')>=0) || (FName.indexOf('"')>=0)|| (FName.indexOf('<')>=0) || (FName.indexOf('>')>=0) || (FName.indexOf('|')>=0)) {
             alert('请输入文件');
             document.template.FILE.focus();
             return false;
         }
     }else{
         if((FName.indexOf('\\')>=0) || (FName.indexOf(':')>=0) || (FName.indexOf('*')>=0) || (FName.indexOf('?')>=0) || (FName.indexOf('"')>=0)|| (FName.indexOf('<')>=0) || (FName.indexOf('>')>=0) || (FName.indexOf('|')>=0)) {
             alert('请输入文件');
             document.template.FILE.focus();
             return false;
         }
     }
     MyStr=document.template.FILE.value;
     i=0;
     while (MyStr.charAt(i)==' ') { i ++ }
     if (i==MyStr.length) {
       alert('请输入文件');
       document.template.FILE.focus();
       return false;
     }
     return true;
  }


</Script>
<html:html clazz="main">
    <form name="template" method="post" action="FinanceVoucherTrans.jspviewhigh?subFunction=save" enctype='multipart/form-data'>
      <!-- 返回信息栏 -->
      <html:message showReturn="false"/>

      <!-- 标题栏 -->
      <html:title clazz="module">财务凭证导入</html:title>

      <!-- 简单信息 -->
      <html:table clazz="simple">
        <tr>
          <td nowrap class="signText" >核算月：</td>
          <td nowrap class="normalText" align=right><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td>
          <td>文件：</td>
          <td><input type=file name="FILE"/></td>
        </tr>
        <tr>
        	<td nowrap class="signText" >清除本月数据：</td>
        	<td>
						<html:select property="yes_no"  name='clean'/>
					</td>
        	<td nowrap class="signText" >强制导入：</td>
        	<td>
						<html:select property="yes_no"  name='force'/>
						
					</td>
					<td><button class="pageBtn" onclick="return check()" >检查</button></td>
					<td>
					<button class="pageBtn" onclick="return trans()" >导入</button> 
					<!--<img src="images/transfers.gif" class="mouse" onclick="return trans()"/>--></td>
					<td><button class="pageBtn" onclick="return remove()">删除</button> </td>
				</tr>
	  	</html:table>
			<br>
			<% 
			String[][] result = (String[][])request.getAttribute("result");
			if (result!=null) {
			%>	  	
	  	<html:table clazz="result">
		    <html:tr clazz='label'>
          <td class="resultLabel">序号</td>
          <td class="resultLabel">记录内容</td>
          <td class="resultLabel"> 错误说明</td>
        </html:tr>

        <%
        for (int i = 0; i < result.length; i++ ) {
         	String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText"><%=(i+1)%></td>
          <td class="normalText"><%=result[ i ][ 0 ]%></td>
          <td class="normalText"><%=result[ i ][ 1 ]%></td>          
        </tr>
        <%}%>
			</html:table>
      <%}%>
    </form>
</html:html>
