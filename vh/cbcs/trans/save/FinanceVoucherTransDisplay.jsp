<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/save/FinanceVoucherTransDisplay.jsp,v 1.1 2012/03/12 01:59:04 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:59:04 $
  $Modtime:  $
  $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../error.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<html:html clazz="child">
<form name="template">

  <!-- 返回信息栏 -->
   <html:message/>

  <%

    String yearMonth = request.getParameter("year_month");

    if(yearMonth != null && !yearMonth.equals(""))
    {
      String year = yearMonth.substring(0, 4);
      String month = yearMonth.substring(4);
      if(yearMonth.substring(4,5).equals("0"))
      {
        month = yearMonth.substring(5,6);
      }
      yearMonth = year + "年" + month + "月";
    }
    String hospitalName = (String)request.getAttribute("hospitalName");
  %>
  <!-- 标题栏 -->
  <html:title clazz="module">财务凭证表</html:title>
  <html:table clazz="complex">
         <br>


          <tr>
        <td class="signText" align="right"><%=yearMonth%>&nbsp;&nbsp;金额单位：元</td>
        <td align=right><button class="pageBtn" onclick="window.close()" >关闭</button> 
        <!--<img src="images/close.gif" class="mouse" onclick="window.close()"/>--></td>

    			</tr>
  </html:table>

<html:table clazz="complex">

  <!-- 结果集 -->
  <tr>
    <td>
        <html:table clazz="result">
        <html:tr clazz='label'>
          <td class="resultLabel">成本单号</td>
          <td class="resultLabel">来源</td>
          <td class="resultLabel">凭证日期</td>
          <td class="resultLabel">摘要</td>
          <td class="resultLabel">部门代码</td>
          <td class="resultLabel">成本项目代码</td>
          <td class="resultLabel">金额</td>
          <td class="resultLabel">服务单号</td>
          <td class="resultLabel">服务材料代码</td>
          <td class="resultLabel">凭证类别</td>
          <td class="resultLabel">凭证号</td>
          <td class="resultLabel">录入人</td>
          <td class="resultLabel">月结标志</td>
        </html:tr>

        <%
          String[][] result = (String[][])request.getAttribute("table_result");
          DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
          if ( result != null )
          {
            for (int i = 0; i < result.length; i++ )
            {
              for (int j = 0; j < result[i].length; j++)
              {
                if (result[i][j] == null || result[i][j].equals("")){
                  if(j==6)
                    result[i][j]="0";
                  else
                    result[i][j] = "&nbsp;";
                }
                StringBuffer sb = new StringBuffer(result[i][j]);

                for(int m = 0; m < sb.length(); m++)
                {
                  if(sb.substring(m, m + 1).equals(" ")) {

                    sb.delete(m, m + 1);
                    sb.insert(m, "&nbsp;");
                  }
                }
                result[i][j] = sb.toString();
              }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";

        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText" nowrap><%=result[ i ][ 0 ]%></td>
          <td class="normalText" nowrap><%=result[ i ][ 1 ]%></td>
          <td class="normalText" nowrap><%=result[ i ][ 2 ].substring(0,10)%></td>
          <td class="normalText" nowrap><%=result[ i ][ 3 ]%></td>
          <td class="normalText" nowrap><%=result[ i ][ 4 ]%></td>
          <td class="normalText" nowrap><%=result[ i ][ 5 ]%></td>
          <td class="numberText" nowrap><%=moneyFormat.format(Double.parseDouble(result[i][6]))%></td>
          <td class="normalText" nowrap><%=result[ i ][ 7 ]%></td>
          <td class="normalText" nowrap><%=result[ i ][ 8 ]%></td>
          <td class="normalText" nowrap><%=result[ i ][ 9 ]%></td>
          <td class="normalText" nowrap><%=result[ i ][ 10 ]%></td>
          <td class="normalText" nowrap><%=result[ i ][ 11 ]%></td>
          <td class="normalText" nowrap><%=result[ i ][ 12 ]%></td>
        </tr>

        <%
              }
            }
        %>
   </html:table>
    </td>
  </tr>
  </html:table>
</form>
</html:html>

