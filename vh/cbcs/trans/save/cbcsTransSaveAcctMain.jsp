<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/save/cbcsTransSaveAcctMain.jsp,v 1.1 2012/03/12 01:59:04 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:59:04 $
  $Modtime: 03-09-22 0:50 $
  $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent"%>

<%@taglib uri="/WEB-INF/taglib/viewhigh-html.tld"prefix="html"%>

<Script Language="JavaScript">
	function trans() {
	  if (CheckInput()) {
	    show_wait();
  	  template.submit();
  	  return true;
  	} else {
  	  return false;
  	}
  }


  function CheckInput() {
     if(document.template.FILE.length==0) {
         alert('You must provide a name!');
         document.template.FILE.focus();
         return false;
     }
     FName=document.template.FILE.value;
     if (navigator.appVersion.indexOf('Win')>0) {
     FName=FName.substring(FName.lastIndexOf('\\')+1,FName.length);
     }else{
     FName=FName.substring(FName.lastIndexOf('/')+1,FName.length);
     }
     if ((navigator.appVersion.lastIndexOf('Win'))!=-1) {
         if((FName.indexOf('\\')>=0) || (FName.indexOf('/')>=0) || (FName.indexOf(':')>=0) || (FName.indexOf('*')>=0) || (FName.indexOf('?')>=0) || (FName.indexOf('"')>=0)|| (FName.indexOf('<')>=0) || (FName.indexOf('>')>=0) || (FName.indexOf('|')>=0)) {
             alert('请输入文件');
             document.template.FILE.focus();
             return false;
         };
     }else{
         if((FName.indexOf('\\')>=0) || (FName.indexOf(':')>=0) || (FName.indexOf('*')>=0) || (FName.indexOf('?')>=0) || (FName.indexOf('"')>=0)|| (FName.indexOf('<')>=0) || (FName.indexOf('>')>=0) || (FName.indexOf('|')>=0)) {
             alert('请输入文件');
             document.template.FILE.focus();
             return false;
         }
     }
     MyStr=document.template.FILE.value;
     i=0;
     while (MyStr.charAt(i)==' ') { i ++ }
     if (i==MyStr.length) {
         alert('请输入文件');
         document.template.FILE.focus();
         return false;
         }
     return true;
  }
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="cbcsTransSave.jspviewhigh?subFunction=save" enctype='multipart/form-data'>
      <!-- 返回信息栏 -->
      <table width="100%" border="0">
        <tr>
          <td>
            <table width="100%" border="0" >
      				<%
      					String message = (String)request.getAttribute("message");
      					if (message!=null && !message.trim().equals("")) {
      				%>
      				<tr>
      					<td class="successText" align="center"><%=message%></td>
      				</tr>
      				<%}
      					String appError = (String)request.getAttribute("appError");
      					if (appError!=null && !appError.trim().equals("")) {
      				%>
      				<tr>
      					<td class="errorText" align="center"><%=appError%></td>
      				</tr>
      				<%}%>
      			</table>
      	  <td>
      	<tr>
      </table>

      <!-- 标题栏 -->
      <table width="100%" border="0">
        <tr>
          <td>
            <table width="100%" border="0" >
      				<tr>
      				  <td  class="moduleTitle" nowrap="nowrap">财务数据导入：</td>
      				</tr>
      			</table>
      	  <td>
      	<tr>
      </table>

      <!-- 简单信息 -->
      <html:table clazz="simple">
        <tr>
          <td nowrap class="signText" >核算月： </td>
          <td nowrap class="normalText" align=right><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month",request.getParameter("year_month"))%></td>
					<td colspan=3></td>
        <tr>
				<br>
        <tr>
					<td nowrap class="signText">文件：</td>
          <td><input type=file name="FILE"/></td>
					<td><img src="images/transfers.gif" class="mouse" onclick="return trans()"/></td>
          <td>
          <button class="pageBtn" name=""  onclick="return win()"  >查询</button>
          <!--<img src="images/find.gif" class="mouse" onclick="return win()" />--></td>
					<td><button class="pageBtn" onclick="return remove();">删除</button></td>
        </tr>
	  </html:table>

</form>

 </html:html>
