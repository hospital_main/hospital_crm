<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/save/RptFundTransDisplay.jsp,v 1.1 2012/03/12 01:59:04 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:59:04 $
  $Modtime: 03-08-14 10:46 $
  $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../error.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<html:html clazz="child">
<form name="template">

  <!-- 返回信息栏 -->
<html:message/>

  <%

    String yearMonth = request.getParameter("year_month");

    if(yearMonth != null && !yearMonth.equals(""))
    {
      String year = yearMonth.substring(0, 4);
      String month = yearMonth.substring(4);
      if(yearMonth.substring(4,5).equals("0"))
      {
        month = yearMonth.substring(5,6);
      }
      yearMonth = year + "年" + month + "月";
    }
    String[][] resultt = (String[][])request.getAttribute("table_result");
    String hospitalName = "";
    if( resultt != null )
    {
      hospitalName = resultt[0][0];
    }

  %>
  <!-- 标题栏 -->
    <html:title clazz='module'>基金变动情况表</html:title>
      <br>
    <html:table clazz="complex">
    <tr>
<td class="signText" align="right"><%=yearMonth%>&nbsp;&nbsp;金额单位：元</td>
<td align=right><button class="pageBtn" onclick="window.close()" >关闭</button> 
<!--<img src="images/close.gif" class="mouse" onclick="window.close()"/>--></td>


          </tr>

  </html:table>




  <html:table clazz="complex">

  <!-- 结果集 -->
  <tr>
    <td>
       <html:table clazz="result">
	        <html:tr clazz='label'>
          <td class="resultLabel">项目</td>
          <td class="resultLabel">行次</td>
          <td class="resultLabel">合计</td>
          <td class="resultLabel">事业基金</td>
          <td class="resultLabel">专用基金小计</td>
          <td class="resultLabel">修购基金</td>
          <td class="resultLabel">福利基金</td>
          <td class="resultLabel">留本基金</td>
          <td class="resultLabel">其他基金</td>
          <td class="resultLabel">固定基金</td>
        </html:tr>

        <%
          String[][] result = (String[][])request.getAttribute("table_result");
          DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");

          if ( result != null )
          {
            for (int i = 0; i < result.length; i++ )
            {
              for(int j = 4; j < result[i].length; j++)
              {
                if(!result[i][j].trim().equals(""))
		{
                  if(result[i][j].trim().equals("0.00")||result[i][j].trim().equals("0"))
                  {
                    result[i][j] = "&nbsp;";
                  }
		  else
                  {
                    result[i][j] =
                      moneyFormat.format(Double.parseDouble(result[i][j]));
                  }
		}
              }

              for (int j = 0; j < result[i].length; j++)
              {
                if (result[i][j] == null || result[i][j].equals(""))
                  result[i][j] = "&nbsp;";
                StringBuffer sb = new StringBuffer(result[i][j]);

                for(int m = 0; m < sb.length(); m++)
                {
                  if(sb.substring(m, m + 1).equals(" ")) {

                    sb.delete(m, m + 1);
                    sb.insert(m, "&nbsp;");
                  }
                }
                result[i][j] = sb.toString();
              }

          String rowColor = "rowGray";
          if (i/2*2==i) rowColor = "rowWhite";

        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText" align="left"><%=result[ i ][ 3 ]%></td>
          <td class="normalText" align="right"><%=result[ i ][ 4 ]%></td>
          <td class="normalText" align="right"><%=result[ i ][ 5 ]%></td>
          <td class="normalText" align="right"><%=result[ i ][ 6 ]%></td>
          <td class="normalText" align="right"><%=result[ i ][ 7 ]%></td>
          <td class="normalText" align="right"><%=result[ i ][ 8 ]%></td>
          <td class="normalText" align="right"><%=result[ i ][ 9 ]%></td>
          <td class="normalText" align="left"><%=result[ i ][ 10 ]%></td>
          <td class="normalText" align="right"><%=result[ i ][ 11 ]%></td>
          <td class="normalText" align="right"><%=result[ i ][ 12 ]%></td>
        </tr>

        <%
              }
            }
        %>
      </html:table>
    </td>
  </tr>
  </html:table>
</form>

</html:html>
