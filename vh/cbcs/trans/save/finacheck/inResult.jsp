<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/save/finacheck/inResult.jsp,v 1.1 2012/03/12 01:59:04 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:59:04 $
  $Modtime: 03-09-22 0:50 $
  $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent"%>

<%@taglib uri="/WEB-INF/taglib/viewhigh-html.tld"prefix="html"%>
<script language="javascript">
function back(){
  template.submit();}</script>

<html:html clazz="main">
    <form name="template" method="post" action="FinanceVoucherTrans.jspviewhigh?subFunction=show">
      <html:title clazz="module">财务凭证数据导入结果信息</html:title>
	      <html:table clazz="simple">
         <tr>
           <td><div align="right"> <button class="pageBtn" onclick="back()">返回</button> 
</div></td>
         </tr>
        </html:table>
        <%
        int[] counts=(int [])request.getAttribute("coun");
        String str="hehe";%>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
            <td>行号</td>
            <td>记录内容</td>
            <td>错误说明</td>
	        </html:tr>
          <tr>
          <td> </td>
          <td>表中共有<%=counts[0]%>条数据本次操作共导入<%=counts[1]%>条数据 </td>
          <td></td>
          </tr>

          <%String [][] errdata=(String[][])request.getAttribute("errmess");
          if(errdata!=null){
            for(int j=0;j<counts[0];j++){
              if(errdata[j][2]!=null&&!errdata[j][2].equals("此行为空行")){%>
          <tr>
          <td><%=errdata[j][0]%></td>
          <td><%=(errdata[j][1].replaceAll(";@",""))%></td>
          <td><%=errdata[j][2]%></td>
          </tr>
          <%}else{ if(errdata[j][2]!=null&&errdata[j][2].equals("此行为空行")){
            str="注意：数据表中存在空数据行！";}
           }}%>
           <%if(!str.equals("hehe")){%>
          <tr>
          <td></td>
          <td></td>
          <td><%=str%></td>
          </tr>
        <%}}%>
	      </html:table>
</form>

 </html:html>
