<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/query/StEfficiencyDeptSave.jsp,v 1.2 2013/01/15 05:17:17 liyan Exp $
 $Author: liyan $
 $Date: 2013/01/15 05:17:17 $
 $Modtime: 03-08-15 8:37 $
 $Revision: 1.2 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
                 com.viewhigh.cbcs.base.mvc.view.component.*,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge,
                 java.text.DecimalFormat " %>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  { if(template.total_bed_used_days.value == "")
    {
      alert('占用床位总数不能为空!');
      return;
    }
     if(template.bed_used_rates.value == "")
    {
      alert('床位使用率不能为空!');
      template.bed_used_rates.focus();
      return;
    }
    if( isNaN(parseFloat( template.bed_used_rates.value ) ) )
    {
      alert('床位使用率必须为数字!');
      template.bed_used_rates.focus();
      return;
    }
     if( isNaN(parseFloat( template.operated_num.value ) ) && template.operated_num.value!='' )
    {
      alert('手术例数必须为数字!');
      template.operated_num.focus();
      return;
    }
    /*if ((parseFloat( template.bed_used_rates.value ) > 1) || (parseFloat( template.bed_used_rates.value ) < 0))
    {
      alert('床位使用率必须在零和壹之间!');
      template.bed_used_rates.focus();
      return;
    }*/
    if(template.admitted_outps.value!=""){
    switch(isDouble(template.admitted_outps,16,0)){
      case 0 : alert('门诊入院人数必须为数字型'); return;
      case 1 : alert('门诊入院人数整数部分不能高于16个字符'); return;
      case 2 : alert('门诊入院人数没有整数部分'); return;
      case 3 : alert('门诊入院人数不能有小数部分'); return;
    }
    }
      switch(isDouble(template.total_bed_used_days,16,0)){
        case 0 : alert('占用床位总数必须为数字型'); return;
        case 1 : alert('占用床位总数整数部分不能高于16个字符'); return;
        case 2 : alert('占用床位总数没有整数部分'); return;
        case 3 : alert('占用床位总数不能有小数部分'); return;
      }
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  {
  	/* for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='preparedFindAll';
    template.initsub.value="sub";
    template.submit();
    return true;
    */
	template.subFunction.value="findAll";
	template.signs.value='findAll';
	show_wait();
	template.submit();
  }
  // 计算床位使用率
  function countBedUsedRates()
  {
	if (trim(template.total_in_dept_days.value) == ''
			|| trim(template.total_in_dept_days.value) == '0'
			|| trim(template.total_bed_used_days.value) == ''
			|| trim(template.total_bed_used_days.value) == '0') 
	{
		template.bed_used_rates.value = '0.00';	
	} 
	else 
	{
		var result = template.total_bed_used_days.value/template.total_in_dept_days.value;
		template.bed_used_rates.value = isNaN(result) ? '0.00' : result;
	}
  }
</Script>
<%
  String[][] dept = (String[][]) request.getAttribute("dept");
  String[] result=(String[])request.getAttribute("result");
%>
<html:html clazz="main">
<form name="template" method="post" action="StEfficiencyDept.jspviewhigh">
 <!-- 信息提示栏 -->
  <html:message/>
  <!-- 标题栏 -->
  <html:title clazz='module'>住院工作量修改页面</html:title>
  <!-- 简单信息 -->
   <html:table clazz="simple">
     <tr>
      <td nowrap class="signText" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;核算月： </td>
      <td nowrap class="normalText">
        <input type='text' name="year_month" value='<%=result[0]%>' disabled style="width:140px;"/>
      </td>
      <td nowrap class="signText" >&nbsp;&nbsp;&nbsp;&nbsp;科室名称： </td>
      <td nowrap class="normalText">
         <input type='text'  value='<%=result[1]%>' disabled style="width:140px;"/>
      </td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;患者费别：</td>
      <td class="normalText" nowrap="nowrap">
        <input type='text' name="charge_kind_name" value='<%=result[2]%>' disabled style="width:140px;"/>
      </td>
      <td class="signText" nowrap="nowrap">门诊入院人数：</td>
      <td class="normalText" nowrap="nowrap">
        <input type='text' name="admitted_outps" class="textInputC" value='<%=result[3]%>'/></td>
    </tr>
    </tr>
      <td class="signText" nowrap="nowrap">出院人数：</td>
      <td class="normalText" nowrap="nowrap"><input type='text' name="discharge_num" class="textInputC", value='<%=result[4]%>' /></td>
      <td class="signText" nowrap="nowrap">&nbsp;&nbsp;实际开放床日数：</td>
      <td class="normalText" nowrap="nowrap"><input type='text' name="total_in_dept_days" class="textInputC" value='<%=result[5]%>' onchange="countBedUsedRates(this);"/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">占用床位总数：</td>
      <td class="normalText" nowrap="nowrap">
        <input type='text' name="total_bed_used_days" class="textInputC" value='<%=result[6]%>' onchange="countBedUsedRates(this);"/></td>
      <td class="signText" nowrap="nowrap">&nbsp;&nbsp;床位使用率：</td>
      <td class="normalText" nowrap="nowrap"><input type='text' name="bed_used_rates" class="textInputC" readonly="readonly" value='<%=new DecimalFormat("##,##0.00").format(Double.parseDouble(result[8]))%>'/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">手术例数：</td>
      <td class="normalText" nowrap="nowrap"><input type='text' name="operated_num" class="textInputC"
      	value='<%=result[10]%>'/></td>
    </tr>
    <tr>
      <td colspan="4" align="center">
      <button class="pageBtn" onclick="return save();">保存</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="return back(template);">返回</button>      
       <!--<img src="images/save.gif" class="mouse" onclick="return save();" /> 
       <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
   </html:table>
   <input type="hidden" name="initsub" value="sub"/>
   <input type='hidden' name="dept_code" value="<%=result[7]%>"/>
   <input type='hidden' name="year_month" value="<%=result[0]%>"/>
   <input type='hidden' name="charge_kind" value="<%=result[9]%>"/>
  <input type='hidden' name="subFunction" value="save"/>
  
  <input type="hidden" name="signs" value="findAll" />
</form>
</html:html>


