<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/query/StEfficiencyDept.jsp,v 1.3 2013/08/14 05:03:31 liyan Exp $
  $Author: liyan $
  $Date: 2013/08/14 05:03:31 $
  $Modtime:$
  $Revision: 1.3 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
                 com.viewhigh.cbcs.base.util.ExtendTool,
                 com.viewhigh.cbcs.base.mvc.view.component.*,
                 com.viewhigh.cbcs.base.sql.BaseRO,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge " %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="javascript">
	function create() {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
		template.subFunction.value='preparedCreate';
    show_wait();
    template.submit();
		return true;
	}

  function remove() {
    var flag = false;
    template.signs.value='findAll';
    for (var i=0; i<template.elements.length; i++) {
  		if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
  		  flag = true;
  	}

    if( flag!=false) {
    	if (confirm('是否删除')) {
	      template.subFunction.value='remove';
	      template.submit();
	      return true;
	    } else
	    	return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
    	if (template.elements[i].name=='primaryKey' && template.elements[i].disabled==false)
    		template.elements[i].checked = true;
    }
  }
function find(){
 template.signs.value='findAll';
 show_wait();
 template.submit();
 return true;
}

function accout() {
template.signs.value='collect';
template.subFunction.value='findAll';   
var d=new Date();
var s=d.toString();
window.showModalDialog("StEfficiencyDept.jspviewhigh?signs="+template.signs.value
       +"&subFunction="+template.subFunction.value 
        +"&yearMonthF=" +template.yearMonthF.value
        +"&yearMonthT=" +template.yearMonthT.value
        +"&dept=" +template.dept.value+"&dates="+s,window,"dialogHeight: 140px; dialogWidth: 500px;");
			
	}

function importData(obj,v){
		   		
				var fileName=fileUploader.choose(1);
				if(fileName.length<4&&fileName.indexOf(".")<0)
					return false;
				var fileType=fileName.substr(fileName.lastIndexOf(".")+1);
				fileUploader.submit(function(m,f){
						if( m != ''){
							alert(m);
							return false;
						}
						for(var k in f){
							fileType+="="+k;
							break;
						}
						var msg = "" ;
						var queryParams = "<comp_file>"+fileType+"</comp_file>" ;
						confirmImportStyleDialog(function(v){
								if(v==0)
									return;

					        window.xmlhttp.post("hosDictsUnitinfoStefficiencydept_import",queryParams+"<q_flag>"+v+"</q_flag>");
					        
					         var responseText = window.xmlhttp._object.responseText ;
					          msg = window.doMsg(responseText,"show")
						  if(msg==true) {
							template.subFunction.value='findAll';
							template.signs.value='findAll';
							show_wait();
					    		template.submit();
					    		return true;
						  }
						  });
					},obj
				)	
			}
</script>
<%String[][] dept = (String[][]) request.getAttribute("dept");%>
<html:html clazz="main">
<form name="template" method="post" action="StEfficiencyDept.jspviewhigh">

  <!-- 返回信息栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>住院工作量主页面</html:title>

  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText" >起止年月： </td>
      <td nowrap class="normalText" colspan='3'>
	      <table cellspacing='0' border='0' cellpadding='0'>
	        <tr>
	          <td>
	          	<%=new MonthComponent("yearMonthF",request.getParameter("yearMonthF"))%>
	          </td>
	          <td class='normalText'> &nbsp;至&nbsp; </td>
	          <td>
	          	<%=new MonthComponent("yearMonthT",request.getParameter("yearMonthT"))%>
	          </td>
	        </tr>
	      </table>      	
      </td>  
      <td nowrap class="signText" >科室名称： </td>
     <!-- <td>
         <%=new SingleSelect(dept, "dept", request.getParameter("dept"), true, false)%>
      </td>-->
      <td><?xml:namespace prefix="hzh"/>
        	<hzh:QInput ID="nosNamea1" name="dept" value="<%=request.getParameter("dept")==null? "":request.getParameter("dept")%>"  AdjustVal="142" previousObj="amount" indexCodeSequence="dept_code|dept_name|spell" codeCol='dept_code' textCol="dept_name" width="140" top="41" left="83" Lheight="5" xmlSource="dic/dept_name_i.xml" init="1"/>
        </td>
      <td align="right">
      <button class="pageBtn" name=""   onclick="find()"  >查询</button> 
      <button class="pageBtn" name=""   onclick="accout()"  >汇总</button>
 <button class="pageBtn" name="hosDictsUnitinfoStefficiencydept_import"
						onclick="importData(this,1)" >导入</button>   
        <!--<img src="images/find.gif" class="mouse" onclick="find()" />
        <img src="images/huizong.GIF" class="mouse" onclick="accout()" />
        <img src="images/transfers.gif" class="mouse" name="hosDictsUnitinfoStefficiencydept_import"
						onclick="importData(this,1)" />-->
      </td>
                   
    </tr>
	  </html:table>
    <%	DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
	      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
	      TableMarge oper = new TableMarge(ro, "return find()");
	      oper.addOptionButton("images/selectedAll.gif", "return selectAll()");// 全选
	      oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
	      oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
	      oper.addNeedButton("images/create.gif", "return create()");     //  添加
	  %>
  <br>
 <html:title clazz='table'>住院工作量</html:title>
  <!-- 复杂信息 -->
	  <html:table clazz="complex">
  <tr><td><%=oper%></td></tr>
  <!-- 结果集 -->
  <tr>
    <td>

	      <html:table clazz="result">
	        <html:tr clazz='label'>
            <td class="resultLabel">选择</td>
            <td class="resultLabel" nowrap="nowrap">统计年月</td>
            <td class="resultLabel" nowrap="nowrap">患者费别</td>
            <td class="resultLabel" nowrap="nowrap">科室名称</td>
            <td class="resultLabel" nowrap="nowrap">门诊入院人数</td>
			<td class="resultLabel" nowrap="nowrap">出院人数</td>
			<td class="resultLabel" nowrap="nowrap">实际开放床日数</td>
            <td class="resultLabel" nowrap="nowrap">占用总床日数</td>
            <td class="resultLabel" nowrap="nowrap">录入人</td>
            <td class="resultLabel" nowrap="nowrap">录入时间</td>
            <td class="resultLabel" nowrap="nowrap">床位使用率</td>
<td class="resultLabel" nowrap="nowrap">手术例数</td>
	        </html:tr>

        <% if (ro!=null) {
          String[][] result = ro.getTableResult();
          if (result != null) {
            for (int i = 0; i < result.length; i++) {
//            System.out.println(result[0].length);
            boolean ischeckout=Boolean.valueOf((String)result[i][9]).booleanValue();
//              String[] temp = {result[i][0], result[i][1],result[i][7],};
				String[] temp = {result[i][0], result[i][12],result[i][9],};
               String primaryKey = ExtendTool.arrayToString(temp);
		              for (int j=0; j<result[i].length; j++) {
		                if (result[i][j]!=null && result[i][j].equals("")) result[i][j]="&nbsp;";
		              }
              String rowColor = "rowGray";
              if (i/2*2 == i) {
                rowColor = "rowWhite";
              }
        %>

        <tr CLASS="<%=rowColor%>">
<%if(!result[i][0].equals("合计")){%>        	
<td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>" <% if(ischeckout) out.print("disabled='disabled'");%>></td>
<%}else{%>
<td></td>
<%}%>
<td class="normalText"><%if(ischeckout || result[i][0].equals("合计")){%><%=result[i][0]%><%}else{%><a href="StEfficiencyDept.jspviewhigh?subFunction=preparedSave&primaryKey=<%=primaryKey%>"><%=result[i][0]%></a><%}%></td>
<td class="normalText"><%=result[i][1]%></td>
<td class="normalText"><%=result[i][2]%></td>
<td class="numberText"><%=moneyFormat.format(Double.parseDouble(result[i][3]))%></td>
<td class="numberText"><%=moneyFormat.format(Double.parseDouble(result[i][4]))%></td>
<td class="numberText"><%=moneyFormat.format(Double.parseDouble(result[i][5]))%></td>
<td class="numberText"><%=moneyFormat.format(Double.parseDouble(result[i][6]))%></td>
<td class="normalText"><%=result[i][7]%></td>
<td class="normalText"><%=result[i][8]%></td>
<td class="numberText"><%=result[i][10]%></td>
<td class="numberText"><%=result[i][11]%></td>
        </tr>

        <%
            }
          }
        }
        %>
	      </html:table>
    </td>
  </tr>
	  </html:table>

  <input type="hidden" name="subFunction" value="findAll" />
  <input type="hidden" name="signs" value="findAll" />
</form>
<div id="upload_file_div" style="position:absolute;display:none;">
	    <iframe src="uploadFileD.cmd" id="uploadFrame"></iframe>
	</div>
	<script Language="JavaScript" >
		_____VHFileCreatFromTimerss=window.setInterval(createVhFile,10);
		  function createVhFile(){
		    if(typeof(uploadFrame)=="undefined")
		      return ;
		    window.clearInterval(_____VHFileCreatFromTimerss);
		    fileUploader=new VhFileUploader(uploadFrame);
		  }
	</script>
</html:html>
