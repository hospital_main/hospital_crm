<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/query/deptPerformStat.jsp,v 1.1 2012/03/12 01:59:04 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:04 $
 $Modtime:$
 $Revision: 1.1 $
 $NoKeywords:$
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.base.mvc.view.component.*,com.viewhigh.cbcs.cbcs.util.DisplayWidth,
  com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<%
  String[][] incomeType={{"T","医疗收入"},{"M","药品收入"}};
  String[][] deptType={{"O","门诊"},{"I","住院"}};
  String[][] depts = (String[][]) request.getAttribute("depts");
  String[][] result = (String[][]) request.getAttribute("table_result");
  String queryType = request.getParameter("queryType");
  DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
%>

<Script language="javascript">
  function query() {
    if (template.yearMonthF.value == '') {
      alert('请选择起始核算年月!');
      return false;
    }
		if (template.yearMonthT.value == '') {
      alert('请选择终止核算年月!');
      return false;
    }
		var deptCodeValue = document.getElementsByName("zbbTest")[0].value;
		if(deptCodeValue == ''){
      alert('请指定执行科室!');
      return false;
    }
    show_wait();
    template.submit();
    return true;
  }
</Script>
<html:html clazz="main" fixRows="1">
<form name="template" method="post" action="stDeptIncome.jspviewhigh">

	<!-- 返回信息栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>科室执行统计</html:title>

  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText">起止年月：</td>
      <td nowrap class="normalText" colspan='1'><%=new BiMonthComponent("yearMonthF", request.getParameter("yearMonthF"),"yearMonthT",request.getParameter("yearMonthT"))%></td>
       <td nowrap class="signText" >收入类型： </td>
      <td nowrap class="normalText">
          <%=new SingleSelect(incomeType, "incomeType", request.getParameter("incomeType"), false, false)%>
      </td>
    </tr>
     <tr>
     	 <td nowrap class="signText" >&nbsp;&nbsp;&nbsp;&nbsp;执行科室： </td>
     <!-- <td>
         <%=new SingleSelect(
          depts, "deptCode", request.getParameter("deptCode"), true, false)
        %>
      </td>-->
      <td><?xml:namespace prefix="hzh"/>
        <hzh:QInput ID="nosNamea1" name="deptCode" value="<%=request.getParameter("deptCode")==null? "":request.getParameter("deptCode")%>" AdjustVal="145" previousObj="amount"  indexCodeSequence="dept_code|dept_name|spell"  codeCol='dept_code' textCol="dept_name" width="140" top="13" left="83" Lheight="5" xmlSource="dic/asst_dict_acct_dept_LTD.xml" init="1"/>
        </td>
    
      <td nowrap class="signText" >开单科室属性： </td>
      <td>
         <%=new SingleSelect(deptType, "deptType", request.getParameter("deptType"), false, false)%>
      </td>
    </tr>
	<tr>
      <td colspan="4" align="right"> <button class="pageBtn" name=""   onclick="return query();"  />查询</button> 
	  <!--<img src="images/find.gif" class="mouse" onclick="return query();" />-->
	</tr>
	  </html:table>

  <br>
          <html:title clazz='table'>科室执行统计</html:title>
  <!-- 复杂信息 -->
 <vh:vhFixTable fixRow=1 fixCol=0>
	      <table  width="100%" class="resultSetTable">
		    <colgroup id=tg>
        <col style = 'width:54mm' >
          
          <col style = 'width:54mm' >
          
          <col style = 'width:55mm' >
          
          <col style = 'width:55mm' >
          
        </colgroup>	            
	        <tr class="resultLabel">
            <td nowrap class="resultLabel">执行科室</td>
            <td nowrap class="resultLabel">收费类别</td>
            <td nowrap class="resultLabel">开单科室</td>
            <td nowrap class="resultLabel">金额</td>
	        </tr>

        <%
        if (result != null) {
          String total[] = null;

          for (int i = 0; i < result.length; i++) {
            String rowColor = "rowGray";
            for (int j = 0; j < result[i].length; j++) {
              if (result[i][j] == null || result[i][j].equals("")) {
                result[i][j] = "&nbsp;";
              }
            }

          if (i/2*2==i) rowColor = "rowWhite";

          if (result[i][1].equals("总计")) {
            total = result[i];
            continue;
          }


          if (result[i][3].equals("合计")) {
            int rowCount = 1;
            for (int j = i + 1; j < result.length; j++) {
              if (result[j][0].equals(result[i][0])) {
                rowCount++;
              } else {
                  break;
              }
            }
        %>
        <tr height="22">
          <td class="normalText" rowspan=<%=rowCount%>><%=result[ i ][ 1 ]%></td>
          <td class="normalText"><%=result[ i ][ 3 ]%></td>
          <td class="normalText"><%=result[ i ][ 5 ]%></td>
          <td class="numberText"><%=moneyFormat.format(Double.parseDouble(result[i][6]))%></td>
        </tr>
        <%
          } else if (result[i][5].equals("小计")) {
            int rowCount = 1;
            for (int j = i + 1; j < result.length; j++) {
              if (result[j][0].equals(result[i][0])
                && result[j][2].equals(result[i][2])) {
                rowCount++;
              } else {
                break;
              }
            }
        %>
        <tr height="22">
          <td class="normalText" rowSpan=<%=rowCount%>>
            <%=result[ i ][ 3 ]%>
          </td>
          <td class="normalText"><%=result[ i ][ 5 ]%></td>
          <td class="numberText"><%=moneyFormat.format(Double.parseDouble(result[i][6]))%></td>
        </tr>
        <%
          } else {
        %>
        <tr height="22">
          <td class="normalText"><%=result[ i ][ 5 ]%></td>
          <td class="numberText"><%=moneyFormat.format(Double.parseDouble(result[i][6]))%></td>
        </tr>

        <%
          }

        }
        if (total != null && result.length != 1) {
        %>
        <tr height="22">
          <td class="normalText"><%=total[ 1 ]%></td>
          <td class="normalText"><%=total[ 3 ]%></td>
          <td class="normalText"><%=total[ 5 ]%></td>
          <td class="numberText"><%=moneyFormat.format(Double.parseDouble(total[6]))%></td>
        </tr>
      <%
        }
      }
      %>
	 	     </table>
</vh:vhFixTable>

  <input type="hidden" name="subFunction" value="groupByPerformCharge" />
</form>
 <script>
  
  document.getElementById("incomeType").style.width=140;
  document.getElementById("deptType").style.width=140;
  </script>
</html:html>


