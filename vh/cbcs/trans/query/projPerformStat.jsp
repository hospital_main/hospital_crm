<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/query/projPerformStat.jsp,v 1.1 2012/03/12 01:59:04 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:04 $
 $Modtime: 03-08-14 9:56 $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
  com.viewhigh.cbcs.base.mvc.view.component.*,com.viewhigh.cbcs.cbcs.util.DisplayWidth,
  com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="javascript">
function find(){
    if (template.yearMonthF.value == '') {
      alert('请选择起始核算年月!');
      return false;
    }
		if (template.yearMonthT.value == '') {
      alert('请选择终止核算年月!');
      return false;
    }
    show_wait();
 template.submit();
 return true;
}
</script>
<%
  String[][] chargeKind = (String[][]) request.getAttribute("chargeKind");
  String[][] incomeType={{"T","医疗收入"},{"M","药品收入"}};
  String[][] deptType={{"O","门诊"},{"I","住院"}};
%>
<html:html clazz="main" fixRows="1">
 <form name="template" method="post" action="stDeptIncome.jspviewhigh">

	<!-- 返回信息栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>项目执行统计</html:title>

  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText">起止年月：</td>
      <td nowrap class="normalText" colspan='1'><%=new BiMonthComponent("yearMonthF", request.getParameter("yearMonthF"),"yearMonthT",request.getParameter("yearMonthT"))%></td>
     	<td nowrap class="signText" >收入类型： </td>
      <td nowrap class="normalText">
          <%=new SingleSelect(incomeType, "incomeType", request.getParameter("incomeType"), false, false)%>
      </td>
    </tr>
    <tr>
    	<td nowrap class="signText" >收费类别： </td>
     <!-- <td>
         <%=new SingleSelect(
          chargeKind, "chargeKind", request.getParameter("chargeKind"), true, false)
        %>
      </td>-->
      <td><?xml:namespace prefix="hzh"/>
        	<hzh:QInput ID="nosNamea1" name="chargeKind" value="<%=request.getParameter("chargeKind")==null? "":request.getParameter("chargeKind")%>"  AdjustVal="142" previousObj="amount" indexCodeSequence="charge_kind_code|charge_kind_name|spell" codeCol='charge_kind_code' textCol="charge_kind_name" width="140" top="41" left="83" Lheight="5" xmlSource="dic/dict_charge_detail_kind.xml" init="1"/>
        </td>
      
      <td nowrap class="signText" >开单科室属性： </td>
      <td>
         <%=new SingleSelect(deptType, "deptType", request.getParameter("deptType"), false, false)%>
      </td>
    </tr>
	<tr>
      <td colspan="4" align="right"> <button class="pageBtn" name=""   onclick="find()"  />查询</button>  
	  <!--<img src="images/find.gif" class="mouse" onclick="find()" />-->
	</tr>
	  </html:table>

  <br>

          <html:title clazz='table'>项目执行统计</html:title>
  <!-- 复杂信息 -->
	   <vh:vhFixTable fixRow=1 fixCol=0>
	      <table  width="100%"class="resultSetTable" >
		    <colgroup id=tg>
          
          <col style = 'width:55mm' >
          
          <col style = 'width:55mm' >
          
          <col style = 'width:55mm' >
          
        </colgroup>	        

          <tr class="resultLabel">
            <td nowrap class="resultLabel">收费类别</td>
            <td nowrap class="resultLabel">执行科室</td>
            <td nowrap class="resultLabel">金额</td>
	        </tr>

        <%
          DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
          String[][] result = (String[][])request.getAttribute( "table_result" );
            if ( result != null )
            {
	      String total[] = null;


              for (int i = 0; i < result.length; i++)
              {
                String rowColor = "rowGray";

                for (int j=0; j<result[i].length; j++)
                {
                  if (result[i][j] == null || result[i][j].equals("")) result[i][j] = "&nbsp;";
                }

                // "合计"
		if(result[i][1].equals("合计"))
                {
                  total = result[i];
                  continue;
		}

                // "科室细项"
                else if(!result[i][2].equals("&nbsp;"))
                {
                  result[i][1] = "&nbsp;";
                  rowColor = "rowWhite";
		}


          //String rowColor = "rowGray";
          //if (i/2*2==i) rowColor = "rowWhite";
        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText"><%=result[ i ][ 1 ]%></td>
          <td class="normalText"><%=result[ i ][ 3 ]%></td>
          <td class="numberText"><%=moneyFormat.format(Double.parseDouble(result[ i ][ 4 ]))%></td>
        </tr>

        <%
              }
              if(total!=null && !total[4].equals("0"))
              {
        %>

        <tr CLASS="rowGray">
          <td class="normalText"><%=total[ 1 ]%></td>
          <td class="normalText"><%=total[ 3 ]%></td>
          <td class="numberText"><%=moneyFormat.format(Double.parseDouble(total[ 4 ]))%></td>
        </tr>

        <%
              }
            }
        %>
	 	     </table>
</vh:vhFixTable>

  <input type="hidden" name="subFunction" value="groupByChargePerform" />
</form>
 <script>
  
  document.getElementById("incomeType").style.width=140;
  document.getElementById("deptType").style.width=140;
  </script>
</html:html>


