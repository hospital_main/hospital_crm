<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/query/TechnicianDeptCreate.jsp,v 1.3 2014/02/27 07:26:44 yuchengying Exp $
 $Author: yuchengying $
 $Date: 2014/02/27 07:26:44 $
 $Modtime: 03-08-15 8:37 $
 $Revision: 1.3 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
                 com.viewhigh.cbcs.base.mvc.view.component.*,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge " %>

<Script Language="JavaScript">
  function create()
  { if(template.yearMonth.value=="")
    {
      alert('核算月不能为空!');
      return false;
    }
    
    if(template.dept.value=="")
    {
      alert('科室名称不能为空!');
      return false;
    }
    if(template.workload.value=="")
    {
      alert('工作量不能为空!');
      return false;
    }

    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='preparedFindAll';
    template.initsub.value="sub";
    template.submit();
    return true;
  }
</Script>
<%
  String[][] dept = (String[][]) request.getAttribute("dept");
%>
<html:html clazz="main">
<form name="template" method="post" action="TechnicianDept.jspviewhigh">
 <!-- 信息提示栏 -->
  <html:message/>
  <!-- 标题栏 -->
  <html:title clazz='module'>医技科室工作量维护添加页面</html:title>
  <!-- 简单信息 -->
   <html:table clazz="simple">
     <tr>
      <td nowrap class="signText" style="text-align:right" >核算月： </td>
      <td nowrap class="normalText"><%=new MonthComponent("yearMonth",request.getParameter("yearMonth"))%></td>
      <td nowrap class="signText" style="text-align:right"  >科室名称： </td>
      <!--<td>
         <%=new SingleSelect(dept, "dept", request.getParameter("dept"), true, true)%>
      </td>-->
      <td><?xml:namespace prefix="hzh"/>
        	<hzh:QInput ID="nosNamea1" name="dept" value="<%=request.getParameter("dept")==null? "":request.getParameter("dept")%>"  AdjustVal="142" previousObj="amount" indexCodeSequence="dept_code|dept_name|spell" codeCol='dept_code' textCol="dept_name" width="140" top="41" left="83" Lheight="5" xmlSource="dic/dept_name_t_V.xml" init="1"/>
        </td>
      <td class="signText" nowrap="nowrap" style="text-align:right" >工作量：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="workload"  class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="6" align="center"> <button class="pageBtn" onclick="return create();" >添加</button> 
     
      <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return back(template);">返回</button>  
     <!--  <img src="images/create.gif" class="mouse" onclick="return create();" /><img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
   </html:table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>
</html:html>