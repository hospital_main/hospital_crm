<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/query/StAsstTreatDept.jsp,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:59:03 $
  $Modtime:$
  $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="error.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>

<%
  String[][] result = (String[][])request.getAttribute("table_result");
%>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<html:html clazz="main">
<form name="template" method="post" action="StAsstTreatDept.jspviewhigh">

  <!-- 返回信息栏 -->
	  <html:message/>
  <!-- 标题栏 -->
	  <html:title clazz='module'>医技工作量</html:title>

  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td nowrap class="signText" >核算月： </td>
      <td nowrap class="normalText" colspan='3'><%=new MonthComponent("yearMonth",request.getParameter("yearMonth"))%></td>

      <td>
      <button class="pageBtn" name="" onclick="template.submit();">查询</button>
      <!-- <img src="images/find.gif" class="mouse" onclick="template.submit();" />--></td>
    </tr>
	  </html:table>

  <br>
          <html:title clazz='table'>医技工作量</html:title>
  <!-- 复杂信息 -->
	  <html:table clazz="complex">

  <!-- 结果集 -->
  <tr>
    <td>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
          <td class="resultLabel">科室名称</td>
          <td class="resultLabel">项目名称</td>
          <td class="resultLabel">患者费别</td>
          <td class="resultLabel">治疗人次</td>
          <td class="resultLabel">录入人</td>
          <td class="resultLabel">录入时间</td>
	        </html:tr>

        <%
          if (result != null) {
            for (int i = 0; i < result.length; i++) {
              String rowColor = "rowGray";
              if (i/2*2 == i) {
                rowColor = "rowWhite";
              }
        %>

        <tr CLASS="<%=rowColor%>">
          <td class="normalText"><%=result[i][1]%></td>
          <td class="normalText"><%=result[i][3]%></td>
          <td class="normalText"><%=result[i][4]%></td>
          <td class="normalText"><%=result[i][5]%></td>
          <td class="normalText"><%=result[i][6]%></td>
          <td class="normalText"><%=result[i][7]%></td>
        </tr>

        <%
            }
          }
        %>
	      </html:table>
    </td>
  </tr>
	      </html:table>

  <input type="hidden" name="subFunction" value="findAll" />
</form>

</html:html>
