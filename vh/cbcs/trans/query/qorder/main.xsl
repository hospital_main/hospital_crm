<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<thead>
		<xsl:for-each select="/root/tbody/tr[position() &lt; 3]">
				<xsl:variable name="curRow" select="position()" />
				<tr noWrap="true" class="mainHead">
					<xsl:for-each select="td">
						<xsl:variable name="curcode" select="."/>
          	<xsl:variable name="curCol" select="position()" />	
          	<xsl:variable name="colspan" select="count(/root/tbody/tr[1]/td[.=$curcode])" />
          	<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[$curCol] = $curcode])" />
						<xsl:choose>
        			<xsl:when test=".!=string(../../tr[$curRow - 1]/td[$curCol]) and .!=string(../td[$curCol - 1])">
	          			<th colspan="{$colspan}" rowspan="{$rowspan}" nowrap='true' ><xsl:value-of select="."/></th>
	          	</xsl:when>         		         	
          		<xsl:otherwise>
            	</xsl:otherwise>
            </xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
	</thead>
			<tbody>
				 	<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
		    <tr>
			    <xsl:for-each select="td"> 
					<xsl:choose>
						<xsl:when test="position() &lt; 3">
						 <td align='left'>	
							<xsl:value-of select="."/>
						 </td>
						</xsl:when>
						<xsl:otherwise>
						 <td align='right'>	
							<xsl:value-of select="format-number(.,'#,##0.00')"/>
						 </td>
						</xsl:otherwise>
					</xsl:choose>
			    </xsl:for-each>
		    </tr>
	    </xsl:for-each>
			</tbody>
</xsl:template>
</xsl:stylesheet>