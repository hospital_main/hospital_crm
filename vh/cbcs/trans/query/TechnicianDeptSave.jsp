<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/query/TechnicianDeptSave.jsp,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:03 $
 $Modtime: 03-08-15 8:37 $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
                 com.viewhigh.cbcs.base.mvc.view.component.*,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge " %>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  { if(template.workload.value == "")
    {
      alert('工作量不能为空!');
      return;
    }
    
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='preparedFindAll';
    template.initsub.value="sub";
    template.submit();
    return true;
  }
</Script>
<%
  String[][] dept = (String[][]) request.getAttribute("dept");
  String[] result=(String[])request.getAttribute("result");
%>
<html:html clazz="main">
<form name="template" method="post" action="TechnicianDept.jspviewhigh">
 <!-- 信息提示栏 -->
  <html:message/>
  <!-- 标题栏 -->
  <html:title clazz='module'>医技科室工作量维护修改页面</html:title>
  <!-- 简单信息 -->
   <html:table clazz="simple">
     <tr>
      <td nowrap class="signText"  style="text-align:right" >核算月： </td>
      <td nowrap class="normalText">
        <input type='year_month' name="year_month" value='<%=result[0]%>' disabled/>

      </td>
      <td nowrap class="signText"  style="text-align:right" >科室名称： </td>
      <td nowrap class="normalText">
         <input type='text'  name = dept_code1 value='<%=result[2]%>' disabled/>
      </td>
      <td class="signText" nowrap="nowrap" style="text-align:right" >工作量：</td>
      <td class="normalText" nowrap="nowrap">
        <input type='text' name="workload" value='<%=result[3]%>' />
      </td>
    </tr>
    <tr>
      <td colspan="6" align="center"> <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
    <tr height="500">
    <td></td>
    </tr>
   </html:table>
   <input type="hidden" name="initsub" value="sub"/>
   <input type='hidden' name="dept_code" value="<%=result[1]%>"/>
   <input type='hidden' name="year_month" value="<%=result[0]%>"/>
  <input type='hidden' name="subFunction" value="save"/>
</form>
</html:html>