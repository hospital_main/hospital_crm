<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/query/counts.jsp,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:59:03 $
     $Modtime: 03-09-02 16:24 $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
  com.viewhigh.cbcs.base.mvc.view.TableMarge,java.lang.String,
  com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.cbcs.util.*,java.text.*" %>
  <%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<html:html clazz="main">
    <body>
   	  	  <html:title clazz='module'><%=request.getAttribute("title")==null? "收入数据汇总": request.getAttribute("title") %></html:title>
            <%
            DecimalFormat nf = new DecimalFormat("#,##0");
            BaseRO ro = (BaseRO)request.getAttribute("baseRO");
            if(ro!=null){
            String[][] result=ro.getTableResult();
            String a="";
            if(result!=null){
              a=result[0][0];
            }
            %>
            <br/><br/>
   	  <table width="50%" align="center" border="0">
        <tr>
          <td nowrap class="signText" width="57%" align="right"><%=request.getAttribute("note")==null? "收入金额":request.getAttribute("note")%>：</td><td nowrap class="normalText" width="50%"><%=nf.format(Double.parseDouble(a))%><%=request.getAttribute("unit")%></td>
  			</tr>
  			<tr><td colspan="2"></td></tr>
  			<tr><td colspan="2"></td></tr>
  			<tr>
  			<td align="center" colspan="2"><button class="pageBtn" onClick="window.close();" >关闭</button></td></tr>
      </table><%}%>
    </body>
</html:html>


