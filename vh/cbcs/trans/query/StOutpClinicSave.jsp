<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/query/StOutpClinicSave.jsp,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:03 $
 $Modtime: 03-08-15 8:37 $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
                 com.viewhigh.cbcs.base.mvc.view.component.*,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge " %>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function save()
  { if(template.outp_num.value=="")
    {
      alert('门诊人次不能为空!');
      return;
    }
  
   if(template.outp_num.value!=""){
    switch(isDouble(template.outp_num,16,0)){
      case 0 : alert('门诊人次必须为数字型'); return;
      case 1 : alert('门诊人次整数部分不能高于16个字符'); return;
      case 2 : alert('门诊人次没有整数部分'); return;
      case 3 : alert('门诊人次不能有小数部分'); return;
    }
    }
  
    if(template.operated_num.value!=""){
      switch(isDouble(template.operated_num,16,0)){
        case 0 : alert('手术人次必须为数字型'); return;
        case 1 : alert('手术人次整数部分不能高于16个字符'); return;
        case 2 : alert('手术人次没有整数部分'); return;
        case 3 : alert('手术人次不能有小数部分'); return;
      }
    }
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='preparedFindAll';
    template.initsub.value="sub";
    template.submit();
    return true;
  }
</Script>
<%
  String[][] dept = (String[][]) request.getAttribute("dept");
  String[] result=(String[])request.getAttribute("result");
%>
<html:html clazz="main">
<form name="template" method="post" action="StOutpClinic.jspviewhigh">
 <!-- 信息提示栏 -->
  <html:message/>
  <!-- 标题栏 -->
  <html:title clazz='module'>门诊工作量修改页面</html:title>
  <!-- 简单信息 -->
   <html:table clazz="simple">
     <tr>
      <td nowrap class="signText" >&nbsp;&nbsp;核算月： </td>
      <td nowrap class="normalText">
        <input type='text' name="year_month" value='<%=result[0]%>' disabled style="width:140px;"/></td>
      <td nowrap class="signText" >科室名称： </td>
      <td nowrap class="normalText">
         <input type='text'  value='<%=result[1]%>' disabled style="width:140px;"/></td>
      <td class="signText" nowrap="nowrap">患者费别：</td>
      <td class="normalText" nowrap="nowrap">
        <input type='text' name="charge_kind" value='<%=result[2]%>' disabled style="width:140px;"/></td>
    </tr>
	<tr>
      <td class="signText" nowrap="nowrap">门诊人次：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="outp_num" class="textInputC" value='<%=result[3]%>'/></td>
      <td class="signText" nowrap="nowrap">手术人次：</td>
      <td class="normalText" nowrap="nowrap">
        <input type=text name="operated_num" class="textInputC" value='<%=result[4]%>'/></td>
		<td></td>
		<td></td>
    </tr>
    <tr>
      <td colspan="6" align="center">
<button class="pageBtn" name=""   onclick="return save();"  >保存</button>
<button class="pageBtn" name=""   onclick="return reset();"  >重置</button>
<button class="pageBtn" name=""   onclick="return back(template);" >返回</button>
<!--<img src="images/reset.gif" class="mouse" onclick="return reset();" /> <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
   </html:table>
   <input type="hidden" name="initsub" value="sub"/>
   <input type='hidden' name="year_month" value="<%=result[0]%>"/>
   <input type='hidden' name="dept_code" value="<%=result[5]%>"/>
   <input type='hidden' name="charge_kind" value="<%=result[6]%>"/>
  <input type='hidden' name="subFunction" value="save"/>
</form>
</html:html>


