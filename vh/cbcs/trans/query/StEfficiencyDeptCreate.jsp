<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/query/StEfficiencyDeptCreate.jsp,v 1.3 2013/09/26 06:00:17 liyan Exp $
 $Author: liyan $
 $Date: 2013/09/26 06:00:17 $
 $Modtime: 03-08-15 8:37 $
 $Revision: 1.3 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent,
                 com.viewhigh.cbcs.base.mvc.view.component.*,
                 com.viewhigh.cbcs.base.mvc.view.TableMarge " %>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  { if(template.yearMonth.value=="")
    {
      alert('核算月不能为空!');
      return;
    }
    //template.subFunction.value='create';
    if(template.dept.value=="")
    {
      alert('科室代码不能为空!');
      return;
    }
    if(template.chatge_kind.value=="")
    {
      alert('患者费别不能为空!');
      return;
    }
     if(template.chatge_kind.value.length>2)
    {
      alert('患者费别不能超过两个字符!');
      return;
    }
     if(template.total_bed_used_days.value=="")
    {
      alert('占用床位总数不能为空!');
      return;
    }
     if(template.bed_used_rates.value == "")
    {
      alert('床位使用率不能为空!');
      template.bed_used_rates.focus();
      return;
    }
    if( isNaN(parseFloat( template.bed_used_rates.value ) ) )
    {
      alert('床位使用率必须为数字!');
      template.bed_used_rates.focus();
      return;
    }
    if( isNaN(parseFloat( template.operated_num.value ) ) && template.operated_num.value!='' )
    {
      alert('手术例数必须为数字!');
      template.operated_num.focus();
      return;
    }
    /*if ((parseFloat( template.bed_used_rates.value ) > 1) || (parseFloat( template.bed_used_rates.value ) < 0))
    {
      alert('床位使用率必须在零和壹之间!');
      template.bed_used_rates.focus();
      return;
    }*/
    if(template.admitted_outps.value!=""){
    switch(isDouble(template.admitted_outps,16,0)){
      case 0 : alert('门诊入院人数必须为数字型'); return;
      case 1 : alert('门诊入院人数整数部分不能高于16个字符'); return;
      case 2 : alert('门诊入院人数没有整数部分'); return;
      case 3 : alert('门诊入院人数不能有小数部分'); return;
    }
    }

      switch(isDouble(template.total_bed_used_days,16,0)){
        case 0 : alert('占用床位总数必须为数字型'); return;
        case 1 : alert('占用床位总数整数部分不能高于16个字符'); return;
        case 2 : alert('占用床位总数没有整数部分'); return;
        case 3 : alert('占用床位总数不能有小数部分'); return;
      }
    show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='preparedFindAll';
    template.initsub.value="sub";
    template.submit();
    return true;
  }
  // 计算床位使用率
  function countBedUsedRates()
  {
	if (trim(template.total_in_dept_days.value) == ''
			|| trim(template.total_in_dept_days.value) == '0'
			|| trim(template.total_bed_used_days.value) == ''
			|| trim(template.total_bed_used_days.value) == '0') 
	{
		template.bed_used_rates.value = '0.00';	
	} 
	else 
	{
		var result = template.total_bed_used_days.value/template.total_in_dept_days.value;
		template.bed_used_rates.value = isNaN(result) ? '0.00' : result;
	}
  }
</Script>
<%
  String[][] dept = (String[][]) request.getAttribute("dept");
%>
<html:html clazz="main">
<form name="template" method="post" action="StEfficiencyDept.jspviewhigh">
 <!-- 信息提示栏 -->
  <html:message/>
  <!-- 标题栏 -->
  <html:title clazz='module'>住院工作量添加页面</html:title>
  <!-- 简单信息 -->
   <html:table clazz="simple">
     <tr>
      <td nowrap class="signText" style="width:15%;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;核算月： </td>
      <td nowrap class="normalText"><%=new MonthComponent("yearMonth",request.getParameter("yearMonth"))%></td>
      <td nowrap class="signText" >&nbsp;&nbsp;&nbsp;&nbsp;科室名称： </td>
     <!-- <td>
         <%=new SingleSelect(dept, "dept", request.getParameter("dept"), true, true)%>
      </td>-->
       <td><?xml:namespace prefix="hzh"/>
        	<hzh:QInput ID="nosNamea1" name="dept" value="<%=request.getParameter("dept")==null? "":request.getParameter("dept")%>"  AdjustVal="142" previousObj="amount" indexCodeSequence="dept_code|dept_name|spell" codeCol='dept_code' textCol="dept_name" width="140" top="41" left="83" Lheight="5" xmlSource="dic/dept_name_i_V.xml" init="1"/>
        </td>
    </tr>
	<tr>
      <td class="signText" nowrap="nowrap">患者费别：</td>
      <td><?xml:namespace prefix="hzh"/>
        	<hzh:QInput ID="nosNamea1" name="chatge_kind" value="<%=request.getParameter("chatge_kind")==null? "":request.getParameter("chatge_kind")%>"  AdjustVal="142" previousObj="amount" indexCodeSequence="charge_type_code|balance_name" codeCol='charge_type_code' textCol="balance_name" width="140" top="41" left="83" Lheight="5" xmlSource="dic/st_outp_clinic_o.xml" init="1"/>
        </td>
      <td class="signText" nowrap="nowrap">门诊入院人数：</td>
      <td class="normalText" nowrap="nowrap"><input type='text' name="admitted_outps" class="textInputC" /></td>
    </tr>
	</tr>
      <td class="signText" nowrap="nowrap">出院人数：</td>
      <td class="normalText" nowrap="nowrap"><input type='text' name="discharge_num" class="textInputC" /></td>
      <td class="signText" nowrap="nowrap">&nbsp;&nbsp;实际开放床日数：</td>
      <td class="normalText" nowrap="nowrap"><input type='text' name="total_in_dept_days" class="textInputC" onchange="countBedUsedRates(this);" /></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">占用床位总数：</td>
      <td class="normalText" nowrap="nowrap"><input type='text' name="total_bed_used_days" class="textInputC" onchange="countBedUsedRates(this);" /></td>
      <td class="signText" nowrap="nowrap">&nbsp;&nbsp;床位使用率：</td>
      <td class="normalText" nowrap="nowrap"><input type='text' name="bed_used_rates" class="textInputC" readonly="readonly"  value='0.00'/></td>
    </tr>
    <tr>
      <td class="signText" nowrap="nowrap">手术例数：</td>
      <td class="normalText" nowrap="nowrap"><input type='text' name="operated_num" class="textInputC"/></td>
    </tr>
    <tr>
      <td colspan="4" align="center"> <button class="pageBtn" onclick="return create();" >添加</button> 
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
   </html:table>
  <input type="hidden" name="initsub" value="sub"/>
  <input type=hidden name="subFunction" value="create"/>
</form>
</html:html>


