<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/remove/HISDataRemove.jsp,v 1.1 2012/03/12 01:59:04 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:59:04 $
  $Modtime:$
  $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
  java.util.*, com.viewhigh.cbcs.base.mvc.view.component.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<script language="JavaScript">
function remove(){
  if(template.tableName.value=="" || template.year_month.value==""){
    alert('请选择报表名称和时间年月')
    return
   }
  template.submit()
}
</script>
<html:html clazz="main">
<form name="template" method="post" action="transRemove.jspviewhigh">

  <!-- 返回信息栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>数据删除页面</html:title>
<br>

  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td class="signText" nowrap width="10%">报表：</td>
      <td class="normalText">
        <%=new SingleSelect((String[][])(request.getAttribute("removeableTables")),"tableName",
          request.getParameter("tableName"), false, false)%></td>
    </tr>
    <tr>
      <td class="signText" nowrap width="10%">核算月：</td>
      <td class="normalText" nowrap="nowrap"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
      <td><button class="pageBtn" onclick="return remove()">删除</button></td>
    </tr>
	  </html:table>

  <input type="hidden" name="subFunction" value="remove"/>
</form>
</html:html>
