<!-- $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/income/counts.jsp,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
     $Author: zhoulidong $
     $Date: 2012/03/12 01:59:03 $
     $Modtime: 03-09-02 16:24 $
     $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.BiDateComponent,
  com.viewhigh.cbcs.base.mvc.view.TableMarge,
  com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.cbcs.util.*" %>
  
<%@ page import="java.text.*" %>
  <%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<html:html clazz="main">
    <body>
      	  	  <html:title clazz='module'>收入数据汇总</html:title>

            <%
            DecimalFormat nf = new DecimalFormat("#,##0.00");
            BaseRO ro = (BaseRO)request.getAttribute("baseRO");
            if(ro!=null){
            String[][] result=ro.getTableResult();
            String a="";
            if(result!=null){
              a=result[0][0];
            }
            %>
   	  <html:table clazz="simpleForm">
        <tr>
          <td nowrap class="signText" width="50%">收入金额：</td>
		  <td nowrap class="normalText"><%=nf.format(Double.parseDouble(a))%>元</td>
  		</tr>
  		<tr>
			<td align="center" colspan="2" height="50"><button class="pageBtn" name=""   onClick="window.close()" />关闭</button>
<!--<img src="images/priClose.gif" onClick="window.close()" class="mouse"/>--></td>
      </tr>
      </html:table><%}%>
    </body>
</html:html>


