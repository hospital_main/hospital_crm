<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/income/incomemain.jsp,v 1.4 2015/01/13 01:07:05 liyan Exp $
 $Author: liyan $
 $Date: 2015/01/13 01:07:05 $
 $Revision: 1.4 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,com.viewhigh.cbcs.base.mvc.view.component.Select,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
                com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript">
  function showmodul(src,name,sty){
    var tim=new Date();
   	window.showModalDialog(src+'&time='+tim.toLocaleString(),name,sty)
   	if (template!=null && template._old_current_page!=null) {
      template._current_page.value = template._old_current_page.value
    }
   	show_wait()
   	find();
  }    
   
	function create() {
		template.subFunction.value='preparedCreate';
		show_wait();
    template.submit();
		return true;
	}

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
  		if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
  		  flag = true;
  	}

    if( flag!=false) {
    	if (confirm('是否删除?')) {
	      template.subFunction.value='remove';
	      show_wait();
	      template.submit();
	      return true;
	    } else
	    	return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }
  
  function opens(src,name,styl){
    var ohan=window.open(src,name,styl);
  }
  
  function selectedAll(){
    for (var i=0; i<template.elements.length; i++) {
    	if (template.elements[i].name == 'primaryKey'
        && template.elements[i].disabled == false) {
          template.elements[i].checked = true;
    	}
    }
  }
	
	function accout() {
    var d=new Date()
    var s=d.toString()
   	template.signs.value="collect"
    template.subFunction.value='findAll';   
		window.showModalDialog("income.jspviewhigh?signs="
		+template.signs.value+"&subFunction="
		+template.subFunction.value+"&start="+template.start.value
		+"&end="+template.end.value+"&abstract="
		+template.abstract.value+"&charge_kind_code="
		+template.charge_kind_code.value+"&order_by="
		+template.order_by.value+"&perform_by="
		+template.perform_by.value+"&operator="
		+template.operator.value+"&amount="+template.amount.value
		+"&s="+s, window,
		"status:no;dialogHeight: 140px; dialogWidth: 500px;");
			
	}


  function find() {
    template.signs.value="find"
    template.subFunction.value='findAll';
    show_wait();
    template.submit();
    return true;
  }
	function exportExcel() {	
		template.subFunction.value = "exportExcel";
		template.submit();
		
		return true;
	}

	function changeCurData() {
		//here//////////
	}

	function importData(obj, v) {

		var fileName = fileUploader.choose(1);
		if (fileName.length < 4 && fileName.indexOf(".") < 0)
			return false;
		var fileType = fileName.substr(fileName.lastIndexOf(".") + 1);
		fileUploader.submit(function(m, f) {
			if (m != '') {
				alert(m);
				return false;
			}
			for ( var k in f) {
				fileType += "=" + k;
				break;
			}
			var msg = "";
			var queryParams = "<comp_file>" + fileType + "</comp_file>";
			confirmImportStyleDialog(function(v) {
				if (v == 0)
					return;

				window.xmlhttp.post(
						"hosDictsUnitinfoStdeptincomekinddetail_import",
						queryParams + "<q_flag>" + v + "</q_flag>");

				var responseText = window.xmlhttp._object.responseText;
				msg = window.doMsg(responseText, "show")
				if (msg == true) {
					template.signs.value = "find"
					template.subFunction.value = 'findAll';
					show_wait();
					template.submit();
					return true;
				}
			});
		}, obj)
	}
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="income.jspviewhigh">
     <!-- 信息提示栏 -->
	  <html:message/>

     <!--信息栏-->
	  <html:title clazz='module'>收入数据维护主页面</html:title>


     <!-- 简单信息 -->
	  <html:table clazz="simple">
      <tr>
        <td nowrap class="signText" style="text-align:right">起止年月：</td>
	        <td nowrap class="normalText"><%=new BiMonthComponent("start", (String)request.getParameter("start"), "end", (String)request.getParameter("end"))%></td>                                        
        <td nowrap class="signText" style="text-align:right">收费类别：</td> 
	        <td><%=new SingleSelect(request.getAttribute("charge_kind"), "charge_kind_code", request.getParameter("charge_kind_code"), false, false)%>
	        </td>
	        
	        <td nowrap class="signText" style="text-align:right">开单科室：</td>
	        <td><?xml:namespace prefix="hzh"/>
	        	<hzh:QInput ID="nosNamea1" name="order_by" value="<%=request.getParameter("order_by")==null? "11":request.getParameter("order_by")%>"  AdjustVal="142" previousObj="amount" indexCodeSequence="dept_code|dept_name|spell" codeCol='dept_code' textCol="dept_name" Lheight="5" xmlSource="dic/dict_acct_dept_LTD.xml" init="1" onValueChange="changeCurData()" width="140"/>
	        </td>    
	      </tr>
	      <tr>
	        <td nowrap class="signText" style="text-align:right">金额(元)：</td> 
	        <td nowrap class="normalText"><input type="text" name="amount" size="15" maxlength="15" value='<%=request.getParameter("amount")==null?"":request.getParameter("amount")%>' style="width:140px"/></td>
	      
	        <td nowrap class="signText" style="text-align:right">摘要：</td>
	        <td><input type="text" name="abstract" size="20" value='<%=request.getParameter("abstract")==null?"":request.getParameter("abstract")%>' style="width:140px"/></td>
	       
	 				<td  nowrap class="signText" style="text-align:right">常用摘要：</td>
	    		<td><%=new SingleSelect(request.getAttribute("dictsummary"), "dictsummary", request.getParameter("dictsummary"), false, false)%>
					</td>
	      </tr>
	      <tr>
	        <td nowrap class="signText" style="text-align:right">执行科室：</td>
	        <td><?xml:namespace prefix="hzh"/>
	        <hzh:QInput ID="nosNamea1" name="perform_by" value="<%=request.getParameter("perform_by")==null? "":request.getParameter("perform_by")%>" AdjustVal="145" previousObj="amount"  indexCodeSequence="dept_code|dept_name|spell"  codeCol='dept_code' textCol="dept_name" width="140" top="13" left="83" Lheight="5" xmlSource="dic/asst_dict_acct_dept_LTD.xml" init="1"/>
	        </td>
	        <td nowrap class="signText" style="text-align:right">录&nbsp;入&nbsp;人：</td>
	        <td nowrap class="normalText"><input type="text" name="operator" size="15" maxlength="15" value='<%=request.getParameter("operator")==null?"":request.getParameter("operator")%>' style="width:140px"/></td>
			    <td></td>
			    <td></td>
			  </tr>
			  <tr>
			     <td colspan='6' align='right'>
							<button class="pageBtn" name="finds" onclick="return find();">查询</button>
							<button class="pageBtn" onclick="accout()">汇总</button>
							<button class="pageBtn" name="hosDictsUnitinfoStdeptincomekinddetail_import" onclick="importData(this,1)">导入</button>
							<button class="pageBtn"  onclick="return exportExcel();" />导出</button>
					 </td>
	      </tr>
		  </html:table>
	    <br>
	      <%
		      DecimalFormat nf = new DecimalFormat("#,##0.00");
		      BaseRO ro = (BaseRO)request.getAttribute("baseRO");
		      if(ro!=null)
		       {
	      %>
    	<html:title clazz='table'>收入数据</html:title>
			  <%
				    TableMarge oper = new TableMarge(ro, "return find()");
			      oper.addOptionButton("images/selectedAll.gif", "return selectedAll()");   // 全选
			      oper.addOptionButton("images/reset.gif", "return reset()");               //  重置
			      oper.addOptionButton("images/remove.gif", "return remove()");             //  删除
			      oper.addNeedButton("images/create.gif", "return create()");               //  添加
			  %>
	    <!-- 复杂信息 -->
		  <html:table clazz="complex">
	      <!-- 操作 -->
	      <tr><td><%=oper%></td></tr>
	      <!-- 结果集 -->	
		  </html:table>
		      <html:table clazz="result">
		        <html:tr clazz='label'>
	                <td class="resultLabel">选择</td>
	                <td class="resultLabel">统计年月</td>
	                <td class="resultLabel">收费类别名称</td>
	                <td class="resultLabel">结算方式</td>
		              <td class="resultLabel">开单科室</td>
		              <td class="resultLabel">执行科室</td>
	                <td class="resultLabel">摘要</td>
	                <td class="resultLabel">金额</td>
	                <td class="resultLabel">录入人</td>
	                <td class="resultLabel">录入时间</td>
		        </html:tr>
	              <%
	                if (ro!=null) {
	              	String[][] result = ro.getTableResult();
	              	if (result!=null) {
	
	                  for (int i = 0; i < result.length; i++ )
	                 {
			                boolean isCheckout =Boolean.valueOf(result[i][13]).booleanValue();
			                String primaryKey = result[i][8];
			                String rowColor = "rowGray";
			                if (i/2*2==i) rowColor = "rowWhite";
	              %>
	              <tr CLASS="<%=rowColor%>">
	                  <td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>" <%if(isCheckout){out.print(" disabled ");}%>></td>
	                  <td class="normalText">
	                    <%if(!isCheckout) {%>
	                    <a href='#' onclick="showmodul('itemmaintenance.jspviewhigh?subFunction=showmod&width=425px&height=314px&src=income.jspviewhigh?subFunction=load@@primaryKey=<%=primaryKey%>',window,'status:no;dialogTop:150px;dialogLeft:270px;dialogHeight: 340px; dialogWidth: 400px;scrolling:no')">
	                    <%}%>
	                      <%=result[i][0]%>
	                    <%if(!isCheckout) {%>
	                    </a>
	                    <%}%>
	                  </td>
	                  <td class="normalText"><%=result[i][1]%></td>
	                  <td class="normalText"><%=result[i][12]%>&nbsp;</td>
	                  <td class="normalText"><%=result[i][2]%></td>
	                  <td class="normalText"><%=result[i][3]%></td>
	                  <td class="normalText"><%=result[i][11]%>&nbsp;</td>
	                  <td nowrap class="numberText"><%=nf.format(Double.parseDouble(result[i][4]))%></td>
	                  <td class="normalText"><%=result[i][5]%></td>
	                  <td class="normalText"><%=result[i][6]%></td>
	              </tr>
	              <%
	                    }
	                  }
	                }
	
	              %>
		      </html:table>
	      <!-- 操作 -->
	
			<%}%>
	        <input type=hidden name="subFunction"/>
	        <input type=hidden name="signs"/>

  </form>
  <script>
  document.all.dictsummary.onchange=function(){
  	document.all.abstract.value=this.options[this.selectedIndex].text;	
  }
  document.getElementById("dictsummary").style.width=140;
    document.getElementById("charge_kind_code").style.width=140;
  </script>
  
  <div id="upload_file_div" style="position:absolute;display:none;">
	    <iframe src="uploadFileD.cmd" id="uploadFrame"></iframe>
	</div>
	<script Language="JavaScript" >
		_____VHFileCreatFromTimerss=window.setInterval(createVhFile,10);
		  function createVhFile(){
		    if(typeof(uploadFrame)=="undefined")
		      return ;
		    window.clearInterval(_____VHFileCreatFromTimerss);
		    fileUploader=new VhFileUploader(uploadFrame);
		  }
	</script>
</html:html>
