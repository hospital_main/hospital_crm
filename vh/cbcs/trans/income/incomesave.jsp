<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/income/incomesave.jsp,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:03 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,com.viewhigh.cbcs.base.mvc.view.component.Select,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
                com.viewhigh.cbcs.base.mvc.view.BiMonthComponent,
                java.text.DecimalFormat"  %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function save() {

    switch(isDouble(template.amount,10,2))
    {
      case 0 : alert('参考单价必须为数字型'); return;
      case 1 : alert('参考单价整数部分不能高于10个字符'); return;
      case 2 : alert('参考单价没有整数部分'); return;
      case 3 : alert('参考单价小数部分不能高于2个字符'); return;
    }
		template.subFunction.value='store';
		show_wait();
    template.submit();
		return true;
	}

function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='findAll';
    self.close();
  }
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="income.jspviewhigh">
     <!-- 信息提示栏 -->
	  <html:message/>

     <!--信息栏-->
	  <html:title clazz='module'>收入数据修改页面</html:title>

     <!-- 简单信息 -->
	  <html:table clazz="simple">
     <%
       String[][] result=(String[][])request.getAttribute("result");
       DecimalFormat nf = new DecimalFormat("#,##0.00");
     %>
      <tr>
       <td nowrap class="signText">统计年月：</td>
       <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text value="<%=result[0][0]%>" disabled />
        <input type=hidden name="year_month" value="<%=result[0][0]%>">
      </td>
      </tr>
     <tr>
      <td nowrap class="signText">收费类别名称：</td>
       <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text   value="<%=result[0][6]%>" disabled />
        <input type=hidden  name="charge_kind_code" value="<%=result[0][1]%>">
      </td>
      </tr>
      <tr>
        <td nowrap class="signText">开单科室：</td>
       <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text  value="<%=result[0][7]%>" disabled />
         <input type=hidden  name="order_by" value="<%=result[0][2]%>">
      </td>
       </tr>
      <tr>
      <td nowrap class="signText">执行科室：</td>
       <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text  value="<%=result[0][8]%>" disabled />
        <input type=hidden  name="perform_by" value="<%=result[0][3]%>">
      </td>
      </tr>
      <tr>
        <td nowrap class="signText">金额：</td>
        <td nowrap class="normalText"><input type="text" name="amount" size="30" maxlength="30" value='<%=new DecimalFormat("##0.00").format(Double.parseDouble(result[0][4]))%>' /></td>
        </tr>
      <tr>
        <td nowrap class="signText">摘要：</td>
        <td nowrap class="normalText">
            <input type="text" name="abstract" size="30" maxlength="30" value="<%=result[0][5]%>" />
            <input type=hidden  name="operator" value="<%=result[0][9]%>">
            <input type=hidden name="id" value="<%=result[0][10]%>"/>
        </td>
      </tr>
      <tr>
        <td class="signText">常用摘要:</td>
        
    <td><%=new SingleSelect(request.getAttribute("dictsummary"), "dictsummary", request.getParameter("dictsummary"), false, false)%>
	</td>
	
      </tr>
    <tr>
      <td colspan="2" align="left">　　　　　　
      <button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);" >关闭</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
      <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/close.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
    <tr height="200">
    <td/>
    </tr>
	  </html:table>
      <br>
      <input type=hidden name="subFunction"/>
      </form>
	<script>
  document.all.dictsummary.onchange=function(){
  	document.all.abstract.value=this.options[this.selectedIndex].text;	
  }
  </script>

</html:html>




