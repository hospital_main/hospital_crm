<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/trans/income/incomecreate.jsp,v 1.2 2013/11/13 03:49:39 liyan Exp $
 $Author: liyan $
 $Date: 2013/11/13 03:49:39 $
 $Revision: 1.2 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,com.viewhigh.cbcs.base.mvc.view.component.Select,
                com.viewhigh.cbcs.base.mvc.view.TableMarge,com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,
                com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript"> 
	function create() {  
    if(template.year_month.value==""){
      alert('统计年月不能为空!');
      return;
    }
     if(template.charge_kind_code.value==""){
      alert('收费类别不能为空!');
      return;
    }
    if(template.order_by.value==""){
      alert('开单科室不能为空!');
      return;
    }
     if(template.balance_code.value==""){
      alert('结算方式不能为空!');
      return;
    }
    if(template.perform_by.value==""){
      alert('执行科室不能为空!');
      return;
    }
    /*数据校验
    var a=getValuePairBySql('depot_charge_kind_check','<td>'+template.charge_kind_code.value+'</td><td>'+template.order_by.value+'</td>');
		if(a[0]!=''){
			alert(a[0]);
			return;
			}*/
    switch(isDouble(template.amount,10,2)){
      case 0 : alert('金额必须为数字型'); return;
      case 1 : alert('金额整数部分不能高于10个字符'); return;
      case 2 : alert('金额没有整数部分'); return;
      case 3 : alert('金额小数部分不能高于2个字符'); return;
    }
		template.start.value = '<%=request.getAttribute("start")%>';
		template.end.value = '<%=request.getAttribute("end")%>';
		template.subFunction.value='create';
		show_wait();
    template.submit();
		return true;
	}
	
	/*function back( element ) { 
		for(var i=0;i<template.elements.length;i++)
                template.elements[i].value="";
		element.start.value = '<%=request.getAttribute("start")%>';
		element.end.value = '<%=request.getAttribute("end")%>';
    template.subFunction.value='findAll';
	template.signs.value="find";
    show_wait();
    element.submit();
  }*/
   function back(element) {
    template.balance_code.value = "";
    template.order_by.value = "";
    template.perform_by.value = "";
    template.amount.value = "";
    template.abstract.value = "";
    template.subFunction.value='findAll';
    template.signs.value="find";
    element.submit();
  }
  function resetAll(){
		template.reset();
		template.amount.value = "";
		document.all.dictsummary.selectedIndex=0; 
		document.all.abstract.value = "";
	}
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="income.jspviewhigh">
     <!-- 信息提示栏 -->
	  <html:message/>

     <!--信息栏-->
	  <html:title clazz='module'>收入数据添加页面</html:title>

     <!-- 简单信息 -->
	  <html:table clazz="simple">
      <tr>
       <td nowrap class="signText">统计年月：</td>
       <td class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.MonthComponent("year_month", request.getParameter("year_month"))%></td>
      </tr>
      
       <tr >
       <td nowrap class="signText" style="text-align:right">收费类别：</td> 
	        <td><%=new SingleSelect(request.getAttribute("init_charge_kind_code"), "charge_kind_code", request.getParameter("init_charge_kind_code"), false, false)%>
        </td>
      </tr>
      <tr >
      	<td nowrap class="signText" >结算方式：</td>
         <td><?xml:namespace prefix="hzh"/>
        	<hzh:QInput ID="nosNamea1" name="balance_code" value="<%=request.getParameter("balance_code")==null? "":request.getParameter("balance_code")%>"  AdjustVal="142" previousObj="amount" indexCodeSequence="balance_code|balance_name" codeCol='balance_code' textCol="balance_name" width="140" top="41" left="83" Lheight="5" xmlSource="dic/dict_medical_insurance_balance.xml" init="1"/>
        </td>
      </tr>

      <tr  >
        <td nowrap class="signText">开单科室：</td>
       <td><?xml:namespace prefix="hzh"/>
        	<hzh:QInput ID="nosNamea1" name="order_by" value="<%=request.getParameter("order_by")==null? "11":request.getParameter("order_by")%>"  AdjustVal="142" previousObj="amount" indexCodeSequence="dept_code|dept_name|spell" codeCol='dept_code' textCol="dept_name" width="140" top="41" left="83" Lheight="5" xmlSource="dic/dict_acct_dept_LVT.xml" init="1" />
        </td>
      </tr>
      <tr >
          <td nowrap class="signText">执行科室：</td>
          <td><?xml:namespace prefix="hzh"/>
          <hzh:QInput ID="nosNamea1" name="perform_by" value="<%=request.getParameter("perform_by")==null? "":request.getParameter("perform_by")%>" AdjustVal="145" previousObj="amount"  indexCodeSequence="dept_code|dept_name|spell"  codeCol='dept_code' textCol="dept_name" width="140" top="13" left="83" Lheight="5" xmlSource="dic/asst_dict_acct_dept_LVTD.xml" init="1"/>
        </td>
      </tr>     
    
      <tr>
        <td nowrap class="signText">金额：</td>
        <td nowrap class="normalText"><input type="text" name="amount" size="30" maxlength="30" style="width:140px;" value="<%=request.getParameter("amount")==null? "":request.getParameter("amount")%>"/></td>
        </tr>
      <tr>
        <td nowrap class="signText">摘要：</td>
        <td><input type="text" name="abstract" size="20" value='<%=request.getParameter("abstract")==null?"":request.getParameter("abstract")%>' style="width:140px;"/></td>
       </tr>
       <tr>
 				<td  nowrap class="signText">常用摘要：</td>
    		<td>						<%=new SingleSelect(request.getAttribute("dictsummary"), "dictsummary", request.getParameter("dictsummary"), false, false)%>
				</td>
        </tr>        
    <tr>
      <td colspan="2" align="left">　　　　　　　　　　　　　　　　　　　
      <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return resetAll();" >重置</button>     
      <button class="pageBtn" onclick="return back(template);">返回</button>    
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center"></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>
	  </html:table>
      <br>
	  <input type="hidden" name="signs" value="find"/>
      <input type=hidden name="subFunction"/>
      <input type=hidden name="start"/>
      <input type=hidden name="end"/>
  	</form>
<Script >
	 document.all.dictsummary.onchange=function(){
  	document.all.abstract.value=this.options[this.selectedIndex].text;	
  }
   document.getElementById("charge_kind_code").style.width=140;
</Script>
</html:html>



