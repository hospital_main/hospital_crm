<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/report/SDReport_Save.jsp,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:03 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
	function designReport(){
		window.open("Murd_design.htm",'child', 'width=800,status=1,height=400, top=100, left=100, scrollbars = 1, resizable = 0,status=0');
	}
  function save()
  {
  	if(trim(template.rep_text.value)=="")
  	{
    		alert("请进行报表定义");
    		return;
    }
    show_wait();
    template.subFunction.value='save';    
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  { for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='define';
    show_wait();
    template.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="selfDreport.jspviewhigh">
  <!-- 信息提示栏 -->
  <html:message/>
  <!-- 标题栏 -->
  <html:title clazz="module">自定义报表结构修改页面</html:title> 
   <%
    String  primaryKey = (String)(request.getAttribute("primaryKey")==null? "":request.getAttribute("primaryKey"));
    String  rep_name = (String)(request.getAttribute("rep_name")==null? "":request.getAttribute("rep_name"));
    String  rep_text = (String)(request.getAttribute("rep_text")==null? "":request.getAttribute("rep_text"));
  %> 
  <table  width="100%" cellspacing="2" border="0">
    <tr>
      <td class="signText" nowrap="nowrap">报表编号：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text value="<%=primaryKey%>" disabled />
      </td>
    </tr>    
    <tr>
      <td class="signText" nowrap="nowrap">报表名称：</td>
      <td width="75%" class="normalText" nowrap="nowrap">
        <input type=text value="<%=rep_name%>" disabled />
      </td>
    </tr>    
   
    <tr>
      <td class="normalText" nowrap="nowrap">
        <img src="images/rep_save.png" class="mouse" onclick="designReport()" /> 
        <TEXTAREA  name="rep_text" style="display:none"><%=rep_text %></TEXTAREA>
      </td>
    </tr>
    <tr>
      <td colspan="2"><button class="pageBtn" onclick="return save();">保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>  <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/save.gif" class="mouse" onclick="return save();" />
       <img src="images/reset.gif" class="mouse" onclick="return reset();" />
<img src="images/return.gif" class="mouse" onclick="return back(template);" />--> </td>
    </tr>
  </table>
  <input type=hidden name="subFunction" value = "save"/>
  <input type="hidden" name="primaryKey" value="<%=primaryKey%>">
  <input type="hidden" name="rep_name" value="<%=rep_name%>">
</form>
</html:html>
