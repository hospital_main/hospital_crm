<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/report/SDReport_Show.jsp,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:03 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<html xmlns:vh>
	<head>
		<title>User Report view </title>
		<link href="css/control.css" rel="stylesheet" type="text/css">
		<meta name="Author" content="hzh">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body onload="inits()">
		<vh:cmm init="0"></vh:cmm>
		<vh:urd_view id="myUserReportView" init="0" ></vh:urd_view>
		<TEXTAREA STYLE="display:none" name="reportRes_xml"><%=request.getAttribute("rep_xml")==null? "":request.getAttribute("rep_xml")%></TEXTAREA>
		<div id='report_title' style="font-size:15px;font-weight:bold;cursor: hand;position:absolute;top:9;left:250;z-index:10"></div>
		<button class="pageBtn"  onclick="return doPrint();" style='cursor:hand;position:absolute;top:5;left:790;z-index:10;'>打印</button>
		<img src="images/output.gif" style="cursor: hand;position:absolute;top:5;left:863;z-index:10" onclick="req();"/>
	</body>
</html>
<script>
var title = ""
var objs = window.dialogArguments.document.getElementsByName('rep_ids')
if (objs.length>0) {
  title = objs[0][objs[0].selectedIndex].text
}

var subtitle = '<%=request.getParameter("year_month")%>';
var subtitle2 = '<%=request.getParameter("end_year_month")%>';
if(subtitle.substring(4, 6)==subtitle2.substring(4, 6)){
subtitle = subtitle.substring(0, 4)+"年"+subtitle.substring(4, 6)+"月";
}else{
subtitle = subtitle.substring(0, 4)+"年"+subtitle.substring(4, 6)+"月";
subtitle = subtitle+"--"+subtitle2.substring(0, 4)+"年"+subtitle2.substring(4, 6)+"月";
}

report_title.innerText = title + "　　　　　　　　" + subtitle

function inits(){
	myUserReportView.dataSource=reportRes_xml.value;
}
function req(){
	window.open("<%=request.getAttribute("fpath")%>");
}
var vTemp = '<%=request.getAttribute("rep_xml")==null? "":request.getAttribute("rep_xml")%>'

function doPrint() {
  var vXml = new ActiveXObject("Microsoft.XMLDOM");
  vXml.async=false;
  vXml.loadXML(vTemp)
  vXml.selectNodes("/user_report")[0].setAttribute("comName", "<%=com.viewhigh.cbcs.base.util.Preference.getCom_name()%>")
  //alert(vTemp)
  var vXsl = new ActiveXObject("Microsoft.XMLDOM");
  vXsl.async=false;
  vXsl.load("base/print/printView.xsl")
  
  vXml.loadXML(vXml.transformNode(vXsl))
  var root = vXml.cloneNode(true)
  
  if (root.selectNodes("/root/tbody/tr").length<1) {
    alert('没有可打印的记录')
    return false;
  }
  
  var d = new Date();
  var month = "00"+(d.getMonth()+1)
  month = month.substring(month.length-2)
  var day = "00"+d.getDate()
  day = day.substring(day.length-2)
  
  var bottom = "<bottom><tr><td></td><td>制作人：<%=com.viewhigh.cbcs.base.util.LoginUser.getUser_name(request)%>  "+d.getYear()+"-"+month+"-"+day+"</td></tr><tr><td></td><td>北京望海康信科技有限公司 制作</td></tr></bottom>";

  vXml.loadXML(bottom)
  root.selectNodes("/root")[0].appendChild(vXml.selectNodes("/bottom")[0])

  if (root.selectNodes("/root/top/subtitle").length>0)
    root.selectNodes("/root/top")[0].removeChild(root.selectNodes("/root/top/subtitle")[0])
  
  var temp = "<subtitle>"+subtitle+"</subtitle>";
  vXml.loadXML(temp)
  root.selectNodes("/root/top")[0].appendChild(vXml.selectNodes("/subtitle")[0])

  temp = "<title>"+title+"</title>";
  vXml.loadXML(temp)
  root.selectNodes("/root/top")[0].appendChild(vXml.selectNodes("/title")[0])


  window._printXml = root
  var screenHeight=window.screen.availHeight;
  var screenWidth=window.screen.availWidth;  
  var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto"
  var win=window.showModalDialog("base/print/print.html",window,dialogStyle)
  return true;
}


</script>

