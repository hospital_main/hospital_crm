<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">   
  <xsl:variable name="count" select="count(//root/tbody/tr[1]/td)"/>
  <root> 
  	<thead>
   <xsl:for-each select="/root/tbody/tr[1]">
  	<tr noWrap='true'>
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							 <td style='fontsize:maintitle;colspan:colcount'></td>
					</xsl:when>
					<xsl:otherwise>
						<td style="display:none"></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each> 
	<xsl:for-each select="/root/tbody/tr[1]">
  	<tr noWrap='true'>
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							 <td style='colspan:colcount'></td>
					</xsl:when>
					<xsl:otherwise>
						<td style="display:none"></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each> 
	<xsl:for-each select="/root/tbody/tr[1]">
  	<tr noWrap='true'>
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							 <td style='colspan:colcount;align:left'></td>
					</xsl:when>
					<xsl:otherwise>
						<td style="display:none"></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each> 
		  <tr noWrap="true" class="mainHead">
	   		
				<td nowrap="true" rowspan="3">医院科室名称</td>
				<td nowrap="true" rowspan="3">标准科室名称</td>
				<td nowrap="true" rowspan="3">行次</td>	
				
				<td nowrap="true" rowspan="2" colspan="3">工作量</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td nowrap="true" colspan="12">次均费用</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>			
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
		</tr> 
		<tr noWrap="true" class="mainHead">
			<td style="display:none"></td>	
			<td style="display:none"></td>
				<td style="display:none"></td>	
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			
					
			<td nowrap="true" colspan="4">每门(急)诊人次</td>
			<td style="display:none"></td>	
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td nowrap="true" colspan="4">每床日</td>
			<td style="display:none"></td>	
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td nowrap="true" colspan="4">每出院人次</td>
			<td style="display:none"></td>	
			<td style="display:none"></td>
			<td style="display:none"></td>			

		</tr> 
		
		
		<tr noWrap="true" class="mainHead">
			<td style="display:none"></td>		
			<td style="display:none"></td>
			<td style="display:none"></td>
	
			
					
			<td nowrap="true">门(急)诊人次数</td>
		  <td nowrap="true">实际占用床日数</td>
			<td nowrap="true">出院人数</td>
			
			<td nowrap="true">平均收费</td>
		  <td nowrap="true">平均医疗成本</td>
			<td nowrap="true">其中：药品费</td>
			<td nowrap="true">均次结余</td>	
			
			<td nowrap="true">平均收费</td>
		  <td nowrap="true">平均医疗成本</td>
			<td nowrap="true">其中：药品费</td>
			<td nowrap="true">均次结余</td>		
			
			<td nowrap="true">平均收费</td>
		  <td nowrap="true">平均医疗成本</td>
			<td nowrap="true">其中：药品费</td>
			<td nowrap="true">均次结余</td>						
		</tr> 		
		
		
		
		
		<tr noWrap="true" class="mainHead">
				<td noWrap="true">栏次</td>
				<td noWrap="true"></td>				
				<td noWrap="true"></td>
				
				
				<td noWrap="true">1</td>
				<td noWrap="true">2</td>
				<td noWrap="true">3</td>
				<td noWrap="true">4</td>
				<td noWrap="true">5</td>
				<td noWrap="true">6</td>
				<td noWrap="true">7=4-5</td>
				<td noWrap="true">8</td>
				<td noWrap="true">9</td>
				<td noWrap="true">10</td>
				<td noWrap="true">11=8-9</td>
				<td noWrap="true">12</td>
				<td noWrap="true">13</td>
				<td noWrap="true">14</td>
				<td noWrap="true">15=12-13</td>				
		</tr> 
		</thead>
		<tbody>
		<xsl:for-each select="/root/tbody/tr">
					<xsl:variable name="positionRow" select="position()"></xsl:variable> 
	<tr>
		<xsl:for-each select="td">
			<xsl:choose>
				<xsl:when test="position()=1 or position()=2">
						<td><xsl:value-of select='.'/></td>
				</xsl:when>
				<xsl:when test="position()=3">
				<td align="center"><xsl:value-of select='$positionRow'/></td>
				</xsl:when>
				
				
				<xsl:otherwise>
				  							<td align="right">
																		<xsl:attribute name="class">numberText</xsl:attribute>
																		<xsl:if test=".=0.00">
																	   0.00
																	  </xsl:if>
																		<xsl:if test=".!=0.00">
																				<xsl:value-of select="format-number(.,'#,##0.00')"/>																
																	  </xsl:if>		
													 </td>   
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</tr>
			</xsl:for-each>
		</tbody>
		<tfoot>
		  	<tr noWrap='true'> 
					<td align="right" colspan='18'></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
			</tr> 
	        </tfoot>
  </root>
	</xsl:template>
</xsl:stylesheet>
