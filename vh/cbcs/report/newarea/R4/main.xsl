<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>

	<xsl:for-each select="/root/tbody/tr[td[1]='1']">
		<tr noWrap="true" class="mainHead"> 
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1 or position()=2">
					</xsl:when>
					
					<xsl:when test="position()=3 or position()=4 or position()=5">
						<td nowrap="true"  rowspan="2"><xsl:value-of select='.'/></td>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test=". != ''">
							<td nowrap="true" align="center" colspan="3"><xsl:value-of select='.'/></td>
							<td style="display:none"/>
							<td style="display:none"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>

	<xsl:for-each select="/root/tbody/tr[td[1]='2']">
		<tr noWrap="true" class="mainHead">
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1 or position()=2">
					</xsl:when>
					<xsl:when test="position()=3 or position()=4 or position()=5">						
							<td style="display:none"/>
					</xsl:when>
					<xsl:otherwise>
						<td nowrap="true" ><xsl:value-of select='.'/></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>	

<xsl:for-each select="/root/tbody/tr[td[1]='222']">
		<tr noWrap="true" class="mainHead">
			<xsl:for-each select='td'>
				<xsl:choose>
						<xsl:when test="position()=1 or position()=2">
					  </xsl:when>
						<xsl:otherwise>
						  <td nowrap="true" ><xsl:value-of select='.'/></td>
						</xsl:otherwise>
					</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>	
	
	
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr[td[1]!='1' and td[1]!='2' and td[1]!='222']">
             <tr>
				<xsl:for-each select="td">
	              <xsl:choose>
	                <xsl:when test="position()=1 or position()=2">
	                </xsl:when>
	                <xsl:when test="position()=3 or position()=4">
	                  	<xsl:if test=".='�ϼ�'">
	                  		  <td noWrap="true" align='right'  style="background-color:lightblue;font-weight:bold">
	                  		      <xsl:value-of select="."/>
	                  		   </td>
	                  	</xsl:if>
	                  	
	                  	<xsl:if test=".!='�ϼ�'">
	                  		  <td noWrap="true">
	                  		      <xsl:value-of select="."/>
	                  		   </td>
	                  	</xsl:if>
	                </xsl:when>
	                <xsl:when test="position()=5">
	                  <td noWrap="true" align="center">
	                    <xsl:value-of select="."/>
	                  </td>
	                </xsl:when>
	                
	                <xsl:otherwise>
	                	<xsl:if test=". != ''">
	                		<td noWrap="true" class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	                	</xsl:if>
	                	<xsl:if test=". = ''">
	                		<td noWrap="true" class="numberText" align="right">0.00</td>
	                	</xsl:if>
	                </xsl:otherwise>
	              </xsl:choose>
	          </xsl:for-each>
			</tr>
	              
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
