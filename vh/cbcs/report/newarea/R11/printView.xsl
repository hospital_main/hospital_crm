<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		  <xsl:variable name="count" select="count(//root/tbody/tr[1]/td)"/>
		  <xsl:variable name="count1" select="count(//root/tbody/tr[1]/td)-2"/>
		<root>
			
			<thead>
					<tr>			
						<td noWrap="true" class="mainHead" style="fontsize:maintitle;colspan:colcount" >
						</td>
						<xsl:for-each select="/root/tbody/tr[position()=3]/td">	
						<xsl:choose>
							<xsl:when test="position()=1 or position()=2 or position()=3 "></xsl:when>
							<xsl:otherwise>
								<td style="display:none"></td>
							</xsl:otherwise>
						</xsl:choose>	
						</xsl:for-each>
					</tr>
					<tr>
					<td noWrap="true" align="center" style="colspan:colcount" >
					</td>
					<xsl:for-each select="/root/tbody/tr[position()=3]/td">	
					<xsl:choose>
						<xsl:when test="position()=1 or position()=2 or position()=3"></xsl:when>
						<xsl:otherwise>
							<td style="display:none"></td>
						</xsl:otherwise>
					</xsl:choose>	
					</xsl:for-each>
				</tr>
			  <tr>
					<td noWrap="true" align="left" style="colspan:colcount-2" >
					</td>
					<xsl:for-each select="/root/tbody/tr[position()=3]/td">	
					<xsl:choose>
						<xsl:when test="position() &gt; 5">
							<td style="display:none"></td>
						</xsl:when>
					</xsl:choose>	
					</xsl:for-each>
					<td noWrap="true" align="right" style="colspan:2" >
						金额单位：元
					</td>
					<td style="display:none"></td>
				</tr>
		
	<xsl:for-each select="/root/tbody/tr[td[1]='1']">
		<tr noWrap="true" class="mainHead"> 
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1 or position()=2">												
					</xsl:when>					
					<xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 ">
						<td nowrap="true"  rowspan="3"><xsl:value-of select='.'/></td>
					</xsl:when>						
					<xsl:otherwise>
						<xsl:if test=". != ''">
							<td nowrap="true" align="center" colspan="5"><xsl:value-of select='.'/></td>
							<td style="display:none"/>
							<td style="display:none"/>
							<td style="display:none"/>
							<td style="display:none"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>	
	<xsl:for-each select="/root/tbody/tr[td[1]='2']">
		<tr noWrap="true" class="mainHead"> 
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1 or position()=2 ">
					</xsl:when>		
							<xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 ">
						<td nowrap="true"  rowspan="2"><xsl:value-of select='.'/></td>
					</xsl:when>									
					<xsl:when test="position()=7 or position()=12 or position()=17 or position()=22 or position()=27">
					<td nowrap="true" align="center" rowspan="2">
						<xsl:value-of select='.'/>						
					</td>
					</xsl:when>				
					<xsl:otherwise>
					<xsl:if test=". != ''">
						<td nowrap="true" align="center" colspan="4"><xsl:value-of select='.'/></td>

							<td style="display:none"/>
							<td style="display:none"/>
							<td style="display:none"/>
					
	
					</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>

	<xsl:for-each select="/root/tbody/tr[td[1]='3']">
		<tr noWrap="true" class="mainHead">
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1 or position()=2 ">
					</xsl:when>
					<xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=12 or position()=17 or position()=22 or position()=27">				
							<td style="display:none"/>				
					</xsl:when>									
					<xsl:otherwise>
						<td nowrap="true" ><xsl:value-of select='.'/></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>	
	
	
	<xsl:for-each select="/root/tbody/tr[td[1]='222']">
		<tr noWrap="true" class="mainHead">
			<xsl:for-each select='td'>
				<xsl:choose>
						<xsl:when test="position()=1 or position()=2">
					  </xsl:when>
						<xsl:otherwise>
						  <td nowrap="true" ><xsl:value-of select='.'/></td>
						</xsl:otherwise>
					</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>	
	
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr[td[1]!='1' and td[1]!='2' and  td[1]!='3' and td[1]!='222']">
      <tr>
				<xsl:for-each select="td">
	              <xsl:choose>
	                <xsl:when test="position()=1 or position()=2">
	                </xsl:when>
	                <xsl:when test="position()=3 or position()=4">
	                  	<xsl:if test=".='合计'">
	                  		  <td noWrap="true" align='right'  style="background-color:lightblue;font-weight:bold">
	                  		      <xsl:value-of select="."/>
	                  		   </td>
	                  	</xsl:if>
	                  	
	                  	<xsl:if test=".!='合计'">
	                  		  <td noWrap="true">
	                  		      <xsl:value-of select="."/>
	                  		   </td>
	                  	</xsl:if>
	                </xsl:when>
	                <xsl:when test="position()=5">
	                  <td noWrap="true" align="center">
	                    <xsl:value-of select="."/>
	                  </td>
	                </xsl:when>
	                
	                <xsl:otherwise>
	                	<xsl:if test=". != ''">
	                		<td noWrap="true" class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	                	</xsl:if>
	                	<xsl:if test=". = ''">
	                		<td noWrap="true" class="numberText" align="right">0.00</td>
	                	</xsl:if>
	                </xsl:otherwise>
	              </xsl:choose>
	          </xsl:for-each>
			</tr>
	              
			</xsl:for-each>
		</tbody>
		<tfoot>
				<xsl:for-each select="/root/tbody/tr[1]">
		  	<tr noWrap='true'>
					<xsl:for-each select='td'>
						<xsl:choose>
							<xsl:when test="position()=1 or position()=2">
							</xsl:when>
							<xsl:when test="position()=3">
									 <td noWrap='true' align='right'>
									 		<xsl:attribute name='colspan'>
									 				<xsl:value-of select='$count1'/>
									 			</xsl:attribute>
									 	</td>
							</xsl:when>
							<xsl:otherwise>
									<td style="display:none"></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
	  </tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
