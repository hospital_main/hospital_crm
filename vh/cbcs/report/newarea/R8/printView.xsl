<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		
	<thead>
			<tr noWrap='true'>
				 <td style='fontsize:maintitle;colspan:10'></td>
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
		
		
				
		 </tr>
		<tr noWrap='true'>
				 <td style='colspan:10'></td>
				 	 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
					<td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 

				  
				
				  
		 </tr>
		<tr noWrap='true'>
				 <td style='colspan:8' align="left"></td>
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
         <td style="display:none"></td> 
				<td  align='right' width='180'>金额单位：元</td> 			 
		 </tr>
	   <tr noWrap="true" class="mainHead">
	   		
				<td nowrap="true" rowspan="2">医院科室名称</td>
						
				<td nowrap="true" rowspan="2">标准科室名称</td>
				<td nowrap="true" rowspan="2">行次</td>	
				<td nowrap="true" rowspan="2">收入-成本</td>			
				<td nowrap="true" rowspan="2">收入</td>	
				<td nowrap="true" rowspan="2">医疗成本</td>
				<td nowrap="true" rowspan="2">直接成本</td>		
				<td nowrap="true" colspan="3">间接成本</td>
				<td style="display:none"></td>
				<td style="display:none"></td>

			
		</tr> 
		<tr noWrap="true" class="mainHead">
			<td style="display:none"></td>		
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		  <td style="display:none"></td>
			<td style="display:none"></td>
			<td nowrap="true">小计</td>
		  <td nowrap="true">分摊行政后勤类科室成本</td>
			<td nowrap="true">分摊医疗辅助类科室成本</td>

		</tr> 
		<tr noWrap="true" class="mainHead">
				<td noWrap="true">栏次</td>
				
								<td noWrap="true"></td>
				<td noWrap="true"></td>
				<td noWrap="true">1=2-3</td>
				<td noWrap="true">2</td>
				<td noWrap="true">3=4+5</td>
				<td noWrap="true">4</td>
				<td noWrap="true">5=6+7</td>
				<td noWrap="true">6</td>
				<td noWrap="true">7</td>
		</tr> 
	</thead>
	<tbody>
	<xsl:for-each select="/root/tbody/tr">
		<xsl:variable name="positionRow" select="position()"></xsl:variable> 
	<tr>
		<xsl:for-each select="td">
			<xsl:choose>
				<xsl:when test="position()=1 or position()=3">
				</xsl:when>
				<xsl:when test="position()=2 or position()=4">
					  <xsl:if test=".='合计'">
					  	<td align="right"><xsl:value-of select='.'/></td>
					  </xsl:if>
					  <xsl:if test=".!='合计'">
					  	<td align="left"><xsl:value-of select='.'/></td>
					  </xsl:if>
				</xsl:when>
				<xsl:when test="position()=5">
				<td align="center"><xsl:value-of select='$positionRow'/></td>
				</xsl:when>
				
				
				<xsl:otherwise>
				  	<td align="right">
							<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
					 </td>   
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</tr>
	</xsl:for-each>
	</tbody>
	<tfoot>
			<tr noWrap='true'>
				 <td style='colspan:10;align:right'></td>
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
		
		
				
		 </tr>
	</tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>
