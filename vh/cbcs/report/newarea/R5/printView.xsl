<!--
 $Header: /cvsroot/OESBASE/ROOT/vh/cbcs/report/newarea/R5/printView.xsl,v 1.7 2016/04/16 06:49:01 wangzhen Exp $
 $Author: wangzhen $
 $Date: 2016/04/16 06:49:01 $
 $Revision: 1.7 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">   
  <xsl:variable name="count" select="count(//root/tbody/tr[1]/td)"/>
  <root>
	  <colgroup>		
		  <xsl:for-each select="/root/tbody/tr[1]">
				<xsl:for-each select='td'>
					<xsl:choose>
						<xsl:when test="position()=1">
							<col style = 'width:200mm'/>	
						</xsl:when>
						<xsl:when test="position() > 1 and position() &lt; ($count - 3) ">
							<col style = 'width:100mm'/>
							<col style = 'width:100mm'/>
						</xsl:when>
						<xsl:otherwise>
							<col style = 'width:100mm'/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:for-each>
		</colgroup>
<thead>
		  <xsl:for-each select="/root/tbody/tr[1]">
		  	<tr noWrap='true'>
					<xsl:for-each select='td'>
						<xsl:choose>
							<xsl:when test="position()=1">
							 	<td noWrap='true' style="fontsize:maintitle;">
							 		<xsl:attribute name='colspan'>
						 				<xsl:value-of select="$count "/>
						 			</xsl:attribute>
							 	</td>
							</xsl:when>
						
							<xsl:otherwise>
								<td style="display:none"></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>   
			<xsl:for-each select="/root/tbody/tr[1]">
		  	<tr noWrap='true'>
					<xsl:for-each select='td'>
						<xsl:choose>
							<xsl:when test="position()=1">
							 	<td noWrap='true' align='center'>
							 		<xsl:attribute name='colspan'>
						 				<xsl:value-of select="$count  "/>
						 			</xsl:attribute>
							 	</td>
							</xsl:when>
					
							<xsl:otherwise>
								<td style="display:none"></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		
			<xsl:for-each select="/root/tbody/tr[1]">
		  	<tr noWrap='true'>
					<xsl:for-each select='td'>
						<xsl:choose>
							<xsl:when test="position()=1">
							  <td noWrap='true' align='left'>
							 		<xsl:attribute name='colspan'>
							 			<xsl:value-of select="$count "/>
							 		</xsl:attribute>
							 	</td>
							</xsl:when>
							<xsl:otherwise>
								<td style="display:none"></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			
			<xsl:for-each select="/root/tbody/tr[position()=1 or position()=2 or position()=3]">
          <xsl:choose>
          						<xsl:when test="position() = 1">
						
						<tr noWrap="true" class="mainHead">
							<xsl:for-each select="td">
					
						<xsl:choose>
							<xsl:when test="position() = 1 or position()=2 or position()=3">
									<td>
										<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:attribute name="rowspan">2</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
												
								</xsl:when>
							  <xsl:when test="position()>3 and position() &lt; last()-3  ">
						
							
									<xsl:if test=" position() mod 2=0">
									  <td>
												
									 	<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:attribute name="colspan">2</xsl:attribute>
										<xsl:value-of select="."/>
										</td>
					        </xsl:if>
	                <xsl:if test=" position() mod 2=1">
									  <td style="display:none">
												
								
										
										<xsl:value-of select="."/>
										</td>
					        </xsl:if>

								</xsl:when>
								<xsl:otherwise>
									<td>
									<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:attribute name="rowspan">2</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
												
								</xsl:otherwise>
									</xsl:choose>
						 
							</xsl:for-each>
						</tr>
					</xsl:when>
						<xsl:when test="position() = 2">
						<tr noWrap="true" class="mainHead">
							<xsl:for-each select="td">
							
								<xsl:choose>
								<xsl:when test="position() = 1 or position()=2 or position()=3">
										<td >
											<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:value-of select="."/>
										</td>
								</xsl:when>
								<xsl:when test="position()>3 and position() &lt; last()-3">
									<td >
									<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
												
								</xsl:when>
									</xsl:choose>
									</xsl:for-each >
							</tr>
						</xsl:when>
							<xsl:when test="position() = 3">
						<tr noWrap="true" class="mainHead">
							<xsl:for-each select="td">
							
				       	<td width="100" align="center">
									
										<xsl:value-of select="."/>
									</td>
				
									</xsl:for-each >
							</tr>
						</xsl:when>
          </xsl:choose>
      </xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:choose>

					<xsl:when test="position() &gt; 3">
						<tr>
							<xsl:for-each select="td">   
							
										<xsl:choose>
								<xsl:when test="position() = 1 or position()=2 ">
	                  	<xsl:if test=".='�ϼ�'">
	                  		  <td noWrap="true" align='right'  style="background-color:lightblue;font-weight:bold">
	                  		      <xsl:value-of select="."/>
	                  		   </td>
	                  	</xsl:if>
	                  	
	                  	<xsl:if test=".!='�ϼ�'">
	                  		  <td noWrap="true">
	                  		      <xsl:value-of select="."/>
	                  		   </td>
	                  	</xsl:if>
								</xsl:when>
									<xsl:when test=" position()=3">
								
								   	<td align="center" >
											<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
									<xsl:when test="position() &gt; 3">
								
											<td align="right" >
											<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:when>
								</xsl:choose>	
							</xsl:for-each>
						</tr>
	        </xsl:when>
				</xsl:choose>
			</xsl:for-each>
		</tbody>
	<tfoot>
			<xsl:for-each select="/root/tbody/tr[1]">
		  	<tr noWrap='true'>
					<xsl:for-each select='td'>
						<xsl:choose>
							<xsl:when test="position()=1">
						 		<td noWrap='true' align='right'>
						 			<xsl:attribute name='colspan'>
						 				<xsl:value-of select="($count - 5)*2 + 5 "/>
						 			</xsl:attribute>
						 		</td>
							</xsl:when>
							<xsl:otherwise>
								<td style="display:none"></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>   
	  </tfoot>
  </root>
	</xsl:template>
</xsl:stylesheet>
