<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:for-each select="/root/tbody/tr[position() &lt; 4]">
			 	<xsl:choose>    
					<xsl:when test="position() = 1">
						<tr noWrap="true" class="mainHead">
							<xsl:for-each select="td">
							
								<xsl:variable name="endcol" select="last()"/>
								<xsl:choose>
								<xsl:when test="position() = 1 or position()=2 or position()=3">
									<td>
										<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:attribute name="rowspan">2</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
												
								</xsl:when>
			
							<xsl:when test="position()>3 and position() &lt; last()-3 ">
									<xsl:if test=" position() mod 2=0">
									  <td>
												
									 	<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:attribute name="colspan">2</xsl:attribute>
										<xsl:value-of select="."/>
										</td>
					        </xsl:if>
												
								</xsl:when>
									<xsl:when test="position()> last()-4">
									<td width="100">
										<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:attribute name="rowspan">2</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
												
								</xsl:when>
									</xsl:choose>
									</xsl:for-each >
							</tr>
						</xsl:when>
					<xsl:when test="position() = 2">
						<tr noWrap="true" class="mainHead">
							<xsl:for-each select="td">
								<xsl:variable name="endcol" select="last()"/>
								<xsl:choose>
								<xsl:when test="position() = 1 or position()=2 or position()=3">
									  <td style="display:none">
												
									
										</td>
												
								</xsl:when>
								<xsl:when test="position()>3  and position() &lt; last()-3">
									<td width="100">
									<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
												
								</xsl:when>
									<xsl:when test="position()> last()-4">
							
												
								</xsl:when>
									</xsl:choose>
									</xsl:for-each >
							</tr>
						</xsl:when>
							<xsl:when test="position() = 3">
						<tr noWrap="true" class="mainHead">
							<xsl:for-each select="td">
								
				       	<td align="center">
									
										<xsl:value-of select="."/>
									</td>
					    
					  
									</xsl:for-each >
							</tr>
						</xsl:when>
						</xsl:choose>
				
				</xsl:for-each>
		</thead>
	
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; 3]">
					  
						<tr>
		
							<xsl:for-each select="td">  
							<xsl:choose>
								<xsl:when test="position() = 1 or position()=2 ">
	                  	<xsl:if test=".='�ϼ�'">
	                  		  <td noWrap="true" align='right'  style="background-color:lightblue;font-weight:bold">
	                  		      <xsl:value-of select="."/>
	                  		   </td>
	                  	</xsl:if>
	                  	
	                  	<xsl:if test=".!='�ϼ�'">
	                  		  <td noWrap="true">
	                  		      <xsl:value-of select="."/>
	                  		   </td>
	                  	</xsl:if>
								</xsl:when>
									<xsl:when test=" position()=3">
								
								   	<td align="center" >
											<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
									<xsl:when test="position() &gt; 3">
								
											<td align="right" >
											<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:when>
								</xsl:choose>	
							</xsl:for-each>
						</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
