<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
	<thead>
		<tr noWrap='true'>
				 <td style='fontsize:maintitle;colspan:3'></td>
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
	
		 </tr>
		<tr noWrap='true'>
				 <td style='colspan:3'></td>
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
		
		 </tr>
		<tr noWrap='true'>
				 <td style='colspan:3' align="left"></td>
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
			
		 </tr>
		 <tr noWrap="true" class="mainHead">
		 	<td nowrap="true" colspan="2">指标名称</td>
				<td nowrap="true" colspan="2" >行次</td>
				<td nowrap="true" colspan="2" width='180'>值</td>
		</tr> 
	</thead>
	 <tbody>
    	<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=2">
								<td align='center'><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=3">
					 			<td align="right">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
				      </xsl:when>
          	</xsl:choose>
  			  </xsl:for-each>
  			  </tr>
   		</xsl:for-each>       
    </tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
