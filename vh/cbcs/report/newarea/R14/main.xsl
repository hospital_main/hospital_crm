<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>指标名称</th>
        <th nowrap='true'>行次</th>
        <th nowrap='true'>值</th>
        <th nowrap='true' style="display:none">年月</th>
        <th nowrap='true' style="display:none">编码</th>
      </tr>
    </thead>
    <tbody>
    	<xsl:for-each select="/root/tbody/tr">
    			<xsl:variable name="pos" select="position()"/>
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=2">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=3">
					 			<td align="right">
					 				<xsl:choose>
					 					<!--合计项-->
					 					 <xsl:when test="$pos=1 or $pos=8 or $pos=13 or $pos=37">
					 					 	<input type='text' name='pdvalues' class='inputTextA' required='true' style="display:none;text-align:right">
														<xsl:attribute name="id">TRTR<xsl:value-of select="$pos"/>Id<xsl:value-of select="position()"/></xsl:attribute>
														<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>		
														<xsl:attribute name="year_month"><xsl:value-of select="../pk/year_month"/></xsl:attribute>
													  <xsl:attribute name="base_code"><xsl:value-of select="../pk/base_code"/></xsl:attribute>																															
												  </input>
					 					 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
					 					 </xsl:when>
                     <!--不合计也不维护项-->
					 					 <xsl:when test="$pos=44 or $pos=45 or $pos=49 or $pos=55 or $pos=58">
					 					  		 	<input type='text' name='pdvalues' class='inputTextA' required='true' style="display:none;text-align:right">
														<xsl:attribute name="id">TRTR<xsl:value-of select="$pos"/>Id<xsl:value-of select="position()"/></xsl:attribute>
														<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>		
														<xsl:attribute name="year_month"><xsl:value-of select="../pk/year_month"/></xsl:attribute>
													  <xsl:attribute name="base_code"><xsl:value-of select="../pk/base_code"/></xsl:attribute>																															
												  </input>
					 					 </xsl:when>
					 					 	
					 					 <xsl:otherwise>
								 					<input type='text' name='pdvalues' class='inputTextA' required='true' style="text-align:right" maxlength="9">
														<xsl:attribute name="id">TRTR<xsl:value-of select="$pos"/>Id<xsl:value-of select="position()"/></xsl:attribute>
														<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>		
														<xsl:attribute name="year_month"><xsl:value-of select="../pk/year_month"/></xsl:attribute>
													  <xsl:attribute name="base_code"><xsl:value-of select="../pk/base_code"/></xsl:attribute>																															
												  </input>
					 					 </xsl:otherwise>
					 					 
					 			  </xsl:choose>
					 			
					 			 

								</td>
				      </xsl:when>
				      	<xsl:when test="position()=4">
								<td style="display:none">
									<xsl:value-of select="."/>	
									</td>
							</xsl:when>
								<xsl:when test="position()=5">
								<td style="display:none">
									<xsl:value-of select="."/>
									</td>
							</xsl:when>
          	</xsl:choose>
  			  </xsl:for-each>
  			  </tr>
   		</xsl:for-each>       
    </tbody>
  </xsl:template>
</xsl:stylesheet>


