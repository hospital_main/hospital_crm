<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
	         <tr noWrap="true" class="mainHead">
				<td nowrap="true" rowspan="2">医院科室名称</td>
				<td nowrap="true" rowspan="2">标准科室名称</td>
				<td nowrap="true" rowspan="2">行次</td>				
				<td nowrap="true" rowspan="2">医疗成本</td>
				<td nowrap="true" rowspan="2">直接成本</td>
				<td nowrap="true" colspan="4">间接成本</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>		
		</tr> 
		<tr noWrap="true" class="mainHead">
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
					<td style="display:none"></td>
							<td style="display:none"></td>
				<td nowrap="true">小计</td>
				<td nowrap="true">分摊行政后勤类科室成本</td>
				<td nowrap="true">分摊医疗辅助类科室成本</td>
				<td nowrap="true">分摊医疗技术类科室成本</td>		
		</tr> 
		<tr noWrap="true" class="mainHead">
				<td noWrap="true">栏次</td>
				<td noWrap="true"></td>
				<td noWrap="true"></td>
				<td noWrap="true">1=2+3</td>
				<td noWrap="true">2</td>
				<td noWrap="true">3=4+5+6</td>
				<td noWrap="true">4</td>
				<td noWrap="true">5</td>
				<td noWrap="true">6</td>
		</tr> 
	</thead>
	<tbody>
	<xsl:for-each select="/root/tbody/tr">
	<xsl:variable name="positionRow" select="position()"></xsl:variable> 
	<xsl:variable name="dept_name" select="td[3]" />
	<tr>
		<xsl:for-each select="td">
			<xsl:choose>
			
				<xsl:when test="position()=1 or position()=2">
				</xsl:when>
				
					<xsl:when test="position()=3" >
					<xsl:if test="$dept_name='临床服务类小计' or $dept_name='医疗技术类小计' or $dept_name='医疗辅助类小计' or $dept_name='行政后勤类小计' or $dept_name='合计'">
						<td align="right" style="background-color:lightblue;font-weight:bold" >
	                				<xsl:value-of select="$dept_name"/>
						</td>
					</xsl:if>
					<xsl:if test="$dept_name!='临床服务类小计' and $dept_name!='医疗技术类小计' and $dept_name!='医疗辅助类小计' and $dept_name!='行政后勤类小计' and $dept_name!='合计'">
						<td>
	                				<xsl:value-of select="."/>
						</td>
					</xsl:if>
				</xsl:when>
				
				<xsl:when test="position()=4">
				</xsl:when>
				
				
				<xsl:when test="position()=5">
						<td>
	                				<xsl:value-of select="."/>
						</td>
				</xsl:when>
				
				<xsl:when test="position()=6 or position()=7 or position()=8">
				</xsl:when>
				
					<xsl:when test="position()=9">
						<td align="center">
	                <xsl:value-of select=	"$positionRow"/>
						</td>
				</xsl:when>
				
				<xsl:when test="position()=12 or position()=13">
					
				</xsl:when>
				
			
				<xsl:when test="position() &gt; 9">
				
					<xsl:if test="$dept_name='临床服务类小计' or $dept_name='医疗技术类小计' or $dept_name='医疗辅助类小计' or $dept_name='行政后勤类小计' or $dept_name='合计'">
						<td align="right">
									<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
						</td>
					</xsl:if>
					<xsl:if test="$dept_name!='临床服务类小计' and $dept_name!='医疗技术类小计' and $dept_name!='医疗辅助类小计' and $dept_name!='行政后勤类小计' and $dept_name!='合计'">
						<td align="right">
									<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
						</td>
					</xsl:if>
				</xsl:when> 
				<xsl:otherwise>
				</xsl:otherwise>
			
			</xsl:choose>
		</xsl:for-each>
	</tr>
	</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
