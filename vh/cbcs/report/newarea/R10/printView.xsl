<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap='true'>
				 <td style='fontsize:maintitle;colspan:32'></td>
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				  <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td>
		
					
		 </tr>
		<tr noWrap='true'>
			 <td style='colspan:32'></td>
					<td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				  <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td>
		 </tr>
		<tr noWrap='true'>
				 <td style='colspan:31' align="left"></td>
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td  align='right' width='180'>金额单位：元</td>
		 </tr>

			<tr noWrap="true" class="mainHead"> 
			        <td nowrap="true" rowspan="2">医院科室名称</td> 
			        <td noWrap="true" rowspan="2">标准科室名称</td> 
			        <td noWrap="true" rowspan="2">行次</td>
			        <td noWrap="true" rowspan="2">收入-成本</td> 
			        <td noWrap="true" rowspan="2">医疗成本合计</td>
			        <td noWrap="true" rowspan="2">医疗收入合计</td>    
			        <td noWrap="true" colspan="12" >门诊收入</td>
			        <td style="display:none"/>		<td style="display:none"/>		<td style="display:none"/>
			        <td style="display:none"/>		<td style="display:none"/>		<td style="display:none"/>
			        <td style="display:none"/>		<td style="display:none"/>		<td style="display:none"/>
			        <td style="display:none"/>		<td style="display:none"/>
			        <td noWrap="true" colspan="13" >住院收入</td>
			        <td style="display:none"/>		<td style="display:none"/>		<td style="display:none"/>
			        <td style="display:none"/>		<td style="display:none"/>		<td style="display:none"/>
			        <td style="display:none"/>		<td style="display:none"/>		<td style="display:none"/>
			        <td style="display:none"/>		<td style="display:none"/>		<td style="display:none"/>
			        <td noWrap="true" rowspan="2">其他业务收入</td> 
			 </tr>
			 <tr noWrap="true" class="mainHead">
			 		<td style="display:none"/>
			 		<td style="display:none"/>
			 		<td style="display:none"/>
			 		<td style="display:none"/>
			 	  <td style="display:none"/>
			 		<td style="display:none"/>
			 	<td nowrap="true">门诊收入合计</td>
			  <td nowrap="true">挂号收入</td>
				<td nowrap="true">诊察收入</td>
				<td nowrap="true">检查收入</td>	
				<td nowrap="true">治疗收入</td>	
				<td nowrap="true">手术收入</td>	
				<td nowrap="true">化验收入</td>

		
				<td nowrap="true">卫生材料收入</td>	
				<td nowrap="true">药品收入</td>
				<td nowrap="true">药事服务费收入</td>	
				<td nowrap="true">其他门诊收入</td>
				<td nowrap="true">门诊结算差额</td>	
				<td nowrap="true">住院收入合计</td>
				<td nowrap="true">床位收入</td>
				<td nowrap="true">诊察收入</td>
				<td nowrap="true">检查收入</td>
				<td nowrap="true">治疗收入</td>
				<td nowrap="true">手术收入</td>
				<td nowrap="true">化验收入</td>

			
				<td nowrap="true">护理收入</td>
				<td nowrap="true">卫生材料收入</td>
				<td nowrap="true">药品收入</td>
				<td nowrap="true">药事服务费收入</td>
        <td nowrap="true">其他住院收入</td>
        <td nowrap="true">住院结算差额</td>
			
	  	</tr>
	                 
			<tr noWrap="true" class="mainHead">
		 		<td nowrap="true">栏次</td>
		 		<td nowrap="true"></td>
		 		<td nowrap="true"></td>
		 		<td nowrap="true">1</td>
		 	  <td nowrap="true">2</td>
		 		<td nowrap="true">3</td>
			 	<td nowrap="true">4</td>
			  <td nowrap="true">5</td>
				<td nowrap="true">6</td>
				<td nowrap="true">7</td>	
				<td nowrap="true">8</td>	
				<td nowrap="true">9</td>	
				<td nowrap="true">10</td>
				<td nowrap="true">11</td>	
				<td nowrap="true">12</td>
				<td nowrap="true">13</td>	
				<td nowrap="true">14</td>
				<td nowrap="true">15</td>	   
				<td nowrap="true">16</td>
				<td nowrap="true">17</td>
				<td nowrap="true">18</td>
				<td nowrap="true">19</td>
				<td nowrap="true">20</td>
				<td nowrap="true">21</td>
				<td nowrap="true">22</td>
				<td nowrap="true">23</td>
				<td nowrap="true">24</td>
				<td nowrap="true">25</td>
				<td nowrap="true">26</td>
        <td nowrap="true">27</td>
        <td nowrap="true">28</td>
			  <td nowrap="true">29</td>
	    </tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="positionRow" select="position()"></xsl:variable> 		
				 <tr>
		      <xsl:for-each select="td">
						<xsl:choose>
				  		<xsl:when test="position()=1 or position()=34 or position()=35 or position()=36 or position()=37"> 
				 			</xsl:when>
							<xsl:when test="position()=2 or position()=3">  

								  <xsl:if test=".='合计'">
                		  <td noWrap="true" align='right'  style="background-color:lightblue;font-weight:bold">
                		      <xsl:value-of select="."/>
                		   </td>
                	</xsl:if>
                	
                	<xsl:if test=".!='合计'">
                		  <td noWrap="true">
                		      <xsl:value-of select="."/>
                		   </td>
                	</xsl:if>
							</xsl:when>
						 <xsl:when test="position()=4">  
							   <td  align='center'>
									<xsl:value-of select="$positionRow"/>
								</td> 
						 </xsl:when>
					<xsl:otherwise>
						  <td align="right">
									<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
						</td> 
		      </xsl:otherwise>
 				</xsl:choose>
					</xsl:for-each>
	    </tr>
		</xsl:for-each>
	</tbody>
		<tfoot>
			<tr noWrap='true'>
				 <td style='colspan:32;align:right'></td>
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				  <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td> 
				 <td style="display:none"></td>
			
		 </tr>
	</tfoot>
	</root>
	</xsl:template>
</xsl:stylesheet>


