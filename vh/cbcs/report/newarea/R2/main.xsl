<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:for-each select="/root/tbody/tr">
						<xsl:choose>
							<xsl:when test="position() = 1 or position()=2 ">
								<tr noWrap="true" class="mainHead">
									<xsl:for-each select="td">   
										<td>
											<xsl:attribute name="noWrap">true</xsl:attribute>
											<xsl:value-of select="."/>
										</td>
									</xsl:for-each>
								</tr>
							</xsl:when>
						</xsl:choose>
			</xsl:for-each>
		
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:choose>
					<xsl:when test="position() = 1">
					</xsl:when>
					<xsl:when test="position() = 2">
					</xsl:when>
					
					
					<xsl:otherwise>
						<tr>
							<xsl:for-each select="td">   
								<xsl:choose>
										<xsl:when test="position() = 1">
								
											
	                	<xsl:if test=". = '临床服务类小计' or .='医疗技术类小计' or .='医疗辅助类小计' or .='医疗业务成本小计' or .='行政后勤类小计' or .='合计'">
	                				<td align="right" style="background-color:lightblue;font-weight:bold">	<xsl:attribute name="noWrap">true</xsl:attribute>
													<xsl:value-of select="."/>	</td>
	                	</xsl:if>
	                	
								    <xsl:if test=". != '临床服务类小计' and . !='医疗技术类小计' and . !='医疗辅助类小计' and . !='医疗业务成本小计' and . !='行政后勤类小计' and . !='合计'">
	                				<td>	<xsl:attribute name="noWrap">true</xsl:attribute>
													<xsl:value-of select="."/>	</td>
	                	</xsl:if>
									
								
									</xsl:when>
									
									<xsl:when test="position() = 2">
									<td>
										<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
									</xsl:when>
									
									<xsl:when test="position() = 3">
									<td align="center">
										<xsl:attribute name="noWrap">true</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
									</xsl:when>
									<xsl:otherwise>
										<td align="right">
											<xsl:attribute name="class">numberText</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</tr>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
