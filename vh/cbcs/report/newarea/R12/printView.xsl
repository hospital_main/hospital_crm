<!--
 $Header: /cvsroot/OESBASE/ROOT/vh/cbcs/report/newarea/R12/printView.xsl,v 1.2 2016/04/18 06:49:57 wangzhen Exp $
 $Author: wangzhen $
 $Date: 2016/04/18 06:49:57 $
 $Revision: 1.2 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  		<xsl:variable name="wnr" select="/root/tbody/tr[1]/td[last()-1]"/>
  		<xsl:variable name="count" select="count(//root/tbody/tr[1]/td)"/>
  		<xsl:variable name="count1" select="count(//root/tbody/tr[1]/td)-1"/>
  <root>
	  <colgroup>		
	  <xsl:for-each select="/root/tbody/tr[1]">
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							<col style = 'width:200mm'/>	
					</xsl:when>
					<xsl:otherwise>
						<col style = 'width:100mm'/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
	
		</xsl:for-each>   
		</colgroup>
  	<thead>
  <xsl:for-each select="/root/tbody/tr[1]">
  	<tr noWrap='true'>
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							 <td style='fontsize:maintitle;colspan:colcount'></td>
					</xsl:when>
					<xsl:otherwise>
						<td style="display:none"></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>   
	  
	  
	 <xsl:for-each select="/root/tbody/tr[1]">
  	<tr noWrap='true'>
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							 <td noWrap='true' align='center'>
							 		<xsl:attribute name='colspan'>
							 				<xsl:value-of select="$count"/>
							 			</xsl:attribute>
							 	</td>
					</xsl:when>
					<xsl:otherwise>
							<td style="display:none"></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>   
	  	
	 <xsl:for-each select="/root/tbody/tr[1]">
  	<tr noWrap='true'>
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							 <td noWrap='true' align='left'>
							 		<xsl:attribute name='colspan'>
							 				<xsl:value-of select="$count1"/>
							 		</xsl:attribute>
							 	</td>
					</xsl:when>
					<xsl:when test="position()=$count">
							 <td noWrap='true' align='right' width='100'>
							 		金额单位：元
							 	</td>
					</xsl:when>
					<xsl:otherwise>
							<td style="display:none"></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>   

	 <xsl:for-each select="/root/tbody/tr[position()=1 or position()=2]">
  	<tr noWrap='true'>
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
								<td noWrap='true'>
									
									<xsl:value-of select="."/>
								</td>
					</xsl:when>
					<xsl:otherwise>
							<td noWrap='true'>
								<xsl:value-of select="."/>
							</td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>   
		
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:choose>
					<xsl:when test="position() = 1 or position()=2">
					</xsl:when>
					<xsl:otherwise>
						<tr>
							<xsl:for-each select="td">   
								<xsl:choose>
									<xsl:when test="position() = 1 or position()=2">
								
										<xsl:attribute name="noWrap">true</xsl:attribute>
	                	<xsl:if test=". = '临床服务类小计' or .='医疗技术类小计' or .='医疗辅助类小计' or .='医疗业务成本小计' or .='行政后勤类小计' or .='合计'">
	                				<td align="right" style="background-color:lightblue;font-weight:bold">	<xsl:attribute name="noWrap">true</xsl:attribute>
													<xsl:value-of select="."/>	</td>
	                	</xsl:if>
	                	
								    <xsl:if test=". != '临床服务类小计' and . !='医疗技术类小计' and . !='医疗辅助类小计' and . !='医疗业务成本小计' and . !='行政后勤类小计' and . !='合计'">
	                				<td>	<xsl:attribute name="noWrap">true</xsl:attribute>
													<xsl:value-of select="."/>	</td>
	                	</xsl:if>
									  
									</xsl:when>
									<xsl:when test="position()=3">
									    <td align="center"><xsl:value-of select="."/></td>
									</xsl:when>
									
									<xsl:otherwise>
										<td align='right'>
											<xsl:attribute name="class">numberText</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</tr>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>	
		</tbody>
	<tfoot>
			
		<xsl:for-each select="/root/tbody/tr[1]">
  	<tr noWrap='true'>
			<xsl:for-each select='td'>
				<xsl:choose>
					<xsl:when test="position()=1">
							 <td noWrap='true' align='right'>
							 		<xsl:attribute name='colspan'>
							 				<xsl:value-of select="$count"/>
							 			</xsl:attribute>
							 	</td>
					</xsl:when>
					<xsl:otherwise>
							<td style="display:none"></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</tr>
	</xsl:for-each>   

	</tfoot>
  </root>
	</xsl:template>
</xsl:stylesheet>
