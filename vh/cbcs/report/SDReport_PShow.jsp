<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/report/SDReport_PShow.jsp,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:03 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO, com.viewhigh.cbcs.base.mvc.view.TableMarge,
								 com.viewhigh.cbcs.base.mvc.view.component.Select" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
function showmodul(){
    var tim=new Date();
   	window.showModalDialog('selfDreport.jspviewhigh?subFunction=show&rep_ids='+template.rep_ids.value+"&year_month="+template.year_month.value+'&time='+tim.toLocaleString(),'modify','status:no;dialogTop:150px;dialogLeft:270px;dialogHeight: 340px; dialogWidth: 400px;scrolling:no')
   	if (template!=null && template._old_current_page!=null) {
      template._current_page.value = template._old_current_page.value
    }
   	show_wait();
   	find();
  }    
function find2() {
		alert(template.rep_ids.value+template.year_month.value);
    template.subFunction.value='show';
    show_wait();
    template.submit();
    return true;
  }
function find() {
	if(template.rep_ids.value == "")
    {
      alert('请选择报表名称!');
      return false;
    }
	if(template.syear.value == "")
    {
      alert('请选择核算年份!');
      return false;
    }
    if(template.fmonth.value == "")
    {
      alert('请选择核算开始月份!');
      return false;
    }
    if(template.tmonth.value == "")
    {
      alert('请选择核算结束月份!');
      return false;
    }
    if(template.fmonth.value > template.tmonth.value ) {
      alert('开始月份不能晚于结束月份!');
      return false;
    }
  var tim=new Date();
  var screenHeight=window.screen.availHeight;
  var screenWidth=window.screen.availWidth;  
  var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto"
  var win=window.showModalDialog('selfDreport.jspviewhigh?subFunction=show&rep_ids='+template.rep_ids.value+"&year_month="+template.syear.value+template.fmonth.value+"&end_year_month="+template.syear.value+template.tmonth.value+"&report_name="+template.rep_ids[template.rep_ids.selectedIndex].text+'&time='+tim.toLocaleString(),window,dialogStyle)
}  
</Script>

<html:html clazz="main">
	<form name="template" method="post" action="selfDreport.jspviewhigh">

	  <html:message/>

	  <html:title clazz='module'>报表查询</html:title>
	    <%
		String[][] year = {{"2000","2000"},{"2001","2001"},{"2002","2002"},{"2003","2003"},{"2004","2004"},{"2005","2005"},{"2006","2006"},{"2007","2007"},{"2008","2008"},{"2009","2009"},{"2010","2010"},{"2011","2011"},{"2012","2012"},{"2013","2013"},{"2014","2014"},{"2015","2015"},{"2016","2016"},{"2017","2017"},{"2018","2018"},{"2019","2019"},{"2020","2020"}};
		String[][] month = {{"01","01"},{"02","02"},{"03","03"},{"04","04"},{"05","05"},{"06","06"},{"07","07"},{"08","08"},{"09","09"},{"10","10"},{"11","11"},{"12","12"}};
		%>
	  <!-- 简单信息 -->
	  <html:table clazz="simple">
	    <tr>
	      <td nowrap class="signText">报表名称：</td>	      
	     <td nowrap colspan='3' align='left'><%=new Select(request.getAttribute("repList"),"rep_ids","",false,false)%></td>
	    </tr>
	    <tr>
   		 <td class="signText">核算月:</td>
		 <td class="signText"> <%=new Select(year,"syear",request.getParameter("syear")==null? "":request.getParameter("syear"),false,false)%>年
		 <%=new Select(month,"fmonth",request.getParameter("fmonth")==null? "":request.getParameter("fmonth"),false,false)%>月 到 
		 <%=new Select(month,"tmonth",request.getParameter("tmonth")==null? "":request.getParameter("tmonth"),false,false)%>月</td>
      </tr> 
	    <tr>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      <td class="normalText"></td>
	      <td class="normalText">
	      <button class="pageBtn" name=""  onclick="return find();" >查询</button>
	      <!--<img src="images/find.gif" class="mouse" onclick="return find();" />--></td>
	    </tr>	
	  </html:table>
	  <input type=hidden name="subFunction"/>
	</form>
</html:html>

