<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
	<xsl:decimal-format NaN=''/>
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true' >指标体系</th>
      	<th nowrap='true' >本期实际</th>
      	<th nowrap='true' >预算</th>
      	<th nowrap='true' >上年同期</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
				            <td>
			  	            <xsl:value-of select="."/>
				            </td>
                </xsl:when>
                                
                <xsl:otherwise>
			            <td align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

