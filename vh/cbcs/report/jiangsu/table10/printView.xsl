<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<root>
    	<thead>
    		<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	        <td style="display:none"/>
	        <td style="display:none"/>
	        <td style="display:none"/>
	  		</tr>
    		<tr noWrap='true'>
	        <td style='fontsize:subtitle;colspan:colcount'></td>
	        <td style="display:none"/>
	        <td style="display:none"/>
	        <td style="display:none"/>
	  		</tr>
				<tr noWrap='true' class='mainHead'>
	      	<td nowrap='true' >指标体系</td>
	      	<td nowrap='true' >本期实际</td>
	      	<td nowrap='true' >预算</td>
	      	<td nowrap='true' >上年同期</td>
				</tr>
			</thead>
    	<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
				            <td>
			  	            <xsl:value-of select="."/>
				            </td>
                </xsl:when>
                                
                <xsl:otherwise>
			            <td align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    	</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>