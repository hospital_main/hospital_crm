<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>统计年月</th>
      	<th nowrap='true'>医疗项目</th>
      	<th nowrap='true'>开单科室</th>
      	<th nowrap='true'>执行科室</th>
      	<th nowrap='true'>金额</th>
      	<th nowrap='true'>录入人</th>
      	<th nowrap='true'>录入时间</th>
      	<th nowrap='true'>工作量</th> 
      </tr>
      
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	 
      	<xsl:variable name="rowPos" select="position()"/>
        <tr>
          <xsl:for-each select="td[position()]">
              <xsl:choose>
                
                <xsl:when test="position() =1">
				            <td>
			  	            <xsl:value-of select="."/>
				            </td>
                </xsl:when>
                <xsl:when test="position() = 5">
				            <td>
			  	            <xsl:value-of select="format-number(.,'#,##0.00')"/>
				            </td>
                </xsl:when>
                                
                <xsl:otherwise>
			            <td align="right">
	                 <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

