<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
    <thead>
    	<tr noWrap='true' class='mainHead'>
      	<td nowrap='true' colspan="9" style="fontsize:maintitle"></td>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<td nowrap='true' colspan="9" style="fontsize:subtitle"></td>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      </tr>
  		<tr noWrap='true' class='mainHead'>
        <td nowrap='true'>统计年月</td>
      	<td nowrap='true'>医疗项目</td>
      	<td nowrap='true'>开单科室</td>
      	<td nowrap='true'>执行科室</td>
      	<td nowrap='true'>金额</td>
      	<td nowrap='true'>录入人</td>
      	<td nowrap='true'>录入时间</td>
      	<td nowrap='true'>工作量</td> 
      	<td nowrap='true'>成本</td> 
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">            
            <xsl:choose>
		           <xsl:when test="position()=1">
		           <td><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position()=5">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
               <xsl:when test="position()=9">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
               <xsl:when test="position()=8">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>              
          </xsl:choose>               
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



