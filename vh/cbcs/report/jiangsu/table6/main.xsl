<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>���</th>
      	<th nowrap='true'>����Ҫ��</th>
      	<xsl:for-each select="/root/tbody/tr[td[1]='1']/td[position() &gt; 2]">
	      	<th nowrap='true'><xsl:value-of select="."/></th>
      	</xsl:for-each>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[td[1] !='1']">
        <tr>
        	<td align='right'>
            <xsl:value-of select="position()"/>
          </td>
          <xsl:for-each select="td[position() &gt; 1]">
              <xsl:choose>
                <xsl:when test="position()=1">
				            <td>
			  	            <xsl:value-of select="."/>
				            </td>
                </xsl:when>
                                
                <xsl:otherwise>
			            <td align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

