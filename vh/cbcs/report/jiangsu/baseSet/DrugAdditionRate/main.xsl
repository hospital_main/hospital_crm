<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>类别编码</th>	
				<th nowrap='true'>类别名称</th>			  
			  <th nowrap='true'>药品加成率</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:attribute name="_change">1</xsl:attribute>
				<xsl:attribute name="_year_month"><xsl:value-of select="td[4]"/></xsl:attribute>
				<xsl:attribute name="_charge_kind_code"><xsl:value-of select="td[1]"/></xsl:attribute>
				<xsl:attribute name="_add_rate"><xsl:value-of select="td[3]"/></xsl:attribute>
				<xsl:for-each select="td[position()&lt;4]">   
						<td>
			  			<xsl:value-of select="."/> 
						</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

