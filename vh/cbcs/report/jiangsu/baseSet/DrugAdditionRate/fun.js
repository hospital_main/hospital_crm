function submitData(obj){
	var res=checkInputs();
	if(res==null||res=="")
		return;
	window.xmlhttp.post(obj.name, res,"")
	var str = window.xmlhttp._object.responseText
	if (!window.doMsg(str,"")) {
	  return false;
	}
}
function checkInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	var res="";
	var tempRate = "",tempYearMonth='',tempKindCode='';
	for(var i=1;i<trs.length;i++){
		  tempYearMonth = trs[i].getAttribute("_year_month");
		  tempKindCode = trs[i].getAttribute("_charge_kind_code");
		  tempRate = trs[i].getAttribute("_add_rate");
      res+="<record>";
      res+="<year_month>"+tempYearMonth+"</year_month>"; 
      res+="<charge_kind_code>"+tempKindCode+"</charge_kind_code>";
      res+="<add_rate>"+tempRate+"</add_rate>";  
  		res+="</record>";
	}
	return res;
}
function initInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	
	for(var i=1;i<trs.length;i++)
		initTrInputs(trs[i]);
}
//原为探测是否有值改动，现改为探测是否是数字
function textChange(obj){
  if(isNaN(obj.value)){
  	alert('药品加成率必须是数字！');
  	obj.value='';
  	obj.focus();
  	return false;
  }
  obj.parentNode.parentNode._add_rate = obj.value;
}
function initTrInputs(tr){
	var pv=tr.getAttribute("_add_rate");
	var inp="";
	var id;
  id=getJsGuid();
  var temp = tr.cells[2].innerText;
  //
	tr.cells[2].innerHTML ="<input type='text' id='"+id+"' name='"+id+"' value='"+temp+"' maxlength='4' width='100' onChange='textChange(this);'/>";
}
