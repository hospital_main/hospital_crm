<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td) - 2"/>
  	<root>
    	<thead>
    		<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNums"/>    
	        </xsl:call-template>
	  		</tr>
    		<tr noWrap='true'>
	        <td style='fontsize:subtitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNums"/>    
	        </xsl:call-template>
	  		</tr>
				<tr noWrap='true' class='mainHead'>
	      	<td nowrap='true'>��</td>
	      	<td nowrap='true'>��</td>
	      	<td nowrap='true'>ժҪ</td>
	      	<xsl:for-each select="/root/tbody/tr[td[1]='1']/td[position() &gt; 4]">
		      	<td nowrap='true'><xsl:value-of select="."/></td>
	      	</xsl:for-each>
				</tr>
			</thead>
    	<tbody>
      <xsl:for-each select="/root/tbody/tr[td[1] !='1']">
        <tr>
          <xsl:for-each select="td[position() &gt; 1]">
              <xsl:choose>
                <xsl:when test="position()=1">
                	<xsl:if test="../td[1] !='2'">
				            <td colspan="3">
			  	            <xsl:value-of select="../td[4]"/>
				            </td>
	               	</xsl:if>
	               	<xsl:if test="../td[1] ='2'">
				            <td>
			  	            <xsl:value-of select="."/>
				            </td>
	  	           </xsl:if>
                </xsl:when>
                
                <xsl:when test="position()=2 or position()=3">
                	<xsl:if test="../td[1] !='2'">
                		<td style='display:none' />
	               	</xsl:if>
	               	<xsl:if test="../td[1] ='2'">
				            <td>
			  	            <xsl:value-of select="."/>
				            </td>
		  	           </xsl:if>
                </xsl:when>
                <xsl:otherwise>
			            <td align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    	</tbody>
 		<!--tfoot>
 			<tr noWrap='true'>
        <td align="left" style='colspan:colcount-1'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNums"/>    
        </xsl:call-template>
  		</tr>
 		</tfoot-->
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>