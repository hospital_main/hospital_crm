<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>��</th>
      	<th nowrap='true'>��</th>
      	<th nowrap='true'>ժҪ</th>
      	<xsl:for-each select="/root/t2head/tr[td[1]='1']/td[position() &gt; 4]">
	      	<th nowrap='true'><xsl:value-of select="."/></th>
      	</xsl:for-each>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td[position() &gt; 1]">
              <xsl:choose>
                <xsl:when test="position()=1">
                	<xsl:if test="../td[1] !='2'">
				            <td colspan="3">
			  	            <xsl:value-of select="../td[4]"/>
				            </td>
	               	</xsl:if>
	               	<xsl:if test="../td[1] ='2'">
				            <td>
			  	            <xsl:value-of select="."/>
				            </td>
	  	           </xsl:if>
                </xsl:when>
                
                <xsl:when test="position()=2 or position()=3">
                	<xsl:if test="../td[1] !='2'">
                		<td style='display:none' />
	               	</xsl:if>
	               	<xsl:if test="../td[1] ='2'">
				            <td>
			  	            <xsl:value-of select="."/>
				            </td>
		  	           </xsl:if>
                </xsl:when>
                
                <xsl:otherwise>
			            <td align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>