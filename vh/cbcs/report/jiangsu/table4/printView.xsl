<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td) - 2"/>
  	<root>
    	<thead>
    		<tr noWrap='true'>
	       
	        <td style='fontsize:subtitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNums"/>    
	        </xsl:call-template>
	  		</tr>
    		<tr noWrap='true'>	        
	        <td style='fontsize:subtitle;colspan:colcount;align:left'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNums"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap='true'>	        
	        <td style='fontsize:subtitle;colspan:colcount;align:left'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNums"/>    
	        </xsl:call-template>
	  		</tr>
      <tr noWrap='true' class='mainHead'>
      	 
      	<td nowrap='true' rowspan="3">序号</td>
      	<td nowrap='true' rowspan="3">费用要素</td>
      	<td nowrap='true' rowspan="2">直接医疗费用合计</td>
      	<td nowrap='true' rowspan="2">医疗技术费用合计</td>
      	<td nowrap='true' rowspan="2">药品供应费用合计</td>
      	<td nowrap='true' rowspan="2">医疗辅助费用合计</td>
      	<td nowrap='true' rowspan="2">管理部门费用合计</td>
      	<td nowrap='true' rowspan="2">不计入成本费用合计</td>
      	<td nowrap='true' colspan="3">费用合计</td>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td nowrap='true' rowspan="2">比重(%)</td>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td nowrap='true'>本期</td>
      	<td nowrap='true'>预算</td>
      	<td nowrap='true'>上期</td>
      	<td style="display:none"/>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td nowrap='true' >1</td>
      	<td nowrap='true' >2</td>
      	<td nowrap='true' >3</td>
      	<td nowrap='true' >4</td>
      	<td nowrap='true' >5</td>
      	<td nowrap='true' >6</td>
      	<td nowrap='true' >7</td>
      	<td nowrap='true' >8</td>
      	<td nowrap='true' >9</td>
      	<td nowrap='true' >10</td>
      </tr>
			</thead>
    	<tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="kindCode" select="td[2]"/>
      	<xsl:variable name="rowPos" select="position()"/>
        <tr>
        	<td align='right'>
            <xsl:value-of select="position()"/>
          </td>
          <xsl:for-each select="td[position() &gt; 2]">
              <xsl:choose>
                <xsl:when test="position()=1">
                	<xsl:if test=". != '合计'">
				            <td>
			  	            <xsl:value-of select="."/>
				            </td>
				           </xsl:if>
				           
                	<xsl:if test=". = '合计'">
                		<xsl:if test="../../tr[$rowPos -1]/td[2] != '合计'">
					            <td rowspan="3">
				  	            <xsl:value-of select="."/>
					            </td>
				            </xsl:if>
                		<xsl:if test="../../tr[$rowPos -1]/td[2] = '合计'">
					            <td style="display:none">
					            </td>
				            </xsl:if>
				           </xsl:if>
                </xsl:when>
                
                <xsl:when test="position() = 2">
				            <td align="right">				            	
			  	            <xsl:value-of select="."/>
			  	            <xsl:value-of select="format-number(.,'#,##0.00')"/>
				            </td>
                </xsl:when>
                                
                <xsl:otherwise>
			            <td align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    	</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>