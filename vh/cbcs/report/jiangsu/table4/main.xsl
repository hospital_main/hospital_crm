<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true' rowspan="3">序号</th>
      	<th nowrap='true' rowspan="3">费用要素</th>
      	<th nowrap='true' rowspan="2">直接医疗费用合计</th>
      	<th nowrap='true' rowspan="2">医疗技术费用合计</th>
      	<th nowrap='true' rowspan="2">药品供应费用合计</th>
      	<th nowrap='true' rowspan="2">医疗辅助费用合计</th>
      	<th nowrap='true' rowspan="2">管理部门费用合计</th>
      	<th nowrap='true' rowspan="2">不计入成本费用合计</th>
      	<th nowrap='true' colspan="3">费用合计</th>
      	<th nowrap='true' rowspan="2">比重</th>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>本期</th>
      	<th nowrap='true'>预算</th>
      	<th nowrap='true'>上期</th>
      </tr>
       <tr noWrap='true' class='mainHead'>
      	
      	<th nowrap='true' >1</th>
      	<th nowrap='true' >2</th>
      	<th nowrap='true' >3</th>
      	<th nowrap='true' >4</th>
      	<th nowrap='true' >5</th>
      	<th nowrap='true' >6</th>
      	<th nowrap='true'>7</th>
      	<th nowrap='true'>8</th>
      	<th nowrap='true'>9</th>
      	<th nowrap='true' >10</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="kindCode" select="td[2]"/>
      	<xsl:variable name="rowPos" select="position()"/>
        <tr>
        	<td align='right'>
            <xsl:value-of select="position()"/>
          </td>
          <xsl:for-each select="td[position() &gt; 2]">
              <xsl:choose>
                <xsl:when test="position()=1">
                	<xsl:if test=". != '合计'">
				            <td>
			  	            <xsl:value-of select="."/>
				            </td>
				           </xsl:if>
				           
                	<xsl:if test=". = '合计'">
                		<xsl:if test="../../tr[$rowPos -1]/td[2] != '合计'">
					            <td rowspan="3">
				  	            <xsl:value-of select="."/>
					            </td>
				            </xsl:if>
				           </xsl:if>
                </xsl:when>
                
                <xsl:when test="position() = 2">
				            <td>
			  	           <xsl:value-of select="format-number(.,'#,##0.00')"/>
				            </td>
                </xsl:when>
                                
                <xsl:otherwise>
			            <td align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

