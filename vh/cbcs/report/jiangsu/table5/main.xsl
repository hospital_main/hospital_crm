<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td) - 3"/>
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true' rowspan="2">序号</th>
      	<th nowrap='true' rowspan="2" colspan="2">费用要素</th>
      	<xsl:for-each select="/root/tbody/tr[td[1]='1']/td[position() &gt; 2]">
	      	<th nowrap='true'><xsl:value-of select="."/></th>
      	</xsl:for-each>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<xsl:for-each select="/root/tbody/tr[td[1]='1']/td[position() &gt; 2] ">
	      	<th nowrap='true'><xsl:value-of select="position()"/></th>
      	</xsl:for-each>
      	
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[td[1] !='1']">
        <tr>
        	<td align='right'>
            <xsl:value-of select="position()"/>
          </td>
          <xsl:for-each select="td[position() &gt; 1]">
              <xsl:choose>
                <xsl:when test="position()=1">
                	<xsl:if test="../td[1] != 5 and ../td[1] != 6 and ../td[1] != 7">
				            <td colspan="2">
			  	            <xsl:value-of select="."/>
				            </td>
				           </xsl:if>
				           <xsl:if test="../td[1]=5">
				           	<td rowspan="3">合计</td>
				           	<td><xsl:value-of select="."/></td>
				           </xsl:if>
				           <xsl:if test="../td[1]=6 or ../td[1] = 7 ">
				           	<td style="display:none">合计</td>
				           	<td><xsl:value-of select="."/></td>
				           </xsl:if>
                </xsl:when>
                                
                <xsl:otherwise>
			            <td align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

