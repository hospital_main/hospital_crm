<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/report/SDReport_Create.jsp,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:03 $
 $Modtime: 03-09-02 11:44 $
 $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
<!--
	function designReport(){
		window.open("urd_design.htm",'child', 'width=800,status=1,height=400, top=100, left=100, scrollbars = 1, resizable = 0,status=0');
	}
//-->
  //添加
    function create() {
    	if(trim(template.rep_name.value)==""){
    		alert("报表名称不能为空");
    		return;
    	}    
    	if(trim(template.report_xml.value)==""){
    		alert("请进行报表定义");
    		return;
    	}
    	show_wait();
        template.subFunction.value='create';
        template.submit();
        return true;
    }
  // 返回
  function back( element )
  {
    for(var i=0;i<template.elements.length;i++)
          template.elements[i].value="";
    template.subFunction.value='define';
    element.submit();
  }
 
  </Script>
<html:html clazz="main">
<form name="template" method="post" action="selfDreport.jspviewhigh">  
  <!-- 信息提示栏 -->
<html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>自定义报表结构添加页面</html:title>

  <!-- 简单信息 -->
  <table  width="100%" cellspacing="2" border="0" >
    <tr>
      <td class="signText" nowrap="nowrap">报表名称：</td>
      <td width="75%" class="normalText" nowrap="nowrap"><input type=text name="rep_name" class="textInputC"/></td>
    </tr>
    <tr>
    	<td><img src="images/Dreport.png" class="mouse" onclick="designReport();" /></p></p></p></p></p></p></td>
    </tr>
     <tr>
      <td colspan="2"> <button class="pageBtn" onclick="return create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/create.gif" class="mouse" onclick="return create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" />
      <img src="images/return.gif" class="mouse" onclick="return back(template);" /> --></td>
    </tr>
    <tr height="500">
    <td>
    </td>
    </tr>

  </table>
  <input type=hidden name="subFunction" value="create"/>
  <TEXTAREA STYLE="display:none" name="report_xml"></TEXTAREA>
</form>

</html:html>   