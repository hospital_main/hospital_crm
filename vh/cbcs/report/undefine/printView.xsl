<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<table>
		<thead>
			<xsl:variable name="colnum" select="count(/root/t2head/tr[1]/td)"/>
			<xsl:for-each select="/root/t2head/tr[1]">
				<tr noWrap="true" class="mainHead">       
					<xsl:for-each select="td">
						<xsl:choose>
	            	    <xsl:when test="position()=1">
							<td style="fontsize:maintitle" colspan="{$colnum}"></td>
						</xsl:when>
						 <xsl:otherwise>
						 	<td style="display:none"></td>
               			 </xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>			
  				</tr>
			
				<tr  noWrap="true" class="mainHead">
					<xsl:for-each select="td">
						<td align="center" >
							<xsl:value-of select="."/>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</thead>
		<tbody>
			
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
					
						<td align="left">
							<xsl:value-of select="."/>
						</td>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		
		</tbody>
	</table>	
	</xsl:template>
</xsl:stylesheet>
