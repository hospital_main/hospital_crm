<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th width='25'><input type='checkbox'/></th>
        <th>单项名称</th>
		<th>单项金额</th>
		<th>类型</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <xsl:if test="td[1]!=''">  
            <input type='checkbox'/>
			</xsl:if>
          </td>
          <xsl:for-each select="td[position()!=2 and position()!=3]">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td style="display:none">
                    <xsl:value-of select="."/>
                </td>
              </xsl:when>       
              <xsl:when test="position()=2">
                <td>
                    <xsl:value-of select="."/>
                </td>
              </xsl:when>       
              <xsl:when test="position()=3">
                <td class="numberText">
                    <xsl:value-of select="."/>
                </td>
              </xsl:when>       
              <xsl:when test="position()=4">
                <td style="display:none">
                    <xsl:value-of select="."/>
                </td>
              </xsl:when>       
              
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


