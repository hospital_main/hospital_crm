<!--
   $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/report/fh/data/datapp.jsp,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
   $Author: zhoulidong $
   $Date: 2012/03/12 01:59:03 $
   $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,com.viewhigh.cbcs.base.mvc.view.MonthComponent,com.viewhigh.cbcs.base.mvc.view.TableMarge" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript">
		function back(){
			template.subFunction.value='show';
			template.submit();
		}
		function apportion(num) {
			if (template.year_month.value=='') {
				alert('请选择核算月');
				return false;
			}
			if(num==2){
				template.subFunction.value='count';
				show_wait();
				template.submit();
			}
			return true;
		}
</Script>
<html:html clazz="main">
    <form name="template" method="post" action="reportDataPP.jspviewhigh" >
      <!-- 返回信息栏 -->
	  <html:message/>

      <!-- 标题栏 -->
	  <html:title clazz='module'>数据准备</html:title>
    <br>

      <!-- 简单信息 -->
	  <html:table clazz="simple">
        <tr>
          <td class="signText" nowrap width="10%">年月：</td>
          <td class="normalText"><%=new MonthComponent("year_month", request.getParameter("year_month"))%></td>
          <td><button class="pageBtn" onclick="return apportion(2)">计算</button></td>
        </tr>
	  </html:table>
		<input type="hidden" name="subFunction" >
  	</form>

</html:html>
