<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
  	<colgroup>
      <xsl:for-each select="/root/tbody/tr[td[2]='0' and td[3]!=0]">
        <col>
          <xsl:attribute name="style" ><xsl:value-of select="td[7]"/></xsl:attribute>
        </col>
    	</xsl:for-each>
    </colgroup>
  	<thead>
      <xsl:for-each select="/root/tbody/tr[td[2]!=0 and td[1]=1]">          
        <xsl:if test="td[3]=0">
          <xsl:text disable-output-escaping="yes">
            &lt;tr noWrap='true' class='mainHead' 
          </xsl:text>
          style="<xsl:value-of select="td[7]"/>"
          <xsl:text disable-output-escaping="yes">
             &gt;
          </xsl:text>
        </xsl:if>
        <xsl:if test="td[3]!=0">
          <th noWrap='true'>
            <xsl:attribute name="rowspan" ><xsl:value-of select="td[4]"/></xsl:attribute>
            <xsl:attribute name="colspan" ><xsl:value-of select="td[5]"/></xsl:attribute>
            <xsl:attribute name="style" ><xsl:value-of select="td[7]"/></xsl:attribute>
            <xsl:variable name="textFormatA" select="substring-before(substring-after(substring-after(td[7],'textFormatF'),':'),';')"/>
            <xsl:if test="string-length($textFormatA) &gt; 0"><xsl:value-of select="format-number(td[6],$textFormatA)"/></xsl:if>
            <xsl:if test="string-length($textFormatA) &lt; 1"><xsl:value-of select="td[6]"/></xsl:if>
          </th>
        </xsl:if>
        <xsl:if test="td[3]=td[8]">
          <xsl:text disable-output-escaping="yes">
            &lt;/tr&gt;
          </xsl:text>
        </xsl:if>
    	</xsl:for-each>
  	</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[td[2]!=0 and td[1]=0]">
        <xsl:if test="td[3]=0">
          <xsl:text disable-output-escaping="yes">
            &lt;tr noWrap='true' 
          </xsl:text>
          style="<xsl:value-of select="td[7]"/>"
          <xsl:text disable-output-escaping="yes">
             &gt;
          </xsl:text>
        </xsl:if>
        <xsl:if test="td[3]!=0">
          <td noWrap='true'>
            <xsl:attribute name="rowspan" ><xsl:value-of select="td[4]"/></xsl:attribute>
            <xsl:attribute name="colspan" ><xsl:value-of select="td[5]"/></xsl:attribute>
            <xsl:attribute name="style" ><xsl:value-of select="td[7]"/></xsl:attribute>
            <xsl:variable name="textFormatA" select="substring-before(substring-after(substring-after(td[7],'textFormatF'),':'),';')"/>
            <xsl:if test="string-length($textFormatA) &gt; 0"><xsl:value-of select="format-number(td[6],$textFormatA)"/></xsl:if>
            <xsl:if test="string-length($textFormatA) &lt; 1"><xsl:value-of select="td[6]"/></xsl:if>
          </td>
        </xsl:if>
    	  <xsl:if test="td[3]=td[8]">
          <xsl:text disable-output-escaping="yes">
            &lt;/tr&gt;
          </xsl:text>
        </xsl:if>
      </xsl:for-each>
    </tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>



