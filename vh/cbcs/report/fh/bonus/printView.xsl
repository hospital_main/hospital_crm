<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <colgroup>
		      <col style = 'width:80mm'/>
		      <col style = 'width:105mm'/>
		      <col style = 'width:105mm'/>
		      <col style = 'width:105mm'/>
		      <col style = 'width:90mm'/>
		    </colgroup>
	      <thead>
			 	  <tr noWrap='true' class='mainHead'>
		      <th style="border: 1px solid #000000;">年月</th>
		      <th style="border: 1px solid #000000;">科室</th>
					<th style="border: 1px solid #000000;">奖金项目</th>
					<th style="border: 1px solid #000000;">项目说明</th>
					<th style="border: 1px solid #000000;">金额</th>
         </tr>
			  </thead>	      
	      <tbody>
		      <xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<xsl:for-each select="td">
	              <xsl:choose>
	                <xsl:when test="position()=5">
	                	<td style="border: 1px solid #000000;" class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	                </xsl:when>
	                <xsl:otherwise>
	                	<td style="border: 1px solid #000000;"><xsl:value-of select="."/></td>
	                </xsl:otherwise>
	              </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
	      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>