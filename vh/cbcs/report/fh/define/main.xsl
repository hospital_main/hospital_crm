<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
      	<th nowrap='true'>报表编码</th>
      	<th nowrap='true'>报表名称</th>
      	<th nowrap='true'>报表类型</th>
      	<th nowrap='true'>定义</th>
      	<th nowrap='true'>复制</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
                <xsl:when test="position()=1">
                  <a tabindex='-1'>
                    <xsl:attribute name="href" >
      	            javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:350px;dialogHeight:170px', result)
    	              </xsl:attribute>
  	            <xsl:value-of select="."/></a>
                </xsl:when>
                <xsl:when test="position()=3">
                     <xsl:choose>
                        <xsl:when test=".='a'">全院报表</xsl:when>
                        <xsl:when test=".='d'">科室报表</xsl:when>
                     </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
          <td align="center"><a>
            <xsl:attribute name="href" >
    	        javascript:openDialog('set.html?load=&lt;code&gt;<xsl:value-of select="td[position()=1]"/>&lt;/code&gt;&lt;name&gt;<xsl:value-of select="td[position()=2]"/>&lt;/name&gt;&lt;type&gt;<xsl:value-of select="td[position()=3]"/>&lt;/type&gt;','dialogWidth:900px;dialogHeight:650px')
  	        </xsl:attribute>设置</a>
  	      </td>
  	      <td align="center"><a>
            <xsl:attribute name="href" >
    	        javascript:openDialog('copy.html?load=&lt;code&gt;<xsl:value-of select="td[position()=1]"/>&lt;/code&gt;&lt;name&gt;<xsl:value-of select="td[position()=2]"/>&lt;/name&gt;&lt;type&gt;<xsl:value-of select="td[position()=3]"/>&lt;/type&gt;','dialogWidth:350px;dialogHeight:170px')
  	        </xsl:attribute>复制</a>
  	      </td>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

