<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <select ondblclick='inputSubj()' onkeyup='if (event.keyCode==13 || event.keyCode==32) inputSubj()' id='acct_subj' style='background:#F6F6F6;width:350px' size='11'>
      <xsl:for-each select="/root/para">
        <option value='{@code}'><xsl:value-of select='@value'/></option>
    	</xsl:for-each>
    </select>
	</xsl:template>
</xsl:stylesheet>



