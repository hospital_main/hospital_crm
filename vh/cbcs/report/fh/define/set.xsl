<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <colgroup>
      <xsl:for-each select="/root/tbody/tr[td[2]='0']">
        <col>
          <xsl:attribute name="style" ><xsl:value-of select="td[7]"/></xsl:attribute>
        </col>
    	</xsl:for-each>
    </colgroup>
    <thead>
      <xsl:for-each select="/root/tbody/tr[td[1]=1]">  
        <xsl:if test="td[2]=0">
          <xsl:if test="td[3]=0">
            <xsl:text disable-output-escaping="yes">
            &lt;tr noWrap='true'&gt;
            </xsl:text>
            <th noWrap='true' class='table_rule' style='cursor: default;text-align:center;'></th>
          </xsl:if>
          <xsl:if test="td[3]!=0">
            <th noWrap='true' class='table_rule'>
              <xsl:attribute name="type"><xsl:value-of select="td[9]"/></xsl:attribute> 
              <xsl:attribute name="rowSpan"><xsl:value-of select="td[4]"/></xsl:attribute> 
              <xsl:attribute name="colSpan"><xsl:value-of select="td[5]"/></xsl:attribute> 
              <xsl:attribute name="style"><xsl:value-of select="td[7]"/></xsl:attribute> 
              <xsl:attribute name="value"><xsl:value-of select="td[6]"/></xsl:attribute>
              <xsl:if test="not(contains(td[6],':'))">
                <xsl:value-of select="position()-1"/>
              </xsl:if>
              <xsl:if test="contains(td[6],':')">
                <xsl:value-of select="position()-1"/>��<xsl:value-of select="substring-after(td[6],'|+|')"/>
              </xsl:if>
            </th>
          </xsl:if>
          <xsl:if test="td[3]=td[8]">
            <xsl:text disable-output-escaping="yes">
            &lt;/tr&gt;
            </xsl:text>
          </xsl:if>
        </xsl:if>
        
        <xsl:if test="td[2]!=0">
          <xsl:if test="td[3]=0">
            <xsl:text disable-output-escaping="yes">
            &lt;tr noWrap='true' class='mainHead' style="cursor: hand;"&gt;
            </xsl:text>
            <th noWrap='true' class='table_rule' >
              <xsl:attribute name="type"><xsl:value-of select="td[9]"/></xsl:attribute> 
              <xsl:attribute name="style"><xsl:value-of select="td[7]"/>;text-align:left;</xsl:attribute> 
              <xsl:attribute name="value"><xsl:value-of select="td[6]"/></xsl:attribute>
              <xsl:if test="not(contains(td[6],':'))">
                <xsl:value-of select="td[2]"/>
              </xsl:if>
              <xsl:if test="contains(td[6],':')">
                <xsl:value-of select="td[2]"/>��<xsl:value-of select="substring-after(td[6],'|+|')"/>
              </xsl:if>
            </th>
          </xsl:if>
          <xsl:if test="td[3]!=0">
            <th noWrap='true' >
              <xsl:attribute name="type"><xsl:value-of select="td[9]"/></xsl:attribute> 
              <xsl:attribute name="rowSpan"><xsl:value-of select="td[4]"/></xsl:attribute> 
              <xsl:attribute name="colSpan"><xsl:value-of select="td[5]"/></xsl:attribute> 
              <xsl:attribute name="style"><xsl:value-of select="td[7]"/></xsl:attribute> 
              <xsl:attribute name="value"><xsl:value-of select="td[6]"/></xsl:attribute>
              <xsl:choose>
                <xsl:when test='td[9]=0'>
                  <xsl:value-of select='td[6]'/>
                </xsl:when>
                <xsl:when test='td[9] >= 1'>
                  ����
                </xsl:when>
              </xsl:choose>
            </th>
          </xsl:if>
          <xsl:if test="td[3]=td[8]">
            <xsl:text disable-output-escaping="yes">
            &lt;/tr&gt;
            </xsl:text>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
    </thead>
    <tbody>
    <xsl:for-each select="/root/tbody/tr[td[1]=0]">
      <xsl:if test="td[3]=0">
        <xsl:text disable-output-escaping="yes">
          &lt;tr noWrap='true'
        </xsl:text>
        style="<xsl:value-of select="td[7]"/>";cursor: hand;
        <xsl:text disable-output-escaping="yes">
          &gt;
        </xsl:text>
        <td noWrap='true' class='table_rule' >
          <xsl:attribute name="style" >text-align:left;<xsl:value-of select="td[7]"/></xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="td[6]"/></xsl:attribute>
          <xsl:if test="not(contains(td[6],':'))">
          <xsl:value-of select="td[2]"/>
          </xsl:if>
          <xsl:if test="contains(td[6],':')">
          	<xsl:value-of select="td[2]"/>��<xsl:value-of select="substring-after(td[6],'|+|')"/>
          </xsl:if>
        </td>
      </xsl:if>
      <xsl:if test="td[3]!=0">
        <td noWrap='true' >
          <xsl:attribute name="type"><xsl:value-of select="td[9]"/></xsl:attribute> 
          <xsl:attribute name="rowSpan"><xsl:value-of select="td[4]"/></xsl:attribute> 
          <xsl:attribute name="colSpan"><xsl:value-of select="td[5]"/></xsl:attribute> 
          <xsl:attribute name="style"><xsl:value-of select="td[7]"/></xsl:attribute> 
          <xsl:attribute name="value"><xsl:value-of select="td[6]"/></xsl:attribute>
          <xsl:choose>
            <xsl:when test='td[9]=0'>
              <xsl:value-of select='td[6]'/>
            </xsl:when>
            <xsl:when test='td[9] >= 1'>
            ����
            </xsl:when>
          </xsl:choose>
        </td>
        </xsl:if>
        <xsl:if test="td[3]=td[8]">
          <xsl:text disable-output-escaping="yes">
            &lt;/tr&gt;
          </xsl:text>
        </xsl:if>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>



