<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <select ondblclick='inputCash()' onkeyup='if (event.keyCode==13 || event.keyCode==32) inputCash()' id='acct_cash' style='background:#F6F6F6;width:320px' size='11'>
      <xsl:for-each select="/root/para">
        <option value='{@code}'><xsl:value-of select='@code'/>:<xsl:value-of select='@value'/></option>
    	</xsl:for-each>
    </select>
	</xsl:template>
</xsl:stylesheet>



