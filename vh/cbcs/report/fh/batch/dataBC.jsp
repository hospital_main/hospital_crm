<!--
   $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/cbcs/report/fh/batch/dataBC.jsp,v 1.1 2012/03/12 01:59:03 zhoulidong Exp $
   $Author: zhoulidong $
   $Date: 2012/03/12 01:59:03 $
   $Revision: 1.1 $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.sql.BaseRO,com.viewhigh.cbcs.base.mvc.view.MonthComponent,com.viewhigh.cbcs.base.mvc.view.TableMarge,
								com.viewhigh.cbcs.base.mvc.view.BiMonthComponent" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>
<%
    String[][] report_list=(String[][])request.getAttribute("report_list");
%>
 
<!-- liwenqing 2006.06.14 -->

<script language="javascript" type="text/javascript">
var xmlHttp = false;
//http://localhost:8080/view.html
/*@cc_on @*/
/*@if (@_jscript_version >= 5)
try{
	xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
} catch(e){
	try{
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	}catch (e2){
		xmlHttp=false;
	}
}
@end @*/

if (!xmlHttp && typeof XMLHttpRequest != 'undefined')
{
	xmlHttp = new XMLHttpRequest();
}
function callServer(){
	var url="/ProcessViewer";
//	var url="/a.txt"
	xmlHttp.open("GET",url,true);
	xmlHttp.onreadystatechange=updatePage;
	xmlHttp.send(null);
}
function callServer_stop(){
	var url="/ProcessViewer?command=stop";
	xmlHttp.open("GET",url,true);
	xmlHttp.onreadystatechange=updatePage;
	xmlHttp.send(null);
}
function updatePage(){
	if (xmlHttp.readyState == 4)
	{
		if (xmlHttp.status == 200) {
			var response = xmlHttp.responseText;
//			document.getElementById("processout").value=response;
			showProcess(xmlHttp.responseXml);
		}
	}
}
	
function showProcess(pmsgxml){
//	alert(pmsgxml.xml);
	var items=pmsgxml.selectNodes("/processmessage");
//	document.getElementById("processout").value +=getNode(pmsgxml,"/processmessage/begin_time");
//	document.getElementById("processout").value +=getNode(pmsgxml,"/processmessage/count");
//	document.getElementById("processout").value +=getNode(pmsgxml,"/processmessage/current");
//	document.getElementById("gcmc").innerHTML =decodeURI(getNode(pmsgxml,"/processmessage/currentname"));
//	document.getElementById("processout").value +=getNode(pmsgxml,"/processmessage/isrunning");
	document.getElementById("staticname").innerHTML =decodeURI(getNode(pmsgxml,"/processmessage/name"));
	var cmd=getNode(pmsgxml,"/processmessage/cmd");
	var isrunning=getNode(pmsgxml,"/processmessage/isrunning");
	if (cmd=="stop"&&isrunning=="true")
	{
		document.getElementById("isrunning").innerHTML = "正在中止...";
		b1.disabled=false;
	}
	if (cmd=="stop"&&isrunning=="false")
	{
		document.getElementById("isrunning").innerHTML = "已经中止!";
		b1.disabled=false;
	}
	if (cmd=="start"&&isrunning=="false")
	{
		document.getElementById("isrunning").innerHTML = "已经完成!";
		b1.disabled=false;
	}
	if (cmd=="start"&&isrunning=="true")
	{
		document.getElementById("isrunning").innerHTML = "正在处理...";
		b1.disabled=true;
	}
	document.getElementById("begin_time").innerHTML =" 开始于 "+getNode(pmsgxml,"/processmessage/begin_time");
//	document.getElementById("isrunning").innerHTML =" 状态 "+isrunning;
	var current=parseInt(getNode(pmsgxml,"/processmessage/current"));
	var count=parseInt(getNode(pmsgxml,"/processmessage/count"));
	var percent="0%";
//	alert(count);
	if (count>0) percent=Math.round(current*100/count).toString()+"%";
	document.getElementById("percent").innerHTML=percent;
	document.getElementById("loaded").style.width=percent;
	var curname=decodeURI(getNode(pmsgxml,"/processmessage/currentname"));
	if (document.getElementById("currentname").innerHTML != curname)
	{
		if (document.getElementById("currentname").innerHTML !="")
		{
			var c_date=new  Date();
			var c_time=c_date.getHours()+":";
			if (c_date.getMinutes()<=9) c_time+="0" ;
			c_time+=c_date.getMinutes()+":";
			if (c_date.getSeconds()<=9) c_time+="0" ;
			c_time+=c_date.getSeconds();
			document.getElementById("processout").value +=document.getElementById("currentname").innerHTML+" 完成 - "+c_time+"\n";
		}
		document.getElementById("currentname").innerHTML=curname;
	}
}
function getNode(doc, xpath) { 
      var retval = ""; 
      var value = doc.selectSingleNode(xpath); 
      if (value) retval = value.text; 
      return retval; 
} 
</script>


<Script Language="JavaScript">
        function back(){
	  template.subFunction.value='show'
	  template.submit();
          }
	function apportion(num) {
	   if (template.start.value==''||template.end.value=='') {
	    alert('请选择核算月');
	    return false;
	    }
          if(num==2)
            template.subFunction.value='batch'
	  b1.disabled=true;
//	  show_wait();
	  template.submit();
	  return true;
	 }
</Script>
<html:html clazz="main">

<SCRIPT LANGUAGE="JavaScript">
setInterval("callServer()",1500);
//setTimeout("callServer()",2000);
</SCRIPT>

    <form name="template" method="post" action="reportDataBatchC.jspviewhigh" >
      <!-- 返回信息栏 -->
	  <html:message/>

      <!-- 标题栏 -->
	  <html:title clazz='module'>批量计算</html:title>
    <br>

      <!-- 简单信息 -->
	  <html:table clazz="simple">
        <tr>
          <td nowrap class="signText">
          	<table>
          		<tr>
          	<td class="signText">年月：</td><td><%=new BiMonthComponent("start", request.getParameter("start"),"end",request.getParameter("end"))%></td>
          	</tr>
          	</table>
          </td>
        	<td align="left">报表名称:</td>
          <td class="normalText"><%=new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(report_list,"report_list",request.getParameter("report_list"),false,true)%></td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
	  </html:table>
		<input type="hidden" name="subFunction" >
  	</form>
  	
  	
  	<table width=450 border=0>

	<table width=450 border=0>

	<tr><td align="center" class="signText"><div id="staticname">统计过程名称</div></td><td align="center"><div id="cmd"></div></td></tr>
	<tr><td align="center" class="signText"><div id="begin_time">开始时间</div></td><td align="center" class="signText"><div id="isrunning">运行状态</div></td></tr>

	<tr height=10><td height=10 align="left">
	<DIV id=loadbar style="display:block;border:1px solid orange;BACKGROUND-COLOR: #EBE8D7;Z-INDEX: 100; LEFT: 40%; WIDTH: 440px; TOP: 12%; HEIGHT: 15px;"><span id=loaded style="align;right;display:block;border:0px solid orange;BACKGROUND-COLOR: #C7C2AF;HEIGHT: 15px;width:0px"></span></DIV>
	</td></tr>
	</table>

	<table width=450 border=0>

	<tr><td width=100 class="normalText"><div id="percent">100%</div></td><td width=100 class="signText">正在计算:</td><td class="normalText"><div id="currentname"></div></td></tr>
	</table>

	<table width=450 border=0>
	<tr><td align="center">
	<TEXTAREA NAME="processout" ROWS="15" cols="60" readonly="yes"></TEXTAREA>
	</td></tr>

	<tr><td align="center">
	<INPUT TYPE="button" value="计算" disabled id="b1" onClick="return apportion(2);"/>
	<INPUT TYPE="button" value="停止" id="b2" onClick="callServer_stop();"/>
	<INPUT TYPE="button" value="刷新"  id="b3" onClick="callServer();"/>
	</td></tr>
	</table>

</table>

</html:html>
