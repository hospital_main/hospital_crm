//<public:property name="extendCoordinateBaseColor" put="putExtendCoordinateBaseColor"/>
//<public:property name="extendCoordinateLineColor" put="putExtendCoordinateLineColor"/>
function jhtc_statPillarGroupStat(win,jhtc_obj){
var pageWin=win;
var element=jhtc_obj;
var objXMLDoc;
var objContainerDiv = null;//容器
var objInfoSpan = null;
var objContextMenuContainersDiv;

var maxPatientNum = 0;//最大发病数
var groupItemNum = 0;//每一组中柱壮统计图的个数
var pillarWidth = 0;
var spaceValue = 100;//组统计图之间的距离

var booleanSlant = false;//x轴刻度是否倾斜(默认水平)

var vLeft = 0;;
var vTop = 0;;
var vWidth = 260;
var vHeight = 260;

var vReportformWidth = 300;
var vReportformHeight = 300;

var booleanHasBorder = true;//是否有边框
var booleanHasLegend = true;//是否有图例
var booleanHasXText = true;//是否有x轴刻度值
var vBorderColor = "#000000";//边框颜色
var vBorderWeight = 1;//边框像素
var vBackColor = "#ffffff";//柱状图背景色

var vCaption = "";
var vTagID = "";
var vPsOrientation = "plumb";//统计图方向(默认垂直)
var booleanSingleExtrusionColor = false;
var booleanYScaleIsInteger = false;//y轴刻度是否为整数(默认为false)
var vExtrusionColor = "#000000";//柱状图填充色
var vExtrusiondepth = 10;//柱状图深度
var vExtendCoordinateBaseColor = "#cccccc";//扩展坐标系基底色
var vExtendCoordinateBackColor = "#dddddd";//扩展坐标系背景色
var vExtendCoordinateLineColor = "#dddddd";//扩展坐标系坐标线颜色

//预置15种颜色作为柱图的分类填充色
var objPillarColorArray = new Array('#9996FD','#953565','#FAFCCD','#D0FDFF','#650068','#F9847D','#0064D7','#33FFCC','#99CC66','#66FF66','#333366','#FF6699','#FF3399','#FFCC00','#CCFFFF','#000000','#009999','#0066CC','#6699FF','#3300CC','#336600','#CCCC99','#996699','#9933FF','#CCCCCC','#CC3333','#33CCCC','#FF6699','#FFFF00','#66FF00','#000099','#CC9999','#666600','#CCCC33','#FFFFCC','#FF6600','#336666');

//////////////////////////////////////////////////////////////////
function initialize(){//组件初始化操作
	objContainerDiv = window.document.createElement("<div class='phms_pgs_containerDiv'/>");
	objContainerDiv = element.appendChild(objContainerDiv);
	objContainerDiv.style.visibility = "visible";
	objContainerDiv.oncontextmenu = processContextMenuEvent;
	//出错信息
	objInfoSpan = element.document.createElement("<span class='phms_pgs_errorInfoSpan'>");
	objInfoSpan = element.appendChild(objInfoSpan);
	objInfoSpan.style.visibility = "hidden";
	
	///////////////////////////////////////////////////
	objContextMenuContainersDiv = element.document.createElement("<div style='position:absolute;background:#cccccc;border:outset 2px;z-index:1;'>");
	objContextMenuContainersDiv.onblur = function(){this.style.visibility="hidden";};
	var objMenuItem = element.document.createElement("<span style='position:absolute;width:125;height:17;padding-top:3;padding-left:3;top:2;font-size:9pt;'>");
	objMenuItem.innerText = "导出统计图到office";
	objMenuItem.onfocus = transVMLtoOfficeDocument;
	objMenuItem = objContextMenuContainersDiv.appendChild(objMenuItem);
	objMenuItem.onmouseover = function(){this.style.background = "#000080";this.style.color = "#ffffff";};
	objMenuItem.onmouseout = function(){this.style.background = "#cccccc";this.style.color = "#000000";};
	objContextMenuContainersDiv = element.appendChild(objContextMenuContainersDiv);
	objContextMenuContainersDiv.style.visibility = "hidden";
	//objMenuItem.bindClick = transVMLtoOfficeDocument;
	///////////////////////////////
	
	refresh();	
	createPlumbPillar();
}

function transVMLtoOfficeDocument(){
	/////////此处添加发送vml导出请求数据到服务器/////////////////////
	var objMessageSelect = window.parent.parent.__objGlobalCommonInst.createMsgBody();
		objMessageSelect.inUseState = "busy";
        objMessageSelect.action = "导出";
        objMessageSelect.target = "统计图";
    var temp = createVMLForWord();
    //window.prompt("office",temp);
	var vmlObj  = window.parent.parent.__objGlobalCommonInst.inputXML(temp);
	objMessageSelect.addOther(vmlObj.firstChild);
   var objReturnData = window.parent.parent.__objGlobalCommonInst.post(objMessageSelect.outerXML,"","0");		
	var node = objReturnData.selectSingleNode("//r");	
    objMessageSelect.inUseState = "free";
	var openPath = "../file_pool/"+node.getAttribute("url");
	window.open(openPath);	
	/////////////////////////////////////////////////
}


function processContextMenuEvent(){
	event.returnValue = false;
	with(objContextMenuContainersDiv.style){
		left = event.x - 10 - vLeft;
		top = event.y - 10 - vTop;
		width = 130;
		height = 25;
		visibility = "visible";
	}
	objContextMenuContainersDiv.focus();
}

function refresh(){//
	with(element.style){//element位置
		left = vLeft;
		top = vTop;
		width = vWidth;
		height = vHeight;
	}
	with(objContainerDiv.style){
		left = element.style.left;
		top = element.style.top;
		width = element.style.width;
		height = element.style.height;
	}
}

function refreshData(){//刷新数据源
	
	createPlumbPillar();
}


////////////////////////////////////////////////////////////////

function createVMLForWord(){
	var oItemNodesList = null		
	var vmlEnd = "";//最终的vml代码
	var vmlGroupPillar = "";//柱图vml代码
	var vmlLegend = "";//图例vml代码
	var vmlTitle = "";//柱图标题vml代码
	var vmlExtendCoordinate = "";//扩展坐标系
	var xScale = 0;
	var yScale = 0;
	var intNodeListLength = 0;//组个数
	var intTitleWidth = 0;//标题宽度
	var vTopDistance = 100;//顶端距离调整值
	if(!objXMLDoc) return;
	var oItemNodesList = objXMLDoc.selectNodes("//root/item");//项目节点	
	intNodeListLength = oItemNodesList.length;
	
	/*
	if(!oItemNodesList || oItemNodesList.length==0){
		objContainerDiv.style.background = "#cccccc";			
		with(objInfoSpan.style){
			visibility = "visible";	
			width = 120;
			height = 30;
			left = (parseInt(objContainerDiv.style.width) + parseInt(element.offsetLeft))/2 - parseInt(objInfoSpan.style.width)/2;
			top = (parseInt(objContainerDiv.style.height) + parseInt(element.offsetTop))/2 - parseInt(objInfoSpan.style.height)/2;
		}
		objInfoSpan.innerText = "数据加载失败......";
		return;	
	}else{
		objContainerDiv.innerHTML = "";
		objContainerDiv.style.background = "";
		objInfoSpan.innerText = "";
	}
	*/
	
	xScale = Math.ceil(2950/intNodeListLength);
	yScale = Math.ceil(maxPatientNum/10);
	pillarWidth = (2950 - (intNodeListLength+1)*(spaceValue/5))/(intNodeListLength*groupItemNum);
	
	if(booleanHasLegend){
		if(intNodeListLength >15){
			intTitleWidth = 5900;
		}else{
			intTitleWidth = 3500;
		}
	}else{
		intTitleWidth = 4400;
	}
	/////////装配柱图标题vml代码////////////////////////
	if(booleanHasBorder){
                vmlTitle = "<v:shape style='position:absolute;left:0px;top:-50px;width:"+intTitleWidth+"px;height:350px;text-align:center;z-index:1'><v:textbox><w:txbxContent><w:p><w:pPr><w:jc w:val='center'/><w:rPr><w:b/><w:b-cs/><w:sz w:val='20'/><w:sz-cs w:val='20'/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:hint='fareast'/><wx:font wx:val='宋体'/><w:b/><w:b-cs/><w:sz w:val='21'/><w:sz-cs w:val='21'/></w:rPr><w:t>" + vCaption + "</w:t></w:r></w:p></w:txbxContent></v:textbox></v:shape>";
        }else{
                vmlTitle = "<v:shape style='position:absolute;left:0px;top:-50px;width:" + intTitleWidth + "px;height:350px;text-align:center;z-index:1'><v:textbox><w:txbxContent><w:p><w:pPr><w:jc w:val='center'/><w:rPr><w:b/><w:b-cs/><w:sz w:val='20'/><w:sz-cs w:val='20'/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:hint='fareast'/><wx:font wx:val='宋体'/><w:b/><w:b-cs/><w:sz w:val='21'/><w:sz-cs w:val='21'/></w:rPr><w:t>" + vCaption + "</w:t></w:r></w:p></w:txbxContent></v:textbox></v:shape>";
        }
	////////装配扩展坐标系vml代码////////////////
        vmlExtendCoordinate += "<v:shape filled='false' style='position:absolute;left:220;width:1800;height:2000;top:" + vTopDistance + "px;z-index:1;' path = 'm 0,2500 l 200,2300, 3500,2300, 3300,2500x e'></v:shape><v:shape filled='false' style='position:absolute;left:220;top:" + vTopDistance + "px;width:1800;height:2000;z-index:1;' path = 'm 0,2500 l 200,2300,3500,2300,3500,300,200,300,0,500x e'></v:shape>";        
        for(var i=0;i<10;i++){//装配扩展坐标系
                //水平左虚线 
                vmlExtendCoordinate += "<v:line  from='220," + (500 + i*200 + vTopDistance) + "' to='400," + (300 + i*200 + vTopDistance) + "' style='z-index:1;' strokecolor='" + vExtendCoordinateLineColor + "' strokeweight='1pt'/>";        
                //水平右虚线
                vmlExtendCoordinate += "<v:line  from='400," + (300 + i*200 + vTopDistance) + "' to='3350," + (300 + i*200 + vTopDistance) + "' style='z-index:1;' strokecolor='" + vExtendCoordinateLineColor + "' strokeweight='1pt'/>";
                //y轴刻度线//////////////////////////////////////////////////////        
                if(booleanYScaleIsInteger && maxPatientNum % 10 != 0){//y轴刻度为整型
                        maxPatientNum = maxPatientNum + (10 - maxPatientNum % 10);
                }
                if(i==0){//y轴第一个刻度
                        vmlExtendCoordinate += "<v:shape style='position:absolute;left:-200px;top:" + (2380 - 200*i + vTopDistance) + "px;width:400px;height:200px;z-index:1'><v:textbox><w:txbxContent><w:p><w:pPr><w:jc w:val='center'/><w:rPr><w:b/><w:b-cs/><w:sz w:val='20'/><w:sz-cs w:val='20'/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:hint='fareast'/><wx:font wx:val='宋体'/><w:b-cs/><w:sz w:val='16'/><w:b/><w:sz-cs w:val='16'/></w:rPr><w:t>0</w:t></w:r></w:p></w:txbxContent></v:textbox></v:shape>";
                }else{//y轴中间刻度
                        vmlExtendCoordinate += "<v:shape style='position:absolute;left:-200px;top:" + (2380 - 200*i + vTopDistance) + "px;width:400px;height:200px;z-index:1'><v:textbox><w:txbxContent><w:p><w:pPr><w:jc w:val='center'/><w:rPr><w:b/><w:b-cs/><w:sz w:val='20'/><w:sz-cs w:val='20'/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:hint='fareast'/><wx:font wx:val='宋体'/><w:b-cs/><w:sz w:val='16'/><w:sz-cs w:val='16'/></w:rPr><w:t>" + Math.round((maxPatientNum/10)*i*100)/100 + "</w:t></w:r></w:p></w:txbxContent></v:textbox></v:shape>";
                }
                if(i==9){//y轴最后一个刻度
                        vmlExtendCoordinate += "<v:shape style='position:absolute;left:-200px;top:" + (2380 - 200*10 + vTopDistance) + "px;width:400px;height:200px;z-index:1'><v:textbox><w:txbxContent><w:p><w:pPr><w:jc w:val='center'/><w:rPr><w:b/><w:b-cs/><w:sz w:val='20'/><w:sz-cs w:val='20'/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:hint='fareast'/><wx:font wx:val='宋体'/><w:b-cs/><w:sz w:val='16'/><w:sz-cs w:val='16'/></w:rPr><w:t>" + maxPatientNum + "</w:t></w:r></w:p></w:txbxContent></v:textbox></v:shape>";
                }
        }
        //装配扩展坐标系交接线         
        vmlExtendCoordinate += "<v:line  from='400," + (300 + vTopDistance) + "' to='400," + (2300 + vTopDistance) + "'  style='z-index:1;' strokecolor='" + vExtendCoordinateLineColor + "' strokeweight='1pt'/>";
        /////////装配柱图vml代码////////////////////////////////
        vmlGroupPillar += "<v:shapetype id='__COLUMN' coordsize='26000,26000' o:spt='22' adj='5400' path='m0,0l0,21600,21600,21600,21600,0xe'><v:path shadowok='t' o:extrusionok='t' strokeok='t' fillok='t' o:connecttype='rect' /><v:stroke joinstyle='miter'/><o:lock v:ext='edit' shapetype='t'/></v:shapetype>";
        vmlGroupPillar += '<v:shapetype id="__QINGXIANXIANSHI" coordsize="21600,21600" o:spt="136" adj="10800" path="m@7,l@8,m@5,21600l@6,21600e"><v:formulas><v:f eqn="sum #0 0 10800"/><v:f eqn="prod #0 2 1"/><v:f eqn="sum 21600 0 @1"/><v:f eqn="sum 0 0 @2"/><v:f eqn="sum 21600 0 @3"/><v:f eqn="if @0 @3 0"/><v:f eqn="if @0 21600 @1"/><v:f eqn="if @0 0 @2"/><v:f eqn="if @0 @4 21600"/><v:f eqn="mid @5 @6"/><v:f eqn="mid @8 @5"/><v:f eqn="mid @7 @8"/><v:f eqn="mid @6 @7"/><v:f eqn="sum @6 0 @5"/></v:formulas><v:path textpathok="t" o:connecttype="custom" o:connectlocs="@9,0;@10,10800;@11,21600;@12,10800" o:connectangles="270,180,90,0"/><v:textpath on="t" fitshape="t"/><v:handles><v:h position="#0,bottomRight" xrange="6629,14971"/></v:handles><o:lock v:ext="edit" text="t" shapetype="t"/></v:shapetype>';
	
	for(var i=0;i<intNodeListLength;i++){//装配柱图		
		
		//x轴标记值//////////////////////////////////////////////////////        
        if(booleanHasXText){//是否显示x轴刻度值
            if(booleanSlant){//倾斜显示
                    vmlGroupPillar += '<v:shape id="_x0000_s1037" type="#__QINGXIANXIANSHI" style="position:absolute;left:' + (320 + i*xScale - (parseInt((oItemNodesList[i].getAttribute("name")).length) * 100)/2) + ';top:' + (2600 + vTopDistance) + ';text-align:left;margin-left:-9px;margin-top:23.4px;width:' + (parseInt((oItemNodesList[i].getAttribute("name")).length) * 100) + 'px;height:70px;rotation:21805313fd;z-index:1"><v:shadow color="#868686"/><v:textpath style="font-family:&quot;宋体&quot;;font-size:8px;v-text-kern:t" trim="t" fitpath="t" string="' + oItemNodesList[i].getAttribute("name") + '"/></v:shape>';
            }else{//水平显示(默认)
                    vmlGroupPillar += "<v:shape style='position:absolute;left:" + (100 + i*xScale) + "px;top:" + (2500 + vTopDistance) + "px;width:700px;height:200px;z-index:1;' rotation='130'><v:textbox><w:txbxContent><w:p><w:pPr><w:jc w:val='center'/><w:rPr><w:b-cs/><w:sz w:val='20'/><w:sz-cs w:val='20'/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:hint='fareast'/><wx:font wx:val='宋体'/><w:b-cs/><w:sz w:val='18'/><w:sz-cs w:val='18'/></w:rPr><w:t>" + oItemNodesList[i].getAttribute("name") + "</w:t></w:r></w:p></w:txbxContent></v:textbox></v:shape>"; 
            }
        }
		//柱状图
		
		if(booleanSingleExtrusionColor){//使用单色填充
			//
		}else{//使用预置色填充(默认)
			for( var j = 0; j < groupItemNum; j ++ ){//装配每组的统计图
				if(!oItemNodesList[i].childNodes[j] || parseFloat(oItemNodesList[i].childNodes[j].getAttribute("value")) == 0){
					vmlGroupPillar += "<v:rect title='" + oItemNodesList[i].getAttribute("name") + "' style='position:absolute;left:" + (400 + i*xScale + j*pillarWidth/1.5) + "px;top:" + (2400 + vTopDistance + 30) + "px;width:" + pillarWidth/1.5 + "px;height:0px;z-index:1' fillcolor='" + objPillarColorArray[j] + "'><v:fill color2='" + objPillarColorArray[j] + "' rotate='t' angle='-180' type='gradient'/><o:extrusion v:ext='view' backdepth='" + vExtrusiondepth + "' color='#cccccc' skewangle='230' on='t'/></v:rect>";
				}else{
					vmlGroupPillar += "<v:rect title='" + oItemNodesList[i].getAttribute("name") + "  " + oItemNodesList[i].childNodes[j].getAttribute("name") + "  " + oItemNodesList[i].childNodes[j].getAttribute("value") + "' style='position:absolute;left:" + (400 + i*xScale + j*pillarWidth/1.5) + "px;top:" + (2400 - (2000/maxPatientNum)*(parseFloat(oItemNodesList[i].childNodes[j].getAttribute("value"))) - 60  + vTopDistance) + "px;width:" + pillarWidth/1.5 + "px;height:" + ((2000/maxPatientNum)*(parseFloat(oItemNodesList[i].childNodes[j].getAttribute("value"))) + 90) + "px;z-index:1' fillcolor='" + objPillarColorArray[j] + "'><v:fill color2='" + objPillarColorArray[j] + "' rotate='t' angle='-180' type='gradient'/><o:extrusion v:ext='view' backdepth='" + vExtrusiondepth + "pt' color='black' on='t'/></v:rect>";
				}
				//柱状图值		
				if(!oItemNodesList[i].childNodes[j] || parseFloat(oItemNodesList[i].childNodes[j].getAttribute("value")) == 0){
					vmlGroupPillar += "<v:shape style='position:absolute;left:" + (170 + i*xScale + j*pillarWidth + pillarWidth/2 - 20) + "px;top:2300px;width:" + pillarWidth + "px;height:100px;z-index:1'><v:textbox><w:txbxContent><w:p><w:pPr><w:jc w:val='center'/><w:rPr><w:b/><w:b-cs/><w:sz w:val='20'/><w:sz-cs w:val='20'/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:hint='fareast'/><wx:font wx:val='宋体'/><w:b-cs/><w:sz w:val='15'/><w:sz-cs w:val='15'/></w:rPr><w:t>0</w:t></w:r></w:p></w:txbxContent></v:textbox></v:shape> ";
				}else{
					vmlGroupPillar += "<v:shape style='position:absolute;left:" + (170 + i*xScale + j*pillarWidth + pillarWidth/2 - 40) + "px;top:" + (2100 - (2000/maxPatientNum)*(parseFloat(oItemNodesList[i].childNodes[j].getAttribute("value"))) + vTopDistance) + "px;width:400px;height:220px;z-index:1'><v:textbox><w:txbxContent><w:p><w:pPr><w:jc w:val='center'/><w:rPr><w:b-cs/><w:sz w:val='20'/><w:sz-cs w:val='20'/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:hint='fareast'/><wx:font wx:val='宋体'/><w:b-cs/><w:sz w:val='15'/><w:sz-cs w:val='15'/></w:rPr><w:t>" + oItemNodesList[i].childNodes[j].getAttribute("value") + "</w:t></w:r></w:p></w:txbxContent></v:textbox></v:shape> ";
				}
				
				//图例
				if(booleanHasLegend && i==0){
					if(j<15){		
						vmlLegend += "<v:shape style='position:absolute;left:250px;top:" + (3400 + j*180) + "px;width:900px;height:210px;z-index:1'><v:textbox><w:txbxContent><w:p><w:pPr><w:jc w:val='left'/><w:rPr><w:b/><w:b-cs/><w:sz w:val='20'/><w:sz-cs w:val='20'/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:hint='fareast'/><wx:font wx:val='宋体'/><w:b-cs/><w:sz w:val='20'/><w:sz-cs w:val='20'/></w:rPr><w:t>" + oItemNodesList[i].childNodes[j].getAttribute("name") + "</w:t></w:r></w:p></w:txbxContent></v:textbox></v:shape><v:rect style='left:10px;top:" + (3500 + j*180) + "px;width:100px;height:60px;z-index:1;' fillcolor='" + objPillarColorArray[j] + "' strokecolor='black'><v:fill color2='" + objPillarColorArray[j] + "' rotate='t' type='gradient'/><o:extrusion v:ext='view' backdepth='3' color='" + objPillarColorArray[j] + "' on='t'/></v:rect>";
					}else{
						vmlLegend += "<v:shape style='position:absolute;left:1200px;top:" + (100 + (i-15)*130) + "px;width:900px;height:100px;z-index:1'><v:textbox><w:txbxContent><w:p><w:pPr><w:jc w:val='center'/><w:rPr><w:b/><w:b-cs/><w:sz w:val='20'/><w:sz-cs w:val='20'/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:hint='fareast'/><wx:font wx:val='宋体'/><w:b/><w:b-cs/><w:sz w:val='20'/><w:sz-cs w:val='20'/></w:rPr><w:t>" + oItemNodesList[i].childNodes[0].text + "</w:t></w:r></w:p></w:txbxContent></v:textbox></v:shape><v:rect title='" + oItemNodesList[i].childNodes[0].text + "' style='left:1000px;top:" + (120 + (i-15)*130) + "px;width:120px;height:60px;z-index:1;' fillcolor='" + objPillarColorArray[i] + "' strokecolor='black'><v:fill color2='white' rotate='t' type='gradient'/><o:extrusion v:ext='view' backdepth='3' color='" + objPillarColorArray[i] + "' on='t'/></v:rect>";
					}
				}
			}
		}
	}
	//vmlLegend = "<v:group style='position:absolute;border:solid 0px red;left:4100px;top:" + (200 + vTopDistance) + "px;width:750px;height:900px;z-index:1;' coordsize='900,900'>" + vmlLegend + "</v:group>";
	// vmlEnd = "<v:group xmlns:v='urn:schemas-microsoft-com:vml' xmlns:w='http://schemas.microsoft.com/office/word/2003/wordml' xmlns:wx='http://schemas.microsoft.com/office/word/2003/auxHint'  xmlns:o='urn:schemas-microsoft-com:office:office' style='position:absolute;background:" + vBackColor + ";left:0px;top:0px;width:300px;height:300px;' coordsize='4000,4000' ><v:rect style='position:absolute;left:0;top:0;height:2000;width:2000' stroked='false'/>" + vmlTitle + vmlCaky + "</v:group>";
	vmlEnd = "<v:group xmlns:v='urn:schemas-microsoft-com:vml' xmlns:w='http://schemas.microsoft.com/office/word/2003/wordml' xmlns:wx='http://schemas.microsoft.com/office/word/2003/auxHint'  xmlns:o='urn:schemas-microsoft-com:office:office' style='position:absolute;background:" + vBackColor + ";top:" + vTop + "px;left:" + vLeft + "px;width:" + vReportformWidth/1.1 + "px;height:" + vReportformHeight/1.2 + "px;' coordsize='2000,2000'>" + vmlTitle + vmlExtendCoordinate + vmlGroupPillar + vmlLegend + "</v:group>";
	return vmlEnd;
}

function createPlumbPillar(){//装配垂直柱状统计图vml代码

	var oItemNodesList = null		
	var vmlEnd = "";//最终的vml代码
	var vmlGroupPillar = "";//柱图vml代码
	var vmlLegend = "";//图例vml代码
	var vmlTitle = "";//柱图标题vml代码
	var vmlExtendCoordinate = "";//扩展坐标系
	var xScale = 0;
	var yScale = 0;
	var intNodeListLength = 0;//组个数
	var intTitleWidth = 0;//标题宽度
	var vTopDistance = 100;//顶端距离调整值
	if(!objXMLDoc) return;
	var oItemNodesList = objXMLDoc.selectNodes("//root/item");//项目节点	
	intNodeListLength = oItemNodesList.length;
	
	if(!oItemNodesList || oItemNodesList.length==0){
		objContainerDiv.style.background = "#cccccc";			
		with(objInfoSpan.style){
			visibility = "visible";	
			width = 120;
			height = 30;
			left = (parseInt(objContainerDiv.style.width) + parseInt(element.offsetLeft))/2 - parseInt(objInfoSpan.style.width)/2;
			top = (parseInt(objContainerDiv.style.height) + parseInt(element.offsetTop))/2 - parseInt(objInfoSpan.style.height)/2;
		}
		objInfoSpan.innerText = "数据加载失败......";
		return;	
	}else{
		objContainerDiv.innerHTML = "";
		objContainerDiv.style.background = "";
		objInfoSpan.innerText = "";
	}
	
	xScale = Math.ceil(2950/intNodeListLength);
	yScale = Math.ceil(maxPatientNum/10);
	pillarWidth = (2950 - (intNodeListLength+1)*spaceValue)/(intNodeListLength*groupItemNum);
	
	if(booleanHasLegend){
		if(intNodeListLength >15){
			intTitleWidth = 5900;
		}else{
			intTitleWidth = 5100;
		}
	}else{
		intTitleWidth = 4400;
	}
	/////////装配柱图标题vml代码////////////////////////
	if(booleanHasBorder){
		vmlTitle = "<v:rect style='position:absolute;width:" + intTitleWidth + "px;height:" + (2900 + vTopDistance) + "px;z-index:1;' coordsize='21600,21600' filled='false' strokecolor='" + vBorderColor + "' strokeweight='" + vBorderWeight + "'></v:rect><v:shape style='position:absolute;left:0px;top:80px;width:" + intTitleWidth + "px;height:400px;text-align:center;z-index:1'><div style='font-size:10.5pt;font-weight:bold;'>" + vCaption + "</div></v:shape>";
	}else{
		vmlTitle = "<v:rect style='position:absolute;width:" + intTitleWidth + "px;height:" + (2900 + vTopDistance) + "px;z-index:1;' coordsize='21600,21600' fillcolor='white' stroked='false'></v:rect><v:shape style='position:absolute;left:0px;top:80px;width:" + intTitleWidth + "px;height:400px;text-align:center;z-index:1'><div style='font-size:10.5pt;font-weight:bold;'>" + vCaption + "</div></v:shape>";
	}
	////////装配扩展坐标系vml代码////////////////
	vmlExtendCoordinate += "<v:shape id='boxbottom' FillColor='" + vExtendCoordinateBaseColor + "'style='position:absolute;width:2000;height:2000;top:" + vTopDistance + "px;z-index:1;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=40);' path = 'm 500,2500 l 700,2300,4000,2300,3800,2500,500,2500,x e'><v:fill color2='white' rotate='t' angle='110' type='gradient'/></v:shape><v:shape id='boxleft' fillColor='" + vExtendCoordinateBackColor + "'style='position:absolute;top:" + vTopDistance + "px;width:2000;height:2000;z-index:1;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=60);' path = 'm 500,2500 l 700,2300,4000,2300,4000,300,700,300,500,500,500,2500,x e'><v:fill color2='white' rotate='t' angle='170' type='gradient'/></v:shape>";	
	for(var i=0;i<10;i++){//装配扩展坐标系
		//水平左虚线 
		vmlExtendCoordinate += "<v:line  from='500," + (500 + i*200 + vTopDistance) + "' to='700," + (300 + i*200 + vTopDistance) + "' style='z-index:1;' strokecolor='" + vExtendCoordinateLineColor + "' strokeweight='1pt'/>";	
		//水平右虚线
		vmlExtendCoordinate += "<v:line  from='700," + (300 + i*200 + vTopDistance) + "' to='4000," + (300 + i*200 + vTopDistance) + "' style='z-index:1;' strokecolor='" + vExtendCoordinateLineColor + "' strokeweight='1pt'/>";
		//y轴刻度线//////////////////////////////////////////////////////		
		if(booleanYScaleIsInteger && maxPatientNum % 10 != 0){//y轴刻度为整型
			maxPatientNum = maxPatientNum + (10 - maxPatientNum % 10);
		}
		if(i==0){//y轴第一个刻度
			vmlExtendCoordinate += "<v:shape style='position:absolute;left:200px;top:" + (2480 - 200*i + vTopDistance) + "px;width:400px;height:100px;z-index:1'><div align='left' style='font-size:8pt;font-weight:bold'>0</div></v:shape>";
		}else{//y轴中间刻度
			vmlExtendCoordinate += "<v:shape style='position:absolute;left:200px;top:" + (2480 - 200*i + vTopDistance) + "px;width:400px;height:100px;z-index:1'><div align='left' style='font-size:8pt;'>" + Math.round((maxPatientNum/10)*i*100)/100 + "</div></v:shape>";
		}
		if(i==9){//y轴最后一个刻度
			vmlExtendCoordinate += "<v:shape style='position:absolute;left:200px;top:" + (2480 - 200*10 + vTopDistance) + "px;width:400px;height:100px;z-index:1'><div align='left' style='font-size:8pt;'>" + maxPatientNum + "</div></v:shape>";
		}
	}
	//装配扩展坐标系交接线	 
	vmlExtendCoordinate += "<v:line  from='700," + (300 + vTopDistance) + "' to='700," + (2300 + vTopDistance) + "'  style='z-index:1;' strokecolor='" + vExtendCoordinateLineColor + "' strokeweight='0pt'/>";
	/////////装配柱图vml代码////////////////////////////////
	vmlGroupPillar += "<v:shapetype id='__COLUMN' coordsize='26000,26000' o:spt='22' adj='5400' path='m0,0l0,21600,21600,21600,21600,0xe'><v:path shadowok='t' o:extrusionok='t' strokeok='t' fillok='t'o:connecttype='rect'/><v:stroke joinstyle='miter'/><o:lock v:ext='edit' shapetype='t'/></v:shapetype>";
	
	for(var i=0;i<intNodeListLength;i++){//装配柱图		
				
		//x轴标记值//////////////////////////////////////////////////////	
		if(booleanHasXText){//是否显示x轴刻度值
			if(booleanSlant){//倾斜显示
				vmlGroupPillar += "<v:line style='position:absolute;left:" + (800 + i*xScale) + "px;top:" + (2600 + vTopDistance) + "px;width:400px;height:100px;z-index:1;' from='-70,100' to='90,0'><v:path textpathok='true'/><v:textpath on='true' style='font-size:8pt;' string='" + oItemNodesList[i].getAttribute("name") + "'/></v:line>"; 
			}else{//水平显示(默认)
				vmlGroupPillar += "<v:shape style='position:absolute;left:" + (700 + i*xScale) + "px;top:" + (2600 + vTopDistance) + "px;width:400px;height:100px;z-index:1;' rotation=130><div align='left' style='font-size:9pt;' title='" + oItemNodesList[i].childNodes[0].text + "'>" + oItemNodesList[i].getAttribute("name") + "</div></v:shape>"; 
			}
		}
		//柱状图
		
		if(booleanSingleExtrusionColor){//使用单色填充
			//
		}else{//使用预置色填充(默认)
			for(var j=0;j<groupItemNum;j++){//装配每组的统计图
				if(!oItemNodesList[i].childNodes[j] || parseFloat(oItemNodesList[i].childNodes[j].getAttribute("value")) == 0){
					vmlGroupPillar += "<v:rect title='" + oItemNodesList[i].getAttribute("name") + "' style='position:absolute;left:" + (800 + i*xScale + j*pillarWidth) + "px;top:" + (2400 + vTopDistance + 30) + "px;width:" + pillarWidth + "px;height:0px;z-index:1' fillcolor='" + objPillarColorArray[j] + "'><v:fill color2='" + objPillarColorArray[j] + "' rotate='t' angle='-180' type='gradient'/><o:extrusion v:ext='view' backdepth='" + vExtrusiondepth + "' color='#cccccc' skewangle='230' on='t'/></v:rect>";
				}else{
					vmlGroupPillar += "<v:rect title='" + oItemNodesList[i].getAttribute("name") + "  " + oItemNodesList[i].childNodes[j].getAttribute("name") + "  " + oItemNodesList[i].childNodes[j].getAttribute("value") + "' style='position:absolute;left:" + (800 + i*xScale + j*pillarWidth) + "px;top:" + (2400 - (2000/maxPatientNum)*(parseFloat(oItemNodesList[i].childNodes[j].getAttribute("value"))) - 60  + vTopDistance) + "px;width:" + pillarWidth + "px;height:" + ((2000/maxPatientNum)*(parseFloat(oItemNodesList[i].childNodes[j].getAttribute("value"))) + 90) + "px;z-index:1' fillcolor='" + objPillarColorArray[j] + "'><v:fill color2='" + objPillarColorArray[j] + "' rotate='t' angle='-180' type='gradient'/><o:extrusion v:ext='view' backdepth='" + vExtrusiondepth + "' color='" + objPillarColorArray[j] + "' skewangle='230' on='t'/></v:rect>";
				}
				//柱状图值		
				if(!oItemNodesList[i].childNodes[j] || parseFloat(oItemNodesList[i].childNodes[j].getAttribute("value")) == 0){
					vmlGroupPillar += "<v:shape style='position:absolute;left:" + (800 + i*xScale + j*pillarWidth + pillarWidth/2 - 20) + "px;top:2400px;width:" + pillarWidth + "px;height:100px;z-index:1'><div style='text-align:left;font-size:8pt;color:black;'>0</div></v:shape> ";
				}else{
					vmlGroupPillar += "<v:shape style='position:absolute;left:" + (800 + i*xScale + j*pillarWidth + pillarWidth/2 - 40) + "px;top:" + (2200 - (2000/maxPatientNum)*(parseFloat(oItemNodesList[i].childNodes[j].getAttribute("value"))) + vTopDistance) + "px;width:400px;height:100px;z-index:1'><div style='text-align:left;font-size:8pt;color:black;'>" + oItemNodesList[i].childNodes[j].getAttribute("value") + "</div></v:shape> ";
				}
				
				//图例
				
				if(booleanHasLegend && i==0){
					if(j<15){		
						vmlLegend += "<v:shape style='position:absolute;left:300px;top:" + (100 + j*130) + "px;width:600px;height:100px;z-index:1'><div style='padding-left:5;font-size:12px;' title='" + oItemNodesList[i].childNodes[j].getAttribute("name") + "'>" + oItemNodesList[i].childNodes[j].getAttribute("name") + "</div></v:shape><v:rect title='" + oItemNodesList[i].childNodes[j].getAttribute("name") + "' style='left:100px;top:" + (120 + j*130) + "px;width:120px;height:60px;z-index:1;' fillcolor='" + objPillarColorArray[j] + "' strokecolor='black'><v:fill color2='" + objPillarColorArray[j] + "' rotate='t' type='gradient'/><o:extrusion v:ext='view' backdepth='3' color='" + objPillarColorArray[j] + "' on='t'/></v:rect>";
					}else{
						vmlLegend += "<v:shape style='position:absolute;left:1200px;top:" + (100 + (i-15)*130) + "px;width:600px;height:100px;z-index:1'><div style='padding-left:5;font-size:12px;' title='" + oItemNodesList[i].childNodes[0].text + "'>" + oItemNodesList[i].childNodes[0].text + "</div></v:shape><v:rect title='" + oItemNodesList[i].childNodes[0].text + "' style='left:1000px;top:" + (120 + (i-15)*130) + "px;width:120px;height:60px;z-index:1;' fillcolor='" + objPillarColorArray[i] + "' strokecolor='black'><v:fill color2='white' rotate='t' type='gradient'/><o:extrusion v:ext='view' backdepth='3' color='" + objPillarColorArray[i] + "' on='t'/></v:rect>";
					}
				}
				
			}
		}
	}
	vmlLegend = "<v:group style='position:absolute;border:solid 0px red;left:4100px;top:" + (200 + vTopDistance) + "px;width:750px;height:900px;z-index:1;' coordsize='900,900'>" + vmlLegend + "</v:group>";

	vmlEnd = "<v:group xmlns:v='urn:schemas-microsoft-com:vml' xmlns:w='http://schemas.microsoft.com/office/word/2003/wordml' xmlns:wx='http://schemas.microsoft.com/office/word/2003/auxHint'  xmlns:o='urn:schemas-microsoft-com:office:office' id='" + vTagID + "' style='position:absolute;background:" + vBackColor + ";top:" + vTop + "px;left:" + vLeft + "px;width:" + vWidth + "px;height:" + vHeight + "px;' coordsize='2000,2000'>" + vmlTitle + vmlExtendCoordinate + vmlGroupPillar + vmlLegend + "</v:group>";
	objContainerDiv.insertAdjacentHTML("afterBegin",vmlEnd);
}


////////////////////////////////////////////////////////////////////////////////
function putXMLSource(){
	var argXMLSource=element.xmlSource;
	var objGlobal;	
	try{
		objGlobal = __objGlobalCommonInst;		
	}catch(e){}
	try{
		if(!objGlobal) objGlobal = parent.__objGlobalCommonInst;
	}catch(e){}
	
	if(!objGlobal){
		objGlobal = parent.parent.__objGlobalCommonInst;
	}
	try{
		if(typeof(objGlobal) == "undefined") objGlobal = window.dialogArguments.__objGlobalCommonInst;
	}catch(e){}
	
	var vSortXSLT = '<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="xml" version="1.0" encoding="gbk" indent="yes"/><xsl:template match="/"><root><xsl:apply-templates select="root"/></root></xsl:template><xsl:template match="root"><xsl:for-each select="item"><xsl:sort select="@idx"/><xsl:copy-of select="."/></xsl:for-each></xsl:template></xsl:stylesheet>';
	objXMLDoc = objGlobal.inputXML(argXMLSource);
	objXMLDoc = objGlobal.transformXMLToNode(objXMLDoc,vSortXSLT);//排序
	var objNodeItem;
	maxPatientNum = 0;
	groupItemNum = 0;
	objItemList = objXMLDoc.selectNodes("//root/item");
	//计算每组中柱状统计图的个数
	for(var i=0;i<objItemList.length;i++){
		groupItemNum = (groupItemNum >parseInt(objItemList[i].childNodes.length))?groupItemNum:parseInt(objItemList[i].childNodes.length);
	}
	//计算最大值
	for(var i=0;i<objItemList.length;i++){
		for(var j=0;j<objItemList[i].childNodes.length;j++){
			if(isNaN(parseFloat(objItemList[i].childNodes[j].getAttribute("value")))) return;
			maxPatientNum = (maxPatientNum >= parseFloat(objItemList[i].childNodes[j].getAttribute("value")))?maxPatientNum:parseFloat(objItemList[i].childNodes[j].getAttribute("value")); 		
		}
	}
}

function putLeft(){
	var argLeft=element.left;
	if(argLeft.toString().match(/^[0-9]*$/) != null){
		vLeft = argLeft;
	}
}

function putTop(){
	var argTop=element.top;
	if(argTop.toString().match(/^[0-9]*$/) != null){
		vTop = argTop;
	}
}

function putWidth(){
	var argWidth=element.width;
	if(argWidth.toString().match(/^[0-9]*$/) != null){
		vWidth = argWidth;
	}
}

function putHeight(){
	var argHeight=element.height;
	if(argHeight.toString().match(/^[0-9]*$/) != null){
		vHeight = argHeight;
	}
}

function putCaption(){
	vCaption = element.caption;
}

function putExtrusionColor(){
	vExtrusionColor = element.extrusionColor;
}

function putTagID(){
	vTagID = element.tagID;
}

function putExtrusionDepth(){
	var argExtrusionDepth=element.extrusionDepth;
	if(argExtrusionDepth.toString().match(/^[0-9]*$/) != null){
		vExtrusiondepth = argExtrusionDepth;
	}
}

function putExtrusionSingleColor(){
	var argExtrusionSingleColor=element.extrusionSingleColor;
	if(argExtrusionSingleColor == "yes"){
		booleanSingleExtrusionColor = true;
	}else{
		booleanSingleExtrusionColor = false;
	}
}

function putPsOrientation(){
	vPsOrientation = element.psOrientation;
}

function putHasBorder(){
	var argHasBorder=element.hasBorder;
	if(argHasBorder == "no"){
		booleanHasBorder = false;
	}else if(argHasBorder == "yes"){
		booleanHasBorder = true;
	}
}

function putHasLegend(){
	var argHasLegend=element.hasLegend;
	if(argHasLegend == "no"){
		booleanHasLegend = false;
	}else if(argHasLegend == "yes"){
		booleanHasLegend = true;
	}
}

function putHasXText(){
	var argHasXText=element.hasXText;
	if(argHasXText == "no"){
		booleanHasXText = false;
	}else if(argHasXText == "yes"){
		booleanHasXText = true;
	}
}

function putBorderColor(){
	vBorderColor = element.borderColor;
}

function putBorderWeight(){
	var argBorderWeight=element.borderWeight;
	if(argBorderWeight.toString().match(/^[0-9]*$/) != null){
		vBorderWeight = parseInt(argBorderWeight);
	}
}

function putBackColor(){
	vBackColor = element.backColor;
}

function putExtendCoordinateBackColor(){
	vExtendCoordinateBackColor = element.extendCoordinateBackColor;
}

function putXTextSlant(){
	var argXTextSlant=element.xTextSlant;
	if(argXTextSlant == "yes"){
		booleanSlant = true;
	}else if(argXTextSlant == "no"){
		booleanSlant = false;
	}
}

function putYScaleIsInteger(){
	var argYScaleIsInteger=element.yScaleIsInteger;
	if(argYScaleIsInteger == "yes"){
		booleanYScaleIsInteger = true;
	}else if(argYScaleIsInteger == "no"){
		booleanYScaleIsInteger = false;
	}
}


	__jhtcBindPropertyChange(element,"xmlSource",putXMLSource);
	__jhtcBindPropertyChange(element,"top",putTop);
	__jhtcBindPropertyChange(element,"left",putLeft);
	__jhtcBindPropertyChange(element,"width",putWidth);
	__jhtcBindPropertyChange(element,"height",putHeight);
	__jhtcBindPropertyChange(element,"hasBorder",putHasBorder);
	__jhtcBindPropertyChange(element,"hasLegend",putHasLegend);
	__jhtcBindPropertyChange(element,"hasXText",putHasXText);
	__jhtcBindPropertyChange(element,"borderColor",putBorderColor);
	__jhtcBindPropertyChange(element,"borderWeight",putBorderWeight);
	__jhtcBindPropertyChange(element,"backColor",putBackColor);
	__jhtcBindPropertyChange(element,"yScaleIsInteger",putYScaleIsInteger);
	__jhtcBindPropertyChange(element,"caption",putCaption);
	__jhtcBindPropertyChange(element,"tagID",putTagID);
	__jhtcBindPropertyChange(element,"psOrientation",putPsOrientation);
	__jhtcBindPropertyChange(element,"xTextSlant",putXTextSlant);
	__jhtcBindPropertyChange(element,"extrusionColor",putExtrusionColor);
	__jhtcBindPropertyChange(element,"extrusionDepth",putExtrusionDepth);
	__jhtcBindPropertyChange(element,"extrusionSingleColor",putExtrusionSingleColor);
	__jhtcBindPropertyChange(element,"extendCoordinateBackColor",putExtendCoordinateBackColor);
	__jhtcBindPropertyChange(element,"init",initialize);

  	jhtc_attr_init(element,"xmlSource","");
  	jhtc_attr_init(element,"top","");
  	jhtc_attr_init(element,"left","");
  	jhtc_attr_init(element,"width","");
  	jhtc_attr_init(element,"height","");
  	
  	jhtc_attr_init(element,"hasBorder","");
  	jhtc_attr_init(element,"hasLegend","");
  	jhtc_attr_init(element,"hasXText","");
  	jhtc_attr_init(element,"borderColor","");
  	jhtc_attr_init(element,"borderWeight","");
  	jhtc_attr_init(element,"backColor","");
  	jhtc_attr_init(element,"yScaleIsInteger","");
  	
  	jhtc_attr_init(element,"caption","");
  	jhtc_attr_init(element,"tagID","");
  	jhtc_attr_init(element,"psOrientation","");
  	jhtc_attr_init(element,"xTextSlant","");
  	
  	jhtc_attr_init(element,"extrusionColor","");
  	jhtc_attr_init(element,"extrusionDepth","");
  	jhtc_attr_init(element,"extrusionSingleColor","");
  	
  	jhtc_attr_init(element,"extendCoordinateBaseColor","");
  	jhtc_attr_init(element,"extendCoordinateBackColor","");
  	jhtc_attr_init(element,"extendCoordinateLineColor","");
  	jhtc_attr_init(element,"init","");

  	
	element.refreshData=refreshData;
	element.createVMLForWord=createVMLForWord;

	element.init=initialize;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_tag_map["statPgs"]=jhtc_statPillarGroupStat;