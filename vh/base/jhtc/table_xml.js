function jhtc_table_xml(win,jhtc_obj){
  var pageWin=win;
  var element=jhtc_obj;
  var fixRow = 2;
  var fixCol = 1;
  var colOffset = -1;

  var baseDiv;
  var baseDivTop = 0
  var baseDivLeft = 0

  var colDiv;
  var rowDiv;
  var commonDiv;

  var baseTable;
  var baseTableTop = 0
  var baseTableLeft = 0

  var form

  var colDiv
  var colTable
  var rowDiv 
  var rowTable
  var commonDiv 
  var commonTable
  var commonWidth = 0
  var commonHeight = 0

  var colors

  // 带箭头的缓存
  var colArrowTemp = null
	function fnInit2(){
		element.fixRow = eval(element.fixRow);
		element.fixCol = eval(element.fixCol);
		element.colOffset = eval(element.colOffset);
		if(element.fixRow=="undefined"||element.fixRow ==null){
			element.fixRow=2;
		}
		if(element.fixCol=="undefined"||element.fixCol==null)
			element.fixCol=1;
		if(element.colOffset=="undefined"||element.colOffset==null)
			element.colOffset=-1;
		
		var vXml = new ActiveXObject("Microsoft.XMLDOM");
    vXml.async=false;
    vXml.load(element.document.URL.replace(/.html.*$/g, ".xsl"));
    var vXml2 = new ActiveXObject("Microsoft.XMLDOM");
    vXml2.async=false;
    vXml2.loadXML("<root/>");
		var mstr=vXml2.transformNode(vXml);
		var start_pos=mstr.indexOf('<thead>');
		var end_pos=mstr.indexOf('</tbody>');
		mstr=mstr.replace("</thead>","");
		mstr=mstr.replace("<tbody>","");
		mstr=mstr.substring(start_pos+7,end_pos);
		jhtcSetHtml(element,"<table   id='_mainDataTable'  border='1' bgColor=white borderColor='#999999' style='line-height:24px;BORDER-COLLAPSE:collapse;font-family:\"宋体\"; font-size:10pt' >"+mstr+"</table>");

		fnInit();
	}
  function setFixRow() {
   // element.fixRow = eval(element.fixRow);
  }

  function setFixCol(argValue) {
   // element.fixCol = eval(argValue)
  }

  function setColOffset(argValue) {
   // element.colOffset = eval(argValue)
  }
  

  /*
   * 初始化
   */
	function fnInit() {


  baseDivTop = 0
  baseDivLeft = 0

  baseTableTop = 0
  baseTableLeft = 0

  commonWidth = 0
  commonHeight = 0
		colDiv = element.document.createElement("<div class='tableSubDiv'/>")
  	rowDiv = colDiv.cloneNode(true)
  	commonDiv = colDiv.cloneNode(true)
	  //创建 baseDIV
	rowDiv.id="_vhTableRowDiv";
	colDiv.id="_vhTableColDiv";
    baseDiv = element.document.createElement("<div class='tableBaseDiv' id='_vhTableBaseDiv'/>");
    element.appendChild(baseDiv)

    baseTable = element.firstChild;
    baseDiv.appendChild(baseTable);

    if (baseTable.rows.length <= element.fixRow){
      return;
     }

    baseDiv.onscroll  = scrollDiv
    baseDiv.ondblclick = initColor

    // 取得baseDiv绝对位置
    form = baseDiv
    while(form.tagName != "BODY") {
	    baseDivTop += form.offsetTop;
	    baseDivLeft += form.offsetLeft;
	    form = form.offsetParent;
    }

    // 取得baseTable绝对位置
    form = baseTable
    while(form.tagName != "BODY") {
	    baseTableTop += form.offsetTop;
	    baseTableLeft += form.offsetLeft;
	    form = form.offsetParent;
    }

    var border = eval(baseTable.border)
    if (border == null)
      border = 0

    // 确定公共区域长宽
    for (var i=0; i<element.fixRow; i++) {
      commonHeight = commonHeight + baseTable.rows[i].offsetHeight + border
    }
    commonHeight = commonHeight + border

    for (var i=0; i<element.fixCol; i++) {
      commonWidth = commonWidth + baseTable.rows[0].cells[i].offsetWidth + border
    }
    commonWidth = commonWidth + border + border

    // 初始化
    colTable = baseTable.cloneNode(true)
    colDiv.appendChild(colTable)
    element.appendChild(colDiv)
    colDiv.ondblclick = initColor

    rowTable = baseTable.cloneNode(true);
    for (var i=element.fixRow; i<colTable.rows.length; i++) {
      rowTable.deleteRow(element.fixRow);
      //baseTable.rows[i].onmousedown = changeColor
      //colTable.rows[i].onmousedown = changeColor
      baseTable.rows[i].onmouseover = changeColor
      baseTable.rows[i].onmouseout = changeColor
      colTable.rows[i].onmouseover = changeColor
      colTable.rows[i].onmouseout = changeColor
    
    }
    rowDiv.appendChild(rowTable)
    element.appendChild(rowDiv)

    commonTable = rowTable.cloneNode(true)
    commonDiv.appendChild(commonTable)
    element.appendChild(commonDiv)

    // 响应表头排序
    if (element.colOffset >= 0 && baseTable.rows.length > element.fixRow) {
      for (var i=element.fixRow-1; i<element.fixRow; i++) {
        for (var j=0; j<rowTable.rows[i].cells.length; j++) {
          rowTable.rows[i].cells[j].onclick = sortTable
          rowTable.rows[i].cells[j].style.cursor = 'hand'
        }
      }
    }
    rowTable.rows[element.fixRow-1].cells[0].click();

    //初始化颜色
    colors = new Array(baseTable.rows.length-element.fixRow);

    // 激活属性baseDiv.style.width
    baseDiv.style.width = form.clientWidth - baseDivLeft
    adjShape();

    //绑定事件
    window.attachEvent("onresize", adjShape);
	}


  function adjShape() {
    baseDiv.style.width = form.clientWidth - baseDivLeft - (baseDiv.offsetWidth - baseDiv.clientWidth)+16
    baseDiv.style.height = form.clientHeight - baseDivTop - (baseDiv.offsetHeight - baseDiv.clientHeight)+16

    // 固定列 判断必要性
    colDiv.style.top = baseTableTop
    colDiv.style.left = baseTableLeft
    colDiv.style.width = commonWidth
    colDiv.style.height = baseDiv.clientHeight

    // 固定行 判断必要性
    rowDiv.style.top = baseTableTop
    rowDiv.style.left = baseTableLeft
    rowDiv.style.width = baseDiv.clientWidth
    rowDiv.style.height = commonHeight


    // 固定公共部分 判断必要性
    commonDiv.style.top = baseTableTop
    commonDiv.style.left = baseTableLeft
    commonDiv.style.width = commonWidth
    commonDiv.style.height = commonHeight
  }

  function scrollDiv() {
    colDiv.scrollTop = baseDiv.scrollTop
    rowDiv.scrollLeft = baseDiv.scrollLeft
  }

  function changeColor() {
    if (colors[this.rowIndex-element.fixRow]==null) {
      colors[this.rowIndex-element.fixRow] = this.bgColor
      colTable.rows[this.rowIndex].bgColor = '#EFEFEF'
      baseTable.rows[this.rowIndex].bgColor = '#EFEFEF'
    } else {
      colTable.rows[this.rowIndex].bgColor = colors[this.rowIndex-element.fixRow]
      baseTable.rows[this.rowIndex].bgColor = colors[this.rowIndex-element.fixRow]
      colors[this.rowIndex-element.fixRow] = null
    }
  }

  function initColor() {
    for (var i=0; i<colors.length; i++) {
      if (colors[i] != null) {
        colTable.rows[i+element.fixRow].bgColor = colors[i]
        baseTable.rows[i+element.fixRow].bgColor = colors[i]
        colors[i] = null;
      }
    }
  }

  function sortTable() {
    if (colArrowTemp!=null)
      colArrowTemp.removeChild(colArrowTemp.lastChild);

    if (this.upMode==null)
      this.upMode = false
    else
      this.upMode = !this.upMode

    var arrowUp = element.document.createElement("SPAN")
    arrowUp.innerHTML	= "5"
    arrowUp.style.cssText 	= "PADDING-RIGHT: 0px; MARGIN-TOP: -3px; PADDING-LEFT: 0px; FONT-SIZE: 10px; MARGIN-BOTTOM: 2px; PADDING-BOTTOM: 2px; OVERFLOW: hidden; WIDTH: 10px; COLOR: blue; PADDING-TOP: 0px; FONT-FAMILY: webdings; HEIGHT: 11px";

    var arrowDown = element.document.createElement("SPAN");
    arrowDown.innerHTML	= "6";
    arrowDown.style.cssText = "PADDING-RIGHT: 0px; MARGIN-TOP: -3px; PADDING-LEFT: 0px; FONT-SIZE: 10px; MARGIN-BOTTOM: 2px; PADDING-BOTTOM: 2px; OVERFLOW: hidden; WIDTH: 10px; COLOR: blue; PADDING-TOP: 0px; FONT-FAMILY: webdings; HEIGHT: 11px";

    this.appendChild(this.upMode?arrowUp:arrowDown);
    colArrowTemp = this

    // 排序
    var sortArray = new Array();
  	var cellValue;
  	var colIndex = this.cellIndex + element.colOffset
  	for(var i=element.fixRow;i<baseTable.rows.length;i++){
  	  cellValue = baseTable.rows[i].cells[colIndex].innerText.toLowerCase();
  	  while (cellValue.indexOf(",")>=0)
        cellValue = cellValue.replace(',','0');
  		sortArray.length++
  		sortArray[sortArray.length-1] = new Array(cellValue, baseTable.rows[i], colTable.rows[i])
  	}

    var mode = this.upMode
    sortArray.sort(function(arr1, arr2) {
                  var a = Number(arr1[0]);
			            var b = Number(arr2[0]);
			           	a=eval(a);
			            b=eval(b);
			            return mode?(a-b):(b-a);
                 });
  	for(var i=0;i<sortArray.length;i++){
  		baseTable.lastChild.appendChild(sortArray[i][1])
  		colTable.lastChild.appendChild(sortArray[i][2])
  	}

  	// 处理打印问题
    element.sortColIndex = colIndex
    element.sortFlag = this.upMode
  }
function refresh(aname, vXmlStr, flag, subFunc, hideMsg) {
			
  	var mhideMsg =hideMsg; 
    // 1.与后台进行数据交换
    if (aname != null && vXmlStr!=null) {
      addr = aname
      submitXml = vXmlStr
      submitXml = submitXml + "<_ActiveHeads>0</_ActiveHeads>";
      begin = 1
      if (flag) {
        isTurn = true
      }
    }


    if (addr==null)
      return
    if (subFunc == null) {
      subFunc = '';
    }
		var isTurn=false;
    if (isTurn) {
      if (begin<1) begin = 1
      if (subFunc != '') {
        window.xmlhttp.post(addr, "<_begin>"+begin+"</_begin><_end>"+(begin+page-1)+"</_end>"+submitXml, subFunc+'&isTurn=ture',"false")
      } else {
        window.xmlhttp.post(addr, "<_begin>"+begin+"</_begin><_end>"+(begin+page-1)+"</_end>"+submitXml, '?isTurn=ture',"false")
      }
    } else {
      window.xmlhttp.post(addr, submitXml, subFunc,"false")
    }

    jhtcBeginQuery();
    //等待返回数据
      window.xmlhttp._object.onreadystatechange = updatePage;  
}

  function refreshTab(source)
  {
    element.serverXml = source
  } 
    
function updatePage(){
	
 if(window.xmlhttp._object.readyState != 4){
    return false;
    }
jhtcEndQuery()

    // -- TBD 关于错误处理, 如果无记录，如何处理，有记录如何处理
    var source = window.xmlhttp._object.responseText;
    if (source.indexOf("<error>")>0){
        return doMsg(source);
    }
    refreshTab(source);
    var vXml = new ActiveXObject("Microsoft.XMLDOM");
    vXml.async=false;
    vXml.load(element.document.URL.replace(/.html.*$/g, ".xsl"));
    var vXml2 = inputXML(source);
		var mstr=vXml2.transformNode(vXml);
		var start_pos=mstr.indexOf('<thead>');
		var end_pos=mstr.indexOf('</tbody>');
		mstr=mstr.replace("</thead>","");
		mstr=mstr.replace("<tbody>","");
		mstr=mstr.substring(start_pos+7,end_pos);
		
		element.innerHTML="<table id='_mainDataTable'  border='1' width='100%'  bgColor=white borderColor='#999999' style='line-height:24px;BORDER-COLLAPSE:collapse;font-family:\"宋体\"; font-size:10pt' >"+mstr+"</table>" //原有height='100%'但因对没有满屏的数据有影响就删了
		window.setTimeout(fnInit,1);
}
 function inputXML(argSource){ ////objXMLDoc接收XML文件数据
    var objXMLDoc = null;
    try{
      switch(typeof(argSource)){
        case "string"://XML字节流
          if(argSource.search(/\./) != -1 && argSource.search(/\</) == -1){ //xml file's location

            objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");
            objXMLDoc.async = false;
            if(argSource.search(/\+/) != -1) argSource = eval(argSource);
              objXMLDoc.load(argSource);
              break;
            }
            if(argSource.search(/\</) != -1){ //xml string
              objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");
              objXMLDoc.loadXML(argSource);
              break;
            }
            objXMLDoc = eval(argSource);
            objXMLDoc = (objXMLDoc.XMLDocument)?(objXMLDoc.XMLDocument):null; //xml data island

            break;
        case "object"://XMLDOM对象
            if(argSource.xml) return objXMLDoc = argSource; //xml document object
            break;
            default:
            objXMLDoc = null;
      }
      if (!objXMLDoc.xml) objXMLDoc = null;
    } catch(err) { objXMLDoc = null; }

    if(objXMLDoc)
    {
  		objXMLDoc.setProperty("SelectionLanguage","XPath");
  		objXMLDoc.setProperty("SelectionNamespaces","xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
    }
    return objXMLDoc;
   }
   
   function refreshTabStyle(source) {
	 // 4. 默认处理 aa)选择提交 a)删除，b)修改，c)上翻页，d)下翻页

    // aa)加入选择提交的处理
    if(element.choose=="1")
    	element.choose="block";
    if (window.trim(element.choose) != '' && /<pk>/.test(response.xml)) {
      // 显示选择框
      var inputs = element.getElementsByTagName('input')

      var flagstr = window.trim(checkflag)

      for (var i=0; i<inputs.length; i++) {
        if (inputs[i].type == 'checkbox') {
          inputs[i].parentNode.style.display = element.choose;//''
		      if(inputs[i].parentNode.tagName=='TH')
			      inputs[i].onclick = setAll; // 全选或全取消
		      else {
    				if(inputs[i].parentNode.tagName=='TD')
    				{
    				  inputs[i].onclick = checkAll; // 检测是否全选或全取消
    				  if(flagstr == 'true'){
	    				  if(inputs[i].parentNode.nextSibling.childNodes[0].data=='1')
	    				  inputs[i].checked = true
    				  }
    				}
    			}
        }
      }
    }

    // a)加入删除的处理
    if (window.trim(remove) != '' && /<pk>/.test(response.xml)) {
      // 加入按钮响应事件
      element.document.getElementById(remove).onclick = del

      // 显示删除
      var inputs = element.getElementsByTagName('input')
      for (var i=0; i<inputs.length; i++) {
        if (inputs[i].type == 'checkbox') {
          inputs[i].parentNode.style.display = ''
		      if(inputs[i].parentNode.tagName=='TH')
			      inputs[i].onclick = setAll; // 全选或全取消
		      else {
    				if(inputs[i].parentNode.tagName=='TD'){
   			      inputs[i].onclick = checkAll; // 检测是否全选或全取消
   			      inputs[i].checked=getCheckboxOldChecked(inputs[i]);
   			    }
    			}
        }
      }
    }

    // b)加入修改的处理
    if (window.trim(update) != '' && /<pk>/.test(response.xml)) {
      // -- TBD 检查权限

      // 加下划线和相应方法
      var vHref = element.getElementsByTagName('a');
      for (var i=0; i<vHref.length; i++) {
        if (vHref[i].href == '') {
          vHref[i].style.textDecoration = 'underline'
          vHref[i].style.cursor = 'hand'
          vHref[i].style.color  = 'blue'

          vHref[i].onclick = modify
        }
      }
    }

    // c) 加入上翻页处理
    var objUp = element.document.getElementById('_up')
    if (objUp != null) {
      objUp.onclick = pageUp
    }
    // d) 加入下翻页处理
    var objDown = element.document.getElementById('_down')
    if (objDown != null) {
      objDown.onclick = pageDown
    }

    var ojbHome = element.document.getElementById('_home')
    if (ojbHome != null) {
      ojbHome.onclick = pageHome
    }

    var objEnd = element.document.getElementById('_end')
    if (objEnd != null) {
      objEnd.onclick = pageEnd
    }

    // 5. 调整nowrap
    var tbody = element.getElementsByTagName('TBODY')[1]
    for (var i=0; i<tbody.rows.length; i++) {
    	tbody.rows[i].style.height='22'
      if (i%2 == 0) { // 两色相间
        tbody.rows[i].style.backgroundColor = window.oddColor
      } else {
        tbody.rows[i].style.backgroundColor = window.evenColor
      }
      tbody.rows[i].onmouseout = function() {
        if (this.rowIndex==element.rowIndex)
          return
        this.runtimeStyle.backgroundColor = ''
      }
      for (var j=0; j<tbody.rows[i].cells.length; j++) {
        tbody.rows[i].cells[j].noWrap = 'true'
        tbody.rows[i].cells[j].onfocus = tdActive
      }
    }

    for (var i=0; i<tbody.rows.length; i++) {
      var hideCount=0;
      for (var k=0; k<tbody.rows[i].cells.length; k++) {
        if (tbody.rows[i].cells[k].style.display=='none') {
          hideCount++
        }
      }
      
      for (var j=0; j<element.cellInputs.length; j++) {
        if (element.cellInputs[j]!=null) {                      
          tbody.rows[i].cells[j+hideCount].setActive()
          break;
        }
      }
      break;
    }

    // 6. 调整位置
    adjPosition()

    // 7. 加入键盘响应
    tbody.onkeydown = navigateKeys
    if(onrefresh!=''){
    	eval(onrefresh)
    }
  }
  
   function submit_choose(btn,addXML,isAll,isOnly,hideMsg,subFunc) {
    if (window.trim(element.choose) == '') {
      alert('choose属性设置错误！');
    }
    
    if (subFunc==null) {
      subFunc = '';
    }
    
    var data = element.getElementsByTagName('tbody')[1];
    var trs = data.getElementsByTagName("tr")
    var temp_xml = '';
    var flag = false;
    var onlyFlag = false;
    
    for (var i=0; i<trs.length; i++){
      var inputs =  trs[i].getElementsByTagName("input")
      for (var j=0; j<inputs.length; j++) {
        if (isAll != null && isAll != "") {
          if (inputs[j].type == 'checkbox' && inputs[j].checked) {
  				  temp_xml = temp_xml + '<record>' + inputs[j].value + '<checked>1</checked></record>';
          } else {
            temp_xml = temp_xml + '<record>' + inputs[j].value + '<checked>0</checked></record>';
          }
          flag = true;
        } else {
          if (inputs[j].type == 'checkbox' && inputs[j].checked) {
            if (isOnly != null && isOnly != "") {
              if (!onlyFlag) {
      				  temp_xml = temp_xml + '<record>' + inputs[j].value + '</record>';
                flag = true;
                onlyFlag = true;
              } else {
                alert("一次只能选择一条记录！");
                return false;
              }
            } else {
              temp_xml = temp_xml + '<record>' + inputs[j].value + '</record>';
              flag = true;
            }
          }
        }
      }
    }

    if (!flag) {
      alert('请先选择')
      return false;
    }
    if (addXML != null && addXML != "") {
      temp_xml = addXML + '<multiData>' + temp_xml + '</multiData>'
  	}
  	//prompt('',temp_xml);
	  window.xmlhttp.post(btn, temp_xml ,subFunc)
    window.xmlhttp._object.onreadystatechange = updateSubmitChoose;  
}

function updateSubmitChoose(){
    if(window.xmlhttp._object.readyState != 4){
    //alert(window.xmlhttp._object.readyState);
    return false;
    }

	  var str = window.xmlhttp._object.responseText

    if (window.doMsg(str,hideMsg)) {
      return true;
    } else 
      return false;

	  refresh();

  }
  
  function adjPosition() {
    //1. 调整baseDiv的位子
    parentObj = baseDiv = element.document.getElementById('_base');
    baseDivTop = baseDivLeft = 0
    while(parentObj.tagName != "BODY") {
	    baseDivTop += parentObj.offsetTop;
	    baseDivLeft += parentObj.offsetLeft;
	    parentObj = parentObj.offsetParent;
    }

    baseDiv.style.pixelWidth = parentObj.clientWidth - baseDivLeft -  parseInt(jQuery(element).attr("rightFix"))
    baseDiv.style.pixelHeight = parentObj.clientHeight - baseDivTop  - parseInt(jQuery(element).attr("bottomFix"))

    //2. ......

    //3. 调整输入域的位置
    var tbody = baseDiv.getElementsByTagName('tbody')[0]

    if (this.rowIndex!=null && this.rowIndex>-1) {
      var hideCount=0;
      if (tbody.parentNode.rows.length>this.rowIndex) {
	      for (i=0; i<tbody.parentNode.rows[this.rowIndex].cells.length; i++) {
	        if (tbody.parentNode.rows[this.rowIndex].cells[i].style.display=='none') {
	          hideCount++
	        }
	      }
      }

      for (var i=0; i<this.cellInputs.length; i++) {
        if (this.cellInputs[i]!=null && this.cellInputs[i].style.display=='' && tbody.parentNode.rows.length>this.rowIndex) {
          var temp = tbody.parentNode.rows[this.rowIndex].cells[i+hideCount]
          var comp = tbody.parentNode.rows[this.rowIndex].cells[i+hideCount]
          var vLeft=0, vTop=0;
          while (temp.tagName != "DIV") {
            vTop += temp.offsetTop;
  	        vLeft += temp.offsetLeft;
  	        temp = temp.offsetParent;
  	      }

          with (this.cellInputs[i].style) {
            pixelHeight = comp.clientHeight
            pixelWidth = comp.clientWidth
  	        left = vLeft + eval(comp.parentNode.parentNode.parentNode.border)
  	        top = vTop + eval(comp.parentNode.parentNode.parentNode.border)
          }
          break;
        }
      }
    }

    //调整headDiv的位子
    headDiv = element.document.getElementById('_head');
    if (headDiv==null) return
    baseDiv.onscroll = function() {
      headDiv.scrollLeft = baseDiv.scrollLeft
    }

    var tHead1s = baseDiv.getElementsByTagName('thead')
    var tHead2s = headDiv.getElementsByTagName('thead')

    for (var i=0; i<tHead1s[0].rows.length; i++) {
      for (var j=0; j<tHead1s[0].rows[i].cells.length; j++) {
        tHead1s[0].rows[i].cells[j].noWrap = 'true'
      }
    }

    with (headDiv.style)  {
      pixelTop = baseDivTop+1;
      pixelLeft = baseDivLeft+1
      pixelWidth = baseDiv.clientWidth
      pixelHeight = tHead1s[0].offsetHeight + tHead1s[0].parentNode.border
    } // -- study baseDiv.offsetWidth-baseDiv.offsetWidth+baseDiv.clientWidth, tHead1s[0].offsetHeight + tHead1s[0].parentNode.border*2

    for (var i=0; i<tHead1s[0].rows.length; i++) {
      for (var j=0; j<tHead1s[0].rows[i].cells.length; j++) {
        tHead2s[0].rows[i].cells[j].noWrap = 'true'
        tHead2s[0].rows[i].cells[j].style.pixelWidth = tHead1s[0].rows[i].cells[j].offsetWidth
        tHead2s[0].rows[i].cells[j].style.pixelHeight = tHead1s[0].rows[i].cells[j].offsetHeight
      }
    }
    //headDiv.style.pixelTop = baseDivTop + tHead1s[0].rows[0].offsetTop+1;
    headDiv.style.display=''
  }

  	jhtc_attr_init(element,"fixRow","");
  	jhtc_attr_init(element,"fixCol","");
  	jhtc_attr_init(element,"colOffset","");
  	jhtc_attr_init(element,"sortColIndex",null);
  	jhtc_attr_init(element,"sortFlag",null);
  	jhtc_attr_init(element,"choose",null);
  	jhtc_attr_init(element,"serverXml","");
  	jhtc_attr_init(element,"bottomFix","0");//设置距离下边框距离
	jhtc_attr_init(element,"rightFix","0");//设置距离右边框距离
  	
	element.refresh=refresh;
	element.submit_choose=submit_choose;
	element.adjPosition=adjPosition;
	element.init=fnInit2;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_class_map["fixTable"]=jhtc_table_xml;
jhtc_tag_map["vhFixTable"]=jhtc_table_xml;
