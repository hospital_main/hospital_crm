function jhtc_select(win,jhtc_obj){
  var pageWin=win;
  var element=jhtc_obj;
  //整个SELECT由两个输入框，一个按钮和一个显示列表组成
  var objBtn=null;
  var objList=null;
  var objInput;
  var srcTree = new ActiveXObject("Microsoft.XMLDOM");
  var xsltTree= new ActiveXObject("Microsoft.XMLDOM");
  srcTree.async=false;
  xsltTree.async=false;
  var hasList = "0";
  var minViewList=1;
  var vListMaxHigh = 81;
  var objXML;
  var oldSetValue;//LZK ADD
  
  function trim(a){
  	return a;
  }
  function setEnabled(e){
  	if(e==true){
  		objBtn.style.display="block";
  		objInput.disabled=false;
  	}else{
  		objBtn.style.display="none";
  		objInput.disabled=true;
  	}
  }
  function hasResult(){
    if (hasList == "0")
      return false;
    else 
      return true;
  }

  function setText(){
    return element.text=objInput.value;
  }

  function initBegin() {
	element.code="false";
    if (element.label != null) {
      // label 的基本长度
      var lableLength = 120;
      var spanWidth = lableLength+Math.ceil((element.label.length-6)/2)*40;
      if (spanWidth < lableLength) spanWidth = lableLength;

      // 快捷键
      var aKey = "";
      if (trim(element.accessKey)!="") {
        aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
      }
      element.insertAdjacentHTML("beforeBegin", "<span id='labelCtn' nowrap style='text-align:right;width:"+spanWidth+"px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";font-weight:"+element.currentStyle.fontWeight+";'>"+element.label+aKey+(element.label==''?"":"：")+"</span>");
    } else {
      var aKey = "";
      if (trim(element.accessKey)!="") {
        aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
      }  
      element.insertAdjacentHTML("beforeBegin", "<span id='labelCtn' nowrap style='text-align:right;width:0px;font-family:"+element.currentStyle.fontFamily+";font-size:"+element.currentStyle.fontSize+";font-weight:"+element.currentStyle.fontWeight+";'></span>");
    }
    element.parentNode.noWrap = true;
    
    // 设置输入框的样式
    objInput = element.document.createElement("<input/>");
    __jhtcBindPropertyChange(objInput,"value",setText);
    element.style.width = element.extent;
    element.style.color = "#FFFFFF";

    with (objInput) {
      accessKey = element.accessKey;
      readOnly = element.readOnly;
      //className = element.className+"_text";
      style.border="1px none red";
      style.position = "absolute";
      style.height = element.offsetHeight - 2;
      style.width = (parseInt(element.extent) - 22)<0 ? 100:parseInt(element.extent) - 22;
      maxLength = element.maxInput;
      if ("true"== trim(element.required)) {
        style.backgroundColor="#DBFCFF";
      }

      onmouseover = overBtn;
      onmouseout = outBtn;
      onkeydown = navigateKeys;
      onblur = function() {
      	clickDocument();
      }
    }
    
    objInput.onkeyup = objInput.ondragend = changeInput;

    element.accessKey="";
    element.tabIndex=-1;
    element.insertAdjacentElement("beforeBegin", objInput);
  	initList();
  }
  
  function changeInput() {
  	if ((event.keyCode>=33 && event.keyCode<=40) || event.keyCode==13 || event.keyCode==16 || event.keyCode==18 || event.keyCode==9
          || event.keyCode==27 || event.keyCode==0 || event.keyCode>250)
      return;
    if(objInput.value == "") {
    	element.value = "";
    }  
  	if (trim(element.checkValue)!="true") {
  	  element.value = objInput.value;
  	}
		if (trim(this.value)=="" && objList.style.display=="none") return;
		showList();
  }
  
  function changeTitle() {
    objInput.title = element.text;
    element.title = element.text;
  }
  
  function initEnd() {
    if (objInput == null) return;
    // 初始化按钮
    element.insertAdjacentHTML("afterEnd","<v:group style='display:none;top:1px;right:1px;position:absolute;width:14px; height:16px;z-index:3;' coordsize='16,16'><v:roundrect arcsize='0.1' style='width:14;height:14;' fillcolor='#3174C2' strokecolor='#ADC3F7'><v:fill type='gradientradial' color2='#58AAE8' angle='75'/></v:roundrect><v:polyline style='position:absolute;top:4; left:3' points='0,0 4,4 8,0' strokecolor='#4A6184' strokeweight='2px' filled='false'/></v:group>");
    objBtn = element.nextSibling;
    with (objBtn) {
      style.width = style.height = (element.offsetHeight-3)+"px";
      onmouseover = overBtn;
      onmouseout = outBtn;
      onclick = clickBtn;
    }
    
    if(element.findpage!=null){
      objBtn.insertAdjacentHTML("afterEnd","<div style='position:absolute'><a href='#' style='text-decoration: none;' onclick='openFindPage("+element.name+")'><span style='font-family:Webdings;font-size: 16px;'>2</span></a></div>");
    }
    
  	window.attachEvent("onresize", adjPosition);
  	adjPosition();
  	window.setTimeout(adjPosition,50);
  	objBtn.style.display=''
  	if (trim(element.initValue) != "") {
  	  setValue(element.initValue);
  	}
  }
  
  // 调整各个元素的位置
  function adjPosition() { // 调整位置
    // 取得element的绝对位置
    if(objBtn==null||objList==null)
    	return ;
    var form = element;
    var elementTop=0, elementLeft=0;

    while(form.tagName != "BODY") {
	    elementTop = elementTop + form.offsetTop + form.clientTop;
	    elementLeft = elementLeft + form.offsetLeft+form.clientLeft;
	    form = form.offsetParent;
    }

    with (objInput.style) {
      top = elementTop;
      left = elementLeft;
    }

    with (objBtn.style) {
      top = elementTop + 1;
      left = elementLeft + element.offsetWidth - element.offsetHeight - 1;
    }

    with (objList.style) {
     	left = elementLeft-1;
     	if(elementTop+element.offsetHeight-1+parseInt(objList.style.height.replace(/px/,'')) > window.document.body.offsetHeight)
     	  top = elementTop + element.offsetHeight-1-101;
     	else
  		  top = elementTop + element.offsetHeight-1;
    }
  }

  function overBtn(){ //鼠标移动到objBtn上的样式
    if (objBtn==null) return;
  	var objBtnBg = objBtn.children(0);
  	objBtnBg.fillcolor = "#3174C2";
  	objBtnBg.fill.color2 = "#58AAE8";
  }

  function outBtn(){ //鼠标移出Btn 或者input时的样式
    if (objBtn==null) return;
  	var objBtnBg;
  	if(objList && objList.style.display != "none")
  	  overBtn();
  	else {
  		objBtnBg = objBtn.children(0);
  		objBtnBg.fillcolor = "#3174C2";
  		objBtnBg.fill.color2 = "#58AAE8";
  	}
  }

  function clickBtn() { // Btn 按下后的样式
    if (objList!=null && objList.style.display != "none"){
      objList.style.display = "none";
      }
    else{
  	  showList();
  	}
  }
  //wsj 从showList 方法中抽取出来，专门用于加载数据源，而不显示。
 function loadListdata(){
 		var lists = window.document.getElementsByTagName("DIV")
		for (var i=0; i<lists.length; i++) {
			if (lists[i].className == "select_list")
				lists[i].style.display = "none";
		}

		if (!element.readOnly && false) {
			srcTree.loadXML(objXML.replace(/key='(.*)'/,"key='"+trim(objInput.value)+"'"))
			objList.innerHTML = srcTree.transformNode(xsltTree);

	    var trs = objList.getElementsByTagName("TR")
	    for (var i=0; i<trs.length; i++) {
	      trs[i].onmouseover = function() {
	        if (this.parentNode.parentNode.choseIndex!=null) {
	          with (this.parentNode.parentNode.rows[this.parentNode.parentNode.choseIndex].runtimeStyle) {
	            backgroundColor="";
	            color="";
	          }
	        }

	        this.parentNode.parentNode.choseIndex = this.rowIndex;
	        this.runtimeStyle.backgroundColor="darkblue";
	        this.runtimeStyle.color="white";
	      }
	      trs[i].onmousedown = choose;
	      if (i%2==0) {
	        trs[i].style.backgroundColor="whitesmoke";
	      }
	    }
    }
	
 }
 //wsj
  // 显示列表
  function showList() {
		loadListdata();	
    objList.style.display = "";

    var trs = objList.getElementsByTagName("TR");
    if (trs.length==0) return false;
    var i=0;
    for (; i<trs.length; i++) {
      if (trs[i].innerText.indexOf(objInput.value)==0) {
        break;
      }
    }

    if (i==trs.length) i=0;

    if (trs[i].parentNode.parentNode.choseIndex!=null) {
      with (trs[i].parentNode.parentNode.rows[trs[i].parentNode.parentNode.choseIndex].runtimeStyle) {
        backgroundColor = "";
        color = "";
      }
    }
    trs[i].runtimeStyle.backgroundColor="darkblue";
    trs[i].runtimeStyle.color="white";
    trs[i].parentNode.parentNode.choseIndex = trs[i].rowIndex;
    // 移动div scroll 1.取trs[i]的绝对top
    var baseDivTop = 0;
    var parentObj = trs[i];
    while (parentObj.tagName != "DIV") {
	    baseDivTop += parentObj.offsetTop;
	    parentObj = parentObj.offsetParent;
    }
    parentObj.scrollTop = baseDivTop;

    window.document.attachEvent("onmousedown",clickDocument);
  }
  function initListByXML(str) {

    if (trim(this.code)=="true") {
      objXML = str.replace("<root>","<root code='true' qtype='0' key=''>");
    } else
      objXML = str.replace("<root>","<root code='false' qtype='0' key=''>");
    srcTree.loadXML(objXML);
    xsltTree.load(window.prefix+"/base/xsl/select1.xsl");
    objList.innerHTML = srcTree.transformNode(xsltTree);

		with (objList) {
  		style.display = "none";
      style.top = objInput.offsetTop+objInput.offsetHeight;
    	style.width = element.offsetWidth;
  	}

  	var trs = objList.getElementsByTagName("TR")
    for (var i=0; i<trs.length; i++) {
      trs[i].onmouseover = function() {
        if (this.parentNode.parentNode.choseIndex!=null) {
          with (this.parentNode.parentNode.rows[this.parentNode.parentNode.choseIndex].runtimeStyle) {
            backgroundColor=''
            color=''
          }
        }

        this.parentNode.parentNode.choseIndex = this.rowIndex
        this.runtimeStyle.backgroundColor='darkblue'
        this.runtimeStyle.color='white'
      }
      trs[i].onmousedown = choose
      if (i%2==0) {
        trs[i].style.backgroundColor='whitesmoke'
      }
    }

  	if (trim(element.required)=="true" && objXML.indexOf("code=\"")!=-1) {
			var str = objXML.substring(objXML.indexOf("code=\"")+"code=\"".length)
			element.value = str.substring(0, str.indexOf("\""))
			str = str.substring(str.indexOf("value=\"")+"value=\"".length)
			objInput.value = str.substring(0, str.indexOf("\""))
			if (trim(element.code)=="true")
				objInput.value = element.value+":"+objInput.value
  	}
  }

  // 初始化显示列表
  function initList(str) {
  	objInput.value = "";
  	element.value = "";
    objList = element.document.createElement("<div class='select_list'></div>");
  	element.insertAdjacentElement("afterEnd", objList);
//added by wsj		
    var loadXML = "";
/*wsj1.1 输入法检索*/    
    if (trim(element.load)=="retrieve_method") {
 //   	para=para+"<text>"+element.text+"</text">;
    	 
      if( element.para.indexOf("<retrieve>")>0)
 	     element.para= element.para.substring(0,element.para.indexOf("<retrieve>"));//如果不是初始状态，则取得，在尾部已经给了输入法的数值，去掉将后面的输入法的数值
   	   element.para=element.para+'<retrieve>'+getRetrieve()+'</retrieve>'  
    }
    
//added by wsj

    var loadXML = new ActiveXObject("Microsoft.XMLDOM");
	loadXML.loadXML(element.load);

    if (trim(this.code)=="true") {
      objXML = loadXML.xml.replace("<root>","<root code='true' qtype='0' key=''>");
    } else
      objXML = loadXML.xml.replace("<root>","<root code='false' qtype='0' key=''>");

    srcTree.loadXML(objXML);
    var objNodeList = srcTree.getElementsByTagName("para");
    var maxLength = 0;
    for (var j=0; j<objNodeList.length; j++) {
      if (maxLength < objNodeList.item(j).attributes.item(1).nodeValue.length) {
        maxLength = objNodeList.item(j).attributes.item(1).nodeValue.length;
      }
    }
    
    xsltTree.load("base/xsl/select1.xsl");
    objList.innerHTML = srcTree.transformNode(xsltTree);
    var trs = objList.getElementsByTagName("TR");
		with (objList) {
  		style.display = "none";
      style.top = objInput.offsetTop+objInput.offsetHeight;
      var viewlist=5;
      if(trim(element.required)=="true"){
      	if(trs.length-1<minViewList)
      		viewlist=minViewList;
      	else if((trs.length-1>=minViewList)&&(trs.length-1<=element.maxViewList))
      		viewlist=trs.length-1;
      	else
      		viewlist=element.maxViewList;
      }else{
      	if(trs.length<minViewList)
      		viewlist=minViewList;
      	else if((trs.length>=minViewList)&&(trs.length<=element.maxViewList))
      		viewlist=trs.length;
      	else
      		viewlist=element.maxViewList;
      }
      style.height=viewlist*16 + 3;
  	}

  	if (maxLength <= 9) {
  	  objList.style.width = element.offsetWidth;
  	} else {
  	  var temp = parseFloat(element.offsetWidth)+parseFloat(((maxLength-9)*20>200)?200:(maxLength-9)*20);
  	  if(isNaN(temp))
		temp=200;
  	  objList.style.width = temp>200?(element.extent>200?element.extent:200):temp;
  	}

  	var trs = objList.getElementsByTagName("TR")
  	if (trs.length != 0) {
  	  hasList = "1";
  	} else {
  	  hasList = "0";
  	}
    for (var i=0; i<trs.length; i++) {
      trs[i].onmouseover = function() {
        if (this.parentNode.parentNode.choseIndex!=null) {
          with (this.parentNode.parentNode.rows[this.parentNode.parentNode.choseIndex].runtimeStyle) {
            backgroundColor="";
            color="";
          }
        }

        this.parentNode.parentNode.choseIndex = this.rowIndex;
        this.runtimeStyle.backgroundColor="darkblue";
        this.runtimeStyle.color="white";
      }
      trs[i].onmousedown = choose;
      if (i%2==0) {
        trs[i].style.backgroundColor="whitesmoke";
      }
    }

    var flag = false;
  	if (trim(element.defaultValue)=="true" && objXML.indexOf("code=\"")!=-1) {
			flag = true;
  	}
  	
    if (trim(element.required)=="true") {
      objList.all("top_line").style.display = "none";
			objInput.style.backgroundColor="#DBFCFF";
  	} else {
  	  objInput.style.backgroundColor="#FFFFFF";
  	}
    
  	if (trim(element.required)=="true" && trim(element.defaultValue) != "false" && objXML.indexOf("code=\"")!=-1) {
			flag = true;
  	}
  	if (flag) {
			var str = objXML.substring(objXML.indexOf("code=\"")+"code=\"".length);
			element.value = str.substring(0, str.indexOf("\""));
			str = str.substring(str.indexOf("value=\"")+"value=\"".length);
			objInput.value = str.substring(0, str.indexOf("\""));
			if (trim(element.code)=="true")
			  objInput.value = element.value+":"+objInput.value;
			changeTitle();
  	}
  	
  	if (trim(str)!='' && !flag) {
	    objInput.value=''
	    element.value=''
	  }
  }
  
  // 刷新页面
  function refresh() {

    //alert(element.parentNode.outerHTML)
    var aKey = "";
    if (trim(element.accessKey)!="") {
      aKey = "(<span style='text-decoration:underline;'>"+element.accessKey.toUpperCase()+"</span>)";
    }
    if (trim(element.label)!="") {
      element.parentNode.getElementsByTagName("SPAN")[0].innerText = element.label+aKey+(element.label==''?"":"：");
    }
    //added by wsj 在重新加载的时候，检查select控件是否下拉。
    if(element.load!=""){
    	var dis=objList.style.display; //检查select控件是否下拉。
	  	initList();
	  	if(element.load=="retrieve_method"){
				if (dis!= "none"){
	      	showList();
	     	 }
		    else{
		  	  loadListdata();	
		  		}			
	    	}
    //wsj added
  	adjPosition();
  	setValue(oldSetValue); 
	}
  }
  
  // 点击选择项响应事件
	function choose() { 
    var table = objList.getElementsByTagName("TABLE")[0];
    if (table.choseIndex==null) {
      return false;
    }
		objInput.value = table.rows[table.choseIndex].innerText;
		changeTitle();
    objList.style.display = "none";
    
    if (table.rows[table.choseIndex].cells.length>0 &&
    	element.value!=table.rows[table.choseIndex].cells[0].value) {
    	element.value=table.rows[table.choseIndex].cells[0].value;
    	//var oEvent = createEventObject();
			//oEvent.result = element.value;
	   //	changeID.fire();
	   __jhtcDispatchEvent(element,"onchange");
	   	//add by 
    }

    objInput.select();
  }
  
  // 点击按钮等响应事件
  function clickDocument() { 
  	if(objList==null) return;

		var objSrc = window.event.srcElement;
		if (element.contains(objSrc) || objList.contains(objSrc) || (objBtn!=null && objBtn.contains(objSrc))) {
  	  if (objSrc.value != null) {
  	    element.value = objSrc.value;
  	  }
  	  return;
  	} 

  	objList.style.display="none";
    window.document.detachEvent("onmousedown",clickDocument);

  }


  // 键盘响应
  function navigateKeys() {
		var nKeyCode=event.keyCode;
    switch(nKeyCode){
      case 113:return true  // F2 
        selectObj.setXML(objXML)
        selectObj.select()
        if (trim(selectObj.value)!='')
          setValue(selectObj.value)
        return true
      case 38://^
        if (objList!=null && objList.style.display=="") {
          scrollUpList();
          return false;
        }
        break;
      case 40://\|/
        if (event.altKey) {
          if (objList==null || objList.style.display=="none") {
            showList();
            return false;
          }
        }
        if (objList!=null && objList.style.display=="") {
          scrollDownList();
          return false;
        }
        break;
      case 13://Enter
        if (element.value == "") {
         //changeID.fire();
         __jhtcDispatchEvent(element,"onchange");
        }
        if (objList!=null && objList.style.display=="") {
          choose();
          objInput.onkeyup = objInput.ondragend = null;
         	return false;
        }
        break;
      case 27:// Esc
        if (objList!=null)
          objList.style.display="none";
        return false;
      case 33://pageup
        if (objList!=null && objList.style.display=="") {
          scrollPageUpList();
          return false;
        }
      case 34://pagedown
      	if (objList!=null && objList.style.display=="") {
          scrollPageDownList();
          return false;
        }
      default:
        objInput.onkeyup = objInput.ondragend = changeInput;
        return true;
    }
    return true;
  }
  
  //向上翻页滚动列表
	function scrollPageUpList() {
    var table = objList.getElementsByTagName("TABLE")[0];
    if (table.rows.length>0) {
      var count = objList.offsetHeight/table.rows[0].offsetHeight-1;
      for (var i=0; i<count; i++) {
        scrollUpList();
      }
    }
  }
  
  //向下翻页滚动列表
  function scrollPageDownList() {
    var table = objList.getElementsByTagName("TABLE")[0];
    if (table.rows.length>0) {
      var count = objList.offsetHeight/table.rows[0].offsetHeight-1;
      for (var i=0; i<count; i++) {
        scrollDownList();
      }
    }
  }
  
  //向下滚动列表
  function scrollDownList() {
    var table = objList.getElementsByTagName("TABLE")[0];
    if (table.rows.length<1) return;
    if (table.choseIndex>=table.rows.length-1) return;

    if (table.choseIndex!=null) {
      with (table.rows[table.choseIndex].runtimeStyle) {
        backgroundColor = "";
        color = "";
      }
    } else {
      table.choseIndex = 0;
    }

    table.choseIndex++;

    var baseDivTop = 0;
    var parentObj = table.rows[table.choseIndex];
    while (parentObj.tagName != "DIV") {
	    baseDivTop += parentObj.offsetTop;
	    parentObj = parentObj.offsetParent;
    }

    if (baseDivTop+table.rows[table.choseIndex].offsetHeight*2 > parentObj.scrollTop+parentObj.offsetHeight) {
      parentObj.scrollTop = parentObj.scrollTop + table.rows[table.choseIndex].offsetHeight;
    }

    table.rows[table.choseIndex].runtimeStyle.backgroundColor="darkblue";
    table.rows[table.choseIndex].runtimeStyle.color="white";
  }

  //向上滚动列表
  function scrollUpList() {
    var table = objList.getElementsByTagName("TABLE")[0];

    if (table.choseIndex==null || table.choseIndex==0) {
      table.choseIndex = 0;
      return;
    } else {
      with (table.rows[table.choseIndex].runtimeStyle) {
        backgroundColor = "";
        color = "";
      }
    }

    table.choseIndex--;

    table.rows[table.choseIndex].runtimeStyle.backgroundColor="darkblue";
    table.rows[table.choseIndex].runtimeStyle.color="white";

    var baseDivTop = 0;
    var parentObj = table.rows[table.choseIndex];
    while (parentObj.tagName != "DIV") {
	    baseDivTop += parentObj.offsetTop;
	    parentObj = parentObj.offsetParent;
    }

    if (baseDivTop < parentObj.scrollTop) {
      parentObj.scrollTop = parentObj.scrollTop - table.rows[table.choseIndex].offsetHeight;
    }
  }
  
  //检查输入项是否正确
  function check() {
    
    if (trim(element.checkValue)!="true") {
  		return true;
  	}
   	if ("true"==element.required) {
      if (window.trim(objInput.value)=="" && trim(element.checkValue)=="true")  {
        if (element.label != null) {
          alert(element.label+"不能为空！");
        } else {
          alert("请选择必选项！");
        }
        objInput.focus();
        objInput.select();
        return false;
      }
    }
    if (trim(element.code)=="true" && objXML.indexOf("\""+trim(objInput.value).replace(/:.*$/, "")+"\" value=\""+trim(objInput.value).replace(/^.*:/, "")+"\"")==-1) {
    	if ((trim(element.required)=="true" || trim(objInput.value)!="") && trim(element.checkValue)=="true") {
	  		alert("请选择合法的值！");
	  		objInput.select();
	  		return false;
  		}
  	}
  	if (trim(element.code)!="true" && objXML.indexOf("value=\""+trim(objInput.value)+"\"")==-1 ) {
  		if ((trim(element.required)=="true" || trim(objInput.value)!="") && trim(element.checkValue)=="true") {
	  		alert("请选择合法的值！");
	  		objInput.select();
	  		return false;
  		}
  	}

    if (objInput.value != "") {
      var table = objList.getElementsByTagName("TABLE")[0];
      if (table.choseIndex != null) {
    		objInput.value = table.rows[table.choseIndex].innerText;
    		changeTitle();
        if (table.rows[table.choseIndex].cells.length>0 &&
        	element.value!=table.rows[table.choseIndex].cells[0].value) {
        	element.value=table.rows[table.choseIndex].cells[0].value;
        }
      }
    }
    return true;
  }
  
  //给SELECT赋值
  function setValue(str) {
 	  oldSetValue=str;
    if (str == null || trim(str)=="") return;
    if (objXML.indexOf("code=\""+str) == -1) return;
  	element.value = str;
  	str = objXML.substring(objXML.indexOf("code=\""+str+"\"")+("code=\""+str).length);
		str = str.substring(str.indexOf("value=\"")+"value=\"".length);
		objInput.value = str.substring(0, str.indexOf("\""));
		if (trim(element.code)=="true") {
			objInput.value=element.value+":"+objInput.value;
    }
    changeTitle();
  }


  	jhtc_attr_init(element,"label",null);
  	jhtc_attr_init(element,"load",null);
  	jhtc_attr_init(element,"para","");
  	jhtc_attr_init(element,"findpage",null);
  	jhtc_attr_init(element,"defaultValue",null);
  	jhtc_attr_init(element,"initValue",null);
  	jhtc_attr_init(element,"code","false");
  	jhtc_attr_init(element,"required","false");
  	jhtc_attr_init(element,"checkValue","true");
  	jhtc_attr_init(element,"extent","140");
  	jhtc_attr_init(element,"maxInput","50");
  	jhtc_attr_init(element,"maxViewList","5");
  	jhtc_attr_init(element,"text","");
  	
  	
	element.initListByXML=initListByXML;
	element.setEnabled=setEnabled;
	element.hasResult=hasResult;
	element.setValue=setValue;
	element.check=check;
	element.refresh=refresh;
	element.initBegin=initBegin;
	element.initEnd=initEnd;
	element.jhtcInit=function(){
		element.initBegin();
		element.initEnd();
	};
	return null;
};
jhtc_class_map["commonSelect"]=jhtc_select;