function jhtc_report(win,jhtc_obj){
  var pageWin=win;
  var element=jhtc_obj;
  var baseDivTop=0, baseDivLeft=0, baseDiv, parentObj; // 调整位置
  var addr, submitXml; // 后台交互信息
  var cellInput = document.createElement("<input type='text' class='inputTextTable' maxlength='3500'/>")

  cellInput.onkeyup = cellInput.ondragend = function() {
    if (event.altKey) return true
    if (this.parentNode.innerText != this.value) {
    	if (this.parentNode.childNodes.length>1)
    		this.parentNode.childNodes[0].data = this.value
    	else
      	this.parentNode.insertAdjacentHTML("afterBegin", this.value)
      this.parentNode.value = this.value;
      adjPosition()
    }
  }

  function init() {
    var vXml = new ActiveXObject("Microsoft.XMLDOM");
    vXml.async = false;
    vXml.loadXML("<root/>");
    element.innerHTML = vXml.transformNode(genBaseXslt())
    adjPosition()
    window.attachEvent("onresize", adjPosition);
  }

  function reset(addr) {
    window.xmlhttp.post(addr,
      '<report_code>'+element.report_code+'</report_code>',
      "?isCheck=false")

    // -- TBD 关于错误处理, 如果无记录，如何处理，有记录如何处理
    var source = window.xmlhttp._object.responseText
    if (window.doMsg(source)) {
      refresh(source)
      adjPosition()
    }
  }

  function genTable(rowNum, colNum, headNum) {
    if (headNum>=rowNum && headNum>0) {
      alert('表头行数应小于表格的行数 并且大于0')
      return false;
    }
    rowNum++;
    colNum++;
    headNum++;
    var vXml="<?xml version='1.0' encoding='GBK'?><root><msg/><error/><record><total/></record><activeHead/><thead/><tbody>"
    for (var i=0; i<headNum; i++) {
      for(var j=0;j<colNum;j++){
        s="vertical-align:middle;width:;font-size:;color:;font-family:;background-color:;border-bottom:black 1px solid;text-align:center;border-top:black 1px solid;border-right:black 1px solid;height:22px;border-left:black 1px solid;";
        if(i==0||j==0)
          vXml=vXml+"<tr><td>1</td><td>"+i+"</td><td>"+j+"</td><td>1</td><td>1</td><td></td><td>width:50px;height:22px</td><td>"+colNum+"</td><td>0</td></tr>"
        else
          vXml=vXml+"<tr><td>1</td><td>"+i+"</td><td>"+j+"</td><td>1</td><td>1</td><td></td><td>"+s+"</td><td>"+colNum+"</td><td>0</td></tr>"
      }
    }
    for (var i=headNum; i<rowNum; i++) {
      for(var j=0;j<colNum;j++){
        s="vertical-align:middle;width:;font-size:;color:;font-family:;background-color:;border-bottom:black 1px solid;text-align:center;border-top:black 1px solid;border-right:black 1px solid;height:22px;border-left:black 1px solid;";
        if(j==0)
          vXml=vXml+"<tr><td>0</td><td>"+i+"</td><td>"+j+"</td><td>1</td><td>1</td><td></td><td>width:50px;height:22px</td><td>"+colNum+"</td><td>0</td></tr>"
        else
          vXml=vXml+"<tr><td>0</td><td>"+i+"</td><td>"+j+"</td><td>1</td><td>1</td><td></td><td>"+s+"</td><td>"+colNum+"</td><td>0</td></tr>"
      }
    }

    vXml=vXml+"</tbody></root>"
    refresh(vXml);
  }

  /**
   * 修改表格
   * 功能代码：01 行加， 02 行减， 03列加， 04 列减
   * 位置
   * 数量
   */
  function updateTable() {
    if (arguments.length != 3) {
      alert('修改表格参数不合法')
      return false;
    }
    if (arguments[0]=='01') {

    } else if (arguments[1]=='02') {

    } else if (arguments[2]=='03') {

    } else if (arguments[3]=='04') {

    } else {
      alert('修改表格参数--功能代码不合法')
      return false;
    }
    return true
  }

  function refresh(vStr) {
    var vXml = new ActiveXObject("Microsoft.XMLDOM");
    vXml.async=false;
    vXml.loadXML(vStr);
    element.innerHTML  = vXml.transformNode(genBaseXslt())

    var tbody = element.getElementsByTagName('TBODY')[1]
    for (var i=0; i<tbody.rows.length; i++) {
    	if (i%2 == 0) { // 两色相间
        tbody.rows[i].style.backgroundColor = window.oddColor
      } else {
        tbody.rows[i].style.backgroundColor = window.evenColor
      }
      for (var j=0; j<tbody.rows[i].cells.length; j++) {
        tbody.rows[i].cells[j].onclick = tdActive
      }
    }
    var thead = element.getElementsByTagName('THEAD')[0]
    for (var i=0; i<thead.rows.length; i++) {
      for (var j=0; j<thead.rows[i].cells.length; j++) {
        thead.rows[i].cells[j].onclick = tdActive
      }
    }

    // 6. 调整位置
    adjPosition()

    // 7. 加入键盘响应
    tbody.parentNode.onkeydown = navigateKeys
  }

  // 生成Base Xslt
  function genBaseXslt() {
    // 取得自定义xsl 的head，body
    var vXml = new ActiveXObject("Microsoft.XMLDOM");
    vXml.async=false;
    vXml.load(element.document.URL.replace(/.html.*$/g, ".xsl"));
    var temp = vXml.xml
    if (temp==null || temp.length==0) {
      alert('此文件'+element.document.URL.replace(/.html.*$/g, ".xsl")+'不符合XSL格式')
      return;
    }
    var vCol = temp.substring(temp.search(/<colgroup>/i), temp.search(/<\/colgroup>/i)+"</colgroup>".length)
    var vHead = temp.substring(temp.search(/<thead>/i), temp.search(/<\/thead>/i)+"</thead>".length)
    var vBody = temp.substring(temp.search(/<tbody>/i), temp.search(/<\/tbody>/i)+"</tbody>".length)

    //生成基础xslt
    vXml.load(window.prefix+"/base/xsl/mainTable.xsl");
    var str = vXml.xml
    // 替换表头
    if (vHead!=null && vHead.length>="</thead>".length) {
      str = str.replace(/<thead\/>/, vCol+vHead)
    }
    // 替换体数据格式
    if (vBody!=null && vBody.length>="</tbody>".length) {
      str = str.replace(/<tbody>(.|\n)*<\/tbody>/, vBody)
    }
    vXml.loadXML(str)
    return vXml
  }

  function adjPosition() {
    parentObj = baseDiv = element.document.getElementById('_base');
    baseDivTop = baseDivLeft = 0
    while(parentObj.tagName != "BODY") {
	    baseDivTop += parentObj.offsetTop;
	    baseDivLeft += parentObj.offsetLeft;
	    parentObj = parentObj.offsetParent;
    }

    baseDiv.style.pixelWidth = parentObj.clientWidth - baseDivLeft - 20
    baseDiv.style.pixelHeight = parentObj.clientHeight - baseDivTop - 20
  }

  // 激活td
  function tdActive() {
    var objTD = event.srcElement

  //  var oEvent = createEventObject();
  //  oEvent.result = objTD;
   // evtTDFocus.fire(oEvent);
    __jhtcDispatchEvent(element,"onTDFocus");

    if (objTD.type>=1) {
      cellInput.readOnly='true'
      cellInput.runtimeStyle.zIndex=-1
    } else {
      cellInput.readOnly=false
      cellInput.runtimeStyle.zIndex=1
    }

    var parentObj = objTD
    var tdTop = tdLeft = 0
    while (parentObj.tagName != "DIV") {
	    tdTop += parentObj.offsetTop;
	    tdLeft += parentObj.offsetLeft;
	    parentObj = parentObj.offsetParent;
    }

    if (element.objTD!=null && element.objTD!=objTD) {
      element.objTD.runtimeStyle.borderColor=''
      element.objTD.runtimeStyle.borderWidth=''
      element.objTD.runtimeStyle.backgroundColor=''
    }
    if (element.objTD!=null && element.objTD.parentNode!=null && element.objTD.parentNode!=objTD.parentNode) {
      element.objTD.parentNode.runtimeStyle.backgroundColor = ''
    }
    var t_row=objTD.parentNode.rowIndex,t_col=objTD.cellIndex;
    if(t_row>0&&t_col>0){
      objTD.parentNode.runtimeStyle.backgroundColor = window.choseColor;
      objTD.runtimeStyle.borderColor='#A3C9F0';
      objTD.runtimeStyle.borderWidth=2;
      objTD.runtimeStyle.backgroundColor='white';
      
      element.objTD = objTD
  
      cellInput.style.display = ''
      if (objTD.contains(cellInput)) {
        return;
      }
      
      cellInput.value = objTD.innerText
  
      objTD.appendChild(cellInput);
      //  补充样式
      cellInput.runtimeStyle.textAlign=objTD.style.textAlign
      cellInput.style.top = tdTop + (objTD.parentNode.rowIndex>0?1:0)
      cellInput.style.left = tdLeft + (objTD.cellIndex>0?1:0) // 第一个cell不需要偏移1px
      cellInput.style.width = objTD.clientWidth
      cellInput.style.height = objTD.clientHeight
      cellInput.focus()
  
      var range = cellInput.createTextRange()
      range.collapse(true);
  		range.moveStart('character',-1);
      range.select();
      ID_col.innerHTML = objTD.cellIndex+1;
      ID_row.innerHTML = objTD.parentNode.rowIndex+1;

    }else if((t_row==0||t_col==0)&&t_row!=t_col){
      cellInput.style["display"]="none";
      objTD.appendChild(cellInput);
    }else{
      cellInput.style["display"]="none";
    }
  }

  //得到光标的位置
  function getCursorPosition(obj) {
    if(typeof(obj.value)=="undefined")
      return 0;
    var qswh="^#@%166!^"
    if (obj.maxLength!=null && obj.maxLength<2000) {
      obj.maxLength = obj.maxLength+qswh.length
    }
    obj.focus();
    var rng=document.selection.createRange();
    rng.text=qswh;
    var nPosition=obj.value.indexOf(qswh)
    rng.moveStart("character", -qswh.length)
    rng.text="";
    if (obj.maxLength!=null && obj.maxLength<2000+qswh.length) {
      obj.maxLength = obj.maxLength-qswh.length
    }
    return nPosition;
  }

  // 键盘响应
  function navigateKeys() {
    if(typeof(event.srcElement.value)=="undefined")
      return false;
    var parentObj = event.srcElement
    while (parentObj.tagName!='TABLE') {
      if (parentObj.tagName=='TD' || parentObj.tagName=='TH') break;
      parentObj = parentObj.parentNode
    }
    var objTD = parentObj;
    var nCell=0; // 不使用objTD.cellIndex, 因为隐藏列队此值有影响
    for (; nCell<objTD.parentNode.childNodes.length-1; nCell++) {
      if (objTD.parentNode.childNodes[nCell]==objTD) break;
    }

    while (parentObj.tagName!='BODY') {
      if (parentObj.tagName=='TR') break;
      parentObj = parentObj.parentNode
    }
    var objTR = parentObj;
    var nRow=parentObj.rowIndex;

    var objTable=objTR.parentNode.parentNode
    var objTBODY=objTR.parentNode;
    var objTHEAD=objTBODY.parentNode.getElementsByTagName('thead')[0]

		var nKeyCode=event.keyCode;
    do {
      switch(nKeyCode){
        case 37://<-
          if (getCursorPosition(event.srcElement)> 0)
            return true;
          nCell--;
          break;
        case 38://^
          nRow--;
          break;
        case 39://->
          if (getCursorPosition(event.srcElement)< event.srcElement.value.length  && !cellInput.readOnly)
            return true;
          nCell++;
          break;
        case 40://\|/
          nRow++;
          break;
        case 9:// tab
          if (event.shiftKey) {
            nCell--;
            break;
          }
        case 13://Enter
          nCell++;
          break;
        default:
          return true;
      }

      if (nCell==-1){
        nRow--;//跳转到上一行
        nCell=objTR.cells.length-1;//最后一列
      }
      if (nCell==objTR.cells.length) {
        nRow++;//跳转到下一行首位置
        nCell=0;//第一列
      }
      if (nRow==-1)
        nRow = objTable.rows.length - 1
      if (nRow==objTable.rows.length) {
        nRow = 0
      }
    } while (objTable.rows[nRow].cells[nCell].style.display=='none');

    if (nRow < objTR.rowIndex) {
      if (objTR.previousSibling!=null && objTR.previousSibling.offsetTop <= baseDiv.scrollTop) {
        baseDiv.scrollTop = objTR.previousSibling.offsetTop-1
      } // 向上调整滚动条
    } else if (nRow > objTR.rowIndex) {
      if (objTR.nextSibling!=null && objTR.nextSibling.nextSibling!=null &&
        objTR.nextSibling.nextSibling.offsetTop >= (baseDiv.scrollTop+baseDiv.clientHeight)) {
        baseDiv.scrollTop = objTR.nextSibling.nextSibling.offsetTop-baseDiv.clientHeight
      } // 向下调整滚动条
    }

    if (nCell < objTD.cellIndex) {
      if (objTD.previousSibling!=null && objTD.previousSibling.offsetLeft <= baseDiv.scrollLeft) {
        baseDiv.scrollLeft = objTD.previousSibling.offsetLeft - 1
      } // 向左调整滚动条
    } else if (nCell > objTD.cellIndex) {
      if (objTD.nextSibling!=null && objTD.nextSibling.nextSibling!=null &&
        objTD.nextSibling.nextSibling.offsetLeft >= (baseDiv.scrollLeft+baseDiv.clientWidth)) {
        baseDiv.scrollLeft = objTD.nextSibling.nextSibling.offsetLeft-baseDiv.clientWidth
      } // 向右调整滚动条
    }

    if (nRow==0) { // 最上
      baseDiv.scrollTop = 0
    } else if (nRow==objTable.rows.length-1) { // 最下
      baseDiv.scrollTop = baseDiv.scrollHeight
    }

    if (nCell==0) { // 最左
      baseDiv.scrollLeft = 0
    } else if (nCell==objTR.cells.length-1) { // 最右
      baseDiv.scrollLeft = baseDiv.scrollWidth
    }

    objTable.rows[nRow].cells[nCell].click()
    return false;
  }

  function submit(btn) {
    if (window.trim(btn.name)=='') {
      alert('请设置name')
      return false;
    }

    var mainStr="report_code='"+element.report_code+"' "
    var theads=baseDiv.getElementsByTagName('thead')
    if (theads.length>0) {
      mainStr+="head_num='"+theads[0].rows.length+"' "
    }

    var trs=baseDiv.getElementsByTagName('tr')
    mainStr+="report_row='"
    for (var i=0; i<trs.length; i++) {
      if (i==0)
        mainStr+=trs[i].style.height
      else
        mainStr+=(';'+trs[i].style.height)
    }
    mainStr+="' "

    var cols=baseDiv.getElementsByTagName('col')
    mainStr+="report_col='"
    for (var i=0; i<cols.length; i++) {
      if (i==0)
        mainStr+=cols[i].style.width
      else
        mainStr+=(';'+cols[i].style.width)
    }
    mainStr+="' "

    var submitStr = assemble()
    if (trim(submitStr)=='') {
      alert('数据没有发生修改')
      return true;
    }

    var http=new ActiveXObject("Microsoft.XMLHTTP")
    http.open("POST", btn.name+".hbviewhigh", false);
    try {
      http.send("<root "+mainStr+">"+submitStr+"</root>");
    } catch (exception){
      alert(exception)
    }

    var str = http.responseText
    if (!window.doMsg(str)) {
      return true;
    }
  }

	function assemble() { // -- TBD待处理，给此行加个标记，并检查此行
    var result = "";
    var table = baseDiv.getElementsByTagName('table')[0]
    for (var i=0; i<table.rows.length; i++) {
      for (var j=0; j<table.rows[i].cells.length; j++) {
        result = result + "<record><report_code>"+element.report_code+"</report_code>"
        result = result +'<row>'+(i+1)+'</row><col>'+(j+1)+'</col><formula>'+trim(table.rows[i].cells[j].value)+'</formula>'
            +'<type>'+(trim(table.rows[i].cells[j].type)==''?'0':trim(table.rows[i].cells[j].type))+'</type>'
            +'<rowspan>'+(trim(table.rows[i].cells[j].rowSpan)==''?'1':trim(table.rows[i].cells[j].rowSpan))+'</rowspan>'
            +'<colspan>'+(trim(table.rows[i].cells[j].colSpan)==''?'1':trim(table.rows[i].cells[j].colSpan))+'</colspan>'
            +'<style>'+getStyleStr(i,j,table.rows[i].cells[j].style)+'</style>';//+'<style>text-align:'+trim(table.rows[i].cells[j].style.textAlign)+';</style>'
        result = result + "</record>"
      }
    }
    return result;
  }
  var style_to_style={"borderTop":"border-top",
                  "borderBottom":"border-bottom",
                  "borderLeft":"border-left",
                  "borderRight":"border-right",
                  "width":"width",
                  "height":"height",
                  "fontFamily":"font-family",
                  "fontSize":"font-size",
                  "textAlign":"text-align",
                  "verticalAlign":"vertical-align",
                  "color":"color",
                  "backgroundColor":"background-color",
                  "display":"display",
                  "textFormatF":"textFormatF"
                  };
  function getStyleStr(r,c,s){
    var sty="";
    for(var p in s){
      if(p=="borderTop"
        ||p=="borderBottom"
        ||p=="borderLeft"
        ||p=="borderRight"
        ||p=="width"
        ||p=="height"
        ||p=="fontFamily"
        ||p=="fontSize"
        ||p=="textAlign"
        ||p=="verticalAlign"
        ||p=="color"
        ||p=="backgroundColor"
        ||p=="display"
        ||p=="textFormatF"
        )
          if(r!=0&&c!=0&&p!="width"&&p!="height")
            sty+=style_to_style[p]+":"+s[p]+";";
          if(r==0&&c!=0&&p=="width")
            sty=style_to_style[p]+":"+(s[p]==""?"50":s[p])+";";
          if(r!=0&&c==0&&p=="height")
            sty=style_to_style[p]+":"+(s[p]==""?"22":s[p])+";";
      }
    return sty;
  }
  function define() {
    if (cellInput==null) return;
    var objTD = cellInput.parentNode
    if (objTD.tagName=='TH') {
      //alert('表头单元不能被定义')
      //return false;
    }

    openDialog('defRela.html', 'dialogWidth:720px;dialogHeight:600px')
    if (objTD.type>=1) {
      if (objTD.childNodes.length>1)
    		//objTD.childNodes[0].data = '定义';
    		objTD.innerHTML="定义";
    	else
      	objTD.insertAdjacentHTML("afterBegin", '定义')

      objTD.runtimeStyle.textAlign='center'
      objTD.click()

      cellInput.readOnly=true
      cellInput.focus()
      cellInput.value='定义'
      cellInput.runtimeStyle.zIndex=-1
    } else {
      cellInput.focus();
    }
  }

  function undef() {
    if (cellInput==null) return;
    var objTD = cellInput.parentNode
    if (objTD.type==0) {
      alert('此单元不是定义单元，不能取消')
      return false;
    }

    if (objTD.childNodes.length>1)
  		objTD.childNodes[0].data = ''
  	else
    	objTD.insertAdjacentHTML("afterBegin", '')

    objTD.type=''
    objTD.value=''
    objTD.runtimeStyle.textAlign=''
    objTD.click()

    cellInput.readOnly=false
    cellInput.focus()
    cellInput.value=''
    cellInput.runtimeStyle.zIndex=1
  }

  function merge(rowSpan, colSpan) {
    // 先撤销，再生成
    renew()

    var objTD = cellInput.parentNode
    var objTable=objTD.parentNode.parentNode.parentNode
    

    // 取得行坐标
    var iRow=0;
    for (; iRow<objTable.rows.length-1; iRow++) {
      if (objTD.parentNode==objTable.rows[iRow]) break;
    }

    // 取得列坐标
    var iCol=0;
    for (; iCol<objTable.rows[iRow].cells.length-1; iCol++) {
      if (objTD==objTable.rows[iRow].cells[iCol]) break;
    }

    for(var i=iRow+1;i<iRow+rowSpan;i++){
      if(objTable.rows[i].cells[iCol].tagName!=objTD.tagName){
        alert("表头不能和表体合并!");
        return ;
      }
    }
    
    
    objTD.colSpan = colSpan
    objTD.rowSpan = rowSpan
    
    for (var i=iRow; i<iRow+rowSpan; i++) {
      for (var j=iCol; j<iCol+colSpan; j++) {
        if (i==iRow && j==iCol) continue;
        objTable.rows[i].cells[j].type='0';
        objTable.rows[i].cells[j].value='';
        objTable.rows[i].cells[j].style.display='none'
      }
    }

    objTD.click()
    return true;
  }

  function renew() {
    var objTD = cellInput.parentNode

    var colSpan=1, rowSpan=1
    if (trim(objTD.colSpan)!='' && eval(objTD.colSpan)>1) {
      colSpan=eval(objTD.colSpan)
    }
    if (trim(objTD.rowSpan)!='' && eval(objTD.rowSpan)>1)
      rowSpan=eval(objTD.rowSpan)

    objTD.rowSpan=1;objTD.colSpan=1

    var objTable=objTD.parentNode.parentNode.parentNode
    for (var i=objTD.parentNode.rowIndex; i<objTD.parentNode.rowIndex+rowSpan; i++) {
      for (var j=objTD.cellIndex; j<objTD.cellIndex+colSpan; j++) {
        if (i==objTD.parentNode.rowIndex && j==objTD.cellIndex) continue;
        objTable.rows[i].cells[j].type=''
        objTable.rows[i].cells[j].style.display=''
      }
    }
    objTD.click()
  }

  function insertRow(idx) {
    idx++;
    var objTable = element.getElementsByTagName("table")[1]
    
    if(idx<=objTable.rows.length){
      var oldInd=idx-1;
      for (var i=0; i<objTable.rows[oldInd].cells.length; i++) {
        if(objTable.rows[oldInd].cells[i].style["display"]=="none"){
          for(var j=oldInd;j>0;j--){
            if(objTable.rows[j].cells[i].rowSpan>1){
              objTable.rows[j].cells[i].rowSpan+=1;
              break;
            }
          }
        }
      }
    }

    var cols = objTable.getElementsByTagName("col")
    var row, refRow;
    if (idx>objTable.rows.length) {
      row = objTable.insertRow()
      refRow = objTable.rows[objTable.rows.length-2]
    } else {
      row = objTable.insertRow(idx-1)
      refRow = objTable.rows[idx]
    }
    row.className = refRow.className
    row.style.height = refRow.style.height
    
    var cell;
    for (var i=0; i<refRow.cells.length; i++) {
      var cells = row.appendChild(refRow.cells[i].cloneNode(false))
      cells.height="24"
      cells.innerHTML = ""
      cells.type='0'
      cells.value=''
      cells.onclick=tdActive
      cells.rowSpan="1";
      if(idx>=objTable.rows.length)
        cells.style["display"]="";
    }
    updateIndex();
    return true;
  }

  function delRow(idx) {
    idx++;
    var objTable = element.getElementsByTagName("table")[1]
    if(objTable.rows.length<3){
      alert("最少要有一行");
      return ;
    }  
    if (idx<1 || idx>objTable.rows.length) {
      alert('请输入合法值')
      return false;
    }
    
    var oldInd=idx-1;
    for (var i=0; i<objTable.rows[oldInd].cells.length; i++) {
      if(objTable.rows[oldInd].cells[i].style["display"]=="none"){
        for(var j=oldInd;j>0;j--){
          if(objTable.rows[j].cells[i].rowSpan>1){
            objTable.rows[j].cells[i].rowSpan-=1;
            break;
          }
        }
      }
      if(objTable.rows[oldInd].cells[i].rowSpan>1){
        for(var m=++oldInd;m<objTable.rows.length;m++){
          if(objTable.rows[m].cells[i].style["display"]=="none"){
            objTable.rows[m].cells[i].style["display"]="";
          }else
            break;
        }
      }
    }
    
    objTable.deleteRow(idx-1);
    updateIndex();
    return true;
  }

  function insertCol(idx) {
    idx++;
    var objTable = element.getElementsByTagName("table")[1]
    var cols = objTable.getElementsByTagName("col")
    var colgroup = objTable.getElementsByTagName("colgroup")[0]
    if(idx-1>objTable.rows[0].cells.length)
      idx=objTable.rows[0].cells.length;
    if(idx<objTable.rows[0].cells.length){
      var oldInd=idx-1;
      for (var i=0; i<objTable.rows.length; i++) {
        if(objTable.rows[i].cells[oldInd].style["display"]=="none"){
          for(var j=idx;j>0;j--){
            if(objTable.rows[i].cells[j].colSpan>1){
              objTable.rows[i].cells[j].colSpan+=1;
              break;
            }
          }
        }
      }
    }
    
    var temp = cols.length
    var hsCol=false;
    for (var i=0; i<objTable.rows.length; i++) {
      
      if (idx>temp) {
        if(hsCol==false){
          colgroup.appendChild(cols[0].cloneNode(false))
          hsCol=true;
        }
        var cell = objTable.rows[i].appendChild(objTable.rows[i].cells[1].cloneNode(false))
        cell.type=0
        cell.value=''
        cell.onclick = tdActive
        cell.style["width"]=100;
        cell.colSpan="1";
      } else {
        if(hsCol==false){
          colgroup.insertBefore(cols[0].cloneNode(false), cols[0])
          hsCol=true;
        }
        var cell = objTable.rows[i].insertBefore(objTable.rows[i].cells[idx-1].cloneNode(false), objTable.rows[i].cells[idx-1])
        cell.type=0
        cell.value=''
        cell.onclick = tdActive
        cell.style["width"]=100;
        cell.colSpan="1";
      }
      if(idx>=cols.length)
        cell.style["display"]="";
    }
    updateIndex();
    return true
  }

  function delCol(idx) {
    var objTable = element.getElementsByTagName("table")[1]
    var cols = objTable.getElementsByTagName("col")  
    if(objTable.rows[0].cells.length<3){
      alert("最少要有一列");
      return ;
    }  
    if (idx<1 || idx>objTable.rows[0].cells.length-1) {
      alert('请输入合法值')
      return false;
    }
    
    var oldInd=idx;
    for (var i=0; i<objTable.rows.length; i++) {
      if(objTable.rows[i].cells[oldInd].style["display"]=="none"){
        for(var j=idx;j>0;j--){
          if(objTable.rows[i].cells[j].colSpan>1){
            objTable.rows[i].cells[j].colSpan-=1;
            break;
          }
        }
      }
      if(objTable.rows[i].cells[oldInd].colSpan>1){
        for(var m=++oldInd;m<objTable.rows[i].cells.length;m++){
          if(objTable.rows[i].cells[m].style["display"]=="none"){
            objTable.rows[i].cells[m].style["display"]="";
          }else
            break;
        }
      }
    }

    var colgroup = objTable.getElementsByTagName("colgroup")[0]
    colgroup.removeChild(cols[idx-1])
    for (var i=0; i<objTable.rows.length; i++) {
      objTable.rows[i].removeChild(objTable.rows[i].cells[idx]);
    }
    updateIndex();
    return true;
  }
  function updateIndex(){
    var objTable = element.getElementsByTagName("table")[1];
    var rows = objTable.getElementsByTagName("tr");
    var cols ;
    var ind;
    if(rows.length>0){
      for(var i=0;i<rows.length;i++){
        cols=rows[i].cells;
        for(var j=0;j<cols.length;j++){
          if((i==0||j==0)&&i!=j){
            if(typeof(cols[j].value)=="undefined")
              cols[j].value="";
            if(i==0)
              ind=j;
            else
              ind=i;
            if(cols[j].value!=""||cols[j].value.indexOf("|+|")>0)
              ind=ind+"　"+cols[j].value.split("|+|")[1];
            else
              ind=ind;
            cols[j].innerText=ind;
          }
        }
      }
    }
  }
  	
	element.reset=reset;
	element.genTable=genTable;
	element.updateTable=updateTable;
	element.submit=submit;
	element.define=define;
	element.undef=undef;
	element.merge=merge;
	element.renew=renew;
	element.insertRow=insertRow;
	element.delRow=delRow;
	element.insertCol=insertCol;
	element.delCol=delCol;
	element.init=init;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_class_map["reportCtn"]=jhtc_report;


