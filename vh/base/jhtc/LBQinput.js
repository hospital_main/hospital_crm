//////////////////////////////////////////////////
//Title: 快速录入组件(全匹配)
/**
 * 本组件为快速录入字典内容而开发，可接受XML文件或XML DOM对象作为数据源
 * 通过设置XML数据源中相应节点属性作为查询关键字来对XML数据源中对应节点的数据进行查询
 * 得到快速选择下拉框中的选择内容
 */
function jhtc_LBQinput(win,jhtc_obj){
	var pageWin=win;
	var element=jhtc_obj;
    var objXMLDoc; //XMLDOM
    var oCurNodesList;//当前操作的r序列对象
    var oCurR;//当前选中内容所在的r行对象
    var listInnerHTML;//下拉框内html        

    var objInput; // input对象
    var oCaption = null;//QInput标题对象
    var vReadOnly = false;//readOnly属性,默认为false

    var vCode,vText;//XML接点属性名
    var vTop = 0,vLeft = 0,vWidth = 80,vHeight = 20;
    var iCode,iText;//QI的value和text

    var Lheight = 5;//select的高度 默认5
    var Lwidth = element.width;//select的宽度
    var vCaption = "";//QInput的标题
    var objList;//select对象
    var dicListAppearFlag = false;//快速选择下拉框是否出现标志
    var firstSelectedFlag = true;//第一次选中下拉框标志
    var boolKeyUpDownBlur = false;
    var vKeyCode = "";//当前用户输入的关键字
    var strXSLT;
    var boolArrowDownOnInput = false;

    function findNode(strText,allFlag){//返回属性值包含strText(从左边第一个字符开始)的r行集合

      var objRNode;
      var objNodeList;
      var strAttName;   
      var tmpLength;   
      
      objRNode = objXMLDoc.selectSingleNode("/*/r");      
      if(objRNode == null) return;
      if(typeof(allFlag)=="undefined") allFlag = false;
      tmpLength = objRNode.attributes.length;          
      
      for(var i = 0; i < tmpLength; i++){//循环r行的所有属性
        if(allFlag){//完全匹配
          objNodeList = objXMLDoc.selectNodes("/*/r[@" + objRNode.attributes(i).name + "='" + strText + "']");
          if(objNodeList && objNodeList.length > 0){
						strXSLT = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="html"/><xsl:template match="/"><xsl:for-each select="/*/*"><xsl:choose><xsl:when test="@' + objRNode.attributes(i).name + ' = \"' + strText + '\""><option><xsl:attribute name="value"><xsl:value-of select="@' + vCode + '"/></xsl:attribute><xsl:value-of select="@' + vText + '"/></option></xsl:when></xsl:choose></xsl:for-each></xsl:template></xsl:stylesheet>';
						break;
          }
        }else{//左包含        
          objNodeList = objXMLDoc.selectNodes("/*/r[contains(@" + objRNode.attributes(i).name + ",'" +  strText + "')]");
          
          
          if(objNodeList && objNodeList.length > 0){
						strXSLT = "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:output method='html'/><xsl:template match='/'><xsl:for-each select='/*/*'><xsl:choose><xsl:when test='contains(@" + objRNode.attributes(i).name + ",\"" +  strText + "\")'><option><xsl:attribute name='value'><xsl:value-of select='@" + vCode + "'/></xsl:attribute><xsl:value-of select='@" + vText + "'/></option></xsl:when></xsl:choose></xsl:for-each></xsl:template></xsl:stylesheet>";
						break;
          }          
        }        
      }
      
      if(!strText || strText.length == 0){
				strXSLT = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="html"/><xsl:template match="/"><xsl:for-each select="/*/*"><option><xsl:attribute name="value"><xsl:value-of select="@' + vCode + '"/></xsl:attribute><xsl:value-of select="@' + vText + '"/></option></xsl:for-each></xsl:template></xsl:stylesheet>';
      }
      
      if(!objNodeList || objNodeList.length == 0){
				strXSLT = "";
				oCurNodesList = null;
				oCurR = null;
				return(-1);
      }else{
				oCurNodesList = objNodeList;//保存当前记录集合
				return(0);
      }
    }

  ////////////////////////////////////////////////////
    
    function onObjInputLostFocus(){//当objInput失去焦点时触发该方法    
			var intFindResult;
			
			if(boolArrowDownOnInput) return;
      if(dicListAppearFlag){//下拉框可见        
        //debugger;
        if(element.contains(window.document.activeElement)) return;
        
        if(event.srcElement.tagName!="INPUT"){
          hideList();
        }
        if(objInput.value){//
          intFindResult = findNode(objInput.value,true);        
          if(intFindResult == -1){
            
            if(dicListAppearFlag){             
              hideList();
            }
            
            alert("输入内容不正确");
            objInput.value = "";
            objInput.focus();
          }else{//
            if(objList){
              objList.focus();
            }
          }
        }
      }else{//下拉框不可见        
        if(objInput.value){
					
					//debugger;
          intFindResult = findNode(objInput.value,true);
          if(intFindResult == -1){
            alert("输入内容不正确");
            objInput.select();
          }
        }
      }
    }
    
    function hideList(){//使下拉框不可见      
      oList.style.visibility = "hidden";
      dicListAppearFlag = false;  
      boolArrowDownOnInput = false;         
    }

    function ifShowList(){//当在objInput上面按键时决定是否出现下拉框
			var strInnerHTML;
			
      vKeyCode = objInput.value;//保存用户当前的录入查询关键字
			////////hzh 2004-04-22/////////
			//evtonItemSelected.fire();
			__jhtcDispatchEvent(element,"onItemSelected");
			//////////////////////
			if(event.keyCode=="27"){//按下了Esc键
			  objInput.value = "";
			  iText = "";
        oCurR = null;
			}
      if(vKeyCode=="" && (event.keyCode=="8" || event.keyCode=="46")){//用户按下backspace键或del键并且input内容为空
        if(dicListAppearFlag) hideList();
        ////hzh 2004-04-22 add//////////////
        iText = "";
        oCurR = null;
        //////////////////////
        return;
      }

      if(event.keyCode != "13"){//根据输入关键字进行查询      
					if(objInput.value != ""){//输入非空
						findNode(objInput.value);          
					}else{
						return(-1);
					}
          
          if(oCurNodesList && oCurNodesList.length == 0){
            hideList();
            return;
          }         
					
					strInnerHTML = getList(oCurNodesList);                    
					if(strInnerHTML.toString() == "-1"){
					
					  hideList();
						if(element.contains(window.document.activeElement) || element == window.document.activeElement){
						}else{
							alert("输入内容不正确");
              objInput.focus();
						}
						
					}else{
						oList.innerHTML = getList(oCurNodesList);                    
						objList = oList.childNodes(0);//objList为下拉框中的select          
						objList.onkeydown = onKeyPressSelect;//处理按键事件      
						objList.onclick = clickList;//处理select的鼠标单击事件        
						objList.onblur = onListLostFocus;//当select失去焦点的时候触发该事件  
						oList.style.visibility = "visible";          
						showList();
					}                
      }     
    }
    
    function onKeyPressSelect(){//处理select的按键事件   
      event.cancelBubble = true;   
      if(event.keyCode == "8" || event.keyCode =="27"){//如果按下了backspace键
         objInput.focus();
         return;
      }      
      saveSelectValue();//保存选择      
    }
    
    function onClickSelect(){//处理select的鼠标单击事件
      saveSelectValue();//保存选择
    }
    
    function onListLostFocus(){//处理select失去焦点    
      var intReturn;
      /////hzh 2004-04-23////////////////////////
      if(objInput.value){//输入框内容非空
        intReturn = findNode(objInput.value,true);
        if(intReturn == -1){//没找到任何记录
          objInput.value = "";
          objInput.focus();
        }else if(intReturn == 0){//找到一条记录
          getRByIndexInCurNodesList(0);
          if(dicListAppearFlag)
            hideList();
            return;
        }
      }else{//objInput内容为空
        if(dicListAppearFlag)
            hideList();
      }
    } 
    
   
  
    function checkNodeByInputValue(){//通过input内输入的内容判断是否能在字典XML数据集中找到记录
    
    }
    

    function saveSelectValue(){//保存当前选中的值

      if(dicListAppearFlag){      
        try{        
          if(objList.selectedIndex!=-1){ 
            if(event.keyCode=="40"){
              findNode(vKeyCode);
              getRByIndexInCurNodesList(objList.selectedIndex);
              objInput.value = objList.options[objList.selectedIndex+1].text; 
              event.keyCode = 0;  
            }//End of if
            
            if(event.keyCode=="38"){
              findNode(vKeyCode);
              getRByIndexInCurNodesList(objList.selectedIndex);
  	          objInput.value = objList.options[objList.selectedIndex-1].text;    	          
  	          
  	          event.keyCode = 0;
  	        }//End of if
          }//End of if
          
        }catch(e){
          //错误描述: select越界 处理方式：丢弃
        }//End of try
        
        ////////////////////////////////////////////////////////////
        if(event.keyCode=="13"){
          try{            
            iCode = objList.options[objList.selectedIndex].value;
            iText = objList.options[objList.selectedIndex].text; 
            objInput.value = iText;
            findNode(vKeyCode);
            getRByIndexInCurNodesList(objList.selectedIndex);  
            /////////hzh 2004-04-22///////
            //evtonItemSelected.fire();
            __jhtcDispatchEvent(element,"onItemSelected");
            ////////////////
            hideList();
          }catch(e){
            //错误丢弃
          }//End of try
          event.keyCode = 0;
        }//End of if
        /////////////////////////////////////////////////////////////////
      }
    }
    
  function showList(){ //使下拉框出现
    
    if(!objList) return(-1);
    
    objList.runtimeStyle.width = element.Lwidth;//设置下拉框宽度
    //if(objInput.value=="") return;//输入框为空,返回
    
    var aimKey = objInput.value;//查询关键字
    locationQInput();//定位oList
    oList.style.visibility = "visible";
    dicListAppearFlag = true;
  }  

  function onInputKeyDown(){//处理objInput的onkeydown事件
    event.cancelBubble = true;  
    if(event.keyCode=="40"){//下光标键
      boolArrowDownOnInput = true;
      ///hzh 2004-04-18 add///////////////
      if(objInput.value == ""){//objInput为空则列出所有条目
      ///add end/////////////////
        oCurNodesList = objXMLDoc.selectNodes("/*/r");
        strXSLT = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="html"/><xsl:template match="/"><xsl:for-each select="/*/*"><option><xsl:attribute name="value"><xsl:value-of select="@' + vCode + '"/></xsl:attribute><xsl:value-of select="@' + vText + '"/></option></xsl:for-each></xsl:template></xsl:stylesheet>';
      //////////////////////////////////
      }else{//objInput不为空则按当前key进行查询
        findNode(objInput.value);
      }
      /////////////////////////////////
      if(!oCurNodesList){
        hideList();
        return;
      }
      //debugger;
      if(oCurNodesList && oCurNodesList.length == 0) return;
      oList.innerHTML = getList(oCurNodesList);   
      /////////////////////////////////
      objList = oList.childNodes(0);//objList为下拉框中的select          
			objList.onkeydown = onKeyPressSelect;//处理按键事件      
			objList.onclick = clickList;//处理select的鼠标单击事件        
			objList.onblur = onListLostFocus;//当select失去焦点的时候触发该事件  
			oList.style.visibility = "visible";    
			////////////////////
      showList();
      boolKeyUpDownBlur = true;    
      if(dicListAppearFlag) 
        if(objList){
          objList.focus();    
        //////////////////////////////////////////////
        //////////////////////////////////////////////
          if(firstSelectedFlag){//首次选中                    
              iCode = objList.options[0].value;
              iText = objList.options[0].text; 
              getRByIndexInCurNodesList(0);
              objInput.value = iText;
              firstSelectedFlag = false;
          }else{//非首次选中
              saveSelectValue();
          } 
       }
      ////////////////////////////// 
        ///////////////////////////////  
      boolKeyUpDownBlur = false;
      event.keyCode = 0;            
    }//End of if keyCode
    
    if(event.keyCode == "8"){//如果按下的是backspace键
    
			if(objInput.value == ""){
				hideList();
			}      
    }
  
  }

  function secContent(){//响应onkeydown事件
    ////////////////////////////////////////
    if(event.keyCode == "27"){//ESC
      if(dicListAppearFlag){//下拉框显示 
        hideList();
      }
    }
    ////////////////////////////////////////
    
    if(event.keyCode=="40" || event.keyCode=="38"){//上下光标键        
      if(dicListAppearFlag){//下拉框显示 
        boolKeyUpDownBlur = true;    
        if(objList) objList.focus();
        boolKeyUpDownBlur = false;       
      }
    }//End of if keyCode
    
    if(event.keyCode=="13" && objInput.value != ""){//回车键并且输入内容不为空     
      if(dicListAppearFlag){
        if(objList && objList.selectedIndex != -1){
          iCode = objList.options[objList.selectedIndex].value;
          iText = objList.options[objList.selectedIndex].text; 
          objInput.value = iText;            
          objInput.focus();
          getRByIndexInCurNodesList(objList.selectedIndex);
          hideList(); //回车键(13)隐藏下拉框            
        }
      }           
    }//End of if keyCode
}//End of secContent
  
  function getRByIndexInCurNodesList(index){//从oCurNodesList中返回索引为index的r行对象
    oCurR = oCurNodesList[index];        
  }

  function getAttribute(attName){//获得当前选中内容所在r行指定属性名的属性值
    if(typeof(attName)!="undefined" && oCurR){
      return oCurR.getAttribute(attName);
    }else{
      return "";
    }           
  }

  function getList(objRNodeList){//根据传入的NodeList对象装配一个select返回
  
      var tmpStr;
      tmpStr = "<select class='phms_qinput_select' size='" + element.Lheight + "'>";
      
      if(typeof(objRNodeList) == "undefined" || objRNodeList==null) return(-1);
      if(!strXSLT) return(-1);
      
      tmpStr = "<select class='phms_qinput_select' size='" + element.Lheight + "'>" + __objGlobalCommonInst.transformDicXML(objXMLDoc,strXSLT) + "</select>";       
      //tmpStr = tmpStr.replace(/[^\S]{0,}[\s]{0,}[^\S]{0,}/g,"").replace(/optionvalue/g,"option value").replace(/selected/g," selected").replace(/selectclass/g,"select class").replace(/size/g," size");
      //tmpStr = tmpStr.replace(/[\s]{4,4}/g, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").replace(/[\s]{3,3}/g, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").replace(/[\s]{2,2}/g, "&nbsp;&nbsp;&nbsp;&nbsp;");
      return tmpStr;
  }


  function locationQInput(){ //处理QInput的定位

    var maxRight;
    var maxHeight;
    
    try{

      //上下反转
      maxHeight = element.parentElement.clientHeight - element.offsetTop - element.offsetHeight - objList.offsetTop;
      if(objList.clientHeight>maxHeight){
        if(element.offsetTop >= objList.clientHeight){
          oList.style.top = - objList.offsetHeight;
        }
      }else{//恢复默认
        oList.style.top = objInput.offsetTop + vHeight - 1;
      }

      //左右反转
      maxRight = element.parentElement.clientWidth - element.offsetLeft - element.offsetWidth - objList.offsetLeft;
      if(objList.offsetWidth >maxRight){
        oList.style.left = objInput.offsetLeft + objInput.offsetWidth - objList.offsetWidth ;
      }else{
        oList.style.left = objInput.offsetLeft;
      }
      
    }catch(e){
      //丢弃错误
    }
  }


    function inputXML(argSource){ //objXMLDoc接收XML文件数据  
	initGlobalCommon();  
      return __objGlobalCommonInst.inputXML(argSource);       
    }  

  function refresh(){//定义QInput外框样式   
      
    with(element){//element的位置、样式
      style.top = vTop;
      style.left = vLeft;
      style.pixelHeight = vHeight;
      style.pixelWidth = vWidth;      
    }
    
    with(oDiv){//oDiv的位置、样式
      style.pixelHeight = parseInt(vHeight) - 2;
      style.pixelWidth =  vWidth;             
      //onmousedown = lostFocus;     
    }
    
    with(objInput){//QInput的位置、样式
      style.left = oCaption.offsetWidth;
      style.pixelHeight = parseInt(vHeight) - 2;
      style.pixelWidth = parseInt(vWidth) - oCaption.offsetWidth;          
      onblur = onObjInputLostFocus;
      onkeydown = onInputKeyDown;//仅处理下光标键
      onkeyup = ifShowList;
      
      ////////////////////////////////
      if(vReadOnly.toString().toLowerCase() == "true"){//
        disabled = true;
        objInput.style.borderColor = "#cccccc";
      }else{
        disabled = false;
        objInput.style.borderColor = "#7B9EBD";
      }
    }
    
    with(oList){//下拉框的位置、样式
      style.left = objInput.offsetLeft;
      style.top = objInput.offsetTop + vHeight;
      style.pixelWidth = objInput.offsetWidth;
      style.visibility = "hidden";
    }
  }
  
  function clickList(){//鼠标单击下拉框事件处理
    try{    
        //debugger;
        iCode = objList.options[objList.selectedIndex].value;
        iText = objList.options[objList.selectedIndex].text;   
        
        getRByIndexInCurNodesList(objList.selectedIndex);   
        objInput.value = iText;       
        //alert(iText);
        ///hzh 2004-04-22////////
        //evtonItemSelected.fire();
        __jhtcDispatchEvent(element,"onItemSelected");
        ///////////////
        vKeyCode = objInput.value;
        hideList();
      }catch(e){
        //alert("clickList错误丢弃");
      }//End of try
  }
  
  function initGlobalCommon(){//初始化__objGlobalCommonInst
  
    var oWindow;
    if(!element.document.parentWindow.__objGlobalCommonInst){//如果还没有初始化      
      oWindow = element.document.parentWindow;
      oWindow.__objGlobalCommonInst = oWindow.parent.__objGlobalCommonInst;
      __objGlobalCommonInst = oWindow.__objGlobalCommonInst;                  
    }                
  }
  
  function initialize(){//初始化操作
    //对象建立 
    oDiv = element.document.createElement("<div class='phms_qinputDiv'/>");//create div
    oDiv = element.appendChild(oDiv);
      
    oCaption = element.document.createElement("<div class='phms_qinputCaption'/>");//create caption
    oCaption = oDiv.appendChild(oCaption);     
    oCaption.innerText = vCaption;
        
    objInput = element.document.createElement("<input class='phms_qinputInput'/>");//create input
    objInput = oDiv.appendChild(objInput);
      
    oList = element.document.createElement("<div class='phms_qinputList' TABINDEX='999'/>");//create list
    oList = element.appendChild(oList);
    
    putXMLSource();
	putTop();
	putLeft();
	putWidth();
	putHeight();
	putLheight();
	putLwidth();
	putValue();
	putText();
	putCaption();
	putReadOnly();
	putCodeCol();
	putTextCol();
    
    refresh();    
    element.runtimeStyle.visibility = "visible";  
  }


  function putCaption(){
    this.vCaption = element.caption;
  }

  function putTop(){
    vTop = element.top;
  }

  function putLeft(){
    vLeft = element.left;
  }

  function putWidth(){
    vWidth = element.width;
  }

  function putHeight(){
    vHeight = element.height;
  }


  function putXMLSource(){    
    objXMLDoc = inputXML(element.xmlSource);
  }
  
  function getXMLSource(){
    return objXMLDoc;
  }  

  function putCodeCol(){
    vCode = element.codeCol;
  }

  function putTextCol(){
    vText = element.textCol;
  }

  function putLheight(){
    Lheight = element.Lheight;
  }

  function putLwidth(){
    Lwidth = element.Lwidth;
  }

  function getValue(){
    if(oCurR){
      return oCurR;   
    }else{
      return null;
    }         
  }
  
  function putReadOnly(){//设置readOnly属性
  	
  	var arg=element.readOnly;

    vReadOnly = (arg.toString().toLowerCase() == "true")?true:false;
    
    if(objInput){
      if(vReadOnly.toString().toLowerCase() == "true"){//
        objInput.disabled = true;
        objInput.style.borderColor = "#cccccc";
      }else{
        objInput.disabled = false;
        objInput.style.borderColor = "#7B9EBD";
      }
    }   
    
  }
  
  function getReadOnly(){
    return vReadOnly;
  }

  function putValue(){
	var anyR=element.value;
    var vXPath;
    var viewText = "";
    var keyValue = "";
    
    switch(typeof(anyR)){//传入r行的类型
      case "string"://字符串
        if(anyR == ""){
          objInput.value = "";
          iText = "";
          oCurR = null;
          return;
        }else{
          anyR = inputXML("<root>" + anyR + "</root>");
        }        
        break;
      case "object"://对象
        anyR = inputXML("<root>" + anyR.xml + "</root>");
        break;
      default:
        break;
    } 
   
    keyValue = anyR.childNodes(0).childNodes(0).getAttribute(vCode);
    vXPath = "/*/r[@" + vCode + "='" +  keyValue + "']";
    oCurR = objXMLDoc.selectSingleNode(vXPath);       
        
    if(oCurR){//
      viewText = oCurR.getAttribute(vText);//从字典中查找出该属性值进行
    }
        
    if(objInput){//
      objInput.value = viewText;
    }    
    iText = viewText;    
  }
  
  function putText(){
    if(objInput) objInput.value = "";
    iText = "";
    oCurR = null;
    if(dicListAppearFlag){
      hideList();
    }  
  }

  function getText(){
    /*
    if(iText)
      return iText;
    else
      return "";
    */
    return(objInput.value);
  }
  
  	__jhtcBindPropertyChange(element,"xmlSource",putXMLSource);
  	__jhtcBindPropertyChange(element,"text",putText);
  	__jhtcBindPropertyChange(element,"caption",putCaption);
  	__jhtcBindPropertyChange(element,"readOnly",putReadOnly);
  	__jhtcBindPropertyChange(element,"codeCol",putCodeCol);
  	__jhtcBindPropertyChange(element,"textCol",putTextCol);
	
	__jhtcBindPropertyChange(element,"top",putTop);
	__jhtcBindPropertyChange(element,"left",putLeft);
	__jhtcBindPropertyChange(element,"width",putWidth);
	__jhtcBindPropertyChange(element,"height",putHeight);
	__jhtcBindPropertyChange(element,"Lwidth",putLwidth);
	__jhtcBindPropertyChange(element,"Lheight",putLheight);
	//__jhtcBindPropertyChange(element,"init",initialize);
    
  	jhtc_attr_init(element,"xmlSource","");
  	jhtc_attr_init(element,"value","");
  	jhtc_attr_init(element,"text","");
  	jhtc_attr_init(element,"caption","");
  	jhtc_attr_init(element,"readOnly","");
  	
  	jhtc_attr_init(element,"codeCol","");
  	jhtc_attr_init(element,"textCol","");
  	
  	jhtc_attr_init(element,"top","");
  	jhtc_attr_init(element,"left","");
  	jhtc_attr_init(element,"width","");
  	jhtc_attr_init(element,"height","");
  	
  	jhtc_attr_init(element,"Lwidth","");
  	jhtc_attr_init(element,"Lheight","");
  	jhtc_attr_init(element,"init","");
  	
	element.getAttribute=getAttribute;
	element.refresh=refresh;
	element.init=initialize;
	element.jhtcInit=function(){
		element.init();
		__jhtcBindPropertyChange(element,"value",putValue);
	};
	return null;
};
jhtc_tag_map["LBQinput"]=jhtc_LBQinput;