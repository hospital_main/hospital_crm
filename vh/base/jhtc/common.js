function jhtc_common(win,jhtc_obj){
	var pageWin=win;
	var element=jhtc_obj;
    var objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");
    var objXMLHttp = new ActiveXObject("MSXML2.XMLHTTP");
    
    var proxyLocation = "http://127.0.0.1:8080/reporttest/main";
    
    function post(argXML){
		var oResponse;//请求的返回结果的XML对象    
		objXMLHttp.Open("POST",proxyLocation,false);
		objXMLHttp.send(objXMLDoc);
		return oResponse = inputXML(objXMLHttp.responseText);//得到返回，并装配为XMLFragement 	
    }
    
	function inputXML(argSource){//返回xmlDOMFragement对象

  		var objXMLFragment = null;
  		var vTempSource = "";
		try{
			switch(typeof(argSource)){
				case "string":
					if(argSource.search(/\./) != -1 && argSource.search(/\</) == -1){ //xml file's location
				  		if(argSource.search(/\+/) != -1) argSource = eval(argSource);						
				  		objXMLDoc.async = false;
						objXMLDoc.load(argSource);
						objXMLFragment = objXMLDoc.createDocumentFragment();
						objXMLFragment.appendChild(objXMLDoc.documentElement);
						break;
					}
					if(argSource.search(/\</) != -1){ //xml string
						argSource = argSource.replace(/xmlns:fo=\"\"/g,"");
						objXMLDoc.loadXML(argSource);
						objXMLFragment = objXMLDoc.createDocumentFragment();
						objXMLFragment.appendChild(objXMLDoc.documentElement);
						break;
					}
					try{
						argSource = argSource.replace(/xmlns:fo=\"\"/g,"");
						objXMLFragment = eval(argSource);//xml data island
					}catch(e){					
					}
					break;
	  				
				case "object":
					if(argSource.xml) 
						vTempSource = argSource.xml;		
						vTempSource = vTempSource.replace(/xmlns:fo=\"\"/g,"");			      
						objXMLDoc.loadXML(vTempSource);//xml document object
						objXMLFragment=objXMLDoc.createDocumentFragment();
						objXMLFragment.appendChild(objXMLDoc.documentElement);
						//return objXMLFragment;
					break;
					case "undefined":
					objXMLFragment = objXMLDoc.createDocumentFragment();
					break;
				default:
					objXMLFragment = null;					  
			}

		}catch(err){
			objXMLFragment = null; 
		}
		return objXMLFragment;
	}
	
	function transformXML(argXML,argXSLT){ //xml的xsl格式化,结果为字符串
      var objXMLFragment,objXSLTFragment;
      var vResult = "";
      var objNode;

      objXMLFragment = inputXML(argXML);
      if(objXMLFragment){
        objXSLTFragment = inputXML(argXSLT);

        if(objXSLTFragment){
          vResult = objXMLFragment.transformNode(objXSLTFragment.childNodes(0));
          while(objXSLTFragment && objXSLTFragment.childNodes(0)){
            objNode = objXSLTFragment.removeChild(objXSLTFragment.childNodes(0));
            objNode = null;
          }
          objXSLTFragment = null;
          delete objXSLTFragment;

  	    }
        while(objXMLFragment && objXMLFragment.childNodes(0)){
          objNode = objXMLFragment.removeChild(objXMLFragment.childNodes(0));
          objNode = null;
        }
        objXMLFragment = null;
        delete objXMLFragment;
      }
      vResult = vResult.replace(/<\?xml[0-9a-zA-Z\s\'\"=\-.\?]{0,}>/g,"");
      
      return vResult;
    }
    
    function transformXMLToNode(argXML,argXSLT){ //xml的xsl格式化,结果为document-fragment对象
      var vResult = transformXML(argXML,argXSLT);
      if(vResult) return inputXML(vResult);
      else return null;
    }
	
	 function initialize(){//初始化操作
      if(!objXMLDoc){//确认工厂已经创建
        objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");    
      }      
      objXMLDoc.setProperty("SelectionLanguage", "XPath");
      
      window.__objGlobalCommonInst = element;
      setGlobCommInst(window);
 
    }
    
    function setGlobCommInst(objFrameWin){
  
        if(objFrameWin.frames.length>0){
          for(var i = 0; i < objFrameWin.frames.length; i++){
            setGlobCommInst(objFrameWin.frames(i)); 
          }
        }else{          
          objFrameWin.__objGlobalCommonInst = element;
        }  
    }
    
    function getXMLDocumentInst(){//获取一个DOM实例
		var objReturnXMLDom;
		objReturnXMLDom = new ActiveXObject("MSXML2.DOMDocument");
		objReturnXMLDom.async = false;
		objReturnXMLDom.setProperty("ServerHTTPRequest",true);
		objReturnXMLDom.setProperty("SelectionLanguage", "XPath");
		return objReturnXMLDom;
    }
  
	__jhtcBindPropertyChange(element,"init",initialize);
  	jhtc_attr_init(element,"init","");
  	
	element.inputXML=inputXML;
	element.post=post;
	element.transformXML=transformXML;
	element.transformXMLToNode=transformXMLToNode;
	element.getXMLDocumentInst=getXMLDocumentInst;
	element.init=initialize;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_tag_map["cmm"]=jhtc_common;