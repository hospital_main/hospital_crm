function jhtc_statPolyline(win,jhtc_obj){
var pageWin=win;
var element=jhtc_obj;
var objXMLDocSource,objXMLDoc;
var objDataArea = null;//折线图显示区域
////////////////////////////////////////////////////////
var vLeft = 40;
var vTop = 50;
var vWidth = 850;
var vHeight = 250;
var yLength = 0;
var xLength = 0;
var vXScale;
var vYScale;
var vYValueScale;
var intLineTop;
var intConstBoard = 25;
var vDivisor = 0;
var vMaxY = 0;
var vMinY = 0;
var vVPerHeight;
var b =1;
var vCaption = "";
var strXAxes = "x";
var strYAxes = "y";
//lpf2005-12-06
var datatype = 'i';

var objGlobal;
var objContextMenuContainersDiv;

var pointDescription = new Array();//折线点的x轴描述文字
//预置polyline配色[15种]
var lineColorArray = new Array('red','#993399','#0099FF','#CC9933','#339933','#FFCC33','#009999','#CC99FF','#99FF33','#FFFF33','#666699','black','#66FFFF','#FF00FF','#0000FF');

//lpf2005-12-06
function datatype(argdatatype){
  element.datatype = argdatatype;
}


function putXAxes(){
	strXAxes = element.xAxes;
}

function putYAxes(){
	strYAxes = element.yAxes;
}

function getPointDescription(){//获取横坐标的描述文字装入pointDescription对象

  var objNode;  
  //按col子节点最大进行排序
  var vXSLT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"><xsl:output method=\"xml\" omit-xml-declaration=\"no\" encoding=\"gb2312\" version=\"1.0\" indent=\"yes\"/><xsl:template match=\"/root\"><xsl:for-each select=\"/*/item\"><xsl:sort select=\"count(child::node())\" data-type=\"number\" order=\"descending\"/><xsl:copy-of  select=\".\"/></xsl:for-each></xsl:template></xsl:stylesheet>";
  objNode = objGlobal.inputXML("<root>" + objGlobal.transformXML(objXMLDoc,vXSLT) + "</root>"); 
  // debugger;
  for(var i=0; i<objNode.childNodes[0].childNodes[0].childNodes.length;i++){//
    pointDescription.push(objNode.childNodes[0].childNodes[0].childNodes[i].getAttribute(element.textCol));
  }
}

function putXMLSource(){
	var argSource=element.xmlSource;
	objXMLDocSource = inputXML(argSource);
	if(objXMLDocSource == null) return;
	objXMLDoc = inputXML(objXMLDocSource);	
}

function calcuScaleOfX(){//计算x轴刻度单位  

	if(!objXMLDoc) return;  
  var oNodeCol = objXMLDoc.selectNodes("/*/item");//所有折线集合
  var vLengthCol = 0;   
    
  if(!oNodeCol) return; 
  
  for(var i=0;i<oNodeCol.length;i++){ //便历所有col节点
    vLengthCol = oNodeCol[i].childNodes.length;
    vDivisor = Math.max(vDivisor,vLengthCol);
  }  
  getMaxValueOfY();
  getPointDescription();//返回x轴坐标描述文字
}

function getMaxValueOfY(){//返回纵坐标每个刻度对应的实际点值的大小

  var oNodeCol = null;
  var vMin = 0;//最小值
  var vMax = 0;//最大值  

	if(!objXMLDoc) return;  
  
  oNodeCol = objXMLDoc.selectNodes("/*/item/item");//所有r节点集合
  if(!oNodeCol) return;
  
  for(var i=0;i<oNodeCol.length;i++){ //便历所有r节点    
	//lpf2005-12-06
	if(element.datatype == '%'){
		var temp = oNodeCol[i].getAttribute(element.valueCol);
		temp = temp.substring(0,temp.length - 1);
		vMax = parseFloat(temp);
		vMin = parseFloat(temp);
	}
	else{
		vMax = parseFloat(oNodeCol[i].getAttribute(element.valueCol));
		vMin = parseFloat(oNodeCol[i].getAttribute(element.valueCol));
    }
    vMaxY = Math.max(vMax,vMaxY);
    vMinY = Math.min(vMin,vMinY);
  }  
  if(vMinY>0) vMinY=0;
//debugger;
  vYValueScale = Math.round((vMaxY-vMinY)/vDivisor*100)/100;//y轴刻度单位
  
}

function paintCoordinate(){ //画坐标系
	if(!objXMLDoc) return;  
  yLength = vHeight;
	xLength = vWidth - 100;

	vXScale = xLength/vDivisor;
	vYScale = yLength/(vDivisor + 3);
  
	var vVML = "<v:group id='vg1' style='position:absolute;border:0px solid red;top:0;left:0;width:1024px;height:768px' coordsize='1024,768'>";
  
  
  //表头	
	vVML += "<v:line style = 'position:absolute;left:-50;height:1;top:-30;width:" + vWidth + ";'><v:textbox style='font-family:宋体;font-size:10.5pt;text-align:center;font-weight:bold;color:" + element.captionColor + ";'>" + vCaption + "</v:textbox></v:line>";
  
  //y轴	
	vVML += "<v:line style = 'position:absolute;border:0 solid green;left:0;top:0' from='35,15' to='35," + (yLength-intConstBoard) + "'' strokeweight='" + element.coordinateWeight + "' strokecolor='" + element.yColor + "' coordsize='0,0'><v:stroke startarrow='open' endarrow='close'/></v:line>";
	
		
	//x轴		
	vVML += "<v:line style = 'position:absolute;left:0;top:0' from='35," + (yLength-intConstBoard) + "' to='" + (xLength - intConstBoard + 50) + "," + (yLength-intConstBoard) + "' strokeweight='" + element.coordinateWeight + "' strokecolor='" + element.xColor + "' coordsize='0,0'><v:stroke startarrow='close' endarrow='open'/></v:line>";
	vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + (xLength + 30) + ";top:" +  (yLength - intConstBoard - 2) + ";' coordsize='21600,21600'><v:textbox style='width:" + vXScale + ";color:red;border:0px solid red;margin-top:0; margin-left:0;'>" + strXAxes + "</v:textbox></v:rect>";
	
	for(var i=1;i< parseInt(vDivisor);i++){//横坐标刻度线	
	  if(element.hasBrokenLine.toString() == "true"){
	  
	      vVML += "<v:line style = 'position:absolute;left:0;top:0;' from='" + (35+i*vXScale + 10) + ", " + (yLength-intConstBoard) + "' to='" + (35+i*vXScale + 10) + ", 25' coordsize='0,0' strokecolor='" + element.voidColor +  "'><v:stroke dashstyle='0,2' endcap='round'/></v:line>";
	      //x轴描述文字
	      vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + ((35+(i-1)*vXScale) - 10) + ";top:" +  (yLength - intConstBoard + 5) + ";' coordsize='21600,21600'><v:textbox style='width:" + vXScale + ";color:" + element.xDescriptionColor + ";font-size:6.5pt;font-family:宋体;border:0px solid red;margin-top:0; margin-left:0;'>" + pointDescription[i-1] + "</v:textbox></v:rect>";
	      //<v:textpath on='true' style='font-size:7pt' string='" + oItemNodesList[i].childNodes[0].text + "'/>
	      //vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + ((35+(i-1)*vXScale) - 10) + ";top:" + (yLength - intConstBoard + 5) + ";' coordsize='21600,21600'><v:path textpathok='true'/><v:textpath on='true' style='font-size:7pt;color:" + element.xDescriptionColor + ";' string='" + pointDescription[i-1] + "'/></v:rect>";
	      //vVML += "<v:line style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + ((35+(i-1)*vXScale) - 10) + ";top:" + (yLength - intConstBoard + 5) + ";' from='-70,100' to='40,0'><v:path textpathok='true'/><v:textpath on='true' style='font-size:7pt' string='" + oItemNodesList[i].childNodes[0].text + "'/></v:line>"; 
	      vVML += "<v:line style = 'position:absolute;color:red;left:0;top:0;' from='" + (35+i*vXScale + 10) + "," + (yLength-intConstBoard) + "' to='" + (35+i*vXScale + 10) + "," + (yLength-intConstBoard - 2) + "' coordsize='0,0' strokecolor='#000000'></v:line>";      
	  }else{
	  
	    if(i!= parseInt(vDivisor)){			
	        vVML += "<v:line style = 'position:absolute;color:red;left:0;top:0;' from='" + (35+i*vXScale) + "," + (yLength-intConstBoard) + "' to='" + (35+i*vXScale) + "," + (yLength-intConstBoard - 3) + "' coordsize='0,0' strokecolor='#000000'></v:line>";      
	        vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + ((35+(i-1)*vXScale) - 20) + ";top:" +  (yLength - intConstBoard) + ";' coordsize='21600,21600'><v:textbox style='width:" + vXScale + ";color:" + element.xDescriptionColor + ";font-size:7pt;font-family:arial;border:0px solid red;margin-top:0; margin-left:0;'>" + pointDescription[i-1] + "</v:textbox></v:rect>";
	      }
	  }
	  
	}	
	//x轴最后一个文字描述
	if (pointDescription.length>0)
		vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + ((35+(parseInt(vDivisor)-1)*vXScale) - 10) + ";top:" +  (yLength - intConstBoard + 5) + ";' coordsize='21600,21600'><v:textbox style='width:" + vXScale + ";color:" + element.xDescriptionColor + ";font-size:6.5pt;font-family:宋体;border:0px solid red;margin-top:0; margin-left:0;'>" + pointDescription[parseInt(vDivisor)-1] + "</v:textbox></v:rect>";
	
	for(var i=0;i<= parseInt(vDivisor);i++){//纵坐标刻度线
		  intLineTop = yLength-(i*vYScale+intConstBoard);
		  
	  if(i == parseInt(vDivisor))	  
	    vVML += "<v:rect style='position:absolute;border:0 solid red;left:-25;top:" + (parseInt(intLineTop) - vYScale - 25) + ";' coordsize='21600,21600'><v:textbox style='color:red;width:200;'>" + strYAxes + "</v:textbox></v:rect>";
	  if(element.hasBrokenLine.toString() == "true"){  
      vVML += "<v:line style = 'position:absolute;top:0;left:0;' from='35," + intLineTop + "' to='" + (xLength - intConstBoard + 20) + "," + intLineTop + "' coordsize='0,0' strokecolor='" + element.voidColor + "'><v:stroke dashstyle='0,2' endcap='round'/></v:line>"; 	    
	    
	    vVML += "<v:line style = 'position:absolute;top:0;left:0;' from='35," + intLineTop + "' to='37," + intLineTop + "' coordsize='0,0' strokecolor='#000000'></v:line>"; 	    
    }else{
      vVML += "<v:line style = 'position:absolute;top:0;left:0;' from='35," + intLineTop + "' to='38," + intLineTop + "' coordsize='0,0' strokecolor='#000000'></v:line>"; 	    
	  }
	    if(!isNaN(vYValueScale) && vYValueScale != 0){
	    //lpf
		if(element.datatype == '%')
				vVML += "<v:rect style='position:absolute;border:0px solid green;left:-10;top:" + (intLineTop - 8) + ";' coordsize='21600,21600'><v:textbox style='color:" + element.yDescriptionColor + ";font-size:8pt;border:0px solid red;margin-top:0; margin-left:0;'>" + String(Math.round(i*vYValueScale+vMinY)) + "%" + "</v:textbox></v:rect>";
			else if(element.datatype == 's')
				vVML += "<v:rect style='position:absolute;border:0px solid green;left:-10;top:" + (intLineTop - 8) + ";' coordsize='21600,21600'><v:textbox style='color:" + element.yDescriptionColor + ";font-size:8pt;border:0px solid red;margin-top:0; margin-left:0;'>" + Math.round((i*vYValueScale+vMinY)*100)/100 + "</v:textbox></v:rect>";
			else 
				vVML += "<v:rect style='position:absolute;border:0px solid green;left:-10;top:" + (intLineTop - 8) + ";' coordsize='21600,21600'><v:textbox style='color:" + element.yDescriptionColor + ";font-size:12px;border:0px solid red;margin-top:0; margin-left:0;'>" + Math.round(i*vYValueScale+vMinY) + "</v:textbox></v:rect>";
      }


	}//End of for
	//debugger;
	if(vMaxY!=0)
	  vVPerHeight = (vHeight - intLineTop -intConstBoard)/(vMaxY-vMinY);
	else
	  vVPerHeight = 0;
	
	vVML += paintPolyline();//
	
	vVML += "</v:group>";		
	
	objDataArea.insertAdjacentHTML("afterBegin",vVML + "<xml:namespace prefix='v' ns='schemas-microsoft-com:vml'/>");

}

/////////////////////////////////////////////////

function paintPolyline(){ //返回画折线的VML

  var vReturnVML = "";//
  var vTextReturnVML = "";//用来标识每条线的各个点的文字描述
  var curLintPoint;//组成一条折线的点的集合
  var vTextPoint = "";//折线上每个点的描述文字
  var vTextLine = "";//折线的描述文字
  var vIdVml = "";//图表标识VML
  var vIconVml = "";//图标VML
  var varYPoint;
  //debugger;

	if(!objXMLDoc) return;  
  
  var oNodeCol = objXMLDoc.selectNodes("/*/item");
   // debugger;
  for(var i=0;i<oNodeCol.length;i++){
    vReturnVML += "<v:polyline style = 'position:absolute;left:10;top:0;' strokeweight='" + element.lineWeight + "' points='";    
    for(var j=0;j<oNodeCol[i].childNodes.length;j++){
      if(vMaxY!=0){
			//lpf
			if(element.datatype == '%'){
				var temp = oNodeCol[i].childNodes[j].getAttribute(element.valueCol);
				temp = temp.substring(0,temp.length - 1);
				varYPoint = ((vVPerHeight * (vMaxY - temp)) + intLineTop);			
			}
			else{
				if(oNodeCol[i].childNodes[j].getAttribute(element.valueCol)<=0){
					varYPoint = ((vVPerHeight * (vMaxY- oNodeCol[i].childNodes[j].getAttribute(element.valueCol))) + intLineTop);
				}else{
					varYPoint = ((vVPerHeight * (vMaxY- oNodeCol[i].childNodes[j].getAttribute(element.valueCol))) + intLineTop);
				}
			}
		}
		else
			varYPoint = yLength - intConstBoard;
			
      vReturnVML += (35+j*vXScale) + "," + varYPoint + " ";      
      if(oNodeCol[i].childNodes[j].getAttribute(element.valueCol))   
        vTextPoint += "<v:rect style='position:absolute;border:0px solid green;cursor:default;left:" + (35+j*vXScale - 15 + 10) + ";top:" + (varYPoint - 17) + ";' coordsize='21600,21600' title='" + oNodeCol[i].getAttribute("name") + oNodeCol[i].childNodes[j].getAttribute(element.textCol) + " (" + oNodeCol[i].childNodes[j].getAttribute(element.valueCol) + ")'><v:textbox style='color:" + element.yDescriptionColor + ";font-size:8pt;color:" + lineColorArray[i] + ";border:0px solid red;margin-top:0; margin-left:0;'>" + oNodeCol[i].childNodes[j].getAttribute(element.valueCol) + "</v:textbox></v:rect>";
       
        //if(j==(oNodeCol[i].childNodes.length - 1))
          //vTextLine += "<v:rect style='position:absolute;width:" + titleWidth + ";height:22;border:0px solid green;left:" + (35+j*vXScale + 10) + ";top:" + (parseInt(varYPoint) - 12) + ";' fillcolor='" + titleBgcolor + "' strokecolor='#dddddd' opacity='0.1' coordsize='21600,21600'><v:fill opacity='0.9'/><v:shadow color='#333333' offset='0.06,0.1' on='t'/><v:textbox style='left:0;top:0;color:" + titleColor + ";font-weight:bold;'>" + oNodeCol[i].getAttribute("title") + "</v:textbox></v:rect>"; 
    }  
    vReturnVML += "' filled='false' strokecolor='";
    ////////////////////////////////////
    if(i < lineColorArray.length){//如果在预置颜色范围内
      vReturnVML += lineColorArray[i];
      vIdVml += "<v:rect style='position:absolute;width:" + (parseInt(element.titleWidth) + 50) + ";height:21;top:" + (i*25 + 25) + ";border:0px solid green;left:" + (xLength +15) + ";'stroked='f' opacity='0.1' coordsize='21600,21600'><v:fill opacity='0.9'/><v:textbox style='left:0;top:2;color:#000000;font-family:宋体;font-size:9pt;'>&nbsp;&nbsp;" + oNodeCol[i].getAttribute("name") + "</v:textbox></v:rect>";
      vIconVml += "<v:rect style='position:absolute;left:" + (xLength + 18) + ";top:" + (i*25 + 31) + ";width:10;height:5;' stroked='f' fillcolor='" + lineColorArray[i] + "'coordsize='21600,21600'></v:rect>";
    }else{//超过预置颜色范围则折线均为黑色
      vReturnVML += "black";
      vIdVml += "<v:rect style='position:absolute;width:" + (parseInt(element.titleWidth) + 50) + ";height:21;top:" + (i*25 + 25) + ";border:0px solid green;left:" + xLength + ";'stroked='f' opacity='0.1' coordsize='21600,21600'><v:fill opacity='0.9'/><v:textbox style='left:0;top:2;color:#000000;font-family:宋体;font-size:9pt;'>&nbsp;&nbsp;" + oNodeCol[i].getAttribute("name") + "</v:textbox></v:rect>";
      vIconVml += "<v:rect style='position:absolute;left:" + (xLength + 3) + ";top:" + (i*25 + 31) + ";width:10;height:5;' stroked='f' fillcolor='#000000'coordsize='21600,21600'></v:rect>";
    }
    /////////////////////////////////////
    vReturnVML += "' strokeweight='1'><v:stroke joinstyle='bevel'/></v:polyline>";
  }
  //debugger;
  //window.prompt("vml",vReturnVML);
  return vReturnVML + vTextPoint + vTextLine + vIdVml + vIconVml; 
}



function refresh(){
	
	//需要重画时需要将objDataArea中的所有对象清除
	while(objDataArea.children.length >0){
		objDataArea.children(0).removeNode(true);
	}
	
	if(pointDescription && pointDescription.length>1){
	  while(pointDescription.length > 0){
	    pointDescription.pop();
	  } 
	}
	
	vDivisor = 0;
	xLength = 0;
	yLength = 0;
	vMaxY = 0;
	vXScale = 0;
	vYScale = 0;

	calcuScaleOfX();//计算x轴刻度	
	paintCoordinate();//创建坐标系	
}

function initialize(){
//debugger;
	//创建图形显示区域
	objDataArea = element.document.createElement("<div class='phms_pl_dataarea'></div>");
	objDataArea = element.appendChild(objDataArea); //外框,加滚动条的层
	//objDataArea.oncontextmenu = processContextMenuEvent;
	
	with(element){
		style.pixelWidth = vWidth;
		style.pixelHeight = vHeight;
		style.top = vTop;
		style.left = vLeft;
	}
	
	with(objDataArea){//设置外层容器的大小
	    objDataArea.style.left = 0;
	    objDataArea.style.top = 0;
	  	objDataArea.style.pixelWidth = vWidth;
	  	objDataArea.style.pixelHeight = vHeight;
	  	objDataArea.style.padding = 0;
	}
	
	calcuScaleOfX();//计算x轴刻度	
	
	paintCoordinate();//创建坐标系
  
	element.style.visibility = "visible";
	element.onkeydown = onVGKeyDown;
	
	///////////////////////////////////////////////////
	objContextMenuContainersDiv = element.document.createElement("<div style='position:absolute;background:#cccccc;border:outset 2px;z-index:1;'>");
	objContextMenuContainersDiv.onblur = function(){this.style.visibility="hidden";};
	var objMenuItem = element.document.createElement("<span style='position:absolute;width:125;height:17;padding-top:3;padding-left:3;top:2;font-size:9pt;'>");
	objMenuItem.innerText = "导出统计图到office";
	objMenuItem.onfocus = transVMLtoOfficeDocument;
	objMenuItem = objContextMenuContainersDiv.appendChild(objMenuItem);
	objMenuItem.onmouseover = function(){this.style.background = "#000080";this.style.color = "#ffffff";};
	objMenuItem.onmouseout = function(){this.style.background = "#cccccc";this.style.color = "#000000";};
	objContextMenuContainersDiv = element.appendChild(objContextMenuContainersDiv);
	objContextMenuContainersDiv.style.visibility = "hidden";
	//objMenuItem.bindClick = transVMLtoOfficeDocument;
	///////////////////////////////
}

function processContextMenuEvent(){
	event.returnValue = false;
	with(objContextMenuContainersDiv.style){
		left = event.x - 10 - vLeft;
		top = event.y - 10 - vTop;
		width = 130;
		height = 25;
		visibility = "visible";
	}
	objContextMenuContainersDiv.focus();
}

function transVMLtoOfficeDocument(){
	/////////此处添加发送vml导出请求数据到服务器/////////////////////
	var objMessageSelect = window.parent.parent.__objGlobalCommonInst.createMsgBody();
		objMessageSelect.inUseState = "busy";
        objMessageSelect.action = "导出";
        objMessageSelect.target = "统计图";
    var temp = createVMLForWord();
    //window.prompt("office",temp);
	var vmlObj  = window.parent.parent.__objGlobalCommonInst.inputXML(temp);
	objMessageSelect.addOther(vmlObj.firstChild);
   var objReturnData = window.parent.parent.__objGlobalCommonInst.post(objMessageSelect.outerXML,"","0");		
	var node = objReturnData.selectSingleNode("//r");	
    objMessageSelect.inUseState = "free";
	var openPath = "../file_pool/"+node.getAttribute("url");
	window.open(openPath);	
	/////////////////////////////////////////////////
}

function createVMLForWord(){
	//paintCoordinate();
	return paintCoordinateForOffice();
}

function paintCoordinateForOffice(){ //画坐标系
	if(!objXMLDoc) return;  
  yLength = vHeight;
	xLength = vWidth - 100;

	vXScale = xLength/vDivisor;
	vYScale = yLength/(vDivisor + 3);
  
	var vVML = "<v:group id='vg1' style='position:absolute;border:0px solid red;top:0;left:0;width:1024px;height:768px' coordsize='1024,768'>";
  
  
  //表头	
	vVML += "<v:line style = 'position:absolute;left:-50;height:1;top:-30;width:" + vWidth + ";'><v:textbox style='font-family:宋体;font-size:10.5pt;text-align:center;font-weight:bold;color:" + element.captionColor + ";'>" + vCaption + "</v:textbox></v:line>";
  
  //y轴	
	vVML += "<v:line style = 'position:absolute;border:0 solid green;left:0;top:0' from='35,15' to='35," + (yLength-intConstBoard) + "'' strokeweight='" + element.coordinateWeight + "' strokecolor='" + element.yColor + "' coordsize='0,0'><v:stroke startarrow='open' endarrow='close'/></v:line>";
	
		
	//x轴		
	vVML += "<v:line style = 'position:absolute;left:0;top:0' from='35," + (yLength-intConstBoard) + "' to='" + (xLength - intConstBoard + 50) + "," + (yLength-intConstBoard) + "' strokeweight='" + element.coordinateWeight + "' strokecolor='" + element.xColor + "' coordsize='0,0'><v:stroke startarrow='close' endarrow='open'/></v:line>";
	vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + (xLength + 30) + ";top:" +  (yLength - intConstBoard - 2) + ";' coordsize='21600,21600'><v:textbox style='width:" + vXScale + ";color:red;border:0px solid red;margin-top:0; margin-left:0;'>" + strXAxes + "</v:textbox></v:rect>";
	
	for(var i=1;i< parseInt(vDivisor);i++){//横坐标刻度线	
	  if(element.hasBrokenLine.toString() == "true"){
	  
	      vVML += "<v:line style = 'position:absolute;left:0;top:0;' from='" + (35+i*vXScale + 10) + ", " + (yLength-intConstBoard) + "' to='" + (35+i*vXScale + 10) + ", 25' coordsize='0,0' strokecolor='" + element.voidColor +  "'><v:stroke dashstyle='0,2' endcap='round'/></v:line>";
	      //x轴描述文字
	      vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + ((35+(i-1)*vXScale) - 10) + ";top:" +  (yLength - intConstBoard + 5) + ";' coordsize='21600,21600'><v:textbox style='width:" + vXScale + ";color:" + element.xDescriptionColor + ";font-size:6.5pt;font-family:宋体;border:0px solid red;margin-top:0; margin-left:0;'>" + pointDescription[i-1] + "</v:textbox></v:rect>";
	      //<v:textpath on='true' style='font-size:7pt' string='" + oItemNodesList[i].childNodes[0].text + "'/>
	      //vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + ((35+(i-1)*vXScale) - 10) + ";top:" + (yLength - intConstBoard + 5) + ";' coordsize='21600,21600'><v:path textpathok='true'/><v:textpath on='true' style='font-size:7pt;color:" + element.xDescriptionColor + ";' string='" + pointDescription[i-1] + "'/></v:rect>";
	      //vVML += "<v:line style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + ((35+(i-1)*vXScale) - 10) + ";top:" + (yLength - intConstBoard + 5) + ";' from='-70,100' to='40,0'><v:path textpathok='true'/><v:textpath on='true' style='font-size:7pt' string='" + oItemNodesList[i].childNodes[0].text + "'/></v:line>"; 
	      vVML += "<v:line style = 'position:absolute;color:red;left:0;top:0;' from='" + (35+i*vXScale + 10) + "," + (yLength-intConstBoard) + "' to='" + (35+i*vXScale + 10) + "," + (yLength-intConstBoard - 2) + "' coordsize='0,0' strokecolor='#000000'></v:line>";      
	  }else{
	  
	    if(i!= parseInt(vDivisor)){			
	        vVML += "<v:line style = 'position:absolute;color:red;left:0;top:0;' from='" + (35+i*vXScale) + "," + (yLength-intConstBoard) + "' to='" + (35+i*vXScale) + "," + (yLength-intConstBoard - 3) + "' coordsize='0,0' strokecolor='#000000'></v:line>";      
	        vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + ((35+(i-1)*vXScale) - 20) + ";top:" +  (yLength - intConstBoard) + ";' coordsize='21600,21600'><v:textbox style='width:" + vXScale + ";color:" + element.xDescriptionColor + ";font-size:7pt;font-family:arial;border:0px solid red;margin-top:0; margin-left:0;'>" + pointDescription[i-1] + "</v:textbox></v:rect>";
	      }
	  }
	}	
	//x轴最后一个文字描述
	vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + ((35+(parseInt(vDivisor)-1)*vXScale) - 10) + ";top:" +  (yLength - intConstBoard + 5) + ";' coordsize='21600,21600'><v:textbox style='width:" + vXScale + ";color:" + element.xDescriptionColor + ";font-size:6.5pt;font-family:宋体;border:0px solid red;margin-top:0; margin-left:0;'>" + pointDescription[parseInt(vDivisor)-1] + "</v:textbox></v:rect>";
	
	for(var i=0;i<= parseInt(vDivisor);i++){//纵坐标刻度线
	
	  intLineTop = yLength-(i*vYScale+intConstBoard);
	  if(i == parseInt(vDivisor))	  
	    vVML += "<v:rect style='position:absolute;border:0 solid red;left:-25;top:" + (parseInt(intLineTop) - vYScale - 25) + ";' coordsize='21600,21600'><v:textbox style='color:red;width:200;'>" + strYAxes + "</v:textbox></v:rect>";
	  if(element.hasBrokenLine.toString() == "true"){  
      vVML += "<v:line style = 'position:absolute;top:0;left:0;' from='35," + intLineTop + "' to='" + (xLength - intConstBoard + 20) + "," + intLineTop + "' coordsize='0,0' strokecolor='" + element.voidColor + "'><v:stroke dashstyle='0,2' endcap='round'/></v:line>"; 	    
	    
	    vVML += "<v:line style = 'position:absolute;top:0;left:0;' from='35," + intLineTop + "' to='37," + intLineTop + "' coordsize='0,0' strokecolor='#000000'></v:line>"; 	    
    }else{
      vVML += "<v:line style = 'position:absolute;top:0;left:0;' from='35," + intLineTop + "' to='38," + intLineTop + "' coordsize='0,0' strokecolor='#000000'></v:line>"; 	    
	  }
	    if(vYValueScale != 0){
	    //lpf
		if(element.datatype == '%')
				vVML += "<v:rect style='position:absolute;border:0px solid green;left:-10;top:" + (intLineTop - 8) + ";' coordsize='21600,21600'><v:textbox style='color:" + element.yDescriptionColor + ";font-size:8pt;border:0px solid red;margin-top:0; margin-left:0;'>" + String(Math.round(i*vYValueScale)) + "%" + "</v:textbox></v:rect>";
			else if(element.datatype == 's')
				vVML += "<v:rect style='position:absolute;border:0px solid green;left:-10;top:" + (intLineTop - 8) + ";' coordsize='21600,21600'><v:textbox style='color:" + element.yDescriptionColor + ";font-size:8pt;border:0px solid red;margin-top:0; margin-left:0;'>" + Math.round(i*vYValueScale*100)/100 + "</v:textbox></v:rect>";
			else 
				vVML += "<v:rect style='position:absolute;border:0px solid green;left:-10;top:" + (intLineTop - 8) + ";' coordsize='21600,21600'><v:textbox style='color:" + element.yDescriptionColor + ";font-size:8pt;border:0px solid red;margin-top:0; margin-left:0;'>" + Math.round(i*vYValueScale) + "</v:textbox></v:rect>";
      }


	}//End of for
	//debugger;
	if(vMaxY!=0)
	  vVPerHeight = (vHeight - intLineTop -intConstBoard)/(vMaxY-vMinY);
	else
	  vVPerHeight = 0;
	
	vVML += paintPolylineForOffice();//
	
	vVML += "</v:group>";		
	return vVML;
	//objDataArea.insertAdjacentHTML("afterBegin",vVML + "<xml:namespace prefix='v' ns='schemas-microsoft-com:vml'/>");

}

function paintPolylineForOffice(){ //返回画折线的VML

  var vReturnVML = "";//
  var vTextReturnVML = "";//用来标识每条线的各个点的文字描述
  var curLintPoint;//组成一条折线的点的集合
  var vTextPoint = "";//折线上每个点的描述文字
  var vTextLine = "";//折线的描述文字
  var vIdVml = "";//图表标识VML
  var vIconVml = "";//图标VML
  var varYPoint;
  //debugger;

	if(!objXMLDoc) return;  
  
  var oNodeCol = objXMLDoc.selectNodes("/*/item");
   // debugger;
  for(var i=0;i<oNodeCol.length;i++){
    vReturnVML += "<v:polyline style = 'position:absolute;left:10;top:0;' strokeweight='" + element.lineWeight + "' points='";    
    for(var j=0;j<oNodeCol[i].childNodes.length;j++){
      if(vMaxY!=0){
			//lpf
			if(element.datatype == '%'){
				var temp = oNodeCol[i].childNodes[j].getAttribute(element.valueCol);
				temp = temp.substring(0,temp.length - 1);
				varYPoint = ((vVPerHeight * (vMaxY - temp)) + intLineTop);			
			}
			else
				varYPoint = ((vVPerHeight * (vMaxY - oNodeCol[i].childNodes[j].getAttribute(element.valueCol))) + intLineTop);
		}
		else
			varYPoint = yLength - intConstBoard;
			
      vReturnVML += (35+j*vXScale) + "," + varYPoint + " ";      
      if(oNodeCol[i].childNodes[j].getAttribute(element.valueCol))   
        vTextPoint += "<v:rect style='position:absolute;border:0px solid green;cursor:default;left:" + (35+j*vXScale - 15 + 10) + ";top:" + (varYPoint - 17) + ";' coordsize='21600,21600' title='" + oNodeCol[i].getAttribute("name") + oNodeCol[i].childNodes[j].getAttribute(element.textCol) + " (" + oNodeCol[i].childNodes[j].getAttribute(element.valueCol) + ")'><v:textbox style='color:" + element.yDescriptionColor + ";font-size:8pt;color:" + lineColorArray[i] + ";border:0px solid red;margin-top:0; margin-left:0;'>" + oNodeCol[i].childNodes[j].getAttribute(element.valueCol) + "</v:textbox></v:rect>";
       
        //if(j==(oNodeCol[i].childNodes.length - 1))
          //vTextLine += "<v:rect style='position:absolute;width:" + element.titleWidth + ";height:22;border:0px solid green;left:" + (35+j*vXScale + 10) + ";top:" + (parseInt(varYPoint) - 12) + ";' fillcolor='" + titleBgcolor + "' strokecolor='#dddddd' opacity='0.1' coordsize='21600,21600'><v:fill opacity='0.9'/><v:shadow color='#333333' offset='0.06,0.1' on='t'/><v:textbox style='left:0;top:0;color:" + titleColor + ";font-weight:bold;'>" + oNodeCol[i].getAttribute("title") + "</v:textbox></v:rect>"; 
    }  
    vReturnVML += "' filled='false' strokecolor='";
    ////////////////////////////////////
    if(i < lineColorArray.length){//如果在预置颜色范围内
      vReturnVML += lineColorArray[i];
      vIdVml += "<v:rect style='position:absolute;width:" + (parseInt(element.titleWidth) + 50) + ";height:21;top:" + (i*25 + 25) + ";border:0px solid green;left:" + (xLength +15) + ";'stroked='f' opacity='0.1' coordsize='21600,21600'><v:fill opacity='0.9'/><v:textbox style='left:0;top:2;color:#000000;font-family:宋体;font-size:9pt;'>&nbsp;&nbsp;" + oNodeCol[i].getAttribute("name") + "</v:textbox></v:rect>";
      vIconVml += "<v:rect style='position:absolute;left:" + (xLength + 18) + ";top:" + (i*25 + 31) + ";width:10;height:5;' stroked='f' fillcolor='" + lineColorArray[i] + "'coordsize='21600,21600'></v:rect>";
    }else{//超过预置颜色范围则折线均为黑色
      vReturnVML += "black";
      vIdVml += "<v:rect style='position:absolute;width:" + (parseInt(element.titleWidth) + 50) + ";height:21;top:" + (i*25 + 25) + ";border:0px solid green;left:" + xLength + ";'stroked='f' opacity='0.1' coordsize='21600,21600'><v:fill opacity='0.9'/><v:textbox style='left:0;top:2;color:#000000;font-family:宋体;font-size:9pt;'>&nbsp;&nbsp;" + oNodeCol[i].getAttribute("name") + "</v:textbox></v:rect>";
      vIconVml += "<v:rect style='position:absolute;left:" + (xLength + 3) + ";top:" + (i*25 + 31) + ";width:10;height:5;' stroked='f' fillcolor='#000000'coordsize='21600,21600'></v:rect>";
    }
    /////////////////////////////////////
    vReturnVML += "' strokeweight='1'><v:stroke joinstyle='bevel'/></v:polyline>";
  }
  //debugger;
  return vReturnVML + vTextPoint + vTextLine + vIdVml + vIconVml; 
}




function putCaption(){
  vCaption = element.caption;
}

function putLeft(){
  vLeft = element.left;
}

function putTop(){
  vTop = element.top;
}

function putWidth(){
  vWidth = element.width;
}

function putHeight(){
  vHeight = element.height;
}

function onVGKeyDown(){

  if(event.keyCode == 107){//"+"[放大]
    b = b * 0.9090909;     
    element.all("vg1").coordsize = (1024 * b) + "," + (768 * b);

  }
  if(event.keyCode == 109){//"-"[缩小]
    b = b * 1.1;
    element.all("vg1").coordsize = (1024 * b) + "," + (768 * b);

  }
}

function inputXML(argXML){
	
	try{
		objGlobal = __objGlobalCommonInst;		
	}catch(e){}
	try{
		if(!objGlobal){
			objGlobal = parent.__objGlobalCommonInst;
		}
	}catch(e){}
	
	if(!objGlobal){
		objGlobal = parent.parent.__objGlobalCommonInst;
	}
	try{
		if(typeof(objGlobal) == "undefined"){
			objGlobal = window.dialogArguments.__objGlobalCommonInst;
		}
	}catch(e){}
	
	 return objGlobal.inputXML(argXML);
}

	__jhtcBindPropertyChange(element,"xmlSource",putXMLSource);
	__jhtcBindPropertyChange(element,"xAxes",putXAxes);
	__jhtcBindPropertyChange(element,"yAxes",putYAxes);
	__jhtcBindPropertyChange(element,"caption",putCaption);
	__jhtcBindPropertyChange(element,"top",putTop);
	__jhtcBindPropertyChange(element,"left",putLeft);
	__jhtcBindPropertyChange(element,"width",putWidth);
	__jhtcBindPropertyChange(element,"height",putHeight);
	__jhtcBindPropertyChange(element,"refresh",refresh);
	__jhtcBindPropertyChange(element,"init",initialize);
	
  	jhtc_attr_init(element,"xmlSource","");
  	jhtc_attr_init(element,"xAxes","");
  	jhtc_attr_init(element,"yAxes","");
  	jhtc_attr_init(element,"xColor","#000000");
  	jhtc_attr_init(element,"yColor","#000000");
  	jhtc_attr_init(element,"xDescriptionColor","#000000");
  	jhtc_attr_init(element,"yDescriptionColor","#000000");
  	jhtc_attr_init(element,"coordinateWeight","1");
  	jhtc_attr_init(element,"textCol",null);
  	jhtc_attr_init(element,"valueCol",null);
  	jhtc_attr_init(element,"hasBrokenLine","true");
  	jhtc_attr_init(element,"voidColor","black");
  	jhtc_attr_init(element,"caption","");
  	jhtc_attr_init(element,"captionColor","#000000");
  	jhtc_attr_init(element,"lineWeight","1.1");
  	jhtc_attr_init(element,"titleWidth","70");
  	jhtc_attr_init(element,"titleBgcolor","red");
  	jhtc_attr_init(element,"titleColor","yellow");
  	jhtc_attr_init(element,"top","");
  	jhtc_attr_init(element,"left","");
  	jhtc_attr_init(element,"width","");
  	jhtc_attr_init(element,"height","");
  	jhtc_attr_init(element,"refresh","");
  	jhtc_attr_init(element,"init","");
  	jhtc_attr_init(element,"datatype",null);
  	
	
	element.init=initialize;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_tag_map["statPl"]=jhtc_statPolyline;