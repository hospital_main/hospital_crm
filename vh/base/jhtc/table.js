function jhtc_table(win,jhtc_obj){
  var pageWin=win;
  var element=jhtc_obj;
  var fixRow = 2;
  var fixCol = 1;
  var colOffset = -1;

  var baseDiv;
  var baseDivTop = 0
  var baseDivLeft = 0

  var colDiv;
  var rowDiv;
  var commonDiv;

  var baseTable;
  var baseTableTop = 0
  var baseTableLeft = 0

  var form

  var colDiv = element.document.createElement("<div class='tableSubDiv'/>")
  var colTable
  var rowDiv = colDiv.cloneNode(true)
  var rowTable
  var commonDiv = colDiv.cloneNode(true)
  var commonTable
  var commonWidth = 0
  var commonHeight = 0

  var colors

  // 带箭头的缓存
  var colArrowTemp = null
	function fnInit2(){
		if(element.waitTime=="0"){
			fnInit();
		}else{
			window.setTimeout(fnInit,parseInt(element.waitTime));
		}
		
		
	}
  function setFixRow(argValue) {
    fixRow = eval(element.fixRow)
  }

  function setFixCol(argValue) {
    fixCol = eval(argValue)
  }

  function setColOffset(argValue) {
    colOffset = eval(argValue)
  }

  /*
   * 初始化
   */
	function fnInit() {
		
	element.fixRow = eval(element.fixRow);
	element.fixCol = eval(element.fixCol);
	element.colOffset = eval(element.colOffset);
	  //创建 baseDIV
	rowDiv.id="_vhTableRowDiv";
	colDiv.id="_vhTableColDiv";
    baseDiv = element.document.createElement("<div class='tableBaseDiv' id='_vhTableBaseDiv'/>");
    element.appendChild(baseDiv)

    baseTable = element.firstChild;
    baseDiv.appendChild(baseTable);

    if (baseTable.rows.length <= element.fixRow)
      return;

    baseDiv.onscroll  = scrollDiv
    baseDiv.ondblclick = initColor

    // 取得baseDiv绝对位置
    form = baseDiv
    while(form.tagName != "BODY") {
	    baseDivTop += form.offsetTop;
	    baseDivLeft += form.offsetLeft;
	    form = form.offsetParent;
    }

    // 取得baseTable绝对位置
    form = baseTable
    while(form.tagName != "BODY") {
	    baseTableTop += form.offsetTop;
	    baseTableLeft += form.offsetLeft;
	    form = form.offsetParent;
    }

    var border = eval(baseTable.border)
    if (border == null)
      border = 0

    // 确定公共区域长宽
    for (var i=0; i<element.fixRow; i++) {
      commonHeight = commonHeight + baseTable.rows[i].offsetHeight + border
    }
    commonHeight = commonHeight + border

    for (var i=0; i<element.fixCol; i++) {
      commonWidth = commonWidth + baseTable.rows[0].cells[i].offsetWidth + border
    }
    commonWidth = commonWidth + border + border

    // 初始化
    colTable = baseTable.cloneNode(true)
    colDiv.appendChild(colTable)
    element.appendChild(colDiv)
    colDiv.ondblclick = initColor

    rowTable = baseTable.cloneNode(true)
    for (var i=element.fixRow; i<colTable.rows.length; i++) {
      rowTable.deleteRow(element.fixRow);
      //baseTable.rows[i].onmousedown = changeColor
      //colTable.rows[i].onmousedown = changeColor
      baseTable.rows[i].onmouseover = changeColor
      baseTable.rows[i].onmouseout = changeColor
      colTable.rows[i].onmouseover = changeColor
      colTable.rows[i].onmouseout = changeColor
    
    }
    rowDiv.appendChild(rowTable)
    element.appendChild(rowDiv)

    commonTable = rowTable.cloneNode(true)
    commonDiv.appendChild(commonTable)
    element.appendChild(commonDiv)

    // 响应表头排序
    if (element.colOffset >= 0 && baseTable.rows.length > element.fixRow) {
      for (var i=element.fixRow-1; i<element.fixRow; i++) {
        for (var j=0; j<rowTable.rows[i].cells.length; j++) {
          rowTable.rows[i].cells[j].onclick = sortTable
          rowTable.rows[i].cells[j].style.cursor = 'hand'
        }
      }
    }
    rowTable.rows[element.fixRow-1].cells[0].click()

    //初始化颜色
    colors = new Array(baseTable.rows.length-element.fixRow);

    // 激活属性baseDiv.style.width
    baseDiv.style.width = form.clientWidth - baseDivLeft;
    adjShape();
		window.setTimeout(adjShape,20);
		window.setTimeout(adjShape,200);
    //绑定事件
    window.attachEvent("onresize", adjShape);
	}


  function adjShape() {
 		form = baseDiv
 		baseDivTop=0
 		baseDivLeft=0
    while(form.tagName != "BODY") {
	    baseDivTop += form.offsetTop;
	    baseDivLeft += form.offsetLeft;
	    form = form.offsetParent;
    }

    // 取得baseTable绝对位置
    form = baseTable
    baseTableTop=0
    baseTableLeft=0
    while(form.tagName != "BODY") {
	    baseTableTop += form.offsetTop;
	    baseTableLeft += form.offsetLeft;
	    form = form.offsetParent;
    }

    baseDiv.style.width = form.clientWidth - baseDivLeft - (baseDiv.offsetWidth - baseDiv.clientWidth)+17
    baseDiv.style.height = form.clientHeight - baseDivTop - (baseDiv.offsetHeight - baseDiv.clientHeight)+17
		
    // 固定列 判断必要性
    colDiv.style.top = baseTableTop
    colDiv.style.left = baseTableLeft
    colDiv.style.width = commonWidth
    colDiv.style.height = baseDiv.clientHeight

    // 固定行 判断必要性
    rowDiv.style.top = baseTableTop
    rowDiv.style.left = baseTableLeft
    rowDiv.style.width = baseDiv.clientWidth
    rowDiv.style.height = commonHeight


    // 固定公共部分 判断必要性
    commonDiv.style.top = baseDivTop
    commonDiv.style.left = baseDivLeft
    commonDiv.style.width = commonWidth
    commonDiv.style.height = commonHeight
    //colDiv.style.display="none"
    //commonDiv.style.display="none"
    //rowDiv.style.display="none"
  }

  function scrollDiv() {
    colDiv.scrollTop = baseDiv.scrollTop
    rowDiv.scrollLeft = baseDiv.scrollLeft
  }

  function changeColor() {
    if (colors[this.rowIndex-element.fixRow]==null) {
      colors[this.rowIndex-element.fixRow] = this.bgColor
      colTable.rows[this.rowIndex].bgColor = '#EFEFEF'
      baseTable.rows[this.rowIndex].bgColor = '#EFEFEF'
    } else {
      colTable.rows[this.rowIndex].bgColor = colors[this.rowIndex-element.fixRow]
      baseTable.rows[this.rowIndex].bgColor = colors[this.rowIndex-element.fixRow]
      colors[this.rowIndex-element.fixRow] = null
    }
  }

  function initColor() {
    for (var i=0; i<colors.length; i++) {
      if (colors[i] != null) {
        colTable.rows[i+element.fixRow].bgColor = colors[i]
        baseTable.rows[i+element.fixRow].bgColor = colors[i]
        colors[i] = null;
      }
    }
  }

  function sortTable() {
    if (colArrowTemp!=null)
      colArrowTemp.removeChild(colArrowTemp.lastChild);

    if (this.upMode==null)
      this.upMode = false
    else
      this.upMode = !this.upMode

    var arrowUp = element.document.createElement("SPAN")
    arrowUp.innerHTML	= "5"
    arrowUp.style.cssText 	= "background-image:url(../../../base/print1/up.gif);PADDING-RIGHT: 0px; MARGIN-TOP: -3px; PADDING-LEFT: 0px; FONT-SIZE: 10px; MARGIN-BOTTOM: 2px; PADDING-BOTTOM: 2px; OVERFLOW: hidden; WIDTH: 10px; COLOR: blue; PADDING-TOP: 0px; FONT-FAMILY: webdings; HEIGHT: 11px";

    var arrowDown = element.document.createElement("SPAN");
    arrowDown.innerHTML	= "6";
    arrowDown.style.cssText = "background-image:url(../../../base/print1/down.gif);PADDING-RIGHT: 0px; MARGIN-TOP: -3px; PADDING-LEFT: 0px; FONT-SIZE: 10px; MARGIN-BOTTOM: 2px; PADDING-BOTTOM: 2px; OVERFLOW: hidden; WIDTH: 10px; COLOR: blue; PADDING-TOP: 0px; FONT-FAMILY: webdings; HEIGHT: 11px";

    this.appendChild(this.upMode?arrowUp:arrowDown);
    colArrowTemp = this

    // 排序
    var sortArray = new Array();
  	var cellValue;
  	var colIndex = this.cellIndex + element.colOffset
  	for(var i=element.fixRow;i<baseTable.rows.length;i++){
  	  cellValue = baseTable.rows[i].cells[colIndex].innerText.toLowerCase();
  	  while (cellValue.indexOf(",")>=0)
        cellValue = cellValue.replace(',','0');
  		sortArray.length++
  		sortArray[sortArray.length-1] = new Array(cellValue, baseTable.rows[i], colTable.rows[i])
  	}

    var mode = this.upMode
    sortArray.sort(function(arr1, arr2) {
                  var a = Number(arr1[0]);
			            var b = Number(arr2[0]);
			           	a=eval(a);
			            b=eval(b);
			            return mode?(a-b):(b-a);
                 });
  	for(var i=0;i<sortArray.length;i++){
  		baseTable.lastChild.appendChild(sortArray[i][1])
  		colTable.lastChild.appendChild(sortArray[i][2])
  	}

  	// 处理打印问题
    element.sortColIndex = colIndex
    element.sortFlag = this.upMode
  }
	jhtc_attr_init(element,"fixRow","2");
  	jhtc_attr_init(element,"fixCol","1");
  	jhtc_attr_init(element,"colOffset","-1");
  	jhtc_attr_init(element,"sortColIndex","");
  	jhtc_attr_init(element,"sortFlag","");
  	jhtc_attr_init(element,"waitTime","0");
  	
	element.adjShape=adjShape;
	element.init=fnInit;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_tag_map["vhFixTable"]=jhtc_table;
