/*
	/////////自定义报表（UserReportDesigner - URD）/////////
	
 */ 
function jhtc_URD_View(win,jhtc_obj){
var pageWin=win;
var element=jhtc_obj;
var objUserReportXMLData = null;

var objUserReportDesignerContainers = null;
var objUserReportDesignerTitleContainers = null;
var objUserReportDesignerCoordinateInfo = null;
var objUserReportDesignerInsertFunctionBtn = null;
var objUserReportDesignerEditWin = null;
var objUserReportDesignerUniteBtn = null;

var objUserReportDesignerDataTableRowsInput = null;
var objUserReportDesignerDataTableColsInput = null;
var objUserReportDesignerCreateDataTableBtn = null;

var objUserReportDesignerTableContainers = null;
var objUserReportDesignerSpace = null;
var objUserReportDesignerTableLeftIndex = null;
var objUserReportDesignerTableLeft = null;
var objUserReportDesignerTableTopIndex = null;
var objUserReportDesignerTableTop = null;
var objUserReportDesignerTableContent = null;
var objUserReportDesignerTableData = null;

var objSplitLeftRightHandle = null;
var objSplitTopBottomHandle = null;

var objUserReportDesignerTableBottomScrollContainers = null;
var objUserReportDesignerTableLeftScrollBtn = null;//
var objUserReportDesignerTableRightScrollBtn = null;//

var objUserReportDesignerTableRightScrollContainers = null;
var objUserReportDesignerTableTopScrollBtn = null;//
var objUserReportDesignerTableBottomScrollBtn = null;//

var objRowsSplitePannel = null;
var objColsSplitePannel = null;

var objCurrentRowIndexCell = null;
var objCurrentColIndexCell = null;

var objUserReportDesignerCurrentRow = null;

var objFocusInput = null;
var objSplitLine = null;
var objUserReportDesignerTableFocusInputContextMenuContainers = null;
var objUserReportDesignerTableContextMenuContainers = null;

var objUserReportDesignerBottomScrollbar = null;
var objUserReportDesignerRightScrollbar = null;
var intScrollBarStartPositionX = 0;
var intScrollBarStartPositionY = 0;

var objUserReportDesignerTableCellContextSecContainers = null;
var objUserReportDesignerTableCellContextSec = null;

var objArrCellIndex = new Array();
var objArrOperator = new Array();

var objStartCell = null;
var objCurrentCell = null;
var objEndCell = null;

var objUserReportDesignerTextAlignLeftBtn = null;
var objUserReportDesignerTextAlignCenterBtn = null;
var objUserReportDesignerTextAlignRightBtn = null;

var strUserReportDesignerTitleHTML = "";

var intHeight = 0;
var intRows = 30;
var intCols = 17;

var curCellRowIndex = 0;
var curCellCellIndex = 0;

var intCurposionX = 0;
var intCurposionY = 0;
var intOffsetVal = 0;

var intBeginRowIndex = 0;
var intBeginCellIndex = 0;
var intEndRowIndex = 0;
var intEndCellIndex = 0;

var intFirstColIndex = 1;
var intFirstRowIndex = 1;

var booleanEndRowSeted = false;
var intActionType = 0;// 0 - 合并  1 - 还原
var intEndRowRowIndex = 0;

var strEndRowBackColor = "#eeeeee";
var strUnitBackcolor = "#f5f5f5";

var inputStyle = 1;
var textAlign = 3;// 1 - 左对齐  2 - 居中对齐  3 - 右对齐

//选中区域
var vBeginRowIndex;
var vBeginCellIndex;
var vEndRowIndex;
var vEndCellIndex;

var intCurrentCellIndex = 0;
var intCurrentRowIndex = 0;

//指标
//var strIndexXMLData = '<?xml version="1.0" encoding="GB2312"?><root><r name="科 室" code="Dept_ListDataItem" type="2"/><r name="本期收入" code="income_current_period" type="2"/><r name="本期成本" code="cost_current_period" type="2"/><r name="本期收益" code="proceeds_current_period" type="2"/><r name="本期未纳入成本" code="cost_uninclude_current_period" type="2"/><r name="本期财政补助收入" code="gov_subsidy_income_current_period" type="2"/><r name="本期医疗收入" code="medical_income_current_period" type="2"/><r name="本期医疗成本" code="medical_cost_current_period" type="2"/><r name="本期医疗收益" code="medical_proceeds_current_period" type="2"/><r name="本期药品收入" code="medicine_income_current_period" type="2"/><r name="本期药品成本" code="medicine_cost_current_period" type="2"/><r name="本期药品收益" code="medicine_proceeds_current_period" type="2"/><r name="本期直接医疗收入" code="direct_medical_income_current_period" type="2"/><r name="本期直接计入成本" code="recorded_cost_current_period" type="2"/><r name="本期直接成本计算计入" code="direct_cost_calculate_record_current_period" type="2"/><r name="本期分摊公用成本" code="public_cost_current_period" type="2"/><r name="本期管理成本" code="management_cost_current_period" type="2"/><r name="本期医辅成本" code="medical_assistant_cost_current_period" type="2"/><r name="本期固定成本" code="fixed_cost_current_period" type="2"/><r name="本期变动成本" code="fluctuant_cost_current_period" type="2"/><r name="本期直接成本" code="direct_cost_current_period" type="2"/><r name="本期人力成本" code="hr_cost_current_period" type="2"/><r name="本期离退休成本" code="retire_cost_current_period" type="2"/><r name="本期材料成本" code="meterial_cost_current_period" type="2"/><r name="本期净药品成本" code="just_medicine_cost_current_period" type="2"/><r name="本期折旧成本" code="depreciation_cost_current_period" type="2"/><r name="本期其他成本" code="other_cost_current_period" type="2"/><r name="本期分摊医技成本" code="Apportion_Medical_Technic_Cost_Current_Period_ListDataItem" type="2"/><r name="累计收入" code="Income_Total_ListDataItem" type="2"/><r name="累计成本" code="Cost_Total_ListDataItem" type="2"/><r name="累计收益" code="Proceeds_Total_ListDataItem" type="2"/><r name="累计未纳入成本" code="Cost_Uninclude_Total_ListDataItem" type="2"/><r name="累计收支结余" code="Income_Expense_Balance_Total_ListDataItem" type="2"/><r name="累计财政补助收入" code="Gov_Subsidy_Income_Total_ListDataItem" type="2"/><r name="累计医疗收入" code="Medical_Income_Total_ListDataItem" type="2"/><r name="累计医疗成本" code="Medical_Cost_Total_ListDataItem" type="2"/><r name="累计医疗收益" code="Medical_Proceeds_Total_ListDataItem" type="2"/><r name="累计药品收入" code="Medicine_Income_Total_ListDataItem" type="2"/><r name="累计药品成本" code="Medicine_Cost_Total_ListDataItem" type="2"/><r name="累计药品收益" code="Medicine_Proceeds_Total_ListDataItem" type="2"/><r name="累计直接医疗收入" code="Direct_Medical_Income_Total_ListDataItem" type="2"/><r name="累计直接计入成本" code="Recorded_Cost_Total_ListDataItem" type="2"/><r name="累计直接计算计入" code="Direct_Cost_Calculate_Record_Total_ListDataItem" type="2"/><r name="累计公用成本" code="Public_Cost_Total_ListDataItem" type="2"/><r name="累计管理成本" code="Management_Cost_Total_ListDataItem" type="2"/><r name="累计医辅成本" code="Medical_Assistant_Cost_Total_ListDataItem" type="2"/><r name="累计固定成本" code="Fixed_Cost_Total_ListDataItem" type="2"/><r name="累计变动成本" code="Fluctuant_Cost_Total_ListDataItem" type="2"/><r name="累计人力成本" code="Hr_Cost_Total_ListDataItem" type="2"/><r name="累计离退休成本" code="Retire_Cost_Total_ListDataItem" type="2"/><r name="累计材料成本" code="Meterial_Cost_Total_ListDataItem" type="2"/><r name="累计净药品成本" code="Just_Medicine_Total_ListDataItem" type="2"/><r name="累计折旧成本" code="Depreciation_Cost_Total_ListDataItem" type="2"/><r name="累计其他成本" code="Other_Cost_Total_ListDataItem" type="2"/><r name="累计分摊医技成本" code="Apportion_Medical_Technic_Cost_Total_ListDataItem" type="2"/></root>';
var strIndexXSLT = '<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="xml" version="1.0" encoding="gbk" indent="yes"/><xsl:preserve-space elements="r"/><xsl:template match="/"><select style="width:100%;background:#eeeeee;"><xsl:apply-templates select="root"/></select></xsl:template><xsl:template match="root"><xsl:for-each select="r"><option value="{@code}">			<xsl:if test="string-length(@name) - string-length(normalize-space(@name))>1"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></xsl:if><xsl:value-of select="@name"/></option></xsl:for-each></xsl:template></xsl:stylesheet>';

var stat = "";
		
var startTd = null;
var endTd = null;

var curSelectCells = null;
var lastSelectCells = null;

var groupWideLength = 0;
var groupHeightLength = 0;
var groupPixelWidth = 0;
var groupPixelHeight = 0;

var cellArray = null;
var lastSelectCellArray = null;

var strReportKey = "";//报表标识码
var strCaption = "";//报表标题

///////////////////////////////////////
var objSelectedCellArray = new Array();
var objColIndexLabelArr = new Array('','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ','DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ','EA','EB','EC','ED','EE','EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ');
var objUniteCellArray = new Array();

Array.prototype.clearColor = function(){
	for(var i = 0; i < this.length; i ++){
		if(this[i] && this[i].parentElement){
			this[i].style.background = (this[i].parentElement.rowIndex == intEndRowRowIndex) ? strEndRowBackColor : this[i].getAttribute("bgcolor");
		}
	}
};

Array.prototype.isInArray = function(objAim){
	var booleanHasAim = false;
	for(var i = 0; i < this.length; i ++){
		if(this[i] == objAim){
			booleanHasAim = true;
			break;
		}
	}
	return booleanHasAim;
};

function getCommonHTML(){//获取报表通用输出,返回结果为一个HTML
						 //下个版本将实现输出xml结果 
	//--------
	var objCurCell = null;
	var objTempCell = null;
	var intCellColSpan = 0;
	var intCellRowSpan = 0;
	var objDataTable = null;

	if(!objUserReportDesignerTableData) return;
	//var objReturnXML = __objGlobalCommonInst.getXMLDocumentInst();
	
	objDataTable = objUserReportDesignerTableData.cloneNode(true);
	__jhtcBindPropertyChange(objDataTable,"outerHTML",getCommonHTML);
	objDataTable.removeAttribute("cellPadding");
	objDataTable.removeAttribute("cellSpacing");
	objDataTable.removeAttribute("border");
	
	for( var i = 0; i < objDataTable.rows.length; i ++ ){
		for( var j = 0; j < objDataTable.rows[i].cells.length; j ++ ){
			
			objCurCell = objDataTable.rows[i].cells[j];
			intCellColSpan = parseInt(objCurCell.getAttribute("colSpan"));
			intCellRowSpan = parseInt(objCurCell.getAttribute("rowSpan"));
			objCurCell.setAttribute("colSpan",intCellColSpan);
			objCurCell.setAttribute("rowSpan",intCellRowSpan);
			
			if(intCellColSpan > 1){
				for( var m = 0; m <intCellColSpan; m ++ ){
					objTempCell = window.document.createElement("td");		
					objTempCell.style.display = "none";
					objTempCell = objCurCell.insertAdjacentElement("afterEnd",objTempCell);
				}
			}
		}
	}
	window.prompt("",objDataTable.outerHTML);
	return element.commonHTML=objDataTable.outerHTML;
}

function processUserReportDesignerTableContentClickEvent(){
	var objSrcElement = event.srcElement;
	var intTempRowIndex = objSrcElement.parentElement.rowIndex;
	var intTempCellIndex = objSrcElement.cellIndex;
	if(objSrcElement.tagName.toLowerCase() == "td" && intTempCellIndex != 0 && intTempRowIndex != 0){
	//debugger;
		if(!objSrcElement.parentElement.getAttribute("isTitleRow")){
			showFocusInput(objSrcElement);
		}
	}
}

function showFocusInput(objCell){
	try{
		if(objFocusInput.tieupCell.colSpan != 1 || objFocusInput.tieupCell.rowSpan != 1 ){
			objFocusInput.tieupCell.style.fontWeight = "bold";
		}else{
			objFocusInput.tieupCell.style.fontWeight = "normal";
			if(objFocusInput.tieupCell.parentElement.rowIndex == intEndRowRowIndex){
				objFocusInput.tieupCell.style.color = "red";	
			}else{
				objFocusInput.tieupCell.style.color = "black";
			}	
		}
		/*
		switch(textAlign){
			case 1:
				objFocusInput.tieupCell.style.textAlign = "left";
				break;
			case 2:
				objFocusInput.tieupCell.style.textAlign = "center";
				break;
			case 3:
				objFocusInput.tieupCell.style.textAlign = "right";
				break;
		}		
		*/
		//dataType   0 - 空白  1 - 输入值  2 - 字段  3 - 公式
		
		switch(inputStyle){
			case 1://直接录入
				//if(objFocusInput.tieupCell.parentElement.getAttribute("isTitleRow") == "y") return;
				objFocusInput.tieupCell.innerText = objFocusInput.value;
				synchronizationXMLDomData(objFocusInput.tieupCell);
				break;
				
			case 2://公式
				break;
				
			default:
				objFocusInput.tieupCell.dataType = 0;
				break;
		}
	}catch(e){}
	
	objUserReportDesignerEditWin.value = "";
	hiddenAllFocusActionPannel();
	
	with(objFocusInput.style){
		visibility = "visible";
		left = objCell.offsetLeft;
		top = objCell.offsetTop;	
		width = objCell.offsetWidth;
		height = objCell.offsetHeight;
	}
	
	objFocusInput.tieupCell = objCell;
	window.focusCell = objCell;
	objFocusInput.rowIndex = objCell.parentElement.rowIndex;
	objFocusInput.cellIndex = ( intFirstColIndex == 1 ) ? objCell.cellIndex : objCell.cellIndex + intFirstColIndex - 1;
	objFocusInput.value = objCell.innerText;
	
	var intCellIndex = objCell.cellIndex;
	objUserReportDesignerCurrentRow = objCell.parentElement;
	objUserReportDesignerCoordinateInfo.value = objUserReportDesignerTableData.rows[0].cells[objFocusInput.cellIndex].innerText 
													+ " : " + objUserReportDesignerTableData.rows[objFocusInput.rowIndex].cells[0].innerText;
	
	inputStyle = 1;//恢复默认
	try{
		objFocusInput.focus();
	}catch(e){}
}

function initialize(){// 组件初始化
	
	window.document.body.scroll = "no";		
	
	//报表容器
	objUserReportDesignerContainers = window.document.createElement("<div class='HZHUserReportDesigner_Containers'>");
	objUserReportDesignerContainers.id = "++++++";
	objUserReportDesignerContainers = window.document.body.appendChild(objUserReportDesignerContainers);
	objUserReportDesignerContainers.style.visibility = "visible";
	objUserReportDesignerContainers.oncontextmenu = function (){hiddenAllFocusActionPannel();return false;};

	//报表标题容器
	objUserReportDesignerTitleContainers = window.document.createElement("<div class='HZHUserReportDesigner_TitleContainers'>");
	objUserReportDesignerTitleContainers = element.appendChild(objUserReportDesignerTitleContainers);
	objUserReportDesignerTitleContainers.style.visibility = "visible";
	
	strUserReportDesignerTitleHTML += "<input class='HZHUserReportDesigner_CoordinateInfo' value='A : 1' readonly>";
	strUserReportDesignerTitleHTML += "<button class='HZHUserReportDesigner_InsertFunctionBtn' disabled></button>";
	strUserReportDesignerTitleHTML += "<input class='HZHUserReportDesigner_EditWin' value='' readonly>";
	strUserReportDesignerTitleHTML += "<button class='HZHUserReportDesigner_UniteCellBtn'>合并单元格</button>";
	strUserReportDesignerTitleHTML += "<button class='HZHUserReportDesigner_textAlignLeftBtn' disabled>左</button>";
	strUserReportDesignerTitleHTML += "<button class='HZHUserReportDesigner_textAlignCenterBtn' disabled>中</button>";
	strUserReportDesignerTitleHTML += "<button class='HZHUserReportDesigner_textAlignRightBtn' disabled>右</button>";
	
	objUserReportDesignerTitleContainers.insertAdjacentHTML("afterBegin",strUserReportDesignerTitleHTML);
	objUserReportDesignerTitleContainers.oncontextmenu = function (){hiddenAllFocusActionPannel();return false;};
	objUserReportDesignerCoordinateInfo = objUserReportDesignerTitleContainers.children(0);
	objUserReportDesignerInsertFunctionBtn = objUserReportDesignerTitleContainers.children(1);
	objUserReportDesignerEditWin = objUserReportDesignerTitleContainers.children(2);
	objUserReportDesignerUniteBtn = objUserReportDesignerTitleContainers.children(3);
	objUserReportDesignerUniteBtn.style.visibility = "hidden";
	
	objUserReportDesignerTextAlignLeftBtn = objUserReportDesignerTitleContainers.children(4);
	objUserReportDesignerTextAlignLeftBtn.action = "left";
	objUserReportDesignerTextAlignLeftBtn.onclick = processUserReportDesignerTableCellAlignBtnClickEvent;
	objUserReportDesignerTextAlignCenterBtn = objUserReportDesignerTitleContainers.children(5);
	objUserReportDesignerTextAlignCenterBtn.action = "center";
	objUserReportDesignerTextAlignCenterBtn.onclick = processUserReportDesignerTableCellAlignBtnClickEvent;
	objUserReportDesignerTextAlignRightBtn = objUserReportDesignerTitleContainers.children(6);
	objUserReportDesignerTextAlignRightBtn.action = "right";
	objUserReportDesignerTextAlignRightBtn.onclick = processUserReportDesignerTableCellAlignBtnClickEvent;
	objUserReportDesignerTextAlignRightBtn.style.background = "#dddddd";
	
	objUserReportDesignerUniteBtn.onclick = processUserReportDesignerUniteBtnclickEvent;
	objUserReportDesignerInsertFunctionBtn.onclick = processUserReportDesignerInsertBtnclickEvent;
	
	//报表内容容器
	objUserReportDesignerTableContainers = window.document.createElement("<div class='HZHUserReportDesigner_TableContainers'>");
	objUserReportDesignerTableContainers = element.appendChild(objUserReportDesignerTableContainers);
	objUserReportDesignerTableContainers.style.visibility = "visible";
	objUserReportDesignerTableContainers.onselectstart = function (){event.returnValue = false;};
	objUserReportDesignerTableContainers.oncontextmenu = function (){return false;};
	
	//列调整控制器
	objColsSplitePannel = window.document.createElement("<div class='HZHUserReportDesigner_ColsSplitePanel'>");
	objColsSplitePannel = objUserReportDesignerTableContainers.appendChild(objColsSplitePannel);
	
	//左右拖动柄
	objSplitLeftRightHandle = window.document.createElement("<div class='HZHUserReportDesigner_LeftRightHandle'>");
	objSplitLeftRightHandle = objColsSplitePannel.appendChild(objSplitLeftRightHandle);
	objSplitLeftRightHandle.onmousedown = processSplitLeftRightHandleMousedownEvent;
	objSplitLeftRightHandle.onmouseup = processSplitLeftRightHandleMouseupEvent;
	
	
	//行调整控制器
	objRowsSplitePannel = window.document.createElement("<div class='HZHUserReportDesigner_RowsSplitePanel'>");
	objRowsSplitePannel = objUserReportDesignerTableContainers.appendChild(objRowsSplitePannel);
	
	//上下拖动柄
	objSplitTopBottomHandle = window.document.createElement("<div class='HZHUserReportDesigner_TopBottomHandle'>");
	objSplitTopBottomHandle = objRowsSplitePannel.appendChild(objSplitTopBottomHandle);
	objSplitTopBottomHandle.onmousedown = processSplitTopBottomHandleMousedownEvent;
	objSplitTopBottomHandle.onmouseup = processSplitTopBottomHandleMouseupEvent;
	
	//调整线
	objSplitLine = window.document.createElement("<div class='HZHUserReportDesigner_SplitLine'>");
	objSplitLine = objUserReportDesignerTableContainers.appendChild(objSplitLine);
	
	
	//报表内容
	objUserReportDesignerTableContent = window.document.createElement("<div class='HZHUserReportDesigner_TableContent'>");
	objUserReportDesignerTableContent = objUserReportDesignerTableContainers.appendChild(objUserReportDesignerTableContent);
	objUserReportDesignerTableContent.style.visible = "visible";
	var strDataTableHTML = '<table border="0" cellspacing="1" cellpadding="2" style="position:absolute;background:#cccccc;width:' + intCols*60 + ';text-align:center;word-break:break-all;cursor:default;z-index:1;">';
	strDataTableHTML += createDataTable();
	strDataTableHTML += "<table>";
	objUserReportDesignerTableContent.innerHTML = strDataTableHTML;
	objUserReportDesignerTableData = objUserReportDesignerTableContent.firstChild;	
	
	//数据区域事件绑定
	objUserReportDesignerTableData.onmouseover = processUserReportDesignerTableContentMouseoverEvent;
	//objUserReportDesignerTableContent.onmousedown = processUserReportDesignerTableContentMousedownEvent;
	//objUserReportDesignerTableData.onmousemove = processUserReportDesignerTableContentMousemoveEvent;
	//objUserReportDesignerTableData.onmouseup = processUserReportDesignerTableContentMouseupEvent;
	objUserReportDesignerTableData.onclick = processUserReportDesignerTableContentClickEvent;
	//objUserReportDesignerTableData.oncontextmenu = processUserReportDesignerTableContentContextmenuEvent;
	
	//焦点input
	objFocusInput = window.document.createElement("<input class='HZHUserReportDesigner_FocusInput'>");
	objFocusInput = objUserReportDesignerTableContent.appendChild(objFocusInput);
	objFocusInput.onkeydown = processUserReportDesignerFocusInputKeyupEvent;
	//objFocusInput.oncontextmenu = processUserReportDesignerFocusInputContextMenuEvent;
	
	//下滚动容器
	objUserReportDesignerTableBottomScrollContainers = window.document.createElement("<div class='HZHUserReportDesigner_TableBottomScrollContainers'>");
	objUserReportDesignerTableBottomScrollContainers = objUserReportDesignerTableContainers.appendChild(objUserReportDesignerTableBottomScrollContainers);
	
	//滚动条左控制按钮
	objUserReportDesignerTableLeftScrollBtn = window.document.createElement("<button class='HZHUserReportDesigner_TableLeftScrollBtn'>");
	objUserReportDesignerTableLeftScrollBtn = objUserReportDesignerTableBottomScrollContainers.appendChild(objUserReportDesignerTableLeftScrollBtn);
	objUserReportDesignerTableLeftScrollBtn.action = "left";
	objUserReportDesignerTableLeftScrollBtn.onclick = processUserReportDesignerTableScrollBtnClickEvent;
	
	//滚动条右控制按钮
	objUserReportDesignerTableRightScrollBtn = window.document.createElement("<button class='HZHUserReportDesigner_TableRightScrollBtn'>");
	objUserReportDesignerTableRightScrollBtn = objUserReportDesignerTableBottomScrollContainers.appendChild(objUserReportDesignerTableRightScrollBtn);
	objUserReportDesignerTableRightScrollBtn.action = "right";
	objUserReportDesignerTableRightScrollBtn.onclick = processUserReportDesignerTableScrollBtnClickEvent;
	
	//bottomScroll bar
	objUserReportDesignerBottomScrollbar = window.document.createElement("<div class='HZHUserReportDesigner_TableBottomScroll'>");
	objUserReportDesignerBottomScrollbar = objUserReportDesignerTableBottomScrollContainers.appendChild(objUserReportDesignerBottomScrollbar);
	objUserReportDesignerBottomScrollbar.onmousedown = processUserReportDesignerTableBottomScrollBarMousedownEvent;
	objUserReportDesignerBottomScrollbar.onmouseup = processUserReportDesignerTableBottomScrollBarMouseupEvent;
	
	//右滚动容器
	objUserReportDesignerTableRightScrollContainers = window.document.createElement("<div class='HZHUserReportDesigner_TableRightScrollContainers'>");
	objUserReportDesignerTableRightScrollContainers = objUserReportDesignerTableContainers.appendChild(objUserReportDesignerTableRightScrollContainers);
	
	//滚动条上控制按钮
	objUserReportDesignerTableTopScrollBtn = window.document.createElement("<button class='HZHUserReportDesigner_TableTopScrollBtn'>");
	objUserReportDesignerTableTopScrollBtn = objUserReportDesignerTableRightScrollContainers.appendChild(objUserReportDesignerTableTopScrollBtn);
	objUserReportDesignerTableTopScrollBtn.action = "top";
	objUserReportDesignerTableTopScrollBtn.onclick = processUserReportDesignerTableScrollBtnClickEvent;
	
	//滚动条下控制按钮
	objUserReportDesignerTableBottomScrollBtn = window.document.createElement("<button class='HZHUserReportDesigner_TableBottomScrollBtn'>");
	objUserReportDesignerTableBottomScrollBtn = objUserReportDesignerTableRightScrollContainers.appendChild(objUserReportDesignerTableBottomScrollBtn);
	objUserReportDesignerTableBottomScrollBtn.action = "bottom";
	objUserReportDesignerTableBottomScrollBtn.onclick = processUserReportDesignerTableScrollBtnClickEvent;
	
	//rightScroll bar
	objUserReportDesignerRightScrollbar = window.document.createElement("<div class='HZHUserReportDesigner_TableRightScroll'>");
	objUserReportDesignerRightScrollbar = objUserReportDesignerTableRightScrollContainers.appendChild(objUserReportDesignerRightScrollbar);
	
	//focusInput contextmenu
	objUserReportDesignerTableFocusInputContextMenuContainers = window.document.createElement("<div class='HZHUserReportDesigner_TableFocusInputContextmenuContainers'>");
	objUserReportDesignerTableFocusInputContextMenuContainers = objUserReportDesignerTableContainers.appendChild(objUserReportDesignerTableFocusInputContextMenuContainers);
	
	//UserReportDesignerTableData contextmenu
	objUserReportDesignerTableContextMenuContainers = window.document.createElement("<div class='HZHUserReportDesigner_TableViewContextmenuContainers'>");
	objUserReportDesignerTableContextMenuContainers = objUserReportDesignerTableContainers.appendChild(objUserReportDesignerTableContextMenuContainers);
	
	//sec
	objUserReportDesignerTableCellContextSecContainers = window.document.createElement("<div class='HZHUserReportDesigner_TableCellContextSecContainers'>");
	objUserReportDesignerTableCellContextSecContainers = objUserReportDesignerTableContainers.appendChild(objUserReportDesignerTableCellContextSecContainers);
	objUserReportDesignerTableCellContextSecContainers.innerHTML = __objGlobalCommonInst.transformXML(objUserReportXMLData,strIndexXSLT);
	window.itemContext = __objGlobalCommonInst.transformXML(objUserReportXMLData,strIndexXSLT);
	objUserReportDesignerTableCellContextSec = objUserReportDesignerTableCellContextSecContainers.firstChild;
		
	objUserReportDesignerTableContent.onscroll = processUserReportDesignerTableContentScrollEvent;

	refresh();
	window.onresize = refresh;
	
}

function createDataTable(){
	var s = "";
	for( var i = 0; i < intRows + 1; i ++ ){
		s += "<tr idx='" + (i + 1)  + "'>";
		for( var j = 0; j < intCols; j ++ ){
			s += ( i == 0 ) ? '<td idx="' + ( j + 1 ) + '" style="height:18;width=60;background:#eeeeee;" rowUnit="1" colUnit="1">' + objColIndexLabelArr[j] + '</td>' : ( j == 0 ) ? '<td idx="' + ( j + 1 ) + '" style="height:18;width=60;background:#eeeeee;cursor:hand" title="设置该行为最终列所在行..." rowUnit="1" colUnit="1">' + i + '</td>' : '<td idx="' + ( j + 1 ) + '" style="height:18;width=60;background:#ffffff;" rowUnit="1" colUnit="1"></td>';
		}
		s += "</tr>";
	}
	return "<tbody>" + s + "</tbody>";
}

function processUserReportDesignerCreateDataTableBtnclickEvent(){
	try{
		intRows = parseInt(objUserReportDesignerDataTableRowsInput.value);
		intCols = parseInt(objUserReportDesignerDataTableColsInput.value);
		
		objUserReportDesignerTableData.innerHTML = createDataTable();
		objUserReportDesignerTableData.style.width = (intCols - 1) * 60;
		intEndRowRowIndex = 0;
	
	}catch(e){
		alert("出现运行期错误，系统将按默认设置生成表格...");	
	}
	
}

function processUserReportDesignerTableBottomScrollBarMousedownEvent(){
	objUserReportDesignerBottomScrollbar.setCapture(true);
	objUserReportDesignerBottomScrollbar.attachEvent("onmousemove",processUserReportDesignerTableBottomScrollBarMousemoveEvent);
	intScrollBarStartPositionX = event.x;
	intScrollBarStartPositionY = event.y;
}

function processUserReportDesignerTableBottomScrollBarMousemoveEvent(){
	var offsetVal = event.x - intScrollBarStartPositionX;
	if(objUserReportDesignerBottomScrollbar.offsetWidth + offsetVal + 17 >= objUserReportDesignerTableContent.clientWidth){
		return;
	}
	objUserReportDesignerBottomScrollbar.style.left = offsetVal;
	if(objUserReportDesignerBottomScrollbar.offsetLeft < 16){
		objUserReportDesignerBottomScrollbar.style.left = 16;	
	}
	if(offsetVal > objUserReportDesignerTableData.rows[0].cells[intFirstColIndex].clientWidth){
		hiddenCellByIndex("col");
	}
	window.status = objUserReportDesignerBottomScrollbar.offsetLeft;
}

function hiddenCellByIndex(f){
	switch(f){
		case "row":
			break;
		case "col":
			for( var i = 0; i <objUserReportDesignerTableData.rows.length; i ++ ){
				objUserReportDesignerTableData.rows[i].cells[intFirstColIndex].style.display = "none";
			} 
			break;
	}
}

function processUserReportDesignerTableBottomScrollBarMouseupEvent(){
	objUserReportDesignerBottomScrollbar.releaseCapture();
	objUserReportDesignerBottomScrollbar.detachEvent("onmousemove",processUserReportDesignerTableBottomScrollBarMousemoveEvent);
}

function processUserReportDesignerTableContentContextmenuEvent(){
	var objSrc = event.srcElement;
	objUserReportDesignerTableFocusInputContextMenuContainers.style.visibility = "hidden";
	objUserReportDesignerTableContextMenuContainers.style.visibility = "hidden";
	if( objSrc.tagName.toLowerCase() != "td" ) return;
	//objSrc.style.background = "white";
	if( objSrc.cellIndex != 0 && objSrc.parentElement.rowIndex != 0){
		objUserReportDesignerTableContextMenuContainers.style.visibility = "visible";
		objUserReportDesignerTableContextMenuContainers.style.left = objSrc.offsetLeft - objUserReportDesignerTableContent.scrollLeft + 2;
		objUserReportDesignerTableContextMenuContainers.style.top = objSrc.offsetTop - objUserReportDesignerTableContent.scrollTop + 2;
		objUserReportDesignerTableContextMenuContainers.innerHTML = "<table style='border:solid 0px black;cursor:default;' cellspacing='1' cellpadding='2' width='100%'><tr><td idx='__hzh_urd_saveReport' onmouseover='this.style.background=\"#000080\";this.style.color=\"white\"' onmouseout='this.style.background=\"#cccccc\";this.style.color=\"black\"'>保存...</td></tr><tr><td idx='__hzh_urd_openReport' onmouseover='this.style.background=\"#000080\";this.style.color=\"white\"' onmouseout='this.style.background=\"#cccccc\";this.style.color=\"black\"'>打开已有报表...</td></tr></table>";
		objUserReportDesignerTableContextMenuContainers.firstChild.onclick = processUserReportDesignerTableCellContextMenuClickEvent;
	}
}

function processUserReportDesignerTableCellContextMenuClickEvent(){
	var objSrc = event.srcElement;
	if( objSrc.tagName.toLowerCase() == "td" ){
		objUserReportDesignerTableContextMenuContainers.style.visibility = "hidden";
		switch(objSrc.idx){
			
			case "__hzh_urd_openReport"://加载已有报表
				//alert("目前版本不支持...");
				hiddenAllFocusActionPannel();
				objUserReportXMLData = __objGlobalCommonInst.inputXML("xml\\reportDataSource_new.xml");
				getReportView(objUserReportXMLData);
				break;
				
			case "__hzh_urd_saveReport"://保存报表
				getCommonHTML();
			/*
				if(!objUserReportXMLData) return;
				objUserReportDesignerTableFocusInputContextMenuContainers.style.visibility = "hidden";
				window.showModalDialog("getFileName.htm",window,"dialogWidth:400px;dialogHeight:20px;help:no;center:yes;status:no");	
				if(strReportKey == ""){//生成随机id
					var d = new Date();
					strReportKey = "__hzh__UserReportDesigner__" + d.getDate().toString() + d.getTime().toString();
				}
				var objUserReportDesignerRoot = objUserReportXMLData.selectSingleNode("//user_report");
				objUserReportDesignerRoot.setAttribute("key",strReportKey);
				objUserReportDesignerRoot.setAttribute("caption",strCaption);
				objUserReportDesignerRoot.setAttribute("fileName",window.reportFileName);
				//window.prompt("要保存的报表数据",objUserReportXMLData.xml);
				*/
				break;	
		}
	}
}

function getReportView(objReportData){
	if(!objReportData || !objUserReportDesignerTableData) return;
	for( var i = objUserReportDesignerTableData.rows.length - 1; i >0; i -- ){
		objUserReportDesignerTableData.rows[i].parentElement.removeChild(objUserReportDesignerTableData.rows[i]);
	}
	var objTempTrNode = null;
	var objTempTdNode = null;
	var intMaxIdx = 0;
	var intColNum = 0;
	
	var objHasIdxTrList = objReportData.selectNodes("//tr");
	for( var i = 0; i < objHasIdxTrList.length; i ++ ){
		intMaxIdx = objHasIdxTrList[i].getAttribute("idx") > intMaxIdx ? objHasIdxTrList[i].getAttribute("idx") : intMaxIdx;
	}
	
	var objTableReturnNode = objReportData.selectSingleNode("//table");
	//window.prompt("**********",objReportData.xml);
	for( var i = 0; i < objTableReturnNode.childNodes.length; i ++ ){
		objTempTrNode = objUserReportDesignerTableData.insertRow();
		objTempTrNode.idx = objTableReturnNode.childNodes[i].getAttribute("idx");
		objTempTrNode.isTitleRow = objTableReturnNode.childNodes[i].getAttribute("isTitleRow");
		objTempTdNode = objTempTrNode.insertCell();
		objTempTdNode.style.background = "#eeeeee";
		objTempTdNode.innerText = i + 1;
		for ( var j = 0; j < objTableReturnNode.childNodes[i].childNodes.length; j ++ ){
			if( j == 0 ) intColNum = objTableReturnNode.childNodes[i].childNodes.length;
			objTempTdNode = objTempTrNode.insertCell();
			if(objTableReturnNode.childNodes[i].childNodes[j].text == "NaN"){
				objTempTdNode.innerText = "-";
			}else{
				objTempTdNode.innerText = objTableReturnNode.childNodes[i].childNodes[j].text;
			}
			objTempTdNode.style.cssText = objTableReturnNode.childNodes[i].childNodes[j].getAttribute("style");
			objTempTdNode.idx = objTableReturnNode.childNodes[i].childNodes[j].getAttribute("idx");
			objTempTdNode.isBlank = objTableReturnNode.childNodes[i].childNodes[j].getAttribute("isBlank");
			if(objTableReturnNode.childNodes[i].childNodes[j].getAttribute("rowSpan") && objTableReturnNode.childNodes[i].childNodes[j].getAttribute("colSpan")){
				objTempTdNode.rowSpan = objTableReturnNode.childNodes[i].childNodes[j].getAttribute("rowSpan");
				objTempTdNode.colSpan = objTableReturnNode.childNodes[i].childNodes[j].getAttribute("colSpan");
			}
		}
	}
	
	////取有效列/////////////////////////////////////////////////////////////////
/*
	var intLeftColIndex = 0;
	for( var i = 1; i < objUserReportDesignerTableData.rows[1].cells.length; i ++ ){
		if(objUserReportDesignerTableData.rows[1].cells[i].innerText != ""){
			intLeftColIndex = i;
			break;
		}
	}
*/
//window.prompt("处理前",objUserReportDesignerTableData.outerHTML);

	for( var i = objUserReportDesignerTableData.rows.length - 1; i > 0; i -- ){
		for( var j = objUserReportDesignerTableData.rows[i].cells.length - 1; j > 0; j -- ){
			if( objUserReportDesignerTableData.rows[i].cells[j].getAttribute("isBlank") && objUserReportDesignerTableData.rows[i].cells[j].getAttribute("isBlank").toString() == "true"){
				objUserReportDesignerTableData.rows[i].removeChild(objUserReportDesignerTableData.rows[i].cells[j]);
			}
		}
	}

	/////////////////////////////////////////////////////////////////////
	//window.prompt("处理后",objUserReportDesignerTableData.outerHTML);
	objUserReportDesignerTableData.style.height = 18;
	var intWidth = 0;
	for( var i = 1; i < objUserReportDesignerTableData.rows[0].cells.length; i ++ ){
		if( i == 1 ){
			objUserReportDesignerTableData.rows[0].cells[i].style.width = 350;
		}else{
			objUserReportDesignerTableData.rows[0].cells[i].style.width = 205;
		}
		intWidth += parseInt(objUserReportDesignerTableData.rows[0].cells[i].offsetWidth);
	}
	objUserReportDesignerTableData.style.width = intWidth;
}

function endRowColInfo(text,value){
	this.text = text;
	this.value = value;
}

function processUserReportDesignerInsertBtnclickEvent(){
	alert();
}

function processUserReportDesignerTableCellAlignBtnClickEvent(){
	switch(event.srcElement.action){
		case "left":
			textAlign = 1;
			objUserReportDesignerTextAlignLeftBtn.style.background = "#eeeeee";
			objUserReportDesignerTextAlignCenterBtn.style.background = "#dddddd";
			objUserReportDesignerTextAlignRightBtn.style.background = "#dddddd";
			break;
		case "center":
			textAlign = 2;
			objUserReportDesignerTextAlignLeftBtn.style.background = "#dddddd";
			objUserReportDesignerTextAlignCenterBtn.style.background = "#eeeeee";
			objUserReportDesignerTextAlignRightBtn.style.background = "#dddddd";
			break;
		case "right":
			textAlign = 3;
			objUserReportDesignerTextAlignLeftBtn.style.background = "#dddddd";
			objUserReportDesignerTextAlignCenterBtn.style.background = "#dddddd";
			objUserReportDesignerTextAlignRightBtn.style.background = "#eeeeee";
			break;
	}
}

function processUserReportDesignerFocusInputContextMenuEvent(){
	var objAimRow = objFocusInput.tieupCell.parentElement;
	objUserReportDesignerTableFocusInputContextMenuContainers.style.visibility = "visible";
	objUserReportDesignerTableFocusInputContextMenuContainers.style.left = objFocusInput.offsetLeft - objUserReportDesignerTableContent.scrollLeft + 2;
	objUserReportDesignerTableFocusInputContextMenuContainers.style.top = objFocusInput.offsetTop + 20 - objUserReportDesignerTableContent.scrollTop + 2;
	objUserReportDesignerTableFocusInputContextMenuContainers.innerHTML = "<table style='border:solid 0px black;cursor:default;' cellspacing='1' cellpadding='2' width='100%'><tr><td idx='__hzh_urd_caculateCellVal' onmouseover='this.style.background=\"#000080\";this.style.color=\"white\"' onmouseout='this.style.background=\"#cccccc\";this.style.color=\"black\"'>计算单元格值</td></tr></table>";
	objUserReportDesignerTableFocusInputContextMenuContainers.firstChild.onclick = processUserReportDesignerTableFocusInputContextMenuClickEvent;
}

function processUserReportDesignerTableFocusInputContextMenuClickEvent(){
	var objSrc = event.srcElement;
	var objCell;
	
	if( objSrc.tagName.toLowerCase() == "td"){
		switch(objSrc.idx){
			case "__hzh_urd_caculateCellVal"://计算单元格的值
				inputStyle = 2;
				objUserReportDesignerTableFocusInputContextMenuContainers.style.visibility = "hidden";
				window.showModalDialog("getCaculateVal.htm",window,"dialogWidth:400px;dialogHeight:20px;help:no;center:yes;status:no");				
				
				if(typeof(window.formular) != "undefined" && window.formular != ""){
					processUserReportDesignerInsertBtnclickEvent(window.formular);
					var strFormular = "";
					for( var i = 0; i < objArrCellIndex.length; i ++ ){

						if(objUserReportDesignerTableData.cells(objArrCellIndex[i])){//cell对象
							if( !isNaN(parseFloat(objUserReportDesignerTableData.cells(objArrCellIndex[i]).innerText)) ){
								strFormular += ( typeof(objArrOperator[i]) != "undefined" ) ? ( parseFloat(objUserReportDesignerTableData.cells(objArrCellIndex[i]).innerText) + objArrOperator[i] ) : parseFloat(objUserReportDesignerTableData.cells(objArrCellIndex[i]).innerText);
							}
						}else{//常数
							if( !isNaN(parseFloat(objArrCellIndex[i])) ){
								strFormular += ( i== objArrCellIndex.length - 1 ) ? parseFloat(objArrCellIndex[i]) : parseFloat(objArrCellIndex[i]) + objArrOperator[i];
							}else{
								alert("输入的计算公式有误");
								return;
							}
						}
					}
	
					try{
						objFocusInput.tieupCell.innerText = isNaN(parseFloat(eval(strFormular))) ? "" : Math.round( parseFloat( eval( strFormular ) ) * 100) / 100;
						objFocusInput.style.visibility = "hidden";
						synchronizationXMLDomData(objFocusInput.tieupCell);//同步对象缓存
						window.formular = "";
					}catch(e){
						alert("无法对选择的单元格应用公式");
					}
				}
				break;			
		}
	}
}

function synchronizationXMLDomData(objCell){
	var vEditCellRowIndex = 0;
	var vEditCellColIndex = 0;
	var objAimCellNode = null;
	if(objCell && objCell.tagName.toLowerCase() == "td"){
		vEditCellRowIndex = objFocusInput.tieupCell.parentElement.getAttribute("idx");
		vEditCellColIndex = objFocusInput.tieupCell.getAttribute("idx");
		objAimCellNode = objUserReportXMLData.selectSingleNode("//td[@idx='" + vEditCellColIndex + "' and parent::*/@idx='" + vEditCellRowIndex + "']");
		if(objAimCellNode){
			objAimCellNode.text = objFocusInput.tieupCell.innerText;
		}
	}
}

function hiddenAllFocusActionPannel(){
	objFocusInput.style.visibility = "hidden";
	objUserReportDesignerTableCellContextSecContainers.style.visibility = "hidden";
	objUserReportDesignerTableFocusInputContextMenuContainers.style.visibility = "hidden";
	objUserReportDesignerTableContextMenuContainers.style.visibility = "hidden";
	if(objCurrentRowIndexCell && objCurrentColIndexCell){
		objCurrentRowIndexCell.style.background = "#eeeeee";
		objCurrentColIndexCell.style.background = "#eeeeee";
		objCurrentRowIndexCell = null;
		objCurrentColIndexCell = null;
	}
}

function processUserReportDesignerTableScrollBtnClickEvent(){

	var objTempCell = null;
	try{
		hiddenAllFocusActionPannel();
		switch(event.srcElement.action){			
		
			case "left":
				for( var i = 0; i < objUserReportDesignerTableData.rows.length; i ++ ){
					objTempCell = objUserReportDesignerTableData.rows[i].cells[intFirstColIndex];
					if( objTempCell.colSpan == 1){
						objTempCell.style.display = "none";
					}else{			
						objUniteCellArray[objUniteCellArray.length] = objTempCell;
						for( var j = 0; j < objUniteCellArray.length; j ++ ){
							if(objUniteCellArray[j].colSpan > 1){
								objUniteCellArray[j].colSpan = objUniteCellArray[j].colSpan - 1;
							}
						}
					}
				}
				objUserReportDesignerTableData.style.width = parseInt(objUserReportDesignerTableData.offsetWidth) - parseInt(objUserReportDesignerTableData.rows[0].cells[intFirstColIndex].clientWidth);
				intFirstColIndex += 1;
				break;
				
			case "right":
				intFirstColIndex -= 1;
				for( var i = 0; i < objUserReportDesignerTableData.rows.length; i ++ ){
					objUserReportDesignerTableData.rows[i].cells[intFirstColIndex].style.display = "block";
				}
				objUserReportDesignerTableData.style.width = parseInt(objUserReportDesignerTableData.offsetWidth) + parseInt(objUserReportDesignerTableData.rows[0].cells[intFirstColIndex].clientWidth);
				break;
				
			case "top":
				intFirstRowIndex -= 1;
				for( var i = 0; i < objUserReportDesignerTableData.rows[0].cells.length; i ++ ){
					objUserReportDesignerTableData.rows[intFirstRowIndex].style.display = "block";
				}
				objUserReportDesignerTableData.style.Height = parseInt(objUserReportDesignerTableData.offsetHeight) + parseInt(objUserReportDesignerTableData.rows[intFirstRowIndex].cells[0].clientHeight);
				break;
				
			case "bottom":
				for( var i = 0; i < objUserReportDesignerTableData.rows[0].cells.length; i ++ ){
					objUserReportDesignerTableData.rows[intFirstRowIndex].style.display = "none";
				}
				objUserReportDesignerTableData.style.Height = parseInt(objUserReportDesignerTableData.offsetHeight) - parseInt(objUserReportDesignerTableData.rows[intFirstRowIndex].cells[0].clientHeight);
				intFirstRowIndex += 1; 
				break;
		}
	}catch(e){
		//error process...
	}
}

function processUserReportDesignerFocusInputKeyupEvent(){

	var objSrc = event.srcElement;
	var vRowIndex = objSrc.rowIndex;
	
	var vCellIndex = ( intFirstColIndex == 1 ) ? objSrc.cellIndex : objSrc.cellIndex + intFirstColIndex - 1;
	
	if(	objSrc.tagName.toLowerCase() == "input" ){
		
		try{
			switch(event.keyCode){
				case 37:
					objSrc.cellIndex - 1 != 0 ? showFocusInput(objUserReportDesignerTableData.rows[vRowIndex].cells[vCellIndex - 1]) : showFocusInput(objUserReportDesignerTableData.rows[vRowIndex].cells[intCols - 1]);
					break;
				case 38:
					objSrc.rowIndex - 1 != 0 ? showFocusInput(objUserReportDesignerTableData.rows[vRowIndex - 1].cells[vCellIndex]) : showFocusInput(objUserReportDesignerTableData.rows[intRows].cells[vCellIndex]);
					break;
				case 39:
					showFocusInput(objUserReportDesignerTableData.rows[vRowIndex].cells[vCellIndex + 1]);
					break;
				case 40:
					showFocusInput(objUserReportDesignerTableData.rows[vRowIndex + 1].cells[vCellIndex]);
					break;
			}
		}catch(e){
		
			switch(event.keyCode){
				case 39:
					showFocusInput(objUserReportDesignerTableData.rows[vRowIndex].cells[1]);
					break;
				case 40:
					showFocusInput(objUserReportDesignerTableData.rows[1].cells[vCellIndex]);
					break;
			}
		
		}
	}
}

function processUserReportDesignerTableTopMouseoverEvent(){//设置列索引鼠标样式
	var objSrcElement = event.srcElement;
	if(objSrcElement.tagName.toLowerCase() == "td" && objSrcElement.cellIndex != 0){
		try{
			objSplitLeftRightHandle.style.visibility = "visible";
			objSplitLeftRightHandle.style.left = objSrcElement.nextSibling.offsetLeft - 8;
			objSplitLeftRightHandle.tieupTD = objSrcElement;
		}catch(e){}
	}	
}

function processSplitLeftRightHandleMousedownEvent(){//
	objSplitLeftRightHandle.setCapture(true);
	objSplitLeftRightHandle.attachEvent("onmousemove",processSplitLeftRightHandleMousemoveEvent);
	hiddenAllFocusActionPannel();
	intOffsetVal = 0;
	
	intCurposionX = event.x;
	intCurposionY = event.y;
	
	objSplitLine.style.visibility = "visible";
	objSplitLine.style.left = event.x - 5;
	objSplitLine.style.width = 1;
	objSplitLine.style.top = 0;
	objSplitLine.style.height = objUserReportDesignerTableContainers.offsetHeight - 1;
}

function processSplitLeftRightHandleMousemoveEvent(){
	objSplitLine.style.left = event.x - 5;
	intOffsetVal = event.x - intCurposionX; 
}

function processSplitLeftRightHandleMouseupEvent(){
	objSplitLeftRightHandle.releaseCapture();
	objSplitLeftRightHandle.detachEvent("onmousemove",processSplitLeftRightHandleMousemoveEvent);
	adjustReportByColsIndex();
	objSplitLine.style.visibility = "hidden";
	
}

function adjustReportByColsIndex(){//调整列宽
	var aimTdCellIndex = 0;
	var objSwapTRNode = null;
	var objSwapTDNode = null;
	var objAimTd = objSplitLeftRightHandle.tieupTD;
	try{
		aimTdCellIndex = objAimTd.cellIndex;
		for( var i = 0; i < objUserReportDesignerTableData.rows.length; i ++ ){
			objUserReportDesignerTableData.rows[i].cells[aimTdCellIndex].style.width = parseInt(objUserReportDesignerTableData.rows[i].cells[aimTdCellIndex].offsetWidth) + intOffsetVal;
		}
		objUserReportDesignerTableData.style.width = parseInt(objUserReportDesignerTableData.offsetWidth) + intOffsetVal;
	}catch(e){}
}

function processUserReportDesignerTableLeftMouseoverEvent(){
	var objSrcElement = event.srcElement;
	if(objSrcElement.tagName.toLowerCase() == "td" && objSrcElement.innerText != 1){
		try{
			objSplitTopBottomHandle.style.visibility = "visible";
			objSplitTopBottomHandle.style.top = objSrcElement.offsetTop - 8;
			objSplitTopBottomHandle.tieupTD = objSrcElement.parentElement.previousSibling.cells[0];
		}catch(e){}
	}
}

function processSplitTopBottomHandleMousedownEvent(){
	objSplitTopBottomHandle.setCapture(true);
	objSplitTopBottomHandle.attachEvent("onmousemove",processSplitTopBottomHandleMousemoveEvent);
	hiddenAllFocusActionPannel();
	intOffsetVal = 0;
	
	intCurposionX = event.x;
	intCurposionY = event.y;
	
	objSplitLine.style.visibility = "visible";
	objSplitLine.style.top = event.y - 30;
	objSplitLine.style.height = 1;
	objSplitLine.style.left = 0;
	objSplitLine.style.width = objUserReportDesignerTableContainers.offsetWidth - 1;
}

function processSplitTopBottomHandleMousemoveEvent(){
	objSplitLine.style.top = event.y - 30;
	intOffsetVal = event.y - intCurposionY; 
}

function processSplitTopBottomHandleMouseupEvent(){
	objSplitTopBottomHandle.releaseCapture();
	objSplitTopBottomHandle.detachEvent("onmousemove",processSplitTopBottomHandleMousemoveEvent);
	adjustReportByRowsIndex();
	objSplitLine.style.visibility = "hidden";
}

function adjustReportByRowsIndex(){

	var objAimTd = objSplitTopBottomHandle.tieupTD;
	var objSwapNode = null;
	try{
		objAimTd.style.height = parseInt(objAimTd.offsetHeight) + intOffsetVal;
		var z = parseInt(objUserReportDesignerTableData.offsetHeight) + intOffsetVal;
		objUserReportDesignerTableData.style.height = z;
		objUserReportDesignerTableLeft.style.height = z;
		objRowsSplitePannel.style.height = z;
		
		for( var i = 0; i < objUserReportDesignerTableLeft.rows.length; i ++){
			 if( i != 0 ){
				objSwapNode = objUserReportDesignerTableData.rows[i].cells[0].cloneNode(true);
				objUserReportDesignerTableLeft.rows[i].cells[0].swapNode(objSwapNode);
			}
		}
		
		objUserReportDesignerTableData.rows[0].style.height = 18;
		
	}catch(e){}
}

function processUserReportDesignerTableContentMouseoverEvent(){
	var objSrcElement = event.srcElement;
		if(objSrcElement.tagName.toLowerCase() == "td" && objSrcElement.cellIndex != 0 && objSrcElement.parentElement.rowIndex != 0){
			curCellRowIndex = objSrcElement.parentElement.rowIndex;
			curCellCellIndex = objSrcElement.cellIndex;
		}else if(objSrcElement.tagName.toLowerCase() == "td" && objSrcElement.cellIndex == 0 && objSrcElement.parentElement.rowIndex != 0){
			if(objSrcElement.parentElement.rowIndex == intRows){//
				objSplitTopBottomHandle.style.visibility = "visible";
				objSplitTopBottomHandle.style.top = objSrcElement.offsetTop + objSrcElement.offsetHeight - 10;
				objSplitTopBottomHandle.tieupTD = objSrcElement;
			}else{
				try{
					objSplitTopBottomHandle.style.visibility = "visible";
					objSplitTopBottomHandle.style.top = objSrcElement.parentElement.nextSibling.offsetTop - 8 - objUserReportDesignerTableContent.scrollTop;
					objSplitTopBottomHandle.tieupTD = objSrcElement.parentElement.cells[0];
				}catch(e){}
			}
			
			if( objSrcElement.isEndRow ) objSrcElement.title = "取消该行为最终列所在行...";
			
		}else if(objSrcElement.tagName.toLowerCase() == "td" && objSrcElement.cellIndex != 0 && objSrcElement.parentElement.rowIndex == 0){
			try{
				objSplitLeftRightHandle.style.visibility = "visible";
				objSplitLeftRightHandle.style.left = objSrcElement.nextSibling.offsetLeft - 8 - objUserReportDesignerTableContent.scrollLeft;
				objSplitLeftRightHandle.tieupTD = objSrcElement;
			}catch(e){}
			if(objSrcElement.cellIndex == intCols - 1){//
				objSplitLeftRightHandle.style.visibility = "visible";
				objSplitLeftRightHandle.style.left = objSrcElement.offsetLeft + objSrcElement.offsetWidth - 10;
				objSplitLeftRightHandle.tieupTD = objSrcElement;
			}
		}
}

function processUserReportDesignerTableContentMouseoutEvent(){
	try{
		objUserReportDesignerTableTop.rows[0].cells[curCellCellIndex].style.background = "#eeeeee";
		objUserReportDesignerTableLeft.rows[curCellRowIndex].cells[0].style.background = "#eeeeee";
	}catch(e){}
}

function processUserReportDesignerTableContentScrollEvent(){

}

function refresh(){

	try{
		with(objUserReportDesignerContainers.style){
			width = window.document.body.offsetWidth - 4;
			height = window.document.body.offsetHeight - 4 - intHeight;
		}	
		with(element.style){
			width = window.document.body.offsetWidth - 14;
			height = window.document.body.offsetHeight - 14 - intHeight;
		}
		with(objUserReportDesignerTitleContainers.style){
			width = element.offsetWidth - 2;
		}
		with(objUserReportDesignerEditWin.style){
			width = element.offsetWidth - 260;
		}
		
		with(objUserReportDesignerTableContainers.style){
			width = element.offsetWidth - 2;
			height = element.offsetHeight - 24;
		}
		with(objUserReportDesignerTableContent.style){
		/*
			width = element.offsetWidth - 3 - 20;
			height = element.offsetHeight - 45;
			*/
			width = element.offsetWidth - 5;
			height = element.offsetHeight - 28;
		}
		with(objColsSplitePannel.style){
			width = (objUserReportDesignerTableContent.clientWidth < objUserReportDesignerTableContent.offsetWidth) ? ( objUserReportDesignerTableData.offsetWidth - (objUserReportDesignerTableContent.offsetWidth - objUserReportDesignerTableContent.clientWidth) - 22 ) : objUserReportDesignerTableData.offsetWidth;
		}
		with(objUserReportDesignerTableBottomScrollContainers.style){
			top = objUserReportDesignerTableContent.offsetHeight + 2;
			width = objUserReportDesignerTableContent.offsetWidth + 1;
		}
		with(objUserReportDesignerTableRightScrollContainers.style){
			height = objUserReportDesignerTableContent.offsetHeight + 1;
		}
		with(objUserReportDesignerTableBottomScrollBtn.style){
			top = objUserReportDesignerTableContent.offsetHeight - 17;
		}
		with(objRowsSplitePannel.style){
			height = objUserReportDesignerTableContent.clientHeight;
		}
		with(objUserReportDesignerBottomScrollbar.style){
			if(objUserReportDesignerTableContent.clientWidth < objUserReportDesignerTableData.offsetWidth){
				width = objUserReportDesignerTableContent.clientWidth-(objUserReportDesignerTableData.offsetWidth - objUserReportDesignerTableContent.clientWidth);
			}else{
				width = objUserReportDesignerTableContainers.offsetWidth - 54;
			}
		}
		
		if(objUserReportDesignerTableContent.clientWidth < objUserReportDesignerTableData.offsetWidth){
			//scroll
		}else{
			//
		}
	}catch(e){}
	
}

function putHeight(){
	var argHeight=element.height;
	intHeight = isNaN(parseInt(argHeight)) ? 0 : parseInt(argHeight);
}

/////////////////////合并处理//////////////////

function processUserReportDesignerTableContentMousemoveEvent(){
	endTd = event.srcElement;
	if( stat =='start'&& endTd.tagName.toLowerCase() == 'td'){
		if(endTd.cellIndex == 0 || endTd.parentElement.rowIndex == 0 ||  endTd.parentElement.rowIndex >= intEndRowRowIndex) return;
		refreshSelect();
	}
}

function processUserReportDesignerTableContentMousedownEvent(){
	objSrc =event.srcElement;
	if( objSrc.tagName.toLowerCase() == "td" ){
		if(objSrc.cellIndex == 0 || objSrc.parentElement.rowIndex == 0) return;
		stat = "start";//选中单元格（onMouseDown），选取要合并的单元格，确定要合并的单元格
		startTd = objSrc;
		endTd = objSrc;
	}
}

function processUserReportDesignerTableContentMouseupEvent(){
	var objSrc = event.srcElement;
	if(objSrc.tagName.toLowerCase() == "td"){
		if(objSrc.cellIndex == 0 || objSrc.parentElement.rowIndex == 0) return;
		stat = "";	
		refreshSelect();
	}
}

function refreshSelect(){
	
	if( startTd && endTd && startTd.parentElement && endTd.parentElement){
		var cStartIdx = parseInt(startTd.getAttribute("idx")) < parseInt(endTd.getAttribute("idx")) ? startTd.getAttribute("idx") : endTd.getAttribute("idx");
		var rStartIdx = parseInt(startTd.parentElement.getAttribute("idx")) < parseInt(endTd.parentElement.getAttribute("idx")) ? startTd.parentElement.getAttribute("idx") : endTd.parentElement.getAttribute("idx");
		
		var cEndIdx = parseInt(startTd.getAttribute("idx")) > parseInt(endTd.getAttribute("idx")) ? startTd.getAttribute("idx") : endTd.getAttribute("idx");
		var rEndIdx = parseInt(startTd.parentElement.getAttribute("idx")) > parseInt(endTd.parentElement.getAttribute("idx")) ? startTd.parentElement.getAttribute("idx"):endTd.parentElement.getAttribute("idx");
		
		groupWideLength = cEndIdx - cStartIdx + 1;
		groupHeightLength = rEndIdx - rStartIdx + 1;
		
		var selectStr = String.fromCharCode( parseInt(cStartIdx) + 96 ) + rStartIdx + ":" + String.fromCharCode( parseInt(cEndIdx) + 96 ) + rEndIdx;
		
		curSelectCells = objUserReportDesignerTableData.cells(selectStr);
		delete lastSelectCellArray;
		
		lastSelectCellArray = new Array();
		
		if(cellArray){
			for( i = 0;i < cellArray.length; i ++ ){
				lastSelectCellArray.push(cellArray[i]);
			}
		}
		
		delete  cellArray;
		cellArray = new Array();

		if(curSelectCells && curSelectCells.length){
			for( i = 0 ;i< curSelectCells.length; i ++ ){
				cellArray.push(curSelectCells.item(i));
			}
		}else{
			cellArray.push(curSelectCells);
		}
		refreshSelectCells();	
	}	
}

function refreshSelectCells(){

	try{
		if(lastSelectCellArray != null){
			for(i =0;i<lastSelectCellArray.length;i++){
				lastSelectCellArray[i].style.background = "white";
			}
		}
		if(cellArray != null){
			for(i = 0; i< cellArray.length;i++){	
				cellArray[i].style.background = "#9999cc";
			}
		}
	}catch(e){}

	var it = cellArray.length;
	for (i = 0 ;i < it;i++){
		var tempI = cellArray[i];
		if(!tempI) return;
		if (tempI.colSpan > 1 || tempI.rowSpan > 1 ){
			intActionType = 1;
			break;
		}
	}
	/////////////////////////
}

function processUserReportDesignerUniteBtnclickEvent(){
	hiddenAllFocusActionPannel();
	if(doUnite()){
		if(intEndRowRowIndex == 0){
			alert("请首先设置最终列所在行后再进行此操作");
			return;
		}else{
			if(endTd.parentElement.rowIndex > intEndRowRowIndex){
				alert("不能对最终列所在行以下的单元格进行合并操作");
				return;
			}
		}
		var items = cellArray.length;
		for( i = 0; i < items; i ++ ){
			var tempItem = cellArray.pop();
			if( i == ( items - 1 ) ){
				tempItem.colSpan = groupWideLength;
				tempItem.style.width = groupWideLength * 60;
				tempItem.rowSpan = groupHeightLength;
				tempItem.style.height = groupHeightLength * 18;
				
				tempItem.colUnit = groupWideLength;
				tempItem.rowUnit = groupHeightLength;
				
				tempItem.style.background = strUnitBackcolor;
				
			}else{
				tempItem.removeNode(true);
			}
		}
	}else{
		judge();
	}
}

function doUnite(){
	var booleanUnite = true;
	for( var i = 0; i < cellArray.length; i ++ ){
		if(!cellArray[i]) return;	
		if(cellArray[i].getAttribute("colSpan") > 1 || cellArray[i].getAttribute("rowSpan") > 1){
			booleanUnite = false;
			break;
		}
	}
	return booleanUnite;
}

function groupCells(){
	var items = cellArray.length;
	for( i = 0; i < items; i ++ ){
		var tempItem = cellArray.pop();
		if( i == ( items -1 ) ){
			tempItem.colSpan = groupWideLength;
			tempItem.rowSpan = groupHeightLength;
			tempItem.colUnit = groupWideLength;
			tempItem.rowUnit = groupHeightLength;
			tempItem.style.background = "white";
		}else{
			tempItem.removeNode(true);
		}
	}
}


/////////////////////////////////////////////
function judge(){
	intActionType == 1 ? reserverCells() : groupCells();
	intActionType = 0;
}

function reserverCells(){
	var items = cellArray.length;
	for (i = 0 ;i < items;i++){
		var tempItem = cellArray[i];

		if (tempItem.colSpan > 1 || tempItem.rowSpan > 1 ){
			var j = 0;
			for(j = 1;j < tempItem.colSpan ;j++){
				var oNewNode = element.document.createElement("TD");
				var newNode = tempItem.insertAdjacentElement("afterEnd",oNewNode);
				newNode.style.background = "white";
				newNode.idx = parseInt(tempItem.idx) + parseInt(tempItem.colSpan) - j;
			}

			var n = tempItem.parentElement.getAttribute("idx");
			var s = tempItem.idx;
			for(m =1 ;m < tempItem.rowSpan;m++){
				if (parseInt(s) == 1)
					{s = 2;}
				var temp = objUserReportDesignerTableData.rows(parseInt(n) + parseInt(m) - 1).cells(parseInt(s) - 1 - 1);
				
				if (temp == null){
					temp = objUserReportDesignerTableData.rows(parseInt(n) + parseInt(m) - 1);
					temp.height = 18;	
					var oNewNode = element.document.createElement("TD");
					temp = temp.appendChild(oNewNode);
					temp.style.background = "white";
					temp.height = 18;
					temp.width = 60;
					temp = objUserReportDesignerTableData.rows(parseInt(n) + parseInt(m) - 1).cells(0);
					temp.idx = 1;
					for(j = 1;j < tempItem.colSpan ;j++){
						oNewNode = element.document.createElement("TD");
						var newNode = temp.insertAdjacentElement("afterEnd",oNewNode);
						newNode.style.background = "white";
						newNode.idx = parseInt(temp.idx)  +  parseInt(tempItem.colSpan) - j;
						}
					}
				else if(parseInt(tempItem.colSpan) + 1 == parseInt(temp.idx) ){				
						for (j = 1;j < temp.idx ;j++){
							oNewNode = element.document.createElement("TD");
							var newNode = temp.insertAdjacentElement("beforeBegin",oNewNode);
							newNode.style.background = "white";
							newNode.idx = j ;
						}
					}
				else{
					for(j = 1;j < tempItem.colSpan + 1 ;j++){
						var oNewNode = element.document.createElement("TD");
						var newNode = temp.insertAdjacentElement("afterEnd",oNewNode);
						newNode.style.background = "white";
						newNode.idx = parseInt(temp.idx) + parseInt(tempItem.colSpan) - j + 1;
					}
				}
			}
			tempItem.colSpan = 1;
			tempItem.rowSpan = 1;
			
			tempItem.colUnit = 1;
			tempItem.rowUnit = 1;
			
			tempItem.height = 18;
			tempItem.width = 60;
			
			tempItem.style.background = "white";
			
		}
	}
}

function processUserReportDesignerInsertBtnclickEvent(arg){

	while( objArrCellIndex.length ){
		objArrCellIndex.pop();
	}
	
	while( objArrOperator.length ){
		objArrOperator.pop();
	}
	var s = ( typeof(arg) == "undefined" ) ?  objUserReportDesignerEditWin.value : arg;
	if( s == "" ) return;
	
	var i,temp,m,n;
		
	for (i = 0;i < s.length;i++){
		temp = s.substr(i,1); 
		if (temp == " ") {continue;}
		if ((temp >= "A" && temp <= "Z") || (temp >= "a" && temp <= "z")){
			m = objArrCellIndex.length;
			var b;
			if ( i == 0) { 
				objArrCellIndex[m] = temp;
			}else{
				b = s.substr(i - 1,1);
				if((b >= "A" && b <= "Z") || (b >= "a" && b <= "z")){
					objArrCellIndex[m - 1] = objArrCellIndex[m - 1] + temp;
				}else{
					objArrCellIndex[m] = temp;
				}
			}
		}
		
		if (temp >= "0" && temp <= "9"){
			m = objArrCellIndex.length;
			var a;
			for (n = 1 ; n < i ;n++){
				a = s.substr(i - n,1);
				if (a != " "){break;}
			}
			if (a == "+" || a == "-" || a == "*" || a == "/"){
				objArrCellIndex[m] =  temp; 
			}else{
				objArrCellIndex[m - 1 ] = objArrCellIndex[m - 1 ] + temp;
			}
		}

		if(temp == "+" || temp == "-" || temp == "*" || temp == "/"){
			n = objArrOperator.length;
			objArrOperator[n] = temp;
		}
	}

	var objChar,objNum,objChar1;

	for(i = 0;i < objArrCellIndex.length; i++){
		temp = objArrCellIndex[i];
		
		objChar = temp.substr(0,1); 
		if (objChar >= "0" && objChar <= "9") continue;
		
		objChar = "";
		objChar1 = "";
		objNum = "";
		for (n = 0;n < temp.length;n++){
			objChar = temp.substr(n,1); 
			//字符数字分开
			if ((objChar >= "A" && objChar <= "Z") || (objChar >= "a" && objChar <= "z")){
				objChar1 = objChar1 + objChar ;
			}else if (objChar >= "0" && objChar <= "9"){
				objNum = objNum + objChar ;
			}
		}
		//增加
		objNum = (parseInt(objNum) + 1).toString(10);
		if (objChar1.length == 1){
			if (objChar1 == "z" || objChar1 == "Z"){
				objChar1 = "AA";
			}else{ //自增
				objChar1 = String.fromCharCode(parseInt(objChar1.charCodeAt(0)) + 1);
			}
		}

		if (objChar1.length == 2){
			objChar = objChar1.substr(1,1);
			if (objChar == "z" || objChar == "Z"){
				objChar1 = String.fromCharCode(parseInt(objChar1.charCodeAt(0)) + 1);
				objChar1 = objChar1 + "A";
			}else{
				objChar1 = objChar1.substr(0,1) + String.fromCharCode(parseInt(objChar.charCodeAt(0)) + 1 );
			}
		}
		objArrCellIndex[i] = objChar1 + objNum;
	}
}

function refreshView(){
	getReportView(objUserReportXMLData);
}

function putRows(){
	var argRows=element.rows;
	intRows = isNaN( parseInt(argRows) ) ? 30 : argRows;
}

function putCols(){
	var argCols=element.cols;
	intCols = isNaN( parseInt(argCols) ) ? 17 : argCols;
}

function putDicXMLData(arg){
	window.deptDicData = __objGlobalCommonInst.inputXML(arg);
}

function putDataSourceXML(){
	objUserReportXMLData = __objGlobalCommonInst.inputXML(element.dataSource);
	getReportView(objUserReportXMLData);
}

function putKey(){
	strReportKey = element.key;
}

function putCaption(){
	strCaption = element.caption;
}

	__jhtcBindPropertyChange(element,"dataSource",putDataSourceXML);
	__jhtcBindPropertyChange(element,"key",putKey);
	__jhtcBindPropertyChange(element,"caption",putCaption);
	__jhtcBindPropertyChange(element,"height",putHeight);
	__jhtcBindPropertyChange(element,"rows",putRows);
	__jhtcBindPropertyChange(element,"cols",putCols);

  	jhtc_attr_init(element,"dataSource","");
  	jhtc_attr_init(element,"commonHTML","");
  	jhtc_attr_init(element,"key","");
  	jhtc_attr_init(element,"caption","");
  	jhtc_attr_init(element,"height","");
  	jhtc_attr_init(element,"rows","");
  	jhtc_attr_init(element,"cols","");
  	
	element.refreshView=refreshView;
	element.init=initialize;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_tag_map["urd_view"]=jhtc_URD_View;