	// data format: 默认第一个为是否未级标志，第二个是否选中标志
	// data formate:<tr><td></td></td></td></td>
function jhtc_treeTableCtn(win,jhtc_obj){
		var pageWin=win;
		var element=jhtc_obj;
		var REQUEST_PARA_TAG="parentCode"
		var SELECT_BG_COLOR="#9999FF";
		var MOUSEOVER_BG_COLOR="#009999";
		var MAX_COL=100;
		var SHOW_FIRST_MINU="-1";
		var TR_STRAT_INDEX=1;
		var oldSelectedTreeItemSpan=null;
		var treeTable=null;
		var treeHeadXsl=null;
		var treeDataXsl=null;
		
		function init() {
			if(element.tagname==null){
				if(element.kind=="check"||element.kind=="lastcheck")
					element.tagname="CHECKBOX_VALUE";
			}
			if(element.headXslFile==""){
				alert("Please set property:headXslFile!");
				return ;
			}
			treeHeadXsl = new ActiveXObject("Microsoft.XMLDOM");
			treeHeadXsl.async=false;
			treeHeadXsl.load(element.headXslFile);
			
			
			treeDataXsl = new ActiveXObject("Microsoft.XMLDOM");
			treeDataXsl.async=false;
			if(element.dataXslFile==""){
				var xsl=""
				xsl+="<?xml version=\"1.0\" encoding=\"GBK\"?>";
				xsl+="<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">";
				xsl+="	<xsl:template match=\"/\">";
				xsl+="		<xsl:for-each select=\"/root/tbody/tr\">";
				xsl+="			<tr>";
				xsl+="			<xsl:attribute name=\"TREEITEMPK\"><xsl:for-each select=\"pk/";
				xsl+="*\">&lt;<xsl:value-of select=\"name()\"/>&gt;<xsl:value-of select=\".\"/>&lt;/<xsl:value-of select=\"name()\"/>&gt;</xsl:for-each></xsl:attribute>";
				xsl+="			<xsl:attribute name=\"TREEITEMISLEAF\"><xsl:value-of select=\"td[1]\"/></xsl:attribute>";
				if(element.kind!="")
					xsl+="			<xsl:attribute name=\"TREEITEMCHECK\"><xsl:value-of select=\"td[2]\"/></xsl:attribute>";
				xsl+="				<xsl:for-each select=\"td[position()!=1]\">";
				xsl+="					<td  nowarp=\"true\"><xsl:value-of select=\".\"/></td>";
				xsl+="				</xsl:for-each>";
				xsl+="			</tr>";
				xsl+="		</xsl:for-each>";
				xsl+="	</xsl:template>";
				xsl+="</xsl:stylesheet>";
			    treeDataXsl.loadXML(xsl);
			}else
				treeDataXsl.load(element.dataXslFile);

			init_element(element,null,"");
			adjPosition()
    		window.attachEvent("onresize", adjPosition);
		}
		function init_element(e,sqlid,sqlpara){
			var tableHtml="";
			tableHtml+="<table  class='mainTable'  border='1' cellpadding='0' cellspacing='0'>";
    		tableHtml+=getHeadHtmlData(e.headid,sqlpara);
    		tableHtml+="</table>";

    		jhtcSetHtml(e,tableHtml);
    		if(sqlid==null)
    			return ;
    		var tables=e.getElementsByTagName("table");
    		treeTable=tables[0];
    		inittable(tables[0],{"headid":e.headid,"dataid":sqlid,"datapara":sqlpara,"kind":e.kind, "action":element.action,"selectedColor":SELECT_BG_COLOR,"mouseOverColor":MOUSEOVER_BG_COLOR,"imageWidth":"19","imageHeight":"16"});
		}
		function getCode(){
			return treeTable.getCode();
		}
		function getLabel(){
			return treeTable.getLabel();
		}
		function refresh(sqlid, para, isTurn, subFunc, hideMsg){
			init_element(element,sqlid,para);
		}
		//part 2 begin
		function inittable(table,paras){
			for(var k in paras){
				table["_tree"+k]=paras[k];
			}
			var trs=table.getElementsByTagName("tr");
			table["_treeSelectedTr"]=null;
			table["_treeFirstTr_"]=trs[trs.length-1];
			loadRoot(table);
		}
		function loadRoot(table){
			var tr=table["_treeFirstTr_"].nextSibling;
			var ntr;
			while(tr!=null){
				ntr=tr.nextSibling;
				tr.parentNode.removeChild(tr);
				tr=ntr;
			}
			table["_treeSelectedTr"]=null;
			var trData=getTrData(0,"",table,table["_treedatapara"]);
			addTreeItems(table,table["_treeFirstTr_"],trData);
		}
		function addTreeItems(table,parentTr,trData){
			var parentSiblingTr=parentTr.nextSibling;
			var previousSiblingTr=parentTr;
			var tempDiv=element.document.createElement("div");
			tempDiv.innerHTML="<table></table>";
			var trs=trData;
			if(trs==null)
				return ;
			if(trs.length==0)
				return ;
			var tr=trs[0];
			var ntr;
			var firstCheckTr=null;

			while(tr!=null){
				if(firstCheckTr==null)
					firstCheckTr=tr;
				ntr=tr.nextSibling;
				if(ntr==null)
					tr["_treeIsLast"]=true;
				else
					tr["_treeIsLast"]=false;
				var span=tr.getElementsByTagName("span")[0];
				
				var imgs=tr.getElementsByTagName("img");
				tr["_treeTable"]=table;
				tr["_treeState"]=null;
				tr["_treeSpan"]=span;
				tr["_treeDisplay"]="block";
				tr["_treeLoadChild"]=1;
				span["_treeTr"]=tr;
				for(var i=imgs.length-1;i>=0;i--){
					var it=imgs[i].getAttribute("treeImgType");
					if(it=="icon"){
						tr["_treeIcon"]=imgs[i];
						imgs[i]["_treeOpenSrc"]=tr["_treeImgOpenSrc"];
						imgs[i]["_treeCloseSrc"]=tr["_treeImgCloseSrc"];
					}
					if(it=="state"){
						tr["_treeState"]=imgs[i];
						imgs[i]["_treeOpenSrc"]=tr["_treeStateOpenSrc"];
						imgs[i]["_treeCloseSrc"]=tr["_treeStateCloseSrc"];
					}
					imgs[i]["_treeTr"]=tr;
				}
				previousSiblingTr.parentNode.insertBefore(tr,parentSiblingTr);
				previousSiblingTr=tr;
				if(parentTr==table["_treeFirstTr_"])
					initTrFun(null,tr);
				else
					initTrFun(parentTr,tr);
				tr=ntr;
			}
			if(firstCheckTr!=null&&firstCheckTr["_treeCheck"]!="undef")
				firstCheckTr.updateParentCheckState();
		}
		function initTrFun(parentTr,tr){
			tr["_treeParentItem"]=parentTr;
			tr.getPrefixTd=function(){
				var str="";
				var p=this;
				while(p!=null&&p["_treeLevel"]!=SHOW_FIRST_MINU){
					if(p["_treeIsLast"]==true)
						str="B"+str;
					else
						str="L"+str;
					p=p["_treeParentItem"];	
				}
				return str;
			}
			tr.getPk=function(){
				return this["_treeCode"];
			}
			initTrFunLabel(tr);
			if(tr["_treeType"]=="folder")
				initTrFunFolder(tr);
			if(tr["_treeCheck"]!="undef")
				initTrCheckFun(tr);
		}
		function initTrFunLabel(tr){
			tr.select=function(sel){
				if(sel==true){
					if(this["_treeTable"]["_treeSelectedTr"]!=null)
						this["_treeTable"]["_treeSelectedTr"].select(false);
					this["_treeTable"]["_treeSelectedTr"]=this;
					this["_treeSpan"]["_oldSelColor"]=this["_treeSpan"].style["background"];
					this["_treeSpan"].style["background"]=this["_treeTable"]["_treeselectedColor"];
				}else{
					this["_treeTable"]["_treeSelectedTr"]=null;
					this["_treeSpan"].style["background"]=this["_treeSpan"]["_oldSelColor"];
				}
			}
			tr["_treeSpan"].onclick=function(){
				this["_treeTr"].select(true);
				if(this["_treeTr"]["_treeCode"]!=""&&this["_treeTr"]["_treeTable"]["_treeaction"]!=null&&this["_treeTr"]["_treeTable"]["_treeaction"]!=""){
					var fun=eval(this["_treeTr"]["_treeTable"]["_treeaction"]);
					fun(this["_treeTr"]["_treeType"]=="file"?true:false,this["_treeTr"]["_treeCode"],this.innerText);
				}
			}
			tr["_treeSpan"].onmouseover=function(){
				this["_oldMouseColor"]=this.style["color"];
				this.style["color"]=this["_treeTr"]["_treeTable"]["_treemouseOverColor"];
			}
			tr["_treeSpan"].onmouseout=function(){
				this.style["color"]=this["_oldMouseColor"];
			}
		}
		function initTrCheckFun(tr){
			tr.setCheckState=function(check){
					this["_treeCheck"]=check;
					if(check=="0"){
						this["_treeIcon"].src=this["_treeIcon"]["_treeCloseSrc"];
					}else{
						this["_treeIcon"].src=this["_treeIcon"]["_treeOpenSrc"];
					}
				}
			tr.getChildCheckState=function(){
					var l=this["_treeLevel"];
					tr=this.nextSibling;
					var res="1";
					while(tr!=null&&parseInt(tr["_treeLevel"])>parseInt(l)){
						if(parseInt(tr["_treeLevel"])==parseInt(l+1)){
							if(tr["_treeCheck"]=="undef")
								continue;
							if(tr["_treeCheck"]=="0")
								res="0";
						}
						tr=tr.nextSibling;
					}	
					return res;
				}
			tr.updateParentCheckState=function(){
					var tr=this;
					tr=tr["_treeParentItem"];
					if(tr==null||typeof(tr.setCheckState)=="undefined")
						return ;
					while(tr!=null){
						tr.setCheckState(tr.getChildCheckState());
						tr=tr["_treeParentItem"];
					}
				}
			tr["_treeIcon"].onclick=function(){
					var newCheck;
					if(this["_treeTr"]["_treeCheck"]=="1"){
						newCheck="0";
					}else{
						newCheck="1";
					}
					this["_treeTr"].setCheckState(newCheck);

					//update child state
					var tr=this["_treeTr"];
					var l=tr["_treeLevel"];
					tr=tr.nextSibling;
					while(tr!=null&&parseInt(tr["_treeLevel"])>parseInt(l)){
						tr.setCheckState(newCheck);
						tr=tr.nextSibling;
					}	
					this["_treeTr"].updateParentCheckState();
				}
		}
		function initTrFunFolder(tr){
			tr.showChildren=function(show){
				if(show==true){
					if(this["_treeCheck"]=="undef")
						this["_treeIcon"].src=this["_treeIcon"]["_treeOpenSrc"];
					if(this["_treeState"]!=null)
						this["_treeState"].src=this["_treeState"]["_treeOpenSrc"];
					this["_treeHideChild"]=false;
				}else{
					if(this["_treeCheck"]=="undef")
						this["_treeIcon"].src=this["_treeIcon"]["_treeCloseSrc"];
					if(this["_treeState"]!=null)
						this["_treeState"].src=this["_treeState"]["_treeCloseSrc"];
					this["_treeHideChild"]=true;
				}
				showHideChild(this,show);
			}
			tr.loadChildren=function(){
				var trData=getTrData(parseInt(this["_treeLevel"])+1,this.getPrefixTd(),tr["_treeTable"],tr["_treeCode"]);
				this["_treeLoadChild"]=2;
				addTreeItems(tr["_treeTable"],this,trData);
				this["_treeLoadChild"]=3;
				if(element.afterload!=""){
					var l=this["_treeLevel"];
					ctr=this.nextSibling;
					var childs=new Array();
					var i=0;
					while(ctr!=null&&parseInt(ctr["_treeLevel"])>parseInt(l)){
						childs[i]=ctr;
						ctr=ctr.nextSibling;
						i++;
					}	
					var fun=eval(element.afterload);
					fun(tr,childs);
				}
				this.showChildren(true);
			}
			tr.doShowChildren=function(){
				var show=true;
				show=this["_treeHideChild"];
				if(this["_treeLoadChild"]==3)
					this.showChildren(show);
				else if(this["_treeLoadChild"]==1&&show==true){
					this.loadChildren();
				}
			}
			if(tr["_treeState"]!=null)
				tr["_treeState"].onclick=function(){
					this["_treeTr"].doShowChildren();
				}
			if(tr["_treeCheck"]=="undef")
				tr["_treeIcon"].ondblclick=function(){
					this["_treeTr"].doShowChildren();
				}
		}
		function showHideChild(tr,show){
			var level=parseInt(tr["_treeLevel"])+1;
			var ts=tr.nextSibling;
			while(ts!=null&&ts["_treeLevel"]>=level){
				if(show==true){
					if(ts["_treeLevel"]==level){
						ts.style["display"]="block";
						ts["_treeDisplay"]="block";
					}else
						ts.style["display"]=ts["_treeDisplay"];
				}else{
					ts.style["display"]="none";
					if(ts["_treeLevel"]==level){
						ts["_treeDisplay"]="none";
					}
				}
				ts=ts.nextSibling;
			}
		}
		function getCheckPks(){
			var trs=treeTable.getElementsByTagName("tr");
			if(trs.length==0)
				return "";
			var res="";
			for(var i=0;i<trs.length;i++){
				if(trs[i].getAttribute("_treeCheck")!=null&&trs[i].getAttribute("_treeCheck")!=trs[i].getAttribute("_treeOldCheck"))
					res+="<record>"+trs[i]["_treeCode"]+"<"+element.tagname+">"+trs[i].getAttribute("_treeCheck")+"</"+element.tagname+"></record>";
			}
			return res;
		}
		//part 3 begin
		function getHeadHtmlData(id,para){
			if(id=="")
				data="<root/>";
			else{
				xmlhttp.post(id,para,"?isCheck=false");
				data = xmlhttp._object.responseXML;
			}
			var objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");
            objXMLDoc.async = false;
            objXMLDoc.loadXML(data);
            return objXMLDoc.transformNode(treeHeadXsl);
		}
		function getTrData(level,prefixTd,paras,itemCode){
			var data="";
			xmlhttp.post(paras["_treedataid"],itemCode);
			data = xmlhttp._object.responseXML;
			window.doMsg(data.xml);
			var tempDiv=element.document.createElement("div");
			tempDiv.innerHTML="<table>"+data.transformNode(treeDataXsl)+"</table>";

			var trs=tempDiv.getElementsByTagName("tr");
			if(trs.length==0)
				return null;

			if(typeof(data)=="undefined")
				return null;
			var img_blank=getImagePath("blank.png");
			var img_vline=getImagePath("vline.png");
			var img_file=getImagePath("file.png");
			var img_folder_open=getImagePath("folderopen.png");
			var img_folder_close=getImagePath("folderclose.png");
			var img_lline_leaf=getImagePath("L.png");
			var img_lline_open=getImagePath("Lminus.png");
			var img_lline_close=getImagePath("Lplus.png");
			var img_tline_leaf=getImagePath("T.png");
			var img_tline_open=getImagePath("Tminus.png");
			var img_tline_close=getImagePath("Tplus.png");
			
			//prefixTd=prefixTd.replace(/B/g,"<td width='"+paras["_treeimageWidth"]+"' height='"+paras["_treeimageHeight"]+"'><img src='"+img_blank+"'/></td>");
			//prefixTd=prefixTd.replace(/L/g,"<td width='"+paras["_treeimageWidth"]+"' height='"+paras["_treeimageHeight"]+"'><img src='"+img_vline+"'/></td>");
			prefixTd=prefixTd.replace(/B/g,"<img src='"+img_blank+"'/>");
			prefixTd=prefixTd.replace(/L/g,"<img src='"+img_vline+"'/>");
			var trsStr="",otherTds;
			var v_code,v_label,v_leaf,v_check;
			var i_state_o,i_state_c,i_image_o,i_image_c,i_image_s,i_label,i_type;
			var pas=trs;

			for(var i=0;i<pas.length;i++){
				v_code=pas[i].getAttribute("TREEITEMPK");
				v_label=pas[i].cells[0].innerHTML;
				v_leaf=pas[i].getAttribute("TREEITEMISLEAF");
				v_check=pas[i].getAttribute("TREEITEMCHECK");
				otherTds=pas[i].innerHTML;

				if(v_leaf=="1")
					i_type="file";
				else
					i_type="folder";
				if(i==(pas.length-1)){
					if(v_leaf==1){
						i_state_o=img_lline_leaf;
						i_state_c=img_lline_leaf;
						i_image_o=img_file;
						i_image_c=img_file;
					}else{
						i_state_o=img_lline_open;
						i_state_c=img_lline_close;
						i_image_o=img_folder_open;
						i_image_c=img_folder_close;
					}
				}else{
					if(v_leaf==1){
						i_state_o=img_tline_leaf;
						i_state_c=img_tline_leaf;
						i_image_o=img_file;
						i_image_c=img_file;
					}else{
						i_state_o=img_tline_open;
						i_state_c=img_tline_close;
						i_image_o=img_folder_open;
						i_image_c=img_folder_close;
					}
				}
				i_image_s=i_image_c;
				if(paras["_treekind"]=="check"||paras["_treekind"]=="lastcheck"&&v_leaf==1){
					img_folder_open=getImagePath("checkset.png")
					img_folder_close=getImagePath("checknul.png");
					i_image_o=img_folder_open;
					i_image_c=img_folder_close;
					if(v_check=="0")
						i_image_s=i_image_c;
					else
						i_image_s=i_image_o;
				}else
					v_check="undef";
				if(level>SHOW_FIRST_MINU){
					//i_state="<td width='"+paras["_treeimageWidth"]+"' height='"+paras["_treeimageHeight"]+"'><img src='"+i_state_c+"'  style='cursor:hand'/></td>";
					i_state="<img src='"+i_state_c+"' treeImgType='state'  style='cursor:hand'/>"
				}else
					i_state="";
				
				//i_image="<td width='"+paras["_treeimageWidth"]+"' height='"+paras["_treeimageHeight"]+"'><img src='"+i_image_c+"'  style='cursor:hand'/></td>";
				//i_label="<td colspan='"+(100-level)+"'  align='left'  nowrap='true'><span style='cursor:hand'>"+v_label+"</span></td>";
				i_image="<img src='"+i_image_s+"'  treeImgType='icon'   style='cursor:hand'/>";
				i_label="<span style='cursor6:hand'>&nbsp;"+v_label+"</span>";
				i_ccc="_A_";
				/// state,image,code 5
				i_class=addTdPara(pas[i],"_treeLevel",level)
					+addTdPara(pas[i],"_treeStateOpenSrc",i_state_o)
					+addTdPara(pas[i],"_treeStateCloseSrc",i_state_c)
					+addTdPara(pas[i],"_treeImgOpenSrc",i_image_o)
					+addTdPara(pas[i],"_treeImgCloseSrc",i_image_c)
					+addTdPara(pas[i],"_treeCode",v_code)
					+addTdPara(pas[i],"_treeCheck",v_check)
					+addTdPara(pas[i],"_treeOldCheck",v_check)
					+addTdPara(pas[i],"_treeHideChild",true);
					addTdPara(pas[i],"_treeType",i_type);
				pas[i].cells[0].innerHTML=prefixTd+i_state+i_image+i_label;

			}
			return trs;
		}
		function getImagePath(name){
			return window.prefix+"base/themes/blue/images/treetable/"+name;
		}
		function addTdPara(t,n,v){
			t[n]=v;
			return "";
		}
		function adjPosition(){
			parentObj =element;
		    baseDivTop = baseDivLeft = 0
		    while(parentObj.tagName != "BODY") {
			    baseDivTop += parentObj.offsetTop;
			    baseDivLeft += parentObj.offsetLeft;
			    parentObj = parentObj.offsetParent;
		    }
		    element.style.pixelWidth = parentObj.clientWidth -  2 - parseFloat(element.rightFix)
		    element.style.pixelHeight = parentObj.clientHeight - baseDivTop - 2 - parseFloat(element.bottomFix)
		}
	
	
	jhtc_attr_init(element,"action","");
  	jhtc_attr_init(element,"afterload","");
  	jhtc_attr_init(element,"kind","");
  	jhtc_attr_init(element,"tagname",null);
  	jhtc_attr_init(element,"headXslFile","");
  	jhtc_attr_init(element,"headid","");
  	jhtc_attr_init(element,"dataXslFile","");
  	jhtc_attr_init(element,"bottomFix","0");
  	jhtc_attr_init(element,"rightFix","0");
  	
  	
	element.getCheckPks=getCheckPks;
	element.refresh=refresh;
	element.init=init;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_class_map["treeTableCtn"]=jhtc_treeTableCtn;