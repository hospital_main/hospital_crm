/**
 * 本组件为快速录入字典内容而开发，可接受XML文件或XML DOM对象作为数据源
 * 通过设置XML数据源中相应节点属性作为查询关键字来对XML数据源中对应节点的数据进行查询
 * 得到快速选择下拉框中的选择内容
 */
function jhtc_input(win,jhtc_obj){
  var pageWin=win;
  var element=jhtc_obj;  
  var activeElement = null;
  var objXMLDoc; //XMLDOM

  var strXSL="<?xml version='1.0' encoding='gb2312'?><xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'><xsl:output method='xml' encoding='gb2312' omit-xml-declaration='yes'/><xsl:param name='keys'>abcd</xsl:param><xsl:param name='Lheight'>16</xsl:param><xsl:param name='valueCol'>hosCode</xsl:param><xsl:param name='textCol'>hosName</xsl:param><xsl:template match='/'><xsl:element name='select'><xsl:attribute name='id'>dicList</xsl:attribute><xsl:attribute name='class'>hzh_select</xsl:attribute><xsl:attribute name='size'><xsl:value-of select='$Lheight'></xsl:value-of></xsl:attribute><xsl:apply-templates select='//r'/></xsl:element></xsl:template><xsl:template match='r'><xsl:for-each select='@*'><xsl:choose><xsl:when test='contains(.,$keys)'><xsl:element name='option'><xsl:attribute name='value'><xsl:value-of select='../@abc'/></xsl:attribute><xsl:value-of select='../@hdfee'/></xsl:element></xsl:when></xsl:choose></xsl:for-each></xsl:template></xsl:stylesheet>";
  var objXSL;
  
  var oDiv = null;//QI容器
  var objInput; // input对象
  var iframe;

  var node = "r";//XML接点名  
  var vValue,vText,vCode;//XML接点属性名
  var iValue,iText;//QI的value和text
	
  var vName ="" ;//隐含value对象的name属性
  var vHiddenValue = null; //隐含的input对象，用于承载value;
  
  var Lheight = 5;//select的高度 默认5
  var Lwidth = 200;//select的宽度
  var objPrevious = null;
  
  var vAdjustVal = 0;
  var sIndexCodeSequence = "";

  function putIndexCodeSequence(){
	sIndexCodeSequence = element.indexCodeSequence;
  }
  

  var flag = false;//快速选择下拉框是否出现标志
  
  function putPreviousObj(){
	objPrevious = window.document.getElementsByName(element.previousObj)[0];
  }

  function putXMLSource(){
    objXMLDoc = inputXML(element.xmlSource);
  }

  function putCodeCol(){
    vCode = element.codeCol;
  }
  function putValueCol(){
    vValue = element.valueCol;
  }

  function putTextCol(){
    vText = element.textCol;
  }

  function putLheight(){
    Lheight = element.Lheight;
  }

  function putLwidth(){
    Lwidth = element.Lwidth;
  }

  function getValue(){
  	if(vHiddenValue.value != iValue) {
  	vHiddenValue.value = iValue;
  	//evtChange.fire();
  	__jhtcDispatchEvent(element,"onValueChange");
  	}
  	return iValue;
  //	return objInput.value;
  }

  function getText(){
    return iText;
  }

  ////////////////////////////////////////////////////

  function lostFocus(){//焦点消失/
    window.document.attachEvent("onmousedown",clickDocument);    
  }
  
  function mouseOver() {
    alert("mouseOver");
  }

  function clickDocument(){//点击QInput以外的Document对象时隐藏QInput
    if(!flag) return;
    var objSrc = window.event.srcElement;
    if(element.contains(objSrc)||oDiv.contains(objSrc)) return;
    
    //QInput消失
    window.document.detachEvent("onmousedown",clickDocument);
    QIDisappear(); 
  }
///////////////////////////////////////////

  //function resizeOdiv(){//window改变大小时QInput重新定位
   // if(flag)locationObject();
  //}
  
  function cl(){//处理鼠标单击
     if(flag){ 
       try{
         activeElement.innerText= oDiv.childNodes[0].options[oDiv.childNodes[0].selectedIndex].text;
         iValue = oDiv.childNodes[0].options[oDiv.childNodes[0].selectedIndex].value;
         iText = oDiv.childNodes[0].options[oDiv.childNodes[0].selectedIndex].text;
       	if(vHiddenValue.value != iValue) {
			  	vHiddenValue.value = iValue;
			  	
			  	//evtChange.fire();
			  	__jhtcDispatchEvent(element,"onValueChange");
			  	}
         
         QIDisappear();
         
         activeElement.focus();
         activeElement.select();
       }catch(e){
         //丢弃错误(activeElement可能为null)
       }//End of try

     }else{
       //当点击document后objInput再次获得焦点时，使QInput可见
       if((objInput.innerText)!=" " && oDiv!=null){
         if((oDiv.innerHTML).indexOf("dicList")!=-1){
           locationObject();
            onFocus();
	   oDiv.style.visibility = "visible"; //使div可见
           iframe.style.display=oDiv.style.display 
           iframe.style.visibility=oDiv.style.visibility
           
           flag = true;
	 }
       }
     }
  }

  
  function inputXML(argSource){ ////objXMLDoc接收XML文件数据
    var objXMLDoc = null;
    try{
      switch(typeof(argSource)){
        case "string"://XML字节流	
          if(argSource.search(/\./) != -1 && argSource.search(/\</) == -1){ //xml file's location
	  
            objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");
            objXMLDoc.async = false;
            if(argSource.search(/\+/) != -1) argSource = eval(argSource);	    
              objXMLDoc.load(argSource);	      
              break;
            }
            if(argSource.search(/\</) != -1){ //xml string
              objXMLDoc = new ActiveXObject("MSXML2.DOMDocument");
              objXMLDoc.loadXML(argSource);
              break;
            }
            objXMLDoc = eval(argSource);
            objXMLDoc = (objXMLDoc.XMLDocument)?(objXMLDoc.XMLDocument):null; //xml data island
	    
            break;
        case "object"://XMLDOM对象
	
            if(argSource.xml) return objXMLDoc = argSource; //xml document object
            break;
            default:
            objXMLDoc = null;
      }      
      if (!objXMLDoc.xml) objXMLDoc = null;
    }catch(err){ objXMLDoc = null; }

    if(objXMLDoc) objXMLDoc.setProperty("SelectionLanguage","XPath");
    return objXMLDoc;	    
   }   
   
   function putAdjustVal(){
	vAdjustVal = element.AdjustVal;
   }

   
   function initialize(){//初始化操作
	var intOffsetVal = 0;
	//debugger;
	if(objPrevious){
		while(objPrevious.tagName.toLowerCase() != "body"){
			intOffsetVal += objPrevious.offsetTop;
			objPrevious = objPrevious.parentElement;
		}
		if(intOffsetVal < 0){
			intOffsetVal = vAdjustVal;
		}
	}
	element.style.display="none";
     //对象建立   
     objInput = element.document.createElement("<input name='zbbTest' class='hzh_input'/>");//create input
     
     objInput = element.parentNode.appendChild(objInput);
     with(objInput){
       onkeyup = showQI;
       onkeydown = secContent;
       onclick = cl;
       onmousedown = lostFocus;
     }
     
//     objInputP = element.appendChild(objInput);
//     objInputP.onclick = cl;
   
     oDiv = element.document.createElement("<DIV class='hzh_div'/>");//creat div
     oDiv = element.parentNode.appendChild(oDiv);
     
     iframe=element.document.createElement("iframe");
	with(iframe){
		style.position = "absolute";
		style.display = "none";
		style.textAlign = "center";
		style.backgroundColor = "#F6F6F6";
		className = "ds_font";
		style.zIndex =8;
		style.overflow = "visible";
		border="0";
	}
	iframe = element.parentNode.appendChild(iframe);
     
     vHiddenValue = element.document.createElement("<input type='hidden' name='"+vName+"' style='display:none' />");
     element.parentNode.appendChild(vHiddenValue);
     
     with(oDiv){
	      style.left = objInput.offsetLeft -2;
	      style.top = objInput.offsetTop + 19;
	      style.pixelWidth = Math.max(element.width,35);
	      style.pixelHeight = 1; 
	      onkeyup = showQI;
        onkeydown = secContent;
        onclick = cl;
        style.zIndex=20;
        onmousedown = lostFocus;
      }
      
      window.attachEvent("onresize", adjPosition);
  	 adjPosition();
  	 window.setInterval(adjPosition,50);
  	 
     objXSL = inputXML(strXSL);
     objXSL.setProperty("SelectionNamespaces","xmlns:xsl='http://www.w3.org/1999/XSL/Transform'");
	
     var node = objXSL.selectSingleNode("//xsl:element[@name='option']/xsl:attribute[@name='value']/xsl:value-of");		
     node.setAttribute("select","../@"+vValue)
     node = objXSL.selectSingleNode("//xsl:element[@name='option']/xsl:value-of");
     node.setAttribute("select","../@"+vText);
     refresh();
     element.style.top = intOffsetVal - element.top;
     //debugger;
     putXMLSource();
	 if(iValue != ""){
		var objAimNode = objXMLDoc.selectSingleNode("//r[@" + vCode + "='" + iValue + "']");
		if(!objAimNode) return;
		objInput.value = objAimNode.getAttribute(vText);
		vHiddenValue.value = iValue;
	 }
	 
  }
function adjPosition() { // 调整位置
    // 取得element的绝对位置
    
    if(objInput==null)
    	return ;
    var form = objInput;
    var elementTop=0, elementLeft=0;

    while(form.tagName != "BODY") {
	    elementTop = elementTop + form.offsetTop + form.clientTop;
	    elementLeft = elementLeft + form.offsetLeft+form.clientLeft;
	    form = form.offsetParent;
    }

    with (oDiv.style) {
     	left = elementLeft-1;
     	if(elementTop+objInput.offsetHeight-1+parseInt(objInput.style.height.replace(/px/,'')) > window.document.body.offsetHeight)
     	  top = elementTop + objInput.offsetHeight-1-101;
     	else
  		  top = elementTop + objInput.offsetHeight-1;
    }
    with (iframe.style) {
		left = elementLeft;
		if(elementTop+objInput.offsetHeight-1+parseInt(oDiv.style.height.replace(/px/,'')) > window.document.body.offsetHeight)
			top = elementTop - parseInt(oDiv.style.height.replace(/px/,''))-1
		else
			top = elementTop + objInput.offsetHeight-1;
		window.setTimeout(function(){
				width=200;//oDiv.clientWidth;
				height=82;//oDiv.clientHeight;
			},10);
	}
  }
  
  function showQI(){//显示QInput
    if(event.keyCode!="40" && event.keyCode!="38" && event.keyCode!="13"){
      if(event.srcElement.value!=undefined && event.srcElement.value!=""){	
        activeElement = event.srcElement;
        
        oDiv.style.visibility = "visible"; //使div可见
        iframe.style.display=oDiv.style.display 
        iframe.style.visibility=oDiv.style.visibility
	      flag = true;
        var tmpListStr = GetList();
        //window.prompt("sec",tmpListStr);
	 try{
     if(tmpListStr.indexOf("option")!= -1){//记录集不为空
	      //oDiv.innerHTML = tmpListStr; 
	      jhtcSetHtml(oDiv,tmpListStr);
	      dicList.runtimeStyle.width = element.Lwidth;
	      oDiv.style.visibility = "visible"; 
	   }else{
	     QIDisappear();
	   }//End of If tmpListStr.indexOf("option")!= -1
	}catch(e){
	   //错误丢弃
	 }//End of Try
	 locationObject();//定位oDiv
      }
      else
        QIDisappear();//QInput消失
  } 

  if(event.keyCode=="40" || event.keyCode=="38"){//首次移动光标选择处理
    if(flag){
      if(oDiv.childNodes[0].selectedIndex=="0" && activeElement!=null)
      {
        activeElement.innerText= oDiv.childNodes[0].options[0].text;
        iValue = oDiv.childNodes[0].options[0].value;
        iText = oDiv.childNodes[0].options[0].text;
        if(vHiddenValue.value != iValue) {
			  	vHiddenValue.value = iValue;
			  	//evtChange.fire();
			  	__jhtcDispatchEvent(element,"onValueChange");
			  	}
       }
    }
    else
    {
    	activeElement = event.srcElement;
        oDiv.style.visibility = "visible"; //使div可见
        iframe.style.display=oDiv.style.display 
        iframe.style.visibility=oDiv.style.visibility
	flag = true;

        var tmpListStr = GetList();
          
	 try{
     		if(tmpListStr.indexOf("option")!= -1)
     		{//记录集不为空
	      		oDiv.innerHTML = tmpListStr; 
	      		dicList.runtimeStyle.width = element.Lwidth;
	   	}
	   	else
	   	{
	     		QIDisappear();
	   	}//End of If tmpListStr.indexOf("option")!= -1
	 }catch(e){
	   //错误丢弃
	 }//End of Try
	 locationObject();//定位oDiv
    }
    
  }
}
      
function refresh(){//定义QInput外框样式   
  var objTemp;            
  with(element){
    style.top = element.top;
    style.left = element.left;
    style.pixelHeight = element.height;
    style.pixelWidth = element.width;
  }
  if(element.width > 18) 
    objInput.style.pixelWidth = element.width - 0;
  else
    objInput.style.pixelWidth = element.width - 8;
}


function secContent(){//响应keydown事件
  if(event.keyCode=="40" || event.keyCode=="38"){//移动上下光标选择进行选择
     if(flag){   
       oDiv.childNodes[0].focus();
       SaveSelectValue();
       
     }
  }
  
  if(event.keyCode=="13") QIDisappear(); //回车键(13)隐藏oDiv   

  //处理backspace(8)键或Del(46)键来删除完输入的字符时隐藏QInput
  if(event.keyCode=="8" || event.keyCode=="46"){
    if(objInput!=null){
      if(objInput.innerText == ""){
		QIDisappear();
		vHiddenValue.value = "";
	 }
    }//End of if(activeElement!=null)
  }

  if(event.keyCode=="37" || event.keyCode=="39")//移动左右光标的处理
      QIDisappear();
      //window.status = iValue;
}


function QIDisappear(){//隐藏QInput
  try{
      oDiv.style.visibility = "hidden"; 
      iframe.style.display=oDiv.style.display 
        iframe.style.visibility=oDiv.style.visibility
      flag = false;
      activeElement.focus();
      activeElement.select();
  }catch(e){
    //错误丢弃（activeElement可能为null）
  }
}


function SaveSelectValue(){//保存当前选中的值
	var temp;
  if(flag){
    try{
      if(oDiv.childNodes[0].selectedIndex!=-1){ 
      
        if(event.keyCode=="40")
        {
          temp = oDiv.childNodes[0].options[oDiv.childNodes[0].selectedIndex+1];
          activeElement.innerText= temp.text; 
          iValue = temp.value;
          if(vHiddenValue.value != iValue) {
			  	vHiddenValue.value = iValue;
			  	//evtChange.fire();
			  	__jhtcDispatchEvent(element,"onValueChange");
			  	}
          iText = temp.text;
        }
        if(event.keyCode=="38")
        {
          temp = oDiv.childNodes[0].options[oDiv.childNodes[0].selectedIndex-1];
          activeElement.innerText= temp.text; 
          iValue = temp.value;
          if(vHiddenValue.value != iValue) {
			  	vHiddenValue.value = iValue;
			  	//evtChange.fire();
			  	__jhtcDispatchEvent(element,"onValueChange");
			  	}
          iText = temp.text;	
        }
  	 
  	   
      }
    }catch(e){
      //错误描述: select越界 处理方式：丢弃
    }
    ////////////////////////////////////////////////////////////
    if(event.keyCode=="13"){
      try{
           //vHiddenValue.value = iValue;     
        QIDisappear();
      }catch(e){
        //错误丢弃
      }
    }
    /////////////////////////////////////////////////////////////////
  }
}


function GetList(){//读取XML文件指定节点数据并装配成一个List返回
//  try{
    key = event.srcElement.value;
    key = key.toLowerCase();
    objXMLDoc.setProperty("SelectionLanguage", "XPath");
    
///////////////////////////////////////////
var lCode = sIndexCodeSequence.split("|");//接收前台的sIndexCodeSequence拼在一起
var sXPath = "";
if( sIndexCodeSequence == "" ){
   try{
    var objNodeList = objXMLDoc.documentElement.selectNodes("//" + node+"[contains(@"+vCode+",'"+key+"')]");
    var tmpStr="";
    tmpStr += "<select id='dicList' class='hzh_select' size='" + element.Lheight + "'>";
    var nodeListLength = objNodeList.length;
    if(nodeListLength > 60)
    {
    	nodeListLength = 60;
    }
    for( var i = 0; i < nodeListLength; i++ ){
   	tmpStr +="<option value='" + objNodeList[i].getAttribute(vValue) + "'>" + objNodeList[i].getAttribute(vText) + "</option>";
    }
    tmpStr += "</select>";
    return tmpStr;

  }catch(e){
    //错误丢弃
  }
}
else{
for(var i = 0; i < lCode.length; i ++ ){
	if(i!=lCode.length-1){
		sXPath += "contains(@" + lCode[i] + ",'" + key + "') or ";
	}else{
		sXPath += "contains(@" + lCode[i] + ",'" + key + "')";
	}
}

///////////////////////
  var objNodeList = objXMLDoc.documentElement.selectNodes("//" + node+"[" + sXPath + "]");
    var tmpStr="";
    tmpStr += "<select id='dicList' class='hzh_select' size='" + element.Lheight + "'>";
    var nodeListLength = objNodeList.length;
    if(nodeListLength > 60)
    {
    	nodeListLength = 60;
    }

    for( var i = 0; i < nodeListLength; i ++ ){
   	tmpStr +="<option value='" + objNodeList[i].getAttribute(vCode) + "'>" + objNodeList[i].getAttribute(vText) + "</option>";
    }
    tmpStr += "</select>";

    return tmpStr;

}
}


function locationObject(){ //处理QInput的定位
  
  try{
   
    //上下反转
    var maxHeight = element.parentElement.clientHeight - element.offsetTop - element.height - dicList.offsetTop;
    if(dicList.clientHeight>maxHeight){
      if(element.offsetTop >= dicList.clientHeight){
        oDiv.style.top = - dicList.clientHeight - 2;
      }
    }else{//恢复默认
      oDiv.style.top = objInput.offsetTop + 19;
    }

    //左右反转
    var maxRight = element.parentElement.clientWidth - element.offsetLeft - element.width - dicList.offsetLeft;
    if(dicList.clientWidth >maxRight){
      oDiv.style.left = - element.Lwidth + 120;
    }else{
      oDiv.style.left = objInput.offsetLeft -2;
    }
  }catch(e){
    //丢弃错误
  }
}

function setValue(){//传入的是某一字典条目对应的code
	iValue = element.value;
}
function setText(){
	objInput.value= element.text;
	vHiddenValue.value=element.text;
}

 function putName()
  {
  	vName=element.name;
  }
  
  function getName()
  {
  	return vName;
  }
 function onFocus()
 { 
  	
   event.srcElement.select();
  
 }
 
 	__jhtcBindPropertyChange(element,"xmlSource",putXMLSource);
	__jhtcBindPropertyChange(element,"value",setValue);

	__jhtcBindPropertyChange(element,"codeCol",putCodeCol);
	__jhtcBindPropertyChange(element,"valueCol",putValueCol);
	__jhtcBindPropertyChange(element,"indexCodeSequence",putIndexCodeSequence);
	__jhtcBindPropertyChange(element,"textCol",putTextCol);
	
	__jhtcBindPropertyChange(element,"Lwidth",putLwidth);
	__jhtcBindPropertyChange(element,"Lheight",putLheight);
	__jhtcBindPropertyChange(element,"AdjustVal",putAdjustVal);
	__jhtcBindPropertyChange(element,"name",putName);
	__jhtcBindPropertyChange(element,"previousObj",putPreviousObj);
	__jhtcBindPropertyChange(element,"init",initialize);

  	jhtc_attr_init(element,"xmlSource","");
  	jhtc_attr_init(element,"value","");
  	jhtc_attr_init(element,"text","");
  	jhtc_attr_init(element,"codeCol","");
  	jhtc_attr_init(element,"valueCol","");
  	jhtc_attr_init(element,"indexCodeSequence","");
  	jhtc_attr_init(element,"textCol","");
  	jhtc_attr_init(element,"top","40");
  	jhtc_attr_init(element,"left","100");
  	jhtc_attr_init(element,"width","120");
  	jhtc_attr_init(element,"height","19");
  	jhtc_attr_init(element,"Lwidth","");
  	jhtc_attr_init(element,"Lheight","");
  	jhtc_attr_init(element,"AdjustVal","");
  	jhtc_attr_init(element,"name","");
  	jhtc_attr_init(element,"previousObj","");
  	jhtc_attr_init(element,"init","");
  	
	element.init2=initialize;
	
	element.jhtcInit=function(){
		putName();
		putCodeCol();
		setValue();
		putValueCol();
		putTextCol();
		putIndexCodeSequence();
		element.init2();
		__jhtcBindPropertyChange(element,"text",setText);
	};
	return null;
};
jhtc_tag_map["QInput"]=jhtc_input;