function jhtc_myPolylineb(win,jhtc_obj){
var pageWin=win;
var element=jhtc_obj;
var objXMLDocSource,objXMLDoc;
var objDataArea = null;//折线图显示区域
////////////////////////////////////////////////////////
var vLeft = 40;
var vTop = 50;
var vWidth = 600;
var vHeight = 250;
var yLength = 0;
var xLength = 0;
var vXScale;
var vYScale;
var vYValueScale;
var vXValueScale;
var intLineTop;
var intConstBoard = 25;
var vDivisor = 10;
var vMaxX = 0;
var vMaxY = 0;
var vVPerHeight;
var b =1;
var vCaption = "";
var line1;
var line2;
var cPoint;
var scal;
var c=1;
var objGlobal;
var rate;
var seed=0;
var pointDescription = new Array();//折线点的x轴描述文字
//预置polyline配色[15种]
var lineColorArray = new Array('red','#993399','#0099FF','#CC9933','#339933','#FFCC33','#009999','#CC99FF','#99FF33','#FFFF33','#666699','black','#66FFFF','#FF00FF','#0000FF');

function getPointDescription(){//获取横坐标的描述文字装入pointDescription对象

  var objNode;  
  //按col子节点最大进行排序
  var vXSLT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"><xsl:output method=\"xml\" omit-xml-declaration=\"no\" encoding=\"gb2312\" version=\"1.0\" indent=\"yes\"/><xsl:template match=\"/root\"><xsl:for-each select=\"col\"><xsl:sort select=\"count(child::node())\" data-type=\"number\" order=\"descending\"/><xsl:copy-of  select=\".\"/></xsl:for-each></xsl:template></xsl:stylesheet>";
  objNode = objGlobal.inputXML("<root>" + objGlobal.transformXML(objXMLDoc,vXSLT) + "</root>"); 
  // debugger;
  for(var i=0; i<objNode.childNodes[0].childNodes[0].childNodes.length;i++){//
    pointDescription.push(objNode.childNodes[0].childNodes[0].childNodes[i].getAttribute(element.textCol));
  }
}

function putXMLSource(){
	var argSource=element.xmlSource;
	objXMLDocSource = inputXML(argSource);
	if(objXMLDocSource == null) return;
	objXMLDoc = inputXML(objXMLDocSource);	
}

function calcuScaleOfXaaa(){//计算x轴刻度单位  

	if(!objXMLDoc) return;  
  var oNodeCol = objXMLDoc.selectNodes("/*/col");//所有折线集合
  var vLengthCol = 0;   
    
  if(!oNodeCol) return; 
  
  for(var i=0;i<oNodeCol.length;i++){ //便历所有col节点
    vLengthCol = oNodeCol[i].childNodes.length;    
    vDivisor = Math.max(vDivisor,vLengthCol);
  }  
  getMaxValueOfY();
  getPointDescription();//返回x轴坐标描述文字
}

function getMaxValueOfYaaa(){//返回纵坐标每个刻度对应的实际点值的大小

  var oNodeCol = null;
  var vMax = 0;//最大值  

	if(!objXMLDoc) return;  
  
  oNodeCol = objXMLDoc.selectNodes("/*/col/r");//所有r节点集合
  if(!oNodeCol) return;
  
  for(var i=0;i<oNodeCol.length;i++){ //便历所有r节点    
    vMax = parseFloat(oNodeCol[i].getAttribute(element.valueCol));
    vMaxY = Math.max(vMax,vMaxY);
  }  
//debugger;

  vYValueScale = Math.round(vMaxY/vDivisor*100)/100;//y轴刻度单位
  vXValueScale = Math.round(vMaxY/vDivisor*100)/100;//x轴刻度单位
  
}

function paintCoordinate(){ //画坐标系

	//if(!objXMLDoc) return;  
	
	
	yLength = vHeight;
	xLength = vWidth-150;
	
	//-----
	//vVML += paintPolyline();//
	//line one
	var line11=element.lineone.split(",");
	var line21=element.linetwo.split(",");
	var line31=element.linethree.split(",");
	// 纵轴与横轴的比例
	rate=line11[3]/line11[2];
	var from=new point(line11[0]*rate,line11[1]),to=new point(line11[2]*rate,line11[3]);
	//line two
	var from2=new point(line21[0]*rate,line21[1]),to2=new point(line21[2]*rate,line21[3]);
	
	line1=new line(from,to);
	line2=new line(from2,to2);

	cPoint=getCrossPoint(line1,line2);

	scal=new scale(Math.max(cPoint.x*2,cPoint.y*2));
	
	line1=new line(scal.scalePoint(from),scal.scalePoint(to));
	line2=new line(scal.scalePoint(from2),scal.scalePoint(to2));
	
	var cpo=getCrossPoint(line1,line2);

	
	//-------

	vXScale =vYScale= Math.min(xLength,yLength)/vDivisor;
	
	//vYScale = yLength/(vDivisor+2);
  
	var vVML = "<v:group id='vg1' style='position:absolute;border:0px solid red;top:0;left:0;width:1024px;height:768px' coordsize='1024,768'>";
  
  
  //表头	
	vVML += "<v:line style = 'position:absolute;left:-50;height:1;top:-30;width:" + vWidth + ";'><v:textbox style='font-family:宋体;font-size:10.5pt;text-align:center;font-weight:bold;color:" + element.captionColor + ";'>" + vCaption + "</v:textbox></v:line>";
  
  //y轴	
	vVML += "<v:line style = 'position:absolute;border:0 solid green;left:0;top:0' from='35,15' to='35," + (yLength-intConstBoard) + "'' strokeweight='" + element.coordinateWeight + "' strokecolor='" + element.yColor + "' coordsize='0,0'><v:stroke startarrow='open' endarrow='close'/></v:line>";
	
		
	//x轴		
	vVML += "<v:line style = 'position:absolute;left:0;top:0' from='35,"+ (yLength-intConstBoard) +"' to='475,"+ (yLength-intConstBoard) +"' strokeweight='" + element.coordinateWeight + "' strokecolor='" + element.xColor + "' coordsize='0,0'><v:stroke startarrow='close' endarrow='open'/></v:line>";
	//vVML += "<v:line style = 'position:absolute;left:0;top:0' from='35," + (yLength-intConstBoard) + "' to='" + (xLength - intConstBoard + 50) + "," + (yLength-intConstBoard) + "' strokeweight='" + element.coordinateWeight + "' strokecolor='" + element.xColor + "' coordsize='0,0'><v:stroke startarrow='close' endarrow='open'/></v:line>";
	vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + (xLength + 30) + ";top:" +  (yLength - intConstBoard - 2) + ";' coordsize='21600,21600'><v:textbox style='width:" + vXScale + ";color:black;border:0px solid red;margin-top:0; margin-left:0;'>" + element.xAxes + "</v:textbox></v:rect>";
	
	
	for(var i=0;i< parseInt(vDivisor);i++){//横坐标刻度线	
		
	  if(element.hasBrokenLine.toString() == "true"){
	      vVML += "<v:line style = 'position:absolute;left:0;top:0;' from='" + (35+i*vXScale) + ", " + (yLength-intConstBoard) + "' to='" + (35+i*vXScale) + ", "+vTop+"' coordsize='0,0' strokecolor='" + element.voidColor +  "'><v:stroke dashstyle='0,2' endcap='round'/></v:line>";
	     
	      vVML += "<v:line style = 'position:absolute;color:red;left:0;top:0;' from='" + (35+i*vXScale) + "," + (yLength-intConstBoard) + "' to='" + (35+i*vXScale) + "," + (yLength-intConstBoard - 5) + "' coordsize='0,0' strokecolor='#000000'></v:line>";      
	  }else{
	    if(i!= parseInt(vDivisor)){
	        vVML += "<v:line style = 'position:absolute;color:red;left:0;top:0;' from='" + (35+i*vXScale) + "," + (yLength-intConstBoard) + "' to='" + (35+i*vXScale) + "," + (yLength-intConstBoard - 3) + "' coordsize='0,0' strokecolor='#000000'></v:line>";      
	        vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + ((35+(i-1)*vXScale) - 20) + ";top:" +  (yLength - intConstBoard) + ";' coordsize='21600,21600'><v:textbox style='width:" + vXScale + ";color:" + element.xDescriptionColor + ";font-size:7pt;font-family:arial;border:0px solid red;margin-top:0; margin-left:0;'>" + pointDescription[i-1] + "</v:textbox></v:rect>";
	      }
	  }
	   if(vXValueScale != 0&&i%2==1)
      vVML += "<v:rect style='position:absolute;border:0px solid green;left:" + (35+i*vXScale - 18) + ";top:"+(yLength-intConstBoard+10)+"' coordsize='21600,21600'><v:textbox style='color:" + element.xDescriptionColor + ";font-size:8pt;border:0px solid red;margin-top:0; margin-left:0;'>" + Math.round(i*scal.sign/rate) + "</v:textbox></v:rect>";
		else
			vVML += "<v:rect style='position:absolute;border:0px solid green;left:" + (35+i*vXScale - 18) + ";top:"+(yLength-intConstBoard)+"' coordsize='21600,21600'><v:textbox style='color:" + element.xDescriptionColor + ";font-size:8pt;border:0px solid red;margin-top:0; margin-left:0;'>" + Math.round(i*scal.sign/rate) + "</v:textbox></v:rect>";
	}
	
	//x轴最后一个文字描述
	//vVML += "<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + ((35+(parseInt(vDivisor)-1)*vXScale) - 10) + ";top:" +  (yLength - intConstBoard + 5) + ";' coordsize='21600,21600'><v:textbox style='width:" + vXScale + ";color:" + xDescriptionColor + ";font-size:6.5pt;font-family:宋体;border:0px solid black;margin-top:0; margin-left:0;'>" + pointDescription[parseInt(vDivisor)-1] + "</v:textbox></v:rect>";
	
	for(var i=0;i<= parseInt(vDivisor-3);i++){//纵坐标刻度线
	
	  intLineTop = yLength-(i*vYScale+intConstBoard);
	  if(i == parseInt(vDivisor-3))	  
	    vVML += "<v:rect style='position:absolute;border:0 solid red;left:-20;top:" + (parseInt(intLineTop) - vYScale ) + ";' coordsize='21600,21600'><v:textbox style='color:black;width:200;'>" + element.yAxes + "</v:textbox></v:rect>";
	  if(element.hasBrokenLine.toString() == "true"){  
      vVML += "<v:line style = 'position:absolute;top:0;left:0;' from='35," + intLineTop + "' to='" + (xLength - intConstBoard + 20) + "," + intLineTop + "' coordsize='0,0' strokecolor='" + element.voidColor + "'><v:stroke dashstyle='0,2' endcap='round'/></v:line>"; 	    
	    
	    vVML += "<v:line style = 'position:absolute;top:0;left:0;' from='35," + intLineTop + "' to='37," + intLineTop + "' coordsize='0,0' strokecolor='#000000'></v:line>"; 	    
    }else{
      vVML += "<v:line style = 'position:absolute;top:0;left:0;' from='35," + intLineTop + "' to='38," + intLineTop + "' coordsize='0,0' strokecolor='#000000'></v:line>"; 	    
	  }
	  if(vYValueScale != 0)
      vVML += "<v:rect style='position:absolute;border:0px solid green;left:-10;top:" + (intLineTop - 8) + ";' coordsize='21600,21600'><v:textbox style='color:" + element.yDescriptionColor + ";font-size:8pt;border:0px solid red;margin-top:0; margin-left:0;'>" + Math.round(i*scal.sign) + "</v:textbox></v:rect>";

	}//End of for
	//debugger;
	if(vMaxY!=0)
	  vVPerHeight = (vHeight - intLineTop -intConstBoard)/vMaxY;
	else
	  vVPerHeight = 0;

	vVML+=drawLineAndGetCroosPoint(scal.scalePoint(from),{x:Math.min(cpo.x*2,xLength),y:line1.getY(Math.min(cpo.x*2,xLength))},scal.scalePoint(from2),{x:Math.min(cpo.x*2,xLength),y:line2.getY(Math.min(cpo.x*2,xLength))});

	vVML += diagram()+"</v:group>";		
	seed=seed+1;
	if(seed==3){
	//objDataArea.insertAdjacentHTML("afterBegin",vVML + "<xml:namespace prefix='v' ns='schemas-microsoft-com:vml'/>");
	selfSet(vVML + "<xml:namespace prefix='v' ns='schemas-microsoft-com:vml'/>");
	}

}

//绘制两条线与交点
function drawLineAndGetCroosPoint(from1,to1,from2,to2){
	var tempLine="";
	var cp;
	tempLine +=drawLine(from1,to1,'red');
	tempLine +=drawLine(from2,to2,'#AAAA00');	
	
	tempLine +=drawHLine({x:to2.x,y:from2.y},'blue');
	cp=getCrossPoint(line1,line2);
	//保本点
	tempLine+=drawLine({x:cp.x,y:cp.y},{x:cp.x+cp.x/5,y:cp.y-cp.y/5},'green',2);
	tempLine+=drawTextBox(cp.x+cp.x/5,cp.y-cp.y/5,"保本点");
	
	return tempLine +=drawVLine(cp,'green')+""+drawHLine(cp,'green');
}

//画textBox
function drawTextBox(left,top,text){
	var textBox="<v:rect style='position:absolute;border:0px solid green;wordWrap:break-word;left:" + (35+parseInt(left)) + ";top:" +  (-parseInt(top)+parseInt(yLength)-parseInt(intConstBoard)) + ";' coordsize='21600,21600'><v:textbox style='width:" + vXScale*4 + ";color:" + element.xDescriptionColor + ";font-size:10Pt;font-family:宋体;border:0px solid red;margin-top:0; margin-left:0;'>" + text+"("+parseInt(cPoint.x/rate+1)+"," + parseInt(cPoint.y+1) + ")" + "</v:textbox></v:rect>";
	return textBox;
	}

//画直线
function drawLine(from,to,color,arrow){
	var vVML;

	if(arrow!=undefined&&arrow){
		vVML = "<v:line style = 'position:absolute;border:0 solid green;left:0;top:0;' from='" + (35+parseInt(from.x)) + ", " + (-parseInt(from.y)+parseInt(yLength)-parseInt(intConstBoard)) + "' to='" + (35+parseInt(to.x)) + ", "+(-parseInt(to.y)+parseInt(yLength)-parseInt(intConstBoard))+"' coordsize='0,0' strokeweight='1' strokecolor='"+color+"'><v:stroke startarrow='open' endarrow='none'/></v:line>";
	}else{
		vVML = "<v:line style = 'position:absolute;border:0 solid green;left:0;top:0;' from='" + (35+parseInt(from.x)) + ", " + (-parseInt(from.y)+parseInt(yLength)-parseInt(intConstBoard)) + "' to='" + (35+parseInt(to.x)) + ", "+(-parseInt(to.y)+parseInt(yLength)-parseInt(intConstBoard))+"' coordsize='0,0' strokeweight='1' strokecolor='"+color+"'></v:line>";
	}
	
	return vVML;	
}
//画垂线
function drawVLine(point,color,arrow){
	var from=new Object();
	var to=new Object();
	from.x=to.x=point.x;
	from.y=0;
	to.y=point.y;
	return drawLine(from,to,color,arrow);
}
//画水平线
function drawHLine(point,color,arrow){
	var from=new Object();
	var to=new Object();
	from.y=to.y=point.y;
	from.x=0;
	to.x=point.x;
	return drawLine(from,to,color,arrow);
}

//取得两条线的交点
function getCrossPoint(line1,line2){
	if(line1.k==line2.k){
		alert("没有交点");
	}
	var crossP=new point(0,0);
	
	crossP.x=(line2.b-line1.b)/(line1.k-line2.k);
	crossP.y=crossP.x*line1.k+line1.b;
	return crossP;
}

/////////////////////////////////////////////////


function refresh(){

	//需要重画时需要将objDataArea中的所有对象清除
	while(objDataArea.children.length >0){
		objDataArea.children(0).removeNode(true);
	}
	
	if(pointDescription && pointDescription.length>1){
	  while(pointDescription.length > 0){
	    pointDescription.pop();
	  } 
	}
	
	vDivisor = 0;
	xLength = 0;
	yLength = 0;
	vMaxY = 0;
	vXScale = 0;
	vYScale = 0;

	calcuScaleOfX();//计算x轴刻度	
	paintCoordinate();//创建坐标系	
}

function initialize(){
	if(element.init==0){		
		return;
	}
	
//debugger;
	//创建图形显示区域
	objDataArea = element.document.createElement("<div class='phms_pl_dataarea'></div>");
	objDataArea = element.appendChild(objDataArea); //外框,加滚动条的层
	
	with(element){
		style.pixelWidth = vWidth;
		style.pixelHeight = vHeight;
		style.top = vTop;
		style.left = vLeft;
	}
	
	with(objDataArea){//设置外层容器的大小
	    objDataArea.style.left = 0;
	    objDataArea.style.top = 0;
	  	objDataArea.style.pixelWidth = vWidth;
	  	objDataArea.style.pixelHeight = vHeight;
	  	objDataArea.style.padding = 0;
	}
	
	//calcuScaleOfX();//计算x轴刻度	
	
	paintCoordinate();//创建坐标系
  	
	element.style.visibility = "visible";
	//element.onkeydown = onVGKeyDown;
}

function putCaption(){
  vCaption = element.caption;
}

function putLeft(){
  vLeft = element.left;
}

function putTop(){
  vTop = element.top;;
}

function putWidth(){
  vWidth = element.width;
}

function putHeight(){
  vHeight = element.height;
}

//放大缩小
function onVGKeyDown(){
	var tl;
  if(event.keyCode == 107){//"+"[放大]
    b = b * 0.9090909;
    c = c*1.05;
    element.all("vg1").coordsize = (1024 * b) + "," + (768 * b);
    tl=element.all("kk").style.fontSize.length;
		element.all("kk").style.fontSize=c*(parseInt(element.all("kk").style.fontSize.substring(0,tl-2)))+'pt';
  }
  if(event.keyCode == 109){//"-"[缩小]
    b = b * 1.1;
    c =c*0.9090909;
    element.all("vg1").coordsize = (1024 * b) + "," + (768 * b);
		element.all("kk").style.fontSize=c*(parseInt(element.all("kk").style.fontSize.substring(0,tl-2)))+'pt';
  }
}

function inputXML(argXML){
	
	try{
		objGlobal = __objGlobalCommonInst;		
	}catch(e){}
	try{
		if(!objGlobal){
			objGlobal = parent.__objGlobalCommonInst;
		}
	}catch(e){}
	
	if(!objGlobal){
		objGlobal = parent.parent.__objGlobalCommonInst;
	}
	try{
		if(typeof(objGlobal) == "undefined"){
			objGlobal = window.dialogArguments.__objGlobalCommonInst;
		}
	}catch(e){}
	
	 return objGlobal.inputXML(argXML);
}

//线
function line(from,to){
	this.from=from;
	this.to=to;
	if(from.x==to.x){
		this.k='NAN';
		this.b="NAN";
	}else{
		this.k=(from.y-to.y)/(from.x-to.x);
		this.b=to.y-(this.k*to.x);
	}
	this.getX=function(y){
		return (y-this.b)/this.k;
	}
	this.getY=function(x){
		return (this.k*x+this.b);	
	}		
}

//点
function point(x,y){
	this.x=x;
	this.y=y;
}
//比例
function scale(v){
	this.sign=parseInt(v/vDivisor);
	this.per=(Math.min(xLength,yLength)/vDivisor)/this.sign;
	this.getXp=function (realP){
		return realp*this.per;
	}
	this.scalePoint=function (p){
		return new point(p.x*this.per,p.y*this.per);	
	}
}
//图示
function diagram(postion){
	  return '<v:RoundRect style="position:relative;width:260;left:'+xLength/4+';top:'+(parseInt(yLength)+10)+';height:20px"><v:shadow on="F" type="single" color="#b3b3b3" offset="5px,5px"/> <v:TextBox id="kk" inset="15pt,4pt,0pt,0pt" style="font-size:8pt;">业务收入　　总 成 本　　固定成本　　保本点</v:TextBox>  </v:RoundRect> <v:Rect fillcolor="red" style="position:relative;top:'+(parseInt(yLength)+15)+';left:'+(xLength/4+5)+';width:10;height:10px"/><v:Rect fillcolor="#AAAA00" style="position:relative;top:'+(parseInt(yLength)+15)+';left:'+(xLength/4+70)+';width:10;height:10px"/><v:Rect fillcolor="blue" style="position:relative;top:'+(parseInt(yLength)+15)+';left:'+(xLength/4+137)+';width:10;height:10px"/><v:Rect fillcolor="green" style="position:relative;top:'+(parseInt(yLength)+15)+';left:'+(xLength/4+203)+';width:10;height:10px"/>';
}

	__jhtcBindPropertyChange(element,"caption",putCaption);
	__jhtcBindPropertyChange(element,"top",putTop);
	__jhtcBindPropertyChange(element,"left",putLeft);
	__jhtcBindPropertyChange(element,"width",putWidth);
	__jhtcBindPropertyChange(element,"height",putHeight);
	
	__jhtcBindPropertyChange(element,"init",initialize);
	
	
  	jhtc_attr_init(element,"xmlSource","");
  	jhtc_attr_init(element,"xAxes","x");
  	jhtc_attr_init(element,"yAxes","y");
  	jhtc_attr_init(element,"xColor","#000000");
  	jhtc_attr_init(element,"yColor","#000000");
  	jhtc_attr_init(element,"xDescriptionColor","#000000");
  	jhtc_attr_init(element,"yDescriptionColor","#000000");
  	jhtc_attr_init(element,"coordinateWeight","1");
  	jhtc_attr_init(element,"textCol",null);
  	jhtc_attr_init(element,"valueCol",null);
  	jhtc_attr_init(element,"lineone",null);
  	jhtc_attr_init(element,"linetwo",null);
  	jhtc_attr_init(element,"linethree",null);
  	jhtc_attr_init(element,"hasBrokenLine","true");
  	jhtc_attr_init(element,"voidColor","gray");
  	jhtc_attr_init(element,"caption","");
  	jhtc_attr_init(element,"captionColor","#000000");
  	jhtc_attr_init(element,"lineWeight","1.1");
  	jhtc_attr_init(element,"titleWidth","70");
  	jhtc_attr_init(element,"titleBgcolor","red");
  	jhtc_attr_init(element,"titleColor","yellow");
  	jhtc_attr_init(element,"top","");
  	jhtc_attr_init(element,"left","");
  	jhtc_attr_init(element,"width","");
  	jhtc_attr_init(element,"height","");
  	jhtc_attr_init(element,"refresh","");
  	jhtc_attr_init(element,"init","");
  		
  	
	element.init=initialize;
	element.jhtcInit=function(){
		element.init();
		__jhtcBindPropertyChange(element,"refresh",refresh);
		__jhtcBindPropertyChange(element,"xmlSource",putXMLSource);

		putXMLSource();
		//putHeight();
	};
	return null;
};
jhtc_tag_map["plb"]=jhtc_myPolylineb;