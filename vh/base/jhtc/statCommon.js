//<public:property name="bindLoad" put="putBindLoad" />

//<public:event name="onload" id="evtLoad" />
//<public:event name="onuddichange" id="evtUDDIChange" />
//<public:event name="onResponse" id="evtResponse" />//当使用post方法时指明使用“异步”请求，则该事件处理XMLHTTP的onStateChange事件
function jhtc_statCommon(win,jhtc_obj){
	var pageWin=win;
	var element=jhtc_obj;

    var strConstErrDivHTML = "";//字符串常量，显示在出现错误时的提示框DIV中的HTML
    var strConstHTTPErrDivHTML = "";
    var xslDateTime; //formatXMLDateTime方法中使用
    var arrDictionary = new ActiveXObject("Scripting.Dictionary");

    var intMsgBodySpoolLength = 0; //MsgBody堆的大小
    var intFreeMsgBodyCount = 0;//目前空闲能够分配的MsgBody
    var arrMsgBody = new Array();
    var arrAsynXMLHttp = new Array();//异步请求的XMLHttp
    var arrUUIDSpool = new Array();//缓存UDDI的数组
    var objXMLDoc;
    var objXMLHttp = new ActiveXObject("MSXML2.XMLHTTP");
    var intResponseState = 0; //返回的结果中是否包含Err或Alert
    var objHelpFragement; //缓存全局帮助文档的Fragement
    
    var boolSyn = true;
    var arrProperty = new Array();//所以动态属性的数组
    var strUIDofSession = "0"; 
    var objSectionListXML = null;
    var objMedicalManListXML = null;
    var objRegionalismListXML = null;
    
    var strRequestRespliteData;//缓存请求，用于分页
    var objInitInstitutionlist;//医疗机构列表
    
    var objLoadingDialog;//loading对象
    
    var objXMLDocTemp;   
    
    var objInfoDivPannel = null;
    
    //EHFS项目/////////////////////////////////
    
    var booleanNewsWinShow = false;//新闻窗口是否显示
    var booleanInfoWinShow = true;//信息窗口是否显示
    var booleanActionWinShow = false;//操作窗口是否显示
    
    
    function putNewsWinShow(){
		booleanNewsWinShow = element.newsWinShow;
    }
    
    function putActionWinShow(){
		booleanActionWinShow = element.actionWinShow;
    }
    
    function putInfoWinShow(){
		booleanInfoWinShow = element.infoWinShow;
    }
    
    
    //EHFS项目结束/////////////////////////////////////////////
    
    function showLoadingWindow(){//加载loading
		//
    }
    
    function hideLoadingWindow(){//卸载loading
		//
    }
    
    function getFrameStru(){//获取当前框架结构
		var objAimWin = window.parent.frames[1];
		booleanNewsWinShow = ( objAimWin.__multiWin__newWin.style.visibility == "visible" ) ? true : false;
		booleanActionWinShow = ( objAimWin.__multiWin__specialWin.style.visibility == "visible" ) ? true : false;
		booleanInfoWinShow = ( objAimWin.__multiWin__appInfoWin.style.visibility == "visible" ) ? true : false;
    }
    
    function showNewsWin(){//显示新闻窗口
		var objAimWin = window.parent.frames[1];
		getFrameStru();//
		if( !booleanNewsWinShow ){
			if( booleanInfoWinShow && booleanActionWinShow ){//操作窗口 - 显示    信息窗口 - 显示
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "visible";
				objAimWin.__multiWin__newWin.style.top = 0;//顶端距离
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.top = 23;//顶端距离
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "visible";
				objAimWin.__multiWin__specialWin.style.top = 23;//顶端距离
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				objAimWin.__multiWin__appInfoWin.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 28;//顶端距离
				objAimWin.__multiWin__appInfoWin.style.height = objAimWin.document.body.offsetHeight  - objAimWin.__multiWin__contextWin.offsetHeight - 32;
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "visible";
				objAimWin.__multiWin__splitLine.style.top = 20;//滚动条顶端距离
				objAimWin.__multiWin__splitLine_close.style.visibility = "visible";
				objAimWin.__multiWin__splitLine_close.style.top = 20;//关闭按钮顶端距离
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.top = 23;//滚动条顶端距离
				objAimWin.__multiWin__leftrightSplit_close.style.top = 30;//关闭按钮顶端距离
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 23;//滚动条顶端距离
				objAimWin.__multiWin__updownSplit_close.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 23;//关闭按钮顶端距离
				
			}else if( !booleanInfoWinShow && !booleanActionWinShow ){//操作窗口 - 不显示   信息窗口 - 不显示
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "visible";
				objAimWin.__multiWin__newWin.style.top = 0;//顶端距离
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.visibility = "visible";
				objAimWin.__multiWin__contextWin.style.top = 23;//顶端距离
				objAimWin.__multiWin__contextWin.style.height = objAimWin.document.body.offsetHeight - 27;
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "hidden";
				objAimWin.__multiWin__specialWin.style.top = 23;//顶端距离
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "hidden";
				objAimWin.__multiWin__appInfoWin.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 28;//顶端距离
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "visible";
				objAimWin.__multiWin__splitLine.style.top = 20;//滚动条顶端距离
				objAimWin.__multiWin__splitLine_close.style.visibility = "visible";
				objAimWin.__multiWin__splitLine_close.style.top = 20;//关闭按钮顶端距离
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.top = 23;//滚动条顶端距离
				objAimWin.__multiWin__leftrightSplit_close.style.top = 30;//关闭按钮顶端距离
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 23;//滚动条顶端距离
				objAimWin.__multiWin__updownSplit_close.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 23;//关闭按钮顶端距离
				//
			}else if( !booleanInfoWinShow && booleanActionWinShow ){//操作窗口 - 显示    信息窗口 - 不显示
				
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "visible";
				objAimWin.__multiWin__newWin.style.top = 0;//顶端距离
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.visibility = "visible";
				objAimWin.__multiWin__contextWin.style.top = 23;//顶端距离
				objAimWin.__multiWin__contextWin.style.height = objAimWin.document.body.offsetHeight - 27;//内容窗口高度
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "visible";
				objAimWin.__multiWin__specialWin.style.top = 23;//顶端距离
				objAimWin.__multiWin__specialWin.style.height = objAimWin.document.body.offsetHeight - 27;//操作窗口高度
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "hidden";
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "visible";
				objAimWin.__multiWin__splitLine.style.top = 20;//滚动条顶端距离
				objAimWin.__multiWin__splitLine_close.style.visibility = "visible";
				objAimWin.__multiWin__splitLine_close.style.top = 20;//关闭按钮顶端距离
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.top = 23;//滚动条顶端距离
				objAimWin.__multiWin__leftrightSplit_close.style.top = 30;//关闭按钮顶端距离
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "hidden";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "hidden";
				
			}else if( booleanInfoWinShow && !booleanActionWinShow ){//操作窗口 - 不显示  信息窗口 - 显示
				//
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "visible";
				objAimWin.__multiWin__newWin.style.top = 0;//顶端距离
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.visibility = "visible";
				objAimWin.__multiWin__contextWin.style.top = 23;//顶端距离
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "hidden";
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				objAimWin.__multiWin__appInfoWin.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 28;//顶端距离
				objAimWin.__multiWin__appInfoWin.style.height = Math.abs(objAimWin.document.body.offsetHeight - objAimWin.__multiWin__contextWin.offsetHeight - 32);//信息窗口高度
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "visible";
				objAimWin.__multiWin__splitLine.style.top = 20;//滚动条顶端距离
				objAimWin.__multiWin__splitLine_close.style.visibility = "visible";
				objAimWin.__multiWin__splitLine_close.style.top = 20;//关闭按钮顶端距离
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "hidden";
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "hidden";
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 23;//滚动条顶端距离
				objAimWin.__multiWin__updownSplit_close.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 23;//关闭按钮顶端距离
			}
		}
    }
    
    function closeNewsWin(){//关闭新闻窗口
		var objAimWin = window.parent.frames[1];
		getFrameStru();//
		if( booleanNewsWinShow ){
			if( booleanInfoWinShow && booleanActionWinShow ){//操作窗口 - 显示    信息窗口 - 显示
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "hidden";
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.top = 0;//顶端距离
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "visible";
				objAimWin.__multiWin__specialWin.style.top = 0;//顶端距离
				objAimWin.__multiWin__specialWin.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				objAimWin.__multiWin__appInfoWin.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 5;//顶端距离
				objAimWin.__multiWin__appInfoWin.style.height = parseInt(objAimWin.__multiWin__appInfoWin.offsetHeight) + 23;
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.top = 0;//滚动条顶端距离
				objAimWin.__multiWin__splitLine_close.style.top = 10;//关闭按钮顶端距离
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "visible";
				objAimWin.__multiWin__leftrightSplit.style.top = 0;//滚动条顶端距离
				objAimWin.__multiWin__leftrightSplit.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "visible";
				objAimWin.__multiWin__leftrightSplit_close.style.top = 10;//关闭按钮顶端距离
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit.style.top = objAimWin.__multiWin__contextWin.offsetHeight;//滚动条顶端距离
				objAimWin.__multiWin__updownSplit_close.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit_close.style.top = objAimWin.__multiWin__contextWin.offsetHeight;//关闭按钮顶端距离
				
			}else if( !booleanInfoWinShow && !booleanActionWinShow ){//操作窗口 - 不显示   信息窗口 - 不显示
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "hidden";
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.top = 0;//顶端距离
				objAimWin.__multiWin__contextWin.style.height = objAimWin.document.body.offsetHeight - 4;
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "hidden";
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "hidden";
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "hidden";
				objAimWin.__multiWin__splitLine_close.style.visibility = "hidden";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "hidden";
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "hidden";
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "hidden";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "hidden";
				//
			}else if( !booleanInfoWinShow && booleanActionWinShow ){//操作窗口 - 显示    信息窗口 - 不显示
				//
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "hidden";
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.top = 0;//顶端距离
				objAimWin.__multiWin__contextWin.style.height = objAimWin.document.body.offsetHeight - 4;//内容窗口高度
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "visible";
				objAimWin.__multiWin__specialWin.style.top = 0;//顶端距离
				objAimWin.__multiWin__specialWin.style.height = objAimWin.document.body.offsetHeight - 4;//操作窗口高度
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "hidden";
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "hidden";
				objAimWin.__multiWin__splitLine_close.style.visibility = "hidden";
				objAimWin.__multiWin__splitLine.style.top = 23;//滚动条顶端距离
				objAimWin.__multiWin__splitLine_close.style.top = 23;//关闭按钮顶端距离
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.top = 0;//滚动条顶端距离
				objAimWin.__multiWin__leftrightSplit_close.style.top = 10;//关闭按钮顶端距离
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "hidden";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "hidden";
				
			}else if( booleanInfoWinShow && !booleanActionWinShow ){//操作窗口 - 不显示  信息窗口 - 显示
				//
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "hidden";
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.top = 0;//顶端距离
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "hidden";
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				objAimWin.__multiWin__appInfoWin.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 5;//顶端距离
				objAimWin.__multiWin__appInfoWin.style.height = Math.abs(objAimWin.document.body.offsetHeight - objAimWin.__multiWin__contextWin.offsetHeight - 9);//信息窗口高度
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "hidden";
				objAimWin.__multiWin__splitLine_close.style.visibility = "hidden";
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.top = objAimWin.__multiWin__contextWin.offsetHeight;//滚动条顶端距离
				objAimWin.__multiWin__updownSplit_close.style.top = objAimWin.__multiWin__contextWin.offsetHeight;//关闭按钮顶端距离
			}
		}
    }
    
    function showInfoWin(){//显示信息窗口
		//-----------+
		var objAimWin = window.parent.frames[1];
		getFrameStru();//
		if( !booleanInfoWinShow ){
			if( booleanNewsWinShow && booleanActionWinShow ){//新闻窗口 - 显示    操作窗口 - 显示
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "visible";
				objAimWin.__multiWin__newWin.style.top = 0;//顶端距离
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.height = parseInt(objAimWin.__multiWin__contextWin.offsetHeight) - 120;
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "visible";
				objAimWin.__multiWin__specialWin.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				objAimWin.__multiWin__appInfoWin.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 28;//顶端距离
				objAimWin.__multiWin__appInfoWin.style.height = 115;
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 23;//滚动条顶端距离
				objAimWin.__multiWin__updownSplit_close.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit_close.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 23;//关闭按钮顶端距离
				
			}else if( booleanNewsWinShow && !booleanActionWinShow ){//新闻窗口 - 显示   操作窗口 - 不显示
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "visible";
				objAimWin.__multiWin__newWin.style.top = 0;//顶端距离
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.height = parseInt(objAimWin.__multiWin__contextWin.offsetHeight) - 120;
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "hidden";
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				objAimWin.__multiWin__appInfoWin.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 28;//顶端距离
				objAimWin.__multiWin__appInfoWin.style.height = 115;
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "hidden";
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "hidden";
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 23;//滚动条顶端距离
				objAimWin.__multiWin__updownSplit_close.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit_close.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 23;//关闭按钮顶端距离
				//
			}else if( !booleanNewsWinShow && booleanActionWinShow ){//操作窗口 - 显示
				
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "hidden";
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.height = objAimWin.document.body.offsetHeight - 120;//内容窗口高度
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "visible";
				objAimWin.__multiWin__specialWin.style.height = objAimWin.__multiWin__contextWin.offsetHeight;//操作窗口高度
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				objAimWin.__multiWin__appInfoWin.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 5;//顶端距离
				objAimWin.__multiWin__appInfoWin.style.height = 111;
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "hidden";
				objAimWin.__multiWin__splitLine_close.style.visibility = "hidden";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 1;
				objAimWin.__multiWin__updownSplit_close.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit_close.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 1;
				
			}else if( !booleanNewsWinShow && !booleanActionWinShow ){//无窗口
				//
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "hidden";
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.height = objAimWin.document.body.offsetHeight - 120;//内容窗口高度
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "hidden";
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				objAimWin.__multiWin__appInfoWin.style.top = objAimWin.__multiWin__contextWin.offsetHeight + 5;//顶端距离
				objAimWin.__multiWin__appInfoWin.style.height = 111;
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "hidden";
				objAimWin.__multiWin__splitLine_close.style.visibility = "hidden";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "hidden";
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "hidden";
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit.style.top = objAimWin.__multiWin__contextWin.offsetHeight;
				objAimWin.__multiWin__updownSplit_close.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit_close.style.top = objAimWin.__multiWin__contextWin.offsetHeight;
			}
		}
    }
    
    function closeInfoWin(){//关闭信息窗口
		var objAimWin = window.parent.frames[1];
		getFrameStru();//
		if( booleanInfoWinShow ){
			if( booleanNewsWinShow && booleanActionWinShow ){//新闻窗口 - 显示    操作窗口 - 显示
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "visible";
				objAimWin.__multiWin__newWin.style.top = 0;//顶端距离
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.height = objAimWin.document.body.offsetHeight - 27;
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "visible";
				objAimWin.__multiWin__specialWin.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "hidden";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "hidden";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "hidden";
				
			}else if( booleanNewsWinShow && !booleanActionWinShow ){//新闻窗口 - 显示   操作窗口 - 不显示
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "visible";
				objAimWin.__multiWin__newWin.style.top = 0;//顶端距离
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.height = parseInt(objAimWin.document.body.offsetHeight) - 27;
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "hidden";
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "hidden";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "hidden";
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "hidden";
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "hidden";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "hidden";
				//
			}else if( !booleanNewsWinShow && booleanActionWinShow ){//操作窗口
				
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "hidden";
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.height = objAimWin.document.body.offsetHeight - 4;//内容窗口高度
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "visible";
				objAimWin.__multiWin__specialWin.style.height = objAimWin.__multiWin__contextWin.offsetHeight;//操作窗口高度
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "hidden";
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "hidden";
				objAimWin.__multiWin__splitLine_close.style.visibility = "hidden";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "hidden";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "hidden";
				
			}else if( !booleanNewsWinShow && !booleanActionWinShow ){//无窗口显示
				//
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "hidden";
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.height = objAimWin.document.body.offsetHeight - 4;//内容窗口高度
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "hidden";
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "hidden";
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "hidden";
				objAimWin.__multiWin__splitLine_close.style.visibility = "hidden";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "hidden";
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "hidden";
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "hidden";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "hidden";
			}
		}
    }
    ////
    function showActionWin(){//显示操作窗口
		var objAimWin = window.parent.frames[1];
		getFrameStru();//
		if( !booleanActionWinShow ){
			if( booleanNewsWinShow && booleanInfoWinShow ){//新闻窗口 - 显示    信息窗口 - 显示
			
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "visible";
				objAimWin.__multiWin__newWin.style.top = 0;//顶端距离
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.width = Math.abs(parseInt(objAimWin.__multiWin__contextWin.offsetWidth) - 180);
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "visible";
				objAimWin.__multiWin__specialWin.style.left = objAimWin.__multiWin__contextWin.offsetWidth + 5;
				objAimWin.__multiWin__specialWin.style.width = parseInt(objAimWin.document.body.offsetWidth - objAimWin.__multiWin__contextWin.offsetWidth - 9);
				objAimWin.__multiWin__specialWin.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				objAimWin.__multiWin__specialWin.style.top = 23;
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "visible";
				objAimWin.__multiWin__leftrightSplit.style.top = 23;
				objAimWin.__multiWin__leftrightSplit.style.left = objAimWin.__multiWin__contextWin.offsetWidth;
				objAimWin.__multiWin__leftrightSplit.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "visible";
				objAimWin.__multiWin__leftrightSplit_close.style.top = 33;
				objAimWin.__multiWin__leftrightSplit_close.style.left = objAimWin.__multiWin__contextWin.offsetWidth;
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "visible";
				
			}else if( booleanNewsWinShow && !booleanInfoWinShow ){//新闻窗口 - 显示   信息窗口 - 不显示
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "visible";
				objAimWin.__multiWin__newWin.style.top = 0;//顶端距离
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.width = Math.abs(parseInt(objAimWin.__multiWin__contextWin.offsetWidth) - 180);
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "visible";
				objAimWin.__multiWin__specialWin.style.left = objAimWin.__multiWin__contextWin.offsetWidth + 5;
				objAimWin.__multiWin__specialWin.style.width = parseInt(objAimWin.document.body.offsetWidth - objAimWin.__multiWin__contextWin.offsetWidth - 9);
				objAimWin.__multiWin__specialWin.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				objAimWin.__multiWin__specialWin.style.top = 23;
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "visible";
				objAimWin.__multiWin__leftrightSplit.style.top = 23;
				objAimWin.__multiWin__leftrightSplit.style.left = objAimWin.__multiWin__contextWin.offsetWidth;
				objAimWin.__multiWin__leftrightSplit.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "visible";
				objAimWin.__multiWin__leftrightSplit_close.style.top = 33;
				objAimWin.__multiWin__leftrightSplit_close.style.left = objAimWin.__multiWin__contextWin.offsetWidth;
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "visible";
				//
			}else if( !booleanNewsWinShow && booleanInfoWinShow ){//信息窗口 - 显示
				
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "hidden";
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.width = Math.abs(parseInt(objAimWin.__multiWin__contextWin.offsetWidth) - 180);
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "visible";
				objAimWin.__multiWin__specialWin.style.left = objAimWin.__multiWin__contextWin.offsetWidth + 5;
				objAimWin.__multiWin__specialWin.style.width = parseInt(objAimWin.document.body.offsetWidth - objAimWin.__multiWin__contextWin.offsetWidth - 9);
				objAimWin.__multiWin__specialWin.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				objAimWin.__multiWin__specialWin.style.top = 0;
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "hidden";
				objAimWin.__multiWin__splitLine_close.style.visibility = "hidden";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "visible";
				objAimWin.__multiWin__leftrightSplit.style.top = 0;
				objAimWin.__multiWin__leftrightSplit.style.left = objAimWin.__multiWin__contextWin.offsetWidth;
				objAimWin.__multiWin__leftrightSplit.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "visible";
				objAimWin.__multiWin__leftrightSplit_close.style.top = 10;
				objAimWin.__multiWin__leftrightSplit_close.style.left = objAimWin.__multiWin__contextWin.offsetWidth;
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "visible";
				
			}else if( !booleanNewsWinShow && !booleanInfoWinShow ){//无窗口
				//
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "hidden";
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.width = Math.abs(parseInt(objAimWin.__multiWin__contextWin.offsetWidth) - 180);
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "visible";
				objAimWin.__multiWin__specialWin.style.left = objAimWin.__multiWin__contextWin.offsetWidth + 5;
				objAimWin.__multiWin__specialWin.style.width = parseInt(objAimWin.document.body.offsetWidth - objAimWin.__multiWin__contextWin.offsetWidth - 9);
				objAimWin.__multiWin__specialWin.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				objAimWin.__multiWin__specialWin.style.top = 0;
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "hidden";
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "hidden";
				objAimWin.__multiWin__splitLine_close.style.visibility = "hidden";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "visible";
				objAimWin.__multiWin__leftrightSplit.style.top = 0;
				objAimWin.__multiWin__leftrightSplit.style.left = objAimWin.__multiWin__contextWin.offsetWidth;
				objAimWin.__multiWin__leftrightSplit.style.height = objAimWin.__multiWin__contextWin.offsetHeight;
				
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "visible";
				objAimWin.__multiWin__leftrightSplit_close.style.top = 10;
				objAimWin.__multiWin__leftrightSplit_close.style.left = objAimWin.__multiWin__contextWin.offsetWidth;
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "hidden";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "hidden";
			}
		}
    }
    
    function closeActionWin(){//隐藏操作窗口
		var objAimWin = window.parent.frames[1];
		getFrameStru();//
		if( booleanActionWinShow ){
			if( booleanNewsWinShow && booleanInfoWinShow ){//新闻窗口 - 显示    信息窗口 - 显示
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "visible";
				objAimWin.__multiWin__newWin.style.top = 0;//顶端距离
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.width = objAimWin.document.body.offsetWidth - 4;
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "hidden";
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "hidden";
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "hidden";
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "visible";
				
			}else if( booleanNewsWinShow && !booleanInfoWinShow ){//新闻窗口 - 显示   信息窗口 - 不显示
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "visible";
				objAimWin.__multiWin__newWin.style.top = 0;//顶端距离
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.width = objAimWin.document.body.offsetWidth - 4;
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "hidden";
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "hidden";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "hidden";
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "hidden";
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "hidden";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "hidden";
				//
			}else if( !booleanNewsWinShow && booleanInfoWinShow ){//信息窗口 - 显示
				
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "hidden";
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.width = Math.abs(parseInt(objAimWin.document.body.offsetWidth) - 4);
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "hidden";
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "visible";
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "hidden";
				objAimWin.__multiWin__splitLine_close.style.visibility = "hidden";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "hidden";
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "hidden";
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "visible";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "visible";
				
			}else if( !booleanNewsWinShow && !booleanInfoWinShow ){//无窗口
				//
				//新闻窗口
				objAimWin.__multiWin__newWin.style.visibility = "hidden";
				
				//内容窗口
				objAimWin.__multiWin__contextWin.style.width = Math.abs(parseInt(objAimWin.document.body.offsetWidth) - 4);
				
				//操作窗口
				objAimWin.__multiWin__specialWin.style.visibility = "hidden";
				
				//信息窗口
				objAimWin.__multiWin__appInfoWin.style.visibility = "hidden";
				
				//固定滚动条
				objAimWin.__multiWin__splitLine.style.visibility = "hidden";
				objAimWin.__multiWin__splitLine_close.style.visibility = "hidden";
				
				//左右活动滚动条
				objAimWin.__multiWin__leftrightSplit.style.visibility = "hidden";
				objAimWin.__multiWin__leftrightSplit_close.style.visibility = "hidden";
				
				//上下活动滚动条
				objAimWin.__multiWin__updownSplit.style.visibility = "hidden";
				objAimWin.__multiWin__updownSplit_close.style.visibility = "hidden";
			}
		}
    }
    
    /////////////////////////////////
    
    function showDetailDialog(url,obje,width,height){//
		var diagWidth = ( typeof(width) == "undefined" ) ? 650 : width;
		var diagHeight = ( typeof(width) == "undefined" ) ? 500 : height;
		window.showModelessDialog(url,obje,"dialogWidth:" + diagWidth + "px;dialogHeight:" + diagHeight + "px;help:no;center:yes;status:no");
    }
       
    function setRespliteData(argData){//写入缓存数据
		//
    }
    
    function getRespliteData(){//获取缓存数据
		return ""; 
    }
    
    function setRespliteTitleData(argTitleData){//写入标题数据
		//
    }
    
    function getRespliteTitleData(){//获取标题数据
		return "";
    }
    
    function getRequestData(){
		return strRequestRespliteData;
    }
    
    function getRootFormMsgBody(argObj){
		var objReturn;
		objReturn = argObj.selectSingleNode("//root");
		return objReturn;
    }
    
    function createMsgBody(){//从堆中分配一个MsgBody实体，如果全部用完则重新分配    
      var oMsgBody;
      var returnMsgBody;
      if(intFreeMsgBodyCount >= 0){//如果当前还有能够分配的MsgBody
        for(var i = 0; i< intMsgBodySpoolLength; i++){
          if(arrMsgBody[i].inUseState == "free"){
            returnMsgBody = arrMsgBody[i];            
            break;
          }//End of if
        }//End of for    
      }
      
      if(intFreeMsgBodyCount < 10){//当空闲的MessageBody数目小于10时，再预装20个MessageBody
        for(var i = 0; i < 20; i++){//预装配20个MessageBody备用
          oMsgBody = element.document.createElement("<ehfs:ms/>");
          oMsgBody = window.document.appendChild(oMsgBody); 
          oMsgBody.init = "1";      
          arrMsgBody[intMsgBodySpoolLength] = oMsgBody;//将其加入到堆中
          intMsgBodySpoolLength += 1;//堆大小加1
          intFreeMsgBodyCount += 1;
        }//End of for
      }//End of if          
      return(returnMsgBody);
    }//End of Function createMsgBody
  

    function putFreeMsgBodyCount(){
    	var iFreeMsgBodyCount=element.freeMsgBodyCount;
      if( iFreeMsgBodyCount < 0 ) return( -1 );
      intFreeMsgBodyCount = iFreeMsgBodyCount;
    }
    
   
    function setRequestData(argRequestData){//缓存分页所需要的业务请求数据
		strRequestRespliteData = (typeof(argRequestData) == "object") ? argRequestData.xml : argRequestData;
    }
    
    function getRFromResponseData(objResponseXML,action,target){//根据指定的action和target来获取返回数据
		var objRNode;
		if( objResponseXML ) objRNode = objResponseXML.selectSingleNode("//MsgBody[@action='" +  action + "' and @target='" + target + "']/root/r");
		return objRNode;
    }
    
    function post(anyXML){//发送请求，并处理onError和onAlert绑定。该方法返回值为responseState
                                  //responseState：-3：资源不可用； -2：有Error； -1：有Alert；0：一切正常
      var objReturn;                                
      var oAsynXMLHttp;//异步XMLHttp
      var oResponse;//请求的返回结果的XML对象    
      var objNodeMsgs;//返回的XML中包含MsgBody的节点    
      var vActionXML = "";
      var aimXML = "";
      var objRoot;

      element.responseState=intResponseState = -3; //初始化返回标志
      if(boolSyn.toString().toLowerCase() == "true"){//同步请求处理      
        objXMLHttp.Open("POST",element.proxyLocation,false);
        aimXML = "<?xml version='1.0' encoding='GBK'?><root UIDofSession='" + strUIDofSession + "' TimeStmp='' Tran='0'>";
        
        if(anyXML.XMLFragement){//        
          objXMLDoc.loadXML(aimXML + anyXML.XMLFragement.xml + "</root>");          
        }else{//
          if(anyXML.xml){//XML DOM
            vActionXML = anyXML.xml;
           
            if(anyXML.selectSingleNode("/root")){
              //已经包含root节点就直接向Proxy提交 
              vActionXML = vActionXML.replace(/<\?xml[\s\S]{0,}Tran=\'0\'>/g,aimXML);
              objXMLDoc.loadXML(vActionXML);
            }else{//如果没有root节点则加入root节点进行提交            
              objXMLDoc.loadXML(aimXML + anyXML.xml + "</root>");            
            }//End of if root
          }else{
            if(typeof(anyXML) == "string"){
              vActionXML = anyXML;
              vActionXML = vActionXML.replace(/<\?xml[\s\S]{0,}Tran=\'0\'>/g,aimXML);
              objXMLDoc.loadXML(vActionXML);
            }
          }//End of if anyXML.xml
        }//End of if MsgBody

        objRoot = objXMLDoc.selectSingleNode("/root");

        if(!objRoot || (objRoot.getAttribute("UIDofSession") == null) || (objRoot.getAttribute("TimeStmp")  == null)){
          return(-1);       
        }else{
          objRoot.setAttribute("UIDofSession", strUIDofSession);       
        }
        
       /////////////////////////////////
       //alert(objXMLDoc);
       objXMLHttp.send(objXMLDoc);
       oResponse = inputXML(objXMLHttp.responseText);//得到返回，并装配为XMLFragement 
       //oResponse = inputXML("<root UIDofSession='0' TimeStmp='0000-0000-0000-0001' ><errors><err errlevel=\"5\" src=\"com.cases.processorBean.inAndOutHospital.ModiAllInHosInfoProcessor_fzwwyf_Impl\"><desc>java.lang.Exception: 复合结构出错：修改_住院动态信息xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</desc><MsgBody Priority=\"0\" Syn=\"0\" TimeStmp=\"\" Tran=\"0\" UIDofMesg=\"\" UIDofSession=\"0\" action=\"修改\" target=\"住院动态信息\"><MsgBody Priority=\"0\" Syn=\"0\" UIDofMesg=\"\" action=\"修改\" target=\"病人\"><Parameters><Parameter Name=\"IDOfSickMan\" Val=\"2\"></Parameter><Parameter Name=\"IDCard\" Val=\"175855\"></Parameter><Parameter Name=\"PName\" Val=\"55\"></Parameter><Parameter Name=\"SexCode\" Val=\"2\"></Parameter><Parameter Name=\"Birthday\" Val=\"12-2月-2004\"></Parameter></Parameters></MsgBody><MsgBody Priority=\"0\" Syn=\"0\" UIDofMesg=\"\" action=\"修改\" target=\"住院主信息\"><Parameters><Parameter Name=\"IDOFDiseaseCase\" Val=\"3\"></Parameter><Parameter Name=\"CaseNumber\" Val=\"#\"></Parameter><Parameter Name=\"SeriesNo\" Val=\"1\"></Parameter><Parameter Name=\"VisitDiagnoseCode\" Val=\"ICD10_000000000001\"></Parameter><Parameter Name=\"DiagnoseDesc\" Val=\"1\"></Parameter><Parameter Name=\"InTime\" Val=\"12-2月-2004\"></Parameter><Parameter Name=\"IDofMedicalMan\" Val=\"23\"></Parameter><Parameter Name=\"StatusCode\" Val=\"1\"></Parameter><Parameter Name=\"idOfSection\" Val=\"1\"></Parameter><Parameter Name=\"VisitTypeCode\" Val=\"1\"></Parameter><Parameter Name=\"OUTDOCTOR\" Val=\"1\"></Parameter><Parameter Name=\"OUTTIME\" Val=\"12-4月-2004\"></Parameter></Parameters></MsgBody><MsgBody Priority=\"0\" Syn=\"0\" UIDofMesg=\"\" action=\"修改\" target=\"转科信息\"><Parameters><Parameter Name=\"IDOFDiseaseCase\" Val=\"3\"></Parameter></Parameters><r DIAGNOSEDESC=\"1\" IDOFDISEASECASE=\"3\" IDOFIOTHOSPITAL=\"143213sdf2sf1s2df12s\" IDOFMEDICALMAN=\"1\" IDOFSECTION=\"1\" INTIME=\"50-2月-2004\" clientAct=\"u\"></r></MsgBody></MsgBody></err><err errlevel=\"5\" src=\"com.cases.processorBean.inAndOutHospital.ModiAllInHosInfoProcessor_fzwwyf_Impl\"><desc>java.lang.Exception: 为什么会出错?</desc><MsgBody Priority=\"0\" Syn=\"0\" TimeStmp=\"\" Tran=\"0\" UIDofMesg=\"\" UIDofSession=\"0\" action=\"修改\" target=\"住院动态信息\"><MsgBody Priority=\"0\" Syn=\"0\" UIDofMesg=\"\" action=\"修改\" target=\"病人\"><Parameters><Parameter Name=\"IDOfSickMan\" Val=\"2\"></Parameter><Parameter Name=\"IDCard\" Val=\"175855\"></Parameter><Parameter Name=\"PName\" Val=\"55\"></Parameter><Parameter Name=\"SexCode\" Val=\"2\"></Parameter><Parameter Name=\"Birthday\" Val=\"12-2月-2004\"></Parameter></Parameters></MsgBody><MsgBody Priority=\"0\" Syn=\"0\" UIDofMesg=\"\" action=\"修改\" target=\"住院主信息\"><Parameters><Parameter Name=\"IDOFDiseaseCase\" Val=\"3\"></Parameter><Parameter Name=\"CaseNumber\" Val=\"#\"></Parameter><Parameter Name=\"SeriesNo\" Val=\"1\"></Parameter><Parameter Name=\"VisitDiagnoseCode\" Val=\"ICD10_000000000001\"></Parameter><Parameter Name=\"DiagnoseDesc\" Val=\"1\"></Parameter><Parameter Name=\"InTime\" Val=\"12-2月-2004\"></Parameter><Parameter Name=\"IDofMedicalMan\" Val=\"23\"></Parameter><Parameter Name=\"StatusCode\" Val=\"1\"></Parameter><Parameter Name=\"idOfSection\" Val=\"1\"></Parameter><Parameter Name=\"VisitTypeCode\" Val=\"1\"></Parameter><Parameter Name=\"OUTDOCTOR\" Val=\"1\"></Parameter><Parameter Name=\"OUTTIME\" Val=\"12-4月-2004\"></Parameter></Parameters></MsgBody><MsgBody Priority=\"0\" Syn=\"0\" UIDofMesg=\"\" action=\"修改\" target=\"转科信息\"><Parameters><Parameter Name=\"IDOFDiseaseCase\" Val=\"3\"></Parameter></Parameters><r DIAGNOSEDESC=\"1\" IDOFDISEASECASE=\"3\" IDOFIOTHOSPITAL=\"143213sdf2sf1s2df12s\" IDOFMEDICALMAN=\"1\" IDOFSECTION=\"1\" INTIME=\"50-2月-2004\" clientAct=\"u\"></r></MsgBody></MsgBody></err><err errlevel=\"5\" src=\"com.cases.processorBean.inAndOutHospital.ModiAllInHosInfoProcessor_fzwwyf_Impl\"><desc>java.lang.Exception: 这已经是第三条错误信息拉?</desc><MsgBody Priority=\"0\" Syn=\"0\" TimeStmp=\"\" Tran=\"0\" UIDofMesg=\"\" UIDofSession=\"0\" action=\"修改\" target=\"住院动态信息\"><MsgBody Priority=\"0\" Syn=\"0\" UIDofMesg=\"\" action=\"修改\" target=\"病人\"><Parameters><Parameter Name=\"IDOfSickMan\" Val=\"2\"></Parameter><Parameter Name=\"IDCard\" Val=\"175855\"></Parameter><Parameter Name=\"PName\" Val=\"55\"></Parameter><Parameter Name=\"SexCode\" Val=\"2\"></Parameter><Parameter Name=\"Birthday\" Val=\"12-2月-2004\"></Parameter></Parameters></MsgBody><MsgBody Priority=\"0\" Syn=\"0\" UIDofMesg=\"\" action=\"修改\" target=\"住院主信息\"><Parameters><Parameter Name=\"IDOFDiseaseCase\" Val=\"3\"></Parameter><Parameter Name=\"CaseNumber\" Val=\"#\"></Parameter><Parameter Name=\"SeriesNo\" Val=\"1\"></Parameter><Parameter Name=\"VisitDiagnoseCode\" Val=\"ICD10_000000000001\"></Parameter><Parameter Name=\"DiagnoseDesc\" Val=\"1\"></Parameter><Parameter Name=\"InTime\" Val=\"12-2月-2004\"></Parameter><Parameter Name=\"IDofMedicalMan\" Val=\"23\"></Parameter><Parameter Name=\"StatusCode\" Val=\"1\"></Parameter><Parameter Name=\"idOfSection\" Val=\"1\"></Parameter><Parameter Name=\"VisitTypeCode\" Val=\"1\"></Parameter><Parameter Name=\"OUTDOCTOR\" Val=\"1\"></Parameter><Parameter Name=\"OUTTIME\" Val=\"12-4月-2004\"></Parameter></Parameters></MsgBody><MsgBody Priority=\"0\" Syn=\"0\" UIDofMesg=\"\" action=\"修改\" target=\"转科信息\"><Parameters><Parameter Name=\"IDOFDiseaseCase\" Val=\"3\"></Parameter></Parameters><r DIAGNOSEDESC=\"1\" IDOFDISEASECASE=\"3\" IDOFIOTHOSPITAL=\"143213sdf2sf1s2df12s\" IDOFMEDICALMAN=\"1\" IDOFSECTION=\"1\" INTIME=\"50-2月-2004\" clientAct=\"u\"></r></MsgBody></MsgBody></err></errors></root>");
       //oResponse = inputXML("<root UIDofSession='0' TimeStmp='0000-0000-0000-0001'><alerts><alert alertlevel='5' src='com.cases.processorBean.inAndOutHospital.ModiAllInHosInfoProcessor_fzwwyf_Impl' info='修改住院信息的时候发生错误'></alert></alerts></root>");
        
       //0、资源不可用
        if(!oResponse){//原因可能:
                       //   a.发生了http404、http500错误
                       //   b.由于网络繁忙等因素造成数据传输阻塞
                       //   c.Proxy服务器没有启动
                       //处理方式:
                       //   通过一个模态对话框来给出提示信息
                       
          var oNotFoundInfo = objXMLHttp.responseText;//服务器错误页InnerHTML
          window.showModalDialog("../htc/notfound.htm", oNotFoundInfo,"dialogHeight:8;dialogWidth:25;status:no;scroll:no");        
          element.responseState=intResponseState = -3;
          return(-1);
        }
        
        //0、首先用root节点上的UIDofSession更新strUIDofSession
        //strUIDofSession = oResponse.selectSingleNode("//root").getAttribute("UIDofSession");
        
        //1、有错误信息返回
        if(oResponse.selectNodes("/*/errors").length > 0){      
          
          var objErroXMLNode = oResponse.selectNodes("/*/errors");//错误对象
          window.showModalDialog("../htc/error.htm", objErroXMLNode,"dialogHeight:8;dialogWidth:25;status:no;scroll:no"); 
          element.responseState=intResponseState = -2;//设置错误状态值
         // evtError.fire(); //抛出错误
         __jhtcDispatchEvent(element,"onError");
          return(-1); 
        }
        
        //2、有提示信息返回
        if(oResponse.selectNodes("/*/alerts").length > 0){
          var objAlertXMLNode = oResponse.selectNodes("/*/alerts");//提示信息对象
          window.showModalDialog("../htc/alert.htm", objAlertXMLNode,"dialogHeight:8;dialogWidth:25;status:no;scroll:no"); 
          element.responseState=intResponseState = -1;        
          //evtAlert.fire(); //抛出警告.也可能是提示信息   
          __jhtcDispatchEvent(element,"onAlert");                               
        }

        //3、一切正常的情况
	    var objRootNode = oResponse.selectSingleNode("//root[@recordCount]");
        var objTempRNode = oResponse.selectNodes("//r");
        try{
			if(objRootNode && objTempRNode && objTempRNode.length > 1){
				element.__recordCount = objRootNode.getAttribute("recordCount");//总记录数
				element.__startIdx = objRootNode.getAttribute("startIdx");//记录集起点
				element.__recordLimit = objRootNode.getAttribute("recordLimit");//记录集大小
				//删除属性节点
				objRootNode.removeAttribute("recordCount");
				objRootNode.removeAttribute("startIdx");
				objRootNode.removeAttribute("recordLimit");
			}else{
				objRootNode.removeAttribute("recordCount");
				objRootNode.removeAttribute("startIdx");
				objRootNode.removeAttribute("recordLimit");
			}
		}catch(e){}
        objNodeMsgs = oResponse.selectNodes("/*/Msgs")(0);
        objReturn = inputXML();//空的XMLFragement片段
        if(objNodeMsgs && objNodeMsgs.childNodes.length > 0){//如果有MsgBody
          if(intResponseState != -1) element.responseState=intResponseState = 0;
          for(var i = 0; i < objNodeMsgs.childNodes.length; i++){
            objReturn.appendChild(objNodeMsgs.childNodes(i).cloneNode(true));
          }
          return(objReturn);//将Proxy服务器返回的数据返回到客户端        
        }
              
      }else{//异步请求处理----->>>>>>>>>暂不考虑<<<<<<<<<<<
        //oAsynXMLHttp = new ActiveXObject("MSXML2.XMLHTTP");
        //arrAsynXMLHttp(arrAsynXMLHttp.length) = oAsynXMLHttp;
        //oAsynXMLHttp.Syn = false;
        //oAsynXMLHttp.onStateChange = funAsynXMLHttpStateChange(oAsynXMLHttp);
        //oAsynXMLHttp.post();
      }//End of if boolSyn == true
    }//End of post 
 
    function getUUID(intCount){//返回指定个数的UUID的组合的字符串，用“\n”分隔，默认返回个数为一个

      var intSpoolLen;
      var strUUID = "";
      var objUUIDNodeList;
      
      intSpoolLen = arrUUIDSpool.length;//获得当前可用UUID池的长度

      //<MsgBody>
      //   <r UUID="4DA65CCC805242E7982780134FC879FF" />
      //   <r UUID="GDA65CCC80F242E7982780134FC879FF" />
      //   <r UUID="PDA65CCC80H242E7982780134FC879FF" />
      //</MsgBody>
      
      if(intSpoolLen < 15){//当UUID池中可用的UUID少于15则向Proxy发起请求再预取500个
        objXMLHttp.Open("POST",element.proxyLocation,false);    
        objXMLDoc.loadXML("<?xml version='1.0' encoding='GBK'?><root UIDofSession='" + strUIDofSession + "' TimeStmp='' Tran='0'><MsgBody target='UUIDArray' action='Refresh' UIDofMesg='' Priority='0' Syn='0'><Parameters><Parameter Name='UUIdCounts' Val='500' /></Parameters></MsgBody></root>");
        objXMLHttp.send(objXMLDoc); //请求Proxy返回指定数量的UUID        
        objUUIDNodeList = inputXML(objXMLHttp.responseText).selectNodes("//r");
        var tmpLength = objUUIDNodeList.length;//Proxy返回的UUID的个数
        
        for(var i = 0; i < tmpLength; i++){//将服务器返回的UUID装入到UUID池
          strUUID = objUUIDNodeList(i).getAttribute("UUID");
          if(strUUID != null && strUUID != "")
          arrUUIDSpool.push(strUUID);
        }
      }    

      strUUID = arrUUIDSpool.pop();
      while(strUUID == null || strUUID == ""){
        strUUID = arrUUIDSpool.pop();
      }
      return(strUUID);        
    }//End of getUUID
  
  
    function funAsynXMLHttpStateChange(anyXMLHttp){
      
      switch(anyXMLHttp.state){
        case 4:
          event.srcXMLHttp = anyXMLHttp; //event附加属性，标识触发当前事件的XMLHttp对象
          fire(onResponse);
          break;
        case 3:
          break;
        case 2:
          break;
        case 1:
          break;
      }//End of switch

      //从队列中找到触发当前事件的XMLHttp对象并回收资源
      for(var i = 0; i < arrAsynXMLHttp.length; i++){
        if(arrAsynXMLHttp(i) == anyXMLHttp){
          arrAsynXMLHttp(i) = null;
          anyXMLHttp = null;
        }//End of if
      }//End of for
    }//End of funAsynXMLHttpStateChange
  
    function showHelp(iHelpID){//显示上下相关的在线帮助
                              //1、通过HelpID在HelpFragement中查看是否存在相关项目的帮助
                              //2、如果存在，则通过XSLT模板将其转换成HTML
                              //3、showModelDialog，并将其about:default的body的innerHTML换成帮助文档
                              //4、如果不存在，则alert：没有相关主题的帮助
    }//End of showHelp
    
    function putHelpBases(){
      try{
          objXMLHttp.open("GET",this.helpBase);
          objHelpFragement.loadXML(objXMLHttp.responseXML);
        }catch(e){
      }//End of try          
    }//End of putHelpBase  
    
    function getXMLDocumentInst(){//获取一个DOM实例
		var objReturnXMLDom;
		objReturnXMLDom = new ActiveXObject("MSXML2.DOMDocument");
		objReturnXMLDom.async = false;
		objReturnXMLDom.setProperty("ServerHTTPRequest",true);
		objReturnXMLDom.setProperty("SelectionLanguage", "XPath");
		return objReturnXMLDom;
    }

    function initialize(){//初始化操作
    
	    element.UDDI=strUIDofSession = getSysInfo("sessionId");	
      var oMsgBody;
      if(objXMLDoc) objXMLDoc.setProperty("SelectionLanguage", "XPath");
      window.__objGlobalCommonInst = element;
      setGlobCommInst(window);
      if(!objXMLDoc){//确认工厂已经创建
        objXMLDoc = getXMLDocumentInst();
      }
      if(!objXMLHttp){//确认发送对象已经创建
        objXMLHttp = new ActiveXObject("MSXML2.XMLHTTP");
      }
      intMsgBodySpoolLength = 0; //MsgBody堆的大小
      intFreeMsgBodyCount = 0;//目前空闲能够分配的MsgBody
      arrMsgBody = new Array();
      
      for(var i = 0; i < window.frames.length; i++){//
        window.frames[i].document.onreadystatechange = funAdjustReadyState;
        if(window.frames[i].document.body)
          window.frames[i].document.body.oncontentReady = initFrameGlobalCommon; 
      } 
      /*
      for(var i = 0; i < 20; i++){//预装配20个MessageBody备用
        oMsgBody = element.document.createElement("<ehfs:ms/>");//创建一个新的MsgBody      
        oMsgBody = window.document.appendChild(oMsgBody); 
        oMsgBody.init = "1";   
        oMsgBody.inUseState = "free";
        arrMsgBody[intMsgBodySpoolLength] = oMsgBody;//将其加入到堆中
        intMsgBodySpoolLength += 1;//堆大小加1
        intFreeMsgBodyCount += 1;
      }    
      */
      //InitDicList();
      /*
      InitInstitutionList();//
      */
      //createLoadingInfo();
    }
    
    function createLoadingInfo(){//loading
		//debugger;
		objLoadingDialog = element.document.createElement("<div style='position:absolute;background:#cccccc;border:outset 2px;'/>");
		
		var loadWindowTitle = element.document.createElement("<div style='position:absolute;font-size:9pt;color:white;background:#000080;'/>");
		with(loadWindowTitle.style){
			width = 195;
			height = 20;
		}
		var objSpanForTitle = element.document.createElement("<span style='position:absolute;font-size:9pt;'/>");
		with(objSpanForTitle.style){
			left = 4;
			top = 4;
		}
		objSpanForTitle.innerText = "提示...";
		objSpanForTitle = loadWindowTitle.appendChild(objSpanForTitle);
		
		var loadWindowMain = element.document.createElement("<div style='position:absolute;text-align:center;font-size:9pt;'/>");
		with(loadWindowMain.style){
			width = 200;
			top = 25;
		}
		loadWindowMain.innerText = "正在加载数据,请稍候...";
		
		loadWindowTitle = objLoadingDialog.appendChild(loadWindowTitle);
		loadWindowMain = objLoadingDialog.appendChild(loadWindowMain);
		
		with(objLoadingDialog.style){
			left = parseInt(screen.availWidth)/2 - 100;
			top = parseInt(screen.availHeight)/2 - 23;
			width = 200;
			height = 55;
			visibility = "hidden";
			zIndex = 9999;
		}
		//window.prompt("",objLoadingDialog.outerHTML);
    }
    
    function InitInstitutionList(){//初始化医疗机构
        var xslt = "<?xml version='1.0' encoding='GBK'?><xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:fo='http://www.w3.org/1999/XSL/Format'><xsl:template match='/'>        <xsl:element name='root'><xsl:apply-templates select='//r'/></xsl:element ></xsl:template><xsl:template match='r'><xsl:copy-of select='.'/></xsl:template></xsl:stylesheet>";
		objXMLHttp.Open("POST",element.proxyLocation,false);  
		objXMLDoc.loadXML("<?xml version='1.0' encoding='GBK'?><root UIDofSession='0' TimeStmp='' Tran='0'><MsgBody target='医疗机构' action='列出全部' UIDofMesg='' Priority='0' Syn='0'/></root>");    
		objXMLHttp.send(objXMLDoc); 
		objInitInstitutionList = transformXMLToNode(objXMLHttp.responseText,xslt);      
	}
	
	function getInstitutionListDic(){//获取医疗机构列表
		return objInitInstitutionList;
	}
    
    function funSetKeyFilter(){			
							
			if(window.event && event.keyCode == 116){
				event.keyCode = 0;
				event.returnValue = false;				
			}else{
				for(var i = 0; i < window.frames.length; i++){//					
					if(window.frames[i].event){						
						if(window.frames[i].event.keyCode == 116){
							window.frames[i].event.keyCode = 0;
							window.frames[i].event.returnValue = false;
							break;
						}						
					}					
				} 
			}			
		}
		
		function funAdjustReadyState(){
			var boolAllReady = true;
			for(var i = 0; i < window.frames.length; i++){//	
        try{
          if((!window.frames[i].document.body) || (window.frames[2].document.readyState != "complete")){
            boolAllReady = false;
            return;				  
            break;
          }
        }catch(e){
        }
			}			
			if(boolAllReady){
				initFrameGlobalCommon();
			}								
		}
		
	function NavigateIFrame(strLocation){
        window.frames[2].navigate(strLocation);
        window.setInterval(funAdjustReadyState, 500);
    }

		
    function initFrameGlobalCommon(){   
      var vTagName;
      window.__objGlobalCommonInst = element;      
			for(var i=0;i<window.frames.length;i++){
			//////////////////////////////////
			/*
			  window.frames[i].document.onkeydown = funSetKeyFilter;
			  window.frames[i].document.body.onkeydown = funSetKeyFilter;
			  window.frames[i].document.oncontextmenu= funNoContexMenu;

			  window.frames[i].document.onselectstart = funNoContexMenu;
			  window.frames[i].document.body.oncontextmenu= funNoContexMenu;        
			*/
        ///////////////////////////////////
			}      
    }
    

    
    function funNoContexMenu(){//屏蔽菜单			
			if(window.event){
				event.returnValue = false;
			}else{
				for(var i = 0; i < window.frames.length; i++){//					
					if(window.frames[i].event){						
						window.frames[i].event.returnValue = false;      
						break;
					}
				} 
			}			
    }
    
    function formatXMLDateTime(argXML,argDateTimePropName,argDatePropName,argTimePropName){//将XML中的日期型数据格式化为可插入oracle数据库的格式
                                                                                          //该操作应该在post之前进行处理 
      var objXML;
      if(!argXML) return;

      if(!xslDateTime){
        xslDateTime = inputXML('<?xml version="1.0" encoding="gb2312"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="xml" version="1.0" encoding="gb2312" indent="no"/><xsl:variable name="DateTimePropName" select="\'\'"/><xsl:variable name="DatePropName" select="\'\'"/><xsl:variable name="TimePropName" select="\'\'"/><xsl:template match="/*"><xsl:copy><xsl:copy-of select="@*"/><xsl:for-each select="*"><xsl:call-template name="childNode"/></xsl:for-each></xsl:copy></xsl:template><xsl:template name="childNode"><xsl:copy><xsl:for-each select="@*"><xsl:choose><xsl:when test="contains($DateTimePropName,name())"><xsl:call-template name="FormatDateTime"><xsl:with-param name="mode" select="\'datetime\'"/></xsl:call-template></xsl:when><xsl:when test="contains($DatePropName,name())"><xsl:call-template name="FormatDateTime"/></xsl:when><xsl:when test="contains($TimePropName,name())"><xsl:call-template name="FormatDateTime"><xsl:with-param name="mode" select="\'time\'"/></xsl:call-template></xsl:when><xsl:otherwise><xsl:copy/></xsl:otherwise></xsl:choose></xsl:for-each><xsl:for-each select="*"><xsl:call-template name="childNode"/></xsl:for-each></xsl:copy></xsl:template><xsl:template name="FormatDateTime"><xsl:param name="mode" select="\'date\'"/><xsl:variable name="dtvalue" select="."/><xsl:attribute name="{name()}"><xsl:choose><xsl:when test="$mode != \'time\' and $dtvalue = \'1900-01-01T00:00:00\'"/><xsl:otherwise><xsl:if test="$mode != \'time\'"><xsl:value-of select="concat(substring-before($dtvalue,\'-\'),\'年\',substring-before(substring-after($dtvalue,\'-\'),\'-\'),\'月\',substring-before(substring-after(substring-after($dtvalue,\'-\'),\'-\'),\'T\'),\'日\')"/></xsl:if><xsl:if test="$mode !=\'date\'"><xsl:value-of select="substring-after($dtvalue,\'T\')"/></xsl:if></xsl:otherwise></xsl:choose></xsl:attribute></xsl:template></xsl:stylesheet>');
      }
      //设置xml属性节点过滤器
      xslDateTime.ownerDocument.setProperty("SelectionLanguage","XSLPattern"); //不设置会出现不识别xsl前缀的错误
      xslDateTime.selectSingleNode("//xsl:variable[@name='DateTimePropName']/@select").nodeValue = "'" + argDateTimePropName + "'";
      xslDateTime.selectSingleNode("//xsl:variable[@name='DatePropName']/@select").nodeValue = "'" + argDatePropName + "'";
      xslDateTime.selectSingleNode("//xsl:variable[@name='TimePropName']/@select").nodeValue = "'" + argTimePropName + "'";
      xslDateTime.ownerDocument.setProperty("SelectionLanguage","XPath");

      switch(typeof(argXML)){
        case "string":
          objXML = inputXML(argXML);
	        if(objXML) return objXML.transformNode(xslDateTime.childNodes(0));
	        else return "";
	        break;
        case "object":
          return argXML.transformNode(xslDateTime.childNodes(0));
          break;
        default:
          return "";
	      break;
      }
    }
    
  
    function formatDateTimeToCN(argDateString,argFormat){ //格式化包含日期的字符串为中文显示的日期
                                                          //argDateString是字符串,argFormat是需要的格式,默认为"date"
      var vTemp;
      var arrTemp;
      var vMonth;

      /**********Oracl数据库日期格式转换******************************************/
      if(!argDateString) return "";
      vTemp = argDateString.replace(/1900.(01|1).(01|1)(.(0{1,2}).(0{1,2}).(0{1,2}).?)/g,"");
      if(vTemp){
		    vTemp = vTemp.replace("日","").replace("月","").replace("年","");
        arrTemp = vTemp.replace(/\D/g,",");
        arrTemp = arrTemp.split(",");
        
          switch(argFormat){
        
            case "oracl"://日期格式(例如:02-02月-2003)
              if(arrTemp.length == 3){
			          if(arrTemp[0].indexOf("0")==0){
				          arrTemp[0] = arrTemp[1].replace("0","");
				        }
			          if(arrTemp[1].indexOf("0")==0){
				          arrTemp[1] = arrTemp[1].replace("0","");
				        }
				        if(arrTemp[0].length == 4){//2004-04-19
				          vTemp = arrTemp[2] + "-" + arrTemp[1] + "月" + "-" + arrTemp[0];
				        }else if((arrTemp[0].length == 2 && parseInt(arrTemp[0]) > 31) || arrTemp[0] == "00"){//99-04-19 || 00-04-19
				          vTemp = arrTemp[2] + "-" + arrTemp[1] + "月" + "-" + arrTemp[0];
				        }else{
				          vTemp = arrTemp[0] + "-" + arrTemp[1] + "月" + "-" + arrTemp[2];
				        }
                
              }else vTemp = argDateString;            
              break;       
                   
            default://日期类型(格式:yyyy年mm月dd日)
              
              if(arrTemp.length == 3){            
                if(arrTemp[1].length == 1) arrTemp[1] = "0" + arrTemp[1];
                if(arrTemp[2].length == 1) arrTemp[2] = "0" + arrTemp[2];
                vTemp = arrTemp[0] + "-" + arrTemp[1] + "-" + arrTemp[2]; //yyyy-mm-dd             
              }else vTemp = "";                      
              break;
          }
        }
        return vTemp;//返回转换后的日期
    }   
    
      
 	  function inputXML(argSource){//返回xmlDOMFragement对象

  		  var objXMLFragment = null;
  		  var vTempSource = "";
	  	  try{
			    switch(typeof(argSource)){
				    case "string":
				  	  if(argSource.search(/\./) != -1 && argSource.search(/\</) == -1){ //xml file's location
				  		  if(argSource.search(/\+/) != -1) argSource = eval(argSource);										  		  
				  		  objXMLDoc.async = false;			  		  
					  	  objXMLDoc.load(argSource);
					  	  objXMLFragment = objXMLDoc.createDocumentFragment();
					  	  objXMLFragment.appendChild(objXMLDoc.documentElement);
					  	  break;
					    }
					    if(argSource.search(/\</) != -1){ //xml string
					      argSource = argSource.replace(/xmlns:fo=\"\"/g,"");					      
					  	  objXMLDoc.loadXML(argSource);
					  	  objXMLFragment = objXMLDoc.createDocumentFragment();
					  	  objXMLFragment.appendChild(objXMLDoc.documentElement);
					  	  break;
					    }
					    try{
					      argSource = argSource.replace(/xmlns:fo=\"\"/g,"");
						    objXMLFragment = eval(argSource);//xml data island
					    }catch(e){					
				  	  }
					    break;
  					  
				    case "object":
					    if(argSource.xml) 
					      vTempSource = argSource.xml;		
					      vTempSource = vTempSource.replace(/xmlns:fo=\"\"/g,"");			      
						    objXMLDoc.loadXML(vTempSource);//xml document object
						    objXMLFragment=objXMLDoc.createDocumentFragment();
						    objXMLFragment.appendChild(objXMLDoc.documentElement);
					    break;
					  case "undefined":
					    objXMLFragment = objXMLDoc.createDocumentFragment();
					    break;
				    default:
					    objXMLFragment = null;					  
			    }

	  	  }catch(err){
			    objXMLFragment = null; 
		    }
		    return objXMLFragment;
	  }
	  
	
	  function createElement(strTagName){  	
	    return(objXMLDoc.createElement(strTagName));  	
	  }	  
	 
	  
    function transformDicXML(argXML,argXSLT){ //字典格式化专用
      var objXMLFragment,objXSLTFragment;
      var vResult = "";
      var objNode;

      if(argXML && argXML.xml){
        
        objXSLTFragment = inputXML(argXSLT);

        if(objXSLTFragment){
          vResult = argXML.transformNode(objXSLTFragment.childNodes(0));
          while(objXSLTFragment && objXSLTFragment.childNodes(0)){
            objNode = objXSLTFragment.removeChild(objXSLTFragment.childNodes(0));
            objNode = null;
          }
          objXSLTFragment = null;
          delete objXSLTFragment;
  	    }
        
      }
      vResult = vResult.replace(/<\?xml[0-9a-zA-Z\s\'\"=\-.\?]{0,}>/g,"");
      
      return vResult;
    }

	  function transformXML(argXML,argXSLT){ //xml的xsl格式化,结果为字符串
      var objXMLFragment,objXSLTFragment;
      var vResult = "";
      var objNode;

      objXMLFragment = inputXML(argXML);
      if(objXMLFragment){
        objXSLTFragment = inputXML(argXSLT);

        if(objXSLTFragment){
          vResult = objXMLFragment.transformNode(objXSLTFragment.childNodes(0));
          while(objXSLTFragment && objXSLTFragment.childNodes(0)){
            objNode = objXSLTFragment.removeChild(objXSLTFragment.childNodes(0));
            objNode = null;
          }
          objXSLTFragment = null;
          delete objXSLTFragment;

  	    }
        while(objXMLFragment && objXMLFragment.childNodes(0)){
          objNode = objXMLFragment.removeChild(objXMLFragment.childNodes(0));
          objNode = null;
        }
        objXMLFragment = null;
        delete objXMLFragment;

      }
      vResult = vResult.replace(/<\?xml[0-9a-zA-Z\s\'\"=\-.\?]{0,}>/g,"");
      
      return vResult;
    }

    function transformXMLToNode(argXML,argXSLT){ //xml的xsl格式化,结果为document-fragment对象
      var vResult = transformXML(argXML,argXSLT);
      if(vResult) return inputXML(vResult);
      else return null;
    }
	

	  function freeMsgBody(objMsgBody){
	    var n = objMsgBody.ChildMsgBodysCount;
  	  
	    for(var i = 0; i < n; i++){
	      freeMsgBody(objMsgBody.ChildMsgBodys[i]);
	    }
  	  
	    objMsgBody.parseState = "free";	
      objMsgBody.init = "1";
	  }
	
	  function setGlobCommInst(objFrameWin){
  
        if(objFrameWin.frames.length>0){
          for(var i = 0; i < objFrameWin.frames.length; i++){
            setGlobCommInst(objFrameWin.frames(i)); 
          }
        }else{          
          objFrameWin.__objGlobalCommonInst = element;
        }  
    }
    
    function RefreshDictionary(){      
      arrDictionary.RemoveAll();      
      InitDicList();
    }
    
    function InitDicList(){//字典初始化
    
        var objDicList;
        var objDicItem;
        var objNodeList;          
        var strDicName;
        
        //以下为checkXML
        objDicList = inputXML("http://" + window.location.host + "/ehfs/web/dics/diclist.xml");
        if(!objDicList){
          window.status = "无法连接服务器，字典初始化失败，请联系系统管理员...";
          return(-1);
        } 
       
        objNodeList = objDicList.selectNodes("/*/r");
        
        for(var i = 0; i < objNodeList.length; i++){//字典初始化                    
            strDicName = objNodeList(i).getAttribute("DicName");
            objDicItem = inputXML("http://" + window.location.host + "/ehfs/web/dics/" + objNodeList(i).getAttribute("XMLFileName"));            
            arrDictionary.Add(strDicName, objDicItem);
        }//End of for 

        /*<XML ID="DServeState" desc="设备可用状态">
              <r value="U" text="在用"/><r value="S" text="停用"/><r value="E" text="检修"/>
          </XML>*/
        arrDictionary.Add("设备可用状态", inputXML('<root><r value="U" text="在用"/><r value="S" text="停用"/><r value="E" text="检修"/></root>'));
        arrDictionary.Add("姓名", inputXML('<root><r value="U" text="刘鹏飞"/><r value="S" text="许哲源"/><r value="E" text="李侠"/></root>'));
///////////////////////////////////////////////////////////
        arrDictionary.Add("国籍", inputXML('<root><r value="ci" text="china"/><r value="S" text="USA"/><r value="E" text="russin"/></root>'));
	arrDictionary.Add("是否在岗", inputXML('<root><r value="ca" text="是"/><r value="sd" text="否"/><r value="dd" text="未知"/></root>'));

	arrDictionary.Add("性别", inputXML('<root><r value="i" text="男"/><r value="S" text="女"/></root>'));
	arrDictionary.Add("角色选择", inputXML('<root><r value="N" text="系统管理员"/><r value="P" text="一般用户"/></root>'));
	arrDictionary.Add("婚否", inputXML('<root><r value="x" text="yes"/><r value="g" text="no"/></root>'));
	arrDictionary.Add("机构类型", inputXML('<root><r value="ti" text="国企"/><r value="op" text="私营"/></root>'));
	arrDictionary.Add("机构状态", inputXML('<root><r value="bv" text="优良"/><r value="xz" text="一般"/></root>'));
	arrDictionary.Add("健康状况", inputXML('<root><r value="A" text="好"/><r value="SA" text="一般"/><r value="DF" text="差"/></root>'));
	arrDictionary.Add("学历", inputXML('<root><r value="V" text="本科"/><r value="J" text="专科"/></root>'));
	arrDictionary.Add("专业", inputXML('<root><r value="SW" text="计算机"/><r value="J" text="信息管理"/></root>'));
	arrDictionary.Add("血型", inputXML('<root><r value="f" text="A"/><r value="r" text="B"/></root>'));
	arrDictionary.Add("角色等级", inputXML('<root><r value="F" text="高"/><r value="DS" text="低"/></root>'));
	arrDictionary.Add("所在部门", inputXML('<root><r value="yt" text="技术部"/><r value="yh" text="运营部"/></root>'));
	arrDictionary.Add("角色状态", inputXML('<root><r value="sz" text="高"/><r value="io" text="低"/></root>'));
	arrDictionary.Add("是否", inputXML('<root><r value="sz" text="是"/><r value="io" text="否"/></root>'));
///////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////
         /*<XML ID="DServeState" desc="用户状态">
              <r value="U" text="在用"/><r value="S" text="停用"/>
          </XML>*/
        arrDictionary.Add("用户状态", inputXML('<root><r value="U" text="在用"/><r value="S" text="停用"/></root>'));

        /*<XML ID="PServeState" desc="人员状态">
              <r value="U" text="在职可用"/><r value="R" text="退休"/><r value="S" text="调离"/>
          </XML>*/
        arrDictionary.Add("人员可用状态", inputXML('<root><r value="U" text="在职可用"/><r value="R" text="退休"/><r value="S" text="调离"/></root>'));

        /*<XML ID="ManagementCode" desc="机构分类管理代码">
              <r value="Y" text="营利"/><r value="N" text="非营利"/>
          </XML>*/
        arrDictionary.Add("机构分类管理代码",inputXML('<root><r value="Y" text="营利"/><r value="N" text="非营利"/></root>'));
        
        /*<XML ID="InOutFlag" desc="入出转标志">
              <r value="I" text="入院"/><r value="O" text="出院"/><r value="T" text="转科"/>
          </XML>*/
        arrDictionary.Add("入出转标志",inputXML('<root><r value="I" text="入院"/><r value="O" text="出院"/><r value="T" text="转科"/></root>'));
        
        /*<XML ID="IfImport" desc="是否进口">
              <r value="0" text="进口"/><r value="1" text="非进口"/>
          </XML>*/
        arrDictionary.Add("是否进口",inputXML('<root><r value="0" text="进口"/><r value="1" text="非进口"/></root>'));

        /*<XML ID="IfNew" desc="是否新设备">
              <r value="0" text="新设备"/><r value="1" text="旧设备"/>
          </XML>*/
        arrDictionary.Add("是否新设备",inputXML('<root><r value="0" text="新设备"/><r value="1" text="旧设备"/></root>'));
         arrDictionary.Add("慢病字典", transformXMLToNode(arrDictionary.Item("ICD10字典"), "<?xml version='1.0' encoding='UTF-8'?><xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:fo='http://www.w3.org/1999/XSL/Format'><xsl:template match='//root'><xsl:copy><xsl:apply-templates select='r'/></xsl:copy></xsl:template><xsl:template match='r[@C != \"N\"]'><xsl:copy-of select='.'/></xsl:template></xsl:stylesheet>"));

        arrDictionary.Add("传染病字典", transformXMLToNode(arrDictionary.Item("ICD10字典"), "<?xml version='1.0' encoding='UTF-8'?><xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:fo='http://www.w3.org/1999/XSL/Format'><xsl:template match='//root'><xsl:copy><xsl:apply-templates select='r'/></xsl:copy></xsl:template><xsl:template match='r[@I != \"N\"]'><xsl:copy-of select='.'/></xsl:template></xsl:stylesheet>"));
         /*<XML ID="ServeState" desc="帐户可用状态">
        <r value='Y' txt="可用"/><r value='N' txt='不可用'/>
        </XML>*/
        arrDictionary.Add("帐户可用状态",inputXML("<root><r value='Y' text='可用'/><r value='N' text='不可用'/></root>"));
        
        /*<XML ID="IfHerbal" desc="是否是中药">
        <r value='Y' txt="可用"/><r value='N' txt='不可用'/>
        </XML>*/
        arrDictionary.Add("是否是中药",inputXML("<root><r value='Y' text='中药'/><r value='N' text='西药'/></root>"));

        /*<XML ID="AccidentLevel" desc="公共卫生事件级别">
        <r value='0' txt="一般"/><r value='1' txt='严重'/><r value='2' txt='重大'/>
        </XML>*/
        arrDictionary.Add("公共卫生事件级别",inputXML("<root><r value='0' text='0级'/><r value='1' text='1级'/><r value='2' text='2级'/></root>"));
        
        
        /*<XML ID="TransfusionFeedback" desc="输血反应">
              <r value="N" text="无"/><r value="Y" text="有"/>
          </XML>*/
        arrDictionary.Add("输血反应",inputXML('<root><r value="N" text="无"/><r value="Y" text="有"/></root>'));

        /*<XML ID="ReceiveState" desc="公文接收状态">
              <r value="0" text="待接收"/><r value="1" text="已接收"/>
          </XML>*/
        arrDictionary.Add("公文接收状态",inputXML('<root><r value="0" text="待接收"/><r value="1" text="已接收"/></root>'));

        /*<XML ID="DocLevel" desc="公文级别">
            <r value="0" text="低"/>
            <r value="1" text="普通"/>
            <r value="2" text="高"/>
          </XML>*/
        arrDictionary.Add("公文级别",inputXML('<root><r value="0" text="低"/><r value="1" text="普通"/><r value="2" text="高"/></root>'));
        

        /*<XML ID="WoundCloseDegree" desc="切口愈合等级">
              <r value="1" text="一级"/><r value="2" text="二级"/><r value="3" text="三级"/><r value="4" text="四级"/><r value="5" text="五级"/>
          </XML>*/
        arrDictionary.Add("切口愈合等级",inputXML('<root><r value="1" text="一级"/><r value="2" text="二级"/><r value="3" text="三级"/><r value="4" text="四级"/><r value="5" text="五级"/></root>'));
        
         /*<XML ID="ChronicDiseaseReportState" desc="慢病报告卡状态">
                <r value="2" text="确诊"/><r value="4" text="死亡"/><r value="5" text="排除"/><r value="6" text="作废"/>
          </XML>*/
        arrDictionary.Add("慢病报告卡状态",inputXML('<root><r value="2" text="确诊"/><r value="4" text="死亡"/><r value="5" text="排除"/><r value="6" text="作废"/></root>'));

        /*<XML ID="InfectionReportState" desc="传染病报告卡状态">
                <r value="1" text="疑似"/><r value="2" text="确诊"/><r value="3" text="治愈"/><r value="4" text="死亡"/><r value="5" text="排除"/><r value="6" text="作废"/>
          </XML>*/
        arrDictionary.Add("传染病报告卡状态",inputXML('<root><r value="1" text="疑似"/><r value="2" text="确诊"/><r value="3" text="治愈"/><r value="4" text="死亡"/><r value="5" text="排除"/><r value="6" text="作废"/></root>'));


        /*<XML ID="InoculationHistory" desc="预防接种史">
                <r value="1" text="已种"/><r value="2" text="未种"/>
          </XML>*/
        arrDictionary.Add("预防接种史",inputXML('<root><r value="1" text="已种"/><r value="2" text="未种"/></root>'));

        /*<XML ID="ReceiveMan" desc="接生人">
                <r value="1" text="有"/><r value="2" text="没有"/>
          </XML>*/
        arrDictionary.Add("接生人",inputXML('<root><r value="1" text="有"/><r value="2" text="没有"/></root>'));

        /*<XML ID="InoculationHistory" desc="传染病类别">
                <r value="1" text="甲类"/><r value="2" text="乙类"/><r value="3" text="丙类"/>
          </XML>*/
        arrDictionary.Add("传染病类别",inputXML('<root><r value="4" text="按甲类处理"/><r value="1" text="甲类"/><r value="2" text="乙类"/><r value="3" text="丙类"/></root>'));        
        /*
          <XML ID="IfRelease" desc="上报状态">
            <r value="0" text="未上报"/><r value="1" text="已上报"/>
          </XML>
        */       
       arrDictionary.Add("上报状态",inputXML('<root><r value="0" text="未上报"/><r value="1" text="已上报"/><r value="3" text="作废"/></root>'));
       arrDictionary.Add("上报年份",inputXML('<root><r value="1998" text="1998"/><r value="1999" text="1999"/><r value="2000" text="2000"/><r value="2001" text="2001"/><r value="2002" text="2002"/><r value="2003" text="2003"/><r value="2004" text="2004"/><r value="2005" text="2005"/><r value="2006" text="2006"/><r value="2007" text="2007"/><r value="2008" text="2008"/><r value="2009" text="2009"/><r value="2010" text="2010"/></root>'));
  }

  function getDictionary(strDicName){//返回各种字典
 
    if(arrDictionary.Exists(strDicName)){
      return(arrDictionary.Item(strDicName));
    }
  }
  
  
  function login(strUName, strUPassword){//登录，并获得一些全局变量，将它们添加到动态属性数组中
                                         //其中UIDofSession是最重要的，用于以后的发送过程中每次携带
    var objFragement;
    var objNode;

    objXMLDoc.loadXML("<root UIDofSession='0' TimeStmp='' Tran='0'><MsgBody target='用户' action='登录' UIDofMesg='' Priority='0' Syn='0'><Parameters><Parameter Name='UName' Val='" + strUName + "' /><Parameter Name='UPassword' Val='" + strUPassword + "'/></Parameters></MsgBody></root>");
    objXMLHttp.send(objXMLDoc);        
    objFragement = inputXML(objXMLHttp.responseText);//得到返回，并装配为XMLFragement
    
    objNode = objFragement.selectSingleNode("//r");
    arrProperty["DEPTCODE"] = objFragement.childNodes(0).getAttribute("DEPTCODE");
    
    element.UDDI=strUIDofSession = objFragement.childNodes(0).getAttribute("UIDOFSESSION");
    arrProperty["UIDOFSESSION"] = strUIDofSession;
    arrProperty["MEDICALINSTITUTION"] = objFragement.childNodes(0).getAttribute("MEDICALINSTITUTION");   
    arrProperty["IDOFMEDICALINSTITUTION"] = objFragement.childNodes(0).getAttribute("IDOFMEDICALINSTITUTION");   
    
    arrProperty["IDOFUSERBASE"] = objNode.getAttribute("IDOFUSERBASE");
    arrProperty["SERVESTATE"] = objNode.getAttribute("SERVESTATE");
    
    funInitSectionList();
    funInitMedicalManList();
    funInitRegionalismList();
  }
  
  function funInitSectionList(){
    objXMLHttp.Open("POST",element.proxyLocation,false);  
    objXMLDoc.loadXML("<?xml version='1.0' encoding='GBK'?><root UIDofSession='" + arrProperty["UIDOFSESSION"] + "' TimeStmp='' Tran='0'><MsgBody target='科室资源' action='列出全部科室名称' UIDofMesg='' Priority='0' Syn='0'/></root>");
    objXMLHttp.send(objXMLDoc); 
    objSectionListXML = transformXML(objXMLHttp.responseText, "<?xml version='1.0' encoding='GBK'?><xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:fo='http://www.w3.org/1999/XSL/Format'><xsl:template match='/'>	<xsl:element name='root'><xsl:apply-templates select='//r'/></xsl:element ></xsl:template><xsl:template match='r'><xsl:copy-of select='.'/></xsl:template></xsl:stylesheet>");    
  }
  
  function funInitMedicalManList(){    
    if(arrProperty["IDOFMEDICALINSTITUTION"] != "-1" && arrProperty["IDOFMEDICALINSTITUTION"] != "-2"){
        objMedicalManListXML = transformXML("http://192.168.1.55:7001/dhrm/dics/medicalManDic.xml",'<?xml version="1.0" encoding="GBK"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format"><xsl:template match="/"><xsl:element name="root"><xsl:apply-templates select="//r"/></xsl:element></xsl:template><xsl:template match="r"><xsl:if test="@IDOFMEDICALINSTITUTION=\'' + arrProperty["IDOFMEDICALINSTITUTION"] + '\'"><xsl:copy-of select="."/></xsl:if></xsl:template></xsl:stylesheet>');
    }else{
       objMedicalManListXML = inputXML("http://192.168.1.55:7001/dhrm/dics/medicalManDic.xml").xml; 
    }
  }
  
  function funInitRegionalismList(){
    objXMLHttp.Open("POST",element.proxyLocation,false);  
    objXMLDoc.loadXML("<?xml version='1.0' encoding='GBK'?><root UIDofSession='" + arrProperty["UIDOFSESSION"] + "' TimeStmp='' Tran='0'><MsgBody target='准字典' action='列出所有行政区域' UIDofMesg='' Priority='0' Syn='0'/></root>");    
    objXMLHttp.send(objXMLDoc); 
    objRegionalismListXML = transformXML(objXMLHttp.responseText, "<?xml version='1.0' encoding='GBK'?><xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:fo='http://www.w3.org/1999/XSL/Format'><xsl:template match='/'>	<xsl:element name='root'><xsl:apply-templates select='//r'/></xsl:element ></xsl:template><xsl:template match='r'><xsl:copy-of select='.'/></xsl:template></xsl:stylesheet>");      
  }
  
  function InitFromContext(objNode){
    
    if(!objNode || !objNode.xml){
      alert("请重新登录。");
      return(-1);
    }

    arrProperty["DEPTCODE"] = objNode.getAttribute("DEPTCODE");
    strUIDofSession = objNode.getAttribute("UIDOFSESSION");
    arrProperty["UIDOFSESSION"] = strUIDofSession;
    
    arrProperty["UNAME"] = objNode.getAttribute("UNAME");   
    arrProperty["INSTITUTIONNAME"] = objNode.getAttribute("INSTITUTIONNAME");   
    arrProperty["IDOFMEDICALINSTITUTION"] = objNode.getAttribute("IDOFMEDICALINSTITUTION"); 
    
    arrProperty["IDOFUSERBASE"] = objNode.getAttribute("IDOFUSERBASE");
    arrProperty["SERVESTATE"] = objNode.getAttribute("SERVESTATE");
    
    funInitSectionList();
    funInitMedicalManList();
    funInitRegionalismList();
  }

  function getProperty(strPropertyName){//获取动态属性
    if(arrProperty[strPropertyName]){
      return(arrProperty[strPropertyName]);
    }else{
      return(null);
    }  
  }
  
  function getSectionList(){
    return objSectionListXML;
  }
  
  function getMedicalManList(){
    return objMedicalManListXML;
  }
  
  function getRegionalismList(){
    return objRegionalismListXML;
  }
  
  function GetCookie(name){//得到Cookie的值
		var arg = name + "=";
		var alen = arg.length;
		var tmpStr,numBe,numEnd;
		tmpStr = window.document.cookie;
		var clen = tmpStr.length;
		numBe = tmpStr.indexOf(arg);
		if(numBe != -1){
			numBe += alen;
			numEnd = tmpStr.indexOf("; ",numBe);
			if( numEnd == -1 ) numEnd = tmpStr.length;
			return unescape(tmpStr.substring(numBe , numEnd));//返回Cookie数据
		}
		return null; //返回null表示没有登录信息
	}//enf function 

	function getSysInfo(argName){
		return GetCookie(argName);
	}
	
	__jhtcBindPropertyChange(element,"init",initialize);
	__jhtcBindPropertyChange(element,"freeMsgBodyCount",putFreeMsgBodyCount);
	__jhtcBindPropertyChange(element,"helpBase",putHelpBases);
	__jhtcBindPropertyChange(element,"newsWinShow",putNewsWinShow);
	__jhtcBindPropertyChange(element,"actionWinShow",putActionWinShow);
	__jhtcBindPropertyChange(element,"infoWinShow",putInfoWinShow);

  
  	jhtc_attr_init(element,"proxyLocation","http://192.168.1.55:7001/dhrm/mainservlet");
  	jhtc_attr_init(element,"bindLoad","");
  	jhtc_attr_init(element,"init","");
  	jhtc_attr_init(element,"UDDI","");
  	jhtc_attr_init(element,"freeMsgBodyCount","");
  	jhtc_attr_init(element,"helpBase","http://www.viewhigh.com/helpBase");
  	jhtc_attr_init(element,"responseState","");
  	jhtc_attr_init(element,"newsWinShow","");
  	jhtc_attr_init(element,"actionWinShow","");
  	jhtc_attr_init(element,"infoWinShow","");
  	
	element.createMsgBody=createMsgBody;
	element.freeMsgBody=freeMsgBody;
	element.inputXML=inputXML;
	element.post=post;
	element.getUUID=getUUID;
	element.print=print;
	element.showHelp=showHelp;
	element.createElement=createElement;
	element.transformXML=transformXML;
	element.transformDicXML=transformDicXML;
	element.transformXMLToNode=transformXMLToNode;
	element.formatXMLDateTime=formatXMLDateTime;
	element.formatDateTimeToCN=formatDateTimeToCN;
	element.processMessage=processMessage;
	element.getUIDFromCommonPool=getUIDFromCommonPool;
	element.getDictionary=getDictionary;
	element.login=login;
	element.getXMLDocumentInst=getXMLDocumentInst;
	element.setRespliteData=setRespliteData;
	element.getRespliteData=getRespliteData;
	element.setRespliteTitleData=setRespliteTitleData;
	element.getRespliteTitleData=getRespliteTitleData;
	element.setRequestData=setRequestData;
	element.getRequestData=getRequestData;
	element.getInstitutionListDic=getInstitutionListDic;
	element.showDetailDialog=showDetailDialog;
	element.getRFromResponseData=getRFromResponseData;
	
	element.getRootFormMsgBody=getRootFormMsgBody;
	element.InitFromContext=InitFromContext;
	element.getProperty=getProperty;
	element.RefreshDictionary=RefreshDictionary;
	element.NavigateIFrame=NavigateIFrame;
	element.getSectionList=getSectionList;
	element.getMedicalManList=getMedicalManList;
	element.getRegionalismList=getRegionalismList;
	
	element.savePageInfo=savePageInfo;
	element.showNewsWin=showNewsWin;
	element.closeNewsWin=closeNewsWin;
	
	element.showInfoWin=showInfoWin;
	element.closeInfoWin=closeInfoWin;
	element.showActionWin=showActionWin;
	element.closeActionWin=closeActionWin;
	
	element.showLoadingWindow=showLoadingWindow;
	element.hideLoadingWindow=hideLoadingWindow;
	element.getSysInfo=getSysInfo;
	element.init=initialize;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_tag_map["statCmm"]=jhtc_statCommon;