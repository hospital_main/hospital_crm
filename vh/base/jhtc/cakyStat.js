function jhtc_cakyStat(win,jhtc_obj){
var pageWin=win;
var element=jhtc_obj;
var objXMLDoc;
var objContainerDiv;//容器
var objVMLGroup;
var patientTotal = 0;//发病总人数
var vLeft;
var vTop;
var vWidth;
var vHeight;
var vCaption;
var vBackColor = "#ffffff";//背景颜色
var vHasBorder = true;//是否有边框(默认为有)
var booleanHasLegend = true;//是否显示图例
var vBorderColor = "#000000";//边框颜色
var vBorderWeight = 1;//边框像素
var vTagID = "";

//预置饼图分类色
var objCakyColorArray = new Array('#CC0000','#66CC00','#990033','#009900','#006699','#006633','#33FFCC','#99CC66','#66FF66','#333366','#FF6699','#FF3399','#FFCC00','#CCFFFF','#000000','#009999','#0066CC','#6699FF','#3300CC','#336600','#CCCC99','#996699','#9933FF','#CCCCCC','#CC3333','#33CCCC','#FF6699','#FFFF00','#66FF00','#000099','#CC9999','#666600','#CCCC33','#FFFFCC','#FF6600','#336666');
//////////////////////////////////////////////////////////////////
function initialize(){//组件初始化操作
	objContainerDiv = element.document.createElement("<div class='hbos_cs_containerDiv'/>");
	objContainerDiv = element.appendChild(objContainerDiv);
	objContainerDiv.style.visibility = "visible";
	createCaky();//生成画饼图的vml代码
	refresh();
}

function refreshData(){
	if(objContainerDiv){
		objContainerDiv.innerHTML = "";
	}
	createCaky();
}

function createCakyVML(sa,ea,cakyColor,altText,cakySize,vIndex){//sa:起始弧度 ea:结束弧度 cakyColor:饼图颜色 altText:提示内容 cakySize:饼图半径
	var sx=parseInt(2000*Math.sin(Math.PI*2*(sa/360)));
	var sy=parseInt(-2000*Math.cos(Math.PI*2*(sa/360)));
	var ex=parseInt(2000*Math.sin(Math.PI*2*(ea/360)));
	var ey=parseInt(-2000*Math.cos(Math.PI*2*(ea/360)));
	var returnVml = "";
	returnVml += "<v:shape title='"+ altText +"' style='position:absolute;border:solid 0px " + cakyColor + ";z-index:1;left:3000;top:2800;width:4000;height:4000' CoordSize='" + cakySize + "," + cakySize + "' strokeweight='0pt' fillcolor='" + cakyColor + "' strokecolor='" + cakyColor + "' path='m0,0 l "+sx+","+sy+" ar -2000,-2000,2000,2000,"+ex+","+ey+","+sx+","+sy+" l0,0 x e'><v:shadow on='true' color='" + cakyColor + "' offset='1pt,1pt'/><v:fill color2='white' rotate='t' angle='180' type='gradient'/></v:shape>";

	if(parseInt(vIndex) == 0){
		returnVml += "<v:rect style='position:absolute;border:solid 0px black;left:" + (sx + 3200) + ";width:2500;top:" + (sy + 2600) + ";height:300;z-index:2' filled='false' stroked='false'><v:textbox style='position:absolute;border:solid 0px blue;left:0;top:0;font-size:9pt;'>" + altText + "</v:textbox></v:rect>";
	}else if(parseInt(vIndex) == 1){
		returnVml += "<v:rect style='position:absolute;border:solid 0px black;left:" + (sx + 3000) + ";width:2500;top:" + (sy + 2800) + ";height:300;z-index:2' filled='false' stroked='false'><v:textbox style='position:absolute;border:solid 0px blue;left:0;top:0;font-size:9pt;'>" + altText + "</v:textbox></v:rect>";
	}else if(parseInt(vIndex) == 2){
		returnVml += "<v:rect style='position:absolute;border:solid 0px black;left:" + (sx + 2950) + ";width:2500;top:" + (sy + 3200) + ";height:300;z-index:2' filled='false' stroked='false'><v:textbox style='position:absolute;border:solid 0px blue;left:0;top:0;font-size:9pt;'>" + altText + "</v:textbox></v:rect>";	
	}else if(parseInt(ea/3.6) < 40){
		returnVml += "<v:rect style='position:absolute;border:solid 0px black;left:" + (sx + 2800) + ";width:2500;top:" + (sy + 2900) + ";height:300;z-index:2' filled='false' stroked='false'><v:textbox style='position:absolute;border:solid 0px blue;left:0;top:0;font-size:9pt;'>" + altText + "</v:textbox></v:rect>";
	}else if(parseInt(ea/3.6) >= 40 && parseInt(ea/3.6) < 50){
		returnVml += "<v:rect style='position:absolute;border:solid 0px black;left:" + (sx + 2600) + ";width:2500;top:" + (sy + 2900) + ";height:300;z-index:2' filled='false' stroked='false'><v:textbox style='position:absolute;border:solid 0px blue;left:0;top:0;font-size:9pt;'>" + altText + "</v:textbox></v:rect>";		
	}else if(parseInt(ea/3.6) >= 50 && parseInt(ea/3.6) < 55){	
		returnVml += "<v:rect style='position:absolute;border:solid 0px black;left:" + (sx + 2000) + ";width:2500;top:" + (sy + 3000) + ";height:300;z-index:2' filled='false' stroked='false'><v:textbox style='position:absolute;border:solid 0px blue;left:0;top:0;font-size:9pt;'>" + altText + "</v:textbox></v:rect>";	
	}else if(parseInt(ea/3.6) >= 55 && parseInt(ea/3.6) < 70){		
		returnVml += "<v:rect style='position:absolute;border:solid 0px black;left:" + (sx + 1600) + ";width:2500;top:" + (sy + 3000) + ";height:300;z-index:2' filled='false' stroked='false'><v:textbox style='position:absolute;border:solid 0px blue;left:0;top:0;font-size:9pt;'>" + altText + "</v:textbox></v:rect>";	
	}else{
		returnVml += "<v:rect style='position:absolute;border:solid 0px black;left:" + (sx + 1800) + ";width:2500;top:" + (sy + 2400) + ";height:300;z-index:2' filled='false' stroked='false'><v:textbox style='position:absolute;border:solid 0px blue;left:0;top:0;font-size:9pt;'>" + altText + "</v:textbox></v:rect>";	
	}
	return returnVml;
}

function createCaky(){//生成饼图的vml代码	
	var oItemNodesList = null;//分类项目节点
	var vmlEnd = "";//最终的vml代码
	var vmlCaky = "";//饼图vml代码
	var vmlTitle = "";//饼图标题vml代码	
	var vAltText = "";//提示内容
	var lastPercent = 0;//起始百分比
	var curPercent = 0;//结束百分比	
	var vLengendLeft = 0;
	var topScale = 0;
	try{
		oItemNodesList = objXMLDoc.selectNodes("//root/item");
		if(!oItemNodesList || !oItemNodesList.length) return; 
		if(element.hasLegend){
			if(oItemNodesList.length <15){//装配vmlTitle
				vmlTitle = "<v:roundrect style='border:solid 0px green;left:600;top:100;width:6700;height:350;' filled='false' stroked='false'><v:textbox style='border:solid 0px blue;font-size:10.5pt;font-weight:bold;text-align:center;'>" + vCaption + "</v:textbox></v:roundrect>";
			}else if(oItemNodesList.length >= 15 && oItemNodesList.length <30){
				vmlTitle = "<v:roundrect style='border:solid 0px green;left:600;top:100;width:8700;height:350;' filled='false' stroked='false'><v:textbox style='border:solid 0px blue;font-size:10.5pt;font-weight:bold;text-align:center;'>" + vCaption + "</v:textbox></v:roundrect>";	
			}else{
				vmlTitle = "<v:roundrect style='border:solid 0px green;left:600;top:100;width:10200;height:350;' filled='false' stroked='false'><v:textbox style='border:solid 0px blue;font-size:10.5pt;font-weight:bold;text-align:center;'>" + vCaption + "</v:textbox></v:roundrect>";	
			}
		}else{
			vmlTitle = "<v:roundrect style='border:solid 0px green;left:800;top:100;width:5200;height:350;' filled='false' stroked='false'><v:textbox style='border:solid 0px blue;font-size:10.5pt;font-weight:bold;text-align:center;'>" + vCaption + "</v:textbox></v:roundrect>";
		}
		for(var i=0;i<oItemNodesList.length;i++){//装配vmlCaky
			if(parseFloat(oItemNodesList[i].childNodes[1].text) != 0){//
				vAltText = oItemNodesList[i].childNodes[0].text + " " + oItemNodesList[i].childNodes[2].text;
			
				if(i==0){//第一个饼图
					lastPercent = parseFloat(oItemNodesList[i].childNodes[2].text);
					curPercent = lastPercent;
					vmlCaky += createCakyVML(0,curPercent*3.6,objCakyColorArray[i],vAltText,4000,i);
				}else if( i== (oItemNodesList.length - 1)){//最后一个饼图
					curPercent = parseFloat(oItemNodesList[i].childNodes[2].text);
					vmlCaky += createCakyVML(lastPercent*3.6,100*3.6,objCakyColorArray[i],vAltText,4000,i);
				}else{//中间的饼图
					curPercent = parseFloat(oItemNodesList[i].childNodes[2].text) + lastPercent;
					vmlCaky += createCakyVML(lastPercent*3.6,curPercent*3.6,objCakyColorArray[i],vAltText,4000,i);
					lastPercent = curPercent;
				}
			}
			if(booleanHasLegend){//有图例
				if(i>=0 && i< 15){//装配图例
					vLengendLeft = 6400;
					topScale = i;
				}else if(i>=15 && i<30){
					vLengendLeft = 7900;
					topScale = i - 15;
				}else if(i>=30){
					vLengendLeft = 9400;
					topScale = i - 30;
				}
				vmlCaky +="<v:rect style='position:absolute;left:" + vLengendLeft + ";top:" + (900 + topScale*250) + ";width:40;height:20;z-index:1;' coordsize='5000,5000' fillcolor='" + objCakyColorArray[i] + "' stroked='false'><v:shadow on='True' offset='0pt,1pt' color='black'/></v:rect>";//图例
				vmlCaky +="<v:rect style='position:absolute;border:solid 0px green;left:" + (vLengendLeft + 150) + ";width:1500;top:" + (820 + topScale*250) + ";height:300' filled='false' stroked='false'><v:textbox style='position:absolute;border:solid 0px blue;left:0;top:0;font-size:9pt;'>" + oItemNodesList[i].childNodes[0].text + "(" + oItemNodesList[i].childNodes[1].text + ")</v:textbox></v:rect>";//图例说明文字
				if(i == oItemNodesList.length-1){//总计
					vmlCaky += "<v:line style='position:absolute;left:" + (vLengendLeft + 50) + ";top:" + (1000 + (topScale+1)*250) + ";z-index:1;' from='0,0' to='110,0' strokecolor='red' strokeweight='3pt'><v:stroke EndArrow='Classic'/></v:line>";
					vmlCaky += "<v:rect style='position:absolute;left:" + (vLengendLeft + 150) + ";width:1500;top:" + (880 + (topScale+1)*250) + ";height:300' filled='false' stroked='false'><v:textbox style='position:absolute;border:solid 0px blue;left:0;top:0;font-size:9pt;font-weight:bold;color:red;'>总计:" + patientTotal + "</v:textbox></v:rect>";
				}	
			}
		}
		if(vHasBorder){//有边框
			vmlEnd = "<v:group id='" + vTagID + "' style='position:absolute;border:solid " + vBorderWeight + "px " + vBorderColor + ";background:" + vBackColor + ";left:" + vLeft + "px;top:" + vTop + "px;width:" + vWidth + "px;height:" + vHeight + "px;' coordsize='2000,2000' xmlns:v='urn:schemas-microsoft-com:vml'>" + vmlTitle + vmlCaky + "<v:rect style='position:absolute;left:0;top:0;height:5500;' stroked='false'/></v:group>";
		}else{//无边框
			vmlEnd = "<v:group id='" + vTagID + "' style='position:absolute;background:" + vBackColor + ";left:" + vLeft + "px;top:" + vTop + "px;width:" + vWidth + "px;height:" + vHeight + "px;' coordsize='2000,2000' xmlns:v='urn:schemas-microsoft-com:vml'>" + vmlTitle + vmlCaky + "<v:rect style='position:absolute;left:0;top:0;height:5500;' stroked='false'/></v:group>";
		}
		objContainerDiv.insertAdjacentHTML("afterBegin",vmlEnd);//生成饼图
	}catch(e){}
}

function refresh(){//
	with(element.style){//element位置
		element.left = vLeft;
		element.top = vTop;
		element.width = vWidth;
		element.height = vHeight;
	}
	with(objContainerDiv.style){
		element.left = element.style.left;
		element.top = element.style.top;
		element.width = element.style.width;
		element.height = element.style.height;
	}
}

function putXMLSource(){
	var argXMLSource=element.xmlSource;
	var vSumXSLT;
	var vAddElementXSLT;
	vSumXSLT = '<?xml version="1.0" encoding="GBK"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="xml" version="1.0" encoding="GBK" indent="no" omit-xml-declaration="yes"/><xsl:template match="/"><xsl:value-of select=\'sum(//value)\'/></xsl:template></xsl:stylesheet>';
	vAddElementXSLT = '<?xml version="1.0" encoding="GBK"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="xml" version="1.0" encoding="GBK" indent="no" omit-xml-declaration="no"/><xsl:param name="valueCnt" select="sum(root/item/value)"/><xsl:template match="/"><root><xsl:apply-templates select="//root"/></root></xsl:template><xsl:template match="//root"><xsl:for-each select="item"><xsl:copy><xsl:element name="name"><xsl:value-of select="name"/></xsl:element><xsl:element name="value"><xsl:value-of select="value"/></xsl:element><xsl:element name="percentValue"><xsl:choose><xsl:when test="format-number(value div $valueCnt,\'###.##%\') = \'%\'"><xsl:value-of select="\'0%\'"/></xsl:when><xsl:otherwise><xsl:value-of select="format-number(value div $valueCnt,\'###.##%\')"/></xsl:otherwise></xsl:choose></xsl:element></xsl:copy></xsl:for-each></xsl:template></xsl:stylesheet>';
	objXMLDoc = transformXMLToNode(argXMLSource,vAddElementXSLT);	//增加扩展节点percentValue
	patientTotal = parseFloat(transformXML(objXMLDoc,vSumXSLT));//计算发病总人数
}

function putLeft(){
	vLeft = element.left;
}

function putTop(){
	vTop = element.top;
}

function putWidth(){
	vWidth = element.width;
}

function putHeight(){
	vHeight = element.height;
}

function putCaption(){
	vCaption = element.caption;
}

function putBackColor(){
	vBackColor = element.backColor;
}

function putHasBorder(){
	var argHasBorder=element.hasBorder;
	if(argHasBorder == "yes"){
		vHasBorder = true;
	}else if(argHasBorder == "no"){
		vHasBorder = false;
	}
}

function putBorderColor(){
	vBorderColor = element.borderColor;
}

function putBorderWeight(){
	var argBorderWeight=element.borderWeight;
	if(argBorderWeight.toString().match(/^[0-9]*$/) != null){
		vBorderWeight = parseInt(argBorderWeight);
	}
}

function putTagID(){
	vTagID = element.tagID;
}

function putHasLegend(){
	var argHasLegend=element.hasLegend;
	if(argHasLegend == "no"){
		booleanHasLegend = false;
	}else if(argHasLegend == "yes"){
		booleanHasLegend = true;
	}
}

////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
function transformXMLToNode(argXML,argXSLT){ //xml的xsl格式化,结果为document-fragment对象
    var vResult = transformXML(argXML,argXSLT);
    if(vResult) return inputXML(vResult);
    else return null;
}

function transformXML(argXML,argXSLT){ //xml的xsl格式化,结果为字符串
    var objXMLFragment,objXSLTFragment;
    var vResult = "";
    var objNode;

    objXMLFragment = inputXML(argXML);
    if(objXMLFragment){
    objXSLTFragment = inputXML(argXSLT);

    if(objXSLTFragment){
        vResult = objXMLFragment.transformNode(objXSLTFragment.childNodes(0));
        while(objXSLTFragment && objXSLTFragment.childNodes(0)){
        objNode = objXSLTFragment.removeChild(objXSLTFragment.childNodes(0));
        objNode = null;
        }
        objXSLTFragment = null;
        delete objXSLTFragment;

  	}
    while(objXMLFragment && objXMLFragment.childNodes(0)){
        objNode = objXMLFragment.removeChild(objXMLFragment.childNodes(0));
        objNode = null;
    }
    objXMLFragment = null;
    delete objXMLFragment;

    }
    vResult = vResult.replace(/<\?xml[0-9a-zA-Z\s\'\"=\-.\?]{0,}>/g,"");
    
    return vResult;
}

function inputXML(argSource){//返回xmlDOMFragement对象
	var objXMLDoc = getXMLDocumentInst();
  	var objXMLFragment = null;
  	var vTempSource = "";
	try{
		switch(typeof(argSource)){
			case "string":
				if(argSource.search(/\./) != -1 && argSource.search(/\</) == -1){ //xml file's location
				  	if(argSource.search(/\+/) != -1) argSource = eval(argSource);										  		  
				  	objXMLDoc.async = false;			  		  
					objXMLDoc.load(argSource);
					objXMLFragment = objXMLDoc.createDocumentFragment();
					objXMLFragment.appendChild(objXMLDoc.documentElement);
					break;
				}
				if(argSource.search(/\</) != -1){ //xml string
					argSource = argSource.replace(/xmlns:fo=\"\"/g,"");					      
					objXMLDoc.loadXML(argSource);
					objXMLFragment = objXMLDoc.createDocumentFragment();
					objXMLFragment.appendChild(objXMLDoc.documentElement);
					break;
				}
				try{
					argSource = argSource.replace(/xmlns:fo=\"\"/g,"");
					objXMLFragment = eval(argSource);//xml data island
				}catch(e){					
				}
				break;
  				
			case "object":
				if(argSource.xml) 
					vTempSource = argSource.xml;		
					vTempSource = vTempSource.replace(/xmlns:fo=\"\"/g,"");			      
					objXMLDoc.loadXML(vTempSource);//xml document object
					objXMLFragment=objXMLDoc.createDocumentFragment();
					objXMLFragment.appendChild(objXMLDoc.documentElement);
				break;
				case "undefined":
				objXMLFragment = objXMLDoc.createDocumentFragment();
				break;
			default:
				objXMLFragment = null;					  
		}

	}catch(err){
		objXMLFragment = null; 
	}
	return objXMLFragment;
}

function getXMLDocumentInst(){//获取一个DOM实例
	var objReturnXMLDom;
	objReturnXMLDom = new ActiveXObject("MSXML2.DOMDocument");
	objReturnXMLDom.async = false;
	objReturnXMLDom.setProperty("ServerHTTPRequest",true);
	objReturnXMLDom.setProperty("SelectionLanguage", "XPath");
	return objReturnXMLDom;
}

	__jhtcBindPropertyChange(element,"xmlSource",putXMLSource);
	__jhtcBindPropertyChange(element,"top",putTop);
	__jhtcBindPropertyChange(element,"left",putLeft);
	__jhtcBindPropertyChange(element,"width",putWidth);
	__jhtcBindPropertyChange(element,"height",putHeight);
	__jhtcBindPropertyChange(element,"caption",putCaption);
	__jhtcBindPropertyChange(element,"tagID",putTagID);
	__jhtcBindPropertyChange(element,"hasLegend",putHasLegend);
	__jhtcBindPropertyChange(element,"backColor",putBackColor);
	__jhtcBindPropertyChange(element,"hasBorder",putHasBorder);
	__jhtcBindPropertyChange(element,"borderColor",putBorderColor);
	__jhtcBindPropertyChange(element,"borderWeight",putBorderWeight);
	__jhtcBindPropertyChange(element,"init",initialize);

  	jhtc_attr_init(element,"xmlSource","");
  	jhtc_attr_init(element,"top","");
  	jhtc_attr_init(element,"left","");
  	jhtc_attr_init(element,"width","");
  	jhtc_attr_init(element,"height","");
  	jhtc_attr_init(element,"caption","");
  	jhtc_attr_init(element,"tagID","");
  	jhtc_attr_init(element,"hasLegend","");
  	jhtc_attr_init(element,"backColor","");
  	jhtc_attr_init(element,"hasBorder","");
  	jhtc_attr_init(element,"borderColor","");
  	jhtc_attr_init(element,"borderWeight","");
  	jhtc_attr_init(element,"init","");

  	
	element.refreshData=refreshData;
	element.init=initialize;
	element.jhtcInit=function(){
		element.init();
	};
	return null;
};
jhtc_tag_map["cs"]=jhtc_cakyStat;