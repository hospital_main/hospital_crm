<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:variable name="colNum" select="count(//top/tr[1]/td)"/>
  <xsl:template match="/root">
    <xsl:for-each select="/root/pagegroup/page">
      <hr class='noprint' width='{/root/@pageWidth}'/>
      <div style='border:0 groove gray;width:{/root/@pageWidth};height:{/root/@pageHeight};' margin='{/root/@topMargin} {/root/@rightMargin} {/root/@bottomMargin} {/root/@leftMargin}'>
      <table width='100%' height='100%' border='0'>
        <tr height='{/root/@topMargin}'>
          <td rowspan='3' width='{/root/@leftMargin}'/>
          <td/>
          <td rowspan='3' width='{/root/@rightMargin}'/>
        </tr>
        <tr>
          <td valign='top'>
            <table border='0' width='{/root/@pageWidth - /root/@leftMargin - /root/@rightMargin}' height='{/root/@pageHeight - /root/@topMargin - /root/@bottomMargin}'>
              <tr>
                <td>
                  <xsl:call-template name="top">
                    <xsl:with-param name="page-num" select="concat(position(),'/',last())"/>
                  </xsl:call-template>
                </td>
              </tr>
              <tr>
                <td>
                  <xsl:call-template name="page">
                    <xsl:with-param name="rowB" select="@rowB"/>
                    <xsl:with-param name="rowE" select="@rowE"/>
                    <xsl:with-param name="colB" select="@colB"/>
                    <xsl:with-param name="colE" select="@colE"/>
                  </xsl:call-template>
                </td>
              </tr>
              <tr height='100%'>
                <td valign='top'>
                  <xsl:call-template name="bottom"/>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr height='{/root/@bottomMargin}'>
          <td/>
        </tr>
      </table>
      </div>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="top">
    <xsl:param name="page-num"/>
    <table width='100%'>
      <xsl:for-each select="/root/top/title | /root/top/maintitle" >
        <tr height='{/root/@titleHeight}'>
          <td colspan="{$colNum}" class='prnTitle' style='{@style}'>
            <xsl:value-of select="."/>
          </td>
        </tr>
      </xsl:for-each>
      <xsl:for-each select="/root/top/subtitle" >
        <tr height='{/root/@subTitleHeight}'>
          <td colspan="{$colNum}" class='prnSubTitle' style='{@style}'>
            <xsl:value-of select="."/>
          </td>
        </tr>
      </xsl:for-each>
      <xsl:for-each select="/root/top/tr" >
        <xsl:call-template name="message">
          <xsl:with-param name="position" select="position()"/>
          <xsl:with-param name="tdNum" select="count(td)"/>
          <xsl:with-param name="page-num" select="$page-num"/>
        </xsl:call-template>
      </xsl:for-each>
    </table>
  </xsl:template>

  <xsl:template name="bottom">
    <br style='font-size:3px'/>
    <table width='100%'>
      <xsl:for-each select="//root/bottom/tr">
        <xsl:call-template name="message">
          <xsl:with-param name="position" select="position()"/>
          <xsl:with-param name="tdNum" select="count(td)"/>
        </xsl:call-template>
      </xsl:for-each>
    </table>
  </xsl:template>

  <xsl:template name="message">
    <xsl:param name="position"/>
    <xsl:param name="tdNum"/>
    <xsl:param name="page-num"/>
    <tr style='{@style}'>
      <xsl:for-each select="td">
        <td class='prnAddInfo' nowrap="nowrap">  
          <xsl:if test="string-length(@width) = 0">
            <xsl:attribute name='width'><xsl:value-of select='100 div $tdNum'/>%</xsl:attribute>            
          </xsl:if>
          <xsl:if test="@width != ''">
            <xsl:attribute name='width'><xsl:value-of select='@width'/></xsl:attribute>            
          </xsl:if>
          <xsl:choose>
            <xsl:when test="position()=1">
              <xsl:attribute name='style'>text-align:left;<xsl:value-of select='@style'/></xsl:attribute>
            </xsl:when>
            <xsl:when test="position()=last()">
              <xsl:attribute name='style'>text-align:right;<xsl:value-of select='@style'/></xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name='style'>text-align:left;<xsl:value-of select='@style'/></xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="contains(., '[ҳ��]/[��ҳ��]')">
              <xsl:value-of select="concat(substring-before(., '[ҳ��]/[��ҳ��]'), $page-num)"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="."/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </xsl:for-each>
    </tr>
  </xsl:template>

  <xsl:template name="page">
    <xsl:param name="rowB"/>
    <xsl:param name="rowE"/>
    <xsl:param name="colB"/>
    <xsl:param name="colE"/>
    <table class="printTable" bgColor="white" borderColor="black" border="1" style='font-size:{13 * /root/@fontScale}'>
      <colgroup>
        <xsl:for-each select="//colgroup/col">
          <xsl:if test="(position() &lt;= /root/@fixCol) or (position() &gt;= $colB and position() &lt;= $colE)">
            <col width='{@width}'/>
          </xsl:if>
        </xsl:for-each>
      </colgroup>
      <thead>
        <xsl:for-each select="//thead/tr">
          <tr class="mainHead" nowrap="true" height='{@height}' style='font-size:{15 * /root/@fontScale}'>
            <xsl:for-each select="th">
              <xsl:if test="(position() &lt;= /root/@fixCol) or (position() &gt;= $colB and position() &lt;= $colE)">
                <th class="{@class}" rowspan="{@rowspan}" colspan="{@colspan}" style='{@style}'><xsl:value-of select="."/></th>
              </xsl:if>
            </xsl:for-each>
          </tr>
        </xsl:for-each>
      </thead>
      <tbody>
        <xsl:for-each select="//tbody/tr">
          <xsl:if test="position() &gt;= $rowB and position() &lt;= $rowE">
            <tr height='{@height}'>
              <xsl:for-each select="td">
                <xsl:if test="(position() &lt;= /root/@fixCol) or (position() &gt;= $colB and position() &lt;= $colE)">
                  <td rowspan="{@rowspan}" colspan="{@colspan}" style='{@style}' class='{@class}'>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:if>
              </xsl:for-each>
            </tr>
          </xsl:if>
        </xsl:for-each>
      </tbody>
    </table>
  </xsl:template>
</xsl:stylesheet>