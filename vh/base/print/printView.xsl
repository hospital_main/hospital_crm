<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- 此XSL实现了用递归替代循环 -->
  <xsl:decimal-format NaN=''/>
  <xsl:variable name="tdNum" select="/user_report/table/tr[position() = 1]/td[position() = last()]/@idx - 1 + /user_report/table/tr[position() = 1]/td[position() = last()]/@colSpan"/>
  
  <xsl:template match="/">
  <root>
    <top>
      <tr>
        <td>编制单位：<xsl:value-of select='/user_report/@comName'/></td>
        <td>页号：[页码]/[总页数]</td>
      </tr>
    </top>
    <colgroup>
      <col style = 'width:250'/>
      <xsl:call-template name="colgroup">
        <xsl:with-param name="index" select="1"/>
      </xsl:call-template>
    </colgroup>
    <thead>
      <xsl:for-each select="/user_report/table/tr">
     	  <xsl:if test='@isTitleRow = "y"'>
          <tr>
            <xsl:call-template name="td">
              <xsl:with-param name="trIdx" select="position()"/>
              <xsl:with-param name="tdIdx" select="0"/>
            </xsl:call-template>
          </tr>
        </xsl:if>
     	</xsl:for-each>
    </thead>
    <tbody>
     	<xsl:for-each select="/user_report/table/tr">
     	  <xsl:if test='not (@isTitleRow = "y")'>
          <tr>
            <xsl:for-each select="td">
              <td colspan='{@colSpan}' rowspan='{@rowSpan}' style='{@style}'>
                <xsl:value-of select='.'/>
              </td>
            </xsl:for-each>
          </tr>
        </xsl:if>
     	</xsl:for-each>
    </tbody>
  </root>
  </xsl:template>
  
  
  <xsl:template name="colgroup">
    <xsl:param name="index"/>
    <xsl:if test='$index &lt; $tdNum'>
      <col style = 'width:105'/>
      <xsl:call-template name="colgroup">
        <xsl:with-param name="index" select="$index + 1"/>
      </xsl:call-template>
    </xsl:if>   
  </xsl:template>
  
  <xsl:template name="td">
    <xsl:param name="trIdx"/>
    <xsl:param name="tdIdx"/>
    <xsl:if test='$tdIdx &lt; $tdNum'>
      <xsl:choose>
        <xsl:when test="count(/user_report/table/tr[@idx = 1+$trIdx]/td[@idx = 2+$tdIdx]) &lt; 1">
          <th style='display:none'/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:for-each select="/user_report/table/tr[@idx = 1+$trIdx]/td[@idx = 2+$tdIdx]">
            <xsl:if test="position()=1">
              <th colspan='{@colSpan}' rowspan='{@rowSpan}'>
                <xsl:value-of select='.'/>
              </th>
            </xsl:if>
          </xsl:for-each>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:call-template name="td">
        <xsl:with-param name="trIdx" select="$trIdx"/>
        <xsl:with-param name="tdIdx" select="$tdIdx+1"/>
      </xsl:call-template>
    </xsl:if>   
  </xsl:template>
</xsl:stylesheet>



