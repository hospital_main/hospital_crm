<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ page import = "com.viewhigh.cbcs.base.util.print.PrintData, com.viewhigh.cbcs.base.exception.SysException"%>
<html>
<head>
<script src="javascript/common.js"></script>
<%
PrintData pd;
if(request.getParameter("flag")!=null && request.getParameter("flag").trim().equals("1"))
  pd = (PrintData)request.getSession().getAttribute("printData1");
else{
  pd = (PrintData)request.getSession().getAttribute("printData");
  }
  if(pd==null)
    throw new SysException("打印数据非法");
%>
</head>
<body onload="init()">
<textarea id="jspPrintXmlData"><%=pd.tablePrint()%></textarea>
<script >
window.prefix = document.URL.replace(/\?[^\?]*$/g,"").replace(/[\w.]+$/g, "");
function init(){
	window.moveTo(5000,5000);
	printStringDataToCell( jspPrintXmlData.value, null, false, false );
	window.close();
}
</script>
</body>
</html>
