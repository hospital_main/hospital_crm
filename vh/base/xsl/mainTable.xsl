<?xml version='1.0' encoding="GBK"?>
<!--xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl"-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/> 
  
  
  <xsl:template name="sumTD">
    <xsl:param name="indexTD"/>
    <xsl:value-of select="format-number(sum(//tbody/tr/td[$indexTD]), '#,##0.00')"/>
  </xsl:template>
  
  <xsl:template match="/">
    <table width='100%' style='background-color: #F6F6F6;'  cellpadding='0' cellspacing='0' >
      <tr>
        <td isBtn='true'></td>
        <td isTurn='true'></td>
	    </tr>
  	  <tr>
  	    <td colspan='100' style="background-image:url('/images2/background/gray_fill.png');background-repeat:no-repeat;background-position:bottom right;">
        <div id='_base' style='overflow:auto;padding:0;SCROLLBAR-FACE-COLOR: #F2F2F2; SCROLLBAR-HIGHLIGHT-COLOR: #999999;SCROLLBAR-SHADOW-COLOR:#999999; SCROLLBAR-3DLIGHT-COLOR:#ffffff; SCROLLBAR-ARROW-COLOR:#999999; SCROLLBAR-TRACK-COLOR:#ffffff;SCROLLBAR-DARKSHADOW-COLOR: #ffffff;'>
          <table id="_mainDataTable"  class='mainTable'>
            <thead/>
            <tbody>
      		    <xsl:for-each select="/root/tbody/tr">
                <tr>
                  <td align='center'  style='display:none'>
                    <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
                      <xsl:attribute name="value" >
                        <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              			  </xsl:attribute>
            			  </input>
                  </td>
                  <xsl:for-each select="td">
                    <td>
                      <xsl:choose>
                        <xsl:when test="position()=1">
                          <a tabindex='-1'><xsl:value-of select="."/></a>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="."/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
          			  </xsl:for-each>
          			</tr>
           		</xsl:for-each>
         		</tbody>
      		</table>
         	</div>
      	</td>
      </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>


