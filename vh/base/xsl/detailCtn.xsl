<?xml version='1.0' encoding="GBK"?>
<!--xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl"-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
    <table width='100%' style='border: inset 2px;background-color: #F6F6F6;'>
      <tr>
        <td isBtn='true'></td>
        <td recodeMsg='true'></td>
	    </tr>
  	  <tr>
  	    <td colspan='100'>
        <div id='_base' style='overflow:auto;padding:0;SCROLLBAR-FACE-COLOR: #F2F2F2; SCROLLBAR-HIGHLIGHT-COLOR: #999999;SCROLLBAR-SHADOW-COLOR:#999999; SCROLLBAR-3DLIGHT-COLOR:#ffffff; SCROLLBAR-ARROW-COLOR:#999999; SCROLLBAR-TRACK-COLOR:#ffffff;SCROLLBAR-DARKSHADOW-COLOR: #ffffff;'>
          <table class='mainTable' border='1' borderColor='#292954' cellpadding='2' cellspacing='1' style='border-style:none;'>
            <thead/>
            <tbody/>
      		</table>
         	</div>
      	</td>
      </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>


