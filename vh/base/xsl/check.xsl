<?xml version='1.0' encoding="GBK"?>
<!--xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl"-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <table width='100%' style='border: inset 2px;background-color: #F6F6F6;'>
      <tr>
        <td width='49%' class='checkTable'><b>银行对帐单</b></td>
        <td width='2%'/>
        <td width='49%' class='checkTable'><b>银行帐</b></td>
	    </tr>
      <tr>
        <td width='49%' valign='top'>
        	<div id='_check1' style='overflow:auto;padding:0;SCROLLBAR-FACE-COLOR: #F2F2F2; SCROLLBAR-HIGHLIGHT-COLOR: #999999;SCROLLBAR-SHADOW-COLOR:#999999; SCROLLBAR-3DLIGHT-COLOR:#ffffff; SCROLLBAR-ARROW-COLOR:#999999; SCROLLBAR-TRACK-COLOR:#ffffff;SCROLLBAR-DARKSHADOW-COLOR: #ffffff;'>
          	<table1/>
          </div>
        </td>
        <td width='2%'/>
        <td width='49%' valign='top'>
        	<div id='_check2' style='overflow:auto;padding:0;SCROLLBAR-FACE-COLOR: #F2F2F2; SCROLLBAR-HIGHLIGHT-COLOR: #999999;SCROLLBAR-SHADOW-COLOR:#999999; SCROLLBAR-3DLIGHT-COLOR:#ffffff; SCROLLBAR-ARROW-COLOR:#999999; SCROLLBAR-TRACK-COLOR:#ffffff;SCROLLBAR-DARKSHADOW-COLOR: #ffffff;'>
          	<table2/>
          </div>
        </td>
	    </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>


