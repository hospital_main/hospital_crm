<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/root">
  	<xsl:variable name="pos" select="/root/@pos"/>
  	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" >
  	<tr height="1">
  		<td>
			  	<table width="100%" border="0" cellpadding="0" cellspacing="0" >
			              <tr id="_3rd_menu_">
			              
			                        <xsl:for-each select="menu_node">
			                          <xsl:if test="$pos=position()">
			                          <td><img src="base/themes/blue/images/3rdMenuChoseHeadBg.gif"/></td>
			                          </xsl:if>
			                          <xsl:if test="$pos!=position()">
			                          <td><img src="base/themes/blue/images/3rdMenuNoChoseHeadBg.gif"/></td>
			                          </xsl:if>
			                          <td>
			                            <xsl:attribute name="class" >
			                              <xsl:if test="$pos=position()">3rdmenuchosebg</xsl:if>
			                              <xsl:if test="$pos!=position()">3rdmenunochosebg</xsl:if>
			                      			</xsl:attribute>
			                      			<xsl:if test="$pos!=position()"><xsl:attribute name="onclick" >leftFrame.bottomf.render3menu(<xsl:value-of select="position()"/>);</xsl:attribute></xsl:if>
			                          <div nowrap="true"><xsl:value-of select="name"/></div></td>
			                          <xsl:if test="$pos=position()">
			                            <td><img src="base/themes/blue/images/3rdMenuChoseEndBg.gif"/></td>
			                          </xsl:if>
			                          <xsl:if test="$pos!=position()">
			                            <td><img src="base/themes/blue/images/3rdMenuNoChoseEndBg.gif"/></td>
			                          </xsl:if>
			                        </xsl:for-each>
			                    
			                <td class="3rdmenuendbg">
			                <div nowrap="true"></div></td>
			             </tr>
			            </table>
          </td>
        </tr>
        <tr>
        	<td>
        	        <iframe id="_iframe" name="_business" width="100%" height="100%" frameborder="0">
                    <xsl:attribute name="src" >
                    	<xsl:value-of select="/root/menu_node[position()=$pos]/src"/>
                    </xsl:attribute>
                  </iframe>
               
  				</td>
  			</tr>
  		</table>
	</xsl:template>
	
</xsl:stylesheet>  