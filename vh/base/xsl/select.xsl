<?xml version='1.0' encoding="GBK"?>
<!--xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl"-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <TABLE class="select_table" style="WIDTH: 100%;" cellSpacing="0" cellPadding="0" border="0">
      <TBODY>
		    <xsl:variable name="is-code" select="/root/@code"/>
		    <xsl:variable name="key" select="/root/@key"/>
		    <xsl:variable name="qtype" select="/root/@qtype"/>
		    <xsl:for-each select="/root/para">
	        <xsl:if test="$qtype='0'">
            <xsl:if test="$key='' or starts-with(@code,$key)">
            <TR style="FONT-SIZE: 13px">
              <TD style="PADDING-LEFT: 5px; PADDING-TOP: 1px" nowrap="true">
                <xsl:attribute name="value">
                  <xsl:value-of select="@code"/>
                </xsl:attribute>
                <xsl:if test="$is-code='true'"><xsl:value-of select="@code"/>:</xsl:if><xsl:value-of select="@value"/>
              </TD>
      			</TR>
            </xsl:if>
	        </xsl:if>
	        <xsl:if test="$qtype='1'">
            <xsl:if test="$key='' or starts-with(@spell,$key)">
            <TR style="FONT-SIZE: 13px">
              <TD style="PADDING-LEFT: 5px; PADDING-TOP: 1px" nowrap="true">
                <xsl:attribute name="value">
                  <xsl:value-of select="@code"/>
                </xsl:attribute>
                <xsl:if test="$is-code='true'"><xsl:value-of select="@code"/>:</xsl:if><xsl:value-of select="@value"/>
              </TD>
      			</TR>
            </xsl:if>
 	        </xsl:if>
	        <xsl:if test="$qtype='2'">
            <xsl:if test="$key='' or starts-with(@define,$key)">
            <TR style="FONT-SIZE: 13px">
              <TD style="PADDING-LEFT: 5px; PADDING-TOP: 1px" nowrap="true">
                <xsl:attribute name="value">
                  <xsl:value-of select="@code"/>
                </xsl:attribute>
                <xsl:if test="$is-code='true'"><xsl:value-of select="@code"/>:</xsl:if><xsl:value-of select="@value"/>
              </TD>
      			</TR>
            </xsl:if>
	        </xsl:if>  
     		</xsl:for-each>
   		</TBODY>
		</TABLE>
  </xsl:template>
</xsl:stylesheet>


