<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/root">
        <xsl:for-each select="table">
        <table border='0'>
          <tr><td>
          <xsl:call-template name="top">
            <xsl:with-param name="page-num" select="concat(position(),'/',last())"/>
          </xsl:call-template>
          </td></tr>
          <tr><td>
          <xsl:apply-templates select="."/>
          </td></tr>
          <tr><td>
          <xsl:call-template name="bottom"/>
          </td></tr>
        </table>
          <xsl:if test="position()!=last()">
            <div style='PAGE-BREAK-AFTER: always'></div>
          </xsl:if>
        </xsl:for-each>
  </xsl:template>

  <xsl:template name="top">
    <xsl:param name="page-num"/>
    <table width='100%'>
      <xsl:for-each select="//root/top/maintitle | //root/top/subtitle | //root/top/tr">
        <xsl:choose>
          <xsl:when test="name(.)='maintitle'">
            <tr>
              <td colspan="3" class="tablePrintTitle1"><xsl:value-of select="."/></td>
            </tr>
          </xsl:when>
          <xsl:when test="name(.)='subtitle'">
            <tr>
              <td colspan="3" class="tablePrintTitle2"><xsl:value-of select="."/></td>
            </tr>
          </xsl:when>
          <xsl:when test="name(.)='tr'">
            <xsl:call-template name="message">
              <xsl:with-param name="position" select="position()"/>
              <xsl:with-param name="page-num" select="$page-num"/>
            </xsl:call-template>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
    </table>
  </xsl:template>

  <xsl:template name="bottom">
    <br style='font-size:3px'/>
    <table width='100%'>
      <xsl:for-each select="//root/bottom/tr">
        <xsl:call-template name="message">
          <xsl:with-param name="position" select="position()"/>
        </xsl:call-template>
      </xsl:for-each>
    </table>
  </xsl:template>

  <xsl:template name="message">
    <xsl:param name="position"/>
    <xsl:param name="page-num"/>
    <tr>
      <xsl:for-each select="td">
        <xsl:choose>
          <xsl:when test="position()=1">
            <td class="tablePrintTitle3" style="text-align:left" nowrap="nowrap"><xsl:value-of select="."/></td>
          </xsl:when>
          <xsl:when test="position()=2">
            <td class="tablePrintTitle3" style="text-align:center" nowrap="nowrap"><xsl:value-of select="."/></td>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="$position=3 and $page-num!=''">
                <td class="tablePrintTitle3" style="text-align:right" nowrap="nowrap"><xsl:value-of select="concat('ҳ��:',$page-num)"/>��������</td>
              </xsl:when>
              <xsl:otherwise>
                <td class="tablePrintTitle3" style="text-align:right" nowrap="nowrap"><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </tr>
  </xsl:template>

  <xsl:template match="*">
    <table class="printTable" bgColor="white" borderColor="black" border="1">
      <colgroup>
        <xsl:for-each select="colgroup/col">
          <col style = "{@style}"/>
        </xsl:for-each>
      </colgroup>
      <thead>
        <xsl:for-each select="thead/tr">
          <tr class="mainHead" nowrap="true" height='29'>
            <xsl:for-each select="th">
              <th class="{@class}" rowspan="{@rowspan}" colspan="{@colspan}" ><xsl:value-of select="."/></th>
            </xsl:for-each>
          </tr>
        </xsl:for-each>
      </thead>
      <tbody>
        <xsl:for-each select="tbody/tr">
          <tr height='25'>
            <xsl:for-each select="td">
              <td class="{@class}" rowspan="{@rowspan}" colspan="{@colspan}"><xsl:value-of select="."/></td>
            </xsl:for-each>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>
  </xsl:template>
</xsl:stylesheet>