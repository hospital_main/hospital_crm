//调用图形窗口
function openGraphDialog(gtype,gTitle,xmlData,xAxes,yAxes){ 
	__dept_sign__ = xmlData;
	__gTitle__ = gTitle;
	if(gtype=="2"){
		__yAxes__='元';
		if( typeof(yAxes) !="undefined" ){
			__yAxes__=yAxes;
		}
		__xAxes__='';
		if( typeof(xAxes) !="undefined" ){
			__xAxes__=xAxes;
		}		
	}
	if(gtype=="1"){
		__yAxes__='元';
		if( typeof(yAxes) !="undefined" ){
			__yAxes__=yAxes;
		}
		__xAxes__='';
		if( typeof(xAxes) !="undefined" ){
			__xAxes__=xAxes;
		}
		
		openDialog(window.prefix+"base/print/LineGraph.html", 'dialogWidth:800px;dialogHeight:423px');
	}else if(gtype=="2"){
		openDialog(window.prefix+"base/print/ShapeGraph.html", 'dialogWidth:800px;dialogHeight:423px');
	}else{
		openDialog(window.prefix+"base/print/PieGraph.html", 'dialogWidth:800px;dialogHeight:423px');
	}
	//return __getDeptsFromListTemp__;
}

//创建控件
function CreateObjectControl(objID, ControlTYPE, ControlWIDTH, ControlHEIGHT, editDiv, isvisible)
{
	var sClsID = "";
	var sCodeBase = "";
	var sURL = window.prefix;
 	sURL = sURL.substr(0,sURL.length - 3);
  
 	if (ControlTYPE == "asst"){
		sClsID = "CLSID:EAB16609-F6BF-4BEA-B6FD-11C64927196F";
		sCodeBase = sURL + "base/activex/asstTable.CAB#version=2,1,0,111";
	} 
  	else if (ControlTYPE == "VHTable")
	{
		sClsID = "CLSID:62C27227-C64C-44ED-B184-E9B54D582A59";
		sCodeBase = sURL + "base/activex/vhControlTableB.CAB#version=3,0,0,109";
	}
	else if (ControlTYPE == "Vouch")
	{
		sClsID = "CLSID:E10E9E28-110A-4146-ABF8-A61DC89FE7CC";
		sCodeBase = sURL + "base/activex/AccountCtl.CAB#version=2,1,0,135";
	}
	else if (ControlTYPE == "vhchart")
	{
		sClsID = "CLSID:88404FA1-2B35-4C75-BBD6-DCAFEED65C52";	
		sCodeBase = sURL +"base/activex/VHChart.CAB#version=1,0,0,39";
	}
	else if (ControlTYPE == "webcell")
	{
		sClsID = "CLSID:3F166327-8030-4881-8BD2-EA25350E574A";
		sCodeBase = sURL + "base/activex/webCell.CAB#version=5.2.6.1210";
	}
	else if (ControlTYPE == "vhBC")
	{
		sClsID="CLSID:5D31608A-9038-4E9F-ABBD-C2348B852460";
		sCodeBase= sURL + "base/activex/vhBarCode.CAB#version=1,0,0,86";
	}
	else if (ControlTYPE == "treeview")
	{
		sClsID = "CLSID:6442D769-4366-424B-9A8B-626177373CB3";
		sCodeBase = sURL + "base/activex/vhTreeView.CAB#version=1,1,0,14";
	}
	else if (ControlTYPE == "Formula")
	{
		sClsID = "CLSID:801CC3CC-DA03-4740-845F-27D8458F531F";
		sCodeBase = sURL + "base/activex/vhFormulaCtrl.CAB#version=1,0,0,52";
	}	

	var strControl = "";
	strControl += '<OBJECT id="' + objID + '" ';
	if(isvisible!="undefined"){
		if(isvisible=="none"){
			strControl=strControl + ' style="display:none" '		
			}	
	}
	strControl += 'CLASSID="' + sClsID + '" ';
	strControl += 'CODEBASE="' + sCodeBase + '" ';
	strControl += 'WIDTH="' + ControlWIDTH + '" ';
	strControl += 'HEIGHT="' + ControlHEIGHT + '">';
	strControl += '<param name="WebURL" value="' + sURL + '">'
	strControl += '<param name="TableID" value="' + objID + '">';
	if (ControlTYPE == "VHTable") strControl += '<param name="VHActionExt" value=".hbviewhigh">'; 
	strControl += '</OBJECT>';

	document.write(strControl);
	if (ControlTYPE == "webcell")
	{
		eval(objID + ".Login('望海康信' , '', '13100104488', '3140-1444-7731-6004');");
		eval(objID + ".LocalizeControl(0x804);");
	}

	if(editDiv==null)
		return ;
	adjPositionObjectControl(objID,editDiv);
	window.attachEvent("onresize", function(){adjPositionObjectControl(objID,editDiv)});
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},50);
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},100);
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},200);
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},400);
}

function CreateTableControl(objID, ControlTYPE, ControlWIDTH, ControlHEIGHT, editDiv)
{
	var sClsID = "";
	var sCodeBase = "";
	var sURL = window.prefix;
 	sURL = sURL.substr(0,sURL.length - 3);
	if (ControlTYPE == "VHTable")
	{
		sClsID = "CLSID:62C27227-C64C-44ED-B184-E9B54D582A59";
		sCodeBase = sURL + "base/activex/vhControlTableB.CAB#version=3,0,0,109";
	}

	if(editDiv!=null)
	{
		ControlWIDTH = 1;
		ControlHEIGHT = 1;
	}

	var strControl = "";
	strControl += '<OBJECT id="' + objID + '" ';
	strControl += 'CLASSID="' + sClsID + '" ';
	strControl += 'CODEBASE="' + sCodeBase + '" ';
	strControl += 'WIDTH="' + ControlWIDTH + '" ';
	strControl += 'HEIGHT="' + ControlHEIGHT + '">';
	strControl += '<param name="WebURL" value="' + sURL + '">';
	strControl += '<param name="TableID" value="' + objID + '">';
	strControl += '<param name="TableShow" value=false>';
	if (ControlTYPE == "VHTable") strControl += '<param name="VHActionExt" value=".hbviewhigh">'; 
	strControl += '</OBJECT>';
	document.write(strControl);

	if(editDiv==null)
		return;
	adjPositionObjectControl(objID,editDiv);
	window.attachEvent("onresize", function(){adjPositionObjectControl(objID,editDiv)});
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},50);
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},100);
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},200);
	window.setTimeout(function(){adjPositionObjectControl(objID,editDiv)},400);
}

function adjPositionObjectControl(axObjId,divObj){
	if (divObj == null || divObj == "") return;
	var axObj=eval(axObjId);
	var rightFix=divObj.getAttribute("rightFix");
	if(rightFix==null)
		rightFix="0";
	var bottomFix=divObj.getAttribute("bottomFix");
	if(bottomFix==null)
		bottomFix="0";
	parentObj =divObj;
    baseDivTop = baseDivLeft = 0
    try{
	    while(parentObj.tagName != "BODY") {
		    baseDivTop += parentObj.offsetTop;
		    baseDivLeft += parentObj.offsetLeft;
		    parentObj = parentObj.offsetParent;
	    }
    }catch(E){return;}
    
    if(rightFix.substr(rightFix.length-1)=="%"){
   		divObj.style.pixelWidth = parentObj.clientWidth -  2 - parentObj.clientWidth*parseFloat(rightFix.replace(/%/g,""))/100;
    }else
    	{
    	divObj.style.pixelWidth = parentObj.clientWidth -  2 - parseFloat(rightFix);
    }
    
    if(bottomFix.substr(bottomFix.length-1)=="%")
    	divObj.style.pixelHeight = parentObj.clientHeight - baseDivTop - 4 - parentObj.clientHeight*parseFloat(bottomFix.replace(/%/g,""))/100
   else
       	divObj.style.pixelHeight = parentObj.clientHeight - baseDivTop - 4 - parseFloat(bottomFix)

    if(typeof(axObj)!="undefined"&&divObj.style.pixelWidth>10&&divObj.style.pixelHeight>0){
   		axObj.width=divObj.style.pixelWidth;
   		axObj.height=divObj.style.pixelHeight;
    }else
    	window.setTimeout(function(){adjPositionObjectControl(axObjId,divObj)},50);
}

var i9=0;