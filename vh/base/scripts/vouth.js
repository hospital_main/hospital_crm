
var activeTD=null;//保存当前的TD
var summaryTD=null//保存当前的摘要TD
var total_left=0;//借.可能动态传来
var total_right=0;//贷.可能动态传来

function roundFloat(d){//保留两位小数   
	n=2;
	with(Math){
  	if(d>=0.00)
  		return floor(d*pow(10,n)+0.5)/pow(10,n); 
  	else
  		return ceil(d*pow(10,n)-0.5)/pow(10,n); 
  }
}

function Arabia_to_Chinese(Num){
  for(i=Num.length-1;i>=0;i--)
  {
    Num = Num.replace(",","")//替换tomoney()中的“,”
    Num = Num.replace(" ","")//替换tomoney()中的空格
  }
  Num = Num.replace("￥","")//替换掉可能出现的￥字符
  if(isNaN(Num)) { //验证输入的字符是否为数字
    alert("请检查小写金额是否正确");
    return;
  }
  //---字符处理完毕，开始转换，转换采用前后两部分分别转换---//
  part = String(Num).split(".");
  newchar = ""; 
  //小数点前进行转化
  for(i=part[0].length-1;i>=0;i--){
    if(part[0].length > 10){ alert("位数过大，无法计算");return "";}//若数量超过拾亿单位，提示
    tmpnewchar = ""
    perchar = part[0].charAt(i);
    switch(perchar){
    case "0": tmpnewchar="零" + tmpnewchar ;break;
    case "1": tmpnewchar="壹" + tmpnewchar ;break;
    case "2": tmpnewchar="贰" + tmpnewchar ;break;
    case "3": tmpnewchar="叁" + tmpnewchar ;break;
    case "4": tmpnewchar="肆" + tmpnewchar ;break;
    case "5": tmpnewchar="伍" + tmpnewchar ;break;
    case "6": tmpnewchar="陆" + tmpnewchar ;break;
    case "7": tmpnewchar="柒" + tmpnewchar ;break;
    case "8": tmpnewchar="捌" + tmpnewchar ;break;
    case "9": tmpnewchar="玖" + tmpnewchar ;break;
  }
  switch(part[0].length-i-1){
    case 0: tmpnewchar = tmpnewchar +"元" ;break;
    case 1: if(perchar!=0)tmpnewchar= tmpnewchar +"拾" ;break;
    case 2: if(perchar!=0)tmpnewchar= tmpnewchar +"佰" ;break;
    case 3: if(perchar!=0)tmpnewchar= tmpnewchar +"仟" ;break;
    case 4: tmpnewchar= tmpnewchar +"万" ;break;
    case 5: if(perchar!=0)tmpnewchar= tmpnewchar +"拾" ;break;
    case 6: if(perchar!=0)tmpnewchar= tmpnewchar +"佰" ;break;
    case 7: if(perchar!=0)tmpnewchar= tmpnewchar +"仟" ;break;
    case 8: tmpnewchar= tmpnewchar +"亿" ;break;
    case 9: tmpnewchar= tmpnewchar +"拾" ;break;
  }
  newchar = tmpnewchar + newchar;
  }
  //小数点之后进行转化
  if(Num.indexOf(".")!=-1){
    if(part[1].length > 2) {
      alert("小数点之后只能保留两位,系统将自动截段");
      part[1] = part[1].substr(0,2)
    }
    for(i=0;i<part[1].length;i++){
      tmpnewchar = ""
      perchar = part[1].charAt(i)
      switch(perchar){
        case "0": tmpnewchar="零" + tmpnewchar ;break;
        case "1": tmpnewchar="壹" + tmpnewchar ;break;
        case "2": tmpnewchar="贰" + tmpnewchar ;break;
        case "3": tmpnewchar="叁" + tmpnewchar ;break;
        case "4": tmpnewchar="肆" + tmpnewchar ;break;
        case "5": tmpnewchar="伍" + tmpnewchar ;break;
        case "6": tmpnewchar="陆" + tmpnewchar ;break;
        case "7": tmpnewchar="柒" + tmpnewchar ;break;
        case "8": tmpnewchar="捌" + tmpnewchar ;break;
        case "9": tmpnewchar="玖" + tmpnewchar ;break;
      }
      if(i==0)tmpnewchar =tmpnewchar + "角";
      if(i==1)tmpnewchar = tmpnewchar + "分";
      newchar = newchar + tmpnewchar;
    }
  }
  //替换所有无用汉字
  while(newchar.search("零零") != -1)
  newchar = newchar.replace("零零", "零");
  newchar = newchar.replace("零亿", "亿");
  newchar = newchar.replace("亿万", "亿");
  newchar = newchar.replace("零万", "万");
  newchar = newchar.replace("零元", "元");
  newchar = newchar.replace("零角", "");
  newchar = newchar.replace("零分", "");
  
  if (newchar.charAt(newchar.length-1) == "元" || newchar.charAt(newchar.length-1) == "角")
    newchar = newchar+"整"
    return newchar;
  }
  /**
  *按下键的判断.1.只能录入数字和小数点.2:小数点只能录入一个.3:小数点后只能录入二位数字
  **/
function isInt(event,o){
  v=o.value;
  
  if (((event.keyCode>=48) && (event.keyCode<=57))||event.keyCode==46) {//如果录入数字和小数点时
	if(event.keyCode==46){//如果是小数点则看是否已存在
		var t=v.indexOf(".");		
		if(t!=-1){//如果存在
			return false;
		}
	}else{
  	var l=v.length;
  	var t=v.indexOf(".");		
  	if(t!=-1&&(l-t)>3)//如果小数点后超过两位.
  		return false
	}
	  return true;
  } else {
    return false;    
  }
}
/*
检测录入的数字小数点二位后不能再录入
*/
function keyUp(o){
  if(event.keyCode==13){
		inputOnblur(o);
		return ;
	}
    v=o.value;
	//alert(v);
	var l=v.length;
	var t=v.indexOf(".");		
	if(t!=-1&&(l-t)>3)//如果小数点后超过两位.
		o.value=v.substr(0,l-1);	
}

function selChange(o){
	/*这里仅仅先做个例子:*/
	test=o.parentNode.value;
	if(test.indexOf('核算帐')!=-1){
		top.coolWindows.createDefWindow('root1-fuzhu-3.html',100,100,340,580,false,false);
		return;
	}
     	if(test.indexOf('往来帐')!=-1){
		top.coolWindows.createDefWindow('root1-fuzhu-1.html',100,100,340,580,false,false);
		return;
	}
		if(test.indexOf('银行帐')!=-1){
		top.coolWindows.createDefWindow('root1-fuzhu-2.html',100,100,340,580,false,false);
		return;
	}
}

function sel(event,o){
  var activeTD_t;
	var summaryTD_t
  ele=event.srcElement;
	//alert(ele.outerHTML);
	//if (!src.contains(event.fromElement)) 
	if(ele.className.indexOf('inputSelectA_text')!=-1||ele.className.indexOf('select_list')!=-1)//
		return;
	//ele.parentNode.parentNode.parentNode.parentNode.id.indexOf('debit')!=-1||ele.parentNode.parentNode.parentNode.parentNode.id.indexOf('lender')!=-1||
	//if(ele.parentNode.id.indexOf('debit')!=-1||ele.id.indexOf('debit')!=-1||ele.parentNode.id.indexOf('lender')!=-1||ele.id.indexOf('lender')!=-1||ele.id.indexOf('total')!=-1||ele.id.indexOf('noname')!=-1||ele.parentNode.parentNode.parentNode.id.indexOf('_table_j')!=-1||ele.parentNode.parentNode.parentNode.id.indexOf('_table_d')!=-1||ele.tagName=='TR'||(ele.childNodes[0]!=null&&(ele.childNodes[0].id.indexOf('_table_j')!=-1))||(ele.childNodes[0]!=null&&(ele.childNodes[0].id.indexOf('_table_d')!=-1))){
	if(o.contains(event.srcElement)&&(ele.id.indexOf("abstract")==-1&&ele.id.indexOf("subject")==-1)){
		//alert("D")
		if(activeTD==null)
			return;
	}
		
	if(ele.id.indexOf('brief')!=-1){
		//如果点击的是摘要的隐藏文本框,则按点击此TD单元格来处理
		//先前以前的TD隐藏并将值回显给文本框
		summaryTD_t=ele.id;
		if(summaryTD!=null){
			activeSEL_old=summaryTD.replace('abstract','summary');
			document.all[activeSEL_old].display="false";
			activeText=summaryTD.replace('abstract','brief');
			document.all[activeText].style.display="block";
			document.all[activeText].value=document.all[activeSEL_old].text;
		}
		if(activeTD!=null){//如果当前科目TD有活动的.则显示为隐藏文本框
			activeSEL_old=activeTD.replace('subject','sel');
			document.all[activeSEL_old].display="false";
			activeText=activeTD.replace('subject','textvalue');
			document.all[activeText].style.display="block";
			document.all[activeText].value=document.all[activeSEL_old].text;
		}
		//然后再将现在这个TD里的东西显示	
			activeSEL_now=summaryTD_t.replace('brief','summary');
			document.all[activeSEL_now].display="block";
			//document.all[activeSEL_now].focus();
	
			activeText=summaryTD_t;
			document.all[activeText].style.display="none";
			summaryTD=summaryTD_t.replace('brief','abstract');
			return;
	}	
	if(ele.id.indexOf('abstract')!=-1){//如果点击的是摘要TD
		//alert(document.all['subject_1'].outerHTML);
		summaryTD_t=ele.id;
		if(summaryTD==null){//如果是第一次点TD或是当前TD没有活动的
			//alert("TD没有活动的");
			summaryTD=ele.id;			
			activeSEL_now=summaryTD_t.replace('abstract','summary');
			document.all[activeSEL_now].display="true";
			//document.all[activeSEL_now].focus();	
			summaryTD=summaryTD_t;
			
		}
		if(activeTD!=null){//如果当前科目TD有活动的.则显示为隐藏文本框
			activeSEL_old=activeTD.replace('subject','sel');
			document.all[activeSEL_old].display="false";
			activeText=activeTD.replace('subject','textvalue');
			document.all[activeText].style.display="block";
			document.all[activeText].value=document.all[activeSEL_old].text;
		}
		//先前以前的TD隐藏并将值回显给文本框
		
			//alert("现在TD有活动的");
			activeSEL_old=summaryTD.replace('abstract','summary');
			document.all[activeSEL_old].display="false";

			activeText=summaryTD.replace('abstract','brief');
			document.all[activeText].style.display="block";
			document.all[activeText].value=document.all[activeSEL_old].text;
			//然后再将现在这个TD里的东西显示	
			activeSEL_now=summaryTD_t.replace('abstract','summary');
			document.all[activeSEL_now].display="true";
			//document.all[activeSEL_now].focus();
	
			activeText=summaryTD_t.replace('abstract','brief');
			document.all[activeText].style.display="none";
			summaryTD=summaryTD_t;
	  return ;
	}
	//如果摘要的TD,则看他是不是摘要的SELECT.通过查找他的父结点中是否
			//alert(ele.parentNode.parentNode.parentNode.outerHTML);
	if(ele.parentNode.parentNode.id.indexOf('abstract')!=-1||ele.parentNode.parentNode.parentNode.id.indexOf('abstract')!=-1||ele.tagName=='TBODY'){//如果是摘要这边SELECT
		//alert("在摘要这片的下拉框中.");
	  return ;
	}
	else{
		//alert("不在摘要这片中!");
		if(summaryTD!=null){
			activeSEL_old=summaryTD.replace('abstract','summary');
			document.all[activeSEL_old].display="false";
			activeText=summaryTD.replace('abstract','brief');
			document.all[activeText].style.display="block";
			document.all[activeText].value=document.all[activeSEL_old].text;
			summaryTD=null;//摘要的活动TD为空.
		}
	}
	if(ele.id.indexOf('textvalue')!=-1){//如果点击的是隐藏文本框,则按点击此TD单元格来处理
		//先前以前的TD隐藏并将值回显给文本框
		//alert("textvalue");
		activeTD_t=ele.id;
		if(activeTD!=null){
			activeSEL_old=activeTD.replace('subject','sel');
			document.all[activeSEL_old].display="false";

			activeText=activeTD.replace('subject','textvalue');
			document.all[activeText].style.display="block";
			document.all[activeText].value=document.all[activeSEL_old].text;
		}
		//然后再将现在这个TD里的东西显示	
		activeSEL_now=activeTD_t.replace('textvalue','sel');
		document.all[activeSEL_now].display="true";
		//document.all[activeSEL_now].focus();

		activeText=activeTD_t;
		document.all[activeText].style.display="none";
		activeTD=activeTD_t.replace('textvalue','subject');
		return;
	}	
	
	if(ele.id.indexOf('subject')!=-1){
		//alert('subject');	
		activeTD_t=ele.id;
		if(activeTD==null){//如果是第一次点TD或是当前TD没有活动的
			activeTD=ele.id;			
			activeSEL_now=activeTD_t.replace('subject','sel');
			document.all[activeSEL_now].display="true";
			//document.all[activeSEL_now].focus();	
			activeTD=activeTD_t;
			return ;
		}
		//先前以前的TD隐藏并将值回显给文本框
		if(activeTD!=null){
			activeSEL_old=activeTD.replace('subject','sel');
			document.all[activeSEL_old].display="false";

			activeText=activeTD.replace('subject','textvalue');
			document.all[activeText].style.display="block";
			document.all[activeText].value=document.all[activeSEL_old].text;
		}
			//然后再将现在这个TD里的东西显示	
		activeSEL_now=activeTD_t.replace('subject','sel');
		document.all[activeSEL_now].display="true";
		//document.all[activeSEL_now].focus();

		activeText=activeTD_t.replace('subject','textvalue');
		document.all[activeText].style.display="none";

		activeTD=activeTD_t;
		return ;
	}
	//如果不是科目这边的TD,则看他是不是SELECT.通过查找他的父结点中是否
			//alert(ele.parentNode.parentNode.parentNode.outerHTML);
	if(ele.parentNode.parentNode.id.indexOf('subject')!=-1||ele.parentNode.parentNode.parentNode.id.indexOf('subject')!=-1||ele.tagName=='TBODY'){
		//alert("在科目中这边的下拉框中.");
		return ;
	}else{
		//alert("不在科目中");
		activeSEL_old=activeTD.replace('subject','sel');
		document.all[activeSEL_old].display="false";
		activeText=activeTD.replace('subject','textvalue');
		document.all[activeText].style.display="block";
		document.all[activeText].value=document.all[activeSEL_old].text;
	}
}
function tableClick(o){
  o.style.display="none";
  var oid=o.id;
  var iid=oid.replace('table','input');
  document.all[iid].style.display="block";
  document.all[iid].focus();
}

/**
显示格式化为X.XX.contain
**/
function formatFloat(v1){
  v=v1;
  var l=v.length;
  
  t=v.indexOf(".");
  if(t==-1)
    v=v+".00";
  
  if(t!=-1&&(l-t)==2){//eg:1.3
    v=v+"0"; 
  }
  if(t!=-1&&(l-t)==1){//eg:1.
    v=v+"00"; 
  }
  return v;

}
function inputOnblur(o){
  var v=o.value;
  if(v==null||v==""||v==".")
  v="0";
  var oid=o.id;
  var iid=oid.replace('input','table');
  v = formatFloat(v);
  
   
  v=v.replace('.','');//显示去掉.号
  if(v=="000")
  v="";
  var n=v.length;
  //alert(_table_2_1.outerHTML);
  for(var i=13,j=1;i>=0;i--,j++){
    document.all[iid].rows(0).cells(i).innerHTML='&nbsp;';
    if(n-j>=0){
      var vS=v.charAt(n-j);
      document.all[iid].rows(0).cells(i).innerText=vS;	
    }
  }

 
  o.style.display="none";
  document.all[iid].style.display="block";
  if(oid.length<=8){
  //统计借方金额
    idn=inputId.value;
    total_left=0;
    for(var i=0;i<idn;i++){
    	inputname="_input_"+(i+1);		
    	inputvalue=document.all[inputname].value;
    	if(inputvalue==""||inputvalue==null||inputvalue==".")
    		inputvalue=0;
    	total_left=total_left+parseFloat(inputvalue);
    }
	
	  total_left=roundFloat(parseFloat(total_left));
	

  var total_left_str=total_left.toString();
	//alert(total_left_str);
	total_left_str = formatFloat(total_left_str.toString());
	total_left_str=total_left_str.replace('.','');
	if(total_left_str=="000")
		total_left_str="";
	n=total_left_str.length;
	for(var i=13,j=1;i>=0;i--,j++){
    _table_j.rows(0).cells(i).innerHTML='&nbsp;';
		if(n-j>=0){
  		var vS=total_left_str.charAt(n-j);
  		_table_j.rows(0).cells(i).innerText=vS;	
		}
	}
  //total.innerText
  //sss=parseFloat(total_left);
  //alert(roundFloat(sss));
  }else{
  	//统计贷方金额
  	idn=inputId.value;
  	total_right=0;
  	for(var i=0;i<idn;i++){
  		inputname="_input_"+(i+1)+"_1";		
  		inputvalue=document.all[inputname].value;
  		if(inputvalue==""||inputvalue==null||inputvalue==".")
  			inputvalue=0;
  		total_right=total_right+parseFloat(inputvalue);
  	}
    total_right=roundFloat(parseFloat(total_right));
    var total_right_str=total_right.toString();
    total_right_str = formatFloat(total_right_str.toString());
    total_right_str=total_right_str.replace('.','');
    if(total_right_str=="000")
      total_right_str="";
    n=total_right_str.length;
    for(var i=13,j=1;i>=0;i--,j++){
      _table_d.rows(0).cells(i).innerHTML='&nbsp;';
      if(n-j>=0){
        var vS=total_right_str.charAt(n-j);
        _table_d.rows(0).cells(i).innerText=vS;	
      }
    }
  }
 if((total_left==total_right)&&(total_left!="0"))
	 total.innerText="合计:"+Arabia_to_Chinese(total_left.toString());
 else	
	 total.innerText="合计:";
}
