// 院长调用
function getDeptsFromList(dept_sign){
	__getDeptsFromListTemp__=["",""];
	__dept_sign__ = "1111";
	if ( typeof(dept_sign) != "undefined" ){
		__dept_sign__ = dept_sign;
	}
   openDialog(window.prefix+"base/scripts/business_DeptSelect.html", 'dialogWidth:400px;dialogHeight:500px');
	return __getDeptsFromListTemp__;
}

// 科主任调用
function getDeptsFrom(dept_code){
	__getDeptsFrom__=["",""];
	__dept_code__ = dept_code;
   openDialog(window.prefix+"base/scripts/business_DeptSelecta.html", 'dialogWidth:400px;dialogHeight:500px');
   //alert("mxs");
	return __getDeptsFrom__;
	}


//调用图形窗口
function openGraphDialog(gtype,gTitle,xmlData,xAxes,yAxes,isTipShow){
	if(xmlData=="<root></root>" || xmlData=="<root>undefined</root>"){
		alert("请选择数据！");
		return;
	}
	fusionchartsShow(gtype,gTitle,xmlData,xAxes,yAxes,isTipShow);
//	openGraphDialog_old(gtype,gTitle,xmlData,xAxes,yAxes,isTipShow);
}

function openGraphDialog_old(gtype,gTitle,xmlData,xAxes,yAxes,isTipShow){ 
	__dept_sign__ = xmlData;
	__gTitle__ = gTitle;
	if(gtype=="2"){
		__yAxes__='元';
		if( typeof(yAxes) !="undefined" ){
			__yAxes__=yAxes;
		}
		__xAxes__='';
		if( typeof(xAxes) !="undefined" ){
			__xAxes__=xAxes;
		}		
	}
	if(gtype=="1"){
		__yAxes__='元';
		if( typeof(yAxes) !="undefined" ){
			__yAxes__=yAxes;
		}
		__xAxes__='';
		if( typeof(xAxes) !="undefined" ){
			__xAxes__=xAxes;
		}
		
		openDialog(window.prefix+"base/scripts/LineGraph.html", 'dialogWidth:800px;dialogHeight:423px');
	}else{
		openDialog(window.prefix+"base/scripts/ShapeGraph.html", 'dialogWidth:800px;dialogHeight:423px');
	}
	//return __getDeptsFromListTemp__;
}

//创建控件
function CreateObjectControl(objID, ControlTYPE, ControlWIDTH, ControlHEIGHT ,CtrlDisplay)
{
	var sClsID = "";
	var sCodeBase = "";

	if (ControlTYPE == "vhchart")
	{
		sClsID = "CLSID:88404FA1-2B35-4C75-BBD6-DCAFEED65C52";	
		sCodeBase = "/vh/base/ActiveX/VHChart.CAB#version=1,0,0,26";
	}
	else
	{
		sClsID = "";
		sCodeBase = "";
	}
  if (CtrlDisplay=="undefined"){
  	CtrlDisplay="N";
  }
  
	var strControl = "";
	strControl += '<OBJECT id="' + objID + '" ';
	strControl += 'CLASSID="' + sClsID + '" ';
	strControl += 'CODEBASE="' + sCodeBase + '" ';
	//alert(CtrlDisplay);
	if (CtrlDisplay!="Y"){
		strControl += ' style="display:none" ';
	}
	strControl += 'WIDTH="' + ControlWIDTH + '" ';
	strControl += 'HEIGHT="' + ControlHEIGHT + '">';
	strControl += '</OBJECT>';

	document.write(strControl);
}

function workDeptSelect ( cfg_code ){
	var s = openDialog(window.prefix+"base/scripts/dept_select.html?load=<cfg>" + cfg_code + "</cfg>", 'dialogWidth:500px;dialogHeight:350px');
	return s;
}
function chargeDetailSelect ( cfg_code,dept_code){
	var s = openDialog(window.prefix+"base/scripts/charge_detail_select.html?load=<cfg>" + cfg_code + "</cfg><dept_code>"+dept_code+"</dept_code>", 'dialogWidth:800px;dialogHeight:500px');
	return s;
}

//FUSIONCHARTS图形展示---------lishix start
		function fusionchartsShow(gtype,gTitle,xmlData,xAxes,yAxes,isTipShow){
			var FusionChartsXML="";
			xAxes==null?xAxes="":xAxes;
			yAxes==null?yAxes="元":yAxes;
			var xmlDoc; 
			if(window.ActiveXObject){ 
				xmlDoc=new ActiveXObject("Microsoft.XMLDOM"); 
			}else if(document.implementation && document.implementation.createDocument){ 
				xmlDoc=document.implementation.createDocument("", "root", null); 
			} 
			
			xmlDoc.async="false";
			xmlDoc.loadXML(xmlData); 

			var chart;
			if(gtype==1){
				FusionChartsXML= encodeURI(Line(gTitle,xmlDoc,xAxes,yAxes,isTipShow));
			}else if(gtype==2){
				FusionChartsXML= encodeURI(Column(gTitle,xmlDoc,xAxes,yAxes,isTipShow));
			}else if(gtype==3){
				FusionChartsXML= encodeURI(Pie(gTitle,xmlDoc,isTipShow));
			}
			window.FusionChartsXML=FusionChartsXML;
			openEg('/base/scripts/FusionChartsShow.jsp?'+encodeURI('type='+gtype),800,423,false,false);//在屏幕中心弹出窗口
		}
		
		function Line(gTitle,xmlDoc,xAxes,yAxes,isTipShow){
			var xmlFusion="<chart imageSave='1' imageSaveURL='/base/scripts/FusionChartsSave.jsp' thousandSeparator=',' formatNumberScale='0' rotateYAxisName='0' xAxisName='"+xAxes+"' yAxisName='"+yAxes+"' caption='"+gTitle+"' palette='1' showValues='0'";
			if(yAxes=="元"){
				xmlFusion+=" numberPrefix='￥'>";
			}else{
				xmlFusion+=" >";
			}
			var items=xmlDoc.documentElement.childNodes;
			xmlFusion+="<categories>";
			for(i=0;i<items.item(0).childNodes.length;i++){ 
				var category =items.item(0).childNodes.item(i).getAttribute("name") ; 
				xmlFusion+="<category label='"+category+"'/>";
			}
			xmlFusion+="</categories>";
			for(i=0;i<items.length;i++){ 
				var item=items.item(i);
				if(item.hasChildNodes()){
					xmlFusion+="<dataset seriesName='"+item.getAttribute("name")+"'>";
					for(j=0;j<item.childNodes.length;j++){
						var node=item.childNodes.item(j);
						xmlFusion+="<set value='"+node.getAttribute("value").replace(/,/g,'')+"'/>";
					}
					xmlFusion+="</dataset>";
				}
			}
			xmlFusion+="</chart>";
			return xmlFusion;
		}

		function Pie3D(gTitle,xmlDoc,isTipShow){
			var xmlFusion="<chart imageSave='1' imageSaveURL='/base/scripts/FusionChartsSave.jsp' caption='"+gTitle+"' showPercentValues='1' showValues='1'>";
			var items=xmlDoc.documentElement.childNodes;
			for(i=0;i<items.length;i++){ 
				var item=items.item(i);
				for(j=0;j<item.childNodes.length;j++){
					var node=item.childNodes.item(j);
					if(node.getAttribute("name")==null){
						return Pie3D(gTitle,xmlDoc,isTipShow);
					}
					//xmlFusion+="<set label='"+node.getAttribute("name")+"' value='"+node.getAttribute("value")+"' />";
					xmlFusion+="<set label='"+node.getAttribute("name")+"' value='"+node.getAttribute("value").replace(/,/g,'')+"' toolText='"+node.getAttribute("name")+","+node.getAttribute("value")+"'/>";
				}
			}
			xmlFusion+="</chart>";
			return xmlFusion;
		}

		function Pie(gTitle,xmlDoc,isTipShow){
			var xmlFusion="<chart imageSave='1' imageSaveURL='/base/scripts/FusionChartsSave.jsp' caption='"+gTitle+"' showPercentValues='1' showValues='1'>";
			var items=xmlDoc.documentElement.childNodes;

			for(i=0;i<items.length;i++){ 
				var item=items.item(i);
				xmlFusion+="<set label='"+item.getElementsByTagName("name")[0].text+"' value='"+item.getElementsByTagName("value")[0].text+"' toolText='"+item.getElementsByTagName("name")[0].text+","+item.getElementsByTagName("value")[0].text+"'/>";
			}
			xmlFusion+="</chart>";
			//alert(xmlFusion)
			return xmlFusion;
		}
		
		function Column(gTitle,xmlDoc,xAxes,yAxes,isTipShow){
			return Line(gTitle,xmlDoc,xAxes,yAxes,isTipShow);
		}
		
		function openEg(url,width,height,left,top){
		 	 var temp = "channelmode=no;location=no;menubar=no;toolbar=no;directories=no;scrollbars=no;resizable=yes";
			 if (width) {
			  temp += ';dialogWidth=' + width+'px';
			 } else {
			  width = 0;
			 }
			 if (height) {
			  temp += ';dialogHeight=' + height+'px';
			 } else {
			  height = 0;
			 }
			 if (left) {
			  temp += ';left=' + left;
			 } else {
			  temp += ';left='
			    + Math.round((window.screen.width - parseInt(width)) / 2);
			 }
			 if (top) {
			  temp += ';top=' + top;
			 } else {
			  temp += ';top='
			    + Math.round((window.screen.height - parseInt(height)) / 2);
			 }
	//	 window.open(url, dialogArguments, temp);
		   window.showModalDialog(url, window, temp);
		}
		
//FUSIONCHARTS图形展示---------lishix end


function translateXmlToDoc(sourceXML){
			var xmlDoc; 
			if(window.ActiveXObject){ 
				xmlDoc=new ActiveXObject("Microsoft.XMLDOM"); 
			}else if(document.implementation && document.implementation.createDocument){ 
				xmlDoc=document.implementation.createDocument("", "root", null); 
			} 
			xmlDoc.async="false";
			xmlDoc.loadXML(sourceXML); 
			return xmlDoc
}
