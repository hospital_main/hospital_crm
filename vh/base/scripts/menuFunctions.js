/* ---------------------------------------------------- */
/* Copyright ?002-2004 Stedy Software and Systems      */
/* Please see http://www.stedy.com for terms of use.    */
/* ---------------------------------------------------- */

var activeChild = null ;
var objLinkWindow = null ;

function setThemeStyle(strPath) {
	var newStyleSheetPath = "themes/" + strPath + "/2k3Styles.css" ;
	var objWindows = window.document.all("2k3WindowsObject") ;
	window.document.styleSheets.item(0).href = newStyleSheetPath ;
	window.parent.frames.item("banner").document.styleSheets.item(0).href = newStyleSheetPath ;
	if (objWindows != null) {
		var objWindowContentWindow = null ;
		var objWindow = objWindows.getWindowByCaption("News") ;
		if (objWindow != null) {
			objWindowContentWindow = objWindow.getContentWindow() ;
			if (objWindowContentWindow != null) {
				objWindowContentWindow.contentWindow.document.styleSheets.item(0).href = newStyleSheetPath ;
			}
		}
		objWindow = objWindows.getWindowByCaption("Picture Gallery") ;
		if (objWindow != null) {
			objWindowContentWindow = objWindow.getContentWindow() ;
			if (objWindowContentWindow != null) {
				objWindowContentWindow.contentWindow.document.styleSheets.item(0).href = newStyleSheetPath ;
			}
		}
		objWindow = objWindows.getWindowByCaption("MP3 Collection") ;
		if (objWindow != null) {
			objWindowContentWindow = objWindow.getContentWindow() ;
			if (objWindowContentWindow != null) {
				objWindowContentWindow.contentWindow.document.styleSheets.item(0).href = newStyleSheetPath ;
			}
		}
	}
}

function openLocation(arrayWindow) {
	var themeName = window.document.all("themeSelect").value ;

	if(window.document.all('2k3WindowsObject').windowCount==6){
		alert("窗口数限制为六个!")
		return ;
	}
	window.document.all('2k3WindowsObject').createWindow(arrayWindow[0] + "?theme=" + themeName + "&date=" + new Date(), arrayWindow[1], arrayWindow[2], arrayWindow[3], arrayWindow[4], arrayWindow[5], arrayWindow[6], arrayWindow[7]) ;
}

function openLinkLocation(strLocation) {
	if (objLinkWindow != null) { objLinkWindow.close() ; }
	objLinkWindow = window.open(strLocation) ;
}

function showCopyright() {
	var dialogObject = new Object() ;
	dialogObject.Title = "Copyright Statement" ;
	dialogObject.SourceUrl = "copyright.aspx" ;
	dialogObject.UseCancel = false ;
	dialogObject.UseApply = false ;
	dialogObject.Theme = window.document.all("themeSelect").value ;
	window.showModalDialog("dialogHost.aspx?title=" + dialogObject.Title, dialogObject, "dialogWidth:505px; dialogHeight:307px; center:yes; help:no; resizable:no; status:no") ;
}
