
function XmlHttp() {
  this._object = new ActiveXObject("Microsoft.XMLHTTP");
}

XmlHttp.prototype.post = function() {
  if (arguments.length < 2) {
    alert("参数个数不对")
    return;
  }
  var postfix = ".viewhigh";
  if (arguments.length == 3) {
    postfix = postfix + arguments[2]
  }
 
  this._object.open("POST", arguments[0]+postfix, false);
  try {
    //var tt = String(arguments[1]).replace(/[&<>\u0080-\uffff]/g, _eaReplace);

    this._object.send("<root>"+arguments[1]+"</root>");
  } catch (exception){
    alert("连接失败，请检查网络！")
  }
}

xmlhttp = new XmlHttp;




var modeCodeToNameList = {
"00":"sys",
"01":"uinfo",
"02":"acct",
"03":"rep",
"04":"mate",
"05":"equi",
"06":"drug",
"07":"pote",
"08":"wage",
"09":"budg",
"10":"payctl",
"11":"ven",
"12":"cbcs",
"13":"perf",
"14":"bonus",
"15":"hisc",
"20":"stat"
};

function getModCode(modValue){
	var mod_code="";
	if(modValue==null || modValue=="" || typeof(modValue)!="string"){
		return "";
	}
	for(var key in modeCodeToNameList){
		if(modeCodeToNameList[key]==modValue){
			mod_code=key;
			break;
		}
	}
	
	return mod_code;
}



function openDialog(addr, argu, obj , scroll_flag) {
	var vUrl = document.URL.replace(/[^\/]*$/, '')

	vUrl = vUrl.replace(/<[.]*>/g, '')
	
	var w=screen.width-50;
	var h=screen.height-50;
	argu=argu.replace("[MAXW]",w+"px");
	argu=argu.replace("[MAXH]",h+"px");

  var count = 100;
  while (vUrl.search(/\.[^\/]*\/$/)!=-1 || vUrl.search(/<\/$/)!=-1) {
    vUrl = vUrl.replace(/[^\/]*\/$/g, "")
    if (--count<0) {
      alert('地址解析错：'+window.document.URL)
      return;
    }
  }
  if (addr.indexOf('http')!=-1) vUrl=''

	if (obj != null)
	  window._obj1 = obj;
	  
	
	if (scroll_flag == "true")
	   return window.showModalDialog(vUrl+addr, window, "help:no;status:no;resizable:yes;scroll:yes;"+argu)
  else
     return window.showModalDialog(vUrl+addr, window, "help:no;status:no;resizable:yes;scroll:no;"+argu)
}


function getPageArg(name){
	if(!document._bufPageArgsXMLObject){
		var urlStr=document.URL.toString();
		var xmlStr="<root>"+urlStr.substr(urlStr.indexOf("?load=")+6)+"</root>";
		document._bufPageArgsXMLObject=new ActiveXObject("Microsoft.XMLDOM");
		document._bufPageArgsXMLObject.loadXML(xmlStr);
	}
	var xml=document._bufPageArgsXMLObject;
	var path="root/"+name;
	if(xml.selectNodes(path).length>0){
		 if(xml.selectNodes(path)[0].childNodes.length>0){
		 		return xml.selectNodes(path)[0].childNodes[0].data;
		 	}
	}
		return "";
}

function showRuleCodeText(obj,formatId,prefixText,comp_copy){
	var xml;
	if(typeof(comp_copy)!="undefined"&&comp_copy!=null&&comp_copy!="")
		xml=getDict("sys_code_format_comp_copy",comp_copy+"<code>"+formatId+"</code>");
	else
		xml=getDict("sys_code_format_para","<code>"+formatId+"</code>");
	var para=xml.getElementsByTagName("para")[0];
	var format=para.getAttribute("code");
	if(typeof(prefixText)=="string")
		format=prefixText+"："+format;
	else
		format="编码规则："+format;
		
	obj.innerHTML=format;
}

var __envionmentValue=new Object();
function _getEnvionmentObject(){
	if(__envionmentValue)
		return __envionmentValue;
	else{
		var p=typeof(window.dialogArguments)=="undefined"?parent:window.dialogArguments;
		if(p)
			return p._getEnvionmentObject();
		else
			return null;
	}
	
}
function _getEnvionmentValue(n){
	var p=_getEnvionmentObject();
	if(p==null)
		return null;
	if(p[n])
		return _getEnvionmentObject()[n];
	else
		return null;
}
function _setEnvionmentValue(n,v){
	var p=_getEnvionmentObject();
	if(p==null)
		return null;
	p[n]=v;
}


function getAcctYear(){
  return _getEnvionmentValue("_Acctyear");
}

function getUserID(){
	return _getEnvionmentValue("_UserID");
}
function getCompCode(){
  return _getEnvionmentValue("_CompCode");
}
function getCopyCode(){
  return _getEnvionmentValue("_CopyCode");
}

function getEmpCode(){
  return _getEnvionmentValue("_nowEmpCode");
}
function getEmpName(){
  return _getEnvionmentValue("_nowEmpName");
}
function getDBTime(){
  return _getEnvionmentValue("_DBTime");
}
function getLoginDate(){
	return _getEnvionmentValue("_LoginDate");
}

function getModuleCode(){
	if(typeof(window.dialogArguments)!="undefined"){
		if(typeof(window.dialogArguments.top.absModule)!="undefined" && window.dialogArguments.top.absModule!=null){
			return getModCode(window.dialogArguments.top.absModule);
		}
		return getModCode(window.dialogArguments.top.module);
	}
	
	if(typeof(top.absModule)!="undefined" && top.absModule!=null){
		return getModCode(top.absModule);
	}
	return getModCode(top.module);
}

function getModuleName(){
	if(typeof(window.dialogArguments)!="undefined"){
		if(typeof(window.dialogArguments.top.absModule)!="undefined" && window.dialogArguments.top.absModule!=null){
			return window.dialogArguments.top.absModule;
		}
		return window.dialogArguments.top.module;
	}
	
	if(typeof(top.absModule)!="undefined" && top.absModule!=null){
		return top.absModule;
	}
	return top.module;
}

function setEnvCompCopy(){
	var xml = "<mode>set</mode><CompCode>" + _getEnvionmentValue("_CompCode") + "</CompCode>";
	xml += "<CopyCode>" + _getEnvionmentValue("_CopyCode") + "</CopyCode>";
  window.xmlhttp.post("login", "<user><subfunction>GlobalEnv</subfunction>" + xml + "</user>");
	var str = window.xmlhttp._object.responseText;
	
	_setEnvionmentValue("_CurCompCode",getEnvionmentValue("_CompCode"));
	_setEnvionmentValue("_CurCopyCode",getEnvionmentValue("_CopyCode"));
}

function reloadData() {
  if(getEmpCode()==null || getEmpName()==null || getDBTime()==null){
    var strLocation = new String(window.location);
		window.top.window.module = strLocation.replace(/^[^\?]*\?/g,'');
    window.xmlhttp.post("repLogin","<user><subfunction>getsession</subfunction><module>cbcs</module><ext>cbcs</ext></user>");
    var str = window.xmlhttp._object.responseText;
    if (str.search(/<error>/)!=-1) {
      var error = str.substring(str.search(/<error>/)+"<error>".length, str.search(/<\/error>/));
      alert(error);
      window.close();
      return false;
      
    }
    var options = str.split(";");
    _setEnvionmentValue("_module",window.module);
    if(typeof(top.absModule)!="undefined" && top.absModule!=null){
    	_setEnvionmentValue("_absModule",window.absModule);
    }
    
    _setEnvionmentValue("_nowEmpCode",options[0]);
    _setEnvionmentValue("_nowEmpName",options[1]);
    _setEnvionmentValue("_historyName",options[2]);
    _setEnvionmentValue("_DBTime",options[3]);
    
    if(getEnvionmentValue("_Acctyear")==null){
      _setEnvionmentValue("_Acctyear",options[4].substring(0,4));
      _setEnvionmentValue("_AcctyearBeginDate",options[5]);
      _setEnvionmentValue("_AcctyearEndDate",options[6]);

	    //登陆用户id
	    _setEnvionmentValue("_UserID",options[7]);
	    //当前登陆用户是否管理员、所操作的单位编码、账套编码、拥有单位总数、账套总数、登陆用户上级ID
	    _setEnvionmentValue("_IsAdmin",options[8]);
	    _setEnvionmentValue("_CompCode",options[9]);
	    _setEnvionmentValue("_CopyCode",options[10]);
	    _setEnvionmentValue("_CompCount",options[11]);
	    _setEnvionmentValue("_CopyCount",options[12]);
	    _setEnvionmentValue("_UserSuperID",options[13]);
	    _setEnvionmentValue("_CurCompCode",options[14]);
	    _setEnvionmentValue("_CurCopyCode",options[15]);
	    _setEnvionmentValue("_LoginDate",options[16]);
	    _setEnvionmentValue("_CurModLevel",options[17]);
    }
  setEnvCompCopy();
  }
  
  
}
reloadData();



function checkRuleCodeFormatBySql(code,allowLetter,sqlId,param,showErr){

	var res=getRuleCodeParentCode(code,allowLetter,sqlId,param);
	if(res==null&&showErr==true)
	
	return res;
}

function getCheckRuleCodeFormatErr(formatId){
	return document["_CODE_FORMATE_ERR_TEXT_"+formatId];
}

function getRuleCodeParentCode(code,allowLetter,sqlId,param){
	//正确时返回 父编码,不正确时返回 null
	
	var xml=getDict(sqlId,param);
	//alert(xml.getElementsByTagName("para")[0]+"EEETTT");
	var para=xml.getElementsByTagName("para")[0];
	//alert(para+"para")
	var format=para.getAttribute("code");
	//alert(format);
	var name=para.getAttribute("value");
	code=code.replace(/(^\s+)|\s+$/g, "");
	var regStr="^(\\d)*$";
	var regErr="只允许数字";
	if(typeof(allowLetter)!="undefined"&&allowLetter==true){
		regErr="只允许数字和字母";
		regStr="^([a-zA-Z0-9_])*$";
	}
	document["_CODE_FORMATE_ERR_TEXT_"+sqlId]="不正确的"+name+" : "+format+","+regErr;
	if(code=="")
		return null;
   	digitExp = new RegExp(regStr);
   	if(digitExp.test(code)==false){
   		return null;
   	}
   	
	var cs=format.split("-");
	var len=0,plen=0;
	for(var i=0;len<code.length&&i<cs.length;i++){
		plen=len;
		len+=parseInt(cs[i],10);
	}
	if(len!=code.length)
		return null;
	if(plen==0)
		return "";
	else
		return code.substr(0,plen);
}


function getDict(argu,para) {

	if (para != null) { 
		xmlhttp.post("global_select", para, "?selectID="+argu);
	}
	else
		xmlhttp.post("global_select", '', "?selectID="+argu);
	
	if (window.doMsg(xmlhttp._object.responseText)) {
		var vXml= new ActiveXObject("Microsoft.XMLDOM");
		vXml.async = false;
		vXml.loadXML(xmlhttp._object.responseText)
		return vXml;
	}
}
function GetPageURL(TableID){
	var sURL = document.location.href.toString();
	sURL_a=sURL.split('?');
	
	eval(TableID + ".CurrentURL = '" + sURL_a[0] + "'");
}

function getValuePairBySql(sql,para){
	var datXml=getDict(sql,para)
	if(!datXml)
		return null;
	var paras=datXml.documentElement.getElementsByTagName("para");
	if(paras.length<1)
		return null;
	else
		return [paras[0].getAttribute("code"),paras[0].getAttribute("value")];
}

function getReportPathCode(mod_code){
	var r=getValuePairBySql("rep_getReportPathCode","<mod_code>"+mod_code+"</mod_code>");
	if(r==null){
		return "";
	}else{
		return r[0];
	}
}

////Cell Print BEGIN d35f169cd160b4b3
function printXmlToCellByXsltFile(xmlData,xslFile,setData,dirPrint,isPrview,pageUrl){
	var vXsl = new ActiveXObject("Microsoft.XMLDOM");
	vXsl.async=false;
	vXsl.load(xslFile)
	var vXml = new ActiveXObject("Microsoft.XMLDOM");
	vXml.async=false;
	vXml.loadXML(xmlData);
	if(setData){
  	var endRow="<annex>";
  	for(var o in setData){
  		endRow+="<"+o+">"+setData[o]+"</"+o+">"
  	}
  	endRow+="</annex>";
  	var vXmlEndRow = new ActiveXObject("Microsoft.XMLDOM");
    vXmlEndRow.async=false;
    vXmlEndRow.loadXML(endRow);
    if(xmlData != ""){
    	vXml.selectNodes("/root")[0].appendChild(vXmlEndRow.selectNodes("/annex")[0]); 
    }
  }
	printStringDataToCell(vXml.transformNode(vXsl),setData,dirPrint,isPrview,pageUrl);
}
function printStringDataToCell(data,setData,dirPrint,isPrview,pageUrl){
	var cellXml = new ActiveXObject("Microsoft.XMLDOM");
	cellXml.async=false;
	cellXml.loadXML(data);
	printXmlDataToCell(cellXml,setData,dirPrint,isPrview,pageUrl);
}
var _printXmlDataToCell_Data_=null;
var _printXmlDataToCell_Param_=null;
var _printXmlDataToCell_Dir_=null;
var _printXmlDataToCell_Prview_=null;
var _printXmlDataToCell_PageUrl_=null;
var _printXmlDataToCell_Emp_=getEmpName();
var _printXmlDataToCell_Init=false;
function printXmlDataToCell(data,setData,dirPrint,isPrview,pageUrl,openFrame){
	var win_prefix=window.prefix;
	var screenHeight=window.screen.availHeight;
	var screenWidth=window.screen.availWidth;  
	var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto";
	if(setData!=null){
		var ks;
		for(var o in setData){
			ks=o.split("_");
			if(ks.length!=3)
				continue;
			if(ks[0]!="thead"&&ks[0]!="tbody"&&ks[0]!="tfoot")
				continue;
			var segs=data.getElementsByTagName(ks[0]);
			if(segs.length==0)
				continue;
			var trs=segs[0].getElementsByTagName("tr");
			if(trs.length==0||trs.length<parseInt(ks[1]-1,10))
				continue;
			var tds=trs[ks[1]-1].getElementsByTagName("td");
			if(tds.length==0||tds.length<parseInt(ks[2]-1,10))
				continue;
			tds[ks[2]-1].text=setData[o];
		}	
	}
	_printXmlDataToCell_Data_=data;
	_printXmlDataToCell_Param_=setData;
	_printXmlDataToCell_Dir_=dirPrint;
	_printXmlDataToCell_Prview_=isPrview;
	if(openFrame==null||openFrame=='ModalDialog'){
		_printXmlDataToCell_Init=true;
		var win=window.showModalDialog(win_prefix+"base/print1/printh2c.html",window,dialogStyle);
		
	}else if(openFrame=='_new'){
		_printXmlDataToCell_Init=true;
		var win=window.open(win_prefix+"base/print1/printh2c.html",'New');
		
	}else{
		_printXmlDataToCell_Init=false;
		//var f=eval(openFrame);
		//f.document.location.href=	win_prefix+"base/print1/printh2c.html";
	}
}
////Cell Print END 3ff51ca139496acb


function isRuleCode(value){
	var va=value.replace(/-/g,"");
	if(value.indexOf("-")<0||value.indexOf("--")>=0)
		return false;
	digitExp = new RegExp("^(\\d)*$");
   	if(digitExp.test(va)==false){
   		return false;
   	}
   	if(value.substr(0,1)=="-"||value.substr(value.length-1,1)=="-")
   		return false;
   	return true;
}
function decodeXmlChar(s){
	s=s.replace(/&quot;/g,"\"");
	s=s.replace(/&amp;/g,"&");
	s=s.replace(/&gt;/g,">");
	s=s.replace(/&lt;/g,"<");
	return s;
}
function encodeXmlChar(s){
	s=s.replace(/&/g,"&amp;");
	s=s.replace(/>/g,"&gt;");
	s=s.replace(/</g,"&lt;");
	return s;
}
function _clearIFdlgTag(u){
	if(window.__ifdlgtag){
		if(u.indexOf("&"+window.__ifdlgtag+"=")>0)
			u=u.substr(0,u.indexOf("&"+window.__ifdlgtag+"="));
	}
	return u;
}
function getSysParaDataValue(c){
	var e=getEnvionmentValue("_tablePageSize"+c);
	if(e==null){
		var p=getValuePairBySql("dict_default_table_page_size","<m>"+modeCodeToNameList[getModuleCode()]+"</m><c>"+c+"</c>");
		if(p==null)
			e="";
		else
			e=p[0];
		setEnvionmentValue("_tablePageSize"+c,e);
	}
	return e;
}
function getDefaultPageSizeByMod(){
	//var v=getSysParaDataValue("0321");
	var v="";//0321含义改变，不再获取0321
	if(v=="")
		v="0";
	return v;
}