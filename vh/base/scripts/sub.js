//非模太窗口打印 0 模太窗口  1 普通窗口
var _printWindow = 0




var xmlHTTP

//String的处理
String.prototype.trim = function(){
  return this.replace(/(^\s+)|\s+$/g, "");
};

//function的处理
Function.READ = 1;
Function.WRITE = 2;
Function.READ_WRITE = 3;
Function.prototype.addProperty = function(sName, nReadWrite){
  nReadWrite = nReadWrite || Function.READ_WRITE;
  var capitalized = sName.charAt(0).toUpperCase() + sName.substr(1);
  if (nReadWrite & Function.READ)
    this.prototype["get" + capitalized] = new Function("", "return this._" + sName + ";");
  if (nReadWrite & Function.WRITE)
    this.prototype["set" + capitalized] = new Function(sName, "this._" + sName + " = " + sName + ";");
};

function XmlHttp() {
  this._object = new ActiveXObject("Microsoft.XMLHTTP");
}

XmlHttp.prototype.post = function() {
  if (arguments.length < 2) {
    alert("参数个数不对")
    return;
  }
  var postfix
  if(typeof(self.posttype)!="undefined" && self.posttype!=null)
  	postfix=self.posttype;
  else
  	postfix = ".hbviewhigh";
  if (arguments.length >= 3) {
    postfix = postfix + arguments[2]
  }
  //alert(self.posttype)
 	if (arguments.length == 4){
 		//alert( arguments[3]);
 		//alert("4");

  	this._object.open("POST", arguments[0]+postfix, true);
	}
	else
	{
		//alert("3");
  	this._object.open("POST", arguments[0]+postfix, false);
	}
  try {
    //var tt = String(arguments[1]).replace(/[&<>\u0080-\uffff]/g, _eaReplace);

    this._object.send("<root>"+arguments[1]+"</root>");
  } catch (exception){
    alert("连接失败，请检查网络！")
  }
}
XmlHttp.prototype.send = function() {
	if (arguments.length < 4) {
		alert("参数个数不对")
		return;
	}
	var cb=arguments[3];
	var postfix = ".hbviewhigh"+ arguments[2];
	var u=arguments[0]+postfix;
	var axml = new ActiveXObject("Microsoft.XMLHTTP");
	axml.open("POST",u,true);
	axml.onreadystatechange=function(){
		if(axml.readyState==4){
			if (axml.status>=400){
				alert("error loading document: "+u+" - "+axml.statusText+" - "+axml.responseText);
				__clearXmlHttpObj(axml);
				return;
      			};
			if(axml.status!=200){
				__clearXmlHttpObj(axml);
		  		return ;
		  	};
		  	cb(axml.responseText,axml.responseXML);
			__clearXmlHttpObj(axml);
		};
	};
	axml.send("<root>"+arguments[1]+"</root>");
}
function __clearXmlHttpFun(){};
function __clearXmlHttpObj(obj){
	obj.onreadystatechange=__clearXmlHttpFun;
	obj=null;
}
function openDialog(addr, argu, obj , scroll_flag) {
	var vUrl = document.URL.replace(/[^\/]*$/, '')

	vUrl = vUrl.replace(/<[.]*>/g, '')

  var count = 100;
  while (vUrl.search(/\.[^\/]*\/$/)!=-1 || vUrl.search(/<\/$/)!=-1) {
    vUrl = vUrl.replace(/[^\/]*\/$/g, "")
    if (--count<0) {
      alert('地址解析错：'+window.document.URL)
      return;
    }
  }
  if (addr.indexOf('http')!=-1) vUrl=''

	if (obj != null)
	  window._obj1 = obj;
	var s = "";
	if (scroll_flag == "true")
	   s = window.showModalDialog(vUrl+addr, window, "status:no;resizable:yes;scroll:yes;"+argu)
  else
     s = window.showModalDialog(vUrl+addr, window, "status:no;resizable:yes;scroll:no;"+argu)
  return s;
}
function _eaReplace( s )
{
	switch ( s )
	{
		case "&":
			return "&amp;";
		case "<":
			return "&lt;";
		case ">":
			return "&gt;";
		default:
			return "&#" + s.charCodeAt(0) + ";";
	}
}
xmlhttp = new XmlHttp;

try {
	window.document.oncontextmenu = window.top.onRightClick ;
}
catch(e) {
}

function getPersonConfig(){
  return window.top.window._personConfig;
}
function getUserId(){
  return window.top.window._nowUserId;
}
function getEmpCode(){
  return window.top.window._nowEmpCode;
}
function getEmpName(){
  return window.top.window._nowEmpName;
}
function getDBTime(){
  return window.top.window._DBTime;
}
function getHistoryName(){
  return window.top.window._historyName;
}
function getAcctYear(){
  return window.top.window._Acctyear;
}
function getAcctYearBeginDate(){
  return window.top.window._AcctyearBeginDate;
}
function getAcctYearEndDate(){
  return window.top.window._AcctyearEndDate;
}
function getDrugDept(){
  return window.top.window._drugDept;
}
function getLoginDate(){
 return window.top.window.currLoginDate;
}
function GetPageURL(TableID){
	var sURL = document.location.href.toString();
	sURL_a=sURL.split('?');

	eval(TableID + ".CurrentURL = '" + sURL_a[0] + "'");
}
/**页面装载或刷新时取得后台数据**/
function reloadData() {
  if(getEmpCode()==null || getEmpName()==null || getDBTime()==null){
    window.xmlhttp.post("login","<user><subfunction>getsession</subfunction><module>"+window.module+"</module></user>","");
    var str = window.xmlhttp._object.responseText;
    if (str.search(/<error>/)!=-1) {
      var error = str.substring(str.search(/<error>/)+"<error>".length, str.search(/<\/error>/));
      alert(error);
      window.close();
      return false;
    }
    var options = str.split(";");
    window.top.window._nowEmpCode=options[0];
    window.top.window._nowEmpName=options[1];
    window.top.window._historyName=options[2];
    window.top.window._nowUserId=options[3];
    window.top.window._personConfig=options[4];
    //for old cbcs jsp begin
    window.top.window.currUnitName=options[2];
    window.top.window.currUserName=options[1];
    window.top.window.currUserId=window.top.window.user_code=options[0];
    window.top.window.currLoginDate=options[5];
    //for old cbcs jsp end
    //window.top.window._DBTime=options[3];
  }
}
reloadData();
if(getEmpCode()==null||getEmpCode()==""){
	alert("操作超时，请重新登录！");
	try{
		 window.top.relogin();
	}catch(E){
		
	}
}
/**************
  补充
**************/
function printView(printTable,subTitle,xslfile,topMessage, bottomMessage){
  var strXml = printTable.printXml
  var vHead = strXml.substring(strXml.search(/<thead>/i), strXml.search(/<\/thead>/i)+"</thead>".length);
  window._printXML = new ActiveXObject("Microsoft.XMLDOM");
  window._printXML.async=false;

  var vXml = new ActiveXObject("Microsoft.XMLDOM");
  vXml.async=false;
  vXml.loadXML(strXml);
  var vXsl = new ActiveXObject("Microsoft.XMLDOM");
  vXsl.async=false;

  var realxslfile = "";
  if(xslfile!=null){
  	realxslfile = xslfile;
  }else{
  	realxslfile = "printView.xsl";
  }

  vXsl.load(window.document.URL.replace(/\?.*$/g,'').replace(/[^\/]*$/, realxslfile));
  var temp = vXsl.xml
  if (temp==null || temp.length==0) {
    alert('此文件'+window.document.URL.replace(/[^\/]*$/, realxslfile)+'不符合XSL格式')
    return;
  }
  //如果是动态表头
  if(temp.search(/<thead\/>/)!=-1){
      temp = temp.replace(/<thead\/>/,vHead);
      vXsl.loadXML(temp)
  }

  //prompt('',vXsl.xml,'')
  if(topMessage != null)
    temp = temp.replace(temp.substring(temp.search(/<top>/),temp.search(/<\/top>/)+6),topMessage);
  if (bottomMessage != null)
    temp = temp.replace(temp.substring(temp.search(/<bottom>/),temp.search(/<\/bottom>/)+9),bottomMessage);
  if(subTitle != null)
    vXsl.loadXML(temp.replace(/<subtitle>.*<\/subtitle>/,"<subtitle>"+subTitle+"</subtitle>"));
  xmlHTTP = new ActiveXObject("Microsoft.XMLHTTP")
  xmlHTTP.open("POST", "print.hbviewhigh", false);
  try {
    // 设置原始的xml, 加编制单位
    window._printXml = vXml.transformNode(vXsl).replace(/<root/, "<root comName='"+window._comName+"'");
  	window._printXML.loadXML(vXml.transformNode(vXsl));
    xmlHTTP.send(window._printXML.xml);
  } catch (exception){
    alert(exception)
  }
  var screenHeight=window.screen.availHeight;
  var screenWidth=window.screen.availWidth;
  var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto"
  var win=window.showModalDialog(window.prefix+"/base/print/print.html",window,dialogStyle)
}



// 表格单列checkbox处理 2-1
function tableDoCheck(headName, bodyName) {
  var heads = document.getElementsByName(headName)
  var bodys = document.getElementsByName(bodyName)

  for (var i=0; i<heads.length; i++) {
    heads[i].onclick = function() { // 全选或全取消
		  for(var j=0;j<bodys.length;j++) {
        if (bodys[j].checked!=this.checked) {
	        bodys[j].parentNode.parentNode.rowSubmit = true
	        bodys[j].checked=this.checked;
          if (bodys[j].checked==true)
            bodys[j].parentNode.value='<td>1</td>'
          else
            bodys[j].parentNode.value='<td>0</td>'
	      }
		  }
    }
  }

  var vFlag=true;
  for (var i=0; i<bodys.length; i++) {
    bodys[i].onclick = function() {
      this.parentNode.parentNode.rowSubmit = true

      // 修改属性
      if (this.checked==true)
        this.parentNode.value='<td>1</td>'
      else
        this.parentNode.value='<td>0</td>'

      if (trim(this.parentNode.show)==trim(this.parentNode.value)) {
        this.parentNode.parentNode.rowSubmit = null
        this.parentNode.value=null
      }

      // 全选或全取消
		  var flag;
	    for(var j=0;j<bodys.length;j++) {
	      if(bodys[j].checked==false){
				  flag=false;
				  break;
			  }
		  }

	    for(var j=0;j<heads.length;j++){
	  	 	if(flag==false)
  				heads[j].checked=false;
  			else
  			  heads[j].checked=true;
	    }
    }

    // 全选或全取消
	  if(bodys[i].checked==false && vFlag) {
			vFlag=false;
	  }
  }

  for(var j=0;j<heads.length;j++){
	 	if(vFlag==false)
			heads[j].checked=false;
		else
		  heads[j].checked=true;
  }
}

// 表格单列checkbox处理 2-2
function tableSave(btn, comp) {
  var submitStr = "";
  var tbody = comp.getElementsByTagName('tbody')[1]

  var vRecord=""
  for (var i=0; i<tbody.rows.length; i++) {
    if (tbody.rows[i].rowSubmit!=true) continue;

    for (var j=0; j<tbody.rows[i].cells.length; j++) {
      vRecord = vRecord + trim(tbody.rows[i].cells[j].value)
    }
    if (trim(vRecord)!='') {
      submitStr = submitStr + "<record>" + vRecord + "</record>"
      vRecord = ''
    }
  }
  if (trim(submitStr)=='') {
    alert('数据没有发生修改')
    return true;
  }
  window.xmlhttp.post(btn.name, submitStr)
  var str = window.xmlhttp._object.responseText
  if (!window.doMsg(str)) {
    return true;
  }

	for (var i=0; i<tbody.rows.length; i++) {
	  if (tbody.rows[i].rowSubmit!=true) continue;
	  tbody.rows[i].rowSubmit=null
	  for (var j=0; j<tbody.rows[i].cells.length; j++) {
      if (trim(tbody.rows[i].cells[j].value)!='' && trim(tbody.rows[i].cells[j].value)!=trim(tbody.rows[i].cells[j].show)) {
     		tbody.rows[i].cells[j].show = trim(tbody.rows[i].cells[j].value)
      }
    }
  }
}
/*
function subInit(comp) {
	if (window.dialogArguments!=null)
		window.dialogArguments.subInit(comp)
	else
		window.top.window.subInit(comp)
}
*/
/*
 * 文件输出方法，使用前先向后台发查询请求，根据返回结果输出成Excel
 * 第一个参数: 输出到文件的结果集
 * 第二个参数：xsl的名字，默认为本文件名加后缀.xsl
 * 第三个参数：提交的地址，则向后台提交为globalOutput_(第二个参数), 默认为globalOutput, 此参数尽可能不用
 */
function doOutput() {
  // 检查查询后是否有输出
  var str = window.xmlhttp._object.responseText
  if (arguments.length >= 1 && trim(arguments[0])!='') str=arguments[0]
  if (str.indexOf("td")==-1) {
    alert('没有可以输出成文件的内容')
    return false;
  }
  var vXml = new ActiveXObject("Microsoft.XMLDOM");
  vXml.async=false;
  vXml.loadXML(str.replace(/<root/, "<root comName='"+window._comName+"'"))

  //  第一个参数
  var xsl = document.URL.replace(/.html.*$/g, "_output.xsl");
  if (arguments.length >= 2 && trim(arguments[1])!='') xsl=arguments[1]
  var vXsl = new ActiveXObject("Microsoft.XMLDOM")
  vXsl.async=false;
  vXsl.load(xsl)

  // 第二个参数
  var subFunc = ''
  if (arguments.length >= 3 && trim(arguments[2])!='') subFunc=arguments[2]

  window.xmlhttp._object.open("open", "globalOutput.hbviewhigh?subFunc="+subFunc, false);
  window.xmlhttp._object.send(vXml.transformNode(vXsl))
  str = window.xmlhttp._object.responseText
  if (str.indexOf("<error>")!=-1) { // 错误检测
    alert(str.substring(str.search(/<error>/)+"<error>".length, str.search(/<\/error>/)))
    return false;
  }
  var path = window.prefix + str.substring(str.search(/<msg>/)+"<msg>".length, str.search(/<\/msg>/))

  //Test
  alert(path);

  window.open(path)
}


function getPrintRoot(strXml, xslFile) {
  var vXsl = new ActiveXObject("Microsoft.XMLDOM");
  vXsl.async=false;
  if (trim(xslFile)=='')
    vXsl.load(window.document.URL.replace(/.html.*$/g, ".xsl"));
  else
    vXsl.load(xslFile)
  var vXml = new ActiveXObject("Microsoft.XMLDOM");
  vXml.async=false;
  vXml.loadXML(strXml)
  //alert(vXml.transformNode(vXsl));
  vXml.loadXML(vXml.transformNode(vXsl))
  return vXml
}

/**
 * 调整跨格
 */
function adjSpan(root) {
 try {
    //补齐cell, 注意有地方存 行宽度
    var colspan, rowspan, style;
    var trs = root.selectNodes("/root/thead/tr");
    for (var i=0; i<trs.length; i++) {
      var ths = trs[i].selectNodes("th");
      for (var j=0; j<ths.length; j++) {
        style = ths[j].getAttribute("style")
        if (style==null || style.indexOf("display:none")==-1) continue;

        // 依据上一个cell
        try  {
          if (i>0)
            rowspan=parseInt(trs[i-1].childNodes[j].getAttribute("rowspan"))
          else
            rowspan=1
        } catch(exception) {
          rowspan=1
        }
        if (isNaN(rowspan)) rowspan=1
        if (rowspan>1) {
          try  {
            colspan=parseInt(trs[i-1].childNodes[j].getAttribute("colspan"))
          } catch(exception) {
            colspan=1
          }
          if (isNaN(colspan)) colspan=1

          ths[j].setAttribute("colspan", colspan)
          ths[j].setAttribute("rowspan", new String(rowspan-1));
          if (ths[j].firstChild!=null) {
            ths[j].firstChild.nodeValue = trs[i-1].childNodes[j].firstChild.nodeValue
          } else
            ths[j].appendChild(trs[i-1].childNodes[j].firstChild.cloneNode(true))
          continue;
        }

        //  依据左一个cell
        try  {
          if (j>0)
            colspan=parseInt(ths[j-1].getAttribute("colspan"))
          else
            colspan=1
        } catch(exception) {
          colspan=1
        }
        if (isNaN(colspan)) colspan=1

        if (colspan>1) {
          try  {
            rowspan=parseInt(ths[j-1].getAttribute("rowspan"))
          } catch(exception) {
            rowspan=1
          }
          if (isNaN(rowspan)) rowspan=1
          ths[j].setAttribute("colspan", new String(colspan-1))
          ths[j].setAttribute("rowspan", rowspan)
          if (ths[j].firstChild!=null) {
            ths[j].firstChild.nodeValue=ths[j-1].firstChild.nodeValue
          } else {
            ths[j].appendChild(ths[j-1].firstChild.cloneNode(true))
          }
          continue;
        } else {
          alert('此cell i:'+i+'  j:'+j+' colspan:'+colspan+'不对')
          alert(style)
          alert(vXml.xml)
          return false;
        }
      }
    }
  } catch (exception) {
    alert(exception)
    return false;
  }
  return true;
}


/**
 * 设置页面
 * root, 数据源
 * pageWidth, pageHeight, 页长, 页宽, 单位mm, 不带单位
 * 页边踞, topMargin, bottomMargin, leftMargin, rightMargin, 单位mm, 不带单位
 * 固定列, fixCol
 * 行比例, 列比例, 字体比例, rowScale, colScale, fontScale
 * 以上参数必输
 */
function setupPage(root, pageWidth, pageHeight, topMargin, bottomMargin, leftMargin, rightMargin, fixCol,
  rowScale, colScale, fontScale) {
  //调整rowspan和colspan
  if (!adjSpan(root)) {
    return false;
  }

  var base = 3.7736
  // 初始化变量
  pageWidth=pageWidth*1
  pageHeight=pageHeight*1
  topMargin=topMargin*1
  bottomMargin=bottomMargin*1
  leftMargin=leftMargin*1
  rightMargin=rightMargin*1
  fixCol=fixCol*1
  rowScale=rowScale*1
  colScale=colScale*1

  var rootNode = root.selectNodes("/root")[0]
  rootNode.setAttribute("pageWidth", (pageWidth*base));
  rootNode.setAttribute("pageHeight", (pageHeight*base));
  rootNode.setAttribute("topMargin", (topMargin*base));
  rootNode.setAttribute("bottomMargin", (bottomMargin*base));
  rootNode.setAttribute("leftMargin", (leftMargin*base));
  rootNode.setAttribute("rightMargin", (rightMargin*base));
  rootNode.setAttribute("fixCol", fixCol);
  rootNode.setAttribute("rowScale", rowScale);
  rootNode.setAttribute("colScale", colScale);
  rootNode.setAttribute("fontScale", fontScale);

  var fixHeight=25  // 最开始的Table cellspacing=1
  // 计算标题头
  var titles = root.selectNodes("/root/top/title")
  for (var i=0; i<titles.length; i++) {
    var temp = getNum(titles[i].getAttribute("style"), "height:", 40)
    rootNode.setAttribute("titleHeight", temp)
    fixHeight=fixHeight+temp+3
  }
  titles = root.selectNodes("/root/top/maintitle") // 历史原因含 maintitle
  for (var i=0; i<titles.length; i++) {
    var temp = getNum(titles[i].getAttribute("style"), "height:", 40)
    rootNode.setAttribute("titleHeight", temp)
    fixHeight=fixHeight+temp+3
  }
  var subtitles = root.selectNodes("/root/top/subtitle")
  for (var i=0; i<subtitles.length; i++) {
    var temp = getNum(subtitles[i].getAttribute("style"), "height:", 22)
    rootNode.setAttribute("subTitleHeight", temp)
    fixHeight=fixHeight+temp+3
  }
  var topTrs = root.selectNodes("/root/top/tr")
  for (var i=0; i<topTrs.length; i++) {
    var style = topTrs[i].getAttribute("style");
    if (style == null) style=""
    if (style.indexOf("height:")==-1)
      topTrs[i].setAttribute("style", (style+";height:18;"))
    fixHeight=fixHeight+getNum(topTrs[i].getAttribute("style"), "height:", 18)+3
  }

  // 计算标题尾
  var bottomTrs = root.selectNodes("/root/bottom/tr")
  for (var i=0; i<bottomTrs.length; i++) {
    var style = bottomTrs[i].getAttribute("style");
    if (style == null) style=""
    if (style.indexOf("height:")==-1)
      bottomTrs[i].setAttribute("style", (style+";height:18;"))
    fixHeight=fixHeight+getNum(bottomTrs[i].getAttribute("style"), "height:", 18)+3
  }

  // 计算表头
  var headTrs = root.selectNodes("/root/thead/tr")
  for (var i=0; i<headTrs.length; i++) {
    var temp = getNum(headTrs[i].getAttribute("style"), "height:", 24)
    temp = temp*rowScale
    headTrs[i].setAttribute("height", temp)
    fixHeight=fixHeight+temp+3
  }

  // 加上上下边距
  fixHeight=fixHeight+(topMargin+bottomMargin)*base

  // 修正fixHeight
  if (_printWindow==0 && window.dialogArguments._fixHeight!=null && !isNaN(window.dialogArguments._fixHeight))
    fixHeight=fixHeight+window.dialogArguments._fixHeight*rowScale

  if (_printWindow==1 && window.opener._fixHeight!=null && !isNaN(window.opener._fixHeight))
    fixHeight=fixHeight+window.opener._fixHeight*rowScale


  var rowB=1, rowE=1, colB=1, colE=1;
  var num=0
  var rowgroup = root.createNode(1, "rowGroup", "");
  var bodyTrs=root.selectNodes("/root/tbody/tr");
  for (var i=0; i<bodyTrs.length; i++) {
    var temp = (getNum(bodyTrs[i].getAttribute("style"), "height:", 22)+3)*rowScale
    bodyTrs[i].setAttribute("height", temp)
    if (i==0) {
      num=fixHeight+temp
      continue;
    }
    if (parseInt((num)/(pageHeight*base))<parseInt(((num+temp))/(pageHeight*base))) {
      rowE=i; // 由于i 从0 计数,所以需要加1,
      var newNode=root.createNode(1, "page", "")
      newNode.setAttribute("rowB", rowB)
      newNode.setAttribute("rowE", rowE)
      rowB=i+1
      rowgroup.appendChild(newNode)
      num=fixHeight+temp
    } else
      num+=temp
  }
  // 最后一行
  var newNode=root.createNode(1, "page", "")
  newNode.setAttribute("rowB", rowB)
  newNode.setAttribute("rowE", bodyTrs.length)
  rowgroup.appendChild(newNode)

  // 计算固定列头
  var fixWidth = 20
  var cols=root.selectNodes("/root/colgroup/col")
  for (var i=0; i<cols.length && i<fixCol; i++) {
    var temp = (getNum(cols[i].getAttribute("style"), "width:", 80)+3)*colScale
    cols[i].setAttribute("width", temp)
    fixWidth+=temp
  }

  // 加上 左右边距
  fixWidth=fixWidth+(leftMargin+rightMargin)*base

  var colgroup = root.createNode(1, "colgroup", "")
  num=0;
  colB=fixCol+1;
  for (var i=fixCol; i<cols.length; i++) {
    var temp = (getNum(cols[i].getAttribute("style"), "width:", 80)+3)*colScale
    cols[i].setAttribute("width", temp)
    if (i==fixCol) {
      num=temp+fixWidth
      continue;
    }

    if (parseInt((num)/(pageWidth*base))<parseInt(((num+temp))/(pageWidth*base))) {
      colE=i;
      newNode=root.createNode(1, "page", "")
      newNode.setAttribute("colB", colB)
      newNode.setAttribute("colE", colE)
      colB=i+1
      colgroup.appendChild(newNode)
      num=temp+fixWidth;
      // 修正表头
      for (var j=0; j<headTrs.length; j++) {
        var ths = headTrs[j].selectNodes("th");
        if (ths.length != cols.length) {
          alert('请在thead中放置th元素,而不是td元素, 同时需要补齐隐藏cell 或者 colgroup/col 的数量和thead/th的数量不一致')
          break;
        }
        var style=ths[i].getAttribute("style")
        if (style!=null && style.indexOf("display:none")!=-1) {
          // 检查上一个cell是否跨行
          if (j>0) {
            try {
              var rowspan = parseInt(headTrs[j-1].selectNodes("th")[i].getAttribute("rowspan"))
              if (!isNaN(rowspan) && rowspan>1) {
                continue;
              }
            } catch(ex) {
            }
          }
          ths[i].setAttribute("style", style.replace(/display:none/, ''))
          try {
            var rowspan = parseInt(ths[i].getAttribute("rowspan"))
            if (rowspan>1)
              j+=(rowspan-1)
          } catch(e) {
          }
        }
      }
    } else
      num+=temp
  }

  // 最后一列
  newNode=root.createNode(1, "page", "")
  newNode.setAttribute("colB", colB)
  newNode.setAttribute("colE", cols.length)
  colgroup.appendChild(newNode)

  // 合并生成pagegroup, 列同样的道理
  var pagegroup=root.createNode(1, "pagegroup", "")
  mergeNode(pagegroup, rowgroup, colgroup, 'true')

  var root = root.selectNodes("/root")[0]
  while (root.selectNodes("/root/pagegroup").length>0)
    root.removeChild(root.selectNodes("/root/pagegroup")[0])

  root.appendChild(pagegroup)
}


function getNum(comp, str, value) {
  if (comp==null) {
    return value;
  }
  var idx = comp.indexOf(str);
  if (idx==-1) {
    return value;
  }
  var temp = comp.substring(idx)
  idx = temp.indexOf(";")
  if (idx==-1)
    idx = temp.length
  try {
    return parseFloat(temp.substring(0, idx).replace(/[^0-9\.]*/, ''))
  } catch (e) {
    return value;
  }
}

/**
 * flag - true 先横向排序, 再纵向排序
 *      - false 相反
 */
function mergeNode(group, rows, cols, flag) {
  var xmlDoc= new ActiveXObject("Microsoft.XMLDOM");
  if (flag=='true') {
    for (var i=0; i<rows.childNodes.length; i++) {
      for (var j=0; j<cols.childNodes.length; j++) {
        var temp = xmlDoc.createNode(1, "page", "")
        temp.setAttribute("rowB", rows.childNodes[i].getAttribute("rowB"))
        temp.setAttribute("rowE", rows.childNodes[i].getAttribute("rowE"))
        temp.setAttribute("colB", cols.childNodes[j].getAttribute("colB"))
        temp.setAttribute("colE", cols.childNodes[j].getAttribute("colE"))
        group.appendChild(temp)
      }
    }
  } else {
    for (var j=0; j<cols.childNodes.length; j++) {
      for (var i=0; i<rows.childNodes.length; i++) {
        var temp = root.createNode(1, "page", "")
        temp.setAttribute("rowB", rows.childNodes[i].getAttribute("rowB"))
        temp.setAttribute("rowE", rows.childNodes[i].getAttribute("rowE"))
        temp.setAttribute("colB", cols.childNodes[j].getAttribute("colB"))
        temp.setAttribute("colE", cols.childNodes[j].getAttribute("colE"))
        group.appendChild(temp)
      }
    }
  }
}

/**
 * 打印预览
 */
function printView1() {
  if (_printWindow==1) {
    var win = window.open(window.prefix+"/base/print1/print1.html", "printView")

    win.focus()

    return false;
  }

  var screenHeight=window.screen.availHeight;
  var screenWidth=window.screen.availWidth;
  var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto"
  var win=window.showModalDialog(window.prefix+"/base/print1/print.html",window,dialogStyle)

}
function printView2() {
  if (_printWindow==1) {

    var win1 = window.open(window.prefix+"/base/print1/print1.html", "printView")
    win1.focus()

    return false;
  }

  var screenHeight=window.screen.availHeight;
  var screenWidth=window.screen.availWidth;
  var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto"

  var win1=window.showModalDialog(window.prefix+"/base/print1/print2.html",window,dialogStyle)
}

/**
 * 文件上传
 */
function uploadFile(path,actionName){
  // 创建 ADO-stream 对象
  var ado_stream = new ActiveXObject("ADODB.Stream");

  // 创建包含默认头信息和根节点的 XML文档
  var xml_dom = new ActiveXObject("MSXML2.DOMDocument");
  xml_dom.loadXML('<?xml version="1.0" ?> <root/>');
  // 指定数据类型
  xml_dom.documentElement.setAttribute("xmlns:dt", "urn:schemas-microsoft-com:datatypes");

  // 创建一个新节点，设置其为二进制数据节点
  var l_node1 = xml_dom.createElement("file_upload");
  l_node1.dataType = "bin.base64";
  // 打开Stream对象，读源文件
  ado_stream.Type = 1;  // 1=adTypeBinary
  ado_stream.Open();
  try {
    ado_stream.LoadFromFile(path);
  } catch(exception) {
    alert("文件有误，不能上传！");
    return false;
  }
  // 将文件内容存入XML节点
  //l_node1.nodeTypedValue = ado_stream.Read(-1); // -1=adReadAll
  this._object = new ActiveXObject("Microsoft.XMLHTTP");
  this._object.open("POST", actionName, false);
  this._object.send(ado_stream.Read(-1));
  ado_stream.Close();
  return window.doMsg(_object.ResponseText);
}

/**
 * 日期比较
 */
function dateCompare(d1,d2){
  var first = new Date(d1.replace(/-.*/g,''),d1.replace(/^[^-]*-|-[^-]*$/g,''),d1.replace(/.*-/g,''));
  var end = new Date(d2.replace(/-.*/g,''),d2.replace(/^[^-]*-|-[^-]*$/g,''),d2.replace(/.*-/g,''));
  if (first > end ) {
    return true;
  } else {
    return false;
  }
}

//是不是电子邮件地址
function isEmail(src) {
	var isEmail1  = /^\w+([\.\-]\w+)*\@\w+([\.\-]\w+)*\.\w+$/;
	var isEmail2  = /^.*@[^_]*$/;
	var rg1=new RegExp(isEmail1);
	var rg2=new RegExp(isEmail2);
   	return (rg1.test(src) && rg2.test(src));
}
//是不是电话号码
function isPhone(src) {
	var Phone = /^(\+\d+ )?(\(\d+\) )?[\d ]+$/;
	var rg2=new RegExp(Phone);
   	return (rg2.test(src));
}
//是不是年龄
function isAge (src) {
	var Phone = /^(1[0-2]\d|\d{1,2})$/;
	var rg2=new RegExp(Phone);
   	return (rg2.test(src));
}
//是不是货币值
function  isMoney(src) {
	var Phone = /^\d+\.\d{4}$/;
	var rg2=new RegExp(Phone);
   	return (rg2.test(src));
}
//是不是IP地址
function  isIP(src) {
	ip_ip = '(25[0-5]|2[0-4]\\d|1\\d\\d|\\d\\d|\\d)';
    	ip_ipdot = ip + '\\.';
    	address = new RegExp('^'+ip_ipdot+ip_ipdot+ipdot+ip_ip+'$');
   	return (address.test(src));
}
//是不是邮编
function  isZipCode(src) {
	var Phone = "^[\\d]{6}$";
	var rg2=new RegExp(Phone);
   	return (rg2.test(src));
}
//是不是身份证号
function  isIdCorrect(src) {
	var isEmail1  =/^\d{15}$/;
	var isEmail2  =/^\d{18}$/;
	var rg1=new RegExp(isEmail1);
	var rg2=new RegExp(isEmail2);
   	return (rg1.test(src) || rg2.test(src));
}
function loadAcctSubj(yearObj,subObj,acctYear){
  if(yearObj!=null)
    yearObj.value=acctYear;
  if(subObj!=null){
    subObj.para="<year>"+acctYear+"</year>";
    subObj.refresh();
  }
}
function setTodayYear(inp){
	var d=new Date();
	inp.setValue(d.getFullYear());
}
function setTodayMonth(inp){
	var d=new Date();
	var m=d.getMonth()+1;
	m='00000'+m;
	m=m.substr(m.length-2);
	inp.setValue(m);
}
function getCurrentDate(){
    var d=new Date();
		var year=d.getFullYear();
		var month=d.getMonth()+1;
		var day=d.getDate();
		year='0000'+year;
		year=year.substring(year.length-4);
	  month='00'+month;
	  month=month.substring(month.length-2);
	  day='00'+day;
	  day=day.substring(day.length-2);
	  return year+'-'+month+'-'+day;
}
function getCurrentDateTime(){
    var myDate=new Date();
		var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
		var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
		var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
		year='0000'+year;year=year.substring(year.length-4)
	  month='00'+month;month=month.substring(month.length-2)
	  day='00'+day; day=day.substring(day.length-2)
	  return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString()
  }
function getPrintTime(){
    return getCurrentDateTime();
  }
function getPrintBottomStr(){
    var bottom= "<bottom><tr><td colspan='7' style='text-align:right;'>"+"打印日期: "+getPrintTime()+" 制表人："+getEmpName()+"</td></tr>"+
	            "<tr><td colspan='7' style='text-align:right'>北京东软望海科技有限公司  制作</td></tr></bottom>";
		return bottom;
  }
function getPrintTopStr(tableTitle,yearMonth,tableId){
	var tt="<top><maintitle>"+tableTitle+"</maintitle><subtitle>"+yearMonth+"</subtitle>";
             tt=tt+"<tr><td ></td><td style='text-align:right;' > 页号：[页码]/[总页数]</td></tr>";
             tt=tt+"<tr><td >"+getHistoryName()+"</td><td style='text-align:right;' >"+tableId+"</td></tr>";
             tt=tt+"<tr><td ></td><td style='text-align:right;' >金额单位：元</td></tr>";
             tt=tt+"";
             tt=tt+"</top>";

    return tt;
}

//不需要经过lineCtn的打印
function printAllDataNOlineCtn(conditionStr,actionName,xsl,tableTitle,yearMonth,tableId){
    window.xmlhttp.post(actionName,conditionStr);
    var serverXml = window.xmlhttp._object.responseText;
    return printCbcsXmlData(serverXml,xsl,tableTitle,yearMonth,tableId);
}

//打印预览所有查询数据
function printViewAllData(lineCtnObj, actionName, xslFileName, tableTitle, subTitle){
    //请求要预览的所有数据
    window.xmlhttp.post(actionName, lineCtnObj.assemble());
    var serverXml = window.xmlhttp._object.responseText;
		if(!window.doMsg(serverXml)){
			return;
		}

		var root = getPrintRoot(serverXml, xslFileName);
		if(!root){
			return;
		}

		var topStr =getPrintTopStr(tableTitle, subTitle);

		var vXml = new ActiveXObject("Microsoft.XMLDOM");
		vXml.async=false;
		vXml.loadXML(topStr);
		root.selectNodes("/root")[0].appendChild(vXml.selectNodes("/top")[0]);

		var bottomStr=getPrintBottomStr();
		var vXml2 = new ActiveXObject("Microsoft.XMLDOM");
		vXml2.async=false;
		vXml2.loadXML(bottomStr);
		root.selectNodes("/root")[0].appendChild(vXml2.selectNodes("/bottom")[0]);

		window._printXml = root;
		window.outputFlag = true;
		printView1();
		return true;
}

function printCbcsTable(tableCtn,xsl,tableTitle,yearMonth,tableId){
        return printCbcsXmlData(tableCtn.serverXml,xsl,tableTitle,yearMonth,tableId);
}
function printCbcsXmlData(xmlData,xsl,tableTitle,yearMonth,tableId){
		var root = getPrintRoot(xmlData, xsl);
     	var topStr =getPrintTopStr(tableTitle,yearMonth,tableId);
			//alert(topStr);
        var vXml = new ActiveXObject("Microsoft.XMLDOM");
        vXml.async=false;
        vXml.loadXML(topStr);
        root.selectNodes("/root")[0].appendChild(vXml.selectNodes("/top")[0]);

        var bottomStr=getPrintBottomStr();
				var vXml2 = new ActiveXObject("Microsoft.XMLDOM");
    	vXml2.async=false;
    	vXml2.loadXML(bottomStr);
    	root.selectNodes("/root")[0].appendChild(vXml2.selectNodes("/bottom")[0]);

        window._printXml = root;
        window.outputFlag = true;
        printView1();
        return true;
}
function printCbcsTable1(tableCtn,xsl,tableTitle,yearMonth,tableId){
		var root = getPrintRoot(tableCtn.serverXml, xsl);
     	var topStr =getPrintTopStr(tableTitle,yearMonth,tableId);
			//alert(topStr);
        var vXml = new ActiveXObject("Microsoft.XMLDOM");
        vXml.async=false;
        vXml.loadXML(topStr);
        root.selectNodes("/root")[0].appendChild(vXml.selectNodes("/top")[0]);

        var bottomStr=getPrintBottomStr();
				var vXml2 = new ActiveXObject("Microsoft.XMLDOM");
    	vXml2.async=false;
    	vXml2.loadXML(bottomStr);
    	root.selectNodes("/root")[0].appendChild(vXml2.selectNodes("/bottom")[0]);

        window._printXml = root;
        window.outputFlag = true;
        printView2();
        return true;
}
function getPageArg(name){
	if(!document._bufPageArgsXMLObject){
		var urlStr=document.URL.toString();
		var xmlStr="<root>"+urlStr.substr(urlStr.indexOf("?load=")+6)+"</root>";
		document._bufPageArgsXMLObject=new ActiveXObject("Microsoft.XMLDOM");
		document._bufPageArgsXMLObject.loadXML(xmlStr);
	}
	var xml=document._bufPageArgsXMLObject;
	var path="root/"+name;
	if(xml.selectNodes(path).length>0){
		 if(xml.selectNodes(path)[0].childNodes.length>0)
		 	return xml.selectNodes(path)[0].childNodes[0].data;
	}
		return "";
}
function initInputValueByPageArg(input2){
	var v=getPageArg(input2.name);
	if(v!=""){
		if(typeof(input2.setValue)!="undefined")
			input2.setValue(v);
		else
			inputs2.value=v;
	}
}

function getOneValueFromDict(sqlName,para){
	var data=getDict(sqlName,para);
	var paras=data.getElementsByTagName("para");
	var code="";
	var text="";
	if(paras.length>0){
		code=paras[0].getAttribute("code");
		text=paras[0].getAttribute("value");
	}
	return [code,text];
}
function setTablePrimary(inp,pkTag,value){
	var tr=inp;
	while(tr.tagName.toLowerCase()!="tr")
		tr=tr.parentNode;
	tr.rowSubmit=true;
	var cs=tr.getElementsByTagName("input");
	var v;
	if(cs.length>0){
		for(var i=0;i<cs.length;i++){
			if(cs[i].type=="checkbox"){
				cs[i].checked=true;
				v=cs[i].value;
				var ex=new RegExp("<"+pkTag+"(.*)<\/"+pkTag+">");
				v=v.replace(ex,"<"+pkTag+">"+value+"</"+pkTag+">");
				cs[i].value=v;
			}
		}
	}
}
function openChart(url) {
		window.open(window.prefix+url,'','width=750,height=570, top=100, left=100, scrollbars = 1, resizable = 1');
}
function openTooMuchLink(pageMap,pos,paras){
	openDialog(pageMap[pos]+'.html?load='+paras, 'dialogWidth:'+pageMap["width"]+';dialogHeight:'+pageMap["height"])
}
function openTooMuchLink2(pageMap,pos,paras){
	openIFDialog(window,pageMap[pos]+'.html?load='+paras, 'dialogWidth:'+pageMap["width"]+';dialogHeight:'+pageMap["height"],'',true)
}
///delay onload begin

function adjustCtl( obj )
{
	if ( ( obj.value!=undefined  && obj.value.trim() == "" ) )
	{
		alert( ( obj.label ? obj.label: "") + '不能为空!');
		if ( obj.focus ) obj.focus();
		return false;
	}
	return true;
}

function execinit(cmd){
	cmd();
	return;
	__onload_cmd__=cmd;
	_execinit();
}

function _execinit(){
	var init=true;

	init=_execinit_test();

	if(init==false){
		if(__onload_timer__==null)
			__onload_timer__=window.setInterval(_execinit,10);
	}
	if(init==true){
		if(__onload_timer__!=null)
			window.clearInterval(__onload_timer__);
		__onload_cmd__();
	}
}
function _execinit_test(){
	var tags=["input","div"];
	for(var i=0;i<tags.length;i++){
		if(_execinit_test2(tags[i])==false)
			return false;
	}
	return true;
}
function _execinit_test2(tag){
	var nodes=document.getElementsByTagName(tag);
	if(nodes.length>0)
		for(var i=0;i<nodes.length;i++){
			if(nodes[i].getAttribute("className")!=null&&nodes[i].getAttribute("id")!=null){
				if(_execinit_test3(nodes[i],nodes[i].getAttribute("className"),nodes[i].getAttribute("id"))==false)
					return false;
			}
		}
	return true;
}
function _execinit_test3(n,clazz,id){
	for(var k in __onload_classToMethod){
		if(clazz==k&&eval("typeof(n."+__onload_classToMethod[k]+")")=="undefined")
			return false;
	}
	return true;
}
var __onload_classToMethod={
		"inputTextA":"check",
		"inputSelect":"check",
		"inputInteger":"check",
		"inputDecimal":"check",
		"lineCtn":"check",
		"tableCtn":"refresh",
		"reportCtn":"reset"
	}
__onload_timer__=null;
__onload_cmd__=null;
////delay onload end
////LZK ADD END
function spell_code(obj,spell,define){
	// 可以接受空对象，如果只生成一个码，可以通过一下引用 spell_code(obj,null,define)
	// 可以接受空对象，如果只生成一个码，可以通过一下引用 spell_code(obj,null,define)
  var srcTree = new ActiveXObject("Microsoft.XMLDOM");
  srcTree.async=false;
  srcTree.load("/base/scripts/data.xml");
  var spell_code="";//存放检索出来的拼音码
  var define_code="";//存放检索出来的用户定义码
  var temp;//存放每次取得输入汉字的值
  var objNodeList = srcTree.getElementsByTagName("record");

	var objText = obj.value.trim();//去除多出的空格
//  alert(objNodeList.item(100).attributes.item(0).nodeValue);
  for(var i=0; i< obj.value.length; i++){
  		temp=  obj.value.charAt(i);
	   for (var j=0; j<objNodeList.length; j++) {
	     if(temp ==objNodeList.item(j).attributes.item(0).nodeValue )
	    	{
		     spell_code = spell_code + objNodeList.item(j).attributes.item(1).nodeValue ;
		     define_code = define_code + objNodeList.item(j).attributes.item(2).nodeValue  ;
		    }
	 	 }
  }

 if(spell !=null) spell.value = spell_code;
 if(define != null)  define.value = define_code;
 return;

}
//add by wsj
    function retrieve_change(){//变换当前输入法，改变系统的参数设置，然后将参数从新榜定，刷新数据源。
 			var win
   		if(event.keyCode =='123'){
			  win= window.self;//取得本身的窗口句柄

			  if(window.dialogArguments !=null)
			 		win=window.dialogArguments;//如果嵌套多层模态窗口，则取得其上层的非摸态窗口，然后由其再取得最上层中的元素，进行赋值
    		while(win.dialogArguments !=null){
   					win=win.dialogArguments;
   				}

 				setRetrieveMethod(win);//输入法切换
				retrieveRefresh();//对数据源进行刷新
 		  }
    }

    function getRetrieve(){//得到当前检索方式
    		var win= window.self;//取得本身的窗口句柄
			  if(window.dialogArguments !=null)
			  	win=window.dialogArguments;//如果嵌套多层模态窗口，则取得其上层的非摸态窗口，然后由其再取得最上层中的元素，进行赋值
			  while(win.dialogArguments !=null){
			        win=win.dialogArguments;
			   		 }
    		return win.top.retrieve_method;
    	}

	function setRetrieveMethod(win){//输入法只有两种属性0，1 ,对检索方式切换进行赋值
			  	 if(win.top.retrieve_method==0){
			     	  win.top.retrieve_method=1;
	           	win.top.retrieve.innerHTML="检索方式：自定义"
			     }
		   	 else{
			     	  win.top.retrieve_method=0;
		     		  win.top.retrieve.innerHTML="检索方式：拼音"
		      }
  	}

    //判断activeX控件失去焦点
  function CheckActivexFocus(table_id){
  	var mo=document.getElementById(table_id);
  	if(document.activeElement==mo){
  		mo.SetActiveXFocus(false);
  	}else{
  		mo.SetActiveXFocus(true);
  	}
  }

  //表格控件用取当前页面URL
  function GetPageURL(TableID)
  {
  	var sURL = document.location.href.toString();
  	sURL_a=sURL.split('?');

  	eval(TableID + ".CurrentURL = '" + sURL_a[0] + "'");
  }

  function retrieveRefresh(){//将所有检索方式的下拉框，更具页面检索方式进行刷新

    	 var objNodeList = window.document.getElementsByTagName("input");
		   for (var i=0; i<objNodeList.length; i++) {
		   		if(objNodeList[i].load == "retrieve_method")
		   	  	objNodeList[i].refresh();

		   	   }
    }
//wsj


/*************** select类 *******/
function SelectObj() {}

SelectObj.prototype.setPara= function() {
  if (arguments.length != 1) {
    alert("参数个数不对")
    return;
  }
  this.para = arguments[0]
}
SelectObj.prototype.setLoad= function() {
  if (arguments.length != 1) {
    alert("参数个数不对")
    return;
  }
  this.load = arguments[0]
}
SelectObj.prototype.setXML = function() {
  if (arguments.length != 1) {
    alert("参数个数不对")
    return;
  }
  this.XML = arguments[0]
}

SelectObj.prototype.setCode = function() {
  if (arguments.length != 1) {
    alert("参数个数不对")
    return;
  }
  this.code = arguments[0]
}

SelectObj.prototype.select = function(para) {
	var p="";
	if(typeof(para)!="undefined"){
		p="?load="+para;
	}
  openDialog(window.rootPath+'base/select/find.html'+p, 'dialogWidth:900px;dialogHeight:423px;')
}

selectObj = new SelectObj
function getValuePairBySql(sql,para){
	if(typeof(getDict)=="undefined")
		return null;
	var datXml=getDict(sql,para)
	if(!datXml)
		return null;
	var paras=datXml.documentElement.getElementsByTagName("para");
	if(paras.length<1)
		return null;
	else
		return [paras[0].getAttribute("code"),paras[0].getAttribute("value")];
}
function getSpellCode(str){
	var p=getValuePairBySql("comm_get_py_spell","<s>"+str+"</s>");
	if(p!=null)
		return p[0];
	else
		return "";
}
//titleStr格式:"银行科目,业务日期,结算方式,票据号,摘要,借方金额,贷方金额"
function creatActiveXHeadXML(titleStr){
		var titleArray = titleStr.split(",");
		var titleXML = "<?xml version='1.0' encoding='GBK'?><root><msg/><error/><record><total/></record><activeHead/><thead/><tbody><tr>";
		for(t in titleArray){
			titleXML += "<td>" + trim(titleArray[t]) + "</td>";
		}
		titleXML += "</tr></tbody></root>";
		return titleXML;
}
/*
	 <iframe id="imgframe" style="position:absolute;display:none;left:0;top:0;width:100%;height:100%;z-index=90000">
		</iframe>
    <div id="img" style="position:absolute;display:none;left:0;top:0;width:100%;height:100%;z-index=90000;">
			<table width="100%" height="100%"><tr><td align="center" valign="middle"><img src="/vh/img/loading.gif"; border="0"></td></tr></table>
		</div>



function createLoading(){
	var imgframe;
	var myhead;
	var img;
	var myTable;
	var myTr;
	var myTd;
	var myImg;

	myhead = document.getElementsByTagName("body").item(0);

	imgframe=document.getElementsByTagName("imgframe").item(0);
	if(imgframe!=null){
		imgframe=document.createElement("iframe");
			with(iframe){
				style.position = "absolute";
				style.display = "none";
				style.left=0;
				style.top=0;
				style.width="100%";
				style.height="100%";
				style.z-index=90000;
			}
		imgframe = myhead.appendChild(imgframe);
	}

	img=document.getElementsByTagName("img").item(0);
	if(img!=null){
		img=document.createElement("div");
			with(img){
				style.position = "absolute";
				style.display = "none";
				style.left=0;
				style.top=0;
				style.width="100%";
				style.height="100%";
				style.z-index=90009;
			}

		myTable=document.createElement("table");
			with(myTable){
				width="100%";
				height="100%";
			}
		myTr=document.createElement("tr");

		myTd=document.createElement("td");
		myTd.align="center";
		myTd.valign="middle";

		myImg=document.createElement("img");
		myImg.src="/vh/img/loading.gif";
		myImg.border="0";

		myImg=myTd.appendChild(myImg);
		myTd=myTr.appendChild(myTd);
		myTr=myTable.appendChild(myTr);
		myTable=img.appendChild(myTable);
		img=myHead.appendChild(img);
		imgframe = myhead.appendChild(imgframe);
	}
}*/
var endElem
function noContextMenu(){
	if((event.srcElement.tagName=="TD")||(event.srcElement.tagName=="TH"))
		endElem=event.srcElement.parentElement.parentElement.parentElement;
	else
		endElem=event.srcElement;
	if(!(endElem.id)||endElem.id.indexOf('_mainDataTable')<0){return true;}
	
	var lang = new Object();
	lang["exportExcel"]		= "导出到Excel..."
	lang["UIMenuWidth"]				= 150
	var menuStyle = "<head><link href='/base/themes/blue/menuarea.css' type='text/css' rel='stylesheet'></head><body scroll='no' onConTextMenu='event.returnValue=false;'>";
	var sMenu1 = "<TABLE border=0 cellpadding=0 cellspacing=0 class=Menu width="+lang["UIMenuWidth"]+"><tr><td width=18 valign=bottom align=center><\/td><td width="+(lang["UIMenuWidth"]-18)+" class=RightBg><TABLE border=0 cellpadding=0 cellspacing=0>";
	var sMenu2 = "<\/TABLE><\/td><\/tr><\/TABLE>";
	var s_MenuRow = "<tr><td align=center valign=middle><TABLE border=0 cellpadding=0 cellspacing=0 width="+(lang["UIMenuWidth"]-18)+"><tr ><td valign=middle height=20 class=MouseOut onMouseOver=this.className='MouseOver'; onMouseOut=this.className='MouseOut';";
	s_MenuRow += " onclick='parent.table_exportToExcel();parent.oPopupMenu.hide();'>";
	s_MenuRow += lang["exportExcel"]+"<\/td><\/tr><\/TABLE><\/td><\/tr>";
	var aheight = 24;
	var lefter = event.clientX;
	var topper = event.clientY;
	var awidth = lang["UIMenuWidth"];
	
	event.cancelBubble   =   true   
  event.returnValue   =   false;   
  oPopupMenu=window.createPopup();
  var oPopDocument = oPopupMenu.document;
  oPopDocument.open();
	oPopDocument.write(menuStyle + sMenu1+s_MenuRow+sMenu2);
	oPopDocument.close();
	if(lefter+awidth > document.body.clientWidth) lefter=lefter-awidth;
	oPopupMenu.show(lefter, topper, awidth, aheight, document.body);
  return   false;  	
}
function table_exportToExcel(){

	if(topwindow.activex_lib==null){
		alert("请安装望海组件");
		return false;
	}
	var oh = endElem.outerHTML.replace(/<TH.*<INPUT.*<\/TH>/gi,"").replace(/<TD.*<\/INPUT>.*<\/TD>/gi,"").replace(/text-decoration.*none/gi,"").replace(/<TH.*: none.*<\/TH>/gi,"").replace(/<TD.*:[ ]{0,1}none.*<\/TD>/gi,"");
	oh = oh.replace(/<A/g, "<span ").replace(/<\/A>/g, "</span>"); //去掉链接
	topwindow.activex_lib.ExportExcel(oh);

}
document.oncontextmenu=noContextMenu;

var oWin = self.top;

while ( oWin.window.dialogArguments != undefined )
{
	oWin = oWin.window.dialogArguments;
	if( oWin.top )
		oWin = oWin.top;
}
topwindow = oWin;

if ( topwindow.activex_lib == undefined ) {
	document.write('<OBJECT id="activex_lib"  style="display:none" CLASSID="CLSID:62C27227-C64C-44ED-B184-E9B54D582A59" CODEBASE="base/ActiveX/vhControlTableB.CAB#version=3.0.0.109" WIDTH="1" HEIGHT="1"><param name="WebURL" value="undefined"><param name="TableID" value="activex_lib"><param name="VHActionExt" value=".hbviewhigh"></OBJECT>');
	//document.write('<OBJECT id="activex_lib"  style="display:none" CLASSID="CLSID:D70A8E49-41AF-42C1-9886-FFAE78B9ED64" CODEBASE="base/ActiveX/vhControlTableB.CAB#version=3.0.0.94" WIDTH="1" HEIGHT="1"><param name="WebURL" value="undefined"><param name="TableID" value="activex_lib"><param name="VHActionExt" value=".hbviewhigh"></OBJECT>');
}

	function setPageEditStatus(obj, table, edit)
	{
		if ( edit == undefined )
		{
			table.TableEditEnable = !table.TableEditEnable;
		} else
		{
			table.TableEditEnable = edit;
		}
		obj.innerText = table.TableEditEnable?"非编辑状态(C)":"编辑状态(C)";
	}


//////////////////////////////////////
function jspReload(){
	if(!window.top.window.cbcs_year1 || window.top.window.cbcs_year1.length!=4){
		var p=getValuePairBySql("sys_get_year_month","<key>cbcs_year1</key>","?isCheck=false");
    //	str = window.xmlhttp._object.responseText;
    	if (p!=null) {
	    	window.top.window.cbcs_year1 = p[0];
	    }else{
	    	window.top.window.cbcs_year1 = ""
	    }
    }
   if(!window.top.window.cbcs_year2 || window.top.window.cbcs_year2.length!=4){
    	var p=getValuePairBySql("sys_get_year_month","<key>cbcs_year2</key>","?isCheck=false");
    	if (p!=null) {
	    	window.top.window.cbcs_year2 = p[0]
	    }else{
	    	window.top.window.cbcs_year2 = ""
	    }
    }

    if(!window.top.window.cbcs_month1 || window.top.window.cbcs_month1.length!=2){
    	var p=getValuePairBySql("sys_get_year_month","<key>cbcs_month1</key>","?isCheck=false");
    	if (p!=null) {
	    	window.top.window.cbcs_month1 = p[0]
	    }else{
	    	window.top.window.cbcs_month1 = ""
	    }
    }

    if(!window.top.window.cbcs_month2 || window.top.window.cbcs_month2.length!=2){
    	var p=getValuePairBySql("sys_get_year_month","<key>cbcs_month2</key>","?isCheck=false");
    	if (p!=null) {
	    	window.top.window.cbcs_month2 = p[0]
	    }else{
	    	window.top.window.cbcs_month2 = ""
	    }
    }
}
jspReload();

function getCBCS_Year1(){
  return window.top.window.cbcs_year1;
}
function getCBCS_Year2(){
  return window.top.window.cbcs_year2;
}
function getCBCS_Month1(){
  return window.top.window.cbcs_month1;
}
function getCBCS_Month2(){
  return window.top.window.cbcs_month2;
}
function setCBCS_Year1(cbcs_year1){
	if(window.top.window.cbcs_year1!=cbcs_year1){
		window.top.window.cbcs_year1 = cbcs_year1;
		window.xmlhttp.post("sys_set_year_month","<key>cbcs_year1</key><value>"+window.top.window.cbcs_year1+"</value>","?isCheck=false");
	}
}
function setCBCS_Year2(cbcs_year2){
	if(window.top.window.cbcs_year2!=cbcs_year2){
		window.top.window.cbcs_year2 = cbcs_year2;
		window.xmlhttp.post("sys_set_year_month","<key>cbcs_year2</key><value>"+window.top.window.cbcs_year2+"</value>","?isCheck=false");
	}
}
function setCBCS_Month1(cbcs_month1){
	if(window.top.window.cbcs_month1!=cbcs_month1){
		window.top.window.cbcs_month1 = cbcs_month1;
		window.xmlhttp.post("sys_set_year_month","<key>cbcs_month1</key><value>"+window.top.window.cbcs_month1+"</value>","?isCheck=false");
	}
}
function setCBCS_Month2(cbcs_month2){
	if(window.top.window.cbcs_month2!=cbcs_month2){
		window.top.window.cbcs_month2 = cbcs_month2;
		window.xmlhttp.post("sys_set_year_month","<key>cbcs_month2</key><value>"+window.top.window.cbcs_month2+"</value>","?isCheck=false");
	}
}
/////////////////////////////
////Cell Print BEGIN d35f169cd160b4b3
function printXmlToCellByXsltFile(xmlData,xslFile,setData,dirPrint,isPrview,pageUrl){

	var vXsl = new ActiveXObject("Microsoft.XMLDOM");
	vXsl.async=false;
	vXsl.load(xslFile)

	var vXml = new ActiveXObject("Microsoft.XMLDOM");
	vXml.async=false;
	vXml.loadXML(xmlData);

	if(setData){
  	var endRow="<annex>";
  	for(var o in setData){
  		endRow+="<"+o+">"+setData[o]+"</"+o+">"
  	}
  	endRow+="</annex>";
  	var vXmlEndRow = new ActiveXObject("Microsoft.XMLDOM");
    vXmlEndRow.async=false;
    vXmlEndRow.loadXML(endRow);

    if(xmlData != ""){
    	vXml.selectNodes("/root")[0].appendChild(vXmlEndRow.selectNodes("/annex")[0]);
    }
  }

	printStringDataToCell(vXml.transformNode(vXsl),setData,dirPrint,isPrview,pageUrl);
}
function printStringDataToCell(data,setData,dirPrint,isPrview,pageUrl){
	var cellXml = new ActiveXObject("Microsoft.XMLDOM");
	cellXml.async=false;
	cellXml.loadXML(data);
	printXmlDataToCell(cellXml,setData,dirPrint,isPrview,pageUrl);
}
var _printXmlDataToCell_Data_=null;
var _printXmlDataToCell_Param_=null;
var _printXmlDataToCell_Dir_=null;
var _printXmlDataToCell_Prview_=null;
var _printXmlDataToCell_PageUrl_=null;
var _printXmlDataToCell_Emp_=getEmpName();
var _printXmlDataToCell_Init=false;
function printXmlDataToCell(data,setData,dirPrint,isPrview,pageUrl,openFrame){
	var win_prefix=window.prefix;
	var screenHeight=window.screen.availHeight;
	var screenWidth=window.screen.availWidth;
	var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:auto";
	if(setData!=null){
		var ks;
		for(var o in setData){
			ks=o.split("_");
			if(ks.length!=3)
				continue;
			if(ks[0]!="thead"&&ks[0]!="tbody"&&ks[0]!="tfoot")
				continue;
			var segs=data.getElementsByTagName(ks[0]);
			if(segs.length==0)
				continue;
			var trs=segs[0].getElementsByTagName("tr");
			if(trs.length==0||trs.length<parseInt(ks[1]-1,10))
				continue;
			var tds=trs[ks[1]-1].getElementsByTagName("td");
			if(tds.length==0||tds.length<parseInt(ks[2]-1,10))
				continue;
			tds[ks[2]-1].text=setData[o];
		}
	}
	_printXmlDataToCell_Data_=data;
	_printXmlDataToCell_Param_=setData;
	_printXmlDataToCell_Dir_=dirPrint;
	_printXmlDataToCell_Prview_=isPrview;

	if(openFrame==null||openFrame=='ModalDialog'){
		_printXmlDataToCell_Init=true;
		
		var win=window.showModalDialog(win_prefix+"../base/print1/printh2c.html",window,dialogStyle);

	}else if(openFrame=='_new'){
		_printXmlDataToCell_Init=true;
		var win=window.open(win_prefix+"../base/print1/printh2c.html",'New');

	}else{
		_printXmlDataToCell_Init=false;
		//var f=eval(openFrame);
		//f.document.location.href=	win_prefix+"base/print1/printh2c.html";
	}
}
////Cell Print END 3ff51ca139496acb
function clearIFdlgTag(u){
	return _clearIFdlgTag(u);
}
function _clearIFdlgTag(u){
	if(window.__ifdlgtag){
		if(u.indexOf("&"+window.__ifdlgtag+"=")>0)
			u=u.substr(0,u.indexOf("&"+window.__ifdlgtag+"="));
	}
	return u;
}


var modeCodeToNameList = {
"00":"sys",               
"01":"uinfo",             
"02":"acct",              
"03":"rep",               
"04":"mate",              
"05":"equi",              
"06":"drug",              
"07":"pote",              
"08":"wage",              
"09":"budg",              
"10":"payctl",            
"11":"ven",               
"12":"cbcs",              
"13":"perf",              
"14":"bonus",             
"15":"hisc",
"16":"dev",  
"17":"imma",            
"20":"stat",
"21":"hr" ,
"25":"pact"         
};                        
                          
function getModCode(modValue){
	var mod_code="";        
	if(modValue==null || modValue=="" || typeof(modValue)!="string"){
		return "";
	}
	for(var key in modeCodeToNameList){
		if(modeCodeToNameList[key]==modValue){
			mod_code=key;
			break;
		}
	}

	return mod_code;
}

 /*
{"name":"","type":"text,select,checkbox,radio,checktype","para":"","afterFun":"fun"}
*/
function QuerySchema(listName,conf,inDlg,textName){
	var p=typeof(window.dialogArguments)=="undefined"?parent:window.dialogArguments;
	if(inDlg)
		this.settingKey=p.location.href;
	else
		this.settingKey=window.location.href;
	if(this.settingKey.indexOf("/hbos")>=0)
		this.settingKey=this.settingKey.substr(this.settingKey.indexOf("/hbos")+5);
	this.settingKey="QS"+this.settingKey;
	this.settingKey=this.settingKey.substr(0,this.settingKey.indexOf("."));
	this.settingKey=this.settingKey.replace(/\//g,"");
	this.settingKey=this.settingKey.replace(/\./g,"");
	this.conf=conf;
	this.schemaListName=listName;
	this.schemaName=textName;
	this.save=function(name){
		var str="<action>save</action><schemaPath>"+this.settingKey+"</schemaPath><schemaName>"+this.encode(name)+"</schemaName><schemaDef>"+this.encode("<root>"+this.getQueryData()+"</root>")+"</schemaDef>";
		window.xmlhttp.post("globalQuerySchema",str,"");
		var estr = window.xmlhttp._object.responseText;
		if(estr.indexOf("<error>")>0){
			estr=estr.substr(estr.indexOf("<error>")+7);
			estr=estr.substr(0,estr.indexOf("</error>"));
			return false;
		}
		alert("保存成功!");
		return true;
	}
	this.load=function(name){
		window.xmlhttp.post("globalQuerySchema","<action>load</action><schemaPath>"+this.settingKey+"</schemaPath><schemaName>"+name+"</schemaName><schemaDef></schemaDef>","");
		var doc = window.xmlhttp._object.responseXML;
		if(doc.documentElement==null)
			return;
		if(window.xmlhttp._object.responseText.indexOf("<td>")<0)
			return;
		var x = window.xmlhttp._object.responseText.substring(window.xmlhttp._object.responseText.search(/<td>/)+"<td>".length, window.xmlhttp._object.responseText.search(/<\/td>/))
		//var tds=doc.getElementsByTagName("td");
		//if(tds.length==0)
		//	return;
		//var x=this.decode(tds[0].text);
		x=this.decode(x);
		var dom=new ActiveXObject("Microsoft.XMLDOM");
		dom.loadXML(x);
		var cn=dom.documentElement.childNodes;
		for(var i=0;i<cn.length;i++){
			if(cn[i].nodeType!=1)
				continue;
			var v="";
			if(cn[i].firstChild!=null)
				v=cn[i].firstChild.nodeValue;
			v=this.decode(v);
			eval("this._setvalue_"+this._getConf(cn[i].nodeName)["type"]+"(this._getConf('"+cn[i].nodeName+"'),"+cn[i].nodeName+",\""+v+"\")");
			if(this._getConf(cn[i].nodeName)["afterFun"])
				eval(this._getConf(cn[i].nodeName)["afterFun"]+"(this._getConf('"+cn[i].nodeName+"'))");
		}
	}
	this.del=function(){
		var obj=eval(this.schemaListName);
		var n=obj.value;
		if(n.length>0)
			n=n.substr(1,n.length-2);
		else{
			alert("请选择!");
			return false;
		}
		var str="<action>del</action><schemaPath>"+this.settingKey+"</schemaPath><schemaName>"+this.encode(n)+"</schemaName><schemaDef>sd</schemaDef>";
		window.xmlhttp.post("globalQuerySchema",str,"");
		var estr = window.xmlhttp._object.responseText;
		if(estr.indexOf("<error>")>0){
			estr=estr.substr(estr.indexOf("<error>")+7);
			estr=estr.substr(0,estr.indexOf("</error>"));
			alert(estr);
			return false;
		}
		return true;
	}
	this._getConf=function(n){
		for(var i=0;i<this.conf.length;i++){
			if(this.conf[i]["name"]==n)
				return this.conf[i];
		}
	}
	this.getQueryData=function(){
		var str="";
		for(var i=0;i<this.conf.length;i++){
			c=this.conf[i];
			str+="<"+c["name"]+">"+this.encode(eval("this._getvalue_"+c["type"]+"(this.conf['"+i+"'],"+c["name"]+")"))+"</"+c["name"]+">";
		}
		return str;
	}
	this.refreshSchemaList=function(){
		obj=eval(this.schemaListName);
		obj.para="<pathCode>"+this.settingKey+"</pathCode><b/><c/><d/>";
		obj.refresh();
		qc=this;
		obj.onchange=function(){
			v=""
			if(this.value.length>0){
				v=this.value.substr(1,this.value.length-2);
			}
			eval(qc.schemaName).value=v;
			qc.load(v);
		}
	}
	this.selFirst=function(){
		obj=eval(this.schemaListName);
		if(obj.firstCode){
			obj.setValue("'"+obj.firstCode+"'");
			this.load(obj.firstCode);
			var no=eval(this.schemaName);
			no.value=obj.firstCode;
		}
	}
	this.encode=function(s){
		s=s.replace(/&/g,"&amp;");
		s=s.replace(/</g,"&lt;");
		s=s.replace(/>/g,"&gt;");
		return s;
	}
	this.decode=function(s){
		s=s.replace(/&amp;/g,"&");
		s=s.replace(/&gt;/g,">");
		s=s.replace(/&lt;/g,"<");
		return s;
	}
	this._getvalue_select=function(conf,obj){
		return obj.value;
	}
	this._setvalue_select=function(conf,obj,v){
		obj.setValue(v);
		if(conf["para"]){
			obj.para=eval(conf["para"]);
			obj.refresh();
		}
	}
	this._getvalue_selects=function(conf,obj){
		return obj.value+"|||"+obj.text;
	}
	this._setvalue_selects=function(conf,obj,v){
		obj.setValue(v);
		if(conf["para"]){
			obj.para=eval(conf["para"]);
			obj.refresh();
		}
	}
	this._getvalue_text=function(conf,obj){
		return obj.value;
	}
	this._setvalue_text=function(conf,obj,v){
		obj.value=v;
	}
	this._getvalue_checkbox=function(conf,obj){
		return obj.value;
	}
	this._setvalue_checkbox=function(conf,obj,v){
		obj.setValue(v);
	}
	this._getvalue_radio=function(conf,obj){
		return obj.value;
	}
	this._setvalue_radio=function(conf,obj,v){
		obj.setValue(v);
	}
	this._getvalue_checktype=function(conf,obj){
		return this.encode(obj.GetCurrentAllData());
	}
	this._setvalue_checktype=function(conf,obj,v){
		//alert(v);
		obj.SetTableFromXML(v);
	}

}


function getReportPathCode(mod_code){
	var r=getValuePairBySql("rep_getReportPathCode","<mod_code>"+mod_code+"</mod_code>");
	if(r==null){
		return "";
	}else{
		return r[0];
	}
}

function getLinkTwoModuleStatus(mod_code1,mod_code2){
	var r=getValuePairBySql("sysGetLinkTwoModule","<mod_code1>"+mod_code1+"</mod_code1><mod_code2>"+mod_code2+"</mod_code2>");
	if(r==null){
		return false;
	}else{
		if(r[0] == "1")
			return true;
		else
			return false
	}
}
function getJsGuid(){
	var d=new Date();
	var s="";
	for(var i=0;i<6;i++)
		s+=parseInt(Math.random()*10000);
	return "guid"+d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds()+s;
}
function decodeXmlChar(s){
	s=s.replace(/&quot;/g,"\"");
	s=s.replace(/&amp;/g,"&");
	s=s.replace(/&gt;/g,">");
	s=s.replace(/&lt;/g,"<");
	return s;
}
function encodeXmlChar(s){
	s=s.replace(/&/g,"&amp;");
	s=s.replace(/>/g,"&gt;");
	s=s.replace(/</g,"&lt;");
	return s;
}
/////////////////////////////////  fusion chart begin ///////////////////////////////////////////////////////////////////////////////////

var FusionChartsSwfMap={
		"bubble":"Bubble.swf"
	};

if(typeof infosoftglobal == "undefined") var infosoftglobal = new Object();
if(typeof infosoftglobal.FusionChartsUtil == "undefined") infosoftglobal.FusionChartsUtil = new Object();
infosoftglobal.FusionCharts = function(swf, id, w, h, debugMode, registerWithJS, c, scaleMode, lang, detectFlashVersion, autoInstallRedirect){
	if (!document.getElementById) { return; }
	
	//Flag to see whether data has been set initially
	this.initialDataSet = false;
	
	//Create container objects
	this.params = new Object();
	this.variables = new Object();
	this.attributes = new Array();
	
	//Set attributes for the SWF
	if(swf) { this.setAttribute('swf', window.prefix+"../base/scripts/swfcharts/"+FusionChartsSwfMap[swf]); }
	if(id) { this.setAttribute('id', id); }
	if(w) { this.setAttribute('width', w); }
	if(h) { this.setAttribute('height', h); }
	
	//Set background color
	if(c) { this.addParam('bgcolor', c); }
	
	//Set Quality	
	this.addParam('quality', 'high');
	
	//Add scripting access parameter
	this.addParam('allowScriptAccess', 'always');
	
	//Pass width and height to be appended as chartWidth and chartHeight
	this.addVariable('chartWidth', w);
	this.addVariable('chartHeight', h);

	//Whether in debug mode
	debugMode = debugMode ? debugMode : 0;
	this.addVariable('debugMode', debugMode);
	//Pass DOM ID to Chart
	this.addVariable('DOMId', id);
	//Whether to registed with JavaScript
	registerWithJS = registerWithJS ? registerWithJS : 0;
	this.addVariable('registerWithJS', registerWithJS);
	
	//Scale Mode of chart
	scaleMode = scaleMode ? scaleMode : 'noScale';
	this.addVariable('scaleMode', scaleMode);
	//Application Message Language
	lang = lang ? lang : 'EN';
	this.addVariable('lang', lang);
	
	//Whether to auto detect and re-direct to Flash Player installation
	this.detectFlashVersion = detectFlashVersion?detectFlashVersion:1;
	this.autoInstallRedirect = autoInstallRedirect?autoInstallRedirect:1;
	
	//Ger Flash Player version 
	this.installedVer = infosoftglobal.FusionChartsUtil.getPlayerVersion();
	
	if (!window.opera && document.all && this.installedVer.major > 7) {
		// Only add the onunload cleanup if the Flash Player version supports External Interface and we are in IE
		infosoftglobal.FusionCharts.doPrepUnload = true;
	}
}

infosoftglobal.FusionCharts.prototype = {
	setAttribute: function(name, value){
		this.attributes[name] = value;
	},
	getAttribute: function(name){
		return this.attributes[name];
	},
	addParam: function(name, value){
		this.params[name] = value;
	},
	getParams: function(){
		return this.params;
	},
	addVariable: function(name, value){
		this.variables[name] = value;
	},
	getVariable: function(name){
		return this.variables[name];
	},
	getVariables: function(){
		return this.variables;
	},
	getVariablePairs: function(){
		var variablePairs = new Array();
		var key;
		var variables = this.getVariables();
		for(key in variables){
			variablePairs.push(key +"="+ variables[key]);
		}
		return variablePairs;
	},
	getSWFHTML: function() {
		var swfNode = "";
		if (navigator.plugins && navigator.mimeTypes && navigator.mimeTypes.length) { 
			// netscape plugin architecture			
			swfNode = '<embed type="application/x-shockwave-flash" src="'+ this.getAttribute('swf') +'" width="'+ this.getAttribute('width') +'" height="'+ this.getAttribute('height') +'"  ';
			swfNode += ' id="'+ this.getAttribute('id') +'" name="'+ this.getAttribute('id') +'" ';
			var params = this.getParams();
			 for(var key in params){ swfNode += [key] +'="'+ params[key] +'" '; }
			var pairs = this.getVariablePairs().join("&");
			 if (pairs.length > 0){ swfNode += 'flashvars="'+ pairs +'"'; }
			swfNode += '/>';
		} else { // PC IE			
			swfNode = '<object id="'+ this.getAttribute('id') +'" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="'+ this.getAttribute('width') +'" height="'+ this.getAttribute('height') +'">';
			swfNode += '<param name="movie" value="'+ this.getAttribute('swf') +'" />';
			var params = this.getParams();
			for(var key in params) {
			 swfNode += '<param name="'+ key +'" value="'+ params[key] +'" />';
			}
			var pairs = this.getVariablePairs().join("&");		
			if(pairs.length > 0) {swfNode += '<param name="flashvars" value="'+ pairs +'" />';}
			swfNode += "</object>";
		}
		return swfNode;
	},
	setDataURL: function(strDataURL){
		//This method sets the data URL for the chart.
		//If being set initially
		if (this.initialDataSet==false){
			this.addVariable('dataURL',strDataURL);
			//Update flag
			this.initialDataSet = true;
		}else{
			//Else, we update the chart data using External Interface
			//Get reference to chart object
			var chartObj = infosoftglobal.FusionChartsUtil.getChartObject(this.getAttribute('id'));
			chartObj.setDataURL(strDataURL);
		}
	},
	setDataXML: function(strDataXML){
		//If being set initially
		if (this.initialDataSet==false){
			//This method sets the data XML for the chart INITIALLY.
			this.addVariable('dataXML',strDataXML);
			//Update flag
			this.initialDataSet = true;
		}else{
			//Else, we update the chart data using External Interface
			//Get reference to chart object
			var chartObj = infosoftglobal.FusionChartsUtil.getChartObject(this.getAttribute('id'));
			chartObj.setDataXML(strDataXML);
		}
	},
	render: function(elementId){
		//First check for installed version of Flash Player - we need a minimum of 8
		if((this.detectFlashVersion==1) && (this.installedVer.major < 8)){
			if (this.autoInstallRedirect==1){
				//If we can auto redirect to install the player?
				var installationConfirm = window.confirm("You need Adobe Flash Player 8 (or above) to view the charts. It is a free and lightweight installation from Adobe.com. Please click on Ok to install the same.");
				if (installationConfirm){
					window.location = "http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash";
				}else{
					return false;
				}
			}else{
				//Else, do not take an action. It means the developer has specified a message in the DIV (and probably a link).
				//So, expect the developers to provide a course of way to their end users.
				//window.alert("You need Adobe Flash Player 8 (or above) to view the charts. It is a free and lightweight installation from Adobe.com. ");
				return false;
			}			
		}else{
			//Render the chart
			var n = (typeof elementId == 'string') ? document.getElementById(elementId) : elementId;
			n.innerHTML = this.getSWFHTML();
			return true;		
		}
	},
	setAutoSize: function(a){
		
	}
}

/* ---- detection functions ---- */
infosoftglobal.FusionChartsUtil.getPlayerVersion = function(){
	var PlayerVersion = new infosoftglobal.PlayerVersion([0,0,0]);
	if(navigator.plugins && navigator.mimeTypes.length){
		var x = navigator.plugins["Shockwave Flash"];
		if(x && x.description) {
			PlayerVersion = new infosoftglobal.PlayerVersion(x.description.replace(/([a-zA-Z]|\s)+/, "").replace(/(\s+r|\s+b[0-9]+)/, ".").split("."));
		}
	}else if (navigator.userAgent && navigator.userAgent.indexOf("Windows CE") >= 0){ 
		//If Windows CE
		var axo = 1;
		var counter = 3;
		while(axo) {
			try {
				counter++;
				axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash."+ counter);
				PlayerVersion = new infosoftglobal.PlayerVersion([counter,0,0]);
			} catch (e) {
				axo = null;
			}
		}
	} else { 
		// Win IE (non mobile)
		// Do minor version lookup in IE, but avoid Flash Player 6 crashing issues
		try{
			var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
		}catch(e){
			try {
				var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
				PlayerVersion = new infosoftglobal.PlayerVersion([6,0,21]);
				axo.AllowScriptAccess = "always"; // error if player version < 6.0.47 (thanks to Michael Williams @ Adobe for this code)
			} catch(e) {
				if (PlayerVersion.major == 6) {
					return PlayerVersion;
				}
			}
			try {
				axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
			} catch(e) {}
		}
		if (axo != null) {
			PlayerVersion = new infosoftglobal.PlayerVersion(axo.GetVariable("$version").split(" ")[1].split(","));
		}
	}
	return PlayerVersion;
}
infosoftglobal.PlayerVersion = function(arrVersion){
	this.major = arrVersion[0] != null ? parseInt(arrVersion[0]) : 0;
	this.minor = arrVersion[1] != null ? parseInt(arrVersion[1]) : 0;
	this.rev = arrVersion[2] != null ? parseInt(arrVersion[2]) : 0;
}
// ------------ Fix for Out of Memory Bug in IE in FP9 ---------------//
/* Fix for video streaming bug */
infosoftglobal.FusionChartsUtil.cleanupSWFs = function() {
	var objects = document.getElementsByTagName("OBJECT");
	for (var i = objects.length - 1; i >= 0; i--) {
		objects[i].style.display = 'none';
		for (var x in objects[i]) {
			if (typeof objects[i][x] == 'function') {
				objects[i][x] = function(){};
			}
		}
	}
}
// Fixes bug in fp9
if (infosoftglobal.FusionCharts.doPrepUnload) {
	if (!infosoftglobal.unloadSet) {
		infosoftglobal.FusionChartsUtil.prepUnload = function() {
			__flash_unloadHandler = function(){};
			__flash_savedUnloadHandler = function(){};
			window.attachEvent("onunload", infosoftglobal.FusionChartsUtil.cleanupSWFs);
		}
		window.attachEvent("onbeforeunload", infosoftglobal.FusionChartsUtil.prepUnload);
		infosoftglobal.unloadSet = true;
	}
}
/* Add document.getElementById if needed (mobile IE < 5) */
if (!document.getElementById && document.all) { document.getElementById = function(id) { return document.all[id]; }}
/* Add Array.push if needed (ie5) */
if (Array.prototype.push == null) { Array.prototype.push = function(item) { this[this.length] = item; return this.length; }}

/* Function to return Flash Object from ID */
infosoftglobal.FusionChartsUtil.getChartObject = function(id)
{
  if (window.document[id]) {
      return window.document[id];
  }
  if (navigator.appName.indexOf("Microsoft Internet")==-1) {
    if (document.embeds && document.embeds[id])
      return document.embeds[id]; 
  } else {
    return document.getElementById(id);
  }
}
/* Aliases for easy usage */
var getChartFromId = infosoftglobal.FusionChartsUtil.getChartObject;
var FusionCharts = infosoftglobal.FusionCharts;
function openFusionCharts(kind,data,w,h){
	var wi=800;
	if(w)
		wi=w;
	var hg=600;
	if(h)
		hg=h;
	openDialog(window.prefix+"../base/scripts/swfcharts/chart.html?load=<type>"+kind+"</type><data>"+data+"</data>", "dialogWidth:"+wi+"px;dialogHeight:"+hg+"px");	
}
var getNextFusionChartColors=["003333","336633","666633","0066FF","3366FF","6666FF","996600","CC6600","FF6600","9966FF","CC66FF","FF66FF"];
var getNextFusionChartColori=0;
function getNextFusionChartColor(){
	var r=getNextFusionChartColors[getNextFusionChartColori++];
	if(getNextFusionChartColori>=getNextFusionChartColors.length){
		getNextFusionChartColori=0;	
	}
	return r;
}
/////////////////////////////////////// fusion chart end ///////////////////////////////////////////////////////////////////
function jhtcSetHtml(element,htmlStr){element.innerHTML=(htmlStr);};
function getDefaultPageSizeByMod(){
	var v="";
	if(v=="")
		v="0";
	return v;
}
function setVHTablePageSize(obj,size){
	var t=getDefaultPageSizeByMod();
	if(t=="0"){
		obj.PageSize=size;	
	}
	if(t=="1"){
		obj.PageSize=0;	
	}else{
		obj.PageSize=t;	
	}
}
////////////////////////////////
function VhFileUploader(frm){
	//_____VHFileCreatFromTimer=window.setInterval(this.__createForm,200);
	document.vhFileUploader=this;
	this.___callbackFun=null;
	this.___callbackObj=null;
	this.__has_submit=false;
	this.__vhFileUploadFrame=frm;
	this.__oldDocument=null;
};
VhFileUploader.prototype.clear=function(){
	var inps=this.__vhFileUploadFrame.document.getElementsByTagName("input");
	for(var i=0;i<inps.length;i++){
		if(inps[i].name.indexOf("upfile")==0)
			inps[i].parentNode.removeChild(inps[i]);	
	}
};
VhFileUploader.prototype.choose=function(indx){
	if(this.__oldDocument!=document)
		this.clear();
	this.__oldDocument=document;
	if(eval("typeof(this.__getForm().upfile"+indx+")")=="undefined"){
		var ft=this.__vhFileUploadFrame.document.createElement("<input type=file id='upfile"+indx+"' name='upfile"+indx+"'/>");
		this.__vhFileUploadFrame.document.getElementById("fileSpanId").appendChild(ft);
	}
	eval("this.__getForm().upfile"+indx+".click()");
	return eval("this.__getForm().upfile"+indx+".value");
};
VhFileUploader.prototype.submit=function(fun,obj){
	if(this.__has_submit==true)
		return ;
	if(obj){
		this.___callbackObj=obj;
		obj.disabled=true;
	}
	this.___callbackFun=fun;
	this.__has_submit=true;
	this.__getForm().submit();
};
VhFileUploader.prototype.__onload=function(message,files){
	this.__has_submit=false;
	if(this.___callbackFun==null)
		return;
	this.___callbackFun(message,files);
	if(this.___callbackObj!=null)
		this.___callbackObj.disabled=false;
	this.___callbackFun=null;
	this.___callbackObj=null;
};
VhFileUploader.prototype.__getForm=function(){
	return this.__vhFileUploadFrame.document.vhUploadForm;
};

//////////////VHFile END

function getIs_Start(code){
	var r=getValuePairBySql("select_is_start","<code>"+code+"</code>");
	if(r==null){
		return 0;
	}else{
		return r[0];
	}
}

function checkLink(mod_code){
	var p = getValuePairBySql("cbcs_check_link","<mod>" + mod_code + "</mod>");
	if(p!=null && p[1]!=null){
		if(p[1]=='yes')
			return true;	
	}else{
		return false;
	}
}
function encodeEscapeChar(s){
	s = s.replace(/\[/g, "[[]"); //此句一定要在最前
  s = s.replace(/\_/g, "[_]");
  s = s.replace(/%/g, "[%]");
  s = s.replace(/\\/g, "\\\\");
	return s;
}

function dataExportToTxt(sqlid,head,cols, paras){
    window.xmlhttp.post("ExportTxt","<func>" + sqlid+ "</func><head>"+head+"</head><cols>"+cols+"</cols>" + paras);

    var str = window.xmlhttp._object.responseText;
    if(window.doMsg(str)){
        open("/FileDownloadServlet?uri=" + str);
    }
}
