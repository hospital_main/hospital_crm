/*
 * $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/base/scripts/menu.js,v 1.1 2012/03/12 01:57:24 zhoulidong Exp $
 * $Author: zhoulidong $
 * $Date: 2012/03/12 01:57:24 $
 * $Modtime: $
 * $Revision: 1.1 $
 */

// 帮助系统共用变量
var _helpModule = "";
var _help1stMenu = "";
var _help2ndMenu = "";
var _help3rdMenu = "";


// 菜单共用变量
var _current_menu = new Array ();
var _1st_menu = new Array();
var _2nd_menu = new Array();
var _3rd_menu = new Array();

var _1st_current=-1, _2nd_current=-1, _3rd_current=-1;

var table = document.getElementById("_1st_menu");

function initMenu(module) {
	_helpModule = module;

  // 初始化_current_menu
  _current_menu.length=0;
  for (var i=0; i<_all_menu.length; i++) {
    if (_all_menu[i][0].indexOf(module)>=0) {
      _current_menu.length++;
      _current_menu[_current_menu.length-1]=_all_menu[i];
    }
  }

  // 一级菜单的基本点
  row = document.getElementById("_1st_base");
  // 初始化一级菜单
  while (row.rowIndex>0) {
    table.deleteRow(0);
  }
  // 初始化三级菜单
  row = document.getElementById("_3rd_menu_");
  while (row.hasChildNodes()) {
    row.deleteCell(0);
  }

  // 初始化一级菜单
  _1st_menu.length=0;

  // 取一级菜单
  for (var i=0; i<_current_menu.length; i++) {
    if (_current_menu[i][3]=="1") {
      _1st_menu.length++;
      _1st_menu[_1st_menu.length-1] = _current_menu[i];
    }
  }

  // 添加一级菜单
  for (var i=_1st_menu.length-1; i>=0; i--) {
    row = table.insertRow(0);
    row.setAttribute("id", _1st_menu[i][0]);

    col = document.createElement("<td class='1stMenuNoChose' id='1stMenu_"+i+"' onclick='drawMenu("+i+", 0, 0)'/>");
    row.appendChild(col);

    div = document.createElement("<div noWrap></div>");
    div.innerText = _1st_menu[i][1];
    col.appendChild(div);

    table.insertRow(0);
  }
}

function drawMenu(first, second, third) {
	_help1stMenu = "";
	_help2ndMenu = "";
	_help3rdMenu = "";

	// 一、二级菜单改变标志  true--改变
  change_flag = true;

  if (_1st_menu.length<=0) return;

  _help1stMenu = _1st_menu[first][0];

  // 初始化新二级菜单
  _2nd_menu.length=0;

  // 声明旧二级菜单
  var _old_2nd_menu = new Array();

  for (var i=0; i<_current_menu.length; i++) {
    if (_current_menu[i][3]!="2") continue;

    if (_current_menu[i][0].indexOf(_1st_menu[first][0]) >= 0) { // 取新二级菜单
      _2nd_menu.length++;
      _2nd_menu[_2nd_menu.length-1] = _current_menu[i];
    } else if (document.getElementById(_current_menu[i][0]) != null){ // 取旧二级菜单
      _old_2nd_menu.length++;
      _old_2nd_menu[_old_2nd_menu.length-1] = _current_menu[i];
    }
  }

  //循环次数
  var count=1;
  var _2nd_idx=_2nd_menu.length, _old_2nd_idx=_old_2nd_menu.length;
  var _old_last_2nd_menu;

  // 一级菜单是否改变
  if (_1st_current!=first) {
    if (_1st_current != -1) {
      document.all("1stMenu_"+parseInt(_1st_current)).className = "1stMenuNoChose";
    }
    document.all("1stMenu_"+parseInt(first)).className = "1stMenuChose";
    while (_2nd_idx+_old_2nd_idx>0) {
      // 删除旧二级菜单
      if (_old_2nd_idx>0) {
        if (_old_2nd_idx==1)
          _old_last_2nd_menu = document.getElementById(_old_2nd_menu[_old_2nd_menu.length-_old_2nd_idx][0]).nextSibling;

        table.deleteRow(document.getElementById(_old_2nd_menu[_old_2nd_menu.length-_old_2nd_idx][0]).rowIndex);
        _old_2nd_idx--;
      }

      // 添加二级菜单
      if (_2nd_idx>0) {
        row = table.insertRow(document.getElementById(_1st_menu[first][0]).rowIndex+_2nd_menu.length-_2nd_idx+1);
        row.setAttribute("id", _2nd_menu[_2nd_menu.length-_2nd_idx][0]);

        if (_2nd_menu.length-_2nd_idx == second) // 选中
          col = document.createElement("<td class='2ndMenuChose'/>");
        else // 未选中
          col = document.createElement("<td class='2ndMenuNoChose' onclick='drawMenu("+first+", "+(_2nd_menu.length-_2nd_idx)+", 0)' />");
        row.appendChild(col);

        div = document.createElement("<div noWrap></div>");
        div.innerText = _2nd_menu[_2nd_menu.length-_2nd_idx][1];
        col.appendChild(div);

        _2nd_idx--;
      }

      count++;
    }

    // 删除旧二级菜单的末尾
    if (_old_last_2nd_menu != null)
      table.deleteRow(_old_last_2nd_menu.rowIndex);

    // 添加二级菜单的末尾
    row = table.insertRow(document.getElementById(_2nd_menu[_2nd_menu.length-1][0]).rowIndex+1);
    col = document.createElement("<td class='2ndMenuBottom'/>");
    row.appendChild(col);

    _1st_current=first;
    _2nd_current=second;
  } else {
    if (_2nd_current!=second) {
      row = document.getElementById(_2nd_menu[_2nd_current][0]);
      row.deleteCell(0);
      col = document.createElement("<td class='2ndMenuNoChose' onclick='drawMenu("+first+", "+_2nd_current+", 0)' />");
      div = document.createElement("<div noWrap></div>");
      div.innerText = _2nd_menu[_2nd_current][1];
      col.appendChild(div);
      row.appendChild(col);


      row = document.getElementById(_2nd_menu[second][0]);
      row.deleteCell(0);
      col = document.createElement("<td class='2ndMenuChose'/>");
      div = document.createElement("<div noWrap></div>");
      div.innerText = _2nd_menu[second][1];
      col.appendChild(div);
      row.appendChild(col);

      _2nd_current=second;
    } else
      change_flag = false;
  }

  if (_2nd_menu.length<=0) return;

  _help2ndMenu = _2nd_menu[second][0];

  // 显示三级菜单
  document.getElementById("_3rd_menu_head_").style.display=''

  // 初始化三级菜单
  _3rd_menu.length=0;

  // 声明旧三级菜单
  var _old_3rd_menu = new Array();

  for (var i=0; i<_current_menu.length; i++) {
    if (_current_menu[i][3]!="3") continue;

    if (_current_menu[i][0].indexOf(_2nd_menu[second][0]) >= 0) { // 取新三级菜单
      _3rd_menu.length++;
      _3rd_menu[_3rd_menu.length-1] = _current_menu[i];
    } else if (document.getElementById(_current_menu[i][0]) != null){ // 取旧三级菜单
      _old_3rd_menu.length++;
      _old_3rd_menu[_old_3rd_menu.length-1] = _current_menu[i];
    }
  }

  // 引用三级菜单
  _3rdRow = document.getElementById("_3rd_menu_");

  //循环次数
  count=1;
  var _3rd_idx=_3rd_menu.length, _old_3rd_idx=_old_3rd_menu.length;
  var _old_last_3rd_menu = null;

  // 一、二级菜单是否改变
  if (change_flag) {
    while (_3rd_idx+_old_3rd_idx>0) {
      // 删除旧三级菜单
      if (_old_3rd_idx>0) {
        _old_last_3rd_menu = document.getElementById(_old_3rd_menu[_old_3rd_menu.length-_old_3rd_idx][0]).nextSibling.nextSibling;

        _3rdRow.deleteCell(document.getElementById(_old_3rd_menu[_old_3rd_menu.length-_old_3rd_idx][0]).cellIndex-1);
        _3rdRow.deleteCell(document.getElementById(_old_3rd_menu[_old_3rd_menu.length-_old_3rd_idx][0]).cellIndex+1);
        _3rdRow.deleteCell(document.getElementById(_old_3rd_menu[_old_3rd_menu.length-_old_3rd_idx][0]).cellIndex);
        _old_3rd_idx--;
      }

      // 添加三级菜单
      if (_3rd_idx>0) {
        var head, end;

        if (_3rd_menu.length-_3rd_idx == third) // 选中
          head = document.createElement("<img src='base/themes/blue/images/3rdMenuChoseHeadBg.gif' />");
        else // 未选中
          head = document.createElement("<img src='base/themes/blue/images/3rdMenuNoChoseHeadBg.gif' />");

        if (_3rd_menu.length-_3rd_idx == third) // 选中
          col = document.createElement("<td class='3rdMenuChoseBg' />");
        else // 未选中
          col = document.createElement("<td class='3rdMenuNoChoseBg' onclick='drawMenu("+first+", "+second+", "+(_3rd_menu.length-_3rd_idx)+")'/>");
        col.setAttribute("id", _3rd_menu[_3rd_menu.length-_3rd_idx][0]);
        div = document.createElement("<div noWrap></div>");
        div.innerText = "  "+_3rd_menu[_3rd_menu.length-_3rd_idx][1]+"  ";
        col.appendChild(div);

        if (_3rd_menu.length-_3rd_idx == third) // 选中
          end = document.createElement("<img src='base/themes/blue/images/3rdMenuChoseEndBg.gif' />");
        else // 未选中
          end = document.createElement("<img src='base/themes/blue/images/3rdMenuNoChoseEndBg.gif' />");

        tdHead = document.createElement("<td/>");
        tdHead.appendChild(head);
        tdEnd  = document.createElement("<td/>");
        tdEnd.appendChild(end);

        if (_old_last_3rd_menu!=null) { // 存在旧三级菜单
          _old_last_3rd_menu.parentNode.insertBefore(tdHead, _old_last_3rd_menu);
          _old_last_3rd_menu.parentNode.insertBefore(col, _old_last_3rd_menu);
          _old_last_3rd_menu.parentNode.insertBefore(tdEnd, _old_last_3rd_menu);
        } else { // 不存在旧三级菜单
          _3rdRow.appendChild(tdHead);
          _3rdRow.appendChild(col);
          _3rdRow.appendChild(tdEnd);
        }

        _3rd_idx--;
      }

      count++;
    }

    // 删除旧三级菜单的末尾
    if (_old_last_3rd_menu!=null)
      _3rdRow.removeChild(_old_last_3rd_menu);

    if (_3rd_menu.length<=0) {
      document.getElementById("_3rd_menu_head_").style.display='none'
      _3rd_current=-1;
    } else {
      // 添加三级菜单的末尾
      col = document.createElement("<td class='3rdMenuEndBg'/>");
      div = document.createElement("<div noWrap></div>");
      col.appendChild(div);
      _3rdRow.appendChild(col);

      _3rd_current=third;
    }
  } else {
    if (_3rd_current!=third) {
      if (_3rd_current>=0 && _3rd_current<_3rd_menu.length && document.getElementById(_3rd_menu[_3rd_current][0])!=null) {
        // 以前选中的菜单
        col=document.getElementById(_3rd_menu[_3rd_current][0]);
        row = col.parentNode;

        newCol = document.createElement("<td class='3rdMenuNoChoseBg' onclick='drawMenu("+first+", "+second+", "+_3rd_current+")'/>");
        newCol.setAttribute("id", _3rd_menu[_3rd_current][0]);
        div = document.createElement("<div noWrap></div>");
        div.innerText = "  "+_3rd_menu[_3rd_current][1]+"  ";
        newCol.appendChild(div);
        row.replaceChild(newCol, col);

        row.deleteCell(newCol.cellIndex-1);
        head = document.createElement("<img src='base/themes/blue/images/3rdMenuNoChoseHeadBg.gif' />");
        tdHead = document.createElement("<td/>");
        tdHead.appendChild(head);
        row.insertBefore(tdHead, newCol);

        end = document.createElement("<img src='base/themes/blue/images/3rdMenuNoChoseEndBg.gif' />");
        tdEnd = document.createElement("<td/>");
        tdEnd.appendChild(end);
        row.replaceChild(tdEnd, newCol.nextSibling);
      }

      if (third>=0 && third<_3rd_menu.length && document.getElementById(_3rd_menu[third][0])!=null) {
        // 现在选中的菜单
        col=document.getElementById(_3rd_menu[third][0]);
        row = col.parentNode;

        newCol = document.createElement("<td class='3rdMenuChoseBg' />");
        newCol.setAttribute("id", _3rd_menu[third][0]);
        div = document.createElement("<div noWrap></div>");
        div.innerText = "  "+_3rd_menu[third][1]+"  ";
        newCol.appendChild(div);
        row.replaceChild(newCol, col);

        row.deleteCell(newCol.cellIndex-1);
        head = document.createElement("<img src='base/themes/blue/images/3rdMenuChoseHeadBg.gif' />");
        tdHead = document.createElement("<td/>");
        tdHead.appendChild(head);
        row.insertBefore(tdHead, newCol);

        end = document.createElement("<img src='base/themes/blue/images/3rdMenuChoseEndBg.gif' />");
        tdEnd = document.createElement("<td/>");
        tdEnd.appendChild(end);
        row.replaceChild(tdEnd, newCol.nextSibling);
      } else {
        document.getElementById("_3rd_menu_head_").style.display='none'
      }

			if (_3rd_menu.length<=0)
      	_3rd_current=-1;
      else
      	_3rd_current=third;
    }
  }

	if (_3rd_menu.length>third)
		_help3rdMenu = _3rd_menu[third][0];


	// 用className改变样式 -- TBD

	// 显示等待
//	wait();


	template.target="_business";
	if (_3rd_current != -1) {
		template.action=_3rd_menu[_3rd_current][2];
	} else {
		template.action=_2nd_menu[_2nd_current][2];
	}
	template.submit();

	/*
	var oldIFrame=document.getElementById("_iframe");
	if (navigator.appVersion.indexOf("MSIE 6.")!=-1) {
		var newIFrame
	  if (_3rd_current!=-1) {
	  	newIFrame = document.createElement("<IFrame id='_iframe' width='100%' height='100%' frameborder='0' src="+_3rd_menu[_3rd_current][2]+"></IFrame>");
	  } else {
	  	newIFrame = document.createElement("<IFrame id='_iframe' width='100%' height='100%' frameborder='0' src="+_2nd_menu[_2nd_current][2]+"></IFrame>");
	  }
	  document.getElementById("_iframe").parentNode.replaceChild(newIFrame, oldIFrame);
	  delete oldIFrame;
	} else {
		if (_3rd_current!=-1) {
	  	oldIFrame.src = _3rd_menu[_3rd_current][2];
	  } else {
	  	oldIFrame.src = _2nd_menu[_2nd_current][2];
	  }
	}
	*/
}


function wait() {return
	var plan = document.getElementById("_wait")
  plan.style.display=''
  plan.style.position='absolute'

  var form = plan
  while(form.tagName != 'BODY') {
    form = form.offsetParent;
  }
  plan.style.top = form.offsetHeight/2 - plan.offsetHeight/2 + 60
  plan.style.left = form.offsetWidth/2 - plan.offsetWidth/2 + 80

	document.getElementById("_interval").style.display='none'
}

function show() {return
	document.getElementById("_wait").style.display='none';
	document.getElementById("_interval").style.display='';
}

function showModule() {
	var tbody = document.getElementById("_moduleLabel").parentNode;
	var clazz;

	for (var i=0; i<_module.length; i++) {
		if (i/2*2==i) clazz="rowWhite";
		else clazz="rowGray";
		row = document.createElement("<tr class='"+clazz+"'></tr>");
		tbody.appendChild(row);

		col = document.createElement("<td/>");
		row.appendChild(col);

		col.appendChild(document.createElement("<input type='radio' onclick=\"changeModule('"+_module[i][0]+"', '"+_module[i][1]+"')\" id='_"+_module[i][0]+"'/>"));

		col = document.createElement("<td/>");
		row.appendChild(col);
		col.innerText=_module[i][1];
	}
}

function changeModule(newModule, name) {
	document.getElementById("_control").style.display='none'
	document.getElementById("_content").style.display='';

	if (_1st_menu.length>0 && _1st_menu[0][0].indexOf(newModule)>=0) { // 未切换系统
		return;
	}
	document.getElementById("_sysinfo").innerText="您当前所在："+name;
	initMenu(newModule);
	_1st_current=-1;
	_2nd_current=-1;
	_3rd_current=-1;
	drawMenu(0, 0, 0);
}