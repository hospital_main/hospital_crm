<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/base/security/baseGroupStore.jsp,v 1.1 2012/03/12 01:57:24 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:24 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ page import="java.util.ArrayList,
								com.viewhigh.cbcs.base.util.*,
								com.viewhigh.cbcs.base.mvc.view.component.SingleSelect,com.viewhigh.cbcs.base.security.*,
                com.viewhigh.cbcs.base.util.ExtendTool" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript">
  function store() {
    trim(template.group_name.value);
    if (isTooLong(template.group_name, 40)) {
			alert('用户组名称长度不能超过40');
			return false;
    }
    if (template.group_name.value=='') {
      alert('用户组名称不能为空');
      template.group_name.focus();
      return false;
    }

		template.subFunction.value='store';
		show_wait();
    template.submit();
  	return true;
	}

	function loads() {
        template.subFunction.value='load';
		show_wait();
        template.submit();
		return true;
	}

  function back() {
    template.group_id.value='';
    template.group_name.value='';
  	template.subFunction.value='find';
  	show_wait();
  	template.submit();
  	return true;
  }
  
  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
      if (template.elements[i].type=='checkbox')
        template.elements[i].checked = true;
    }
  }

</Script>

<html:html clazz="main" scrollCtl="yes">
  <form name="template" method="post" action="baseGroup.jspviewhigh">
      <!-- 信息提示栏 -->
	  <html:message/>

      <!-- 标题栏 -->
	  <html:title clazz='module'>用户组修改页面</html:title>


      <%
			  ArrayList list=(ArrayList)request.getAttribute("result");
			  String group_id = request.getParameter("group_id");
        String group_name = (String)list.get(0);
      %>
      <!-- 简单信息 -->
	  <html:table clazz="simple">
        <tr>
          <td class="normalText" nowrap="nowrap">用户组代码：</td>
          <td width="75%" class="normalText" nowrap="nowrap">
            <input type=text disabled value='<%=group_id%>'/>
            <input type=hidden name="group_id" value='<%=group_id%>'/>
          </td>
        </tr>
        <tr>
          <td class="normalText" nowrap="nowrap">用户组名称：</td>
          <td class="normalText" nowrap="nowrap"><input type=text name="group_name" class="textInputC" value='<%=group_name%>' maxlength="40"/></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2"> 
          	<img src="images/selectedAll.gif" class="mouse" onclick="return selectAll();" /> 
          	<button class="pageBtn" onclick="return reset();" >重置</button> 
         	<button class="pageBtn" name=""   onclick="return store();"  >保存</button> 
          	<button class="pageBtn" onclick="return back();">返回</button>           	
          	<!--<img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
          	<img src="images/return.gif" class="mouse" onclick="return back();" />-->
          </td>
        </tr>
        <tr>
          <td colspan=100><hr></td>
        </tr>
        <tr>
          <td class='normalText' colspan=100>
	          <%
	          String[][] module_value = Preference.getModuleCodeName();
	          SingleSelect module = new SingleSelect(module_value, "module", request.getParameter("module"), false, true);
	          module.setAttribute("onchange", "loads()");
	          out.println(module);
	          %>
	          <br>
	          <p>
            <%
            String[][] author_infos = null;
						if (list.size()>1)
							author_infos=(String[][])list.get(1);

            if (author_infos!=null && author_infos.length>0) {
            	out.println("            <table  BORDERCOLOR='#214597' cellspacing='1' width='100%' class='normalText'>");
              out.println("              <tr>");
              out.println("          	<td>");
              // 缩进比较值
              int compareIndent = 1024;
              for (int i=0; i<author_infos.length; i++) {
              	int indent = Integer.parseInt(author_infos[i][2]);
              	
                // 去掉在经营分析中的项目和病种, 这种方法是一个不稳定的
              	if ("eco".equals(request.getParameter("module"))) { // 经营分析模块
              		if (indent>compareIndent) continue;              		
              		else compareIndent=1024;
              	
              		int[] moduleValue = Dog.getModule();
              		if (moduleValue[1]!=1) { // 不显示项目核算相应的菜单
              			if (author_infos[i][1].indexOf("项目")>=0) {
              				compareIndent = indent;
              				continue;
              			}
              		}
              		if (moduleValue[4]!=1) { // 不显示病种核算相应的菜单
              			if (author_infos[i][1].indexOf("病种")>=0) {
              				compareIndent = indent;
              				continue;
              			}
              		}
              	}
              
                String app_name = author_infos[i][0].substring(1);
                app_name = app_name.substring(0, app_name.indexOf("_"));

                out.println();
                for (int j=0; j<indent; j++) out.print("&nbsp;&nbsp;");

                out.print(author_infos[i][1]+"&nbsp;");  // 菜单名称

                while (true) {
                  if (author_infos[i][3]!=null && !author_infos[i][3].equals("")) {
                    String[] temp= {author_infos[i][0], author_infos[i][3]};
                    out.print("<input type='checkbox' "+("true".equals(author_infos[i][5])?"checked=true":" ")+" name='authorInfo' value='"+ExtendTool.arrayToString(temp)+"'/>");  // primary key
                  }
                  out.print(author_infos[i][4]);// 权限名称

                  if (i+1>=author_infos.length) break;
                  if (!author_infos[i][0].equals(author_infos[i+1][0])) break;
                  else i++;
                }
                out.println("<br>");
              }
              out.println("          </td>");
              out.println("              </tr>");
              out.println("            </table>");
            }
            %>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
	  </html:table>
    <input type=hidden name="subFunction"/>
  </form>

</html:html>