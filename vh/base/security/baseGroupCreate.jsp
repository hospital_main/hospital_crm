<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/base/security/baseGroupCreate.jsp,v 1.1 2012/03/12 01:57:24 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:24 $
 $Revision: 1.1 $
-->

<%@ page language="java" contentType="text/html;charset=GBK" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage" %>

<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
								com.viewhigh.cbcs.base.util.*,
								com.viewhigh.cbcs.base.mvc.view.component.SingleSelect" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>



<Script Language="JavaScript">
  function create() {
    trim(template.group_id.value);
    if (isTooLong(template.group_id, 20)) {
			alert('用户组代码长度不能超过20');
	 		return false;
    }
    if (template.group_id.value=='') {
      alert('用户组代码不能为空');
      template.group_id.focus();
      return false;
    }

    trim(template.group_name.value);
    if (isTooLong(template.group_name, 40)) {
			alert('用户组名称长度不能超过40');
			return false;
    }
    if (template.group_name.value=='') {
      alert('用户组名称不能为空');
      template.group_name.focus();
      return false;
    }

    template.subFunction.value='create';
    show_wait();
  	template.submit();
  	return true;
	}

	function preparedCreate(){
		template.subFunction.value='preparedCreate';
		show_wait();
		template.submit();
		return true;
	}

  function back() {
    template.group_id.value='';
    template.group_name.value='';
  	template.subFunction.value='find';
  	show_wait();
  	template.submit();
  	return true;
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
      if (template.elements[i].type=='checkbox')
        template.elements[i].checked = true;
    }
  }
</Script>

<html:html clazz="main">
  <form name="template" method="post" action="baseGroup.jspviewhigh">
      <!-- 信息提示栏 -->
	  <html:message/>

      <!-- 标题栏 -->
	  <html:title clazz='module'>用户组添加页面</html:title>


      <!-- 简单信息 -->
	  <html:table clazz="simple">
        <tr>
          <td class="normalText" nowrap="nowrap">用户组代码：</td>
          <td width="75%" class="normalText" nowrap="nowrap"><input type=text name="group_id" class="textInputC" maxlength="20"/></td>
        </tr>
        <tr>
          <td class="normalText" nowrap="nowrap">用户组名称：</td>
          <td class="normalText" nowrap="nowrap"><input type=text name="group_name" class="textInputC" maxlength="40"/></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2"> 
          	<img src="images/selectedAll.gif" class="mouse" onclick="return selectAll();" /> 
           	<button class="pageBtn" onclick="return create();" >添加</button>
         	<button class="pageBtn" onclick="return reset();" >重置</button> 
           	<button class="pageBtn" onclick="return back();">返回</button>  
         	<!--<img src="images/create.gif" class="mouse" onclick="return create();" />
          	<img src="images/reset.gif" class="mouse" onclick="return reset();" />          	
          	<img src="images/return.gif" class="mouse" onclick="return back();" />-->
          </td>
        </tr>
        <tr>
          <td colspan=100><hr></td>
        </tr>
        <tr>
          <td class='normalText' colspan=100>
          	<%
	          String[][] module_value = Preference.getModuleCodeName();
	          SingleSelect module = new SingleSelect(module_value, "module", request.getParameter("module"), false, true);
	          module.setAttribute("onchange", "preparedCreate()");
	          out.println(module);
	          %>
	          <br>
	          <p>
            <%
            String[][] author_infos = (String[][])request.getAttribute("author_infos");
            if (author_infos!=null && author_infos.length>0) {
              out.println("            <table  BORDERCOLOR='#214597' cellspacing='1' width='100%' class='normalText'>");
              out.println("              <tr>");
              out.println("          <td>");
              boolean dept_flag = true, economy_flag = true;
              for (int i=0; i<author_infos.length; i++) {
                String app_name = author_infos[i][0].substring(1);
                app_name = app_name.substring(0, app_name.indexOf("_"));
                /*
                if (app_name.equals("dept") && dept_flag) {
                  out.println("          </td>");
                  out.println("          <td class='normalText' valign='top'>");
                  out.println("<B>科室系统</B><br>");
                  dept_flag = false;
                } else if (app_name.equals("economy") && economy_flag) {
                  out.println("          </td>");
                  out.println("          <td class='normalText' valign='top'>");
                  out.println("<B>经营预测与投资决策支持系统</B><br>");
                  economy_flag = false;
                }*/

                out.println();
                int indent = Integer.parseInt(author_infos[i][2]);
                for (int j=0; j<indent; j++) out.print("&nbsp;&nbsp;");

                out.print(author_infos[i][1]+"&nbsp;");  // 菜单名称

                while (true) {
                  if (author_infos[i][3]!=null && !author_infos[i][3].equals("")) {
                    String[] temp= {author_infos[i][0], author_infos[i][3]};
                    out.print("<input type='checkbox' checked=true name='authorInfo' value='"+ExtendTool.arrayToString(temp)+"'/>");  // primary key
                  }
                  out.print(author_infos[i][4]);// 权限名称

                  if (i+1>=author_infos.length) break;
                  if (!author_infos[i][0].equals(author_infos[i+1][0])) break;
                  else i++;
                }
                out.println("<br>");
              }
            }
            %>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><input type=hidden name="subFunction"/></td>
        </tr>
	  </html:table>
 	</form>

</html:html>