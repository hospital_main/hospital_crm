<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/base/security/vhUserCreate.jsp,v 1.1 2012/03/12 01:57:24 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:24 $
 $Modtime: 03-09-03 10:02 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>


<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
  function create()
  {
    //template.subFunction.value='create';


    if(isEmpty(template.emp_id))
    {
      alert('职工代码不能为空!');
      return;
    }
    if(isEmpty(template.user_id))
    {
      alert('用户代码不能为空!');
      return;
    }
     if(isEmpty(template.group_id))
    {
      alert('用户代码不能为空!');
      return;
    }
    if(isEmpty(template.password))
    {
      alert('密码不能为空!');
      return;
    }
      if(isTooLong(template.password,20))
    {
      alert('密码不能高于10个字符!');
      return;
    }
     if(template.password.value!=template.ensurepassword.value)
     { alert("确认密码输入不正确");
       template.ensurepassword.value="";
       template.ensurepassword.focus();
       return;
     }
		show_wait();
    template.submit();
    return true;
  }

  // 返回
  function back( element )
  {
    template.subFunction.value='findAll';
    show_wait();
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="vhUser.jspviewhigh">

  <!-- 信息提示栏 -->
	  <html:message/>

  <!-- 标题栏 -->
	  <html:title clazz='module'>用户添加页面</html:title>


  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td class="normalText" nowrap="nowrap">用户代码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="user_id" class="textInputC" /></td>
    </tr>
    <tr>
       <%
        String[][] employee =
          (String[][])request.getAttribute("employee");

       %>
      <td class="normalText" nowrap="nowrap">职工代码：</td>
      <td class="normalText" nowrap="nowrap">
		    <select type="select" name="emp_id"/>
          <option value="">---------</option>
          <%
          if (employee!=null) {
	          for(int i = 0; i < employee.length; i++) {
	        %>

	          <option value="<%=employee[i][0]%>">
	              <%=employee[i][0]+":"+employee[i][1]%>
	          </option>
	        <%
          	}
          }
          %>
        </select>
		  </td>
    </tr>
    <tr>
       <%
        String[][] groupInfo =
          (String[][])request.getAttribute("groupInfo");

       %>
      <td class="normalText" nowrap="nowrap">用户组：</td>
      <td class="normalText" nowrap="nowrap">
		    <select type="select" name="group_id"/>
           <option value="">---------</option>
          <%
          if (groupInfo!=null) {
	          for(int i = 0; i < groupInfo.length; i++) {
	        %>

	          <option value="<%=groupInfo[i][0]%>">
	              <%=groupInfo[i][1]%>
	          </option>
	        <%
          	}
          }
          %>
        </select>
		  </td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">密码：</td>
      <td class="normalText" nowrap="nowrap"><input type=password name="password" class="textInputC" /></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">确认密码：</td>
      <td class="normalText" nowrap="nowrap"><input type=password name="ensurepassword" class="textInputC" /></td>
    </tr>
    <tr>
      <td colspan="2"> <button class="pageBtn" onclick="create();" >添加</button>
      <button class="pageBtn" onclick="return reset();" >重置</button> 
      <button class="pageBtn" onclick="return back(template);">返回</button> 
      <!--<img src="images/create.gif" class="mouse" onclick="create();" /> <img src="images/reset.gif" class="mouse" onclick="return reset();" /> 
      <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
	  </html:table>
  <input type=hidden name="subFunction" value="create"/>
</form>

</html:html>
