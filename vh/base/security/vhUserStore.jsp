<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/base/security/vhUserStore.jsp,v 1.1 2012/03/12 01:57:24 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:24 $
 $Modtime: 03-09-03 10:01 $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="javascript/check.js" ></Script>
<Script Language="JavaScript">
function save()
  {
    //template.subFunction.value='create';



    if(isEmpty(template.user_id))
    {
      alert('用户代码不能为空!');
      return;
    }
    if(isEmpty(template.password))
    {
      alert('密码不能为空!');
      return;
    }
     if(isEmpty(template.group_id))
    {
      alert('用户代码不能为空!');
      return;
    }
      if(isTooLong(template.password,20))
    {
      alert('密码不能高于20个字符!');
      return;
    }
     if(template.password.value!=template.ensurepassword.value)
     { alert("确认密码输入不正确");
       template.ensurepassword.value="";
       template.ensurepassword.focus();
       return;
     }
     show_wait();
     template.submit();
    return true;
  }


  // 返回
  function back( element )
  {
    template.subFunction.value='findAll';
    element.submit();
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="vhUser.jspviewhigh">

  <!-- 信息提示栏 -->
	  <html:message/>


  <!-- 标题栏 -->
	  <html:title clazz='module'>用户修改页面</html:title>


  <%
    String[] result = (String[])request.getAttribute( "result" );
    if ( result != null )
    {
  %>

  <!-- 简单信息 -->
	  <html:table clazz="simple">
    <tr>
      <td class="normalText" nowrap="nowrap">用户代码：</td>
      <td class="normalText" nowrap="nowrap"><input type=text name="user_id" readonly style='border:none' value=<%=result[0]%> class="textInputC" /></td>
    </tr>
    <tr>
       <%
        String[][] employee =
          (String[][])request.getAttribute("employee");

       %>
      <td class="normalText" nowrap="nowrap">职工代码：</td>
      <td class="normalText" nowrap="nowrap">
		    <select type="select" name="emp_id"/>
          <%
          if (employee!=null) {
	          for(int i = 0; i < employee.length; i++) {
	        %>

	          <option value="<%=employee[i][0]%>" <%if(result[1]!=null&&employee[i][0].equals(result[1])) out.print("selected");%> >
	              <%=employee[i][0]+":"+employee[i][1]%>
	          </option>
	        <%
          	}
          }
          %>
        </select>
		  </td>
    </tr>
    <tr>
       <%
        String[][] groupInfo =
          (String[][])request.getAttribute("groupInfo");

       %>
      <td class="normalText" nowrap="nowrap">用户组：</td>
      <td class="normalText" nowrap="nowrap">
		    <select type="select" name="group_id"/>

          <%
          if (groupInfo!=null) {
	          for(int i = 0; i < groupInfo.length; i++) {
	        %>

	          <option value="<%=groupInfo[i][0]%>" <%if(result[3]!=null&&groupInfo[i][0].equals(result[3])) out.print("selected");%>>
	              <%=groupInfo[i][1]%>
	          </option>
	        <%
          	}
          }
          %>
        </select>
		  </td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">修改密码：</td>
      <td class="normalText" nowrap="nowrap"><input type=password name="password"value=<%=result[2]%> class="textInputC" /></td>
    </tr>
    <tr>
      <td class="normalText" nowrap="nowrap">确认密码：</td>
      <td class="normalText" nowrap="nowrap"><input type=password name="ensurepassword" value=<%=result[2]%> class="textInputC" /></td>
    </tr>
     <tr>
      <td colspan="2">
      <button class="pageBtn" name=""  onclick="return save();"  >保存</button>
      <button class="pageBtn" onclick="return reset();" >重置</button>
      <button class="pageBtn" onclick="return back(template);">返回</button>  
      <!--<img src="images/reset.gif" class="mouse" onclick="return reset();" />
       <img src="images/return.gif" class="mouse" onclick="return back(template);" />--></td>
    </tr>
    <tr height="500">
    <td/>
    </tr>
	  </html:table>
  <input type=hidden name="subFunction" value = "store"/>
  <input type="hidden" name="emp_id" value="<%=result[1]%>">
  <%}%>
</form>

</html:html>
