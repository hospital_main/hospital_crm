<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/base/security/statusreset.jsp,v 1.1 2012/03/12 01:57:24 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:24 $
 $Modtime: 03-09-03 10:01 $
 $Revision: 1.1 $
 $NoKeywords: $
-->
<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../vh/error.jsp" %>
<%@ page import="com.viewhigh.cbcs.cbcs.dist.base.CommonData" %>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>

<Script Language="JavaScript" src="vh/javascript/check.js" ></Script>
<script language='javascript' src='vh/javascript/common.js'></script>
<Script Language="JavaScript">
function dept1(){
   autoSelect();
   if(template.dept.checked){//选中科室核算
    for(var i=0;i<template.elements.length;i++){
      if(template.elements[i].type=="checkbox")
        template.elements[i].checked=true;
    }
  }
}
function item1(){
  if(template.item.checked){//选中项目核算
       template.itemIndex.checked=true;
  }else{
    template.itemIndex.checked=false;
  }
}
function disease1(){
    if(template.disease.checked){//选中病种核算
       template.diseaseIndex.checked=true;
    }else{
    template.diseaseIndex.checked=false;
  }
}
function dealInDept1(){
  
  if(template.dealInDept.checked){//选中经营科室
       template.manageDept.checked=true;
    }else{
    template.manageDept.checked=false;
  }
  autoSelect();
}
function autoSelect(){
  if(template.dealInDept.checked==true&&template.manageDept.checked==true&&template.serveDept.checked==true&&template.other.checked==true){
    template.emp_bonus.checked=true;
    template.dept_clin_code.disabled=true;
    template.BonusItems.disabled=true;
  }else{
    template.emp_bonus.checked=false;
    template.dept_clin_code.disabled=false;
    template.BonusItems.disabled=false;
  }
}
function save(){
 if (template.year_month.value=="") {
  		alert("请输入日期");
  		return false;
 		}
 	if(!confirm('['+template.year_month.value+']该年月以后的月结标记将全部被取消, 请确认！')){
 	return false;
 	}
 	
//    show_wait();
     template.submit();
    return true;
  }


  // 返回
  function back( element )
  {
    template.subFunction.value='findAll';
    element.submit();
  }
   function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].type=='checkbox')
            template.elements[i].checked = true;
    }
  }
</Script>
<html xmlns:vh>
<head>
  <title>登录用户：系统超级管理员</title>
  <meta http-equiv='Content-Type' content='text/html; charset=GBK'>
  <link rel='stylesheet' href='vh/vh.css' type='text/css'>
  <link rel='stylesheet' href='vh/newVh.css' type='text/css'>
  
  <style>
  	fc\:webprint {behavior: url(webprint/webgrid.htc);}
  	.commonYearMonth{
			behavior:url(vh/base/htc/yearmonth.htc);
			width:140px;
		
		}
		
		.pageBtn {
  cursor:hand;
	behavior: expression("url(base/htc/button.htc)");
	/*line-height:  16px;
	background-image: expression("url(images2/button/bian.png)");
	border: 1px solid #2DA0F5;
	height:21;
	color: #FFFFFF;
	padding: 0px;
	margin: 0px;
	font-family: "宋体";
	font-size: 9pt;*/
	border:1px solid #0F71B0; background:url(../../../base/themes/default/skins/skins01/images/bu.gif) repeat-x; height:23px; line-height:21px;padding:0px 0px;color:#fff; cursor:pointer; font-size:12px; overflow:hidden; margin:5px 0;
}
.normalText {width:auto;}
.normalText select.selectBg{ width:140px;}	
  	</style>
  <script language='javascript' src='vh/javascript/common.js'></script><script language='javascript'>
function KeyDown1(){ 
 if (event.keyCode==222||(event.shiftKey&&event.keyCode==190)||(event.shiftKey&&event.keyCode==188)) { 
     return false;   }
}
</script>
  <script language='javascript1.2'>
    function window_onload() {

      if (parent._1st_current!=null) parent.show();
    }
  </script>
</head>
<body scroll='no' bgcolor='#FFFFFF' rightMargin='0' leftMargin='0' onload='window_onload();if(typeof(window_onload2)=="function"){window_onload2();}' style='overflow:auto;'onkeydown='return KeyDown1()'>
<img id='_image' src='img/loading.gif' style='display:none'/>
<div id='_process'>
<form name="template" method="post" action="statusreset.jspviewhigh">

  <!-- 信息提示栏 -->
	  <html:message/>
  <!-- 标题栏 -->
	  <html:title clazz='module'>取消月结标识</html:title>
  <%
    String[][] result = (String[][])request.getAttribute( "result" );
    String yearmonth=(String)request.getAttribute( "yearmonth" );
    boolean disease=false,assign=false,proj=false,eco=false;
    if ( result != null &&yearmonth!=null)
    { for(int i=0;i<result.length;i++){
        if(result[i][0].equals("disease"))
          disease=true;
        if(result[i][0].equals("dist"))//如果包括"内部分配"模块
          assign=true;
        if(result[i][0].equals("proj"))
          proj=true;
        if(result[i][0].equals("eco"))
          eco=true;
    }
  %>

  <!-- 简单信息 -->
  <div class="simpleQuery">
	  <table  border="0" align="center">
    <tr>
      <td align="right" nowrap="nowrap" class="normalText" width="20%">&nbsp;&nbsp;核算月：</td>
  <td nowrap class="normalText" colspan="2"><%=new MonthComponent("year_month",request.getParameter("year_month"))%></td>
      

    </tr>
    <tr>
      <td align="right" nowrap="nowrap" class="normalText" rowspan="1" valign="top">功能选择：</td>
      <td class="normalText" nowrap="nowrap" colspan="2">
        <input type="checkbox" name="dept" value="0101" onClick="dept1()"
          <%if(request.getParameter("dept")!=null&&request.getParameter("dept").equals("0101")) out.print("checked");%>
          />科室核算
	    </td>
    </tr>
    <tr <%if(proj==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText"></td>
      <td class="normalText" nowrap="nowrap" colspan="2">
        <input type="checkbox" name="item" value="0301"
          <%if(request.getParameter("item")!=null&&request.getParameter("item").equals("0301")) out.print("checked");%>
          <%if(proj==false) out.print("disabled");%> onClick="item1()">项目核算
	    </td>
    </tr>
    <tr <%if(disease==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText"></td>
      <td class="normalText" nowrap="nowrap" colspan="2" style='display:none'>
        <input type="checkbox" name="disease" value="0401"
          <%if(request.getParameter("disease")!=null&&request.getParameter("disease").equals("0401")) out.print("checked");%>
          <%if(disease==false) out.print("disabled");%> onClick="disease1()">病种核算
	    </td>
    </tr>
    <tr <%if(assign==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText">&nbsp;</td>
      <td class="normalText" nowrap="nowrap" colspan="2">
        &nbsp;&nbsp;&nbsp;&nbsp;
	    </td>
    </tr>
    <tr <%if(assign==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText" rowspan="1" valign="top">内部分配：</td>
      <td class="normalText" nowrap="nowrap" colspan="2"><input type="checkbox" name="dealInDept"
           <%if(request.getParameter("dealInDept")!=null&&request.getParameter("dealInDept").equals("0201")) out.print("checked");%>
          <%if(assign==false) out.print("disabled");%> value="0201" onClick="dealInDept1()">经营科室
	    </td>
    </tr>
    <tr <%if(assign==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText"></td>
      <td class="normalText" nowrap="nowrap" colspan="2"><input type="checkbox" name="manageDept"
         <%if(request.getParameter("manageDept")!=null&&request.getParameter("manageDept").equals("0202")) out.print("checked");%>
          <%if(assign==false) out.print("disabled");%> value="0202" onClick="autoSelect()">管理科室
	    </td>
    </tr>
    <tr <%if(assign==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText"></td>
      <td class="normalText" nowrap="nowrap" colspan="2"><input type="checkbox" name="serveDept"
          <%if(request.getParameter("serveDept")!=null&&request.getParameter("serveDept").equals("0203")) out.print("checked");%>
          <%if(assign==false) out.print("disabled");%> value="0203" onClick="autoSelect()">服务科室
	    </td>
    </tr>
    <tr <%if(assign==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText"></td>
      <td class="normalText" nowrap="nowrap"><input type="checkbox" name="other" <%if(request.getParameter("other")!=null) out.print("checked");%>
       <%if(assign==false) out.print("disabled");%> value="9*" onClick="autoSelect()">其它奖金
	    </td>
      <!--td class="normalText" nowrap="nowrap">
    <%String[][] otherBonus = (String[][])request.getAttribute("otherBonus");%>
      <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(otherBonus, "otherBonus", request.getParameter("otherBonus"), true, false)%>
     
      </td-->
    </tr>
    <tr <%if(assign==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText"></td>
      <td class="normalText" nowrap="nowrap" colspan="2"><input type="checkbox" name="emp_bonus" <%if(request.getParameter("emp_bonus")!=null) out.print("checked");%>
       <%if(assign==false) out.print("disabled");%> value="">个人奖金
	    </td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
    <tr <%if(assign==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText">科室名称：</td>
      <td class="normalText" nowrap colspan="2">
       <%String[][] dept_clin_code = (String[][])request.getAttribute("dept_clin_code");%>

            <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(dept_clin_code, "dept_clin_code", request.getParameter("dept_clin_code"), true, false)%>


      </td>
	 </tr>
	 <tr>
      <td align="right" nowrap class="normalText">奖金项目：</td>
      <td class="normalText" nowrap  colspan="2">
             <%String[][] BonusItems = (String[][])request.getAttribute("BonusItems");%>
            <%= new com.viewhigh.cbcs.base.mvc.view.component.SingleSelect(BonusItems, "BonusItems", request.getParameter("BonusItems"), true, false)%>

      </td>
    </tr>
 <tr <%if(eco==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText"></td>
      <td class="normalText" nowrap="nowrap" colspan="2"></td>
    </tr>
      <tr <%if(eco==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText" rowspan="1" valign="top">经营分析：</td>
      <td class="normalText" nowrap="nowrap" colspan="2"><input type="checkbox" name="wholeIndex"
          <%if(request.getParameter("wholeIndex")!=null&&request.getParameter("wholeIndex").equals("0411")) out.print("checked");%>
          <%if(eco==false) out.print("disabled");%> value="0411">全院指标计算
	    </td>
    </tr>
    <tr <%if(eco==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText"></td>
      <td class="normalText" nowrap="nowrap" colspan="2"><input type="checkbox"
          <%if(request.getParameter("deptIndex")!=null&&request.getParameter("deptIndex").equals("0412")) out.print("checked");%>
          <%if(eco==false) out.print("disabled");%> name="deptIndex" value="0412">科室指标计算
	    </td>
    </tr>
    <tr <%if(eco==false||proj==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText"></td>
      <td class="normalText" nowrap="nowrap" colspan="2"><input type="checkbox" name="itemIndex"
          <%if(request.getParameter("itemIndex")!=null&&request.getParameter("itemIndex").equals("0413")) out.print("checked");%>
          <%if(eco==false||proj==false) out.print("disabled");%> value="0413">项目指标计算
	    </td>
    </tr>
    <tr <%if(eco==false||disease==false) out.print("style='display:none'");%>>
      <td align="right" nowrap="nowrap" class="normalText"></td>
      <td class="normalText" nowrap="nowrap" colspan="2" style='display:none'><input type="checkbox" name="diseaseIndex"
          <%if(request.getParameter("diseaseIndex")!=null&&request.getParameter("diseaseIndex").equals("0414")) out.print("checked");%>
          <%if(eco==false||disease==false) out.print("disabled");%> value="0414">病种指标计算
	    </td>
    </tr>
     <tr>
      <td colspan="4" align="center">
        <!--img src="images/selectedAll.gif" class="mouse" onclick="return selectAll()" />
         <img src="images/reset.gif" class="mouse" onclick="return reset();" />
        <img src="images/save.gif" class="mouse" onclick="return save();" /-->
			 <button class='pageBtn' accessKey='A' name='select_all' onClick="return selectAll()">全选</button>
        <button class='pageBtn' accessKey='R' name='reset_all' onClick="return reset();">重置</button>
        <button class='pageBtn' accessKey='S' name='save_all' onClick="return save();">保存</button>
        </td>
    </tr>
    </table>
	</div>
  <input type='hidden' name="subFunction" value = "save"/>
  <%}%>
</form>
</div>
</body>
</html>
