<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/base/security/vhUserMain.jsp,v 1.1 2012/03/12 01:57:24 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:57:24 $
 $Modtime: 03-09-01 23:53 $
 $Revision: 1.1 $
 $NoKeywords: $
-->

<%@ page language="java" contentType="text/html;charset=GBK" errorPage="../../error.jsp" %>
<%@ page import="com.viewhigh.cbcs.base.util.ExtendTool,
                com.viewhigh.cbcs.base.sql.BaseRO,
                java.util.*,
                java.text.*,
                com.viewhigh.cbcs.base.mvc.view.TableMarge" %>

<%@ taglib uri="/WEB-INF/taglib/viewhigh-html.tld" prefix="html"%>



<Script Language="JavaScript">
    function create() {
        template.subFunction.value='preparedCreate';
        template.submit();
        return true;
    }

  function remove() {
    var flag = false;
    for (var i=0; i<template.elements.length; i++) {
          if (template.elements[i].name=='primaryKey' && template.elements[i].checked==true)
            flag = true;
      }

    if( flag!=false) {
        if (confirm('是否删除')) {
          template.subFunction.value='remove';
          template.submit();
          return true;
        } else
            return false;
    } else {
      alert( "请先选择,再删除!");
      return false;
    }
  }

  function selectAll(){
    for (var i=0; i<template.elements.length; i++) {
        if (template.elements[i].name=='primaryKey')
            template.elements[i].checked = true;
    }
  }
  function find() {
    template.subFunction.value='findAll';
    template.submit();return true;
  }
</Script>
<html:html clazz="main">
<form name="template" method="post" action="vhUser.jspviewhigh">

	<!-- 信息提示栏 -->
	  <html:message/>


  <!-- 标题栏 -->
	  <html:title clazz='module'>用户主页面</html:title>


  <br>
         <html:title clazz='table'>用户管理</html:title>
  <%
    BaseRO ro = (BaseRO)request.getAttribute("baseRO");
    TableMarge oper = new TableMarge(ro, "return find()");
    oper.addOptionButton("images/selectedAll.gif", "return selectAll()");   // 全选
    oper.addOptionButton("images/reset.gif", "return reset()");     //  重置
    oper.addOptionButton("images/remove.gif", "return remove()");   //  删除
    oper.addNeedButton("images/create.gif", "return create()");     //  添加
  %>
  <!-- 复杂信息 -->
	  <html:table clazz="complex">
    <!-- 操作 -->
    <tr><td><%=oper%></td></tr>

    <!-- 结果集 -->
    <tr>
      <td>
	      <html:table clazz="result">
	        <html:tr clazz='label'>
            <td class="resultLabel">选择</td>
            <td class="resultLabel">用户代码</td>
            <td class="resultLabel">用户名</td>
            <td class="resultLabel">用户组代码</td>
            <td class="resultLabel">用户组名称</td>
	        </html:tr>

          <%
            if (ro!=null) {
              String[][] result = ro.getTableResult();
              if ( result != null ) {
                for (int i = 0; i < result.length; i++ ) {
                  String primaryKey = result[i][0];
                  for (int j=0; j<result[i].length; j++) {
                    if (result[i][j]!=null && result[i][j]=="") result[i][j]="&nbsp;";
                  }

                  String rowColor = "rowGray";
                  if (i/2*2==i) rowColor = "rowWhite";
          %>

          <tr class="<%=rowColor%>">
            <td><input type="checkbox" name="primaryKey" value="<%=primaryKey%>"></td>
            <td class="normalText"><a href="vhUser.jspviewhigh?subFunction=load&primaryKey=<%=primaryKey%>"><%=result[i][0]%></a></td>
            <td class="normalText"><%=result[ i ][ 1 ]%></td>
            <td class="normalText"><%=result[ i ][ 2 ]%></td>
            <td class="normalText"><%=result[ i ][ 3 ]%></td>

          </tr>

          <%    }
              }
            }
          %>
	      </html:table>
      </td>
    </tr>

	      </html:table>
  <input type=hidden name="subFunction"/>
</form>

</html:html>

