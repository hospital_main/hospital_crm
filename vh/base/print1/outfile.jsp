<%@ page language="java" contentType="application/msexcel" %>
<%@ page import = "java.io.FileInputStream,java.io.IOException,java.io.InputStream,java.io.OutputStream"%>
<%
	String fileName=request.getParameter("file");
	try {
		String fn=fileName.substring(fileName.lastIndexOf("/")+1);
		response.setHeader("Content-Disposition", "attachment; filename=" + new String(fn.getBytes(),"iso-8859-1") );
		response.setHeader("Cache-Control", "max-age=0");
		OutputStream os=response.getOutputStream();
		byte[] bytes=new byte[1024];
		InputStream is=new FileInputStream(request.getSession().getServletContext().getRealPath(fileName));
		int size=is.read(bytes);
		while(size>0){
			os.write(bytes, 0, size);
			size=is.read(bytes);
		}
	} catch (IOException e) {
		throw new Exception(e.getMessage());
	}
%>