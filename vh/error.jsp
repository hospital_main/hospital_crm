<!--
  $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/error.jsp,v 1.1 2012/03/12 01:59:04 zhoulidong Exp $
  $Author: zhoulidong $
  $Date: 2012/03/12 01:59:04 $
  $Revision: 1.1 $
-->
<%@page language="java" contentType="text/html;charset=GBK" isErrorPage="true"%>
<%@page import="com.viewhigh.cbcs.base.logger.*" %>

<%
	Log log = LogFactory.newInstance();

  Throwable ex = exception;
  if (ex==null)
    ex=(Throwable)request.getAttribute("exception");

	String errorMessage="系统错误！";
  if ( ex != null ) {
  	log.error("error.jsp", ex);
    java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
    ex.printStackTrace(new java.io.PrintStream(baos));
    out.println("<!-- \n错误明细\n" + baos + "\n -->");

    errorMessage = ex.getMessage();
		if (errorMessage!=null && errorMessage.indexOf("[Microsoft][SQLServer 2000 Driver for JDBC][SQLServer]用户")>-1) {
			errorMessage = errorMessage.substring(errorMessage.indexOf("[Microsoft][SQLServer 2000 Driver for JDBC][SQLServer]")+"[Microsoft][SQLServer 2000 Driver for JDBC][SQLServer]".length());
			errorMessage = "数据库" + errorMessage + "<br>请查看C:\\CBCSV1_5\\conf\\server.xml的配置！";
		}
	}
%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <title>CBCSV2.0.0 - 科室核算系统</title>
  <link href="newVh.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" text="#000000" link="#FFFFFF" vlink="#FFFFFF" onload='if (parent._1st_current!=null) parent.show();'>
<br><br><br><br><br>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="60%" border="0" cellspacing="0" cellpadding="10" align="center">
        <tr>
          <td colspan="2" align="center" style=" font-weight:700; height:50px;line-height:50px; text-align:center;font-size:20px;"><%=errorMessage%></td>
        </tr>
        <tr>
        	<td align='center' width="30%" valign="middle"><img src='img/error.gif'> </td>
          <td align="left" valign="top">
            <font color="#000000" style='font-size:14px;font-family:"宋体";'>
<!--              <div nowrap style=" font-weight:700; line-height:40px; text-align:center;font-size:20px;"></div>--> 
             <div nowrap>详细信息：<%=ex.getLocalizedMessage()%></div>
              <br/>
              <div id="link" nowrap>请&nbsp;联&nbsp;系：010-66410808</div>
            </font>          </td>
        </tr>
        <tr>
          <td colspan="2"  align="center" >
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr>
                <td align="center" height="50"><button class="pageBtn" name="" id='_message_return'  onclick='history.go(-1)' style='cursor:hand; padding:0 6px;' />返回</button>                </td>
              </tr>
              <script language='javascript'>
			          if (history.length==0) {
			          	document.all['_message_return'].style.display='none'
			        	}
			        </script>
            </table>          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
