<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/vh/main.jsp,v 1.1 2012/03/12 01:59:20 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:59:20 $
 $Revision: 1.1 $
-->

<%@page language="java" contentType="text/html;charset=GBK" isErrorPage="true" extends="com.viewhigh.cbcs.base.mvc.controller.MainJspPage"%>
<%@page import="com.viewhigh.cbcs.base.util.Preference"%>
<%@ page import="com.viewhigh.cbcs.base.mvc.view.MonthComponent" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <title>CBCSV2.0.0 - 科室核算系统</title>
  <link href="newVh.css" rel="stylesheet" type="text/css">
  <style type="text/css">
	  .banner {font-size:14px;vertical-align:middle}
	</style>


  <%if (Preference.getShield().equals("true")) {%>
  <script for="document" event="oncontextmenu">return false;</script>
  <%}%>
  <script language='javascript'>
	  function openHelp() {
		  window.open("help/Frameset.htm","Help","width="+screen.availWidth-100+", height="+screen.availHeight-100+", top=0, left=0, scrollbars=1, resizable=1");
		}

	  <%if (Preference.getShield().equals("true")) {%>
		document.onkeydown=shield;
		function shield() {
			if(event.keyCode==116){
				window.event.keyCode=0;
				return false;
			}
		}
		<%}%>
  </script>
</head>

<body leftmargin="0" rightmargin='0' topmargin="0" marginwidth="0" marginheight="0" scroll="no">
	<form name="template" method="post"/>
  <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height="70">
      <td background="img/top<%=String.valueOf(Preference.getZoneVersion())%>.png" colspan="2">&nbsp;</td>
    </tr>
    <tr height="25">
      <td colspan="2" width="100%" >
        <table cellpadding="0" cellspacing="0" class="info">
          <tr valign="middle">
            <td width="200" noWrap align="left">&nbsp;</td>
            <td width="25%" noWrap align="center" class='banner'><span id="currUnitName"><%=Preference.getCom_name()%></span></td>
            <td width="20%" noWrap align="left" class='banner'>操作者：<span id="currUserName"><%=com.viewhigh.cbcs.base.util.LoginUser.getUser_name(request)%></span><span id="currUserId" style="display:none"><%=com.viewhigh.cbcs.base.util.LoginUser.getUser_id(request)%></span></td>
            <td style="display:none" width="20%" noWrap align="left" class='banner'><input type='hidden' id='user_code' value="<%=com.viewhigh.cbcs.base.util.LoginUser.getUser_id(request)%>"></td>
         <% if (request.getParameter("user_id")!=null && request.getParameter("user_id").trim().equalsIgnoreCase("administrator")) {%>
            <td width="27%" noWrap align="left" class='banner'>您当前所在:权限管理系统</td>
         <% } else { %>
            <td width="27%" align="left" class='banner'>
            	<div id='_sysInfo' noWrap class='mouse' onclick="document.getElementById('_control').style.display='';document.getElementById('_content').style.display='none';document.getElementById('_moduleTable').style.display='';document.getElementById('_passwd').style.display='none';">
            	</div>
            </td>
         <% } %>
            <td width="3%"  noWrap align="left" class='banner'><img src="img/password.gif" class="mouse" onclick="modifyPswd()"></td>
            <td width="7%"  noWrap align="left" class='banner'>修改密码</td>
            <td width="3%"  noWrap align="left" class='banner'><img src="img/help.gif" class="mouse" onclick="openHelp()"></td>
            <td width="5%"  noWrap align="left" class='banner'>帮助</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr id='_control' style='display:none'>
    	<td>
      	<table width="100%" height="100%" border="0" bgColor="#FFFFFF">
				  <tr style="height:23%">
				    <td colspan='3'>&nbsp;</td>
				  </tr>
				  <tr style="height:34%">
				    <td width="30%">&nbsp;</td>
				    <td width="40%">
				    	<table id='_moduleTable' width='100%' border='1' bgColor='white' borderColor='black' style='BORDER-COLLAPSE:collapse;font-family:"宋体"; font-size:10pt'>
				    		<tr id='_moduleLabel' class='resultLabel'>
			    				<td>选择</td>
							    <td>系统名称</td>
			    			</tr>
				    	</table>
				    	<IFrame id="_passwd" width="100%" height="100%" frameborder="0" src="" style='display:none'></IFrame>
				    </td>
				    <td width="30%">&nbsp;</td>
				  </tr>
				  <tr style="height:43%">
				    <td colspan='3'>&nbsp;</td>
				  </tr>
      	</table>
    	</td>
    </tr>
    <tr id='_content'>
      <td width="150" valign="top" background="img/bodyGg.gif">
        <table id="_1st_menu" width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr id="_1st_base"><td noWrap valign='top' width='150'></td></tr>
        </table>
      </td>
      <td width="100%">
        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" bgColor="#FFFFFF">
          <tr id="_3rd_menu_head_" height="24" style="display:none">
            <td>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                <tr id="_3rd_menu_">
                </tr>
              </table>
            </td>
          </tr>
          <tr>
          	<td id='_wait' align='center' width='140' nowrap><img id='_image' src='img/loading.gif'/ onclick="show()" style="cursor:hand"></td>
            <td id='_interval' style="display:none">
              <IFrame id="_iframe" name="_business" width="100%" height="100%" frameborder="0" src="" ></IFrame>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>

<script language="javascript">
// 有访问权限的模块
var _module = new Array();
<%
String[][] _module = (String[][])request.getAttribute("user_module");
if (_module!=null && _module.length>0) {
	out.println("_module=[");
	for (int i=0; i<_module.length; i++) {
		out.print("              ['"+_module[i][0]+"', '"+_module[i][1]+"']");
		if (i<_module.length-1)
			out.println(",");
	}
	out.println("            ];");
}
%>

// 1--菜单代码(_root_1_), 2--菜单名称, 3--链接地址, 4--级别
var _all_menu = new Array();
<%
String[][] user_menu = (String[][])request.getAttribute("user_menu");
if (user_menu!=null && user_menu.length>0) {
  out.println("_all_menu = [ ");
  for (int i=0; i<user_menu.length; i++) {
    out.print("              ['"+user_menu[i][0]+"', '"+user_menu[i][1]+"', '"+user_menu[i][2]+"', '"+user_menu[i][3]+"']");
    if (i<user_menu.length-1)
      out.println(",");
  }
  out.println("            ];");
}
%>
</script>

<script Language="JavaScript" src="javascript/main.js" ></script>

<script>
// 初始化
<% if (request.getParameter("user_id")!=null && request.getParameter("user_id").trim().equalsIgnoreCase("administrator")) {%>
initMenu("root");
drawMenu(0, 0, 0);
<% } else { %>
showModule();
document.getElementById("_<%=request.getParameter("appModuleName")%>").click();
<% } %>
</script>
