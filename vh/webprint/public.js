/*! servlet的相对路径,如没有相对路径则给值:servlet 如有相对路径值report则给值:webreport/servlet */
var servletPath="servlet";  
/*! 调用哪种数据库,用于备注字段是否是Clob型,access/sqlserver/oracle/db2 */
var fc_Database="access";

/*! 客户端缓存用的对象变量 */
  var userData
  
  
/**
*把数据存到数据缓存中
*@param Main 为主key, 如"List".
*@param Sub 为子key,如"CustomerFlag"
*@param strContent 为要存入的内容
*@return 无返回
*/
function SaveUserData(Main,Sub,strContent){

	userData.setAttribute(Main+userData.value,strContent)
	userData.save(Sub+userData.value)
}
/**
*从数据缓存中装入到变量中
*@param Main 为主key, 如"List".
*@param Sub 为子key,如"CustomerFlag"
*@return 返回取出的内容
*/
function LoadUserData(Main,Sub){

	userData.load(Sub+userData.value)  
	var sTmp=userData.getAttribute(Main+userData.value)
	if (sTmp==null) sTmp=""
	return sTmp

}
/**
*替代非法XML字符,如<>
*@param sSql 为要替代的字符串,
*@return 返回替代后的字符串
*/
function TransXml(sSql){
	var sql1=""
	for(var i=0;i<sSql.length;i++) {
		switch (sSql.charAt(i)) {
			case "<" :
				sql1=sql1+"&lt;";
				break;
			case ">" :
				sql1=sql1+"&gt;";
				break;
			default:
				sql1=sql1+sSql.charAt(i);
		}
	}
	return sql1
}
/**
*按小数位数格式化字符
*@param sValue 为要格式化的字符串,
*@param sPointNum 为小数位数,整型
*@return 返回格式化后的字符串
*/
function ContDec(sValue,sPointNum) {
	var dblValue=parseFloat(sValue)
	if (isNaN(dblValue)) return sValue
	var iPointNum=parseInt(sPointNum)
	if (isNaN(iPointNum)) iPointNum=0
	if (iPointNum>9) iPointNum=9
	if (iPointNum<0) iPointNum=0
	var dbl1=Math.round(dblValue*Math.pow(10,iPointNum))/Math.pow(10,iPointNum)
	var s1=dbl1+""
	var num0=0
	if(s1.indexOf(".")==-1){
		num0=iPointNum
	}
	else {
		var num1=s1.length-s1.indexOf(".")-1
		
		if(num1<iPointNum )
			num0=iPointNum-num1
	}

	if (num0>0) {
		var s2="000000000000000"
		if(num0==iPointNum)
			s1=s1+"."+s2.substring(0,num0)
		else
			s1=s1+s2.substring(0,num0)
	}
	//if (right(s1,1)==".")
	//	s1=s1.substring(s1.length-1,s1.length)
	return s1
}
/**
*将"1-3,5" --> 1 2 3 5的数组
*@param cText 为要处理的字符串,
*@return 返回一个数组
*/
function PrintArray(cText) {
//返回一个数组
//"1-3,5"	--> 1 2 3 5的数组
    var i =0, ilen =0, iul =0, j=0
    var cstring ="", csubstr=""
    var cstrStart="", cstrEnd =""
    var iPosSeperator=0 , iposMinus =0
    var ival =0, ival1 =0, ival2 =0
    var iarray=new Array()
    var iParray=new Array()
    
    
    cstring = trim(cText)
    
    do
    {
        iPosSeperator = cstring.indexOf(",")
        if (iPosSeperator < 0){
            csubstr = cstring
		}            
        else {
            csubstr = cstring.substring(0, iPosSeperator)
            cstring = cstring.substring(iPosSeperator + 1,cstring.length)
            //alert(csubstr+" "+cstring)
        }
        
        
        iposMinus = csubstr.indexOf("-")
        if( iposMinus > 0 ){
            cstrStart = csubstr.substring( 0, iposMinus )
            cstrEnd = trim(csubstr.substring(iposMinus + 1,csubstr.length))
            ival1 = parseInt(cstrStart)
            ival2 = parseInt(cstrEnd)
            
            if (ival1 != 0 || ival2 != 0 ){
                if (ival1 == 0 && ival2 > 0 ) {
                    iul = iarray.length
                    //ReDim Preserve iarray(iul + 1)
                    iarray[iul] = ival2
                }
                else {
                    if (ival1 > 0 && ival2 == 0 ) {
                        iul = iarray.length
                        iarray[iul] = ival1
                    }
                    else {   
                        if( ival1 > ival2 ){
                            ival = ival1
                            ival1 = ival2
                            ival2 = ival
                        }
                        iul = iarray.length
                        //ReDim Preserve iarray(iul + ival2 - ival1 + 1)
                        for( i = 0 ;i<= ival2 - ival1;i++){
                            iarray[iul + i] = ival1 + i
                        }
					}
                }
			}
        }
        else {
            ival = parseInt(csubstr)
            //If ival < 0 Then GoTo PError
            if( ival > 0 ){
                iul = iarray.length
                //ReDim Preserve iarray(iul + 1)
                iarray[iul] = ival
            }
        }
    }        
    while (iPosSeperator > 0) 
    
    ilen = iarray.length
    if( ilen > 0) {
        for( i = 0 ;i<= ilen - 2;i++){
            for( j = i + 1;j<=ilen - 1;j++) {
                if (iarray[i] > iarray[j] ){
                    ival = iarray[i]
                    iarray[i] = iarray[j]
                    iarray[j] = ival
                }
            }
        }
    
        ival = -1
        for( i = 0 ;i<= ilen - 1;i++) {
            if( iarray[i] != ival) {
                iul = iParray.length
                //ReDim Preserve iParray(iul + 1)
                iParray[iul] = iarray[i]
                ival = iarray[i]
            }
        }
    }
return iParray
    
}
/**
*通过SQL返回一个字段的第一个记录值,返回类型:字符
*@param sql 为要处理的字符串,
*@return 返回一个数组
*/
function SqlToField(sql) {
//通过SQL返回一个字段的第一个记录值,返回类型:字符
	var sRet=""
	var sXml=fc_select(sql,1,2)
	if(sXml!="<root></root>") {
		var oXml=new ActiveXObject("Microsoft.XMLDOM")
		oXml.async="false";
		oXml.loadXML(sXml)
		sRet=oXml.documentElement.childNodes(0).text
		
	}
	return sRet	
}

/**
* 去掉根结点标记
* 13==>15 -7==>-9 是指结尾用换行回车符
*@param strX 为要处理前的字符串,
*@return 返回处理后的字符串
*/
function RemoveRoot(strX){
  //去掉根结点标记
  //13==>15 -7==>-9 是指结尾用换行回车符
  if (strX.length>13)
		{
			strX=strX.substring(6,strX.length-7)
			return strX
		}
	else
		{
			return ""
		}	
}
function getreportcell(iRow,iCol,strTableName){
	return GETREPORTCELL(iRow,iCol,strTableName)
}
function GETREPORTCELL(iRow,iCol,strTableName) {
	//跨表取数
	//sdgdfg
	var sXml="<Row>"+iRow+"</Row>"+"<Col>"+iCol+"</Col>"+"<TableName>"+escape(strTableName)+"</TableName>"
	var retX=SendHttp("http://"+location.host+"/"+servletPath+"/WebPrint?GetReportCell",sXml)
	retX=RemoveRoot(retX)
	var s1=unescape(retX)
	if (isNaN(parseFloat(s1)))
		var v=s1
	else
		var v=parseFloat(s1)

	return v
	
}
function GETDB(strConnection,strSql) {
//从数据库中取数函数//取第一条记录的第一个字段的值
	var sXml="<Conn>"+strConnection+"</Conn><sSql>"+strSql+"</sSql>"
//	var sT=SendHttp("servlet/javaReport?GetDB",s1)
   //alert(sXml)
	var retX=SendHttp("http://"+location.host+"/"+servletPath+"/WebPrint?GetDB",sXml)
	return retX

}
function DelReport(sRepName) {
//删除报表
//sRepName为未加码的报表名称

	var sRet=fc_insert("delete * from fccell where fstrTableName='"+escape(sRepName)+"'")
	if(isSpace(sRet)){
		ShowXml("")
	}
	else {
		alert(sRet)
	}

}
/**
*执行插入
**/
function fc_insert(sSql) {
//执行插入
	var sXml="<No>"+sSql+"</No>"
	
	var retX=SendHttp("http://"+location.host+"/"+servletPath+"/WebPrint?fc_insert",sXml)
	return retX
}
/**
//执行为一对多表插入
//参数串先为主表，后为子表，子表的最后字段值主子表关联字段值，在这不用管了（包括后括号也不用管了）。	
**/
function fc_insert1(sXml) {
//执行为一对多表插入
//参数串先为主表，后为子表，子表的最后字段值主子表关联字段值，在这不用管了（包括后括号也不用管了）。	
	var retX=SendHttp("http://"+location.host+"/"+servletPath+"/WebPrint?fc_insert1",sXml)
	return retX
}
/**
//执行为一对多表修改
//参数串先为主表，后为子表

**/
function fc_update1(sXml) {
//执行为一对多表修改
//参数串先为主表，后为子表
	var retX=SendHttp("http://"+location.host+"/"+servletPath+"/WebPrint?fc_update1",sXml)
	return retX
}
/**
*执行查询
*@param PageNo 页码
*@param PageSize 页尺寸,即一页含多少行
*@return 查询结果
**/
function fc_select(sSql,PageNo,PageSize) {
//执行查询
//PageNo页码
//PageSize页尺寸,即一页含多少行
	var sql1=""
	for(var i=0;i<sSql.length;i++) {
		switch (sSql.charAt(i)) {
			case "<" :
				sql1=sql1+"&lt;";
				break;
			case ">" :
				sql1=sql1+"&gt;";
				break;
			default:
				sql1=sql1+sSql.charAt(i);
		}
	}
	//替代非法XML字符
	var sXml="<No>"+sql1+"</No>"+"<No1>"+PageNo+"</No1>"+"<No2>"+PageSize+"</No2>"
	var retX=SendHttp("http://"+location.host+"/"+servletPath+"/WebPrint?fc_select",sXml)
	return retX
}
/**
*判断是否为空
**/
function isSpace(strMain){

	strComp=strMain

	if (strComp=="　" || strComp=="" || strComp==" " || strComp==null || strComp.length==0 ) { 
		return true
	}
	else
	{
		return false
	}
}
/**
*和后台通讯函数
*@param sAspFile 后台文件url
*@param sSend 要发送的数据
*@return 处理结果
**/
function SendHttp(sAspFile,sSend)
{
    if (navigator.onLine==false) 
    {
		return "你现在处于脱机状态,请联机后再试!"		
    }
    var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    xmlhttp.Open("POST", sAspFile, false);
	try
	{
	    xmlhttp.Send("<root>"+sSend+"</root>");
	}
	catch (exception)
	{
		alert("服务器忙!")
	}	
	try
	{
		var str11=xmlhttp.responseText
	}
	catch (exception)
	{
		if (exception.description=='系统错误: -1072896748。') 
		{	
			str11=""
		}
			
	}
	return str11	
}
/**
*取得当前日期
**/
function curDate() {
	var dDate=new Date()
	var s1=""+dDate.getYear()
	var s2=dDate.getMonth()+1
	if (s2<10) 
		s2="0"+s2
	else
		s2=""+s2
			
	var s3=dDate.getDate()
	if (s3<10) 
		s3="0"+s3
	else
		s3=""+s3

	return s1+"-"+s2+"-"+s3 
}
//字符串实用函数
function getFront(mainStr,searchStr){
	foundOffset=mainStr.indexOf(searchStr)
	if (foundOffset==-1) {
		return null
	}
	return mainStr.substring(0,foundOffset)
}
function getEnd(mainStr,searchStr) {
	foundOffset=mainStr.indexOf(searchStr)
	if (foundOffset==-1) {
		return null
	}
	return mainStr.substring(foundOffset+searchStr.length,mainStr.length)
}
/**
//代替字符串
//mainStr为源串  searchStr为要查找的串  replaceStr为要改为的串
//返回替换后的串
**/
function replaceString(mainStr,searchStr,replaceStr) {
//代替字符串
//mainStr为源串  searchStr为要查找的串  replaceStr为要改为的串
//返回替换后的串
	var front=getFront(mainStr,searchStr)
	var end=getEnd(mainStr,searchStr)
	if (front!=null && end!=null) {
		return front+replaceStr+end
	}
	return mainStr
}		

function left(mainStr,lngLen) {
	if (lngLen>0) {
		return mainStr.substring(0,lngLen)
	}
	else
	{
		return null
	}
}

function right(mainStr,lngLen) {
//	alert(mainStr.length)
	if (mainStr.length-lngLen>=0 && mainStr.length>=0 && mainStr.length-lngLen<=mainStr.length) {
		return mainStr.substring(mainStr.length-lngLen,mainStr.length)
	}
	else
	{
		return null
	}
}

/**
//滤掉左右空格
**/
function trim(strMain) {
	if (strMain==null) return ""
  var str1=strMain
	
	//去掉回车符
  var ascMain=strMain.charCodeAt(strMain.length-1)
	if (ascMain==32) str1=left(strMain,strMain.length-1)

	if (str1==null) return ""

  for (var i=0;i<=str1.length-1;i++) {
       var mychar=str1.charAt(i);
       if ((mychar!=" ") && (mychar!="　")) {
           str1=str1.substring(i,str1.length);
           break;
           } 
      }

  for (var i=str1.length-1;i>0;i--) {
      var mychar=str1.charAt(i);
      if ((mychar!=" ")  && (mychar!="　")) {
         str1=str1.substring(0,i+1);
         break;
      }
  }

  return str1;
}
/**
//多次替代字符串

**/
function fc_RepStr(mainStr,findStr,replaceStr){
//多次替代
	var iStart=0
	var iEnd=0
	var sRet=""
	while (iStart<mainStr.length) {
		iEnd=mainStr.indexOf(findStr,iStart)
		if (iEnd<0) {
			iEnd=mainStr.length
			sRet=sRet+mainStr.substring(iStart,iEnd)
		}
		else {
			sRet=sRet+mainStr.substring(iStart,iEnd)+replaceStr
		}
		iStart=iEnd+findStr.length
		
	}
	return sRet
}
function fillcombox(sSql) {
//资料选择返回SQL等
//sXml:1方案号 2过滤参数
	var sql1=""
	for(var i=0;i<sSql.length;i++) {
		switch (sSql.charAt(i)) {
			case "<" :
				sql1=sql1+"&lt;";
				break;
			case ">" :
				sql1=sql1+"&gt;";
				break;
			default:
				sql1=sql1+sSql.charAt(i);
		}
	}
	//替代非法XML字符
	var sXml="<No>"+sql1+"</No>"
	
	var retX=SendHttp("http://"+location.host+"/"+servletPath+"/WebPrint?fillcombox",sXml)
	return retX
	//返回的串不带根节点
	//<option>...</option>...
}
function repXml(sSql) {
//替代非法XML字符	
	var sql1=""
	for(var i=0;i<sSql.length;i++) {
		switch (sSql.charAt(i)) {
			case "<" :
				sql1=sql1+"&lt;";
				break;
			case ">" :
				sql1=sql1+"&gt;";
				break;
			default:
				sql1=sql1+sSql.charAt(i);
		}
	}
	return sql1
}



/**
*检查SQL语句是否正确
*@param sSql 被检查的sql语句,字符串
*@return 如正确返回true,sql为空也返回true.
*@date 2003-05-20
**/
function checksql(sSql){
	if(isSpace(sSql)) return true
	
	var sql1=""
	for(var i=0;i<sSql.length;i++) {
		switch (sSql.charAt(i)) {
			case "<" :
				sql1=sql1+"&lt;";
				break;
			case ">" :
				sql1=sql1+"&gt;";
				break;
			default:
				sql1=sql1+sSql.charAt(i);
		}
	}
	//替代非法XML字符
	var sXml="<No>"+sql1+"</No>"
	
	var retX=SendHttp("http://"+location.host+"/"+servletPath+"/WebPrint?checksql",sXml)
	if(isSpace(retX)){ 
		return true
	}else{
		alert("SQL语句非法!")
		return false
	}
}


/**
//执行多个查询,主要实现象带子查询的情况
//sXml为包含条件的XML串
**/
function fc_select1(sXml) {
	var retX=SendHttp("http://"+location.host+"/"+servletPath+"/WebPrint?fc_select1",sXml)
	return retX
}
/**
//执行多个查询,主要实现小计行等汇总数据和明细数据同时要的情况
//sXml为包含条件的XML串
//<page><pageno>1</pageno><pagesize>30</pagesize></page>
//<source><datasource>webgrid</datasource><username>sa</username><password></password></source>
//<mainsql><sql>select fstrCustomerName,fstrItemName,convert(decimal(16,2),fdblQuantity),convert(decimal(16,2),fdblPrice),convert(decimal(16,2), SendOutDetail.fcurAmount) from SendOut Inner Join SendOutDetail On SendOut.flngSendOutID=SendOutDetail.flngSendOutID  where 2000=2000</sql>
//</mainsql>
//<group><sql>select fstrCustomerName,null,convert(decimal(16,2),sum(fdblQuantity)),convert(decimal(16,2),sum(fdblPrice)),convert(decimal(16,2), sum(SendOutDetail.fcurAmount)) from SendOut Inner Join SendOutDetail On SendOut.flngSendOutID=SendOutDetail.flngSendOutID  where 2000=2000 group by SendOut.fstrCustomerName order by SendOut.fstrCustomerName</sql>
//<groupfield>SendOut.fstrCustomerName</groupfield>
//</group>
//<group><sql>select null,SendOutDetail.fstrItemName,convert(decimal(16,2),sum(fdblQuantity)),convert(decimal(16,2),sum(fdblPrice)),convert(decimal(16,2), sum(SendOutDetail.fcurAmount)) from SendOut Inner Join SendOutDetail On SendOut.flngSendOutID=SendOutDetail.flngSendOutID  where 2000=2000 group by SendOutDetail.fstrItemName order by SendOutDetail.fstrItemName</sql>
//<groupfield>SendOutDetail.fstrItemName</groupfield>
//</group>
**/
function fc_select2(sXml) {
/*
sXml="<pageno>1</pageno><pagesize>30</pagesize>"
+"<mainsql><sql>select fstrCustomerName,fstrItemName,convert(decimal(16,2),fdblQuantity),convert(decimal(16,2),fdblPrice),convert(decimal(16,2), SendOutDetail.fcurAmount) from SendOut Inner Join SendOutDetail On SendOut.flngSendOutID=SendOutDetail.flngSendOutID  where 2000=2000</sql>"
+"</mainsql>"
+"<group><sql>select fstrCustomerName,fstrCustomerName,null,convert(decimal(16,2),sum(fdblQuantity)),convert(decimal(16,2),sum(fdblPrice)),convert(decimal(16,2), sum(SendOutDetail.fcurAmount)) from SendOut Inner Join SendOutDetail On SendOut.flngSendOutID=SendOutDetail.flngSendOutID  where 2000=2000 group by SendOut.fstrCustomerName order by SendOut.fstrCustomerName</sql>"
+"<groupfield>SendOut.fstrCustomerName</groupfield>"
+"</group>"
+"<group><sql>select SendOutDetail.fstrItemName,null,SendOutDetail.fstrItemName,convert(decimal(16,2),sum(fdblQuantity)),convert(decimal(16,2),sum(fdblPrice)),convert(decimal(16,2), sum(SendOutDetail.fcurAmount)) from SendOut Inner Join SendOutDetail On SendOut.flngSendOutID=SendOutDetail.flngSendOutID  where 2000=2000 group by SendOutDetail.fstrItemName order by SendOutDetail.fstrItemName</sql>"
+"<groupfield>SendOutDetail.fstrItemName</groupfield>"
+"</group>"
*/
	var retX=SendHttp("http://"+location.host+"/"+servletPath+"/WebPrint?fc_select2",sXml)
	return retX
}
/**
//判断字段sField是否存在于sTable表中,如存在则返回"",
**/
function fc_FieldInTable(sField,sTable) {
	var sXml="<No>"+sField+"</No>"+"<No1>"+sTable+"</No1>"
	var retX=SendHttp("http://"+location.host+"/"+servletPath+"/WebPrint?fc_FieldInTable",sXml)
	return retX
	
}
/**
//插入带备注型的表
**/
function fc_insertClob(sXml){
	var retX=SendHttp("http://"+location.host+"/WebPrint?fc_insertClob",sXml)
	return retX
}
/**
//更新带备注型的表
**/
function fc_updateClob(sXml){
	var retX=SendHttp("http://"+location.host+"/WebPrint?fc_updateClob",sXml)
	return retX
}
/**
//装入带备注型的表
//sXml为带XML标签的表名 如<no>%20dd</no>
**/
function fc_loadClob(sXml){
	var retX=SendHttp("http://"+location.host+"/WebPrint?fc_loadClob",sXml)
	return retX
}
/**
//装入带备注型的表
//sXml为带XML标签的表名 如<no>%20dd</no>
//求fstrtable1的值
**/
function fc_loadClob1(sXml){
	var retX=SendHttp("http://"+location.host+"/WebPrint?fc_loadClob1",sXml)
	return retX
}
/**
*提示非法XML字符
*@date 2003-06-18
*@param checkStr 被检查的字符串
*@return 返回""表示合法,
**/
function checkXmlStr(checkStr){
	var s1="<>"
	var tip="不能包含<>字符,应将<用&lt;替代,将>用&gt;替代."
	if(checkStr.indexOf('<')>=0){
		alert(tip)
		return s1
	}
	if(checkStr.indexOf('>')>=0){
		alert(tip)
		return s1
	}
	return ""
}
/**
*进入日期参照
**/
function selectdate() {
	var ctrlName=event.srcElement.id
	new DateControl20(ctrlName);

}
