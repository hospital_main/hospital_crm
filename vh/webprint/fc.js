///////////////////////接口设置
var prnSet=new prnSet1()
prnSet.ReportName="report"//设置报表名称,注意：不同的报表其名称不能相同

/////////////
prnSet.SetColWidth="<col colKey='第1列' colWidth='60' colSelect='block'/>"+"<col colKey='第2列' colWidth='100' colSelect='block'/>"+"<col colKey='第3列' colWidth='60' colSelect='block'/>"
				+"<col colKey='第4列' colWidth='60' colSelect='block'/>"+"<col colKey='第5列' colWidth='100' colSelect='none'/>"+"<col colKey='第6列' colWidth='60' colSelect='block'/>"
				+"<col colKey='第7列' colWidth='100' colSelect='block'/>"+"<col colKey='第8列' colWidth='100' colSelect='block'/>"+"<col colKey='第9列' colWidth='100' colSelect='none'/>"
				+"<col colKey='第10列' colWidth='100' colSelect='block'/>"
//注意:变量SetColWidth是设置报表初始列宽的,colKey的值是列标题，colWidth的值是该列的列宽，单位是像素（PX），colSelect的值是设置该列是否可见，它有两个值可供选择，如设置为"block"则该列可见，如设置为"none"则为不可见。

////////////////////
prnSet.title1="存货汇总表";
prnSet.title2=""
prnSet.top11=""
prnSet.top12=""
prnSet.top13=""
prnSet.top21=""
prnSet.top22="";
prnSet.top23=""
prnSet.top31=""
prnSet.top32=""
prnSet.top33=""
//===================
prnSet.bottom11=""
prnSet.bottom12=""
prnSet.bottom13=""
prnSet.bottom21=""
prnSet.bottom22=""
prnSet.bottom23=""
prnSet.bottom31=""
prnSet.bottom32=""
prnSet.bottom33=""
prnSet.fontTop=15
prnSet.fontBottom=15

/////////////////////
prnSet.repWidth=787//纸张的宽度
prnSet.repHeight=1114//纸张的高度
prnSet.portrait=true//纵向
prnSet.level=false//水平方向
prnSet.tabTop=50//表格的上边距
prnSet.tabBottom=50//表格的下边距
prnSet.tabLeft=15//表格的左边距
prnSet.tabRight=15//表格的右边距
prnSet.tabHead=1//表头的行数
prnSet.tabCauda=0//表尾的行数
prnSet.topOffset=0//打印机的上偏移量
prnSet.leftOffset=0//打印机的左偏移量
prnSet.coverPrint=false//是否套打
prnSet.AutoturnRow=true//是否自动转行
prnSet.imgsrc=""//图片的地址
prnSet.imgshow=true//是否打印图片的开关
prnSet.imgLeft=0//图片的左边距
prnSet.imgTop=0//图片的上边距
////////////////////////////
prnSet.changeUserDate=true///////////如果为true,就按传来的参数重新刷新缓存中附加信息的值

function prnSet1(alltable,ReportName,SetColWidth,title1,title2,top11,top12,top13
	,top21,top22,top23,top31,top32,top33,bottom11,bottom12
	,bottom13,bottom21,bottom22,bottom23,bottom31,bottom32,bottom33,fontTop
	,fontBottom,repWidth,repHeight,portrait,level,tabTop,tabBottom,tabLeft
	,tabRight,tabHead,tabCauda,topOffset,leftOffset,coverPrint,AutoturnRow,imgsrc
	,imgshow,imgLeft,imgTop,changeUserDate	){
//保存报表条件的数组
	this.alltable=alltable
	this.ReportName=ReportName
	this.SetColWidth=SetColWidth
	this.title1=title1
	this.title2=title2
	this.top11=top11
	this.top12=top12
	this.top13=top13
	this.top21=top21
	this.top22=top22
	this.top23=top23
	this.top31=top31
	this.top32=top32
	this.top33=top33
	this.bottom11=bottom11
	this.bottom12=bottom12
	this.bottom13=bottom13
	this.bottom21=bottom21
	this.bottom22=bottom22
	this.bottom23=bottom23
	this.bottom31=bottom31
	this.bottom32=bottom32
	this.bottom33=bottom33
	this.fontTop=fontTop
	this.fontBottom=fontBottom
	this.repWidth=repWidth
	this.repHeight=repHeight
	this.portrait=portrait
	this.level=level
	this.tabTop=tabTop
	this.tabBottom=tabBottom
	this.tabLeft=tabLeft
	this.tabRight=tabRight
	this.tabHead=tabHead
	this.tabCauda=tabCauda
	this.topOffset=topOffset
	this.leftOffset=leftOffset
	this.coverPrint=coverPrint
	this.AutoturnRow=AutoturnRow
	this.imgsrc=imgsrc
	this.imgshow=imgshow
	this.imgLeft=imgLeft
	this.imgTop=imgTop
	this.changeUserDate=changeUserDate
	
}	

function cmdPrint() {
/*
	var arrParameter=new Array() //存每页的HTML内容
	if(t.style.display=="none"){
	//	t.style.display="block"
		arrParameter[0]=t.outerHTML
	//	t.style.display="none"
	}
	else {
		arrParameter[0]=t.outerHTML
	
	}
	arrParameter[1]=ReportName
	arrParameter[2]=SetColWidth
	/////////////////////=======================
	arrParameter[3]=title1	
	arrParameter[4]=title2
	arrParameter[5]=top11
	arrParameter[6]=top12
	arrParameter[7]=top13
	arrParameter[8]=top21
	arrParameter[9]=top22
	arrParameter[10]=top23
	arrParameter[11]=top31
	arrParameter[12]=top32
	arrParameter[13]=top33
	arrParameter[14]=bottom11
	arrParameter[15]=bottom12
	arrParameter[16]=bottom13
	arrParameter[17]=bottom21
	arrParameter[18]=bottom22
	arrParameter[19]=bottom23
	arrParameter[20]=bottom31
	arrParameter[21]=bottom32
	arrParameter[22]=bottom33
	arrParameter[23]=fontTop
	arrParameter[24]=fontBottom
	/////////////////////===========================
	arrParameter[25]=repWidth
	arrParameter[26]=repHeight
	arrParameter[27]=portrait
	arrParameter[28]=level
	arrParameter[29]=tabTop
	arrParameter[30]=tabBottom
	arrParameter[31]=tabLeft
	arrParameter[32]=tabRight
	arrParameter[33]=tabHead
	arrParameter[34]=tabCauda
	arrParameter[35]=topOffset
	arrParameter[36]=leftOffset
	arrParameter[37]=coverPrint
	arrParameter[38]=AutoturnRow
	arrParameter[39]=imgsrc
	arrParameter[40]=imgshow
	arrParameter[41]=imgLeft
	arrParameter[42]=imgLeft
	arrParameter[43]=changeUserDate
	*/
	
	var screenHeight=window.screen.height;
	var screenWidth=window.screen.width;
	var dialogStyle="dialogHeight:"+screenHeight+"px;dialogWidth:"+screenWidth+"px;dialogTop:0px;dialogLeft:0px;status:no;scroll:no"
	var sRet=window.showModalDialog("webprint\\dsnprint.htm",prnSet,dialogStyle) 
}