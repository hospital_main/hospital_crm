<?xml version="1.0" encoding="gb2312"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">
<xsl:template match="/">
<TABLE>
<xsl:for-each select="root/record" order-by="-number(col0)" >
<TR>
<TD><xsl:value-of select="row0"/></TD>
<TD><xsl:value-of select="col0"/></TD>
<TD><xsl:value-of select="row1"/></TD>
<TD><xsl:value-of select="col1"/></TD>
</TR>
</xsl:for-each>
</TABLE>	  	 
</xsl:template>
</xsl:stylesheet>
