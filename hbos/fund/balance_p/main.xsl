
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">       
				<th rowspan="2" valign="center">项目编码</th>
				<th rowspan="2" valign="center">项目名称</th>
				<th rowspan="2" valign="center">项目负责人</th>
				<th rowspan="2" valign="center">项目状态</th>
				<th colspan="4">科研账</th>							
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th rowspan="2" >会计支出</th>						
  		</tr>
  		<tr noWrap="true" class="mainHead">       
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>	
				<th>期初额度</th>				
				<th>本期增加</th>			
				<th>本期支出</th>
				<th>本期余额</th>				
				<th style="display:none"></th>	
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:if test="count(/root/tbody/tr) &gt;1">
	    <xsl:for-each select="/root/tbody/tr">
	    	 
        <tr>
        
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td align="left">			           
			                  <a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					           <!--javascript:openDialog('update_project.html?load=&lt;proj_code&gt;<xsl:value-of select="."/>&lt;/proj_code&gt;', 'dialogWidth:1000px;dialogHeight:500px', result)-->
					           javascript:openDialog("../manage/info/update.html?load='<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>'", "dialogWidth:1000px;dialogHeight:500px", result)
					             
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			                </a>	                
			            </td>
                </xsl:when>
                <xsl:when test="position()=2 or position()=3 or position()=4">
			            <td><xsl:value-of select="."/></td>
                </xsl:when> 
                
              <xsl:when test="position()=6 and .!=0  and ../td[2]!='合 计'">
			            <td align="right">
			            	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					           javascript:benqizengjia('&lt;id&gt;<xsl:value-of select="../pk/id"/>&lt;/id&gt;&lt;fund_source_code&gt;02&lt;/fund_source_code&gt;')
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'0.00')"/>
			                </a>		                	
			            </td>
                </xsl:when> 
   
                  <xsl:when test="position()=7 and .!=0 and ../td[2]!='合 计'">
			            <td align="right">
			            	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					           javascript:payOut('&lt;id&gt;<xsl:value-of select="../pk/id"/>&lt;/id&gt;&lt;fund_source_code&gt;02&lt;/fund_source_code&gt;')
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'0.00')"/>
			                </a>		                	
			            </td>
                </xsl:when>   
                
			  	 <xsl:otherwise>
			  	 		<td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>			           
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr> 			
  		
   		</xsl:for-each>  
   	  </xsl:if>	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
