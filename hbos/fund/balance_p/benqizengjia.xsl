
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">     
				<th>单据编号</th>
				<th>项目编码</th>
				<th>项目名称</th>
				<th>制单日期</th>
				<th>摘要</th>
				<th>收入金额</th>					
				<th>状态</th>			
				<th>制单人</th>
				<th>审核人</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:if test="count(/root/tbody/tr) &gt;1">
	    <xsl:for-each select="/root/tbody/tr">
	    	 
        <tr>
           
          <xsl:for-each select="td">
              <xsl:choose>
                 
                 <xsl:when test="position() = 1 and ../td[3]!='合 计'">
					<td align="left" >
						
							<xsl:value-of select="."/>
						
					</td>
				</xsl:when>
                                
                <xsl:when test="position()=6">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                
			  	 <xsl:otherwise>
			            <td >
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr> 			
  		
   		</xsl:for-each>  
   	  </xsl:if>	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
