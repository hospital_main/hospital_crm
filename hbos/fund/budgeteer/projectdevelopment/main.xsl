
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>项目编码</th>
				<th>项目名称</th>
				<th>凭证号</th>
				<th>立项年度</th>
				<th>资金来源</th>
				<th>支出项目</th>				
				<th>预算金额</th>
				<th>编制日期</th>
				<th>项目状态</th>
				<th>审核状态</th>
				<th>预算状态</th>
				<th>项目负责人</th>
				<th>预算编制人</th>
				<th>预算审核人</th>			
  		</tr>
  	</thead>
  	<tbody>
  	 	<xsl:if test="count(/root/tbody/tr) &gt;1">
	    <xsl:for-each select="/root/tbody/tr">	 
        <tr>    
         <xsl:if  test="td[2]!='合 计'">
         <td align='center'>            
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      		  </xsl:attribute>
    	  </input>
          </td>
          </xsl:if>
          
          <xsl:if  test="td[2]='合 计'">
          <td></td>
          </xsl:if>
           
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td align="left">			           
			                  <a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					           javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:1000px;dialogHeight:700px', result)
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			                </a>	                
			            </td>
                </xsl:when>
                <xsl:when test="position()=7">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                
			  	 <xsl:otherwise>
			            <td >
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
		
  			</tr> 			
  		
   		</xsl:for-each>  
   		  </xsl:if> 	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
