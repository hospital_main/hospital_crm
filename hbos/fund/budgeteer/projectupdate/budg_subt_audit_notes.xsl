<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style='display:none' width='25'><input type='checkbox'/></th>
				<th>预算年度</th>
				<th>批复日期</th>
				<th>批复文号</th>
				<th>批复意见</th>
				<th>批复金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
          <td align='center'  noWrap='true'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1 or position()=2">
								<td align="left" noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()=3">
								<td noWrap="true" align="left">
									<a tabindex='-1'><xsl:value-of select="."/></a>
								</td>
							</xsl:when>
							<!--xsl:when test="position()=5">
								<td align="center" noWrap="true">
									<a tabindex="-2">
		                <xsl:attribute name="href" >
		                  javascript:openDialog('docList.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:600px;dialogHeight:500px', result)
		                </xsl:attribute><xsl:value-of select="."/></a>
								</td>
							</xsl:when-->
							
							<xsl:when test="position()=5">
							<td align="right" ><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							
							<xsl:otherwise>
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
