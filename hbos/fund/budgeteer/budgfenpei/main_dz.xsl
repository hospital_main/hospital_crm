<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<content><![CDATA[20<br />]]></content>
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
  		<!--th style='display:none' width='25'><input type='checkbox'/></th-->
  				<th>操作</th>				
				<th>到账日期</th>
				<th>摘要</th>	
				<th>到账金额</th>
				<th>项目编码</th>
				<th>项目状态</th>		
				<th>项目名称</th>
						
  		</tr>
  	</thead>
  	<tbody>
	  	<xsl:if test="count(/root/tbody/tr) &gt;1">
	  	
	    <xsl:for-each select="/root/tbody/tr">
        <tr>    
        <xsl:if  test="td[2]!='合 计'">
          <td>
	  		<a href="#">
			<xsl:attribute name="onclick">
					openBudgPage("<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;summary&gt;<xsl:value-of select="td[2]"/>&lt;/summary&gt;&lt;change_date&gt;<xsl:value-of select="td[1]"/>&lt;/change_date&gt;")
			</xsl:attribute>分配预算
			</a>
  		</td>  	
         
          <xsl:for-each select="td">
              <xsl:choose>               
                <xsl:when test="position()=3">
			            <td align="right">               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                <xsl:when test="position()=4">
			            <td align="left">			           
			                  <a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					           javascript:openDialog('update_project.html?load=&lt;proj_code&gt;<xsl:value-of select="."/>&lt;/proj_code&gt;', 'dialogWidth:1000px;dialogHeight:500px', result)
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			                </a>	                
			            </td>
                </xsl:when>
                <xsl:when test="position()=2 or position()=6">
                	<td><xsl:value-of select="."/></td>
                </xsl:when>
			  	<xsl:otherwise>
			            <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  		  </xsl:for-each> 
  		           
        </xsl:if>
        
         <xsl:if  test="td[2]='合 计'">
        <td></td> 
         
         <xsl:for-each select="td">
              <xsl:choose>               
                <xsl:when test="position()=3">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>                
			  	      <xsl:otherwise>
			            <td ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  		  </xsl:for-each> 
  		  
  	 	
         
  		 </xsl:if>	
  		 
  			  
  		</tr> 	  	
  				
   		</xsl:for-each>     	
   	 </xsl:if> 
   	 
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
