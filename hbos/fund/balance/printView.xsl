<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<table>
  	<thead>
  		<tr noWrap="true" class="mainHead" >       
				<td style="colspan:13;fontsize:maintitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			
  		</tr>  	
  		<tr noWrap="true" class="mainHead">       
				<td rowspan="2" valign="center">项目编码</td>
				<td rowspan="2" valign="center">项目名称</td>
				<td rowspan="2" valign="center">经费来源</td>
				<td rowspan="2" valign="center">项目负责人</td>
				<td rowspan="2" valign="center">项目状态</td>
				<td colspan="4">科研账</td>							
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan="4">会计账</td>						
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				
  		</tr>
  		<tr noWrap="true" class="mainHead">       
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>	
				<td style="display:none"></td>
				<td>期初余额</td>				
				<td>本期增加</td>
				<td>本期支付</td>				
				<td>期末余额</td>
				<!--td>非支付状态数</td-->	
				<td>期初余额</td>				
				<td>本期增加</td>
				<td>本期支出</td>				
				<td>期末余额</td>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:if test="count(/root/tbody/tr) &gt;1">
	    <xsl:for-each select="/root/tbody/tr">
	    	 
        <tr>
        <!--xsl:if  test="td[2]!='合 计'">
          <td align='center'>                   
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    		  </input>  
          </td>
          </xsl:if>
          
          <xsl:if  test="td[2]='合 计'">
          <td></td>
           </xsl:if-->
           
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td align="center">			           
			                  <a tabindex='-1'>
				  	          <xsl:value-of select="."/>
			                </a>	                
			            </td>
                </xsl:when>
                <xsl:when test="position()=2 or position()=3 or position()=4 or position()=5">
			            <td><xsl:value-of select="."/></td>
                </xsl:when> 
                
                <xsl:when test="position()>=6 and .&gt;0  and ../td[2]!='合 计'">
			            <td align="right">
			            	<a tabindex='-1'>					          
				  	          <xsl:value-of select="format-number(.,'0.00')"/>
			                </a>		                	
			            </td>
                </xsl:when>
                
			  	 <xsl:otherwise>
			  	 		<td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>			           
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr> 			
  		
   		</xsl:for-each>  
   	  </xsl:if>	
  	</tbody>
</table>
</xsl:template>
</xsl:stylesheet>
