<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>凭证编号</th>
				<th>凭证日期</th>
				<th>摘要</th>	
				<th>金额</th>
				<th>项目状态</th>		
				<th>项目名称</th>							
				<th>科目编码</th>
				<th>科目名称</th>
  		</tr>
  	</thead>
  	<tbody>
	  	<xsl:if test="count(/root/tbody/tr) &gt;1">
	    <xsl:for-each select="/root/tbody/tr">
        <tr>    
          <xsl:for-each select="td">
              <xsl:choose>               
                <xsl:when test="position()=4">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>                
			  	      <xsl:otherwise>
			            <td ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
		
  			</tr> 			
  		
   		</xsl:for-each>  
   	 </xsl:if> 
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
