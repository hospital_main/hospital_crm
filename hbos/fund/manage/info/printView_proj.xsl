<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
   <root>
    
    	<thead>
    	<!--
       	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
	  		
	-->
	 			<tr noWrap="true" class="mainHead">
					<td>项目编码</td>
					<td>项目名称</td>
					<td>项目类型</td>
					<td>财务编码</td>
					<td>申请科室</td>
					<td>方向</td>
					<td>项目级别</td>
					<td>课题来源</td>				
					<td>预算资金总额</td>
					<td>状态</td>
					<td>项目负责人</td>
					<td>财务负责人</td>
					<td>申请人</td>
					<td>审核人</td>
				</tr>
   		</thead>
   
	  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr">
	  			<tr>
					<xsl:for-each select="td">
	           <xsl:choose>
                <xsl:when test="position()=9">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               	</td>
                </xsl:when>
			  	 			<xsl:otherwise>
				            <td nowrap='true'><xsl:value-of select="."/></td>
                </xsl:otherwise>
             </xsl:choose>
					</xsl:for-each>
					</tr>
			</xsl:for-each>
	  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>