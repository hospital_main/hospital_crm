<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="idex1" select="count(/root/tbody/tr)"/>   
  		<tr noWrap="true" class="mainHead">     
  		 <th ><input type="checkbox" width="10px"/></th>
  		 		<th nowrap='true' width="53px">发票序号</th>
  		 		<th nowrap='true'>报销单据号</th>
				<th nowrap='true'>发票号码</th>				
				<th nowrap='true'>发票日期</th>
				<th nowrap='true'>摘要</th>
				<th nowrap='true'>单位</th>
				<th nowrap='true'>金额</th>
				<th nowrap='true'>备注</th>			 	
			 		 
			</tr>
  	</thead>
  	<tbody>
  	
	  		<xsl:if test="count(/root/tbody/tr) &gt;1">	  		
	  		<xsl:for-each select="/root/tbody/tr">
			<tr>
			   <xsl:if  test="td[2]!='合 计'">		
				 <td align='center' width="10px">
	            	<input type='checkbox' TABINDEX='-1' >
	              	<xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			 </xsl:attribute>
	    			 </input>
	          	</td>	
	          	
	          		<xsl:for-each select="td">  		
				
				 <xsl:choose>
	              <xsl:when test="position()=1">
	                <td align="left">               
	             
	                <a tabindex='-1'>
			           <xsl:attribute name="onclick" >
			           	javascript:openDialog('bill_update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:900px;dialogHeight:400px', result)
		  	          </xsl:attribute>
		  	          <xsl:attribute name="href" >#</xsl:attribute>
		  	          <xsl:value-of select="."/>
	                </a>	                
	                </td>
	              </xsl:when>	             
	              
	               <xsl:when test="position()=7">
	                <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	                </td>
	              </xsl:when>
	              
	               <xsl:otherwise>				
				<td align="left"><xsl:value-of select="."/></td>
				</xsl:otherwise>
				 </xsl:choose>
				 
				</xsl:for-each>			
	          			
          	   </xsl:if>
          	  
          	    <xsl:if  test="td[2]='合 计'">	
          	    <td></td>
          	    <xsl:for-each select="td">
          	   	 <xsl:choose>
	              <xsl:when test="position()=1">
	                <td></td>
	              </xsl:when>	             
	              
	               <xsl:when test="position()=7">
	                <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	                </td>
	              </xsl:when>
	              
	               <xsl:otherwise>				
				<td align="left"><xsl:value-of select="."/></td>
				</xsl:otherwise>
				 </xsl:choose>
				 	</xsl:for-each>	
          	      </xsl:if>
          	         
			
			</tr>
		
			</xsl:for-each>
			</xsl:if>	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
