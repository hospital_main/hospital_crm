<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <table>
  	<thead>
  		<xsl:variable name="idex1" select="count(/root/tbody/tr)"/>
  		<tr noWrap='true' class='mainHead'>
				<td style="colspan:8;fontsize:maintitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
		</tr>
  		
  		<tr noWrap="true" class="mainHead">     
  		 		<td nowrap='true' widtd="50px">发票序号</td>
  		 		<td nowrap='true'>报销单据号</td>
				<td nowrap='true'>发票号码</td>				
				<td nowrap='true'>发票日期</td>
				<td nowrap='true'>摘要</td>
				<td nowrap='true'>单位</td>
				<td nowrap='true'>金额</td>
				<td nowrap='true'>备注</td>			 	
			 		 
			</tr>
  	</thead>
  	<tbody>
  	
	  		<xsl:if test="count(/root/tbody/tr) &gt;1">	  		
	  		<xsl:for-each select="/root/tbody/tr">
			<tr>
			   <xsl:if  test="td[2]!='合 计'">		
					          	
	          <xsl:for-each select="td">  		
				
				 <xsl:choose>
	              <xsl:when test="position()=1">
	                <td align="left">               
	             
	                <!--a tabindex='-1'>
			           <xsl:attribute name="onclick" >
			           	javascript:openDialog('bill_update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:900px;dialogHeight:400px', result)
		  	          </xsl:attribute>
		  	          <xsl:attribute name="href" >#</xsl:attribute>
		  	          <xsl:value-of select="."/>
	                </a-->	  
	                <xsl:value-of select="."/>              
	                </td>
	              </xsl:when>	             
	              
	               <xsl:when test="position()=7">
	                <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	                </td>
	              </xsl:when>
	              
	               <xsl:otherwise>				
				<td align="left"><xsl:value-of select="."/></td>
				</xsl:otherwise>
				 </xsl:choose>
				 
				</xsl:for-each>			
	          			
          	   </xsl:if>
          	  
          	    <xsl:if  test="td[2]='合 计'">	
          	    <xsl:for-each select="td">
          	   	 <xsl:choose>
	              <xsl:when test="position()=1">
	                <td></td>
	              </xsl:when>	             
	              
	               <xsl:when test="position()=7">
	                <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	                </td>
	              </xsl:when>
	              
	               <xsl:otherwise>				
				<td align="left"><xsl:value-of select="."/></td>
				</xsl:otherwise>
				 </xsl:choose>
				 	</xsl:for-each>	
          	      </xsl:if>
          	         
			
			</tr>
		
			</xsl:for-each>
			</xsl:if>	
  	</tbody>
  	</table>
	</xsl:template>
</xsl:stylesheet>
