
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>单据编号</th>
				<th>项目编码</th>
				<th>项目名称</th>
				<th>申请日期</th>
				<th>报销事由</th>
				<th>报销人</th>
				<th>申请金额</th>
				<th>报销金额</th>
				<th>状态</th>
				<th>审核人</th>
				<th>支付人</th>
				<th>处理意见</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	 
        <tr>
        <xsl:if  test="td[3]!='合 计'">
          <td align='center'>                   
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/pay_id">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    		  </input>  
          </td>
          
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td align="left">			           
			                  <a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					           javascript:openDialog('update.html?load=<xsl:for-each select="../pk/pay_id">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:1000px;dialogHeight:700px', result)
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			                </a>	                
			            </td>
                </xsl:when>
                
                 <xsl:when test="position()=2">
			            <td align="left">			           
			                  <a tabindex='-1'>
					           <xsl:attribute name="onclick" >
							   javascript:projSelect("&lt;id&gt;<xsl:value-of select="../pk/id"/>&lt;/id&gt;&lt;proj_code&gt;<xsl:value-of select="../pk/proj_code"/>&lt;/proj_code&gt;")
					         
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			                </a>	                
			            </td>
                </xsl:when>
                
                <xsl:when test="position()=3">
			            <td align="left" width="300px">               
	                    <xsl:value-of select="."/>	                	                
	               		 </td>
                </xsl:when>
                
                <xsl:when test="position()=7">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                
                <xsl:when test="position()=12">
                	
                   <td align="left">			           
				               <a tabindex='-1'>
						             <xsl:attribute name="onclick" >
								          	javascript:openDialog('remark.html?load=<xsl:for-each select="../pk/pay_id">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:700px;dialogHeight:300px', result)
						             </xsl:attribute>
						  	         <xsl:attribute name="href" >#</xsl:attribute>
						  	         <xsl:value-of select="."/>
				               </a>	                
				           </td>
				           
                </xsl:when>
                
			  	      <xsl:otherwise>
			            <td >
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>        
          
          </xsl:if>
          
          
          <xsl:if  test="td[3]='合 计'">
          <td></td>
              <xsl:for-each select="td">
              <xsl:choose>
              	 <xsl:when test="position()=1">
			<td/>
		</xsl:when>
                 <xsl:when test="position()=7">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                
			  	 <xsl:otherwise>
			            <td >
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>          
      		 </xsl:choose>
      		 </xsl:for-each>        
           </xsl:if>
           
           
           
        	</tr>   		
   		</xsl:for-each>  
   	  
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
