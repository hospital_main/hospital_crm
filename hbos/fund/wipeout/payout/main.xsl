<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>单据编号</th>
				<th>项目编码</th>
				<th>项目名称</th>
				
				<th>申请日期</th>
				<th>报销事由</th>					
				<th>报销人</th>
				<th>报销金额</th>
				<th>状态</th>			
				<th>审核人</th>
				<th>审核日期</th>
				<th>支付人</th>
				<th>支付日期</th>
				<th>凭证号</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:if  test="td[3]!='合 计'">
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td align="left">			           
			                  <a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					           javascript:openDialog('update.html?load=<xsl:for-each select="../pk/pay_id">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:1000px;dialogHeight:700px', result)
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			                </a>	                
			            </td>
                </xsl:when>
                 <xsl:when test="position()=2">
			            <td align="left">			           
			                  <a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					          javascript:projSelect("&lt;id&gt;<xsl:value-of select="../pk/id"/>&lt;/id&gt;&lt;proj_code&gt;<xsl:value-of select="../pk/proj_code"/>&lt;/proj_code&gt;")
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			                </a>	                
			            </td>
                </xsl:when>
                
                <xsl:when test="position()=7">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                
                <xsl:when test="position()=13">
                	<xsl:if test="../td[13]!='' and ../td[14]!='null' ">
	                  <td noWrap="true">
	                    <a>
	  						<xsl:attribute name="onclick">
	  							javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../td[14]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
	  						</xsl:attribute>
	  						<xsl:attribute name="href" >#</xsl:attribute>
		  						<xsl:value-of select="."/>
		  					</a>
	                  </td>
                  </xsl:if>
                  <xsl:if test="../td[13]=''">
                  		<td><xsl:value-of select="."/></td>
                  </xsl:if>
                </xsl:when>
                
                <xsl:when test="position()=14">
                </xsl:when>
                
			  	 <xsl:otherwise>
			            <td >
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
          </xsl:if>
          
          <xsl:if  test="td[3]='合 计'">
            <xsl:for-each select="td">
              <xsl:choose>
                 <xsl:when test="position()=7">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                
                <xsl:when test="position()=14">
                </xsl:when>
                
			  	 <xsl:otherwise>
			            <td >
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>          
      		 </xsl:choose>
      		 </xsl:for-each>        
           </xsl:if>           
        
  			</tr> 			
  		
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
