<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<table>
  	<thead>
  		<tr noWrap="true" class="mainHead" >       
				<td style="colspan:17;fontsize:maintitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>  	
  		<tr noWrap="true" class="mainHead">   
  				<td>单据编号</td>
				<td>项目编码</td>
				<td>项目名称</td>
				<td>申请日期</td>
				<td>报销事由</td>
				<td>资金来源</td>
				<td>支出项目</td>
				<td>发票号码</td>
				<td>发票日期</td>
				<td>报销人</td>
				<td>报销金额</td>
				<td>状态</td>			
				<td>审核人</td>
				<td>审核日期</td>
				<td>支付人</td>
				<td>支付日期</td>
				<td>凭证号</td>
  		</tr>
  	
  	</thead>
  	<tbody>
  		<xsl:if test="count(/root/tbody/tr) &gt;1">
	    <xsl:for-each select="/root/tbody/tr">
	    	 
        <tr>
        <!--xsl:if  test="td[2]!='合 计'">
          <td align='center'>                   
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    		  </input>  
          </td>
          </xsl:if>
          
          <xsl:if  test="td[2]='合 计'">
          <td></td>
           </xsl:if-->
           
          <xsl:for-each select="td">
              <xsl:choose>                               
                <xsl:when test="position()=11">
			            <td align="right">
		                	<xsl:value-of select="format-number(.,'0.00')"/>
			            </td>
                </xsl:when>              
                
			  	 <xsl:otherwise>
			  	 		<td align="left" >               
	                    <xsl:value-of select="."/>	                	                
	               		 </td>			           
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr> 			
  		
   		</xsl:for-each>  
   	  </xsl:if>	
  	</tbody>
</table>
</xsl:template>
</xsl:stylesheet>
