
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th>选择</th>
				<th>凭证编号</th>
				<th>凭证日期</th>
				<th>摘要</th>	
				<th>金额</th>
				<th>项目名称</th>
				<th>负责人</th>						
				<th>科目编码</th>
				<th>科目名称</th>
  		</tr>
  	</thead>
  	<tbody>
  	
	    <xsl:for-each select="/root/tbody/tr">	 
        <tr>    
       
         <td align="center" >
						<input type="radio" TABINDEX="-1" name="checkedradio" >
							<xsl:attribute name="value"><xsl:for-each select="pk/*"><xsl:value-of select="."/>;</xsl:for-each></xsl:attribute>
						</input>
			</td>
        
           
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			           	<td>
								 <a href="#">
								 <xsl:attribute name="onclick">
								 openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;&lt;/edit_mask&gt;")
								 </xsl:attribute>
								<xsl:value-of select="."/>
								</a>
							
	                  </td>
                </xsl:when>
                <xsl:when test="position()=4">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                
			  	 <xsl:otherwise>
			            <td ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
		
  			</tr> 			
  		
   		</xsl:for-each>  
   	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
