<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
	  <table>
	  	<thead>
	  		<xsl:variable name="idex1" select="count(/root/tbody/tr)"/>   
				<tr noWrap="true" class="mainHead">     
			 		<td style="colspan:14;fontsize:maintitle"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap="true" class="mainHead">     
			 		<td nowrap='true'>项目编码</td>
					<td nowrap='true'>项目名称</td>
					<td nowrap='true'>经费来源</td>
					<td nowrap='true'>项目类型</td>
					<td nowrap='true'>财政用途</td>
					<td nowrap='true'>项目负责人</td>
					<td nowrap='true'>报销单据号</td>
					<td nowrap='true'>报销科室</td>
					<td nowrap='true'>报销人</td>
					<td nowrap='true'>合同编号</td>
					<td nowrap='true'>支付方式</td>
					<td nowrap='true'>发票单位</td>
					<td nowrap='true'>报销状态</td>
					<td nowrap='true'>支出项目</td>
					<td nowrap='true'>发票金额</td>
					<td nowrap='true'>发票号码</td>				
					<td nowrap='true'>发票日期</td>
				</tr>
	  	</thead>
	  	<tbody> 		
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:if  test="td[1]!='合 计'">		
							<xsl:for-each select="td">  		
								<xsl:choose>
									<xsl:when test="position()=15">
										<td align="right" >               
										<xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
										</td>
									</xsl:when>
									<xsl:otherwise>				
										<td align="left"><xsl:value-of select="."/></td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>				
						</xsl:if>
						<xsl:if  test="td[1]='合 计'">	
							<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=15">
										<td align="right" >               
											<xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
										</td>
									</xsl:when>
									<xsl:otherwise>				
										<td align="left"><xsl:value-of select="."/></td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>	
						</xsl:if>
					</tr>
				</xsl:for-each>
			</tbody>
  	</table>
	</xsl:template>
</xsl:stylesheet>
