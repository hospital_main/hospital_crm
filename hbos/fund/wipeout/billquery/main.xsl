<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="idex1" select="count(/root/tbody/tr)"/>   
  		<tr noWrap="true" class="mainHead"> 
  			<th nowrap='true'>报销单据号</th>    
				<th nowrap='true'>项目编码</th>
				<th nowrap='true'>项目名称</th>
				<th nowrap='true'>经费来源</th>
				<th nowrap='true'>项目类型</th>
				<th nowrap='true'>财政用途</th>
				<th nowrap='true'>项目负责人</th>
				<th nowrap='true'>报销科室</th>
				<th nowrap='true'>报销人</th>
				<th nowrap='true'>合同编号</th>
				<th nowrap='true'>支付方式</th>
				<th nowrap='true'>发票单位</th>
				<th nowrap='true'>报销状态</th>
				<th nowrap='true'>支出项目</th>
				<th nowrap='true'>发票金额</th>
				<th nowrap='true'>发票号码</th>				
				<th nowrap='true'>发票日期</th>
			</tr>
  	</thead>
  	<tbody>	  		
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:if  test="td[1]!='合 计'">		
						<xsl:for-each select="td">  		
							<xsl:choose>
								<xsl:when test="position()=2">
									<td align="left">               
										<a tabindex='-1'>
											<xsl:attribute name="onclick" >
												javascript:projSelect("&lt;id&gt;<xsl:value-of select="../pk/id"/>&lt;/id&gt;&lt;proj_code&gt;<xsl:value-of select="../pk/proj_code"/>&lt;/proj_code&gt;")
											</xsl:attribute>
											<xsl:attribute name="href" >#</xsl:attribute>
											<xsl:value-of select="."/>
										</a>	                
									</td>
								</xsl:when>
								<xsl:when test="position()=1">
									<td align="left">               
										<a tabindex='-1'>
											<xsl:attribute name="onclick" >
												javascript:openDialog('payout_update2.html?load=<xsl:for-each select="../pk/pay_id">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:950px;dialogHeight:700px', result)
											</xsl:attribute>
											<xsl:attribute name="href" >#</xsl:attribute>
											<xsl:value-of select="."/>
										</a>	                
									</td>
								</xsl:when>	            
								<xsl:when test="position()=15">
									<td align="right" >               
										<xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
									</td>
								</xsl:when>
								<xsl:otherwise>				
									<td align="left"><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>			
					</xsl:if>
					<xsl:if  test="td[1]='合 计'">	
						<xsl:for-each select="td">
							<xsl:choose>            
								<xsl:when test="position()=15">
									<td align="right" >               
										<xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
									</td>
								</xsl:when>
								<xsl:otherwise>				
									<td align="left"><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>	
					</xsl:if>
				</tr>
			</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
