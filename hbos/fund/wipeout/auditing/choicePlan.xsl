<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
        <th>计划编号</th>
        <th>编制部门</th>
        <th>编制日期</th>
        <th>业务编号</th>
        <th>计划日期</th>
        <th>业务科室</th>
        <th>资金项目</th>
        <th>计划金额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'><input type='radio' name='select' onclick='choose(this)'>
            <xsl:attribute name="plan_detail_id" > <xsl:value-of select="pk/plan_detail_id"/> </xsl:attribute>
            <xsl:attribute name="adjust_detail_id" > <xsl:value-of select="pk/adjust_detail_id"/> </xsl:attribute>
            </input></td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=8">
                <td align="right" ><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
