<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <table>
  	<thead>
  		<xsl:variable name="idex1" select="count(/root/tbody/tr)"/>
  		<tr noWrap='true' class='mainHead'>
				<td style="colspan:10;fontsize:maintitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				
			</tr>
  		<tr noWrap="true" class="mainHead">     
  		 	<td>单据编号</td>
				<td>项目编码</td>
				<td>项目名称</td>
				<th>经费来源</th>
				<th>项目类型</th>
				<td>申请日期</td>
				<td>报销事由</td>					
				<td>报销人</td>
				<td>报销金额</td>
				<td>状态</td>			
				<td>审核人</td>
				<td>审核日期</td>
				 
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	 
        <tr>
        <xsl:if  test="td[3]!='合 计'">
           <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td align="left">			           
				  	          <xsl:value-of select="."/>             
			            </td>
                </xsl:when>
                
                 <xsl:when test="position()=2">
			            <td align="left">			           
				  	          <xsl:value-of select="."/>
			            </td>
                </xsl:when>
                
                <xsl:when test="position()=7">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                
			  	 <xsl:otherwise>
			            <td >
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
          
          
          </xsl:if>
          
         <xsl:if  test="td[3]='合 计'">        
              <xsl:for-each select="td">
              <xsl:choose>
                 <xsl:when test="position()=7">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                
			  	 <xsl:otherwise>
			            <td >
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>          
      		 </xsl:choose>
      		 </xsl:for-each>        
           </xsl:if>           
         
  			</tr> 			
  		
   		</xsl:for-each>  
  	</tbody>
  	</table>
	</xsl:template>
</xsl:stylesheet>
