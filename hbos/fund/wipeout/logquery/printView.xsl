<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <table>
  	<thead>
  		<xsl:variable name="idex1" select="count(/root/tbody/tr)"/>   
  			<tr noWrap="true" class="mainHead">     
  		 		<td style="colspan:4;fontsize:maintitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			
  		<tr noWrap="true" class="mainHead">     
  		 		<td nowrap='true'>单据编号</td>
  		 		<td nowrap='true'>操作人</td>
  		 		<td nowrap='true'>操作时间</td>
				<td nowrap='true'>操作状态</td>				
				<td nowrap='true'>操作记录</td>
			</tr>
  	</thead>
  	<tbody>
  	
	  	<xsl:for-each select="/root/tbody/tr">
			<tr>
	          	<xsl:for-each select="td">  		
				<td align="left"><xsl:value-of select="."/></td>
				</xsl:for-each>	
			</tr>	 	
			</xsl:for-each>	
  	</tbody>
  	</table>
	</xsl:template>
</xsl:stylesheet>
