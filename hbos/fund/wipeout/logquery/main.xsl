<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="idex1" select="count(/root/tbody/tr)"/>   
  		<tr noWrap="true" class="mainHead">     
  		 		<th nowrap='true'>单据编号</th>
  		 		<th nowrap='true'>操作人</th>
  		 		<th nowrap='true'>操作时间</th>
				<th nowrap='true'>操作状态</th>				
				<th nowrap='true'>操作记录</th>
			</tr>
  	</thead>
  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr">
			<tr>
	          	<xsl:for-each select="td">  		
				<td align="left"><xsl:value-of select="."/></td>
				</xsl:for-each>	
			</tr>	 	
			</xsl:for-each>			
          	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
