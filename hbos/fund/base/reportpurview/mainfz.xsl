<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
  		<th>用户编码</th>
  		<th>用户名称</th>
  		<th>是否授权</th>
  	 </tr>
   </thead>
   <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>          
           <td align='center'>             
  						<input type='checkbox' TABINDEX='-1' >
  							<xsl:attribute name="value" >
  								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
  							</xsl:attribute>
  								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
  						</input>				           
          </td>
         <xsl:for-each select="td">
          		<xsl:choose>
	         		<xsl:when test="position() = 1">
	         			<td align='center'>
		         			<a href="#">
									<xsl:attribute name="onclick" >
									javascript:openDialog("detailfz.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>",'dialogWidth:900px;dialogHeight:600px',result);
									</xsl:attribute>
									<xsl:value-of select="."/>
									</a>
								</td>
							</xsl:when>
						  
						  <xsl:otherwise>
						  	<td align='center'><xsl:value-of select="."/></td>
						  </xsl:otherwise>
          	</xsl:choose>
          </xsl:for-each>
        </tr>
     </xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
