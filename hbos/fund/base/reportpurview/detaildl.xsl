<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap='true' class='mainHead'>
  		<th>项目编码</th>
  		<th>项目名称</th>
  		<th>负责人</th>
  		<th>项目级别</th>
  	 </tr>
   </thead>
   <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>     
         <xsl:for-each select="td">
          		<td><xsl:value-of select="."/></td>
          </xsl:for-each>
        </tr>
     </xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
