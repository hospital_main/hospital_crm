<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<!--th style="display:none"><input type="checkbox"/></th-->
				<th noWrap="true" width="20%">支出项目编码</th>
				<th noWrap="true" width="20%">支出项目名称</th>
				<th noWrap="true" width="50%">会计科目</th>
				<th noWrap="true" width="10%">操作</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	        	<!--td align='center'  style='display:none'>
            	<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            	  <xsl:attribute name="value" >
            	    <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      				  </xsl:attribute>
    			  	</input>
          	</td--> 
	         <xsl:for-each select="td">
	          
	           <xsl:choose>
	           	<xsl:when test="position() = 1">
						<td>
						<xsl:value-of select="."/>					
						</td>	
								
				</xsl:when>
				<xsl:when test="position() = 4">
						<td>							
						[<a >
							<xsl:attribute name="onclick">
							setting('<xsl:value-of select="."/>');
							</xsl:attribute>
							<xsl:attribute name="href">
							#
							</xsl:attribute>
							设置
						</a>]
						[<a >
							<xsl:attribute name="onclick">
							deleteSkcode('<xsl:value-of select="."/>');
							</xsl:attribute>
							<xsl:attribute name="href">
							#
							</xsl:attribute>
							删除
						</a>]
						</td>	
								
				</xsl:when>
				<xsl:otherwise>
						<td><xsl:value-of select="."/></td> 
				</xsl:otherwise>
				</xsl:choose>    
	       
		   </xsl:for-each>
		   
  		</tr>
   	    </xsl:for-each>
 	</tbody>
  </xsl:template>
</xsl:stylesheet>