<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style="display:none"><input type="checkbox"/></th>
				<th nowrap='true' width="100px">支出项目编码</th>
				<th nowrap='true' width="100px">支出项目名称</th>			
				<th nowrap='true' width="50px">预算比例</th>
			</tr>           
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>

				<xsl:for-each select="td">   
					<xsl:choose>
						<!--xsl:when test="position() = 1">
							<td align="left">
								<a href='#'>
									<xsl:attribute name="onclick">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
									</xsl:attribute><xsl:value-of select="."/></a>
							</td>
						</xsl:when-->	
						<xsl:when test="position() = 3">
							<td align="left"  width="50px">
								<xsl:value-of select="."/>%
							</td>		
						</xsl:when>					
						<xsl:otherwise >
							<td align="left" width="100px"><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

