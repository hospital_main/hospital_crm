<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>课题来源编码</th>
				<th nowrap='true'>课题来源名称</th> 
				<th nowrap='true'>是否停用</th> 
			</tr>        
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 1">
								<a href='#'>
								<xsl:attribute name="onclick">
									javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
								</xsl:attribute><xsl:value-of select="."/></a>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

