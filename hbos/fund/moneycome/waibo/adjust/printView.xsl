<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	
  	    <td style="display:none"/>
  	  	<td style="display:none"/>
		    <td style="display:none"/>
		     <td style="display:none"/>
  	  </tr>
  	 
        <tr noWrap='true' class='mainHead'>  	  
          <td>单据编号</td>
				<td>项目编码</td>
				<td>项目名称</td>
				<td>经费来源</td>
				<td>项目类型</td>
				<td>财政用途</td>
				<td>项目级别</td>
				<td>项目负责人</td>
				<td>调整日期</td>
				<td>调整说明</td>
				<td>状态</td>
				<td>制单人</td>
				<td>审核人</td>	
				     
	</tr> 		     
      </thead>	      
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
	  <tr>
            <xsl:for-each select="td">          
	      <xsl:choose>            
		<xsl:when test="position()=3">
      	          <td align="left">
		    <xsl:value-of select="."/>
		  </td>
      	        </xsl:when>
      	        
      	      <xsl:when test="position()=5">
									<td noWrap="true" align="right">
										<xsl:value-of select="."/>
								</td>
              </xsl:when>
              
              
		<xsl:otherwise>
		  <td align="left">
		    <xsl:value-of select="."/>
		  </td>
		</xsl:otherwise>
	      </xsl:choose>
            </xsl:for-each>
	  </tr>
        </xsl:for-each>
      </tbody>
      <!--<tfoot>
    	<tr noWrap="true" class="mainHead">
    		<td noWrap="true" style="fontsize:foot;colspan:3;align:left"/>
    		<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:foot;colspan:4;align:right"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
				<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:foot;colspan:2;align:right"/>
  	  	<td style="display:none"/>
    	</tr>
    </tfoot>-->
    </root>   
  </xsl:template>
</xsl:stylesheet>