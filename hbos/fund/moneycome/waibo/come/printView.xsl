<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
  	  </tr>
  	 
        <tr noWrap='true' class='mainHead'>  	  
          <td>单据编号</td>
				<td>项目编码</td>
				<td>项目名称</td>
				<td>到账日期</td>
				<td>支付方式</td>
				<td>支票号码</td>
				<td>到账金额</td>
								
				<td>拨款单位</td>
				<td>到账说明</td>
				<td>状态</td>
				<td>制单人</td>
				<td>审核人</td>
				<td>分解人</td>
				<td>确认人</td>
				<td>凭证号</td>
	</tr> 		     
      </thead>	      
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
	  <tr>
            <xsl:for-each select="td">          
	      <xsl:choose>            
		<xsl:when test="position()=3">
      	          <td align="left">
		    <xsl:value-of select="."/>
		  </td>
      	        </xsl:when>
      	        
      	      <xsl:when test="position()=7">
									<td noWrap="true" align="right">
										<xsl:value-of select="."/>
								</td>
              </xsl:when>
              
              
		<xsl:otherwise>
		  <td align="left">
		    <xsl:value-of select="."/>
		  </td>
		</xsl:otherwise>
	      </xsl:choose>
            </xsl:for-each>
	  </tr>
        </xsl:for-each>
      </tbody>
      <!--<tfoot>
    	<tr noWrap="true" class="mainHead">
    		<td noWrap="true" style="fontsize:foot;colspan:3;align:left"/>
    		<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:foot;colspan:4;align:right"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
				<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:foot;colspan:2;align:right"/>
  	  	<td style="display:none"/>
    	</tr>
    </tfoot>-->
    </root>   
  </xsl:template>
</xsl:stylesheet>