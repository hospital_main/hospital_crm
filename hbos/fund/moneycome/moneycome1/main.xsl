<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>单据编号</th>
				<th>项目编码</th>
				<th>项目名称</th>
				<th>到账/批复日期</th>
				<th>支付方式</th>
				<th>支票号码</th>
				<th>批复文号</th>
				<th>到账/批复金额</th>
				<th>经费来源</th>
				<th>项目类型</th>	
				<th>财政用途</th>
				<th>拨款单位</th>
				<th>到账/批复说明</th>
				<th>备注</th>
				<th>状态</th>			
				<th>制单人</th>
				<th>审核人</th>
				<th>确认人</th>
				<th>凭证号</th>
				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:if test="td[2]!='合计：'">
          <td align='center'  noWrap='true'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          </xsl:if>
          
          <xsl:if test="td[2]='合计：'">
          <td align='center'  noWrap='true'>
            
          </td>
          </xsl:if>
          
          
          
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
									<td align='left'>
										<a tabindex='-1'>
						                  <xsl:attribute name="href" >
						    	            javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:900px;dialogHeight:650px', result)
						  	          </xsl:attribute><xsl:value-of select="."/></a>
								 	</td>
              </xsl:when>
              
              <xsl:when test="position()=8">
									<td noWrap="true" align="right">
										<xsl:value-of select="."/>
								</td>
              </xsl:when>
              
              <xsl:when test="position()=20">
									
              </xsl:when>
              
              
              <xsl:when test="position()=19">
									<td><a href='#'>									
												<xsl:attribute name="onclick">
				  							javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../td[20]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
				  							</xsl:attribute>
			                  <xsl:value-of select="."/>
			  	          	</a></td>
              </xsl:when>
              
							<xsl:otherwise>
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
