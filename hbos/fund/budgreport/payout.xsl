
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">     
				<th>单据编号</th>
				<th>申请日期</th>
				<th>项目名称</th>
				<th>报销事由</th>	
				<th>附件张数</th>
				<th>报销科室</th>
				<th>报销人</th>
				<th>资金来源</th>
				<th>支出项目</th>
				<th>报销金额</th>					
				<th>发票号码</th>			
				<th>发票日期</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=5">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               	</td>
                </xsl:when>                
                <xsl:when test="position()=10">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               	</td>
                </xsl:when>
			  	 			<xsl:otherwise>
			            <td >
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
