<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
      	<th>日期</th>
		<th>项目编码</th>
		<th>项目名称</th>
		<th>负责人</th>
		<th>摘要</th>
      	<th>单据编码</th>
      	<th>到账金额</th>
      	<th>支出金额</th>
      	<th>余额</th>
      	<th style="display:none">pk</th>
      </tr>  		
    </thead>
    <tbody>
    <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">

<xsl:if test="td[5]!='本月合计' and  td[5]!='本年累计'">
      <tr>
	  <xsl:for-each select="td">
	    <xsl:choose>
	      <xsl:when test="position()=6">
	    	<td>
	          <a href="#">
	          	<xsl:attribute name="onclick" >
					<xsl:if test="../td[5]='到账单'">
						javascript:openDialog('../../moneycome/moneycome1/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
					</xsl:if>
					<xsl:if test="../td[5]='报销单'">
						javascript:openDialog('update.html?load=&lt;pay_bill_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/pay_bill_id&gt;','dialogWidth:850px;dialogHeight:650px',result)
					</xsl:if>
				</xsl:attribute>
	          	<xsl:value-of select="."/>
	          </a>
	        </td>
	      </xsl:when>
	      <xsl:when test="position()=7 or position()=8 or position()=9">
	        <td align='right' class='moneyCol'>
	          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
	        </td>
	      </xsl:when>
	      <xsl:when test="position()=last()">
	        <td align='left' style='display:none'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:when>
	      <xsl:otherwise>
	        <td align='left'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:otherwise>			                        
	    </xsl:choose>
	  </xsl:for-each>
	</tr>

</xsl:if>




<xsl:if test="td[5]='本月合计' or  td[5]='本年累计'">
	<tr>
	  <xsl:for-each select="td">
	    <xsl:choose>
	      <xsl:when test="position()=6">
	    	<td>
	          <a href="#">
	          	<xsl:attribute name="onclick" >
	          		<xsl:if test="../td[5]='采购发票'">
					javascript:openDialog('../invoice/update_detail.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
				</xsl:if>
				<xsl:if test="../td[5]='入库单'">
					javascript:openDialog('in_detail.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
				</xsl:if>
				<xsl:if test="../td[5]='付款单'">
					javascript:openDialog('../pay/pay/update_detail.html?load=&lt;pay_bill_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/pay_bill_id&gt;','dialogWidth:850px;dialogHeight:650px',result)
				</xsl:if>
			</xsl:attribute>
	          	<xsl:value-of select="."/>
	          </a>
	        </td>
	      </xsl:when>
	      <xsl:when test="position()=7 or position()=8 or position()=9">
	        <td align='right' class='moneyCol'>
	          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
	        </td>
	      </xsl:when>

	      <xsl:when test="position()=1">
	        <td>
	          
	        </td>
	      </xsl:when>


	      <xsl:when test="position()=last()">
	        <td align='left' style='display:none'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:when>
	      <xsl:otherwise>
	        <td align='left'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:otherwise>			                        
	    </xsl:choose>
	  </xsl:for-each>
	</tr>

</xsl:if>

      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

