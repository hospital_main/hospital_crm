<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
				<tr noWrap="true" class="mainHead">
					<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				 </tr>	     
		 	  	<tr>
			 	  	<td>日期</td>
					<td>项目编码</td>
					<td>项目名称</td>
					<td>负责人</td>
					<td>摘要</td>
					<td>单据编码</td>
					<td>到账金额</td>
					<td>支出金额</td>
					<td>余额</td>
				</tr>
        </thead>	      
	      <tbody>
    <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">

<xsl:if test="td[5]!='本月合计' and  td[5]!='本年累计'">
      <tr>
	  <xsl:for-each select="td">
	    <xsl:choose>
	      <xsl:when test="position()=7 or position()=8 or position()=9">
	        <td align='right' class='moneyCol'>
	          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
	        </td>
	      </xsl:when>
	      <xsl:when test="position()=last()">
	        <td align='left' style='display:none'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:when>
	      <xsl:otherwise>
	        <td align='left'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:otherwise>			                        
	    </xsl:choose>
	  </xsl:for-each>
	</tr>

</xsl:if>




<xsl:if test="td[5]='本月合计' or  td[5]='本年累计'">
	<tr>
	  <xsl:for-each select="td">
	    <xsl:choose>
	      <xsl:when test="position()=7 or position()=8 or position()=9">
	        <td align='right' class='moneyCol'>
	          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
	        </td>
	      </xsl:when>

	      <xsl:when test="position()=1">
	        <td>
	          
	        </td>
	      </xsl:when>


	      <xsl:when test="position()=last()">
	        <td align='left' style='display:none'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:when>
	      <xsl:otherwise>
	        <td align='left'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:otherwise>			                        
	    </xsl:choose>
	  </xsl:for-each>
	</tr>

</xsl:if>

      </xsl:for-each>
    </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>