
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">       
				<th rowspan="2" valign="center">支出项目编码</th>
				<th rowspan="2" valign="center">支出项目名称</th>				
				<th colspan="4">合计</th>							
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th colspan="4">财政资金</th>						
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th colspan="4">科教资金</th>						
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th colspan="4">自筹资金</th>							
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
  		</tr>
  		<tr noWrap="true" class="mainHead">       
				<th style="display:none"></th>
				<th style="display:none"></th>			
				<th>期初余额</th>				
				<th>本期增加</th>
				<th>本期支出</th>				
				<th>期末余额</th>				
				<th>期初余额</th>				
				<th>本期增加</th>
				<th>本期支出</th>				
				<th>期末余额</th>
				<th>期初余额</th>				
				<th>本期增加</th>
				<th>本期支出</th>				
				<th>期末余额</th>
				<th>期初余额</th>				
				<th>本期增加</th>
				<th>本期支出</th>				
				<th>期末余额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	
                <xsl:when test="position()=1 ">
               	<td align="left">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
					             javascript:projDetail("&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;")					                                                          
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>	
			             </a>
			             </td>
               </xsl:when>
  							<xsl:when test="position()=2 ">
  								<td align="left">	
  									<xsl:value-of select="."/>
  								 </td>
  							 </xsl:when>
                 <xsl:when test="position()=5 and .!=0 and ../td[1]!=''">
               	<td align="right">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
					             javascript:payOut("&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;")					                                                          
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>	
			             </a>
			             </td>
               </xsl:when>
                <xsl:when test="position()=9 and .!=0 and ../td[1]!=''">
               	<td align="right">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
					             javascript:payOut("&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;&lt;fund_source_code&gt;01&lt;/fund_source_code&gt;")					                                                          
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>	
			             </a>
			             </td>
               </xsl:when>
               <xsl:when test="position()=13 and .!=0 and ../td[1]!=''">
               	<td align="right">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
					             javascript:payOut("&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;&lt;fund_source_code&gt;02&lt;/fund_source_code&gt;")					                                                          
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>	
			             </a>
			             </td>
               </xsl:when>
                <xsl:when test="position()=17 and .!=0 and ../td[1]!=''">
               	<td align="right">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
					             javascript:payOut("&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;&lt;fund_source_code&gt;03&lt;/fund_source_code&gt;")					                                                          
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>	
			             </a>
			             </td>
               </xsl:when>
               <xsl:when test="position()=4 and .!=0 and ../td[1]!=''">
               	<td align="right">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
					             javascript:income("&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;&lt;fund_source_code&gt;&lt;/fund_source_code&gt;")					                                                          
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>	
			             </a>
			             </td>
               </xsl:when>
                <xsl:when test="position()=8 and .!=0 and ../td[1]!=''">
               	<td align="right">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
					             javascript:income("&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;&lt;fund_source_code&gt;01&lt;/fund_source_code&gt;")					                                                          
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>	
			             </a>
			             </td>
               </xsl:when>
               <xsl:when test="position()=12 and .!=0 and ../td[1]!=''">
               	<td align="right">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
					             javascript:income("&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;&lt;fund_source_code&gt;02&lt;/fund_source_code&gt;")					                                                          
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>	
			             </a>
			             </td>
               </xsl:when>
               <xsl:when test="position()=16 and .!=0 and ../td[1]!=''">
               	<td align="right">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
					             javascript:income("&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;&lt;fund_source_code&gt;03&lt;/fund_source_code&gt;")					                                                          
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>	
			             </a>
			             </td>
               </xsl:when>
					  	 <xsl:otherwise>
					  	 		<td align="right" >               
			                <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
			            </td>			           
		           </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
