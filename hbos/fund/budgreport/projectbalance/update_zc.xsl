
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">       
				<th rowspan="2" valign="center">项目支出编码</th>
				<th rowspan="2" valign="center">项目支出名称</th>
				<th colspan="4">合计</th>							
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th colspan="4">财政资金</th>						
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th colspan="4">科教资金</th>						
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th colspan="4">自筹资金</th>							
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
  		</tr>
  		<tr noWrap="true" class="mainHead">       
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th>期初余额</th>				
				<th>本期增加</th>
				<th>本期支出</th>				
				<th>期末余额</th>				
				<th>期初余额</th>				
				<th>本期增加</th>
				<th>本期支出</th>				
				<th>期末余额</th>
				<th>期初余额</th>				
				<th>本期增加</th>
				<th>本期支出</th>				
				<th>期末余额</th>
				<th>期初余额</th>				
				<th>本期增加</th>
				<th>本期支出</th>				
				<th>期末余额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<tr>
          <xsl:for-each select="td">
             <xsl:choose>
               <xsl:when test="position()=1 or position()=2">
                	<td>				  	         
				  	          <xsl:value-of select="."/>
			            </td>
               </xsl:when>        			   
			  	<xsl:otherwise>
				  	 <td align="right">               
		               <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
		            </td>			           
          		</xsl:otherwise>
          	 </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
