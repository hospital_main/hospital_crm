<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
  	<thead>  	
  		<xsl:variable name="rownum" select="count(/root/tbody/tr)"/>
  		<xsl:variable name="rownum1" select="number($rownum div 2)"/> 
  		<tr noWrap="true" class="mainHead" >       
				<td style="colspan:7;fontsize:maintitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>				
  		</tr>  	  	
  		<tr noWrap="true" class="mainHead">       
				<td>资金来源</td>
				<td>项目级别</td>				
				<td>数量</td>							
				<td>期初</td>
				<td>收入</td>
				<td>支出</td>
				<td>余额</td>	
  		</tr>  
  	</thead>
  	<tbody>
  	 <xsl:for-each select="/root/tbody/tr">	    	 
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1 or position()=2">
			            <td align="left"><xsl:value-of select="."/></td>
                </xsl:when>                
			   				<xsl:otherwise>
			  	 				<td align="right" >               
	                   <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               	</td>			           
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
</root>
</xsl:template>
</xsl:stylesheet>
