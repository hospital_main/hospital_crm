
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
	
  		<tr noWrap="true" class="mainHead">       
				<th>经费来源</th>
				<th>项目级别</th>				
				<th>数量</th>							
				<th>期初</th>
				<th>收入</th>
				<th>支出</th>
				<th>余额</th>	
  		</tr>  
  	</thead>
  	<tbody>
  	
	    <xsl:for-each select="/root/tbody/tr">	    	 
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1 or position()=2">
			            <td align="left"><xsl:value-of select="."/></td>
                </xsl:when>                
			   				<xsl:otherwise>
			  	 				<td align="right" >               
	                   <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               	</td>			           
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>   
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
