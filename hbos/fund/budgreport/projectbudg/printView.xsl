<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<table>
  	<thead>
  		<tr noWrap="true" class="mainHead" >       
				<td style="colspan:15;fontsize:maintitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>  	
  		<tr noWrap="true" class="mainHead">       
				<td rowspan="2" valign="center">项目编码</td>
				<td rowspan="2" valign="center">项目名称</td>
				<td rowspan="2" valign="center">项目状态</td>
				<td colspan="4">合计</td>							
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan="4">财政资金</td>						
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan="4">科教资金</td>							
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan="4">自筹资金</td>							
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">       
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>			
				<td>到达合计</td>				
				<td>支出金额</td>
				<td>余额</td>
				<td>执行进度(%)</td>			
				<td>到达合计</td>				
				<td>支出金额</td>
				<td>余额</td>
				<td>执行进度(%)</td>			
				<td>到达合计</td>				
				<td>支出金额</td>
				<td>余额</td>
				<td>执行进度(%)</td>
				<td>下达合计</td>				
				<td>支出金额</td>
				<td>余额</td>
				<td>执行进度(%)</td>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
	    	<tr>
          <xsl:for-each select="td">
             <xsl:choose>
               <xsl:when test="position()=1 or position()=2 or position()=3">
                	<td>
				  	         <xsl:value-of select="."/>               
			            </td>
               </xsl:when>
               <xsl:when test="position()=7 or position()=11 or position()=15 or position()=19">
                	<td  align="right">
				  	         <xsl:value-of select="format-number(.,'#,##0.00')"/>%              
			            </td>
               </xsl:when>
			  			 <xsl:otherwise>
				  	 		<td align="right">               
		               <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
		            </td>			           
          		 </xsl:otherwise>
          	 </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
</table>
</xsl:template>
</xsl:stylesheet>
