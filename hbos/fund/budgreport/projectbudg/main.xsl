
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">       
				<th rowspan="2" valign="center">项目编码</th>
				<th rowspan="2" valign="center">项目名称</th>
				<th rowspan="2" valign="center">项目状态</th>
				<th colspan="4">合计</th>							
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th colspan="4">财政资金</th>						
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th colspan="4">科教资金</th>							
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th colspan="4">自筹资金</th>							
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
  		</tr>
  		<tr noWrap="true" class="mainHead">       
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>			
				<th>到达合计</th>				
				<th>支出金额</th>
				<th>余额</th>
				<th>执行进度(%)</th>			
				<th>到达合计</th>				
				<th>支出金额</th>
				<th>余额</th>
				<th>执行进度(%)</th>			
				<th>到达合计</th>				
				<th>支出金额</th>
				<th>余额</th>
				<th>执行进度(%)</th>
				<th>下达合计</th>				
				<th>支出金额</th>
				<th>余额</th>
				<th>执行进度(%)</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<tr>
          <xsl:for-each select="td">
             <xsl:choose>
               <xsl:when test="position()=1">
                	<td>				  	         
			             <a tabindex='-1'>
					           <xsl:attribute name="onclick" >					                                                                     
								 javascript:payOutSelect("&lt;id&gt;<xsl:value-of select="../pk/id"/>&lt;/id&gt;&lt;proj_code&gt;<xsl:value-of select="../pk/proj_code"/>&lt;/proj_code&gt;")
							 </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			             </a>
			            </td>
               </xsl:when>
			   <xsl:when test="position()=2">
                	<td>				  	         
			             <a tabindex='-1'>
					           <xsl:attribute name="onclick" >					                                                                     
								 javascript:projSelect("&lt;id&gt;<xsl:value-of select="../pk/id"/>&lt;/id&gt;&lt;proj_code&gt;<xsl:value-of select="../pk/proj_code"/>&lt;/proj_code&gt;")
							  </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			             </a>
			            </td>
               </xsl:when>
               <xsl:when test="position()=3">
                	<td>				  	         
				  	          <xsl:value-of select="."/>
			            </td>
               </xsl:when>
               <xsl:when test="position()=5  and .!=0  and ../td[1]!=''">
               	<td align="right">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
					             javascript:payOut("&lt;id&gt;<xsl:value-of select="../pk/id"/>&lt;/id&gt;&lt;proj_code&gt;<xsl:value-of select="../pk/proj_code"/>&lt;/proj_code&gt;")						                                                          
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>	
			             </a>
			             </td>
               </xsl:when>
                <xsl:when test="position()=9 and .!=0  and ../td[1]!=''">
               	<td align="right">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
								 javascript:payOut("&lt;id&gt;<xsl:value-of select="../pk/id"/>&lt;/id&gt;&lt;proj_code&gt;<xsl:value-of select="../pk/proj_code"/>&lt;/proj_code&gt;&lt;fund_source_code&gt;01&lt;/fund_source_code&gt;")	
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>	
			             </a>
			             </td>
               </xsl:when>
                <xsl:when test="position()=13  and .!=0  and ../td[1]!=''">
               	<td align="right">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
					             javascript:payOut("&lt;id&gt;<xsl:value-of select="../pk/id"/>&lt;/id&gt;&lt;proj_code&gt;<xsl:value-of select="../pk/proj_code"/>&lt;/proj_code&gt;&lt;fund_source_code&gt;02&lt;/fund_source_code&gt;")							                                                          
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>	
			             </a>
			             </td>
               </xsl:when>
			    <xsl:when test="position()=17  and .!=0  and ../td[1]!=''">
               	<td align="right">
                	<a tabindex='-1'>
					           <xsl:attribute name="onclick" >					           
					             javascript:payOut("&lt;id&gt;<xsl:value-of select="../pk/id"/>&lt;/id&gt;&lt;proj_code&gt;<xsl:value-of select="../pk/proj_code"/>&lt;/proj_code&gt;&lt;fund_source_code&gt;03&lt;/fund_source_code&gt;")						                                                          
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>	
			             </a>
			             </td>
               </xsl:when>
               <xsl:when test="position()=7 or position()=11 or position()=15 or position()=19">
                	<td  align="right">
				  	         <xsl:value-of select="format-number(.,'#,##0.00')"/>%              
			            </td>
               </xsl:when>
			  			 <xsl:otherwise>
				  	 		<td align="right">               
		               <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
		            </td>			           
          		 </xsl:otherwise>
          	 </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
