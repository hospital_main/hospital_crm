
<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>	
				<tr class="mainHead">
				<td>单据编码</td>
        <td>项目编码</td>
        <td>项目名称</td>
        <td>负责人</td>
        <td>制单日期</td>
        <td>支付方式</td>
        <td>票据号码</td>
		  	<td>报销事由</td>
		  	<td>报销部门</td>
		  	<td>报销人</td>
		  	<td>报销金额</td>
		  	<td>合同编码</td>
		  	<td>发票单位</td>
		  	<td>处理意见</td>
		  	<td>附件张数</td>
		  	<td>审核日期</td>
		  	<td>支付日期</td>
		  	<td>领款人姓名</td>
		  	<td>制单人</td>
		  	<td>审核人</td>
		  	<td>支付人</td>
		  	<td>凭证序号</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							 <xsl:when test="position()=11 ">
                	<td align="right">
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>		
             </xsl:when>
						    <xsl:otherwise>
									<td ><xsl:value-of select="."/></td>
								</xsl:otherwise>
						</xsl:choose>
			    </xsl:for-each>
				</tr>
			</xsl:for-each>	
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>