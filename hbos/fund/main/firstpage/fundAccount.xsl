<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/fund/main/fund_account.xsl,v 1.3 2013/12/05 07:38:46 yuchengying Exp $
 $Author: yuchengying $
 $Date: 2013/12/05 07:38:46 $
 $Revision: 1.3 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead> 
  		<tr noWrap="true" class="mainHead">
  			<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>单据编号</th>
				<th>项目编码</th>
				<th>项目名称</th>
				<th>负责人</th>
				<th>到账日期</th>
				<th>支付方式</th>
				<th>支票号码</th>
				<th>批复文号</th>
				<th>到账金额</th>
				<th>经费来源</th>
				<th>项目类别</th>			
				<th>拨款单位</th>
				<th>到账说明</th>
				<th>状态</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>确认人</th>
				<th>凭证号</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>    
        	  <td align='center'  noWrap='true'  style='display:none'>
             <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			   </input>
            </td>      
						<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td align='left'>
										<a tabindex='-1'>
						           <xsl:attribute name="href" >
                		 	javascript:openDialog('../../moneycome/moneycome1/update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:900px;dialogHeight:650px', '')  	
                		 </xsl:attribute>
                		 <xsl:value-of select="."/>
                		</a>
								 </td>
								</xsl:when>
								<xsl:when test="position()=9">
									<td noWrap="true" align="right">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
              </xsl:when>
                <xsl:otherwise>
								<td>
		                 <xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
							 	 
							 </xsl:for-each>
						
  			</tr>
   		</xsl:for-each>    
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
