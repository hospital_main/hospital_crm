<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>科室</th>
  			<th nowrap='true'>是否使用</th>
  			<th nowrap='true'>是否归口部门</th>
  			<!--th nowrap='true'>验证码</th-->
  			<th nowrap='true'>科室编码</th>
  			<th nowrap='true'>科室属性</th>
  			<th nowrap='true'>科室类别</th>
  		</tr>
  	</thead>

  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                  <xsl:value-of select="../td[7]"/>
                  <xsl:choose>
	                	<xsl:when test="../td[8]=0">
	                		<img src="../../../../../base/themes/blue/images/treetable/folderopen.png" border="0"/>
	                	</xsl:when>
	                	<xsl:otherwise>
	                		<img src="../../../../../base/themes/blue/images/treetable/file.png" border="0"/>
	                	</xsl:otherwise>
                	</xsl:choose>
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:when test="position()=2">
                <td align='center'>
                	<div>
                		<input type="checkbox" TABINDEX='-1' style='font-size:8px;'>
             				<xsl:attribute name="value">
			            		<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
			        			</xsl:attribute>
			        			<xsl:if test="../td[2]='1' or ../td[2]='2'">
											<xsl:attribute name="checked"/>										
										</xsl:if>
										<xsl:attribute name="id">
											<xsl:value-of select="../td[3]"/>
										</xsl:attribute>
										<xsl:attribute name="onclick">
											javascript:isStopCheckDept(this.parentNode.parentNode.parentNode,this);
										</xsl:attribute>
										<xsl:attribute name="level">
											<xsl:value-of select="../td[9]"/><!-- 保存节点level -->
										</xsl:attribute>
										<xsl:attribute name="isLast">
											<xsl:value-of select="../td[10]"/><!-- 是否是最后一级 -->
										</xsl:attribute>									
										<xsl:attribute name="sdisabled">
											<xsl:value-of select="../td[13]"/><!-- 是否可以编辑 -->
										</xsl:attribute>																										
            				</input>
            			</div>
                </td>
              </xsl:when>
              <xsl:when test="position()=3">
                <td align='center'><div>
                	<input type="checkbox" TABINDEX='-1' style='font-size:8px;'>
             			<xsl:attribute name="value">
			            <xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
			        		</xsl:attribute>
			        		<xsl:if test="../td[3]='1' or ../td[3]='2'">
										<xsl:attribute name="checked"/>							
									</xsl:if>
									<xsl:attribute name="id">
										<xsl:value-of select="../td[3]"/>
									</xsl:attribute>
									<xsl:attribute name="onclick">
										javascript:isManageCheckDept(this.parentNode.parentNode.parentNode,this);
									</xsl:attribute>
									<xsl:attribute name="level">
										<xsl:value-of select="../td[9]"/><!-- 保存节点level -->
									</xsl:attribute>
									<xsl:attribute name="isLast">
										<xsl:value-of select="../td[10]"/><!-- 是否是最后一级 -->
									</xsl:attribute>
									<xsl:attribute name="mdisabled">
										<xsl:value-of select="../td[14]"/><!-- 是否可以编辑 -->
									</xsl:attribute>									
            		</input></div>
                </td>
              </xsl:when>
              
              <xsl:when test="position() = 4"><!-- 验证码不显示 -->
              </xsl:when>
              
              <xsl:when test="position()>7">
                <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>