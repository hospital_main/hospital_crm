<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th width="25">
					<input type="checkbox" onclick="checkInit(this)"/>
				</th>
				<th>预算科目编码</th>
				<th>预算科目名称</th>
				<th>科目级别</th>
				<th>科目类别</th>
				<th>是否末级</th>
				<th>是否事件项目</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" >
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
							
								<xsl:if test="td[5] != '是'">
								<xsl:attribute name="disabled">	true</xsl:attribute>
								</xsl:if>
							
						</input>
					
					</td>
					
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td noWrap="true">
									<a tabindex="-1">
										<xsl:attribute name="href">
    	            javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:600px;dialogHeight:500px', result)
  	          </xsl:attribute>
										<xsl:value-of select="."/>
									</a> 
								</td>
							</xsl:when>
							<xsl:when test="position()=3 or position()=5 or position()=6">
								<td noWrap="true" align="center">
										<xsl:value-of select="."/>
									</td>
							</xsl:when>
							
							<xsl:otherwise>
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
