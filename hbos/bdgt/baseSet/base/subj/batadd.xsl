<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th noWrap="true">
					ѡ��
				</th>
				<th>Ԥ���Ŀ����</th>
				<th>Ԥ���Ŀ����</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center"><div>
						<input type="checkbox" TABINDEX="-1" onclick="setSelect(this,this)"  style="font-size:8px;">
							<xsl:if test="td[3] ='1'">
								<xsl:attribute name="disabled">true</xsl:attribute>
							</xsl:if>
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input></div>
					</td>
					
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td noWrap="true">
										<xsl:value-of select="."/>
									
								</td>
							</xsl:when>
							<xsl:when test="position()=3">
								<td noWrap="true" style="display:none">
										<xsl:value-of select="."/>
									
								</td>
							</xsl:when>						
							<xsl:otherwise>
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
