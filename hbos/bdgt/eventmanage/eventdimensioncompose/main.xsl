<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <thead>
            <tr noWrap="true" class="mainHead">
                <th noWrap="true" onclick="checkAll(this)">
                    <input type="checkbox"/>
                </th>
                <th>事件项目编码</th>
                <th>事件项目名称</th>
                <th>事件项目全称</th>
                <th>事件项目分类</th>
                <th>预算科目</th>
                <th>归口部门</th>
                <th>预算金额</th>
                <th>提交人</th>
                <th>提交日期</th>
                <th>审批人</th>
                <th>审批日期</th>
                <th>审批状态</th>
                <!--□	项目编码	项目名称	项目全称	预算科目	项目分类	归口部门	预算金额	提交人	提交日期	审批人	审批日期	审批状态-->

            </tr>
        </thead>
        <tbody>
            <xsl:for-each select="/root/tbody/tr">
                <tr>
                    <td align="center">
                        <div>
                            <input type="checkbox" TABINDEX="-1" style="font-size:8px;" onclick="onSelect(this)">
                                <xsl:attribute name="data-id">
                                    <xsl:value-of select="pk/id"/>
                                </xsl:attribute>
                                <xsl:attribute name="data-status">
                                    <xsl:value-of select="pk/status"/>
                                </xsl:attribute>
                            </input>
                        </div>
                    </td>

                    <xsl:for-each select="td">
                        <xsl:choose>
                            <xsl:when test="position()=2">
                                <td align="left">
                                    <a herf="#">
                                        <xsl:attribute name="style">color:blue;text-decoration:underline;cursor:hand
                                        </xsl:attribute>
                                        <xsl:attribute name="onclick">openAddDialog(this)
                                        </xsl:attribute>
                                        <xsl:attribute name="data-a">
                                            <xsl:value-of select="../td[1]"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="data-b">
                                            <xsl:value-of select="../td[2]"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="data-c">
                                            <xsl:value-of select="../td[3]"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="data-d">
                                            <xsl:value-of select="../td[6]"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="data-e">
                                            <xsl:value-of select="../td[5]"/>
                                        </xsl:attribute>


                                        <xsl:value-of select="."/>

                                    </a>
                                </td>

                            </xsl:when>
                            <xsl:when test="position() = 7 ">
                                <td align="right">
                                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
                                </td>
                            </xsl:when>
                            <xsl:otherwise>
                                <td noWrap="true" align="left">
                                    <xsl:value-of select="."/>
                                </td>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                </tr>
            </xsl:for-each>
        </tbody>
    </xsl:template>
</xsl:stylesheet>
