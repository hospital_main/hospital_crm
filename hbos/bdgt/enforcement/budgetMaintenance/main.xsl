<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th width="25">
					<input type="checkbox" onclick="checkInit(this)"/>
				</th>
				<th>年</th>
				<th>月</th>
				<th>科室名称</th>
				<th>科目名称</th>
				<th>执行金额</th>
				<th>数据来源</th>
				<th>操作员</th>
				<th>操作时间</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" >
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
					
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td noWrap="true">
									<a tabindex="-1">
										<xsl:attribute name="href">
    	            javascript:openDialog('add.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:400px;dialogHeight:300px', result)
  	          </xsl:attribute>
										<xsl:value-of select="."/>
									</a>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
