<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
    <root>
        <thead>
            <tr>
                <td style="fontsize:maintitle;colspan:colcount;"></td>
                <td style='display:none'/>
                <td style='display:none'/>
                <td style='display:none'/>
		    </tr>
            <tr noWrap="true" class="mainHead">
                <td style="fontsize:coltitle;" width="">科室编码</td>
                <td style="fontsize:coltitle;" width="">科室名称</td>
                <td style="fontsize:coltitle;" width="">历史数据</td>
                <td style="fontsize:coltitle;" width="">本年预算</td>
			</tr>
        </thead>
        <tbody>
        <xsl:for-each select="/root/tbody/tr">
            <tr>
            <xsl:for-each select="td">
                <td><xsl:value-of select="."/></td>
            </xsl:for-each>
            </tr>
        </xsl:for-each>
        </tbody>
    </root>
    </xsl:template>
</xsl:stylesheet>