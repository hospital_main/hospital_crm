<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <root>
            <thead>
                <tr>
                    <td style="fontsize:maintitle;colspan:colcount;"></td>
                    <td style='display:none'/>
                    <td style='display:none'/>
                    <td style='display:none'/>
                </tr>
                <tr noWrap="true" class="mainHead">
                    <td nowrap='true' style="fontsize:coltitle;" width="180">科室编码</td>

                    <td nowrap='true' style="fontsize:coltitle;" width="400">科室名称</td>

                    <td nowrap='true' style="fontsize:coltitle;" width="200">历史数据</td>

                    <td nowrap='true' style="fontsize:coltitle;" width="200">本年预算</td>

                </tr>
            </thead>
            <tbody>
                <xsl:for-each select="/root/tbody/tr">
                    <tr nowrap='true'>
                        <xsl:for-each select="td">

                            <xsl:choose>
                                <xsl:when test="position()=1 or position()=2">

                                    <td noWrap="true" align="left">
                                        <xsl:value-of select="."/>
                                    </td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td noWrap="true" align="right">
                                        <xsl:value-of select="format-number(.,'#,##0.00')"/>
                                    </td>


                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                    </tr>
                </xsl:for-each>
            </tbody>
        </root>
    </xsl:template>
</xsl:stylesheet>