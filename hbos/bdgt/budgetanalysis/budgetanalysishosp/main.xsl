<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th rowspan="2" valign="middle">选择</th>
				<th nowrap="true" rowspan="2" dataIndex="0" valign="middle">科目编码</th>
				<th nowrap="true" rowspan="2" dataIndex="0" valign="middle">科目名称</th>
				<th nowrap="true" colspan="3" dataIndex="0">年度预算</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">1月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">2月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">3月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">4月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">5月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">6月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">7月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">8月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">9月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">10月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">11月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">12月</th>
				<th style="display:none"/>
				<th style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th style="display:none"/>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <td align='center'  noWrap='true'>
            <button class="graphButton">
            	 <xsl:attribute name="index"><xsl:value-of select='$CurTrPos'/></xsl:attribute>
            	 <xsl:attribute name="onclick">openGraph(this)</xsl:attribute>
            	 ___
    			  </button>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true'>
							<xsl:choose>
								<xsl:when test="../td[1]='0' or ../td[1]='99999999'">
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
			            </td>
                </xsl:when>
                <xsl:when test="position()=2">
			            <td  noWrap='true'>
	                  	<xsl:value-of select="."/>
			            </td>
                </xsl:when>
                <xsl:when test="position()=42 ">
			            <td  noWrap='true' style="display:none">
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td noWrap="true" align="right">
							<xsl:value-of select="format-number(.,'#,##0.00')"/>
						</td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
