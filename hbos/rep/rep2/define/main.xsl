<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
			<xsl:for-each select="/root/t2head/tr">
	  		<tr noWrap="true" class="mainHead">
        	<th width='25'><input type='checkbox' onclick="checkInit(this)"/></th>
	  			<th nowrap='true'>ģ�����</th>
	  			<th nowrap='true'>ģ������</th>
					<xsl:for-each select="th">
            <xsl:choose>
              <xsl:when test="position()=1">
								<th noWrap="true">
									<xsl:value-of select="."/>
								</th>
              </xsl:when>
              <xsl:when test="position()=2 or position()=3">
								<xsl:if test="../th[10]='09'">
									<th noWrap="true">
											<xsl:value-of select="."/>
									</th>
								</xsl:if>
              </xsl:when>
              <xsl:when test="position()=5">
									<xsl:if test="../th[11]='1'">
										<th noWrap="true">
												<xsl:value-of select="."/>
										</th>
									</xsl:if>
              </xsl:when>
              <xsl:when test="position()=7">
									<xsl:if test="../th[12]=1">
											<th noWrap="true">
													<xsl:value-of select="."/>
											</th>
									</xsl:if>
              </xsl:when>
              <xsl:when test="position() &gt; 12 ">
								<th style="display:none" noWrap="true">
										<xsl:value-of select="."/>
								</th>
              </xsl:when>
              <xsl:otherwise>
								<th noWrap="true">
                	<xsl:value-of select="."/>
								</th>
              </xsl:otherwise>
            </xsl:choose>
					</xsl:for-each>
	  		</tr>
			</xsl:for-each>
  	</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td>
	                  <a tabindex='-1'>
	                    <xsl:attribute name="href" >
	      	            	javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:400px;dialogHeight:410px', result)
	    	              </xsl:attribute>
	  	            	<xsl:value-of select="."/></a>
			            </td>
                </xsl:when>
                <xsl:when test="position()=4 or position()=5">
									<xsl:if test="../td[11]='09'">
				            <td>
		  	            	<xsl:value-of select="."/>
				            </td>
			            </xsl:if>
                </xsl:when>
                <xsl:when test="position()=7">
									<xsl:if test="../td[12]='1'">
				            <td>
		  	            	<xsl:value-of select="."/>
				            </td>
			            </xsl:if>
                </xsl:when>
                <xsl:when test="position()=9">
									<xsl:if test="../td[13]='1'">
				            <td>
		  	            	<xsl:value-of select="."/>
				            </td>
			            </xsl:if>
                </xsl:when>
                <xsl:when test="position() &gt; 13 ">
				            <td style="display:none">
		  	            	<xsl:value-of select="."/>
				            </td>
                </xsl:when>
                <xsl:otherwise>
			            <td>
                		<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
          <td><a>
            <xsl:attribute name="href" >
    	      javascript:openDialog('set.html?load=<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:'+screen.availWidth+';dialogHeight:'+screen.availHeight)
  	    </xsl:attribute>����</a></td>
        </tr>
      </xsl:for-each>
    </tbody>

  </xsl:template>
</xsl:stylesheet>

