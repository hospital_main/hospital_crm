<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				
			</colgroup>
  	<thead>
  		<tr noWrap='true' class='mainHead'>
		<th nowrap='true'>汇总表编码</th>
      	<th nowrap='true'>汇总表名称</th>
      	<th nowrap='true'>汇总说明</th>
  	  
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                <td><xsl:value-of select="."/></td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>

