			var monthArray = new Array();
			var m1 = new Array();
			m1[0] = "01";
			m1[1] = "01月份";
			monthArray[0]	= m1;
			var m2 = new Array();
			m2[0] = "02";
			m2[1] = "02月份";
			monthArray[1]	= m2;
			var m3 = new Array();
			m3[0] = "03";
			m3[1] = "03月份";
			monthArray[2]	= m3;
			var m4 = new Array();
			m4[0] = "04";
			m4[1] = "04月份";
			monthArray[3]	= m4;
			var m5 = new Array();
			m5[0] = "05";
			m5[1] = "05月份";
			monthArray[4]	= m5;
			var m6 = new Array();
			m6[0] = "06";
			m6[1] = "06月份";
			monthArray[5]	= m6;
			var m7 = new Array();
			m7[0] = "07";
			m7[1] = "07月份";
			monthArray[6]	= m7;
			var m8 = new Array();
			m8[0] = "08";
			m8[1] = "08月份";
			monthArray[7]	= m8;
			var m9 = new Array();
			m9[0] = "09";
			m9[1] = "09月份";
			monthArray[8]	= m9;
			var m10 = new Array();
			m10[0] = "10";
			m10[1] = "10月份";
			monthArray[9]	= m10;
			var m11 = new Array();
			m11[0] = "11";
			m11[1] = "11月份";
			monthArray[10]	= m11;
			var m12 = new Array();
			m12[0] = "12";
			m12[1] = "12月份";
			monthArray[11]	= m12;
						
			var quarterArray = new Array();
			var q1 = new Array();
			q1[0] = "1";
			q1[1] = "1季度";
			quarterArray[0]	= q1;
			var q2 = new Array();
			q2[0] = "2";
			q2[1] = "2季度";
			quarterArray[1]	= q2;
			var q3 = new Array();
			q3[0] = "3";
			q3[1] = "3季度";
			quarterArray[2]	= q3;
			var q4 = new Array();
			q4[0] = "4";
			q4[1] = "4季度";
			quarterArray[3]	= q4;
			
			var halfYearArray = new Array();
			var h1 = new Array();
			h1[0] = "1";
			h1[1] = "上半年";
			halfYearArray[0]	= h1;
			var h2 = new Array();
			h2[0] = "2";
			h2[1] = "下半年";
			halfYearArray[1]	= h2;

			var oneYearArray = new Array();
			var y1 = new Array();
			y1[0] = getAcctYear();
			y1[1] = y1[0] + "年";
			oneYearArray[0]	= y1;
			
			var dataArray = new Array();
			dataArray[0] = monthArray;
			dataArray[1] = quarterArray;
			dataArray[2] = halfYearArray;
			dataArray[3] = oneYearArray;

			var selBgColor = "#CCCCCC";
			var unselBgColor = "#FFFFFF";
			
			var monthXML = getDictXML(dataArray[0]);
			var quarterXML = getDictXML(dataArray[1]);
			var halfYearXML = getDictXML(dataArray[2]);
			var oneYearXML = getDictXML(dataArray[3]);
			
			function getDictXML(dataArray){
				var xml = '<?xml version="1.0" encoding="GB2312"?><root>';
				if(dataArray){
						for(var i = 0; i < dataArray.length; i++){
							var item = dataArray[i][0];
							xml += ('<para code="' + item + '" value="' + item + '"/>"');
						}
				}
				xml += '</root>';
				return xml;
			}