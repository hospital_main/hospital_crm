<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>单位编码</th>
				<th>单位名称</th>
			</tr> 
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none2'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" ><xsl:value-of select="pk/sub_comp"/></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<td >
							<xsl:choose>
								<xsl:when test="position()=1">
									<a href="#">
										<xsl:attribute name="onclick">
										showCell("<xsl:value-of select="../pk/sub_comp"/>");
										</xsl:attribute>
									<xsl:value-of select="."/></a>
								</xsl:when>	
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>							
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



