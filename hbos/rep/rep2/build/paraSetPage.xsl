<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th width='40'>ѡ��</th>
      	<th nowrap='true'>����</th>
      	<th nowrap='true'>����</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:'>
            <input type='radio' name='select' onclick='chose(this)'>
             <xsl:attribute name="code_value" ><xsl:value-of select="td[1]"/></xsl:attribute>
             <xsl:attribute name="name_value" ><xsl:value-of select="td[2]"/></xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <td>     
              <xsl:value-of select="."/>  
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

