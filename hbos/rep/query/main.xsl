<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
  	<colgroup>
      <xsl:for-each select="/root/colgroup/col">
        <col>
          <xsl:attribute name="style"><xsl:value-of select='@style'/></xsl:attribute>
        </col>
    	</xsl:for-each>
    </colgroup>
  	<thead>
      <xsl:for-each select="/root/thead/tr">
        <tr noWrap='true' class='mainHead' style='{@style}'>
          <xsl:for-each select="th">
            <th noWrap='true' colspan='{@colspan}' rowspan='{@rowspan}'>
              <xsl:choose>
                <xsl:when test='@type=2'>
                  <xsl:attribute name="style">display:none</xsl:attribute>
                </xsl:when>
                <xsl:when test='@type=0'>
                  <xsl:attribute name="style" ><xsl:value-of select="@style"/></xsl:attribute>
                  <xsl:value-of select='.'/>
                </xsl:when>
              </xsl:choose>
            </th>
          </xsl:for-each>
        </tr>
    	</xsl:for-each>
  	</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:attribute name="style"><xsl:value-of select='@style'/></xsl:attribute>
          <xsl:for-each select="td">
            <td noWrap='true' colspan='{@colspan}' rowspan='{@rowspan}'>
              <xsl:choose>
                <xsl:when test='@type=1'><!--����-->
                  <xsl:attribute name="style">text-align:right;</xsl:attribute>
                	<xsl:if test="contains(., '.')">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </xsl:if>
	                <xsl:if test="not(contains(., '.'))">
	                  <xsl:value-of select="format-number(.,'#,###')"/>
	                </xsl:if>
                </xsl:when>
                <xsl:when test='@type=2'>
                  <xsl:attribute name="style">display:none;<xsl:value-of select="@style"/></xsl:attribute>
                </xsl:when>
                <xsl:when test='@type=0'>
                  <xsl:attribute name="style" ><xsl:value-of select="@style"/></xsl:attribute>
                  <xsl:value-of select='.'/>
                </xsl:when>
              </xsl:choose>
            </td>
          </xsl:for-each>
    	  </tr>
      </xsl:for-each>
    </tbody>
  </root>
	</xsl:template>
</xsl:stylesheet>



