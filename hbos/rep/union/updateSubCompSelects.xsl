<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th><input type='checkbox'/></th>
		  	<th nowrap='true'>����</th>
		  	<th nowrap='true'>����</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
	        <td align='center'>
	          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	            <xsl:attribute name="value">
	              <xsl:value-of select="pk/comp_subj_code"/>
	            </xsl:attribute>
	          </input>
	        </td>
          <xsl:for-each select="td">
            	<td><xsl:value-of select="."/></td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>