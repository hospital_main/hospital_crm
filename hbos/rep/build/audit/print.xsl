<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<root>
			<colgroup>		       
			<col style = 'width:180'/>	
				<col style = 'width:40'/>	
				<col style = 'width:40'/>	
				<col style = 'width:80'/>	
				<col style = 'width:510'/>	
				<col style = 'width:70'/>	
				
			</colgroup>
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  		      <th>报表名称</th>
		        <th>行</th>
		  	<th>列</th>
		  	<th>项目编码</th>
		  	<th>项目公式</th>
		  	<th>审核结果</th>
  	  	<th>错误提示</th>
  		</tr>
  		
  	</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:if test="td[position()=4]!='正确'">
            <xsl:attribute name="style"> color :red</xsl:attribute>            
          </xsl:if>
          <xsl:for-each select="td">
            <td><xsl:value-of select="."/></td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>

