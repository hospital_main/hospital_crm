<?xml version='1.0' encoding="GB2312" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=last()">
	    			<tr noWrap='true' class='mainHead'>
			    		<xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=1 or position()=3 or position()=5">
			          	</xsl:when>
			          	<xsl:otherwise>
			          			<th nowrap='true'><xsl:value-of select="."/></th>
		              		</xsl:otherwise>
		 		</xsl:choose>
		          </xsl:for-each>
	         		 </tr>
	        	</xsl:when>
	      </xsl:choose>
	    		</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position()!=last()]">
				<tr>
					<xsl:for-each select="td">
					 <xsl:variable name='CurTdPos' select='position()'/>
						<xsl:choose>
			       
					<xsl:when test="position()=1 or position()=3 or position()=5">
					</xsl:when>
					<xsl:when test="position()=2">
						<td  style="text-align:left;;width:65px">
                				 <xsl:value-of  select="."/> 
						</td>	
					</xsl:when>
					<xsl:when test="position()=4">
						<td  style="text-align:left;width:140px">
                				 <xsl:value-of  select="."/> 
						</td>	
					</xsl:when>
					<xsl:when test="position()=6">
						<td  style="text-align:left;width:140px">
                				 <xsl:value-of  select="."/> 
						</td>	
					</xsl:when>
					<xsl:when test="position()=8 ">
						<td align="right">
						<input type='text' name='year_target_value'  onkeydown='editskip(event)' required='true' style="text-align:right;;width:65px">
                				<xsl:attribute name="dept_code"><xsl:value-of select="../td[1]"/></xsl:attribute>
                				<xsl:attribute name="sub_index_group_code"><xsl:value-of select="../td[3]"/></xsl:attribute>
                				<xsl:attribute name="sub_index_code"><xsl:value-of select="../td[5]"/></xsl:attribute>
                				<xsl:attribute name="value"><xsl:value-of  select="format-number(.,'#,##0.00')"/></xsl:attribute>
                				</input>					
						</td>	
					</xsl:when>
					<xsl:when test="position()>8">
						<td align="right" >
						<xsl:if test="string(number(.))!='NaN'">
							<input type='text' name='target_value'  required='true' onkeydown='editskip(event)' style="text-align:right;width:65px">
	                				<xsl:attribute name="dept_code"><xsl:value-of select="../td[1]"/></xsl:attribute>
	                				<xsl:attribute name="sub_index_group_code"><xsl:value-of select="../td[3]"/></xsl:attribute>
	                				<xsl:attribute name="sub_index_code"><xsl:value-of select="../td[5]"/></xsl:attribute>
	                				<xsl:attribute name="value"><xsl:value-of  select="format-number(.,'#,##0.00')"/></xsl:attribute>
	                				<xsl:attribute name="month"><xsl:value-of select="/root/tbody/tr[last()]/td[$CurTdPos]"/></xsl:attribute>
	                		</input>				
                		</xsl:if>
                		<xsl:if test="string(number(.))='NaN'">
							<input type='text' name='target_value'  required='true' onkeydown='editskip(event)' style="text-align:right;width:65px;background-color:#EEEEEE;border:1px solid #808080;">
	                				<xsl:attribute name="dept_code"><xsl:value-of select="../td[1]"/></xsl:attribute>
	                				<xsl:attribute name="sub_index_group_code"><xsl:value-of select="../td[3]"/></xsl:attribute>
	                				<xsl:attribute name="sub_index_code"><xsl:value-of select="../td[5]"/></xsl:attribute>
	                				<xsl:attribute name="readonly">true</xsl:attribute>
	                				<xsl:attribute name="value"><xsl:value-of  select="'--'"/></xsl:attribute>
	                				<xsl:attribute name="month"><xsl:value-of select="/root/tbody/tr[last()]/td[$CurTdPos]"/></xsl:attribute>
	                		</input>				
                		</xsl:if>
						</td>	
					</xsl:when>
					<xsl:otherwise>
						<td align="left">
						      	<xsl:value-of select="."/>
						</td>	
					</xsl:otherwise>
						</xsl:choose>            
					</xsl:for-each>
				</tr>
				
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>