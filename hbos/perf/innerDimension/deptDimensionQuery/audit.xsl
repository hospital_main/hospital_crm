<?xml version='1.0' encoding="GB2312" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=last()">
	    			<tr noWrap='true' class='mainHead'>
			    		<xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=1 or position()=2 or position()=3 ">
			          	</xsl:when>
			          	<xsl:otherwise>
			          			<th nowrap='true'><xsl:value-of select="."/></th>
		              		</xsl:otherwise>
		 		</xsl:choose>
		          </xsl:for-each>
	         		 </tr>
	        	</xsl:when>
	      </xsl:choose>
	    		</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<xsl:variable name='CurTrPos' select='position()'/>
			<xsl:choose>
				
				<xsl:when test="position()!=last() ">
				<tr>
					<xsl:for-each select="td">
					 <xsl:variable name='CurTdPos' select='position()'/>
						<xsl:choose>
			       
					<xsl:when test="position()=1 or position()=2 or position()=3 ">
					</xsl:when>
					<xsl:when test="position()>4 ">
									
		              			<td align="right">
				                	
					  	          <xsl:value-of  select="format-number(.,'#,##0.00')"/>
				  	          	
			          		</td>
              				</xsl:when>
					<xsl:otherwise>
								
						<td align="left">
						      	<xsl:value-of select="."/>
						</td>	
					</xsl:otherwise>
						</xsl:choose>            
					</xsl:for-each>
				</tr>
				</xsl:when>
			</xsl:choose>
			</xsl:for-each>	
			
			
		</tbody>
				
	</xsl:template>
</xsl:stylesheet>