<?xml version='1.0' encoding="GB2312" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			
	    		
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>指标名称</th>
				<th nowrap='true'>计量单位</th>
				<th nowrap='true'>目标值</th>
				<th nowrap='true'>实际值</th>
				<th nowrap='true'>权重</th>
				<th nowrap='true'>得分</th>
			</tr>     
		
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
			       
					<xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 ">
									
		              			<td align="right">
				                	
					  	          <xsl:value-of  select="format-number(.,'#,##0.00')"/>
				  	          	
			          		</td>
              				</xsl:when>
									
						
					<xsl:otherwise>
								
						<td align="left">
						      	<xsl:value-of select="."/>
						</td>	
					</xsl:otherwise>
						</xsl:choose>            
					</xsl:for-each>
				</tr>
				
			</xsl:for-each>
			<!--
			<tr>
						<td ></td>
              					<td ></td>
              					<td ></td>
              					<td ></td>
              					<td ></td>
			          		<td align="right">
			          		<xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/>
			          		</td>
			          	
			</tr>
			-->
		</tbody>
	</xsl:template>
</xsl:stylesheet>