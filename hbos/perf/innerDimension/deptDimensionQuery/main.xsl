<?xml version='1.0' encoding="GB2312" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=last()">
	    			<tr noWrap='true' class='mainHead'>
			    		<xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=1 or position()=3 ">
			          	</xsl:when>
			          	<xsl:otherwise>
			          			<th nowrap='true'><xsl:value-of select="."/></th>
		              		</xsl:otherwise>
		 		</xsl:choose>
		          </xsl:for-each>
	         		 </tr>
	        	</xsl:when>
	      </xsl:choose>
	    		</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position()!=last()]">
				<tr>
					<xsl:for-each select="td">
					 <xsl:variable name='CurTdPos' select='position()'/>
					 
						<xsl:choose>
			       
					<xsl:when test="position()=1 or position()=3">
					</xsl:when>
					<xsl:when test="position()=4 ">
									
		              			<td align="left">
				                	<a tabindex='-1'>
					                  <xsl:attribute name="href">
					    	            	javascript:openDialog
					    	            	('audit.html?load=
					    	            	&lt;dept_code&gt;<xsl:value-of select="../td[1]"/>&lt;/dept_code&gt;
					    	            	&lt;dept_name&gt;<xsl:value-of select="../td[2]"/>&lt;/dept_name&gt;
					    	            	&lt;sub_index_group_code&gt;<xsl:value-of select="../td[3]"/>&lt;/sub_index_group_code&gt;
					    	            	&lt;sub_index_group_name&gt;<xsl:value-of select="../td[4]"/>&lt;/sub_index_group_name&gt;
					    	            	&lt;start_month&gt;<xsl:value-of select="/root/tbody/tr[last()]/td[5]"/>&lt;/start_month&gt;
					    	            	&lt;end_month&gt;<xsl:value-of select="/root/tbody/tr[last()]/td[last()]"/>&lt;/end_month&gt;
					    	            	', 'dialogWidth:800px;dialogHeight:650px')
					  	          </xsl:attribute>
					  	          <xsl:value-of select="."/>
				  	          	</a>
			          		</td>
              				</xsl:when>
					<xsl:when test="position()>4">
                						
		              			<td align="right">
				                	<a tabindex='-1'>
					                  <xsl:attribute name="href" >
					    	            	javascript:openDialog('index.html?load=
					    	            	&lt;dept_code&gt;<xsl:value-of select="../td[1]"/>&lt;/dept_code&gt;
					    	            	&lt;dept_name&gt;<xsl:value-of select="../td[2]"/>&lt;/dept_name&gt;
					    	            	&lt;sub_index_group_code&gt;<xsl:value-of select="../td[3]"/>&lt;/sub_index_group_code&gt;
					    	            	&lt;sub_index_group_name&gt;<xsl:value-of select="../td[4]"/>&lt;/sub_index_group_name&gt;
					    	            	&lt;perf_month&gt;<xsl:value-of select="/root/tbody/tr[last()]/td[$CurTdPos]"/>&lt;/perf_month&gt;
					    	            	', 'dialogWidth:800px;dialogHeight:650px')
					  	          </xsl:attribute>
					  	          <xsl:value-of  select="format-number(.,'#,##0.00')"/>
				  	          	</a>
			          		</td>				
						
					</xsl:when>
					<xsl:otherwise>
								
						<td align="left">
						      	<xsl:value-of select="."/>
						</td>	
					</xsl:otherwise>
						</xsl:choose>            
					</xsl:for-each>
				</tr>
				
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>