<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
				<th width='100px' nowrap='true'>科室名称</th>
				<th width='100px' nowrap='true'>指标名称</th>
				<th width='60px' nowrap='true'>计量单位</th>    
				<th width='80px' nowrap='true'>目标值</th>
				<th width='80px' nowrap='true'>实际值</th>
				<th width='100px' nowrap='true'>权重</th>
				<th width='40px' nowrap='true'>得分</th>
				<th width='100px' nowrap='true'>审核状态</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
              	<td>
              		<!--
                	<a tabindex='-1'>
	                  <xsl:attribute name="href" >
	    	            	javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:650px')
	  	          		</xsl:attribute>
	  	          		
	  	          		<xsl:value-of select="."/>
  	          		</a>
  	          		-->
  	          		<xsl:value-of select="."/>
	          		</td>
              </xsl:when>
              
               <xsl:when test="position()=4 or position()=5 or position()=6 or position()=7">
              	<td align='right'>
              		<xsl:value-of select="format-number(.,'###,##0.00')" />
              	</td> 
              </xsl:when>
              <xsl:when test="position()=9">
              	<td style="display:none">
						<xsl:value-of select="." />
              	</td> 
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
      <tr>
						<td colspan='6' align="center">合计：</td>
						<td class="numberText">
							<xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/>
						</td>
						<td/>
				</tr>
    </tbody>
  </xsl:template>
</xsl:stylesheet>