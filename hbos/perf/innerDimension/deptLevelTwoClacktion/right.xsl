<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <!--<th width='100px' nowrap='true'>科室考评表</th>-->
	  	<th width='100px' nowrap='true'>指标名称</th>
	  	<th width='60px' nowrap='false'>单位</th>    
		<th width='80px' nowrap='true'>目标值</th>
		<th width='80px' nowrap='true'>实际值</th>
		<th width='80px' nowrap='true'>评分标准</th>
		<th width='100px' nowrap='true'>权重</th>
		<th width='40px' nowrap='true'>得分</th>
		<th width='100px' nowrap='true'>审核状态</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1 or position()=2">
              	<td style='display:none'>
	  	          		<xsl:value-of select="."/>
	          		</td>
              </xsl:when>
              <xsl:when test="position()=3">
              	<td style='width:140px'>
	  	          		<xsl:value-of select="."/>
	          		</td>
              </xsl:when>
               <xsl:when test="position()=4">
              	<td style='width:40px;' nowrap='true'>
	  	          		<xsl:value-of select="."/>
	          		</td>
              </xsl:when>
              <xsl:when test="position()=6">
              	<td>
              		<input type="text" name="actual_value" onkeydown="return parent.ignoreIllegal_pri(this)" onfocus="parent.saveModifyBefore(this)" style="text-align:right;width:70px">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###,##0.00')"/></xsl:attribute>
											<xsl:attribute name='onblur'>
				    						parent.updateData(this,"<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
				    					</xsl:attribute>	
											<xsl:if test="../td[8] = 'confirm'">
				              	<xsl:attribute name="readonly">true</xsl:attribute>
				       				</xsl:if>
									</input>
              	</td> 
              </xsl:when>
              
               <xsl:when test="position()=5 or position()=6  or position()=8 or position()=9">
              	<td align='right' >
              		<xsl:value-of select="format-number(.,'###,##0.00')" />
              	</td> 
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>