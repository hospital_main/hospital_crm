<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <thead>
        <tr noWrap='true' class='mainHead'>
        
	  	<th width='100px' nowrap='true'>科室</th>
	  	<th width='60px' nowrap='false'>科室考评表</th>    
		<th width='80px' nowrap='true'>得分</th>
	    <th width='0'  nowrap='true' style="display:none">科室编码</th>
      </tr> 
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="trAll" select="count(/root/tbody/tr)"></xsl:variable>
      	<xsl:variable name="trNum" select="position()"></xsl:variable>
        <tr> 
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=2">
              	<td>
                	<a tabindex='-1' href='#'>
	                  <xsl:attribute name="onclick" >
	    	            	javascript:parent.queryDetail('<xsl:for-each select="../pk/*"> <xsl:value-of select="."/> </xsl:for-each>__<xsl:value-of select="../td[4]"/>__<xsl:value-of select="$trNum"/>__<xsl:value-of select="$trAll"/>');
	    	            	 
	  	          		</xsl:attribute>
	  	          		<xsl:value-of select="."/>
  	          		</a>
	          		</td>
              </xsl:when>
              <xsl:when test="position()=3">
              	 <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              	<xsl:when test="position()=4">
              	 <td style="display:none" align="right" >
              	 	<xsl:value-of select="."/>
              	 </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
      <!--
      	<tr>
						<td colspan='2' align="center">合计：</td>
						<td class="numberText">
							<xsl:value-of select="format-number(sum(/root/tbody/tr/td[4]),'#,##0.00')"/>
						</td>
				</tr>
		-->
    </tbody>
  </xsl:template>
</xsl:stylesheet>