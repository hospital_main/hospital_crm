<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
		<tr noWrap='true' class='mainHead'>
		<th nowrap='true' width='10px'><span><input type='checkbox' id="first">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span>
		</th>
		               
		<th noWrap="true">成本项目代码</th>
		<th noWrap="true">成本项目名称</th>
		<th nowrap='true'>参与奖金计算<span><input type='checkbox' id="iscalc">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span>
		</th>
		<th nowrap='true'>直接成本<span><input type='checkbox' id="directcost">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span>
		</th>
			<th nowrap='true'>计算计入成本<span><input type='checkbox' id="calccost">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span>
		</th>
		<th nowrap='true'>分摊公用成本<span><input type='checkbox' id="publiccost">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span>
		</th>
		<th nowrap='true'>分摊管理成本<span><input type='checkbox' id="managecost">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span>
		</th>
		<th nowrap='true'>分摊医疗辅助成本<span><input type='checkbox' id="assistcost">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span>
		</th>
		<th nowrap='true'>分摊医疗技术成本 <span><input type='checkbox' id="skillcost">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span>
		</th>

		     </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">

	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <td align='center'>
            <div><input type='checkbox'>
	            <xsl:attribute name="id">first_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;					</xsl:for-each>
        	    </xsl:attribute>
     			  </input></div>
     			</td>

          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
              	<td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position() = 4">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">iscalc_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">readClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              
              <xsl:when test="position() = 5">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">directcost_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">readClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 6">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">calccost_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">readClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 7">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">publiccost_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">readClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
					<xsl:when test="position() = 8">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">managecost_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">readClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 9">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">assistcost_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">readClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 10">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">skillcost_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">readClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:otherwise>
                <td  width="102"><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			   
   		</xsl:for-each>
 		</tbody>
 	</xsl:template>
</xsl:stylesheet>


