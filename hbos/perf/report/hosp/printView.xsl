<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
		<root>
			<thead>
				<tr noWrap='true'>
					<xsl:if test="/root/tbody/tr[1]/td[7]!='nonono'">
						<td style='fontsize:maintitle;colspan:9'></td>
						<td style="display:none"/>
						<td style="display:none"/>
					</xsl:if>
					<xsl:if test="/root/tbody/tr[1]/td[7] = 'nonono'">
						<td style='fontsize:maintitle;colspan:7'></td>
					</xsl:if>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>


				</tr>
				
				<tr noWrap='true' class='mainHead'>
					<td noWrap='true'>维度</td>
					<td noWrap='true'>分数</td>
					<td noWrap='true'>二级指标</td>
					<xsl:if test="/root/tbody/tr[1]/td[7]!='nonono'">
						<td noWrap='true'>分数</td>
						<td noWrap='true'>三级指标</td>			
					</xsl:if>  			  
					<td noWrap='true'>计算单位</td>
					<td noWrap='true'>数据采集部门</td>
					<td noWrap='true'>评分标准</td>			  
					<td noWrap='true'>指标分数</td>
				</tr>
			</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="td1" select="./td[1]" />
				<xsl:variable name="td4" select="./td[4]" />
				<xsl:variable name="rowspan_1" select="count(/root/tbody/tr[td[1]=$td1])" />
				<xsl:variable name="rowspan_3" select="count(/root/tbody/tr[td[4]=$td4])" />
				<xsl:variable name="mpos" select="position()-1" />
				
				<xsl:if test="position()=1">
					<tr>
						<td rowspan="{$rowspan_1}">
							<xsl:value-of select="./td[1]" />
						</td>
						
						<td rowspan="{$rowspan_1}" align='right' valign="center">
							<xsl:value-of select="format-number(./td[3],'#,##0.00')" />
						</td>
						
						<td rowspan="{$rowspan_3}">
							<xsl:value-of select="td[4]" />
						</td>
						
						<xsl:if test="./td[7]!='nonono'">
							<td rowspan="{$rowspan_3}" align='right'>
								<xsl:value-of select="format-number(./td[6],'#,##0.00')" />
							</td>							
							<td>							
								<xsl:value-of select="td[7]" />
							</td>
						</xsl:if>
						
						<td><xsl:value-of select="td[19]" /></td>
						<td><xsl:value-of select="td[20]" /></td>
						<td><xsl:value-of select="td[22]" disable-output-escaping="yes"/></td>
						<td align='right'>
							<xsl:value-of select="format-number(td[11],'#,##0.00')"/>              
						</td>
					</tr>
				</xsl:if>
				
				<xsl:if test="position()>1">
					<tr>
						<xsl:if test="$td1 != ../tr[$mpos]/td[1]">
							<td rowspan="{$rowspan_1}">
								<xsl:value-of select="./td[1]" />
							</td>
							<td rowspan="{$rowspan_1}" align='right'>
								<xsl:value-of select="format-number(./td[3],'#,##0.00')" />
							</td>
							<td rowspan="{$rowspan_3}">
								<xsl:value-of select="td[4]" />
							</td>
							
							<xsl:if test="./td[7]!='nonono'">
								<td rowspan="{$rowspan_3}" align='right'>
									<xsl:value-of select="format-number(./td[6],'#,##0.00')" />
								</td>							
								<td>							
									<xsl:value-of select="td[7]" />
								</td>
							</xsl:if>
						</xsl:if>						
						
						<xsl:if test="$td1 = ../tr[$mpos]/td[1]">
							<td style="display:none"/>
							<td style="display:none"/>
							
							<xsl:if test="$td4 != (../tr[$mpos]/td[4])">
								<td rowspan="{$rowspan_3}">
									<xsl:value-of select="td[4]" />
								</td>
								
								<xsl:if test="./td[7]!='nonono'">
									<td rowspan="{$rowspan_3}" align='right'>
										<xsl:value-of select="format-number(./td[6],'#,##0.00')" />
									</td>							
									<td>							
										<xsl:value-of select="td[7]" />
									</td>
								</xsl:if>
							</xsl:if>
							
							<xsl:if test="$td4 = (../tr[$mpos]/td[4])">
								<td style="display:none"/>
								
								<xsl:if test="./td[7]!='nonono'">
									<td rowspan="{$rowspan_3}" align='right'>
										<xsl:value-of select="format-number(./td[6],'#,##0.00')" />
									</td>							
									<td>							
										<xsl:value-of select="td[7]" />
									</td>
								</xsl:if>
							</xsl:if>

						</xsl:if>						
						
						
						
						<td><xsl:value-of select="td[19]" /></td>
						<td><xsl:value-of select="td[20]" /></td>
						<td><xsl:value-of select="td[22]" disable-output-escaping="yes"/></td>
						<td align='right'>
							<xsl:value-of select="format-number(td[11],'#,##0.00')"/>              
						</td>
					</tr>
				</xsl:if>
				
			</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>

</xsl:stylesheet>