<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test=" position() &lt; 4 ">
	    			<xsl:variable name="rows" select="position()"/>
	    			<tr noWrap='true' class='mainHead'>
	    				<xsl:if test=" $rows =1 ">
	          		<td  rowspan="3" valign="middle" width='60' >科室代码</td>
	          		<td rowspan="3" valign="middle" width='80' >科室名称</td>
          		</xsl:if>
          		<xsl:if test=" $rows =2 ">
	          		<td style="display:none" >科室代码</td>
	          		<td style="display:none" >科室名称</td>
          		</xsl:if>
          		<xsl:if test=" $rows =3 ">
	          		<td style="display:none" >科室代码</td>
	          		<td style="display:none" >科室名称</td>
          		</xsl:if>
		          <xsl:for-each select="td">
		          	<xsl:variable name="cols" select="position()"/>
		          	<xsl:variable name="cols_name" select="."/>
		          	<xsl:variable name="colNums" select="count(/root/tbody/tr[ $rows ]/td[.= $cols_name]) "/>
		          	<xsl:choose>
			          	<xsl:when test=" position()>2 and position()!=last()">
			          		<xsl:if test=" $rows=1">
				          		<xsl:if test=" ../td[$cols]!= ../td[$cols -1 ]">
				          			<td>
				          				<xsl:attribute name="colspan" >
				          				<xsl:value-of select="$colNums"/>
						            	</xsl:attribute>
						            	<xsl:value-of select="."/>
				          			</td>
					          	</xsl:if>
					          	<xsl:if test=" ../td[$cols] = ../td[$cols -1 ]">
				          			<td style="display:none" >
						            	<xsl:value-of select="."/>
				          			</td>
					          	</xsl:if>
					          </xsl:if>
					          <xsl:if test=" $rows=2">
				          		<xsl:if test=" ../td[$cols]!= ../td[$cols -1 ]">
				          			<td>
				          				<xsl:attribute name="colspan" >
				          				<xsl:value-of select="$colNums"/>
						            	</xsl:attribute>
						            	<xsl:if test=" ../td[$cols]= ../../tr[3]/td[$cols ]">
							            	<xsl:attribute name="rowspan" >
					          					<xsl:value-of select="2"/>
							            	</xsl:attribute>
						            	</xsl:if>
						            	<xsl:value-of select="."/>
				          			</td>
					          	</xsl:if>
					          	<xsl:if test=" ../td[$cols] = ../td[$cols -1 ]">
				          			<td style="display:none" >
						            	<xsl:value-of select="."/>
				          			</td>
					          	</xsl:if>
					          </xsl:if>
					          
					          <xsl:if test=" $rows=3">
				          		<xsl:if test=" ../td[$cols]!= ../td[$cols -1 ]">
				          			<td>
				          				<xsl:attribute name="colspan" >
				          				<xsl:value-of select="$colNums"/>
						            	</xsl:attribute>
						            	<xsl:if test=" ../td[$cols]= ../../tr[2]/td[$cols ]">
							            	<xsl:attribute name="style" >
					          					<xsl:value-of select="'display:none'"/>
							            	</xsl:attribute>
						            	</xsl:if>
						            	<xsl:value-of select="."/>
				          			</td>
					          	</xsl:if>
					          	<xsl:if test=" ../td[$cols] = ../td[$cols -1 ]">
				          			<td style="display:none" >
						            	<xsl:value-of select="."/>
				          			</td>
					          	</xsl:if>
					          </xsl:if>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		          <xsl:if test=" $rows =1 ">
	          		<td rowspan="3" valign="middle" width='60' >总分</td>
          		</xsl:if>
          		<xsl:if test=" $rows =2 ">
	          		<td style="display:none" >总分</td>
          		</xsl:if>
          		<xsl:if test=" $rows =3 ">
	          		<td style="display:none" >总分</td>
          		</xsl:if>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test=" position()>3 ">
	    			<tr>
		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		<xsl:when test=" position()=1 ">
			            	<td width="60"><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test=" position()=2 ">
			            	<td width="80"><xsl:value-of select="."/></td>
			            </xsl:when>
			          	<xsl:when test="position() > 2">
			            	<td align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>