<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>选择</th>
				<th nowrap='true'>函数代码</th>
				<th nowrap='true'>函数说明</th>
				<th nowrap='true'>函数格式</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center">
						<input type="radio" TABINDEX="-1" name="radioFunc" onclick="radioClick(this);">
							<xsl:attribute name="fNote"><xsl:value-of select="pk/fun_note"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="pk/fun_code"/></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<td><xsl:value-of select="." /></td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
