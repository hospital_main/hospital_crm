<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'>
					<input type='checkbox' style='display:none'/>
				</th>
				<th nowrap='true' width="60">指标名称</th>
				<th nowrap='true' width="40">单位</th>
				<th nowrap='true' width="60">基准值</th>
				<th nowrap='true' width="60">目标值</th>
				<th nowrap='true' width="80">最佳目标值</th>
				<th nowrap='true' width="100">小于基准值分数</th>
				<th nowrap='true' width="100">小于目标值分数</th>
				<th nowrap='true' width="100">小于最佳值分数</th>
				<th nowrap='true' width="100">大于最佳值分数</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;display:none;" >
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1" >
								<td align="left">									
									<a tabindex="-2" style="text-decoration:none">
										<xsl:attribute name="href">
											javascript:openDialog('viewTarget.html?load=&lt;sequence_no&gt;<xsl:value-of select="../pk"/>&lt;/sequence_no&gt;', 'dialogWidth:570px;dialogHeight:600px', result)
										</xsl:attribute> 
										<xsl:value-of select="." />  
									</a>								
								</td>
							</xsl:when>												
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
