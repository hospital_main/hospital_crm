<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th >
					<input type='checkbox' onclick='setAll1();'/>
				</th>
				<th nowrap='true' >基本指标</th>
				<th nowrap='true' >目标值</th>
				<th nowrap='true' >计算公式</th>
				<th nowrap='true' >数据来源</th>
				<th nowrap='true' >统计部门</th>
				<th nowrap='true' id="calcuteValue" >权重</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" >
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;" name="factorPk" onclick='checkAll1();'>
							<xsl:attribute name="value"><xsl:for-each select="pk/*"><xsl:value-of select="."/></xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>	
							<xsl:when test="position()=6" >
								<td align="left">									
									<input type="text" TABINDEX="-1" onblur="calculate(this)" name="weight" style="font-size:12px;border:1px solid gray;text-align:right;" size="10" >
										<xsl:attribute name="id"><xsl:value-of select="../pk"/></xsl:attribute>
										<xsl:attribute name="target_name"><xsl:value-of select="../td[1]"/></xsl:attribute>										
										<xsl:attribute name="value" ><xsl:value-of select="."/></xsl:attribute>	%								
									</input>							
								</td>
							</xsl:when>	
							<xsl:when test="position()=7" >

							</xsl:when>																									
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
