<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th >选择</th>
				<th nowrap='true' >基本指标代码</th>
				<th nowrap='true' >基本指标名称</th>
				<th nowrap='true' >数据来源</th>												
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" >
						<input type="radio" TABINDEX="-1" name="radiofactor" onclick="facotrPro(this)"  >
							<xsl:attribute name="factor_code"><xsl:value-of select="td[1]"/></xsl:attribute>				
							<xsl:attribute name="formula_chn"><xsl:value-of select="td[3]"/></xsl:attribute>						
						</input>
					</td>
					<xsl:for-each select="td">

							<td>
								<xsl:value-of select="." />
							</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
