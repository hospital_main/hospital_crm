<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				
				<th width="100" style="display:none">题目代码</th>
				<th width="200">题目名称</th>
				<th width="100">题目描述</th>
				<th width="100" style="overflow:hidden">问卷类别</th>
				<th width="100" style="overflow:hidden">答案名称</th>
				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">
						<xsl:choose>
							
							<xsl:when test="position()=1">
								<td style="display:none">
									<xsl:value-of select="."/>
									
								</td>
							</xsl:when>
							
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
