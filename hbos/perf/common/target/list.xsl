<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th >选择</th>
				<th nowrap='true' >指标代码</th>
				<th nowrap='true' >指标名称</th>
				<th nowrap='true' width="140">评测目的</th>												
				<th nowrap='true' >收集部门</th>												
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" >
						<input type="radio" TABINDEX="-1" name="radiofactor" onclick="Prochecked(this)"  >
							<xsl:attribute name="target_code"><xsl:value-of select="td[1]"/></xsl:attribute>				
							<xsl:attribute name="target_name"><xsl:value-of select="td[2]"/></xsl:attribute>						
							<xsl:attribute name="value"><xsl:for-each select="pk/*"><xsl:value-of select="."/></xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=3" >
								<td align="left" style="width:140px;word-break;keep-all">									
									<xsl:value-of select="." />								
								</td>
							</xsl:when>																			
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
