/******1、指定目标、科室分类的指标树操作函数******/
/*
功能描述：指定目标、科室分类的指标树参数对象
属性说明：
stratagemSequenceNo		战略目标序列号，必输项
perfUnitCode 					科室分类代码，必输项
bulletinFrequency			考核期间：All:全部;M:月;Q:季;H:半年;Y:年;默认为All;
perfMonth							绩效月份：0:全年;1:1月;…;12:12月;默认为0;
isWeight							节点是否加权重：1：是;0：否;默认为0;
appraiseMethod				评分方法：All：全部;I:区间法;DA:扣、加分法;默认为'All';
*/
function TargetUnitTreeParam(){
 	this.stratagemSequenceNo = '';
 	this.perfUnitCode        = '';
 	this.bulletinFrequency   = '';
 	this.perfMonth           = '';
 	this.isWeight            = '';
	this.appraiseMethod      = '';
	this.check = checkUTParamValid;
	this.toString = getSubmitUTParam;
}

function checkUTParamValid(){
	if(!this.stratagemSequenceNo){
		alert("构建指标树时缺少战略目标序列号！");
		return false;
	}

	if(!this.perfUnitCode){
		alert("构建指标树时缺少科室分类代码！");
		return false;
	}

	return true;
}

function getSubmitUTParam(){
	var param = "<stratagemSequenceNo>" + this.stratagemSequenceNo + "</stratagemSequenceNo>";
	param += "<perfUnitCode>"        + this.perfUnitCode        + "</perfUnitCode>";
	param += "<bulletinFrequency>"   + this.bulletinFrequency   + "</bulletinFrequency>";
	param += "<perfMonth>"           + this.perfMonth           + "</perfMonth>";
	param += "<isWeight>"            + this.isWeight            + "</isWeight>";
	param += "<appraiseMethod>"      + this.appraiseMethod      + "</appraiseMethod>";
	//alert(param);
	return param;
}

/*
功能描述：指定目标、科室分类指标树初始化操作
参数说明：
sTreeCtnObj：页面中定义的sTreeCtn对象的ID值，如：window.unitTree
isChk：指定指标树是否为多选树（true：是；false：否）
initParam：包含初始化参数值的TargetUnitTreeParam对象
*/
function initTargetUnitTree(sTreeCtnObj, isChk, initParam){
	initTree(sTreeCtnObj, isChk);
	if(!isValidTreeParam(sTreeCtnObj, initParam))
		return;
		
	sTreeCtnObj.setSqlData("perfCommonTargetUnitTreeQuery", initParam.toString(), "");
}

/*
功能描述：指定目标、科室分类指标树刷新操作
参数说明：
sTreeCtnObj：页面中定义的sTreeCtn对象的ID值，如：window.unitTree
param：包含刷新参数值的TargetUnitTreeParam对象
*/
function freshTargetUnitTree(sTreeCtnObj, param){
	if(!isValidTreeParam(sTreeCtnObj, param))
		return;

	sTreeCtnObj.setSqlData("perfCommonTargetUnitTreeQuery", param.toString(), "");
}
/******2、指定目标、科室分类、科室的指标树操作函数******/
/*
功能描述：指定目标、科室分类、科室的指标树参数对象
属性说明：
stratagemSequenceNo		战略目标序列号，必输项
perfUnitCode 					科室分类代码，必输项
deptCode							科室代码，必输项
bulletinFrequency			考核期间：All:全部;M:月;Q:季;H:半年;Y:年;默认为All;
perfMonth							绩效月份：0:全年;1:1月;…;12:12月;默认为0;
isWeight							节点是否加权重：1：是;0：否;默认为0;
appraiseMethod				评分方法：All：全部;I:区间法;DA:扣、加分法;默认为'All';
*/
function TargetUnitDeptTreeParam(){
 	this.stratagemSequenceNo = '';
 	this.perfUnitCode        = '';
 	this.deptCode						 = '';
 	this.bulletinFrequency   = '';
 	this.perfMonth           = '';
 	this.isWeight            = '';
	this.appraiseMethod      = '';
	this.check = checkUDTParamValid;
	this.toString = getSubmitUDTParam;
}

function checkUDTParamValid(){
	if(!this.stratagemSequenceNo){
		alert("构建指标树时缺少战略目标序列号！");
		return false;
	}

	if(!this.perfUnitCode){
		alert("构建指标树时缺少科室分类代码！");
		return false;
	}


	if(!this.deptCode){
		alert("构建指标树时缺少科室代码！");
		return false;
	}
	
	return true;
}

function getSubmitUDTParam(){
	var param = "<stratagemSequenceNo>" + this.stratagemSequenceNo + "</stratagemSequenceNo>";
	param += "<perfUnitCode>"        + this.perfUnitCode        + "</perfUnitCode>";
	param += "<deptCode>"            + this.deptCode            + "</deptCode>";
	param += "<bulletinFrequency>"   + this.bulletinFrequency   + "</bulletinFrequency>";
	param += "<perfMonth>"           + this.perfMonth           + "</perfMonth>";
	param += "<isWeight>"            + this.isWeight            + "</isWeight>";
	param += "<appraiseMethod>"      + this.appraiseMethod      + "</appraiseMethod>";
	//alert(param);
	return param;
}

/*
功能描述：指定目标、科室分类、科室指标树初始化操作
参数说明：
sTreeCtnObj：页面中定义的sTreeCtn对象的ID值，如：window.unitTree
isChk：指定指标树是否为多选树（true：是；false：否）
initParam：包含初始化参数值的TargetUnitDeptTreeParam对象
*/
function initTargetUnitDeptTree(sTreeCtnObj, isChk, initParam){
	initTree(sTreeCtnObj, isChk);
	if(!isValidTreeParam(sTreeCtnObj, initParam))
		return;
		
	sTreeCtnObj.setSqlData("perfCommonTargetUnitDeptTreeQuery", initParam.toString(), "");
}

/*
功能描述：指定目标、科室分类、科室指标树刷新操作
参数说明：
sTreeCtnObj：页面中定义的sTreeCtn对象的ID值，如：window.unitTree
param：包含刷新参数值的TargetUnitDeptTreeParam对象
*/
function freshTargetUnitDeptTree(sTreeCtnObj, param){
	if(!isValidTreeParam(sTreeCtnObj, param))
		return;

	sTreeCtnObj.setSqlData("perfCommonTargetUnitDeptTreeQuery", param.toString(), "");
}
/******3、公用操作函数******/
/*
功能描述：获取指标树上选中的节点值
参数说明：
sTreeCtnObj：页面中定义的sTreeCtn对象的ID值，如：window.unitTree
返回值：包含选中节点值的二维数组
内层数组格式：
数组索引   数组值
	0			   指标代码；
	1			   序列号；
	2			   是否末级（1：是；0：否）
*/
function getCheckedValues(sTreeCtnObj){
	var val = sTreeCtnObj.getCheckValues("on");
	//window.prompt("", val);
	return getArrayValue(val);
}

function getArrayValue(xmlstr){
	var val = new Array();
	if(!xmlstr)
		return val;
		
  var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
  xmlDoc.async = false;
  xmlDoc.loadXML("<root>" + xmlstr + "</root>");
  var recordNodes = xmlDoc.documentElement.childNodes;
  for(var i = 0; i < recordNodes.length; i++){
  	var vNodes = recordNodes.item(i).childNodes;
  	var r = new Array();
  	for(var j = 0; j < 3; j++){
  		r[j] = vNodes.item(j).childNodes.item(0).nodeValue;
  	}
 	
  	val[i] = r;
  }
	
	//alert(val);
	return val;
}

/*
功能描述：点击指标树节点时触发的JS函数，并且指标树负责提供下述四个参数值。在定义sTreeCtn树对象时指定的action属性值即为此函数。
参数说明：
isLeaf：是否末级节点（是：true；否：false）
pk：节点的主键内容，格式：<tg_code>指标代码</tg_code><seq_no>序列号</seq_no><last>是否末级</last><chk_flag>是否选中</chk_flag>，
	如：<tg_code>L030101</tg_code><seq_no>4</seq_no><last>1</last><chk_flag>0</chk_flag>，<tg_code>L0301</tg_code><seq_no>4</seq_no><last>0</last><chk_flag>1</chk_flag>
	“是否末级”：1：是；0：否；“是否选中”：1：是；0：否
parameter：节点的参数内容，格式：指标代码;序列号;是否末级，如：L030101;4;1，L0301;4;0
	“是否末级”：1：是；0：否
label：节点上显示的文本内容
*/
/*function itemClick(isLeaf, pk, parameter, label){
	alert("isLeaf:" + isLeaf + "\r\rpk:" + pk + "\r\rparameter:" + parameter + "\r\rlabel:" + label);
}*/

function isValidTreeParam(treeObj, paramObj){
	return (treeObj && paramObj.check());
}

function initTree(sTreeCtnObj, isChk){
	sTreeCtnObj.kind = isChk ? "check" : "";
	if(isChk){
		sTreeCtnObj.checkCascade = "true";
		sTreeCtnObj.cascadeMode = "One";
	}

	sTreeCtnObj.tagname = "chk_flag";
	sTreeCtnObj.expand = "10000";
}