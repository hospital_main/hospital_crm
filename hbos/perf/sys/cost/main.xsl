<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th nowrap='true'>项目代码</th>
		  	<th nowrap='true'>项目名称</th>
		  	<th nowrap='true'>参与绩效计算<span><input type='checkbox' id="calc">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span></th>
		  	<th nowrap='true'>直接成本<span><input type='checkbox' id="prime">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span></th>
		  	<th nowrap='true'>分摊公用成本<span><input type='checkbox' id="public">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span></th>
		  	<th nowrap='true'>分摊管理成本<span><input type='checkbox' id="cost2">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span></th>
		  	<th nowrap='true'>分摊医疗辅助成本<span><input type='checkbox' id="cost3">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span></th>
		  	<th nowrap='true'>分摊医疗技术成本<span><input type='checkbox' id="cost4">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span></th>
      </tr>
    </thead>
    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <td align='center' style='display:none'>
            <div><input type='checkbox'>
	            <xsl:attribute name="id">id_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
        	    </xsl:attribute>
     			  </input></div>
     			</td>

          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position() = 3">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">calc_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">calcClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 4">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">prime_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">primeClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 5">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">public_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">publicClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 6">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">cost2_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">publicClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 7">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">cost3_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">publicClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 8">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">cost4_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">publicClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
     </tbody>
  </xsl:template>
</xsl:stylesheet>