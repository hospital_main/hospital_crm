<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>指标</th>
				<th nowrap='true'>科室</th>
				<th nowrap='true'>目标值</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:attribute name="_Pk"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
				<xsl:for-each select="td[position()&lt;5]">   
						<xsl:choose>
							<xsl:when test="position() = 3">
					<td align="right">
								<input type='text' name='targetValues' class='inputTextA'>
									<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>
									<xsl:attribute name="pk_target"><xsl:value-of select="../pk/target_code"/></xsl:attribute>																															
									<xsl:attribute name="pk_dept"><xsl:value-of select="../pk/dept_code"/></xsl:attribute>																															
								</input>
					</td>
							</xsl:when>
							<xsl:otherwise>
					<td>
								<xsl:value-of select="."/>  
					</td>
							</xsl:otherwise>
						</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>



