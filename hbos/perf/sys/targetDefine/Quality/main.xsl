<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
		  	<th style='display:none' width='25'><input type='checkbox'/></th>
				<th nowrap='true' width="120">指标名称</th>
				<th nowrap='true' width="60">计量单位</th>
				<th nowrap='true' width="160">评测目的</th>
				<th nowrap='true' width="160">描述</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				  <td align='center'  noWrap='true'  style='display:none'>
            <input name="cbSeqNo" type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&quot;<xsl:value-of select="."/>&quot;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>					
          <xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1" >
								<td align="left">									
									<a tabindex="-2" style="text-decoration:none">
										<xsl:attribute name="href">
											javascript:openDialog('update.html?load=&lt;sequence_no&gt;<xsl:value-of select="../pk"/>&lt;/sequence_no&gt;', 'dialogWidth:570px;dialogHeight:600px', '')
										</xsl:attribute> 
										<xsl:value-of select="." />  
									</a>								
								</td>
							</xsl:when>						
							<xsl:when test="position()=2" >
								<td align="center" >
										<xsl:value-of select="." />
								</td>
							</xsl:when>							
							<xsl:when test="position()=3" >
								<td align="left" style="width:160px;word-break;keep-all;">
										<xsl:value-of select="." />
								</td>
							</xsl:when>			
							<xsl:when test="position()=4" >
								<td align="left" style="width:160px;word-break;keep-all;">
										<xsl:value-of select="." />
								</td>
							</xsl:when>													
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
