<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'>
					<input type='checkbox' style='display:none'/>
				</th>
				<th nowrap='true'  width="30">选项</th>
				<th nowrap='true' width="100">年度</th>
				<th nowrap='true' width="120">战略目标</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;display:none;" >
							<xsl:attribute name="value"><xsl:for-each select="pk/*"><xsl:value-of select="."/></xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1" >
								<td align="center">
									<input type="radio" name="radiobutton" onclick="clickRadio(this);">
										<xsl:attribute name="id"><xsl:value-of select="../pk"/></xsl:attribute>
									</input>							
								</td>
							</xsl:when>
							<xsl:when test="position()=3" >
								<td align="center">
									<a tabindex="-2" onclick="click_A(this)" href="#">
										<xsl:attribute name="id"><xsl:value-of select="../pk"/></xsl:attribute>										
										 <xsl:value-of select="."/>
									</a>								
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td align="center">
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
