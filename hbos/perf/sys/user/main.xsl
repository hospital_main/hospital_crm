<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>用户代码</th>
				<th nowrap='true'>用户名称</th>
				<th nowrap='true' width='50%'>用户描述</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
			  <xsl:attribute name="_groupId"><xsl:value-of select="td[4]"/></xsl:attribute>
				<xsl:for-each select="td[position() &lt; 4]">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 1">
								<a>
									<xsl:attribute name="onclick">Link_OnClick();</xsl:attribute>
									<xsl:attribute name="href">#</xsl:attribute>
									<xsl:value-of select="."/>
								</a>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>