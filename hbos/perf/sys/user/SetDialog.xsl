<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' width="25%">基本指标代码</th>
				<th nowrap='true' width="40%"><input type='checkbox' style="display:none" />基本指标名称</th>
				<th nowrap='true'>是否有权限<span><input type='checkbox' id="read">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span></th>
				<!--<th nowrap='true'>写权限<span><input type='checkbox' id="write">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span></th>
				<th nowrap='true'>审核权限<span><input type='checkbox' id="check">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span></th>-->
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
			  <xsl:attribute name="_factorCode"><xsl:value-of select="td[1]"/></xsl:attribute>
				<xsl:for-each select="td">   
					<td>
							<xsl:choose>
							  <xsl:when test="position() = 3">
							    <xsl:attribute name="align">center</xsl:attribute>
									<input type='checkbox' name='is_read' onclick='IsRead_OnClick();'>
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
										<xsl:if test=" text() = 1">
											<xsl:attribute name="checked">true</xsl:attribute>
										</xsl:if>
									</input>
								</xsl:when>
								<!--<xsl:when test="position() = 4">
								  <xsl:attribute name="align">center</xsl:attribute>
									<input type='checkbox' name='is_write' onclick='IsWrite_OnClick();'>
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
										<xsl:if test=" text() = 1">
											<xsl:attribute name="checked">true</xsl:attribute>
										</xsl:if>
									</input>
								</xsl:when>
								<xsl:when test="position() = 5">
								  <xsl:attribute name="align">center</xsl:attribute>
									<input type='checkbox' name='is_check' onclick='IsCheck_OnClick();'>
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
										<xsl:if test=" text() = 1">
											<xsl:attribute name="checked">true</xsl:attribute>
										</xsl:if>
									</input>
								</xsl:when>-->
								<xsl:otherwise>
								  <xsl:attribute name="align">center</xsl:attribute>
									<xsl:value-of select="."/>  
								</xsl:otherwise>
							</xsl:choose> 
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>