<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			  <th nowrap='true'>年度</th>
				<th nowrap='true' width='50%'>目标名称</th>
				<th nowrap='true' width='10%'>状态</th>
				<th nowrap='true'>是否虚拟</th>
				<th nowrap='true' ><input type='checkbox'  style="display:none"/>是否有权限</th>
				<th nowrap='true'>是否当前战略</th>
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:attribute name="_rowId"><xsl:value-of select="td[7]"/></xsl:attribute>
					<xsl:attribute name="_stateFlag"><xsl:value-of select="td[3]"/></xsl:attribute>
					<xsl:attribute name="_isDummy"><xsl:value-of select="td[4]"/></xsl:attribute>
					<xsl:for-each select="td[position()&lt;7]">   
						<td>
							<xsl:choose>
							  <xsl:when test="position() = 2">
								  <xsl:attribute name="align">left</xsl:attribute>
								  <xsl:value-of select="."/>
								</xsl:when>
							  <xsl:when test="position() = 5">
							    <xsl:attribute name="align">center</xsl:attribute>
									<input type='checkbox' name='has_purview' onclick='HasPurview_OnClick();'>
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
										<xsl:if test=" text() = 1">
											<xsl:attribute name="checked">true</xsl:attribute>
										</xsl:if>
									</input>
								</xsl:when>
								<xsl:when test="position() = 6">
								  <xsl:attribute name="align">center</xsl:attribute>
									<input type='radio' name='is_current' onclick='IsCurrent_OnClick();'>
									  <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
									  <xsl:if test=" text() = 1">
											<xsl:attribute name="checked">true</xsl:attribute>
										</xsl:if>
									</input>
								</xsl:when>
								<xsl:otherwise>
								  <xsl:attribute name="align">center</xsl:attribute>
									<xsl:value-of select="."/>  
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
