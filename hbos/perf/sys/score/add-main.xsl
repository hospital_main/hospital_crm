<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' width="120">指标名称</th>
				<th nowrap='true' width="60">计量单位</th>
				<th nowrap='true' width="80">加减</th>
				<th nowrap='true' width="80">加减量</th>
				<th nowrap='true' width="80">加减分</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				        <xsl:variable name="ad_flag" select="td[3]"/>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=2   ">
								<td>
									<xsl:if test="../td[6] = 0"></xsl:if>
									<xsl:if test="../td[6] = 1">
										<xsl:value-of select="." />
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test=" position()=3   ">
								<xsl:if test="../td[6] = 0">
									<td></td>
								</xsl:if>
								<xsl:if test="../td[6] = 1">
									<td >
										
										<select name="objSelect"  style="width:100px" > 
											<xsl:attribute name="vv">
														<xsl:value-of select="." />
											</xsl:attribute>	
											<xsl:attribute name="id">
														<xsl:value-of select="../td[7]" />
											</xsl:attribute>
											<xsl:if test = '$ad_flag = &quot;D&quot;'>
												<xsl:text disable-output-escaping="yes"><![CDATA[<option value="D" selected >扣分法</option> <option value="A">加分法</option>]]></xsl:text>
											</xsl:if>
											<xsl:if test = '$ad_flag = &quot;A&quot;'>
												<xsl:text disable-output-escaping="yes"><![CDATA[<option value="D"  >扣分法</option> <option value="A" selected >加分法</option>]]></xsl:text>
											</xsl:if>
										</select>				
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="  position()=4">
								<xsl:if test="../td[6] = 0">
									<td></td>
								</xsl:if>	
								<xsl:if test="../td[6] = 1">														
									<td>
										<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="12">
											<xsl:attribute name="value">
												<xsl:value-of select="format-number(.,'#,##0.00')" />
											</xsl:attribute>
											<xsl:attribute name="name">
												<xsl:value-of select="../td[7]" />
											</xsl:attribute>
											<xsl:attribute name="is_last">
												<xsl:value-of select="../td[6]" />
											</xsl:attribute>
										</input>
									</td>
								</xsl:if>									
							</xsl:when>
							<xsl:when test="  position()=5">
								<xsl:if test="../td[6] = 0">
									<td></td>
								</xsl:if>	
								<xsl:if test="../td[6] = 1">									
									<td>
										<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="12">
											<xsl:attribute name="value">
													<xsl:value-of select="." />
											</xsl:attribute>
											<xsl:attribute name="name">
												<xsl:value-of select="../td[7]" />
											</xsl:attribute>
											<xsl:attribute name="is_last">
												<xsl:value-of select="../td[6]" />
											</xsl:attribute>
										</input>
									</td>
								</xsl:if>									
							</xsl:when>							
							<xsl:when test="position()=6 or position()=7 ">
								<td style="display:none">
									<xsl:value-of select="." />
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
