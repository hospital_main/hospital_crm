<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th width='60'>选择</th>
				<th>指标名称</th>
				<th>计量单位</th>
				<th>极值</th>
				<th>下限2</th>
				<th>下限1</th>
				<th>最佳值</th>
				<th>上限1</th>
				<th>上限2</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:if test="./td[9] = 0">
						<td align='center' noWrap='true'>
							<input type='radio' name="radioPerf" TABINDEX='-1' onclick="getValue(this)" style='font-size:8px;display:none'>
								<xsl:attribute name="value">
									<xsl:for-each select="pk/*">
										<xsl:value-of select="." />
									</xsl:for-each>
								</xsl:attribute>
								<xsl:attribute name="is_last">
									<xsl:value-of select="./td[10]" />
								</xsl:attribute>
							</input>
						</td>
					</xsl:if>
					<xsl:if test="./td[9] = 1">
						<td align='center' noWrap='true'>
							<input type='radio' name="radioPerf" TABINDEX='-1' onclick="getValue(this)" style='font-size:8px;'>
								<xsl:attribute name="value">
									<xsl:for-each select="pk/*">
										<xsl:value-of select="." />
									</xsl:for-each>
								</xsl:attribute>
								<xsl:attribute name="is_last">
									<xsl:value-of select="./td[10]" />
								</xsl:attribute>
							</input>
						</td>
					</xsl:if>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=2 or position()=3">
								<td noWrap='true'>
									<xsl:if test="../td[9] = 0"></xsl:if>
									<xsl:if test="../td[9] = 1">
										<xsl:value-of select="." />
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test=" position()=5">
								<td noWrap='true'>
									<xsl:if test="../td[9] = 0"></xsl:if>
									<xsl:if test="../td[9] = 1">
										<xsl:value-of select="format-number(.,'#,##0.00')" />
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()=4">
								<td noWrap='true'>
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="12">
										<xsl:attribute name="value">
											<xsl:value-of select="format-number(.,'#,##0.00')" />
										</xsl:attribute>
										<xsl:attribute name="name">
											<xsl:value-of select="../td[10]" />
										</xsl:attribute>
										<xsl:attribute name="is_last">
											<xsl:value-of select="../td[9]" />
										</xsl:attribute>
									</input>
									<input type="hidden" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="12">
										<xsl:attribute name="value">
											<xsl:value-of select="format-number(../td[5],'#,##0.00')" />
										</xsl:attribute>
										<xsl:attribute name="name">
											<xsl:value-of select="../td[10]" />
										</xsl:attribute>
										<xsl:attribute name="is_last">
											<xsl:value-of select="../td[9]" />
										</xsl:attribute>
									</input>
								</td>
							</xsl:when>
							<xsl:when test="position()=6">
								<td noWrap='true'>
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="12">
										<xsl:attribute name="value">
											<xsl:value-of select="format-number(.,'#,##0.00')" />
										</xsl:attribute>
										<xsl:attribute name="name">
											<xsl:value-of select="../td[10]" />
										</xsl:attribute>
										<xsl:attribute name="is_last">
											<xsl:value-of select="../td[9]" />
										</xsl:attribute>
									</input>
								</td>
							</xsl:when>
							<xsl:when test="position()=7">
								<td noWrap='true'>
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="12">
										<xsl:attribute name="value">
											<xsl:value-of select="format-number(.,'#,##0.00')" />
										</xsl:attribute>
										<xsl:attribute name="name">
											<xsl:value-of select="../td[10]" />
										</xsl:attribute>
										<xsl:attribute name="is_last">
											<xsl:value-of select="../td[9]" />
										</xsl:attribute>
									</input>
								</td>
							</xsl:when>
							<xsl:when test="position()=8">
								<td noWrap='true'>
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="12">
										<xsl:attribute name="value">
											<xsl:value-of select="format-number(.,'#,##0.00')" />
										</xsl:attribute>
										<xsl:attribute name="name">
											<xsl:value-of select="../td[10]" />
										</xsl:attribute>
										<xsl:attribute name="is_last">
											<xsl:value-of select="../td[9]" />
										</xsl:attribute>
									</input>
								</td>
							</xsl:when>
							<xsl:when test="position()=8 or position()=9  or position()=10  ">
								<td style="display:none">
									<xsl:value-of select="." />
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap='true'>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
