<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th >选择</th>
				<th nowrap='true' >基本指标代码</th>
				<th nowrap='true' >基本指标名称</th>
				<th nowrap='true' >说明</th>
				<th nowrap='true' >性质</th>
				<th nowrap='true' >对应函数名称</th>
				<th nowrap='true'>计算公式</th>
				<th nowrap='true'>是否绩效用</th>
				<th nowrap='true'>是否奖金用</th>												
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" >
						<input type="radio" TABINDEX="-1" name="checkedradio" >
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=2" >
								<td align="left">									
									<a tabindex="-2" style="text-decoration:none" >
										<xsl:attribute name="href">
											javascript:openDialog('update.html?load=&lt;sequence_no&gt;<xsl:value-of select="../pk"/>&lt;/sequence_no&gt;', 'dialogWidth:700px;dialogHeight:450px', result)
										</xsl:attribute> 
										<xsl:value-of select="." />  
									</a>								
								</td>
							</xsl:when>
							<xsl:when test="position()=4" >
								<td align="center" style="width:140px;word-break;keep-all">
										<xsl:value-of select="." />
								</td>
							</xsl:when>			
							<xsl:when test="position()=5" >
								<td align="center" style="width:140px;word-break;keep-all">
										<xsl:value-of select="." />
								</td>
							</xsl:when>													
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
