<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
				<th nowrap="true">科室代码</th>
				<th nowrap="true">科室名称</th>
				<th nowrap="true">指标值</th>
      </tr>
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td[position()&lt;4]">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 3">
								<input type='text' name='target_value' class='inputTextA'>
									<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
									<xsl:attribute name="sequence_no"><xsl:value-of select="../td[4]"/></xsl:attribute>
									<xsl:attribute name="dept_code"><xsl:value-of select="../td[1]"/></xsl:attribute>
								</input>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
     </tbody>
  </xsl:template>
</xsl:stylesheet>