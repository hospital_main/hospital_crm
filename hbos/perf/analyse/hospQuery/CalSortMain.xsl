<?xml version='1.0' encoding="GB2312" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=last()">
	    			<tr noWrap='true' class='mainHead'>
			    		<xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=1 or position()=2 or  position()=6  or  position()=9 or position()=10 or position()=11  or position()=12 or position()=13 or position()=14 or position()=15 or position()=16">
			          	
			          	</xsl:when>
			          	<xsl:when test="position()=5">
			          		<xsl:if test="/root/tbody/tr[1]/td[5]!='nonono'">
			          				<th nowrap='true'><xsl:value-of select="."/></th>
			          		</xsl:if>
			          		
			          	</xsl:when>
			          	<xsl:otherwise>
			          		
			          		<xsl:if test="position()!=last()">
			          			
			          			<th nowrap='true'><xsl:value-of select="."/></th>
			          		</xsl:if>
		                
		              </xsl:otherwise>
		            </xsl:choose>
		          </xsl:for-each>
	          </tr>
	        </xsl:when>
	      </xsl:choose>
	    </xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position()!=last()]">
				<xsl:variable name="unit_name" select="td[3]" />
				<xsl:variable name="kpi" select="td[4]" />
				<xsl:variable name="cur_pos" select="position()" />					
				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[3]=$unit_name])" />
				<xsl:variable name="rowspan2" select="count(/root/tbody/tr[td[4]=$kpi])" />
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
	    				<xsl:when test="position()=1 or position()=2 or position()=6 or   position()=9 or position()=10 or position()=11  or position()=12 or position()=13 or position()=14 or position()=15 or position()=16">
			        </xsl:when>
							
							<xsl:when test="position()=3">
								<xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos - 1]/td[3]">
									<td rowspan="{$rowspan}">
										<xsl:value-of select="." />
									</td>
								</xsl:if>
								<xsl:if test="$unit_name = ../tr[$cur_pos - 1]/td[3]">
									<td style="display:none2"><xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:when>
								
		
							
							<xsl:when test="position()=4">
								<xsl:if test="$cur_pos = 1 or $kpi != ../../tr[$cur_pos - 1]/td[4]">
									<td align="left" rowspan="{$rowspan2}">
			            		<xsl:if test="../td[5]='' or ../td[5]='nonono'">
												<xsl:value-of select="." disable-output-escaping="yes"/>
											</xsl:if>
											<xsl:if test="../td[5]!='' and ../td[5]!='nonono'">
													<xsl:value-of select="." disable-output-escaping="yes"/>
											</xsl:if>
			            </td>
			           </xsl:if>
							</xsl:when>
              <xsl:when test="position()=5">
              	<xsl:if test=".!='nonono'">
              				<xsl:if test=".!=''">
              					<td nowrap='true' align='left'>
													<xsl:value-of select="."/>
												</td>
											</xsl:if>
											<xsl:if test=".=''">
				            		<td nowrap='true' align='left'><xsl:value-of select="."/></td>
				            	</xsl:if>
			          </xsl:if>
			          
              </xsl:when>
						
							<xsl:when test="position() > 16 and ../td[16] = 1">
								<xsl:if test="position()!=last()">
			          			<xsl:choose>
											<xsl:when test="../td[13]=1">
												<td align="right">											
													<a tabindex="-2" style="text-decoration:none" bulletin_frequency="M"  href="#" onclick="win_Open2(this);" name="target_value">		
														<xsl:attribute name="sequence_no"><xsl:value-of select="../td[1]"/></xsl:attribute>
														<xsl:attribute name="appraise_method"><xsl:value-of select="../td[11]"/></xsl:attribute>
														<xsl:attribute name="dept_sequence_no"><xsl:value-of select="substring-before(.,'*')" /></xsl:attribute>																																																																																					
														<xsl:value-of select="format-number((substring-after(.,'*')),'#,##0.00')" />
													</a>									
						            </td>
											</xsl:when>
											<xsl:otherwise>
												<td></td>
											</xsl:otherwise>
										</xsl:choose>
			          </xsl:if>
								
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="position()!=last()">
									<td align="center">
						      	<xsl:value-of select="."/>
						      </td>
					      </xsl:if>
			          
								
							</xsl:otherwise>
						</xsl:choose>            
					</xsl:for-each>
				</tr>
				
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>