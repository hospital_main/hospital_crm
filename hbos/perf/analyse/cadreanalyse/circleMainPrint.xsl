<?xml version="1.0" encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='' />
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">6</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">6</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td nowrap='true' >指标名称</td>
					<td nowrap='true' >年月</td>
					<td nowrap='true' >绩效分数</td>
					<td nowrap='true' >环比率</td>
					<td nowrap='true' >目标值</td>
					<td nowrap='true' >实际值</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:variable name="unit_name" select="td[1]" />
					<xsl:variable name="rowspan" select="td[9]" />
					<xsl:variable name="cur_pos" select="position()" />
					<tr>
						<xsl:if test="$cur_pos = 1 or $unit_name != ../tr[$cur_pos - 1]/td[1]">
							<td align="center" rowspan="{$rowspan}">
								<xsl:value-of select="td[1]" />
							</td>
						</xsl:if>
						<xsl:if test="$cur_pos != 1">
							<td align="center"   >		
							</td>
						</xsl:if>
						<td align="center">
							<xsl:value-of select="td[2]"/>
						</td>
						<td align="center" >
							<a tabindex="-2" style="text-decoration:none" bulletin_frequency="M"  href="#" onclick="win_Open(this);" name="target_value">		
								<xsl:attribute name="sequence_no"><xsl:value-of select="td[7]"/></xsl:attribute>
								<xsl:attribute name="appraise_method"><xsl:value-of select="td[8]"/></xsl:attribute>																																																																																			
								<xsl:value-of select="format-number(td[3],'#,##0.00')" />  
							</a>
						</td>						
						<td align="center"><B>
							<xsl:value-of select="td[4]"/>
							</B>
						</td>	
						<td align="center">
							<xsl:value-of select="format-number(td[5],'#,##0.00')"/>
						</td>																
						<td align="center">
							<xsl:value-of select="format-number(td[6],'#,##0.00')"/>
						</td>														
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>