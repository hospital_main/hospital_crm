<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			  <th nowrap='true' >维度</th>
				<th nowrap='true' >KPI指标名称</th>
				<!-- <xsl:if test="/root/tbody/tr[1]/td[3]!='nonono'"> -->
					<th nowrap='true' >三级指标名称</th>
				<!-- </xsl:if> --> 
				<th nowrap='true' >目标值分数</th>
				<th nowrap='true' >指标值分数</th>							
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:variable name="cur_pos" select="position()" />
					<xsl:variable name="target0" select="td[1]" />
					<xsl:variable name="target1" select="td[2]" /> 
					<xsl:variable name="rowspan0" select="count(../tr[td[1]=$target0])" />
					<xsl:variable name="rowspan1" select="count(../tr[td[2]=$target1])" />
					<xsl:if test="$cur_pos = 1 or td[1] != ../tr[$cur_pos - 1]/td[1]">
						<td align="left" rowspan="{$rowspan0}">
							<xsl:value-of select="td[1]" />
						</td>
					</xsl:if>
					<xsl:if test="$cur_pos != 1 and td[1]= ../tr[$cur_pos - 1]/td[1]">
						<td style="display:none" >
							<xsl:value-of select="td[1]" />
						</td>
					</xsl:if>
					<xsl:if test="$cur_pos = 1 or td[2] != ../tr[$cur_pos - 1]/td[2]">
						<td align="left" rowspan="{$rowspan1}">
							<xsl:value-of select="td[2]" />
						</td>
					</xsl:if>
					<xsl:if test="$cur_pos != 1 and td[2]= ../tr[$cur_pos - 1]/td[2]">
						<td style="display:none" >
							<xsl:value-of select="td[2]" />
						</td>
					</xsl:if>
					<!-- <xsl:if test="/root/tbody/tr[1]/td[3]!='nonono'"> -->
					<td align="left">
						<xsl:value-of select="td[3]" />
					</td>			
					<!-- </xsl:if> -->
					<td class="numberText">
						<xsl:value-of select="td[5]" />
					</td>				
					<td class="numberText">
						<xsl:value-of select="td[6]" />
					</td>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>