<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' >指标名称</th>
				<th nowrap='true' >干部姓名</th>
				<th nowrap='true' >绩效分数</th>
				<th nowrap='true' >该指标的平均分</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="unit_name" select="td[1]" />
				<xsl:variable name="rowspan" select="td[9]" />
				<xsl:variable name="cur_pos" select="position()" />
				<tr>
					<xsl:if test="$cur_pos = 1 or $unit_name != ../tr[$cur_pos - 1]/td[1]">
						<td align="center" rowspan="{$rowspan}">
							<xsl:value-of select="td[1]" />
						</td>
					</xsl:if>
					<td align="center"><B name="emp_name">
						<xsl:value-of select="td[2]"/>
						</B>
					</td>
					<td align="center" >
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="M"  href="#" onclick="win_Open(this);" name="target_value">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[5]"/></xsl:attribute>
							<xsl:attribute name="appraise_method"><xsl:value-of select="td[6]"/></xsl:attribute>
							<xsl:attribute name="bulletin_frequency"><xsl:value-of select="td[7]"/></xsl:attribute>
							<xsl:attribute name="emp_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="emp_code"><xsl:value-of select="td[8]"/></xsl:attribute>
							<xsl:value-of select="format-number(td[3],'#,##0.00')"  />  
						</a>
					</td>
					<td align="center">																																																																																	
							<xsl:value-of select="format-number(td[4],'#,##0.00')" />  
					</td>															
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
