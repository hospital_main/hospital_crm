<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' >指标名称</th>
				<th nowrap='true' >年月</th>
				<th nowrap='true' >绩效分数</th>
				<th nowrap='true' >环比率</th>
				<th nowrap='true' >目标值</th>
				<th nowrap='true' >实际值</th>								
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="unit_name" select="td[1]" />
				<xsl:variable name="rowspan" select="td[9]" />
				<xsl:variable name="cur_pos" select="position()" />
				<tr>
					<xsl:if test="$cur_pos = 1 or $unit_name != ../tr[$cur_pos - 1]/td[1]">
						<td align="center" rowspan="{$rowspan}">
							<xsl:value-of select="td[1]" />
						</td>
					</xsl:if>
					<td align="center">
						<xsl:value-of select="td[2]"/>
					</td>
					<td align="right" >
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="M"  href="#" onclick="win_Open(this);" name="target_value">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[7]"/></xsl:attribute>
							<xsl:attribute name="appraise_method"><xsl:value-of select="td[8]"/></xsl:attribute>
							<xsl:attribute name="perf_month"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:value-of select="format-number(td[3],'#,##0.00')" />  
						</a>
					</td>						
					<td align="right"><B>
						<xsl:value-of select="td[4]"/>
						</B>
					</td>	
					<td align="right">
						<xsl:value-of select="format-number(td[5],'#,##0.00')"/>
					</td>																
					<td align="right">
						<xsl:value-of select="format-number(td[6],'#,##0.00')"/>
					</td>														
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
