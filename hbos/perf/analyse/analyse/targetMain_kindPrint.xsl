<?xml version="1.0" encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='' />
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">4</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>

				<tr noWrap='true' class='mainHead'>
					<td nowrap='true' >指标名称</td>
					<td nowrap='true' >科室名称</td>
					<td nowrap='true' >绩效分数</td>
					<td nowrap='true' >该指标的平均分</td>
				</tr>
			</thead>
				<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="unit_name" select="td[1]" />
				<xsl:variable name="rowspan" select="td[9]" />
				<xsl:variable name="cur_pos" select="position()" />
				<tr>
					<xsl:if test="$cur_pos = 1 or $unit_name != ../tr[$cur_pos - 1]/td[1]">
						<td align="center" rowspan="{$rowspan}">
							<xsl:value-of select="td[1]" />
						</td>
					</xsl:if>
					<td align="center">
						<xsl:value-of select="td[2]"/>
					</td>
					<td align="center" >
							<xsl:value-of select="format-number(td[3],'#,##0.00')"  />  
					</td>
					<td align="center">																																																																																	
							<xsl:value-of select="format-number(td[4],'#,##0.00')" />  
					</td>															
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>