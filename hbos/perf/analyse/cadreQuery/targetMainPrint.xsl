<?xml version="1.0" encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='' />
	<xsl:template match="/">
		<root>
			<thead>
			<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">6</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">6</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td nowrap='true' width="100">干部</td>
					<td nowrap='true' width="80">指标</td>
					<td nowrap='true' width="130">月考核分数</td>
					<td nowrap='true' width="130">季考核分数</td>
					<td nowrap='true' width="130">半年考核分数</td>
					<td nowrap='true' width="130">年考核分数</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:variable name="unit_name" select="td[1]" />
					<xsl:variable name="rowspan" select="td[9]" />
					<xsl:variable name="cur_pos" select="position()" />
					<tr>
					<xsl:if test="$cur_pos = 1 or $unit_name != ../tr[$cur_pos - 1]/td[1]">
						<td align="center" rowspan="{$rowspan}">
							<xsl:value-of select="td[1]" />
						</td>						
					</xsl:if>
					<xsl:if test="$unit_name = ../tr[$cur_pos - 1]/td[1]">
						<td align="center" >
						</td>						
					</xsl:if>
						<td align="center">
							<xsl:value-of select="td[2]"/>
						</td>
						<td align="center" >
							<a tabindex="-2" style="text-decoration:none" bulletin_frequency="M"  href="#" onclick="win_Open(this);">		
								<xsl:attribute name="sequence_no"><xsl:value-of select="td[8]"/></xsl:attribute>
								<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>																																																																																			
								<xsl:value-of select="td[3]" />  
							</a>
						</td>
						<td align="center">
							<a tabindex="-2" style="text-decoration:none" bulletin_frequency="Q"  href="#" onclick="win_Open(this);">		
								<xsl:attribute name="sequence_no"><xsl:value-of select="td[8]"/></xsl:attribute>
								<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>																																																																																			
								<xsl:value-of select="td[4]" />  
							</a>
						</td>
						<td align="center">
							<a tabindex="-2" style="text-decoration:none" bulletin_frequency="H"  href="#" onclick="win_Open(this);">		
								<xsl:attribute name="sequence_no"><xsl:value-of select="td[8]"/></xsl:attribute>	
								<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>																																																																																		
								<xsl:value-of select="td[5]" />  
							</a>
						</td>	
						<td align="center">
							<a tabindex="-2" style="text-decoration:none" bulletin_frequency="Y"  href="#" onclick="win_Open(this);">		
								<xsl:attribute name="sequence_no"><xsl:value-of select="td[8]"/></xsl:attribute>	
								<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>																																																																																		
								<xsl:value-of select="td[6]" />  
							</a>
						</td>															
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>