<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>科室分类</th>
				<th nowrap='true'>科室</th>
				<th nowrap='true'>绩效总分</th>
				<th nowrap='true'>财务维度比例</th>
				<th nowrap='true'>财务维度得分</th>
				<th nowrap='true'>非财务维度比例</th>
				<th nowrap='true'>非财务维度得分</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="unit_name" select="td[1]" />
				<xsl:variable name="rowspan" select="td[8]" />
				<xsl:variable name="cur_pos" select="position()" />
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<xsl:if test="$cur_pos = 1 or $unit_name != ../tr[$cur_pos - 1]/td[1]">
									<td align="left" rowspan="{$rowspan}">
										<xsl:value-of select="." />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=3">
								<td align="right">
									<a href="#" onclick="win_Open(this);">
										<xsl:attribute name="sequence_no"><xsl:value-of select="../td[8]"/></xsl:attribute>
										<xsl:attribute name="dept_code"><xsl:value-of select="../td[11]"/></xsl:attribute>
										<xsl:attribute name="dept_name"><xsl:value-of select="../td[2]"/></xsl:attribute>
										<xsl:attribute name="bulletin_frequency"><xsl:value-of select="../td[9]"/></xsl:attribute>
										<xsl:attribute name="appraise_method"><xsl:value-of select="../td[12]"/></xsl:attribute>
										<xsl:attribute name="score"><xsl:value-of select="."/></xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')" />
									</a>									
								</td>
							</xsl:when>
							<xsl:when test="position()=4 or position()=5 or position()=6 or position()=7">
            		<td align="right">
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
            	<xsl:when test="position()=8 or position()=9 or position()=10 or position()=11 or position()=12">
            		<td align="left" style="display:none">
                	<xsl:value-of select="."/>
                </td>
              </xsl:when>
             	<xsl:otherwise>
              	<td align="left">
                	<xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>