<?xml version='1.0' encoding="GB2312" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=last()">
	    			<tr noWrap='true' class='mainHead'>
			    		<xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=1 or position()=2 or position()=6 or   position()=10  or position()=11  or position()=12 or position()=13 or position()=14 or position()=15 or position()=16">
			          	
			          	</xsl:when>
			          	<xsl:when test="position()=5">
			          		<xsl:if test="/root/tbody/tr[1]/td[5]!='nonono'">
			          			<th nowrap='true'><xsl:value-of select="."/></th>
			          		</xsl:if>
			          		
			          	</xsl:when>
			          	<xsl:otherwise>
		                <th nowrap='true'><xsl:value-of select="."/></th>
		              </xsl:otherwise>
		            </xsl:choose>
		          </xsl:for-each>
	          </tr>
	        </xsl:when>
	      </xsl:choose>
	    </xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position()!=last()]">
				<xsl:variable name="unit_name" select="td[3]" />
				<xsl:variable name="kpi" select="td[4]" />
				<xsl:variable name="cur_pos" select="position()" />					
				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[3]=$unit_name])" />
				<xsl:variable name="rowspan2" select="count(/root/tbody/tr[td[4]=$kpi])" />
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
	    				<xsl:when test="position()=1 or position()=2 or position()=6 or position()=10 or position()=11  or position()=12 or position()=13 or position()=14 or position()=15 or position()=16">
			        </xsl:when>
							
							<xsl:when test="position()=3">
								<xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos - 1]/td[3]">
									<td rowspan="{$rowspan}">
										<xsl:value-of select="." />
									</td>
								</xsl:if>
								<xsl:if test="$unit_name = ../../tr[$cur_pos - 1]/td[3]">
									<td style="display:none"><xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:when>
								
							<xsl:when test="position()=4">
								
								<td align="left">
										<xsl:if test="$cur_pos = 1 or $kpi != ../../tr[$cur_pos - 1]/td[4]">
											<xsl:attribute name="rowspan">
												<xsl:value-of select="$rowspan2" />
											</xsl:attribute>
										</xsl:if>
										<xsl:if test="$kpi = ../../tr[$cur_pos - 1]/td[4]">
											<xsl:attribute name="style">
												display:none
											</xsl:attribute>
										</xsl:if>
										<xsl:if test="../td[5]='' or ../td[5]='nonono'">
			            		<a tabindex="-2" style="text-decoration:none" bulletin_frequency="M"  href="#" onclick="win_Open1(this);" name="target_value">		
			            		<xsl:attribute name="target_code"><xsl:value-of select="../td[2]"/></xsl:attribute>
													<xsl:value-of select="." disable-output-escaping="yes"/>
												</a>
										</xsl:if>
										<xsl:if test="../td[5]!='' and ../td[5]!='nonono'">
												<xsl:value-of select="." disable-output-escaping="yes"/>
										</xsl:if>
		            </td>
							</xsl:when>
							<xsl:when test="position()=5">
								<xsl:if test=".!='nonono'">
	          			<td align="left">
										<xsl:if test=".!='nonono'">
			            		<a tabindex="-2" style="text-decoration:none" bulletin_frequency="M"  href="#" onclick="win_Open1(this);" name="target_value">		
			            		<xsl:attribute name="target_code"><xsl:value-of select="../td[2]"/></xsl:attribute>
													<xsl:value-of select="." disable-output-escaping="yes"/>
												</a>
										</xsl:if>
										
		            
		            	</td>
	          		     </xsl:if>
								
							</xsl:when>				
							<xsl:when test="position()=9">
								<td align="left" style="width:100%;word-break;keep-all;">
									<xsl:choose>
										<xsl:when test="../td[11] = 'D' and ../td[13] = 1">
											<!-- <a tabindex="-2" style="text-decoration:none" bulletin_frequency="M"  href="#" onclick="win_Open3(this);" name="target_value">		
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[1]"/></xsl:attribute>
												<xsl:attribute name="appraise_method"><xsl:value-of select="../td[11]"/></xsl:attribute>
												<xsl:attribute name="extremum"><xsl:value-of select="../td[14]"/></xsl:attribute>
												<xsl:attribute name="target_name"><xsl:value-of select="../td[15]"/></xsl:attribute>
												<xsl:value-of select="." disable-output-escaping="yes"/> 
											</a> -->
											<xsl:value-of select="." disable-output-escaping="yes"/> 
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="." disable-output-escaping="yes"/>
										</xsl:otherwise>
									</xsl:choose>
		                      </td>
							</xsl:when>
							<xsl:when test="position() > 16 and ../td[16] = 1">
								<xsl:choose>
									<xsl:when test="../td[13]=1">
										<td align="right">
											<xsl:attribute name="emp_code"><xsl:value-of select="substring-before(.,'*')" /></xsl:attribute>
											<a tabindex="-2" style="text-decoration:none" bulletin_frequency="M"  href="#" onclick="win_Open2(this);" name="target_value">		
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[1]"/></xsl:attribute>
												<xsl:attribute name="appraise_method"><xsl:value-of select="../td[11]"/></xsl:attribute>																																																																																			
												<xsl:value-of select="format-number((substring-after(.,'*')),'#,##0.00')" />
											</a>									
				            </td>
									</xsl:when>
									<xsl:otherwise>
										<td></td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:when test="position()=6">
	          			<td align="right">
											<xsl:value-of select="format-number(., '#,##0.00')"/>
		            	</td>
							</xsl:when>				
							<xsl:otherwise>
								<td align="center">
		            	<xsl:value-of select="."/>
		            </td>
							</xsl:otherwise>
						</xsl:choose>            
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>