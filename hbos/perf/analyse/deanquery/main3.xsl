<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:variable name="num" select="count(//root/tbody/tr)" />
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<xsl:for-each select="//root/tbody/tr">
					<td align="center">
						<a href="#">
							<xsl:attribute name="onclick" >
								view(this,'../../check/calculate/detail.html');
							</xsl:attribute>
							<xsl:attribute name="code">
								<xsl:value-of select="td[1]"/>
							</xsl:attribute>
							<xsl:attribute name="name">
								<xsl:value-of select="td[2]"/>
							</xsl:attribute>
							<xsl:attribute name="values">
								<xsl:value-of select="td[4]"/>
							</xsl:attribute>
							<img border="0" src="../../../../images2/perf/deanquery/analyze/dept.png">
								<!--  
								<xsl:attribute name="src" >
									<xsl:value-of select="td[3]"/>
								</xsl:attribute>
								-->
							</img>
							<xsl:value-of select="td[2]" />
						</a>
					</td>
					<xsl:if test="position() mod 4 = 0">
						<xsl:text disable-output-escaping="yes">
							&lt;/tr&gt;&lt;tr&gt;
						</xsl:text>
					</xsl:if>
				</xsl:for-each>
			</tr>
		</table>		
	</xsl:template>
</xsl:stylesheet>