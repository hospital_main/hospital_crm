<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:variable name="num" select="count(//root/tbody/tr)" />
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<xsl:choose>
				<xsl:when test="$num = 1">
					<tr>
						<td align="center" valign="top">
							<img width="1" height="48" src="/images2/perf/deanquery/middle.gif"></img>
	     				</td>
					</tr>
			    	<tr>
			    		<td align="center">
			    			<a href="#">
			    				<xsl:attribute name="onclick" >
			    					view('main2.html?load=&lt;code&gt;<xsl:value-of select="td[1]"/>&lt;/code&gt;&lt;name&gt;<xsl:value-of select="td[2]"/>&lt;/name&gt;');
	     			  			</xsl:attribute>
	     			  			<img width="80" height="80" src="/images2/perf/deanquery/perf_kind_detail.jpg"></img>
			    				<br/>
	     			  			<xsl:value-of select="td[2]" />
		     			  	</a>
			    		</td>
			    	</tr>
		    	</xsl:when>
		    	<xsl:when test="$num > 1">
		    		<xsl:variable name="width" select="100 div $num" />
		    		
		    		<tr>
	   					<xsl:for-each select="/root/tbody/tr">
	     					<xsl:choose>
	     						<xsl:when test="position()=1">
	     							<td>
	     								<xsl:attribute name="width">
	     									<xsl:value-of select="$width div 2" />%
	     								</xsl:attribute>
	     							</td>
	     							<td align="left" valign="top" class="flowline">
	     								<xsl:attribute name="width">
	     									<xsl:value-of select="$width div 2" />%
	     								</xsl:attribute>
	     								<img width="2" height="48" src="/images2/perf/deanquery/left.gif"></img>
	     							</td>
	     						</xsl:when>
	     						<xsl:when test="position()=$num">
	     							<td align="right" valign="top" class="flowline">
	     								<xsl:attribute name="width">
	     									<xsl:value-of select="$width div 2" />%
	     								</xsl:attribute>
	    								<img width="2" height="48" src="/images2/perf/deanquery/right.gif"></img>
	    							</td>
	    							<td>
	     								<xsl:attribute name="width">
	     									<xsl:value-of select="$width div 2" />%
	     								</xsl:attribute>
	     							</td>
	    						</xsl:when>
	     						
	     						<xsl:otherwise>
	     							<td align="center" valign="top" class="flowline">
	     								<xsl:attribute name="width">
	     									<xsl:value-of select="$width" />%
	     								</xsl:attribute>
	     								<img width="3" height="48" src="/images2/perf/deanquery/middle.gif"></img>
	     							</td>
	     						</xsl:otherwise>
	     					</xsl:choose>
	   					</xsl:for-each>
	   				</tr>
	   				<tr>
	   					<xsl:for-each select="/root/tbody/tr">
	   						<td align="center">
	   							<xsl:if test="position()=1 or position()=$num"> 
	   								<xsl:attribute name="colspan" >2</xsl:attribute>
	   							</xsl:if>
				    			<a href="#">
				    				<xsl:attribute name="onclick" >
				    					view('main2.html?load=&lt;code&gt;<xsl:value-of select="td[1]"/>&lt;/code&gt;&lt;name&gt;<xsl:value-of select="td[2]"/>&lt;/name&gt;');
		     			  			</xsl:attribute>
		     			  			<img border="0" width="113" height="115" src="/images2/perf/deanquery/perf_kind_detail.png"></img>
		     			  			<br/>
		     			  			<xsl:value-of select="td[2]" />
		     			  		</a>
	    					</td>
	   					</xsl:for-each>
	   				</tr>
   				</xsl:when>
		    	
		    	
			</xsl:choose>
			
		    
		</table>
		
		
		
	</xsl:template>
	
</xsl:stylesheet>