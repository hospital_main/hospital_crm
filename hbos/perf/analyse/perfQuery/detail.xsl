<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			  <th noWrap='true'>维度</th>
			  <th noWrap='true'>分数</th>
			  <th noWrap='true'>二级指标</th>			  			  
			  
			  <xsl:if test="/root/tbody/tr[1]/td[7]!='nonono'">
			  	<th noWrap='true'>分数</th>
			  	<th noWrap='true'>三级指标</th>			
			  </xsl:if>  			  
			  
			  <th noWrap='true'>计算单位</th>
			  <th noWrap='true'>数据采集部门</th>
			  <th noWrap='true'>评分标准</th>			  
			  <th noWrap='true'>指标分数</th>
			  <th noWrap='true'>指标状态</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="td1" select="./td[1]" />
				<xsl:variable name="td4" select="./td[4]" />
 				<xsl:variable name="rowspan_1" select="count(/root/tbody/tr[td[1]=$td1])" />
 				<xsl:variable name="rowspan_3" select="count(/root/tbody/tr[td[4]=$td4])" />
 				<xsl:variable name="mpos" select="position()-1" />
	      <xsl:if test="position()=1">
  		    <tr>
  		      <td rowspan="{$rowspan_1}">
  		        <xsl:value-of select="./td[1]" />
  		      </td>
  		      <td rowspan="{$rowspan_1}" align='right'>
  		        <xsl:value-of select="format-number(./td[3],'#,#00.00')" />
  		      </td>
            <td rowspan="{$rowspan_3}">
            	<xsl:if test="td[7]='nonono' or td[7]=''">
								<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
									<xsl:attribute name="sequence_no">
										<xsl:value-of select="td[13]" />
									</xsl:attribute>
									<xsl:attribute name="appraise_method">
										<xsl:value-of select="td[15]" />
									</xsl:attribute>
									<xsl:attribute name="score">
										<xsl:value-of select="td[6]" />
									</xsl:attribute>
									<xsl:value-of select="td[4]" />
								</a>
							</xsl:if>
							<xsl:if test="td[7]!='nonono' and td[7]!=''">
									<xsl:value-of select="td[4]" />
							</xsl:if>
            </td>
            <td rowspan="{$rowspan_3}" align='right'>
            	<xsl:if test="/root/tbody/tr[1]/td[7]='nonono'">
								  <xsl:attribute name="style">
  		      				display:none
									</xsl:attribute>
							</xsl:if>
  		        <xsl:value-of select="format-number(./td[6],'#,#00.00')" />
  		      </td>
  		       
  		      <td>
  		      	<xsl:if test="/root/tbody/tr[1]/td[7]='nonono'">
								  <xsl:attribute name="style">
  		      				display:none
									</xsl:attribute>
							</xsl:if>
							<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
								<xsl:attribute name="sequence_no">
									<xsl:value-of select="td[13]" />
								</xsl:attribute>
								<xsl:attribute name="appraise_method">
									<xsl:value-of select="td[15]" />
								</xsl:attribute>
								<xsl:attribute name="score">
									<xsl:value-of select="td[9]" />
								</xsl:attribute>
								<xsl:value-of select="td[7]" />
							</a>
            </td>
            
            <td><xsl:value-of select="td[19]" /></td>
            <td><xsl:value-of select="td[20]" /></td>
            <td><xsl:value-of select="td[22]" disable-output-escaping="yes"/></td>
            <td align='right'>
              <xsl:value-of select="format-number(td[11],'#,#00.00')"/>              
            </td>
            <td align="center">
							<img>
								<xsl:attribute name="SRC">
									<xsl:value-of select="td[14]" />
								</xsl:attribute>
							</img>
            </td>
          </tr>
        </xsl:if>
        <xsl:if test="position()>1">
	      <xsl:if test="$td1=../tr[$mpos]/td[1]">
  		    <tr>
  		      <xsl:if test="$td4!=(../tr[$mpos]/td[4])">
		            <td rowspan="{$rowspan_3}">
		            	<xsl:if test="td[7]='nonono' or td[7]=''">
									<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
										<xsl:attribute name="sequence_no">
											<xsl:value-of select="td[13]" />
										</xsl:attribute>
										<xsl:attribute name="appraise_method">
											<xsl:value-of select="td[15]" />
										</xsl:attribute>
										<xsl:attribute name="score">
											<xsl:value-of select="td[6]" />
										</xsl:attribute>
										<xsl:value-of select="td[4]" />
									</a>
									</xsl:if>
									<xsl:if test="td[7]!='nonono' and td[7]!=''">
										<xsl:value-of select="td[4]" />
									</xsl:if>
		            </td>
		            <td rowspan="{$rowspan_3}" align='right'>
		            	<xsl:if test="/root/tbody/tr[1]/td[7]='nonono'">
										  <xsl:attribute name="style">
		  		      				display:none
											</xsl:attribute>
									</xsl:if>
		  		        <xsl:value-of select="format-number(./td[6],'#,#00.00')" />
		  		      </td>
		  		  </xsl:if>
		  		  <xsl:if test="$td4=(../tr[$mpos]/td[4])">
		  		  </xsl:if>
		  		    <td>
		  		    <xsl:if test="/root/tbody/tr[1]/td[7]='nonono'">
								  <xsl:attribute name="style">
  		      				display:none
									</xsl:attribute>
							</xsl:if>
							<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
								<xsl:attribute name="sequence_no">
									<xsl:value-of select="td[13]" />
								</xsl:attribute>
								<xsl:attribute name="appraise_method">
									<xsl:value-of select="td[15]" />
								</xsl:attribute>
								<xsl:attribute name="score">
									<xsl:value-of select="td[9]" />
								</xsl:attribute>
								<xsl:value-of select="td[7]" />
							</a>
            </td>
            
            <td><xsl:value-of select="td[19]" /></td>
            <td><xsl:value-of select="td[20]" /></td>
            <td><xsl:value-of select="td[22]" disable-output-escaping="yes"/></td>
            <td align='right'>
              <xsl:value-of select="format-number(td[11],'#,#00.00')"/>              
            </td>
            <td align="center">
							<img>
								<xsl:attribute name="SRC">
									<xsl:value-of select="td[14]" />
								</xsl:attribute>
							</img>
            </td>
          </tr>
        </xsl:if>
        <xsl:if test="$td1 != ../tr[$mpos]/td[1]">
        	<tr>
  		      <td rowspan="{$rowspan_1}">
  		        <xsl:value-of select="./td[1]" />
  		      </td>
  		      <td rowspan="{$rowspan_1}" align='right'>
  		        <xsl:value-of select="format-number(./td[3],'#,#00.00')" />
  		      </td>
            <xsl:if test="$td4!=(../tr[$mpos]/td[4])">
		            <td rowspan="{$rowspan_3}">
		            	<xsl:if test="td[7]='nonono' or td[7]=''">
									<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
										<xsl:attribute name="sequence_no">
											<xsl:value-of select="td[13]" />
										</xsl:attribute>
										<xsl:attribute name="appraise_method">
											<xsl:value-of select="td[15]" />
										</xsl:attribute>
										<xsl:attribute name="score">
											<xsl:value-of select="td[6]" />
										</xsl:attribute>
										<xsl:value-of select="td[4]" />
									</a>
									</xsl:if>
									<xsl:if test="td[7]!='nonono' and td[7]!=''">
										<xsl:value-of select="td[4]" />
									</xsl:if>
		            </td>
		            <td rowspan="{$rowspan_3}" align='right'>
		            	<xsl:if test="/root/tbody/tr[1]/td[7]='nonono'">
								  <xsl:attribute name="style">
		  		      				display:none
											</xsl:attribute>
									</xsl:if>
		  		        <xsl:value-of select="format-number(./td[6],'#,#00.00')" />
		  		      </td>
		  		  </xsl:if>
		  		  <xsl:if test="$td4=(../tr[$mpos]/td[4])">
		  		  </xsl:if>
		  		    <td>
		  		    <xsl:if test="/root/tbody/tr[1]/td[7]='nonono'">
								  <xsl:attribute name="style">
  		      				display:none
									</xsl:attribute>
							</xsl:if>
							<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
								<xsl:attribute name="sequence_no">
									<xsl:value-of select="td[13]" />
								</xsl:attribute>
								<xsl:attribute name="appraise_method">
									<xsl:value-of select="td[15]" />
								</xsl:attribute>
								<xsl:attribute name="score">
									<xsl:value-of select="td[9]" />
								</xsl:attribute>
								<xsl:value-of select="td[7]" />
							</a>
            </td>
            
            <td><xsl:value-of select="td[19]" /></td>
            <td><xsl:value-of select="td[20]" /></td>
            <td><xsl:value-of select="td[22]" disable-output-escaping="yes"/></td>
            <td align='right'>
              <xsl:value-of select="format-number(td[11],'#,#00.00')"/>              
            </td>
            <td align="center">
							<img>
								<xsl:attribute name="SRC">
									<xsl:value-of select="td[14]" />
								</xsl:attribute>
							</img>
            </td>
          </tr>
        </xsl:if>
        </xsl:if>
			</xsl:for-each>

		</tbody>
	</xsl:template>
</xsl:stylesheet>