<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' width="100">科室分类</th>
				<th nowrap='true' width="80">科室</th>
				<th nowrap='true' width="130">月考核分数</th>
				<th nowrap='true' width="130">季考核分数</th>
				<th nowrap='true' width="130">半年考核分数</th>
				<th nowrap='true' width="130">年考核分数</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="unit_name" select="td[1]" />
				<xsl:variable name="rowspan" select="td[12]" />
				<xsl:variable name="cur_pos" select="position()" />
				<tr>
					<xsl:if test="$cur_pos = 1 or $unit_name != ../tr[$cur_pos - 1]/td[1]">
						<td align="center" rowspan="{$rowspan}">
							<xsl:value-of select="td[1]" />
						</td>
					</xsl:if>
					<td align="center">
						<xsl:value-of select="td[2]"/>
					</td>
					 <xsl:if test="td[3]!='---'">
					<td align="center" >
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="M"  href="#" onclick="win_Open(this);">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[8]"/></xsl:attribute>
							<xsl:attribute name="dept_code"><xsl:value-of select="td[10]"/></xsl:attribute>
							<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="bulletin_frequency"><xsl:value-of select="td[9]"/></xsl:attribute>
							<xsl:attribute name="score"><xsl:value-of select="td[3]"/></xsl:attribute>
							<xsl:attribute name="appraise_method"><xsl:value-of select="td[11]"/></xsl:attribute>
							<xsl:value-of select="format-number(td[3],'#,##0.00')" />  
						</a>
					</td>
					</xsl:if>
					<xsl:if test="td[3]='---'">
					<td align="center" >
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="M"  href="#" onclick="win_Open(this);">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[8]"/></xsl:attribute>
							<xsl:attribute name="dept_code"><xsl:value-of select="td[10]"/></xsl:attribute>
							<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="bulletin_frequency"><xsl:value-of select="td[9]"/></xsl:attribute>
							<xsl:attribute name="score"><xsl:value-of select="td[3]"/></xsl:attribute>
							<xsl:attribute name="appraise_method"><xsl:value-of select="td[11]"/></xsl:attribute>
							<xsl:value-of select="td[3]" />  
						</a>
					</td>
					</xsl:if>
					 <xsl:if test="td[4]='---'">
					<td align="center">
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="Q"  href="#" onclick="win_Open(this);">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[8]"/></xsl:attribute>
							<xsl:attribute name="dept_code"><xsl:value-of select="td[10]"/></xsl:attribute>
							<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="bulletin_frequency">Q</xsl:attribute>
							<xsl:attribute name="score"><xsl:value-of select="td[4]"/></xsl:attribute>
							<xsl:attribute name="appraise_method"><xsl:value-of select="td[11]"/></xsl:attribute>
							<xsl:value-of select="td[4]" />  
						</a>
					</td>
					</xsl:if>
					 <xsl:if test="td[4]!='---'">
					<td align="center">
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="Q"  href="#" onclick="win_Open(this);">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[8]"/></xsl:attribute>
							<xsl:attribute name="dept_code"><xsl:value-of select="td[10]"/></xsl:attribute>
							<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="bulletin_frequency">Q</xsl:attribute>
							<xsl:attribute name="score"><xsl:value-of select="td[4]"/></xsl:attribute>
							<xsl:attribute name="appraise_method"><xsl:value-of select="td[11]"/></xsl:attribute>
							<xsl:value-of select="format-number(td[4],'#,##0.00')"/>  
						</a>
					</td>
					</xsl:if>
					 <xsl:if test="td[5]!='---'">
					<td align="center">
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="H"  href="#" onclick="win_Open(this);">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[8]"/></xsl:attribute>
							<xsl:attribute name="dept_code"><xsl:value-of select="td[10]"/></xsl:attribute>
							<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="bulletin_frequency">H</xsl:attribute>
							<xsl:attribute name="score"><xsl:value-of select="td[5]"/></xsl:attribute>
							<xsl:attribute name="appraise_method"><xsl:value-of select="td[11]"/></xsl:attribute>
							<xsl:value-of select="format-number(td[5],'#,##0.00')" />  
						</a>
					</td>	
						</xsl:if>
							 <xsl:if test="td[5]='---'">
					<td align="center">
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="H"  href="#" onclick="win_Open(this);">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[8]"/></xsl:attribute>
							<xsl:attribute name="dept_code"><xsl:value-of select="td[10]"/></xsl:attribute>
							<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="bulletin_frequency">H</xsl:attribute>
							<xsl:attribute name="score"><xsl:value-of select="td[5]"/></xsl:attribute>
							<xsl:attribute name="appraise_method"><xsl:value-of select="td[11]"/></xsl:attribute>
							<xsl:value-of select="td[5]" />  
						</a>
					</td>	
						</xsl:if>
						 <xsl:if test="td[6]='---'">
					<td align="center">
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="Y"  href="#" onclick="win_Open(this);">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[8]"/></xsl:attribute>
							<xsl:attribute name="dept_code"><xsl:value-of select="td[10]"/></xsl:attribute>
							<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="bulletin_frequency">Y</xsl:attribute>
							<xsl:attribute name="score"><xsl:value-of select="td[6]"/></xsl:attribute>
							<xsl:attribute name="appraise_method"><xsl:value-of select="td[11]"/></xsl:attribute>
							<xsl:value-of select="td[6]"/>  
						</a>
					</td>		
					</xsl:if>			
								 <xsl:if test="td[6]!='---'">
					<td align="center">
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="Y"  href="#" onclick="win_Open(this);">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[8]"/></xsl:attribute>
							<xsl:attribute name="dept_code"><xsl:value-of select="td[10]"/></xsl:attribute>
							<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="bulletin_frequency">Y</xsl:attribute>
							<xsl:attribute name="score"><xsl:value-of select="td[6]"/></xsl:attribute>
							<xsl:attribute name="appraise_method"><xsl:value-of select="td[11]"/></xsl:attribute>
							<xsl:value-of select="format-number(td[6],'#,##0.00')" />  
						</a>
					</td>		
					</xsl:if>											
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>