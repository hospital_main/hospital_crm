<?xml version='1.0' encoding="GB2312" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=last()">
	    			<tr noWrap='true' class='mainHead'>
			    		<xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=1 or position()=3 ">
			          	
			          	</xsl:when>
			          	<xsl:otherwise>
			          			<th nowrap='true'><xsl:value-of select="."/></th>
		              		</xsl:otherwise>
		 		</xsl:choose>
		          </xsl:for-each>
	         		 </tr>
	        	</xsl:when>
	      </xsl:choose>
	    		</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position()!=last()]">
				<xsl:variable name="unit_name" select="td[2]" />
				<xsl:variable name="cur_pos" select="position()" />					
				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[2]=$unit_name])" />
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
	    				<xsl:when test="position()=1 or position()=3 ">
			        	</xsl:when>
			        	<xsl:when test="position()=2">
								<xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos - 1]/td[2]">
									<td rowspan="{$rowspan}" align="left">
										<xsl:value-of select="." />
									</td>
								</xsl:if>
								<xsl:if test="$unit_name = ../tr[$cur_pos - 1]/td[2]">
									<td style="display:none2"><xsl:value-of select="."/>
									</td>
								</xsl:if>			
					</xsl:when>
					<xsl:when test="position()>4 and position()!=last()">
									<xsl:variable name="sum_value" select="td[position()>4 and position()!=last()]"/>
									<xsl:variable name="now_col" select="position()"/>
									<td align="right">
										<xsl:if test=".='---'">
											<xsl:value-of select="."/>
											</xsl:if>
										<a tabindex="-2" style="text-decoration:none" bulletin_frequency="Q" href="#" onclick="win_Open(this);">
											<xsl:attribute name="perf_unit_code"><xsl:value-of select="../td[1]"/></xsl:attribute>
											<xsl:attribute name="dept_code"><xsl:value-of select="../td[3]"/></xsl:attribute>
											<xsl:attribute name="dept_name"><xsl:value-of select="../td[4]"/></xsl:attribute>
											<xsl:attribute name="now_rap"><xsl:value-of select="/root/tbody/tr[last()]/td[$now_col]"/></xsl:attribute>
											
											<xsl:if test=".!='---'">
											<xsl:value-of select="format-number(.,'###,##0.00')"/>
											</xsl:if>
										</a>
									</td>
								</xsl:when>
					<xsl:when test=" position()=last()">
						<td align="right">
						      	<xsl:value-of select="format-number(.,'###,##0.00')"/>
						</td>
					</xsl:when>
					<xsl:otherwise>
								
						<td align="left">
						      	<xsl:value-of select="."/>
						</td>	
					</xsl:otherwise>
						</xsl:choose>            
					</xsl:for-each>
				</tr>
				
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>