<?xml version="1.0" encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='' />
	<xsl:template match="/">
		<root>
			<thead>
			<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">7</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">7</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>科室分类</td>
				<td nowrap='true'>科室</td>
				<td nowrap='true'>绩效总分</td>
				<td nowrap='true'>财务维度比例</td>
				<td nowrap='true'>财务维度得分</td>
				<td nowrap='true'>非财务维度比例</td>
				<td nowrap='true'>非财务维度得分</td>
			</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:variable name="cur_pos" select="position()" />
						<xsl:variable name="target0" select="td[1]" />
						<xsl:variable name="target1" select="td[2]" />
						<xsl:variable name="rowspan0" select="count(../tr[td[1]=$target0])" />
						<xsl:variable name="rowspan1" select="count(../tr[td[2]=$target1])" />
						<xsl:if test="$cur_pos = 1 or td[1] != ../tr[$cur_pos - 1]/td[1]">
							<td align="center" valign="middle" rowspan="{$rowspan0}">
								<xsl:value-of select="td[1]" />
							</td>
						</xsl:if>
						<xsl:if test="$cur_pos >1 and td[1] = ../tr[$cur_pos - 1]/td[1]">
							<td align="left" style="display:none" >
							</td>
						</xsl:if>
						<xsl:if test="$cur_pos = 1 or td[2] != ../tr[$cur_pos - 1]/td[2]">
							<td align="center" valign="middle" rowspan="{$rowspan1}">
								<B><xsl:value-of select="td[2]" /></B>
							</td>
						</xsl:if>
						<xsl:if test="$cur_pos >1 and td[2]= ../tr[$cur_pos - 1]/td[2]">
							<td align="left" style="display:none" >
							</td>
						</xsl:if>
						 
						<td class="numberText">
							<xsl:value-of select="td[3]" />
						</td>				
						<td>
							<xsl:value-of select="td[4]" />
						</td>		
												<td>
							<xsl:value-of select="td[5]" />
						</td>		
												<td>
							<xsl:value-of select="td[6]" />
						</td>	
																		<td>
							<xsl:value-of select="td[7]" />
						</td>		
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>