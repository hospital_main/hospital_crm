<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="colCount" select="count(/root/tbody/tr[1]/td)" />
			<xsl:if test="$colCount = 4">
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="/root/tbody/tr">
						<th nowrap='true'>
							<xsl:value-of select="td[2]" />
						</th>
					</xsl:for-each>
				</tr>
			</xsl:if>
			<xsl:if test="$colCount = 5">
				<tr noWrap='true' class='mainHead'>
					<th nowrap='true' width="100">基本指标</th>
					<th nowrap='true' width="80">目标值</th>
					<th nowrap='true' width="130">计算公式</th>
					<th nowrap='true' width="130">权重</th>
					<th nowrap='true' width="130">指标值</th>
				</tr>
			</xsl:if>
		</thead>
		<tbody>
			<xsl:if test="$colCount = 4">
				<tr>
					<xsl:for-each select="/root/tbody/tr">
						<td>
							<xsl:if test="td[4] = 'func'">
								<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
									<xsl:value-of select="td[3]" />
								</a>
							</xsl:if>
							<xsl:if test="td[4] != 'func'">
								<xsl:value-of select="td[3]" />
							</xsl:if>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:if>
			<xsl:if test="$colCount = 5">

					<xsl:for-each select="/root/tbody/tr">
						<xsl:if test="position()=last()">
						<tr>
						<td  align="center">合计</td>
						<td> </td>
						<td> </td>
						<td align="center">100%</td>
						<td id="real_value"  align="center"></td>																		
						</tr>
						</xsl:if>
						<xsl:if test="position()!=last()">						
				<tr>					
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
									<td noWrap='true' align='center'>
										<xsl:value-of select="." />
									</td>
								</xsl:when>
								<xsl:when test="position()=4">
									<td noWrap='true' align='center'>
										<xsl:value-of select="format-number(.,'#,##0.00%')"/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td noWrap='true' align='center'>
										<xsl:value-of select="." />
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
				</tr>			
						</xsl:if>							
					</xsl:for-each>

			</xsl:if>
		</tbody>
	</xsl:template>
</xsl:stylesheet>