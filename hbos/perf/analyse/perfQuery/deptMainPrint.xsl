<?xml version="1.0" encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='' />
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>
				<tr noWrap='true' class='maintitle'>
					<td nowrap='true' width="100">科室分类</td>
					<td nowrap='true' width="180">科室</td>
					<td nowrap='true' width="130">月考核分数</td>
					<td nowrap='true' width="130">季考核分数</td>
					<td nowrap='true' width="130">半年考核分数</td>
					<td nowrap='true' width="130">年考核分数</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:variable name="cur_pos" select="position()" />
					<xsl:variable name="unit_name" select="td[1]" />
					<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$unit_name])" />
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos - 1]/td[1]">
										<td rowspan="{$rowspan}" >
											<xsl:value-of select="." />
										</td>
									</xsl:if>
									<xsl:if test="$unit_name = ../../tr[$cur_pos - 1]/td[1]">
										<td style="display:none"></td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=3 or position()=4 or position()=5 or position()=6">
											<td><xsl:value-of select="format-number(.,'#,##0.00')" /></td>
								</xsl:when>
								<xsl:otherwise>
									<td align="center">
			            	<xsl:value-of select="."/>
			            </td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>