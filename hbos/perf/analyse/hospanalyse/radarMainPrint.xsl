<?xml version="1.0" encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='' />
	<xsl:template match="/">
		<root>
			<thead>
				<xsl:choose>
	          <xsl:when test="/root/tbody/tr[1]/td[3]!='nonono'">
	            <tr noWrap='true'>
	        			<td style='fontsize:maintitle;colspan:colcount'></td>
	  						<td style="display:none"></td>
	  						<td style="display:none"></td>
	  						<td style="display:none"></td>
	  						<td style="display:none"></td>
	  					</tr>
	          </xsl:when> 
	          <xsl:otherwise>
	            <tr noWrap='true'>
	        			<td style='fontsize:maintitle;colspan:colcount'></td>
	  						<td style="display:none"></td>
	  						<td style="display:none"></td>
	  						<td style="display:none"></td>
	  					</tr>
	          </xsl:otherwise>
	       </xsl:choose>
				
				<tr noWrap='true' class='mainHead'>
				  <td nowrap='true' >维度</td>
					<td nowrap='true' >二级指标名称</td>
					<xsl:if test="/root/tbody/tr[1]/td[3]!='nonono'">
						<td nowrap='true' >三级指标名称</td>
					</xsl:if>
					<td nowrap='true' >目标值分数</td>
					<td nowrap='true' >指标值分数</td>							
				</tr>
			</thead>
			<tbody>
					<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:variable name="cur_pos" select="position()" />
						<xsl:variable name="target0" select="td[1]" />
						<xsl:variable name="target1" select="td[2]" />
						<xsl:variable name="rowspan0" select="count(../tr[td[1]=$target0])" />
						<xsl:variable name="rowspan1" select="count(../tr[td[2]=$target1])" />
						<xsl:if test="$cur_pos = 1 or td[1] != ../tr[$cur_pos - 1]/td[1]">
							<td align="center" valign="middle" rowspan="{$rowspan0}">
								<xsl:value-of select="td[1]" />
							</td>
						</xsl:if>
						<xsl:if test="$cur_pos >1 and td[1] = ../tr[$cur_pos - 1]/td[1]">
							<td align="left" style="display:none" >
							</td>
						</xsl:if>
						<xsl:if test="$cur_pos = 1 or td[2] != ../tr[$cur_pos - 1]/td[2]">
							<td align="center" valign="middle" rowspan="{$rowspan1}">
								<B><xsl:value-of select="td[2]" /></B>
							</td>
						</xsl:if>
						<xsl:if test="$cur_pos >1 and td[2]= ../tr[$cur_pos - 1]/td[2]">
							<td align="left" style="display:none" >
							</td>
						</xsl:if>
						<xsl:if test="/root/tbody/tr[1]/td[3]!='nonono'">
						<td align="left">
							<xsl:value-of select="td[3]" />
						</td>			
						</xsl:if>
						
						<td class="numberText">
							<xsl:value-of select="td[5]" />
						</td>				
						<td class="numberText">
							<xsl:value-of select="td[6]" />
						</td>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>