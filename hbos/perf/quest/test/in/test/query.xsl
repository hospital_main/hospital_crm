<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>录入序号</th>
  			<th nowrap='true'>科室名称</th>
  			<th nowrap='true'>考核年度</th>
  			<th nowrap='true'>考核期间</th>
  			<th nowrap='true'>问卷名称</th>
  			<th nowrap='true'>测评科室</th>
  			<th nowrap='true'>测评人</th>
  			<th nowrap='true'>测评时间</th>
  			<th nowrap='true'>操作员</th>
  			<th nowrap='true'>操作时间</th>
  			<th nowrap='true'>测评情况</th>
  		</tr>
  	</thead>
  	
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
                <xsl:when test="position()=11"><a tabindex='-1'>
                  <xsl:attribute name="href">
    	            javascript:openDialog('detail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:880px;dialogHeight:660px', result)
  	          </xsl:attribute><xsl:value-of select="."/></a>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>