<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th style='display:none' width='25'><input type='checkbox'/></th>
		  	<th nowrap='true'>题目代码</th>
		  	<th nowrap='true'>题目名称</th>
		  	<th nowrap='true'>题目类别</th>
		  	<th nowrap='true'>答案名称</th>
		  	<th nowrap='true'>题目描述</th>
      </tr>
    </thead>
    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  noWrap='true'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position() = 5 or position() = 6 or position() = 8 or position() = 9 or position() = 10">
			            <td noWrap='true' style="display:none"><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:when test="position() = 4">
			            <td noWrap='true'>
	                  <a tabindex='-1' href='#'>
		                <xsl:attribute name="onclick" >
		                  viewResult('../../../questset/questresult/view.html', '<xsl:value-of select="../td[9]"/>');
		                </xsl:attribute><xsl:value-of select="."/></a>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
     </tbody>
  </xsl:template>
</xsl:stylesheet>