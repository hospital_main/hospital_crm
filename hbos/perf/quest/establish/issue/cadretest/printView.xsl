<?xml version="1.0" encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='' />
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style='width:300mm' />
				<col style='width:300mm' />
				<col style='width:300mm' />
			</colgroup>
			<thead>
				<tr>
					<td colspan="3">
					</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr>
					<td colspan="3">
					</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr>
					<td colspan="3">
					</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr>
					<td colspan="3">
					</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td nowrap='true' >测评项目</td>
					<td nowrap='true' >测评选项</td>
					<td nowrap='true' >测评结果</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td>
										<xsl:value-of select="." />
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
				<tr>
					<td colspan='3'>主要工作改善建议：</td>
				</tr>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>