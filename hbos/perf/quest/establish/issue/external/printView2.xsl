<?xml version="1.0" encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='' />
	<xsl:template match="/">
		<xsl:variable name="colCount" select="count(/root/tbody/tr[1]/td)" />
		<root>
			<colgroup>
				<col style='width:300mm' />
				<xsl:for-each select="/root/tbody/tr[1]/td">
				
					<xsl:if test="position() != last()">
						<col style='width:100mm' />
					</xsl:if>
				</xsl:for-each>
				<col style='width:300mm' />
			</colgroup>
			<thead>
			<tr>
				<td>
					<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="count(/root/tbody/tr[1]/td)+1"/>
					</xsl:attribute>
				</td>
				<xsl:for-each select="/root/tbody/tr[1]/td">
						<xsl:if test="position() != last()">
							<td  style='display:none' ></td>
						</xsl:if>
				</xsl:for-each>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td>
					<xsl:attribute name="style">fontsize:coltitle;colspan:<xsl:value-of select="count(/root/tbody/tr[1]/td)+1"/>
					</xsl:attribute>
				</td>
				<xsl:for-each select="/root/tbody/tr[1]/td">
						<xsl:if test="position() != last()">
							<td  style='display:none' ></td>
						</xsl:if>
				</xsl:for-each>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td>
					<xsl:attribute name="style">fontsize:coltitle;colspan:<xsl:value-of select="count(/root/tbody/tr[1]/td)+1"/>
					</xsl:attribute>
				</td>
				<xsl:for-each select="/root/tbody/tr[1]/td">
						<xsl:if test="position() != last()">
							<td  style='display:none' ></td>
						</xsl:if>
				</xsl:for-each>
				<td style="display:none"></td>
			</tr>
			
				<tr noWrap='true' class='mainHead'>
					<td nowrap='true' >评价要素</td>
					<xsl:for-each select="/root/tbody/tr[1]/td">
						<xsl:if test="position() != last()">
							<td nowrap='true' ><xsl:value-of select="." /></td>
						</xsl:if>
					</xsl:for-each>
					<td nowrap='true' >评语</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<xsl:value-of select="/root/tbody/tr[2]/td[2]" />
						
					</td>
					<xsl:for-each select="/root/tbody/tr[2]/td">
						<xsl:if test="position() != last()">
							<td></td>
						</xsl:if>
					</xsl:for-each>
					<td></td>
				</tr>
				<tr>
					<td>
						<xsl:value-of select="/root/tbody/tr[2]/td[3]" />
					</td>
					<xsl:for-each select="/root/tbody/tr[2]/td">
						<xsl:if test="position() != last()">
							<td></td>
						</xsl:if>
					</xsl:for-each>
					<td></td>
				</tr>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:choose>
						<xsl:when test="position()>2"> 
						    <xsl:variable name="prevPos" select="position()-1" />
							<xsl:variable name="curPos" select="position()" />
							<xsl:variable name="prevCode" select="/root/tbody/tr[$prevPos]/td[2]" />
							<xsl:variable name="curCode" select="/root/tbody/tr[$curPos]/td[2]" />
							<xsl:choose>
								<xsl:when test="not($curCode=$prevCode)">
									<tr>
										<td>
											<xsl:value-of select="/root/tbody/tr[$curPos]/td[2]" />
										</td>
										<xsl:for-each select="/root/tbody/tr[$curPos]/td">
											<xsl:if test="position() != last()">
												<td></td>
											</xsl:if>
										</xsl:for-each>
										<td></td>
									</tr>
									<tr>
										<td>
											<xsl:value-of select="/root/tbody/tr[$curPos]/td[3]" />
										</td>
										<xsl:for-each select="/root/tbody/tr[$curPos]/td">
											<xsl:if test="position() != last()">
												<td></td>
											</xsl:if>
										</xsl:for-each>
										<td></td>
									</tr>
								</xsl:when>
								<xsl:otherwise>
									<tr>
										<td>
											<xsl:value-of select="/root/tbody/tr[$curPos]/td[3]" />
										</td>
										<xsl:for-each select="/root/tbody/tr[$curPos]/td">
											<xsl:if test="position() != last()">
												<td></td>
											</xsl:if>
										</xsl:for-each>
										<td></td>
									</tr>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
					<tr>
					<td>
						<xsl:attribute name="colspan" >
							<xsl:value-of select="$colCount + 1 "/>
						</xsl:attribute>
					主要工作改善建议：</td>
				</tr>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>