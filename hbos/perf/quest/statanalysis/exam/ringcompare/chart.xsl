<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
		    
			<item name='����'>
				<xsl:for-each select="/root/tbody/tr">
				    <xsl:variable name="period">
				      <xsl:value-of select="td[1]" />
				      <xsl:value-of select="td[2]" />
				    </xsl:variable>
				    <xsl:variable name="ringcompare" select="substring-before(td[4], '%')" />
					<item name='{$period}' value='{$ringcompare}'/>
				</xsl:for-each>
			</item>
		</root>
	</xsl:template>
</xsl:stylesheet>


