<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>干部名称</th>
				<th>考核年度</th>
				<th>考核周期</th>
				<th>问卷名称</th>
				<th>总得分</th>
				<th>转换比例</th>
				<th>最终得分</th>
				<th>发放张数</th>
				<th>有效张数</th>
				<th>总人数</th>
				<th>备注</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			    <xsl:variable name="para4"><xsl:for-each select="pk/*"><xsl:if test="position()=1 or position()=2 or position()=3">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:if></xsl:for-each></xsl:variable>
				<xsl:variable name="para5">
					<xsl:for-each select="pk/*">
						&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;
					</xsl:for-each>
				</xsl:variable>
				<tr>
					<xsl:for-each select="td">
							<xsl:choose>
							    <xsl:when test="position()=4">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
						                		view('view.html?load=<xsl:value-of select="$para4"/>')
						                	</xsl:attribute>
						                	<xsl:value-of select="." />
						                </a>
	                				</td>
								</xsl:when>
								<xsl:when test="position()=5">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
						                		view('view_score1.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
						                	</xsl:attribute>
						                	<xsl:value-of select="." />
						                </a>
	                				</td>
								</xsl:when>
								  <xsl:when test="position()=7">
                <td class="numberText" bgColor='#99FFCC'><B>
                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
                 </B></td>
                </xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select="." />
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>