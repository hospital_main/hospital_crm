<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>题目名称</th>
  			<th nowrap='true'>答案名称</th>
  			<th nowrap='true'>得分</th>
  		</tr>
  	</thead>
  	
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
			    <xsl:variable name="para4"><xsl:for-each select="pk/*"><xsl:if test="position()=1 or position()=2 or position()=3">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:if></xsl:for-each></xsl:variable>
				<xsl:variable name="para5">
					<xsl:for-each select="pk/*">
						&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;
					</xsl:for-each>
				</xsl:variable>
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
                <xsl:when test="position()=2"><xsl:value-of select="."/>
                </xsl:when>
                <xsl:when test="position()=3">
                 <xsl:if test="../td[1] = '合计'">
                  <xsl:value-of select="."/>
                 </xsl:if>
                   <xsl:if test="../td[1] != '合计'">
  	                <xsl:value-of select="."/>
  	            </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>