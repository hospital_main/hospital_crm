<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<item name='总得分'>
				<xsl:for-each select="/root/tbody/tr">
				    <xsl:variable name="deptname" select="td[1]" />
				    <xsl:variable name="deptscore" select="td[2]" />
					<item name='{$deptname}' value='{$deptscore}'/>
				</xsl:for-each>
			</item>
			<item name='平均分'>
				<xsl:for-each select="/root/tbody/tr">
				    <xsl:variable name="deptname" select="td[1]" />
				    <xsl:variable name="deptscore" select="td[3]" />
					<item name='{$deptname}' value='{$deptscore}'/>
				</xsl:for-each>
			</item>
			<item name='上限值'>
				<xsl:for-each select="/root/tbody/tr">
				    <xsl:variable name="deptname" select="td[1]" />
				    <xsl:variable name="deptscore" select="td[4]" />
					<item name='{$deptname}' value='{$deptscore}'/>
				</xsl:for-each>
			</item>
			<item name='下限值'>
				<xsl:for-each select="/root/tbody/tr">
				    <xsl:variable name="deptname" select="td[1]" />
				    <xsl:variable name="deptscore" select="td[5]" />
					<item name='{$deptname}' value='{$deptscore}'/>
				</xsl:for-each>
			</item>
		</root>
	</xsl:template>
</xsl:stylesheet>


