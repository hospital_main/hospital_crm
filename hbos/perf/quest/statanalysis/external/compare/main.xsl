<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=last()">
	    			<tr noWrap='true' class='mainHead'>
	    				<th noWrap="true">ѡ��</th>
			    		<xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=1">
			          	</xsl:when>
			          	<xsl:otherwise>
		                <th nowrap='true'><xsl:value-of select="."/></th>
		              </xsl:otherwise>
		            </xsl:choose>
		          </xsl:for-each>
	          </tr>
	        </xsl:when>
	      </xsl:choose>
	    </xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position()!=last()]">
				<tr>
				<xsl:if test="td[1]='title'"><xsl:attribute name="style">display:none</xsl:attribute></xsl:if>
				 <td align='center'  style='display:block'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
					<xsl:for-each select="td">
							<xsl:choose>
			          	<xsl:when test="position()=1">
			          		</xsl:when>
			          		<xsl:when test="position()=2">
			          		<td nowrap='true'> <xsl:value-of select="."/></td>
			          		</xsl:when>
			          	<xsl:otherwise>
		                <td align="right"> <xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		              </xsl:otherwise>
		            </xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>