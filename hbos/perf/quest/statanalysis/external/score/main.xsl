<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>科室名称</th>
				<th>总得分</th>
				<th>平均分</th>
				<!--th>上限值</th>
				<th>下限值</th-->
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
				<xsl:choose>	
							<xsl:when test="position()=2 or position()=3" >
								<td align="right">									
										<xsl:value-of select="format-number(.,'#,##0.00')" />						
								</td>
							</xsl:when>	
					  <xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>