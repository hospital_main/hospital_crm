<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style='display:none' width='25'>
					<input type='checkbox' />
				</th>
				<th>题目代码</th>
				<th>题目名称</th>
				<th>拼音码</th>
				<th style="display:none">答案编码</th>
				<th>答案名称</th>
				<th>题目类别</th>
				<th>外部问卷</th>
				<th>内部问卷</th>
				<th>停用标志</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
            			<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              				<xsl:attribute name="value" >
                				<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  			</xsl:attribute>
    			  		</input>
          			</td>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												openUpdatePage('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
		                					</xsl:attribute>
		                					<xsl:value-of select="." />
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=4">
									<td style="display:none;">
	                					<xsl:value-of select="." />
	                				</td>
								</xsl:when>
								<xsl:when test="position()=5">
									<td>
										<a href="#">
										<xsl:attribute name="onclick" >
		                  						view('../questresult/view.html?load=&lt;comp_code&gt;replace_code&lt;/comp_code&gt;&lt;id&gt;<xsl:value-of select="../td[4]"/>&lt;/id&gt;');
		                				</xsl:attribute>
		                				<xsl:value-of select="." />
		                				</a>
	                				</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="." /></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>

