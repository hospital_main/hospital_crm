<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>考评人代码</th>
  			<th nowrap='true'>考评人名称</th>
  			<th nowrap='true'>考评人数</th>
  			<th nowrap='true'>考评人平均分</th>
  			<th nowrap='true'>考评人权重</th>
  			<th nowrap='true'>得分</th>
  		</tr>
  	</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test=" position()=3 or position()=4 or position()=5  ">
								<td align="right">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()=6">
                <xsl:if test="../td[5] = '合计'">
                  <td><xsl:value-of select="."/></td>
                </xsl:if>
                <xsl:if test="../td[5] != '合计'">
                	<td>
	                <a tabindex='-1' href="#">
	                  <xsl:attribute name="onclick">
	    	            viewDlg('scores_score.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>')
	  	              </xsl:attribute>
	  	              <xsl:value-of select="."/>
	  	            </a>
	  	            </td>
  	            </xsl:if>
              </xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>