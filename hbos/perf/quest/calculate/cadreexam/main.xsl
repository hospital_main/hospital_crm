				<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>科室名称</th>
  			<th nowrap='true'>干部名称</th>
  			<th nowrap='true'>考核年度</th>
  			<th nowrap='true'>考核期间</th>
  			<th nowrap='true'>问卷名称</th>
  			<th nowrap='true'>总得分</th>
  			<th nowrap='true'>转换比例</th>
  			<th nowrap='true'>最终得分</th>
  			<th nowrap='true'>发放张数</th>
  			<th nowrap='true'>有效张数</th>
  			<th nowrap='true'>总人数</th>
  			<th nowrap='true'>备注</th>
  		</tr>
  	</thead>
  	
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
			    <xsl:variable name="para4"><xsl:for-each select="pk/*"><xsl:if test="position()=1 or position()=2 or position()=3">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:if></xsl:for-each></xsl:variable>
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;' >
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
           
              <xsl:choose>
                <xsl:when test="position()=5"><td>
                  <xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:when test="position()=6"><td class="numberText"><a tabindex='-1'>
                  <xsl:attribute name="href">
    	            javascript:openDialog('scores.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:880px;dialogHeight:660px', result)
  	          </xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/></a></td>
                </xsl:when>
                <xsl:when test="position()=7  or position()=9 or position()=10 or position()=11">
                <td class="numberText">
                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
                 </td>
                </xsl:when>
                 <xsl:when test="position()=8">
                <td class="numberText" bgColor='#99FFCC'><B>
                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
                 </B></td>
                </xsl:when>
                 <xsl:when test="position()=13">
                </xsl:when>
                 <xsl:when test="position()=14">
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
            
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>