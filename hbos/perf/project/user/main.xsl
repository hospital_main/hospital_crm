<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th nowrap='true'>用户代码</th>
		  	<th nowrap='true'>用户名称</th>
		  	<th nowrap='true'>用户描述</th>
      </tr>
    </thead>
    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td noWrap='true' >
            <a tabindex='-1' href='#'>
            	<xsl:attribute name="onclick">
              	viewDetail('<xsl:value-of select="td[1]"/>');
            	</xsl:attribute><xsl:value-of select="td[2]"/>
            </a>
          </td>
          <td noWrap='true' ><xsl:value-of select="td[3]"/></td>
          <td noWrap='true' ><xsl:value-of select="td[4]"/></td>
  			</tr>
   		</xsl:for-each>
     </tbody>
  </xsl:template>
</xsl:stylesheet>