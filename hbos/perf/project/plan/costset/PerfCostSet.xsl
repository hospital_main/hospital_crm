<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style="display:none"><input type="checkbox"/></th>
				<th nowrap='true' align="center">科室代码</th>
				<th nowrap='true' width="35%" align="center">科室名称</th>
				<th nowrap='true' align="center">绩效科室</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:if test="td[6] = '1'">
				<td align='center'  style='display:none'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
    			  </xsl:attribute>
  			  </input>
        </td>
        </xsl:if>
        <xsl:if test="td[6] != '1'">
        	<td align='center' >
        	</td>
        </xsl:if>
        <xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position() = 3">
							
							<xsl:if test="../td[5] = '1'">
									<td>
										<xsl:value-of select="../td[1]"/>
									</td>
								</xsl:if>
								<xsl:if test="../td[5] != '1'">
									<td>
										<xsl:if test="../td[6] = '1'">
										<input type="text" name="perf_dept"  load="bonus_dept_code_pym" required="false" class="inputSelectS">
											<xsl:attribute name="para">&lt;a&gt;<xsl:value-of select="../td[4]"/>&lt;/a&gt;</xsl:attribute>
											<xsl:attribute name="initValue"><xsl:value-of select="."/></xsl:attribute>
											<xsl:attribute name="dept_code"><xsl:value-of select="../td[1]"/></xsl:attribute>
											<xsl:attribute name="dept_name"><xsl:value-of select="../td[2]"/></xsl:attribute>
											<xsl:attribute name="stratagem_sequence_no"><xsl:value-of select="../td[4]"/></xsl:attribute>
										</input>
										</xsl:if>
										<xsl:if test="../td[6] != '1'">
										</xsl:if>
									</td>
								</xsl:if>
						</xsl:when>
						<xsl:when test="position() = 4  ">
						</xsl:when>
						<xsl:when test="position() = 5  ">
						</xsl:when>
						<xsl:when test="position() = 6  ">
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>