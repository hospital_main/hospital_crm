<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' width="120"><input type='checkbox'  style="display:none"/>指标名称</th>
				<th nowrap='true' width="60">计量单位</th>
				<th nowrap='true' width="160">评测目的</th>
				<th nowrap='true' width="160">描述</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position() &lt; 5]">
						<xsl:choose>
							<xsl:when test="position()=1" >
								<td align="left" >
									<xsl:if test="../td[5]/text() = 1">
										<input name="cbSeqNo" type="checkbox">
											<xsl:attribute name="value"><xsl:value-of select="../td[6]" /></xsl:attribute>
										</input>
									</xsl:if>
									<xsl:if test="../td[5]/text() != 1">
									　
									</xsl:if>
									<xsl:value-of select="." />
								</td>
							</xsl:when>						
							<xsl:when test="position()=2" >
								<td align="center" >
										<xsl:value-of select="." />
								</td>
							</xsl:when>							
							<xsl:when test="position()=3" >
								<td align="center" style="width:160px;word-break;keep-all;">
										<xsl:value-of select="." />
								</td>
							</xsl:when>			
							<xsl:when test="position()=4" >
								<td align="center" style="width:160px;word-break;keep-all;">
										<xsl:value-of select="." />
								</td>
							</xsl:when>													
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
