<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			
				<tr noWrap="true" class="mainHead">
					<th style="display:none" width="25">
						<input type="checkbox" name="isAll"/>
					</th>
					<th nowrap="true">指标名称</th>
					<th nowrap="true">科室名称</th>
					<th nowrap="true">计量单位</th>
					<th nowrap="true">年目标值</th>
					<th nowrap="true">1月</th>
					<th nowrap="true">2月</th>
					<th nowrap="true">3月</th>
					<th nowrap="true">4月</th>
					<th nowrap="true">5月</th>
					<th nowrap="true">6月</th>
					<th nowrap="true">7月</th>
					<th nowrap="true">8月</th>
					<th nowrap="true">9月</th>
					<th nowrap="true">10月</th>
					<th nowrap="true">11月</th>
					<th nowrap="true">12月</th>
				</tr>
			
		</thead>
		<tbody>
			
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<td align="center">
							<input type="checkbox" TABINDEX="-1" style="font-size:8px;" name="rowId">
								<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
							</input>
						</td>
						<xsl:for-each select="td[position() &lt; 17]">

							<xsl:choose>
								<xsl:when test="position() = 4">
									<td align="center">
										<input type="text" onfocus="this.select()"  style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[17]"/></xsl:attribute>
											<xsl:attribute name="name">mv_0</xsl:attribute>
										</input>
									</td>
								</xsl:when>
								<xsl:when test="position() = 5">
									<xsl:if test="string(number(.))!='NaN'">
										<td align="center">
											<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[18]"/></xsl:attribute>
												<xsl:attribute name="name">mv_1</xsl:attribute>
											</input>
										</td>
									</xsl:if>
									<xsl:if test="string(number(.))='NaN'">
										<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[18]"/></xsl:attribute>
												<xsl:attribute name="name">mv_1</xsl:attribute>
											</input>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = 6 ">
								<xsl:if test="string(number(.))!='NaN'">
									<td align="center">
										<input type="text"  onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[19]"/></xsl:attribute>
											<xsl:attribute name="name">mv_2</xsl:attribute>
										</input>
									</td>
									</xsl:if>
									<xsl:if test="string(number(.))='NaN'">
										<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[19]"/></xsl:attribute>
												<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
												<xsl:attribute name="name">mv_2</xsl:attribute>
											</input>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = 7">
								<xsl:if test="string(number(.))!='NaN'">
									<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[20]"/></xsl:attribute>
											<xsl:attribute name="name">mv_3</xsl:attribute>
										</input>
									</td>
									</xsl:if>
									<xsl:if test="string(number(.))='NaN'">
										<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[20]"/></xsl:attribute>
												<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
												<xsl:attribute name="name">mv_3</xsl:attribute>
											</input>  
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = 8">
								<xsl:if test="string(number(.))!='NaN'">
									<td align="center">
										<input type="text" onfocus="this.select()"  style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[21]"/></xsl:attribute>
											<xsl:attribute name="name">mv_4</xsl:attribute>
										</input>
									</td>
									</xsl:if>
									<xsl:if test="string(number(.))='NaN'">
										<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[21]"/></xsl:attribute>
												<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
												<xsl:attribute name="name">mv_4</xsl:attribute>
											</input>  
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = 9">
								<xsl:if test="string(number(.))!='NaN'">
									<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[22]"/></xsl:attribute>
											<xsl:attribute name="name">mv_5</xsl:attribute>
										</input>
									</td>
									</xsl:if>
									<xsl:if test="string(number(.))='NaN'">
										<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[22]"/></xsl:attribute>
												<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
												<xsl:attribute name="name">mv_5</xsl:attribute>
											</input>  
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = 10">
								<xsl:if test="string(number(.))!='NaN'">
									<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[23]"/></xsl:attribute>
											<xsl:attribute name="name">mv_6</xsl:attribute>
										</input>
									</td>
									</xsl:if>
									<xsl:if test="string(number(.))='NaN'">
										<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[23]"/></xsl:attribute>
												<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
												<xsl:attribute name="name">mv_6</xsl:attribute>
											</input>  
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = 11">
								<xsl:if test="string(number(.))!='NaN'">
									<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[24]"/></xsl:attribute>
											<xsl:attribute name="name">mv_7</xsl:attribute>
										</input>
									</td>
									</xsl:if>
									<xsl:if test="string(number(.))='NaN'">
										<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[24]"/></xsl:attribute>
												<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
												<xsl:attribute name="name">mv_7</xsl:attribute>
											</input>  
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = 12">
								<xsl:if test="string(number(.))!='NaN'">
									<td align="center">
										<input type="text" onfocus="this.select()"  style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[25]"/></xsl:attribute>
											<xsl:attribute name="name">mv_8</xsl:attribute>
										</input>
									</td>
									</xsl:if>
									<xsl:if test="string(number(.))='NaN'">
										<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[25]"/></xsl:attribute>
												<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
												<xsl:attribute name="name">mv_8</xsl:attribute>
											</input>  
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = 13">
								<xsl:if test="string(number(.))!='NaN'">
									<td align="center" >
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13" onkeydown='if(event.keyCode==13) event.keyCode=9  '  >
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[26]"/></xsl:attribute>
											<xsl:attribute name="name">mv_9</xsl:attribute>
										</input>
									</td>
									</xsl:if>
									<xsl:if test="string(number(.))='NaN'">
										<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[26]"/></xsl:attribute>
												<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
												<xsl:attribute name="name">mv_9</xsl:attribute>
											</input>  
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = 14">
								<xsl:if test="string(number(.))!='NaN'">
									<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[27]"/></xsl:attribute>
											<xsl:attribute name="name">mv_10</xsl:attribute>
										</input>
									</td>
									</xsl:if>
									<xsl:if test="string(number(.))='NaN'">
										<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[27]"/></xsl:attribute>
												<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
												<xsl:attribute name="name">mv_10</xsl:attribute>
											</input>  
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = 15">
								<xsl:if test="string(number(.))!='NaN'">
									<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[28]"/></xsl:attribute>
											<xsl:attribute name="name">mv_11</xsl:attribute>
										</input>
									</td>
									</xsl:if>
									<xsl:if test="string(number(.))='NaN'">
										<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[28]"/></xsl:attribute>
												<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
												<xsl:attribute name="name">mv_11</xsl:attribute>
											</input>  
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() = 16">
								<xsl:if test="string(number(.))!='NaN'">
									<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[29]"/></xsl:attribute>
											<xsl:attribute name="name">mv_12</xsl:attribute>
										</input>
									</td>
									</xsl:if>
									<xsl:if test="string(number(.))='NaN'">
										<td align="center">
										<input type="text" onfocus="this.select()" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
												<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
												<xsl:attribute name="sequence_no"><xsl:value-of select="../td[29]"/></xsl:attribute>
												<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
												<xsl:attribute name="name">mv_12</xsl:attribute>
											</input>  
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			
		</tbody>
	</xsl:template>
</xsl:stylesheet>
