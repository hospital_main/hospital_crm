<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead' id="tr_0">
				<th width='25' >

				</th>
				<th nowrap="true">调整内容</th>
				<th nowrap="true">调整原因</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center' noWrap='true' style='display:none'>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;' name="rowId">
							<xsl:attribute name="value">
								<xsl:for-each select="pk/*">
									<xsl:value-of select="." />
								</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td noWrap='true'>
									<xsl:value-of select="." />
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap='true'>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>