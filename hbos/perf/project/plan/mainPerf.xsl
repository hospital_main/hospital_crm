<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th width='60'>选择</th>
				<th>年度</th>
				<th>目标名称</th>
				<th>状态</th>
				<th>是否虚拟</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center' noWrap='true'>
						<input type='radio' name="radioPerf" TABINDEX='-1' onclick="getValue(this)" style='font-size:8px;'>
							<xsl:attribute name="value">
								<xsl:for-each select="pk/sequence_no">
									<xsl:value-of select="." />
								</xsl:for-each>
							</xsl:attribute>
							<xsl:attribute name="value2">
								<xsl:for-each select="pk/state_flag">
									<xsl:value-of select="." />
								</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td noWrap='true' align='center'>
									<xsl:value-of select="." />
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap='true' align='center'>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
