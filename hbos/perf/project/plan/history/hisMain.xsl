<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>指标代码</th>
				<th>指标名称</th>
				<th>评测目的</th>
				<th>描述</th>
				<th>指标关系</th>
				<th>计量单位</th>
				<th>报告频率</th>
				<th>收集方式</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
									<td><a tabindex="-1">
										<xsl:value-of select="."/>
									</a></td>
								</xsl:when>
								<xsl:when test="position()=5 ">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
								<!--测试新增-->

								
								<xsl:when test="position()=9">
									<td><a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('contractpay_main.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/contract_no&gt;', 'dialogWidth:850px;dialogHeight:650px', result)

		                </xsl:attribute>管理</a></td>
								</xsl:when>
								<xsl:when test="position()=10">
									<td><a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('contractdoc_main.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/contract_no&gt;', 'dialogWidth:850px;dialogHeight:650px', result)
		                </xsl:attribute>管理</a></td>
								</xsl:when>
								<!--测试新增-->
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
