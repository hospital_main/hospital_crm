<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th style='display:none' width='25'><input type='checkbox'/></th>
				<th nowrap="true">大类代码</th>
				<th nowrap="true">大类名称</th>
				<th nowrap="true">小类代码</th>
				<th nowrap="true">小类名称</th>
      </tr>
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="kind_code" select="td[1]"/>
				<xsl:variable name="rowspan" select="td[5]"/>
				<xsl:variable name="cur_pos" select="position()"/>
				<tr>
					<xsl:if test="$cur_pos = 1 or $kind_code != ../tr[$cur_pos - 1]/td[1]">
						<td align="center" rowspan="{$rowspan}">
							<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
    			  	</input>
						</td>
						<td align="center" rowspan="{$rowspan}">
							<xsl:value-of select="td[1]"/>
						</td>
						<td align="center" rowspan="{$rowspan}">
							<xsl:value-of select="td[2]"/>
						</td>
					</xsl:if>
					<td align="center">
						<xsl:value-of select="td[3]"/>
					</td>
					<td align="center">
						<xsl:value-of select="td[4]"/>
					</td>
				</tr>
			</xsl:for-each>
     </tbody>
  </xsl:template>
</xsl:stylesheet>