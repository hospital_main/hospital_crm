<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' >基本指标代码</th>
				<th nowrap='true' >基本指标名称</th>
				<th nowrap='true' >描述</th>		
				<th nowrap='true' >计量单位</th>	
				<th nowrap='true' >所属分类</th>	
				<th nowrap='true' >基本指标性质</th>	
				<th nowrap='true' >对应函数名称</th>	
				<th nowrap='true' >计算公式</th>												
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=3" >
								<td align="left">		
								<p>																																																	
										<xsl:value-of select="." />  
								</p>							
								</td>
							</xsl:when>																								
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
