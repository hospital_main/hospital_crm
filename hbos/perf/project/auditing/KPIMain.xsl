<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' >指标代码</th>
				<th nowrap='true' >指标名称</th>
				<th nowrap='true' >评测目的</th>
				<th nowrap='true' >描述</th>
				<th nowrap='true' >收集部门</th>
				<th nowrap='true'>数据来源</th>
				<th nowrap='true'>计量单位</th>
				<th nowrap='true'>报告频率</th>		
				<th nowrap='true'>收集方式</th>																		
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=2" >
								<td align="left">									
									<a tabindex="-2" style="text-decoration:none" >
										<xsl:attribute name="href">
											javascript:openDialog('update.html?load=&lt;target_code&gt;<xsl:value-of select="../pk"/>&lt;/target_code&gt;', 'dialogWidth:600px;dialogHeight:550px', result)
										</xsl:attribute> 
										<xsl:value-of select="." />  
									</a>								
								</td>
							</xsl:when>
							<xsl:when test="position()=3" >
								<td align="center" style="width:140px;word-break;keep-all">
										<xsl:value-of select="." />
								</td>
							</xsl:when>			
							<xsl:when test="position()=4" >
								<td align="center" style="width:140px;word-break;keep-all">
										<xsl:value-of select="." />
								</td>
							</xsl:when>													
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
