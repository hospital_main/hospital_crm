<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' >基本指标</th>
				<th nowrap='true' >计算公式</th>
				<th nowrap='true' >数据来源</th>
				<th nowrap='true' >统计部门</th>
				<th nowrap='true' >计分方法</th>
				<th nowrap='true' id="calcuteValue" >权重</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>	
							<xsl:when test="position()=2" >
								<td align="right">									
									<xsl:value-of select="." />
								</td>
							</xsl:when>
							<xsl:when test="position()=5" >
								<td>
									<xsl:value-of select="." disable-output-escaping="yes"/>
								</td>
							</xsl:when>
							<xsl:when test="position()=6" >
								<td align="right">
									<xsl:value-of select="." />
								</td>
							</xsl:when>
							<xsl:when test="position()=7" >
								<td style="display:none">
									<xsl:value-of select="." />										
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
