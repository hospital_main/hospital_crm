<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' >科室代码</th>
				<th nowrap='true' >科室名称</th>
				<th nowrap='true' >目标值</th>
				<th nowrap='true' >评价标准</th>												
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=3" >
								<td align="right">																																																	
										<xsl:value-of select="format-number(.,'#,##0.00')" />  						
								</td>
							</xsl:when>				
							<xsl:when test="position()=4" >
								<td align="left">		
								<p>																																																	
										<xsl:value-of select="." />  
								</p>							
								</td>
							</xsl:when>																								
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
