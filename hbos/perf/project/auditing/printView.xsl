<?xml version="1.0" encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='' />
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style='width:80mm' />
				<col style='width:100mm' />
				<col style='width:170mm' />
				<col style='width:170mm' />
				<col style='width:80mm' />
				<col style='width:80mm' />
				<col style='width:80mm' />
				<col style='width:80mm' />	
				<col style='width:80mm' />								
			</colgroup>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<th nowrap='true' >指标代码</th>
					<th nowrap='true' >指标名称</th>
					<th nowrap='true' >评测目的</th>
					<th nowrap='true' >描述</th>
					<th nowrap='true' >收集部门</th>
					<th nowrap='true'>数据来源</th>
					<th nowrap='true'>计量单位</th>
					<th nowrap='true'>报告频率</th>		
					<th nowrap='true'>收集方式</th>																		
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position() = 7">
									<td  nowrap='true'>
										<xsl:value-of select="." />
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td nowrap='true'>
										<xsl:value-of select="." />
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>