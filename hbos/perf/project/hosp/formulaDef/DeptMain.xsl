<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th width='60'>选择</th>
				<th>指标代码</th>
				<th>指标名称</th>
				<th>计算公式</th>
				<th>评测目的</th>
				<th>描述</th>				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center' noWrap='true'>
						<input type='radio' name="radioPerf" TABINDEX='-1' onclick="getValue(this)" style='font-size:8px;'>
							<xsl:attribute name="value">
								<xsl:for-each select="pk/*">
									<xsl:value-of select="." />
								</xsl:for-each>
							</xsl:attribute>
							<xsl:attribute name="gather_type">
									<xsl:value-of select="./td[6]" />
							</xsl:attribute>							
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=6 or position()=7">
							</xsl:when>	
							<xsl:when test="position()=3">
							 <td width="400">
									<xsl:value-of select="." />
								</td>
							</xsl:when>	
							<xsl:when test="position()=4 or position()=5">
							 <td width="180">
									<xsl:value-of select="." />
								</td>
							</xsl:when>									
							<xsl:otherwise>
								<td noWrap='true'>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
