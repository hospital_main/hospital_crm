<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th >月份</th>
				<th nowrap='true' >基本值</th>
				<th nowrap='true' >目标值</th>
				<th nowrap='true' >最佳值</th>												
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=2" >
								<td align="center">									
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="sequence_no"><xsl:value-of select="../td[5]"/></xsl:attribute>																				
									</input> 
									<input type="hidden" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(../td[3],'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="sequence_no"><xsl:value-of select="../td[5]"/></xsl:attribute>																				
									</input> 								
								</td>
							</xsl:when>
							<xsl:when test="position()=4 " >
								<td align="center">									
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="sequence_no"><xsl:value-of select="../td[5]"/></xsl:attribute>																				
									</input> 								
								</td>
							</xsl:when>	
							<xsl:when test="position()=3" >
								<td  align="center">
									<xsl:value-of select="format-number(.,'#,##0.00')" />
								</td>							
							</xsl:when>								
							<xsl:when test="position()=5 or position()=6" >
							</xsl:when>																								
							<xsl:otherwise>
								<td  align="center">
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
