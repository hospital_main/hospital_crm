<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th style='display:none' width='25'><input type='checkbox' name='isAll'/></th>
				<th nowrap="true">指标名称</th>
				<th nowrap="true">计量单位</th>
				<th nowrap="true">年目标值</th>
				<th style='display:none' nowrap="true">12月</th>
      </tr>
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center">
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;' name='rowId'>
	             <xsl:attribute name="value" >
	               <xsl:for-each select="pk/*"><xsl:value-of select="."/></xsl:for-each>
	      		  </xsl:attribute>
    			 	</input>
					</td>
					<xsl:for-each select="td[position() &lt; 5]">
					  <xsl:choose>
							<xsl:when test="position() = 3" >
								<td align="center">
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
									  <xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')" /></xsl:attribute>
									  <xsl:attribute name="sequence_no"><xsl:value-of select="../td[5]" /></xsl:attribute>	
										<xsl:attribute name="name">mv_0</xsl:attribute>
									</input>
								</td>
							</xsl:when>
							<xsl:when test="position() = 4" >
								<td align="center" style='display:none'>
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
									  <xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')" /></xsl:attribute>
									  <xsl:attribute name="sequence_no"><xsl:value-of select="../td[6]" /></xsl:attribute>	
										<xsl:attribute name="name">mv_12</xsl:attribute>
									</input>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
     </tbody>
  </xsl:template>
</xsl:stylesheet>