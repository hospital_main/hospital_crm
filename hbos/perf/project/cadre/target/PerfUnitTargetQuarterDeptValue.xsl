<?xml version="1.0" encoding="gb2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="TotalRow" select="count(/root/tbody/tr)"/>
			<xsl:variable name="PerfMonth" select="/root/tbody/tr[1]/td[5]"/>
			<xsl:variable name="TotalCol" select="count(/root/tbody/tr[td=$PerfMonth])"/>
			<xsl:variable name="LoopStep" select="$TotalRow div $TotalCol"/>
			<tr noWrap="true" class="mainHead">
			  <th style='display:none;' width='25'><input type='checkbox' name='isAll'/></th>
				<th nowrap="true" valign="middle">指标名称</th>
				<th nowrap="true" valign="middle">干部名称</th>
				<th nowrap="true" valign="middle">计量单位</th>
				<xsl:for-each select="/root/tbody/tr">
				  <xsl:if test="position() &lt;= $LoopStep">
						<th nowrap="true">
							<xsl:value-of select="td[6]"/>
						</th>
					</xsl:if>
				</xsl:for-each>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="Step" select="position() mod $LoopStep"/>
				<xsl:if test="$LoopStep = 1 or $Step = 1">
				  <xsl:text disable-output-escaping="yes"><![CDATA[<tr><td noWrap="true" align="center">]]></xsl:text>
				    <input type='checkbox' TABINDEX='-1' style='font-size:8px;' name='rowId'>
				    	<xsl:attribute name="value" >
	               <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      		  </xsl:attribute>
				    </input>
					<xsl:text disable-output-escaping="yes"><![CDATA[</td><td noWrap="true">]]></xsl:text>
					<xsl:value-of select="td[2]"/>
					<xsl:text disable-output-escaping="yes"><![CDATA[</td><td noWrap="true">]]></xsl:text>
					<xsl:value-of select="td[3]"/>
					<xsl:text disable-output-escaping="yes"><![CDATA[</td><td noWrap="true">]]></xsl:text>
					<xsl:value-of select="td[4]"/>
					<xsl:text disable-output-escaping="yes"><![CDATA[</td>]]></xsl:text>
				</xsl:if>
				<td align="right">
					<xsl:if test="string(number(td[8])) != 'NaN'">
								<input type="text" TABINDEX="-1"  style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
								<xsl:attribute name="sequence_no"><xsl:value-of select="td[1]"/></xsl:attribute>
								<xsl:attribute name="value"><xsl:value-of select="format-number(td[8],'###0.00####')"/></xsl:attribute>
								<xsl:attribute name="name">mv_<xsl:value-of select="td[5]"/></xsl:attribute>
							</input>
					</xsl:if>
					<xsl:if test="string(number(td[8])) = 'NaN'">
							<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
								<xsl:attribute name="sequence_no"><xsl:value-of select="td[1]"/></xsl:attribute>
								<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
								<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
								<xsl:attribute name="name">mv_<xsl:value-of select="td[5]"/></xsl:attribute>
							</input>
					</xsl:if>

        </td>
				<xsl:if test="$LoopStep = 1 or $Step = 0">
					<xsl:text disable-output-escaping="yes"><![CDATA[</tr>]]></xsl:text>
				</xsl:if>
			</xsl:for-each> 
		</tbody>
	</xsl:template>
</xsl:stylesheet>
	