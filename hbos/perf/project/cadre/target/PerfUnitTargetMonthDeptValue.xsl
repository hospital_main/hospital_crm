<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
				<tr noWrap="true" class="mainHead">
					<th style="display:none" width="25">
						<input type="checkbox" name="isAll"/>
					</th>
					<th nowrap="true">指标名称</th>
					<th nowrap="true">干部姓名</th>
					<th nowrap="true">计量单位</th>
					<th nowrap="true">年目标值</th>
					<th nowrap="true">1月</th>
					<th nowrap="true">2月</th>
					<th nowrap="true">3月</th>
					<th nowrap="true">4月</th>
					<th nowrap="true">5月</th>
					<th nowrap="true">6月</th>
					<th nowrap="true">7月</th>
					<th nowrap="true">8月</th>
					<th nowrap="true">9月</th>
					<th nowrap="true">10月</th>
					<th nowrap="true">11月</th>
					<th nowrap="true">12月</th>
				</tr>
		</thead>
		<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<td align="center">
							<input type="checkbox" TABINDEX="-1" style="font-size:8px;" name="rowId">
								<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
							</input>
						</td>
						<xsl:for-each select="td[position() &lt; 17]">
						<xsl:variable name="seq_no" select="position()+13"></xsl:variable>
						<xsl:variable name="input_name_no" select="position()-4"></xsl:variable>
							<xsl:choose>
								<xsl:when test="position() &gt;= 4 and position() &lt;=16">
									<td align="center">
									<xsl:if test="string(number(.)) != 'NaN'">
										<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00####')"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[$seq_no]"/></xsl:attribute>
											<xsl:attribute name="name">mv_<xsl:value-of select="$input_name_no"/>
											</xsl:attribute>
										</input>
									</xsl:if>
									<xsl:if test="string(number(.)) = 'NaN'">
										<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="12" maxlength="13">
											<xsl:attribute name="value"><xsl:value-of select="'--'"/></xsl:attribute>
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[$seq_no]"/></xsl:attribute>
											<xsl:attribute name="readonly"><xsl:value-of select="'true'"/></xsl:attribute>
											<xsl:attribute name="name">mv_<xsl:value-of select="$input_name_no"/>
											</xsl:attribute>
										</input>
									</xsl:if>

									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
