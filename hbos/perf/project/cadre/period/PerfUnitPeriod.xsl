<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			  <th nowrap='true'>���ҷ������</th>
				<th nowrap='true'>���ҷ�������</th>
				<th nowrap='true'>�¿���</th>
				<th nowrap='true'>������</th>
				<th nowrap='true'>���꿼��</th>
				<th nowrap='true'>�꿼��</th>
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:attribute name="_ID">
				  	<xsl:value-of select="td[7]"/>
				  </xsl:attribute>
					<xsl:for-each select="td[position() &lt; 7]">   
						<td>
							<xsl:choose>
							  <xsl:when test="position() = 2">
								  <xsl:attribute name="align">left</xsl:attribute>
								  <xsl:value-of select="."/>
								</xsl:when>
								<xsl:when test="position() = 3 ">
							    <xsl:attribute name="align">center</xsl:attribute>
									<input type='checkbox' name='is_month' onclick='IsMonth_OnClick();'>
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
										<xsl:if test=" text() = 1">
											<xsl:attribute name="checked">true</xsl:attribute>
										</xsl:if>
									</input>
								</xsl:when>
								<xsl:when test="position() = 4 ">
							    <xsl:attribute name="align">center</xsl:attribute>
									<input type='checkbox' name='is_quarter' onclick='IsQuarter_OnClick();'>
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
										<xsl:if test=" text() = 1">
											<xsl:attribute name="checked">true</xsl:attribute>
										</xsl:if>
									</input>
								</xsl:when>
							  <xsl:when test="position() = 5 ">
							    <xsl:attribute name="align">center</xsl:attribute>
									<input type='checkbox' name='is_halfyear' onclick='IsHalfyear_OnClick();'>
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
										<xsl:if test=" text() = 1">
											<xsl:attribute name="checked">true</xsl:attribute>
										</xsl:if>
									</input>
								</xsl:when>
								<xsl:when test="position() = 6">
								  <xsl:attribute name="align">center</xsl:attribute>
									<input type='checkbox' name='is_year' onclick='IsYear_OnClick();'>
									  <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
									  <xsl:if test=" text() = 1">
											<xsl:attribute name="checked">true</xsl:attribute>
										</xsl:if>
									</input>
								</xsl:when>
								<xsl:otherwise>
								  <xsl:attribute name="align">center</xsl:attribute>
									<xsl:value-of select="."/>  
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>