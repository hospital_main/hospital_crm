<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th width='60'>选择</th>
				<th>指标名称</th>
				<th>计量单位</th>
				<th>极值</th>
				<th>考核期间</th>
				<th>年基准值</th>
				<th>年目标值</th>
				<th>年最佳值</th>												
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center' noWrap='true'>
						<input type='radio' name="radioPerf" TABINDEX='-1' onclick="getValue(this)" style='font-size:8px;'>
							<xsl:attribute name="value">
								<xsl:for-each select="pk/*">
									<xsl:value-of select="." />
								</xsl:for-each>
							</xsl:attribute>
							<xsl:attribute name="is_last"><xsl:value-of select="./td[8]"/></xsl:attribute>								
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=2 or position()=3 or position()=4">
								<td noWrap='true'>
									<xsl:if test="../td[8] = 0">										
									</xsl:if>	
									<xsl:if test="../td[8] = 1">		
										<xsl:value-of select="." />																	
									</xsl:if>																		
								</td>
							</xsl:when>																											
							<xsl:when test="position()=6">
								<td noWrap='true' align='right'>
									<xsl:if test="../td[8] = 0">										
									</xsl:if>	
									<xsl:if test="../td[8] = 1">		
										<xsl:value-of select="format-number(.,'#,##0.00')" />																	
									</xsl:if>																		
								</td>
							</xsl:when>
							<xsl:when test="position()=5">
								<td noWrap='true'>
									<input type="text" maxlength="12" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="12">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="name"><xsl:value-of select="../td[9]"/></xsl:attribute>	
										<xsl:attribute name="is_last"><xsl:value-of select="../td[8]"/></xsl:attribute>																			
									</input>
									<input type="hidden" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="12">
										<xsl:attribute name="value"><xsl:value-of select="format-number(../td[6],'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="name"><xsl:value-of select="../td[9]"/></xsl:attribute>	
										<xsl:attribute name="is_last"><xsl:value-of select="../td[8]"/></xsl:attribute>																			
									</input>
								</td>
							</xsl:when>		
							<xsl:when test="position()=7">
								<td noWrap='true'>
									<input type="text" maxlength="12" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="12">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="name"><xsl:value-of select="../td[9]"/></xsl:attribute>	
										<xsl:attribute name="is_last"><xsl:value-of select="../td[8]"/></xsl:attribute>																			
									</input>
								</td>
							</xsl:when>		
							<xsl:when test="position()=8 or position()=9">
								<td style="display:none">
									<xsl:value-of select="." />
								</td>
							</xsl:when>																		
							<xsl:otherwise>
								<td noWrap='true'>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
