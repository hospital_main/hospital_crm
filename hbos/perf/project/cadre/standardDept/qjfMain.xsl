<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>		
				<th nowrap='true' width="120">指标名称</th>
				<th nowrap='true' width="60">计量单位</th>
				<th nowrap='true' width="60">极值</th>
				<th nowrap='true' width="80">区间1</th>
				<th nowrap='true' width="80">区间2</th>
				<th nowrap='true' width="80">区间3</th>
				<th nowrap='true' width="80">区间4</th>
				<th nowrap='true' width="80">区间5</th>
				<th nowrap='true' width="80">区间6</th>																				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>				
							<xsl:when test="position()=2" >		
									<td >	
										<xsl:if test="../td[24] = 0">										
										</xsl:if>	
										<xsl:if test="../td[24] = 1">										
											<xsl:value-of select="." />	
										</xsl:if>																				
									</td>
							</xsl:when>	
							<xsl:when test="position()=3" >		
									<td >	
										<xsl:if test="../td[24] = 0">										
										</xsl:if>	
										<xsl:if test="../td[24] = 1">										
											<xsl:value-of select="." />	
										</xsl:if>																				
									</td>
							</xsl:when>																									
							<xsl:when test="position()=4" >
								  <xsl:text disable-output-escaping="yes"><![CDATA[<td align="center">]]></xsl:text>
							</xsl:when>
							<xsl:when test="position()=5" >
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="name"><xsl:value-of select="../td[4]"/></xsl:attribute>	
										<xsl:attribute name="is_last"><xsl:value-of select="../td[24]"/></xsl:attribute>																			
									</input> -
							</xsl:when>
							<xsl:when test="position()=6" >			
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="name"><xsl:value-of select="../td[4]"/></xsl:attribute>	
										<xsl:attribute name="is_last"><xsl:value-of select="../td[24]"/></xsl:attribute>																															
									</input>	
							<xsl:text disable-output-escaping="yes"><![CDATA[</td>]]></xsl:text>
							</xsl:when>		
							<xsl:when test="position()=7" >
								  <xsl:text disable-output-escaping="yes"><![CDATA[<td align="center">]]></xsl:text>
							</xsl:when>											
							<xsl:when test="position()=8" >
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="name"><xsl:value-of select="../td[7]"/></xsl:attribute>	
										<xsl:attribute name="is_last"><xsl:value-of select="../td[24]"/></xsl:attribute>																										
									</input> -
							</xsl:when>
							<xsl:when test="position()=9" >			
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="name"><xsl:value-of select="../td[7]"/></xsl:attribute>		
										<xsl:attribute name="is_last"><xsl:value-of select="../td[24]"/></xsl:attribute>																		
									</input>	
							<xsl:text disable-output-escaping="yes"><![CDATA[</td>]]></xsl:text>
							</xsl:when>	
							<xsl:when test="position()=10" >
								  <xsl:text disable-output-escaping="yes"><![CDATA[<td align="center">]]></xsl:text>
							</xsl:when>					
							<xsl:when test="position()=11" >
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="name"><xsl:value-of select="../td[10]"/></xsl:attribute>		
										<xsl:attribute name="is_last"><xsl:value-of select="../td[24]"/></xsl:attribute>																										
									</input> -
							</xsl:when>
							<xsl:when test="position()=12" >			
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="name"><xsl:value-of select="../td[10]"/></xsl:attribute>	
										<xsl:attribute name="is_last"><xsl:value-of select="../td[24]"/></xsl:attribute>																			
									</input>	
							<xsl:text disable-output-escaping="yes"><![CDATA[</td>]]></xsl:text>
							</xsl:when>	
							<xsl:when test="position()=13" >
								  <xsl:text disable-output-escaping="yes"><![CDATA[<td align="center">]]></xsl:text>							
							</xsl:when>							
							<xsl:when test="position()=14" >
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>		
										<xsl:attribute name="name"><xsl:value-of select="../td[13]"/></xsl:attribute>		
										<xsl:attribute name="is_last"><xsl:value-of select="../td[24]"/></xsl:attribute>																								
									</input> -
							</xsl:when>
							<xsl:when test="position()=15" >			
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="name"><xsl:value-of select="../td[13]"/></xsl:attribute>		
										<xsl:attribute name="is_last"><xsl:value-of select="../td[24]"/></xsl:attribute>																																							
									</input>	
							<xsl:text disable-output-escaping="yes"><![CDATA[</td>]]></xsl:text>
							</xsl:when>	
							<xsl:when test="position()=16" >
								  <xsl:text disable-output-escaping="yes"><![CDATA[<td align="center">]]></xsl:text>						
							</xsl:when>							
							<xsl:when test="position()=17" >
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>		
										<xsl:attribute name="name"><xsl:value-of select="../td[16]"/></xsl:attribute>	
										<xsl:attribute name="is_last"><xsl:value-of select="../td[24]"/></xsl:attribute>																									
									</input> 
									<xsl:text disable-output-escaping="yes"><![CDATA[<i>]]></xsl:text>-
									<xsl:text disable-output-escaping="yes"><![CDATA[</i>]]></xsl:text>									
							</xsl:when>
							<xsl:when test="position()=18" >			
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="name"><xsl:value-of select="../td[16]"/></xsl:attribute>	
										<xsl:attribute name="is_last"><xsl:value-of select="../td[24]"/></xsl:attribute>																														
									</input>	
							<xsl:text disable-output-escaping="yes"><![CDATA[</td>]]></xsl:text>
							</xsl:when>		
							<xsl:when test="position()=19" >
								    <xsl:text disable-output-escaping="yes"><![CDATA[<td align="center">]]></xsl:text>						
							</xsl:when>							
							<xsl:when test="position()=20" >
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>		
										<xsl:attribute name="name"><xsl:value-of select="../td[19]"/></xsl:attribute>		
										<xsl:attribute name="is_last"><xsl:value-of select="../td[24]"/></xsl:attribute>																								
									</input> 
									<xsl:text disable-output-escaping="yes"><![CDATA[<i>]]></xsl:text>-
									<xsl:text disable-output-escaping="yes"><![CDATA[</i>]]></xsl:text>
							</xsl:when>
							<xsl:when test="position()=21" >			
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="5">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>		
										<xsl:attribute name="name"><xsl:value-of select="../td[19]"/></xsl:attribute>	
										<xsl:attribute name="is_last"><xsl:value-of select="../td[24]"/></xsl:attribute>																													
									</input>	
							</xsl:when>		
							<xsl:when test="position()=22" >		
									<td style="display:none">	
										<xsl:value-of select="." />	
									</td>
							</xsl:when>		
							<xsl:when test="position()=23" >		
									<td style="display:none">	
										<xsl:value-of select="." />	
									</td>
							</xsl:when>	
							<xsl:when test="position()=24" >		
									<td style="display:none">	
										<xsl:value-of select="." />	
									</td>
							</xsl:when>								
																																								
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
