<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>下级指标名称</th>
				<th nowrap='true'>权重</th>
				<th nowrap='true'>优先级</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position() &lt; 4]">
						<xsl:choose>
							<xsl:when test="position()=2" >
								<td align="left" >
								<input class="inputDecimal" type="text" point="2" maxlength="10" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="12">
								  <xsl:attribute name="target_code"><xsl:value-of select="../td[4]"/></xsl:attribute>
									<xsl:attribute name="value"><xsl:value-of select="format-number(.,'###0.00')" /></xsl:attribute>
									<xsl:attribute name="name">percentage_x</xsl:attribute>
								</input>
								</td>
							</xsl:when>							
							<xsl:when test="position()=3" >
								<td align="left" >
								<input type="text" maxlength="10" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="12">
									<xsl:attribute name="value"><xsl:value-of select="." /></xsl:attribute>
									<xsl:attribute name="name">priority_level</xsl:attribute>
								</input>
								</td>
							</xsl:when>			
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
