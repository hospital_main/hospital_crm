<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>维度</th>
				<th>二级KPI指标</th> 
				<th>三级KPI指标</th>
				<th>目标值</th>
				<th>计量单位</th>
				<th>采集部门</th>
				<th>评价标准</th>	
				<th>计算公式</th>								
			</tr>
		</thead>	
		<tbody>
			
		</tbody>
	</xsl:template>
</xsl:stylesheet>
