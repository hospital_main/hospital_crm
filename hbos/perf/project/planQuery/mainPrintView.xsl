<?xml version="1.0" encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN='' />
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<xsl:if test = "/root/tbody/tr[1]/td[20]!='nonono'">
	  			<td style="display:none"></td>
	  			</xsl:if>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>
				<tr noWrap='true' class='maintitle'>
					<td nowrap='true' width="100">维度</td>
					<td nowrap='true' width="100">二级KPI指标</td>
					<xsl:if test = "/root/tbody/tr[1]/td[20]!='nonono'">
					<td nowrap='true' width="100">三级KPI指标</td>
					</xsl:if>
					<td nowrap='true' width="100">目标值</td>
					<td nowrap='true' width="100">计量单位</td>
					<td nowrap='true' width="100">采集部门</td>
					<td nowrap='true' width="100">评价标准</td>
					<td nowrap='true' width="100">计算公式</td>																		
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
				  <xsl:variable name="currPos" select="position()"/>
					<xsl:variable name="unit_name" select="td[1]" />
					<xsl:variable name="target_name" select="td[2]" />
					<xsl:variable name="rowspan1" select="count(/root/tbody/tr[td[1]=$unit_name])" />
					<xsl:variable name="rowspan2" select="count(/root/tbody/tr[td[2]=$target_name])"/>
					<tr>
						<xsl:if test="$currPos = 1 or $unit_name != ../tr[$currPos - 1]/td[1]">
							<td rowspan="{$rowspan1}">
								<xsl:value-of select="td[1]" />
							</td>
						</xsl:if>
						<xsl:if test="$unit_name = ../tr[$currPos - 1]/td[1]">
							<td style="display:none">
								<xsl:value-of select="td[1]" />
							</td>
						</xsl:if>
						<xsl:if test="$currPos = 1 or $target_name != ../tr[$currPos - 1]/td[2]">
							<td rowspan="{$rowspan2}">
								<xsl:value-of select="td[2]" />
							</td>
						</xsl:if>
						<xsl:if test="$target_name = ../tr[$currPos - 1]/td[2]">
							<td style="display:none">
								<xsl:value-of select="td[2]" />
							</td>
						</xsl:if>
						<xsl:if test = "td[20]!='nonono'">
							<td noWrap='true'>
								<xsl:value-of select="td[3]" />
							</td>
						</xsl:if>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=4">
									<td noWrap='true'>
									  <xsl:value-of select="." />
									</td>							
								</xsl:when>		
								<xsl:when test="position()=7 "  >
									<td align="left" style="width:280px;word-break;keep-all;">
										<xsl:if test="../td[9] ='U'">										
											<xsl:value-of select="." disable-output-escaping="yes"/>>
										</xsl:if>	
										<xsl:if test="../td[9] ='D'">																																																															
											<xsl:value-of select="." disable-output-escaping="yes"/>   
									</xsl:if>	
									</td>
								</xsl:when>	
									
								<xsl:when test="position()=8" >
									<td align="left" >										
																																																																																											
										<xsl:value-of select="." />											
									</td>
								</xsl:when>								
								<xsl:when test="position()=1 or position()=2 or position()=3 or position()>=9" >
								</xsl:when>																				
								<xsl:otherwise>
									<td noWrap='true'>
										<xsl:value-of select="." />
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>