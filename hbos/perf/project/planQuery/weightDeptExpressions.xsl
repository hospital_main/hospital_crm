<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' >基本指标</th>
				<th nowrap='true' >目标值</th>
				<th nowrap='true' >计算公式</th>
				<th nowrap='true' >数据来源</th>
				<th nowrap='true' >统计部门</th>
				<th nowrap='true' id="calcuteValue" >权重</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>	
							<xsl:when test="position()=6" >
								<td align="left">									
										<xsl:value-of select="." />						
								</td>
							</xsl:when>	
							<xsl:when test="position()=7" >

							</xsl:when>																									
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
