<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' >维度</th>
				<th nowrap='true' >二级KPI指标</th>
				<xsl:if test="/root/tbody/tr[1]/td[11]!='nonono'">
					<th nowrap='true' >三级KPI指标</th>
				</xsl:if>
				<th nowrap='true' >评测目的</th>
				<th nowrap='true' >描述</th>
				<th nowrap='true' >收集部门</th>
				<th nowrap='true'>数据来源</th>
				<th nowrap='true'>计量单位</th>
				<th nowrap='true'>报告频率</th>		
				<th nowrap='true'>收集方式</th>																		
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				 <xsl:variable name="unit_name" select="td[1]" />
				 <xsl:variable name="target_name" select="td[2]" />
				<xsl:variable name="cur_pos" select="position()" />					
				<xsl:variable name="rowspan1" select="count(/root/tbody/tr[td[1]=$unit_name])" />
				<xsl:variable name="rowspan2" select="count(/root/tbody/tr[td[2]=$target_name and td[1]=$unit_name ])" />
					<xsl:for-each select="td">
						<xsl:choose>
						 <xsl:when test="position()=1">
								<xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos - 1]/td[1]">
									<td rowspan="{$rowspan1}">
										<xsl:value-of select="." />
									</td>
								</xsl:if>
								<xsl:if test="$unit_name = ../../tr[$cur_pos - 1]/td[1]">
									<td style="display:none"><xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=2" >
								<xsl:if test="$cur_pos = 1 or $target_name != ../../tr[$cur_pos - 1]/td[2]">
									<td align="left" rowspan="{$rowspan2}">									
										<xsl:value-of select="." />
									</td>
								</xsl:if>
								<xsl:if test="$target_name = ../../tr[$cur_pos - 1]/td[2] and $unit_name = ../../tr[$cur_pos - 1]/td[1]">
									<td align="left" style="display:none" >									
										<xsl:value-of select="." />  
									</td>
								</xsl:if>
								<xsl:if test="$target_name = ../../tr[$cur_pos - 1]/td[2] and $unit_name != ../../tr[$cur_pos - 1]/td[1]">
									<td align="left" rowspan="{$rowspan2}" >									
										<xsl:value-of select="." />  
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=3" >
								<xsl:if test="../td[11]!='nonono'">
									<td>
										<xsl:value-of select="." />
									</td>
								</xsl:if>
							</xsl:when>	
							<xsl:when test="position()=4" >
								<td align="center" style="width:140px;word-break;keep-all">
										<xsl:value-of select="." />
								</td>
							</xsl:when>			
							<xsl:when test="position()=5" >
								<td align="center" style="width:140px;word-break;keep-all">
										<xsl:value-of select="." />
								</td>
							</xsl:when>													
							<xsl:otherwise>
								<xsl:if test="position()!=11">
									<td>
										<xsl:value-of select="." />
									</td>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
