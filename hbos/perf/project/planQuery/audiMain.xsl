<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
			  <th>维度</th>
				<th>二级KPI指标</th>
				<xsl:if test = "/root/tbody/tr[1]/td[20]!='nonono'">
				<th>三级KPI指标</th>
				</xsl:if>
				<th>目标值</th>
				<th>计量单位</th>
				<th>采集部门</th>
				<th>评价标准</th>	
				<th>计算公式</th>								
			</tr>
		</thead>	
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			  <xsl:variable name="currPos" select="position()"/>
				<xsl:variable name="unit_name" select="td[1]" />
				<xsl:variable name="target_name" select="td[2]" />
				<xsl:variable name="rowspan1" select="count(/root/tbody/tr[td[1]=$unit_name])" />
				<xsl:variable name="rowspan2" select="count(/root/tbody/tr[td[2]=$target_name])"/>
				<tr>
					<xsl:if test="$currPos = 1 or $unit_name != ../tr[$currPos - 1]/td[1]">
						<td rowspan="{$rowspan1}">
							<xsl:value-of select="td[1]" />
						</td>
					</xsl:if>
					<xsl:if test="$currPos = 1 or $target_name != ../tr[$currPos - 1]/td[2]">
						<xsl:if test = "td[2]!=''">
									<td rowspan="{$rowspan2}">
										<xsl:value-of select="td[2]" />
									</td>
							</xsl:if> 
					</xsl:if>
					<xsl:if test = "td[2]=''">
							<td rowspan="1">
								<xsl:value-of select="td[2]" />
							</td>
					</xsl:if>
					<xsl:if test = "td[20]!='nonono'">
						<td noWrap='true'>
							<xsl:value-of select="td[3]" />
						</td>
					</xsl:if>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=4">
								<td noWrap='true'>
								  <xsl:value-of select="." />
								  
								   
								</td>							
							</xsl:when>		
							<xsl:when test="position()=7 "  >
								<td align="left" style="width:280px;word-break;keep-all;">
								<p>
								  
									<xsl:if test="../td[9] ='U'">										
										<xsl:value-of select="." disable-output-escaping="yes"/>
									</xsl:if>	
									<xsl:if test="../td[9] ='D'">										
										<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[18]"/></xsl:attribute>		
											<xsl:attribute name="appraise_method"><xsl:value-of select="../td[10]"/></xsl:attribute>	
											<xsl:attribute name="extremum"><xsl:value-of select="../td[11]"/></xsl:attribute>
											<xsl:attribute name="target_name"><xsl:value-of select="../td[19]"/></xsl:attribute>																																																																			
											<xsl:value-of select="." disable-output-escaping="yes"/>   
										</a>
								</xsl:if>	
								</p>
								</td>
							</xsl:when>	
								
							<xsl:when test="position()=8" >
								<td align="left" >										
										<a tabindex="-2" style="text-decoration:none" href="#" onclick="formula_win(this);" name="forumla_A">
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[18]"/></xsl:attribute>											
											<xsl:attribute name="gather_type"><xsl:value-of select="../td[12]"/></xsl:attribute>		
											<xsl:attribute name="expressions"><xsl:value-of select="../td[13]"/></xsl:attribute>	
											<xsl:attribute name="cal_method_type"><xsl:value-of select="../td[14]"/></xsl:attribute>	
											<xsl:attribute name="formula_chn"><xsl:value-of select="."/></xsl:attribute>																																																																																			
											<xsl:value-of select="." />  
										</a>
								</td>
							</xsl:when>								
							<xsl:when test="position()=1 or position()=2 or position()=3 or position()>=9" >
							</xsl:when>																				
							<xsl:otherwise>
								<td noWrap='true'>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
