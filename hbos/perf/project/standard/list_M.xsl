<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th >月份</th>
				<th nowrap='true' >下限2</th>
				<th nowrap='true' >下限1</th>
				<th nowrap='true' >最佳值</th>			
				<th nowrap='true' >上限1</th>		
				<th nowrap='true' >上限2</th>																
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=2" >
								<td align="center">									
									<input class="inputDecimal" name="col1" point="2" maxInput="11" type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="12">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="sequence_no"><xsl:value-of select="../td[7]"/></xsl:attribute>																				
									</input> 
									<input type="hidden" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="12">
										<xsl:attribute name="value"><xsl:value-of select="format-number(../td[3],'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="sequence_no"><xsl:value-of select="../td[7]"/></xsl:attribute>																				
									</input> 								
								</td>
							</xsl:when>
							<xsl:when test="position()=4  or position()=5  or position()=6" >
								<td align="center">									
									<input class="inputDecimal" name="col2" point="2" maxInput="11" type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="12">
										<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>	
										<xsl:attribute name="sequence_no"><xsl:value-of select="../td[7]"/></xsl:attribute>																				
									</input> 								
								</td>
							</xsl:when>	
							<xsl:when test="position()=3" >
								<td  align="center">
									<xsl:value-of select="format-number(.,'#,##0.00')" />
								</td>							
							</xsl:when>								
							<xsl:when test="position()=7 or position()=8" >
							</xsl:when>																								
							<xsl:otherwise>
								<td  align="center">
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
