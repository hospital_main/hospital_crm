<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th nowrap='true'>指标名称</th>
		  	<th nowrap='true'><input type='checkbox' style="display:none" />计量单位</th>
		  	<th nowrap='true'>是否有权限<span><input type='checkbox' id="read">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span></th>
		  	<!--<th nowrap='true'>写权限<span><input type='checkbox' id="write">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span></th>
		  	<th nowrap='true'>审核权限<span><input type='checkbox' id="check">
		  	<xsl:attribute name="onclick">checkBox_onclick(this);</xsl:attribute>
		  	</input></span></th>-->
      </tr>
    </thead>
    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <td align='center' style='display:none'>
            <div><input type='checkbox'>
	            <xsl:attribute name="id">id_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
        	    </xsl:attribute>
     			  </input></div>
     			</td>

          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position() = 3">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">read_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">readClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <!--<xsl:when test="position() = 4">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">write_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">writeClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 5">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">check_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">checkClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>-->
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
     </tbody>
  </xsl:template>
</xsl:stylesheet>