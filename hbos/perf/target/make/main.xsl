<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th><input type='checkbox'/></th>
				<th nowrap='true'>年度</th>
				<th nowrap='true'>核算月</th>
				<th nowrap='true'>目标名称</th>
				<th nowrap='true'>状态</th>    
				<th nowrap='true'>是否虚拟</th>
				<th nowrap='true'>复制战略</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
									<a tabindex='-1'>
										<xsl:attribute name="href" >
											javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:650px')
										</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
								</td>
							</xsl:when>
							<xsl:when test="position()=6">
								<td style="display:none"><xsl:value-of select="."/></td> 
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
									<a tabindex="-1">
										<xsl:attribute name="href">
											javascript:var a=openDialog('copy.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:420px;dialogHeight:240px')
										</xsl:attribute>
										<xsl:value-of select="'复制'"/>
									</a>
								</td>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>