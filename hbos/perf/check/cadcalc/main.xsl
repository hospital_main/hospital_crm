<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' width="100">科室分类</th>
				<th nowrap='true' width="80">科室</th>
				<th nowrap='true' width="80">干部</th>
				<th nowrap='true' width="130">月考核分数</th>
				<th nowrap='true' width="130">季考核分数</th>
				<th nowrap='true' width="130">半年考核分数</th>
				<th nowrap='true' width="130">年考核分数</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="unit_name" select="td[1]" />
				<xsl:variable name="dept_name" select="td[2]" />
				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$unit_name])" />
				<xsl:variable name="rowspan2" select="count(/root/tbody/tr[td[2]=$dept_name])" />
				<xsl:variable name="cur_pos" select="position()" />
				<tr>
					<xsl:if test="$cur_pos = 1 or $unit_name != ../tr[$cur_pos - 1]/td[1]">
						<td align="center" rowspan="{$rowspan}">
							<xsl:value-of select="td[1]" />
						</td>
					</xsl:if>
					<xsl:if test="$cur_pos = 1 or $dept_name!=../tr[$cur_pos -1 ]/td[2]">
						<td align="center" rowspan="{$rowspan2}">
							<xsl:value-of select="td[2]"/>
						</td>
					</xsl:if>
					<td align="center"  >
						<xsl:value-of select="td[3]"/>
					</td>
					<td align="center" >
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="M"  href="#" onclick="win_Open(this);">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[9]"/></xsl:attribute>
							<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="emp_name"><xsl:value-of select="td[3]"/></xsl:attribute>
							<xsl:attribute name="dept_code"><xsl:value-of select="td[10]"/></xsl:attribute>
							<xsl:attribute name="emp_code"><xsl:value-of select="td[11]"/></xsl:attribute>
							<xsl:value-of select="td[4]" />  
						</a>
					</td>
					<td align="center">
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="Q"  href="#" onclick="win_Open(this);">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[9]"/></xsl:attribute>
							<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="emp_name"><xsl:value-of select="td[3]"/></xsl:attribute>
							<xsl:attribute name="dept_code"><xsl:value-of select="td[10]"/></xsl:attribute>
							<xsl:attribute name="emp_code"><xsl:value-of select="td[11]"/></xsl:attribute>
							<xsl:value-of select="td[5]" />  
						</a>
					</td>
					<td align="center">
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="H"  href="#" onclick="win_Open(this);">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[9]"/></xsl:attribute>
							<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="emp_name"><xsl:value-of select="td[3]"/></xsl:attribute>
							<xsl:attribute name="dept_code"><xsl:value-of select="td[10]"/></xsl:attribute>
							<xsl:attribute name="emp_code"><xsl:value-of select="td[11]"/></xsl:attribute>
							<xsl:value-of select="td[6]" />  
						</a>
					</td>	
					<td align="center">
						<a tabindex="-2" style="text-decoration:none" bulletin_frequency="Y"  href="#" onclick="win_Open(this);">		
							<xsl:attribute name="sequence_no"><xsl:value-of select="td[9]"/></xsl:attribute>
							<xsl:attribute name="dept_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="emp_name"><xsl:value-of select="td[3]"/></xsl:attribute>
							<xsl:attribute name="dept_code"><xsl:value-of select="td[10]"/></xsl:attribute>
							<xsl:attribute name="emp_code"><xsl:value-of select="td[11]"/></xsl:attribute>
							<xsl:value-of select="td[7]" />  
						</a>
					</td>															
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
