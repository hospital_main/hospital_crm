<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			  <th noWrap='true'>维度</th>
			  <th noWrap='true'>分数</th>
			  <th noWrap='true'>指标名称</th>
			  <th noWrap='true'>指标分数</th>
			  <th noWrap='true'>指标状态</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
 				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]!='']) - 1" />
	      <xsl:if test="position()=2 and td[1]!=''">
  		    <tr>
  		      <td rowspan="{$rowspan}">
  		        <xsl:value-of select="../tr[1]/td[1]" />
  		      </td>
  		      <td rowspan="{$rowspan}" align='right'>
  		        <xsl:value-of select="format-number(../tr[1]/td[2],'#,#00.00')" />
  		      </td>
            <td>
								<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
									<xsl:attribute name="sequence_no">
										<xsl:value-of select="td[3]" />
									</xsl:attribute>
									<xsl:attribute name="appraise_method">
										<xsl:value-of select="td[6]" />
									</xsl:attribute>
									<xsl:attribute name="score">
										<xsl:value-of select="td[2]" />
									</xsl:attribute>
									<xsl:value-of select="td[1]" />
								</a>
            </td>
            <td  align='right'>
              <xsl:value-of select="format-number(td[2],'#,#00.00')"/>              
            </td>
            <td align="center">
							<img>
								<xsl:attribute name="SRC">
									<xsl:value-of select="td[4]" />
								</xsl:attribute>
							</img>
            </td>
          </tr>
        </xsl:if>
	      <xsl:if test="position()&gt;2 and td[1]!=''">
  		    <tr>
            <td>
								<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
									<xsl:attribute name="sequence_no">
										<xsl:value-of select="td[3]" />
									</xsl:attribute>
									<xsl:attribute name="appraise_method">
										<xsl:value-of select="td[6]" />
									</xsl:attribute>
									<xsl:attribute name="score">
										<xsl:value-of select="td[2]" />
									</xsl:attribute>
									<xsl:value-of select="td[1]" />
								</a>
            </td>
            <td  align='right'>
              <xsl:value-of select="format-number(td[2],'#,#00.00')"/>              
            </td>
            <td align="center">
							<img>
								<xsl:attribute name="SRC">
									<xsl:value-of select="td[4]" />
								</xsl:attribute>
							</img>
            </td>
          </tr>
        </xsl:if>
			</xsl:for-each>

			<xsl:for-each select="/root/tbody/tr">
 				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[7]!='']) - 1" />
	      <xsl:if test="position()=2 and td[7]!=''">
  		    <tr>
  		      <td rowspan="{$rowspan}">
  		        <xsl:value-of select="../tr[1]/td[7]" />
  		      </td>
  		      <td rowspan="{$rowspan}" align="right">
  		        <xsl:value-of select="format-number(../tr[1]/td[8],'#,#00.00')" />
  		      </td>
            <td>
								<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
									<xsl:attribute name="sequence_no">
										<xsl:value-of select="td[9]" />
									</xsl:attribute>
									<xsl:attribute name="appraise_method">
										<xsl:value-of select="td[12]" />
									</xsl:attribute>
									<xsl:attribute name="score">
										<xsl:value-of select="td[8]" />
									</xsl:attribute>
									
									<xsl:value-of select="td[7]" />
								</a>
            </td>
            <td align="right">
              <xsl:value-of select="format-number(td[8],'#,#00.00')"/>              
            </td>
            <td align="center">
							<img>
								<xsl:attribute name="SRC">
									<xsl:value-of select="td[10]" />
								</xsl:attribute>
							</img>
            </td>
          </tr>
        </xsl:if>
	      <xsl:if test="position()&gt;2 and td[7]!=''">
  		    <tr>
            <td>
								<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
									<xsl:attribute name="sequence_no">
										<xsl:value-of select="td[9]" />
									</xsl:attribute>
									<xsl:attribute name="appraise_method">
										<xsl:value-of select="td[12]" />
									</xsl:attribute>
									<xsl:attribute name="score">
										<xsl:value-of select="td[8]" />
									</xsl:attribute>
									<xsl:value-of select="td[7]" />
								</a>
            </td>
            <td align="right">
              <xsl:value-of select="format-number(td[8],'#,#00.00')"/>              
            </td>
            <td align="center">
							<img>
								<xsl:attribute name="SRC">
									<xsl:value-of select="td[10]" />
								</xsl:attribute>
							</img>
            </td>
          </tr>
        </xsl:if>
			</xsl:for-each>

			<xsl:for-each select="/root/tbody/tr">
 				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[13]!='']) - 1" />
	      <xsl:if test="position()=2 and td[13]!=''">
  		    <tr>
  		      <td rowspan="{$rowspan}">
  		        <xsl:value-of select="../tr[1]/td[13]" />
  		      </td>
  		      <td rowspan="{$rowspan}" align="right">
  		        <xsl:value-of select="format-number(../tr[1]/td[14],'#,#00.00')" />
  		      </td>
            <td>
								<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
									<xsl:attribute name="sequence_no">
										<xsl:value-of select="td[15]" />
									</xsl:attribute>
									<xsl:attribute name="appraise_method">
										<xsl:value-of select="td[18]" />
									</xsl:attribute>
									<xsl:attribute name="score">
										<xsl:value-of select="td[14]" />
									</xsl:attribute>
									<xsl:value-of select="td[13]" />
								</a>
            </td>
            <td align="right">
              <xsl:value-of select="format-number(td[14],'#,#00.00')"/>              
            </td>
            <td align="center">
							<img>
								<xsl:attribute name="SRC">
									<xsl:value-of select="td[16]" />
								</xsl:attribute>
							</img>
            </td>
          </tr>
        </xsl:if>
	      <xsl:if test="position()&gt;2 and td[13]!=''">
  		    <tr>
            <td>
								<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
									<xsl:attribute name="sequence_no">
										<xsl:value-of select="td[15]" />
									</xsl:attribute>
									<xsl:attribute name="appraise_method">
										<xsl:value-of select="td[18]" />
									</xsl:attribute>
									<xsl:attribute name="score">
										<xsl:value-of select="td[14]" />
									</xsl:attribute>
									<xsl:value-of select="td[13]" />
								</a>
            </td>
            <td align="right">
              <xsl:value-of select="format-number(td[14],'#,#00.00')"/>              
            </td>
            <td align="center">
							<img>
								<xsl:attribute name="SRC">
									<xsl:value-of select="td[16]" />
								</xsl:attribute>
							</img>
            </td>
          </tr>
        </xsl:if>
			</xsl:for-each>

			<xsl:for-each select="/root/tbody/tr">
 				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[19]!='']) - 1" />
	      <xsl:if test="position()=2 and td[19]!=''">
  		    <tr>
  		      <td rowspan="{$rowspan}">
  		        <xsl:value-of select="../tr[1]/td[19]" />
  		      </td>
  		      <td rowspan="{$rowspan}" align="right">
  		        <xsl:value-of select="format-number(../tr[1]/td[20],'#,#00.00')" />
  		      </td>
            <td>
								<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
									<xsl:attribute name="sequence_no">
										<xsl:value-of select="td[21]" />
									</xsl:attribute>
									<xsl:attribute name="appraise_method">
										<xsl:value-of select="td[24]" />
									</xsl:attribute>
									<xsl:attribute name="score">
										<xsl:value-of select="td[20]" />
									</xsl:attribute>
									<xsl:value-of select="td[19]" />
								</a>
            </td>
            <td align="right">
              <xsl:value-of select="format-number(td[20],'#,#00.00')"/>              
            </td>
            <td align="center">
							<img>
								<xsl:attribute name="SRC">
									<xsl:value-of select="td[22]" />
								</xsl:attribute>
							</img>
            </td>
          </tr>
        </xsl:if>
	      <xsl:if test="position()&gt;2 and td[19]!=''">
  		    <tr>
            <td>
								<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
									<xsl:attribute name="sequence_no">
										<xsl:value-of select="td[21]" />
									</xsl:attribute>
									<xsl:attribute name="appraise_method">
										<xsl:value-of select="td[24]" />
									</xsl:attribute>
									<xsl:attribute name="score">
										<xsl:value-of select="td[20]" />
									</xsl:attribute>
									<xsl:value-of select="td[19]" />
								</a>
            </td>
            <td align="right">
              <xsl:value-of select="format-number(td[20],'#,##0.00')"/>              
            </td>
            <td align="center">
							<img>
								<xsl:attribute name="SRC">
									<xsl:value-of select="td[22]" />
								</xsl:attribute>
							</img>
            </td>
          </tr>
        </xsl:if>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
