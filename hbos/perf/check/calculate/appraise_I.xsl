<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="colCount" select="count(/root/tbody/tr[1]/td)" />
			<xsl:if test="$colCount = 5">
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="/root/tbody/tr">
						<th nowrap='true'>
							<xsl:value-of select="td[2]" />
						</th>
					</xsl:for-each>
				</tr>
			</xsl:if>
			<xsl:if test="$colCount = 10">
				<tr noWrap='true' class='mainHead'>
					<th nowrap='true' width="100">基本指标</th>
					<th nowrap='true' width="130">权重</th>
					<th nowrap='true' width="80">目标值</th>
					<th nowrap='true' width="130">计算公式</th>
					<th nowrap='true' width="130">指标值</th>
					<th nowrap='true' width="130">记分方法</th>
					<th nowrap='true' width="130">实际分数</th>
					<th nowrap='true' width="130">数据来源</th>
					<th nowrap='true' width="130">收集部门</th>
					<th nowrap='true' width="130">报告频率</th>
				</tr>
			</xsl:if>
		</thead>
		<tbody>
			<xsl:if test="$colCount = 5">
				<tr>
					<xsl:for-each select="/root/tbody/tr">
						<td>
							<xsl:if test="td[4] = 'func'">
								<xsl:if test="td[5] = 1">
									<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
									 <xsl:attribute name="factor_code">
								     <xsl:value-of select="td[1]" />
							     </xsl:attribute>
									 <xsl:attribute name="incomeMoney">
								     <xsl:value-of select="format-number(td[3],'0.00')" />
							     </xsl:attribute>
								  <xsl:value-of select="format-number(td[3],'#,##0.00')" />
										</a>
								</xsl:if>	
								<xsl:if test="td[5] = 0">
									<xsl:value-of select="format-number(td[3],'#,##0.00')" />
								</xsl:if>									
							</xsl:if>
							<xsl:if test="td[4] != 'func'">
								<xsl:value-of select="format-number(td[3],'#,##0.00')" />
							</xsl:if>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:if>
			<xsl:if test="$colCount = 10">
				<xsl:for-each select="/root/tbody/tr">
					<tr>					
							<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=2">
										<td noWrap='true' align='right'>
											<xsl:value-of select="format-number(.,'#,##0.00%')"/>
										</td>
									</xsl:when>
									<xsl:when test="position()=3 or position()=5">
										<td noWrap='true' align='right'>
											<xsl:value-of select="."/>
										</td>
									</xsl:when>
									<xsl:when test="position()=7">
									<td align="right">
										<xsl:value-of select="." disable-output-escaping="yes"/>
									</td>
								</xsl:when>
									<xsl:otherwise>
										<td noWrap='true' align='left'>
											<xsl:value-of select="." />
										</td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
					</tr>			
				</xsl:for-each>
			</xsl:if>
		</tbody>
	</xsl:template>
</xsl:stylesheet>