<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead_acc'>
				<th nowrap="true">科室分类</th>
				<th nowrap="true">科室</th>
				<th nowrap="true">干部</th>
				<th nowrap="true">计量单位</th>
				<th nowrap="true">指标值</th>
      </tr>
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="perf_unit_name" select="td[1]"/>
				<xsl:variable name="dept_name" select="td[2]"/>
				<xsl:variable name="rowspan" select="td[8]"/>
				<xsl:variable name="rowspan2" select="td[9]"/>
				<xsl:variable name="cur_pos" select="position()"/>
				<tr>
					<xsl:if test="$cur_pos = 1 or $perf_unit_name != ../tr[$cur_pos - 1]/td[1]">
						<td align="left" rowspan="{$rowspan}">
							<xsl:value-of select="td[1]"/>
						</td>
					</xsl:if>
					<xsl:if test="$perf_unit_name = ../tr[$cur_pos - 1]/td[1]">
						<td style="display:none"> 
						</td>
					</xsl:if>
					<xsl:if test="$cur_pos = 1 or $dept_name != ../tr[$cur_pos - 1]/td[2]">
						<td align="left" rowspan="{$rowspan2}">
							<xsl:value-of select="td[2]"/>
						</td>
					</xsl:if>
					<xsl:if test="$dept_name = ../tr[$cur_pos - 1]/td[2]">
						<td style="display:none"> 
						</td>
					</xsl:if>
					<td align="left">
						<xsl:value-of select="td[3]"/>
					</td>
					<td align="left">
						<xsl:value-of select="td[4]"/>
					</td>
					
					<td align="left">
						<input type="text" maxlength="12" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="12">
						  <xsl:attribute name="sequence_no"><xsl:value-of select="td[6]"/></xsl:attribute>
						  <xsl:attribute name="emp_code"><xsl:value-of select="td[7]"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="format-number(td[5],'###0.00####')" /></xsl:attribute>
							<xsl:attribute name="name">factor_value</xsl:attribute>
							<xsl:attribute name="TABINDEX"><xsl:value-of select="$cur_pos"/></xsl:attribute>
						</input>
					</td>
					
				</tr>
			</xsl:for-each>
     </tbody>
  </xsl:template>
</xsl:stylesheet>