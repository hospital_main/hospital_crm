<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
				<th nowrap="true">基本指标代码</th>
				<th nowrap="true">基本指标名称</th>
				<th nowrap="true">说明</th>
				<th nowrap="true">性质</th>
				<th nowrap="true">状态</th>
				<th nowrap="true">对应函数名称</th>
				<th nowrap="true">计算公式</th>
      </tr>
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td[position() &lt; 8]">
				  	<xsl:choose>
				  		<xsl:when test="position()=2" >
				  		  <xsl:variable name="factor_code" select="../td[1]"/>
				  			<xsl:variable name="factor_peropty" select="../td[4]"/>
				  			<xsl:variable name="state_flag" select="../td[5]"/>
				  			<xsl:variable name="sequence_no" select="../td[8]"/>
								<td noWrap='true'>
									 <a tabindex='-1' href='#'>
		                <xsl:if test = '$factor_peropty = &quot;手工录入&quot; or $factor_peropty = &quot;数据导入&quot; '>
			                <xsl:attribute name="onclick" >
			                  viewDialog(<xsl:value-of select="$sequence_no"/>,0);
			                </xsl:attribute>
		                </xsl:if>
		                <xsl:if test = '$factor_peropty = &quot;计算公式&quot; or $factor_peropty = &quot;加权计算&quot; '>
			                <xsl:attribute name="onclick" >
			                   viewDialog(<xsl:value-of select="$sequence_no"/>,1);
			                </xsl:attribute>
		                </xsl:if>
		                <xsl:value-of select="."/>
		               </a>
		               <xsl:if test = '$state_flag != &quot;审核通过&quot;'> 
		               		<input type="hidden" name="sequence_no">
		               		  <xsl:if test = '$factor_peropty = &quot;手工录入&quot; '>
					                <xsl:attribute name="is_cal" >0</xsl:attribute>
					                <xsl:attribute name="factor_code" ><xsl:value-of select="$factor_code"/></xsl:attribute>
				                </xsl:if>
				                <xsl:if test = '$factor_peropty = &quot;计算公式&quot; or $factor_peropty = &quot;加权计算&quot; or $factor_peropty = &quot;数据导入&quot;'>
					                <xsl:attribute name="is_cal" >1</xsl:attribute>
					                <xsl:attribute name="factor_code" ><xsl:value-of select="$factor_code"/></xsl:attribute>
				                </xsl:if>
		               			<xsl:attribute name="value" ><xsl:value-of select="$sequence_no"/></xsl:attribute>
		               		</input>
		               </xsl:if>
								</td>
							</xsl:when>
					  	<xsl:otherwise>
	            <td  noWrap='true' >
	              <xsl:value-of select="."/>
	            </td>
	            </xsl:otherwise>
	          </xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
     </tbody>
  </xsl:template>
</xsl:stylesheet>