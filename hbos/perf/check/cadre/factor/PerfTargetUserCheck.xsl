<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
				<th nowrap="true">KPI指标</th>
				<th nowrap="true">评测目的</th>
				<th nowrap="true">考核频率</th>
				<th nowrap="true">采集方式</th>
				<th nowrap="true">计量单位</th>
				<th nowrap="true">状态</th>
				<th nowrap="true">计算公式</th>
      </tr>
    </thead>
    <tbody>
    	<xsl:for-each select="/root/tbody/tr">
 				<xsl:variable name="gather_type" select="td[4]"/>
				<xsl:variable name="state_flag" select="td[6]"/>
				<xsl:variable name="sequence_no" select="td[8]"/>
				<xsl:variable name="is_last" select="td[9]"/>
				<xsl:variable name="name_space" select="td[10]"/>
				<xsl:variable name="target_code" select="td[11]"/>
				<xsl:variable name="cal_method_type" select="td[12]"/>
				<tr>
				  <xsl:for-each select="td[position() &lt; 8]">
				  	<xsl:choose>
				  		<xsl:when test="position()=1" >
								<td noWrap='true'>
									 <xsl:if test = '$is_last = 1'>
									   <xsl:value-of select="$name_space"/>
										 <a tabindex='-1' href='#'>
			                <xsl:if test = '$gather_type = &quot;录入&quot; '>
				                <xsl:attribute name="onclick" >viewDialog(<xsl:value-of select="$sequence_no"/>,&quot;<xsl:value-of select="."/>&quot;,&quot;<xsl:value-of select="$target_code"/>&quot;,&quot;<xsl:value-of select="$cal_method_type"/>&quot;,0);</xsl:attribute>
			                </xsl:if>
			                <xsl:if test = '$gather_type = &quot;计算&quot; '>
				                <xsl:attribute name="onclick" >viewDialog(<xsl:value-of select="$sequence_no"/>,&quot;<xsl:value-of select="."/>&quot;,&quot;<xsl:value-of select="$target_code"/>&quot;,&quot;<xsl:value-of select="$cal_method_type"/>&quot;,1);</xsl:attribute>
			                </xsl:if>
			                <xsl:if test = '$gather_type = &quot;加权计算&quot; '>
				                <xsl:attribute name="onclick" >viewDialog(<xsl:value-of select="$sequence_no"/>,&quot;<xsl:value-of select="."/>&quot;,&quot;<xsl:value-of select="$target_code"/>&quot;,&quot;<xsl:value-of select="$cal_method_type"/>&quot;,2);</xsl:attribute>
			                </xsl:if>
			                <xsl:value-of select="."/>
			               </a>
			             </xsl:if>
			             <xsl:if test = '$is_last = 0'>
			              <xsl:value-of select="$name_space"/>
			             	<xsl:value-of select="."/>
			             </xsl:if>
		               <xsl:if test = '$state_flag != &quot;审核通过&quot; and $is_last = 1'>
		               		<input type="hidden" name="sequence_no">
		               			<xsl:attribute name="value" ><xsl:value-of select="$sequence_no"/></xsl:attribute>
		               		</input>
		               </xsl:if>
								</td>
							</xsl:when>
					  	<xsl:otherwise>
					  	<xsl:if test = '$is_last = 0'>
					  		<td  noWrap='true' >
	              	
	            	</td>
					  	</xsl:if>
					  	<xsl:if test = '$is_last = 1'>
		            <td  noWrap='true' >
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:if>
	            </xsl:otherwise>
	          </xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
     </tbody>
  </xsl:template>
</xsl:stylesheet>