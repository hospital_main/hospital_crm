<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
				<th nowrap="true">序号</th>
				<th nowrap="true">基本指标代码</th>
				<th nowrap="true">基本指标名称</th>
				<th nowrap="true">权重</th>
				<th nowrap="true">计量单位</th>
				<th nowrap="true">目标值</th>
				<th nowrap="true">指标值</th>
				<th nowrap="true">计算方法</th>
				<th nowrap="true">数据来源</th>
				<th nowrap="true">收集部门</th>
				<th nowrap="true">报告频率</th>
      </tr>
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
    	  <xsl:variable name="cur_pos" select="position()"/>
				<tr>
				  <td noWrap='true'>
						<xsl:value-of select="$cur_pos"/>
					</td>
				  <xsl:for-each select="td">
				  	<td  noWrap='true' >
		          <xsl:value-of select="."/>
            </td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
     </tbody>
  </xsl:template>
</xsl:stylesheet>