<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th nowrap="true">序号</th>
				<th nowrap="true">基本指标代码</th>
				<th nowrap="true">基本指标名称</th>
				<th nowrap="true">权重</th>
				<th nowrap="true">计量单位</th>
				<th nowrap="true">目标值</th>
				<th nowrap="true">计算方法</th>
				<th nowrap="true">指标值</th>
				<th nowrap="true">计分方法</th>
				<th nowrap="true">实际得分</th>
				<th nowrap="true">数据来源</th>
				<th nowrap="true">收集部门</th>
				<th nowrap="true">报告频率</th>
			</tr>
		</thead>
		<tbody>
			<xsl:variable name="totalVal" select="sum(/root/tbody/tr/td[16])"/>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="cur_pos" select="position()"/>
				<tr>
					<td noWrap="true">
						<xsl:value-of select="$cur_pos"/>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=3">
								<td align="right">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()=5">
								<td align="right">
									<xsl:choose>
										<xsl:when test="../td[13] = 'C'">											
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="."/>
										</xsl:otherwise>
									</xsl:choose>									
								</td>
							</xsl:when>
							<xsl:when test="position()=8">
								<td>
									<xsl:value-of select="." disable-output-escaping="yes"/>
								</td>
							</xsl:when>
							<xsl:when test="position()=7 or position()=9">
								<td>
									<input type="text" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="8">
										<xsl:attribute name="sequence_no"/>
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
										<xsl:attribute name="name">real_value</xsl:attribute>
									</input>
								</td>
							</xsl:when>
							<xsl:when test="position() >= 13">
								<td style="display:none">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			<tr>
				<td align="center" colspan="3">综合得分</td>
				<td/>
				<td/>
				<td/>
				<td/>
				<td/>
				<td/>
				<td class="numberText">
					<xsl:value-of select="format-number($totalVal,'#,##0.00')"/>
				</td>
				<td/>
				<td/>
				<td/>
			</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>