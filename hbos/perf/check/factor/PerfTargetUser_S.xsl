<?xml version="1.0" encoding="gb2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="DeptCode" select="/root/tbody/tr[1]/td[2]"/>
			<xsl:variable name="TotalCol" select="count(/root/tbody/tr[td=$DeptCode])"/>
			<tr noWrap="true" class="mainHead">
				<th nowrap="true" valign="middle">科室分类</th>
				<th nowrap="true" valign="middle">科室</th>
				<th nowrap="true" valign="middle">计量单位</th>
				<xsl:for-each select="/root/tbody/tr">
				  <xsl:if test="position() &lt;= $TotalCol">
						<th nowrap="true">
							<xsl:value-of select="td[4]"/>
						</th>
					</xsl:if>
				</xsl:for-each>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="Step" select="position() mod $TotalCol"/>
				<xsl:if test="$Step = 1  or $TotalCol = 1">
				  <xsl:variable name="UnitCode" select="td[1]"/>
				  <xsl:variable name="cur_pos" select="position()"/>
				  <xsl:if test="$cur_pos = 1 or $UnitCode != ../tr[$cur_pos - 1]/td[1]">
				    <xsl:variable name="TotalRow" select="count(/root/tbody/tr[td=$UnitCode])"/>
				    <xsl:variable name="rowspan" select="$TotalRow div $TotalCol"/>
					  <xsl:text disable-output-escaping="yes"><![CDATA[<tr>]]></xsl:text>
						<td noWrap="true" align="center" rowspan="{$rowspan}">
							<xsl:value-of select="td[1]"/>
						</td>
					</xsl:if>
					<xsl:text disable-output-escaping="yes"><![CDATA[<td noWrap="true">]]></xsl:text>
					<xsl:value-of select="td[2]"/>
					<xsl:text disable-output-escaping="yes"><![CDATA[</td><td noWrap="true">]]></xsl:text>
					<xsl:value-of select="td[3]"/>
					<xsl:text disable-output-escaping="yes"><![CDATA[</td>]]></xsl:text>
				</xsl:if>
				<td align="right">
					<xsl:if test="$Step = 0  or $TotalCol = 1">
						<input type="hidden" TABINDEX="-1" style="font-size:12px;border:1px solid gray;text-align:right;" size="12">
						  <xsl:attribute name="sequence_no"><xsl:value-of select="td[6]"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="format-number(td[5],'###0.00####')" /></xsl:attribute>
							<xsl:attribute name="name">actual_value</xsl:attribute>
						</input>
					</xsl:if>
					<xsl:value-of select="format-number(td[5],'###0.00####')" />
        </td>
				<xsl:if test="$Step = 0  or $TotalCol = 1">
					<xsl:text disable-output-escaping="yes"><![CDATA[</tr>]]></xsl:text>
				</xsl:if>
			</xsl:for-each> 
		</tbody>
	</xsl:template>
</xsl:stylesheet>
	