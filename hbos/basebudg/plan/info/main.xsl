<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/basebudg/plan/info/main.xsl,v 1.1 2012/03/12 01:46:03 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:46:03 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th>�ĵ�����</th>
				<th>�ĵ�����</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td ><a tabindex='-1'><xsl:value-of select="."/></a></td>
                </xsl:when>
                <xsl:when test="position()=2">
			            <td>                  
	                  <a tabindex="-1" href="#">
	                    <xsl:attribute name="onclick" >
	                       javascript:window.open('/upload/<xsl:value-of select="../td[3]"/>','','width=800, height=600, top='+(screen.availHeight-600)/2+', left='+(screen.availWidth-800)/2+',  scrollbars=1, resizable=1')
	                    </xsl:attribute>
	                    <xsl:value-of select="."/>
	                  </a>
	                </td> 
                </xsl:when>
                <xsl:when test="position()=3">
                  <td style="display:none"><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:otherwise>
                  <td ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
