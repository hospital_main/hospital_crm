<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th nowrap='true'>参数名称</th>
		  	<th nowrap='true'>参数标识</th>
		  	<th nowrap='true'>数据类型</th>
		  	<th nowrap='true'>初始值</th>
		  	<th nowrap='true'>触发标记</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
              	<td nowrap="true">
              		<xsl:attribute name="value" >
                     <xsl:for-each select="../*">
                      <xsl:choose>
                        <xsl:when test="position()=6 ">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:when>
                      </xsl:choose>
                    </xsl:for-each>
            		  </xsl:attribute>
              		<xsl:value-of select="."/>
              	</td>
              </xsl:when>  
              <xsl:when test="position()=6">
              </xsl:when> 
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise> 
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

