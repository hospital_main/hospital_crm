<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
		  	<th nowrap='true'>����</th>
		  	<th nowrap='true'>����</th>
		  	<th nowrap='true'>�ĵ�</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' >
	                  <a tabindex='-1'><xsl:value-of select="."/></a>
			            </td>
                </xsl:when>
                <xsl:when test="position()=2"><td>
                	<a>
                  <xsl:attribute name="href" >
    	            	javascript:changeCalc_ident(<xsl:value-of select="../td[3]"/>);
  	          		</xsl:attribute>
  	          		<xsl:value-of select="."/>
  	          		</a>
  	          		</td>
                </xsl:when>
                <xsl:when test="position()=3 ">
                </xsl:when>
                <xsl:when test="position()=4">
			            <td  noWrap='true' >
	                  <a target="_new" href="d">
	                  <xsl:attribute name="onclick">
	                    javascript:window.open('/upload/<xsl:value-of select="../td[5]"/>')
	                  </xsl:attribute><xsl:value-of select="."/></a>
			            </td>
                </xsl:when>
                <xsl:when test="position()=5">
			            <td style="display:none">
                  	<xsl:value-of select="."/>
			            </td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
            
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

