<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
					<td>卡片编码</td>
					<td>资产名称</td>
					<td>规格</td>
					<td>型号</td>
					<td>责任科室</td>
					<td>合同号</td>
					<td>业务类型</td>
					<td>租借单位</td>
					<td>开始日期</td> 
					<td>结束日期</td> 
					<td>资产原值</td> 
					<td>摊销年限</td> 
					<td>摊销金额</td> 
					<td>净值</td> 
	  		</tr>
	  	</thead>
	  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=11 ">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             	</xsl:when>
             	<xsl:when test="position()=13 ">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             	</xsl:when>
             	<!--xsl:when test="position()=14 ">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             	</xsl:when-->
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>