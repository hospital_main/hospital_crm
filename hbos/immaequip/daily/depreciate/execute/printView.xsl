<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td colspan='{$colNum}' style='fontsize:maintitle;'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum - 1"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap='true' class='mainHead'>
	        <td nowrap='true'>提取年月</td>
					<td nowrap='true'>所属部门</td>
					<td nowrap='true'>资产卡片号</td>
					<td nowrap='true'>资产编码</td>
					<td nowrap='true'>资产名称</td>
					<td nowrap='true'>型号规格</td>
					<td nowrap='true'>入库日期</td>
					<td nowrap='true'>资产原值</td>
					<td nowrap='true'>本月计提</td>
					<td nowrap='true'>累计计提</td>
					<td nowrap='true'>资金来源</td>
					<td nowrap='true'>累计折旧调整</td>
				</tr>
  		</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">            
	            <xsl:choose>
	              <xsl:when test="position()=1">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>  
	              <xsl:when test="position() = 9 ">
	                <td class='numberText' nowrap='true' align='right'>
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	      		      </td>
	              </xsl:when>
	              <xsl:when test="position() = 8 or position() = 10 or position() = 13">
	                <td align='right' class='numberText' nowrap='true'>
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>       		 	
	      		      </td>
	              </xsl:when>
	              
	              <xsl:when test="position() = 12">
	              </xsl:when>
	              <xsl:otherwise>
	                <td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          </xsl:choose>          
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	   		<!--tr>
	     		<td>合计</td>
	     		<td></td>
	     		<td></td>
	     		<td></td>
	     		<td></td>
	     		<td></td>
	     		<td></td>
	     		<td class='numberText' noWrap='true'>
	     		  <xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/>
	     		</td>
	     		<td class='numberText' noWrap='true'>
	     		  <xsl:value-of select="format-number(sum(/root/tbody/tr/td[9]),'#,##0.00')"/>
	     		</td>
	     		<td class='numberText' noWrap='true'>
	     		  <xsl:value-of select="format-number(sum(/root/tbody/tr/td[10]),'#,##0.00')"/>
	     		</td>
	   		</tr-->
	 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>