<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th nowrap='true'>分摊年月</th>
				<th nowrap='true'>所在部门</th>
				<th nowrap='true'>资产卡片号</th>
				<th nowrap='true'>资产编码</th>
				<th nowrap='true'>资产名称</th>
				<th nowrap='true'>附件编码</th>
				<th nowrap='true'>附件名称</th>
				<th nowrap='true'>型号规格</th>
				<th nowrap='true'>开始分摊年月</th>
				<th nowrap='true'>设备原值</th>
				<th nowrap='true'>设备单价/附件金额</th>
				<th nowrap='true'>本月分摊</th>
				<th nowrap='true'>累计分摊</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><xsl:value-of select="."/></td>
              </xsl:when>  
              <xsl:when test="position() = 12 ">
                <td class='numberText' nowrap='true'>
                  <a href='#'>
  									<xsl:attribute name="onclick">
  									    depreDetail(this);
  									</xsl:attribute>
                    <xsl:value-of select="format-number(.,'#,##0.00')"/> 
                  </a>      		 	
      		      </td>
              </xsl:when>
              <xsl:when test="position() = 10 or position() = 11 or position() = 13">
                <td class='numberText' nowrap='true'>
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>       		 	
      		      </td>
              </xsl:when>
              <xsl:when test="position()=14 or position()=15 ">
                <td style="display:none">
                  <xsl:value-of select="."/>       		 	
      		      </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<tr>
     		<td>合计</td>
     		<td></td>
     		<td></td>
     		<td></td>
     		<td></td>
     		<td></td>
     		<td></td>
     		<td></td>
     		<td></td>
     		<td></td>
     		<td class='numberText' noWrap='true'>
     		  <xsl:value-of select="format-number(sum(/root/tbody/tr/td[11]),'#,##0.00')"/>
     		</td>
     		<td class='numberText' noWrap='true'>
     		  <xsl:value-of select="format-number(sum(/root/tbody/tr/td[12]),'#,##0.00')"/>
     		</td>
     		<td class='numberText' noWrap='true'>
     		  <xsl:value-of select="format-number(sum(/root/tbody/tr/td[13]),'#,##0.00')"/>
     		</td>
     		<td style="display:none"></td>
     		<td style="display:none"></td>
   		</tr>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

