<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>选择</th>
      	<th nowrap='true'>保养项目编号</th>
      	<th nowrap='true'>保养项目名称</th>
      	<th nowrap='true'>保养说明</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <xsl:if test="td[4] = '0' ">
              <input type='checkbox' style='font-size:8px;' ></input>
            </xsl:if>
            <xsl:if test="td[4] = '1' ">
              <input type='checkbox' style='font-size:8px;' checked='true'></input>
            </xsl:if>
          </td>
          <xsl:for-each select="td[position() &lt; 4]">
            <td>
              <xsl:value-of select="."/>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

