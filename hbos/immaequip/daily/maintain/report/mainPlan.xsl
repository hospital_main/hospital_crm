<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/immaequip/daily/maintain/report/mainPlan.xsl,v 1.1 2012/03/12 01:54:09 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:54:09 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th>计划编号</th>
				<th>计划名称</th>
				<th>保养设备</th>
				<th>保养周期</th>
				<th>周期单位</th>				
				<th>计划开始日期</th>
				<th>最近保养日期</th>
				<th>下次保养日期</th>
				<th>计划结束日期</th>
				<!--
				<th>审核人</th>
				<th>审核日期</th>
				-->
				<th>状态</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <td >
              <xsl:choose>
                <xsl:when test="position()=1">
                  <a href='#' onclick="openPlanDetail(this);"><xsl:value-of select="."/></a>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
