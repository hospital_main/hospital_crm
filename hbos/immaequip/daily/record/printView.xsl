<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:12'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>
	  		<tr noWrap='true' class='mainHead'>	        
	        <td nowrap='true'>维护记录号</td>
					<td nowrap='true'>资产卡片号</td>
					<td nowrap='true'>资产名称</td>
					<td nowrap='true'>规格</td>
					<td nowrap='true'>型号</td>
					<td nowrap='true'>制单日期</td>
					<td nowrap='true'>维护工时</td>
					<td nowrap='true'>供货单位</td>
					<td nowrap='true'>维护总金额</td>
					<td nowrap='true'>状态</td>
					<td nowrap='true'>制单人</td>
					<td nowrap='true'>审核人</td>
					<td nowrap='true'>维护说明</td>
	      </tr>
  		</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">            
	            <xsl:choose>
	              <xsl:when test="position()=1">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	           		<xsl:when test="position()=2">
	                <td align='center'><xsl:value-of select="."/></td>
	              </xsl:when>
	              <xsl:when test="position()=10 ">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		 </xsl:when>
	             		 <xsl:when test="position()=8 ">
	             		 </xsl:when>
	              <xsl:otherwise>
	                <td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          	</xsl:choose>          
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
