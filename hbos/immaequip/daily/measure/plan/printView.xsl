<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
					<td>计划编号</td>
					<td>制单日期</td>
					<td>计划名称</td>
					<td>制单人</td>
					<td>审核人</td>
					<td>状态</td>
	  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	            <td >
	              <xsl:choose>
	                <xsl:when test="position()=1">
	                  <xsl:value-of select="."/>
	                </xsl:when>
	                <xsl:otherwise>
	                  <xsl:value-of select="."/>
	                </xsl:otherwise>
	              </xsl:choose>
	            </td>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>