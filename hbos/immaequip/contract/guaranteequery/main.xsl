<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>合同编号</th>
				<th>合同名称</th>
				<th>卡片编号</th>
				<th>设备名称</th>
				<th>规格</th>
				<th>型号</th>
				<th>原值</th>
				<th>入库日期</th>
				<th>保修期</th>	  
			</tr>
			 
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					 
					<xsl:for-each select="td">
						<td>
							<xsl:choose>
								<xsl:when test="position()=1">
									<a tabindex="-1">
										<xsl:value-of select="."/>
									</a>
								</xsl:when>
								<xsl:when test="position()=7">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
