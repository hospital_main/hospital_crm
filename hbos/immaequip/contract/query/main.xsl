<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th rowspan="2">合同编号</th>
				<th rowspan="2">供应商</th>
				<th rowspan="2">资产名称</th>
				<th rowspan="2">型号规格</th>
				<th rowspan="2">产地厂家</th>
				<th colspan="3">数量</th>
				 
				 
			</tr>
			<tr noWrap="true" class="mainHead">
				<th>合同</th>
				<th>到货</th>
				<th>未到</th>
				 
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<td>
							<xsl:choose>
								<xsl:when test="position()=1">
									<a tabindex="-1">
										<xsl:value-of select="."/>
									</a>
								</xsl:when>
								<xsl:when test="position()=16">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
