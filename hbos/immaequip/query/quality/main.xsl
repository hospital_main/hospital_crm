<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">  
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>设备名称</th>
				<th>是否执行质控</th>
				<th>未执行质控原因</th>
				<th>质控结果及说明</th>
				<th>质控执行人</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td align='left'>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								 <td align='left'>
									<xsl:value-of select="."/>
								 </td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
