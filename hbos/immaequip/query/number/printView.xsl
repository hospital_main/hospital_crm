<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>  
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
        <td>资产代码</td>
        <td>资产名称</td>
        <td>规格</td>
        <td>型号</td>
        <td>资产类别</td>
				<td>数量</td>
				<td>单价</td>
				<td>金额</td>	
	  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
        	<xsl:for-each select="td">
           	<xsl:choose>
	            <xsl:when test="position()=6">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0')"/>
								
	              </td>
	            </xsl:when>
	          	<xsl:when test="position()=7">
	              <td align='right'>
	                <xsl:if test="../td[7]=''">
	                    <xsl:value-of select="."/>
	                </xsl:if>
	                <xsl:if test="../td[7]!=''">
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </xsl:if>
	              	
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=8">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:when>
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
	  	<tfoot>
		    <tr noWrap='true'>
	        <td style='fontsize:foot;colspan:7;align:left;border:none'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
		  	</tr>
	  	</tfoot>
 		</root>
	</xsl:template>
</xsl:stylesheet>
