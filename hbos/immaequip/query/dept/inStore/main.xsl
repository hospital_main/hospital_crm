<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>  
			<tr noWrap="true" class="mainHead">
				<th rowspan="2">类别编码</th>
				<th rowspan="2">类别名称</th>
				<th rowspan="2">期初余额</th>
				<th colspan="5">本期增加</th>
				<th colspan="4">本期减少</th>
				<th rowspan="2">期末余额</th>
				
			</tr>
			<tr noWrap="true" class="mainHead">
				<th>入库增加</th>
				<th>科室退库</th>
				<th>采购退货</th>
				<th>原值增加</th>
				<th>增加合计</th>
				<th>科室领用</th>
				<th>处置减少</th>
				<th>原值减少</th>
				<th>减少合计</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<!--td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td-->
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
								 <td align='left'>	
										<xsl:value-of select="."/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=3">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=4">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=5">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=6">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=7">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=8">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=9">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=10">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=11">
								 <td align='right' >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
							 
								<xsl:otherwise>
								 <td align='left'>	
									<xsl:value-of select="."/>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
