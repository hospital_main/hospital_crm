<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">  
				<th>类别编码</th>
				<th>类别名称</th>
				<th>期初余额</th>
				<th>本期增加金额</th>
				<th>本期减少金额</th>
				<th>期末余额</th>
				<!--th rowspan="2">累计折旧</th-->
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<!--td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td-->
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
								 <td align='left'>	
									<a tabindex="-1">
									 
										<xsl:value-of select="."/>
									 
									</a>
								 </td>
								</xsl:when>
								<xsl:when test="position()=3">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=4">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=5">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=6">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=7">
								 <td align='right' style="display:none">	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:otherwise>
								 <td align='left'>	
									<xsl:value-of select="."/>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
