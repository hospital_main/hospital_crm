<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th nowrap='true'>编号</th>
				<th nowrap='true'>名称</th>
				<th nowrap='true'>缺省值</th>
				<th nowrap='true' style='display:none'>选项</th>
				<th nowrap='true'>说明</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:attribute name="_editPk"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
				<xsl:attribute name="_editParaType"><xsl:value-of select="td[6]"/></xsl:attribute>
				<xsl:attribute name="_editDataType"><xsl:value-of select="td[7]"/></xsl:attribute>
				<xsl:attribute name="_editDataValue"><xsl:value-of select="td[3]"/></xsl:attribute>
				<xsl:attribute name="_editInputValue"><xsl:value-of select="td[3]"/></xsl:attribute>
				<xsl:attribute name="_paraCode"><xsl:value-of select="td[1]"/></xsl:attribute>
				<xsl:attribute name="_paraValue"><xsl:value-of select="td[4]"/></xsl:attribute>
				
				<xsl:for-each select="td[position()&lt;6]">   
						<xsl:choose>
							<xsl:when test="position() = 1">
      					<td>
						  		<xsl:value-of select="."/>
			      		</td>
							</xsl:when>
													
							<xsl:when test="position() = 4">
							  <xsl:if test="../td[1] = '0311'">
    							<td style="display:none">
      							<xsl:value-of select="."/>
          				</td>
        				</xsl:if>
							  <xsl:if test="../td[1] != '0311'">
    							<td style="display:none">
      							<xsl:value-of select="."/>
          				</td>
        				</xsl:if>
							</xsl:when>
							
							<xsl:when test="position() = 5">
							  <xsl:if test="../td[1]='0311'">
            			<td id="seleAcct">
  					  			<xsl:value-of select="."/>  
  								</td>
								</xsl:if>
							  <xsl:if test="../td[1]!='0311'">
            			<td>
  					  			<xsl:value-of select="."/>  
  								</td>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
          			<td>
					  			<xsl:value-of select="."/>  
								</td>
          	</xsl:otherwise>
						</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

