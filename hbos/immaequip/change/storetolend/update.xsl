<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>设备编码</th>
				<th>设备名称</th>
				<th>型号规格</th>
				<th>产地厂家</th>
				<th>单位</th>
				<th>单价</th>
				<th>数量</th>
				<th>金额</th>
				<th>设备明细</th>
				<th>设备附件</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  noWrap='true'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' >
	                  <!--<a tabindex='-1' href='#'>
		                <xsl:attribute name="onclick" >
		                	view('equipTrUpdate.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
		                </xsl:attribute><xsl:value-of select="."/></a>-->
		                <xsl:value-of select="."/>
			            </td>
                </xsl:when>
                <xsl:when test="position()=last()">
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
			            <td  noWrap='true' ><a tabindex="-2" href='#'>
		                <xsl:attribute name="onclick" >
		                  openDialog('equipTrEquipList.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:600px', null)
		                </xsl:attribute>管理</a></td>
			            <td  noWrap='true' ><a tabindex="-2" href='#'>
		                <xsl:attribute name="onclick" >
		                  openDialog('equipTrFittingsList.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:600px', null)
		                </xsl:attribute>管理</a></td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
