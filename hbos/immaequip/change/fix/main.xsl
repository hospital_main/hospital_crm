<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th noWrap="true">安装单号</th>
				<th noWrap="true">安装日期 </th>
				<th noWrap="true">摘要</th>
				<th noWrap="true">资产名称</th>
				<th noWrap="true">安装部门</th>
				<th noWrap="true">合同编号</th>
				<th noWrap="true">安装单位</th>
				<th noWrap="true">安装费用</th>
				<th noWrap="true">状态</th>
				<th noWrap="true">附带文档</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">            
            <xsl:choose>
		           <xsl:when test="position()=1">
		            <td  noWrap='true' >
	              <a tabindex="-2">
	                <xsl:attribute name="href" >
	                  javascript:openDialog('update.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;', 'dialogWidth:810px;dialogHeight:600px', result)
	                </xsl:attribute><xsl:value-of select="."/></a>
		            </td>
              </xsl:when>
              <xsl:when test="position()=8">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
              
          </xsl:choose> 
               
  			  </xsl:for-each>
  			  <td>
             <a>
               <xsl:attribute name="href">
                  javascript:openDialog('manage.html?load=&lt;id&gt;<xsl:value-of select="pk/*"/>&lt;/id&gt;', 'dialogWidth:800px;dialogHeight:580px', result)
               </xsl:attribute>
               管理
            </a>
         </td>    
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


