<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th  width='25'><input type='checkbox'/></th>
				<th>盘点单据号</th>
				<th>摘要</th>
				<th>盘点部门</th>
				<th>盘点日期</th>
				<th>盘点人</th>
				<th>审核人</th>
				<th>审核日期</th>				
				<th>状态</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>       
          <td align='center'  noWrap='true'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>                
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' >
	                  <a tabindex='-1' ><xsl:value-of select="."/></a>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>  	
	</xsl:template>
</xsl:stylesheet>
