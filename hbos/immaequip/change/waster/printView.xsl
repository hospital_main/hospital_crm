<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-2"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
					<td>处置单号</td>
					<td>制单日期</td>
					<td>业务类型</td>
					<td>往来单位</td>
					<td>部门</td>
					<td>卡片号</td>
					<td>资产名称</td>
					<td>类别</td>
					<td>规格</td>
					<td>型号</td>
					<td>制单人</td>
					<td>审核人</td>
					<td>状态</td>
					<td>备注</td>
					<td>金额</td>
					<td>记账日期</td>
	  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td[position() != 13]">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' >
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
                <xsl:when test="position()=8">
			            <td  noWrap='true' align="center">
    								<xsl:value-of select="."/>
			            </td>
                </xsl:when>
              	<xsl:when test="position()=15 ">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             	</xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>