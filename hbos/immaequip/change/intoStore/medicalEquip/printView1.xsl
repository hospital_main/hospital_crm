<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:12'></td>
	  			<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:12;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
	  		<tr noWrap='true' >
	        <td align='left' style="fontsize:subtitle;colspan:12;align:left"/>
	  		</tr>
	  		<tr noWrap='true' class='mainHead'>
      	<td noWrap='true' valign="middle">资产类别</td>
        <td noWrap='true' valign="middle">资产名称</td>
        <td noWrap='true' valign="middle">规格</td>
        <td noWrap='true'>型号</td>
        <td noWrap='true'>品牌</td>
        <td noWrap='true'>生产国家</td>
        <td noWrap='true' valign="middle">生产厂家</td>
        <td noWrap='true' valign="middle">单价</td>
        <td noWrap='true' valign="middle">验收数量</td>
        <td noWrap='true' valign="middle">缺损程度</td>
        <td noWrap='true' valign="middle">验收说明</td>
        <td noWrap='true' valign="middle">验收项目</td>
      </tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">            
	            <xsl:choose>
			           <xsl:when test="position()=1">
			            <td  noWrap='true' >
		              	<xsl:value-of select="."/>
			            </td>
	              </xsl:when>
	              <xsl:when test="position()=8 or position()=9">
	                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	              </xsl:when>
	              <xsl:otherwise>
	                <td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          	</xsl:choose>
	  			  </xsl:for-each>
	  			  <td>
	            管理
	         	</td>    
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>