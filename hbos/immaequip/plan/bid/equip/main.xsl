<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
		  <tr noWrap='true' class='mainHead'>
	      <th style=''><input type='checkbox'/></th>
				<th noWrap="true">单据编号</th>
				<th noWrap="true">招标日期</th>
	      <th noWrap="true">供应商</th>
				<th noWrap="true">资产信息</th>
				<th noWrap="true">规格</th>
				<th noWrap="true">型号</th>
				<th noWrap="true">需求科室</th>
				<th noWrap="true">资金来源</th>
				<th noWrap="true">生产厂家</th>
				<th noWrap="true">生产许可证</th>
				<th noWrap="true">注册证</th>
				<th noWrap="true">注册证到期日期</th>
	      <th noWrap="true">数量</th>
	      <th noWrap="true">价格</th>
	      <th noWrap="true">金额</th>
	      <th noWrap="true">联系人</th>
	      <th noWrap="true">状态</th>
      </tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr[position()=1]">
        <tr>
          <td align='center' colspan="2"  style=''>合计
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
							  <td  noWrap='true' style="display:none;">
							  </td>
							</xsl:when>
							<xsl:when test="position()=2">
							  <td  noWrap='true'>
							  </td>
							</xsl:when>
							<xsl:when test="position()=14 ">
								<td align='right'></td>
							</xsl:when>
							<xsl:when test="position()=15 ">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
        <tr>
          <td align='center'  style=''>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
							  <td  noWrap='true' >
							  <a tabindex="-1">
							    <xsl:attribute name="href" >
							      javascript:view('update.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;&lt;state&gt;<xsl:value-of select="../td[position()=17]"/>&lt;/state&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:when test="position()=14 or position()=15 ">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


