<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap='true' class='mainHead'>
					<td noWrap="true">单据编号</td>
		      <td noWrap="true">招标日期</td>
		      <td noWrap="true">供应商</td>
		      <td noWrap="true">资产信息</td>
					<td noWrap="true">规格</td>
					<td noWrap="true">型号</td>
					<td noWrap="true">需求科室</td>
	        <td noWrap="true">资金来源</td>
					<td noWrap="true">生产厂家</td>
					<td noWrap="true">生产许可证</td>
					<td noWrap="true">注册证</td>
	        <td noWrap="true">到期日期</td>
	        <td noWrap="true">数量</td>
					<td noWrap="true">价格</td>
					<td noWrap="true">金额</td>
					<td noWrap="true">联系人</td>
	        <td noWrap="true">状态</td>
				</tr>
	  	</thead>
	  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr[position()=1]">
        <tr>
          
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
							  <td  noWrap='true'>
							  	合计
							  </td>
							</xsl:when>
							<xsl:when test="position()=2">
							  <td  noWrap='true'>
							  </td>
							</xsl:when>
							<xsl:when test="position()=14 ">
								<td align='right'></td>
							</xsl:when>
							<xsl:when test="position()=15 ">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
		    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	        <tr>
	        	
	          <xsl:for-each select="td">
	            <xsl:choose>
								<xsl:when test="position()=1">
									<td noWrap='true' >
										<xsl:if test=".!='zzzzzzzz'">
										<xsl:value-of select="."/>
										</xsl:if>
										<xsl:if test=".='zzzzzzzz'">
										</xsl:if>
									</td>
								</xsl:when>
								<xsl:when test="position()=14 or position()=15">
									<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
	          	</xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>