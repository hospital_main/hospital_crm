<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
       <xsl:for-each select="/root/tbody/tr[position() = 1]">
        <tr class='mainHead'>
        	<xsl:for-each select="td[position() != 1]">
            <td>
              <xsl:value-of select="."/>
            </td>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position() != 1]">
        <tr>
        	<xsl:for-each select="td[position() != 1]">
           	<xsl:choose>
	            <xsl:when test="position() &gt; 2">
	              <xsl:if test='.=""'>
	            		<td align='right'>
	              	0.00
	              	</td>
	            	</xsl:if>
	              <xsl:if test='.!=""'>
	            		<td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	              	</td>
	            	</xsl:if>
	            </xsl:when>
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


