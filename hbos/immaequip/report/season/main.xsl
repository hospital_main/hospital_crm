<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<thead>
			<xsl:variable name="colPosition" select="2"/>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' rowspan="2">统计分类</th>
				<xsl:for-each select="/root/tbody/tr[1]/td">    		      
					<xsl:choose>
						<xsl:when test="position() &gt; $colPosition and position() mod 2 = 0 ">
							<th nowrap='true' colspan="2"><xsl:value-of select="."/></th>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<xsl:for-each select="/root/tbody/tr[1]/td">    		      
					<xsl:if test="position() &gt; $colPosition ">
						<xsl:if test=" position() mod 2 = 0 ">
							<th nowrap='true'>金额</th>
						</xsl:if>
						<xsl:if test=" position() mod 2 = 1 ">
							<th nowrap='true'>数量</th>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:if test="position() &gt; 2">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position() = 1 ">
									<td style="display:none" ></td>
								</xsl:when>
								<xsl:when test="position() = 2 ">
									<td><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:when test="position() mod 2 = 1 ">
									<td align='right'><xsl:value-of select="format-number(.,'#,##0')"/></td>
								</xsl:when>
								<xsl:when test="position() mod 2 = 0 ">
									<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:if>	 
			</xsl:for-each>
		</tbody>
  </xsl:template>
</xsl:stylesheet>

