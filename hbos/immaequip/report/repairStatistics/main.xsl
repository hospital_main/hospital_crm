<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<thead>
  		<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test=" position() &lt; 3 ">
	    			<xsl:variable name="rows" select="position()"/>
	    			<tr noWrap='true' class='mainHead'>
	    				<xsl:if test=" $rows =1 ">
	          		<th  rowspan="2" valign="middle" >姓名</th>
          		</xsl:if>
          		<xsl:if test=" $rows =2 ">
	          		<th style="display:none" >姓名</th>
          		</xsl:if>
		          <xsl:for-each select="td">
		          	<xsl:variable name="cols" select="position()"/>
		          	<xsl:variable name="cols_name" select="."/>
		          	<xsl:variable name="colNums" select="count(/root/tbody/tr[ $rows ]/td[.= $cols_name]) "/>
		          	<xsl:choose>
			          	<xsl:when test=" position()>4">
			          		<xsl:if test=" $rows=1">
				          		<xsl:if test=" ../td[$cols]!= ../td[$cols -1 ]">
				          			<th valign="middle"  >
				          				<xsl:attribute name="colspan" >
				          				<xsl:value-of select="$colNums"/>
						            	</xsl:attribute>
						            	<xsl:if test=" ../td[$cols]= ../../tr[2]/td[$cols ]">
							            	<xsl:attribute name="rowspan" >
					          					<xsl:value-of select="2"/>
							            	</xsl:attribute>
						            	</xsl:if>
						            	<xsl:value-of select="."/>
				          			</th>
					          	</xsl:if>
					          	<xsl:if test=" ../td[$cols] = ../td[$cols -1 ]">
				          			<th style="display:none" >
						            	<xsl:value-of select="."/>
				          			</th>
					          	</xsl:if>
					          </xsl:if>
					          <xsl:if test=" $rows=2">
			          			<th valign="middle" >
					            	<xsl:value-of select="."/>
			          			</th>
					          </xsl:if>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		          <xsl:if test=" $rows =1 ">
	          		<th  colspan="2" >总计</th>
	          		<th style="display:none" >总计</th>
          		</xsl:if>
          		<xsl:if test=" $rows =2 ">
	          		<th valign="middle" >维修次数(次)</th>
	          		<th valign="middle" >维修耗时(h)</th>
          		</xsl:if>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test=" position()>2 ">
	    			<tr noWrap='true'>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			            <xsl:when test=" position()=1 ">
			            	<td ><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test="position() >4">
			            	<td align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		<xsl:when test=" position()=2 ">
			            	<td align="right">
			            		<xsl:value-of select="."/>
			            	</td>
			            </xsl:when>
			            <xsl:when test=" position()=3 ">
			            	<td align="right"><xsl:value-of select="."/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>