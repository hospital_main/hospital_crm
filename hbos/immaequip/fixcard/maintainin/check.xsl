<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
  			<th>资产卡号</th>
				<th>资产编码</th>
				<th>资产名称</th>
				<th>单价</th>
				<th>原值</th>
				<th>使用部门</th>
				<th>资金来源</th>
				<th>折旧年限</th>
				<th>资产性质</th>
				<th>资产分类</th>
				<th>入库日期</th>
				<th>使用状态</th>
				<th>是否折旧</th>
				
  		</tr>
  	</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				    <xsl:choose>
				       <xsl:when test="position()=5">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
	              <xsl:when test="position()=4">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>			                        
				    </xsl:choose>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>