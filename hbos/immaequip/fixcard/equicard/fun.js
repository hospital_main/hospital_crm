function submitData(obj){
  if(seq_no.value=='自动生成'){
	  var p = getValuePairBySql("equi_maintain_plan_no","<card_no>"+getLoginDate()+"</card_no><bill>24</bill>");
    seq_no.value = p[0];
	  par.tempCard = p[0];
  }
  
	var res=checkInputs();
	if(res==null||res=="")
		return ;
	window.xmlhttp.post(obj.name, res,'')
	var str = window.xmlhttp._object.responseText
	if (!window.doMsg(str,"")) {
	  return false;
	}
}

function checkInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	var res="";
	var temp = "";
	var inputs;
	for(var i=1;i<trs.length;i++){
    inputs = trs[i].getElementsByTagName("input");
	  if(trs[i].getAttribute("_change")=='1' && inputs[0].checked){
      res+="<record>";
      res+=trs[i]._editPk
      res+="<date>"+seq_no.value+"</date>"
      res+="<pref>"+trs[i].getAttribute("_is_exec")+"</pref>";
      res+="<cyc>"+trs[i].getAttribute("_is_well")+"</cyc>";
      res+="<desc>"+trs[i].getAttribute("_desc")+"</desc>";
  		res+="</record>";
  	}
	}
	return res;
}


function initInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	for(var i=1;i<trs.length;i++)
		initTrInputs(trs[i]);
}

function textChange(obj){
  obj.parentNode.parentNode._desc = obj.value;
  obj.parentNode.parentNode._change = '1';  
}

function setValue(j,obj){
  obj.parentNode.parentNode._change = '1';  
  if(j==3)
	  obj.parentNode.parentNode._is_well = obj.value;
  if(j==2)
	  obj.parentNode.parentNode._is_exec = obj.value;
}

function initTrInputs(tr){
	var inp="";
	var id;
  id=getJsGuid();
  var temp = tr.cells[4].innerText;
	tr.cells[4].innerHTML ="<input type='text' id='"+id+"' name='"+id+"' value='"+temp+"' maxlength='200' width='100' onChange='textChange(this);'/>";

	for(var j=2;j<=3;j++){
  	temp=tr.cells[j].innerText;
    id=getJsGuid();
  	inp="<select id='"+id+"' name='"+id+"' align='center' onChange='setValue(" + j + ",this);'>";
  	tag="select"
  	if(temp=='0'){
    		inp+="<option selected value='0'>否</option>";
    		inp+="<option value='1'>是</option>";
  	}else{
    		inp+="<option value='0'>否</option>";
    		inp+="<option selected value='1'>是</option>";
  	}
  	inp+="</select>";
  	tr.cells[j].innerHTML=inp;
  }
}
