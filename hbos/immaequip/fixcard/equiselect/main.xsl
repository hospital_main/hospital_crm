<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<!--th  nowrap='true'>选择</th-->
				<th style='display:none'><input type='checkbox'/></th>
				
				<xsl:for-each select="/root/t2head/tr[position()=1]">
				  <xsl:for-each select="td[position()!=1]">
							<xsl:variable name="colNum2" select="position()+1"/>
									<th>
										<xsl:value-of select="."/>
									</th>
						</xsl:for-each>
				</xsl:for-each>
				<!--
				<th nowrap='true'>资产卡片号</th>
				<th nowrap='true'>资产编号</th>
				<th nowrap='true'>资产名称</th>
				<th nowrap='true'>型号</th>
				<th nowrap='true'>规格</th>
				<th nowrap='true'>产地</th>
				<th nowrap='true'>厂家</th>
				<th nowrap='true'>购入日期</th>
				<th nowrap='true'>启用日期</th>
				<th nowrap='true'>建档日期</th>
				-->
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
			    <xsl:if test="td[2] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[2] != '合计'">
  				<td align='center'>
  					<input type='checkbox' TABINDEX='-1'>
  						<xsl:attribute name="value" >
  							<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
  						</xsl:attribute>
  						<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
  					</input>
  				</td> 
  				</xsl:if>
  				<xsl:if test="td[2] = '合计'">	
    			<xsl:for-each select="td[position()!=1]">   
						<xsl:variable name="colNum" select="position()+1"/>
							<xsl:choose>
								<xsl:when test="position()=1">
		              <td>
		                <xsl:value-of select="."/>
		              </td>
		            </xsl:when>
							<!--判断是否是全角空格-->
								<xsl:when test="substring(/root/t2head/tr[1]/td[$colNum],string-length(/root/t2head/tr[1]/td[$colNum] ),1)='　'">
									<td align='right'>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
				   </xsl:for-each>
				   </xsl:if>
				  <xsl:if test="td[2] != '合计'">	
    			<xsl:for-each select="td[position()!=1]">   
						<xsl:variable name="colNum" select="position()+1"/>
							<xsl:choose>
								<xsl:when test="position()=1">
		              <td>
		                <a tabindex='-1'><xsl:value-of select="."/></a>
		              </td>
		            </xsl:when>
							<!--判断是否是全角空格-->
								<xsl:when test="substring(/root/t2head/tr[1]/td[$colNum],string-length(/root/t2head/tr[1]/td[$colNum] ),1)='　'">
									<td align='right'>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
				   </xsl:for-each>
				   </xsl:if>				   
			  </tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>


