<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td rowspan="3">类别</td> 
				<td colspan="2">期初余额</td>
				<td style="display:none"></td>
				<td colspan="5">本期增加</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan="5">本期减少</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan="2">期末余额</td>
				<td style="display:none"></td>
				<!--td rowspan="2">累计折旧</td-->
			</tr>
			<tr noWrap="true" class="mainHead">
				<td style="display:none"></td>
				<td rowspan="2">台数</td>
				<td rowspan="2">金额</td>
				<td rowspan="2">采购增加</td>
				<td rowspan="2">无偿增加</td>
				<td rowspan="2">其他增加</td>
				<td colspan="2">合计</td>
				<td style="display:none"></td>
				<td rowspan="2">报废减少</td>
				<td rowspan="2">无偿减少</td>
				<td rowspan="2">其他减少</td>
				<td colspan="2">合计</td>
				<td style="display:none"></td>
				<td rowspan="2">台数</td>
				<td rowspan="2">金额</td>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td>台数</td>
				<td>金额</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td>台数</td>
				<td>金额</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				 
				<tr>
					<!--td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td-->
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
								 <td align='left'>	
									<a tabindex="-1">
									 
										<xsl:value-of select="."/>
									 
									</a>
								 </td>
								</xsl:when>
								<xsl:when test="position()=2">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=3">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=4">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=5">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=6">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=7">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=8">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=9">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=10">
								 <td align='right' >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=11">
								 <td align='right' >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=12">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=13">
								 <td align='right' >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=14">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:otherwise>
								 <td align='left'>	
									<xsl:value-of select="."/>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		 
		</tbody>
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>