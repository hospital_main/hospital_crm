<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>  
			<tr noWrap="true" class="mainHead">
				<th rowspan="2">类别编码</th>
				<th rowspan="2">类别名称</th>
				<th colspan="4">原值</th>
				<th colspan="4">累计摊销</th>
				<th colspan="2">净值</th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th>期初</th>
				<th>借方</th>
				<th>贷方</th>
				<th>余额</th>
				<th>期初</th>
				<th>借方</th>
				<th>贷方</th>
				<th>余额</th>
				<th>期初</th>
				<th>期末</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1 or position()=2">
								  <td align='left'>	
									<xsl:value-of select="."/>
								 </td>
								</xsl:when>
								<xsl:otherwise>
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
