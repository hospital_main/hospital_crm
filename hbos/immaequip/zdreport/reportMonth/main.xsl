<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">  
		<thead>
			<tr noWrap="true" class="mainHead">
				<th rowspan="2">分类名称</th>
				<th colspan="2">期初余额</th>
				<th colspan="2">本月增加</th>
				<th colspan="2">本月减少</th>
				<th colspan="2">期末余额</th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th>数量</th>
				<th>金额</th>
				<th>数量</th>
				<th>金额</th>
				<th>数量</th>
				<th>金额</th>
				<th>数量</th>
				<th>金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<!--td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td-->
					<xsl:for-each select="td">
					
					<xsl:choose>
					   <xsl:when test="position()=1">
								<td align='right'>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
						  <xsl:when test="position()=2 or position()=4 or position()=6 or position()=8">
								<td align='right'>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								</td>
							</xsl:when>
							<xsl:when test="position()=3 or position()=5 or position()=7 or position()=9">
								<td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								 <td align='left'>
									<xsl:value-of select="."/>
								 </td>
							</xsl:otherwise>
						
						</xsl:choose>
					</xsl:for-each>
					
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
