 <?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
  		<tr noWrap="true" class="mainHead">
        <td rowspan="2" valign="middle">���ұ���</td>
        <td rowspan="2" valign="middle" >��������</td>
		    <td colspan="4">ԭֵ</td>
		    <td style='display:none;'></td> <td style='display:none;'></td> <td style='display:none;'></td> 
		    <td colspan="4">�ۼ�̯��</td>
		    <td style='display:none;'></td> <td style='display:none;'></td> <td style='display:none;'></td> 
		    <!--<td colspan="2" valign="middle" >��ֵ</td>-->
  		</tr>
  		<tr noWrap="true" class="mainHead">
  			<td style='display:none;'></td> 
  			<td style='display:none;'></td> 
  			<td valign="middle">�ڳ�</td>
		    <td valign="middle">�跽</td>
		    <td valign="middle">����</td>
		    <td valign="middle">���</td>	
		    <td valign="middle">�ڳ�</td>
		    <td valign="middle">�跽</td>
		    <td valign="middle">����</td>
		    <td valign="middle">���</td>	
		    <!--
		    <td valign="middle">�ڳ�</td>
		    <td valign="middle">��ĩ</td>		
		    -->    		    
		  </tr>  		
  	</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      <xsl:variable name='CurTrPos' select='position()'/>
        <tr>          
          <xsl:for-each select="td[position() &lt; 11]">
          	<xsl:choose>							
              <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12">
                <td align='right'>
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              
              <xsl:otherwise>
                <td align='left'><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
   </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>