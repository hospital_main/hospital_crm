<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>  
      <tr noWrap='true' class='mainHead'>
        <th>使用部门</th>  
				<th>资产常用名称</th>
				<th>资产财务分类</th>
				<th>条形码</th>
				<th>设备卡号</th>
				<th>保管人</th>
				<th>经费来源</th>
				<th>金额</th>
				<th>发票号</th>
				<th>购置日期</th>
				<th>入账日期</th>
				<th>供应商</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>
        	<xsl:for-each select="td">
           	<xsl:choose>
	          	 
	            <xsl:when test="position()=8">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
	            </xsl:when>
	           <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  		</xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


