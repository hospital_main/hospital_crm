<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:12'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
			<td style='display:none'></td>
  			<td style='display:none'></td>
  		  <td style='display:none'></td>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
        <!--td></td-->
        
        <td>资产编号</td>
				<td>资产名称</td>	
				<td>常用名称</td>				
				<td>品牌</td>				
				<td>型号规格</td>
				<td>计量单位</td>
				<td>单价</td> 
				<td>卡片数量</td>
				<td>金额</td>  
				<td>所在部门</td>
				<td>摘要</td>
				<td>财务分类名称</td>
				<!--WXZC9 无形资产—统计报表—无形资产分布 王羽 (添加 ：fund_source_name,proj_cod) 2017-03-28 -->
				<td>资金来源</td>
				<td>项目名称</td>
				<!---->
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:if test="./td[1]!='合计'">
      	<tr>
        		<xsl:for-each select="td">
            <xsl:choose>
               <xsl:when test="position()=7">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when> 
								<xsl:when test="position()=9">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when> 
								 <xsl:when test="position()=8">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
                <xsl:otherwise>
                  <td align='left'><xsl:value-of select="."/></td>
                </xsl:otherwise>
           </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  		
  			
  			</xsl:if>
  			<xsl:if test="./td[1]='合计'">
  			<tr>
  				<td align='center' >合计</td>
					
        <td></td>
        <td></td>
				<td></td>
				<td></td>				
				<td></td>
				<td></td>
				<td><xsl:value-of select="format-number(./td[8],'#,##0')"/></td>
				<td><xsl:value-of select="format-number(./td[9],'#,##0.00')"/></td>
				<td></td>
				
  				
  			</tr>
  			</xsl:if>
      </xsl:for-each>  

    </tbody>
   </root>
	</xsl:template>
	
</xsl:stylesheet>