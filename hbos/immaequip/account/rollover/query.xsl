<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
		    <th rowspan="2" valign="middle">资产名称</th>
		    <th rowspan="2" valign="middle">型号规格</th>
		    <th rowspan="2" valign="middle">计量单位</th>
		    <th colspan="2" valign="middle">期初</th>
		    <th colspan="2" valign="middle">本期增加</th>
		    <th colspan="2" valign="middle">累计增加</th>
		    <th colspan="2" valign="middle">本期减少</th>
		    <th colspan="2" valign="middle">累计减少</th>
  		</tr>
  		<tr noWrap="true" class="mainHead"> 
  			<th  valign="middle" class="style4">数量</th>
  			<th  valign="middle" class="style4">金额</th>
		    <th  valign="middle" class="style4">数量</th>
		    <th  valign="middle" class="style4">金额</th>
		    <th  valign="middle" class="style4">数量</th>
		    <th  valign="middle" class="style4">金额</th>
		    <th  valign="middle" class="style4">数量</th>
		    <th  valign="middle" class="style4">金额</th>
		    <th  valign="middle" class="style4">数量</th>
		    <th  valign="middle" class="style4">金额</th>
		  </tr>  		
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position() &gt; 4">
                  <td>
                    <xsl:value-of select="format-number(.,'#,#00.00')"/>                  
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td align="center">
                    <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
