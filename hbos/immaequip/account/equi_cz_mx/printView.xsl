<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-0"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td>序号</td>
				<td>部门/仓库编码</td>				
				<td>部门/仓库名称</td>		
				<td>卡片编码</td>				
				<td>资产名称</td>				
				<td>资产类别</td>				
				<td>规格</td>
				<td>型号</td>				
				<td>增加日期</td>
				<td>处置日期</td>				
				<td>原值</td>
				<td>累计摊销</td>				
				<td>现值</td>				
				<td>处理收入</td>				
				<td>处置金额</td>				
				<td>业务类型</td>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<td><xsl:value-of select="position()"/></td>
          <xsl:for-each select="td">
              <xsl:choose>							
              <xsl:when test="position()=10 or position()=11 or position()=12 or position()=13 or position()=14">
                <td align='right'>
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>              
              <xsl:otherwise>
                <td align='left'><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
   </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>