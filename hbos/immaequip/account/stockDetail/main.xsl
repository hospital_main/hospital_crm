<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>类别编码</th>
				<th>类别名称</th>
				<th>期初库存金额</th>
				<th>本期增加金额</th>
				<th>本期减少金额</th>
				<th>期末结存金额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td[position() &lt; 7]">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                	<xsl:if test="../td[7] = '1'">
                	<a tabindex="-2">
                  	<xsl:attribute name="href" >
                    	javascript:openDialog('querystock.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:800px;dialogHeight:600px') 
                  	</xsl:attribute>
                  	<xsl:value-of select="."/>
                	</a>
                	</xsl:if>
                	<xsl:if test="../td[7] != '1'">
                  	<xsl:value-of select="."/>
                	</xsl:if>
                </td >
              </xsl:when>
              <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:otherwise>
                <td ><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
