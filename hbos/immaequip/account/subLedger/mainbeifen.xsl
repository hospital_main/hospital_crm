<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>设备编码</th>
				<th>设备名称</th>
				<th>型号规格</th>
				<th>产地厂家</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td >
                  	<a tabindex="-2">
                    	<xsl:attribute name="href" >
                      	javascript:openDialog('queryDetail.html?load=&lt;root&gt;&lt;equi_code&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/equi_code&gt;&lt;equi_name&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/equi_name&gt;&lt;equi_model&gt;<xsl:value-of select="../td[position()=3]"/>&lt;/equi_model&gt;&lt;equi_space&gt;<xsl:value-of select="../td[position()=4]"/>&lt;/equi_space&gt;&lt;e_model&gt;<xsl:value-of select="../td[position()=5]"/>&lt;/e_model&gt;&lt;e_spec&gt;<xsl:value-of select="../td[position()=6]"/>&lt;/e_spec&gt;&lt;e_nation&gt;<xsl:value-of select="../td[position()=7]"/>&lt;/e_nation&gt;&lt;e_manufacturer&gt;<xsl:value-of select="../td[position()=8]"/>&lt;/e_manufacturer&gt;&lt;/root&gt;', 'dialogWidth:900px;dialogHeight:600px', result)
                    	</xsl:attribute>
                    	<xsl:value-of select="."/>
                  	</a>
                  </td >
                </xsl:when> 
                <xsl:when test="position()=5 or position()=6 or position()=7 or position()=8">
                	<td style="display:none"><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:otherwise>
                  <td ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
