<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th rowspan="2" valign="middle">分类编码</th>
				<th rowspan="2" valign="middle">类别名称</th>
				<th colspan="2" valign="middle">期初结存</th>
				<th colspan="2" valign="middle">本月增加</th>
				<th colspan="2" valign="middle">本月减少</th>
				<th colspan="2" valign="middle">本月结存</th>
  		</tr>
  		<tr noWrap="true" class="mainHead">
  			<th valign="middle" class="style4">数量</th>
  			<th valign="middle" class="style4">金额</th>
  			<th valign="middle" class="style4">数量</th>
  			<th valign="middle" class="style4">金额</th>
  			<th valign="middle" class="style4">数量</th>
  			<th valign="middle" class="style4">金额</th>
  			<th valign="middle" class="style4">数量</th>
  			<th valign="middle" class="style4">金额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td ><a tabindex="-2">
                    <xsl:attribute name="href" >
                      javascript:openDialog('queryDetail.html?load=&lt;root&gt;&lt;equi_kind_code&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/equi_kind_code&gt;&lt;equi_kind_name&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/equi_kind_name&gt;&lt;/root&gt;', 'dialogWidth:900px;dialogHeight:600px', result)
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                  </a></td >
                </xsl:when> 
                
								<xsl:when test="position()=3">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=4">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=5">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=6">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=7">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=8">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=9">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=10">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
                <xsl:otherwise>
                  <td align="left"><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>