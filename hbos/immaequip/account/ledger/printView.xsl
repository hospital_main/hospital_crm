<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td rowspan="2" valign="middle">分类编码</td>
				<td rowspan="2" valign="middle">类别名称</td>
				<td colspan="2" valign="middle">期初结存</td>
				<td style="display:none"></td>
				<td colspan="2" valign="middle">本月增加</td>
				<td style="display:none"></td>
				<td colspan="2" valign="middle">本月减少</td>
				<td style="display:none"></td>
				<td colspan="2" valign="middle">本月结存</td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">
  		  <td style="display:none"></td>
  		  <td style="display:none"></td>
  			<td valign="middle">数量</td>
  			<td valign="middle">金额</td>
  			<td valign="middle">数量</td>
  			<td valign="middle">金额</td>
  			<td valign="middle">数量</td>
  			<td valign="middle">金额</td>
  			<td valign="middle">数量</td>
  			<td valign="middle">金额</td>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td ><a tabindex="-2">
                    <xsl:attribute name="href" >
                      javascript:openDialog('queryDetail.html?load=&lt;root&gt;&lt;equi_kind_code&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/equi_kind_code&gt;&lt;equi_kind_name&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/equi_kind_name&gt;&lt;/root&gt;', 'dialogWidth:900px;dialogHeight:600px', result)
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                  </a></td >
                </xsl:when> 
                
								<xsl:when test="position()=3">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=4">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=5">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=6">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=7">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=8">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=9">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=10">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
                <xsl:otherwise>
                  <td align="left"><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
   </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>