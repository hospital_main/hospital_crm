<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th></th>
        <th>资产卡片号</th>
        <th>资产编号</th>
				<th>资产名称</th>				
				<th>型号规格</th>
				<th>产地厂家</th>
				<th>计量单位</th>
				<th>原值</th>
				<th>卡片数量</th>
				<th>入库日期</th>
				<th>启用日期</th>
				<th>所在部门</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>
        	<td align="center" style="cursor:hand" onclick="changeDenotation(this)">
         		  <xsl:attribute name="id">row<xsl:value-of select="$rowindex"/></xsl:attribute>
           		<xsl:attribute name="apply_no"><xsl:value-of select="td[1]"/></xsl:attribute>
           		+
           		</td>
        	<xsl:for-each select="td">
           	<xsl:choose>
	          	<xsl:when test="position()=1">
	              <td>
	              	<a href="#">
										<xsl:attribute name="onclick">
              			  javascript:openDialog('../../fixcard/add/update.html?load=&lt;card_arch_no&gt;<xsl:value-of select ="../td[position()=1]"/>&lt;/card_arch_no&gt;&lt;type&gt;1&lt;/type&gt;','dialogWidth:800px;dialogHeight:650px');
										</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
	              </td>
	            </xsl:when>
	            <!--xsl:when test="position()=2">
	            </xsl:when-->
	            <xsl:when test="position()=7">
	          		<td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
	            <!--xsl:when test="position()=12">
	            </xsl:when-->
	            <xsl:otherwise>
		            <td align="center">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			<tr style="display:none">
  				<xsl:attribute name="id">annexrow<xsl:value-of select="$rowindex"/></xsl:attribute>
  				<td align='center'>
          </td>
          <td colspan="11" ><xsl:attribute name="id">revelation<xsl:value-of select="$rowindex"/></xsl:attribute>
          </td>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


