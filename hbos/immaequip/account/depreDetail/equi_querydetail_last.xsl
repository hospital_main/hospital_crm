<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
  			<th>提取年月</th>
  			<th>资产卡片</th>
				<th>资产编码</th>
				<th>资产名称</th>
				<th>规格型号</th>
				<th>产地厂家</th>
				<th>入库日期</th>
				<th>启用日期</th>
				<th>原值</th>
				<th>月折旧率</th>
				<th>本月计提</th>
				<th>累计计提</th>
				<th>所在部门</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td >
                  	<!--a tabindex="-2" href="#">
                    	<xsl:attribute name="onclick" >
                    		javascript:openDialog('querydepre.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:800px;dialogHeight:600px') 
                      	/*javascript:openDialog('equi_queryDetail.html?load=&lt;root&gt;&lt;depre_year&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/depre_year&gt;&lt;depre_month&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/depre_month&gt;&lt;equi_kind_code&gt;<xsl:value-of select="../td[position()=3]"/>&lt;/equi_kind_code&gt;&lt;equi_kind_name&gt;<xsl:value-of select="../td[position()=4]"/>&lt;/equi_kind_name&gt;&lt;/root&gt;', 'dialogWidth:900px;dialogHeight:600px', result)*/
                    	</xsl:attribute-->
                    	<xsl:value-of select="."/>
                  	<!--/a-->
                  </td >
                </xsl:when> 
                <xsl:when test="position()=9 or position()=10 or position()=11 or position()=12">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td >
                </xsl:when> 
                <xsl:otherwise>
                  <td ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
