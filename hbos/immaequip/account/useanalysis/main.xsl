<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>序号</th>
				<th>资产卡片号</th>
				<th>资产编号</th>
				<th>资产名称</th>
				<th>型号规格</th>
				<th>产地厂家</th>
				<th>库房|科室</th>
				<th>购入日期</th>
				<th>启用日期</th>
				<th>使用年限</th>
				<th>已用月份</th>
				<th>状态</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<td><xsl:value-of select="position()"/></td>
          <xsl:for-each select="td">
              <td><xsl:value-of select="."/></td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>

