<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>		         
				<th>编号</th>
				<th>合同编号</th>
				<th>合同名称</th>
				<th>期号</th>
				<th>供应商</th>
				<th>金额</th>
				<th>本次付款</th>
				<th>付款日期</th>
				<th>状态</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<td>
							<xsl:choose>
								<xsl:when test="position()=1">
									<a tabindex="-1">
									<xsl:attribute name="href">
		                  javascript:openDialog('update.html?load=&lt;change_code&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/change_code&gt;', 'dialogWidth:700px;dialogHeight:300px;scroll:1', result)
		                </xsl:attribute><xsl:value-of select="."/>
										
									</a>
								</xsl:when>
								<xsl:when test="position()=6 ">
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
             		</xsl:when>
								<xsl:when test="position()=7">
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
             		</xsl:when>
								<!--测试新增>
								<xsl:when test="position()=2">
									<a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('contract_view.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/contract_no&gt;', 'dialogWidth:650px;dialogHeight:450px', result)
		                </xsl:attribute>										<xsl:value-of select="."/></a>
								</xsl:when-->
								
								<!--xsl:when test="position()=5">
									<a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('contractequipchange_main.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/contract_no&gt;', 'dialogWidth:850px;dialogHeight:650px', result)
		                </xsl:attribute>管理</a>
								</xsl:when>
								<xsl:when test="position()=6">
									<a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('contractappechange_main.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/contract_no&gt;', 'dialogWidth:850px;dialogHeight:650px', result)
		                </xsl:attribute>管理</a>
								</xsl:when>
								<xsl:when test="position()=7">
									<a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('contractpaychange_main.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/contract_no&gt;', 'dialogWidth:850px;dialogHeight:650px', result)

		                </xsl:attribute>管理</a>
								</xsl:when-->
								 
								<!--测试新增-->
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
