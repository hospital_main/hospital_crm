<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
      <colgroup>		       
	      <col style = 'width:100'/>
	      <col style = 'width:140'/>
	      <col style = 'width:100'/>
	      <col style = 'width:100'/>
	      <col style = 'width:100'/>
	      <col style = 'width:100'/>
	      <col style = 'width:100'/>
	    </colgroup>
		  <thead>
	  		<tr noWrap="true" class="mainHead">
	        <th>发票编号</th>
	        <th>开票日期</th>
					<th>摘要</th>				
					<th>采购员</th>
					<th>发票金额</th>
					<th>已付款金额</th>		
					<th>本次付款金额</th>		
	  		</tr>
	  	</thead>
	  	<tbody>
	  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	              <xsl:choose>
	                <xsl:when test="position()=1">
              		</xsl:when>
	                <xsl:when test="position()=6 or position()=7 or position()=8 ">
	                  <td class='numberText'>
  	                		<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
  	              	</td>
	                </xsl:when>
	                <xsl:when test="position()=9 or position()=10 ">
	                </xsl:when>
	                <xsl:otherwise>
	                  <td><xsl:value-of select="."/></td>
	                </xsl:otherwise>
	              </xsl:choose>
	          </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
	 </root> 	
	</xsl:template>
</xsl:stylesheet>