<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
	    <xsl:for-each select="/root/tbody/tr[position()=1]/td">
	        <th nowrap='true'>
	       		<xsl:value-of select="."/>  
	        </th>	    
	     </xsl:for-each>
     
        <!-- th nowrap='true'>职位名称</th>
        <th nowrap='true'>描述</th>
        <th nowrap='true'>职务类别</th>
        <th nowrap='true'>职务等级</th>
        <th nowrap='true'>职务级别</th>
        <th nowrap='true'>职务名称</th>
        <th nowrap='true'>职位编制</th>
        <th nowrap='true'>职位人数</th>
        <th nowrap='true'>所属部门</th-->
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position()!=1]">
        <tr>
          <xsl:for-each select="td"> 
            <td>
              <!-- <xsl:if test="contains(.,'/upload/') and contains(.,'jpg')"> -->
              <xsl:if test="contains(.,'/upload/') and (contains(.,'jpg') or contains(.,'bmp'))">
                <a>
                  <xsl:attribute name="href">javascript:openDialog(window.prefix+'<xsl:value-of select="."/>','dialogWidth:800px;dialogHeight:600px',null,'true');</xsl:attribute>
                    查看</a>
              </xsl:if>
              <xsl:if test="not(contains(.,'/upload/') and (contains(.,'jpg') or contains(.,'bmp')))">
                <xsl:value-of select="."/>
              </xsl:if>
              <!-- <xsl:value-of select="."/> -->  
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

