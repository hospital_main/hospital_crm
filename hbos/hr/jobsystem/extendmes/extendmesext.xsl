<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th></th>
	    <xsl:for-each select="/root/tbody/tr[position()=1]/td">
	        <th nowrap='true'>
	       		<xsl:value-of select="."/>  
	        </th>	    
	     </xsl:for-each>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position()!=1]">
        <tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td"> 
            <td>
              <xsl:if test="contains(.,'/upload/') and (contains(.,'jpg') or contains(.,'bmp'))">
                <a>
                  <xsl:attribute name="href">javascript:openDialog(window.prefix+'<xsl:value-of select="."/>','dialogWidth:800px;dialogHeight:600px',null,'true');</xsl:attribute>
                    �鿴</a>
              </xsl:if>
              <xsl:if test="not(contains(.,'/upload/') and (contains(.,'jpg') or contains(.,'bmp')))">
                <xsl:value-of select="."/>
              </xsl:if>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

