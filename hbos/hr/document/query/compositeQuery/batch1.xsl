<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th></th>
	    <xsl:for-each select="/root/tbody/tr[position()=1]/td">
	        <th nowrap='true'>
	       		<xsl:value-of select="."/>  
	        </th>	    
	     </xsl:for-each>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position()!=1]">
        <tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td"> 
            <td>
              <xsl:value-of select="."/>  
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

