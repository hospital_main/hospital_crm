<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		<xsl:for-each select="/root/t2head/tr">
			<tr noWrap="true" class="mainHead">
				<xsl:for-each select="th">
					<xsl:if test="position()=1">
						<th noWrap="true">
							<input type='checkbox' TABINDEX='-1' style='font-size:8px;display:none1'>
						      <xsl:attribute name="value">
						          <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							  </xsl:attribute>
							</input>
						</th>
						<th noWrap="true">
							<xsl:value-of select="." />
						</th>
					</xsl:if>
					<xsl:if test="position()!=1">
						<th noWrap="true">
							<xsl:value-of select="." />
						</th>
					</xsl:if>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</thead>
    
    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
		    <tr>
			    <td align='center'>
			    	<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
				      <xsl:attribute name="value">
				          <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					  </xsl:attribute>
					</input>
			    </td>
			    <xsl:for-each select="td"> 
					<td noWrap='true'>
						<xsl:if test="contains(.,'/upload/') and contains(.,'jpg')">
							<a>
							<xsl:attribute name="href">#</xsl:attribute>
							<xsl:attribute name="onclick">IsExistsFile("<xsl:value-of select="."/>")</xsl:attribute>
							�鿴	</a>
						</xsl:if>
						<xsl:if test="not(contains(.,'/upload/') and contains(.,'jpg'))">
							<xsl:value-of select="."/>
						</xsl:if>
					</td>
			    </xsl:for-each>
		    </tr>
	    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>