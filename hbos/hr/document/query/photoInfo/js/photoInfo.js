	  var showstop=0,showlower=0,texpands=1;
      //全局变量 职工基本信息empidvalue
      var empidvalue="";
      //条件说明cond_desc 条件列表cond_list 条件内容cond
      var cond_desc = "";
      var cond_list = "";
      var cond = "";
      
      //条件名称
      var cond_title = "";
      
      function init(){//初始化左侧树
      	condTable2.style.display="none";
        var pk=getPk();
        unitTree.expand=1;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        unitTree.init();
      }
      
      function loadData(isLeaf,pk,parameter,label){//点击数节点时触发，用于加载右侧的列表数据
        //document.getElementById("firstNode").style.display='';
        //alert(isLeaf+":"+pk+":"+parameter+":"+label);
        
        var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
		xmlDoc.async="false";  
		xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + pk + "</root>");
		//从PK中取得dcode sign
		dept_code.value = xmlDoc.getElementsByTagName("dcode")[0].firstChild.nodeValue;
		sign.value = xmlDoc.getElementsByTagName("sign")[0].firstChild.nodeValue;
		inlower.value = xmlDoc.getElementsByTagName("inlower")[0].firstChild.nodeValue;
		userid.value = getUserID();
		hr_empDocumentPhotoInfo_select.click();
        //editTable.submit(hr_empDocumentBasicInfoMain_select,result);
      }
      
      function show_isstop(t){
        showstop=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
      }
      
      function include_lower(t){
        showlower=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
      }
      
      function show_all(t){
        var pk=getPk();
        if(t.checked){
          texpands=10;
          unitTree.expand=10;
          unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        }else{
          texpands=1;
          unitTree.expand=1;
          unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        }
      }
      
      function getPk(){
        return "<comp_code>"+getCompCode()+"</comp_code>"+"<user_id>"+getUserID()+"</user_id>"
               +"<show_duty>1</show_duty>"+"<show_stop>"+showstop+"</show_stop>"
               +"<include_lower>"+showlower+"</include_lower>"+"<is_super>0</is_super>"
               +"<show_count>0</show_count>";
      }
      
      //按钮权限查询
      function getPurview(){
      	window.xmlhttp.post('hr_empDocumentBasicInfoMain_buttonPurview', "<tableid>sys_emp</tableid><userid>"+getUserID()+"</userid>");
		var resText = window.xmlhttp._object.responseXml;
      	
      	var addPerm = resText.getElementsByTagName("td")[0].firstChild.nodeValue;
      	var editPerm = resText.getElementsByTagName("td")[1].firstChild.nodeValue;
      	var deletePerm = resText.getElementsByTagName("td")[2].firstChild.nodeValue;
      	var photoPerm = resText.getElementsByTagName("td")[3].firstChild.nodeValue;
      	
      }
      
      //判断result复选框 互斥
      var checkFlag=-1;    //记录第一次选择的复选框的行号
	  function judgeCheck(){
	  	if(result.getRows()>0){
		  	var count = 0;
	      	var data = result.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(checkFlag==-1){
			        		checkFlag=i;
			        	}else{
			        		if(checkFlag!=i){
						    	result.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag].getElementsByTagName("input")[0].checked=false;
			        			checkFlag=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0) checkFlag=-1;  //恢复复选框行号记录初始值
	    }
	  }
	
      
      //刷新结果集
      function refreshParent(){
      	if(unitTree.style.display==''){
      		//alert("科室职位目录树");
      		window.document.body.all['hr_empDocumentBasicInfoMain_select'].click();
      	}else{
      		//alert("条件控件");
      		judgeCommonCondListCheck();
      	}
	  }
	  
	  
	  //科室职位目录按钮
	  function deptDutyList(){
	  	unitTree.style.display='';
	  	condTable.style.display='none';
	  	condTable2.style.display='none';
	  	commonCondDispaly.value='';
	  	treeCheckBox.style.display="";
	  	//清空职工列表
        result.innerHTML = "";
        //判断职工功能按钮的权限
	  	getPurview(); 
	  }
	  
	  //条件查询按钮 
	  function condSelect(){
	  	unitTree.style.display='none';
	  	condTable.style.display='';
	  	condTable2.style.display='none';
	  	treeCheckBox.style.display="none";
	  	commonCondSelectByUserid.value = getUserID();
        //清空职工列表
        result.innerHTML = "";
        //判断职工功能按钮的权限
        getPurview();  
	  	hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  }
	  
	  function condSelect2(){
	  	
	  	unitTree.style.display='none';
	  	condTable.style.display='none';
	  	condTable2.style.display='';
	  	
	  	cond_item_list.style.display="";
	  	user_id.value = getUserID();
	  	
	  	commonCondSelectByUserid.value = getUserID();
	  	treeCheckBox.style.display="none";
        //// 清空职工列表
         result.innerHTML = "";
	  		
	  }
	  
	  
	  //判断commonCondList复选框 互斥 给条件显示框赋值具体条件
      var commonCondListCheckFlag=-1;    //记录第一次选择的复选框的行号
	  function judgeCommonCondListCheck(){
	  	if(commonCondList.getRows()>0){
		  	var count = 0;
	      	var data = commonCondList.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(commonCondListCheckFlag==-1){
			        		commonCondListCheckFlag=i;
			        	}else{
			        		if(commonCondListCheckFlag!=i){
						    	commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
			        			commonCondListCheckFlag=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0){ 
	    		commonCondListCheckFlag=-1;  //恢复复选框行号记录初始值
	    		commonCondDispaly.value = "";  //清空条件显示框
				//清空职工列表
				result.innerHTML = "";
	    	}else{
	    		//给条件显示框赋值具体条件
				commonCondDispaly.value = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				//按常用条件查询职工
				var userid_cond = getUserID();
				var common_cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
				var common_cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
				var common_cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				
				//赋值给全局变量
				cond=common_cond;
				cond_list=common_cond_list;
				cond_desc=common_cond_desc;
				
				common_cond = common_cond.replace(/</g,"&lt;");
				common_cond = common_cond.replace(/>/g,"&gt;");
				
				window.xmlhttp.post('hr_empDocumentBasicInfoMain_picture', "<userid_cond>"+userid_cond+"</userid_cond><common_cond>"+common_cond+"</common_cond><common_cond_list>"+common_cond_list+"</common_cond_list>");
				var resText = window.xmlhttp._object.responseXml;
				
				var srcTree = new ActiveXObject("Microsoft.XMLDOM");
				srcTree.async=false;
				srcTree.load(xmlhttp._object.responseXML);
				var errNode = srcTree.selectSingleNode("/root/error");
				if(errNode.text){
					alert(errNode.text);
					return;
				}
				
				result.setDataXml(resText.xml);
	    	}
	    	getPurview();  //判断职工功能按钮的权限
	    }
	  }
	  
	  
	  function cond_query(obj)
	  {

	  	window.xmlhttp.post('hr_empDocumentBasicInfoMain_picture2', "<cond_item_list>"+cond_item_list.value+"</cond_item_list><cond_rep_list>"+cond_rep_list.value+"</cond_rep_list><cond_value>"+cond_value.value+"</cond_value><user_id>"+user_id.value+"</user_id>");
				var resText = window.xmlhttp._object.responseXml;
				
				var srcTree = new ActiveXObject("Microsoft.XMLDOM");
				srcTree.async=false;
				srcTree.load(xmlhttp._object.responseXML);
				var errNode = srcTree.selectSingleNode("/root/error");
				if(errNode.text){
					alert(errNode.text);
					return;
				}
				result.setDataXml(resText.xml);	  	
	  }
	  
	  
	  function newCondOk(common_cond,common_cond_list){
		  var userid_cond = getUserID();
		  window.xmlhttp.post('hr_empDocumentBasicInfoMain_picture', "<userid_cond>"+userid_cond+"</userid_cond><common_cond>"+common_cond+"</common_cond><common_cond_list>"+common_cond_list+"</common_cond_list>");
		  var resText = window.xmlhttp._object.responseXml;
		  result.setDataXml(resText.xml);
	  }
	  //新条件 在页面顶定义三个全局变量 条件说明cond_desc 条件列表cond_list 条件内容cond
	  function newCond(){
	  		//把全局变量清空
	  		cond_desc = "";
	  		cond_list = "";
	  		cond = "";
	  		//清空条件显示框
			commonCondDispaly.value = "";
			//清空职工列表
			result.innerHTML = "";
			//判断职工功能按钮的权限
        	getPurview();  
	  		if(commonCondListCheckFlag!=-1){  //判断是否选择了常用条件
		  		//把列表commonCondList的复选框选择去掉
		  		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
		  		//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1;  
		  		
			}
  			openIFDialog(window,'cond.html?load=<cond_type>new</cond_type>','dialogWidth:520px;dialogHeight:500px');
	  }
	  
	  //修改条件
	  function updateCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请选择条件！！！");
	  		return ;
	  	}
	  	/*
		  		//条件说明cond_desc 条件列表cond_list 条件内容cond
		  		cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
		  		cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
		  		cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
		  		//把列表commonCondList的复选框选择去掉
		  		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
		  		//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1;  
		  		//清空条件显示框
				commonCondDispaly.value = "";
				//清空职工列表
				result.innerHTML = "";
		*/
		cond = cond.replace(/</g,"小于");
		cond = cond.replace(/>/g,"大于");
		cond = cond.replace(/&lt;/g,"小于");
		cond = cond.replace(/&gt;/g,"大于");
		//判断职工功能按钮的权限
		getPurview();  
  		openIFDialog(window,'cond.html?load=<cond_type>update</cond_type><cond_desc>'+cond_desc+'</cond_desc><cond_list>'+cond_list+'</cond_list><cond>'+cond+'</cond>','dialogWidth:520px;dialogHeight:500px');
	  }
	  
	  //添加新条件后判断按钮的显示
	  function getPurviewAfterInsertCond(){
	  		getPurview(); 
	  }

	  //保存条件 输入条件名称
	  function saveCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
	  		openIFDialog(window,'../../../condName.html','dialogWidth:400px;dialogHeight:200px');
	  	}
	  }
	  
	  //保存条件
	  function saveCond1(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
  			if(cond_title==null || cond_title.length==0){
  				alert("条件名称为空！！！");
  				return false;
  			}else{
				cond = cond.replace(/</g,"&lt;");
				cond = cond.replace(/>/g,"&gt;");
	  			//getUserID() 条件名称cond_title 条件说明cond_desc 条件列表cond_list 条件内容cond
	  			window.xmlhttp.post('hr_empDocumentBasicInfoMain_insertCond', "<user_id>"+getUserID()+"</user_id><cond_title>"+cond_title+"</cond_title><cond_desc>"+cond_desc+"</cond_desc><cond_list>"+cond_list+"</cond_list><cond>"+cond+"</cond>");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空职工列表
				result.innerHTML = "";
				//判断职工功能按钮的权限
	        	getPurview();  
	  			//刷新常用条件
	  			hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  		}
	  	}
	  }
	  
	  //删除条件
	  function deleteCond(){
	  	if(commonCondListCheckFlag==-1){
	  		alert("请选择一个常用条件！！！");
	  		return false;
	  	}else{
	  		if(confirm('确定删除该常用条件吗？')){
		  		var cond_id = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[2].firstChild.nodeValue;
		  		window.xmlhttp.post('hr_empDocumentBasicInfoMain_deleteCond', "<cond_id>"+cond_id+"</cond_id>");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
			  	commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空职工列表
				result.innerHTML = "";
				//判断职工功能按钮的权限
	        	getPurview();  
	  			//刷新常用条件
	  			hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  		}else{
	  			return false;
	  		}
	  	}
	  }
	//图片批量处理
	  function photoBatchHandle(){
	  	
	  	openDialog('photoBatchHandle.html', 'dialogWidth:700px;dialogHeight:520px');
	  }
	  