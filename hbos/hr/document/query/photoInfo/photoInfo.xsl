<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <tbody>
      <tr>
	  <xsl:for-each select="/root/tbody/tr">
		<xsl:if test="position() mod 6 = 1">
			<xsl:text disable-output-escaping="yes"><![CDATA[</tr>]]></xsl:text>
		    <xsl:text disable-output-escaping="yes"><![CDATA[<tr>]]></xsl:text>
	    </xsl:if>
	     <td noWrap='true' align="center">
		  <table>
		    <xsl:for-each select="td">
					<xsl:if test="contains(.,'/upload/') and (contains(.,'GIF') or contains(.,'jpg') or contains(.,'JPG') or contains(.,'bmp') or contains(.,'BMP'))">
						<tr>
						<td align="center">
						 <img>
						 	<xsl:attribute name="src"><xsl:value-of select="."/></xsl:attribute>
						 	<xsl:attribute name="width">92</xsl:attribute>
						 	<xsl:attribute name="height">128</xsl:attribute>
						 </img>
						 </td>
						 </tr>
					</xsl:if>
					<xsl:if test="not(contains(.,'/upload/') and (contains(.,'GIF') or contains(.,'jpg') or contains(.,'JPG') or contains(.,'bmp') or contains(.,'BMP')))">
						<xsl:if test="position() = 2">
							<xsl:text disable-output-escaping="yes"><![CDATA[<tr><td align="center">]]></xsl:text>
							<xsl:value-of select="."/>
						</xsl:if>
						<xsl:if test="position() = 3">
							<xsl:value-of select="." />
							<xsl:text disable-output-escaping="yes"><![CDATA[</td></tr>]]></xsl:text>
						</xsl:if>
					</xsl:if>
		    </xsl:for-each>
		   </table>
		  </td>
	  </xsl:for-each>
	  </tr>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

