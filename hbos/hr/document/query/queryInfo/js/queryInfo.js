	  var showstop=0,showlower=0,texpands=1;
      //全局变量 职工基本信息empidvalue
      var empidvalue="";
      //条件说明cond_desc 条件列表cond_list 条件内容cond
      var cond_desc = "";
      var cond_list = "";
      var cond = "";
      var cond_title = "";
      
      function init(){//初始化左侧树
      	condTable2.style.display="none";
        var pk=getPk();
        unitTree.expand=1;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        userid.value=getUserID();
        rid.value='';
        condID.value='';
        cond_listID.value='';
        unitTree.init();
      }
      
      function loadData(isLeaf,pk,parameter,label){//点击数节点时触发，用于加载右侧的列表数据
        //document.getElementById("firstNode").style.display='';
        //alert(isLeaf+":"+pk+":"+parameter+":"+label);
        
        var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
		xmlDoc.async="false";  
		xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + pk + "</root>");
		//从PK中取得dcode sign
		var dept_code = xmlDoc.getElementsByTagName("dcode")[0].firstChild.nodeValue;
		var sign = xmlDoc.getElementsByTagName("sign")[0].firstChild.nodeValue;
		var inlower = xmlDoc.getElementsByTagName("inlower")[0].firstChild.nodeValue;
		//判断部门还是职位
		if(sign=="dept")
		{
			//判断是否含下级部门
			if(showlower=="0")
			{
				cond="sys_dept.dept_code='"+dept_code+"'";
				cond_list="sys_dept";
			}
			else
			{
				cond="sys_dept.dept_code like '"+dept_code+"%'";
				cond_list="sys_dept";
			}
			//cond="sys_dept.dept_code like '"+dept_code+"%'";
			//cond_list="sys_dept";
		}
		else
		{
			cond="sys_emp.duty_code='"+dept_code+"'";
		}
		condID.value=cond;
		cond_listID.value=cond_list;
		if(rid.value!='')
		{
			hr_empDocumentQueryInfo_select.click();
		}
        //editTable.submit(hr_empDocumentBasicInfoMain_select,result);
      }
      
  	  //设置方案
      function setScheme()
  	  {
  		  openIFDialog(window,'setScheme.html','dialogWidth:1000px;dialogHeight:600px');
  	  }
      //选择方案显示查询结果
      var cond_flag = 0;
      function scheme_change()
      {
      	
      	if(cond_flag==1)
      	{
      		return;
      	}
    	  condID.value=cond;
    	  cond_listID.value=cond_list;
    	  if(schemeID.value!='')
    	  {
    		  rid.value=schemeID.value;
    		  
    		  hr_empDocumentQueryInfo_select.click();
    	  }
      }
      
      function show_isstop(t){
        showstop=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
      }
      
      function include_lower(t){
        showlower=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
      }
      
      function show_all(t){
        var pk=getPk();
        if(t.checked){
          texpands=10;
          unitTree.expand=10;
          unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        }else{
          texpands=1;
          unitTree.expand=1;
          unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        }
      }
      
      function getPk(){
        return "<comp_code>"+getCompCode()+"</comp_code>"+"<user_id>"+getUserID()+"</user_id>"
               +"<show_duty>1</show_duty>"+"<show_stop>"+showstop+"</show_stop>"
               +"<include_lower>"+showlower+"</include_lower>"+"<is_super>0</is_super>"
               +"<show_count>0</show_count>";
      }
      
      
      //判断result复选框 互斥
      var checkFlag=-1;    //记录第一次选择的复选框的行号
	  function judgeCheck(){
	  	if(result.getRows()>0){
		  	var count = 0;
	      	var data = result.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(checkFlag==-1){
			        		checkFlag=i;
			        	}else{
			        		if(checkFlag!=i){
						    	result.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag].getElementsByTagName("input")[0].checked=false;
			        			checkFlag=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0) checkFlag=-1;  //恢复复选框行号记录初始值
	    }
	  }
	  
	  
	  
	  //// 条件查询
	  
	  function cond_query(obj)
	  {
	  	//alert(scheme_id.value);
	  	if (scheme_id.value=='')
	  	{
	  		alert("请先选择查询方案！");
	  		return;
	  	}
	  	rid_id.value = scheme_id.value;
	  	if(checkFormByFieldType()){
	  		condTable2.submit(obj,result,true);
	  	}
	  	
	  }
	
	  
	  //科室职位目录按钮
	  function deptDutyList(){
	  	cond_flag=0;
	  	unitTree.style.display='';
	  	condTable.style.display='none';
	  	condTable2.style.display='none';
	  	commonCondDispaly.value='';
	  	treeCheckBox.style.display="";
	  	//清空职工列表
	  	result.innerHTML = "";
	  }
	  
	  //条件查询按钮 
	  function condSelect(){
	  	cond_flag=0;
	  	unitTree.style.display='none';
	  	condTable.style.display='';
	  	condTable2.style.display='none';
	  	commonCondSelectByUserid.value = getUserID();
	  	treeCheckBox.style.display="none";
	  		result.innerHTML = "";
        //清空职工列表
        // result.innerHTML = "";
	  	hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  }
	  
	  function condSelect2(){
	  	cond_flag=1;
	  	unitTree.style.display='none';
	  	condTable.style.display='none';
	  	condTable2.style.display='';
	  	
	  	cond_item_list.style.display="";
	  	user_id.value = getUserID();
	  	
	  	commonCondSelectByUserid.value = getUserID();
	  	treeCheckBox.style.display="none";
        //// 清空职工列表
         result.innerHTML = "";
	  		
	  }
	  
	  
	  
	  //判断commonCondList复选框 互斥 给条件显示框赋值具体条件
      var commonCondListCheckFlag=-1;    //记录第一次选择的复选框的行号
	  function judgeCommonCondListCheck(){
	  	if(commonCondList.getRows()>0){
		  	var count = 0;
	      	var data = commonCondList.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(commonCondListCheckFlag==-1){
			        		commonCondListCheckFlag=i;
			        	}else{
			        		if(commonCondListCheckFlag!=i){
						    	commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
			        			commonCondListCheckFlag=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0){ 
	    		commonCondListCheckFlag=-1;  //恢复复选框行号记录初始值
	    		commonCondDispaly.value = "";  //清空条件显示框
				//清空职工列表
				result.innerHTML = "";
			  	cond='';
			  	cond_list='';
			  	condID.value='';    
		  	    cond_listID.value='';
	    	    if(schemeID.value!='')
	    	    {
	    	  	    rid.value=schemeID.value;
	    	    }
	    	    else
	    	    {
	    	    	return ;
	    	    }
	    	    hr_empDocumentQueryInfo_select.click();
	    	}else{
	    		//给条件显示框赋值具体条件
				commonCondDispaly.value = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				//按常用条件查询职工
				var userid_cond = getUserID();
				var common_cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
				var common_cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
				var common_cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				//赋值给全局变量
				cond=common_cond;
				cond_list=common_cond_list;
				cond_desc=common_cond_desc;
				condID.value=common_cond;
				cond_listID.value=common_cond_list;
				if(rid.value=='')
				{
					rid.value=schemeID.value;
					if(rid.value!='')
					{
						hr_empDocumentQueryInfo_select.click();
					}
				}
				else
				{
					hr_empDocumentQueryInfo_select.click();
				}
	    	}
	    }
	  }
	  
	  //新条件 在页面顶定义三个全局变量 条件说明cond_desc 条件列表cond_list 条件内容cond
	  function newCond(){
	  		//把全局变量清空
	  		cond_desc = "";
	  		cond_list = "";
	  		cond = "";
	  		//清空条件显示框
			commonCondDispaly.value = "";
			//清空职工列表
			//result.innerHTML = "";
			//判断职工功能按钮的权限
	  		if(commonCondListCheckFlag!=-1){  //判断是否选择了常用条件
		  		//把列表commonCondList的复选框选择去掉
		  		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
		  		//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1;  
		  		
			}
  			openIFDialog(window,'../cond/cond2.html?load=<cond_type>new</cond_type>','dialogWidth:520px;dialogHeight:500px');
	  }
	  
	  //修改条件
	  function updateCond(){
		  	if(commonCondDispaly.value.length==0){
		  		alert("条件为空，请选择条件！！！");
		  		return ;
		  	}
  			/*
	  		//条件说明cond_desc 条件列表cond_list 条件内容cond
	  		cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
	  		cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
	  		cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
	  		//把列表commonCondList的复选框选择去掉
	  		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
	  		//恢复复选框行号记录初始值
	  		commonCondListCheckFlag=-1;  
	  		//清空条件显示框
			commonCondDispaly.value = "";
			//清空职工列表
			//result.innerHTML = "";
			*/
			cond = cond.replace(/</g,"小于");
			cond = cond.replace(/>/g,"大于");
			cond = cond.replace(/&lt;/g,"小于");
			cond = cond.replace(/&gt;/g,"大于");
			openIFDialog(window,'../cond/cond.html?load=<cond_type>update</cond_type><cond_desc>'+cond_desc+'</cond_desc><cond_list>'+cond_list+'</cond_list><cond>'+cond+'</cond>','dialogWidth:520px;dialogHeight:500px');
	  }
	  
	  //保存条件 输入条件名称
	  function saveCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
	  		openIFDialog(window,'../../../condName.html','dialogWidth:400px;dialogHeight:200px');
	  	}
	  }
	  
	  //保存条件
	  function saveCond1(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
  			if(cond_title==null || cond_title.length==0){
  				alert("条件名称为空！！！");
  				return false;
  			}else{
				cond = cond.replace(/</g,"&lt;");
				cond = cond.replace(/>/g,"&gt;");
	  			//getUserID() 条件名称cond_title 条件说明cond_desc 条件列表cond_list 条件内容cond
	  			window.xmlhttp.post('hr_empDocumentBasicInfoMain_insertCond', "<user_id>"+getUserID()+"</user_id><cond_title>"+cond_title+"</cond_title><cond_desc>"+cond_desc+"</cond_desc><cond_list>"+cond_list+"</cond_list><cond>"+cond+"</cond>");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空职工列表
				//result.innerHTML = "";
	  			//刷新常用条件
	  			hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  		}
	  	}
	  }
	  
	  //删除条件
	  function deleteCond(){
	  	if(commonCondListCheckFlag==-1){
	  		alert("请选择一个常用条件！！！");
	  		return false;
	  	}else{
	  		if(confirm('确定删除该常用条件吗？')){
		  		var cond_id = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[2].firstChild.nodeValue;
		  		window.xmlhttp.post('hr_empDocumentBasicInfoMain_deleteCond', "<cond_id>"+cond_id+"</cond_id>");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
			  	commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空职工列表
				result.innerHTML = "";
	  			//刷新常用条件
	  			hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  		}else{
	  			return false;
	  		}
	  	}
	  }
	  