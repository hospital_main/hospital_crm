<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
  		
    	<thead>
	     <tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	      <tr noWrap='true' class="mainHead">> 
					<xsl:for-each select="/root/t2head/tr/th"> 
						    <td noWrap="true">
									<xsl:value-of select="." />
								</td>
						 
					</xsl:for-each>
    		</tr>
     
  	</thead>
  	
  	<tbody>
      <xsl:for-each select="/root/tbody/tr[td[1]]">
      	 
			<tr>
        	 
          <xsl:for-each select="td[position()]">
             <xsl:choose>
								<xsl:when test="position()=1">
		              <td>
		                <a tabindex='-1'><xsl:value-of select="."/></a>
		              </td>
		            </xsl:when>
		            <xsl:otherwise>
									<td noWrap='true'>
										<xsl:if test="contains(.,'/upload/') and contains(.,'jpg')">
											<a>
											<xsl:attribute name="href">javascript:openDialog(window.prefix+'<xsl:value-of select="."/>','dialogWidth:800px;dialogHeight:600px',null,'true');</xsl:attribute>
											�鿴	</a>
										</xsl:if>
										<xsl:if test="not(contains(.,'/upload/') and contains(.,'jpg'))">
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
								</xsl:otherwise>
							</xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>

  