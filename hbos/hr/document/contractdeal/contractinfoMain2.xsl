<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		<xsl:for-each select="/root/t2head/tr">
			<tr noWrap="true" class="mainHead">
				<th style='display:none' width='25'><input type='checkbox'/></th>
				<xsl:for-each select="th">
					<th noWrap="true">
						<xsl:value-of select="." />
					</th>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</thead>
    
    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
		    <tr>
			    <td align='center'>
			    	<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
				      <xsl:attribute name="value">
				          <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					  </xsl:attribute>
					</input>
			    </td>
			    <xsl:for-each select="td"> 
			    <xsl:choose>
					    	<xsl:when test="position()=1">
				              <td>
				                <a tabindex='-1' href='#'>
				                	 <xsl:attribute name="onclick">
															openDetailItems("<xsl:value-of select="."/>")
					  								</xsl:attribute>
				                	<xsl:value-of select="."/></a>
				              </td>
				        </xsl:when>
								<xsl:when test="position()=3">
		              <td>
		                <a tabindex='-1'><xsl:value-of select="."/></a>
		              </td>
		            </xsl:when>
		            <xsl:when test="position()=last()">
		              <td>
		                <a href='#'><xsl:attribute name="onclick" >
							      openDialog('docUpload.html?load=&lt;onFileUploaded&gt;onFileUploaded&lt;/onFileUploaded&gt;&lt;pactId&gt;'+<xsl:value-of select="../pk/rid"/>+'&lt;/pactId>&gt;', 'dialogWidth:500px;dialogHeight:200px', null)
							    	</xsl:attribute>
							    	上传</a>
							    	<xsl:if test=".!=''">
							    		　
							    		<a href='#'>
							    			<xsl:attribute name="onclick" >deleteDoc('<xsl:value-of select="../pk/rid"/>')</xsl:attribute>
							    			删除
							    		</a>
							    		　
							    		<a href='#'>
							    			<xsl:attribute name="onclick" >downLoad('<xsl:value-of select="."/>')</xsl:attribute>							    		
							    			查看
							    		</a>
							    	</xsl:if>
		              </td>
		            </xsl:when>
		            <xsl:otherwise>
									<td noWrap='true'>
										<xsl:if test="contains(.,'/upload/') and contains(.,'jpg')">
											<a>
											<xsl:attribute name="href">javascript:openDialog(window.prefix+'<xsl:value-of select="."/>','dialogWidth:800px;dialogHeight:600px',null,'true');</xsl:attribute>
											查看	</a>
										</xsl:if>
										<xsl:if test="not(contains(.,'/upload/') and contains(.,'jpg'))">
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
								</xsl:otherwise>
							</xsl:choose>
							
					
			    </xsl:for-each>
		    </tr>
	    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

