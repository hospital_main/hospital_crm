
//条件说明cond_desc 条件列表cond_list 条件内容cond
var cond_desc = "";
var cond_list = "";
var cond = "";

//条件名称
var cond_title = "";

//合同rid集合
var ridList = "";

function init() {
	//条件查询按钮 
	commonCondSelectByUserid.value = getUserID();
	hr_empDocumentBasicInfoMain_commonCondSelect.click();
	
	//setiframebody('hr_empDocumentContractinfoMain_showFields','<userid>'+getUserID()+'</userid>','','',new insertMess(""));
	//contractinfoFieldList.style.display="none";
}

//判断commonCondList复选框 互斥 给条件显示框赋值具体条件
var commonCondListCheckFlag = -1;    //记录第一次选择的复选框的行号
function judgeCommonCondListCheck() {
	if (commonCondList.getRows() > 0) {
		var count = 0;
		var data = commonCondList.getElementsByTagName("tbody")[1];
		var trs = data.getElementsByTagName("tr");
		for (var i = 0; i < trs.length; i++) {
			var inputs = trs[i].getElementsByTagName("input");
			for (var j = 0; j < inputs.length; j++) {
				if (inputs[j].type == "checkbox" && inputs[j].checked) {
					count++;
					if (commonCondListCheckFlag == -1) {
						commonCondListCheckFlag = i;
					} else {
						if (commonCondListCheckFlag != i) {
							if (commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag] != null) {
								commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked = false;
							}
							commonCondListCheckFlag = i;
						}
					}
				}
			}
		}
		if (count == 0) {
			commonCondListCheckFlag = -1;  //恢复复选框行号记录初始值
			commonCondDispaly.value = "";  //清空条件显示框
			result.innerHTML="";
			//contractinfoFieldList.style.display="none";
		} else {
			//contractinfoFieldList.style.display="";
  			//给条件显示框赋值具体条件
			commonCondDispaly.value = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
			//按常用条件查询职工
			var userid_cond = getUserID();
			var common_cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
			var common_cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
			var common_cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
			cond_desc = common_cond_desc;				
			//给全局变量赋值
			cond = common_cond;
			cond_list = common_cond_list;
	
			//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  
			common_cond = common_cond.replace(/</g, "&lt;");
			common_cond = common_cond.replace(/>/g, "&gt;");
			cond_list = common_cond_list;
			cond = common_cond;
			cond_desc = common_cond_desc;
	
			//window.xmlhttp.post('hr_empDocumentContractinfoMain_findEmpByCommonCond', "<userid_cond>"+userid_cond+"</userid_cond><common_cond>"+common_cond+"</common_cond><common_cond_list>"+common_cond_list+"</common_cond_list>");
			//var resText = window.xmlhttp._object.responseXml;
			//result.setDataXml(resText.xml);
			//王楠修改，因为setDataXml方法不能设分页，所以只能用editTable1提交方式查询
			userid_cond1.value = userid_cond;
			common_cond1.value = common_cond;
			common_cond_list1.value = common_cond_list;
			hr_empDocumentContractinfoMain_findContractinfoByCommonCond.click();
			
			//合同信息字段的加载
			//loadField();
		}
	}
}

//合同信息添加字段加载
//function loadField(){}

//页面信息是否显示
function getPurviewByCond(){
	if(result.getRows()==0){
  		result.innerHtml="";
  	}else{
	  	commonCondListCheckFlag=-1;
  	} 
}

//新条件 在页面顶定义三个全局变量 条件说明cond_desc 条件列表cond_list 条件内容cond
function newCond() {
	//把全局变量清空
	cond_desc = "";
	cond_list = "";
	cond = "";
	//清空条件显示框
	commonCondDispaly.value = "";
	//清空页面上的相关信息
	result.innerHTML = "";  //清空合同列表
	//contractinfoFieldList.style.display="none";
	
	if(commonCondListCheckFlag != -1) {  //判断是否选择了常用条件
		//把列表commonCondList的复选框选择去掉
		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked = false;
		//恢复复选框行号记录初始值
		commonCondListCheckFlag = -1;
	}
	openIFDialog(window, "batchCond.html?load=<cond_type>new</cond_type><module>Contractinfo</module>", "dialogWidth:520px;dialogHeight:500px");
}

//修改条件
function updateCond(){
	if(commonCondDispaly.value.length==0){
		alert("条件为空，请选择条件！！！");
		return false;
	}else{
		/*if(commonCondListCheckFlag==-1){
			alert("请选择一个常用条件！！！");
			return false;
			}else{*/
	 		//条件说明cond_desc 条件列表cond_list 条件内容cond
	 		//cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
	 		//cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
	 		//cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
	 		//把列表commonCondList的复选框选择去掉
	 		//commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
	 		//恢复复选框行号记录初始值
	 		//commonCondListCheckFlag=-1;  
	 		//清空条件显示框
			//commonCondDispaly.value = "";
			//清空页面上的相关信息
			//clearRelationInfo(); 
					
			//处理特殊字符 "<"、">" 变成 "小于"、"大于"  
			cond = cond.replace(/</g,"小于");
			cond = cond.replace(/>/g,"大于");
			//处理特殊字符 "&lt;"、"&gt;" 变成 "小于"、"大于"  
			cond = cond.replace(/&lt;/g,"小于");
			cond = cond.replace(/&gt;/g,"大于");
		
	 		openIFDialog(window,'batchCond.html?load=<cond_type>update</cond_type><module>Contractinfo</module><cond_desc>'+cond_desc+'</cond_desc><cond_list>'+cond_list+'</cond_list><cond>'+cond+'</cond>','dialogWidth:520px;dialogHeight:500px');
 		//}
	}
}

//保存条件 输入条件名称
function saveCond(){
	if(commonCondDispaly.value.length==0){
		alert("条件为空，请新建条件！！！");
		return false;
	}else{
		openIFDialog(window,'../../condName.html','dialogWidth:400px;dialogHeight:200px');
	}
}
//保存条件
function saveCond1(){
	if(commonCondDispaly.value.length==0){
		alert("条件为空，请新建条件！！！");
		return false;
	}else{
		//var cond_title = window.prompt("请输入输入常用条件名称！！！","");
		if(cond_title==null || cond_title.length==0){
			alert("条件名称为空！！！");
			return false;
		}else{
			//getUserID() 条件名称cond_title 条件说明cond_desc 条件列表cond_list 条件内容cond
			
			//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  
			cond = cond.replace(/</g,"&lt;");
			cond = cond.replace(/>/g,"&gt;");
	
			window.xmlhttp.post('hr_empDocumentBasicInfoMain_insertCond', "<user_id>"+getUserID()+"</user_id><cond_title>"+cond_title+"</cond_title><cond_desc>"+cond_desc+"</cond_desc><cond_list>"+cond_list+"</cond_list><cond>"+cond+"</cond>");
			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
			//恢复复选框行号记录初始值
 			commonCondListCheckFlag=-1; 
			//清空条件显示框
			commonCondDispaly.value = "";
			//清空页面上的相关信息
			result.innerHTML = "";  //清空职工列表
	   		//contractinfoFieldList.style.display="none"; //不显示批量签订按钮
			//刷新常用条件
			hr_empDocumentBasicInfoMain_commonCondSelect.click();
		}
	}
}

//删除条件
function deleteCond(){
	if(commonCondListCheckFlag==-1){
		alert("请选择一个常用条件！！！");
		return false;
	}else{
		if(confirm('确定删除该常用条件吗？')){
	 		var cond_id = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[2].firstChild.nodeValue;
	 		window.xmlhttp.post('hr_empDocumentBasicInfoMain_deleteCond', "<cond_id>"+cond_id+"</cond_id>");
			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
			//恢复复选框行号记录初始值
  			commonCondListCheckFlag=-1; 
			//清空条件显示框
			commonCondDispaly.value = "";
			//清空页面上的相关信息
 			result.innerHTML = "";  //清空职工列表
	   		//contractinfoFieldList.style.display="none"; //不显示批量签订按钮
			//刷新常用条件
			hr_empDocumentBasicInfoMain_commonCondSelect.click();
		}else{
			return false;
		}
	}
}

//批量修改
function updateBatch(){
	if(result.getRows()>0){
		var count = 0;
  		var data = result.getElementsByTagName('tbody')[1];
		var trs = data.getElementsByTagName("tr");
		for (var i=0; i<trs.length; i++){
			var inputs =  trs[i].getElementsByTagName("input");
			for (var j=0; j<inputs.length; j++) {	        
				if (inputs[j].type == 'checkbox' && inputs[j].checked) {
	      			count++;
	    		}
 			}
		}
		if (count == 0) {
			alert('请选择要修改的记录!');
			return false;
	    }
		if (count == 1) {
			alert('至少选择2条以上的记录!');
			return false;
	    }
		openIFDialog(window,'updateBatchFieldList.html','dialogWidth:600px;dialogHeight:600px');
	}
}
//批量修改页面需要的参数
function insertMess(id){
	this.pk={id:'rid',value:"updateBatch##"+"hr_pct_pact"};
	this.button={text:'保存',name:'hr_empDocumentContractinfoMain_updateBatch',onclick:'if(judgeRidCount()){if(insertBatchContractinfo(editTable,ridList)){if(window.parentPage.checkFlag!=-1){window.parentPage.refreshParent();}closePage();}}',accessKey:'S'};
}
//批量修改页面需要的参数
function insertCallbacks(){
	setInsertPage('hr_empDocumentContractinfoBatchUpdateMain_showFields','<userid>'+getUserID()+'</userid>','','',new insertMess(""));
}
//刷新结果集
function refreshParent(){
	window.document.body.all['hr_empDocumentContractinfoMain_findContractinfoByCommonCond'].click();
}
function checkDelete(ridList){
				subbody.load="hr_empDocumentContractinfoMain_checkDelete";
				subbody.para="<rid>" + ridList + "</rid>";
				subbody.post();
				var p=subbody.getOneDim()[0];
				if (p == 0){
					alert("存在不是最后一条合同的记录!");
					return false;
				}
				return true;
			}
//批量删除
var paraTotal="";
var ridList="";
function deleteBatch(){
	if(result.getRows()>0){
		paraTotal="";
		ridList="";
		var count = 0;
  		var data = result.getElementsByTagName('tbody')[1];
		var trs = data.getElementsByTagName("tr");
		for (var i=0; i<trs.length; i++){
			var inputs =  trs[i].getElementsByTagName("input");
			for (var j=0; j<inputs.length; j++) {	        
				if (inputs[j].type == 'checkbox' && inputs[j].checked) {
					var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
					xmlDoc.async="false";  
					xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + inputs[j].value + "</root>"); 
					ridList = ridList + xmlDoc.getElementsByTagName("rid")[0].firstChild.nodeValue + ",";
	      			count++;
	    		}
 			}
		}
		ridList = ridList.substring(0,ridList.length-1);
		if (count == 0) {
			alert('请选择要修改的记录!');
			return false;
	    }
		if (count == 1) {
			alert('至少选择2条以上的记录!');
			return false;
	    }
	  if(!checkDelete(ridList)) return;
	  
		  paraTotal = paraTotal + "<rid>" + ridList + "</rid><pk_rid>deleteBatch##hr_pct_pact</pk_rid>";
	    window.xmlhttp.post('hr_empDocumentContractinfoMain_deleteBatch', paraTotal);
	    if(!doMsg(window.xmlhttp._object.responseText)){
	    	return false;
	    }else{
			if(window.xmlhttp._object.responseXml.getElementsByTagName("error")[0].firstChild!=null){
				alert(window.xmlhttp._object.responseXml.getElementsByTagName("error")[0].firstChild.nodeValue);
			}
			window.document.body.all['hr_empDocumentContractinfoMain_findContractinfoByCommonCond'].click();
			return true;
		}
	}
}


