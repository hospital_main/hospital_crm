      var showstop=0,showlower=0,texpands=1;
      //全局变量 职工附属信息ridvalue
      var ridvalue="";
      //条件说明cond_desc 条件列表cond_list 条件内容cond
      var cond_desc = "";
      var cond_list = "";
      var cond = "";
      
       var cond_item  = "";
				var cond_rep  = "";
				var condvalue  = "";
      
      //定义变量 用于批量添加页面判断是通过树查询职工还是通过条件查询职工
      var findEmpByTreeOrCond = "tree";
      
      //条件名称
      var cond_title = "";
	        
      function init(){//初始化左侧树
      	condTable2.style.display="none";
        var pk=getPk();
        unitTree.expand=1;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        unitTree.init();
      }
      
      function loadData(isLeaf,pk,parameter,label){//点击数节点时触发，用于加载右侧的列表数据
        var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
		xmlDoc.async="false";  
		xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + pk + "</root>"); 
		//从PK中取得dcode sign
		 
		dept_code.value = xmlDoc.getElementsByTagName("dcode")[0].firstChild.nodeValue;
		sign.value = xmlDoc.getElementsByTagName("sign")[0].firstChild.nodeValue;
		inlower.value = xmlDoc.getElementsByTagName("inlower")[0].firstChild.nodeValue;
		userid.value = getUserID();
        hr_empDocumentContractinfoMain_selectEmp.click();
      }

      function show_isstop(t){
        showstop=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
      }
      
      function include_lower(t){
        showlower=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
      }
      
      function show_all(t){
        var pk=getPk();
        if(t.checked){
          texpands=10;
          unitTree.expand=10;
          unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        }else{
          texpands=1;
          unitTree.expand=1;
          unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        }
      }
      
      function getPk(){
        return "<comp_code>"+getCompCode()+"</comp_code>"+"<user_id>"+getUserID()+"</user_id>"
               +"<show_duty>1</show_duty>"+"<show_stop>"+showstop+"</show_stop>"
               +"<include_lower>"+showlower+"</include_lower>"+"<is_super>0</is_super>"
               +"<show_count>0</show_count>";
      }
      
      //判断按钮是否显示
      function getInsertBatchPurview(){
      	if(result.getRows()==0){
      		result.innerHtml="";
      		addBatchPermButton.style.display="none";
      	}else{
      		addBatchPermButton.style.display="";
    	  	checkFlag=-1;
    	  	selectFlag=true;
      	} 
      	noDisplayBaseButton();
      	//result2.innerHTML="";
      	result2.style.display="none"; //隐藏result2
      }
      
      //判断result复选框 互斥
      var checkFlag=-1;    //记录第一次选择的复选框的行号
      var selectFlag=true; //判断是否需要查询
	  function judgeCheck(){
	  	var count = 0;
      	var data = result.getElementsByTagName('tbody')[1];
    	var trs = data.getElementsByTagName("tr");
    	for (var i=0; i<trs.length; i++){
    		var inputs =  trs[i].getElementsByTagName("input");
    		for (var j=0; j<inputs.length; j++) {	        
		        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
		        	count++;
		        	if(checkFlag==-1){
		        		checkFlag=i;
		        		selectFlag=true;
		        	}else{
		        		if(checkFlag!=i){
		        			if(result.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag]!=null){
					    		result.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag].getElementsByTagName("input")[0].checked=false;
					    	}
		        			checkFlag=i;
		        			selectFlag=true;
		        		}else{
		        			selectFlag=false;
		        		}
		        	}
		        }
	    	}
    	}
    	if(count==0) checkFlag=-1;  //恢复复选框行号记录初始值
	  }
      
      //向checkBox的onclick事件中加入查询方法
      function addFunctionToCheckBox(){
	      	var data = result.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr"); 
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox') {
			        	inputs[j].attachEvent('onclick',getContractinfoList);
			        }
		    	}
	    	}
      }
      
      //当点击职工信息列表时查询对应的合同信息
      function getContractinfoList(){
      	if(result.getRows()==0){
      		checkFlag=-1;  //如果result记录数为零，恢复复选框行号记录初始值。
      	}else{
	      	judgeCheck();
	      	if(!selectFlag) return false;  //如果result选择未发生改变就不查询数据库
	      	var empidTemp = "";
	      	var count = 0;
	      	var data = result.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr"); 
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	//得到选择的职工 empid
			        	var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
						xmlDoc.async="false";  
						xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + inputs[j].value + "</root>"); 
						empidTemp = xmlDoc.getElementsByTagName("empid")[0].firstChild.nodeValue;
			        	count++;
			        }
		    	}
	    	}
	    	//判断是否选择职工列表中的记录
	    	if (count==0) {
	    	  alert('请选择一条职工记录!');
	    	  result2.innerHTML="";  //清空result2
	    	  noDisplayBaseButton();  //如果没有选择职工记录，按钮不显示
		      return false;
		    }
		    if (count > 1) {
		      alert('只能选择一条职工记录!');
		      result2.innerHTML="";
		      return false;
		    }
		    contractInfoUserid.value = getUserID();
		    contractInfoEmpid.value = empidTemp;
		    hr_empDocumentContractinfoMain_select.click();
		    result2.style.display="";  //显示result2
      	}
      	result2.adjPosition();
      }
      
      //按钮权限查询
      function getPurview(){
      	window.xmlhttp.post('hr_empDocumentContractinfoMain_buttonPurview', "<tableid>hr_pct_pact</tableid><userid>"+getUserID()+"</userid>");
		var resText = window.xmlhttp._object.responseXml;
      	
      	var addPerm = resText.getElementsByTagName("td")[0].firstChild.nodeValue;
      	var editPerm = resText.getElementsByTagName("td")[1].firstChild.nodeValue;
      	var deletePerm = resText.getElementsByTagName("td")[2].firstChild.nodeValue;
      	var photoPerm = resText.getElementsByTagName("td")[3].firstChild.nodeValue;
      	
      	if(addPerm!='0'){
      		addPermButton.style.display="";
      	}else{
      		addPermButton.style.display="none";
      	}
      	if(editPerm!='0' && result2.getRows()>0){
      		updatePermButton.style.display="";
      	}else{
      		updatePermButton.style.display="none";
      	}
      	if(deletePerm!='0' && result2.getRows()>0){
      		deletePermButton.style.display="";
      	}else{
      		deletePermButton.style.display="none";
      	}
      	if(photoPerm!='0' && result2.getRows()>0){
      		photoPermButton.style.display="";
      	}else{
      		photoPermButton.style.display="none";
      	}
      }
      
      //判断result2复选框 互斥
      var checkFlag2=-1;    //记录第一次选择的复选框的行号
	  function judgeCheck2(){
	  	if(result2.getRows()==0){
      		checkFlag2=-1;//如果result2记录数为零，恢复复选框行号记录初始值。
      	}else{
		  	var count = 0;
	      	var data = result2.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(checkFlag2==-1){
			        		checkFlag2=i;
			        	}else{
			        		if(checkFlag2!=i){
			        			if(result2.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag2]!=null){
						    		result2.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag2].getElementsByTagName("input")[0].checked=false;
						    	}
			        			checkFlag2=i;
			        		}
			        	}
			        }
		    	}
	    	}
    		if(count==0) checkFlag2=-1;  //恢复复选框行号记录初始值
   		}
	  }
      
      //添加
      function insert(){
	  	ridvalue="";
      	openIFDialog(window,'insert.html','dialogWidth:600px;dialogHeight:600px')
	  }
	  //添加页面需要的参数
      function insertMess(id){
			this.pk={id:'rid',value:id+"##"+"hr_pct_pact"};
			this.button={text:'保存',name:'hr_empDocumentContractinfoMain_insert',onclick:'editTable.submit(this);window.parentPage.refreshParent();',accessKey:'S'};
	  }
	  //添加页面需要的参数
	  function insertCallbacks(){
				setInsertPage('hr_empDocumentContractinfoMain_showFields','<userid>'+getUserID()+'</userid>','',"<rid>"+ridvalue+"</rid><userid>"+getUserID()+"</userid>",new insertMess(ridvalue));
	  }
	  
	  //批量添加
	  function insertBatch(){
	  	ridvalue="";
	  
	  	openIFDialog(window,'insertBatch.html?load=<dept_code>'+dept_code.value+'</dept_code><sign>'+sign.value+'</sign><inlower>'+inlower.value+'</inlower><userid>'+getUserID()+'</userid>','dialogWidth:1000px;dialogHeight:600px')
	  }
	  //批量添加页面需要的参数
      function insertBatchMess(id){
			this.pk={id:'rid',value:id+"##"+"hr_pct_pact"};
			this.button={text:'保存',name:'hr_empDocumentContractinfoMain_insertBatch',onclick:'if(judgeEmpCount()){insertBatchContractinfo(editTable,empidList);if(window.parentPage.checkFlag!=-1){window.parentPage.refreshParent();}}',accessKey:'S'};
	  }
	  //批量添加页面需要的参数
	  function insertBatchCallbacks(){
				setInsertBatchPage('hr_empDocumentContractinfoMain_showFields','<userid>'+getUserID()+'</userid>','',"<rid>"+ridvalue+"</rid><userid>"+getUserID()+"</userid>",new insertBatchMess(ridvalue));
	  }
	  
	  //批量调整
	  function adjustBatch(){
	  		openIFDialog(window,'adjustBatch.html','dialogWidth:900px;dialogHeight:600px');
	  }
	  
	  //修改
	  function update(){
      	var count = 0;
      	var data = result2.getElementsByTagName('tbody')[1];
    	var trs = data.getElementsByTagName("tr"); 
    	for (var i=0; i<trs.length; i++){
    		var inputs =  trs[i].getElementsByTagName("input");
    		for (var j=0; j<inputs.length; j++) {	        
		        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
		        	//得到要修改的记录的rid
		        	var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
					xmlDoc.async="false";  
					xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + inputs[j].value + "</root>"); 
					ridvalue = xmlDoc.getElementsByTagName("rid")[0].firstChild.nodeValue;
		        	count++;
		        }
	    	}
    	}
    	   	
    	window.xmlhttp.post('hr_empDocumentContractinfoMain_checkstate', "<pactrid>"+ridvalue+"</pactrid>",'?isCheck=false');
			var resText = window.xmlhttp._object.responseXml;
			
      	
      var pactstate = resText.getElementsByTagName("td")[0].firstChild.nodeValue;
      
      if (pactstate==2||pactstate==3)
      {
      alert("终止或解除的合同不能变更！");
      return;
    	}
               	
      	
    	if (count == 0) {
	      alert('请选择要修改的记录!');
	      return false;
	    }
	    if (count > 1) {
	      alert('只能选择一条记录进行修改!');
	      return false;
	    }
	    
      	openDialog('update.html','dialogWidth:600px;dialogHeight:600px')
      }
      //修改页面需要的参数
      function updateMess(id){
			this.pk={id:'rid',value:id};
			this.button={text:'保存',name:'hr_empDocumentContractinfoMain_update',onclick:'editTable.submit(this);window.parentPage.refreshParent();',accessKey:'S'};
	  }
	  //修改页面需要的参数
	  function updateCallbacks(){
				setUpdatePage('hr_empDocumentContractinfoMain_update_list','<userid>'+getUserID()+'</userid>','hr_empDocumentContractinfoMain_update',"<rid>"+ridvalue+"</rid><userid>"+getUserID()+"</userid>",new updateMess(ridvalue));
	  }
	  
	  //刷新结果集
      function refreshParent(){
		window.document.body.all['hr_empDocumentContractinfoMain_select'].click();
	  }
	  
	  //删除
	  function deleteItems(obj){
      	var count = 0;
      	var data = result2.getElementsByTagName('tbody')[1];
    	var trs = data.getElementsByTagName("tr"); 
    	for (var i=0; i<trs.length; i++){
    		var inputs =  trs[i].getElementsByTagName("input");
    		for (var j=0; j<inputs.length; j++) {	        
		        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
		        	count++;
		        }
	    	}
    	}
    	if (count == 0) {
	      alert('请选择要删除的记录!');
	      return false;
	    }
	    if(confirm("确认要删除选中的记录吗？")){
			result2.submit_choose(obj.name,"<userid>"+getUserID()+"</userid>");
			result2.refresh();
	    }
	    
      }
      
      //图片处理
      function photoHandle(){
      	var count = 0;
      	var data = result2.getElementsByTagName('tbody')[1];
    	var trs = data.getElementsByTagName("tr");
    	
    	var ridtemp = ""; 
    	
    	for (var i=0; i<trs.length; i++){
    		var inputs =  trs[i].getElementsByTagName("input");
    		for (var j=0; j<inputs.length; j++) {	        
		        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
		        	//得到要修改的记录的rid
		        	var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
					xmlDoc.async="false";  
					xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + inputs[j].value + "</root>"); 
					ridtemp = xmlDoc.getElementsByTagName("rid")[0].firstChild.nodeValue;
		        	count++;
		        }
	    	}
    	}
    	if (count == 0) {
	      alert('请选择记录!');
	      return false;
	    }
	    if (count > 1) {
	      alert('只能选择一条记录进行修改!');
	      return false;
	    }
	    rid.value = ridtemp;
	  	openDialog('photoHandle.html', 'dialogWidth:700px;dialogHeight:520px');
      }
      
      //不显示合同信息的添加、更新、删除、图片处理按钮
      function noDisplayBaseButton(){
      	  addPermButton.style.display="none";
	   	  updatePermButton.style.display="none";
	   	  deletePermButton.style.display="none";
	   	  photoPermButton.style.display="none";
      }
      
      //科室职位目录按钮
	  function deptDutyList(){
	  	treeTable.style.display='';
	  	unitTree.style.display='';
	  	condTable.style.display='none';
	  	condTable2.style.display='none';
	  	//清空页面上的相关信息
	  	clearRelationInfo();
	  	//批量更新页面判断通过树还是条件查询职工
	  	findEmpByTreeOrCond = "tree";
	  }
	  
	  //清空页面上的相关信息
	  function clearRelationInfo(){
        result.innerHTML = "";  //清空职工列表
   		checkFlag=-1;  //初始化result的选择初始值
   		selectFlag=true;  //初始化 判断是否需要查询变量
   		checkFlag2=-1;    //初始化result2的选择初始值
   		noDisplayBaseButton();  //不显示合同信息功能按钮
   		addBatchPermButton.style.display="none"; //不显示批量签订按钮
   		result2.style.display="none"; //隐藏result2
	  }
	  
	  //条件查询按钮 
	  function condSelect(){
	  	treeTable.style.display='none';
	  	unitTree.style.display='none';
	  	condTable.style.display='';
	  	condTable2.style.display='none';
	  	//清空页面上的相关信息
	  	clearRelationInfo(); 
	  	//批量更新页面判断通过树还是条件查询职工
	  	findEmpByTreeOrCond = "cond";
	  	
	  	commonCondSelectByUserid.value = getUserID();
	  	hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  }
	  
	  	  
	  function condSelect2(){	  	
	  	unitTree.style.display='none';
	  	condTable.style.display='none';
	  	condTable2.style.display='';
	  		  	
	  	cond_item_list.style.display="";
	  	user_id.value = getUserID();
	  	
	  	commonCondSelectByUserid.value = getUserID();
	  	treeTable.style.display="none";
        //// 清空职工列表
      //批量更新页面判断通过树还是条件查询职工
	  	findEmpByTreeOrCond = "cond2";
         result.innerHTML = "";
	  		
	  }
	  
	  
	  
	  //判断commonCondList复选框 互斥 给条件显示框赋值具体条件
      var commonCondListCheckFlag=-1;    //记录第一次选择的复选框的行号
	  function judgeCommonCondListCheck(){
	  	if(commonCondList.getRows()>0){
		  	var count = 0;
	      	var data = commonCondList.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(commonCondListCheckFlag==-1){
			        		commonCondListCheckFlag=i;
			        	}else{
			        		if(commonCondListCheckFlag!=i){
			        			if(commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag]!=null){
						    		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
						    	}
			        			commonCondListCheckFlag=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0){ 
	    		commonCondListCheckFlag=-1;  //恢复复选框行号记录初始值
	    		commonCondDispaly.value = "";  //清空条件显示框
				//清空页面上的相关信息
	  			clearRelationInfo(); 
	    	}else{
	    		//给条件显示框赋值具体条件
				commonCondDispaly.value = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				//按常用条件查询职工
				var userid_cond = getUserID();
				var common_cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
				var common_cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
				var common_cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				cond_desc = common_cond_desc;				
				//给全局变量赋值
				cond = common_cond;
				cond_list = common_cond_list;
				 
				//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  
				common_cond = common_cond.replace(/</g,"&lt;");
				common_cond = common_cond.replace(/>/g,"&gt;");
				
				cond_list = common_cond_list;
				cond = common_cond;
				cond_desc = common_cond_desc;
				
				//window.xmlhttp.post('hr_empDocumentContractinfoMain_findEmpByCommonCond', "<userid_cond>"+userid_cond+"</userid_cond><common_cond>"+common_cond+"</common_cond><common_cond_list>"+common_cond_list+"</common_cond_list>");
				//var resText = window.xmlhttp._object.responseXml;
				//result.setDataXml(resText.xml);
				//王楠修改，因为setDataXml方法不能设分页，所以只能用editTable1提交方式查询
				userid_cond1.value = userid_cond;
				common_cond1.value = common_cond;
				common_cond_list1.value = common_cond_list;
				hr_empDocumentContractinfoMain_findEmpByCommonCond.click();
				
				getInsertBatchPurview();  //判断合同信息功能按钮的权限
	    	}
	    }
	  }
	  
	  //新条件 在页面顶定义三个全局变量 条件说明cond_desc 条件列表cond_list 条件内容cond
	  function newCond(){
	  		//把全局变量清空
	  		cond_desc = "";
	  		cond_list = "";
	  		cond = "";
	  		//清空条件显示框
			commonCondDispaly.value = "";
			//清空页面上的相关信息
	  		clearRelationInfo(); 
	  		if(commonCondListCheckFlag!=-1){  //判断是否选择了常用条件
		  		//把列表commonCondList的复选框选择去掉
		  		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
		  		//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1;  
			}
  			openIFDialog(window,'../cond/cond.html?load=<cond_type>new</cond_type><module>Contractinfo</module>','dialogWidth:520px;dialogHeight:500px');
	  }
	  
	  //修改条件
	  function updateCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请选择条件！！！");
	  		return false;
	  	}else{
	  		/*if(commonCondListCheckFlag==-1){
	  			alert("请选择一个常用条件！！！");
	  			return false;
	  		}else{*/
		  		//条件说明cond_desc 条件列表cond_list 条件内容cond
		  		//cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
		  		//cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
		  		//cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
		  		//把列表commonCondList的复选框选择去掉
		  		//commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
		  		//恢复复选框行号记录初始值
		  		//commonCondListCheckFlag=-1;  
		  		//清空条件显示框
				//commonCondDispaly.value = "";
				//清空页面上的相关信息
	  			//clearRelationInfo(); 
	  			
	  			//处理特殊字符 "<"、">" 变成 "小于"、"大于"  
				cond = cond.replace(/</g,"小于");
				cond = cond.replace(/>/g,"大于");
				//处理特殊字符 "&lt;"、"&gt;" 变成 "小于"、"大于"  
				cond = cond.replace(/&lt;/g,"小于");
				cond = cond.replace(/&gt;/g,"大于");
				
		  		openIFDialog(window,'../cond/cond.html?load=<cond_type>update</cond_type><module>Contractinfo</module><cond_desc>'+cond_desc+'</cond_desc><cond_list>'+cond_list+'</cond_list><cond>'+cond+'</cond>','dialogWidth:520px;dialogHeight:500px');
		  		
		  		cond = cond.replace(/小于/g,"&lt;");
		  		cond = cond.replace(/大于/g,"&gt;");
		  		
		  	//}
	  	}
	  }
	  
	  //添加新条件后判断按钮的显示
	  function getPurviewAfterInsertCond(){
	  		getInsertBatchPurview(); 
	  }
	  
	  //保存条件 输入条件名称
	  function saveCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
	  		openIFDialog(window,'../../condName.html','dialogWidth:400px;dialogHeight:200px');
	  	}
	  }
	  //保存条件
	  function saveCond1(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
  			//var cond_title = window.prompt("请输入输入常用条件名称！！！","");
  			if(cond_title==null || cond_title.length==0){
  				alert("条件名称为空！！！");
  				return false;
  			}else{
	  			//getUserID() 条件名称cond_title 条件说明cond_desc 条件列表cond_list 条件内容cond
	  			
	  			//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  
				cond = cond.replace(/</g,"&lt;");
				cond = cond.replace(/>/g,"&gt;");
				
	  			window.xmlhttp.post('hr_empDocumentBasicInfoMain_insertCond', "<user_id>"+getUserID()+"</user_id><cond_title>"+cond_title+"</cond_title><cond_desc>"+cond_desc+"</cond_desc><cond_list>"+cond_list+"</cond_list><cond>"+cond+"</cond>");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空页面上的相关信息
	  			clearRelationInfo(); 
	  			//刷新常用条件
	  			hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  		}
	  	}
	  }
	  
	  //删除条件
	  function deleteCond(){
	  	if(commonCondListCheckFlag==-1){
	  		alert("请选择一个常用条件！！！");
	  		return false;
	  	}else{
	  		if(confirm('确定删除该常用条件吗？')){
		  		var cond_id = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[2].firstChild.nodeValue;
		  		window.xmlhttp.post('hr_empDocumentBasicInfoMain_deleteCond', "<cond_id>"+cond_id+"</cond_id>");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
			  	commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空页面上的相关信息
		  		clearRelationInfo(); 
	  			//刷新常用条件
	  			hr_empDocumentBasicInfoMain_commonCondSelect.click();
  			}else{
  				return false;
  			}
	  	}
	  }
	  function clickCond(){
	  	 
	  	  cond_item = cond_item_list.value;
				 cond_rep = cond_rep_list.value;
				 condvalue = cond_value.value; 
			  
	  }
	  
	  