<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<td width="40">
            		<input type='checkbox' TABINDEX='-1'  onclick="selectAll(this)"></input>
         		</td>
				<th  >明细编码</th>
				<th>明细名称</th>
				<th>明细条件</th> 
			</tr> 
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr><td align='center'>
            <input type='checkbox' TABINDEX='-1'  onclick="">
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
					<xsl:for-each select="td">
												
						<td><xsl:choose>
								<xsl:when test="position() = 1">
									<a href="#" onclick="openDialog('detail_update.html?load='+this.parentNode.parentNode.firstChild.firstChild.value,'dialogWidth:600px;dialogHeight:380px');">										
										<xsl:value-of select="."/>
									</a>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/> 
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>