<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
       <xsl:for-each select="/root/tbody/tr[position()=1]">
		   <tr noWrap='true' class='mainHead'>
				<!-- <th style='display:none'><input type='checkbox'/></th>-->
		  		<xsl:for-each select="td">
					<th noWrap='true'>
						<xsl:value-of select="."/>
					</th>
				</xsl:for-each>
		    </tr>
      	</xsl:for-each> 
    </thead>
    
    <tbody>
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
		    <tr>
			    <xsl:for-each select="td"> 
					<td noWrap='true' align="center">
							<xsl:value-of select="format-number(.,'#,##0')"/>
					</td>
			    </xsl:for-each>
		    </tr>
	    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

