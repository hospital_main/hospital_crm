<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
   <root>
    
    	<thead>
    	<!--
       	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
	  		<tr noWrap="true" class="mainHead">
					<td>标题</td>
					<td>制单日期</td>
					<td>开始日期</td>
					<td>结束日期</td>
					<td>操作人</td>
			</tr>
	-->
	 		<tr noWrap="true" class="mainHead">
	 			<xsl:for-each select="/root/t2head/tr">
					<xsl:for-each select="th">
	              		<td nowrap='true'><xsl:value-of select="."/></td>
					</xsl:for-each>
				</xsl:for-each>
	 		</tr>
   		</thead>
   
	  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr">
	  			<tr>
						<xsl:for-each select="td">
			        <td noWrap='true'>
								<xsl:if test="contains(.,'/upload/') and contains(.,'jpg')">
									<a>
									<xsl:attribute name="href">#</xsl:attribute>
									<xsl:attribute name="onclick">IsExistsFile("<xsl:value-of select="."/>")</xsl:attribute>
									查看	</a>
								</xsl:if>
								<xsl:if test="not(contains(.,'/upload/') and contains(.,'jpg'))">
									<xsl:value-of select="."/>
								</xsl:if>
							</td>
						</xsl:for-each>
					</tr>
			</xsl:for-each>
	  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>