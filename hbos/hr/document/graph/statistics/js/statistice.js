
function fusionchartsShow(gtype,gTitle,xmlData,xAxes,yAxes,isTipShow){
			var FusionChartsXML="";
			xAxes==null?xAxes="":xAxes;
			yAxes==null?yAxes="元":yAxes;
			var xmlDoc; 
			if(window.ActiveXObject){ 
				xmlDoc=new ActiveXObject("Microsoft.XMLDOM"); 
			}else if(document.implementation && document.implementation.createDocument){ 
				xmlDoc=document.implementation.createDocument("", "root", null); 
			} 
			
			xmlDoc.async="false";
			xmlDoc.loadXML(xmlData); 

			var chart;
			if(gtype==1){
				FusionChartsXML= encodeURI(Line(gTitle,xmlDoc,xAxes,yAxes,isTipShow));
			}else if(gtype==2){
				FusionChartsXML= encodeURI(Column(gTitle,xmlDoc,xAxes,yAxes,isTipShow));
			}else if(gtype==3){
				FusionChartsXML= encodeURI(Pie(gTitle,xmlDoc,isTipShow));
			}		
			window.FusionChartsXML=FusionChartsXML;
			//openEg('/base/scripts/FusionChartsShow.jsp?'+encodeURI('type='+gtype),800,423,false,false);//在屏幕中心弹出窗口
			//页面内容
			//var args = GetUrlParms();
			var win = window.dialogArguments;
			var xml=FusionChartsXML
			var type = encodeURI(gtype);
			if(xml!=null&&type!=null){
				var chart;
				if(type==1){
					chart =new FusionCharts("MSLine.swf", "ChartId", "100%", "100%", "0", "0");
				}else if(type==2){
					chart =new FusionCharts("MSColumn2D.swf", "ChartId", "100%", "100%", "0", "0");
				}else if(type==3){
					chart =new FusionCharts("Pie2D.swf", "ChartId", "100%", "100%", "0", "1");
				}
				chart.setDataXML(xml);   
				chart.render("FunsionChart_div");
			}else{
				alert("传入参数不足！");
			}

			function GetUrlParms(){
			    var args=new Object();   
			    var query=location.search.substring(1);//获取查询串   
			    query=decodeURI(query);
			    var pairs=query.split("&");//在逗号处断开   
			    for(var   i=0;i<pairs.length;i++){   
			        var pos=pairs[i].indexOf('=');//查找name=value   
			            if(pos==-1)   continue;//如果没有找到就跳过   
			            var argname=pairs[i].substring(0,pos);//提取name   
			            var value=pairs[i].substring(pos+1);//提取value   
			            args[argname]=unescape(value);//存为属性   
			    }
			    return args;
			}
}
