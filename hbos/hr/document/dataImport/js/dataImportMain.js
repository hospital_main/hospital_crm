var res=-1;//记录是否导入了文件
    		
//全局变量 记录导入的excel的行数和列数	
var cellARows = 0;
var cellACols = 0;

var fieldsXml = "";  //字段的xml串
var dataField = "";
var trsField = "";

var keyWordCount = 0; //关键字的数量
var uniqueFieldCount = 0; //唯一性校验字段的数量

var paraStrOfSelectFields = '';  //要导入的表的字段的下拉框字符串集合
   			
function init(){
	eval("dataCellA.Login('望海康信' , '', '13100104488', '3140-1444-7731-6004');");
	eval("dataCellA.LocalizeControl(0x804);");
	eval("dataCellB.Login('望海康信' , '', '13100104488', '3140-1444-7731-6004');");
	eval("dataCellB.LocalizeControl(0x804);");
	
	//设置dataCellB工作簿只读
	dataCellB.WorkbookReadonly = true;  
}
    		 
//选择文件
function loadDataFromFile(){
	
	//清除cellA的内容
	dataCellA.ResetContent();  
	
	res = dataCellA.ImportExcelDlg();
	
	if(res==1){
	
		//清除cellB的内容
		dataCellB.ResetContent();  
		
		cellARows = dataCellA.GetRows(0)-1;
		cellACols = dataCellA.GetCols(0)-1;
		
		if(cellARows!=1 && cellACols!=0){  //判断导入的文件是否有数据
			//让字段配置部分显示出来
			importTd.style.display="";
			
			//cell列与数据库字段匹配关系显示
			var importTableDivInnerHTML = "<table id='importTable' name='importTable' class='lineCtn' border=1>"
											+ "<tr><td align='right'>"
											+ "<b>要导入的表:<b><input id='tableid' name='tableid' type='text' class='inputSelect' load='hr_empDocumentDataImportMain_selectImportTable' para='<table_kind_id>hr_emp</table_kind_id>' defaultvalue='false' onchange='changeTable();'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
											+ "</td></tr>"
											+ "<tr id='keyWordTr' name='keyWordTr' style='display: none'></tr>"
											+ "<tr id='uniqueTr' name='uniqueTr' style='display: none'></tr>";
											
			//设置dataCellB 并把标题提出来放到与字段对应部分
			if(cellACols>20){ //如果列数大于20，设置dataCellB的列数，否则dataCellB中大于20的列不显示
				dataCellB.SetCols(cellACols+1,0);
			}
			for(var i=1;i<=cellARows;i++){
				for(var j=1;j<=cellACols;j++){
					dataCellB.S(j,i-1,0,dataCellA.GetCellString(j,i,0));
					dataCellB.SetColWidth(1,dataCellA.GetColWidth(1,j,0),j,0);
					//dataCellB.SetCellBackColor(j,i,0,255); //刷新dataCellB
					//设置dataCellB中的字段列表
					if(i==1){
						//如果列名称为空，说明该列无数据，不做字段对应
						if(dataCellA.GetCellString(j,i,0)=="" || dataCellA.GetCellString(j,i,0).length==0){
							importTableDivInnerHTML = importTableDivInnerHTML + "<tr style='display:none;'><td align='right'>" + dataCellA.GetCellString(j,i,0) + "对应:<input id='field_"+ j +"' name='field_"+ j +"' type='text' class='inputSelect'  load='hr_empDocumentDataImportMain_selectFieldsOfImportTable' para='<table_id></table_id>' defaultvalue='false' onchange='changeField(this.value," + j + ");'/>&nbsp;字段</td></tr>";
						}else{
							importTableDivInnerHTML = importTableDivInnerHTML + "<tr><td align='right'>" + dataCellA.GetCellString(j,i,0) + "对应:<input id='field_"+ j +"' name='field_"+ j +"' type='text' class='inputSelect'  load='hr_empDocumentDataImportMain_selectFieldsOfImportTable' para='<table_id></table_id>' defaultvalue='false' onchange='changeField(this.value," + j + ");'/>&nbsp;字段</td></tr>";
						}
					}
				}
			}
			dataCellB.ReDraw();  //刷新dataCellB
			importTableDivInnerHTML = importTableDivInnerHTML + "</table>";
			jhtcSetHtml(importTableDiv,importTableDivInnerHTML);
		}else{
			res = -1;
			alert("导入的文件没有数据！！！");
			return false;
		}
	}else{
		return false;
	}
}
    		
//更改表操作
function changeTable(){

	//查询表中所有字段标识、字段类型、有无相关字典表、是否必填
	window.xmlhttp.post('hr_empDocumentDataImportMain_selectAllFieldsByTable', "<tableid>"+tableid.value+"</tableid>");
	fieldsXml = window.xmlhttp._object.responseXml;;  //字段的xml串
	dataField = fieldsXml.getElementsByTagName('tbody')[0];
	trsField = dataField.getElementsByTagName('tr');
	//设置关键字
	if(tableid.value=="sys_emp"){
		keyWordCount = 0;  //记录唯一性字段的数量
		keyWordTr.style.display = "none";  //不显示关键字行
		var keyWordTrInnerHtml = "";
		jhtcSetHtml(keyWordTr,keyWordTrInnerHtml);
	}else{
		keyWordCount = trsField.length;  //记录唯一性字段的数量
		keyWordTr.style.display = "";  //显示关键字行
		var keyWordTrInnerHtml = "<td><table id='keyWordTable' name='keyWordTable'>";
		keyWordTrInnerHtml = keyWordTrInnerHtml + "<tr><td><b>关键字：</b></td><td><table>";
		//设定关键字
		for(var i=0;i<trsField.length;i++){
			var field_id = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[i].getElementsByTagName('td')[0].firstChild.nodeValue;
			var field_name = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[i].getElementsByTagName('td')[1].firstChild.nodeValue;
			if( (i%2)==0 ){  //如果单数行
				if(i==trsField.length-1){  //如果是最后一个
					keyWordTrInnerHtml = keyWordTrInnerHtml + "<tr><td>&nbsp;&nbsp;<input type='text' id='"+field_id+"-"+field_name+"' name='keyWord_"+(i+1)+"' label='' class='inputCheckBox' load=\"<root><item name='"+field_name+"' value='0'/></root>\"></td><td></td><tr>";
				}else{
					keyWordTrInnerHtml = keyWordTrInnerHtml + "<tr><td>&nbsp;&nbsp;<input type='text' id='"+field_id+"-"+field_name+"' name='keyWord_"+(i+1)+"' label='' class='inputCheckBox' load=\"<root><item name='"+field_name+"' value='0'/></root>\"></td>";
				}
			}else{  //如果双数行
				keyWordTrInnerHtml = keyWordTrInnerHtml + "<td>&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' id='"+field_id+"-"+field_name+"' name='keyWord_"+(i+1)+"' label='' class='inputCheckBox' load=\"<root><item name='"+field_name+"' value='0'/></root>\"></td></tr>";
			}
		}
		keyWordTrInnerHtml = keyWordTrInnerHtml + "</table></td></tr></table></td>";
		jhtcSetHtml(keyWordTr,keyWordTrInnerHtml);
	}
	
	
	//唯一性字段查询
	//window.xmlhttp.post('hr_empDocumentDataImportMain_selectUniqueFields', '<tableid>sys_dept</tableid>');
	window.xmlhttp.post('hr_empDocumentDataImportMain_selectUniqueFields', "<tableid>"+tableid.value+"</tableid>");
	var resText = window.xmlhttp._object.responseXml;
	var trs = resText.getElementsByTagName("tr");
	//设置唯一性字段
	if(trs.length>0){  //如果存在唯一性字段校验
		uniqueFieldCount = trs.length;  //记录唯一性字段的数量
		uniqueTr.style.display = "";  //显示唯一性字段
		var uniqueTrInnerHtml = "<td><table id='uniqueTable' name='uniqueTable'>";
		for(var i=0;i<trs.length;i++){
			var uniqueId = resText.getElementsByTagName("tr")[i].getElementsByTagName("td")[0].firstChild.nodeValue;
			var uniqueDesc = resText.getElementsByTagName("tr")[i].getElementsByTagName("td")[1].firstChild.nodeValue;
			
			uniqueTrInnerHtml = uniqueTrInnerHtml + "<tr><td><input type='text' id='"+uniqueId+"' name='uniqueField_"+(i+1)+"' label='表的唯一性校验" + (i+1) + "' class='inputCheckBox' load=\"<root><item name='"+uniqueDesc+"' value='0'/></root>\"></td></tr>";
			//uniqueTrInnerHtml = uniqueTrInnerHtml + "<tr><td>表的唯一性校验"+(i+1)+":"+uniqueDesc+"</td></tr>";
		}
		uniqueTrInnerHtml = uniqueTrInnerHtml + "</table></td>";
		jhtcSetHtml(uniqueTr,uniqueTrInnerHtml);
	}else{  //如果不存在唯一性字段校验
		uniqueFieldCount = 0;  //初始化唯一性校验的数量
		uniqueTr.style.display = "none";  //不显示唯一性字段
		var uniqueTrInnerHtml = "";
		jhtcSetHtml(uniqueTr,uniqueTrInnerHtml);
	}
	
	//设置对应的数据库字段下拉列表的para属性
	for(var i=1;i<=cellACols;i++){
		eval("field_" + i).para = "<table_id>" + tableid.value + "</table_id>";
		eval("field_" + i).refresh();
		for(var j=0;j<trsField.length;j++){
			var field_id = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[j].getElementsByTagName('td')[0].firstChild.nodeValue;
			var field_name = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[j].getElementsByTagName('td')[1].firstChild.nodeValue;
			if(field_name==dataCellB.GetCellString(i,0,0) && field_id!="comp_code"){
				eval("field_" + i).setValue(field_id);
				break;
			}
		}
	}
	/*paraStrOfSelectFields = '<root>';
	window.xmlhttp.post('hr_empDocumentDataImportMain_selectFieldsOfImportTable1', "<tableid>"+tableid.value+"</tableid>");
	var resOfSelectFields = window.xmlhttp._object.responseXml;
	var trsOfSelectFields = resOfSelectFields.getElementsByTagName("tr");
	for(var i=0;i<trsOfSelectFields.length;i++){
		var fieldCode = resOfSelectFields.getElementsByTagName("tr")[i].getElementsByTagName("td")[0].firstChild.nodeValue; 
		var fieldValue = resOfSelectFields.getElementsByTagName("tr")[i].getElementsByTagName("td")[1].firstChild.nodeValue;
		paraStrOfSelectFields = paraStrOfSelectFields + '<para code="'+fieldCode+'" value="'+fieldValue+'"/>';
	}
	paraStrOfSelectFields = paraStrOfSelectFields + '</root>';
	for(var i=1;i<=cellACols;i++){
		eval("field_" + i).setValue("");
		eval("field_" + i).initListByXML('<root><para code="" value=""/></root>');
		eval("field_" + i).initListByXML(paraStrOfSelectFields);
	}*/
}

//更改字段操作
function changeField(field_id,field_position){
	for(var i=1;i<=cellACols;i++){
		if(i!=field_position){  //判断是不是当前下拉列表
			if(eval("field_" + i).value.length!=0 && eval("field_" + i).value == field_id){ //如果选择的字段重复
				alert("选择的字段重复！！！");
				eval("field_" + field_position).refresh();
				/*eval("field_" + field_position).setValue("");
				eval("field_" + field_position).initListByXML('<root><para code="" value=""/></root>');
				eval("field_" + field_position).initListByXML(paraStrOfSelectFields);*/
				return false;
			}
		} 
	}
}
    		
//导入数据
function importData(){
	if(res!=1){
		alert("没有导入excel文件！！！");
		return false;
	}
	
	if(cellARows==1 || cellACols==0){
		alert("导入的数据为空！！！");
		return false;
	}
	
	if(tableid.value.length==0 || tableid.value==null){
		alert("请选择要导入的数据库表");
		return false;
	}
	
	//如果有关键字设置，判断是否选择至少一项
	if(keyWordCount!=0){  
		var keyWordChoiceCount = 0;
		for(var i=1;i<=keyWordCount;i++){
			if(eval("keyWord_"+i).value=="1"){
				keyWordChoiceCount = keyWordChoiceCount +1;
			}
		}
		if(keyWordChoiceCount==0){
			alert("至少选择一个关键字！！！");
			return false;
		}
	}
	
	//如果有唯一性校验，判断是否选择至少一项
	if(uniqueFieldCount>0){  
		var uniqueChoiceCount = 0;
		for(var i=1;i<=uniqueFieldCount;i++){
			if(eval("uniqueField_"+i).value=="1"){
				uniqueChoiceCount = uniqueChoiceCount +1;
			}
		}
		if(uniqueChoiceCount==0){
			alert("至少选择一个唯一性校验！！！");
			return false;
		}
	}
	
	//判断是否选择了字段的对应
	if(cellACols>0){
		var fieldChoiceCount = 0;
		for(var i=1;i<=cellACols;i++){
			if(eval("field_"+i).value.length!=0){
				fieldChoiceCount = fieldChoiceCount + 1;
			}
		}
		if(fieldChoiceCount==0){
			alert("至少选择一个对应的字段！");
			return false;
		}
	}
	
	//如果有唯一性校验，字段是否对应设置了唯一性校验的字段
	if(uniqueFieldCount>0){  
		for(var i=1;i<=uniqueFieldCount;i++){
			if(eval("uniqueField_"+i).value=="1"){
				for(var j=0;j<eval("uniqueField_"+i).id.split("-").length;j++){
					var tempUniqueField = eval("uniqueField_"+i).id.split("-")[j];
					var uniqueFieldChoiceFlag = false;
					for(var k=1;k<=cellACols;k++){
						if(eval("field_"+k).value==tempUniqueField || tempUniqueField=="comp_code"){
							uniqueFieldChoiceFlag = true;
							break;
						}
					}
					if(!uniqueFieldChoiceFlag){
						var tempUniqueFieldName = getValuePairBySql("hr_empDocumentDataImportMain_selectFieldNameByFieldId","<tableid>"+tableid.value+"</tableid><fieldid>"+tempUniqueField+"</fieldid>");
						alert("没有选择\""+tempUniqueFieldName[0]+"\",该列为指定了唯一性校验的列！");
						return false;
					}
				}
			}
		}
	}
	
	//判断是否选择了字段对应的 主键关联 或者 外键关联
	if(cellACols>0){
		if(tableid.value=="sys_emp"){
			var PKflag = false;  //主键选择标识
			for(var i=1;i<=cellACols;i++){
				if(eval("field_"+i).value=="emp_code"){
					PKflag = true;
					break;
				}
			}
			if(!PKflag){  //判断是否选择了字段对应的主键关联
				alert("请选择主键！！！(职工编码)");
				return false;
			}
		}else{
			var FKflag = false;  //外键选择标识
			for(var i=1;i<=cellACols;i++){
				if(eval("field_"+i).value.indexOf("emp_code")>-1){
					FKflag = true;
					break;
				}
			}
			if(!FKflag){  //判断是否选择了字段对应的外键关联
				alert("请选择外键的关联！！！(职工编码)");
				return false;
			}
		}
	}
	
	//判断是否选择了字段对应的关键字
	if(keyWordCount!=0){
		for(var i=1;i<=keyWordCount;i++){
			if(eval("keyWord_"+i).value=="1"){
				var keyWordChoiceFlag = false;  //字段是否选择对应的关键字的标识
				for(var j=1;j<=cellACols;j++){
					if( eval("field_"+j).value==eval("keyWord_"+i).id.split("-")[0] ){
					
						keyWordChoiceFlag = true;  //如果字段名跟关键字相等，就说明已经选择了关键字
						
						//判断选择了关键字对应的列是否为空
						for(var k=1;k<=cellARows-1;k++){
							if(dataCellB.GetCellString(j,k,0)==""){
								alert("第 " + k + " 行 " + " 第 " + j + "列\""+dataCellB.GetCellString(j,0,0)+"\"为关键字项，不能为空！！！");
								return false;
							}
						}
					}
					if(keyWordChoiceFlag){
						break;
					}
				}
				if(!keyWordChoiceFlag){
					alert("对应的字段没有选择的关键字 " + eval("keyWord_"+i).id.split("-")[1] + "！！！");
					return false;
				}
			}
		}
	}
	
	//判断选择唯一性校验对应的列是否为空
	if(uniqueFieldCount>0){  
		for(var i=1;i<=uniqueFieldCount;i++){
			if(eval("uniqueField_"+i).value=="1"){
				for(var j=0;j<eval("uniqueField_"+i).id.split("-").length;j++){
					var tempUniqueField = eval("uniqueField_"+i).id.split("-")[j];
					for(var k=1;k<=cellACols;k++){  //循环列
						if(eval("field_"+k).value==tempUniqueField){
							for(var l=1;l<=cellARows-1;l++){  //循环行
								if(dataCellB.GetCellString(k,l,0)==""){
									alert("第 " + l + " 行第 " + k + " 列\""+dataCellB.GetCellString(k,0,0)+"\"数据为空，此列设置了唯一性校验，请检查数据！！！");
									return false;
								}
							}
							break;
						}
					}
				}
			}
		}
	}
	
	//判断选择了外键对应的列是否为空
	for(var i=1;i<=cellACols;i++){	
		if(tableid.value=="sys_emp"){  
			if(eval("field_"+i).value=="emp_code"){
				for(var j=1;j<=cellARows-1;j++){
					if(dataCellB.GetCellString(i,j,0)==""){
						alert("第 " + j + " 行 " + " 第 " + i + " 列\""+dataCellB.GetCellString(i,0,0)+"\"为主键关联项，不能为空！！！");
						return false;
					}
				}
			}
		}else{
			if(eval("field_"+i).value.indexOf("emp_code")>-1){
				for(var j=1;j<=cellARows-1;j++){
					if(dataCellB.GetCellString(i,j,0)==""){
						alert("第 " + j + " 行 " + " 第 " + i + "列\""+dataCellB.GetCellString(i,0,0)+"\"为外键关联项，不能为空！！！");
						return false;
					}
				}
			}
		}
	}
	
	//如果有唯一性校验，校验唯一性字段是否重复
	if(cellARows-1>1){
		if(uniqueFieldCount>0){  
			for(var i=1;i<=uniqueFieldCount;i++){
				var colNumberStr = "";  //记录唯一性对应的字段的列号字符串集合
				if(eval("uniqueField_"+i).value=="1"){
					for(var j=0;j<eval("uniqueField_"+i).id.split("-").length;j++){
						var tempUniqueField = eval("uniqueField_"+i).id.split("-")[j];
						for(var k=1;k<=cellACols;k++){  //循环列
							if(eval("field_"+k).value==tempUniqueField){
								colNumberStr = colNumberStr + k + ",";
								break;
							}
						}
					}
				}
				colNumberStr = colNumberStr.substr(0, colNumberStr.length-1);
				if(colNumberStr!="" || colNumberStr.length!=0){
					var colNumberArray = colNumberStr.split(",");  //记录唯一性校验对应的字段的列号集合
					for(var j=1;j<=cellARows-1;j++){  //循环cell的所有行跟后面的行进行比较
						for(var j1=j+1;j1<=cellARows-1;j1++){
							var uniqueArray1 = "";  //记录唯一性校验相关的字段组合1
							var uniqueArray2 = "";  //记录唯一性校验相关的字段组合2
							for(var k=0;k<colNumberArray.length;k++){
								uniqueArray1 = uniqueArray1 + dataCellB.GetCellString(parseInt(colNumberArray[k]),j,0) + ",";
								uniqueArray2 = uniqueArray2 + dataCellB.GetCellString(parseInt(colNumberArray[k]),j1,0) + ",";
							}
							if(uniqueArray1==uniqueArray2){
								alert("第 " + j + " 行跟第 " + j1 + " 行的唯一性字段重复，请检查数据！！！");
								return false;
							}
						}
						if(j=cellARows-2){  //如果循环到倒数第二条，退出
							break;
						}
					}
				}
			}
		}
	}
	
	//判断关键字、外键组合的主键是否重复
	if(cellARows-1>1){
		if(cellACols>0){
			var colNumberStr = "";  //记录关键字、外键对应的字段的列号字符串集合
			if(tableid.value=="sys_emp"){  //如果表名是 sys_emp ，不用判断关键字，把主键的列号记录下来
				for(var i=1;i<=cellACols;i++){	
					if(eval("field_"+i).value=="emp_code"){
						colNumberStr = colNumberStr + i + ",";
					}
				}
			}else{  //如果表名不是 sys_emp ，把选择的外键和关键字对应的列号记录下来
				for(var i=1;i<=cellACols;i++){	
					if(eval("field_"+i).value.indexOf("emp_code")>-1){
						colNumberStr = colNumberStr + i + ",";
					}
					if(keyWordCount!=0){
						for(var j=1;j<=keyWordCount;j++){
							if(eval("keyWord_"+j).value=="1" && eval("keyWord_"+j).id.split("-")[0]==eval("field_"+i).value){
								colNumberStr = colNumberStr + i + ",";
							}
						}
					}
				}
			}
			colNumberStr = colNumberStr.substr(0, colNumberStr.length-1);
			var colNumberArray = colNumberStr.split(",");  //记录关键字、外键对应的字段的列号集合
			for(var i=1;i<=cellARows-1;i++){  //循环cell的所有行跟后面的行进行主键的比较
				for(var i1=i+1;i1<=cellARows-1;i1++){
					var PKArray1 = "";  //记录关键字、外键组合的主键1
					var PKArray2 = "";  //记录关键字、外键组合的主键2
					for(var j=0;j<colNumberArray.length;j++){
						PKArray1 = PKArray1 + dataCellB.GetCellString(parseInt(colNumberArray[j]),i,0) + ",";
						PKArray2 = PKArray2 + dataCellB.GetCellString(parseInt(colNumberArray[j]),i1,0) + ",";
					}
					if(PKArray1==PKArray2){
						if(tableid.value=="sys_emp"){
							alert("第 " + i + " 行跟第 " + i1 + " 行的主键重复，请检查数据！！！");
							return false;
						}else{
							alert("第 " + i + " 行跟第 " + i1 + " 行的联合主键重复，请检查数据！！！");
							return false;
						}
					}
				}
				if(i==cellARows-2){  //如果循环到倒数第二条，退出
					break;
				}
			}
		}
	}
	
	//判断非空字段是否已经选择
	for(var i=0;i<trsField.length;i++){
		var field_id = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[i].getElementsByTagName('td')[0].firstChild.nodeValue;
		var field_name = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[i].getElementsByTagName('td')[1].firstChild.nodeValue;
		var field_kind = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[i].getElementsByTagName('td')[2].firstChild.nodeValue;
		var code_id = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[i].getElementsByTagName('td')[3].firstChild.nodeValue;
		var field_nonull_flag = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[i].getElementsByTagName('td')[4].firstChild.nodeValue;
		var nonullfieldflag = false;  //非空字段字段是否已经选择标识
		if( !(field_id=="comp_code" || field_id=="copy_code") ){
			if(field_nonull_flag=="1"){
				for(var j=1;j<=cellACols;j++){	
					//判断非空字段是否已经选择
					if(eval("field_"+j).value==field_id){
						nonullfieldflag = true;
						break;
					}
				}
				if(!nonullfieldflag){
					nonullfieldflag = false;  //初始化非空字段字段是否已经选择标识
					alert(field_name+"为非空字段，必须选择！！！");
					return false;
				}
			}
		}
	}
	
	
	//按行、列进行数据校验
	for(var i=1;i<=cellACols;i++){
		if(eval("field_"+i).value.length!=0){
			var dateTypeFlag = false; 	//数据类型验证标识
			for(var j=0;j<trsField.length;j++){
				var field_id = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[j].getElementsByTagName('td')[0].firstChild.nodeValue;
				var field_name = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[j].getElementsByTagName('td')[1].firstChild.nodeValue;
				var field_kind = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[j].getElementsByTagName('td')[2].firstChild.nodeValue;
				var code_id = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[j].getElementsByTagName('td')[3].firstChild.nodeValue;
				var field_nonull_flag = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[j].getElementsByTagName('td')[4].firstChild.nodeValue;
				var field_length = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[j].getElementsByTagName('td')[5].firstChild.nodeValue;
				
				//校验对应的excel列的数据是否符合该类型
				if(eval("field_"+i).value == field_id){
					//判断field_nonull_flag为"1"的字段对应的数据是否为空
					if(field_nonull_flag=="1"){
						for(var k=1;k<=cellARows-1;k++){
							if(dataCellB.GetCellString(i,k,0).length==0){
								alert("第"+k+"行"+"第"+i+"列\""+dataCellB.GetCellString(i,0,0)+"\"为空，此列为非空列！！！");
								return false;
							}
						}
					}else{
						for(var k=1;k<=cellARows-1;k++){
							//如果数据不为空进行长度校验
							if(dataCellB.GetCellString(i,k,0).length!=0 && field_kind!="日期型"){
								if(dataCellB.GetCellString(i,k,0).length>parseInt(field_length)){
									alert("第"+k+"行"+"第"+i+"列\""+dataCellB.GetCellString(i,0,0)+"\"长度超过"+field_length+"，数据格式不正确！！！");
									return false;
								}
							}
						}
						//字段类型为日期型的校验
						if(field_kind=="日期型"){
							for(var k=1;k<=cellARows-1;k++){
								//如果数据不为空进行校验
								if(dataCellB.GetCellString(i,k,0).length!=0){
									//日期型正则表达式
									var dateTypeValidate = /^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))$/;
									if(!dateTypeValidate.test(dataCellB.GetCellString(i,k,0))){
										alert("第"+k+"行"+"第"+i+"列\""+dataCellB.GetCellString(i,0,0)+"\"应为日期型，数据格式不正确！！！");
										return false;
									}
								}
							}
						}
						//字段类型为数值型的校验
						if(field_kind=="数值型"){
							for(var k=1;k<=cellARows-1;k++){
								//如果数据不为空进行校验
								if(dataCellB.GetCellString(i,k,0).length!=0){
									//数字正则表达式：/^\d+(\.\d+)?$/
									//正整数正则表达式：/^-?[1-9]\d*$/	
									//正浮点数正则表达式：/^-?([1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0)$/
									if( !(/^\d+(\.\d+)?$/.test(dataCellB.GetCellString(i,k,0))) ){
										alert("第"+k+"行"+"第"+i+"列\""+dataCellB.GetCellString(i,0,0)+"\"应为数值型，数据格式不正确！！！");
										return false;
									}
								}
							}
						}
					}
					
					dateTypeFlag = true;  //整个列校验完毕，退出类型判断的循环
				}
				if(dateTypeFlag){  //如果字段类型判断完就退出字段类型判断，进行下一列的判断
					dateTypeFlag = false;
					break;
				}
			}
		}
	}
	//***************数据校验完毕******************
	
	if(confirm("导入的数据校验完毕，确定导入？")){
		//导入数据
		
		//拼表相关的字符串
		var tableStr = tableid.value + "!" + getCompCode();  
		//向表相关的字符串追加导入类型, 0追加导入 还是 1更新导入
		tableStr = tableStr + "!" + importType.value;
		//向表相关的字符串追加 主键 或者 联合主键
		if(tableid.value=="sys_emp"){
			tableStr = tableStr + "!";
			for(var i=1;i<=cellACols;i++){	
				if(eval("field_"+i).value=="emp_code"){
					tableStr = tableStr + "emp_code,";
				}
				//if(eval("field_"+i).value=="emp_name"){
				//	tableStr = tableStr + "emp_name,";
				//}
			}
			tableStr = tableStr.substr(0, tableStr.length-1);
		}else{
			tableStr = tableStr + "!";
			for(var i=1;i<=cellACols;i++){
				if(eval("field_"+i).value.indexOf("emp_code")>-1){
					tableStr = tableStr + eval("field_"+i).value + ",";
				}
			}
			for(var i=1;i<=keyWordCount;i++){
				if(eval("keyWord_"+i).value=="1"){
					tableStr = tableStr + eval("keyWord_"+i).id.split("-")[0] + ",";
				}
			}
			tableStr = tableStr.substr(0, tableStr.length-1);
		}
		//向表相关的字符串追加 唯一性校验 的字段
		if(uniqueFieldCount==0){
			tableStr = tableStr + "!null";
		}else{
			tableStr = tableStr + "!";
			for(var i=1;i<=uniqueFieldCount;i++){
				if(eval("uniqueField_"+i).value=="1"){
					tableStr = tableStr + eval("uniqueField_"+i).id + ",";
				}
			}
			tableStr = tableStr.substr(0, tableStr.length-1);
		}
		
		//拼数据相关的字符串
		var dataStr = "";  
		for(var i=1;i<=cellARows-1;i++){
			for(var j=1;j<=cellACols;j++){
				//alert(dataCellB.GetCellString(j,i,0)+" 111");
				if(eval("field_"+j).value!=""){
					if(eval("field_"+j).value.indexOf("sys_emp")>-1){
						dataStr = dataStr + j + "," + eval("field_"+j).value + ",null,null,0," + dataCellB.GetCellString(j,i,0) + "#";
						continue;
					}else{
						for(var k=0;k<trsField.length;k++){
							var field_id = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[k].getElementsByTagName('td')[0].firstChild.nodeValue;
							var field_name = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[k].getElementsByTagName('td')[1].firstChild.nodeValue;
							var field_kind = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[k].getElementsByTagName('td')[2].firstChild.nodeValue;
							var code_id = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[k].getElementsByTagName('td')[3].firstChild.nodeValue;
							var field_nonull_flag = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[k].getElementsByTagName('td')[4].firstChild.nodeValue;
							
							//把选择的字段及其字段属性拼到串里
							if(eval("field_"+j).value==field_id){  
								//如果值为空，把null拼到串里
								if(dataCellB.GetCellString(j,i,0)=="" || dataCellB.GetCellString(j,i,0).length==0){
									dataStr = dataStr + j +","+ field_id +","+ field_kind +","+ code_id +","+ field_nonull_flag +",null#";
								}else{
									//如果字段为是否型，更改汉字 是 为 1
									if(dataCellB.GetCellString(j,i,0)=='是'){
										dataStr = dataStr + j +","+ field_id +","+ field_kind +","+ code_id +","+ field_nonull_flag +",1#";
									}else{
										//如果字段为是否型，更改汉字 否 为 0
										if(dataCellB.GetCellString(j,i,0)=='否'){
											dataStr = dataStr + j +","+ field_id +","+ field_kind +","+ code_id +","+ field_nonull_flag +",0#";
										}else{
											dataStr = dataStr + j +","+ field_id +","+ field_kind +","+ code_id +","+ field_nonull_flag +","+ dataCellB.GetCellString(j,i,0) +"#";
										}
									}
								}
								break;
							}
						}
					}
				}
			}
			
			//如果有数据库中该表字段中含有 comp_code 或者 copy_code，拼到串里, 选择对应字段的时候已经过滤了 comp_code 和 copy_code
			for(var k=0;k<trsField.length;k++){
				var field_id = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[k].getElementsByTagName('td')[0].firstChild.nodeValue;
				var field_name = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[k].getElementsByTagName('td')[1].firstChild.nodeValue;
				var field_kind = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[k].getElementsByTagName('td')[2].firstChild.nodeValue;
				var code_id = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[k].getElementsByTagName('td')[3].firstChild.nodeValue;
				var field_nonull_flag = fieldsXml.getElementsByTagName('tbody')[0].getElementsByTagName('tr')[k].getElementsByTagName('td')[4].firstChild.nodeValue;
				
				
				if(field_id=='comp_code'){
					dataStr = dataStr + j +","+ field_id +","+ field_kind +","+ code_id +","+ field_nonull_flag +","+ getCompCode() +"#";
				} 
				if(field_id=='copy_code' ){
					dataStr = dataStr + j +","+ field_id +","+ field_kind +","+ code_id +","+ field_nonull_flag +","+ getCopyCode() +"#";
				}
			}
			
			dataStr = dataStr.substr(0, dataStr.length-1);
			dataStr = dataStr + "!";
		}  
		dataStr = dataStr.substr(0, dataStr.length-1);
		
		//alert(tableStr);
		//alert(dataStr);
		window.xmlhttp.post("hr_empDocumentDataImportMain_importDate", "<tableStr>" + tableStr + "</tableStr><dataStr>" + dataStr + "</dataStr>");
		var returnText = window.xmlhttp._object.responseText;
		//var returnTrs = returnText.getElementsByTagName("tr");
		//alert(returnText);
		//循环返回的字符串
		var successCount = 0;  //正确操作的数量
		var keyWordError = "";  // 主键或者联合主键 原因造成的 错误行号 集合
		var uniqueError = "";  // 唯一性 原因造成的 错误行号 集合
		var dataError = "";  // 数据操作异常 原因造成的 错误行号 集合
		var returnTextArray = returnText.split("#");
		for(var i=1;i<returnTextArray.length;i++){
			var rowArray = returnTextArray[i].split(",");
			//alert(rowArray[0]+":"+rowArray[1]+":"+rowArray[2]+":"+rowArray[3]);
			/*
				'rowNumber', 'handleType', 'isError',  'errorType'
				 (i+1),       insert,       success,    keyWord
				 (i+1),       update,       error,      unique
				                                        data
			*/
			
			if(rowArray[2].indexOf("success")>-1){  //数据导入成功
				successCount = successCount + 1;
			}else{  //数据导入失败
				if(rowArray[3].indexOf("keyWord")>-1){
					keyWordError = keyWordError + rowArray[0] + ",";
				}else{
					if(rowArray[3].indexOf("unique")>-1){
						uniqueError = uniqueError +rowArray[0] + ",";
					}else{
						if(rowArray[3].indexOf("data")>-1){
							dataError = dataError +rowArray[0] + ",";
						}
					}
				}
			}
		}
		//提示导入几行成功
		alert("  共有 " + successCount + " 行导入成功！！！");
		//提示哪些行由于 主键或者联合主键 原因未导入
		if(keyWordError!=""){
			keyWordError = keyWordError.substr(0, keyWordError.length-1);
			alert(" 第 " + keyWordError + " 行由于 主键或者联合主键 原因导入失败！！！");
		}
		//提示哪些行由于 唯一性 原因未导入
		if(uniqueError!=""){
			uniqueError = uniqueError.substr(0, uniqueError.length-1);
			alert(" 第 " + uniqueError + " 行由于 唯一性 原因导入失败！！！");
		}
		//提示哪些行由于 数据操作异常 原因未导入
		if(dataError!=""){
			dataError = dataError.substr(0, dataError.length-1);
			alert(" 第 " + dataError + " 行由于 数据操作异常 原因导入失败！！！");
		}
		
		if(confirm("是否查看日志？")){
			//logDown.href = window.prefix+"hbos/hr/document/dataImport/hrDataImportLog.txt";
			//logDown.click();
			window.open(window.prefix+"hbos/hr/document/dataImport/hrDataImportLog.txt");
		}
		
	}else{
		return false;
	}
}

			
