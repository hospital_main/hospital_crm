<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/hr/document/personMove/checkView.xsl,v 1.2 2013/02/26 04:16:21 wangyateng Exp $
 $Author: wangyateng $
 $Date: 2013/02/26 04:16:21 $
 $Revision: 1.2 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
		<thead>
			<tr>
				<td style="colspan:14;fontsize:maintitle">人员调动查询</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
					<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td style='fontsize:coltitle'>职工编码</td>
				<td style='fontsize:coltitle'>职工姓名</td>
				<td style='fontsize:coltitle'>人员类别</td>
				<td style='fontsize:coltitle'>调入部门</td>
				<td style='fontsize:coltitle'>原部门</td>
				<td style='fontsize:coltitle'>调动时间</td>
				<td style='fontsize:coltitle'>调动说明</td>
				<td style='fontsize:coltitle'>状态</td>
				<td style='fontsize:coltitle'>操作人</td>
				<td style='fontsize:coltitle'>申请时间</td>
				<td style='fontsize:coltitle'>审核人</td>
				<td style='fontsize:coltitle'>审核时间</td>
				<td style='fontsize:coltitle'>确认人</td>
				<td style='fontsize:coltitle'>确认时间</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=3 or position()=4">
								<td ><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
