<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>	  		    		
			<th noWrap="true">职工编码</th>
			<th noWrap="true">职工名称</th>
			<th noWrap="true">人员类别</th>
			<th><input type='checkbox'></input></th>
     	</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>     			
				<xsl:for-each select="td">
					<xsl:choose>
						<!--
						<xsl:when test="position()=4">
							<td align='center'><div>
		            <input type='checkbox' TABINDEX='-1' style='font-size:8px;' >
		      			  <xsl:if test="text() = '1'">
		      			    <xsl:attribute name="checked"><xsl:value-of select="."/></xsl:attribute>
		      			  </xsl:if>
		    			  </input></div>
		          </td>	
						</xsl:when>-->
						<xsl:when test="position()=4">
							<td align='center'>
		           <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
		              <xsl:attribute name="value" >
		                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		      			  </xsl:attribute>
		    			  </input>
		          </td>	
						</xsl:when>
						
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
							
			</tr>			
		</xsl:for-each> 		
	  </tbody> 	
 	</xsl:template>
</xsl:stylesheet>


