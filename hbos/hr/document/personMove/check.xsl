<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th><input type='checkbox'/></th>
				<th style=''>职工编码</th>
				<th nowrap='true'>职工姓名</th>
				<th nowrap='true'>人员类别</th>
				<th nowrap='true'>调入部门</th>
				<th nowrap='true'>原部门</th>
				<th nowrap='true'>调动时间</th>
				<th nowrap='true'>调动说明</th>
				<th nowrap='true'>状态</th>
				<th nowrap='true'>操作人</th>
				<th nowrap='true'>申请时间</th>
				<th nowrap='true'>审核人</th>
				<th nowrap='true'>审核时间</th>
				<th nowrap='true'>确认人</th>
				<th nowrap='true'>确认时间</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:choose>
					<xsl:when test="td[15]=1 or td[15]=2">
						<td align='center'  style='display:none'>
							<input type='checkbox' TABINDEX='-1' disabled="disabled">
								<xsl:attribute name="value" >
									<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
								</xsl:attribute>
							</input>
						</td>
					</xsl:when>
					<xsl:otherwise>
						<td align='center'  style='display:none'>
							<input type='checkbox' TABINDEX='-1'>
								<xsl:attribute name="value" >
									<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
								</xsl:attribute>
							</input>
						</td>
					</xsl:otherwise>
					</xsl:choose>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=1">
								<td >
									<a tabindex='-1'>
						         <xsl:attribute name="href" >
						    	 javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;state&gt;<xsl:value-of select="../td[position()=8]"/>&lt;/state&gt;', 'dialogWidth:400px;dialogHeight:400px')
						  	     </xsl:attribute><xsl:value-of select="."/>
						  	  </a>
								</td>
							</xsl:when>
							<xsl:when test="position()=3 or position()=4">
								<td ><xsl:value-of select="."/></td>
							</xsl:when>
							
							<xsl:when test="position()=15">
								<td style="display:none"><xsl:value-of select="."/></td>
							</xsl:when>
							
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>