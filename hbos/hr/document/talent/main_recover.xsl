<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		
			<tr noWrap="true" class="mainHead">
				<th noWrap="true">
					选择
				</th>
				<th noWrap="true">
					人才编码
				</th>
				<th noWrap="true">
					人才姓名
				</th>
				<th noWrap="true">
					人才类别
				</th>
				<th noWrap="true">
					职工编码
				</th>
				<th noWrap="true">
					科室
				</th>
				<th noWrap="true">
					职工类别
				</th>
				<th noWrap="true">
					考勤类别
				</th>
				<th noWrap="true">
					是否发工资
				</th>
				<th noWrap="true">
					发放方式
				</th>
				<th noWrap="true">
					是否使用
				</th>
			</tr>
	</thead>
    
    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
		    <tr>
			    <td align='center'>
			    	<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
				      <xsl:attribute name="value">
				          <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					  </xsl:attribute>
						<xsl:if test="td[10]='是'">
							<xsl:attribute name="disabled">true</xsl:attribute>
						</xsl:if>
					 
					</input>
			    </td>
			    <xsl:for-each select="td"> 
					<td noWrap='true'>
						
								<xsl:value-of select="."/>
							
					</td>
			    </xsl:for-each>
		    </tr>
	    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

