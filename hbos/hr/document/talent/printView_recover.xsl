<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
   <root>
    
    	<thead>
    		<!--
       	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	-->
	  		<tr noWrap="true" class="mainHead">
					<td>人才编码</td>
					<td>人才姓名</td>
					<td>人才类别</td>
					<td>职工编码</td>
					<td>科室</td>
					<td>职工类别</td>
					<td>考勤类别</td>
					<td>是否发工资</td>
					<td>发放方式</td>
					<td>是否使用</td>
			</tr>
	
	 		
   		</thead>
   
	  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr">
	  			<tr>
					<xsl:for-each select="td">
	              		<td nowrap='true'><xsl:value-of select="."/></td>
					</xsl:for-each>
					</tr>
			</xsl:for-each>
	  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>