<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>编制部门</th>
				<th>职工编码</th>
				<th>职工名称</th>
				<th>培训活动名称</th>
				<th>编制日期</th>
				<th>培训活动年度</th>
				<th>培训活动目的</th>
				<th>培训活动内容</th>
				<th>培训活动地点</th>
				<th>活动开始日期</th>
				<th>活动结束日期</th>
				<th>活动类型</th>
				<th>培训方式</th>
				<th>培训机构</th>
				<th>备注</th>
				<th>课程名称</th>
				<th>课程内容</th>
				<th>培训导师</th>
				<th>课程学时</th>
				<th>教材名称</th>
				<th>预备知识</th>
				<th>培训费用</th>
				<th>单位支付</th>
				<th>个人支付</th>
				<th>培训成绩</th>
				<th>是否通过</th>
				<th>导师评语</th>
				<th>证书</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=4">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openPage('<xsl:value-of select="../pk/activity_id"/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=22 or position()=23 or position()=24">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
	              <xsl:when test="position()=25">
				            <td align='right'>
	              	    <xsl:value-of select="."/>
								    </td>
	              </xsl:when>	
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>			
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
