<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	 <tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		 </tr>		 
				 <tr noWrap="true" class="mainHead">
				<td>编制部门</td>
				<td>职工编码</td>
				<td>职工名称</td>
				<td>培训活动</td>
				<td>编制日期</td>
				<td>培训活动年度</td>
				<td>培训活动目的</td>
				<td>培训活动内容</td>
				<td>培训活动地点</td>
				<td>活动开始日期</td>
				<td>活动结束日期</td>
				<td>活动类型</td>
				<td>培训方式</td>
				<td>培训机构</td>
				<td>备注</td>
				<td>课程名称</td>
				<td>课程内容</td>
				<td>教师姓名</td>
				<td>课程学时</td>
				<td>教材名称</td>
				<td>预备知识</td>
				<td>培训费用</td>
				<td>单位支付</td>
				<td>个人支付</td>
				<td>培训成绩</td>
				<td>是否通过</td>
				<td>导师评语</td>
				<td>证书</td>
			 </tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=22 or position()=23 or position()=24">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
	              <xsl:when test="position()=25">
				            <td align='right'>
	              	    <xsl:value-of select="."/>
								    </td>
	              </xsl:when>	
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>			
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>