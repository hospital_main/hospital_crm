<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none"><input type="checkbox"/></th>
				<th>计划级别</th>
				<th>计划编码</th>
				<th>编制部门</th>
				<th>编制日期</th>
				<th>活动编码</th>
				<th>活动名称</th>
				<th>培训内容</th>	
				<th>计划开始时间</th>
				<th>计划结束时间</th>
				<th>参训人数</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
        	<xsl:if test="td[1] != '合计'">
	          <td align='center'  noWrap='true'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
	    			  </input>
	          </td>
          </xsl:if>
					<xsl:for-each select="td">																
						<td><xsl:value-of select="."/></td>							
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
