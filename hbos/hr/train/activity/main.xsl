<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none"><input type="checkbox"/></th>
				<th>指定人员</th>
				<th>培训活动编码</th>
				<th>培训活动名称</th>
				<th>编制日期</th>
				<th>培训活动级别</th>
				<th>部门</th>
				<th>培训活动类别</th>
				<th>活动年度</th>
				<th>活动季度</th>
				<th>活动月份</th>
				<th>培训机构</th>
				<th>活动开始时间</th>
				<th>活动结束时间</th>
				<th>总预算</th>
				<th>编制人</th>
				<th>审核人</th>
				<th>确认人</th>
				<th>状态</th>
				<th>计划活动</th>
				<th style="display:none"></th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
        	<xsl:if test="td[1] != '合计'">
	          <td align='center'  noWrap='true'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
	    			  </input>
	          </td>
          </xsl:if>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". != '合计'"> 
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					javascript:openSelectEmp('<xsl:value-of select="../pk/activity_id"/>');
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=2">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openPage('<xsl:value-of select="../pk/activity_id"/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=14">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
	              <xsl:when test="position()=19">
	              	<td>
				            <a href="#">
											 <xsl:attribute name="onclick">
                					openPlanPage('<xsl:value-of select="../td[position()=20]"/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
	              </xsl:when>
	              <xsl:when test="position()=20 ">
                	<td style="display:none"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
             		<!--xsl:when test="position()=4 and ../td[1]!='合计'">
                	<td>
                		<a href="#">
                		 <xsl:attribute name="onclick" >
                		 	  	openPactPage('<xsl:value-of select="."/>','2','01','<xsl:value-of select="../pk/comp_code"/>','<xsl:value-of select="../pk/copy_code"/>')
                		 </xsl:attribute>
                		 <xsl:value-of select="."/>
                		</a>
                	</td>
             		</xsl:when-->
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
