<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap="true" class="mainHead">
				<td>计划编码</td>
				<td>计划名称</td>
				<td>编制日期</td>
				<td>计划级别</td>
				<td>部门</td>
				<td>计划类型</td>
				<td>计划年度</td>
				<td>计划季度</td>			
				<td>计划月份</td>
				<td>计划开始时间</td>
				<td>计划结束时间</td>
				<td>总预算</td>
				<td>编制人</td>
				<td>审核人</td>
				<td>确认人</td>
			</tr>
	  	</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				<xsl:for-each select="td">
					<xsl:choose>
					<xsl:when test="position()=12">
					<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
					</xsl:when>
					<xsl:otherwise>
					<td><xsl:value-of select="."/></td>
					</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				</tr>
			</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>