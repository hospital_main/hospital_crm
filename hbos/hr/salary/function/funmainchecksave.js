var left_fieldid;
var left_field_type;
var formula_type;
var formula_error;
var formula_field_dec;
var myFormula;
var formula_sql;
var myTableId;
var formula_list1=[];   
var formula_list2=[];
var formula_list3=[];

function resetCheckAndSaveParams(){
	left_fieldid="";
	left_field_type="";
	formula_type="";
	formula_error="";
	formula_field_dec="";
	myFormula="";
	formula_sql="";
	myTableId="";
	formula_list1=[];   
	formula_list2=[];
	formula_list3=[];
}

function formulaCheckAndSave(formula,formulaType,Table_id,comp_code,copy_code,acct_year,kind_id,rid,is_save){
/*
1、校验并保存公式主函数
功能描述：校验公式是否符合语法规则。如果参数isSave为true,则校验合格后直接保存计算公式。
函数名称：String formulaCheckAndSave(String formula,int formulaType,String Table_id,String comp_code,String copy_code,String acct_year,int kind_id,int rid,Boolean is_Save);
*/
	resetCheckAndSaveParams();
	
	myFormula=formula;
	myTableId=Table_id;

	// 初始化公共变量：
	//left_fieldid,left_field_type,formula_type,formula_error,formula_field_dec,formula_list1,formula_list2,formula_list3

	 //处理公式被注释
	 if((formula=='')||(formula.substr(0,2)=='//')){
	    getFormulaSQL(); 
	    if(is_save){
	    	return SaveFormula(kind_id);
	    }
	 }
	 //分解表达式
	 //Formula 中是否有等号？
	 if(formula.indexOf('=')<0){
		 return '语句:['+ formula +']: 缺少= !';
	 }
	  
	 var expr1=formula.substr(0,formula.indexOf('='));
	 
	 var field_ret=getfieldinfo(Table_id,expr1);
	 
	 if(!field_ret.has){
		 return ('语句:['+formula+']:['+expr1+']项目不存在!');
	 }
	 var fieldid1=field_ret.fieldid;
	 
	 formula_list3.push(Table_id);
	 
	 left_fieldid=field_ret.fieldid;
	 left_field_type=field_ret.fieldkind;
	 
	 expr2=formula.substr(formula.indexOf('=')+1);
	 //alert(formula+":"+expr2+":"+left_field_type);
	 //开始处理右边
	 if(left_field_type=='字符型'){
		 formula_error=formulaCharCheck(expr2);
	 }
	 if(left_field_type=='日期型'){
		 formula_error=formulaDateCheck(expr2);
	 }
	 if(left_field_type=='数值型'){
		 formula_error=formulaNumCheck(expr2, formulaType);
	 }
	 if(formula_error!=undefined&&formula_error!=''){
		 return  formula_error;
	 }
	/*
	 //ceshi
	 getFormulaSQL();
	 //ceshi
	 */
	 if(!is_save){
		 return "";
	 }
	//得到可用的SQL公式
	getFormulaSQL();
	if(_fundebug){
		alert(formula_sql);
	}
	//保存计算公式
	return saveFormula(kind_id);
}

function formulaCharCheck(expr){
/*
2、字符型校验函数
功能描述：校验公式右边表达式是否符合字符型语法规则，符合规则返回空串，否则返回错误提示。
函数名称：String formulaCharCheck(String expr)
函数主要内容如下：
*/
	if(expr.substr(0,8).toUpperCase()=='GETFIELD'){
		 return FuncGetFieldCheck(expr);
	 }
	if(expr.substr(0,6).toUpperCase()=='SUBSTR'){
		return '无效函数名SUBSTR';
	}
	//字符常量
	formula_type='字符常量';
	return charConstCheck(expr);
}

function formulaDateCheck(expr){
/*
3、日期型校验函数
功能描述：校验公式右边表达式是否符合日期型语法规则，符合规则返回空串，否则返回错误提示。
函数名称：String formulaDateCheck(String expr)
*/
	if(expr.substr(0,8).toUpperCase()=='GETFIELD'){
		 return FuncGetFieldCheck(expr);
	}
	var ep1=expr.toUpperCase();
	if((ep1=='DATE()')||(ep1=='PERIODBEGINDATE()')||(ep1=='PERIODENDDATE()')){
	   formula_list1.push(expr);
	   formula_list2.push(ep1);
	   formula_type=ep1;
	   return '';
	  }
	 //日期常量
	 formula_type='日期常量';
	 return dateConstCheck(expr);
}

function formulaNumCheck(expr,formulaType){
/*
4、数值型校验函数
功能描述：校验公式右边表达式是否符合数值型语法规则，符合规则返回空串，否则返回错误提示。
函数名称：String formulaNumCheck(String expr, int formulaType)

*/
	//取人员数量函数GETPERSONNUMBER()
	 if(expr.toUpperCase()=='GETPERSONNUMBER()'&&formulaType!=5){
		 formula_list1.push(expr);
	 	 formula_list2.push('GETPERSONNUMBER()');
	 	 formula_type='GETPERSONNUMBER()';
	 	 return '';
	 }
	 //GETFIELD函数
	 if(expr.substr(0,8).toUpperCase()=='GETFIELD'){
		 return FuncGetFieldCheck(expr);
	 }
	 if(expr.substr(0,6).toUpperCase()=='GETTAX'){
		 return FuncTaxCheck(expr);
	 }
	 if(expr.substr(0,12).toUpperCase()=='GETTAXBEFORE'){
		 return FuncTaxCheck(expr);
	 }
	 //取值函数
	 if(expr.substr(0,3).toUpperCase()=='INT'){
		 return FuncIntCheck(expr);
	 }

	 if(expr.substr(0,3).toUpperCase()=='DEC'){
		 return FuncDecCheck(expr);
	 }
	 
	 if(expr.substr(0,5).toUpperCase()=='ROUND'){
		 return FuncRoundCheck(expr);
	 }
	 //日期时间函数
	 if(expr.substr(0,7).toUpperCase()=='GETYEAR'){
		 return FuncDateTimeCheck(expr);
	 }
	  
	 if(expr.substr(0,8).toUpperCase()=='GETMONTH'){
		 return FuncDateTimeCheck(expr);
	 }
	 
	 if(expr.substr(0,6).toUpperCase()=='GETDAY'){
		 return FuncDateTimeCheck(expr);
	 }
	 
	 if(expr.substr(0,7).toUpperCase()=='GETHOUR'){
		 return FuncDateTimeCheck(expr);
	 }

	 if(expr.substr(0,9).toUpperCase()=='GETMINUTE'){
		 return FuncDateTimeCheck(expr);
	 }
	 if(expr.substr(0,10).toUpperCase()=='GETSECONDS'){
		 return FuncDateTimeCheck(expr);
	 }
	 if(expr.substr(0,11).toUpperCase()=='GETFULLYEAR'){
		 return FuncDateTimeCheck(expr);
	 }
	 //合计函数
	 if(expr.substr(0,3).toUpperCase()=='SUM'){
		 return FuncStatisticsCheck(expr);
	 }
	 if(expr.substr(0,3).toUpperCase()=='AVG'){
		 return FuncStatisticsCheck(expr);
	 }
	 if(expr.substr(0,3).toUpperCase()=='MAX'){
		 return FuncStatisticsCheck(expr);
	 }
	 if(expr.substr(0,3).toUpperCase()=='MIN'){
		 return FuncStatisticsCheck(expr);
	 }
	 if(expr.substr(0,15).toUpperCase()=='GETPERSONNUMBER'){
		 return FuncStatisticsCheck(expr);
	 }
	 if(expr.substr(0,13).toUpperCase()=='GETPERSONTIME'){
		 return FuncStatisticsCheck(expr);
	 }
	 //四则运算
	 formula_type='四则运算';
	 return arithmeticCheck(expr);
}

function getFormulaSQL(){
	/*
5、得到用SQL语句表示的计算公式
功能描述： 拼出SQL语句表示的计算公式。
函数名称： getFormulaSQL();
*/
	if(formula_type==''){
		formula_sql='错误公式';
		return false;
	}
	//建立列表
	//formula_sql='##########'+ formula_list3[0];
	formula_sql='<viewhigh>'+ formula_list3[0];
	for(var i=1;i<formula_list3.length;i++){
		formula_sql+=','+formula_list3[i];
	}
	formula_sql+= '</viewhigh>';
	//formula_sql+= '###########';
	//字符型处理
	if(left_field_type=='字符型'){
		if(formula_type=='字符常量'){
			formula_sql+="Update "+ myTableId +" set "+ left_fieldid +"='"+ formula_list1[0]+"'";
		}
		if(formula_type=='GETFIELD'){
			var getField_rets=FuncGetFieldSQL();
			formula_sql+=getField_rets;
		}
	}
	//日期型处理
	if(left_field_type=='日期型'){
		if(formula_type=='日期常量'){
			formula_sql += "Update "+ myTableId +" set "+ left_fieldid +"='"+ formula_list1[0]+"'";
		}else{
			var str1='update '+ myTableId +' set '+ left_fieldid +'=';
			if(formula_type=='GETFIELD'){
				var getField_rets=FuncGetFieldSQL();
				formula_sql+=getField_rets;
			}else if(formula_type ='DATE()'){
				formula_sql += str1+' convert(varchar(10),getDate(),121)';
			}else{
				formula_sql += str1+ formula_type;
			}
		}
	}
	
	//数值型处理
	if(left_field_type=='数值型'){
			if(formula_type =='四则运算'){
					formula_sql +='Update '+ myTableId +' set '+ left_fieldid +'=';
					var str1='';
					for(var i=0;i<formula_list1.length-1;i++){
						//alert(formula_list2[i]+":"+formula_list1[i]);
						if(formula_list2[i]=='变量'){
							str1=str1+'isnull('+formula_list1[i]+',0)';
						}else{
							str1=str1+ formula_list1[i];
						}
					}
					formula_sql += str1;
			}
			if(formula_type=='GETFIELD'){
				var getField_rets=FuncGetFieldSQL();
				formula_sql+=getField_rets;
			}
			//处理取值函数
			if(formula_type=='INT'){
				formula_sql += 'Update '+ myTableId +' set '+ left_fieldid +'= floor (';
				for(var i=0;i<formula_list1.length-1;i++){
					formula_sql += formula_list1[i];
				}
				formula_sql += ')';
			}
			if(formula_type=='DEC'){
				var str1='';
				for(var i=0;i<formula_list1.length-1;i++){
					str1=str1+ formula_list1[i];
				}
				formula_sql += 'Update '+ myTableId +' set '+ left_fieldid +'=('+str1+')-floor('+str1+')';
			}
			if(formula_type=='ROUND'){
				var str1='';
				for(var i=0;i<formula_list1.length-1;i++){
					if(formula_list1[i]!=")"){
						str1=str1+ formula_list1[i];
					}
				}
				if(formula_field_dec==''){
					formula_field_dec ='0';
				}
				formula_sql += 'Update '+ myTableId +' set '+ left_fieldid +'= round ('+str1+','+ formula_field_dec +')';      
			}
			
			//处理日期时间函数
			if((formula_type=='GETYEAR') || (formula_type=='GETMONTH') || (formula_type=='GETDAY') || (formula_type=='GETHOUR') || (formula_type=='GETMINUTE') || (formula_type=='GETSECONDS')){
				
				formula_sql +=  'Update '+ myTableId +' set '+ left_fieldid +'=isnull(datediff(';
				if(formula_type=='GETYEAR'){
					formula_sql +='yy';
				}
				if(formula_type=='GETMONTH'){
					formula_sql +='mm';
				}
				if(formula_type=='GETDAY'){
					formula_sql += 'dy';
				}
				if(formula_type=='GETHOUR'){
					formula_sql +='hh';
				}
				if(formula_type=='GETMINUTE'){
					formula_sql += 'mi';
				}
				if(formula_type=='GETSECONDS'){
					formula_sql += 'ss';
				}
				formula_sql += ','+formula_list1[0].toUpperCase()+','+formula_list1[1].toUpperCase()+'),0)';
			}
			
			if(formula_type=='GETFULLYEAR'){
				formula_sql += 'DATEFUNC,'+ formula_type +','+ myTableId +','+ left_fieldid;
				for(var i=0;i<formula_list1.length;i++){
					formula_sql +=  ','+ formula_list1[i];
				}
			}
			//处理合计函数
			if ((formula_type=='SUM')||(formula_type=='AVG')||(formula_type=='MAX')||(formula_type=='MIN')||(formula_type=='GETPERSONNUMBER')||(formula_type=='GETPERSONTIME')){
				formula_sql +=  'HJFUNC,'+ formula_type +','+ myTableId +','+ left_fieldid;
				//GETPERSONNUMBER的listbox1.Items[1]为空
				formula_sql += ','+ formula_list1[0]+','+ formula_list1[1];
				if(formula_list1[2]!=''){
					formula_sql += ','+ formula_list1[2];
				}
				if(formula_list1.length>3){
					for(var i=3;i<formula_list1.length;i++){
						formula_sql += ','+ formula_list1[i];
					}
				}
			}
			//GETTAX处理
			if (formula_type=='GETTAX'||formula_type=='GETTAXBEFORE'){
				formula_sql+=formula_type;
				for(var i=0;i<formula_list1.length;i++){
					formula_sql += ','+ formula_list1[i];
				}
			}
			//GetPersonNumber()
			if(formula_type=='GETPERSONNUMBER()'){
				formula_sql += 'GETPERSONNUMBER'+','+ myTableId +','+ left_fieldid;
			}
	}
}

function saveFormula(kid){
	
/*
6、保存校验正确的计算公式
功能描述：保存计算公式
函数名称： String saveFormula();
	If(params2.rid==0){
		@copy_code=参数copy_code
		If not isCopyByTableid (table_id) @copy_code=null
		Insert hr_formula (kind_id , comp_code, copy_code , acct_year, table_id, formula_index ,
				formula_desc , formula , is_system,is_stop) values (参数kind_id , @copy_code,
						, 参数acct_year, 参数table_id, 99 ,	参数formula , formula_sql , 0,0) 
	} else {
		Update hr_formula set formula_desc=参数formula,formula= formula_sql  where rid=参数rid
	}
	//保存成功
	return "" 
	else return ‘保存失败’
 */
	formula_sql = formula_sql.replace(new RegExp("<","gm"),"&lt;");
	formula_sql = formula_sql.replace(new RegExp(">","gm"),"&gt;");
	
	myFormula = myFormula.replace(new RegExp("<","gm"),"&lt;");
	myFormula = myFormula.replace(new RegExp(">","gm"),"&gt;");
	
	//alert("<rid>"+params2.rid+"</rid><kid>"+kid+"</kid><comp>"+params2.comp_code+"</comp><copy>"+params2.copy_code+"</copy><year>"+params2.acct_year+"</year><tid>"+params2.table_id+"</tid><formula_desc>"+myFormula+"</formula_desc><formula>"+formula_sql+"</formula>");
	getParams("hr_funSetWizard_save","<rid>"+params2.rid+"</rid><kid>"+kid+"</kid><comp>"+params2.comp_code+"</comp><copy>"+params2.copy_code+"</copy><year>"+params2.acct_year+"</year><tid>"+params2.table_id+"</tid><formula_desc>"+myFormula+"</formula_desc><formula>"+formula_sql+"</formula>",true);
	return "";
}

function FuncGetFieldSQL(){
/*
7、得到 getField()函数的SQL
功能描述：得到getField()函数的SQL
函数名称：String FuncGetFieldSQL()
函数主要内容如下：
 */	
	var str1='GETFIELD,'+ left_fieldid +','+ formula_list2[0]+',';
	str1+= formula_list2[1]+',' +formula_list2[2]+',';
	str1+= formula_list2[3]+',' + formula_list2[4];
	return str1;
}

function FuncTaxCheck(expr){
/*
24、个人所得税函数校验函数
功能描述：校验个人所得税函数是否正确
函数名称：String FuncTaxCheck(String expr)
函数主要内容如下：

*/	
	return '预留函数，以后扩展';
}
function FuncRoundCheck(expr){
/*	
25、四舍五入函数校验函数
功能描述：校验四舍五入函数是否正确
函数名称：String FuncRoundCheck(String expr)
函数主要内容如下：
 */
	var len1=expr.length;
	if(expr.substr(5,1)!='('){
		return '语句:['+ myFormula +']: ROUND函数缺少"("!';
	}
	if(expr.substr(len1-1,1)!=')'){
		return '语句:['+ myFormula +']: ROUND函数缺少")"!';
	}
	var str1=expr.substr(6);
	formula_type='ROUND';
	
	pos1=str1.indexOf(',');
	if(pos1<0){
	   formula_field_dec=0;
	   return arithmeticCheck(str1);
	}else{
	   len1=str1.length;
	   str2=str1.substr(0,pos1);
	   str3=str1.substr(pos1+1,len1-pos1-2);
	  //如果str3不是整数，则
	   
		try{
			parseInt(str3);
		}catch(err){
			return '语句:['+ myFormula +']:ROUND函数第二个参数应为整数!';
		}
		  
		formula_field_dec=str3;
		return arithmeticCheck(str2);
	}
}

function FuncIntCheck(expr){
/*
26、取整函数校验函数
功能描述：校验取整函数是否正确
函数名称：String FuncIntCheck(String expr)
 */	
	var len1=expr.length;
	if(expr.substr(3,1)!='('){
		return '语句:['+ myFormula +']:INT函数缺少"("!';
	}
	 
	if(expr.substr(len1-1,1)!=')'){
		return '语句:['+ myFormula +']:INT函数缺少")"!';
	}
	 
	str1=expr.substr(4,len1-5);
	formula_type='INT';
	return arithmeticCheck(str1);
}

function FuncDecCheck(expr){
/*
27、取尾数函数校验函数
功能描述：校验尾数函数是否正确
函数名称：String FuncDecCheck(String expr)
 */	
	var len1=expr.length;
	if(expr.substr(3,1)!='('){
		return '语句:['+ myFormula +']: DEC函数缺少"("!';
	} 
	if(expr.substr(len1-1,1)!=')'){
		return '语句:['+ myFormula +']: DEC函数缺少")"!';
	} 
	 var str1=expr.substr(4,len1-5);
	 formula_type='DEC';
	 return arithmeticCheck(str1);
}

function FuncDateTimeCheck(expr){
/*
29、日期函数校验函数
功能描述：校验日期函数是否正确
函数名称：String FuncDateTimeCheck(String expr)
*/	
var pos1=8;
 if (expr.substr(0,7).toUpperCase()=='GETYEAR'){
	pos1=8;
	formula_type='GETYEAR';   
  }
 if(expr.substr(0,8).toUpperCase()=='GETMONTH'){
	pos1=9;
	formula_type='GETMONTH';
 }
 if(expr.substr(0,6).toUpperCase()=='GETDAY'){
	pos1=7;
	formula_type='GETDAY';
}
 if(expr.substr(0,11).toUpperCase()=='GETFULLYEAR'){
    pos1=12;
    formula_type='GETFULLYEAR';
  }
 if(expr.substr(0,7).toUpperCase()=='GETHOUR'){
    pos1=8;
    formula_type='GETHOUR';
  }
 if(expr.substr(0,9).toUpperCase()=='GETMINUTE'){
    pos1=10;
    formula_type='GETMINUTE';
  }
 if(expr.substr(0,10).toUpperCase()=='GETSECONDS'){
    pos1=11;
    formula_type='GETSECONDS';  
}
 
 var len1=expr.length;
 if(expr.substr(pos1-1,1)!='('){
    return '语句:['+ myFormula +']:'+ formula_type +'函数缺少"("!';
 }
 if(expr.substr(expr.length-1,1)!=')'){
	return '语句:['+ myFormula +']:'+ formula_type +'函数缺少")"!';
 }
 var str1=expr.substr(pos1,len1-pos1-1);
 pos1=str1.indexOf(",");
 if(pos1<0){
	 return '语句:['+ myFormula +']:'+ formula_type +'函数必须有两个参数!';
 }
 len1=str1.length;
 str2=str1.substr(0,pos1);
 str3=str1.substr(pos1+1);
 //分析str2
if((str2.toUpperCase()=='DATE()')||(str2.toUpperCase()=='TIME()')||(str2.toUpperCase()=='PERIODBEGINDATE()')||(str2.toUpperCase()=='PERIODENDDATE()')){
	if(str2.toUpperCase()=='DATE()'){
		formula_list1.push('getDate()');
	}else if(str2.toUpperCase()=='TIME()'){
		formula_list1.push('getDate()');
	}else{
		formula_list1.push(str2);
	}
}else{
	var _temp_ret=getfieldinfo(myTableId,str2);
	var fieldid1=_temp_ret.fieldid;
	var fieldtype1=_temp_ret.fieldkind;
	if(fieldid1==''){
        return '语句:['+ myFormula +']:('+str2+')变量不存在!';
	}
    if(fieldtype1!='日期型'&&fieldtype1!='时间型'){
        return '语句:['+ myFormula +']:('+str2+')变量不为日期型或时间型!';
    }
    formula_list1.push(myTableId+'.'+fieldid1);
}
 //分析str3
 if (str3.toUpperCase()=='DATE()'||str3.toUpperCase(str3)=='TIME()'||str3.toUpperCase()=='PERIODBEGINDATE()'||str3.toUpperCase()=='PERIODENDDATE()'){
     if(str3.toUpperCase()=='DATE()'){
    	 formula_list1.push('getDate()');
     }else if(str3.toUpperCase()=='TIME()'){
    	 formula_list1.push('getDate()');
     }else{
    	 formula_list1.push(str3);
     }
}else{
	var _temp_ret=getfieldinfo(myTableId,str3);
	var fieldid1=_temp_ret.fieldid;
	var fieldtype1=_temp_ret.fieldkind;
	
	if(fieldid1==''){
		return '语句:['+ myFormula +']:('+str3+')变量不存在!';
	}
    if(fieldtype1!='日期型'&&fieldtype1!='时间型'){
       return '语句:['+ myFormula +']:('+str3+')变量不为日期型或时间型!';
    }
    formula_list1.push(myTableId+'.'+fieldid1);
 }
 return "";
}

function FuncStatisticsCheck(expr){
/*
31、统计函数校验函数
功能描述：校验统计函数是否正确
函数名称：String FuncStatisticsCheck(String expr)
函数主要内容如下：
 */	
var expr2=expr;
var xjflag='';
var pos1=expr.indexOf('where');//'where’在expr中位置;
var sqlwhere='';
if(expr.indexOf('where')>=0){
	sqlwhere=' '+expr.substr(pos1,expr.length-pos1+1);
	expr2=$.trim(expr.substr(0,pos1));
} 
 if(expr2.substr(0,15).toUpperCase()=='GETPERSONNUMBER'){
	 pos1=15;
 }else if(expr2.substr(0,13).toUpperCase()=='GETPERSONTIME'){
	 pos1=13;
 }else{
	 pos1=3;
 }
 formula_type=expr2.substr(0,pos1).toUpperCase();
 len1=expr2.length;
 if(expr2.substr(pos1,1)!='('){
	 return '语句:['+ myFormula +']:'+ formula_type +"函数缺少'('!";  
 }
 if(expr2.substr(len1-1,1)!=')'){
	 return  '语句:['+ myFormula +']:'+ formula_type +"函数缺少')'!";
 }
 
 pos2=expr2.indexOf(",");//逗号在expr2中位置
 if(pos2<0){
	 str1=expr2.substr(pos1+1,len1-pos1-2);
 }else{
	 str1=expr2.substr(pos1+1,pos2-pos1-1);
	 xjflag=expr2.substr(pos2+1,len1-pos2-2);
 }
 var fieldid1;
 var fieldtype1;
 if(pos1==3){  //非GETPERSONNUMBER函数
    var _temp_ret=getfieldinfoEX(str1);
    fieldid1=_temp_ret.fieldid;
    fieldtype1=_temp_ret.fieldkind;
    tableid1=_temp_ret.tableid;
    
    if(tableid1==''||fieldid1==''){
    	return '语句:['+ myFormula +']:('+str1+')变量不存在!';
    }
    if(fieldtype1!='数值型'){
    	return '语句:['+ myFormula +']:('+str1+')变量不为数值型!';
    }
 }else{ //GETPERSONNUMBER函数
    tableid1=GetTableidByTablename(str1);
    if(tableid1==''){
    	return'语句:['+ myFormula +']:('+str1+')变量不存在!';
    }
    fieldid1='';  
 }
 if(_fundebug){
	 alert(xjflag.toUpperCase());
 }
 if(xjflag!=''&&(!(xjflag.toUpperCase()=='TRUE'||xjflag.toUpperCase()=='FALSE'))){
	 return  '语句:['+ myFormula +']:('+xjflag+')应为TRUE或FALSE!';      
 }
 formula_list1.push(tableid1);
 formula_list3.push(tableid1);
 formula_list1.push(tableid1+'.'+fieldid1);
 formula_list1.push(sqlwhere);
 if(xjflag!=''){
	 formula_list1.push(xjflag);
 }
 return "";
	
}

function arithmeticCheck(expr){
/*
33、四则运算表达式校验函数
功能描述：校验四则运算表达式是否正确
函数名称：String arithmeticCheck(String expr)
函数主要内容如下：
*/
//首先进行符号分析
 var len1=expr.length;
 var pos1=0;
 var chr1=expr.substr(pos1,1);
 while(true){
    if(chr1=='+'){
       formula_list1.push('+');
       formula_list2.push('+');
       pos1=pos1+1;
       if(pos1>=len1){
    	   break;
       }
       
       chr1=expr.substr(pos1,1);
       continue;
    }
    if(chr1=='-'){
       formula_list1.push('-');
       formula_list2.push('-');
       pos1=pos1+1;
       if(pos1>=len1){
    	   break;
       }
       chr1=expr.substr(pos1,1);
       continue;
    }
    if(chr1=='*'){
       formula_list1.push('*');
       formula_list2.push('*');
       pos1=pos1+1;
       if(pos1>=len1){
    	   break;
       }
       chr1=expr.substr(pos1,1);
       continue;
    }
	if(chr1=='/'){
		formula_list1.push('/');
		formula_list2.push('/');
		pos1=pos1+1;
		if(pos1>=len1){
			break;
		}
		chr1=expr.substr(pos1,1);
		continue;
	}
    if(chr1=='('){
       formula_list1.push('(');
       formula_list2.push('(');
       pos1=pos1+1;
       if(pos1>=len1){
    	   break;
       }
       chr1=expr.substr(pos1,1);
       continue;
    }
    if(chr1==')'){
       formula_list1.push(')');
       formula_list2.push(')');
       pos1=pos1+1;
       if(pos1>=len1){
    	   break;
       }
       chr1=expr.substr(pos1,1);
       continue;
    }
    if(((chr1>='0')&&(chr1<='9'))||(chr1=='.')){ //数值常量
       str1=chr1;
       formula_list2.push('常量');
       pos1=pos1+1;
       if(pos1>=len1){
          formula_list1.push(str1);
          break;
        }
       chr1=expr.substr(pos1,1);
       while(true){
          if(((chr1>='0')&&(chr1<='9'))||(chr1=='.')){
             str1=str1+chr1;
             pos1=pos1+1;
             if(pos1>=len1){
            	 break;
             }
             chr1=expr.substr(pos1,1);
          }
          else break;
      }
       //alert(str1)
       formula_list1.push(str1);
       if(pos1>=len1){
    	   break;
       }
       continue;
    }
  //变量处理
    var str1=chr1;
    formula_list2.push('变量');
    pos1=pos1+1;
    if(pos1>=len1){
      formula_list1.push(str1);
      break;
    }
    chr1=expr.substr(pos1,1);
    while(true){
       if((chr1!='+')&&(chr1!='-')&&(chr1!='*')&&(chr1!='/')&&(chr1!='(')&&(chr1!=')')){
          str1=str1+chr1;
          pos1=pos1+1;
          if(pos1>=len1){
        	 break;
          }
	      chr1=expr.substr(pos1,1);
       }else{
    	  break;
       }
    }
    formula_list1.push(str1);
    if(pos1>=len1){
    	break;
    }
}
 //符号分析完成

 //分析变量是否有效
 for(var pos1=0;pos1<formula_list1.length;pos1++){
    if(formula_list2[pos1]=='变量'){
    	var _temp_ret=getfieldinfo(myTableId, formula_list1[pos1]);
	   	var fieldid1=_temp_ret.fieldid;
	   	var fieldtype1=_temp_ret.fieldkind;
	   	
       if(fieldid1==''){
    	   return '语句:['+ myFormula +']:['+ formula_list1[pos1]+']项目不存在!';
       }
       if(fieldtype1!='数值型'){
    	   return '语句:['+ myFormula +']:['+ formula_list1[pos1]+']项目不是数值型!';
       }
       formula_list1[pos1]=myTableId+'.'+fieldid1;
    }
 }
 //分析变量完成

 //语法分析
 formula_list1.push('停机');
 formula_list2.push('停机');
 
 funSyntax.pos1=0;
 len1=formula_list2.length-1;
 
 while(funSyntax.pos1<=len1){
	funSyntax.id1= formula_list2[funSyntax.pos1];
    funSyntax.pos1=funSyntax.pos1+1;
    if(is_expression(funSyntax.pos1,funSyntax.id1)){
       if(funSyntax.id1=='停机'){
    	   return "";
       }else{
    	   return formula_error;
       }
     }else{
    	 return formula_error;
     } 
  }
 //语法分析完成
return "";
//<表达式>-><项>+<表达式>|<项>
}

var funSyntax={
		pos1:0,
		id1:''
};

function is_expression(pos1,id1){
/*
	35、是否是表达式
功能描述：校验表达式
函数名称：boolean is_expression(var pos1:integer;var id1:string)
*/	
	funSyntax.pos1=pos1;
	funSyntax.id1=id1;
	
	if(is_term(funSyntax.pos1,funSyntax.id1)==false){
		return false;
	}
	if((funSyntax.id1=='+')||(funSyntax.id1=='-')){
		funSyntax.id1= formula_list2[funSyntax.pos1];
		funSyntax.pos1=funSyntax.pos1+1;
	    if(is_expression(funSyntax.pos1,funSyntax.id1)==false){
	    	return false;
	    }else{
	    	return true;
	    }
	}else{
		return true;
	}
//<项>-><因子>*<项>|<因子>
}

function is_term(pos1,id1){
/*
36、是否是项
功能描述：校验表达式中的项
函数名称：boolean is_term(var pos1:integer;var id1:string)
 */	
	funSyntax.pos1=pos1;
	funSyntax.id1=id1;
		
	if(is_factor(funSyntax.pos1, funSyntax.id1)==false){
		return false;
	}
	if((funSyntax.id1=='*')||(funSyntax.id1=='/')){
		funSyntax.id1=formula_list2[funSyntax.pos1];
		funSyntax.pos1=funSyntax.pos1+1;
	    if(is_term(funSyntax.pos1, funSyntax.id1)==false){
	    	return false;
	    }else{
	    	return true;
	    }
	}else{
		return true;
	}

//<因子>->(<表达式>)|<变量>|<常量>
	
}

function is_factor(pos1,id1){
/*
37、是否是因子
功能描述：校验表达式中的因子
函数名称：boolean is_factor(var pos1:integer;var id1:string)
 */	
	funSyntax.pos1=pos1;
	funSyntax.id1=id1;

	if(funSyntax.id1=='('){
		funSyntax.id1=formula_list2[funSyntax.pos1];
		funSyntax.pos1=funSyntax.pos1+1;
	    if(is_expression(funSyntax.pos1,funSyntax.id1)==false){
	    	formula_error ='语句:['+ myFormula +']:【左括号错误】';
	    	return false;
	    }
	    if(funSyntax.id1!=')'){
	    	formula_error ='语句:['+ myFormula +']:【右括号错误】';
	    	return false;
	    }else{
	    	funSyntax.id1=formula_list2[funSyntax.pos1];
	    	funSyntax.pos1=funSyntax.pos1+1;
	    	return true;
	    }
	 }
	 if(funSyntax.id1!='变量'){
	  if(funSyntax.id1!='常量'){
	      formula_error ='语句:['+ myFormula +']:【错误的常量或变量】';
	      return false;
	    }else{
	     funSyntax.id1=formula_list2[funSyntax.pos1];
	     funSyntax.pos1=funSyntax.pos1+1;
	     return true;
	    }
	 }else{
		funSyntax.id1= formula_list2[funSyntax.pos1];
	    funSyntax.pos1=funSyntax.pos1+1;
	    return true;
	 }
}

function dateConstCheck(expr){
/*
38、日期常量校验函数
功能描述：校验字符串是否是日期常量
函数名称：String dateConstCheck (String expr)
 */	
	var len1=expr.length;
	if(expr.substr(0,1)!="'"){
		return "语句:["+ myFormula +"]: 日期常量前面缺少'!'";
	} 
	  
	if(expr.substr(len1-1,1)!="'"){
		return '语句:['+ myFormula +"]: 日期常量后面缺少'!'";
	}
	 
	var str1=expr.substr(1,len1-2);
	var ymdString =/^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))$/;
	if(ymdString.test(str1)){
		formula_list1.push(str1);
		formula_list2.push('日期型常量');
		return "";
	}else{
		return  '语句:['+ myFormula +']: 日期常量错误!';
	}
}

function charConstCheck(expr){
/*
39、字符常量校验函数
功能描述：校验字符串是否是字符常量
函数名称：String charConstCheck (String expr)
函数主要内容如下：
 */	
	var len1=expr.length;
	if(expr.substr(0,1)!="'"){
		return "语句:["+ myFormula +"]: 字符常量前面缺少' !";
	} 
	  
	if(expr.substr(len1-1,1)!="'"){
		return "语句:["+ myFormula +"]: 字符常量后面缺少'!";
	}
	 
	var str1=expr.substring(1,len1-1);
	formula_list1.push(str1);
	formula_list2.push('字符型常量');
	return "";
}

function FuncGetFieldCheck(expr){
/*
40、getfield函数校验
功能描述：GetField函数用于在人员基本信息等表取数，本校验用于确定取数的字段项是否存在
函数名称：String FuncGetFieldCheck(string expr)
*/	
var len1=expr.length;
 if(expr.substr(8,1)!='('){
	 return '语句:['+myFormula+']:GETFIELD函数缺少"("!';  
 }
 if(expr.substr(len1-1,1)!=')'){
	 return '语句:['+ myFormula +']:GETFIELD函数缺少")"!';
 }
 
 var str1=expr.substr(9,len1-10);

 var pos1=str1.indexOf(",");
 if(pos1<0){
	 return  '语句:['+myFormula+']:GETFIELD函数格式为GETFIELD(<人事字段>,<整数>,1|-1)!';
 }
 //分解函数体
 len1=str1.length;
 var tabfld1=str1.substr(0,pos1);
 str1=str1.substr(pos1+1,len1-1);
 pos1=str1.indexOf(",");
 if(pos1<0){
	 return '语句:['+ myFormula +']:GETFIELD函数格式为GETFIELD(<人事字段>,<整数>,1|-1)!';
 }
 len1=str1.length;
 var Fldrec1=str1.substr(0,pos1);
 var Fldfx1=str1.substr(pos1+1);
 //第三个参数是否正确
 if((Fldfx1!='-1')&&(Fldfx1!='1')){
	 return '语句:['+ myFormula +']:GETFIELD函数第三个参数应为1或-1)!';
 }
 //第二个参数fldrec1必须是大于等于1的整数，否则
 try{
	 var _temp_inttest=parseInt(Fldrec1);
	 if(_temp_inttest<1){
		 return '语句:['+ myFormula +']:GETFIELD函数第二个参数应为大于等于1的整数!';
	 }
 }catch(err){
	 return '语句:['+ myFormula +']:GETFIELD函数第二个参数应为大于等于1的整数!';
 }
 
 //判断人事字段是否存在
 var _temp_filed_ret=getfieldinfoEX(tabfld1);
 var fieldid1=_temp_filed_ret.fieldid;
 var fieldtype1=_temp_filed_ret.fieldkind;
 var tableid1=_temp_filed_ret.tableid;
 
 if(!_temp_filed_ret.has){ 
	 return '语句:['+ myFormula +']:人事变量错误!';
 }
 
 //判断人事字段类型
if(fieldtype1!= left_field_type){
	if(!((left_field_type=='字符型')&&(fieldtype1=='数值型'))){
		return '语句:['+ myFormula +']:数据类型不一致!';
	}
}
 //保存信息
 formula_list2.push(tableid1);
 formula_list3.push(tableid1);
 formula_list2.push(fieldid1);
 formula_list2.push(Fldrec1);
 formula_list2.push(Fldfx1);
 formula_list2.push(fieldtype1);
 formula_type ='GETFIELD';
 return "";
}
