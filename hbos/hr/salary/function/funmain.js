var _currentTab=1;
var _MAXTAB=6;
var _currTr;// 记录主页面上的选中的公式项目
var is_statistics=false;
function init(){
	setParams();
	
	getFunList();
	initWizard();
	if(_fundebug){
		alert(params2.toString());
	}
}

function getFunList(){// 刷新已经设置的公示列表
	a1.value=params.comp_code;
	a2.value=params.table_id;
	a3.value=params.kind_id;
	a4.value=params.copy_code;
	a5.value=params.acct_year;
	
	editTable.submit({name:'hr_salaryFunList'},result,false,'?isCheck=false');
	
	setMainPageButtonStatus(false);
}
function setMainPageButtonStatus(e){
	if(e){
		$("#_up").attr("disabled","");
		$("#_down").attr("disabled","");
		$("#_del").attr("disabled","");
		$("#_edit").attr("disabled","");
		
		
		$("#_setcon").attr("disabled","");
		$("#_clearcon").attr("disabled","");
	}else{
		$("#_up").attr("disabled","disabled");
		$("#_down").attr("disabled","disabled");
		$("#_del").attr("disabled","disabled");
		$("#_edit").attr("disabled","disabled");
		
		$("#_setcon").attr("disabled","disabled");
		$("#_clearcon").attr("disabled","disabled");
	}
}

function initWizard(){
	// init button

	$('button').click(function(){
		doaction($(this).attr("id"));
		if($(this).attr("id")==""){
			var reg=new RegExp("&nbsp;","g"); 
			arithmeticClick($(this).attr('value').replace(reg,""));
		}
	});
	
	initWizardParams();// 取二级页面需要的参数
	
	initWizardShowlist();// 将取到的参数设置到对应控件
}


function initWizardParams(){
	if(params2.table_id=="hr_slr_pay"||params2.table_id=="hr_slr_change"||params2.table_id=="hr_slr_retire_change"){
		params2.calculate_field_list=getFormulaFieldList(params2.comp_code,params2.copy_code,params2.kind_id,params2.user_id,params2.table_id,"set_type='d'",params2.acct_year,"");
	}else{
		params2.calculate_field_list=getFormulaFieldList(params2.comp_code,params2.copy_code,params2.kind_id,params2.user_id,params2.table_id,"field_kind='数值型' or field_kind='日期型' or field_kind='字符型'",params2.acct_year,"");
	}
	params2.express_field_list=getFormulaFieldList(params2.comp_code,params2.copy_code,params2.kind_id,params2.user_id,params2.table_id,"field_kind='数值型'",params2.acct_year,"");
	//params2.date_field_list=getFormulaFieldList(params2.comp_code,params2.copy_code,params2.kind_id,params2.user_id,params2.table_id,"field_kind='数值型'",params2.acct_year,"");
	params2.date_field_list=getFormulaFieldList(params2.comp_code,params2.copy_code,params2.kind_id,params2.user_id,params2.table_id,"field_kind='日期型'",params2.acct_year,"");
	params2.tax_field_list="";// 目前不考虑
	params2.FormulaItemKindList=getFormulaItemKindList();
	//toStr(params2.FormulaItemKindList);
	params2.FormulaItemTableList=getFormulaItemTableList(params2.comp_code,params2.copy_code,params2.user_id,params2.table_id);
	//toStr(params2.FormulaItemTableList);
	//下面一行是因为在点击[设置公式]按钮,在日志里有报错.实际中又用不到,所以注释掉.522 2012.1.15 刘顺发确认
	//params2.FormulaItemFieldList=getFormulaItemFieldList(params2.comp_code,params2.copy_code,params2.user_id,params2.table_id);
	//toStr(params2.FormulaItemFieldList);
	params2.EnabledSumFunc=getEnabledSumFunc(params2.user_id,params2.table_id);
	//toStr(params2.EnabledSumFunc);
}
function getFormulaFieldList(comp_code,copy_code,kind_id,user_id,table_id,para_range,acct_year,acct_month){
	/*
	 * 1、获取公式计算所需字段列表 功能描述：根据传入参数得到公式计算中所需的各种类型的字段列表串，该函数用于获取公式设置demo中各页面所需字段列表
	 * 函数名称：getFormulaFieldList (string comp_code,String copy_code,int
	 * kind_id,int user_id,String table_id,String para_range,String
	 * acct_year,String acct_month)
	 */
	var p="<comp>"+comp_code+"</comp><copy>"+copy_code+"</copy><kid>"+kind_id+"</kid><uid>"+user_id+"</uid><tid>"+table_id+"</tid><prange>"+para_range+"</prange><year>"+acct_year+"</year><month>"+acct_month+"</month>";
	var obj=getParams("getFormulaFieldList",p);
	return obj;
}

function getFormulaItemKindList(){
	/*
	 * 2、获取公式参数项目数据类型列表
	 * 功能描述：根据传入参数得到公式计算中所需的数据表类列表串，该函数用于获取公式设置demo中Sheet5-项目参数页面所需数据表类列表
	 * 函数名称：getFormulaItemKindList ()
	 */
	var obj=getParams("getFormulaItemKindList","");
	return obj;
}

function getFormulaItemTableList(comp_code,copy_code,user_id,table_id){
	/*
	 * 3、获取公式参数项目数据表列表
	 * 功能描述：根据传入参数得到公式计算中所需的数据表列表串，该函数用于获取公式设置demo中Sheet5-项目参数页面所需数据表列表
	 * 函数名称：getFormulaItemTableList (string comp_code,String copy_code,int
	 * user_id,String table_id)
	 */
	var p="<comp>"+comp_code+"</comp><copy>"+copy_code+"</copy><uid>"+user_id+"</uid><tid>"+table_id+"</tid>";
	var obj=getParams("getFormulaItemTableList",p);
	return obj;
}

function getFormulaItemFieldList(comp_code,copy_code,user_id,table_id){
	/*
	 * 4、获取公式参数项目数据字段列表
	 * 功能描述：根据传入参数得到公式计算中所需的数据字段列表串，该函数用于获取公式设置demo中Sheet5-项目参数页面所需数据字段列表
	 * 函数名称：getFormulaItemFieldList (string comp_code,String copy_code,int
	 * user_id,String table_id)
	 * 
	 */
	var p="<uid>"+user_id+"</uid><tid>"+table_id+"</tid><comp>"+comp_code+"</comp><copy>"+copy_code+"</copy>";
	var obj=getParams("getFormulaItemFieldList",p);
	return obj;
}

function getEnabledSumFunc(user_id,table_id){
	/*
	 * 5、公式中是否可以设置汇总函数
	 * 功能描述：根据传入参数得到公式计算中所需的数据字段列表串，该函数用于获取公式设置demo中Sheet5-项目参数页面所需数据字段列表
	 * 函数名称：getEnabledSumFunc() (int user_id,String table_id)
	 * 
	 */
	var p="<uid>"+user_id+"</uid><tid>"+table_id+"</tid>";
	var obj=getParams("getEnabledSumFunc",p);
	return obj;
}

function toStr(o){
	var str="";
	for(var p in o){
		str=str+p+":'"+o[p]+"',";
	}
	if(_fundebug){
		alert(str);
	}
	return str;
}

function initWizardShowlist(){
	/*
	 * isStatistics=false; memo1.text=’’; memo2.text=’’;
	 * cmbFuncKind.items.add(‘日期函数,数学函数,汇总函数,全部函数’) //目前暂不考虑“个人所得税函数”
	 * cmbFuncKind.ItemIndex=0; cmbFuncKindChange(); //建个人所得税表列表，暂不考虑，以后根据需要扩展
	 */
	// 在 “Sheet0-计算项目”建计算项列表
	createFieldList0(params2.calculate_field_list,'Sheet0'); 
	// 在 “Sheet2-四则运算”建四则运算项列表
	createFieldList2(params2.express_field_list,'Sheet2'); 
	// 在 “Sheet4-日期参数”建日期运算项列表
	createFieldList4(params2.date_field_list,'Sheet4'); 
	// createTaxFieldList (参数tax_field_list) //在 “Sheet3-个税参数”建个税运算项列表
	createCmbFunKind();
}

function createFieldList0(rows, id){
	var rStr="";
	if(rows.length>0){
		for(var i=0;i<rows.length;i++){
			var obj=eval("["+rows[i]+"]");			
			rStr=rStr+"<tr id='sheet0"+i+"' onclick='sheet0Click(this);'><td style='display:none'>"+obj[0].tid+"</td><td>"+obj[0].fname+"</td><td>"+obj[0].fid+"</td><td>"+obj[0].kind+"</td></tr>";
		}
		$("#Sheet0").append(rStr);
	}
}
var sheet0_temp={
	fieldmc:'',
	fieldid:'',
	fieldtype:'',
	choosed:false,
	reset:function(){
		this.fieldid="";
		this.fieldmc="";
		this.fieldtype="";
		this.choosed=false;
		
		$('#Sheet0 tr').css("background-color","");
	}		
};
function sheet0Click(t){
	$('#Sheet0 tr').css("background-color","");
	$('#'+t.id+'').css("background-color",window.choseColor);
	
	//alert(params2.calculate_field_list[t.id.substr(6)]);
	//需要赋值
	var obj=eval("["+params2.calculate_field_list[t.id.substr(6)]+"]");
	sheet0_temp.fieldmc=obj[0].fname;
	sheet0_temp.fieldid=obj[0].fid;
	sheet0_temp.fieldtype=obj[0].kind;
	
	sheet0_temp.choosed=true;
}

function createFieldList2(rows,id){
	var rStr="";
	if(rows.length>0){
		for(var i=0;i<rows.length;i++){
			var obj=eval("["+rows[i]+"]");			
			rStr=rStr+"<tr id='"+id+i+"' onclick='sheet2Click(this);'><td style='display:none'>"+obj[0].tid+"</td><td>"+obj[0].fname+"</td><td>"+obj[0].fid+"</td><td>"+obj[0].kind+"</td></tr>";
		}
		$("#"+id).append(rStr);
	}
}

var sheet2_temp={
	tid:'',
	fid:'',
	fname:'',
	kind:'',
	choosed:false,
	reset:function(){
		this.tid="";
		this.fid="";
		this.fname="";
		this.kind="";
		this.choosed=false;
		
		$('#Sheet0 tr').css("background-color","");
	}		
};

function sheet2Click(t){
	//alert(params2.express_field_list[t.id.substr(6)]);
	$('#Sheet2 tr').css("background-color","");
	$('#'+t.id+'').css("background-color",window.choseColor);
	
	var obj=eval("["+params2.express_field_list[t.id.substr(6)]+"]");
	sheet2_temp.tid=obj[0].tid;
	sheet2_temp.fid=obj[0].fid;
	sheet2_temp.fname=obj[0].fname;
	sheet2_temp.kind=obj[0].kind;
	sheet2_temp.choosed=true;
	
	arithmeticListClick();
}

function createFieldList4(rows,id){
	/*
	 * 6、创建公式中显示使用的字段列表 功能描述：创建Sheet0-计算项目等多个页面显示用字段列表
	 * 函数名称：createFieldList(String field_list, TControl ctl)
	 */
	var hr_paylist=new Array();
	
	hr_paylist[0]="{tid:'',fid:'Date()',fname:'Date()',kind:'日期型'}";
	
	if(params2.table_id=="hr_slr_pay"){
		hr_paylist[1]="{tid:'',fid:'PeriodBeginDate()',fname:'PeriodBeginDate()',kind:'日期型'}";
		hr_paylist[2]="{tid:'',fid:'PeriodEndDate()',fname:'PeriodEndDate()',kind:'日期型'}";
	}
	rows=rows.concat(hr_paylist);
	params2.date_field_list=params2.date_field_list.concat(hr_paylist);
	
	var rStr="";
	if(rows.length&&rows.length>0){
		for(var i=0;i<rows.length;i++){
			var obj=eval("["+rows[i]+"]");			
			rStr=rStr+"<tr id='"+id+i+"' onclick='sheet4Click(this);'><td style='display:none'>"+obj[0].tid+"</td><td>"+obj[0].fname+"</td><td>"+obj[0].fid+"</td><td>"+obj[0].kind+"</td></tr>";
		}
		$("#"+id).append(rStr);
	}
} 
var sheet4_temp={
		fieldmc:'',
		fieldid:'',
		fieldtype:'',
		choosed:false,
		reset:function(){
			this.fieldid="";
			this.fieldmc="";
			this.fieldtype="";
			this.choosed=false;
			
			$('#Sheet4 tr').css("background-color","");
		}
};
function sheet4Click(t){
	//alert(params2.date_field_list[t.id.substr(6)]);
	$('#Sheet4 tr').css("background-color","");
	$('#'+t.id+'').css("background-color",window.choseColor);
	
	var obj=eval("["+params2.date_field_list[t.id.substr(6)]+"]");
	sheet4_temp.fieldmc=obj[0].fname;
	sheet4_temp.fieldid=obj[0].fid;
	sheet4_temp.fieldtype=obj[0].kind;
	
	sheet4_temp.choosed=true;
}

function createCmbFunKind(){
	var cmbFunStr='<option value ="日期函数" selected="selected">日期函数</option><option value ="数学函数">数学函数</option><option value="汇总函数">汇总函数</option>';
	if(params2.table_id=='hr_slr_pay'){
		cmbFunStr=cmbFunStr+'<option value="取值函数">取值函数</option>';
	}
	cmbFunStr=cmbFunStr+'<option value="全部函数">全部函数</option>';
	$("#cmbFuncKind").empty();
	$("#cmbFuncKind").append(cmbFunStr);
}

function CreateExprFromListBox(){
	// 8.显示公式的设置情况
	// 功能描述：在0-设置公式总界面的memo1中显示已设置公式的情况
	/*
	 * memo1.text=''; for i=0 to listbox1.items.count-1 {
	 * memo1.text=memo1.text+listbox1.Items[i]; }
	 */
	var str="";
	$('#innermemo1').empty();
	for(var i=0;i<listbox1.Items.length;i++){
		str=str+listbox1.Items[i];
	}
	$('#innermemo1').attr("value",str);
}

function CheckFunctionEnabled(curFunc,curFieldKind){
	// 9、检查函数与计算项字段类型是否匹配
	// 功能描述：检查函数与计算项字段类型是否匹配
	// 函数名称：boolean CheckFunctionEnabled(String curFunc,String curFieldKind)
	//alert("CheckFunctionEnabled("+curFunc+","+curFieldKind+")");
	var func2=curFunc.toUpperCase();
	if((curFieldKind=='字符型'||curFieldKind=='备注型')&&func2=='GETFIELD'){ 
		return true;
	}
	if(curFieldKind=='数值型'){
		var funList={
				GETFIELD:true,
				GETYEAR:true,
				GETMONTH:true,
				GETDAY:true,
				GETHOUR:true,
				GETMINUTE:true,
				GETSECONDS:true,
				GETFULLYEAR:true,
				INT:true,
				DEC:true,
				ROUND:true,
				SUM:true,
				AVG:true,
				MAX:true,
				MIN:true,
				GETPERSONNUMBER:true,
				GETTAX:true,
				GETTAXBEFORE:true,
				GETPERSONTIME:true
				};
		if(funList[func2]){
			return true;
		}
	}
	if(curFieldKind=='日期型'){
		var funList={
				GETFIELD:true,
				DATE:true,
				PERIODBEGINDATE:true,
				PERIODENDDATE:true
				};
		 if(funList[func2]){
			 return true;
		 }
	}
	return false;
}

function setCurrentTr(t){
	_currTr=t.id;
	
	$('#memo1').attr('value',$('#'+_currTr+' td:nth-child(4)').html());
	$('#memo3').attr('value',$('#'+_currTr+' td:nth-child(5)').html());
	
	if($('#'+_currTr+' td:nth-child(6)').html().toUpperCase().indexOf('UPDATE')>0){
		sgtable=params2.table_id;
	}else{
		sgtable="";
	}
	if("1"==$('#'+_currTr+' td:nth-child(10)').html()){
		_stop.setValue(1);
	}else{
		_stop.setValue(0);
	}
	setMainPageButtonStatus(true);
}

function updateFunIsStop(t){
	// 需要先检测是否选中了一个公式
	// 如果有选中的就同步更新其状态到数据库
	var rid=$('#'+_currTr+' td:nth-child(2)').html();
	if(hasSelectFunList(rid)){
		getParams("hr_salaryFun_stop","<stop>"+t.value+"</stop><rid>"+rid+"</rid>");
		// 更新完成后刷新展示数据
		//getFunList();
		$('#'+_currTr+' td:nth-child(10)').html(t.value);
	}else{
		alert("请先选择进行停用的记录！");
	}
	
}

function resetWizard(){
	_currentTab=1;
	is_statistics=false;
	$("#_preStep").attr("disabled","true");
	$("#_nextStep").removeAttr("disabled");
	
	// initTab
	$("#tab1").css("display","");
	$("#tab2").css("display","none");
	$("#tab3").css("display","none");
	$("#tab4").css("display","none");
	$("#tab5").css("display","none");
	$("#tab6").css("display","none");
	
}
var ridForUpDown="";
function doaction(t){
	var rid=$('#'+_currTr+' td:nth-child(2)').html();
	ridForUpDown=rid;
	if(t=="_up"){
		if(hasSelectFunList(rid)){
			moveUp(rid);
			getFunList();
		}
	}else if(t=="_down"){
		if(hasSelectFunList(rid)){
			moveDown(rid);
			getFunList();
		}
	}else if(t=="_add"){
		$("#mainfundiv").css("display","none");
		$("#mainwizard").css("display","");
		$('#PanelQj').css("display","none");
		
		resetWizard();
		params2.rid="";
	}else if(t=="_edit"){
		if(hasSelectFunList(rid)){
			$("#mainfundiv").css("display","none");
			$("#mainwizard").css("display","");
			$('#PanelQj').css("display","none");
			// 根据rid加载公式
			resetWizard();
			setMessForEdit(rid);
		}
	}else if(t=="_del"){
		removeFun(rid);
	}else if(t=="_check"){
		
		checkFun(rid);
	}else if(t=="_print"){
		// 将所显示的公式及条件列表输出到文本文件。
		//alert(t);
		printXmlToCellByXsltFile(result.serverXml,"printView.xsl",null,false,false);
	}else if(t=="_close"){
		if(window.parentPage.document.body.all[params.refreshHandle]){
			window.parentPage.document.body.all[params.refreshHandle].click();
		}
		closePage();
	}else if(t=="_setcon"){
		// 调用条件控件，如果设置条件的公式本身已有条件，则应传入已有条件。设置条件完成后更新数据库，刷新一级界面。
		if(hasSelectFunList(rid)){
			openFW(rid);
		}
	}else if(t=="_clearcon"){
		if(hasSelectFunList(rid)){
			if(confirm("点击“确定”将清空当前选中公式的条件")){
				clearCon(rid);
			}
		}
	}else if(t=="_preStep"){
		if(_currentTab==1){
			$("#_preStep").attr("disabled","true");
		}
		piriorStepClick();
		if(_currentTab!=6){
			$('#PanelQj').css("display","none");
		}
	}else if(t=="_nextStep"){
		nextStepClick();
		if(_currentTab>1){
			$("#_preStep").removeAttr("disabled");
		}
	}else if(t=="_cancel"){
		$("#mainfundiv").css("display","");
		$("#mainwizard").css("display","none");
		
		//需要重置向导的状态
		$("#cmbFuncKind option").eq(0).attr("selected","selected");
		cmbFuncKindChange(cmbFuncKind);
		
		_currentTab=1;
		sheet0_temp.reset();
		sheet4_temp.reset();
		sheet5_temp.reset();
		funcList_temp.reset();
		
		listbox1.clear();
		listbox2.clear();
		listbox3.clear();
		
		$('#innermemo1').empty();
		
		formula_list1=[];   
		formula_list2=[];
		formula_list3=[];
		params2.rid="";
	}else if(t=='_funOk'){
		if(saveFun()){
			clearWizardStatus();
			params2.rid="";
			
			$("#mainfundiv").css("display","");
			$("#mainwizard").css("display","none");
			getFunList();
		}
	}else if(t=='_funCheck'){
		funCheckClick();
	}
}

function reSelectUpDownedTr(rid){
	if(rid==null){
		if(ridForUpDown==""){
			return;
		}
		rid=ridForUpDown;
	}
	var inputs=result.getElementsByTagName("input");
	var tempTrid="";
	for(var i=0;i<inputs.length;i++){
		tempTrid=inputs[i].parentNode.parentNode.id;
		if(tempTrid!=""){
			if(rid==$('#'+tempTrid+' td:nth-child(2)').html()){
				$('#'+tempTrid).click();
				//$('#'+tempTrid).css("background",window.choseColor);
			}
		}
	}
}
function openFW(r){
	if(r){
		if(sgtable == 'hr_talent') {
				openIFDialog(window,'../function/cond/cond_talent.html?load=<cond_type>new</cond_type><rid>'+r+'</rid><sgtable>'+sgtable+'</sgtable>','dialogWidth:520px;dialogHeight:600px');
		}else{
				openIFDialog(window,'../function/cond/cond.html?load=<cond_type>new</cond_type><rid>'+r+'</rid><sgtable>'+sgtable+'</sgtable>','dialogWidth:520px;dialogHeight:600px');
		}
	
	}else{
		alert("请选择设置范围的公式！");
	}
}

function clearWizardStatus(){
	//需要重置向导的状态
	$("#cmbFuncKind option").eq(0).attr("selected","selected");
	cmbFuncKindChange(cmbFuncKind);
	
	_currentTab=1;
	sheet0_temp.reset();
	sheet4_temp.reset();
	sheet5_temp.reset();
	funcList_temp.reset();
	
	listbox1.clear();
	listbox2.clear();
	listbox3.clear();
	
	$('#innermemo1').empty();
	
	//resetWizard();
}

function hasSelectFunList(rid){
	if(rid){
		return true;
	}else{
		return false;
	}
}
function setMessForEdit(rid){
	params2.rid=rid;
	var rets=getParams("hr_funSetGetFunmess_forEdit","<rid>"+rid+"</rid>");
	if(rets.length==1){
		if(_fundebug){
			alert(rets[0][0]+":"+rets[0][1]);
		}
		$('#innermemo1').attr("value",rets[0][0]);
	}
}
function saveFun(){
	// 保存设置的公式
	var gs1=$('#innermemo1').attr("value");

	if(gs1==""){
		return true;
	}
	if(_currentTab==6&&chkQj.checked){
		if(!((beginYM.value==0)&&(endYM.value==0))){
			if(fromAS.value*beginYM.value>toAS.value*endYM.value){
				alert("结束时间不能小于开始时间！");
				return false;
			}
			gs1=gs1+' where 开始期间>=当前期间'+(fromAS.value==1? "之后":"之前")+beginYM.value+'月';
			gs1=gs1+' and 结束期间<=当前期间'+(toAS.value==1? "之后":"之前")+endYM.value+'月';
		}
	}
	var kind_id=0;
	if("hr_slr_pay"==params2.table_id||"hr_slr_change"==params2.table_id||"hr_slr_retire_change"==params2.table_id){
		kind_id=params2.kind_id;
	}
	
	gs1=formulaCheckAndSave(gs1,_currentTab,params2.table_id,params2.comp_code,params2.copy_code,params2.acct_year,kind_id,params2.rid,true);
	if(gs1==""){
		return true;
	}else{
		$('#innermemo2').attr("value",gs1);
		if(_fundebug){
			alert(gs1);
		}
		return false;
	}
}

function moveUp(rid){
	getParams("hr_funSetWizard_moveUP","<rid>"+rid+"</rid>",true);
}

function moveDown(rid){
	getParams("hr_funSetWizard_moveDOWN","<rid>"+rid+"</rid>",true);
}
	
function removeFun(rid){
	// 检查是否有选中的公式，或者先检测，设置remove按钮的状态来保证有公示选中
	// 删除对应的rid的公式
	if(confirm("确定将删除该公式？")){
		getParams("hr_funSetWizard_remove","<rid>"+rid+"</rid>",true);
		// 更新公式列表
		getFunList();
	}
}

function checkFun(rid){
	/*
	 * 逐条公式调用formulaCheckAndSave（）函数，函数返回‘’则显示“公式【formula_desc】正确”,否则显示“公式【formula_desc】”+返回值。
	 * 校验结果显示在memo1右侧的另一个memo中。
	 */
	// 检查是否有选中，检测公式
	//alert("待实现");
	$('#memo3').attr("value","");
	
	$("tr[id^='_tr']").each(function(i){
		var gs1=$(this).children("td")[3].innerHTML;
		var ms=formulaCheckAndSave(gs1,0,params2.table_id,params2.comp_code,params2.copy_code,params2.acct_year,0,"",false);
		if(ms==""){
			$('#memo3').attr("value",$('#memo3').attr("value")+"【"+gs1+"】:公式校验正确！"+"\n");
		}else{
			$('#memo3').attr("value",$('#memo3').attr("value")+ms+"\n");
		}
	});
}

function clearCon(rid){
	getParams("hr_funSetWizard_clearCond","<desc></desc><list></list><cond></cond><rid>"+rid+"</rid>",true);
	// 更新公式列表
	getFunList();
}

function cmbFuncKindChange(t){
	var selectedIndex = $("#"+t.id).attr("selectedIndex");  
	var label = $("#"+t.id+" option").eq(selectedIndex).attr("label");  
	var value = $("#"+t.id+" option").eq(selectedIndex).attr("value");  
	
	/*
	 * 1、函数类别变化事件 功能描述：函数类别变化时刷新函数列表 事件名称：cmbFuncKindChange();
	 */
	funcKind=value;
	$("#funcList").empty();
	var funStr="";
	if(funcKind=='日期函数'||funcKind=='全部函数'){
		funStr=funStr+"<option value='Date'>Date</option>"
			+"<option value='GetYear'>GetYear</option>"
			+"<option value='GetMonth'>GetMonth</option>"
			+"<option value='GetDay'>GetDay</option>"
			+"<option value='GetHour'>GetHour</option>"
			+"<option value='GetMinute'>GetMinute</option>"
			+"<option value='GetSeconds'>GetSeconds</option>"
			+"<option value='GetFullYear'>GetFullYear</option>"
			+"<option value='PeriodBeginDate'>PeriodBeginDate</option>"
			+"<option value='PeriodEndDate'>PeriodEndDate</option>";
	}
	if(funcKind=='数学函数'||funcKind=='全部函数') {
		funStr=funStr+"<option value='Int'>Int</option>"
			+"<option value='Dec'>Dec</option>"
			+"<option value='Round'>Round</option>";
	}
	if (funcKind=='汇总函数'||funcKind=='全部函数')  {
		if(params2.EnabledSumFunc[0]=="true"){
			funStr=funStr+"<option value='Sum'>Sum</option>"
				+"<option value='Avg'>Avg</option>"
				+"<option value='Max'>Max</option>"
				+"<option value='Min'>Min</option>"
				+"<option value='GetPersonTime'>GetPersonTime</option>";
		}
		//alert(params2.table_type+":"+params2.EnabledSumFunc)
	 if ((params2.table_id=='sys_emp_duty') || ((params2.curDataType=='dpt') && params2.EnabledSumFunc=="true")){
		 funStr=funStr+"<option value='GetPersonNumber'>GetPersonNumber</option>";
	 } 
	}
	/*
	 * 此注释掉内容可留做以后扩充用 if (table_id ='hr_slr_pay') and ((funcKind ='个人所得税函数') or
	 * (funcKind ='全部函数')){ funcList.Items.Add('GetTax');
	 * funcList.Items.Add('GetTaxBefore'); }
	 */
	if (((params2.table_id=='hr_slr_pay') || (params2.table_id=='hr_slr_change')) && ((funcKind=='取值函数') || (funcKind=='全部函数')))
	{
		funStr=funStr+"<option value='GetField'>GetField</option>";
	}
	$("#funcList").append(funStr);
	
	$("#funcList option").eq(0).attr("selected","selected");

	funcListClick();
}
function funcListClick(){
/*	
2、函数列表单击事件
功能描述：单击函数时刷新函数功能描述
事件名称：funcListClick()
	事件主要内容如下：
    func= funcList.Items[funcList.ItemIndex]
 MemoGSTS1.text=GetFuncDesc(func)
*/
	funcListChange(funcList);
}
var funcList_temp={
		choose:false,
		fun:'',
		reset:function(){
			this.fun="";
			this.choosed=false;
		}
};
function funcListChange(t){
	var selectedIndex = $("#"+t.id).attr("selectedIndex");
	if(selectedIndex==-1){
		alert('本表不支持汇总函数!');
		return;
	}		
	
	var label = $("#"+t.id+" option").eq(selectedIndex).attr("label");  
	var value = $("#"+t.id+" option").eq(selectedIndex).attr("value");
	
	$('#MemoGSTS1').empty();
	$('#MemoGSTS1').append(GetFuncDesc(value));
	funcList_temp.fun=value;
	funcList_temp.choose=true;
}

var g_funcDescCONST={
GETTAX_CONST:'个人所得税计税函数，GETTAX(个人所得税表，应税工资项)',
GETTAXBEFORE_CONST:'个人所得税税后工资还原税前工资函数，GETTAXBEFOR(个人所得税表，税后工资项)',
DATE_CONST:'取当前日期函数，DATE()，无参数',
PERIODBEGINDATE_CONST:'取帐套当前期间开始日期函数，PERIODBEGINDATE()，无参数',
PERIODENDDATE_CONST:'取帐套当前期间结束日期函数，PERIODENDDATE()，无参数',
GETYEAR_CONST:'计算两个日期之间相差的年数，GETYEAR(<日期表达式>,<日期表达式>)，参数<日期表达式>必须为同表内的项目或DATE()，不能为日期常数。',
GETFULLYEAR_CONST:'计算两个日期之间相差的足日年数，年数差以对日计算，GETFULLYEAR(<日期表达式>,<日期表达式>)，参数<日期表达式>必须为同表内的项目或DATE()，不能为日期常数。',
GETMONTH_CONST:'计算两个日期之间相差的月份，GETMONTH(<日期表达式>,<日期表达式>)，参数<日期表达式>必须为同表内的项目或DATE()，不能为日期常数。',
GETDAY_CONST:'计算两个日期之间相差的天数，GETDAY(<日期表达式>,<日期表达式>)，参数<日期表达式>必须为同表内的项目或DATE()，不能为日期常数。',
GETHOUR_CONST:'计算两个时间之间相差的小时数，GETHOUR(<时间表达式>,<时间表达式>)，参数<时间表达式>必须为同表内的时间型项目或TIME()，不能为时间常数。',
GETMINUTE_CONST:'计算两个时间之间相差的分钟数，GETMINUTE(<时间表达式>,<时间表达式>)，参数<时间表达式>必须为同表内的时间型项目或TIME()，不能为时间常数。',
GETSECONDS_CONST:'计算两个时间之间相差的秒数，GETSECONDS(<时间表达式>,<时间表达式>)，参数<时间表达式>必须为同表内的时间型项目或TIME()，不能为时间常数。',
GETWORKAGE:'计算某人的某个时间的工龄，GETWORKAGE(<用户ID>,<时间>)，参数<时间>为参与计算的时间',
INT_CONST:'取整函数，INT(<数值表达式>)，  参数为<数值表达式>',
DEC_CONST:'取小数函数，DEC(<数值表达式>)，  参数为<数值表达式>',
ROUND_CONST:'四舍五入函数，ROUND(<数值表达式>,<整数>)，参数为1<数值表达式>，参数2为<整数>表示保留的精度，单机版此参数无效',
SUM_CONST:'汇总函数，SUM(<人事项目>)，参数为人事数据表中的项目，其格式为<人事数据表名称>.<项目名称>',
AVG_CONST:'平均值函数，AVG(<人事项目>)，参数为人事数据表中的项目，其格式为<人事数据表名称>.<项目名称>',
MAX_CONST:'最大值值函数，MAX(<人事项目>)，参数为人事数据表中的项目，其格式为<人事数据表名称>.<项目名称>',
MIN_CONST:'最小值函数，Min(<人事项目>)，参数为人事数据表中的项目，其格式为<人事数据表名称>.<项目名称>',
GETPERSONNUMBER_CONST:'统计人数函数，GETPERSONNUMBER(<人事表名>,TRUE|FALSE)，用于部门及岗位统计，不能用于人员数据的运算。 “TRUE|FALSE”为可选项，当统计时若需要上级部门包含下级部门数据，则“TRUE|FALSE”参数为TRUE，否则为FALSE',
GETPERSONTIME_CONST:'统计人次函数，GETPERSONTIME(<人事表名>,TRUE|FALSE)，用于部门及人员的次数统计。“TRUE|FALSE”为可选项，当统计时若需要上级部门包含下级部门数据，则“TRUE|FALSE”参数为TRUE，否则为FALSE',
GETFIELD_CONST:'从人事数据表取项目函数，GETFIELD(<人事项目>,<整数>,1|-1) 取多记录字段函数，类型与左边有关，第一个参数为人事数据表中的项目，其格式为<人事数据表名称>.<项目名称>，第二个参数为取人事数据表的第几条记录，第三个参数为1或-1，1表示按人事表的正序取值，-1表示按人事表的倒序取值。如在工资计算公式中有如下公式：任职日期=Getfield(员工基本情况.任职日期,1,1)，表示人员工资情况中的任职日期是从人员基本情况的任职日期中取值的。'
};

var g_funcDesc={
  GETTAX:g_funcDescCONST.GETTAX_CONST,
  GETTAXBEFORE:g_funcDescCONST.GETTAXBEFORE_CONST,
  DATE:g_funcDescCONST.DATE_CONST,
  GETFULLYEAR:g_funcDescCONST.GETFULLYEAR_CONST,
  PERIODBEGINDATE:g_funcDescCONST.PERIODBEGINDATE_CONST,
  PERIODENDDATE:g_funcDescCONST.PERIODENDDATE_CONST,
  GETYEAR:g_funcDescCONST.GETYEAR_CONST,
  GETMONTH:g_funcDescCONST.GETMONTH_CONST,
  GETDAY:g_funcDescCONST.GETDAY_CONST,
  GETHOUR:g_funcDescCONST.GETHOUR_CONST,
  GETMINUTE:g_funcDescCONST.GETMINUTE_CONST,
  GETSECONDS:g_funcDescCONST.GETSECONDS_CONST,
  GETWORKAGE:g_funcDescCONST.GETWORKAGE_CONST,
  INT:g_funcDescCONST.INT_CONST,
  DEC:g_funcDescCONST.DEC_CONST,
  ROUND:g_funcDescCONST.ROUND_CONST,
  SUM:g_funcDescCONST.SUM_CONST,
  AVG:g_funcDescCONST.AVG_CONST,
  MAX:g_funcDescCONST.MAX_CONST,
  MIN:g_funcDescCONST.MIN_CONST,
  GETPERSONNUMBER:g_funcDescCONST.GETPERSONNUMBER_CONST,
  GETPERSONTIME:g_funcDescCONST.GETPERSONTIME_CONST,
  GETFIELD:g_funcDescCONST.GETFIELD_CONST
};
function GetFuncDesc(formulaFunc){
/*
 3、得到函数功能描述
功能描述：根据函数名得到函数功能描述
函数名称：String GetFuncDesc(String formulaFunc)

 */
	var desc=formulaFunc.toUpperCase();
	return g_funcDesc[desc];
}

var listbox1={
		Items:[],
		count:0,
		clear:function(){
			this.Items=[];
			this.count=0;
		},
		add:function(t){
			this.Items.push(t);
			this.count++;
		}
	};//按步骤记录公式说明的记录器
var listbox2={
		Items:[],
		count:0,
		clear:function(){
			this.Items=[];
			this.count=0;
		},
		add:function(t){
			this.Items.push(t);
			this.count++;
		}

	};//记录所有的当前步骤的页面序号的记录器
var listbox3={
		Items:[],
		count:0,
		clear:function(){
			this.Items=[];
			this.count=0;
		},
		add:function(t){
			this.Items.push(t);
			this.count++;
		}

	};//记录所有的上一步骤的页面序号的记录器

function nextStepClick() {
	/*
	4、下一步事件的处理流程
	功能描述：单击下一步时根据当前步骤的选择确定下一步的走向并跳转到下一步
	事件名称：nextStepClick()
	*/
	var op1; //当前操作步骤
	var fieldid1,fieldmc1,fieldtype1;  //当前计算项字段、字段名称、字段类型
	var func1,hzstr;//当前函数名称、
	if(listbox2.Items.length<1){
		op1=0;                      //当前步骤无内容时就为0
	}else{
		op1=parseInt(listbox2.Items[listbox2.Items.length-1]);//否则从当前步骤记录器得到//当前步骤序号
	}
	
	if(!sheet0_temp.choosed){//Sheet0-计算项列表组件 未选择项目 
		alert('请选择要计算的项目');
		return false;
	}

	fieldmc1=sheet0_temp.fieldmc;
	fieldid1=sheet0_temp.fieldid;
	fieldtype1=sheet0_temp.fieldtype;
	
	func1=funcList_temp.fun.toUpperCase();//Sheet1-选择运算-funcList选择的函数
	if(op1==0){ // 在“Sheet0-计算项目”选择下一步
	    listbox1.clear();
	    listbox2.clear();
	    
	    listbox1.add(fieldmc1+'=');
	    listbox2.add('1');
	    listbox3.add('0');
	    //页面跳转到“Sheet1-选择运算”
	    go2Tab(2,1);
	    CreateExprFromListBox();        //生成公式表达式
	}
	if(op1==1){ // 在“Sheet1-选择运算”选择下一步
		var caculType=$(":radio[name='_caculate']:checked").val();
	    if(caculType=='四则运算'){//选择四则运算
	     listbox1.add('');
	     listbox2.add('2');
	     listbox3.add('1');
	     //页面跳转到“Sheet2-四则运算”
	     go2Tab(3,2);
	    }
	    if(caculType=='函数运算'){
	       if(CheckFunctionEnabled(func1,fieldtype1)==false){
	          alert('函数类型不匹配');
	          return false;
	        }
		    if(func1=='GETFIELD') {
		       listbox1.add('GetField(');
		       listbox2.add('5');
		       listbox3.add('1');
		       //页面跳转到“Sheet5-项目参数”
		       go2Tab(6,2);
		       //根据函数名和计算项生成项目参数字段列表
		       createFormulaItem(func1,fieldtype1,is_statistics);
		       CreateExprFromListBox();                //生成公式表达式
		     }
	     /*以后扩展用
	     if (func1='GETTAX') or (func1='GETTAXBEFORE') {
	        listbox1.items.add(func1+'(');
	         listbox2.Items.add('3');
	         listbox3.Items.add('1');
	         页面跳转到“Sheet3-个税参数”
	         CreateExprFromListBox;        //生成公式表达式
	      }
	      */
	    if((func1=='DATE')||(func1=='PERIODBEGINDATE')||(func1=='PERIODENDDATE')||((func1=='GETPERSONNUMBER')&&(params2.table_id=='sys_emp_duty'))) {
			if((func1=='PERIODBEGINDATE'||func1=='PERIODENDDATE')&&params2.table_id!='hr_slr_pay') {
				alert('[PERIODBEGINDATE]或[PERIODENDDATE]函数只能用于工资福利计算!');
			    return false;
			}
			listbox1.add(func1+'()');
			listbox2.add('2');
			listbox3.add('1');
			CreateExprFromListBox();           //生成公式表达式
	    }
	    if(func1=='GETYEAR'||func1=='GETMONTH'||func1=='GETDAY'||func1=='GETHOUR'||func1=='GETMINUTE'||func1=='GETSECONDS'||func1=='GETFULLYEAR'){
	        listbox1.add(func1+'(');
	        listbox2.add('4');
	        listbox3.add('1');
	        //页面跳转到“Sheet4-日期参数”
	        go2Tab(5,2);
	        CreateExprFromListBox();                 //生成公式表达式
	    }
	    if (func1=='INT'||func1=='DEC'||func1=='ROUND') {
			listbox1.add(func1+'(');
			listbox2.add('2');
			listbox3.add('1');
			//页面跳转到“Sheet2-四则运算”
			go2Tab(3,2);
			CreateExprFromListBox();       //生成公式表达式
		}
	  if (func1=='SUM'||func1=='AVG'||func1=='MAX'||func1=='MIN'||(func1=='GETPERSONNUMBER'&&params2.table_id!='sys_emp_duty')||func1=='GETPERSONTIME') {
	        listbox1.add(func1+'(');
	        listbox2.add('5');
	        listbox3.add('1');
	        //页面跳转到“Sheet5-项目参数”
	        go2Tab(6,2);
	        CreateExprFromListBox();               //生成公式表达式
	        is_statistics=(func1=='GETPERSONNUMBER'||func1=='GETPERSONTIME');
	        //根据函数名和计算项生成项目参数字段列表
	        createFormulaItem(func1,'数值型',is_statistics);
	      }
	   }
	}
	if(op1==2){ //在“Sheet2-四则运算”选择下一步,不做任何跳转处理
		
	}
	/*   //个税不考虑，以后扩展用
	if op1=3 { //在“Sheet3-个税参数”选择下一步
	 if “Sheet3-个税运算项列表组件”未选中 {exit}; //Sheet3-个税运算项列表组件未选择则退出事件
	 if “Sheet3-个税表目录” 未选中 {退出事件};
	 if listbox1.items.count=2 {
	listbox1.items.add(“Sheet3-个税表目录”选中项+','+“ Sheet3-个税运算项列表组件”选中项+')')}
	    else 退出事件;
	    listbox2.Items.add('3');
	    listbox3.Items.add('3');
	    CreateExprFromListBox;   //生成公式表达式
	}
	*/
	
	if(op1==4){ //在“Sheet4-日期参数”选择下一步
		if(!sheet4_temp.choosed){
			return false;
		}
		if(listbox1.Items.length==2){
			listbox1.add(sheet4_temp.fieldmc+',');
		}else if(listbox1.Items.length==3){
			listbox1.add(sheet4_temp.fieldmc+')');
		}else{
			return false;
		}
		listbox2.add('4');
		listbox3.add('4');
		CreateExprFromListBox();    //生成公式表达式
	}
	if(op1==5){ //在“Sheet5-项目参数”选择下一步
		if((!sheet5_temp.choosed)){
			alert("请选择项目！");
			return false;
		}
		 //alert(listbox1.Items.length+":"+listbox1.Items.length);
		 
		 if(listbox1.Items.length==2){
		    if(listbox1.Items[1].toUpperCase()=='GETFIELD('){
		       listbox1.add(sheet5_temp.sname+',1,1)');
		     }else{
		        if(!is_statistics){
		        	hzstr=sheet5_temp.sname;
		        }else{
		        	hzstr=sheet5_temp.sname;
		        }
		       
		        //alert($('#PanelHzxj').css("display"));
		        
		        if($('#PanelHzxj').css("display")=="block"){
		          if(checkBox1.checked){
		        	  hzstr=hzstr+',TRUE';
		          }else{
		        	  hzstr=hzstr+',FALSE';
		          }
		        }
		        hzstr=hzstr+')';
		        if(_fundebug){
		        	alert(hzstr);
		        }
		        listbox1.add(hzstr);
		    }
		 }else{
			 return false;
		 }
		 listbox2.add('5');
		 listbox3.add('5');
		 CreateExprFromListBox();
	}
	if(_currentTab==6&&func1.toUpperCase()!='GETFIELD'){
//	PanelQj.Visible='(页面在第5页)'&&(func1.toUpperCase()!='GETFIELD');
		$('#PanelQj').css("display","block");
		chkQj.checked=false;
	}else{
		$('#PanelQj').css("display","none");
	}
	
	if(_currentTab==6&&(params2.curDataType=='dpt')){
//	PanelHzxj.Visible='(页面在第5页)'&&(params2.curDataType=='dpt');
		$('#PanelHzxj').css("display","block");
	}else{
		$('#PanelHzxj').css("display","none");
	}
}
//进行向导页面切换
function go2Tab(toIndex,hideIndex){
	if(_currentTab!=toIndex){
		_currentTab=toIndex;
		$("#tab"+toIndex).css("display","");
		$("#tab"+hideIndex).css("display","none");
	}
}

function createFormulaItem(fun,ftype,is_statistics){
	//根据参数重新设置树控件
	unitTree.expand=1;
	unitTree.setSqlData("hr_getFormulaItem","<uid>"+params2.user_id+"</uid><tid>"+params2.table_id+"</tid><comp>"+params2.comp_code+"</comp><copy>"+params2.copy_code+"</copy><static>"+(is_statistics? 1:0)+"</static><kind>"+ftype+"</kind><func>"+fun+"</func>");
}
var sheet5_temp={
		choosed:false,
		id:'',
		name:'',
		kind:'',
		tid:'',
		sname:'',
		reset:function(){
			this.id="";
			this.name="";
			this.kind="";
			this.tid="";
			this.sname="";
			this.choosed=false;
		}
};

function loadData(isLeaf,pk,parameter,label){//点击数节点时触发
	//alert("isLeaf:"+isLeaf+",pk:"+pk+",parameter:"+parameter+",label:"+label);
	if(isLeaf){
		sheet5_temp.choosed=true;
		var idString = new RegExp("<tablekid>(.*)</tablekid>");
		var kindString=new RegExp("<tkid>(.*)</tkid>");
		var tidString=new RegExp("<tid>(.*)</tid>");
		var snameString=new RegExp("<sname>(.*)</sname>");
		var hasYearMonth=new RegExp("<hym>(.*)</hym>");
		
		sheet5_temp.id=(pk.match(idString)[1]);
		sheet5_temp.kind=(pk.match(kindString)[1]);
		sheet5_temp.tid=(pk.match(tidString)[1]);
		sheet5_temp.name=label;
		
		if(pk.match(hasYearMonth)[1]==0){
			chkQj.checked=false;
			chkQj.disabled=true;
			$('#PanelQj').css("display","none");
		}else{
			chkQj.disabled=false;
			//chkQj.checked=false;
			$('#PanelQj').css("display","block");
		}
		if(pk.match(snameString)==null){
			sheet5_temp.sname=label;
		}else{
			sheet5_temp.sname=pk.match(snameString)[1];
		}
	}else{
		alert("请选择末级节点");
	}
}

function piriorStepClick(){
/*
5、上一步事件的处理流程
功能描述：单击上一步时根据当前步骤的选择确定上一步的走向并跳转到上一步
事件名称：piriorStepClick()
*/
	var op1; //当前操作步骤
	if(listbox2.Items.length<1){
		return false;  //即未做操作前不做上一步操作
	}
	//alert(listbox3.Items);
	
	op1=listbox3.Items[listbox3.Items.length-1];//取上一步序号
	//跳转到上一步op1页面
	go2Tab(parseInt(op1)+1,_currentTab);

	listbox1.Items.pop();    // 删除当前步骤的信息
	listbox2.Items.pop();    // 删除当前步骤的信息
	listbox3.Items.pop();    // 删除当前步骤的信息
	CreateExprFromListBox();         //显示退到上一步后的公式提示
}

function arithmeticClick(t){
/*
 6、“Sheet2-四则运算”页面的数值及运算符按钮事件
功能描述：单击数值及运算符后的响应处理
事件名称：arithmeticClick()
*/
	$('#innermemo1').attr("value",$('#innermemo1').attr("value")+t);

	listbox1.add(t);
	listbox2.add('2');
	listbox3.add('2');
}

function arithmeticListClick(){
/*
7、“Sheet2-四则运算”页面四则运算项列表组件双击事件
功能描述：单击四则运算项列表组件的响应处理
事件名称：arithmeticListClick()
*/
	$('#innermemo1').attr("value",$('#innermemo1').attr("value")+sheet2_temp.fname);

	//memo1.text=memo1.text+arithmeticList.Selected.Caption;
	listbox1.add(sheet2_temp.fname);
	listbox2.add('2');
	listbox3.add('2');
}

function chkQjClick(t){
	if(!t.checked){
		beginYM.value=0;
		endYM.value=0;
	}
}

function funCheckClick(){
	var gs1=$('#innermemo1').attr("value");
	if(_currentTab==6&&chkQj.checked){
		if(!((beginYM.value==0)&&(endYM.value==0))){
			if(fromAS.value*beginYM.value>toAS.value*endYM.value){
				alert("结束时间不能小于开始时间！");
				return false;
			}
			gs1=gs1+' where 开始期间>=当前期间'+(fromAS.value==1? "之后":"之前")+beginYM.value+'月';
			gs1=gs1+' and 结束期间<=当前期间'+(toAS.value==1? "之后":"之前")+endYM.value+'月';
		}
	}
	var kind_id=0;
	if("hr_slr_pay"==params2.table_id||"hr_slr_change"==params2.table_id||"hr_slr_retire_change"==params2.table_id){
		kind_id=params2.kind_id;
	}
		//formulaCheckAndSave(formula,formulaType,Table_id,comp_code,copy_code,acct_year,kind_id,rid,is_Save)
	gs1=formulaCheckAndSave(gs1,_currentTab,params2.table_id,params2.comp_code,params2.copy_code,params2.acct_year,kind_id,"",false);
	
	if(gs1==''){
		$('#innermemo2').attr("value",'计算公式正确!');
	}else{
		$('#innermemo2').attr("value",gs1);
	}
}
