<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
       <xsl:for-each select="/root/tbody/tr[position()=1]">
		   <tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
		  		<xsl:for-each select="td">
					<th noWrap='true'>
						<xsl:value-of select="."/>
					</th>
				</xsl:for-each>
		    </tr>
      	</xsl:for-each> 
    </thead>
    
    <tbody>
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
		    <tr>
			    <td align='center'>
			    	<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
				      <xsl:attribute name="value">
				          <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					  </xsl:attribute>
					  
					  <xsl:if test="pk/is_select=1">
						  <xsl:attribute name="checked">
						  true
						  </xsl:attribute>
					  </xsl:if>
					</input>
			    </td>
			    <xsl:for-each select="td"> 
				    <xsl:if test="position()!=2">
						<td noWrap='true'>
							<xsl:value-of select="."/>
						</td>
					</xsl:if>
				    <xsl:if test="position()=2">
						<td noWrap='true'>
							<select name="is_seleted" style='font-size:12px;'>
								<xsl:if test=".='继承项'">
								 	<option value="c" selected="select">继承项</option>
								 	<option value="b">清空项</option>
								    <option value="d">计算项</option>
								    <option value="e">津贴项</option>
								</xsl:if>
								<xsl:if test=".='清空项'">
								 	<option value="c">继承项</option>
								 	<option value="b" selected="select">清空项</option>
								    <option value="d">计算项</option>
								    <option value="e">津贴项</option>
								</xsl:if>								
								<xsl:if test=".='计算项'">
								 	<option value="c">继承项</option>
								 	<option value="b">清空项</option>
								    <option value="d" selected="select">计算项</option>
								    <option value="e">津贴项</option>
								</xsl:if>								
								<xsl:if test=".='津贴项'">
								 	<option value="e" selected="select">津贴项</option>
								 	<option value="b">清空项</option>
								    <option value="d">计算项</option>
								    <option value="c">继承项</option>
								</xsl:if>
							</select>
						</td>
					</xsl:if>
			    </xsl:for-each>
		    </tr>
	    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

