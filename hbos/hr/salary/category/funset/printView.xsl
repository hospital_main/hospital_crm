<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td width='400' style='colspan:2;fontsize:maintitle'>工资清除项</td>
					<td style="display:none"></td>
				</tr>
				
				<tr noWrap='true' class='mainHead'>
				  	<td width='200' nowrap='true'>人员类别</td>
				  	<td width='200' nowrap='true'>工资项</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td[position()>1]">							
								<td align="left" width='200'>
									<xsl:value-of select="."/>
								</td>						
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
