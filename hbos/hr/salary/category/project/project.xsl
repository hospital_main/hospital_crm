<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='nodrop nodrag mainHead' id="id_0">
        <th><input type='checkbox' value=''/></th>
      	<th nowrap='true'>序号</th>
      	<th nowrap='true'>薪酬项名称</th>
      	<th nowrap='true'>属性</th>
      	<th nowrap='true'>关联标准</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position()]">
        <tr>
        	<xsl:attribute name="id">id_<xsl:value-of select="position()"/></xsl:attribute>
          <td align='center' style="cursor:auto;">
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="judgeCheck()">
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td"> 
            <td>
              <xsl:value-of select="."/>  
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

