<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>类别id</th>
      	<th nowrap='true'>类别名称</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position()]">
        <tr>
          <xsl:for-each select="td"> 
            <td>
              <xsl:choose>
	          	<xsl:when test="position()=1">
						<xsl:attribute name="style">
							display:none
						</xsl:attribute>
	          	</xsl:when>
	          	<xsl:otherwise>
	              <xsl:value-of select="."/>  
	          	</xsl:otherwise>
	          </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

