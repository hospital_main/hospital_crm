<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th nowrap='true'>项目编码</th>
				<th nowrap='true'>项目名称</th>
				<th nowrap='true'>金额</th>
			</tr>
		</thead>                          
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:variable name="pp" select="position()"/>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" ><xsl:value-of select="pk/code"/></xsl:attribute>
							<xsl:attribute name="ipObj" >ip<xsl:value-of select="$pp"/></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=3">
								<td >
									<input type="text" class="inputDecimal" point="2" maxInput="12" ><!--disabled="false"-->
									<xsl:attribute name="name">ips<xsl:value-of select="$pp"/></xsl:attribute>
									<xsl:attribute name="id">ip<xsl:value-of select="$pp"/></xsl:attribute>
									<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
									</input>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>