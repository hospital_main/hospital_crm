<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/hr/salary/personnel/wage/input/queryView.xsl,v 1.1 2012/07/06 07:11:44 wuzhibo Exp $
 $Author: wuzhibo $
 $Date: 2012/07/06 07:11:44 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
				<tr noWrap='true'>
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;1]">
						<td nowrap='true' style="display:none"><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
				<tr noWrap='true'>
					<xsl:for-each select="/root/tbody/tr[1]/td">
						<td  nowrap='true' style='fontsize:coltitle'>
							
						</td>
					</xsl:for-each>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td"> 
							<td align="left"><xsl:value-of select="."/></td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
