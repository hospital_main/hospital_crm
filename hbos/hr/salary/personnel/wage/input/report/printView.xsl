<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
	  
    <thead>
    	<tr>
		<td style="fontsize:maintitle;colspan:8">机关、事业单位工作人员工资变动审批表</td>
		<td/>
		<td/>
		<td/>
		<td/>
		<td/>
		<td/>
		<td/>	
	</tr>
	<tr>
		<td style="fontsize:10;colspan:8" align="right">单位性质:事业   经费拨款情况:差额   单位:元</td>
		<td/>
		<td/>
		<td/>
		<td/>
		<td/>
		<td/>
		<td/>
	</tr>
      
    </thead>
    <tbody>
    	<tr align="center">
    		<td align="center">姓名</td>
    		<td><xsl:value-of select="/root/tbody/tr[1]/td[9]"/></td>
		<td >性别</td>
    		<td><xsl:value-of select="/root/tbody/tr[1]/td[10]"/></td>
		<td>出生年月</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[11]"/></td>
    		<td>参加工作时间</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[15]"/></td>
    		
    	</tr>
    	<tr align="center">
    		<td colspan="1">学历</td>
    		<td colspan="1"><xsl:value-of select="/root/tbody/tr[1]/td[13]"/></td>
		<td >未计算工龄的学历年限</td>
    		<td colspan="1"></td>
		<td>身份</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[38]"/></td>
    		<td colspan="1">工作单位及职务</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[6]"/></td>
    	</tr>
    	<tr align="center">
    		<td colspan="1" rowspan="2">原职务(岗位)</td>
    		<td colspan="1">职务(岗位)名称</td>
		<td colspan="1"><xsl:value-of select="/root/tbody/tr[1]/td[57]"/></td>
    		
    		
    		<td rowspan="2">现职务(岗位)</td>
    		<td>职务(岗位)名称</td>
    		<td colspan="3"><xsl:value-of select="/root/tbody/tr[2]/td[57]"/></td>
    	</tr>
    	<tr align="center">
	<td></td>
	<td>任职时间</td>
	<td><xsl:value-of select="/root/tbody/tr[1]/td[37]"/></td>
	<td></td>
	<td>任职时间</td>
	<td colspan="3"><xsl:value-of select="/root/tbody/tr[2]/td[37]"/></td>
	</tr>
    	<tr align="center">
    		<td rowspan="9">变动前工资情况</td>
    		<td >执行何种工资标准</td>
    		<td><xsl:value-of select="/root/tbody/tr[1]/td[51]"/></td>
    		<td rowspan="9" valign="center">变动后工资情况</td>
    		<td >执行何种工资标准</td>
    		<td align="center" colspan="3"><xsl:value-of select="/root/tbody/tr[2]/td[51]"/></td>	
    	</tr>
    	<tr align="center">
	<td style="display:none"></td>
	    	<td>级别(岗位)、薪级档次</td>
	    	<td><xsl:value-of select="/root/tbody/tr[1]/td[5]"/></td>
	    	<td style="display:none"></td>
	    	<td>级别(岗位)、薪级档次</td>
	    	<td align="center" colspan="3"><xsl:value-of select="/root/tbody/tr[2]/td[5]"/></td>
    	</tr>
    	<tr align="center">
	<td style="display:none"></td>
	    	<td>职务(岗位)工资</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[1]"/></td>
		<td style="display:none"></td>
	    	<td>职务(岗位)工资</td>
	    	<td align="center" colspan="3"><xsl:value-of select="/root/tbody/tr[2]/td[1]"/></td>
	    </tr>	
      	
      	<tr align="center">
	<td style="display:none"></td>
      		<td>技术等级工资</td>
		<td></td>
		<td style="display:none"></td>
      		<td>技术等级工资</td>
      		<td align="center" colspan="3"></td>
      		<td style="display:none"></td>
      		
      	</tr>
      	<tr align="center">
	<td style="display:none"></td>
      		<td>
      		级别(薪级)工资 
      		</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[2]"/></td>
		<td style="display:none"></td>
      		<td>级别(薪级)工资</td>
      		<td align="center" colspan="3"><xsl:value-of select="/root/tbody/tr[2]/td[2]"/></td>
      		<td style="display:none"></td>
      		
      	</tr>
      	<tr align="center">
	<td style="display:none"></td>
      		<td>
      		工资标准提高部分
      		</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[3]"/></td>
		<td style="display:none"></td>
      		<td>工资标准提高部分</td>
      		<td align="center" colspan="3"><xsl:value-of select="/root/tbody/tr[2]/td[3]"/></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		
      	</tr>
      	<tr align="center">
	<td style="display:none"></td>
      		<td>
      		其他（临时性补贴）
      		</td>
		<td></td>
		<td style="display:none"></td>
      		<td>其他（临时性补贴）</td>
      		<td align="center" colspan="3"></td>
      		<td style="display:none"></td>
      		
      		
      	</tr>
      	<tr align="center">
	<td style="display:none"></td>
      		<td rowspan="2">
      		小计
      		</td>
      		<td rowspan="2"><xsl:value-of select="/root/tbody/tr[1]/td[4]"/></td>
		<td style="display:none"></td>
		<td>小计</td>
		<td align="center" colspan="3"><xsl:value-of select="/root/tbody/tr[2]/td[4]"/></td>
      		
      	</tr>
	<tr align="center">
	<td style="display:none"></td>
	<td style="display:none"></td>
	<td style="display:none"></td>	
	<td style="display:none"></td>	
	<td>月增资额</td>
	<td align="center" colspan="3"><xsl:value-of select="format-number(/root/tbody/tr[2]/td[4]-/root/tbody/tr[1]/td[4],'#,##0.00')"/></td>
	</tr>
      	
      	<tr align="center">
      		<td height="100">工资变动原因</td>
      		<td colspan="7">正常薪级调资</td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		
      	</tr>

    </tbody>
    
    <tfoot>
			<tr>
				<td height="100">呈报单位意见</td>
				<td colspan="2" align="right">  年  月  日</td>
				<td style="display:none"></td>
				<td height="100">主管部门意见</td>
				<td colspan="4" align="right" valign="bottom">  年  月  日</td>
			</tr>
			
			<tr align="center">
				<td height="100">审批单位意见</td>
				<td colspan="7"></td>
			</tr>
    </tfoot>
    </root>
  </xsl:template>
</xsl:stylesheet>

