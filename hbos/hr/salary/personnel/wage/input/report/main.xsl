<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="numbers">
    <thead>
		<tr noWrap='true' class='mainHead'>
			<th align="center" colspan="9">机关、事业单位工作人员工资变动审批表</th>
		</tr>
		<tr  noWrap='true' class='mainHead'>
			<th align="center" colspan="9">单位性质：事业       经费拨款情况：差额         单位： 元   </th>
		</tr>
    </thead>
    <tbody>
    	<!--tr align="center">
    		<td>单位性质：</td>
    		<td>事业</td>
    		<td align="center">经费拨款情况：</td>
    		<td align="center">差额</td>
    		<td align="center">单位：</td>
    		<td align="center" colspan="4">元</td>
    		
    	</tr-->	
    	<tr align="center">
    		<td align="center">姓名：</td>
    		<td><xsl:value-of select="/root/tbody/tr[1]/td[9]"/></td>
		<td >性别：</td>
    		<td><xsl:value-of select="/root/tbody/tr[1]/td[10]"/></td>
		<td >出生年月：</td>
		<td colspan="1"><xsl:value-of select="/root/tbody/tr[1]/td[11]"/></td>
    		<td>参加工作时间：</td>
		<td colspan="2"><xsl:value-of select="/root/tbody/tr[1]/td[15]"/></td>
    		
    	</tr>
    	<tr align="center">
    		<td>学历：</td>
    		<td><xsl:value-of select="/root/tbody/tr[1]/td[13]"/></td>
		<td >未计算工龄的学历年限：</td>
    		<td></td>
		<td>身份：</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[38]"/></td>
    		<td colspan="1">工作单位及职务：</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[6]"/></td>
    	</tr>
    	<tr align="center">
    		<td colspan="1" rowspan="2">原职务(岗位)：</td>
    		<td colspan="1">职务(岗位)名称：</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[57]"/></td>
    		
    		
    		<td colspan="1" rowspan="2">现职务(岗位)：</td>
    		<td colspan="1">职务(岗位)名称</td>
		<td style="display:none"></td>
		<td style="display:none"></td>
    		<td colspan="4"><xsl:value-of select="/root/tbody/tr[2]/td[57]"/></td>
    	</tr>
    	<tr align="center">
	<td colspan="1">任职时间：</td><td colspan="1"><xsl:value-of select="/root/tbody/tr[1]/td[37]"/></td>
	<td colspan="1">任职时间：</td><td colspan="4"><xsl:value-of select="/root/tbody/tr[2]/td[37]"/></td>
	</tr>
    	<tr align="center">
    		<td rowspan="9">变动前工资情况</td>
    		<td >执行何种工资标准</td>
    		<td><xsl:value-of select="/root/tbody/tr[1]/td[51]"/></td>
    		<td rowspan="9">变动后工资情况</td>
    		<td >执行何种工资标准</td>
    		<td align="center" colspan="4"><xsl:value-of select="/root/tbody/tr[2]/td[51]"/></td>
	    		
    		
    		
    	</tr>
    	<tr align="center">
	    	<td>级别(岗位)、薪级档次</td>
	    	<td><xsl:value-of select="/root/tbody/tr[1]/td[5]"/></td>
	    	
	    	<td>级别(岗位)、薪级档次</td>
	    	<td align="center" colspan="4"><xsl:value-of select="/root/tbody/tr[2]/td[5]"/></td>
    	</tr>
    	<tr align="center">
	    	<td>职务(岗位)工资</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[1]"/></td>
	    	<td>职务(岗位)工资</td>
	    	<td align="center" colspan="4"><xsl:value-of select="/root/tbody/tr[2]/td[1]"/></td>
	    </tr>	
    	
      <!--xsl:for-each select="/root/tbody/tr">
        <tr align="center">
          <xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position() = 6 or position() = 7 or position() = 8 or position() = 9 or position() = 10 or position() = 22 or position() = 24 ">
                        <td align='left'>
                          <xsl:value-of select="."/>
                        </td>
                      </xsl:when>
                      <xsl:when test="position() = 11 or position() = 12 or position() = 13 or position() = 14 or position() = 15 or position() = 16 or position() = 17 or position() = 18 or position() = 19 or position() = 20  or position() = 21 or position() = 23">
                        <td align='right'>
                          <xsl:value-of select="format-number(.,'#,##0.00')"/>
                        </td>
                      </xsl:when>
                    </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each-->
      
      	
      	<tr align="center">
      		<td>技术等级工资</td>
		<td></td>
      		<td>技术等级工资</td>
      		<td align="center" colspan="4"></td>
      		<td style="display:none"></td>
      		
      	</tr>
      	<tr align="center">
      		<td>
      		级别(薪级)工资 
      		</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[2]"/></td>
      		<td>级别(薪级)工资</td>
      		<td align="center" colspan="4"><xsl:value-of select="/root/tbody/tr[2]/td[2]"/></td>
      		<td style="display:none"></td>
      		
      	</tr>
      	<tr align="center">
      		<td>
      		工资标准提高部分
      		</td>
		<td><xsl:value-of select="/root/tbody/tr[1]/td[3]"/></td>
      		<td>工资标准提高部分</td>
      		<td align="center" colspan="4"><xsl:value-of select="/root/tbody/tr[2]/td[3]"/></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		
      	</tr>
      	<tr align="center">
      		<td>
      		其他（临时性补贴）
      		</td>
		<td></td>
      		<td>其他（临时性补贴）</td>
      		<td align="center" colspan="4"></td>
      		<td style="display:none"></td>
      		
      		
      	</tr>
      	<tr align="center">
      		<td rowspan="2">
      		小计
      		</td>
      		<td rowspan="2"><xsl:value-of select="/root/tbody/tr[1]/td[4]"/></td>
		<td>小计</td>
		<td align="center" colspan="4"><xsl:value-of select="/root/tbody/tr[2]/td[4]"/></td>
      		
      	</tr>
	<tr align="center">
	<td>月增资额</td>
	<td align="center" colspan="4"><xsl:value-of select="format-number(/root/tbody/tr[2]/td[4]-/root/tbody/tr[1]/td[4],'#,##0.00')"/></td>
	</tr>
      	
      	<tr align="center">
      		<td height="100">工资变动原因：</td>
      		<td colspan="8">正常薪级调资</td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		<td style="display:none"></td>
      		
      	</tr>
      	<tr align="center">
      		<td height="100">呈报单位意见</td>
      		<td colspan="2" align="right" valign="bottom">  年  月  日</td>
      		<td height="100">主管部门意见</td>
		<td colspan="5" align="right" valign="bottom">  年  月  日</td>
      		
      	</tr>
      	<tr align="center">
      		<td height="100">审批单位意见</td>
      		<td colspan="8"></td>
      		
      	</tr>
      	
      	
    </tbody>
  </xsl:template>
</xsl:stylesheet>

