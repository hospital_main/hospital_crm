      var showstop=0,showlower=0,texpands=1;
      //全局变量 职工附属信息ridvalue
      var ridvalue="";
      //条件说明cond_desc 条件列表cond_list 条件内容cond
      var cond_desc = "";
      var cond_list = "";
      var cond = "";
      var changety = '02';
      
      //条件名称
      var cond_title = "";
	        
      function init(){//初始化左侧树
      	condTable2.style.display='none';
      	
      	tdType.style.display="none";//隐藏下拉列表
        var pk=getPk();
        unitTree.expand=1;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        unitTree.init();
        
      }
      
      function loadData(isLeaf,pk,parameter,label){//点击数节点时触发，用于加载右侧的列表数据
        //document.getElementById("firstNode").style.display='';
        //alert(isLeaf+":"+pk+":"+parameter+":"+label);
        
        var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
		xmlDoc.async="false";  
		xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + pk + "</root>"); 
		//从PK中取得dcode sign
		dept_code.value = xmlDoc.getElementsByTagName("dcode")[0].firstChild.nodeValue;
		sign.value = xmlDoc.getElementsByTagName("sign")[0].firstChild.nodeValue;
		inlower.value = xmlDoc.getElementsByTagName("inlower")[0].firstChild.nodeValue;
		userid.value = getUserID();
        hr_slrSalchangeBusijobchangeMain_baseselect.click();
      }

      function show_isstop(t){
        showstop=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
      }
      
      function include_lower(t){
        showlower=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
      }
      
      function show_all(t){
        var pk=getPk();
        if(t.checked){
          texpands=10;
          unitTree.expand=10;
          unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        }else{
          texpands=1;
          unitTree.expand=1;
          unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        }
      }
      
      function getPk(){
        return "<comp_code>"+getCompCode()+"</comp_code>"+"<user_id>"+getUserID()+"</user_id>"
               +"<show_duty>1</show_duty>"+"<show_stop>"+showstop+"</show_stop>"
               +"<include_lower>"+showlower+"</include_lower>"+"<is_super>0</is_super>"
               +"<show_count>0</show_count>";
      }
      
      //通过判断职工的人数来确定下拉列表显示不显示
      function disTypeByCount(){
      	if(result.getRows()>0){
      		tdType.style.display="";
      	}else{
      		tdType.style.display="none";
      		checkFlag=-1;
    	  	selectFlag=true;
      	}
   		tableType.refresh();
   		noDisplayBaseButton();
   		//result2.innerHTML="";
   		result2.style.display="none"; //隐藏result2
      }
      
      //判断result复选框 互斥
      var checkFlag=-1;    //记录第一次选择的复选框的行号
      var selectFlag=true; //判断是否需要查询
	  function judgeCheck(){
	  	var count = 0;
      	var data = result.getElementsByTagName('tbody')[1];
    	var trs = data.getElementsByTagName("tr");
    	for (var i=0; i<trs.length; i++){
    		var inputs =  trs[i].getElementsByTagName("input");
    		for (var j=0; j<inputs.length; j++) {	        
		        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
		        	count++;
		        	if(checkFlag==-1){
		        		checkFlag=i;
		        		selectFlag=true;
		        	}else{
		        		if(checkFlag!=i){
			        		if(result.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag]!=null){
					    		result.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag].getElementsByTagName("input")[0].checked=false;
					    	}
		        			checkFlag=i;
		        			selectFlag=true;
		        		}else{
		        			selectFlag=false;
		        		} 
		        	}
		        }
	    	}
    	}
    	//恢复复选框行号记录初始值
    	if(count==0){
    		checkFlag=-1;  //恢复复选框行号记录初始值。
	    	noDisplayBaseButton();  //如果没选择职工，按钮不显示
    	}   
	  }
      //当点击职工信息列表时判断条件以便产生附属信息记录
      function getSubInfoList(selectChange){
      	if(result.getRows()==0){
      		checkFlag=-1;  //如果result记录数为零，恢复复选框行号记录初始值。
      	}else{
	      	judgeCheck();
	      	var empidTemp = "";
	      	var tableidTemp=tableType.value;
	      	//判断选择下拉框中是否有值
	      	if(tableidTemp==null || tableidTemp.length==0){
	      		result2.innerHTML="";  //清空result2
		    	noDisplayBaseButton();  //如果下拉框为空，按钮不显示
	      		return false;
	      	}
	      	
	      	var count = 0;
	      	var data = result.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr"); 
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	//得到选择的职工empid
			        	var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
						xmlDoc.async="false";  
						xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + inputs[j].value + "</root>"); 
						empidTemp = xmlDoc.getElementsByTagName("empid")[0].firstChild.nodeValue;
			        	count++;
			        }
		    	}
	    	}
	    	//判断是否选择职工列表中的记录
	    	if (count==0) {
	    	  result2.innerHTML="";
	    	  noDisplayBaseButton();  //如果没有选择职工记录，按钮不显示
	    	  if(selectChange=="") 
	    	    return false;
	    	  alert('请选择一条职工记录!');
		      return false;
		    }
		    if (count > 1) {
		      result2.innerHTML="";
		      alert('只能选择一条职工记录!');
		      return false;
		    }
		    if(!selectFlag && selectChange=="") return false; //如果result选择未发生改变并且下拉框也未改变就不查询数据库
		    subInfoUserid.value = getUserID();
		    subInfoEmpid.value = empidTemp;
		    subInfoTableid.value = tableidTemp;
		    hr_slrSalchangeBusijobchangeMain_select.click();
		    result2.style.display="";  //显示result2
	  	}
      	result.adjPosition();
      }
      
      //按钮权限查询
      function getPurview(){
        var acctyear = '';
        acctyear = getAcctYear();
        if(acctyear == ''){
          addPermButton.style.display="none";
          changeSalPermButton.style.display="none";
        }else{
      	  window.xmlhttp.post('hr_slrSalchangeBusijobchangeMain_buttonPurview', "<tableid>"+tableType.value+"</tableid><userid>"+getUserID()+"</userid><empid>"+subInfoEmpid.value+"</empid>",'?isCheck=false');
		  var resText = window.xmlhttp._object.responseXml;
      	
      	  var addPerm = resText.getElementsByTagName("td")[0].firstChild.nodeValue;
      	  var salPerm = resText.getElementsByTagName("td")[1].firstChild.nodeValue;
      	  if(addPerm!='0' && tableType.value == 'hr_slr_dutylevel'){
            addPermButton.style.display="";
      	  }else{
      		addPermButton.style.display="none";
      	  }
      	  salPerm = '1';
      	  if(salPerm!='0' && tableType.value == 'hr_slr_change'){
      	    changeSalPermButton.style.display="";
      	  }else{
      	  	changeSalPermButton.style.display="none";
      	  }
      	}
      }
      
      //判断result2复选框 互斥
      var checkFlag2=-1;    //记录第一次选择的复选框的行号
	  function judgeCheck2(){
	  	if(result2.getRows()==0){
      		checkFlag2=-1;  //如果result2记录数为零，恢复复选框行号记录初始值。
      	}else{
		  	var count = 0;
	      	var data = result2.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(checkFlag2==-1){
			        		checkFlag2=i;
			        	}else{
			        		if(checkFlag2!=i){
			        		    if(result2.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag2]!=null){
						    		result2.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag2].getElementsByTagName("input")[0].checked=false;
			        			}
			        			checkFlag2=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0) checkFlag2=-1;  //恢复复选框行号记录初始值
	    }
	  }
	  
	  //添加
	  function insert(){
	  	ridvalue="";
      	openIFDialog(window,'insert.html','dialogWidth:520px;dialogHeight:349px')
	  }
	  //添加页面需要的参数
      function insertMess(id){
			this.pk={id:'rid',value:id+"##"+subInfoTableid.value};
			this.button={text:'保存',name:'hr_slrSalchangeBusijobchangeMain_insert',onclick:'if(editTable.submit(this)){closePage();window.parentPage.refreshParent();}',accessKey:'S'};
	  }
	  //添加页面需要的参数
	  function insertCallbacks(){
				setInsertPage('hr_slrSalchangeBusijobchangeMain_showFields','<tableid>'+tableType.value+'</tableid><userid>'+getUserID()+'</userid>','',"<rid>"+ridvalue+"</rid><userid>"+getUserID()+"</userid>",new insertMess(ridvalue));
	  }
	  
	  //调整津补贴
      function changeSal(){
        openDialog("../../salchange/newmember/changeSal.html?load=<empid>"+subInfoEmpid.value+"</empid>",'dialogWidth:700px;dialogHeight:520px');
      }
      //核定工资
      function checkSal(){
      	window.xmlhttp.post('hr_slrSalchangeChangeMaintenance_checksal', "<userid>"+getUserID()+"</userid><empid>"+subInfoEmpid.value+"</empid>",'?isCheck=false');
		var resText = window.xmlhttp._object.responseXml;
		var temp = resText.xml;
		if(temp.indexOf('核定工资成功') > 0){
		  refreshParent();
		  alert('核定工资成功');
		}else{
          var resultContent=temp.substr(temp.indexOf("<msg>")+"<msg>".length);
 		  resultContent=resultContent.substr(0,resultContent.indexOf("</msg>"));
		  refreshParent();
		  alert(resultContent);
		}
      }
	  //刷新结果集
      function refreshParent(){
		window.document.body.all['hr_slrSalchangeBusijobchangeMain_select'].click();
	  }
	  
	  
      
      //不显示按钮
      function noDisplayBaseButton(){
      	  addPermButton.style.display="none";
	   	  changeSalPermButton.style.display="none";
      }
      
      //科室职位目录按钮
	  function deptDutyList(){
	  	treeTable.style.display='';
	  	unitTree.style.display='';
	  	condTable.style.display='none';
	  	condTable2.style.display='none';
	  	//清空页面上的相关信息
	  	clearRelationInfo();
	  }
	  
	  //清空页面上的相关信息
	  function clearRelationInfo(){
        result.innerHTML = "";  //清空职工列表
        tdType.style.display="none";  //不显示下拉列表
   		checkFlag=-1;  //初始化result的选择初始值
   		selectFlag=true;  //初始化 判断是否需要查询变量
   		checkFlag2=-1;    //初始化result2的选择初始值
   		noDisplayBaseButton();  //不显示附属信息功能按钮
   		result2.style.display="none"; //隐藏result2
	  }
	  
	  //条件查询按钮 
	  function condSelect(){
	  	treeTable.style.display='none';
	  	unitTree.style.display='none';
	  	condTable.style.display='';
	  	condTable2.style.display='none';
	  	commonCondDispaly.value = '';
        //清空页面上的相关信息
	  	clearRelationInfo();
	  	commonCondSelectByUserid.value = getUserID();
	  	hr_slrSalchangeNewmemberMain_commonCondSelect.click();
	  }
	  
	  
	  function condSelect2(){	  	
	  	unitTree.style.display='none';
	  	condTable.style.display='none';
	  	condTable2.style.display='';
	  		  	
	  		  	//alert(2);
	  	//cond_item_list.style.display="";
	  	user_id.value = getUserID();
	  	change_type.value = changety;
	  	commonCondSelectByUserid.value = getUserID();
	  	treeTable.style.display="none";
        //// 清空职工列表
        //alert(1);
     result.innerHTML = "";
	  		
	  }
	  
	  
	  
	  //判断commonCondList复选框 互斥 给条件显示框赋值具体条件
      var commonCondListCheckFlag=-1;    //记录第一次选择的复选框的行号
	  function judgeCommonCondListCheck(){
	  	if(commonCondList.getRows()>0){
		  	var count = 0;
	      	var data = commonCondList.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(commonCondListCheckFlag==-1){
			        		commonCondListCheckFlag=i;
			        	}else{
			        		if(commonCondListCheckFlag!=i){
						    	commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
			        			commonCondListCheckFlag=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0){ 
	    		commonCondListCheckFlag=-1;  //恢复复选框行号记录初始值
	    		commonCondDispaly.value = "";  //清空条件显示框
				//清空页面上的相关信息
	  			clearRelationInfo();
	    	}else{
	    		//给条件显示框赋值具体条件
				commonCondDispaly.value = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				//按常用条件查询职工
				var userid_cond = getUserID();
				var common_cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
				var common_cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
				var common_cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				//cond=common_cond;
				//cond_list=common_cond_list;
				cond_desc=common_cond_desc;
				
				//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  
				common_cond = common_cond.replace(/</g,"&lt;");
				common_cond = common_cond.replace(/>/g,"&gt;");
				//alert(common_cond);
				
				cond_list = common_cond_list;
				cond = common_cond;
				cond_desc = common_cond_desc;
				
				//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  
				//common_cond = common_cond.replace("<","&lt;");
				//common_cond = common_cond.replace(">","&gt;");
				//window.xmlhttp.post('hr_slrSalchangeChangeMaintenance_findEmpByCommonCond', "<userid_cond>"+userid_cond+"</userid_cond><common_cond>"+common_cond+"</common_cond><common_cond_list>"+common_cond_list+"</common_cond_list>");
				//var resText = window.xmlhttp._object.responseXml;
				//result.setDataXml(resText.xml);
				//因为setDataXml方法不能设分页，所以只能用editTable1提交方式查询
				userid_cond1.value = userid_cond;
				common_cond1.value = common_cond;
				common_cond_list1.value = common_cond_list;
				changetype.value = changety;
				hr_slrSalchangeNewmemberMain_findEmpByCommonCond.click();
				disTypeByCount();  //通过判断职工的人数来确定下拉列表显示不显示
	    	}
	    }
	  }
	  
	  //新条件 在页面顶定义三个全局变量 条件说明cond_desc 条件列表cond_list 条件内容cond
	  function newCond(){
	  		//把全局变量清空
	  		cond_desc = "";
	  		cond_list = "";
	  		cond = "";
	  		//清空条件显示框
			commonCondDispaly.value = "";
			//清空页面上的相关信息
  			clearRelationInfo();
	  		if(commonCondListCheckFlag!=-1){  //判断是否选择了常用条件
		  		//把列表commonCondList的复选框选择去掉
		  		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
		  		//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1;  
		  		
			}
  			openIFDialog(window,'../cond/cond.html?load=<cond_type>new</cond_type><changety>'+changety+'</changety>','dialogWidth:520px;dialogHeight:520px');
	  }
	  
	  //修改条件
	  function updateCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请选择条件！！！");
	  		return false;
	  	}else{
	  	/*
	  		if(commonCondListCheckFlag==-1){
	  			alert("请选择一个常用条件！！！");
	  			return false;
	  		}else{
		  		//条件说明cond_desc 条件列表cond_list 条件内容cond
		  		cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
		  		cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
		  		cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
		  		//把列表commonCondList的复选框选择去掉
		  		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
		  		//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1;  
		  		//清空条件显示框
				commonCondDispaly.value = "";
				*/
        		//处理特殊字符 "<"、">" 变成 "小于"、"大于"  
				cond = cond.replace(/</g,"小于");
				cond = cond.replace(/>/g,"大于");
				//处理特殊字符 "&lt;"、"&gt;" 变成 "小于"、"大于"  
				cond = cond.replace(/&lt;/g,"小于");
				cond = cond.replace(/&gt;/g,"大于");
				//清空页面上的相关信息
	  			clearRelationInfo();
		  		openIFDialog(window,'../cond/cond.html?load=<cond_type>update</cond_type><cond_desc>'+cond_desc+'</cond_desc><cond_list>'+cond_list+'</cond_list><cond>'+cond+'</cond><changety>'+changety+'</changety>','dialogWidth:520px;dialogHeight:520px');
		  	//}
	  	}
	  }
	  
	  //添加新条件后判断按钮的显示
	  function getPurviewAfterInsertCond(){
	  		disTypeByCount(); 
	  }
	  
	  //保存条件 输入条件名称
	  function saveCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
	  		openIFDialog(window,'../../../condName.html','dialogWidth:400px;dialogHeight:200px');
	  	}
	  }
	  
	  //保存条件
	  function saveCond1(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
  			//var cond_title = window.prompt("请输入输入常用条件名称！！！","");
  			if(cond_title==null || cond_title.length==0){
  				alert("条件名称为空！！！");
  				return false;
  			}else{
	  			//getUserID() 条件名称cond_title 条件说明cond_desc 条件列表cond_list 条件内容cond
  				cond = cond.replace(/</g,"&lt;");
				cond = cond.replace(/>/g,"&gt;");
	  			window.xmlhttp.post('hr_slrSalchangeNewmemberMain_insertCond', "<user_id>"+getUserID()+"</user_id><cond_title>"+cond_title+"</cond_title><cond_desc>"+cond_desc+"</cond_desc><cond_list>"+cond_list+"</cond_list><cond>"+cond+"</cond>");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空页面上的相关信息
		  		clearRelationInfo();
	  			//刷新常用条件
	  			hr_slrSalchangeNewmemberMain_commonCondSelect.click();
	  		}
	  	}
	  }
	  
	  //删除条件
	  function deleteCond(){
	  	if(commonCondListCheckFlag==-1){
	  		alert("请选择一个常用条件！！！");
	  		return false;
	  	}else{
	  		if(confirm('确定删除该常用条件吗？')){
		  		var cond_id = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[2].firstChild.nodeValue;
		  		window.xmlhttp.post('hr_slrSalchangeNewmemberMain_deleteCond', "<cond_id>"+cond_id+"</cond_id>");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空页面上的相关信息
	  			clearRelationInfo();
	  			//刷新常用条件
	  			hr_slrSalchangeNewmemberMain_commonCondSelect.click();
	  		}else{
	  			return false;
	  		}
	  	}
	  }
  function IsExistsFile(filepath)
      {
          var xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          xmlhttp.open("GET",filepath,false);
          xmlhttp.send();
          if(xmlhttp.readyState==4){   
              if(xmlhttp.status==200) openDialog(window.prefix+filepath,'dialogWidth:800px;dialogHeight:600px',null,'true');  //url存在   
              else if(xmlhttp.status==404) alert("该照片已经丢失!请重新上传"); //url不存在   
              else return alert("该照片已经丢失!请重新上传");//其他状态   
          } 
      }