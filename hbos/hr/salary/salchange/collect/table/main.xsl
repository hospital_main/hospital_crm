<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th rowspan='2'>职工编码</th>
				<th rowspan='2'>职工姓名</th>
        <xsl:for-each select="/root/tbody/tr[position()=1]/td[position() &gt; 3]">
        	<xsl:variable name="cols" select="position()"/>
        	<xsl:variable name="cols_name" select="."/>
        	<xsl:variable name="counts" select="count(../td[text()= $cols_name]) "/>
        	<xsl:choose>
          	<xsl:when test=" text()!=../td[$cols+2 ] ">
        			<th>
        				<xsl:attribute name="colspan" ><xsl:value-of select="$counts"/></xsl:attribute>
	            	<xsl:value-of select="."/>
        			</th>
          	</xsl:when>
          	<xsl:otherwise>
          		<th style="display:none"></th>
          	</xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
  			<th style="display:none"></th>
  			<th style="display:none"></th>
        <xsl:for-each select="/root/tbody/tr[position()=2]/td[position() &gt; 3]">
        	<th><xsl:value-of select="."/></th>
        </xsl:for-each>
  		</tr>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr[position()>2] ">
				<tr>
					<xsl:for-each select="td[position() &gt; 1]">
						<td><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>