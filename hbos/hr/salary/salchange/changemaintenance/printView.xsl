<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
   <root>
    
    	<thead>
    	<!--
       	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
	  		<tr noWrap="true" class="mainHead">
					<td>标题</td>
					<td>制单日期</td>
					<td>开始日期</td>
					<td>结束日期</td>
					<td>操作人</td>
			</tr>
	-->
	 		<tr noWrap="true" class="mainHead">
	 			<xsl:for-each select="/root/tbody/tr[position()=1]">
					<xsl:for-each select="td">
	              		<td nowrap='true'><xsl:value-of select="."/></td>
					</xsl:for-each>
				</xsl:for-each>
	 		</tr>
   		</thead>
   
	  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr[position()!=1]">
	  			<tr>
					<xsl:for-each select="td">
	              		<td nowrap='true'><xsl:value-of select="."/></td>
					</xsl:for-each>
					</tr>
			</xsl:for-each>
	  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>