function getParams(l,p,showMess){
	subbody.load=l;
	subbody.para=p;
	if(showMess){
		subbody.hideMsg=false;
	}else{
		subbody.hideMsg=true;
	}
	subbody.post();
	return subbody.getHiddenVs();
}
function getTableKindByTableId(tableid){
/*
5.20得到数据表类型
功能描述：查询某个数据表的数据类型
函数名称：String getTableKindByTableId (varchar(20) table_id);
	处理逻辑：             
            Select table_kind_id from sys_hr_table_content where table_id=参数table_id
            return table_kind_id
*/
alert("函数getTableKindByTableId：已经以数据库函数的形式实现了");	
}
function GetFieldNameByTableIdAndFieldId(tableid,fieldid){
/*
5.21得到字段名称
功能描述：查询某个字段的字段名称
函数名称：String GetFieldNameByTableIdAndFieldId(table_id,field_id)
处理逻辑：             
            Select field_name from sys_hr_table_structure where table_id=参数table_id and field_id=参数field_id
            If 记录存在 return field_name else ruturn ‘’	  	
*/
	var obj=getParams("hr_GetFieldNameByTableIdAndFieldId","<tid>"+tableid+"</tid><fid>"+fieldid+"</fid>");
	if(obj.length==1){
		return obj[0][0];
	}else{
		return "";
	}
}


function getfieldinfo(table_id,field_name){
/*
5.23得到字段信息
功能描述：查询某个字段名称的字段信息
函数名称：
getfieldinfo(table_id, field_name:string;var fieldid,fieldkind:string):boolean;
	处理逻辑：             
            Select field_id,field_kind from sys_hr_table_structure where table_id=参数table_id and field_name=参数field_name
            If 记录存在 {
             输出参数fieldid=field_id
             输出参数fieldkind=field_kind
 return true 
}  else ruturn false
*/
	var obj=getParams("hr_getfieldinfo","<tid>"+table_id+"</tid><fname>"+field_name+"</fname>");
		
	if(obj.length==1){
		return {fieldid:obj[0][0],fieldkind:obj[0][1],has:true};
	}else{
		return {fieldid:"",fieldkind:"",has:false};
	}
}



function gettargetinfo(field_name){
/*
5.23得到字段信息
功能描述：查询某个字段名称的字段信息
函数名称：
getfieldinfo(table_id, field_name:string;var fieldid,fieldkind:string):boolean;
	处理逻辑：             
            Select field_id,field_kind from sys_hr_table_structure where table_id=参数table_id and field_name=参数field_name
            If 记录存在 {
             输出参数fieldid=field_id
             输出参数fieldkind=field_kind
 return true 
}  else ruturn false
*/
	var obj=getParams("hr_gettargetinfo","<fname>"+field_name+"</fname>");
		//alert(obj[0][0]);
	if(obj.length==1){
		return {fieldid:obj[0][0],fieldkind:obj[0][3],fielddict:obj[0][4],has:true};
	}else{
		return {fieldid:"",fieldkind:"",fielddict:"",has:false};
	}
}


function getfieldinfoEX(tabfld){
/*	
5.24根据“table_name.field_name”得到字段信息
功能描述：查询某个字段名称的字段信息
函数名称：
getfieldinfoEX(tabfld:string;var tableid,fieldid,fieldkind:string):boolean;
处理逻辑：  
 */
	var pos1=tabfld.indexOf(".");// '.'在tabfld中的位置
    if(pos1<0){
    	return {tableid:"",fieldid:"",fieldkind:"",has:false};
    }
	var	len1=tabfld.length;
	var tn1=tabfld.substr(0,pos1);
	var fn1=tabfld.substr(pos1+1,len1-pos1);
	var tableid = GetTableidByTablename(tn1);
	if(tableid==''){
		return {tableid:"",fieldid:"",fieldkind:"",has:false};
	}
	var rets=getfieldinfo(tableid,fn1);
	
	return {tableid:tableid,fieldid:rets.fieldid,fieldkind:rets.fieldkind,has:rets.has}; 
}

function GetTableidByTablename(tableName){
/*
5.25根据table_name得到table_id
功能描述：查询某个数据表的信息
函数名称：string GetTableidByTablename(String tableName)
处理逻辑：
    select table_id from sys_hr_table_content where table_name=参数table_name
  如果记录存在  return table_id 否则 return ‘’
*/
	var obj=getParams("hr_GetTableidByTablename","<tname>"+tableName+"</tname>");
	if(obj.length==1){
		return obj[0][0];
	}else{
		return "";
	}
}

  function template211(){
  /*
  	
  */	
  }