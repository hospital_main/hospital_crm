<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
		  <colgroup>		       
				<col style = 'width:70'/>
				<col style = 'width:180'/>
				<col style = 'width:180'/>
			</colgroup>  		
    	<thead>
    		<tr noWrap='true' class='mainHead'>
    			<td nowrap='true'>���</td>
    			<td nowrap='true'>����</td>
    			<td nowrap='true'>����</td>
  	  	</tr>
    	</thead>
    	<tbody>
  	    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:for-each select="td">
			          <xsl:choose>
			          	<xsl:when test="position()=4 or position()=2 or position()=3">
              		<td>
										<xsl:value-of select="."/>
									</td>
			          	</xsl:when>
			          	<xsl:otherwise>

			          	</xsl:otherwise>
			          </xsl:choose>
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>