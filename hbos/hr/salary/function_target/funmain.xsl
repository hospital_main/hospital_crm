<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox' value=''/></th>
      	<th nowrap='true'>���</th>
      	<th nowrap='true'>����</th>
      	<th nowrap='true'>����</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr onclick="setCurrentTr(this);">
        <xsl:attribute name="id">_tr<xsl:value-of select="position()"></xsl:value-of></xsl:attribute>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
					<td>
	          <xsl:choose>
	          	<xsl:when test="position()=4 or position()=2 or position()=3">
								<xsl:value-of select="."/>
	          	</xsl:when>
	          	<xsl:otherwise>
		              <xsl:attribute name="style">display:none</xsl:attribute>
		              <xsl:value-of select="."/>
	          	</xsl:otherwise>
	          </xsl:choose>
					</td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

