<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>岗位</th>
      	<th nowrap='true'>职龄下限</th>
      	<th nowrap='true'>职龄上限</th>
      	<th nowrap='true'>工龄下限</th>
      	<th nowrap='true'>工龄上限</th>
      	<th nowrap='true'>薪级</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position()]">
        <tr>
          <xsl:for-each select="td"> 
            <td>
              <xsl:value-of select="."/>  
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

