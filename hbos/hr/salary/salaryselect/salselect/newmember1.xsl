<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		<xsl:for-each select="/root/t2head/tr">
			<tr noWrap="true" class="mainHead">
				<xsl:for-each select="th">
					<xsl:if test="position()=1">
						<th noWrap="true">
							<input type='checkbox' TABINDEX='-1' style='font-size:8px;display:none'>
						      <xsl:attribute name="value">
						          <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							  </xsl:attribute>
							</input>
						</th>
						<th noWrap="true">
							<xsl:value-of select="." />
						</th>
					</xsl:if>
					<xsl:if test="position()!=1">
						<th noWrap="true">
							<xsl:value-of select="." />
						</th>
					</xsl:if>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</thead>
    
    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
		    <tr>
			    <td align='center'>
			    	<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
				      <xsl:attribute name="value">
				          <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					  </xsl:attribute>
					</input>
			    </td>
			    <xsl:for-each select="td"> 
			    <xsl:if test="contains(.,'/upload/') and contains(.,'jpg')">
						<td><a>
						<xsl:attribute name="href">javascript:openDialog(window.prefix+'<xsl:value-of select="."/>','dialogWidth:800px;dialogHeight:600px',null,'true');</xsl:attribute>
						�鿴	</a>
						</td>
					</xsl:if>
					<xsl:if test="not(contains(.,'/upload/') and contains(.,'jpg'))">
						<td align="left" noWrap='true'><xsl:value-of select="."/></td>
					</xsl:if>
			    </xsl:for-each>
		    </tr>
	    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

