<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
    <thead>
      	<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>职工编码</td>
				<td nowrap='true'>职工姓名</td>
				<td nowrap='true'>编制科室</td>
				<td nowrap='true'>出勤科室</td>
				<td nowrap='true'>考勤类别</td>
				<td nowrap='true'>申请时间</td>
				<td nowrap='true'>开始时间</td>
				<td nowrap='true'>结束时间</td>
				<td nowrap='true'>制单人</td>
				<td nowrap='true'>审核状态</td>
				<td nowrap='true'>审核人</td>
      	</tr>
    </thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=1">
								<td > <xsl:value-of select="."/> 
								</td>
							</xsl:when>
							<xsl:when test="position()=3 or position()=4">
								<td ><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>