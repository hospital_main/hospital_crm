<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
	      <tr noWrap='true' class='mainHead'>
	    		<td style='colspan:3;fontsize:maintitle'></td>
    			<td style="display:none"></td>
    		  <td style="display:none"></td>
  	  	</tr>
	  		<tr noWrap="true" class="mainHead">
					<td>休假项目</td>
					<td>所属科室</td>
					<td>职工信息</td>
				</tr>
  		</thead>
	  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr">
	  			<tr>
					<xsl:for-each select="td">
	              		<td nowrap='true'><xsl:value-of select="."/></td>
					</xsl:for-each>
					</tr>
				</xsl:for-each>
	  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>