<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>编号</td>
        <td nowrap='true'>名称</td>
		  	<td nowrap='true'>招聘渠道</td>
		  	<td nowrap='true'>预算费用</td>
		  	<td nowrap='true'>招聘人数</td>
		  	<td nowrap='true'>登记日期</td>
		  	<td nowrap='true'>开始日期</td>
		  	<td nowrap='true'>结束日期</td>
		  	<td nowrap='true'>招聘说明</td>
		  	<td nowrap='true'>招聘备注</td>
		  	<td nowrap='true'>审批人</td>
		  	<td nowrap='true'>状态</td>				
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=4 or position()=5">
										<td align='right'><xsl:value-of select="."/></td>
              </xsl:when>
							<xsl:otherwise>
		                 <td align='left'><xsl:value-of select="."/></td>
				  		</xsl:otherwise>
            </xsl:choose>
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
