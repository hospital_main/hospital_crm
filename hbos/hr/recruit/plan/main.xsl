<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th nowrap='true'>编号</th>
        <th nowrap='true'>名称</th>
		  	<th nowrap='true'>招聘渠道</th>
		  	<th nowrap='true'>预算费用</th>
		  	<th nowrap='true'>招聘人数</th>
		  	<th nowrap='true'>登记日期</th>
		  	<th nowrap='true'>开始日期</th>
		  	<th nowrap='true'>结束日期</th>
		  	<th nowrap='true'>招聘说明</th>
		  	<th nowrap='true'>招聘备注</th>
		  	<th nowrap='true'>审批人</th>
		  	<th nowrap='true'>状态</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        		<td align='center'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	              </xsl:attribute>
	            </input>
         		</td>
          
          <xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
									<td align='left'>
										<a tabindex='-1'>
						                  <xsl:attribute name="href" >
						    	            javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:1000px;dialogHeight:570px', result)
						  	          </xsl:attribute><xsl:value-of select="."/></a>
								 	</td>
              </xsl:when>
              <xsl:when test="position()=4 or position()=5">
									<td align='right'>
											<xsl:value-of select="."/>
								 	</td>
              </xsl:when>
              <xsl:otherwise>
								<td>
		                 <xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

