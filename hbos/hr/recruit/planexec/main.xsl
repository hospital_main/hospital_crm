<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>计划编码</th>
        <th nowrap='true'>计划名称</th>
		  	<th nowrap='true'>登记日期</th>
		  	<th nowrap='true'>需求部门</th>
		  	<th nowrap='true'>招聘职位</th>
		  	<th nowrap='true'>职位编制人数</th>
		  	<th nowrap='true'>拟招聘人数</th>
		  	<th nowrap='true'>已招聘人数</th>
		  	<th nowrap='true'>差额</th>
      </tr>
    </thead>
    	
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1 and ../td[1]!='合 计'">
									<td align='left'>
										<a tabindex='-1'>
						              <xsl:attribute name="href" >
						    	            javascript:openDialog('../plan/update.html?load=&lt;plan_no&gt;<xsl:value-of select="."/>&lt;/plan_no&gt;', 'dialogWidth:1000px;dialogHeight:570px', result)
						  	          </xsl:attribute><xsl:value-of select="."/></a>
								 	</td>
              </xsl:when>
              <xsl:when test="position()=6 or position()=7 or position()=9 or (position()=8 and (../td[1]='合 计' or  . = 0 and ../td[1]!='合 计'))">
								<td align='right'><xsl:value-of select="."/></td>
							</xsl:when>
              <xsl:when test="position()=8 and . != 0 and ../td[1]!='合 计'">
									<td align='right'>
										<a tabindex='-1' >
						              <xsl:attribute name="href" >
						    	            javascript:openDialog('selectPer.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:1000px;dialogHeight:570px', result)
						  	          </xsl:attribute><xsl:value-of select="."/></a>
								 	</td>
              </xsl:when>
							<xsl:when test="position()=10">
								 </xsl:when>
              <xsl:otherwise>
								<td>
		                 <xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

