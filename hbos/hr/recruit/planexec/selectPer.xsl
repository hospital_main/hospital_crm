<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>姓名</th>
        <th nowrap='true'>文化程度</th>
		  	<th nowrap='true'>应聘职务</th>
		  	<th nowrap='true'>渠道来源</th>
		  	<th nowrap='true'>身份证号</th>
		  	<th nowrap='true'>联系电话</th>
		  	<th nowrap='true'>性别</th>
		  	<th nowrap='true'>手机</th>
		  	<th nowrap='true'>电子邮件</th>
		  	<th nowrap='true'>民族</th>
        <th nowrap='true'>其他联系</th>
		  	<th nowrap='true'>家庭地址</th>
		  	<th nowrap='true'>联系地址</th>
		  	<th nowrap='true'>登记日期</th>
		  	<th nowrap='true'>当前状态</th>
		  	<th nowrap='true'>出生年月</th>
		  	<th nowrap='true'>婚姻状况</th>
		  	<th nowrap='true'>毕业院校</th>
		  	<th nowrap='true'>籍贯</th>
		  	<th nowrap='true'>专业</th>
		  	<th nowrap='true'>备注</th>
		  	<th nowrap='true'>简历</th>
      </tr>
    </thead>
    	
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
									<td align='left'>
										<a tabindex='-1'>
						              <xsl:attribute name="href" >
						    	            javascript:openDialog('../person/update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:1000px;dialogHeight:650px', result)
						  	          </xsl:attribute><xsl:value-of select="."/></a>
								 	</td>
              </xsl:when>
              <xsl:when test="position()=22 and . != ''">
									<td align='left'>
										<a tabindex='-1' target="_blank">
							                  <xsl:attribute name="href" >../../../..<xsl:value-of select="."/></xsl:attribute>下载</a>	

								 	</td>
							</xsl:when>
              <xsl:otherwise>
								<td>
		                 <xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

