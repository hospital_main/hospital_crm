<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>计划编码</td>
        <td nowrap='true'>计划名称</td>
		  	<td nowrap='true'>登记日期</td>
		  	<td nowrap='true'>需求部门</td>
		  	<td nowrap='true'>招聘职位</td>
		  	<td nowrap='true'>职位编制人数</td>
		  	<td nowrap='true'>拟招聘人数</td>
		  	<td nowrap='true'>已招聘人数</td>
		  	<td nowrap='true'>差额</td>
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1 and .='合 计'">
								<td colspan = '5' align='left'><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=6 or position()=7 or position()=8 or position()=9">
								<td align='right'><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=10">
							</xsl:when>
							<xsl:otherwise>
								<td align='left'><xsl:value-of select="."/></td>
				  		</xsl:otherwise>
				   </xsl:choose>
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
