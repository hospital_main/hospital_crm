<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		
      	</tr>

			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>需求编号</td>
        <td nowrap='true'>需求单名称</td>
		  	<td nowrap='true'>需求部门</td>
		  	<td nowrap='true'>申请人</td>
		  	<td nowrap='true'>制单日期</td>
		  	<td nowrap='true'>失效日期</td>
		  	<td nowrap='true'>岗位要求</td>
		  	<td nowrap='true'>备注</td>
		  	<td nowrap='true'>需求状态</td>
		  	<td nowrap='true'>审批人</td>
				
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
						
								<td align='left'><xsl:value-of select="."/></td>
							
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
