<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:8;fontsize:maintitle'>部门编制</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	
  	  </tr>   	
				<tr noWrap='true' class='mainHead'>
				  	<td nowrap='true'>部门编码</td>
				  	<td nowrap='true'>部门名称</td>
				  	<td nowrap='true'>部门类别</td>
				  	<td nowrap='true'>职位编码</td>
				  	<td nowrap='true'>职位名称</td>
				  	<td nowrap='true'>编制人数</td>
				  	<td nowrap='true'>实编人数</td>
				  	<td nowrap='true'>缺编人数</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td[position()=1 or position()=2 or position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8]">							
							<xsl:choose>
								<xsl:when test="position()=1 and ../td[1]='合 计'">
								<td colspan = '5' align = 'left'><xsl:value-of select="../td[1]"/></td>
							</xsl:when>	
								<xsl:when test="position()=6 or position()=7 or position()=8">
								<td align="right"><xsl:value-of select="."/></td>
							</xsl:when>	
							<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
							</xsl:choose>							
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
