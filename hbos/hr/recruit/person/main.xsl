<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th nowrap='true'>单号</th>
        <th nowrap='true'>姓名</th>
        <th nowrap='true'>学历</th>
        <th nowrap='true'>部门</th>
		  	<th nowrap='true'>应聘职位</th>
		  	<th nowrap='true'>渠道来源</th>
		  	<th nowrap='true'>身份证号</th>
		  	<th nowrap='true'>联系电话</th>
		  	<th nowrap='true'>性别</th>
		  	<th nowrap='true'>手机</th>
		  	<th nowrap='true'>电子邮件</th>
		  	<th nowrap='true'>民族</th>
		  	<th nowrap='true'>其他联系</th>
		  	<th nowrap='true'>家庭地址</th>
		  	<th nowrap='true'>联系地址</th>
		  	<th nowrap='true'>登记日期</th>
		  	<th nowrap='true'>当前状态</th>
		  	<th nowrap='true'>出生年月</th>
		  	<th nowrap='true'>婚姻状况</th>
		  	<th nowrap='true'>毕业院校</th>
		  	<th nowrap='true'>籍贯</th>
		  	<th nowrap='true'>专业</th>
		  	<th nowrap='true'>备注</th>
		  	<th nowrap='true'>招聘计划</th>
		  	<th nowrap='true'>照片</th>
		  	<th nowrap='true'>简历</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        		<td align='center'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	              </xsl:attribute>
	            </input>
         		</td>
          
          <xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
									<td align='left'>
										<a tabindex='-1'>
						                  <xsl:attribute name="href" >
						    	            javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:1000px;dialogHeight:670px', result)
						  	          </xsl:attribute><xsl:value-of select="."/></a>
								 	</td>
              </xsl:when>
							<xsl:when test="position()=last()-1">
								<xsl:if test=". != ''">
										<td align='left'>
										  <a>
												<xsl:attribute name="href">#</xsl:attribute>
												<xsl:attribute name="onclick">IsExistsFile("<xsl:value-of select="."/>")</xsl:attribute>
												查看	</a>
								 	</td>
								</xsl:if>
								<xsl:if test=". = ''">
										<td align='left'>
						
								 	</td>
								</xsl:if>
									
              </xsl:when>
							<xsl:when test="position()=last()">
									<td align='left'>
										<table>
												<tr>
													<xsl:if test="1=1">
															<td>
																<a tabindex='-1'>
							                  <xsl:attribute name="href" >
							    	            	javascript:openDialog('docUpload.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:600px;dialogHeight:250px', result)
							  	          		</xsl:attribute>上传</a>	
															</td>
													</xsl:if>
													
													<td> </td>
													<xsl:if test=". != ''">
														<td>						
															
															<a href='#'>
							    							<xsl:attribute name="onclick" >downLoad('<xsl:value-of select="."/>')</xsl:attribute>							    		
							    							下载
							    						</a>
							    						
																<!--<a tabindex='-1' target="_blank">
							                  <xsl:attribute name="href" >../../../..<xsl:value-of select="."/></xsl:attribute>下载</a>	-->
														</td>	
													</xsl:if>
													
												</tr>
										</table>
										        
						  	    
								 	</td>
              </xsl:when>
           
              <xsl:otherwise>
								<td>
		                 <xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

