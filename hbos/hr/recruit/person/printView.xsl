<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>单号</td>
				<td nowrap='true'>姓名</td>
        <td nowrap='true'>学历</td>
		  	<td nowrap='true'>部门</td>
		  	<td nowrap='true'>应聘职位</td>
		  	<td nowrap='true'>渠道来源</td>
		  	<td nowrap='true'>身份证号</td>
		  	<td nowrap='true'>联系电话</td>
		  	<td nowrap='true'>性别</td>
		  	<td nowrap='true'>手机</td>
		  	<td nowrap='true'>电子邮件</td>
		  	<td nowrap='true'>民族</td>
		  	<td nowrap='true'>其他联系</td>
		  	<td nowrap='true'>家庭地址</td>			
		  	<td nowrap='true'>联系地址</td>			
		  	<td nowrap='true'>登记日期</td>			
		  	<td nowrap='true'>当前状态</td>			
		  	<td nowrap='true'>出生年月</td>			
		  	<td nowrap='true'>婚姻状况</td>			
		  	<td nowrap='true'>毕业院校</td>			
		  	<td nowrap='true'>籍贯</td>			
		  	<td nowrap='true'>专业</td>			
		  	<td nowrap='true'>备注</td>			
		  	<td nowrap='true'>招聘计划</td>					
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td[position() &lt; last()-1]">
					
		                 <td align='left'><xsl:value-of select="."/></td>
				  		
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
