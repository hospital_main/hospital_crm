<?xml version='1.0' encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">

    <thead>

		<xsl:variable name='imgPath'>../../../../image</xsl:variable>

		<xsl:variable name='height'>15</xsl:variable>

		<xsl:variable name='tdWidth'>2</xsl:variable>

		<xsl:variable name='zbWidth'>100</xsl:variable>

		<xsl:variable name='zbBg1'>#6699CC</xsl:variable>

		<xsl:variable name='zbBg2'>#EDEDED</xsl:variable>

		<xsl:variable name="level" select="/root/tbody/tr[1]/td[3]"/>

		<xsl:variable name="step" select="count(/root/tbody/tr[td[3] = $level])"/>

    </thead>

    <tbody>

  	<xsl:if test="$step &gt; 0">

	    	<tr><td><table align="center">



      <xsl:for-each select="/root/tbody/tr">

      

      <!--a ine start-->

       <xsl:if test="(position() mod $step) = 1">

       		<xsl:text disable-output-escaping="yes"><![CDATA[<tr>]]></xsl:text>

      	 </xsl:if>

      	 

		<!--add one space-->

		<td height="{$height * 4}" width="{$tdWidth}"></td>

	  <xsl:choose>

	  		<!--display a fact target-->

			<xsl:when test="td[1] = 0">

				<td><table>

				<tr><td align="center" height="{$height}" width="{$zbWidth}">

				<xsl:choose>

					<xsl:when test="td[4] = 'L'"><img src="{$imgPath}/la.png" /></xsl:when>

					<xsl:when test="td[4] = 'R'"><img src="{$imgPath}/ra.png" /></xsl:when>

					<xsl:when test="td[4] = 'H'"><img src="{$imgPath}/ha.png" /></xsl:when>

					<xsl:when test="td[4] = 'V'"><img src="{$imgPath}/va.png" /></xsl:when>

				</xsl:choose>
				</td></tr>

				

				<tr><td bgcolor="{$zbBg1}" align="center" height="{$height}" width="{$zbWidth}" nowrap="nowrap"><xsl:value-of select="td[2]"/></td></tr>

				<tr><td bgcolor="{$zbBg2}" align="center" height="{$height}" width="{$zbWidth}" nowrap="nowrap"></td></tr>

				<tr><td align="center" height="{$height}" width="{$zbWidth}"><xsl:if test="td[5] != 1"><img src="{$imgPath}/v.png" /></xsl:if></td></tr>

				</table></td>

			</xsl:when>

			<xsl:otherwise>

				<xsl:choose>

			  		<!--display a space target with line-->

					<xsl:when test="td[4] ='H'">

						<td><table>

						<tr><td align="center" height="{$height}" width="{$zbWidth}"><img src="{$imgPath}/h.png" /></td></tr>

						<tr><td height="{$height}" width="{$zbWidth}">��</td></tr>

						<tr><td height="{$height}" width="{$zbWidth}">��</td></tr>

						<tr><td height="{$height}" width="{$zbWidth}">��</td></tr>

						</table></td>

					</xsl:when>

			  		<!--display a space target with nothing-->

					<xsl:otherwise><td height="{$height * 4}" width="{$zbWidth}"></td></xsl:otherwise>

				</xsl:choose>

			</xsl:otherwise>

		</xsl:choose>

		

      <!--a line end-->

       <xsl:if test="(position() mod $step) = 0">

       		<td height="{$height * 4}" width="{$tdWidth}"></td>

       		<xsl:text disable-output-escaping="yes"><![CDATA[</tr>]]></xsl:text>

       </xsl:if>

       

     	</xsl:for-each>

     	</table></td></tr>

     	</xsl:if>

    </tbody>

  </xsl:template>

</xsl:stylesheet>



