<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none' width='25'><input type='checkbox'/></th>
				<th nowrap='true'>当前记录标示</th>
				<th nowrap='true'>接触对象</th>
				<th nowrap='true'>频率</th>
				<th nowrap='true'>方式</th> 
				<th nowrap='true'>备注</th> 
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:variable name="row" select="position()" />		
		<tr>
			<td align='center'  style='display:none'>
				<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
					<xsl:attribute name="value" >
						<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					</xsl:attribute>
				</input>
			</td>
			<xsl:for-each select="td">
				<xsl:choose>
					<xsl:when test="position()=1">
						<td>
							  <xsl:value-of select="$row"/>
						</td>
			    </xsl:when>
			    
			    <xsl:otherwise>
						<td>
							<xsl:value-of select="."/>
						</td>
					</xsl:otherwise>
			  </xsl:choose>
			</xsl:for-each>
				</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

