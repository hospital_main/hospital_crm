<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td)"/>
	<root>
		<thead>
			<tr noWrap="true" >
				<td noWrap="true" align='left' >
					<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
				</td>
				<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
					<td noWrap="true" style="display:none"/>
				</xsl:for-each>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>职位编号</td>
				<td nowrap='true'>职位名称</td> 
				<td nowrap='true'>所属部门</td> 
				<td nowrap='true'>直接上级</td> 
				<td nowrap='true'>成立日期</td>
				
				<td nowrap='true'>职位序列</td>
				<td nowrap='true'>职位等级</td> 
				<td nowrap='true'>工作目的</td> 
				<td nowrap='true'>工作概要</td> 
				<td nowrap='true'>工作要求</td>
				
				<td nowrap='true'>工作环境</td> 
				<td nowrap='true'>工作地点</td> 
				<td nowrap='true'>是否任期</td> 
				<td nowrap='true'>最长任期</td>
			</tr>     
		</thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>					
	      	<xsl:for-each select="td">
						<td>
		          <xsl:value-of select="."/>
				  	</td>
		      </xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>

