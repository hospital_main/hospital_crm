<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none' width='25'><input type='checkbox'/></th>
				<th nowrap='true'>职位编号</th>
				<th nowrap='true'>职位名称</th> 
				<th nowrap='true'>所属部门</th> 
				<th nowrap='true'>直接上级</th> 
				<th nowrap='true'>成立日期</th>
				
				<th nowrap='true'>职位序列</th>
				<th nowrap='true'>职位等级</th> 
				<th nowrap='true'>工作目的</th> 
				<th nowrap='true'>工作概要</th> 
				<th nowrap='true'>工作要求</th>
				
				<th nowrap='true'>工作环境</th> 
				<th nowrap='true'>工作地点</th> 
				<th nowrap='true'>是否任期</th> 
				<th nowrap='true'>最长任期</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
		<tr>
			<td align='center'  style='display:none'>
				<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
					<xsl:attribute name="value" >
						<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					</xsl:attribute>
				</input>
			</td>
			<xsl:for-each select="td">
				<xsl:choose>
					<xsl:when test="position()=1">
						<td>
							<xsl:value-of select="."/>
						</td>
			    </xsl:when>
			    
			    <xsl:otherwise>
						<td>
							<xsl:value-of select="."/>
						</td>
					</xsl:otherwise>
			  </xsl:choose>
			</xsl:for-each>
				</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

