/**
 * 人力资源管理系统 - 条件查询表单验证
 * 
 */
//设置全局变量
var SYS_FIELD_ID_EMP_AGE = 'emp_age';//系统内置"年龄"字段

function checkFormByFieldType() {
	var FORM_FIELD_ID = cond_item_list.value;
	var FORM_FIELD_VALUE = cond_value.value;
	
	if (FORM_FIELD_ID == '') {
		alert("请选择查询项！")
		return false;
	}
	if (FORM_FIELD_VALUE == '') {
		alert("请输入匹配值！")
		return false;
	}
	window.xmlhttp.post("hr_getFieldTypeByFieldId", "<fieldId>" + FORM_FIELD_ID + "</fieldId>", "?isCheck=false");
	var srcTree = new window.ActiveXObject("Microsoft.XMLDOM");
	srcTree.async=false;
	srcTree.load(xmlhttp._object.responseXML);
	var field_type = srcTree.getElementsByTagName("td").item(0).firstChild.nodeValue;

	if (field_type != null) {
		//AJAX查询取得字段类型
		if (field_type == '日期型') {
			var reg = /^(\d{4})-(\d{2})-(\d{2})$/;
			if (reg.test(FORM_FIELD_VALUE)
					&& parseDate(FORM_FIELD_VALUE) != null) 
				return true;
			alert("请输入正确的日期格式yyyy-mm-dd！");
			return false;
		}
		if (field_type == '数值型') {
			var myReg = /^(0|\d+(\.\d+)?)$/
			if (myReg.test(FORM_FIELD_VALUE)) {
				// 针对内置字段名进行校验
				if (FORM_FIELD_ID == SYS_FIELD_ID_EMP_AGE) {
					var myIntReg = /^\d+$/
					if (myIntReg.test(FORM_FIELD_VALUE))
						return true;
					alert("匹配值只能填写正整数！");
					return false;
				} 
				return true;
			} else {
				alert("匹配值只能输入数值型！");
				return false;
			}
		}
		return true;
	}
	
}
//左侧查询部分显示或隐藏
function leftShow() {
	leftTree.style.display =leftTree.style.display == "none" ? "" : "none";
	leftBtn.style.display = leftBtn.style.display == "none" ? "" : "none";
	btn_show.src = leftTree.style.display == "none" ? "/images/hr_show.gif" : "/images/hr_hidden.gif";
	treeTable.style.display = leftTree.style.display == "none" ? "none" : (unitTree.style.display=='none' ? "none" : "");
	//判断result对象是否存在，是否有adjPosition()方法
	if("undefined" != typeof result && typeof(eval(result.adjPosition))=="function") 
		result.adjPosition();
	//判断result2对象是否存在
	if("undefined" != typeof result2) 
		result2.adjPosition();
}