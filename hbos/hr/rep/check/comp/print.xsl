<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<root>
  	<thead>
  		<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">6</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
		    <td>行</td>
		  	<td>列</td>
		  	<td>项目编码</td>
		  	<td>项目公式</td>
		  	<td>审核结果</td>
  	  	<td>错误提示</td>
  		</tr>
  		
  	</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:if test="td[position()=3]!='正确'">
            <xsl:attribute name="style"> color :red</xsl:attribute>            
          </xsl:if>
          <xsl:for-each select="td">
            <td><xsl:value-of select="."/></td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>

