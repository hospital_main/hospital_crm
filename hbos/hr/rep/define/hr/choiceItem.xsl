<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
	   <tr noWrap='true' class='mainHead'>
			<!-- <th style='display:none'><input type='checkbox'/></th>-->
			<th noWrap='true'></th>		
			<th noWrap='true'>�ֶ�����</th>
	    </tr>
    </thead>
    
    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
		    <tr>
			    <td align='center'>
			    	<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
				      <xsl:attribute name="value">
				          <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					  </xsl:attribute>
					</input>
			    </td>
			    <xsl:for-each select="td[position()=1]">
					<td noWrap='true' style="display:none;">
						<xsl:value-of select="."/>
					</td>
			    </xsl:for-each>
			    <xsl:for-each select="td[position()!=1]">
					<td noWrap='true'>
						<xsl:value-of select="."/>
					</td>
			    </xsl:for-each>
		    </tr>
	    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

