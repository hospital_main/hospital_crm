<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
      	<th nowrap='true'>报表编码</th>
      	<th nowrap='true'>报表名称</th>
      	<th nowrap='true'>报表类型</th>
      	<th nowrap='true'>报表说明</th>
      	<th style='display:none' nowrap='true'>汇总方式</th>
      	<th nowrap='true'>备份状态</th>
      	<th nowrap='true'>定义</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td[position() != 1 and position() != 6]">
            <td>
              <xsl:choose>
                <xsl:when test="position()=1">
                  <a tabindex='-1'>
                    <xsl:attribute name="href" >
      	            javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:400px;dialogHeight:280px', result)
    	              </xsl:attribute>
  	            <xsl:value-of select="."/></a>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
          <td><a>
            <xsl:attribute name="href" >
    	      javascript:openDialog('set.html?load=<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:'+screen.availWidth+';dialogHeight:'+screen.availHeight)
  	    </xsl:attribute>设置</a></td>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

