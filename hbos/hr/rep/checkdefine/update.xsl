<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th width='25'><input type='checkbox'/></th>
      	<th nowrap='true'>������</th>
      	<th nowrap='true'>������ʾ</th>
      	<th nowrap='true'>��⹫ʽ</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                  <a href='#'>
                    <xsl:attribute name="onclick">openDialog('dupdate.html','dialogWidth:620px;dialogHeight:450px', this);</xsl:attribute>
                    <xsl:value-of select="."/>
                  </a>
                </td>
              </xsl:when>
              <xsl:when test="position()=2 or position()=3">
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:when test="position()=4">
                <td style='display:none'>
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


