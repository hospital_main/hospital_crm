<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<root>
  	<thead>
  			<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">3</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">3</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
  		<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>汇总表编码</td>
      	<td nowrap='true'>汇总表名称</td>
      	<td nowrap='true'>汇总说明</td>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>    	
          <xsl:for-each select="td">
		 				<xsl:choose>
	              <xsl:when test="position()>1">
	                <td><xsl:value-of select="."/></td>
								</xsl:when>
	          </xsl:choose>    
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>

