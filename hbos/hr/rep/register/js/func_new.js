//Copyright (c) 2000 Microsoft Corporation.  All rights reserved.
//<script>
	//设置REGISTERFIELD
var pageNo=window.dialogArguments.CellWeb1.GetCurSheet();
var maxrows = window.dialogArguments.CellWeb1.GetRows(pageNo);  //当前页总行数
var maxcols = window.dialogArguments.CellWeb1.GetCols(pageNo);  //当前页总列数

function setRegisterFieldSubCond()
{
	var table_id=statistic_item_value.value.split(".")[0];
	for(var row1=0;row1<maxrows;row1++)
	{
		for(var col1=0;col1<maxcols;col1++)
		{
			var str1 = window.dialogArguments.CellWeb1.GetFormula(col1,row1,pageNo);  //取单元格(col1,row1)公式
			var str1_desc = window.dialogArguments.CellWeb1.GetCellTip(col1,row1,pageNo);
	      	if (str1!="")
	        {
	        	var strs="";
	        	var funcname = str1.substring(0, str1.indexOf("("));
	        	
	        	if(funcname=="SUBCOND") 
	        	{
	           		//解析str1得到table_id1=str1的table_id、cond1=str1的cond、desc1=str1的cond_desc;
		           	strs=str1.split(",");
		           	var temp = strs[0].substr(strs[0].indexOf("("),strs[0].length-strs[0].indexOf("("));
			    	var tf1 = temp.substr(2,temp.length-3);
			    	var tid=tf1.split(".")[0];
					if(tid==table_id)
	           		{
	           			subcond=strs[1].substr(1,strs[1].length-3);
	           			strs=str1_desc.split(",");
	                	subdesc=strs[1].substr(0,strs[1].length-1);
	                	setSub=false;
	             	}  
	           	}
	         }
	         if (subcond!="") break;
	     }
   	 }  
}

function setRegisterFieldItemCond()
{
	var table_id=statistic_item_value.value.split(".")[0];
	//读取ITEMCOND 按行读取
	for(var row1=0;row1<maxrows;row1++)
	{
    	var str1 = window.dialogArguments.CellWeb1.GetFormula(col_next,row1,pageNo);  //取单元格(col1,row1)公式
		var str1_desc = window.dialogArguments.CellWeb1.GetCellTip(col_next,row1,pageNo);
		//解析str1得到table_id2=str1的table_id、cond2=str1的cond
		if(str1!="")
		{
			var strs="";
        	var funcname = str1.substring(0, str1.indexOf("("));
        	if(funcname=="ITEMCOND") 
        	{
           		//解析str1得到table_id1=str1的table_id、cond1=str1的cond、desc1=str1的cond_desc;
	           	strs=str1.split(",");
	           	var temp = strs[0].substr(strs[0].indexOf("("),strs[0].length-strs[0].indexOf("("));
		    	var tf1 = temp.substr(2,temp.length-3);
		    	var tid=tf1.split(".")[0];
				if(tid==table_id)
           		{
           			itemcond=strs[1].substr(1,strs[1].length-3);
           			strs=str1_desc.split(",");
                	itemdesc=strs[1].substr(0,strs[1].length-1);
             	}
             }
         }  
	}
	for(var col1=0;col1<maxcols;col1++)
	{
		var str1 = window.dialogArguments.CellWeb1.GetFormula(col1,row_next,pageNo);  //取单元格(col1,row1)公式
		var str1_desc = window.dialogArguments.CellWeb1.GetCellTip(col1,row_next,pageNo); 
		//解析str1得到table_id2=str1的table_id、cond2=str1的cond
		if(str1!="")
		{
			var strs="";
        	var funcname = str1.substring(0, str1.indexOf("("));
        	if(funcname=="ITEMCOND") 
        	{
           		//解析str1得到table_id1=str1的table_id、cond1=str1的cond、desc1=str1的cond_desc;
	           	strs=str1.split(",");
	           	var temp = strs[0].substr(strs[0].indexOf("("),strs[0].length-strs[0].indexOf("("));
		    	var tf1 = temp.substr(2,temp.length-3);
		    	var tid=tf1.split(".")[0];
				if(tid==table_id)
           		{
           			itemcond=strs[1].substr(1,strs[1].length-3);
           			strs=str1_desc.split(",");
                	itemdesc=strs[1].substr(0,strs[1].length-1);
             	}
             }
         }
	}
}

function setSubCond()
{
	//当前数据表id
   	var table_id=statistic_item_value.value.split(".")[0];
   	var table_name=statistic_item.value.split(".")[0];

	window.dialogArguments.CellWeb1.SetFormula(col,row,pageNo,attr_id.value);
	window.dialogArguments.CellWeb1.SetCellTip(col,row,pageNo,"SUBCOND("+table_name+","+cond_desc.value+")");


	for(var row1=0;row1<maxrows;row1++)
	{
		for(var col1=0;col1<maxcols;col1++)
		{    
			var str1 = window.dialogArguments.CellWeb1.GetFormula(col1,row1,pageNo);  //取单元格(col1,row1)公式
			var str1_desc = window.dialogArguments.CellWeb1.GetCellTip(col1,row1,pageNo);
			if (str1!="")
	        {
	        	var strs="";
	        	var funcname = str1.substring(0, str1.indexOf("("));
	            if(funcname!="SUBCOND" && funcname!="REGISTERFIELD") continue;
	            if(funcname=="SUBCOND") 
	            {
	            	if(col1==col && row1==row) continue;
	               	strs=str1.split(",");
		           	var temp = strs[0].substr(strs[0].indexOf("("),strs[0].length-strs[0].indexOf("("));
			    	var tf1 = temp.substr(2,temp.length-3);
			    	var tid=tf1.split(".")[0];
			    	if(table_id==tid)
			    	{
			    		//清除coly,rowx单元格的已有函数和函数提示  一个表只允许有一个表条件
			    		window.dialogArguments.CellWeb1.SetFormula(col1,row1,pageNo,"");
			    		window.dialogArguments.CellWeb1.SetCellTip(col1,row1,pageNo,"");
			    		window.dialogArguments.CellWeb1.S(col1,row1,pageNo,"");
			    	}
	            }
	          //处理REGISTERFIELD函数
	            if(funcname=="REGISTERFIELD") 
	            {//解析出REGISTERFIELD函数内容的五部分：
				    strs=str1.split(",");
				    var temp = strs[0].substr(strs[0].indexOf("("),strs[0].length-strs[0].indexOf("("));
				    var tf1 = temp.substr(2,temp.length-3);
				    var tid=tf1.split(".")[0];
				    var scond=strs[1].substr(1,strs[1].length-2);
				    var icond=strs[2].substr(1,strs[2].length-2);
				    var sort=strs[3].substr(1,strs[3].length-2);
				    var dateType=strs[4].substr(1,strs[4].length-3);
				    
				    if(tid==table_id)  //是本表的函数做函数的重新组合
	                { //设置函数
				    	attr_id.value='REGISTERFIELD("'+tf1+'","'+cond.value+'","'+icond+'","'+sort+'","'+dateType+'")';
				    	//解析出REGISTERFIELD函数提示的五部分
				    	var tempFuncDesc="REGISTERFIELD(";
					    strs=str1_desc.split(",");
					    var tf_desc = strs[0].substr(strs[0].indexOf("(")+1,strs[0].length-strs[0].indexOf("("));
					    tempFuncDesc+=tf_desc;
					    //增加第二个参数解释
					    if(scond!="")
					    {
					    	tempFuncDesc+=","+cond_desc.value;
					    }
					    else
					    {
					    	tempFuncDesc+=","+cond_desc.value;
					    	tempFuncDesc+=","+strs[1];
					    }
					    if(strs[2]!=undefined)
					    {
					    	tempFuncDesc+=","+strs[2];
					    }
					    if(strs[3]!=undefined)
					    {
					    	tempFuncDesc+=","+strs[3];
					    }
					    if(strs[4]!=undefined)
					    {
					    	tempFuncDesc+=","+strs[4];
					    }
			    		window.dialogArguments.CellWeb1.SetFormula(col1,row1,pageNo,attr_id.value);
			    		window.dialogArguments.CellWeb1.SetCellTip(col1,row1,pageNo,tempFuncDesc);
	                }
	            }
	        } 
		}
	}
}

function setITEMCOND()
{
	var startCol = window.dialogArguments.CellWeb1.GetSelectRangeJ(0);
	var startRow = window.dialogArguments.CellWeb1.GetSelectRangeJ(1);
	var endCol = window.dialogArguments.CellWeb1.GetSelectRangeJ(2);
	var endRow = window.dialogArguments.CellWeb1.GetSelectRangeJ(3);

	//当前数据表id
   	var table_id=statistic_item_value.value.split(".")[0];
   	var table_name=statistic_item.value.split(".")[0];
   	
	window.dialogArguments.CellWeb1.SetFormula(startCol,startRow,pageNo,attr_id.value);
	window.dialogArguments.CellWeb1.SetCellTip(startCol,startRow,pageNo,"ITEMCOND("+table_name+","+cond_desc.value+")");
	
	for(var row1=startRow;row1<=endRow;row1++)
	{
		for(var col1=startCol;col1<=endCol;col1++)
		{
			var str1 = window.dialogArguments.CellWeb1.GetFormula(col1,row1,pageNo);  //取单元格(col1,row1)公式
			var str1_desc = window.dialogArguments.CellWeb1.GetCellTip(col1,row1,pageNo);
			if (str1!="")
	        {
	        	var strs="";
	        	var funcname = str1.substring(0, str1.indexOf("("));
	          //处理REGISTERFIELD函数
	            if(funcname=="REGISTERFIELD") 
	            {//解析出REGISTERFIELD函数内容的五部分：
				    strs=str1.split(",");
				    var temp = strs[0].substr(strs[0].indexOf("("),strs[0].length-strs[0].indexOf("("));
				    var tf1 = temp.substr(2,temp.length-3);
				    var tid=tf1.split(".")[0];
				    var scond=strs[1].substr(1,strs[1].length-2);
				    var icond=strs[2].substr(1,strs[2].length-2);
				    var sort=strs[3].substr(1,strs[3].length-2);
				    var dateType=strs[4].substr(1,strs[4].length-3);
				    
				    if(tid==table_id)  //是本表的函数做函数的重新组合
	                { //设置函数
				    	attr_id.value='REGISTERFIELD("'+tf1+'","'+scond+'","'+cond.value+'","'+sort+'","'+dateType+'")';
				    	//解析出REGISTERFIELD函数提示的五部分
				    	var tempFuncDesc="REGISTERFIELD(";
					    strs=str1_desc.split(",");
					    var tf_desc = strs[0].substr(strs[0].indexOf("(")+1,strs[0].length-strs[0].indexOf("("));
					    tempFuncDesc+=tf_desc;
					    //增加第二个参数解释
					    if(scond!="")
					    {
					    	tempFuncDesc+=","+strs[1];
						    //第三个参数
						    if(icond!="")
						    {
						    	tempFuncDesc+=","+cond_desc.value;
						    }
						    else
						    {
						    	tempFuncDesc+=","+cond_desc.value;
						    	tempFuncDesc+=","+strs[2];
						    }
					    }
					    else
					    {
						    //第三个参数
						    if(icond!="")
						    {
						    	tempFuncDesc+=","+cond_desc.value;
						    	tempFuncDesc+=","+strs[2];
						    }
						    else
						    {
						    	tempFuncDesc+=","+cond_desc.value;
						    	tempFuncDesc+=","+strs[1];
							    if(strs[2]!=undefined)
							    {
							    	tempFuncDesc+=","+strs[2];
							    }
						    }
					    }
					    if(strs[3]!=undefined)
					    {
					    	tempFuncDesc+=","+strs[3];
					    }
					    if(strs[4]!=undefined)
					    {
					    	tempFuncDesc+=","+strs[4];
					    }
			    		window.dialogArguments.CellWeb1.SetFormula(col1,row1,pageNo,attr_id.value);
			    		window.dialogArguments.CellWeb1.SetCellTip(col1,row1,pageNo,tempFuncDesc);
	                }
	            }
	        }
		}
	}
}

