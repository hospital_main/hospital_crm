<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<td >
            <input type='checkbox' TABINDEX='-1'  onclick="selectAll(this)"></input>
         </td>
				<th nowrap='true'>日期</th>
				<th nowrap='true'>所属科室</th> 
				<th nowrap='true'>职工信息</th>
				<th nowrap='true'>休假项目</th>
				<th nowrap='true'>休假额度</th>
				<th nowrap='true'>休假说明</th>
				<th nowrap='true'>数据状态</th>
			</tr> 
		</thead> 
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'>
              <xsl:if test="td[8] = 3" >
                <input type='checkbox' TABINDEX='-1'  onclick="">
		              <xsl:attribute name="value" >
		                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		              </xsl:attribute>
              	</input>
              </xsl:if>
            	            
          </td>
					<xsl:for-each select="td[position() &lt; 8]"> 
						<td>
							<xsl:choose>
								<xsl:when test="position() = 1 and ../td[8] = 3">
									<a>
										<xsl:attribute name="href">
											javascript:openDialog("insert.html?load='<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>'", "dialogWidth:450px;dialogHeight:270px;");hr_vacal_add_select.click();
										</xsl:attribute>										
										<xsl:value-of select="."/>
									</a>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/> 
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



