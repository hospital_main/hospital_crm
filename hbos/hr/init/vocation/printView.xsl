<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:4;fontsize:maintitle'>年初余额</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				
				<tr noWrap='true' class='mainHead'>
				  	<td nowrap='true'>所属科室</td>
				  	<td nowrap='true'>职工信息</td>
				  	<td nowrap='true'>休假项目</td>
				  	<td nowrap='true'>年初余额</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td[position()=2 or position()=4 or position()=6 or position()=7]">							
							<xsl:choose>
								<xsl:when test="position()=1 and . = '' and ../td[6]='合 计'">
								<td colspan = '3' align = 'center'><xsl:value-of select="../td[6]"/></td>
							</xsl:when>	
							<xsl:when test="position()=1 or position()=2 or position()=3">
								<td><xsl:value-of select="."/></td>
							</xsl:when>							
							<xsl:otherwise>
								<td align="right">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
							</xsl:choose>							
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
