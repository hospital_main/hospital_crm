<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="TotalRow" select="count(/root/tbody/tr)"/>
		<xsl:variable name="TotalCol" select="6"/>
		<xsl:variable name="LoopStep" select="$TotalRow div $TotalCol"/>
		<tr class="mainHead">
			<th width='14%'>星期一</th>
			<th width='14%'>星期二</th>
			<th width='14%'>星期三</th>
			<th width='14%'>星期四</th>
			<th width='14%'>星期五</th>
			<th background="#99CDFF"  bgcolor="#99CDFF" width='15%'>星期六</th>
			<th background="#99CDFF"  bgcolor="#99CDFF" width='15%'>星期日</th>
		</tr>
	</thead>
	<tbody>
			<xsl:for-each select="/root/tbody/tr[position() mod 7 = 1]">
				<xsl:variable name="rowindex" select="position()-1 "/>
				<!--<xsl:variable name="Step" select="position() mod $LoopStep"/>-->
				<tr>
				<td style="font-family:impact;font-size:16;" >
				  <xsl:attribute name="align">center</xsl:attribute>
					<!--<input type='checkbox' name='is_checked' onclick='IsChecked_OnClick();'>
					  <xsl:attribute name="main_sequence_no"><xsl:value-of select="td[1]"/></xsl:attribute>
					  <xsl:attribute name="perf_kind_code"><xsl:value-of select="td[4]"/></xsl:attribute>
					  <xsl:attribute name="perf_kind_detail_code"><xsl:value-of select="td[5]"/></xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="td[9]"/></xsl:attribute>
						<xsl:if test=" $is_checked = 1">
							<xsl:attribute name="checked">true</xsl:attribute>
						</xsl:if>
					</input>-->
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 1 ]/td[3]"/>
        </td>
        <td style="font-family:impact;font-size:16;" >
        	<xsl:attribute name="align">center</xsl:attribute>
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 2 ]/td[3]"/>
        </td>
        <td style="font-family:impact;font-size:16;" >
        	<xsl:attribute name="align">center</xsl:attribute>
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7  + 3 ]/td[3]"/>
        </td>
        <td style="font-family:impact;font-size:16;" >
        	<xsl:attribute name="align">center</xsl:attribute>
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7  + 4 ]/td[3]"/>
        </td>
        <td style="font-family:impact;font-size:16;">
        	<xsl:attribute name="align">center</xsl:attribute >
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 5 ]/td[3]"/>
        </td>
        <td style="font-family:impact;font-size:16;">
        	<xsl:attribute name="align">center</xsl:attribute>
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 6 ]/td[3]"/>
        </td>
        <td style="font-family:impact;font-size:16;">
        	<xsl:attribute name="align">center</xsl:attribute>
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 7 ]/td[3]"/>
        </td>
				<!--<xsl:if test="$LoopStep = 1 or $Step = 0">
					<xsl:text disable-output-escaping="yes"><![CDATA[</tr>]]></xsl:text>
				</xsl:if>-->
				</tr>
				
				<tr>
				<td align="left">
				<xsl:if test="/root/tbody/tr[$rowindex * 7 + 1 ]/td[3] != ''">
				  <xsl:attribute name="align">left</xsl:attribute>
				  <xsl:attribute name="id">tdid_<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 1 ]/td[1]"/></xsl:attribute>
				  <xsl:attribute name="show_type"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 1 ]/td[2]"/></xsl:attribute>
				  <xsl:attribute name="c_date"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 1 ]/td[5]"/></xsl:attribute>
          <xsl:attribute name="pk" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 1 ]/td[6]"/></xsl:attribute>
          <xsl:attribute name="week_xh" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 1 ]/td[7]"/></xsl:attribute>
          <xsl:attribute name="week_day" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 1 ]/td[8]"/></xsl:attribute>
					<!--<input type='checkbox' name='is_checked' onclick='IsChecked_OnClick();'>
					  <xsl:attribute name="main_sequence_no"><xsl:value-of select="td[1]"/></xsl:attribute>
					  <xsl:attribute name="perf_kind_code"><xsl:value-of select="td[4]"/></xsl:attribute>
					  <xsl:attribute name="perf_kind_detail_code"><xsl:value-of select="td[5]"/></xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="td[9]"/></xsl:attribute>
						<xsl:if test=" $is_checked = 1">
							<xsl:attribute name="checked">true</xsl:attribute>
						</xsl:if>
					</input>-->
					<xsl:choose>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 1 ]/td[2] = 0 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 1 ]/td[2] = 1 ">
				  	<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 1 ]/td[2] = 2 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
						<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:otherwise>
				  </xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 1 ]/td[4]"/>
				</xsl:if>
        </td>
        <td align="left">
        <xsl:if test="/root/tbody/tr[$rowindex * 7 + 2 ]/td[3] != ''">
				  <xsl:attribute name="align">left</xsl:attribute>
				  <xsl:attribute name="id">tdid_<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 2 ]/td[1]"/></xsl:attribute>
				  <xsl:attribute name="show_type"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 2 ]/td[2]"/></xsl:attribute>
				  <xsl:attribute name="c_date"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 2 ]/td[5]"/></xsl:attribute>
          <xsl:attribute name="pk" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 2 ]/td[6]"/></xsl:attribute>
          <xsl:attribute name="week_xh" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 2 ]/td[7]"/></xsl:attribute>
          <xsl:attribute name="week_day" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 2 ]/td[8]"/></xsl:attribute>
					<xsl:choose>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 2 ]/td[2] = 0 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 2 ]/td[2] = 1 ">
				  	<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 2 ]/td[2] = 2 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
						<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:otherwise>
				  </xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 2 ]/td[4]"/>
        </xsl:if>
        </td>
				<td align="left">
				<xsl:if test="/root/tbody/tr[$rowindex * 7 + 3 ]/td[3] != ''">
				  <xsl:attribute name="align">left</xsl:attribute>
				  <xsl:attribute name="id">tdid_<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 3 ]/td[1]"/></xsl:attribute>
				  <xsl:attribute name="show_type"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 3 ]/td[2]"/></xsl:attribute>
				  <xsl:attribute name="c_date"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 3 ]/td[5]"/></xsl:attribute>
          <xsl:attribute name="pk" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 3 ]/td[6]"/></xsl:attribute>
          <xsl:attribute name="week_xh" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 3 ]/td[7]"/></xsl:attribute>
          <xsl:attribute name="week_day" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 3 ]/td[8]"/></xsl:attribute>
					<xsl:choose>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 3 ]/td[2] = 0 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this)</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 3 ]/td[2] = 1 ">
				  	<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 3 ]/td[2] = 2 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
						<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:otherwise>
				  </xsl:otherwise>
					</xsl:choose>						
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7  + 3 ]/td[4]"/>
        </xsl:if>
        </td>
        <td align="left">
				<xsl:if test="/root/tbody/tr[$rowindex * 7 + 4 ]/td[3] != ''">
				  <xsl:attribute name="align">left</xsl:attribute>
				  <xsl:attribute name="id">tdid_<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 4 ]/td[1]"/></xsl:attribute>
				  <xsl:attribute name="show_type"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 4 ]/td[2]"/></xsl:attribute>
				  <xsl:attribute name="c_date"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 4 ]/td[5]"/></xsl:attribute>
          <xsl:attribute name="pk" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 4 ]/td[6]"/></xsl:attribute>
          <xsl:attribute name="week_xh" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 4 ]/td[7]"/></xsl:attribute>
          <xsl:attribute name="week_day" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 4 ]/td[8]"/></xsl:attribute>
					<xsl:choose>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 4 ]/td[2] = 0 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 4 ]/td[2] = 1 ">
				  	<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 4 ]/td[2] = 2 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
						<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:otherwise>
				  </xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7  + 4 ]/td[4]"/>
				</xsl:if>
				</td>
				<td align="left">
				<xsl:if test="/root/tbody/tr[$rowindex * 7 + 5 ]/td[3] != ''">
				  <xsl:attribute name="align">left</xsl:attribute>
				  <xsl:attribute name="id">tdid_<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 5 ]/td[1]"/></xsl:attribute>
				  <xsl:attribute name="show_type"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 5 ]/td[2]"/></xsl:attribute>
				  <xsl:attribute name="c_date"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 5 ]/td[5]"/></xsl:attribute>
          <xsl:attribute name="pk" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 5 ]/td[6]"/></xsl:attribute>
          <xsl:attribute name="week_xh" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 5 ]/td[7]"/></xsl:attribute>
          <xsl:attribute name="week_day" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 5 ]/td[8]"/></xsl:attribute>
					<xsl:choose>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 5 ]/td[2] = 0 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 5 ]/td[2] = 1 ">
				  	<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 5 ]/td[2] = 2 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
						<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:otherwise>
				  </xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 5 ]/td[4]"/>
        </xsl:if>
        </td>
        <td align="left">
				<xsl:if test="/root/tbody/tr[$rowindex * 7 + 6 ]/td[3] != ''">
				  <xsl:attribute name="align">left</xsl:attribute>
				  <xsl:attribute name="id">tdid_<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 6 ]/td[1]"/></xsl:attribute>
				  <xsl:attribute name="show_type"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 6 ]/td[2]"/></xsl:attribute>
				  <xsl:attribute name="c_date"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 6 ]/td[5]"/></xsl:attribute>
          <xsl:attribute name="pk" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 6 ]/td[6]"/></xsl:attribute>
          <xsl:attribute name="week_xh" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 6 ]/td[7]"/></xsl:attribute>
          <xsl:attribute name="week_day" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 6 ]/td[8]"/></xsl:attribute>
					<xsl:choose>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 6 ]/td[2] = 0 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 6 ]/td[2] = 1 ">
				  	<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 6 ]/td[2] = 2 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
						<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:otherwise>
				  </xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 6 ]/td[4]"/>
        </xsl:if>
        </td>
        <td align="left">
				<xsl:if test="/root/tbody/tr[$rowindex * 7 + 7 ]/td[3] != ''">
				  <xsl:attribute name="align">left</xsl:attribute>
				  <xsl:attribute name="id">tdid_<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 7 ]/td[1]"/></xsl:attribute>
				  <xsl:attribute name="show_type"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 7 ]/td[2]"/></xsl:attribute>
				  <xsl:attribute name="c_date"><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 7 ]/td[5]"/></xsl:attribute>
          <xsl:attribute name="pk" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 7 ]/td[6]"/></xsl:attribute>
          <xsl:attribute name="week_xh" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 7 ]/td[7]"/></xsl:attribute>
          <xsl:attribute name="week_day" ><xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 7 ]/td[8]"/></xsl:attribute>
          
					<xsl:choose>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 7 ]/td[2] = 0 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 7 ]/td[2] = 1 ">
				  	<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="/root/tbody/tr[$rowindex * 7 + 7 ]/td[2] = 2 ">
				  	<xsl:attribute name="onDblClick">selectTdData(this);</xsl:attribute>
						<xsl:attribute name="bgcolor">#D3D3D3</xsl:attribute>
				  </xsl:when>
				  <xsl:otherwise>
				  </xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="/root/tbody/tr[$rowindex * 7 + 7 ]/td[4]"/>
				</xsl:if>
        </td>
				<!--<xsl:if test="$LoopStep = 1 or $Step = 0">
					<xsl:text disable-output-escaping="yes"><![CDATA[</tr>]]></xsl:text>
				</xsl:if>-->
				</tr>
			</xsl:for-each> 
		</tbody>
	
	</xsl:template>
</xsl:stylesheet>

