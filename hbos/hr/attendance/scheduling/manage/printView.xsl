<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<root>
<thead>
	<tr noWrap='true' class='mainHead'>
		<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 0]">
			<xsl:choose>
				<xsl:when test="position() &lt; 5">
		<td nowrap='true' rowspan="2" ><xsl:value-of select="."/></td>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="string-length(.) = 3 ">
						<xsl:if test="string(.) = '星期六' or string(.) = '星期日'">
			<td nowrap='true' background="#99CDFF"  bgcolor="#99CDFF" width='80'>
				<xsl:value-of select="."/>
			</td>
						</xsl:if>
						<xsl:if test="string(.) != '星期六' and string(.) != '星期日'">
			<td nowrap='true' width='80'>
				<xsl:value-of select="."/>
			</td>
						</xsl:if>
					</xsl:if>
					<xsl:if test="string-length(.) != 3">
			<td nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF" width='80'>		
				<xsl:value-of select="substring(.,2,string-length(.))"/>
			</td>
					</xsl:if>
				</xsl:otherwise>
			 </xsl:choose>
		</xsl:for-each>
		</tr>
		<tr noWrap='true' class='mainHead'>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		<xsl:for-each select="/root/tbody/tr[2]/td[position() &gt; 4]">
			<xsl:if test="string-length(.) = 1 or string-length(.) = 2 ">
				<xsl:if test="string-length(.) = 1">
			<td nowrap='true'><xsl:value-of select="."/></td>
				</xsl:if>
				<xsl:if test="string-length(.) = 2">
					<xsl:if test="starts-with(.,'_') = true() ">
			<td nowrap='true' background="#99CDFF"  bgcolor="#99CDFF"><xsl:value-of select="substring-after(.,'_')"/></td>
					</xsl:if>
					<xsl:if test="starts-with(.,'_') = false() ">
			<td nowrap='true'><xsl:value-of select="."/></td>
					</xsl:if>	
				</xsl:if>
			</xsl:if>
			<xsl:if test="string-length(.) &gt; 2 ">
				<xsl:if test="starts-with(.,'_') = true() ">
			<td nowrap='true' background="#99CDFF"  bgcolor="#99CDFF"><xsl:value-of select="substring-after(.,'_')"/></td>
				</xsl:if>
				<xsl:if test="starts-with(.,'_') = false() ">
			<td nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF"><xsl:value-of select="."/></td>
				</xsl:if>
			</xsl:if>
	</xsl:for-each>
  	</tr>
</thead>
    <tbody>
		<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
			<xsl:variable name="EMP_CODE" select="td[1]"/>
			<tr>
				
				<xsl:for-each select="td[position() &gt; 0]">
				
				<td align="right">
          <xsl:choose>
				  <xsl:when test="position() &lt; 5">
					 <xsl:value-of select="."/>
				  </xsl:when>
				  <xsl:otherwise>
					<xsl:if test="string-length(.) &lt;= 5 ">
						<xsl:attribute name="width">
						 <xsl:value-of select="80"/>
						 </xsl:attribute>
						<xsl:value-of select="."/>
					</xsl:if>
					<xsl:if test="string-length(.) &gt; 5">
						<xsl:value-of select="substring(.,0,6)"/>...
					</xsl:if>
				  </xsl:otherwise>
				</xsl:choose>
			 	</td>
          </xsl:for-each>
			</tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>
