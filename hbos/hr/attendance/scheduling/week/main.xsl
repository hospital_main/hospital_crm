<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
<thead>
	<tr noWrap='true' class='mainHead'>
		<th style='display:none' width='25' rowspan="2" ><input type='checkbox'/></th>
		<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 0]">
			<xsl:choose>
				<xsl:when test="position() = 3 or  position() = 4 "> <!--前6列,和最后一列-->
					
				</xsl:when>
				<xsl:when test="position() &lt; 7 or position() = 14 "> <!--前6列,和最后一列-->
					<th nowrap='true' rowspan="2" width='80'><xsl:value-of select="."/></th>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="string-length(.) = 2 "> <!--正常情况下 周几 2个字符-->
						<xsl:if test="string(.) = '周六' or string(.) = '周日'">
					<th nowrap='true' background="#99CDFF"  bgcolor="#99CDFF" width='80'>
						<xsl:value-of select="."/>
					</th>
						</xsl:if>
						<xsl:if test="string(.) != '周六' and string(.) != '周日'">
					<th nowrap='true' width='80'>
						<xsl:value-of select="."/>
					</th>
						</xsl:if>
					</xsl:if>
					<xsl:if test="string-length(.) != 2"> <!--有节假日或者休息 正常情况下 周几 2个字符-->
					<th nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF" width='80'>		
						<xsl:value-of select="."/>
					</th>
					</xsl:if>
				</xsl:otherwise>
			 </xsl:choose>
		</xsl:for-each>
		</tr>
		<tr noWrap='true' class='mainHead'>
		<xsl:for-each select="/root/tbody/tr[2]/td[position() &gt; 6 ]">
			<xsl:choose>
				<xsl:when test="position() = 8"><!--最后一列去掉-->
				</xsl:when>
				<xsl:otherwise>
						<xsl:if test="string-length(.) = 10"><!--正常日期-->
							<th nowrap='true'>
								 <xsl:value-of select="."/></th>
						</xsl:if>
						<xsl:if test="string-length(.) &gt; 10 ">
							<xsl:if test="starts-with(.,'_') = true() and string-length(.) = 11" ><!--周末 蓝色-->
								<th nowrap='true' background="#99CDFF"  bgcolor="#99CDFF">
								<xsl:value-of select="substring(.,2,10)"/></th>
							</xsl:if>
							<xsl:if test="starts-with(.,'_') = true() and string-length(.) &gt; 11" ><!--周末 + 节假日 粉色-->
								<th nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF">
								<xsl:value-of select="substring(.,2,10)"/></th>
							</xsl:if>
							<xsl:if test="starts-with(.,'_') = false() "><!--节假日 粉色-->
							<th nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF">
								<xsl:value-of select="substring(.,1,10)"/></th>
							</xsl:if>
						</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
  	</tr>
</thead>
    <tbody>
		<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
			<xsl:variable name="EMP_CODE" select="td[1]"/>
			<xsl:variable name="SUMB_STATE" select="td[last()]"/>
			<tr>
				<td align="center" width='25'>
					<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
						<xsl:attribute name="value" >
						<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
						</xsl:attribute>
						<xsl:if test="td[last()] = '是'">
							<xsl:attribute name="disabled" >disabled</xsl:attribute>
						</xsl:if>
					</input>
				</td>
				<xsl:for-each select="td[position() &gt; 0]">
				<xsl:variable name="NUM" select="position()-6"></xsl:variable>
          <!-- <xsl:variable name="DATA_TIME" select="/root/tbody/tr[2]/td[$NUM]"/>-->
				<td align="left">
				<xsl:attribute name="onDblClick">
					selectTdData(this,"<xsl:value-of select="$EMP_CODE"/>","<xsl:value-of select="$NUM"/>","<xsl:value-of select="$SUMB_STATE"/>")
                 </xsl:attribute>
          <xsl:attribute name="id">tdid_<xsl:value-of select="../pk/check_id"/>_<xsl:value-of select="$NUM"/>_</xsl:attribute>
          <xsl:attribute name="pk" ><xsl:value-of select="../pk/check_id"/></xsl:attribute>
          <xsl:choose>
          <xsl:when test="position() = 3 or position() = 4 ">
          	<xsl:attribute name="style" >display:none;</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="position() &lt; 7 or position() = 14 ">
					 <xsl:value-of select="."/>
				  </xsl:when>
				  <xsl:otherwise>
					<xsl:if test="string-length(.) &lt;= 5 ">
						<xsl:attribute name="width">
						 <xsl:value-of select="80"/>
						 </xsl:attribute>
						<xsl:value-of select="."/>
					</xsl:if>
					<xsl:if test="string-length(.) &gt; 5">
						<xsl:value-of select="substring(.,0,6)"/>...
					</xsl:if>
				  </xsl:otherwise>
				</xsl:choose>
			 	</td>
          </xsl:for-each>
			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
