<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead></thead>
<tbody>
		<xsl:for-each select="/root/tbody/tr[position() &gt; 0]">
			<xsl:variable name="EMP_CODE" select="td[1]"/>
			<xsl:variable name="SUMB_STATE" select="td[last()]"/>
			<tr>
				<td align="center" width="25">
					<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
						<xsl:attribute name="value" >
						<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
						</xsl:attribute>
						<xsl:if test="td[last()] = '��'">
							<xsl:attribute name="disabled" >disabled</xsl:attribute>
						</xsl:if>
					</input>
				</td>
				<xsl:for-each select="td[position() &gt; 0]">
				<xsl:variable name="NUM" select="position()-6"></xsl:variable>
          <!-- <xsl:variable name="DATA_TIME" select="/root/tbody/tr[2]/td[$NUM]"/>-->
				<td align="left">
				<xsl:attribute name="onDblClick">
					selectTdData(this,"<xsl:value-of select="$EMP_CODE"/>","<xsl:value-of select="$NUM"/>","<xsl:value-of select="$SUMB_STATE"/>")
                 </xsl:attribute>
          <xsl:attribute name="id">tdid_<xsl:value-of select="../pk/check_id"/>_<xsl:value-of select="$NUM"/>_</xsl:attribute>
          <xsl:attribute name="pk" ><xsl:value-of select="../pk/check_id"/></xsl:attribute>
          <xsl:choose>
          <xsl:when test="position() = 3 or position() = 4 ">
          	<xsl:attribute name="style" >display:none;</xsl:attribute>
				  </xsl:when>
				  <xsl:when test="position() &lt; 7 or position() = 14 ">
					 <xsl:value-of select="."/>
				  </xsl:when>
				  <xsl:otherwise>
					<xsl:if test="string-length(.) &lt;= 6 ">
						<xsl:attribute name="width">
						 <xsl:value-of select="80"/>
						 </xsl:attribute>
						<xsl:value-of select="."/>
					</xsl:if>
					<xsl:if test="string-length(.) &gt; 6">
						<xsl:value-of select="substring(.,0,6)"/>...
					</xsl:if>
				  </xsl:otherwise>
				</xsl:choose>
			 	</td>
          </xsl:for-each>
			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
