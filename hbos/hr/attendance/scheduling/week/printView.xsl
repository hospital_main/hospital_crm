<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td)"/>
  	<root>
<thead>	
			<tr noWrap="true" >
				<td noWrap="true" >
					<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
				</td>
				<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
					<td noWrap="true" style="display:none"/>
				</xsl:for-each>
			</tr>
			<tr noWrap="true" >
				<td noWrap="true" align='right' >
					<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
				</td>
				<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
					<td noWrap="true" style="display:none"/>
				</xsl:for-each>
			</tr>
	<tr noWrap='true' class='mainHead'>
		<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 0]">
			<xsl:choose>
				<xsl:when test="position() &lt; 7 or position() = 14 ">
		<td nowrap='true' rowspan="2" ><xsl:value-of select="."/></td>
				</xsl:when>
				<xsl:otherwise>
		<td nowrap='true' ><xsl:value-of select="."/></td>
				</xsl:otherwise>
			 </xsl:choose>
		</xsl:for-each>
	</tr>
	<tr noWrap='true' class='mainHead'>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		<xsl:for-each select="/root/tbody/tr[2]/td[position() &gt; 6 ]">
			<xsl:choose>
				<xsl:when test="position() = 8"><!--最后一列去掉-->
					<td></td>
				</xsl:when>
				<xsl:otherwise>
						<xsl:if test="string-length(.) = 10"><!--正常日期-->
							<td nowrap='true'>
								 <xsl:value-of select="."/></td>
						</xsl:if>
						<xsl:if test="string-length(.) &gt; 10 ">
							<xsl:if test="starts-with(.,'_') = true() and string-length(.) = 11" ><!--周末 蓝色-->
								<td nowrap='true' background="#99CDFF"  bgcolor="#99CDFF">
								<xsl:value-of select="substring(.,2,10)"/></td>
							</xsl:if>
							<xsl:if test="starts-with(.,'_') = true() and string-length(.) &gt; 11" ><!--周末 + 节假日 粉色-->
								<td nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF">
								<xsl:value-of select="substring(.,2,10)"/></td>
							</xsl:if>
							<xsl:if test="starts-with(.,'_') = false() and string-length(.) &gt; 11 "><!--节假日 粉色-->
							<td nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF">
								<xsl:value-of select="substring(.,0,10)"/></td>
							</xsl:if>
						</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
  	</tr>
</thead>
    <tbody>
		<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
			<tr>
				<xsl:for-each select="td[position() &gt; 0]">
				<td align="left">
          <xsl:choose>
				  <xsl:when test="position() &lt; 7">
					 <xsl:value-of select="."/>
				  </xsl:when>
				  <xsl:otherwise>
					<xsl:if test="string-length(.) &lt;= 5 ">
						<xsl:attribute name="width">
						 <xsl:value-of select="80"/>
						 </xsl:attribute>
						<xsl:value-of select="."/>
					</xsl:if>
					<xsl:if test="string-length(.) &gt; 5">
						<xsl:value-of select="substring(.,0,6)"/>...
					</xsl:if>
				  </xsl:otherwise>
				</xsl:choose>
			 	</td>
          </xsl:for-each>
			</tr>
      </xsl:for-each>
    </tbody>
	<tfoot>
		<tr noWrap="true" >
				<td noWrap="true" align='left' >
					<xsl:attribute name="style">colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
				</td>
				<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
					<td noWrap="true" style="display:none"/>
				</xsl:for-each>
			</tr>
			<tr noWrap="true" >
				<td noWrap="true" align='right' >
					<xsl:attribute name="style">colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
				</td>
				<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
					<td noWrap="true" style="display:none"/>
				</xsl:for-each>
			</tr>
	</tfoot>
    </root>
  </xsl:template>
</xsl:stylesheet>
