<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none' width='25' ><input type='checkbox'/></th>
				<th>出勤科室编码</th>
				<th>出勤科室名称</th>
				<th>考勤类别</th>
				<th>是否提交</th>
				<th>提交人</th>
				<th>是否审核</th>
				<th>审核人</th>
			</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position() &gt; 0]">
        <tr>
					<td align="right">
			   	<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
				    <xsl:attribute name="value" >
						<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;				
						</xsl:for-each>
					</xsl:attribute>
					</input>
          </td>
          <xsl:for-each select="td">
        	<td>
						<xsl:choose>
              <xsl:when test="position()=1">
              	<a href="#">
								  <xsl:attribute name="onclick">
								openDeptClass("<xsl:value-of select="."/>");
								  </xsl:attribute>
								<xsl:value-of select="."/>
								</a>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="."/>
              </xsl:otherwise>
            </xsl:choose>
					</td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

