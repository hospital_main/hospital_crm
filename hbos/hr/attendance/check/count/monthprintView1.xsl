<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td)"/>
  	<root>
<thead>
	<tr noWrap="true" >
		<td noWrap="true" >
			<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
		</td>
		<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
			<td noWrap="true" style="display:none"/>
		</xsl:for-each>
	</tr>
	<tr noWrap="true" >
		<td noWrap="true" align="left" >
			<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
		</td>
		<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
			<td noWrap="true" style="display:none"/>
		</xsl:for-each>
	</tr>
	<tr noWrap='true' class='mainHead'>
		<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 0]">
			<xsl:variable name="TR1_AMOUNT" select="/root/tbody/tr[1]/td[last()]"/>
			<xsl:choose>
				<xsl:when test="position() &lt; 2">
		<td nowrap='true' rowspan="2" align="left"><xsl:value-of select="."/></td>
				</xsl:when>
				<xsl:when test="position() &gt; $TR1_AMOUNT+1 and position() &lt; last()">
						<td nowrap='true' rowspan="2" width='40'><xsl:value-of select="."/></td>
				</xsl:when>
				<xsl:when test="position() = last()">
					<td nowrap='true' rowspan="2" width='40'>本月休假情况说明</td>
				</xsl:when>
				<xsl:otherwise>
			<td nowrap='true' width='10'>
				<xsl:value-of select="."/>
			</td> 
				</xsl:otherwise>
			 </xsl:choose>
		</xsl:for-each>
		</tr>
		<tr noWrap='true' class='mainHead'>
			<td></td>
		<xsl:for-each select="/root/tbody/tr[2]/td[position() &gt; 1]">
			<xsl:variable name="TR2_AMOUNT" select="/root/tbody/tr[2]/td[last()]"/>
			<xsl:choose>
					<xsl:when test="position() &gt; $TR2_AMOUNT">
						
					</xsl:when>
					<xsl:otherwise> 
						<td nowrap='true'><xsl:value-of select="."/></td> 
					</xsl:otherwise>
				</xsl:choose>
	</xsl:for-each>
  	</tr>
</thead>
    <tbody>
		<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
			<xsl:variable name="EMP_CODE" select="td[1]"/>
			<tr>
				<xsl:for-each select="td[position() &gt; 0]"> 
        <xsl:choose>
					<xsl:when test="position() = last()">
						<td align="right"></td>
					</xsl:when>
					<xsl:otherwise>
						
						<xsl:choose>
						  <xsl:when test="position() &lt; 2">
								<td align="left" width='80'><xsl:value-of select="." />	</td>
						  </xsl:when>
						  <xsl:otherwise>
								<td align="left" width="5"><xsl:value-of select="." /></td>
						  </xsl:otherwise>
						</xsl:choose>
					 
					</xsl:otherwise>
				</xsl:choose>
				
          </xsl:for-each>
			</tr>
      </xsl:for-each>
    </tbody>
		<tfoot>
			<tr noWrap="true" >
				<td noWrap="true" align='right' >
					<xsl:attribute name="style">colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
				</td>
				<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
					<td noWrap="true" style="display:none"/>
				</xsl:for-each>
			</tr>
		</tfoot>
    </root>
  </xsl:template>
</xsl:stylesheet>
