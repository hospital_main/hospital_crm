<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 0]">
				<th nowrap='true'><xsl:value-of select="."/></th>
			</xsl:for-each>
			</tr>
		</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position() &gt; 1]">
				<xsl:variable name="spanvalue" select="td[1]" />
				<xsl:variable name="cur_pos" select="position() + 1" />					 
				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]= $spanvalue ])" />
        <tr>
          <xsl:for-each select="td[position() &gt; 0]">
				  <xsl:choose>
					<xsl:when test="position()=1"> 
						<xsl:if test="$cur_pos = 1">
							<xsl:if test="$spanvalue != ''">													
								<td rowspan="{$rowspan}"> 
									<xsl:value-of select="." />											
								</td>
							 </xsl:if>
						</xsl:if>
						<xsl:if test="$cur_pos &gt; 1">
							<xsl:if test="$spanvalue != (../../tr[$cur_pos - 1 ]/td[1]) ">
								<td rowspan="{$rowspan}">
									<xsl:value-of select="." />											
								</td>
							</xsl:if>
					
							<xsl:if test="$spanvalue = (../../tr[$cur_pos - 1 ]/td[1]) ">
								<xsl:if test="$spanvalue != ''">
								<td align="center" style="display:none" >
								</td>
								</xsl:if>
								<xsl:if test="$spanvalue = ''">
								<td align="center" >
									<xsl:value-of select="."/>
								</td>
								</xsl:if>
							</xsl:if>
					</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						 <td><xsl:value-of select="."/></td>
					</xsl:otherwise>
			  	</xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>