<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
    <thead>
      	<tr noWrap='true' class='mainHead'>
				<td>出勤科室编码</td>
				<td>出勤科室名称</td>
				<td>考勤类别</td>
				<td>是否提交</td>
				<td>提交人</td>
				<td>是否审核</td>
				<td>审核人</td>
				<td>是否确认</td>
				<td>确认人</td>
      	</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position() &gt; 0]">
        <tr>
          <xsl:for-each select="td[position() &gt; 0]">
					<td><xsl:value-of select="."/></td>			
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
	</root>
  </xsl:template>
</xsl:stylesheet>

