<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<tr noWrap='true' class='mainHead'>
				<!--th style='display:none' width='25' ><input type='checkbox'/></th-->
				<th>科室编码</th>
				<th>科室名称</th>
				<th>职工编码</th>
				<th>职工姓名</th>
				<th>考勤类别</th>
				<th>职工类别</th>
			</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position() &gt; 0]">
        <tr>
          <xsl:for-each select="td[position() &gt; 0]">
					  <xsl:choose>
						  <xsl:when test="position()=1">
						  <td>
						  	<xsl:value-of select="."/>
						  </td>
						  </xsl:when>
						  <xsl:otherwise>
							<td><xsl:value-of select="."/></td>
			  			</xsl:otherwise>
	      		</xsl:choose>
						
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

