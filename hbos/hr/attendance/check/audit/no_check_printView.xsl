<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
    <thead>
      	<tr noWrap='true' class='mainHead'>
					<td>科室编码</td>
					<td>科室名称</td>
					<td>职工编码</td>
					<td>职工姓名</td>
					<td>考勤类别</td>
					<td>职工类别</td>
      	</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position() &gt; 0]">
        <tr>
          <xsl:for-each select="td[position() &gt; 0]">
					<td><xsl:value-of select="."/></td>			
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
	</root>
  </xsl:template>
</xsl:stylesheet>

