<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<tr noWrap='true' class='mainHead'>
				<th nowrap='true' width='100'>职工编码</th>
				<th nowrap='true' width='100'>职工名称</th>
				<th nowrap='true' width='100'>编制科室</th>
				<th nowrap='true' width='100'>出勤科室</th>
				<th nowrap='true' width='100'>考勤类别</th>
				<th nowrap='true' width='*'>错误原因</th>
      </tr>
    </thead>
    <tbody>
    	<xsl:for-each select="/root/tbody/tr">
      <tr>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td align='left'>
								<xsl:value-of select="."/>
							</td>
            </xsl:when>
            <xsl:otherwise>
							<td>
								<xsl:value-of select="."/>
			  			</td>
			  		</xsl:otherwise>
          </xsl:choose>
				</xsl:for-each>
      </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

