<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
    <thead>
      	<tr noWrap='true' class='mainHead'>
				<td nowrap='true' width='100'>职工编码</td>
				<td nowrap='true' width='100'>职工名称</td>
				<td nowrap='true' width='100'>编制科室</td>
				<td nowrap='true' width='100'>出勤科室</td>
				<td nowrap='true' width='100'>考勤类别</td>
				<td nowrap='true' width='300'>错误原因</td>
      	</tr>
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
      <tr>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td align='left'>
								<xsl:value-of select="."/>
							</td>
            </xsl:when>
            <xsl:otherwise>
							<td>
								<xsl:value-of select="."/>
			  			</td>
			  		</xsl:otherwise>
          </xsl:choose>
				</xsl:for-each>
      </tr>
      </xsl:for-each>
    </tbody>
	</root>
  </xsl:template>
</xsl:stylesheet>
