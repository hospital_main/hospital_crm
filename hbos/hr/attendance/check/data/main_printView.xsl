<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
    <thead>
      	<tr noWrap='true' class='mainHead'>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 0 ]">
				<xsl:variable name="TR1_AMOUNT" select="/root/tbody/tr[1]/td[last()]"/>
				 <xsl:choose>
					<xsl:when test="position() = 1 ">
						
					</xsl:when>
					<xsl:when test="position() = last()">
						
					</xsl:when>
					<xsl:when test="position() &lt; 5 and position() &gt; 1">
						<td nowrap='true' rowspan="2" width='80'><xsl:value-of select="."/></td>
					</xsl:when>
					
					<xsl:when test="position() &gt; $TR1_AMOUNT + 4 and position() &lt; last()">
						<td nowrap='true' rowspan="2" width='80'><xsl:value-of select="."/></td>
					</xsl:when>
					
					
					<xsl:otherwise>
						<xsl:if test="string-length(.) = 3 ">
							<xsl:if test="string(.) = '星期六' or string(.) = '星期日'">
								<td nowrap='true' background="#99CDFF"  bgcolor="#99CDFF" width='80'>
									<xsl:value-of select="."/>
								</td>
							</xsl:if>
							<xsl:if test="string(.) != '星期六' and string(.) != '星期日'">
								<td nowrap='true' width='80'>
									<xsl:value-of select="."/>
								</td>
							</xsl:if>
						</xsl:if>
						<xsl:if test="string-length(.) != 3">
							<td nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF" width='80'>		
							<xsl:value-of select="substring(.,2,string-length(.))"/>
							</td>
						</xsl:if>
						
					 </xsl:otherwise>
				 </xsl:choose>
			</xsl:for-each>
      	</tr>
      	<tr noWrap='true' class='mainHead'>
		    <td></td><td></td><td></td>
			<xsl:for-each select="/root/tbody/tr[2]/td[position() &gt; 4 ]">
				<xsl:variable name="TR2_AMOUNT" select="/root/tbody/tr[2]/td[last()]"/>
				<xsl:choose>
					<xsl:when test="position() &gt; $TR2_AMOUNT">
							<td  nowrap='true' style="display:none"></td>
					</xsl:when>
					<xsl:otherwise>
									<xsl:if test="string-length(.) = 1 or string-length(.) = 2 ">
									<xsl:if test="string-length(.) = 1">
										<td nowrap='true'><xsl:value-of select="."/></td>
									</xsl:if>
									<xsl:if test="string-length(.) = 2">
										<xsl:if test="starts-with(.,'_') = true() ">
										<td nowrap='true' background="#99CDFF"  bgcolor="#99CDFF"><xsl:value-of select="substring-after(.,'_')"/></td>
										</xsl:if>
										<xsl:if test="starts-with(.,'_') = false() ">
										<td nowrap='true'><xsl:value-of select="."/></td>
										</xsl:if>	
									</xsl:if>
								</xsl:if>
								<xsl:if test="string-length(.) &gt; 2 ">
									<xsl:if test="starts-with(.,'_') = true() ">
										<td nowrap='true' background="#99CDFF"  bgcolor="#99CDFF"><xsl:value-of select="substring-after(.,'_')"/></td>
									</xsl:if>
									<xsl:if test="starts-with(.,'_') = false() ">
										<td nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF"><xsl:value-of select="."/></td>
									</xsl:if>	
								</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				
				
			</xsl:for-each>
      	</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
		<xsl:variable name="SUB_STATE" select="td[1]"/>
		<xsl:variable name="EMP_CODE" select="td[2]"/>
		<xsl:variable name="CHECK_ID" select="./pk/check_id"/>
		
        <tr>
          <xsl:for-each select="td[position() &gt; 0]">
          <xsl:variable name="NUM" select="position()- 5 "></xsl:variable>
          <xsl:choose>
         	<xsl:when test="position() = 1">
				  	
				  </xsl:when>
				  <xsl:when test="position() = last()">
						
					</xsl:when>
				  <xsl:when test="position() &lt; 6 and position() &gt; 1 ">
				  <td>
					 <xsl:value-of select="."/>
				  </td>
				  </xsl:when>
				  
				  <xsl:otherwise>
				  	<!-- colorChange(this);getTdData(this,"<xsl:value-of select="$CHECK_ID"/>","<xsl:value-of select="$NUM"/>")-->
				  <td>
				     <xsl:attribute name="onDblClick">
				     			selectTdData(this,"<xsl:value-of select="$CHECK_ID"/>","<xsl:value-of select="$NUM"/>","<xsl:value-of select="$SUB_STATE"/>")
						 </xsl:attribute>
						 <xsl:attribute name="id">tdid_<xsl:value-of select="../pk/check_id"/>_<xsl:value-of select="$NUM"/>_</xsl:attribute>
					<xsl:if test="string-length(.) &lt;= 6 ">
						<xsl:attribute name="width">
							 <xsl:value-of select="80"/>
							 </xsl:attribute>
						
						 <xsl:value-of select="."/>
					</xsl:if>
					<xsl:if test="string-length(.) &gt; 5">
						<xsl:value-of select="substring(.,0,6)"/>...
					</xsl:if>
					</td>
				  </xsl:otherwise>
				</xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
	</root>
  </xsl:template>
</xsl:stylesheet>