<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:12;fontsize:maintitle'>职工学历信息</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap='true' class='mainHead'>
				  	<td nowrap='true'>职工编码</td>
				  	<td nowrap='true'>职工姓名</td>
				  	<td nowrap='true'>开始时间</td>
				  	<td nowrap='true'>结束时间</td>
				  	<td nowrap='true'>地点</td>
				  	<td nowrap='true'>主题</td>
				  	<td nowrap='true'>学时</td>
				  	<td nowrap='true'>学分</td>
				  	<td nowrap='true'>类别</td>
				  	<td nowrap='true'>主办单位</td>
				  	<td nowrap='true'>主办单位性质</td>
				  	<td nowrap='true'>备注</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td align="left">
								<xsl:value-of select="."/>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
