<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:15;fontsize:maintitle'>职工论文及学术会议信息</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				
				<tr noWrap='true' class='mainHead'>
					<td nowrap='true'>作者排名</td>
				  	<td nowrap='true'>职工编码</td>
				  	<td nowrap='true'>职工姓名</td>
				  	<td nowrap='true'>日期</td>
				  	<td nowrap='true'>CN刊号</td>
				  	<td nowrap='true'>ISSN刊号</td>
				  	<td nowrap='true'>刊物名称</td>
				  	<td nowrap='true'>刊物级别</td>
				  	<td nowrap='true'>核心统计源</td>
				  	<td nowrap='true'>论文名称</td>
				  	<td nowrap='true'>会议类型</td>
				  	<td nowrap='true'>卷</td>
				  	<td nowrap='true'>期</td>
				  	<td nowrap='true'>页码</td>
				  	<td nowrap='true'>备注</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td align="left">
								<xsl:value-of select="."/>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
