<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:10;fontsize:maintitle'>职工计划生育信息</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					
				</tr>
				
				<tr noWrap='true' class='mainHead'>
				  	<td nowrap='true'>职工编码</td>
				  	<td nowrap='true'>职工姓名</td>
				  	<td nowrap='true'>夫姓名</td>
				  	<td nowrap='true'>夫民族</td>
				  	<td nowrap='true'>妇姓名</td>
				  	<td nowrap='true'>妇民族</td>
				  	<td nowrap='true'>夫是否独生</td>
				  	<td nowrap='true'>妇是否独生</td>
				  	<td nowrap='true'>夫身份证</td>
				  	<td nowrap='true'>妇身份证</td>
				  	<td nowrap='true'>夫工作单位</td>
				  	<td nowrap='true'>妇工作单位</td>
				  	<td nowrap='true'>夫婚姻状态</td>
				  	<td nowrap='true'>妇婚姻状态</td>
				  	<td nowrap='true'>初婚日期</td>
				  	<td nowrap='true'>婚变日期</td>
				  	<td nowrap='true'>怀孕政策</td>
				  	<td nowrap='true'>怀孕日期</td>
				  	<td nowrap='true'>孕产结果</td>
				  	<td nowrap='true'>怀孕终止日期</td>
				  	<td nowrap='true'>终止手术地点</td>				  	
				  	<td nowrap='true'>避孕节育措施</td>
				  	<td nowrap='true'>避孕手术日期</td>
				  	<td nowrap='true'>医院名称</td>
				  	<td nowrap='true'>子女身份证</td>
				  	<td nowrap='true'>子女姓名</td>
				  	<td nowrap='true'>子女性别</td>
				  	<td nowrap='true'>子女出生日期</td>
				  	<td nowrap='true'>子女政策内外</td>
				  	<td nowrap='true'>子女孩次</td>
				  	<td nowrap='true'>子女出生证号</td>
				  	<td nowrap='true'>子女健康状况</td>
				  	<td nowrap='true'>户籍地址</td>
				  	<td nowrap='true'>现居住地</td>
				  	<td nowrap='true'>夫户籍地址</td>
				  	<td nowrap='true'>夫现地址</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td align="left">
								<xsl:value-of select="."/>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
