<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:9;fontsize:maintitle'>职工专利登记信息</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				
				<tr noWrap='true' class='mainHead'>
					<td nowrap='true'>作者排名</td>
				  	<td nowrap='true'>职工编码</td>
				  	<td nowrap='true'>职工姓名</td>
				  	<td nowrap='true'>专利编号</td>
				  	<td nowrap='true'>专利名称</td>
				  	<td nowrap='true'>申请日期</td>
				  	<td nowrap='true'>授权日期</td>
				  	<td nowrap='true'>专利类别</td>
				  	<td nowrap='true'>备注</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td align="left">
								<xsl:value-of select="."/>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
