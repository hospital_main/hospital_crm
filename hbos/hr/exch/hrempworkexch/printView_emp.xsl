<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:10;fontsize:maintitle'>职工工作履历</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				 
				<tr noWrap='true' class='mainHead'>
				  	<td nowrap='true'>职工编码</td>
				  	<td nowrap='true'>职工姓名</td>
				  	<td nowrap='true'>开始时间</td>
				  	<td nowrap='true'>结束时间</td>
				  	<td nowrap='true'>工作单位</td>
				  	<td nowrap='true'>工作部门</td>
				  	<td nowrap='true'>担任职务</td>
				  	<td nowrap='true'>技术职称</td>
				  	<td nowrap='true'>备注</td>
				  	<td nowrap='true'>证明人</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td align="left">
								<xsl:value-of select="."/>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
