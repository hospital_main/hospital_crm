	  var flag = "1";
	  var showstop=0,showlower=0,texpands=1;
      //全局变量 职工附属信息ridvalue
      var ridvalue="";
      //定义变量 用于批量添加页面判断是通过树查询职工还是通过条件查询职工
      var findEmpByTreeOrCond = "tree";
      //全局变量 职工基本信息empidvalue
      var empidvalue="";
      //条件说明cond_desc 条件列表cond_list 条件内容cond
      var cond_desc = "";  
      var cond_list = "";
      var cond = "";
      
      //条件名称
      var cond_title = "";
      var cond_item  = "";
				var cond_rep  = "";
				var condvalue  = "";
				var saveResult = false;
      
       var num = 1;
      function init(){//初始化左侧树
    	condTable2.style.display="none";
    	var pk=getPk();
        unitTree.expand=1;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        unitTree.init();
        
        //清空部门标志，部门代码文本框缓存内容，
        sign.value="";
        dept_code.value="";
        //319固定 列（单位名称、部门、职工编码、职工姓名、性别）需求修改start
				subBody.para = "<userid>"+getUserID()+"</userid>";
				subBody.load = "hr_empDocumentBasicInfoMain_frozencolumn";
				subBody.post();
				var datas = subBody.getOneDim();
				if(datas[0]!=0){
					num = parseInt(datas[0])+num;
					result.fixCol="1,"+num
		  	  result.async="true"
		  	  num= 1;
				}
				
		  	//319固定 列（单位名称、部门、职工编码、职工姓名、性别）需求修改end
      }
      
      function loadData(isLeaf,pk,parameter,label){//点击数节点时触发，用于加载右侧的列表数据
        //document.getElementById("firstNode").style.display='';
        //alert(isLeaf+":"+pk+":"+parameter+":"+label);
        
        var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
				xmlDoc.async="false";  
				xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + pk + "</root>");
				//从PK中取得dcode sign
				dept_code.value = xmlDoc.getElementsByTagName("dcode")[0].firstChild.nodeValue;
				sign.value = xmlDoc.getElementsByTagName("sign")[0].firstChild.nodeValue;
				inlower.value = xmlDoc.getElementsByTagName("inlower")[0].firstChild.nodeValue;
				userid.value = getUserID();
				
				
		    hr_empDocumentBasicInfoMain_select.click();
      	
      }
      
      function show_isstop(t){
        showstop=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
      }
      
      function include_lower(t){
        showlower=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
      }
      
      function show_all(t){
        var pk=getPk();
        if(t.checked){
          texpands=10;
          unitTree.expand=10;
          unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        }else{
          texpands=1;
          unitTree.expand=1;
          unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        }
      }
      
      function getPk(){
        return "<comp_code>"+getCompCode()+"</comp_code>"+"<user_id>"+getUserID()+"</user_id>"
               +"<show_duty>1</show_duty>"+"<show_stop>"+showstop+"</show_stop>"
               +"<include_lower>"+showlower+"</include_lower>"+"<is_super>0</is_super>"
               +"<show_count>0</show_count>";
      }
      
      //按钮权限查询
      var vXml ;var len;
      function getPurview(){
      	window.xmlhttp.post('hr_empDocumentBasicInfoMain_buttonPurview', "<tableid>sys_emp</tableid><userid>"+getUserID()+"</userid>","?isCheck=false");
		var resText = window.xmlhttp._object.responseXml;
      	
      	var addPerm = resText.getElementsByTagName("td")[0].firstChild.nodeValue;
      	var editPerm = resText.getElementsByTagName("td")[1].firstChild.nodeValue;
      	var deletePerm = resText.getElementsByTagName("td")[2].firstChild.nodeValue;
      	var photoPerm = resText.getElementsByTagName("td")[3].firstChild.nodeValue;
      	var vXml = new ActiveXObject("Microsoft.XMLDOM");
				vXml.async=false;
				vXml.loadXML(result.serverXml);
      	len = vXml.selectNodes("//tbody/tr").length;
      	
      	if(addPerm!='0'){
      		addPermButton.style.display="";
      	}else{
      		addPermButton.style.display="none";
      	}
      	if(editPerm!='0' && len >0){//result.getRows()>0){
      		updatePermButton.style.display="";
      		batchEditPermButton.style.display="";
      	}else{
      		updatePermButton.style.display="none";
      		batchEditPermButton.style.display="none";
      	}
      	if(deletePerm!='0' && len >0){//result.getRows()>0){
      		deletePermButton.style.display="";
      	}else{
      		deletePermButton.style.display="none";
      	}
      	if(photoPerm!='0' && len >0){//result.getRows()>0){
      		photoPermButton.style.display="";
      	}else{
      		photoPermButton.style.display="none";
      	}
      }
      
      //判断result复选框 互斥
      var checkFlag=-1;    //记录第一次选择的复选框的行号
	  function judgeCheck(){
	  	var vXml = new ActiveXObject("Microsoft.XMLDOM");
			vXml.async=false;
			vXml.loadXML(result.serverXml);
			len = vXml.selectNodes("//tbody/tr").length;
	  	if(len>0){
		  	var count = 0;
	      	var data = result.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(checkFlag==-1){
			        		checkFlag=i;
			        	}else{
			        		if(checkFlag!=i){
			        			if(result.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag]!=null){
						    		result.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag].getElementsByTagName("input")[0].checked=false;
						    	}
			        			checkFlag=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0) checkFlag=-1;  //恢复复选框行号记录初始值
	    }
	  }
	  
	  //添加
	  function insert(){
      	empidvalue="";
      	openDialog('insertSet.html','dialogWidth:1000px;dialogHeight:650px');
      	
      }
      //添加页面需要的参数
      function insertMess(id){
			this.pk={id:'empid',value:id+"##"+"sys_emp"};
			this.button={text:'保存',name:'hr_empDocumentBasicInfoMain_insert',onclick:'saveHandler(this);',accessKey:'S'};
	  }
	  //添加页面需要的参数
	  function insertCallbacks(){
				setInsertPage('hr_empDocumentBasicInfoMain_showFields','<userid>'+getUserID()+'</userid><sign>'+sign.value+'</sign><dept_code>'+dept_code.value+'</dept_code>','',"<empid>"+empidvalue+"</empid><userid>"+getUserID()+"</userid>",new insertMess(empidvalue));
	  }
      
      //修改
      function update(obj){
      	var count = 0;
      	var data = result.getElementsByTagName('tbody')[1];
    	var trs = data.getElementsByTagName("tr"); 
    	for (var i=0; i<trs.length; i++){
    		var inputs =  trs[i].getElementsByTagName("input");
    		for (var j=0; j<inputs.length; j++) {	        
		        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
		        	//得到要修改的记录的empid
		        	var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
					xmlDoc.async="false";  
					xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + inputs[j].value + "</root>"); 
					empidvalue = xmlDoc.getElementsByTagName("empid")[0].firstChild.nodeValue;
		        	count++;
		        }
	    	}
    	}
    	if (count == 0) {
	      alert('请选择要修改的记录!');
	      return false;
	    }
	    if (count > 1) {
	      alert('只能选择一条记录进行修改!');
	      return false;
	    }
	    
      	openDialog('updateSet.html?load=<emp_id>'+empidvalue+'</emp_id>','dialogWidth:1000px;dialogHeight:650px');
      }
      //点击链接页面
      function updateSet(obj){
      	empidvalue=obj;
      	openDialog('updateSet.html?load=<emp_id>'+empidvalue+'</emp_id>','dialogWidth:1000px;dialogHeight:650px');
      }
      //修改页面需要的参数
      function updateMess(id){
			this.pk={id:'empid',value:id};
			this.button={text:'保存',name:'hr_empDocumentBasicInfoMain_update',onclick:'saveHandler(this);',accessKey:'S'};
	  }
	  //修改页面需要的参数
	  function updateCallbacks(){
				setUpdatePage('hr_empDocumentBasicInfoMain_update_list','<userid>'+getUserID()+'</userid>','hr_empDocumentBasicInfoMain_update',"<empid>"+empidvalue+"</empid><userid>"+getUserID()+"</userid>",new updateMess(empidvalue));
	  }
      
      //删除
      function deleteItems(obj){
      	var count = 0;
      	var data = result.getElementsByTagName('tbody')[1];
    	var trs = data.getElementsByTagName("tr"); 
    	for (var i=0; i<trs.length; i++){
    		var inputs =  trs[i].getElementsByTagName("input");
    		for (var j=0; j<inputs.length; j++) {	        
		        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
		        	count++;
		        }
	    	}
    	}
    	if (count == 0) {
	      alert('请选择要删除的记录!');
	      return false;
	    }
	    
	    //删除职工提示信息
	    var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
		xmlDoc.async="false";  
		xmlDoc.loadXML(result.getSelectXml());
	    window.xmlhttp.post('hr_empDocumentBasicInfoMain_deleteSuggestiveInfo', "<userid>"+getUserID()+"</userid><empid>"+xmlDoc.getElementsByTagName('empid')[0].firstChild.nodeValue+"</empid>");
		var resText = window.xmlhttp._object.responseXml;
	    if(resText.getElementsByTagName("td")[0].firstChild.nodeValue=="false"){
	    	alert(resText.getElementsByTagName("td")[1].firstChild.nodeValue);
	    }else{
	    	if(resText.getElementsByTagName("td")[1].firstChild==null)
	    	{
	    		if(confirm("是否删除用户?"))
	    		{
		    		result.submit_choose(obj.name,"<empid>"+xmlDoc.getElementsByTagName('empid')[0].firstChild.nodeValue+"</empid><userid>"+getUserID()+"</userid>");
					result.refresh();
	    		}
	    	}
	    	else
	    	{
		    	if(confirm("如果要删除用户，那么所有与该用户相关的 "
		    				+resText.getElementsByTagName("td")[1].firstChild.nodeValue
		    				+" 表中的记录都将被删除，确认要删除用户及其相关信息吗？")){
		    		result.submit_choose(obj.name,"<empid>"+xmlDoc.getElementsByTagName('empid')[0].firstChild.nodeValue+"</empid><userid>"+getUserID()+"</userid>");
					result.refresh();
		    	}
	    	}
	    }
      }
	    
	  //批量修改
	  function batchEdit(obj){
	  	ridvalue="";
		openIFDialog(window,'batch.html?load=<dept_code>'+dept_code.value+'</dept_code><sign>'+sign.value+'</sign><inlower>'+inlower.value+'</inlower><userid>'+getUserID()+'</userid>','dialogWidth:850px;dialogHeight:570px')
	  }
      
      //刷新结果集
      function refreshParent(){
      	if(unitTree.style.display==''){
      		//alert("科室职位目录树");
      		window.document.body.all['hr_empDocumentBasicInfoMain_select'].click();
      		
      	}else{
      		//alert("条件控件");
      		judgeCommonCondListCheck();
      	}
	  }
	  
	  //图片处理
	function photoHandle(){
	  	var count = 0;
      	var data = result.getElementsByTagName('tbody')[1];
    	var trs = data.getElementsByTagName("tr");
    	
    	var empidtemp = ""; 
    	
    	for (var i=0; i<trs.length; i++){
    		var inputs =  trs[i].getElementsByTagName("input");
    		for (var j=0; j<inputs.length; j++) {	        
		        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
		        	//得到要修改的记录的emp_id
		        	var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
					xmlDoc.async="false";  
					xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + inputs[j].value + "</root>"); 
					empidtemp = xmlDoc.getElementsByTagName("empid")[0].firstChild.nodeValue;
		        	count++;
		        }
	    	}
    	}
    	if (count == 0) {
	      alert('请选择记录!');
	      return false;
	    }
	    if (count > 1) {
	      alert('只能选择一条记录进行修改!');
	      return false;
	    }
	    empid.value = empidtemp;
	  	openDialog('photoHandleM.html?load=<empid>'+ empid.value + '</empid>', 'dialogWidth:700px;dialogHeight:520px');
	  }
	  
	  //科室职位目录按钮
	  function deptDutyList(){
	  	flag = "1";
	  	treeTable.style.display='';
	  	unitTree.style.display='';
	  	condTable.style.display='none';
	  	condTable2.style.display='none';
	  	commonCondDispaly.value='';
	  	//批量更新页面判断通过树还是条件查询职工
	  	findEmpByTreeOrCond = "tree";
	  	//清空职工列表
        result.innerHTML = "";
        //判断职工功能按钮的权限
	  	getPurview(); 
	  }
	  
	  //条件查询按钮 
	  function condSelect(){
	  	flag = "3";
	  	treeTable.style.display='none';
	  	unitTree.style.display='none';
	  	condTable.style.display='';
	  	condTable2.style.display="none";
	  	commonCondSelectByUserid.value = getUserID();
	  	//批量更新页面判断通过树还是条件查询职工
	  	findEmpByTreeOrCond = "cond";
        //清空职工列表
        result.innerHTML = "";
        //判断职工功能按钮的权限
        getPurview();  
	  	hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  }
	  
	  //向常用条件列表commonCondList的checkBox的onclick事件中加入查询方法
      function addFunctionToCheckBoxOfCommonCond(){
	      	var data = commonCondList.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr"); 
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox') {
			        	inputs[j].attachEvent('onclick',judgeCommonCondListCheck);
			        }
		    	}
	    	}
      }
	  
	  //判断commonCondList复选框 互斥 给条件显示框赋值具体条件
      var commonCondListCheckFlag=-1;    //记录第一次选择的复选框的行号
	  function judgeCommonCondListCheck(){
	  	if(commonCondList.getRows()>0){
		  	var count = 0;
	      	var data = commonCondList.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(commonCondListCheckFlag==-1){
			        		commonCondListCheckFlag=i;
			        	}else{
			        		if(commonCondListCheckFlag!=i){
			        			if(commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag]!=null){
						    		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
						    	}
			        			commonCondListCheckFlag=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0){ 
	    		commonCondListCheckFlag=-1;  //恢复复选框行号记录初始值
	    		commonCondDispaly.value = "";  //清空条件显示框
				//清空职工列表
				result.innerHTML = "";
	    	}else{
	    		//给条件显示框赋值具体条件
				commonCondDispaly.value = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				//按常用条件查询职工
				var userid_cond = getUserID();
				var common_cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
				var common_cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
				var common_cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				cond_desc = common_cond_desc;
				//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  
				common_cond = common_cond.replace(/</g,"&lt;");
				common_cond = common_cond.replace(/>/g,"&gt;");
				
				cond_list = common_cond_list;
				cond = common_cond;
				cond_desc = common_cond_desc;
				
				//window.xmlhttp.post('hr_empDocumentBasicInfoMain_findEmpByCommonCond', "<userid_cond>"+userid_cond+"</userid_cond><common_cond>"+common_cond+"</common_cond><common_cond_list>"+common_cond_list+"</common_cond_list>");
				//var resText = window.xmlhttp._object.responseXml;
				//result.setDataXml(resText.xml);
				//王楠修改，因为setDataXml方法不能设分页，所以只能用editTable1提交方式查询
				userid_cond1.value = userid_cond;
				common_cond1.value = common_cond;
				common_cond_list1.value = common_cond_list;
				
				hr_empDocumentBasicInfoMain_findEmpByCommonCond.click();
	    	}
	    	getPurview();  //判断职工功能按钮的权限
	    }
	  }
	  
	  //新条件 在页面顶定义三个全局变量 条件说明cond_desc 条件列表cond_list 条件内容cond
	  function newCond(){
	  		//把全局变量清空
	  		cond_desc = "";
	  		cond_list = "";
	  		cond = "";
	  		//清空条件显示框
			commonCondDispaly.value = "";
			//清空职工列表
			result.innerHTML = "";
			//判断职工功能按钮的权限
        	getPurview();  
	  		if(commonCondListCheckFlag!=-1){  //判断是否选择了常用条件
		  		//把列表commonCondList的复选框选择去掉
		  		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
		  		//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1;  
		  		
			}
  			openIFDialog(window,'../cond/cond.html?load=<cond_type>new</cond_type><module>basic</module>','dialogWidth:520px;dialogHeight:500px');
	  }
	  
	  //修改条件
	  function updateCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请选择条件！！！");
	  		return false;
	  	}else{
	  		/*if(commonCondListCheckFlag==-1){
	  			alert("请选择一个常用条件！！！");
	  			return false;
	  		}else{*/
		  		//条件说明cond_desc 条件列表cond_list 条件内容cond
		  		//cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
		  		//cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
		  		//cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
		  		//把列表commonCondList的复选框选择去掉
		  		//if(commonCondListCheckFlag!=-1){
		  		//	commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
		  		//}
		  		//恢复复选框行号记录初始值
		  		//commonCondListCheckFlag=-1;  
		  		//清空条件显示框
				//commonCondDispaly.value = "";
				//清空职工列表
				//result.innerHTML = "";
				//判断职工功能按钮的权限
        		//getPurview();  
        		
        		//处理特殊字符 "<"、">" 变成 "小于"、"大于"  
				cond = cond.replace(/</g,"小于");
				cond = cond.replace(/>/g,"大于");
				//处理特殊字符 "&lt;"、"&gt;" 变成 "小于"、"大于"  
				cond = cond.replace(/&lt;/g,"小于");
				cond = cond.replace(/&gt;/g,"大于");
				
		  		openIFDialog(window,'../cond/cond.html?load=<cond_type>update</cond_type><module>basic</module><cond_desc>'+cond_desc+'</cond_desc><cond_list>'+cond_list+'</cond_list><cond>'+cond+'</cond>','dialogWidth:520px;dialogHeight:500px');
		  		cond = cond.replace(/小于/g,"&lt;");
		  		cond = cond.replace(/大于/g,"&gt;");
		  		
		  	//}
	  	}
	  }
	  
	  //添加新条件后判断按钮的显示
	  function getPurviewAfterInsertCond(){
	  		getPurview(); 
	  }
	  
	  //保存条件 输入条件名称
	  function saveCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
	  		openIFDialog(window,'../../../condName.html','dialogWidth:400px;dialogHeight:200px');
	  	}
	  }
	  //保存条件
	  function saveCond1(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
  			//var cond_title = window.prompt("请输入输入常用条件名称！！！","");
  			if(cond_title==null || cond_title.length==0){
  				alert("条件名称为空！！！");
  				return false;
  			}else{
	  			//getUserID() 条件名称cond_title 条件说明cond_desc 条件列表cond_list 条件内容cond
	  			
	  			//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  
				cond = cond.replace(/</g,"&lt;");
				cond = cond.replace(/>/g,"&gt;");
				
	  			window.xmlhttp.post('hr_empDocumentBasicInfoMain_insertCond', "<user_id>"+getUserID()+"</user_id><cond_title>"+cond_title+"</cond_title><cond_desc>"+cond_desc+"</cond_desc><cond_list>"+cond_list+"</cond_list><cond>"+cond+"</cond>");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空职工列表
				result.innerHTML = "";
				//判断职工功能按钮的权限
	        	getPurview();  
	  			//刷新常用条件
	  			hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  		}
	  	}
	  }
	  
	  //删除条件
	  function deleteCond(){
	  	if(commonCondListCheckFlag==-1){
	  		alert("请选择一个常用条件！！！");
	  		return false;
	  	}else{
	  		if(confirm('确定删除该常用条件吗？')){
		  		var cond_id = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[2].firstChild.nodeValue;
		  		window.xmlhttp.post('hr_empDocumentBasicInfoMain_deleteCond', "<cond_id>"+cond_id+"</cond_id>");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
			  	commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空职工列表
				result.innerHTML = "";
				//判断职工功能按钮的权限
	        	getPurview();  
	  			//刷新常用条件
	  			hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  		}else{
	  			return false;
	  		}
	  	}
	  }
	  
	  
	  function IsExistsFile(filepath)
      {
          var xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          xmlhttp.open("GET",filepath,false);
          xmlhttp.send();
          if(xmlhttp.readyState==4){   
              if(xmlhttp.status==200) openDialog(window.prefix+filepath,'dialogWidth:800px;dialogHeight:600px',null,'true');  //url存在   
              else if(xmlhttp.status==404) alert("该照片已经丢失!请重新上传"); //url不存在   
              else return alert("该照片已经丢失!请重新上传");//其他状态   
          } 
      }
	
	  function condSelect2(){
		  	flag = "2";
		  	unitTree.style.display='none';
		  	condTable.style.display='none';
		  	condTable2.style.display='';
		  	
		  	cond_item_list.style.display="";
		  	user_id.value = getUserID();
		  	
		  	commonCondSelectByUserid.value = getUserID();
		  	treeTable.style.display="none";
	        //// 清空职工列表
	      findEmpByTreeOrCond = "cond2";
	         result.innerHTML = "";
		  		
	 }
	  function clickCond(){
	  	 
	  	  cond_item = cond_item_list.value;
				 cond_rep = cond_rep_list.value;
				 condvalue = cond_value.value; 
			  
	  }
	  function importData(){
				openDialog("import.html", 'dialogWidth:900px;dialogHeight:600px;');
		}
		function exportData(){
			var vXml = new ActiveXObject("Microsoft.XMLDOM");
			vXml.async=false;
			vXml.loadXML(result.serverXml);
			len = vXml.selectNodes("//tbody/tr").length;
			if(len<=0){
				alert("无结果集需要导出！");
				return ;
			}
			if(flag == "1") {
			 editTable.submit(hr_empDocumentBasicInfoMain_select);
			}
			if(flag == "2") {
			 condTable2.submit(hr_empDocumentBasicInfoMain_findEmpByCommonCond3);
			}
			if(flag == "3") {
			 editTable1.submit(hr_empDocumentBasicInfoMain_findEmpByCommonCond);
			}
		 
			var al=getEnvionmentValue("gActivexLibObj");
			if(al==null){
				alert("请安装望海组件");
				return false;
			}
			var data=new Object();
			//data["thead_1_1"]="三栏明细账";
			printXmlToCellByXsltFile(window.xmlhttp._object.responseText,"printView.xsl",data,"exportExcel",false);	
			
		}
			function importData2(){
				openDialog("part_imort.html", 'dialogWidth:900px;dialogHeight:600px;');
		}