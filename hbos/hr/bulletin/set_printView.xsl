<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      
	  		<tr noWrap="true" class="mainHead">
					<td nowrap='true'>标题</td>
	        <td nowrap='true'>公告类别</td>
			  	<td nowrap='true'>制单日期</td>
			  	<td nowrap='true'>开始日期</td>
			  	<td nowrap='true'>结束日期</td>
			  	<td nowrap='true'>用户权限</td>
			  	<td nowrap='true'>操作人</td>
			</tr>
  		</thead>
	  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr">
	  			<tr>
					<xsl:for-each select="td">
	              		<td nowrap='true'><xsl:value-of select="."/></td>
					</xsl:for-each>
					</tr>
				</xsl:for-each>
	  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>