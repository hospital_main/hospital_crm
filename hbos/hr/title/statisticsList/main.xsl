<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>职业分类</th>
				<th nowrap='true'>正高</th> 
				<th nowrap='true'>副高</th>
				<th nowrap='true'>中职</th> 
				<th nowrap='true'>初职</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td[position()!=last()]">
						<xsl:choose>
							<xsl:when test='position()=1 and .="合计"'>
								<td align='left' style='font-weight:800;'><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test='position()=1'>
								<td align='left'><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise>
	          			<td align='right'><xsl:value-of select="."/></td>
              </xsl:otherwise>
						</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

