<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
			<xsl:for-each select="/root/t2head/tr[position() &lt; 4]">	
				<xsl:variable name="curRow" select="position()" />				
				<tr noWrap='true' class='mainHead'>
          <xsl:for-each select="th[position()>1]">
          	<xsl:variable name="curcode" select="."/>
          	<xsl:variable name="curCol" select="position()" />	
          	<xsl:variable name="colspan" select="count(/root/t2head/tr[$curRow]/th[.=$curcode])" />
          	<xsl:variable name="rowspan" select="count(/root/t2head/tr[th[$curCol + 1] = $curcode])" />
          	<xsl:choose>		           
          		  <xsl:when test="$curRow=1 and .!=../th[$curCol]">
		          		<th colspan='{$colspan}' rowspan='{$rowspan}' nowrap='true' ><xsl:value-of select="."/></th>
		          	</xsl:when>       			
          			<xsl:when test=".!=../../tr[$curRow - 1]/th[$curCol + 1] and .!=../th[$curCol]">
		          			<th colspan='{$colspan}' rowspan='{$rowspan}' nowrap='true' ><xsl:value-of select="."/></th>
		          		</xsl:when>         		         	
	          		<xsl:otherwise>
              	</xsl:otherwise>
            </xsl:choose>       
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
		</thead>
	<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td[position()>1]">
							<td>
									<xsl:value-of select="."/>
							</td>						
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
	</xsl:template>
</xsl:stylesheet>

