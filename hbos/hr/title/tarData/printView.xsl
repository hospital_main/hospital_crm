<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		  	<root>
		<thead>
			<xsl:for-each select="/root/tbody/tr[td[1]='1']">	
				<xsl:variable name="curRow" select="position()" />				
				<tr noWrap='true' class='mainHead'>
          <xsl:for-each select="td[position()>2]">
          	<xsl:variable name="curcode" select="."/>
          	<xsl:variable name="curCol" select="position()" />	
          	<xsl:variable name="colspan" select="count(/root/tbody/tr[ $curRow + 1 ]/td[.=$curcode])" />
          	<xsl:variable name="rowspan" select="count(/root/tbody/tr[ td[$curCol + 2] = $curcode])" />
          	<xsl:choose>		           
        		  <xsl:when test="$curRow =1">
        		  	<xsl:if test="$curCol = 1">
        		  		<td valign="middle" >
	          				<xsl:attribute name="colspan" >
	          					<xsl:value-of select="$colspan"/>
			            	</xsl:attribute>
			            	<xsl:attribute name="rowspan" >
	          					<xsl:value-of select="$rowspan"/>
			            	</xsl:attribute>
			            	<xsl:value-of select="."/>
	          			</td>
        		  	</xsl:if>
        		  	<xsl:if test=" $curCol > 1">
          		  	<xsl:if test=" ../td[ $curCol + 2 ]!= ../td[$curCol + 1 ]">
		          			<td valign="middle" >
		          				<xsl:attribute name="colspan" >
		          					<xsl:value-of select="$colspan"/>
				            	</xsl:attribute>
				            	<xsl:attribute name="rowspan" >
		          					<xsl:value-of select="$rowspan"/>
				            	</xsl:attribute>
				            	<xsl:value-of select="."/>
		          			</td>
			          	</xsl:if>
			          	<xsl:if test=" ../td[ $curCol + 2 ] = ../td[$curCol + 1 ]">
		          			<td style="display:none" >
				            	<xsl:value-of select="."/>
		          			</td>
			          	</xsl:if>
			          </xsl:if>
	          	</xsl:when>
	          	<xsl:when test="$curRow > 1">
	          		<xsl:if test=" $curCol = 1">
	          			<td valign="middle" >
	          				<xsl:attribute name="colspan" >
	          					<xsl:value-of select="$colspan"/>
			            	</xsl:attribute>
			            	<xsl:if test=" ../td[$curCol + 2 ]= ../../tr[$curRow  ]/td[$curCol +2 ] ">
				            	<xsl:attribute name="style" >display:none</xsl:attribute>
			            	</xsl:if>
			            	<xsl:if test=" ../td[$curCol + 2 ]!= ../../tr[$curRow ]/td[$curCol + 2 ] ">
				            	<xsl:attribute name="rowspan" >
		          					<xsl:value-of select="$rowspan"/>
				            	</xsl:attribute>
			            	</xsl:if>
			            	<xsl:value-of select="."/>
	          			</td>
		          	</xsl:if>
		          	<xsl:if test=" $curCol > 1">
          		  	<xsl:if test=" ../td[$curCol + 2 ]!= ../td[ $curCol + 1 ]">
		          			<td valign="middle" >
		          				<xsl:attribute name="colspan" >
		          					<xsl:value-of select="$colspan"/>
				            	</xsl:attribute>
				            	<xsl:if test=" ../td[$curCol + 2 ]= ../../tr[$curRow  ]/td[$curCol + 2 ] ">
					            	<xsl:attribute name="style" >display:none</xsl:attribute>
				            	</xsl:if>
				            	<xsl:if test=" ../td[$curCol + 2 ]!= ../../tr[$curRow ]/td[$curCol + 2 ] ">
					            	<xsl:attribute name="rowspan" >
			          					<xsl:value-of select="$rowspan"/>
					            	</xsl:attribute>
				            	</xsl:if>
				            	<xsl:value-of select="."/>
		          			</td>
			          	</xsl:if>
			          	<xsl:if test=" ../td[$curCol + 2 ] = ../td[$curCol + 1 ]">
		          			<td style="display:none" >
				            	<xsl:value-of select="."/>
		          			</td>
			          	</xsl:if>
		          	</xsl:if>
	          	</xsl:when>
            </xsl:choose>       
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
		</thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr[td[1]='9']">
				<tr>
					<xsl:for-each select="td[position() &lt; 3 ]">
					</xsl:for-each>
					<xsl:for-each select="td[position()> 2  and position() &lt; 7 ]">
						<td><xsl:value-of select="."/></td>
					</xsl:for-each>
					<xsl:for-each select="td[position() > 6  ]">
						<!--
						<xsl:variable name="curCol2" select="position()" />
						<xsl:choose>
	            <xsl:when test="starts-with(/root/tbody/tr[1]/td[$curCol2 + 6 ],'1')">
			    			<td tdtype="T">
			    				<input type='text' class='inputDecimal' max='999999' min='0' name="columntext" >
			    					<xsl:attribute name='value'>
			    					</xsl:attribute>
			    					<xsl:attribute name='onblur'>
			    						checkInput(this)
			    					</xsl:attribute>
			    				</input>
		            </td>
			    		</xsl:when>
			    		 <xsl:when test="starts-with(/root/tbody/tr[1]/td[ $curCol2 + 6 ],'2')">
			    			<td>
			    				<input type='text' class='inputSelect' load='hr_dict_empDocument_contractinfo_tablelist' onchange="changeSelect(this)">
			    					<xsl:attribute name='name' >columndict_<xsl:value-of select='$curCol2'/></xsl:attribute>
			    					<xsl:attribute name='para'>&lt;a&gt;<xsl:value-of select="substring-before(substring-after(/root/tbody/tr[1]/td[ $curCol2 + 6 ],':'),':') "/>&lt;/a&gt;</xsl:attribute>
			    					<xsl:attribute name="emp_code"><xsl:value-of select="../td[3]"/></xsl:attribute>
										<xsl:attribute name="item_code"><xsl:value-of select="substring-after(substring-after(/root/tbody/tr[1]/td[ $curCol2 + 6 ],':'),':') " /></xsl:attribute>
			    					<xsl:attribute name='initValue'><xsl:value-of select="."/></xsl:attribute>
			    				</input>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td>
			          		<xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>		                        
	          </xsl:choose>-->
	          <td><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
      	</root>
  </xsl:template>
</xsl:stylesheet>

