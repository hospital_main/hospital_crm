<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<!--<th style="display:none">
					<input type="radiobox"/>
				</th>-->
				<th>选择</th>
				<th>职工编码</th>
				<th>职工姓名</th>
				<th>科室</th>
				<th>人员类别</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<!--<td align="center" style="display:none">
						<input type="radio" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>-->
					<td align='center'>
	  	  			<input type='radio' name='select_empId'>
	              <xsl:attribute name="value" ><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
	      			</input>
				   </td>
					<xsl:for-each select="td">
						<td><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
