<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>������Ϣ</th> 
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				 <xsl:for-each select="td"> 
					<td noWrap='true'>
					        <xsl:if test="position()=1">
					          <a>
						   <xsl:attribute name="href">#</xsl:attribute>  
						   <xsl:attribute name="onClick">
					             getSubInfoList('<xsl:value-of select="../pk"/>')
					           </xsl:attribute>
					           <xsl:value-of select="."/>
					          </a>
					        </xsl:if>  	
					</td>
			    </xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

