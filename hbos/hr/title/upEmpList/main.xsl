<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		<xsl:for-each select="/root/t2head/tr">
			<tr noWrap="true" class="mainHead">
				<xsl:for-each select="th">
					<th noWrap="true">
						<xsl:value-of select="." />
					</th>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</thead>
    
    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
		    <tr>
			    <xsl:for-each select="td"> 
					<td noWrap='true'>
						<xsl:if test="position()=2">
					          <a>
						   <xsl:attribute name="href">#</xsl:attribute>  
						   <xsl:attribute name="onClick">
					             updateSet1('<xsl:value-of select="../pk/empid"/>')
					           </xsl:attribute>
					           <xsl:value-of select="."/>
					          </a>
					     </xsl:if>  
						<xsl:if test="contains(.,'/upload/') and (contains(.,'jpg') or contains(.,'bmp'))">
							<a>
							<xsl:attribute name="href">#</xsl:attribute>
							<xsl:attribute name="onclick">IsExistsFile("<xsl:value-of select="."/>")</xsl:attribute>
							�鿴	</a>
						</xsl:if>
						<xsl:if test="not(contains(.,'/upload/') and (contains(.,'jpg') or contains(.,'bmp')))">
							<xsl:if test="position()!=2">
								<xsl:value-of select="."/>
							</xsl:if>
						</xsl:if>
					</td>
			    </xsl:for-each>
		    </tr>
	    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

