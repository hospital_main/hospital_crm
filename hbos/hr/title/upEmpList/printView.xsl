<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
   <root>
    <xsl:variable name="colNum" select="count(/root/t2head/tr[1]/th)"/>
    	<thead>
    		<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum - 1"/>    
	        </xsl:call-template>
	  		</tr>	
	 		<tr noWrap="true" class="mainHead">
	 			<xsl:for-each select="/root/t2head/tr">
					<xsl:for-each select="th">
	              		<td nowrap='true'><xsl:value-of select="."/></td>
					</xsl:for-each>
				</xsl:for-each>
	 		</tr>
   		</thead>
   
	  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr">
	  			<tr>
					<xsl:for-each select="td">	         
	          <xsl:choose>
							<xsl:when test="contains(.,'/upload/') and (contains(.,'jpg') or contains(.,'bmp'))">
							<td align='left'>�鿴</td>								
						</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
					</tr>
			</xsl:for-each>
	  	</tbody>
    </root>
  </xsl:template>
  <xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>