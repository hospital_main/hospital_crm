<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<td noWrap="true">选择</td>
				<td nowrap='true'>试题编码</td>
				<td nowrap='true'>试题内容</td> 
				<td nowrap='true'>项目序号</td> 
				<td nowrap='true'>项目名称</td> 
				<td nowrap='true'>答案内容</td>
				<td nowrap='true' style="display:none;"></td>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'  style='display:none'>
		            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
		              <xsl:attribute name="value" >
		                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		              </xsl:attribute>
		            </input>
		        </td>
				<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
									<a href='#' >
									<xsl:attribute name="onclick">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
									</xsl:attribute><xsl:value-of select="."/>
								</a>
								 </td>
		          </xsl:when>
		          <xsl:when test="position()=2 or position()=5">
								<td width="450" style="word-wrap:break-word;word-break:break-all;">
									<p><xsl:value-of select="."/></p>
								 </td>
		          </xsl:when>
		          <xsl:when test="position()=7">
								<td style="display:none;">
									<xsl:value-of select="."/>
								 </td>
		          </xsl:when>
		          <xsl:otherwise>
								<td>
				                	 <xsl:value-of select="."/>
						  		</td>
						</xsl:otherwise>
		            </xsl:choose>
		          </xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

