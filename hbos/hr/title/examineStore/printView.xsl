 <?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td)"/>
  	<root>
    	<thead>
	    	 <tr noWrap="true" >
				<td noWrap="true" >
					<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
				</td>
				<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
					<td noWrap="true" style="display:none"/>
				</xsl:for-each>
			</tr>
			<tr noWrap="true" >
				<td noWrap="true" align='left' >
					<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
				</td>
				<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
					<td noWrap="true" style="display:none"/>
				</xsl:for-each>
			</tr>
			<tr noWrap="true" >
				<td noWrap="true" align='left' >
					<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
				</td>
				<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
					<td noWrap="true" style="display:none"/>
				</xsl:for-each>
			</tr>
	  		<tr noWrap="true" class="mainHead">
					<td nowrap='true'>试题编码</td>
					<td nowrap='true'>试题内容</td> 
					<td nowrap='true'>项目序号</td> 
					<td nowrap='true'>项目名称</td> 
					<td nowrap='true'>答案内容</td>
	  		</tr>
	  	</thead>
	  	<tbody>
		   	<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td align="left">
								<xsl:value-of select="."/>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
	  	</tbody>
 		</root>
	</xsl:template>

</xsl:stylesheet>