<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:template match="/">
		<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)"/>
		<root>
			<thead>
				<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum - 1"/>
	        </xsl:call-template>
	  		</tr>				
				<tr noWrap='true' class='mainHead'>
				  	<td noWrap='true'>职工编码</td>
				  	<td noWrap='true'>职工姓名</td>
				  	<td noWrap='true'>申报类别</td>
				  	<td noWrap='true'>性别</td>
				  	<td noWrap='true'>履职年限</td>
				  	<td noWrap='true'>累计年限</td>
				  	<td noWrap='true'>聘任职称</td>
				  	<td noWrap='true'>学历</td>
				  	<td noWrap='true'>申报资格</td>
				  	<td noWrap='true'>申报专业</td>
				  	<td noWrap='true'>投票人数</td>
				  	<td noWrap='true'>同意票数</td>
				  	<td noWrap='true'>不同意票数</td>
				  	<td noWrap='true'>是否通过</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td>
									<xsl:value-of select="."/>
							</td>						
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
