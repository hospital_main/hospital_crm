<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<xsl:for-each select="/root/t2head/tr[1]/td">
					<th nowrap='true'>
						<xsl:if test="contains( . ,'��')">
							<xsl:value-of select="substring-before(. ,'��')"/>
						</xsl:if>
						<xsl:if test="not(contains( . ,'��'))">
							<xsl:value-of select="."/>
						</xsl:if>
					</th>
				</xsl:for-each>
			</tr>
		</thead>                 
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr onclick='getPkOfSub(this);'>
					<xsl:for-each select="td"> 
						<xsl:variable name="c" select="position()"/>
						<xsl:if test="string-length($c)&gt;0">
							<xsl:if test="contains( /root/t2head/tr[1]/td[$c] ,'��')">
								<td align="right">
									<xsl:if test=".!=0">
										<xsl:value-of select="format-number(.,substring-after(/root/t2head/tr[1]/td[$c] ,'��'))"/>
									</xsl:if>
								</td>
							</xsl:if>
							<xsl:if test="not(contains( /root/t2head/tr[1]/td[$c] ,'��'))">
								<td align="left">
								<xsl:if test="position() = last()">
									<xsl:attribute name="style">display:none</xsl:attribute>
								</xsl:if>
								<xsl:if test="contains(.,'/upload/') and contains(.,'jpg')">
									<a>
									<xsl:attribute name="href">javascript:openDialog(window.prefix+'<xsl:value-of select="."/>','dialogWidth:800px;dialogHeight:600px',null,'true');</xsl:attribute>
									�鿴	</a>
								</xsl:if>
								<xsl:if test="not(contains(.,'/upload/') and contains(.,'jpg'))">
									<xsl:value-of select="."/>
								</xsl:if>
								</td>
							</xsl:if>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>