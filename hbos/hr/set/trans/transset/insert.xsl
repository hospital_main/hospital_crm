<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<xsl:for-each select="/root/tbody/tr[position()=1]">
	        <th></th>
	        <xsl:for-each select="td"> 
		        <th nowrap='true'>
	              <xsl:choose>
		          	<xsl:when test="position()=3">
						<xsl:attribute name="style">
							display:none
						</xsl:attribute>
		          	</xsl:when>
		          	<xsl:otherwise>
		              <xsl:value-of select="."/>  
		          	</xsl:otherwise>
		          </xsl:choose>
		        </th>	    
		     </xsl:for-each>
		 </xsl:for-each>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position()!=1]">
        <tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="judgeCheckinsert();">
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
                <xsl:if test="td[1]=td[3]">
				  <xsl:attribute name="checked">true</xsl:attribute>
         	    </xsl:if>
            </input>
          </td>
          <xsl:for-each select="td"> 
            <td>
              <xsl:choose>
	          	<xsl:when test="position()=3">
					<xsl:attribute name="style">
						display:none
					</xsl:attribute>
	          	</xsl:when>
	          	<xsl:otherwise>
	              <xsl:value-of select="."/>  
	          	</xsl:otherwise>
	          </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

