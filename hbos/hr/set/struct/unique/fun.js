function submitData(obj){
  var res=checkInputs('result_mainDataTable');
  //alert(res);
  if(res==null||res=="")
    return false;

  window.xmlhttp.post(obj.name, res,"")
  var str = window.xmlhttp._object.responseText
  if (window.doMsg(str,"")) {
    return true;
  }
}

function checkInputs(tableId){
  if(typeof(tableId)=="undefined")
    tableId="_mainDataTable";
  var table=document.getElementById(tableId);
  var trs=table.getElementsByTagName("tr");
  var res="";
  var temp = "";
  var issamedes = new Array();
  for(var i=1;i<trs.length;i++){
    //if(trs[i].getAttribute("_groupnumber")>0){
      var obj = new Object();
      //alert("当前groupnumber值:"+trs[i].getAttribute("_groupnumber")+";说明："+trs[i].getAttribute("_descript")+";数组长度："+issamedes.length);
        if(trs[i].getAttribute("_groupnumber") > 0 && trs[i].getAttribute("_descript") == ''){
          alert("分组号存在，说明也必须存在");
          return "";
        }
        if(trs[i].getAttribute("_groupnumber") == '' && trs[i].getAttribute("_descript") != ''){
          alert("说明存在，分组号也必须存在");
          return "";
        }
      for(var j = 0;j < issamedes.length;j++){
        //alert("数组groupnumber"+j+"值:"+issamedes[j].groupnumber+";说明："+issamedes[j].descript);
        //alert("groupnumber:"+issamedes[j].groupnumber);
        //alert("descript:"+issamedes[j].descript);
        if(issamedes[j].groupnumber == trs[i].getAttribute("_groupnumber")){
          if(issamedes[j].descript != trs[i].getAttribute("_descript")){
            alert("分组号相同，说明必须相同，请重新输入");
            return "";
          }
        }
        if(issamedes[j].descript == trs[i].getAttribute("_descript")){
          if(issamedes[j].groupnumber != trs[i].getAttribute("_groupnumber")){
            alert("说明相同，分组号必须相同，请重新输入");
            return "";
          }
        }
      }
      if(trs[i].getAttribute("_groupnumber") > 0 && trs[i].getAttribute("_descript") != ''){
        obj.groupnumber = trs[i].getAttribute("_groupnumber");
        obj.descript = trs[i].getAttribute("_descript");
        issamedes.push(obj);
      }
      
      res+="<record>";
      res+="<tableid>"+document.getElementById("tableid").value+"</tableid>"
      res+="<fieldid>"+trs[i].getAttribute("_fieldid")+"</fieldid>"
      res+="<groupnumber>"+trs[i].getAttribute("_groupnumber")+"</groupnumber>";
      res+="<descript>"+escape(trs[i].getAttribute("_descript"))+"</descript>";
      res+="</record>";
    //}
  }
  return res;
}

function initInputs(tableId){
  if(typeof(tableId)=="undefined")
    tableId="_mainDataTable";
  var table=document.getElementById(tableId);
  var trs=table.getElementsByTagName("tr");
  for(var i=0;i<trs.length;i++){
    //alert(trs[i]);
    initTrInputs(trs[i],i);
  }
    //alert(table.outerHTML);
}

function textGroupnumber(obj){
  if(isNaN(obj.value)){
    alert("分组号应为数字");
    obj.value = "";
    //return "";
  }else{
    obj.parentNode.parentNode._groupnumber = obj.value;
  }
}

function textDescript(obj){
  obj.parentNode.parentNode._descript = obj.value;
}

function initTrInputs(tr,i){
  var inp="";
  var tp = tr.cells[2].innerHTML;
  //alert(tr.cells[1].innerHTML);
  var groupnumberFlag = "groupnumber" + i;
  var descriptFlag = "descript" + i;
  if(tp!='分组号'){
    tr.cells[2].innerHTML ="<input type='text' id='"+groupnumberFlag+"' name='"+groupnumberFlag+"' class='inputInteger' maxlength='20' value='"+tp+"' onblur='textGroupnumber(this);'/>";
  }
  var temp = tr.cells[3].innerHTML;
  if(temp!='说明'){
    tr.cells[3].innerHTML ="<input type='text' id='"+descriptFlag+"' name='"+descriptFlag+"' class='inputTextA' maxlength='1000' value='"+temp+"' onChange='textDescript(this);'/>";
  }
  var sysFlag = tr.cells[1].innerHTML;
  //alert(glotableid);
  if(glotableid == 'sys_emp'){
    if(sysFlag == 'comp_code' || sysFlag == 'dept_name' || sysFlag == 'emp_code' || sysFlag == 'emp_name'){
    //alert(11);
    var oGroupnumber = eval("("+groupnumberFlag+")");
    oGroupnumber.disabled = true;
    var oDescript = eval("("+descriptFlag+")");
    oDescript.disabled = true;
    //descript.disabled = true;
  }
  }else{
    if(sysFlag == 'comp_code' || sysFlag == 'dept_code' || sysFlag == 'dept_name'){
    //alert(11);
    var oGroupnumber = eval("("+groupnumberFlag+")");
    oGroupnumber.disabled = true;
    var oDescript = eval("("+descriptFlag+")");
    oDescript.disabled = true;
    //descript.disabled = true;
  }
  }
  /*
  if(sysFlag == 'comp_code' || sysFlag == 'dept_code' || sysFlag == 'dept_name' || sysFlag == 'emp_code' || sysFlag == 'emp_name'){
    //alert(11);
    var oGroupnumber = eval("("+groupnumberFlag+")");
    oGroupnumber.disabled = true;
    var oDescript = eval("("+descriptFlag+")");
    oDescript.disabled = true;
    //descript.disabled = true;
  }
  */
}
