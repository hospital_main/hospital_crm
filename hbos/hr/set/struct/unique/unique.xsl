<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true' width="50">名称</th>
      	<th nowrap='true' width="50">标志</th>
      	<th nowrap='true' width="50">分组号</th>
      	<th nowrap='true' width="50">说明</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:attribute name="_change">0</xsl:attribute>
          <xsl:attribute name="_fieldid"><xsl:value-of select="td[2]"/></xsl:attribute>
          <xsl:attribute name="_groupnumber"><xsl:value-of select="td[3]"/></xsl:attribute>
          <xsl:attribute name="_descript"><xsl:value-of select="td[4]"/></xsl:attribute>
          <xsl:for-each select="td[position()&lt;6]">   
            <td>
              <xsl:value-of select="."/>  
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

