<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='nodrop nodrag mainHead' id="id_0">
        <th style='display:none'><input type='checkbox' value=''/></th>
      	<th nowrap='true'>名称</th>
      	<th nowrap='true'>标志</th>
      	<th nowrap='true'>类型</th>
      	<th nowrap='true'>长度</th>
      	<th nowrap='true'>小数位</th>
      	<th nowrap='true'>类别</th>
      	<th nowrap='true'>代码表名称</th>
      	<th nowrap='true'>代码表标志</th>
      	<th nowrap='true'>是否必填</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:attribute name="id">id_<xsl:value-of select="position()"/></xsl:attribute>
          <td align='center'  style='display:none;cursor:auto;'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
			<td>
	          <xsl:choose>
	          	<xsl:when test="position()=2">
						<a tabindex='-1'><xsl:value-of select="."/></a>
	          	</xsl:when>
	          	<xsl:otherwise>
		              <xsl:value-of select="."/>
	          	</xsl:otherwise>
	          </xsl:choose>
			</td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

