<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox' value=''/></th>
      	<th nowrap='true'>名称</th>
      	<th nowrap='true'>数据级别</th>
      	<th nowrap='true'>查询权限</th>
      	<th nowrap='true'>增加权限</th>
      	<th nowrap='true'>删除权限</th>
      	<th nowrap='true'>修改权限</th>
      	<th nowrap='true'>处理范围</th>
      	<th nowrap='true'>范围用表</th>
      	<th nowrap='true'>范围条件</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:variable name="pos"><xsl:value-of select="position()"></xsl:value-of></xsl:variable>
          <xsl:for-each select="td">
			<td>
	          <xsl:choose>
	          	<xsl:when test="position()=2 or position()=1">
						<xsl:value-of select="."/>
	          	</xsl:when>
	          	<xsl:when test="position()=10">
						<xsl:attribute name="style">
							display:none
						</xsl:attribute>
	          	</xsl:when>
	          	<xsl:otherwise>
		          	<xsl:if test="../td[10]=2 and position() &lt;7">
		          		<input type="checkbox" onclick="clickedcbox(this)">
			          		<xsl:if test=".=1 or .=2">
											<xsl:attribute name="checked">true</xsl:attribute>
			          		</xsl:if>
			          		<xsl:attribute name="value">
			          			<xsl:if test="position()=3">s</xsl:if>
			          			<xsl:if test="position()=4">a</xsl:if>
			          			<xsl:if test="position()=5">d</xsl:if>
			          			<xsl:if test="position()=6">u</xsl:if>
			          		</xsl:attribute>
			          		<xsl:attribute name="id">
			          			<xsl:if test="position()=3">s<xsl:value-of select="$pos"></xsl:value-of></xsl:if>
			          			<xsl:if test="position()=4">a<xsl:value-of select="$pos"></xsl:value-of></xsl:if>
			          			<xsl:if test="position()=5">d<xsl:value-of select="$pos"></xsl:value-of></xsl:if>
			          			<xsl:if test="position()=6">u<xsl:value-of select="$pos"></xsl:value-of></xsl:if>
			          		</xsl:attribute>
		          			</input>
			        </xsl:if>
			        
			        <xsl:if test="../td[10]=3 and position() &lt;7">
			        	<xsl:if test="position()=3 or position()=6">
			          		<input type="checkbox">
				          		<xsl:if test=".=1 or .=2">
				          			<xsl:attribute name="checked">true</xsl:attribute>
				          		</xsl:if>
				          		<xsl:attribute name="value">
				          			<xsl:if test="position()=3">s</xsl:if>
				          			<xsl:if test="position()=6">u</xsl:if>
				          		</xsl:attribute>
				          		<xsl:attribute name="id">
				          			<xsl:if test="position()=3">s<xsl:value-of select="$pos"></xsl:value-of></xsl:if>
				          			<xsl:if test="position()=6">u<xsl:value-of select="$pos"></xsl:value-of></xsl:if>
				          		</xsl:attribute>
			          		</input>
			        	</xsl:if>
			        </xsl:if>
					<xsl:if test="position()=7 or position()=8 or position()=9">
						<xsl:value-of select="."/>
			        </xsl:if>
	          	</xsl:otherwise>
	          </xsl:choose>
			</td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

