<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
   <root>
    
    	<thead>
    	
       	<tr noWrap="true" class="mainHead">
       		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
       		<xsl:for-each select="/root/t2head/tr[1]/th[position() &gt; 1]">		
		             <td style="display:none"/>
					</xsl:for-each>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<xsl:for-each select="/root/t2head/tr[1]/th[position() &gt; 1]">		
		             <td style="display:none"/>
					</xsl:for-each>
      	</tr>
	  
	
	 		<tr noWrap="true" class="mainHead">
	 			<xsl:for-each select="/root/t2head/tr">
					<xsl:for-each select="th">
	              		<td nowrap='true'><xsl:value-of select="."/></td>
					</xsl:for-each>
				</xsl:for-each>
	 		</tr>
   		</thead>
   
	  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr">
	  			<tr>
					<xsl:for-each select="td">	         
	          <xsl:choose>
							<xsl:when test="contains(.,'/upload/') and (contains(.,'jpg') or contains(.,'bmp'))">
							<td align='left'>�鿴</td>								
						</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
					</tr>
			</xsl:for-each>
	  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>