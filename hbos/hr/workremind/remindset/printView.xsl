<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap='true' class='mainHead'>
					<td nowrap='true'>序号</td>      	
	      	<td nowrap='true'>提醒名称</td>
	      	<td nowrap='true'>提醒类别</td>
	      	<td nowrap='true'>提醒项目</td>      	
	      	<td nowrap='true'>方案</td>
	      	<td nowrap='true'>用户权限</td>
	      	<td nowrap='true'>是否自动</td>
	      	<td nowrap='true'>操作人</td>
				</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
        <tr>
       	<xsl:variable name="y" select="position()"/>
          <xsl:for-each select="td"> 
            <td>
	          <xsl:choose>	          	
	          	<xsl:when test="position()=1">
									<xsl:value-of select="$y"/>
	          	</xsl:when>	          	
	          	<xsl:otherwise>
		              <xsl:value-of select="."/>
	          	</xsl:otherwise>	          	
	          </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>