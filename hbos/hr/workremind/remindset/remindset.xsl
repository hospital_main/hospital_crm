<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th></th>
      	<th nowrap='true'>序号</th>      	
      	<th nowrap='true'>提醒名称</th>
      	<th nowrap='true'>提醒类别</th>
      	<th nowrap='true'>提醒项目</th>      	
      	<th nowrap='true'>方案</th>
      	<th nowrap='true'>用户权限</th>
      	<th nowrap='true'>是否自动</th>
      	<th nowrap='true'>操作人</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position()]">
        <tr>
       	<xsl:variable name="y" select="position()"/> 
       	<xsl:variable name="key" select="pk/alarm_id"/> 
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="judgeCheck()">
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td"> 
            <td>
	          <xsl:choose>	          	
	          	<xsl:when test="position()=1">
									<xsl:value-of select="$y"/>
	          	</xsl:when>	          	
	          	<xsl:when test="position()=2">
	          		<a href="#">
	          			<xsl:attribute name="onclick">javascrip:selectCondDesc(<xsl:value-of select="$key"/>)</xsl:attribute>
	          			<xsl:value-of select="."/>
	          		</a>
	          	</xsl:when>	          	
	          	<xsl:otherwise>
		              <xsl:value-of select="."/>
	          	</xsl:otherwise>	          	
	          </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

