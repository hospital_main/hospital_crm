<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>发票号</th>
				<th>日期</th>
				<th>采购员</th>
				<th>供货单位</th>
				<th>金额</th>
				<th>状态</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
				<tr>
       		<xsl:if test="td[1]!='合计'">
						<td align='center' style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
	    			  </input>
	          </td>
   			  </xsl:if>
         	<xsl:if test="td[1]='合计'">
         	  <td></td>
   			  </xsl:if>
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test="position()=1">
          		<xsl:if test="../td[1]!='合计'">
                <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
              </xsl:if>
          		<xsl:if test="../td[1]='合计'">
                <td><xsl:value-of select="."/></td>
              </xsl:if>
          		</xsl:when>
          		<xsl:when test="position()=5">
          			<td align="right"><xsl:value-of select="format-number(.,$VHMONEYFORMAT )"/></td>
          		</xsl:when>
          		<xsl:when test="position()=7">
          		</xsl:when>
	          	<xsl:otherwise>
	          		<td align="left">
	          			<xsl:value-of select="."/>
	          		</td>
	          	</xsl:otherwise>
	          </xsl:choose>
          </xsl:for-each>
        </tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>