<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <tr noWrap='true' class='mainHead'>
        <th width='25'><input type='checkbox'/></th>
        <th>入库单号</th>
				<th>仓库</th>
				<th>入库日期</th>
				<th>采购员</th>
				<th>应开票据金额</th>
				<th>已开票据金额</th>
				<th>本期发票金额</th>		
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=2">
                <td>
                  <a href='#'>
                    <xsl:attribute name="onclick">openDialog('dupdate.html','dialogWidth:320px;dialogHeight:320px', this);</xsl:attribute>
                    <xsl:value-of select="."/>
                  </a>
                </td>
              </xsl:when>
              <xsl:when test="position()=1">
              </xsl:when>
	            <xsl:when test="position()=6 or position()=7 or position()=8 ">
	              <td  align="right">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
      <xsl:if test="count(/root/total/td)!=0" >      
        <xsl:for-each select="/root/total">
        	<tr>
            <td align='center'></td>
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=2">
                  <td>合计：</td>
                </xsl:when>
                <xsl:when test="position()=1">
              </xsl:when>
                <xsl:when test="position()=8">	 
                  <td  align="right">
                    <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                  </td>
                </xsl:when>		            
                <xsl:otherwise>
                  <td></td>
                </xsl:otherwise>
              </xsl:choose>
      		  </xsl:for-each>
          </tr>
        </xsl:for-each>
      </xsl:if>    	
    </tbody>
  </xsl:template>
</xsl:stylesheet>


