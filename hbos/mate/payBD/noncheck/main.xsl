<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
      	<th>仓库</th>
      	<th>入库单号</th>
      	<th>入库日期</th>
      	<th>供货单位</th>
      	<th>采购员</th>
      	<th>应付金额</th>
      	<th>已开票金额</th>
      	<th>未开票金额</th>
      </tr>  		
    </thead>
    <tbody>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				    <xsl:choose>
				      <xsl:when test="position()=8 or position()=6 or position()=7">
				        <td align='right' class='moneyCol'>
				          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
				        </td>
				      </xsl:when>
				      <xsl:otherwise>
				        <td align='left'>
				          <xsl:value-of select="."/>
				        </td>
				      </xsl:otherwise>			                        
				    </xsl:choose>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

