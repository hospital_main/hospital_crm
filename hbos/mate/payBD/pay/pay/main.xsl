<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>付款单号</th>
				<th>日期</th>
				<th>采购员</th>
				<th>供货单位</th>				
				<th>状态</th>
				<th>单据类型</th>
  		</tr>
  	</thead>
  	<tbody/>
	</xsl:template>
</xsl:stylesheet>
