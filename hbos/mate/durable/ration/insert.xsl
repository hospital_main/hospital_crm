<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
		<thead>
  	  <tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th noWrap="true">科室名称(E)</th>
				<th noWrap="true">耐用品编码</th>
				<th noWrap="true">耐用品名称(E)</th>
				<th noWrap="true">规格型号</th>
				<th noWrap="true">计量单位</th>
				<th noWrap="true">生产厂商</th>
				<th noWrap="true">供应商</th>
				<th noWrap="true">定额数量(E)</th>
				<th style='display:none' noWrap="true">科室在用量(E)</th>
    	</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=8">
                <td align='right' class='moneyCol'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	              </td>
              </xsl:when>
              <xsl:when test="position()=9">
                <td align='right' class='moneyCol' style="display:none">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	              </td>
              </xsl:when>
              <xsl:when test="position()=10">
                <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
