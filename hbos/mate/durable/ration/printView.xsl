<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
    <thead>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
				<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
				<td style="display:none"/>
     		<td style="display:none"/>
      </tr>
		  
	  		<tr noWrap="true" class="mainHead">
	        <td>科室</td>
					<td>耐用品编码</td>
					<td>耐用品名称</td>
					<td>定额数量</td>
					<td>当前数量</td>
	  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	              <xsl:choose>
	                <xsl:when test="position()=4  ">
	                  <td class='numberText'>
  	                		<xsl:value-of select="format-number(.,'#,##0.00')"/>  
  	              	</td>
	                </xsl:when>
	                <xsl:when test="position()=5">
	              	</xsl:when>
		              <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		              </xsl:otherwise>
	              </xsl:choose>
	          </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
	 </root> 	
	</xsl:template>
</xsl:stylesheet>