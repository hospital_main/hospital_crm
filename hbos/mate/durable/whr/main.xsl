<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/mate/durable/whr/main.xsl,v 1.1 2012/03/12 01:54:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:54:35 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th noWrap="true" >单据号</th>
				<th noWrap="true" >流水号</th>
				<th noWrap="true">日期</th>				
				<th noWrap="true">仓库</th>
				<th noWrap="true">科室</th>
				<th noWrap="true">摘要</th>
				<th noWrap="true">处理分类</th>
				<th noWrap="true">耐用品编码</th>
				<th noWrap="true">耐用品名称</th>
				<th noWrap="true">规格型号</th>
				<th noWrap="true">计量单位</th>				
				<th noWrap="true">数量</th>
				<th noWrap="true">经手人</th>
				<th noWrap="true">批准人</th>
				<th noWrap="true">库管员</th>
				<th noWrap="true">状态</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <td >
              <xsl:choose>
                <xsl:when test="position()=1">
                  <a tabindex='-1'>
                  <xsl:value-of select="."/>
                  <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
                  </a>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
