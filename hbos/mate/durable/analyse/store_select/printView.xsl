<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN =' '/>
	<xsl:template match="/">
	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
	<root>
		<thead>
			<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">17</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr noWrap='true' class='mainHead'>
		    	  <td nowrap='true' colspan="5"></td>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td nowrap='true' colspan="5"></td>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td nowrap='true' colspan="7"></td>
		    	  <td nowrap='true'></td>
		    	  <td nowrap='true'></td>
		    	  <td nowrap='true'></td>
		    	  <td nowrap='true'></td>
		    	  <td nowrap='true'></td>
		    	  <td nowrap='true'></td>
		    	</tr>
			<tr noWrap='true' class='mainHead'>
		    	  <td nowrap='true' rowspan="2">物资编号</td>
		    	  <td nowrap='true' rowspan="2">物资名称</td>
		    	  <td nowrap='true' rowspan="2">规格型号</td>
		    	  <td nowrap='true' rowspan="2">计量单位</td>
		    	  <td nowrap='true' rowspan="2">批号</td>
		    	  <td nowrap='true' colspan="3">期初金额</td>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td nowrap='true' colspan="3">入库金额</td>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td nowrap='true' colspan="3">出库金额</td>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td nowrap='true' colspan="3">结存金额</td>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	</tr>
		    	<tr noWrap='true' class='mainHead'>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td style='display:none'/>
		    	  <td nowrap='true'>合计</td>
		    	  <td nowrap='true'>正常</td>
		    	  <td nowrap='true'>流转</td>
		    	  <td nowrap='true'>合计</td>
		    	  <td nowrap='true'>正常</td>
		    	  <td nowrap='true'>流转</td>
		    	  <td nowrap='true'>合计</td>
		    	  <td nowrap='true'>正常</td>
		    	  <td nowrap='true'>流转</td>
		    	  <td nowrap='true'>合计</td>
		    	  <td nowrap='true'>正常</td>
		    	  <td nowrap='true'>流转</td>
		    	</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
	        	<tr>
	          	<xsl:for-each select="td">
	            
		              <xsl:choose>
				<xsl:when test="position()>5">
			               <td align="right">
					  <xsl:value-of select="format-number(.,'#,##0.00')"/>
					</td>
		                </xsl:when>
		                <xsl:otherwise>
				<td>
		                  <xsl:value-of select="."/>
		                </td>
				</xsl:otherwise>
		              </xsl:choose>
	  		</xsl:for-each>
	  		</tr>
	   		</xsl:for-each> 
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>



