<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/mate/durable/analyse/store_select/main.xsl,v 1.1 2012/03/12 01:54:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:54:35 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th rowspan="2">物资编号</th>
				<th rowspan="2">物资名称</th>
				<th rowspan="2">规格型号</th>
				<th rowspan="2">计量单位</th>
				<th rowspan="2">批号</th>
				<th colspan="3">期初金额</th>
				<th colspan="3">入库金额</th>
				<th colspan="3">出库金额</th>
				<th colspan="3">结存金额</th>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<th>合计</th>
				<th>正常</th>
				<th>流转</th>
				<th>合计</th>
				<th>正常</th>
				<th>流转</th>
				<th>合计</th>
				<th>正常</th>
				<th>流转</th>
				<th>合计</th>
				<th>正常</th>
				<th>流转</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            
              <xsl:choose>
		<xsl:when test="position()>5">
	               <td align="right">
			  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			</td>
                </xsl:when>
                <xsl:otherwise>
		<td>
                  <xsl:value-of select="."/>
                </td>
		</xsl:otherwise>
              </xsl:choose>
  		</xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
