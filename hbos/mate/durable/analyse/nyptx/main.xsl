<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  	    <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<xsl:for-each select="/root/tbody/tr">
  			<xsl:choose>
	    		<xsl:when test=" position() = 1 ">
	    		  <tr noWrap='true' class='mainHead'> 
		          <xsl:for-each select="td">
  							<xsl:choose>
  							<xsl:when test=" position() = 3 ">
				          	       <th style="display:none" >
						            	<xsl:value-of select="."/>
				          			</th>  
					        </xsl:when> 
	          	            <xsl:otherwise>
	          		          <td><xsl:value-of select="."/></td>
                            </xsl:otherwise>
					      </xsl:choose>
		           </xsl:for-each>
		  		   </tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr[position() > 1]">
	    	<tr>
	    	  <xsl:for-each select="td">
	    	    <xsl:choose>
	    	    <xsl:when test=" position() = 1">
	    	      	<td> 
					  <xsl:value-of select="."/> 
	          		</td>
	          	</xsl:when>
	    	    <xsl:when test=" position() = 2">
	    	      	<td>
	    	      	<xsl:if test=" ../td[3] = 0 ">
	    	      		<img src='/images2/menu/folderopen.png' _exp='-' onclick='expand(this)'/>
	          		</xsl:if>
	          		<xsl:value-of select="."/>
	          		</td>
	          	</xsl:when>
	    	    <xsl:when test="position() = 3">
	    	      	<td style="display:none">�Ƿ�ĩ��</td>
	    	    </xsl:when> 
	          	<xsl:otherwise>
	          		<td align="right" noWrap="true">
                   <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                    </td>
              </xsl:otherwise>
	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>