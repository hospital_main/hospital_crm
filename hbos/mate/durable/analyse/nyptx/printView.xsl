<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  <xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td)-1"/> 
  	<root>
    	<thead>
    	 <tr noWrap="true" >
	      			<td noWrap="true" width="600">
	      				<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="count(/root/tbody/tr[1]/td[position()!=last()])"/></xsl:attribute>
	      			</td>
	      		<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;2]">
	      			<td noWrap="true" style="display:none"/>
				 </xsl:for-each>
	     </tr>
    	 <tr noWrap="true" >
	      			<td noWrap="true" width="600">
	      				<xsl:attribute name="style">fontsize:subtitle;colspan:<xsl:value-of select="count(/root/tbody/tr[1]/td[position()!=last()])"/></xsl:attribute>
	      			</td>
	      		  <xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;2]">
	      			<td noWrap="true" style="display:none"/>
				   </xsl:for-each>
	     </tr>
	     <xsl:for-each select="/root/tbody/tr[position()=1]">
  			<tr noWrap='true' class='mainHead'>
          <xsl:for-each select="td">
          	<xsl:choose>
	          	 <xsl:when test=" position() != 3 ">
	          		<td nowrap='true' width='200'><xsl:value-of select="."/></td>
	          	 </xsl:when> 
	          	 <xsl:otherwise> 
                 </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
    	</thead>
    	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	      <xsl:variable name="colName" select="position()"/>
	    	<tr>
	    	  <xsl:for-each select="td">
	    	    <xsl:choose> 
	    	      <xsl:when test=" position() = 3">
	          	  </xsl:when> 
	          	  <xsl:otherwise>
	          	    <xsl:if test=" position() = 1 ">
	    	      		<td><xsl:value-of select="."/></td>
	          		</xsl:if>
	          		<xsl:if test=" position() = 2 ">
	    	      		<td><xsl:value-of select="."/></td>
	          		</xsl:if>
	          		<xsl:if test="position() &gt; 3">
	          		    <td align="right" > 
	          		        <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	          		    </td>
	          		</xsl:if>
                 </xsl:otherwise>
	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
    	 
 		</root>
	</xsl:template>
		
</xsl:stylesheet>