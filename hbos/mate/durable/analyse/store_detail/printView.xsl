<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN =' '/>
	<xsl:template match="/">
	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
	<root>
		<thead>
			<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">14</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr noWrap='true' class='mainHead'>
	    	  <td nowrap='true' colspan="4"></td>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td nowrap='true' colspan="4"></td>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td nowrap='true' colspan="6"></td>
	    	  <td nowrap='true'></td>
	    	  <td nowrap='true'></td>
	    	  <td nowrap='true'></td>
	    	  <td nowrap='true'></td>
	    	  <td nowrap='true'></td>
	    	</tr>
				<tr noWrap='true' class='mainHead'>
	    	  <td nowrap='true'>仓库</td>
	    	  <td nowrap='true'>物资编号</td>
	    	  <td nowrap='true'>物资名称</td>
	    	  <td nowrap='true'>规格型号</td>
	    	  <td nowrap='true'>计量单位</td>
	    	  <td nowrap='true'>批号</td>
	    	  <td nowrap='true'>期初数</td>
	    	  <td nowrap='true'>期初金额</td>
	    	  <td nowrap='true'>入库数</td>
	    	  <td nowrap='true'>入库金额</td>
	    	  <td nowrap='true'>出库数</td>
	    	  <td nowrap='true'>出库金额</td>
	    	  <td nowrap='true'>结存数</td>
	    	  <td nowrap='true'>结存金额</td>
	    	</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
                </xsl:when>


		<xsl:when test="position()>6">
               <td align="right">
		  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		</td>
                </xsl:when>

                <xsl:otherwise>
		<td>
                  <xsl:value-of select="."/>
                </td>
		</xsl:otherwise>
              </xsl:choose>
           
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>



