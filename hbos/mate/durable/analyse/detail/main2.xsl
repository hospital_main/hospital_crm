<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">日期</th>
  			<th noWrap="true">单据号</th>
  			<th noWrap="true">单据类型</th>
  		  <th noWrap="true">业务类型</th>
  			<th noWrap="true">摘要</th>
  			<th noWrap="true">操作科室</th>
  			<th noWrap="true">操作人</th>
  			<th noWrap="true">单据数量</th>
  			<th noWrap="true">实际库存</th>
  			<th noWrap="true">实物盘点数量</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test="position()=2">
		          	<td >
							<xsl:if test="'53'=../td[13] or '54'=../td[13]" >
					          <a href="#">
							    <xsl:attribute name="onclick" >
								  javascript:openDialog('update21.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:1000px;dialogHeight:650px',result)
								</xsl:attribute>
								<xsl:value-of select="."/>
							  </a>
							</xsl:if>
							<xsl:if test="'55'=../td[13] or '56'=../td[13]" >
					          <a href="#">
							    <xsl:attribute name="onclick" >
								  javascript:openDialog('update22.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:1000px;dialogHeight:650px',result)
								</xsl:attribute>
								<xsl:value-of select="."/>
							  </a>
							</xsl:if>
							<xsl:if test="'57'=../td[13]" >
					          <a href="#">
							    <xsl:attribute name="onclick" >
								  javascript:openDialog('update23.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:1000px;dialogHeight:650px',result)
								</xsl:attribute>
								<xsl:value-of select="."/>
							  </a>
							</xsl:if>
							<xsl:if test="'59'=../td[13] or '60'=../td[13]" >
					          <a href="#">
							    <xsl:attribute name="onclick" >
								  javascript:openDialog('update24.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:1000px;dialogHeight:650px',result)
								</xsl:attribute>
								<xsl:value-of select="."/>
							  </a>
							</xsl:if>
	              </td>
		          </xsl:when>
	            <xsl:when test="position()=8 or position()=9 or position()=10">
		          	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=11 or position()=12 or position()=13">
		          	<td style="display:none">
         					<xsl:value-of select="."/>
        			  </td>
		          </xsl:when>
				  		<xsl:otherwise>
			          <td>
         					<xsl:value-of select="."/>
        			  </td>   
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>