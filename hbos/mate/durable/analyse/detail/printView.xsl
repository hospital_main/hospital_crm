<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	    <thead>
			<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">10</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr noWrap='true' class='mainHead'>
	    	  <td nowrap='true' colspan="3"></td>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td nowrap='true' colspan="3"></td>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td nowrap='true' colspan="4"></td>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	</tr>
			<tr noWrap='true' class='mainHead'>
	    	  <td nowrap='true' colspan="5"></td>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td nowrap='true' colspan="5"></td>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	</tr>
				<tr noWrap='true' class='mainHead'>
	    	  <td nowrap='true'>日期</td>
	    	  <td nowrap='true'>单据号</td>
	    	  <td nowrap='true'>单据类型</td>
	    	  <td nowrap='true'>业务类型</td>
	    	  <td nowrap='true'>摘要</td>
	    	  <td nowrap='true'>操作科室</td>
	    	  <td nowrap='true'>操作人</td>
	    	  <td nowrap='true'>单据数量</td>
	    	  <td nowrap='true'>实际库存</td>
	    	  <td nowrap='true'>实物盘点数量</td>
	    	</tr>      
	      </thead>	      
	      <tbody>
		      <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=8 or position()=9 or position()=10">
		          	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=11 or position()=12 or position()=13">
		          	<td style="display:none">
         					<xsl:value-of select="."/>
        			  </td>
		          </xsl:when>
				  		<xsl:otherwise>
			          <td>
         					<xsl:value-of select="."/>
        			  </td>   
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
	      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>