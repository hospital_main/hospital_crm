<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>制单日期</th>
				<th>单据号</th>
				<th>科室</th>
				<th>仓库</th>
				<th>材料编码</th>
				<th>材料名称</th>
				<th>批号</th>
				<th>单价</th>
				<th>数量</th>
				<th>金额</th>
				<th style="display:none">iow_id</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	              <xsl:choose>
	              <xsl:when test="position()=2">
	               <td align="right">
		               <a tabindex='-1'>
		               	  <xsl:if test="../td[3]=''">
		               	  	<xsl:attribute name="href">
    	            				javascript:openDialog("store/update.html?load=&lt;iow_id&gt;<xsl:value-of select='../td[last()]' />&lt;/iow_id&gt;", "dialogWidth:1000px;dialogHeight:700px")
  	          	  	  	</xsl:attribute>
  	          	  	  </xsl:if>
  	          	  	  <xsl:if test="../td[4]=''">
  	          	  	  	<xsl:attribute name="href">
    	            				javascript:openDialog("dept/update.html?load=&lt;iow_id&gt;<xsl:value-of select='../td[last()]' />&lt;/iow_id&gt;", "dialogWidth:1000px;dialogHeight:700px")
  	          	  	  	</xsl:attribute>
  	          	  	  </xsl:if>
				  <xsl:value-of select="."/>
				</a>
			</td>
	                </xsl:when>
	                <xsl:when test="position()=last()">
		               <td style="display:none">
				  <xsl:value-of select="."/>
				</td>
	                </xsl:when>
			<xsl:when test="position()>=8">
	               <td align="right">
			  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			</td>
	                </xsl:when>
	                <xsl:otherwise>
			<td>
	                  <xsl:value-of select="."/>
	                </td>
			</xsl:otherwise>
	              </xsl:choose>
  		  </xsl:for-each>
  		</tr>
   	     </xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
