<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN =' '/>
	<xsl:template match="/">
	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
	<root>
		<thead>
			<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">11</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<!--<tr noWrap='true' class='mainHead'>
	    	  <td nowrap='true' colspan="4"></td>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td nowrap='true' colspan="4"></td>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td style='display:none'/>
	    	  <td nowrap='true' colspan="2"></td>
	    	  <td nowrap='true'></td>
	    	</tr>-->
		<tr noWrap='true' class='mainHead'>
		    	<td>制单日期</td>
			<td>单据号</td>
			<td>科室</td>
			<td>仓库</td>
			<td>材料编码</td>
			<td>材料名称</td>
			<td>批号</td>
			<td>单价</td>
			<td>数量</td>
			<td>金额</td>
	    	</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
		        <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
				<xsl:when test="position()>=8">
		               <td align="right">
				  <xsl:value-of select="format-number(.,'#,##0.00')"/>
				</td>
		                </xsl:when>
		                <xsl:otherwise>
				<td>
		                  <xsl:value-of select="."/>
		                </td>
				</xsl:otherwise>
		              </xsl:choose>
	  		  </xsl:for-each>
	  		</tr>
	   	     </xsl:for-each>  	
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>



