<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root> 
 <xsl:variable name="colNum1" select="/root/tbody/tr[1]/td[1]"/>
 <xsl:variable name="colNum2" select="/root/tbody/tr[1]/td[2]"/>
<thead>  
   <tr noWrap="true" class="mainHead">
    <td rowspan="2">物资类别编码</td>
    <td rowspan="2">物资类别名称</td>
    <td rowspan="2">期初金额</td>
    <td>
     <xsl:attribute name="colspan">
       <xsl:value-of select="$colNum1" />
     </xsl:attribute>
           增加
    </td>
     <xsl:for-each select="/root/tbody/tr[1]/td">
       <xsl:if test="position()&lt;$colNum1">
		 <td style='display:none'/> 
	   </xsl:if>
     </xsl:for-each> 
    <td>
     <xsl:attribute name="colspan">
       <xsl:value-of select="$colNum2" />
     </xsl:attribute>
          减少
    </td>
     <xsl:for-each select="/root/tbody/tr[1]/td">
       <xsl:if test="position()&lt;$colNum2">
		 <td style='display:none'/> 
	   </xsl:if>
     </xsl:for-each> 
     <td rowspan="2">期末余额</td>
   </tr>
   <tr noWrap="true" class="mainHead">
     <xsl:for-each select="/root/tbody/tr[2]/td">
     <xsl:choose>
         <xsl:when test="position()=4 or position()=5">
         </xsl:when>
         <xsl:when test="position()=1 or position()=2 or position()=3 or position()=last()">
         <td style='display:none'/>
         </xsl:when> 
         <xsl:otherwise>
         <td><xsl:value-of select="." /></td>
         </xsl:otherwise>
     </xsl:choose>
    </xsl:for-each> 
   </tr>
  </thead>
  <tbody>
   <xsl:for-each select="/root/tbody/tr">
     <xsl:if test="position()>2">
     <tr>
      <xsl:for-each select="td">
        <xsl:choose>
         <xsl:when test="position()=1">
          <td align='left'> 
          <xsl:value-of select="."/>
          </td>
         </xsl:when>
         <xsl:when test="position()=2">
          <td align='left'>
          <xsl:value-of select="."/>
          </td>
         </xsl:when>
         <xsl:when test="position()=3">
          <td align='right'>
          <xsl:value-of select="format-number(.,'#,##0.00')"/>
          </td>
         </xsl:when>
         <xsl:when test="position()=4 or position()=5"> 
         </xsl:when>
         <xsl:otherwise>
          <td align='right'>
          <xsl:value-of select="format-number(.,'#,##0.00')"/>
          </td>
         </xsl:otherwise>
        </xsl:choose>
       
      </xsl:for-each>
     </tr>
    </xsl:if>
   </xsl:for-each>
   </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>