<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>  
			<tr noWrap="true" class="mainHead">
				<th rowspan="2">物资类别编码</th>
				<th rowspan="2">物资类别名称</th>
				<th rowspan="2">期初金额</th>
				<th rowspan="2" style="display:none">level</th>
				<th rowspan="2" style="display:none">is_last</th>
				<th>
					<xsl:attribute name="colspan">
					  <xsl:value-of select="/root/tbody/tr[1]/td[1]" />
					</xsl:attribute>
					增加</th>
				<th>
					<xsl:attribute name="colspan">
					  <xsl:value-of select="/root/tbody/tr[1]/td[2]" />
					</xsl:attribute>
				减少</th>
				<th rowspan="2">期末余额</th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<xsl:for-each select="/root/tbody/tr[2]/td">
					<xsl:choose>
						 <xsl:when test="position()=1 or position()=2 or position()=3 or position()=4 or position()=5 or position()=last()">
					</xsl:when>
					<xsl:otherwise>
							<th><xsl:value-of select="." /></th>
					</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			  <xsl:if test="position()>2">
					<tr>
						<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=1">
									 <td align='left'>	
										<xsl:value-of select="."/>
									 </td>
									</xsl:when>
									<xsl:when test="position()=2">
									 <td align='left' width='180'>
									 	<xsl:if test=" ../td[5] = 0 ">
	    	      		   							<img src='/images2/menu/folderopen.png' _exp='-' onclick='expand(this)'/>
	          		      						</xsl:if>	
										<xsl:value-of select="."/>
									 </td>
									</xsl:when>
									<xsl:when test="position()=4 or position()=5">
									 <td align='left' style="display:none">	
										<xsl:value-of select="."/>
									 </td>
									</xsl:when>
									<xsl:otherwise>
									 <td align='right'>	
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									 </td>
									</xsl:otherwise>
								</xsl:choose>
							
						</xsl:for-each>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
