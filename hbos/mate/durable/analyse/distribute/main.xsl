<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/mate/durable/analyse/distribute/main.xsl,v 1.1 2012/03/12 01:54:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:54:35 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>��������</th>
				<th>@active_1</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                <xsl:if test="position()=3">
                  <td ><a tabindex='-1'><xsl:value-of select="."/></a></td>
                </xsl:if>
                <xsl:if test="position()>3">
                  <td ><xsl:value-of select="."/></td>
                </xsl:if>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
