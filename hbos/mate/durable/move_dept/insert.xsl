<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
        <th nowrap="true">序号</th>
				<th nowrap="true">材料编码</th>
				<th nowrap="true">材料名称</th>
				<th nowrap="true">规格型号</th>
				<th nowrap="true">批号</th>
				<th nowrap="true">计量单位</th>
				<th nowrap="true">数量</th>
				<th nowrap="true">当前库存</th>
    	</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <td align='right'>
            <xsl:value-of select="position()"/>
          </td>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1 or position()=3 or position()=4 or position()=10 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16">
              </xsl:when>
              <xsl:when test="position()=9 or position()=11">
                <td align='right' class='moneyCol'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	              </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>
