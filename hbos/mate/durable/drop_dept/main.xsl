<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th nowrap="true">单据号</th>
				<th nowrap="true">日期</th>
				<th nowrap="true">摘要</th>
				<th nowrap="true">科室</th>
				<th nowrap="true">业务类型</th>
				<th noWrap="true">金额</th>
				<th nowrap="true">制单人</th>
				<th nowrap="true">确认人</th>
				<th nowrap="true">状态</th>
			</tr>
  	</thead>
  	<tbody>
  	  <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	    <xsl:for-each select="/root/tbody/tr">
	     <xsl:variable name="iow_no" select="td[1]"/>
  	  	 <xsl:variable name="iow_id" select="./pk/iow_id"/>
  	  	 <xsl:variable name="money" select="td[6]"/>
        <tr>
         <xsl:if test="td[1]!='合计'">
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          </xsl:if>
          <xsl:if test="td[1]='合计'">
						<td/>
		  </xsl:if>   
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                   <xsl:if test=". !='合计'">
		          		<td><a tabindex='-1'><xsl:value-of select="."/></a></td>
		          	</xsl:if>
		          	<xsl:if test=". ='合计'">
		          		<td><xsl:value-of select="."/></td>
		          	</xsl:if>
              </xsl:when>
              <xsl:when test="position()=6">
		            <td align='right'> 
					<xsl:if test="$iow_no !='合计'">
  		             <a href="#">
					  <xsl:attribute name="onclick">
					  openDetailItems("&lt;iow_id&gt;<xsl:value-of select="$iow_id"/>&lt;/iow_id&gt;&lt;iow_no&gt;<xsl:value-of select="$iow_no"/>&lt;/iow_no&gt;&lt;money&gt;<xsl:value-of select="format-number($money,'#,##0.00')"/>&lt;/money&gt;")
					  </xsl:attribute>
					       <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
					  </a>
  		              </xsl:if>
  		              <xsl:if test="$iow_no ='合计'">
  		              	 <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
  		              </xsl:if>
		            </td>
			  </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>

