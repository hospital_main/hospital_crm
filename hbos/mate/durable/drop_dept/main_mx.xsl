<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th nowrap="true">材料编码</th>
				<th nowrap="true">材料名称</th>
				<th nowrap="true">规格型号</th>
				<th nowrap="true">移出仓库</th>
				<th nowrap="true">日期</th>
				<th nowrap="true">移入仓库</th>
				<th nowrap="true">数量</th>
				<th nowrap="true">单价</th>
				<th nowrap="true">金额</th>
				<th nowrap="true">制单人</th>
				<th nowrap="true">移出确认人</th>
				<th nowrap="true">移入确认人</th>
				<th nowrap="true">状态</th>
				<th nowrap="true">单据号</th>
				<th nowrap="true">摘要</th>
				<th nowrap="true">主要供应商</th>
		</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=8">
                <td align="right">
									<xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=9">
                <td align="right">
									<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=7">
                <td align="right">
                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>

