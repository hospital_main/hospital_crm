<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/mate/durable/check_dept/main.xsl,v 1.1 2012/03/12 01:54:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:54:35 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th noWrap="true">�̵㵥�ݺ�</th>
				<th noWrap="true">ժҪ</th>				
				<th noWrap="true">�̵����</th>
				<th noWrap="true">�̵�����</th>
				<th noWrap="true">�̵���</th>
				<th noWrap="true">�����</th>
				<th noWrap="true">�������</th>
				<th noWrap="true">״̬</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
         <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
           
              <xsl:choose>
                <xsl:when test="position()=1">
                	 <td >
                  <a tabindex='-1'><xsl:value-of select="."/></a>
                  </td>
                </xsl:when>
               <xsl:when test="position()=8">
						    <xsl:if test="../td[8]='δ���'">
							  <td style='Color:red'>
								<xsl:value-of select="."/>
							</td>
					 	  </xsl:if>
						  <xsl:if test="../td[8]!='δ���'">
							<td>
								<xsl:value-of select="."/>
							</td>
						 </xsl:if>
				     	</xsl:when>
                <xsl:otherwise>
                	<td>
                  <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
