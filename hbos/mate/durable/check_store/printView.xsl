<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
    <colgroup>           
      <col style = 'width:120'/>
      <col style = 'width:100'/>
      <col style = 'width:120'/>
      <col style = 'width:120'/>
      <col style = 'width:80'/>
      <col style = 'width:80'/>
      <col style = 'width:80'/>
      <col style = 'width:80'/>
      <col style = 'width:80'/>
      <col style = 'width:120'/>
    </colgroup>
    <thead>

      <tr noWrap='true' class='mainHead'> 
      
        <th noWrap='true' valign="middle">科室名称</th>
        <th noWrap='true' valign="middle">耐用品编码</th>
        <th noWrap='true' valign="middle">耐用品名称</th>
        <th noWrap='true' valign="middle">规格型号</th>
        <th noWrap='true'>科室定额数量</th>
        <th noWrap='true'>科室在用账面数量</th>
        <th noWrap='true'>库存数量</th>
        <th noWrap='true'>盘点数量</th>
        <th noWrap='true'>盈亏数量</th>
        <th noWrap='true' valign="middle">盈亏说明</th>
      </tr>
    </thead>     
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>            
              <xsl:when test="position()=1">  
              </xsl:when>
              <xsl:when test="position()=6 or position()=7 or position()=8 or position()=9 or position()=10">
                <td class="numberText">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>  
              <xsl:otherwise>
                <td align="left"><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>