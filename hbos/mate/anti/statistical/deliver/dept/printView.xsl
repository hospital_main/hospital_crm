<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
			<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
			<thead>
				<tr noWrap="true" class="mainHead">
					<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr noWrap="true" class="mainHead">
					<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td nowrap="true">领包科室编码</td>
					<td nowrap="true">领包科室名称</td>
					<td nowrap="true">领用数量</td>
					<td nowrap="true">金额</td>
				</tr>
			</thead>
			<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test="position()=1">
	                	<td><xsl:value-of select="."/></td>
	              </xsl:when>
	              <xsl:when test="position()=3 ">
			           	<td align='right'>
		                <xsl:value-of select="format-number(.,'#,##0.00')"/>
		              </td>
			          </xsl:when>
			          <xsl:when test="position()=4">
			           	<td align='right'>
		                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
		              </td>
			          </xsl:when>
	              <xsl:otherwise>
	                <td>
	                  <xsl:value-of select="."/>
	                </td>
	              </xsl:otherwise>
	            </xsl:choose>
	          </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>