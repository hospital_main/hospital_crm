<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true">领包科室名称</th>
				<th nowrap="true">领用日期</th>
				<th nowrap="true">物品名称</th>
				<th nowrap="true">规格型号</th>
				<th nowrap="true">计量单位</th>
				<th nowrap="true">领用数量</th>
				<th nowrap="true">单价</th>
				<th nowrap="true">金额</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                	<td><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position()=6 ">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
              <xsl:when test="position()=7 ">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=8">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
		          </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
