<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
			<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
			<thead>
				<tr noWrap="true" class="mainHead">
					<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr noWrap="true" class="mainHead">
					<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td nowrap="true">物品名称</td>
					<td nowrap="true">规格型号</td>
					<td nowrap="true">计量单位</td>
					<td nowrap="true">接收数量</td>
					<td nowrap="true">消毒数量</td>
					<td nowrap="true">发放数量</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
							</xsl:when>
							<xsl:when test="position()=5 or position()=6 or position()=7 ">
								<td align='right'>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>			                        
						</xsl:choose>
					</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>