<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	
    		<tr noWrap="true" class="mainHead">
    			<td noWrap="true" style="fontsize:maintitle;colspan:9;align:center;border:none"/>
    			<td style='display:none'/>
    			<td style='display:none'/> 
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/> 
    			<td style='display:none'/>
    			<td style='display:none'/>
    		</tr>
    	
  		<tr noWrap='true' class='mainHead'>
        <td style='border:1'>消毒包编码</td>
        <td style='border:1'>消毒包名称</td>
				<td style='border:1'>规格型号</td>
				<td style='border:1'>计量单位</td>
				<td style='border:1'>材料编码</td>
        <td style='border:1'>材料名称</td>
				<td style='border:1'>规格型号</td>
				<td style='border:1'>计量单位</td>
				<td style='border:1'>数量</td>
				
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
       <xsl:variable name="dis_code" select="td[1]" />
       <xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$dis_code])"/>
       <xsl:variable name="cur_pos" select="position()" />
        <tr>
        	<xsl:for-each select="td">
           	<xsl:choose>
	          	<xsl:when test="position() &lt; 5">
								<xsl:if test="$cur_pos = 1 ">
									<td nowrap="true" rowspan="{$rowspan}">
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="$cur_pos >1 and $dis_code != ../../tr[$cur_pos - 1]/td[1]">
									<td nowrap="true" rowspan="{$rowspan}">
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="$cur_pos >1 and $dis_code = ../../tr[$cur_pos - 1]/td[1]">
									<td style="display:none"><xsl:value-of select="."/>
								  </td>
								</xsl:if>
									
						  </xsl:when>
						  
	            <xsl:when test="position()=9">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0')"/>
								
	              </td>
	            </xsl:when>
	            
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
	            
            </xsl:choose>
  			  </xsl:for-each>
  			 </tr>
  			 
      </xsl:for-each>
    </tbody>
    <tfoot>
        
 		     <tr noWrap='true'>
		        <td style='fontsize:foot;colspan:colcount;align:left;border:none'></td>
		  			<xsl:call-template name="repeat">
		  				<xsl:with-param name="times" select="$colNum"/>    
		        </xsl:call-template>
  	  	</tr>
    		
    	</tfoot>
   </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>