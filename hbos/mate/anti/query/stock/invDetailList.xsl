<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th>消毒包编码</th>
        <th>消毒包名称</th>
				<th>规格型号</th> 
				<th>计量单位</th>
				<th>材料编码</th>
        <th>材料名称</th>
				<th>规格型号</th> 
				<th>计量单位</th>
				<th>数量</th>
			</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
       <xsl:variable name="dis_code" select="td[1]" />
       <xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$dis_code])"/>
       <xsl:variable name="cur_pos" select="position()" />
        <tr>
        	<xsl:for-each select="td">
           	<xsl:choose>
	          	<xsl:when test="position() &lt; 5">
								<xsl:if test="$cur_pos = 1 ">
									<td nowrap="true" rowspan="{$rowspan}">
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="$cur_pos >1 and $dis_code != ../../tr[$cur_pos - 1]/td[1]">
									<td nowrap="true" rowspan="{$rowspan}">
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="$cur_pos >1 and $dis_code = ../../tr[$cur_pos - 1]/td[1]">
									<td style="display:none"><xsl:value-of select="."/>
								  </td>
								</xsl:if>
									
						  </xsl:when>
						  
	            <xsl:when test="position()=9">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
	            
            </xsl:choose>
  			  </xsl:for-each>
  			 </tr>
  			 
      </xsl:for-each>
      
    </tbody>
  </xsl:template>
</xsl:stylesheet>


