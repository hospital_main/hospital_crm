<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=' '/>
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap="true">材料编码</th>
				<th nowrap="true">材料名称</th>
				<th nowrap="true">规格型号</th>
				<th nowrap="true">计量单位</th>
				<th nowrap="true">科室</th>
				<th nowrap="true">数量</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="unit_name" select="td[1]" />
				<xsl:variable name="kpi" select="td[3]" />
				<xsl:variable name="cur_pos" select="position()" />
				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$unit_name])" />
				<xsl:variable name="rowspan2" select="count(/root/tbody/tr[td[3] = $kpi and td[1]= $unit_name ])" />
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() &lt; 5">
								<xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos - 1]/td[1]">
									<td rowspan="{$rowspan}">
										<xsl:value-of select="." />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position() = 6">
								<td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>