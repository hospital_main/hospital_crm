<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
  		<tr noWrap='true' >
			<td colspan="9" style="fontsize:maintitle;"/>
			<td style='display:none' />
			<td style='display:none' />
			<td style='display:none' />
			<td style='display:none' />
			<td style='display:none' />
			<td style='display:none' />
			<td style='display:none' />
			<td style='display:none' />
			<td style='display:none' />
		</tr>
		<tr noWrap='true' class='mainHead'>
			<td nowrap="true">消毒包编码</td>
			<td nowrap="true">消毒名称</td>
			<td nowrap="true">规格型号</td>
			<td nowrap="true">计量单位</td>
			<td nowrap="true">科室</td>
			<td nowrap="true">状态</td>
			<td nowrap="true">服务单价</td>
			<td nowrap="true">材料金额</td>
			<td nowrap="true">数量</td>
			<td nowrap="true">金额</td>
		</tr>
	      </thead>
	      <tbody>
			<xsl:for-each select="/root/tbody/tr">
		        <xsl:variable name="unit_name" select="td[1]" />
			<xsl:variable name="kpi" select="td[3]" />
			<xsl:variable name="cur_pos" select="position()" />
			<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$unit_name])" />
			<xsl:variable name="rowspan2" select="count(/root/tbody/tr[td[3] = $kpi and td[1]= $unit_name ])" />
			<tr>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position() &lt; 5">
							<xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos - 1]/td[1]">
								<td rowspan="{$rowspan}">
									<xsl:value-of select="." />
								</td>
							</xsl:if>
							<xsl:if test="$cur_pos > 1 and $unit_name = ../../tr[$cur_pos - 1]/td[1]">
								<td style='display:none' />
							</xsl:if>
						</xsl:when>
						<xsl:when test="position() &gt; 6 and position() != 9">
							<td align='right'>
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
						</xsl:when>
						<xsl:when test="position() = 9">
							<td align='right'>
								<xsl:value-of select="format-number(.,'#,##0')"/>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td>
								<xsl:value-of select="."/>
							</td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
			</xsl:for-each>
	      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>