<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=' '/>  
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th noWrap="true" style = 'display:none'>科室编码</th>
				<th noWrap="true" >科室名称</th>
				<th noWrap="true" >消毒包编码</th>
				<th noWrap="true" >消毒包名称</th>
				<th noWrap="true" >规格型号</th>
				<th noWrap="true" >计量单位</th>
				<th noWrap="true" >服务单价</th>
				<th noWrap="true" >材料金额</th>
				<th noWrap="true" >数量</th>
				<th noWrap="true" >金额</th>		
			</tr>
		</thead>
		<tbody>
			<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="dept_code" select="td[1]" />
				
				<xsl:variable name="cur_pos" select="position()" />					
				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$dept_code])" />
				
				<tr>
					<xsl:for-each select="td [position () &gt; 1]">
						<xsl:choose>
							
							<xsl:when test=" position()=1 ">
						
									<xsl:if test="../td[1]=../../tr[$cur_pos - 1 ]/td[1]">
											<td style='display:none'></td>
									</xsl:if>
									
									<xsl:if test="../td[1] !=../../tr[$cur_pos - 1 ]/td[1] or $cur_pos = 1 ">
											<td>
												<xsl:attribute name = "rowspan">
													<xsl:value-of  select = "$rowspan" />
												</xsl:attribute>
												<xsl:value-of select="." />
											</td>
									</xsl:if>
							</xsl:when>
						
							
							<xsl:when test="position()=1 or position()=2 or position()=3 or position()=4 or position()=5">
								<td>
									<xsl:value-of select="." />
		            </td>
							</xsl:when>
							<xsl:when test="position()=6 or position()=7 or position()=9 ">
							<td>
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
							</xsl:when>
							<xsl:when test="position()=8 ">
							<td>
								<xsl:value-of select="format-number(.,'#,##0')"/>
							</td>
							</xsl:when>
						</xsl:choose>            
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>