<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=' '/>  
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th noWrap="true" >消毒包编码</th>
				<th noWrap="true" >消毒包名称</th>
				<th noWrap="true" >规格型号</th>
				<th noWrap="true" >计量单位</th>
				<th noWrap="true" style = 'display:none'>科室编码</th>
				<th noWrap="true" >科室名称</th>
				<th noWrap="true" >材料编码</th>
				<th noWrap="true" >材料名称</th>
				<th noWrap="true" >规格型号</th>
				<th noWrap="true" >计量单位</th>
				<th noWrap="true" >数量</th>
			</tr>
		</thead>
		<tbody>
			<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
			<xsl:for-each select="/root/tbody/tr">
				
				<xsl:variable name="package_code" select="td[1]" />
				<xsl:variable name="cur_pos" select="position()" />					
				
				<xsl:variable name="rowspan2" select="count(/root/tbody/tr[td[1]=$package_code ])" />
				<tr>
					<xsl:for-each select="td [position () != 5]">
						<xsl:choose>
							
							<xsl:when test=" position()=1  or position()=2 or position()=3 or position()=4  ">
						
									<xsl:if test="../td[1]=../../tr[$cur_pos - 1 ]/td[1]  ">
											<td style='display:none'></td>
									</xsl:if>
									
									<xsl:if test="../td[1] !=../../tr[$cur_pos - 1 ]/td[1]  or $cur_pos = 1 ">
											<td>
												<xsl:attribute name = "rowspan">
													<xsl:value-of  select = "$rowspan2" />
												</xsl:attribute>
												<xsl:value-of select="." />
											</td>
									</xsl:if>
							</xsl:when>
							<xsl:when test=" position()=5 ">
								<xsl:variable name="dept_code" select="../td[5]" />
								<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[5]=$dept_code and td[1]=$package_code ])" />
									<xsl:if test="../td[1] =../../tr[$cur_pos - 1 ]/td[1] and ../td[5] =../../tr[$cur_pos - 1 ]/td[5] ">
											<td style='display:none'></td>
									</xsl:if>
									<xsl:if test="../td[1] =../../tr[$cur_pos - 1 ]/td[1] and ../td[5] !=../../tr[$cur_pos - 1 ]/td[5]  or  $cur_pos = 1">
											<td>
												<xsl:attribute name = "rowspan">
													<xsl:value-of  select = "$rowspan" />
												</xsl:attribute>
												<xsl:value-of select="." />
											</td>
									</xsl:if>
									<xsl:if test="../td[1] !=../../tr[$cur_pos - 1 ]/td[1] and ../td[5] !=../../tr[$cur_pos - 1 ]/td[5]  ">
											<td>
												<xsl:attribute name = "rowspan">
													<xsl:value-of  select = "$rowspan" />
												</xsl:attribute>
												<xsl:value-of select="." />
											</td>
									</xsl:if>
									
							
							</xsl:when>
							<xsl:when test="position() = 10">
								<td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise >
								<td>
									<xsl:value-of select="." />
		            			</td>
							</xsl:otherwise>
							
						</xsl:choose>            
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>