<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
  		<tr noWrap='true' >
			<td colspan="7" style="fontsize:maintitle;"/>
			<td style='display:none' />
			<td style='display:none' />
			<td style='display:none' />
			<td style='display:none' />
			<td style='display:none' />
			<td style='display:none' />
		</tr>
		<tr noWrap='true' >
			<td colspan="3" style="align:left"/>
			<td style='display:none' />
			<td style='display:none'/>
			<td colspan="4" style="align:left" />
			<td style='display:none'/>
			<td style='display:none' />
			<td style='display:none' />
		</tr>
		<tr noWrap='true' align="left">
			<td colspan="3" style="align:left"/>
			<td style='display:none' />
			<td style='display:none'/>
			<td colspan="4" style="align:left"  />
			<td style='display:none' />
			<td style='display:none' />
			<td style='display:none' />
		</tr>
		<tr noWrap='true' align="left">
			<td>消毒包编码</td>
			<td>消毒包名称</td>
			<td>规格型号</td>
			<td>库存账面数量</td>
			<td>盘点数量</td>
			<td>盈亏数量</td>
			<td>盈亏说明</td>
		</tr>
	      </thead>
	      <tbody>
			<xsl:for-each select="/root/tbody/tr">
		        <tr>
				<xsl:for-each select="td">
					<xsl:if test="position() &gt; 6 or position() &lt;4">
						<td><xsl:value-of select="."/></td>
					</xsl:if>
					<xsl:if test="position() &gt; 3 and position() &lt;7">
						<td align="right"><xsl:value-of select="format-number(.,'#,##0')"/></td>
					</xsl:if>
				</xsl:for-each>
  			</tr>
			</xsl:for-each>
	      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>