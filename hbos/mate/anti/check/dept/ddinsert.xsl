<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
		<th><input type="checkbox" onclick="setAll(this)"/></th>
		<th nowrap="true">消毒包编码</th>
		<th nowrap="true">消毒包名称</th>
		<th nowrap="true">规格型号</th>
		<th nowrap="true">计量单位</th>
		<th nowrap="true">消毒包别名</th>
		<th nowrap="true" style="display:none">科室</th>
		<th nowrap="true" style="display:none">库存账面数量</th>
		<th nowrap="true" style="display:none">定额数</th>
    	</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' />
          </td>
          <xsl:for-each select="td">
		<xsl:if test="position() &lt; 6">
            		<td><xsl:value-of select="."/></td>
		</xsl:if>
		<xsl:if test="position() &gt; 5">
            		<td style="display:none"><xsl:value-of select="."/></td>
		</xsl:if>
  	  </xsl:for-each>
  	</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
