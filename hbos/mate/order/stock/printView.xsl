<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      <td>备货单号</td>
      <td>备货日期</td>
      <td>审核日期</td>
      <td>摘要</td>
      <td>采购员</td>
      <td>制单人</td>
      <td>审核人</td>
      <td>状态</td>
    </tr>
  </thead>      
      <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        
          <xsl:for-each select="td">
            <td >
              <xsl:choose>
                <xsl:when test="position()=1">
                  <a tabindex='-1'><xsl:value-of select="."/></a>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
  	  </xsl:for-each>
  	</tr>
      </xsl:for-each>  	
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>