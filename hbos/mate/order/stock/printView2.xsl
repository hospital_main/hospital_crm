<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
       <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>      
        <tr noWrap="true" class="mainHead">
      	<td>名称</td>
      	<td>规格</td>
      	<td>单位</td>
      	<td>数量</td>
      	<td>生产厂商</td>
      	<td>备注</td>
				</tr> 		      
      </thead>	      
      <tbody>
        <xsl:for-each select="/root/tbody/tr[td[1]!='']">
	  			<tr>
            <xsl:for-each select="td">          
	      			<xsl:choose>         
              <xsl:when test="position()=9">
                <td align='right' class='numberText'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	              </td>
              </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
            </xsl:for-each>
	  			</tr>
        </xsl:for-each>
      </tbody>
    </root>
  </xsl:template>

</xsl:stylesheet>