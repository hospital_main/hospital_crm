<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <colgroup>
        <col style = 'width:300'/>
        <col style = 'width:300'/>
        <col style = 'width:60'/>
        <col style = 'width:60'/>
        <col style = 'width:100'/>
        <col style = 'width:180'/>
        
      </colgroup>
      <thead>
      	<xsl:variable name="endRow" select="count(/root/tbody/tr) mod 17"/>
        <tr noWrap='true' class='mainHead'>
      	<th>名称</th>
      	<th>规格</th>
      	<th>单位</th>
      	<th>数量</th>
      	<th>生产厂商</th>
      	<th>备注</th>
				</tr> 		      
      </thead>	      
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
	  			<tr>
            <xsl:for-each select="td">          
	      			<xsl:choose>         
              <xsl:when test="position()=1 or position()=6 or position()=7  or position()=9">
              </xsl:when>
              <xsl:when test="position()=5">
                <td align='right' class='numberText'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	              </td>
              </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
            </xsl:for-each>
	  			</tr>
        </xsl:for-each>
        <xsl:if test="$endRow>0">
	        <xsl:call-template  name="repeat">  
	           <xsl:with-param  name="times"  select="17 - $endRow"  />  
	        </xsl:call-template>  
	      </xsl:if>
      </tbody>
    </root>
  </xsl:template>
  <xsl:template  name="repeat">  
		<xsl:param  name="times"  select="0"  />  
		<xsl:if  test="$times  >  0">
			<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>
			<xsl:call-template  name="repeat">  
			<xsl:with-param  name="times"  select="$times  -  1"  />  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>