<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
	      <th>订单编号</th>				
	      <th>日期</th>
	      <th>供应商</th>
	      <th>科室</th>
	      <th>采购员</th>
      	<th>材料名称</th>
        <th>单位</th>
        <th>数量</th>
        <th>金额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	<tr>
	  <xsl:for-each select="td">
	    <xsl:choose>
	      <xsl:when test="position()=8 or position()=9">
	        <td align='right' class='moneyCol'>
	          <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	        </td>
	      </xsl:when>
	      <xsl:otherwise>
	        <td align='left'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:otherwise>			                        
	    </xsl:choose>
	  </xsl:for-each>
	</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>