<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:right"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:5;align:right"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:5;align:right"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
        <tr noWrap='true' class='mainHead'>  	  
          <td>订单编号</td>
          <td>订单日期</td>
          <td>供应商</td>
          <td>科室</td>
          <td>采购员</td>
          <td>到货地址</td>
          <td>采购类型</td>
          <td>审核日期</td>
          <td>备注</td>
          <td>状态</td>
	</tr> 		      
      </thead>	      
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
	  <tr>
            <xsl:for-each select="td">          
	      <xsl:choose>            
		<xsl:when test="position()=3">
      	          <td align="left">
		    <xsl:value-of select="."/>
		  </td>
      	        </xsl:when>
		<xsl:otherwise>
		  <td align="left">
		    <xsl:value-of select="."/>
		  </td>
		</xsl:otherwise>
	      </xsl:choose>
            </xsl:for-each>
	  </tr>
        </xsl:for-each>
      </tbody>
      <tfoot>
    	<tr noWrap="true" class="mainHead">
    		<td noWrap="true" style="fontsize:foot;colspan:3;align:left"/>
    		<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:foot;colspan:3;align:right"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:foot;colspan:2;align:right"/>
  	  	<td style="display:none"/>
    	</tr>
    </tfoot>
    </root>   
  </xsl:template>
</xsl:stylesheet>