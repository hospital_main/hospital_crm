<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
      <!--th style="display:none"><input type="checkbox"/></th-->
      <th>订单编号</th>
      <th>订单日期</th>
      <th>供应商</th>
      <th>科室</th>
      <th>采购员</th>
      <th>到货地址</th>
      <th>采购类型</th>
      <th>审核日期</th>
      <th>备注</th>
      <th>状态</th>
    </tr>
  </thead>
  <tbody>
  	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        		 <td align='center'  style='display:none'>
       
    	</td>
          <xsl:for-each select="td">
            <xsl:choose>
               <xsl:when test="position()=1">
                 <td>
                  <a tabindex='-1'>
                  	<xsl:attribute name="href" >
              				javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;ven_name&gt;<xsl:value-of select="../td[3]"/>&lt;/ven_name&gt;&lt;emp_name&gt;<xsl:value-of select="../td[5]"/>&lt;/emp_name&gt;', 'dialogWidth:800px;dialogHeight:700px;')
      	    				</xsl:attribute>
                  	<xsl:value-of select="."/>
                 	</a>
                 </td>
                </xsl:when>
     
              <!--如果状态是未审核，改变状态颜色为红色-->
              	<xsl:when test="position()=10">
								<xsl:if test="../td[10]='未审核'">
									<td style='Color:red'>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="../td[10]!='未审核'">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:when>
              
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
    </tbody>
  </xsl:template>
</xsl:stylesheet>

                