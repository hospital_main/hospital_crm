<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
        <!--th style="display:none"><input type="checkbox"/></th-->
      	<th>���ϱ���</th>
      	<th>��������</th>
      	<th>���ұ���</th>
      	<th>��������</th>
      	<th>����</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="pos" select="position()" />
				<xsl:variable name="inv_code" select="td[1]" />
				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$inv_code])" /> 	
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1 or position() = 2">
								<xsl:if test="$pos = 1 or $inv_code != ../../tr[$pos - 1]/td[1]">
									<td rowspan="{$rowspan}">
										<xsl:value-of select="." />
									</td>
								</xsl:if>
              </xsl:when>
              <xsl:when test="position()=5">
                <td align='right' class='numberText'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	              </td>
              </xsl:when>
              <xsl:otherwise>
                <td noWrap='true'>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  	      </xsl:for-each>
  	    </tr>
      </xsl:for-each>  	
    </tbody>
  </xsl:template>
</xsl:stylesheet>