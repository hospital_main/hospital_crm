<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
      	<th>序号</th>
      	<th>材料名称</th>
      	<th>规格型号</th>
      	<th>计量单位</th>
      	<th>数量</th>
      	<th>单价</th>
      	<th>金额</th>
      	<th>生产厂家</th>
      	<th>计划到货日期</th>
      	<th>备注</th>
      	<th>库房</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      	      </xsl:attribute>
    	    </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=5 ">
    	        	<td class="numberText">
				           <xsl:value-of select="format-number(.,'#,##0.00')"/>  
				        </td>
    	        </xsl:when>
    	        <xsl:when test="position()=6 ">
		           	<td class="numberText" >
	                <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=7">
		           	<td class="numberText">
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
		          </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  	      </xsl:for-each>
  	    </tr>
      </xsl:for-each>  	
    </tbody>
  </xsl:template>
</xsl:stylesheet>