<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
		<td style="display:none"/>
  	  </tr>
      <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
		<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
      <tr noWrap="true" class="mainHead">
      	<td>序号</td>
      	<td>材料名称</td>
      	<td>规格型号</td>
      	<td>计量单位</td>
      	<td>数量</td>
      	<td>单价</td>
      	<td>金额</td>
      	<td>生产厂家</td>
      	<td>计划到货日期</td>
      	<td>备注</td>
      	<td>库房</td>
      </tr>	      
      </thead>	      
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=5 ">
    	        	<td class="numberText">
				           <xsl:value-of select="format-number(.,'#,##0.00')"/>  
				        </td>
    	        </xsl:when>
    	        <xsl:when test="position()=6 ">
		           	<td class="numberText" >
	                <xsl:value-of select="format-number(.,'#,##0.0000')"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=7">
		           	<td class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  	      </xsl:for-each>
  	    </tr>
      </xsl:for-each>  	
    </tbody>
      <tfoot>
    	<tr noWrap="true" class="mainHead">
    		<td noWrap="true" style="fontsize:foot;colspan:12;align:left"/>
    		<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
			<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
  	  		<td style="display:none"/>
			<td style="display:none"/>
    	</tr>
    	<tr noWrap="true" class="mainHead">
    		<td noWrap="true" style="fontsize:foot;colspan:6;align:left"/>
    		<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
			<td style="display:none"/>
  	  		<td noWrap="true" style="fontsize:foot;colspan:6;align:left"/>
    		<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
			<td style="display:none"/>
    	</tr>
    </tfoot>
    </root>   
  </xsl:template>
</xsl:stylesheet>