<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
      <th><input type="checkbox"/></th>
      <th>拣货单编号</th>
      <th>制单日期</th>
      <th>制单人</th>
      <th>拣货员</th>	  
      <th>拣货库房</th>
	  	<!--th>计划类型</th-->
      <th>需求科室</th>
	  <th nowrap="true">备注</th>
	  <th>是否出库</th>
	  <th style="display:none">标红</th>
    </tr>
  </thead>
  <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        <td align='center'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      	    </xsl:attribute>
    	  	</input>
        </td>
          <xsl:for-each select="td">            
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td style='width:120px'><a tabindex='-1'><xsl:value-of select="."/></a></td>
                </xsl:when>
				<xsl:when test="position()=2">
                  <td style='width:80px'><xsl:value-of select="."/></td>
                </xsl:when>
								<xsl:when test="position()=3">
                  <td style='width:80px'><xsl:value-of select="."/></td>
                </xsl:when>
								<xsl:when test="position()=4">
                  <td style='width:80px'><xsl:value-of select="."/></td>
                </xsl:when>
				<xsl:when test="position()=5">
                  <td style='width:80px'><xsl:value-of select="."/></td>
                </xsl:when>
								<xsl:when test="position()=6">
                  <td style='width:480px'><xsl:value-of select="."/></td>
                </xsl:when>
				<xsl:when test="position()=9">					
                  <td style='display:none'><xsl:value-of select="."/></td>
                </xsl:when>
				<!--xsl:when test="position()=7">
                <td width="400px">
                	<xsl:value-of select="."/>
                </td>
              </xsl:when-->
				<xsl:when test="position()=7">
                <td width="650px">
                	<xsl:value-of select="."/>
                </td>
				</xsl:when>
				<xsl:when test="position()=8">
                <td width="100px">
                	<xsl:value-of select="."/>
                </td>
              </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  	  </xsl:for-each>
  	</tr>
      </xsl:for-each>  	
    </tbody>
  </xsl:template>
</xsl:stylesheet>
