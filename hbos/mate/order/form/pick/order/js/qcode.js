qcode_detail=function(_id,_type){
	var id=_id;
	var type=_type;
	// 获取二维码
	function getQcode(){
		window.xmlhttp.post('matein_QCode_getPath','<a>'+id+'</a><b>'+type+'</b>','?isCheck=false');
		var xml=xmlhttp._object.responseText.replace(/tr/g,'b').replace(/td/g,'a');
		if($($('<xml>'+xml+'</xml>').find('a')[0]).text()==id)	{
			return $($('<xml>'+xml+'</xml>').find('a')[1]).text();
		}
		return null;
	}
	
	// 保存生成二维码
	function saveQcode(){
		// 通过配置文件获取qcode值
		var qcode='';
		window.xmlhttp.post('matein_QCode_getCode','<a>'+id+'</a><b>'+type+'</b>','?isCheck=false');
		var xml=xmlhttp._object.responseText.replace(/tr/g,'b').replace(/td/g,'a');
		if($($('<xml>'+xml+'</xml>').find('error').text()=='')){
			qcode= $(xml).find('a').text();
		} else{			
			throw '没有获取到打印字段配置信息，请先设置配置信息后再打印！';
			return;
		}		
		
		// 保存二维码到数据库
		window.xmlhttp.post('matein_QCode_savePath', '<item_no>'+id+'</item_no>'+'<type>'+type+'</type>'+'<barCode>'+qcode+'</barCode>'+'<barType>QR_CODE</barType>','?isCheck=false');
		var xml=xmlhttp._object.responseText.replace(/tr/g,'b').replace(/td/g,'a');
		if($('<xml>'+xml+'</xml>').find('error').text()==''){
			return true;
		}
		return false;
	}
	
	function i(){
		var path=getQcode();
		if(path==null || path==''){
			if(saveQcode()){
				path= getQcode();
			}
		}
		return path;
	}
	return i();	
}
,//条码打印2
qcode_detail_print=function(args) {
	// 组合参数
	var ds = new Object();
	ds.begin = function (error) {
		var arr = [];
		$(args).each(function(i,e){
			var data = [];
			data.push('');//编码
			data.push(e.inv_name); //材料名称
			data.push(e.inv_model); //规格型号
			data.push(e.ven_name);      //供应商
			data.push(e.hos_code);//院内条码
			data.push(e.batch_no);//生产批号
			data.push(e.valid_date);//有效日期
			data.push('1');//数量
			data.push('计量单位');//计量单位
			data.push(e.iow_no)//detail.GetCellData(rows[i], 16));//生产厂商
			data.push('产品注册证号');//产品注册证号	
			data.push(qcode_detail(e.auto_id,'000'));//二维码地址;
			arr.push(data);			
		});		
		this.temp = arr;
	};

	ds.end = function () {
		this.temp = null;
	};

	ds.getNextHeadData = function () {
		if (this.temp == null || this.temp.length == 0)
			return null;
		return this.temp.pop();
	};

	ds.getNextRowData = function () {
		return null;
	};
	//ds.hide=true;
	//ds.autoBegin=true;
	printCellByTemplate("vendor_in_PrintBar2", ds);
}