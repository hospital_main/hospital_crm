<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
      <th><input type="checkbox"/></th>
      <th>科室编码</th>
      <th>科室名称</th>
      <th>材料编码</th>      
      <th>材料名称</th>
	  	<th>规格型号</th>
      <th>计量单位</th>
	  	<th>申请数量</th>
	  	<th>剩余数量</th>	 
	  	<th>差量（申请减剩余）</th>	
	  	<th>供应商名称</th>	  	  
    	</tr>
  </thead>
  <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        <td align='center'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      	    </xsl:attribute>
    	  </input>
          </td>
          <xsl:for-each select="td">            
              <xsl:choose>
                <xsl:when test="position()=9">
					<td>
						<input style='width:120px' type='text' >
							<xsl:attribute name="value" >
								<xsl:value-of select="."/>
							</xsl:attribute>			
							<xsl:attribute name="onkeypress">return event.keyCode>=48&amp;&amp;event.keyCode&lt;=57||event.keyCode==46</xsl:attribute>				
						</input>
					</td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
		  </xsl:for-each>
  	  </tr>
    </xsl:for-each>  	
  </tbody>
  </xsl:template>
</xsl:stylesheet>
