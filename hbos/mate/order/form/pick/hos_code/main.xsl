<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
      <th><input type="checkbox"/></th>
      <th>送货单号</th>
      <th>供应商编码</th>
      <th>供应商名称</th>
      <th>材料编码</th>	  
      <th>材料名称</th>
	  <th>规格型号</th>
      <th>批号</th>
	  <th>院内码</th>
	  <th>有效期</th>
	  <th style="display:none">二维码地址</th>
    </tr>
  </thead>
  <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        <td align='center'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      	    </xsl:attribute>
    	  </input>
          </td>
          <xsl:for-each select="td">            
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td style='width:180px'><a tabindex='-1'><xsl:value-of select="."/></a></td>
                </xsl:when>
				<xsl:when test="position()=10">					
                  <td style='display:none'><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  	  </xsl:for-each>
  	</tr>
      </xsl:for-each>  	
    </tbody>
  </xsl:template>
</xsl:stylesheet>
