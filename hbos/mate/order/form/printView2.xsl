<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <colgroup>	
      	       
        <col style = 'width:150'/>
        <col style = 'width:100'/>
        <col style = 'width:70'/>
        <col style = 'width:70'/>
        <col style = 'width:70'/>
        <col style = 'width:70'/>
        <col style = 'width:70'/>
        <col style = 'width:70'/>
	<col style = 'width:70'/>
        <col style = 'width:70'/>
      </colgroup>
      <thead>
      	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  			<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
        <tr noWrap='true' class='mainHead'> 
        	 	  
       	  <th noWrap="true">材料名称</th>
      	  <th noWrap="true">规格型号</th>
      	  <th noWrap="true">单位</th>
      	  <th noWrap="true">数量</th>
      	  <th noWrap="true">单价</th>
      	  <th noWrap="true">金额</th>
      	  <th noWrap="true">生产厂家</th>
      	  <th noWrap="true">计划到货日期</th>
	  <th noWrap="true">备注</th>
      	  <th noWrap="true">库房</th>
				</tr> 		      
      </thead>	      
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
	  			<tr>
            <xsl:for-each select="td">          
	      			<xsl:choose>            
      	        <xsl:when test="position()=1   or position()=11">
      	        </xsl:when>
      	        <xsl:when test="position()=5 ">
      	        	<td class="numberText">
					           <xsl:value-of select="format-number(.,'#,##0.00')"/>  
					        </td>
      	        </xsl:when>
      	        <xsl:when test="position()=6 ">
			           	<td class="numberText" >
		                <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
		              </td>
			          </xsl:when>
			          <xsl:when test="position()=7">
			           	<td class="numberText">
		                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
		              </td>
			          </xsl:when>
								<xsl:otherwise>
		  				  	<td align="left">
		    						<xsl:value-of select="."/>
		  						</td>
								</xsl:otherwise>
	      			</xsl:choose>
            </xsl:for-each>
	  			</tr>
        </xsl:for-each>
      </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>