<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	   <thead>
	     <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:right"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
	 	  <tr>
	 			<td rowspan="2" valign="middle">订单编号</td>
        <td rowspan="2" valign="middle">材料名称</td>
        <td rowspan="2" valign="middle">规格型号</td>
   	    <td rowspan="2" valign="middle">计量单位</td>
        <td colspan="2">订单</td>
        <td style="display:none"/>
        <td colspan="2">累计入库</td>
        <td style="display:none"/>
        <td rowspan="2" valign="middle">未入库数量</td>
      </tr>
      <tr noWrap="true" class="mainHead"> 
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
				<td>数量</td>
				<td>金额</td>
				<td>数量</td>
				<td>金额</td>  
				<td style="display:none"/>
      </tr>
	      </thead>	      
	      <tbody>
		      <xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<xsl:for-each select="td">          
		            <xsl:choose>            
		              <xsl:when test="position()= 5 or position()= 6 or position()= 7 or position()= 8 or position()= 9">
		              	<td class="numberText">
		              		<xsl:value-of select="format-number(.,'#,##0.00')"/>
		              	</td>
		              </xsl:when>  
		              <xsl:otherwise>
		              	<td align="left">
		                	<xsl:value-of select="."/>
		                </td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
	      </tbody>
	    
	    <!--bottom>
	      <tr>
	        <td></td>
	        <td></td>
	        <td>.</td>
	      </tr>
	      <tr>
	        <td></td>
	        <td></td>
	        <td></td>
	      </tr>
	      <tr>
	        <td></td>
	        <td></td>
	        <td></td>
	      </tr>
	    </bottom-->
	  </root>
	</xsl:template>
</xsl:stylesheet>