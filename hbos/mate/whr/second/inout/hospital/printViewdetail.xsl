<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
		<root>
		  <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/> 
		  
	    <thead>
	      <tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      </tr>
	  	  <tr noWrap='true' class='mainHead'>
	  	  	<td noWrap="true" rowspan="2" valign="middle">科室编码</td>
	  	  	<td noWrap="true" rowspan="2" valign="middle">科室名称</td>
	  	  	<td noWrap="true" colspan="2">材料领用</td>
	  	  	<td style="display:none">材料领用</td>
	  	  	<td noWrap="true" colspan="2">材料收入</td>
	  	  	<td style="display:none">材料收入</td>
	  	  	<td noWrap="true" colspan="2">差异</td>
	  	  	<td style="display:none">差异</td>
	  	  	<td noWrap="true" colspan="2">差异率</td>
	  	  	<td style="display:none">差异率</td>
	  	  </tr>
	      <tr noWrap="true" class="mainHead">
	      	<td style="display:none">收费项目编码</td>
	      	<td style="display:none">收费项目名称</td> 	
	  			<td noWrap="true">数量</td>
	  			<td noWrap="true">金额</td>
	  			<td noWrap="true">数量</td>
	  			<td noWrap="true">金额</td>
	  			<td noWrap="true">数量</td>
	  			<td noWrap="true">金额</td>
	  			<td noWrap="true">数量</td>
	  			<td noWrap="true">金额</td>
	  	  </tr>
	  	</thead>
	  	<tbody> 
	  	  <xsl:for-each select="/root/tbody/tr">
		      <tr>
		        <xsl:for-each select="td">
		          <xsl:choose>
		            <xsl:when test="position()=1">
			          	<td>
	         					<xsl:value-of select="."/>
	        			  </td>
								</xsl:when>
								<xsl:when test="position()=2">
			          	<td>
		            		<xsl:value-of select="."/>
	        			  </td>
								</xsl:when>
								<xsl:when test="position()=3 or position()=5 or position()=7 ">
		  					  <td align="right">
		  					    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	   					    </td>
								</xsl:when>
								<xsl:when test="position()=9 or position()=10">
		  					  <td align="right">
		  					    <xsl:value-of select="format-number(.,'#,##0.00')"/>%
	   					    </td>
								</xsl:when>
				    		<xsl:otherwise>
				          <td align='right'>
		                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
		              </td>
		            </xsl:otherwise>			                        
		          </xsl:choose>
		        </xsl:for-each>
		      </tr>
		    </xsl:for-each>
	   	</tbody>
	  </root>
  </xsl:template>
</xsl:stylesheet>



