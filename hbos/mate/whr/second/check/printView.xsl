<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
<xsl:template match="/">
  <root>
    <thead>
    	<tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:4;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td noWrap="true" style="fontsize:subtitle;colspan:5;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td noWrap="true" style="fontsize:subtitle;colspan:4;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
	    <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
	  	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <tr noWrap='true' class='mainHead'>
        <td noWrap="true" rowspan="3">材料代码</td>
        <td noWrap="true" rowspan="3">材料名称</td>
        <td noWrap="true" rowspan="3">计量单位</td>
        <td noWrap="true" rowspan="2" colspan="2">期初</td>
        <td style='display:none'/>
        <td noWrap="true" rowspan="2" colspan="2">本月收入</td>
        <td style='display:none'/>
        <td noWrap="true" colspan="4">本月支出</td>
        <td style='display:none'/>
        <td style='display:none'/>
        <td style='display:none'/>
        <td noWrap="true" rowspan="2" colspan="2">本月结存</td>
        <td style='display:none'/>
      </tr>
      <tr noWrap="true" class="mainHead">  	
        <td style='display:none'/>
        <td style='display:none'/>
        <td style='display:none'/>
        <td style='display:none'/>
        <td style='display:none'/>
        <td style='display:none'/>
        <td style='display:none'/>
        <td noWrap="true" colspan="2">出库单</td>
        <td style='display:none'/>
        <td noWrap="true" colspan="2">盘点</td> 			
        <td style='display:none'/>
        <td style='display:none'/>
        <td style='display:none'/>
      </tr>
      <tr noWrap="true" class="mainHead">  	
        <td style='display:none'/>
        <td style='display:none'/>
        <td style='display:none'/>
      	<td noWrap="true">数量</td>
      	<td noWrap="true">金额</td>
      	<td noWrap="true">数量</td>
      	<td noWrap="true">金额</td>
      	<td noWrap="true">数量</td>
      	<td noWrap="true">金额</td>
      	<td noWrap="true">数量</td>
      	<td noWrap="true">金额</td>
      	<td noWrap="true">盘点数量</td>
      	<td noWrap="true">金额</td>
      </tr>
    </thead>	      
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">          
						<xsl:choose>            
						  <xsl:when test="position() = 1">
		            <td nowrap='true'>
		              <xsl:value-of select="."/>               		 	
		            </td>
		          </xsl:when>
							<xsl:when test="position()=4 or position()=6 or position()=8  or position()=10  or position()=12 ">
		            <td class="numberText">
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
		            </td>
			    		</xsl:when>
			    		<xsl:when test=" position()=5 or position()=7 or position()=9 or position()=11 or position()=13">
		            <td class="numberText">
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
		            </td>
			    		</xsl:when>
			    	  <xsl:when test="position()=14 or position()=15 or position()=16">
			    	  </xsl:when>
			    	  <xsl:otherwise>
			    	    <td align='left'>
			    	      <xsl:value-of select="."/>
			    	    </td>
			    	  </xsl:otherwise>			
						</xsl:choose>
					</xsl:for-each>
	      </tr>
      </xsl:for-each>
    </tbody>
  </root>
</xsl:template>
</xsl:stylesheet>