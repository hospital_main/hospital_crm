<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<table>
  	<thead>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		
  		<tr noWrap="true" class="mainHead" >       
				<td style="colspan:6;fontsize:maintitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>  	
  		<tr noWrap="true" class="mainHead">
				<td>材料编码</td>
				<td>材料名称</td>
				<td>计量单位</td>
				<td>规格型号</td>
				<td>材料数量</td>				
				<td>病人数量</td>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
		          <xsl:when test="position() = 5" >
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position() = 6">
		          	  <td align='right'>
		          	  <a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					           javascript:openDialog('detail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:500px;dialogHeight:500px', result)
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>
			          </a>	
			          </td>
		          </xsl:when>
			    <xsl:otherwise>
			          <td>
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>		                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
   	</table>
	</xsl:template>
</xsl:stylesheet>
