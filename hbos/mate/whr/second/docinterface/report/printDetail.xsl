<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<table>
  	<thead>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		
  		<tr noWrap="true" class="mainHead" >       
				<td style="colspan:19;fontsize:maintitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<!--td style="display:none"></td-->
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>	
  		</tr>  	
  		<tr noWrap="true" class="mainHead">
  		
				<td>医嘱日期</td>
				<td>科室编码</td>
				<td>科室名称</td>
				<td>住院号</td>
				<td>医生工号</td>
				<td>病历号</td>
				<td>病人姓名</td>
				<td>材料编码</td>
				<td>材料名称</td>
				<td>规格型号</td>
				<!--td>供货单位</td-->
				<td>计量单位</td>
				<td>采购价格</td>
				<td>收费价格</td>
				<td>数量</td>
				<td>批号</td>
				<td>条形码</td>
				<td>材料属性</td>
				<td>单据号</td>		
				<!--td>是否打印</td-->
				<td>提示</td>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <!--td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td-->
	        <xsl:for-each select="td">
	          <xsl:choose>
			    		<xsl:when test="position()=12 or position()=13">
		           	<td align='right'>
		           		<xsl:if test="../td[1] = '合计'">
		           			<xsl:value-of select="."/>
		           		</xsl:if>
		           		<xsl:if test="../td[1] != '合计'">		           			
	                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
		          		</xsl:if>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=14">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>          	          
			    <xsl:otherwise>
			          <td>
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>		                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
   	</table>
	</xsl:template>
</xsl:stylesheet>
