<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
		<td style="display:none"/>
		<td style="display:none"/>
      		<!--td style="display:none"/-->
      	</tr>
	  		<tr noWrap="true" class="mainHead">
					<td>医嘱日期</td>
					<!--td>科室编码</td-->
					<td>科室名称</td>
					<td>医生工号</td>
					<td>病历号</td>
					<td>病人姓名</td>
					<td>材料编码</td>
					<td>材料名称</td>
					<td>规格型号</td>
					<td>供货单位</td>
					<td>计量单位</td>
					<td>采购价格</td>
					<!--td>收费价格</td-->
					<td>数量</td>
					<td>批号</td>
					<td>条形码</td>
				</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
                </xsl:when>
                <xsl:when test="position()=11 or position()=13">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:when test="position()=12">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>