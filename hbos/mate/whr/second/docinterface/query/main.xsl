<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none" width="25"><input type="checkbox"/></th>
				<th>医嘱日期</th>
				<th>科室编码</th>
				<th>科室名称</th>
				<th>医生工号</th>
				<th>病历号</th>
				<th>病人姓名</th>
				<th>材料编码</th>
				<th>材料名称</th>
				<th>规格型号</th>
				<th>供货单位</th>
				<th>计量单位</th>
				<th>采购价格</th>
				<th>收费价格</th>
				<th>数量</th>
				<th>批号</th>
				<th>条形码</th>
				<th>是否导入</th>
				<!--
				<th>单据号</th>	
				-->
				<th>是否打印</th>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test="position()=5">
		          </xsl:when>
			    		<xsl:when test="position()=12 or position()=13">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=14">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
		          
			    		<xsl:otherwise>
			          <td>
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>		                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>
