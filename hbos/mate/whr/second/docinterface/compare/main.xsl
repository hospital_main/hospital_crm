<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<tr noWrap="true" class="mainHead">
				<th>材料编码</th>
				<th>材料名称</th>
				<th>计量单位</th>
				<th>出库数量</th>
				<th>采购价格</th>
				<th>材料消耗金额</th>
				<th>医嘱数量</th>
				<th>收费价格</th>
				<th>材料收入金额</th>
				<th>差异数量(收-支)</th>
				<th>差异金额(收-支)</th>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
		          <xsl:when test="position() &gt; 3">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
			    		<xsl:otherwise>
			          <td>
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>		                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>
