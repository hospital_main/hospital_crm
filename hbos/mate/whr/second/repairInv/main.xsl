<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>维修日期</th>
				<th>维修科室</th>
				<th>资产名称</th>
				<th>所在科室</th>
				<th>资产卡片号</th>
				<th>材料编码</th>
				<th>材料名称</th>				
				<th>规格型号</th>
				<th>计量单位</th>
				<th>数量</th>
				<th>价格</th>
				<th>金额</th>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
		          <xsl:when test="position()=11 or position()=12">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
			    		<xsl:otherwise>
			          <td>
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>		                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>
