<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">

				<th>出库单号</th>
				<th>仓库</th>
				<th>出库日期</th>
				<th>病历号</th>
				<th>病人姓名</th>
				<th>摘要</th>
				<th>科室</th>
				<th>医生</th>
				<th>材料编码</th>
				<th>材料名称</th>
				<th>规格型号</th>
				<th>数量</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                <xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:when test="position()=12">
 		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0')"/>
		            </td>

              </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
