<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
<xsl:template match="/">
  <root>
    <colgroup>		       
      <col style = 'width:100'/>
      <col style = 'width:140'/>
      <col style = 'width:100'/>
      <col style = 'width:100'/>
      <col style = 'width:80'/>
      <col style = 'width:105'/>
      <col style = 'width:105'/>
      <col style = 'width:105'/>
    </colgroup>
    <thead>
      <tr noWrap='true' class='mainHead'> 
    	<th>材料编码</th>
  	<th>材料名称</th>
  	<th>规格型号</th>
  	<th>批号</th>
  	<th>计量单位</th>
  	<th>数量</th>
  	<th>单价</th>
	<th>金额</th>
      </tr>
    </thead>     
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	<tr>
	  <xsl:for-each select="td">          
	    <xsl:choose>            
  	      <xsl:when test="position()=2 or position()=3 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16">               
              </xsl:when>
              <xsl:when test="position()=8 or position()=9 or position()=10">
                <td align='right' class='moneyCol'>
  	          <xsl:value-of select="format-number(.,'#,##0.00')"/>  
  	        </td>
              </xsl:when>
	      <xsl:otherwise>
	        <td align="left">
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:for-each>
	</tr>
      </xsl:for-each>
    </tbody>
  </root>
</xsl:template>
</xsl:stylesheet>