<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th noWrap='true' width='25'><input type='checkbox'/></th>
        <th noWrap='true'>材料代码</th>
				<th noWrap='true'>材料名称</th>
				<th noWrap='true'>规格型号</th>
				<th noWrap='true'>单位</th>
				<th noWrap='true'>当前库存</th>
				<th noWrap='true'>科室需求数量</th>
				<th noWrap='true'>采购数量</th>
				<th noWrap='true'>计划单价</th>
				<th noWrap='true'>金额</th>
				<th noWrap='true'>计划到货日期</th>
				<th noWrap='true'>采购员</th>
				<th noWrap='true'>生产厂商</th> 
        <th noWrap='true'>供应商</th>
        <th noWrap='true'>需求科室</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=1">
            	 <td >
            	 		<a href="#">
									  <xsl:attribute name="onclick" >
									    window.showModalDialog(window.prefix+'hbos/mate/inv_detail.html?load=&lt;mate_inv_code&gt;<xsl:value-of select="."/>&lt;/mate_inv_code&gt;',window,'dialogWidth:570px;dialogHeight:350px')
									  </xsl:attribute>
									  <xsl:value-of select="."/>
									</a>
                </td>
	            </xsl:when>
              <xsl:when test="position()=2 or position()=3 or position()=14 or position()=18 or position()=19">
	                <td style="display:none">
	                    <xsl:value-of select="."/>
	                </td>	 
	            </xsl:when>                    
	            <xsl:when test="position()=7 or position()=8 or position()=9 or position()=10 or position()=11 ">
	              <td  align="right" class="moneyCol">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>           
              <xsl:otherwise>
                <td align="left">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
	      <xsl:if test="count(/root/total/td)!=0" >      
		      <xsl:for-each select="/root/total">
		      	<tr>
		          <td align='center'></td>
		          <xsl:for-each select="td">
		            <xsl:choose>
		              <xsl:when test="position()=1">
		                <td>合计：</td>
		              </xsl:when>
			            <xsl:when test="position()=2 or position()=3 or position()=14 or position()=18 or position()=19">
			                <td style="display:none">
			                    <xsl:value-of select="."/>
			                </td>	 
			            </xsl:when> 
			            <xsl:when test="position()=9 or position()=11 ">
	              <td  align="right" class="moneyCol">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>             
		              <xsl:otherwise>
		                <td></td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		        </tr>
		      </xsl:for-each>
	      </xsl:if> 
    </tbody>
  </xsl:template>
</xsl:stylesheet>


