<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
<xsl:template match="/">
  <thead>
    <tr noWrap="true" class="mainHead">
      <th style="display:none"><input type="checkbox"/></th>
      <th>送货单号</th>
      <th>制单日期</th>
      <th>供应商</th>
      <th>备注</th>
      <th>材料编码</th>
      <th>材料名称</th>
      <th>单位</th>
      <th>数量</th>
      <th>单价</th>
      <th>审核状态</th>
    </tr>
  </thead>
  <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      	      </xsl:attribute>
    	    </input>
          </td>
        <xsl:for-each select="td">            
          <xsl:choose>
            <xsl:when test="position()=1">
              <td>
             <xsl:value-of select="."/>
              </td>
            </xsl:when>
            <xsl:when test="position()=8">
							<td><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:when>
            <xsl:when test="position()=9">
							<td><xsl:value-of select="format-number(.,'#,##0.0000')"/></td>
            </xsl:when>
            <xsl:otherwise>
              <td><xsl:value-of select="."/></td>
            </xsl:otherwise>
          </xsl:choose>            
  	</xsl:for-each>
      </tr>
    </xsl:for-each>
  </tbody>
</xsl:template>
</xsl:stylesheet>
