<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	    <xsl:variable name="PAYFLAG" select="/root/annex/PAYFLAG"/>
  		<tr class="mainHead">
			<th style='display:none' width='25'><input type='checkbox'/></th>
			<th   nowrap="true">入库单号</th>
			<th   nowrap="true">制单日期</th>
			<th   nowrap="true">入库日期</th>
			<th   nowrap="true">备注</th>
			<th   nowrap="true">仓库</th>
			<th   nowrap="true">业务类型</th>
			<th   nowrap="true">采购类型</th>
			<th   nowrap="true" >订单编号</th>
			<th   nowrap="true">供应商</th>
			<th   nowrap="true">采购员</th>
			<th   nowrap="true">制单人</th>
			<th   nowrap="true">库管员</th>
			<th   nowrap="true">状态</th>
			<th   nowrap="true">金额</th>
			<xsl:if test=" $PAYFLAG =2">
				<th style="display:none">发票号</th>
				<th style="display:none">发票日期</th>
				<th style="display:none">对账年月</th>
			</xsl:if>
			<xsl:if test=" $PAYFLAG !=2">
				<th   nowrap="true">发票号</th>
				<th   nowrap="true">发票日期</th>
				<th   nowrap="true">对账年月</th>
			</xsl:if>
			<th   nowrap="true">申请科室</th>
			<th  style="display:none">flag</th>
			<th   nowrap="true">冲帐状态</th>
			<th   nowrap="true">打印状态</th>
			<th   nowrap="true">出库单查看</th>
  		</tr>
  	</thead>
		<tbody>
	    
  	  <xsl:for-each select="/root/tbody/tr">
  	  	<xsl:variable name="iow_no" select="td[1]"/>
  	  	<xsl:variable name="iow_id" select="./pk/iow_id"/>
  	  	<xsl:variable name="money" select="td[14]"/>
  	      <tr>
  	      	<xsl:if test="td[1]!='合计'">
				<td align='center' style='display:none'>
					<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
						<xsl:attribute name="value" >
							<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
						</xsl:attribute>
					</input>
				</td>
			</xsl:if>
			<xsl:if test="td[1]='合计'">
				<td/>
			</xsl:if>
						
  	        <xsl:for-each select="td">
				<xsl:choose>
					<xsl:when test="position()=19">
						<td style="display:none"><xsl:value-of select="."/></td>
					</xsl:when>
					<xsl:when test="position()=1">
		          		<xsl:if test=". !='合计'">
			          		<td>
			          			<xsl:if test="../td[19]='1'">
									<xsl:attribute name="bgcolor">red</xsl:attribute>
								</xsl:if>
								<a tabindex='-1'><xsl:value-of select="."/></a>
							</td>
			          	</xsl:if>
			          	<xsl:if test=". ='合计'">
			          		<td><xsl:value-of select="."/></td>
			          	</xsl:if>
					</xsl:when>
					<xsl:when test="position()=14">
		            	<td align='right'>
							<xsl:if test="../td[19]='1'">
								<xsl:attribute name="bgcolor">red</xsl:attribute>
							</xsl:if> 
							<xsl:if test="$iow_no !='合计'">
								<a href="#">
									<xsl:attribute name="onclick">
										openDetailItems("&lt;iow_id&gt;<xsl:value-of select="$iow_id"/>&lt;/iow_id&gt;&lt;iow_no&gt;<xsl:value-of select="$iow_no"/>&lt;/iow_no&gt;&lt;money&gt;<xsl:value-of select="format-number($money,$VHMONEYFORMAT)"/>&lt;/money&gt;")
									</xsl:attribute>
									<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
								</a>
							</xsl:if>
							<xsl:if test="$iow_no ='合计'">
								<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
							</xsl:if>
		  		        </td>
					</xsl:when>
					<!--未审核的改红色显示 -->
					<xsl:when test="position()=13">
						<xsl:if test="../td[13]='未审核'">
							<td style='Color:red'>
								<xsl:value-of select="."/>
							</td>
						</xsl:if>
						<xsl:if test="../td[13]!='未审核'">
							<td>
								<xsl:value-of select="."/>
							</td>
						</xsl:if>
					</xsl:when>
					<xsl:when test="position()=15 or position()=16 or position()=17 ">
  			    		<xsl:if test=" $PAYFLAG =2">
							<td style="display:none">
								<xsl:value-of select="."/>  
							</td>
						</xsl:if>
						<xsl:if test=" $PAYFLAG !=2">
							<td>
								<xsl:if test="../td[19]='1'">
									<xsl:attribute name="bgcolor">red</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="."/>  
							</td>
						</xsl:if>
					</xsl:when>
					<xsl:when test="position()=22">
						<td>
							<a tabindex='-1'>
								<xsl:attribute name="href" >
									javascript:selectPage('<xsl:value-of select="../pk/iow_id"/>');
								</xsl:attribute><xsl:value-of select="."/>
							</a>
						</td>						
					</xsl:when>
					<xsl:otherwise>
						<td align='left'>
						<xsl:if test="../td[19]='1'">
						<xsl:attribute name="bgcolor">red</xsl:attribute>
						</xsl:if>
						<xsl:value-of select="."/>
						</td>
					</xsl:otherwise>
				</xsl:choose>
  	        </xsl:for-each>
  	      </tr>
	    </xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>