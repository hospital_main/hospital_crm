<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <tr noWrap='true' class='mainHead'>
        <th width='25'><input type='checkbox'/></th>
        <th></th>
        <th>入库单号</th>
				<th>仓库</th>
				<th>入库时间</th>
				<th>采购员</th>
				<th>票据金额</th>
				<th>优惠金额</th>
      </tr>
    </thead>
    <tbody>
     <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>
          <td align='center'>
            <input type='checkbox'>
	    <xsl:attribute name="check_id">checkbox<xsl:value-of select="$rowindex"/></xsl:attribute>
	    </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=1">
            		<td onclick="changeDenotation(this)">
            			<xsl:attribute name="id">row<xsl:value-of select="$rowindex"/></xsl:attribute>
            			<xsl:attribute name="auto_ids"><xsl:value-of select="../td[8]"/></xsl:attribute>
	            		<xsl:attribute name="iow_id"><xsl:value-of select="../td[1]"/></xsl:attribute>+</td>
              </xsl:when>
              <xsl:when test="position()=2">
                <td>
                	<xsl:value-of select="."/>
                </td>
              </xsl:when>
	            <xsl:when test="position()=6">
	              <td  align="right">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>


		     <xsl:when test=" position()=7 ">
              	<td align="right">
              		<xsl:attribute name="id">row_favourable_id<xsl:value-of select="$rowindex"/></xsl:attribute>
              		<xsl:value-of select="."/>
              	</td>
	            </xsl:when>



	            <xsl:when test=" position()=8 ">
              	<td style="display:none" >
              		<xsl:attribute name="id">row_auto_id<xsl:value-of select="$rowindex"/></xsl:attribute>
              		<xsl:value-of select="."/>
              	</td>
	            </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			<tr style="display:none">
  				<xsl:attribute name="id">annexrow<xsl:value-of select="$rowindex"/></xsl:attribute>
  				<td style="display:none" align='center'>
          </td>
          <td>
          </td>
          <td>
          </td>
          <td colspan="5">
          	<xsl:attribute name="id">revelation<xsl:value-of select="$rowindex"/></xsl:attribute>
          </td>
  			</tr>
      </xsl:for-each>
      <xsl:if test="count(/root/total/td)!=0" >      
        <xsl:for-each select="/root/total">
        	<tr>
            <td align='center'></td>
            <td align='center'></td>
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=2">
                  <td>合计：</td>
                </xsl:when>
                <xsl:when test="position()=1">
              </xsl:when>
                
                <xsl:when test="position()=6">	 
                  <td  align="right">
                    <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                  </td>
                </xsl:when>
                
                <xsl:when test="position()=8">
              	</xsl:when>				            
                <xsl:otherwise>
                  <td></td>
                </xsl:otherwise>
              </xsl:choose>
      		  </xsl:for-each>
          </tr>
        </xsl:for-each>
      </xsl:if>    	
    </tbody>
  </xsl:template>
</xsl:stylesheet>


