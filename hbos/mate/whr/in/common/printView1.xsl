<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
    <colgroup>     
      <col style = 'width:140'/>  
      <col style = 'width:140'/>
      <col style = 'width:130'/>
      <col style = 'width:40'/>
      <col style = 'width:40'/>
      <col style = 'width:70'/>
      <col style = 'width:85'/>
      <col style = 'width:70'/>
      <col style = 'width:60'/>
      <col style = 'width:90'/>
      <col style = 'width:90'/>
    </colgroup>
    <thead>
    <!--开始############addddddddd-->
    <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
    <xsl:variable name="RowS" select="/root/annex/endRow"/>
    <xsl:variable name="endRow" select="count(/root/tbody/tr) mod $RowS"/>
    <xsl:variable name="rowlast" select="count(/root/tbody/tr)"/>
    <xsl:variable name="allmoney" select="format-number(sum(/root/tbody/tr/td[10]),$VHMONEYFORMAT)"/>
    <!--结束############addddddddd-->
      <tr noWrap='true' class='mainHead'>
      	<th noWrap='true' valign="middle">材料编码</th>
        <th  noWrap='true' valign="middle">材料名称</th>
        <th  noWrap='true' valign="middle">规格型号</th>
        <th noWrap='true'>单位</th>
        <th noWrap='true'>数量</th>
        <th noWrap='true'>单价</th>
        <th noWrap='true' valign="middle">金额</th>
        <th noWrap='true' valign="middle">生产批号</th>
        <th noWrap='true' valign="middle">条形码</th>
        <th noWrap='true' valign="middle">院内条码</th>
        <th noWrap='true' valign="middle">备注</th>
      </tr>
    </thead>     
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>
          <xsl:for-each select="td">          
            <xsl:choose>            
              <xsl:when test="position()=1  or position()=3 or position()=6 or position()=11 or position()=12 or position()=13 or position()=14 or position()=16 or position()=17 or position()=18 or position()=19 or position()=20  or position()=23">  
              </xsl:when>
              <xsl:when test="position()=4">
                <td align="left">
                  <xsl:value-of select="substring(.,1,10)"/>
                </td>
              </xsl:when>
							<xsl:when test="position()=5">
                <td align="left">
                  <xsl:value-of select="substring(.,1,11)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=8">
                <td class="numberText">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              
              <xsl:when test="position()=9 ">
                <td class="numberText">
                  <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
                </td>
              </xsl:when>
              <xsl:when test=" position()=10">
                <td class="numberText">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=15">
                <td align="left">
                  <xsl:value-of select="substring(.,1,12)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=21">
                <td align="left">
                  <xsl:value-of select="substring(.,1,10)"/>
                </td>
              </xsl:when>
              <!--xsl:when test="position()=14">
                <td align="left">
                  <xsl:value-of select="substring(../td[12],string-length(../td[12]) - 7,8)"/>
                </td>
              </xsl:when-->
              <xsl:otherwise>
                <td align="left">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
        <xsl:if test="position() mod $RowS = 0">
	  			<tr SumTitle="合计：">
	  				<td class="numberText" colspan="10"  style="font-weight: bold;" >本页合计：
			      <xsl:value-of select="format-number(sum(/root/tbody/tr[ position() > $rowindex - $RowS and  position() &lt; $rowindex+1 ]/td[10]),$VHMONEYFORMAT)"/>
			      总计：
			      <xsl:value-of select="$allmoney"/>
			      </td>
	  			</tr>
	  		</xsl:if>
      </xsl:for-each>
      
      <!--开始############addddddddd-->
      <xsl:if test="$endRow>0">
        <xsl:call-template  name="repeat">  
           <xsl:with-param  name="times"  select="$RowS - $endRow"  />    
        </xsl:call-template>  
      </xsl:if>
     <!--结束############addddddddd-->
      <xsl:if test="count(/root/total/td)!=0" >      
        <xsl:for-each select="/root/total">
          <tr SumTitle="合计：">
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td></td>
                </xsl:when>
                <!--xsl:when test="position()=2  or position()=8 or position()=9 or position()=10 or position()=11">  
                </xsl:when-->
                <xsl:when test="position()=1  or position()=3 or position()=6 or position()=11 or position()=12 or position()=13 or position()=14 or position()=16 or position()=17 or position()=18 or position()=19 or position()=20  or position()=23">  
              	</xsl:when>
                <xsl:when test="position()=13">  
                  <td  class="numberText">
                    <xsl:value-of select="format-number(.,'#,##0.0000')"/>
                  </td>
                </xsl:when>             
                <xsl:otherwise>
                  <td></td>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </tr>
        </xsl:for-each>
      </xsl:if> 
      
      <xsl:if test="string(number($endRow))='NaN'">
	      <tr SumTitle="合计：">
					<td class="numberText" colspan="10" style="font-weight: bold;">本页合计：
						<xsl:value-of select="format-number(sum(/root/tbody/tr/td[10]),$VHMONEYFORMAT)"/>
						总计：
						<xsl:value-of select="$allmoney"/>
					</td>
	      </tr>
      </xsl:if >
      <xsl:if test="string(number($endRow))!='NaN'">
	      <xsl:if test="$endRow!=0 ">
		      <tr SumTitle="合计：">
						<td class="numberText" colspan="10" style="font-weight: bold;">本页合计：
							<xsl:value-of select="format-number(sum(/root/tbody/tr[ position() > $rowlast - $endRow  and  position() &lt; $rowlast+1 ]/td[10]),$VHMONEYFORMAT)"/>
							总计：
							<xsl:value-of select="$allmoney"/>
						</td>
		      </tr>
	      </xsl:if>
      </xsl:if >
    </tbody>
  </root>
  </xsl:template>
  <!--开始############addddddddd-->
   <xsl:template  name="repeat">  
         <xsl:param  name="times"  select="0"  />  
         <xsl:if  test="$times  >  0">  
            <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
            <xsl:call-template  name="repeat">  
              <xsl:with-param  name="times"  select="$times  -  1"  />  
            </xsl:call-template>  
         </xsl:if>  
   </xsl:template>
   <!--结束############addddddddd-->
</xsl:stylesheet>