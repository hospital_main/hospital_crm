<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true">类型</th>
				<th nowrap="true">单据号</th>
				<th nowrap="true">日期</th>
			<!--	<th nowrap="true">物资编码</th> -->
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	                <xsl:when test="position()=2">
	                	
                    <xsl:if test="../td[1]='需求计划'">
                     <td>
	                     	<a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('xqjh_mx.html?load=&lt;id&gt;<xsl:value-of select="../pk"/>&lt;/id&gt;','dialogWidth:950px;dialogHeight:1000px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
									  </td>
									  </xsl:if>
									   <xsl:if test="../td[1]='汇总需求'">
                     <td>
	                     	<a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('xqjh_mx.html?load=&lt;id&gt;<xsl:value-of select="../pk"/>&lt;/id&gt;','dialogWidth:950px;dialogHeight:1000px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
									  </td>
									  </xsl:if>
									   <xsl:if test="../td[1]='采购计划'">
                     <td>
	                     	<a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('cgjh_mx.html?load=&lt;id&gt;<xsl:value-of select="../pk"/>&lt;/id&gt;','dialogWidth:950px;dialogHeight:1000px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
									  </td>
									  </xsl:if>
									  <xsl:if test="../td[1]='采购订单'">
                     <td>
	                     	<a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('cgdd_mx.html?load=&lt;id&gt;<xsl:value-of select="../pk"/>&lt;/id&gt;','dialogWidth:950px;dialogHeight:1000px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
									  </td>
									  </xsl:if>
									  	  <xsl:if test="../td[1]='材料入库'">
                     <td>
	                     	<a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('update.html?load=&lt;id&gt;<xsl:value-of select="../pk"/>&lt;/id&gt;','dialogWidth:950px;dialogHeight:1000px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
									  </td>
									  </xsl:if>
									  	  <xsl:if test="../td[1]='材料出库'">
                     <td>
	                     	<a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('kslyck_mx.html?load=&lt;id&gt;<xsl:value-of select="../pk"/>&lt;/id&gt;','dialogWidth:950px;dialogHeight:1000px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
									  </td>
									  </xsl:if>
									  <xsl:if test="../td[1]='材料移出库' or ../td[1]='材料移入库'">
                     <td>
	                     	<a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('yk.html?load=&lt;id&gt;<xsl:value-of select="../pk"/>&lt;/id&gt;','dialogWidth:950px;dialogHeight:1000px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
									  </td>
									  </xsl:if>
									   <xsl:if test="../td[1]='采购发票'">
                     <td>
	                     	<a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('../../../pay/invoice/update.html?load=&lt;id&gt;<xsl:value-of select="../pk"/>&lt;/id&gt;','dialogWidth:950px;dialogHeight:1000px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
									  </td>
									  </xsl:if>
									   <xsl:if test="../td[1]='采购付款'">
                     <td>
	                     	<a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('../../../pay/pay/pay/update.html?load=&lt;id&gt;<xsl:value-of select="../pk"/>&lt;/id&gt;','dialogWidth:900px;dialogHeight:900px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
									  </td>
									  </xsl:if>																  								  
              </xsl:when>             
			    <xsl:otherwise>
							   	<td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>