<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
<xsl:template match="/">
  <thead>
    <tr noWrap="true" class="mainHead">
      <th>材料编码</th>
      <th>材料名称</th>
      <th>规格型号</th>
      <th>数量</th>
      <th>计划单价</th>
      <th>金额</th>
      <th>计划到货日期</th>
      <th>采购员</th>
    </tr>
  </thead>
  <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        <xsl:for-each select="td">            
          <xsl:choose>
            <xsl:when test="position()=1">
              <td>
              <a tabindex='-1'><xsl:value-of select="."/></a>
              </td>
            </xsl:when>
            <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6">
	       <td align="right" noWrap='true' >
                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
               </td>
	    </xsl:when> 
            <xsl:otherwise>
              <td><xsl:value-of select="."/></td>
            </xsl:otherwise>
          </xsl:choose>            
  	</xsl:for-each>
      </tr>
    </xsl:for-each>
  </tbody>
</xsl:template>
</xsl:stylesheet>
