<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
      <th style="display:none"><input type="checkbox" /></th>
				<th>材料编码</th>
				<th style='display:none'>物资类别</th>
				<th>材料名称</th>
				<th>规格型号</th>
				<th>包装规格</th>
				<th>计量单位</th>
				<th>上期数量</th>
				<th>单价</th>
				<th>有效日期</th>
				<!--th>存放仓库名称</th-->
				<th style='display:none'>零售单价</th>
				<th style='display:none'>零售金额</th>
				<th style='display:none'>是否批管理</th>
  		</tr>
  	</thead>
  	<tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        <td align='center'  style='display:none'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	          <xsl:attribute name="value" >
	            <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	  	      </xsl:attribute>
    	    </input>
        </td>
        <xsl:for-each select="td">
        	<xsl:if test="position() = 2 or position() = 10 or position() = 11 or position() = 12 ">      	
	          <td style='display:none'><xsl:value-of select="."/></td>
          </xsl:if> 
        	<xsl:if test="position() = 7">      	
	          <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
          </xsl:if>
          <xsl:if test="position() = 8">      	
	          <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
          </xsl:if>
          <xsl:if test="position() != 2 and position() != 7 and position() != 8 and position() != 10 and position() != 11 and position() != 12">      	
	          <td><xsl:value-of select="."/></td>
          </xsl:if>
  			</xsl:for-each>
      </tr>
    </xsl:for-each>
	</tbody>  	
	</xsl:template>
</xsl:stylesheet>