<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
<xsl:template match="/">
  <root>
	<thead>
		<tr noWrap="true" class="mainHead">
			<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			
		</tr>
		<tr noWrap='true' class='mainHead'>
		  
			<td noWrap="true" style="border:thin;fontsize:coltitle">序号      </td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">供应商编码</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">供应商名称</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">供应商小计</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">物资分类  </td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">材料编码  </td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">材料名称  </td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">规格型号  </td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">发票号    </td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">对账年月  </td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">业务类型  </td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">数量      </td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">单价      </td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">金额      </td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">入库单号  </td>
			
		</tr>
	</thead>	      
	<tbody>
				<xsl:for-each select="/root/tbody/tr">
			<tr>
						<td align="center">
							<xsl:value-of select="position()"/>
						</td>           
				<xsl:for-each select="td">          
					<xsl:choose> 
						
						<xsl:when test="position()=3 or position()=13 or position()=12">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
						</xsl:when>
						<xsl:when test=" position()=12">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0')"/>
							</td>
						</xsl:when>
						<xsl:when test=" position()=14">
							<td>
								<a href="#">
									<xsl:attribute name="onclick">
										openDialog('../../../whr/in/common/update.html?load=&lt;iow_id&gt;<xsl:value-of select="../td[15]"/>&lt;/iow_id&gt;','dialogWidth:1000px;dialogHeight:650px')
									</xsl:attribute>
									<xsl:value-of select="."/>	
								</a>
							</td>
						</xsl:when>
						<xsl:when test=" position()=15">
						</xsl:when>
						<xsl:otherwise>
							<td align='left'>
								<xsl:value-of select="."/>
							</td>
						</xsl:otherwise>			
					</xsl:choose>
				</xsl:for-each>
				
			</tr>
		</xsl:for-each>
		<tr><td>合计</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
		<td align="right">
			<xsl:value-of select="format-number(sum(//tr/td[13]),'#,##0.00')"/>
		</td><td></td></tr>

	</tbody>
  </root>
</xsl:template>
</xsl:stylesheet>