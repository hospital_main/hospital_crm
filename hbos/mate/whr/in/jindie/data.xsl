<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	    <xsl:variable name="PAYFLAG" select="/root/annex/PAYFLAG"/>
  		<tr class="mainHead">
        <th noWrap="true">序号</th>
				<th noWrap="true">供应商编码</th>
				<th noWrap="true">供应商名称</th>
				<th noWrap="true">供应商小计</th>
				<th noWrap="true">物资分类</th>
				<th noWrap="true">材料编码</th>
				<th noWrap="true">材料名称</th>
				<th noWrap="true">规格型号</th>
				<th noWrap="true">发票号</th>
				<th noWrap="true">对账年月</th>
				<th noWrap="true">业务类型</th>
				<th noWrap="true">数量</th>
				<th noWrap="true">单价</th>
				<th noWrap="true">金额</th>
				<th noWrap="true">入库单号</th>
				
  		</tr>
  	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
						<td align="center">
							<xsl:value-of select="position()"/>
						</td>           
				<xsl:for-each select="td">          
					<xsl:choose> 
						
						<xsl:when test="position()=3 or position()=13 or position()=12">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
						</xsl:when>
						<xsl:when test=" position()=12">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0')"/>
							</td>
						</xsl:when>
						<xsl:when test=" position()=14">
							<td>
								<a href="#">
									<xsl:attribute name="onclick">
										openDialog('update.html?load=&lt;iow_id&gt;<xsl:value-of select="../td[15]"/>&lt;/iow_id&gt;','dialogWidth:1150px;dialogHeight:650px')
									</xsl:attribute>
									<xsl:value-of select="."/>	
								</a>
							</td>
						</xsl:when>
						<xsl:when test=" position()=15">
						</xsl:when>
						<xsl:otherwise>
							<td align='left'>
								<xsl:value-of select="."/>
							</td>
						</xsl:otherwise>			
					</xsl:choose>
				</xsl:for-each>
				
			</tr>
		</xsl:for-each>
		<tr><td>合计</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
		<td align="right">
			<xsl:value-of select="format-number(sum(//tr/td[13]),'#,##0.00')"/>
		</td><td></td></tr>
	</tbody>
	</xsl:template>
</xsl:stylesheet>