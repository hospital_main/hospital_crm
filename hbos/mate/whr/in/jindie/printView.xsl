<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
<xsl:template match="/">
  <root>
	<thead>
		<tr noWrap="true" class="mainHead">
			<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
		</tr>
		<tr noWrap='true' class='mainHead'>
			<td noWrap="true" style="border:thin;fontsize:coltitle">单据日期</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">单据号</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">单位名称</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">发票号</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">对帐年月</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">自由项</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">HBOS代码</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">商品代码</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">商品名称</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">规格型号</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">辅助属性</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">计量单位</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">实收数量</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">单位成本</td>
			<td noWrap="true" style="border:thin;fontsize:coltitle">成本</td>
		</tr>
	</thead>	      
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">          
					<xsl:choose>            
						<xsl:when test="position()=13 or position()=14 or position()=15">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>  
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td align='left'>
								<xsl:value-of select="."/>
							</td>
						</xsl:otherwise>			
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
  </root>
</xsl:template>
</xsl:stylesheet>