<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th noWrap="true">材料编码</th>
				<th noWrap="true">材料名称</th>
				<th noWrap="true">规格型号</th>
				<th noWrap="true">仓库</th>
				<th noWrap="true">业务类型</th>
				<th noWrap="true">采购类型</th>
				<th noWrap="true" >数量</th>
				<th noWrap="true" >单价</th>
				<th noWrap="true" >金额</th>
				<th noWrap="true" >入库时间</th>
				<th noWrap="true">采购员</th>
				<th noWrap="true">制单人</th>
				<th noWrap="true">库管员</th>
				<th noWrap="true">状态</th>
				<th noWrap="true">入库单号</th>
				<th noWrap="true">备注</th>
				<th noWrap="true">批号</th>
				<th noWrap="true">院内条码</th>
				<th noWrap="true">发票号</th>
				<th noWrap="true">供应商</th>
				<th  style="display:none">flag</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=21">
								<td style="display:none"><xsl:value-of select="."/></td>
							</xsl:when>
            	<xsl:when test="position()=8">
                <td align="right">
                	<xsl:if test="../td[22]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
									<xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=9">
                <td align="right">
                	<xsl:if test="../td[22]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
									<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=7">
                <td align="right">
                	<xsl:if test="../td[22]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td>
                	<xsl:if test="../td[22]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>