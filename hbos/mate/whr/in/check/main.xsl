<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
<xsl:template match="/">
  <thead>
    <tr class="mainHead">
      <th ><input type="checkbox"/></th>
      <th>送货单号</th>
      <th>制单日期</th>
      <th>供应商</th>
      <th>备注</th>
      <th>总金额</th>
      <th>验收人</th>
      <th>验收状态</th>
      <th>单据类型</th>
      <th>物流状态</th>
    </tr>
  </thead>
  <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      	      </xsl:attribute>
    	    </input>
          </td>
        <xsl:for-each select="td">            
          <xsl:choose>
            <xsl:when test="position()=1">
              <td>
              <a tabindex='-1'><xsl:value-of select="."/></a>
              </td>
            </xsl:when>
            <xsl:when test="position()=5">
							<td><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:when>
            <xsl:otherwise>
              <td><xsl:value-of select="."/></td>
            </xsl:otherwise>
          </xsl:choose>            
  			</xsl:for-each>
      </tr>
    </xsl:for-each>
  </tbody>
</xsl:template>
</xsl:stylesheet>
