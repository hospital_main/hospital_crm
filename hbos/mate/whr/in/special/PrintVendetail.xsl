<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <table>
  	<thead>
  		<tr noWrap="true" class="mainHead" >
  		<td style="colspan:colcount;fontsize:maintitle"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
	
			
  		</tr>
  	   	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	    <td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	    <td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>	
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	    <td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>	
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	
  	  </tr>  
  	    <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	    <td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	    <td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>	
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	    <td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>	
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	
  	  </tr>  	
  		<tr noWrap='true' class='mainHead'>  
		      <td noWrap="true">材料编码</td>
	  	  	<td noWrap="true">材料名称</td>
	  	  	<td noWrap="true">规格型号</td>
	  	  	<td noWrap="true">仓库名称</td>
	  	  	<td noWrap="true">业务类型</td>
	  	  	<td noWrap="true">采购类型</td>	
	  	  	<td noWrap="true">数量</td>
	  	  	<td noWrap="true">单价</td>
	  	  	<td noWrap="true">金额</td>	
	  	  	<td noWrap="true">入库时间</td>	
	  	  	<td noWrap="true">采购员</td>	
	  	  	<td noWrap="true">制单人</td>	
	  	  	<td noWrap="true">库管员</td>	
	  	  	<td noWrap="true">状态</td>	
	  	  	<td noWrap="true">入库单号</td>	
	  	  	<td noWrap="true">备注</td>	
	  	  	<td noWrap="true">发票号</td>	
	  	  	<td noWrap="true">对账年月</td>	
	  	  	<td noWrap="true">供应商</td>
	  	  	<td noWrap="true">住院号</td>	
	  	  	<td noWrap="true">床位号</td>	
	  	  	<td noWrap="true">病人姓名</td>	
	  	  	<td noWrap="true">病人性别</td>	
	  	  	<td noWrap="true">医嘱医生</td>	
     </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>     			
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
              	 <td><xsl:value-of select="."/></td>
								
              </xsl:when>
              
               <xsl:when test="position()=25">
              	 <td style='display:none'><xsl:value-of select="."/></td>
								
              </xsl:when>

              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</table>	
 	</xsl:template>
</xsl:stylesheet>


