<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th nowrap="true">入库单号</th>
				<th nowrap="true">制单日期</th>
				<th nowrap="true">入库日期</th>
				<th nowrap="true">摘要</th>
				<th nowrap="true">仓库</th>
				<th nowrap="true">业务类型</th>
				<th nowrap="true">采购类型</th>
				<th nowrap="true">供应商名称</th>
				<th nowrap="true">采购员</th>
				<th nowrap="true">制单人</th>
				<th nowrap="true">审核员</th>
				<th nowrap="true">库管员</th>
				<th nowrap="true">状态</th>
				<th noWrap="true">金额</th>
				<th noWrap="true">冲账状态</th>
				<th noWrap="true">打印状态</th>
				<th noWrap="true">出库单查看</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <xsl:for-each select="/root/tbody/tr">
  	      <tr>
  	      	<xsl:if test="td[1]!='合计'">
		          <td align='center'  style='display:none'>
		            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
		              <xsl:attribute name="value" >
		                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		      			  </xsl:attribute>
		      			    <xsl:attribute name="iow_code" >
		               <xsl:value-of select="./td[1]"/>
		      			  </xsl:attribute>
		    			  </input>
		          </td>
	          </xsl:if>
						<xsl:if test="td[1]='合计'">
							<td/>
						</xsl:if>
  	        <xsl:for-each select="td">
  	          <xsl:choose>
  	          	<xsl:when test="position()=1">
		          		<xsl:if test=". !='合计'">
			          		<td><a tabindex='-1'><xsl:value-of select="."/></a></td>
			          	</xsl:if>
			          	<xsl:if test=". ='合计'">
			          		<td><xsl:value-of select="."/></td>
			          	</xsl:if>
				    		</xsl:when>
  			    		<xsl:when test="position()=14">
  		            <td align='right'>
  		              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
  		            </td>
  			    		</xsl:when>
  			    		<!--如果状态是未审核，改变状态颜色为红色-->
              	<xsl:when test="position()=13">
								<xsl:if test="../td[13]='未审核'">
									<td style='Color:red'>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="../td[13]!='未审核'">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=17">
						<td>
							<a tabindex='-1'>
								<xsl:attribute name="href" >
									javascript:selectPage('<xsl:value-of select="../pk/iow_id"/>');
								</xsl:attribute><xsl:value-of select="."/>
							</a>
						</td>						
					</xsl:when>
  			    		<xsl:otherwise>
  			          <td align='left'>
  	                <xsl:value-of select="."/>
  	              </td>
  	            </xsl:otherwise>
  	          </xsl:choose>
  	        </xsl:for-each>
  	      </tr>
	    </xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>