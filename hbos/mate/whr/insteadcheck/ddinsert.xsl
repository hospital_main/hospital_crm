<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th><input type="checkbox"/></th>
				<th>材料代码</th>
				<th>材料名称</th>
				<th>规格型号</th>
				<th>计量单位</th>
				<th>生产厂商</th>
				<th>供应商</th>
				<th>货位</th>
				<th>批号</th>
				<th>有效期</th>
				<th>账面数量</th>
				<th>单价</th>
				<th>条形码</th>
				<th>金额</th>
    	</tr>
  	</thead>
  	<tbody>
  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">   
          <xsl:choose>         
            <xsl:when test="position()=10">
	              <td noWrap="true" align="right">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=11">
	              <td noWrap="true" align="right">
                  <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=13">
	              <td noWrap="true" align="right">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>
	             <xsl:when test="position()=14 or position()=15">
	              <td style="display:none"><xsl:value-of select="."/></td>
	            </xsl:when>
	            <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
              <!--<xsl:when test="position()=4  or position()=7 or position()=8 ">
                <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>-->
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
