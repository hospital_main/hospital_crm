<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
      <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
      <tr noWrap="true" class="mainHead">
      	<td>材料编码</td>
      	<td>材料名称</td>
      	<td>规格型号</td>
      	<td>货位号</td>
      	<td>批号</td>
      	<td>计量单位</td>
      	<td>有效期</td>
      	<td>单价</td>
      	<td>条形码</td>
      	<td>供应商</td>
      	<td>金额</td>
      	<td>账面数量</td>
      	<td>盘点数量</td>
      	<td>盘点金额</td>
      	<td>盈亏数量</td>
      	<td>盈亏金额</td>
      	<td>盈亏说明</td>
      </tr>	      
      </thead>	      
<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=8 ">
	              <td noWrap="true" align="right">
                  <xsl:value-of select="format-number(.,'#,##0.0000')"/>
                </td>
	            </xsl:when>
	            	            
              <xsl:when test=" position()=11 or position()=12 or position()=14 or position()=13 or position()=15 or position()=16">
	              <td noWrap="true" align="right">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=18 or position()=19 or position()=20 or position()=21">
	            </xsl:when>
              <xsl:otherwise>
                <td noWrap="true">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
      <tr>
	    	<td>合计：</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td align='right'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[11]),'#,##0.00')"/></td>
				<td></td>
				<td></td>
				<td align='right'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[14]),'#,##0.00')"/></td>
				<td></td>
				<td align='right'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[16]),'#,##0.00')"/></td>
				<td></td>
			</tr>
    </tbody>
      <tfoot>
    	<tr noWrap="true" class="mainHead">
    		<td noWrap="true" style="fontsize:foot;colspan:6;"/>
    		<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
			<td style="display:none"/>
  	  		<td noWrap="true" style="fontsize:foot;colspan:6;"/>
    		<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
			<td style="display:none"/>
			<td noWrap="true" style="fontsize:foot;colspan:5;"/>
    		<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
  	  		<td style="display:none"/>
    	</tr>
    </tfoot>
    </root>   
  </xsl:template>
</xsl:stylesheet>