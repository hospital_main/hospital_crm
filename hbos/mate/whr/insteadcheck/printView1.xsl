<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <colgroup>		       
		      <col style = 'width:100'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:80'/>
		      <col style = 'width:105'/>
		    </colgroup>
	      <thead>
    		  <tr noWrap='true' class='mainHead'> 
    		    <th noWrap="true">材料代码</th>
    				<th noWrap="true">材料名称</th>
    				<th noWrap="true">规格型号</th>
    				<th noWrap="true">货位号</th>
    				<th noWrap="true">批号</th>
    				<th noWrap="true">计量单位</th>
    				<th noWrap="true">有效期</th>
    				<th noWrap="true">单价</th>
    				<th noWrap="true">条形码</th>
    				<th noWrap="true">供应商</th>
    				<th noWrap="true">金额</th>
    				<th noWrap="true">帐面数量</th>
    				<th noWrap="true">盘点数量</th>
    				<th noWrap="true">盘点金额</th>
    				<th noWrap="true">盈亏数量</th>
    				<th noWrap="true">盈亏金额</th>
    				<th noWrap="true">盈亏说明</th>	
    		  </tr>
    		</thead>     
	      <tbody>
	      <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
        <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
		      <xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<xsl:for-each select="td">          
		            <xsl:choose>
                  <xsl:when test="position()=13 or position()=12 or position()=15">
	                  <td class="numberText">
		              		<xsl:value-of select="format-number(.,'#,##0.00')"/>
		              	</td>
		              </xsl:when>
		              <xsl:when test="position()=8">
	                  <td class="numberText">
		              		<xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
		              	</td>
		              </xsl:when>
		              <xsl:when test="position()=11 or position()=14 or position()=16">
	                  <td class="numberText">
		              		<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
		              	</td>
		              </xsl:when>
		              <xsl:otherwise>
		              	<td align="left">
		                	<xsl:value-of select="."/>
		                </td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
  		  </tbody>
	   
	  </root>
	</xsl:template>
</xsl:stylesheet>