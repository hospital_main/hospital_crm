<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th><input type="checkbox"/></th>
        <th style="display:none">业务类型</th>
				<th>材料代码</th>
				<th>材料名称</th>
				<th>规格型号</th>
				<th>计量单位</th>
				<th>生产厂商</th>
				<th>供应商</th>
    	</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=5">
                <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
