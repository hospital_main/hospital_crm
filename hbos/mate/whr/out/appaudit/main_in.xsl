<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=" "/>
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
  			<th nowrap="true">库房编码</th>
  			<th nowrap="true">库房名称</th>
				<th nowrap="true">材料编码</th>
				<th nowrap="true">材料名称</th>
				<th nowrap="true">规格</th>
				<th nowrap="true">数量</th>
				
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test="position()=6 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0')"/>
		            </td>
			    		</xsl:when>
	            <xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>