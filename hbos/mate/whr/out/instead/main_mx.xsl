<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
        <th nowrap="true">物资编码</th>
				<th nowrap="true">物资名称</th>
				<th nowrap="true">规格型号</th>
				<th nowrap="true">出库类型</th>
				<th nowrap="true">仓库</th>
				<th nowrap="true">数量</th>
				<th nowrap="true">单价</th>
				<th nowrap="true">金额</th>
				<th nowrap="true">批号</th>
				<th nowrap="true">条形码</th>
				<th nowrap="true">领料科室</th>
				<th nowrap="true">领料人</th>
        <th nowrap="true">出库单号</th>
				<th nowrap="true">状态</th>
				<th nowrap="true">日期</th>
				<th nowrap="true">摘要</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=7">
                <td align="right">
									<xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=8">
                <td align="right">
									<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=6">
                <td align="right">
                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              
                 <xsl:when test="position()=14">
                <xsl:if test="../td[14]='未审核'">
		            <td align='left' style='Color:red'>	            	
		              <xsl:value-of select="../td[14]"/>  
		            </td>
		          </xsl:if>
		          <xsl:if test="../td[14]!='未审核'">
								<td>
									<xsl:value-of select="../td[14]"/>
								</td>
							</xsl:if>
			    		</xsl:when>
	            <xsl:otherwise>
                <td noWrap="true">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>