<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		  <tr noWrap='true' class='mainHead'> 
		  	<th width='25'><input type='checkbox'/></th>
		  	<th>材料代码</th>
		    <th>材料名称</th>
		    <th>规格型号</th>
		    <th>计量单位</th>
		    <th>批号</th>
		    <th>条形码</th>
		    <th>当前库存</th>
		    <th>数量</th>
		  </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=7 ">
	              <td  align="right" class="moneyCol" noWrap="true">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=8">
	              <td  align="right" class="moneyCol" noWrap="true">
                  <xsl:value-of select="format-number(.,'##0.00')"/>
                </td>
	            </xsl:when>
	            <xsl:otherwise>
                <td noWrap="true">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>