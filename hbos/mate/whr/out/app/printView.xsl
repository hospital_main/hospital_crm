<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <colgroup>		       
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
      </colgroup>
      <thead>
        <tr noWrap='true' class='mainHead'>  	  
          <th>材料编码</th>
					<th>材料名称</th>
					<th>规格型号</th>
					<th>生产厂商</th>
					<th>计量单位</th>
					<th>请领数量</th>
					<th>处理数量</th>
					<th>已处理数量</th>
					<th>单价</th>
					<th>金额</th>
					<th>处理结果</th>
				</tr>
      </thead>	      
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
	  			<tr>
            <xsl:for-each select="td">          
				      <xsl:choose>            
								<xsl:when test="position()= 1">
								  <td align="left">
								    <xsl:value-of select="."/>
								  </td>
								</xsl:when>  
								<xsl:otherwise>
								  <td align="left">
								    <xsl:value-of select="."/>
								  </td>
								</xsl:otherwise>
				      </xsl:choose>
            </xsl:for-each>
	  			</tr>
        </xsl:for-each>
      </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>