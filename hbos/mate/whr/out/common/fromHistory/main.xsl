<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
      <th style="display:none"><input type="checkbox" /></th>
				<th>材料编码</th>
				<th>材料名称</th>
				<th>规格型号</th>
				<th>包装规格</th>
				<th>计量单位</th>
				<th>上期耗用量</th>
				<th>批号</th>
				<th>条码</th>
				<th>库存</th>
				<th>单价</th>
				<th >日期</th>
				<th>即时库存</th>
				<th style="display:none">金额</th>
				<th style="display:none">零售单价</th>
				<th style="display:none">货位名称</th>
				<th style="display:none">货位编码</th>
				<th style="display:none">物资类别</th>
				<th>生产厂商</th>
  		</tr>
  	</thead>
  	<tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        <td align='center'  style='display:none'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	          <xsl:attribute name="value" >
	            <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	  	      </xsl:attribute>
    	    </input>
        </td>
        
        <xsl:for-each select="td"> 
        	<xsl:choose>
        		 <xsl:when test=" position()=13">
                <td style="display:none"><xsl:value-of select="."/></td>
             </xsl:when>
             <xsl:when test="position()=14 or position()=15 or position()=16 or position()=17">
                <td style="display:none"><xsl:value-of select="."/></td>
             </xsl:when>
          <xsl:otherwise>
	          <td><xsl:value-of select="."/></td>
	        </xsl:otherwise>
            </xsl:choose>  
  			</xsl:for-each>
      </tr>
    </xsl:for-each>
	</tbody>  	
	</xsl:template>
</xsl:stylesheet>