<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th nowrap="true">材料编码</th>
				<th nowrap="true">材料名称</th>
				<th nowrap="true">规格型号</th>
				<th nowrap="true">库房</th>
				<th nowrap="true">业务类型</th>
				<th nowrap="true">制单日期</th>
				<th nowrap="true">出库日期</th>
				<th nowrap="true">批号</th>
				<th nowrap="true">条形码</th>
				<th nowrap="true">数量</th>
				<th nowrap="true">单价</th>
				<th nowrap="true">金额</th>				
				<th nowrap="true">科室</th>
				<th nowrap="true">制单人</th>
				<th nowrap="true">库管员</th>				
				<th nowrap="true">状态</th>
				<th nowrap="true">出库单号</th>
				<th nowrap="true">备注</th>
				<th nowrap="true">主要供应商</th>
				<th nowrap="true">生产厂商</th>
				<th  style="display:none">flag</th>
			</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=21">
								<td style="display:none"><xsl:value-of select="."/></td>
							</xsl:when>
              <xsl:when test="position()=11">
                <td align="right">
                	<xsl:if test="../td[20]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
									<xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=12">
                <td align="right">
                	<xsl:if test="../td[20]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
									<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=10">
                <td align="right">
                	<xsl:if test="../td[20]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
             <xsl:when test="position()=16">
                <xsl:if test="../td[16]='未审核'">
		            <td align='left' style='Color:red'>	            	
		              <xsl:value-of select="../td[16]"/>  
		            </td>
		          </xsl:if>
		          <xsl:if test="../td[16]!='未审核'">
								<td>
									<xsl:value-of select="../td[16]"/>
								</td>
							</xsl:if>
			    		</xsl:when>
              <xsl:otherwise>
                <td>
                	<xsl:if test="../td[20]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
