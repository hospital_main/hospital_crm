<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th nowrap="true">出库单号</th>
				<th nowrap="true">领料科室</th>
				<th nowrap="true">制单日期</th>
				<th nowrap="true">出库日期</th>
				<th nowrap="true">领料人</th>
				<th nowrap="true">摘要</th>
				<th nowrap="true">状态</th>	
				<th noWrap="true">金额</th>		
				<th nowrap="true">制单人</th>
				
				<th nowrap="true">仓库名称</th>
				<th nowrap="true">业务类型</th>
				<th nowrap="true">库管员</th>	
				<th  style="display:none">13</th>
				<th  style="display:none">flag</th>
				<th nowrap="true">冲账状态</th>	
				<th nowrap="true">打印状态</th>	
			</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	    <xsl:for-each select="/root/tbody/tr">
	   <xsl:variable name="iow_no" select="td[1]"/>
  	  	<xsl:variable name="iow_id" select="./pk/iow_id"/>
  	  	<xsl:variable name="money" select="td[13]"/>
        <tr>
          <xsl:if test="td[1]!='合计'">
	          <td align='center'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
	      			    <xsl:attribute name="iow_code" >
	               <xsl:value-of select="./td[1]"/>
	      			  </xsl:attribute>
	    			  </input>
	          </td>
          </xsl:if>
					<xsl:if test="td[1]='合计'">
						<td/>
					</xsl:if>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=14">
								<td style="display:none"><xsl:value-of select="."/></td>
							</xsl:when>
              <xsl:when test="position()=1">
	          		<xsl:if test=". !='合计'">
	          			
		          		<td>
		          			<xsl:if test="../td[14]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
		          			<a tabindex='-1'><xsl:value-of select="."/></a>
		          		</td>
		          	</xsl:if>
		          	<xsl:if test=". ='合计'">
		          		<td><xsl:value-of select="."/></td>
		          	</xsl:if>
			    		</xsl:when>
						<xsl:when test="position()=2">
		            <td align='left'>
		            	<xsl:if test="../td[14]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
		              <xsl:value-of select="../td[7]"/>  
		            </td>
			    		</xsl:when> 
              <xsl:when test="position()=3">
		            <td align='left'>
		            	<xsl:if test="../td[14]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
		              <xsl:value-of select="../td[4]"/>  
		            </td>
			    		</xsl:when>
              <xsl:when test="position()=4">
		            <td align='left'>
		            	<xsl:if test="../td[14]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
		              <xsl:value-of select="../td[5]"/>  
		            </td>
			    		</xsl:when>
              <xsl:when test="position()=5">
		            <td align='left'>
		            	<xsl:if test="../td[14]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
		              <xsl:value-of select="../td[8]"/>  
		            </td>
			    		</xsl:when>
            
			    		   <xsl:when test="position()=6">
                <td>
                	<xsl:if test="../td[14]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
      	          <xsl:if test="substring(../td[6],0,7)='科室审核生成'">
        	          <a tabindex='-1'>
                		 <xsl:attribute name="href" >
      	            javascript:openDialog('update_detail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:950px;dialogHeight:1000px', result)
    	              </xsl:attribute>
                		<xsl:value-of select="../td[6]"/></a>
      	          </xsl:if>
                	<xsl:if test="substring(../td[6],0,7)!='科室审核生成'">        	          
                		<xsl:value-of select="../td[6]"/>
      	          </xsl:if>
                	
                </td>
              </xsl:when>
              <xsl:when test="position()=7">
              <xsl:if test="../td[12]='未审核'">
		            <td align='left' style='Color:red'>	            	
		              <xsl:value-of select="../td[12]"/>  
		            </td>
		          </xsl:if>
		          <xsl:if test="../td[12]!='未审核'">
								<td>
									<xsl:value-of select="../td[12]"/>
								</td>
							</xsl:if>
			    		</xsl:when>
              <xsl:when test="position()=8">
		            <td align='right'>
			       			<xsl:if test="../td[14]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
									<xsl:if test="$iow_no !='合计'">
				  		             <a href="#">
									  <xsl:attribute name="onclick">
									  openDetailItems("&lt;iow_id&gt;<xsl:value-of select="$iow_id"/>&lt;/iow_id&gt;&lt;iow_no&gt;<xsl:value-of select="$iow_no"/>&lt;/iow_no&gt;&lt;money&gt;<xsl:value-of select="format-number($money,$VHMONEYFORMAT)"/>&lt;/money&gt;")
									  </xsl:attribute>
						       <xsl:value-of select="format-number(../td[13],$VHMONEYFORMAT)"/>
					  				</a>
  		              </xsl:if>
  		              <xsl:if test="$iow_no ='合计'">
  		              	 <xsl:value-of select="format-number(../td[13],$VHMONEYFORMAT)"/>
  		              </xsl:if>
		            </td>
			    		</xsl:when>
              <xsl:when test="position()=9">
		            <td align='left'>
		            	<xsl:if test="../td[14]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
		              <xsl:value-of select="../td[9]"/>  
		            </td>
			    		</xsl:when>
            	<!--未审核的改红色显示 -->
	  			    <xsl:when test="position()=11">
	  			    	<td>
									<xsl:value-of select="../td[2]"/>
								</td>
							</xsl:when>
							 <xsl:when test="position()=15">
	  			    	<td>
									<xsl:value-of select="../td[15]"/>
								</td>
							</xsl:when>
							 <xsl:when test="position()=16">
	  			    	<td>
									<xsl:value-of select="../td[16]"/>
								</td>
							</xsl:when>
              <xsl:when test="position()=12">
		            <td align='left'>
		            	<xsl:if test="../td[14]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
		              <xsl:value-of select="../td[3]"/>  
		            </td>
			    		</xsl:when>
              <xsl:when test="position()=13">
		            <td align='left'>
		            	<xsl:if test="../td[14]='1'">
        	          <xsl:attribute name="bgcolor">red</xsl:attribute>
      	          </xsl:if>
		              <xsl:value-of select="../td[11]"/>  
		            </td>
			    		</xsl:when>
			    		
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
