<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
      <colgroup>		       
	      <col style = 'width:90'/>
	      <col style = 'width:150'/>
	      <col style = 'width:80'/>
	      <col style = 'width:40'/>
	      <col style = 'width:80'/>
	      <col style = 'width:60'/>
	      <col style = 'width:60'/>
	      <col style = 'width:60'/>
	      <col style = 'width:80'/>
	      <col style = 'width:80'/>
	      
	    </colgroup>
      <thead>
      	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  			<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      	<xsl:variable name="RowS" select="/root/annex/endRow"/>
		    <xsl:variable name="endRow" select="count(/root/tbody/tr) mod $RowS"/>
		    <xsl:variable name="rowlast" select="count(/root/tbody/tr)"/>
		    <xsl:variable name="allmoney" select="format-number(sum(/root/tbody/tr/td[18]),$VHMONEYFORMAT)"/>
  		  <tr noWrap='true' class='mainHead'> 
  		  	<th>材料编码</th>
  				<th>材料名称</th>
  				<th>规格型号</th>  				
  				<th>单位</th>
  				<th>批号</th>
  				<th>数量</th>
  				<th>单价</th>
  				<th>条形码</th>
  				<th>金额</th>
  				<th>备注</th>
  		  </tr>
  		</thead>     
      <tbody>
	      <xsl:for-each select="/root/tbody/tr">
	      	<xsl:variable name="rowindex" select="position()"/>
	        <tr>
	        	<xsl:for-each select="td">          
	            <xsl:choose>            
  	            <xsl:when test="position()=1 or position()=7 or position()=8 or position()=9 or position()=10 or position()=13 or position()=14 or position()=16 or position()=17">               
                </xsl:when>
                <xsl:when test="position()=15">
	                <td align='left' >
		                <xsl:value-of select="."/>  
		              </td>
                </xsl:when>
                <xsl:when test="position()=11 ">
	                <td align='right' class='numberText'>
		                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
		              </td>
                </xsl:when>
                <xsl:when test="position()=12 ">
	                <td class="numberText">
	                  <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
	                </td>
	              </xsl:when>
	              <xsl:when test=" position()=18">
	                <td class="numberText">
	                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	                </td>
	              </xsl:when>  
	              <xsl:otherwise>
	              	<td align="left">
	                	<xsl:value-of select="."/>
	                </td>
	              </xsl:otherwise>
	            </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	  			<xsl:if test="position() mod $RowS = 0">
		  			<tr SumTitle="合计：">
		  				<td class="numberText" colspan="10"  style="font-weight: bold;" >本页合计：
				      <xsl:value-of select="format-number(sum(/root/tbody/tr[ position() > $rowindex - $RowS and  position() &lt; $rowindex+1 ]/td[18]),$VHMONEYFORMAT)"/>
				      总计：
				      <xsl:value-of select="$allmoney"/>
				      </td>
		  			</tr>
		  		</xsl:if>
	   		</xsl:for-each>
	   		<xsl:if test="$endRow>0">
	        <xsl:call-template  name="repeat">  
	           <xsl:with-param  name="times"  select="$RowS - $endRow"  />  
	        </xsl:call-template>  
	      </xsl:if>
	      <xsl:if test="string(number($endRow))='NaN'">
		      <tr SumTitle="合计：">
						<td class="numberText" colspan="10" style="font-weight: bold;">本页合计：
							<xsl:value-of select="format-number(sum(/root/tbody/tr/td[18]),$VHMONEYFORMAT)"/>
							总计：
							<xsl:value-of select="$allmoney"/>
						</td>
		      </tr>
	      </xsl:if >
	      <xsl:if test="string(number($endRow))!='NaN'">
		      <xsl:if test="$endRow!=0 ">
			      <tr SumTitle="合计：">
							<td class="numberText" colspan="10" style="font-weight: bold;">本页合计：
								<xsl:value-of select="format-number(sum(/root/tbody/tr[ position() > $rowlast - $endRow  and  position() &lt; $rowlast+1 ]/td[18]),$VHMONEYFORMAT)"/>
								总计：
								<xsl:value-of select="$allmoney"/>
							</td>
			      </tr>
		      </xsl:if>
	      </xsl:if >
		  </tbody>		  
	  </root>
	</xsl:template>
	<xsl:template  name="repeat">  
		<xsl:param  name="times"  select="0"  />  
		<xsl:if  test="$times  >  0">
			<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
			<xsl:call-template  name="repeat">  
			<xsl:with-param  name="times"  select="$times  -  1"  />  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>