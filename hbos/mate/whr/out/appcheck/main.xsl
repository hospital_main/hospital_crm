<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th><input type="checkbox"/></th>
				<th nowrap="true">科室申领单号</th>
				<th nowrap="true">申领科室</th>
				<th nowrap="true">摘要</th>
				<th nowrap="true">申领人</th>
				<th nowrap="true">申领日期</th>
				<th nowrap="true">发送日期</th>
				<th nowrap="true">库房审核人</th>
				<th nowrap="true">库房审核日期</th>
				<th nowrap="true">生成出库单据操作人</th>
				<th nowrap="true">生成出库单据操作日期</th>
				<th nowrap="true">审核意见</th>	
				<th nowrap="true">配送时间</th>	
				<th nowrap="true">配送说明</th>	
				<th nowrap="true">拣货单号</th>	
				<th nowrap="true" style="display:none"></th>
				<th nowrap="true">常备材料出库单</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr >
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                	<a tabindex='-1'><xsl:value-of select="."/></a>
                </td>
              </xsl:when>
			  			<xsl:when test="position()=15">
                <td>
                 <a tabindex='-1'>
                		 <xsl:attribute name="href" >
      	            javascript:openDialog('inDetail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:600px;dialogHeight:350px', result)
    	              </xsl:attribute>
                		<xsl:value-of select="."/></a>
                </td>
              </xsl:when>
              
              <xsl:otherwise>
              	<xsl:if test="../td[11]='未处理'">
									<td>
                  <xsl:value-of select="."/>
                </td>
							</xsl:if>
							<xsl:if test="../td[11]!='未处理'">
									<td >
                  <xsl:value-of select="."/>
                </td>
							</xsl:if>
                
              </xsl:otherwise>
            </xsl:choose>
              <!--未审核的改红色显示 -->
		  			    <!--xsl:when test="position()=11">
									<xsl:if test="../td[11]='未处理'">
										<tr style='Color:red'>
											<xsl:value-of select="."/>
										</tr>
									</xsl:if>
									<xsl:if test="../td[11]!='未处理'">
										<td>
											<xsl:value-of select="."/>
										</td>
									</xsl:if>
								</xsl:when-->
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
