<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true">出库单号</th>
				<th nowrap="true">出库日期</th>
  		</tr>
  	</thead>
  		<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr >
          <td align='center' style="display:none;"> 
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                	<a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('update_ck.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:950px;dialogHeight:1000px', result)

											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
                </td>
              </xsl:when>
              
              
              <xsl:otherwise>
              
									<td>
                  <xsl:value-of select="."/>
                </td>
						
                
              </xsl:otherwise>
            </xsl:choose>
              
          </xsl:for-each>
            
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>