<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th><input type="checkbox"/></th>
				<th nowrap="true">材料编码</th>
				<th nowrap="true">材料名称</th>
				<th nowrap="true">规格型号</th>
				<th nowrap="true">单价</th>
				<th nowrap="true">请领数量</th>
				<th nowrap="true">已处理数量</th>
				<th nowrap="true">已通过数量</th>
				<th nowrap="true">未通过数量</th>
				<th nowrap="true">申请科室</th>
				<th nowrap="true">响应仓库</th>
				<th nowrap="true">备注</th>			
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                	<!--a tabindex='-1'><xsl:value-of select="."/></a-->
									<a tabindex='-1'>
										<xsl:attribute name="href" >
											javascript:openUpdate('<xsl:value-of select="../pk/id"/>')
										</xsl:attribute><xsl:value-of select="."/>
									</a>
                </td>
              </xsl:when>
              <xsl:when test="position() &gt; 3 and position() &lt; 9">
                <td align="right">
                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
