<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=""/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>
				<tr noWrap="true" class="mainHead">
					<td nowrap="true">盘点单号</td>
					<td nowrap="true">仓库</td>
					<td nowrap="true">盘点日期</td>
					<td nowrap="true">材料编码</td>
					<td nowrap="true">材料名称</td>
					<td nowrap="true">规格型号</td>
					<td nowrap="true">单位</td>
					<td nowrap="true">盘点前数量</td>
					<td nowrap="true">盘点数量</td>
					<td nowrap="true">盈亏数量</td>
					<td nowrap="true">盈亏金额</td>
				</tr>
			</thead>
			<tbody>
				<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test="position()=8 or position()=9 or position()=10">
	                <td align="right">
	                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </td>
	              </xsl:when>
	              <xsl:when test="position()=11">
			            <td align='right'>
			              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
			            </td>
				    		</xsl:when>
	              <xsl:otherwise>
	                <td>
	                  <xsl:value-of select="."/>
	                </td>
	              </xsl:otherwise>
	            </xsl:choose>
	          </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>