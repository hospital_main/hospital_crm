<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
				<th noWrap="true">单据编号</th>
				<th noWrap="true">编制日期</th>
				<th noWrap="true">备注</th>
				<th noWrap="true">仓库</th>
				<th noWrap="true">业务类型</th>
				<th noWrap="true">采购类型</th>
				<th noWrap="true" >订单编号</th>
				<th noWrap="true">采购员</th>
				<th noWrap="true">制单人</th>
				<th noWrap="true">库管员</th>
				<th noWrap="true">状态</th>
  		</tr>
  	</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				    <xsl:choose>
				      <xsl:when test="position()=1">
				      	<xsl:if test="../td[1]='入库' or ../td[1]='出库'">
					      	<td colspan='11' align='center'>
					      		<xsl:value-of select="."/>
					      	</td>
				        </xsl:if>
				        <xsl:if test="../td[1]!='入库' and ../td[1]!='出库'">
					      	<td>
					      		<xsl:value-of select="."/>
					      	</td>
				        </xsl:if>
				      </xsl:when>
				      <xsl:otherwise>
				        <xsl:if test="../td[1]='入库' or ../td[1]='出库'">
				      		
				        </xsl:if>
				        <xsl:if test="../td[1]!='入库' and ../td[1]!='出库'">
				      		<td><xsl:value-of select="."/></td>
				        </xsl:if>
				      </xsl:otherwise>			                        
				    </xsl:choose>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>