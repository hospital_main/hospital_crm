<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=""/>
	<xsl:template match="/">
		<root>
			<!--colgroup>		       
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
      </colgroup-->
			<thead>
				<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>
				<tr noWrap="true" class="mainHead">
					<td>仓库</td>
					<td>盘点表单号</td>
					<td>日期</td>
					<td>摘要</td>
					<td>盘点科室</td>
					<td>经手人</td>
					<td>制单人</td>
					<td>审核人</td>
					<td>状态</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td align="left">
								<xsl:value-of select="."/>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>