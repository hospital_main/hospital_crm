<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
        <th><input type="checkbox"/></th>
        <th noWrap="true">材料代码</th>
				<th noWrap="true">材料名称</th>
				<th noWrap="true">规格型号</th>
				<th noWrap="true">货位号</th>
				<th noWrap="true">批号</th>
				<th noWrap="true">计量单位</th>
				<th noWrap="true">有效期</th>
				<th noWrap="true">帐面数量</th>
				<th noWrap="true">盘点数量</th>
				<th noWrap="true">盈亏数量</th>
				<th noWrap="true">盈亏说明</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align="center">
            <input type="checkbox"/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=9">
	              <td noWrap="true" align="right">
                  <xsl:value-of select="format-number(.,'##0.00')"/>
                </td>
	            </xsl:when>
              <xsl:when test="position()=8 or position()=10">
	              <td noWrap="true" align="right">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>
              <xsl:otherwise>
                <td noWrap="true">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


