var CONST_BAR_SEP="*|*";
var CONST_BAR_SEPE="*E*|";
var CONST_BAR_SEPG="*G*|";
var CONST_BAR_SEPL="*L*|";
function getReadBarParaStr(){
	return window.__readBarInitXmlStr;	
}
function initReadBarConfig(fileName){
	g_bar_config=fileName;
	readBarConfigFromFile();
}
function parseReadBarValue(obj,row,v){
	if(v==""||v=="null")
		return;
	var vv=v.split(CONST_BAR_SEP);
	for(var i=0;i<vv.length&&i<window.__readBarInitIndex.length;i++){
		obj.SetCellData(row,window.__readBarInitIndex[i],vv[i]);
	}
}
function readBarConfigFromFile(){
	var vXml = new ActiveXObject("Microsoft.XMLDOM");
	vXml.async=false;
	try{
		vXml.load(window.prefix+"hbos/mate/"+g_bar_config);
		var items=vXml.getElementsByTagName("Item");
		var str="<root>";
		window.__readBarInitIndex=[];
		for(var i=0;i<items.length;i++){
			window.__readBarInitIndex.push(items[i].getAttribute("UITableIndex"));
			var fa=items[i].getAttribute("BarCodeFactor");
			if(fa==""||fa==null){
				fa="";
			}else{
				fa="{{"+fa+"}}";
			}
			str+="<item UITableIndex=\""+i+"\" BarCodeFactor=\""+fa+"\" DataTable=\""+items[i].getAttribute("DataTable")+"\" Field=\""+items[i].getAttribute("Field")+"\"></item>";
		}
		str+="</root>";
		window.__readBarInitXmlStr="<conf>"+encodeXmlChar(str).replace(/&gt;/g,CONST_BAR_SEPG).replace(/&lt;/g,CONST_BAR_SEPL).replace(/=/g,CONST_BAR_SEPE)+"</conf>";
	}catch(E){
		alert("����!�����ļ�config_mate_bar.xml!");
		return;
	}
}