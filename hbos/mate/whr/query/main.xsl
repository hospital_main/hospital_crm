<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">日期</th>
  			<th noWrap="true">单据号</th>
  			<th noWrap="true">单据类型</th>
  		  <th noWrap="true">业务类型</th>
  			<th noWrap="true">摘要</th>
  			<th noWrap="true">操作科室</th>
  			<th noWrap="true">操作人</th>
  			<th noWrap="true">单据数量</th>
  			<th noWrap="true">实际库存</th>
  			<th noWrap="true">实物盘点数量</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test="position()=2">
		          	<td >
	                <xsl:if test="'0采购入库'=../td[4] or '1自制品入库'=../td[4] or '3有偿调入'=../td[4] or '4无偿调入'=../td[4] or '9其他入库'=../td[4] or '11采购退货'=../td[4] or '20委托加工入库'=../td[4] or '23捐赠入库'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:[MAXW];dialogHeight:[MAXH]',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'0'=../td[12] and '7盘盈入库'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'1'=../td[12] and '7盘盈入库'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/in/instead/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'2科室领用'=../td[4] or '5有偿调出'=../td[4] or '6无偿调出'=../td[4] or '10其他出库'=../td[4] or '12报废出库'=../td[4] or '15二级库盘点'=../td[4] or '17自制品原材料领用'=../td[4] or '22科室退库'=../td[4] or '24捐赠出库'=../td[4] or '31借库'=../td[4] or '32还库'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/common/update.html?load=&lt;a&gt;&lt;/a&gt;&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:[MAXW];dialogHeight:[MAXH]',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'8盘亏出库'=../td[4] and '0'=../td[12] " >
							  		<a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'8盘亏出库'=../td[4] and '1'=../td[12] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/instead/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'14移出库'=../td[4] or '13移入库'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/move/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'18代销入库'=../td[4] or '41代销退货'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/in/instead/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'36代销出库'=../td[4] or '33代销借库'=../td[4] or '34代销还库'=../td[4] or '35代销退库'=../td[4]  " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/instead/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'37代销移入库'=../td[4] or '38代销移出库'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/insteadmove/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	
							  	<xsl:if test="'16医生医嘱'=../td[4] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/second/advice/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'21专购品'=../td[4] " >
					          <a href="#" tabindex='-1'>
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:1150px;dialogHeight:750px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										 </a>
							  	</xsl:if>



								<xsl:if test=" '51计量转换出库'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../out/transform/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>


								<xsl:if test="'52计量转换入库'=../td[4] " >										 
										  <xsl:value-of select="."/>										
							  	</xsl:if>



							  	<!--
							  	<xsl:if test="'39期初入库'=../td[4] or '40期初出库'=../td[4]">
										<xsl:if test="'0'=../td[12]">
											<a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('mate/info/init/noconsignee/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="'1'=../td[12]">
											<a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('mate/whr/in/consignee/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
										</xsl:if>
							  	</xsl:if>
							  	-->
	              </td>
		          </xsl:when>
	            <xsl:when test="position()=8 or position()=9 or position()=10">
		          	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=11 or position()=12">
		          	<td style="display:none">
         					<xsl:value-of select="."/>
        			  </td>
		          </xsl:when>
				  		<xsl:otherwise>
			          <td>
         					<xsl:value-of select="."/>
        			  </td>   
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>