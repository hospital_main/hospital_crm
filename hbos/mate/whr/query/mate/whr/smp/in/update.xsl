<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		  <tr class='mainHead'> 
		  	<th width='25' noWrap='true'><input type='checkbox'/></th>
		    <th noWrap='true'>自制品代码</th>
		    <th noWrap='true'>自制品名称</th>
		    <th noWrap='true'>规格型号</th>
		    <th noWrap='true'>计量单位</th>
		    <th noWrap='true'>院内金额</th>
		    <th noWrap='true'>院外金额</th>
		    <th noWrap='true'>数量</th>
		    <th noWrap='true'>单价</th>
		    <th noWrap='true'>金额</th>	
		    <th noWrap='true'>生产批号</th>	    
		    <th noWrap='true'>失效日期</th>
		  </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td noWrap='true'>
                  <a href='#'>
                    <xsl:attribute name="onclick">openDialog('dupdate.html','dialogWidth:400px;dialogHeight:550px', this);</xsl:attribute>
                    <xsl:value-of select="."/>
                  </a>
                </td>
              </xsl:when>
	            <xsl:when test="position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17 or position()=18">	
	                </xsl:when>                 
	            <xsl:when test="position()=5 or position()=6 or position()=7 or position()=8 or position()=9">
	              <td  align="right" class="moneyCol" noWrap="true">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>
	            <xsl:otherwise>
                <td noWrap="true">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
        <xsl:if test="count(/root/total/td)!=0" >      
		      <xsl:for-each select="/root/total">
		      	<tr>
		          <td align='center'></td>
		          <xsl:for-each select="td">
		            <xsl:choose>
		              <xsl:when test="position()=1">
		                <td>合计：</td>
		              </xsl:when>
			            <xsl:when test="position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17 or position()=18">	
	                </xsl:when> 
			            <xsl:when test="position()=9">	 
			              <td  align="right" class="moneyCol">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
			            </xsl:when>	          
		              <xsl:otherwise>
		                <td></td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		        </tr>
		      </xsl:for-each>
      	</xsl:if>
    </tbody>
  </xsl:template>
</xsl:stylesheet>