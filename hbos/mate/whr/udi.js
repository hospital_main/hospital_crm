﻿   //udi条码解析(udiBarCode:01069012345678921716062410HMBP3022100156389;udiKeyCode:48,49,48,54,57,48,49,50,51,52,53,54,55,56,57,50,49,55,49,54,48,54,50,52,49,48,16,72,77,66,80,51,48,50,119,50,49,48,48,49,53,54,51,56,57,13,)
function parsingUdi(udiBarCode,udiKeyCode){
	//alert(udiKeyCode)
	if(udiBarCode==""){  
		document.getElementById("barTip").innerText="请扫描条码";
		return null;
	}
	//0条码(01)、1有效期(17)、2批号(10)、3序列号(21、91)、4生产日期(11)
	var udiBarCodeArray=new Array();
	udiBarCodeArray=parsingUdiStart(udiBarCode,udiKeyCode);

	if(udiBarCodeArray==null){
		return null;
	}
	if(udiBarCodeArray[1]==null)udiBarCodeArray[1]="";
	if(udiBarCodeArray[1]!="" && udiBarCodeArray[1].length==6){
		 udiBarCodeArray[1]="20"+udiBarCodeArray[1];//20170800
		 if(udiBarCodeArray[1].substring(6,8)=="00"){
			udiBarCodeArray[1]=udiBarCodeArray[1].substring(0,6)+"01";
		 }
		 var altdata=parseDate(udiBarCodeArray[1]);
		 if(altdata!=null){
		  	var year = altdata.getYear();
		  	var month = parseInt(altdata.getMonth()) + 1;
		  	var day = altdata.getDate();
		  	
		  	if(month.toString().length==1){
		  		month = "0"+month;
		  	}
		  	if(day.toString().length==1){
		  		day = "0"+day;
		  	}
		  	var retDate = year + "-" + month + "-" + day;
		  	udiBarCodeArray[1]=retDate;
		}
	}
	if(udiBarCodeArray[2]==null)udiBarCodeArray[2]="默认批号";
	if(udiBarCodeArray[3]==null)udiBarCodeArray[3]="";
	if(udiBarCodeArray[4]==null)udiBarCodeArray[4]="";
	if(udiBarCodeArray[4]!="" && udiBarCodeArray[4].length==6){
		 udiBarCodeArray[4]="20"+udiBarCodeArray[4];//20170800
		 if(udiBarCodeArray[4].substring(6,8)=="00"){
			udiBarCodeArray[4]=udiBarCodeArray[4].substring(0,6)+"01";
		 }
		 var altdata=parseDate(udiBarCodeArray[4]);
		 if(altdata!=null){
		  	var year = altdata.getYear();
		  	var month = parseInt(altdata.getMonth()) + 1;
		  	var day = altdata.getDate();
		  	
		  	if(month.toString().length==1){
		  		month = "0"+month;
		  	}
		  	if(day.toString().length==1){
		  		day = "0"+day;
		  	}
		  	var retDate = year + "-" + month + "-" + day;
		  	udiBarCodeArray[4]=retDate;
		}
	}
		if(udiBarCodeArray[1]!="" && udiBarCodeArray[1].length==6){
		 udiBarCodeArray[1]="20"+udiBarCodeArray[1];//20170800
		 if(udiBarCodeArray[1].substring(6,8)=="00"){
			udiBarCodeArray[1]=udiBarCodeArray[1].substring(0,6)+"01";
		 }
		 var altdata=parseDate(udiBarCodeArray[1]);
		 if(altdata!=null){
		  	var year = altdata.getYear();
		  	var month = parseInt(altdata.getMonth()) + 1;
		  	var day = altdata.getDate();
		  	
		  	if(month.toString().length==1){
		  		month = "0"+month;
		  	}
		  	if(day.toString().length==1){
		  		day = "0"+day;
		  	}
		  	var retDate = year + "-" + month + "-" + day;
		  	udiBarCodeArray[1]=retDate;
		}
	}
	return udiBarCodeArray;
}

/*
01-14位，条形码
10-不定长度，批号
11-6位，生产日期
17-6位，有效期
21-不定长度，序列号
91-不定长度，公司内部编号
*/
function parsingUdiStart(udiBarCode,udiKeyCode){
	var udiBarCodeArray=new Array();
	var udiArray="";
	//************二维码(01)06901234567892(17)160624(10)HMBP302(21)00156389************************************
	if(udiBarCode.indexOf("(01)")!=-1){
		udiArray=udiBarCode.split("(");
		for(var i=1;i<udiArray.length;i++){
			if(udiArray[i].indexOf("01)")!=-1){
				udiBarCodeArray[0]=udiArray[i].substring(3,udiArray[i].length);//0条码(01)
			}else if(udiArray[i].indexOf("17)")!=-1){
				udiBarCodeArray[1]=udiArray[i].substring(3,udiArray[i].length);//1有效期(17)
			}else if(udiArray[i].indexOf("10)")!=-1){
				udiBarCodeArray[2]=udiArray[i].substring(3,udiArray[i].length);//2批号(10)
			}else if(udiArray[i].indexOf("21)")!=-1){
				udiBarCodeArray[3]=udiArray[i].substring(3,udiArray[i].length);//3序列号(21)
			}else if(udiArray[i].indexOf("11)")!=-1){
				udiBarCodeArray[4]=udiArray[i].substring(3,udiArray[i].length);//4生产日期(11)
			}
		}
		return udiBarCodeArray;
	}else{
	//************一维码01069012345678921716062410HMBP3022100156389************************************
		udiArray=udiBarCode.split("");
		var udiKeyArray=udiKeyCode.split(",");
		udiBarCodeArray[0]=udiBarCode.substring(2,16);//0条码(01)
	
		var udiBarPH="";
		if(udiArray[0]+udiArray[1]!="01"){
			return null;
		}
		//条码反转
		var udiReverseArray=reverse(udiBarCode);
		
		if(udiArray[0]+udiArray[1]=="01" && udiArray[16]+udiArray[17]=="10" && udiReverseArray[14]+udiReverseArray[15]=="11" && udiReverseArray[7]+udiReverseArray[6]=="17" && udiKeyCode.indexOf("119,")==-1){
			//条码规则（01-10-11-17）
			udiBarCodeArray[1]=udiBarCode.substring(udiBarCode.length-6,udiBarCode.length);//1有效期(17)
			udiBarCodeArray[2]=udiBarCode.substring(18,udiBarCode.length-16);//2批号(10)
			udiBarCodeArray[4]=udiBarCode.substring(udiBarCode.length-14,udiBarCode.length-8);//4生产日期(11)
			return udiBarCodeArray;
			
		}else if(udiArray[0]+udiArray[1]=="01" && udiArray[16]+udiArray[17]=="11" && udiArray[24]+udiArray[25]=="17" && udiArray[32]+udiArray[33]=="10"){
			//条码规则（01-11-17-10）
			udiBarCodeArray[1]=udiBarCode.substring(18,24);//1有效期(17)
			udiBarCodeArray[2]=udiBarCode.substring(34,udiBarCode.length);//2批号(10)
			return udiBarCodeArray;
			
		}else if(udiArray[0]+udiArray[1]=="01" && udiArray[16]+udiArray[17]=="17" && udiArray[24]+udiArray[25]=="11" && udiArray[32]+udiArray[33]=="10"){
			//条码规则（01-17-11-10）
			udiBarCodeArray[1]=udiBarCode.substring(18,24);//1有效期(17)
			udiBarCodeArray[4]=udiBarCode.substring(26,32);//生产日期(11)
			udiBarCodeArray[2]=udiBarCode.substring(34,udiBarCode.length);//2批号(10)
			return udiBarCodeArray;
			
		}else if(udiArray[0]+udiArray[1]=="01" && udiArray[16]+udiArray[17]=="17" && udiArray[24]+udiArray[25]=="10" && udiKeyCode.indexOf("119,57,49")!=-1){//57,49=91
			//条码规则（01-17-10-91）
			udiBarCodeArray[1]=udiBarCode.substring(18,24);//1有效期(17)
			
			for(var i=0;i<udiKeyArray.length-1;i++){
				if(udiKeyArray[i]=="8"){//8:BackSpace
					continue;
				}
				else if(udiKeyArray[i]=="16"){//16:shift
					continue;
				}else if(udiKeyArray[i]=="119"){//119:f8
					udiBarPH=udiBarPH+"%";
					continue;
				}else if(udiKeyArray[i]=="13"){//13:Enter
					continue;
				}
				udiBarPH=udiBarPH+udiKey[udiKeyArray[i]];
			}
			udiBarCodeArray[2]=udiBarPH.substring(26,udiBarPH.indexOf("%"));//2批号(10)
			udiBarCodeArray[3]=udiBarPH.substring(udiBarPH.indexOf("%")+3,udiBarPH.length);//3序列(91)
		    //udiBarCodeArray[2]=udiBarPH.substring(26,33);//2批号(10)
			//udiBarCodeArray[3]=udiBarPH.substring(36,udiBarPH.length);//3序列(91)
			return udiBarCodeArray;
			
		}else if(udiArray[0]+udiArray[1]=="01" && udiArray[16]+udiArray[17]=="17" && udiArray[24]+udiArray[25]=="10" && udiKeyCode.indexOf("119,50,49,")!=-1){//50,49=21
			//条码规则（01-17-10-21）
			udiBarCodeArray[1]=udiBarCode.substring(18,24);//1有效期(17)
			
			for(var i=0;i<udiKeyArray.length-1;i++){
				if(udiKeyArray[i]=="8"){//8:BackSpace
					continue;
				}else if(udiKeyArray[i]=="16"){//16:shift
					continue;
				}else if(udiKeyArray[i]=="119"){//119:f8
					udiBarPH=udiBarPH+"%";
					continue;
				}else if(udiKeyArray[i]=="13"){//13:Enter
					continue;
				}
				udiBarPH=udiBarPH+udiKey[udiKeyArray[i]];
			}
		
			udiBarCodeArray[2]=udiBarPH.substring(26,udiBarPH.indexOf("%"));//2批号(10)
			udiBarCodeArray[3]=udiBarPH.substring(udiBarPH.indexOf("%")+3,udiBarPH.length);//3序列(91)
					
			return udiBarCodeArray;
			
		}else if(udiArray[0]+udiArray[1]=="01" && udiArray[16]+udiArray[17]=="17" && udiArray[24]+udiArray[25]=="10" && udiKeyCode.indexOf("119,57,49")==-1 && udiKeyCode.indexOf("119,50,49,")==-1){//57,49=91；50,49=21
			//条码规则（01-17-10）
			udiBarCodeArray[1]=udiBarCode.substring(18,24);//1有效期(17)
			udiBarCodeArray[2]=udiBarCode.substring(26,udiBarCode.length);//2批号(10)
			return udiBarCodeArray;
			
		}else if(udiArray[0]+udiArray[1]=="01" && udiArray[16]+udiArray[17]=="17" && udiArray[24]+udiArray[25]=="21" && udiKeyCode.indexOf("119,57,49")==-1 && udiKeyCode.indexOf("119,50,49,")==-1){//57,49=91；50,49=21
			//条码规则（01-17-21）
			udiBarCodeArray[1]=udiBarCode.substring(18,24);//1有效期(17)
			udiBarCodeArray[3]=udiBarCode.substring(26,udiBarCode.length);//3序列号(21)
			return udiBarCodeArray;
			
		}else if(udiArray[0]+udiArray[1]=="01" && udiBarCode.substring(16,18)=="17"){
			//条码规则（01-17）
			udiBarCodeArray[1]=udiBarCode.substring(18,24);//1有效期(17)
			return udiBarCodeArray;
			
		}else if(udiArray[0]+udiArray[1]=="01" && udiBarCode.substring(16,18)=="21"){
			//条码规则（01-21）
			udiBarCodeArray[3]=udiBarCode.substring(18,udiBarCode.length);//3序列号(21)
			return udiBarCodeArray;
			
		}else{
			return udiBarCodeArray
		}
	}

}

function reverse(str){
	var udiArray=str.split("");
	var udiReverseArray=new Array();
	var j=0;
	for(var i=udiArray.length-1;i>=0;i--){
		udiReverseArray[j]=udiArray[i];
		j++;
	}
	return udiReverseArray;
}

var udiKey={
//数字键的键码值
"48":"0",
"49":"1",
"50":"2",
"51":"3",
"52":"4",
"53":"5",
"54":"6",
"55":"7",
"56":"8",
"57":"9",
//字母键的键码值
"65":"A",
"66":"B",
"67":"C",
"68":"D",
"69":"E",
"70":"F",
"71":"G",
"72":"H",
"73":"I",
"74":"J",
"75":"K",
"76":"L",
"77":"M",
"78":"N",
"79":"O",
"80":"P",
"81":"Q",
"82":"R",
"83":"S",
"84":"T",
"85":"U",
"86":"V",
"87":"W",
"88":"X",
"89":"Y",
"90":"Z",
//数字键盘上的键码值
"96":"0",
"97":"1",
"98":"2",
"99":"3",
"100":"4",
"101":"5",
"102":"6",
"103":"7",
"104":"8",
"105":"9",
"106":"*",
"107":"+",
"108":"Enter",
"109":"-",
"110":".",
"111":"/",
//功能键盘上的键码值
"112":"F1",
"113":"F2",
"114":"F3",
"115":"F4",
"116":"F5",
"117":"F6",
"118":"F7",
"119":"F8",
"120":"F9",
"121":"F10",
"122":"F11",
"123":"F12",
//控制键盘上的键码值
"8":"BackSpace",
"9":"Tab",
"12":"Clear",
"13":"Enter",
"16":"Shift",
"17":"Control",
"18":"Alt",
"20":"CapeLock"
};



