<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<tr noWrap="true" class="mainHead">
  			<th nowrap="true">材料编码</th>
				<th nowrap="true">材料名称</th>
				<th nowrap="true">规格</th>
				<th nowrap="true">单位</th>
				<th nowrap="true">盘点前数量</th>
				<th nowrap="true">盘点数量</th>
				<th nowrap="true">单价</th>
				<th nowrap="true">盈亏数量</th>
				<th nowrap="true">盈亏金额</th>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test="position()=5 or position()=6 or position()=8 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
			    		</xsl:when>
	            <xsl:when test="position()=7">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position()=9">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>