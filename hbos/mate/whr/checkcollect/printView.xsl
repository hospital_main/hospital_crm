<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
	  <root>
	  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	    <thead>
		    <tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      	<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	      </tr>
	      <tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
	      	<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	      </tr>
	  		<tr noWrap="true" class="mainHead">
	  			<td nowrap="true">材料编码</td>
					<td nowrap="true">材料名称</td>
					<td nowrap="true">规格</td>
					<td nowrap="true">单位</td>
					<td nowrap="true">盘点前数量</td>
					<td nowrap="true">盘点数量</td>
					<td nowrap="true">单价</td>
					<td nowrap="true">盈亏数量</td>
					<td nowrap="true">盈亏金额</td>
	  		</tr>
	  	</thead>
	  	<tbody> 
	  	  <xsl:for-each select="/root/tbody/tr">
		      <tr>
		        <xsl:for-each select="td">
		          <xsl:choose>
		          	<xsl:when test="position()=5 or position()=6 or position()=8 ">
			            <td align='right'>
			              <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
				    		</xsl:when>
		            <xsl:when test="position()=7">
			            <td align='right'>
			              <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
			            </td>
				    		</xsl:when>
				    		<xsl:when test="position()=9">
			            <td align='right'>
			              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
			            </td>
				    		</xsl:when>
				    		<xsl:otherwise>
				          <td align='left'>
		                <xsl:value-of select="."/>
		              </td>
		            </xsl:otherwise>			                        
		          </xsl:choose>
		        </xsl:for-each>
		      </tr>
		    </xsl:for-each>
	   	</tbody>
	  </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>