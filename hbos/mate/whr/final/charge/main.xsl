<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
				<th noWrap="true" valign="center" rowspan="3">仓库编码</th>
				<th noWrap="true" valign="center" rowspan="3">仓库名称</th>
				<th noWrap="true" valign="center" rowspan="3">物资编码</th>
				<th noWrap="true" valign="center" rowspan="3">物资名称</th>
				<th noWrap="true" colspan="4">期初</th>
				<th noWrap="true" colspan="4">本期增加</th>
				<th noWrap="true" colspan="4">本期减少</th>
				<th noWrap="true" colspan="4">本期结余</th>
  		</tr>
			<tr class="mainHead">
				<th noWrap="true" colspan="2">数量</th>
				<th noWrap="true" colspan="2">金额</th>
				<th noWrap="true" colspan="2">数量</th>
				<th noWrap="true" colspan="2">金额</th>
				<th noWrap="true" colspan="2">数量</th>
				<th noWrap="true" colspan="2">金额</th>
				<th noWrap="true" colspan="2">数量</th>
				<th noWrap="true" colspan="2">金额</th>
			</tr>  		
  		<tr class="mainHead">
				<th noWrap="true">报表</th>
				<th noWrap="true">计算</th>
				<th noWrap="true">报表</th>
				<th noWrap="true">计算</th>
				<th noWrap="true">报表</th>
				<th noWrap="true">计算</th>
				<th noWrap="true">报表</th>
				<th noWrap="true">计算</th>
				<th noWrap="true">报表</th>
				<th noWrap="true">计算</th>
				<th noWrap="true">报表</th>
				<th noWrap="true">计算</th>
				<th noWrap="true">报表</th>
				<th noWrap="true">计算</th>
				<th noWrap="true">报表</th>
				<th noWrap="true">计算</th>
  		</tr>
  	</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
	      		<xsl:if test="position() &lt; 5">
			      	<td>
			      		<xsl:value-of select="."/>
			      	</td>
	      		</xsl:if>
	      		<xsl:if test="position() &gt; 4">
			      	<td align="right">
			      		<xsl:value-of select="format-number(.,'#,##0.00')"/>
			      	</td>
	      		</xsl:if>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>