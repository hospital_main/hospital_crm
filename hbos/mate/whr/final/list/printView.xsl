<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		<tr class="mainHead">
				<td noWrap="true">业务类型</td>
				<td noWrap="true">仓库名称</td>
				<td noWrap="true">单据号</td>
				<td noWrap="true">制单人</td>
				<td noWrap="true">制单日期</td>
				<td noWrap="true">单据状态</td>
				<td noWrap="true">是否代销</td>
  		</tr>
  	</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				     <td><xsl:value-of select="."/></td>
				    
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
    </tbody>
   </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>