<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
				<th noWrap="true">业务类型</th>
				<th noWrap="true">仓库名称</th>
				<th noWrap="true">单据号</th>
				<th noWrap="true">制单人</th>
				<th noWrap="true">制单日期</th>
				<th noWrap="true">单据状态</th>
				<th noWrap="true">是否代销</th>
  		</tr>
  	</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				     <td><xsl:value-of select="."/></td>
				    
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>