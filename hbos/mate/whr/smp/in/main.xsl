<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th noWrap="true">入库单号</th>
				<th noWrap="true">入库日期</th>
				<th noWrap="true">摘要</th>
				<th noWrap="true">仓库</th>
				<th noWrap="true">业务类型</th>
				<th noWrap="true">生产科室</th>
				<th noWrap="true">自制品批号</th>
				<th noWrap="true" >送货人</th>
				<th noWrap="true">制单人</th>
				<th noWrap="true">审核员</th>
				<th noWrap="true">库管员</th>
				<th noWrap="true">状态</th>
  		</tr>
  	</thead>
  	<tbody/>
	</xsl:template>
</xsl:stylesheet>