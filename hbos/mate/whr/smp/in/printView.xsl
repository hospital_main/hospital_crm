<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
      <colgroup>		       
	      <col style = 'width:100'/>
	      <col style = 'width:140'/>
	      <col style = 'width:100'/>
	      <col style = 'width:100'/>
	      <col style = 'width:80'/>
	      <col style = 'width:105'/>
	      <col style = 'width:105'/>
	      <col style = 'width:105'/>
	      <col style = 'width:105'/>
	      <col style = 'width:105'/>
	      <col style = 'width:105'/>
	      <col style = 'width:105'/>
	      <col style = 'width:105'/>
	    </colgroup>
      <thead>
      	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  			<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		  <tr noWrap='true' class='mainHead'> 
  		  	<th>自制品代码</th>
  				<th>自制品名称</th>
  				<th>规格型号</th>
  				<th>计量单位</th>
  				<th>院内金额</th>
  				<th>院外金额</th>
  				<th>数量</th>
  				<th>单价</th>
				  <th>金额</th>
				  <th>生产批号</th>
				  <th>实效日期</th>
				  <th>条形码</th>
				  <th>院内条码</th>
  		  </tr>
  		</thead>     
      <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=2">
            		<td align="left">
                  <xsl:value-of select="substring(.,1,11)"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=3">
            		<td align="left">
                  <xsl:value-of select="substring(.,1,15)"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=12 or position()=13 or position()=16 or position()=17 or position()=18 ">	 
	            </xsl:when>
	            <xsl:when test="position()=8 ">
	                <td class="numberText">
	                  <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
	                </td>
	              </xsl:when>
	              <xsl:when test=" position()=5 or position()=6  or position()=9">
	                <td class="numberText">
	                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	                </td>
	              </xsl:when>  
	              <xsl:when test="position()=7">
	                <td class="numberText">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </td>
	              </xsl:when>  
	            <xsl:otherwise>
                <td noWrap="true">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
        <xsl:if test="count(/root/total/td)!=0" >      
		      <xsl:for-each select="/root/total">
		      	<tr SumTitle="合计：">
		          <xsl:for-each select="td">
		            <xsl:choose>
		              <xsl:when test="position()=1">
		                <td>合计：</td>
		              </xsl:when>
			            <xsl:when test="position()=13 or position()=14">	 
	                </xsl:when> 
			            <xsl:when test="position()=10">	 
			              <td align="right" class="numberText">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
			            </xsl:when>	          
		              <xsl:otherwise>
		                <td></td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		        </tr>
		      </xsl:for-each>
      	</xsl:if>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>