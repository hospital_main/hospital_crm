<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	    <thead>
	    <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">  
        <td noWrap="true">材料代码</td>
  			<td noWrap="true">材料名称</td>
  			<td noWrap="true">规格型号</td>
  			<td noWrap="true">计量单位</td>
        <td noWrap="true">数量</td>
  			<td noWrap="true">单价</td>
  			<td noWrap="true">金额</td>
  	  </tr>
	    </thead>
    	<tbody>
    	<xsl:variable name="colsnumber" select="count(/root/tbody/tr[1]/td)"/>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:if test="$colsnumber =9">
	      	<xsl:if test="position() &gt; 1 ">
		        <tr>
		          <xsl:for-each select="td">
		            <xsl:choose>
		              <xsl:when test="position()=1">
		                <td noWrap='true'>
		                  <xsl:value-of select="."/>
		                </td>
		              </xsl:when>
			            <xsl:when test="position()=5 or position()=6 or position()=9">
			              <td   class="numberText" noWrap="true">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
			            </xsl:when>
			            <xsl:when test="position()=7 ">
			              <td   class="numberText" noWrap="true">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
			            </xsl:when>
			            <xsl:when test="position()=8 ">
			              <td   class="numberText" noWrap="true">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
			            </xsl:when>
			            <xsl:otherwise>
		                <td noWrap="true">
		                  <xsl:value-of select="."/>
		                </td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
	  			</xsl:if>
	  		</xsl:if>
	  		<xsl:if test="$colsnumber =7">
	      	<xsl:if test="position() &gt; 1 ">
		        <tr>
		          <xsl:for-each select="td">
		            <xsl:choose>
		              <xsl:when test="position()=1">
		                <td noWrap='true'>
		                  <xsl:value-of select="."/>
		                </td>
		              </xsl:when>
			            <xsl:when test=" position()=7 ">
			              <td  class="numberText" noWrap="true">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
			            </xsl:when>
			            <xsl:when test="position()=5 ">
			              <td  class="numberText" noWrap="true">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
			            </xsl:when>
			            <xsl:when test="position()=6 ">
			              <td  class="numberText" noWrap="true">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
			            </xsl:when>
			            <xsl:otherwise>
		                <td noWrap="true">
		                  <xsl:value-of select="."/>
		                </td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
	  			</xsl:if>
	  		</xsl:if>
      </xsl:for-each>
    </tbody>
    </root>
	</xsl:template>
</xsl:stylesheet>