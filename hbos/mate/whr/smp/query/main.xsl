<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:if test="position() &lt; 2">
	        <tr noWrap="true" class="mainHead">
	          <xsl:for-each select="td">
	            <th noWrap="true">
	            	<xsl:value-of select='.'/>
	            </th>
	          </xsl:for-each>
	        </tr>
        </xsl:if>
    	</xsl:for-each>
    </thead>
    <tbody>
    	<xsl:variable name="colsnumber" select="count(/root/tbody/tr[1]/td)"/>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:if test="$colsnumber =9">
	      	<xsl:if test="position() &gt; 1 ">
		        <tr>
		          <xsl:for-each select="td">
		            <xsl:choose>
		              <xsl:when test="position()=1">
		                <td noWrap='true'>
		                  <xsl:value-of select="."/>
		                </td>
		              </xsl:when>
			            <xsl:when test="position()=5 or position()=6 or position()=9">
			              <td  align="right" class="moneyCol" noWrap="true">
		                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
		                </td>
			            </xsl:when>
			            <xsl:when test="position()=7 ">
			              <td  align="right" class="moneyCol" noWrap="true">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
			            </xsl:when>
			            <xsl:when test="position()=8 ">
			              <td  align="right" class="moneyCol" noWrap="true">
		                  <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
		                </td>
			            </xsl:when>
			            <xsl:otherwise>
		                <td noWrap="true">
		                  <xsl:value-of select="."/>
		                </td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
	  			</xsl:if>
	  		</xsl:if>
	  		<xsl:if test="$colsnumber =7">
	      	<xsl:if test="position() &gt; 1 ">
		        <tr>
		          <xsl:for-each select="td">
		            <xsl:choose>
		              <xsl:when test="position()=1">
		                <td noWrap='true'>
		                  <xsl:value-of select="."/>
		                </td>
		              </xsl:when>
			            <xsl:when test=" position()=7 ">
			              <td  align="right" class="moneyCol" noWrap="true">
		                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
		                </td>
			            </xsl:when>
			            <xsl:when test="position()=5 ">
			              <td  align="right" class="moneyCol" noWrap="true">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
			            </xsl:when>
			            <xsl:when test="position()=6 ">
			              <td  align="right" class="moneyCol" noWrap="true">
		                  <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
		                </td>
			            </xsl:when>
			            <xsl:otherwise>
		                <td noWrap="true">
		                  <xsl:value-of select="."/>
		                </td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
	  			</xsl:if>
	  		</xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>