<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
				<th noWrap="true">材料代码</th>
				<th noWrap="true">材料名称</th>
				<th noWrap="true">规格型号</th>
				<th noWrap="true">计量单位</th>
				<th noWrap="true">生产厂商</th>
				<th noWrap="true">供应商</th>
				<th noWrap="true">赠送类型</th>
				<th noWrap="true">累计数量/金额</th>
				<th noWrap="true">赠品数量</th>
				<th noWrap="true">已入赠品库数量</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=8 or position()=9 or position()=10">
                  <td align='right' class='moneyCol'>
	                	<xsl:value-of select="format-number(.,'#,##0.00')"/>  
	              	</td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>