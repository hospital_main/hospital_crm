<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <tr noWrap='true' class='mainHead'>
  			<th noWrap="true" width="100" >材料名称</th>
        <th noWrap="true" width="100" >规格型号</th>
  			<th noWrap="true" width="100" >计量单位</th>
  			<th noWrap="true" width="100" >使用数量</th>
        <th noWrap="true" width="100" >单价</th>  	
        <th noWrap="true" width="100" >结算金额</th>  
        <th noWrap="true" width="100" >批号</th>
  	  	<th noWrap="true" width="100" >条形码</th>
  	  	<th noWrap="true" width="100" >仓库</th>
  			<th noWrap="true" width="100" >供货单位</th>
  			<th noWrap="true" width="100" >材料编码</th>	
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	     <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	           	<xsl:when test=" position()=4">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=5">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=6 ">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
		          </xsl:when>
							<xsl:otherwise>
			          <td>
         			    <xsl:value-of select="."/>
        			  </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
	    <tr>
				<td>合计：</td>
				<td></td>
				<td></td>
				<td align='right' ><xsl:value-of select="format-number(sum(/root/tbody/tr/td[4]),'#,##0.00')"/></td>
				<td></td>
				<td align='right' ><xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),$VHMONEYFORMAT)"/></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>