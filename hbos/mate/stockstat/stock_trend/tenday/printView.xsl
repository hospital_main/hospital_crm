<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
    <thead>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
      	<xsl:for-each select="/root/activeHead/mateStockstatStockTrendTenDay_select_head_1/th">
      	<td style="display:none"/>
	</xsl:for-each>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
      	<xsl:for-each select="/root/activeHead/mateStockstatStockTrendTenDay_select_head_1/th">
      	<td style="display:none"/>
	</xsl:for-each>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      	<xsl:for-each select="/root/activeHead/mateStockstatStockTrendTenDay_select_head_1/th">
      	<td style="display:none"/>
	</xsl:for-each>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:coltitle;"/>
      	<xsl:for-each select="/root/activeHead/mateStockstatStockTrendTenDay_select_head_1/th">
      	<td noWrap="true" style="fontsize:coltitle;"/>
	</xsl:for-each>
      </tr>
    </thead>   
  	<tbody>
	<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
          <xsl:choose>
                <xsl:when test="position()=1">
                  <td><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:otherwise>
                  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  	</tr>
      </xsl:for-each>  	
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



