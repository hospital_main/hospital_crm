<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th noWrap="true">入库单号</th>
				<th noWrap="true">入库日期</th>
				<th noWrap="true">仓库</th>
				<th noWrap="true">业务类型</th>
				<th noWrap="true">库管员</th>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test=" position()=1">
          			<td>
	          			<xsl:if test="'0'=../td[7] or '9'=../td[7] or '11'=../td[7] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../whr/query/mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[6]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'21'=../td[7] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../barcode/query/mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[6]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>				 
	          		</td>
	          	</xsl:when>
			    		<xsl:when test="position()>5">
			    			<td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>		                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>