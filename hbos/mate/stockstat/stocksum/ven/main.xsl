<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=' '/>  
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th noWrap="true">序号</th>
				<th noWrap="true">供应商编码</th>
				<th noWrap="true">供应商名称</th>
				<th noWrap="true">发票号</th>	
				<th noWrap="true">入库单号</th>		
				<th noWrap="true">入库金额</th>
				<th noWrap="true">结算方式</th>  			  			
				<th noWrap="true">开户银行</th>  			
				<th noWrap="true">银行账号</th>  			
				<th noWrap="true">联系人</th>  			
				<th noWrap="true">联系电话</th>  			
			</tr>
		</thead>
		<tbody>
			<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
			<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:attribute name="pkStr"><xsl:value-of select="td[12]"/></xsl:attribute>
					<xsl:attribute name="pkLev"><xsl:value-of select="td[15]"/></xsl:attribute>
					<xsl:attribute name="vCode"><xsl:value-of select="td[2]"/></xsl:attribute>
					<xsl:if test="td[2]=''"><xsl:attribute name="style">display:none</xsl:attribute></xsl:if>
					<xsl:for-each select="td[position()&lt;12]">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td style="border-style:none"><xsl:if test="../td[2]!=''"><xsl:value-of select="."/></xsl:if><xsl:if test="../td[2]=''">　</xsl:if></td>
							</xsl:when>
							<xsl:when test="position()=2">
								<td style="border-style:none">
								<a href="#">
								<xsl:attribute name="onclick" >
								javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:700px;overflow:auto;', '','true')
								</xsl:attribute>
								<xsl:value-of select="."/>
								</a>　
								</td>
							</xsl:when>
							<xsl:when test="position()=3">
								<td style="border-style:none"><span onclick="showHideChildren(this)" style="cursor:hand"><xsl:value-of select="../td[13]"/></span><xsl:value-of select="."/>　</td>
							</xsl:when>
							<xsl:when test="position()=4">
								<td style="border-style:none">
									<xsl:if test="../td[2]!='' and ../td[14]='' and .=''">
										◆
									</xsl:if>
									<xsl:if test="../td[14]!='' or .!=''">
										<span onclick="showHideChildren(this)" style="cursor:hand"><xsl:value-of select="../td[14]"/></span><xsl:value-of select="."/>　
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()=6">
								<td style="border-style:none" align='right'><xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td style="border-style:none"><xsl:value-of select="."/>　</td>
							</xsl:otherwise>			                        
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			<tr>
				<td style="border-style:none">合计：</td>
				<td style="border-style:none">　</td>
				<td style="border-style:none">　</td>
				<td style="border-style:none">　</td>
				<td style="border-style:none">　</td>
				<td style="border-style:none" align='right' ><xsl:value-of select="format-number(sum(/root/tbody/tr[td[2]!='']/td[6]),$VHMONEYFORMAT)"/></td>
				<td style="border-style:none">　</td>
				<td style="border-style:none">　</td>
				<td style="border-style:none">　</td>
				<td style="border-style:none">　</td>
				<td style="border-style:none">　</td>
			</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>