<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
      <thead>
      <tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>

	     </tr>
	     <tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	     </tr>
	     <tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:7;align:left"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td noWrap="true" style="fontsize:subtitle;colspan:4;align:left">
	      			<xsl:value-of select="/root/annex/rightTitle"/>
	      		</td>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	     </tr>
  	 	<tr noWrap='true' class='mainHead'>
				<td noWrap="true">序号</td>
				<td noWrap="true">供应商编码</td>
				<td noWrap="true">供应商名称</td>
				<td noWrap="true">发票号</td>	
				<td noWrap="true">入库单号</td>		
				<td noWrap="true">入库金额</td>
				<td noWrap="true">结算方式</td>  			  			
				<td noWrap="true">开户银行</td>  			
				<td noWrap="true">银行账号</td>  			
				<td noWrap="true">联系人</td>  			
				<td noWrap="true">联系电话</td>  			
			</tr>
  	</thead>
  	<tbody>
			<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
			<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position()&lt;12]">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td><xsl:if test="../td[2]!=''"><xsl:value-of select="."/></xsl:if></td>
							</xsl:when>
							<xsl:when test="position()=2">
								<td>
								<a href="#">
								<xsl:attribute name="onclick" >
								javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:700px;overflow:auto;', '','true')
								</xsl:attribute>
								<xsl:value-of select="."/>
								</a>
								</td>
							</xsl:when>
							<xsl:when test="position()=3">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=4">
								<td style="border-style:none">
									<xsl:if test="../td[2]!='' and ../td[14]='' and .=''">
										◆
									</xsl:if>
									<xsl:if test="../td[14]!='' or .!=''">
										<span onclick="showHideChildren(this)" style="cursor:hand"><xsl:value-of select="../td[14]"/></span><xsl:value-of select="."/>　
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()=6">
								<td align='right'><xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>			                        
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			<tr>
				<td>合计：</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td align='right' ><xsl:value-of select="format-number(sum(/root/tbody/tr[td[2]!='']/td[6]),$VHMONEYFORMAT)"/></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>