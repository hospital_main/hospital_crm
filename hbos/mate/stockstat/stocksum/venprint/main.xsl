<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th ><input type="checkbox"/></th>
  	  	<th noWrap="true">序号</th>
  	  	<th noWrap="true">供应商编码</th>
  			<th noWrap="true">供应商名称</th>	
        <th noWrap="true">入库金额</th>
        <th noWrap="true">结算方式</th>  			  			
        <th noWrap="true">开户银行</th>  			
        <th noWrap="true">银行账号</th>  			
        <th noWrap="true">联系人</th>  			
        <th noWrap="true">联系电话</th>  			
  	  </tr>
  	</thead>
  	<tbody>
  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/> 
  	  <xsl:for-each select="/root/tbody/tr">
	     <tr>
	     		<td align='center' >
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
	     		<td><xsl:value-of select="position()"/>
	        </td>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test="position()=1">
		           	<td align='right'>
									<xsl:value-of select="."/>
	              </td>
		          </xsl:when>
	           	<xsl:when test="position()=3">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
		          </xsl:when>
							<xsl:otherwise>
			          <td>
         			    <xsl:value-of select="."/>
        			  </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
	    <tr>
	    	<td></td>
	    	<td>合计：</td>
	    	<td></td>
	    	<td></td>
	    	<td align='right' ><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),$VHMONEYFORMAT)"/></td>
	    	<td></td>
	    	<td></td>
	    	<td></td>
	    	<td></td>
	    	<td></td>
	    </tr>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>