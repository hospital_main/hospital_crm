<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<thead>
  		      <tr noWrap='true' class='mainHead'>
  		              <!--<th>供应商编码</th>-->
  		               <!--<th>材料编码</th>-->
				<th nowrap='true'>供应商</th> 
				<th nowrap='true'>材料名称</th>
				<th nowrap='true'>规格</th> 
				<th nowrap='true'>单价</th>
				<th nowrap='true'>数量</th>
				<th nowrap='true'>金额</th>
				<th nowrap='true'>生产厂商</th>
			</tr>   
  	</thead>
  	
 	 
 	 <tbody>
 	    <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/> 
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:variable name="ven_code" select="td[1]" />
         	<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$ven_code])"/>
      	        <xsl:variable name="cur_pos" select="position()" />
                  <tr>
                      <xsl:for-each select="td">
            			<xsl:choose>
				            	<xsl:when test="position()=1">
				            	
				            	</xsl:when>
				            	<xsl:when test="position()=2">
				            	
				            	</xsl:when>
				            	<xsl:when test="position()=3">
								<xsl:if test="$cur_pos = 1 ">
									<td nowrap="true" rowspan="{$rowspan}">
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="$cur_pos >1 and $ven_code != ../../tr[$cur_pos - 1]/td[1]">
									<td nowrap="true" rowspan="{$rowspan}">
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="$cur_pos >1 and $ven_code = ../../tr[$cur_pos - 1]/td[1]">
									<td style="display:none"><xsl:value-of select="."/>
								  </td>
								</xsl:if>			
						</xsl:when>
						<xsl:when test="position() = 6 ">
								<td align="right">
									<xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
								</td>	
						</xsl:when>
						<xsl:when test="position() = 8 ">
								<td align="right">
									<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
								</td>	
						</xsl:when>
						<xsl:when test="position() = 7">
								<td align="right">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>	
						</xsl:when>
								
						<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>  
								</td>	
						</xsl:otherwise>
          	              </xsl:choose>
  		    </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>