<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-2"/>
	  <root>
	    <thead>
		    <tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      	<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	      </tr>
	      <tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
	      	<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	      </tr>
		 		<xsl:for-each select="/root/tbody/tr">
		    	<xsl:choose>
		    		<xsl:when test="position()=1">
		    			<tr noWrap='true' class='mainHead'>
			          <xsl:for-each select="td">
			          	<xsl:choose>
				          	<xsl:when test=" position() &lt; 3 or position()>4 ">
			          			<td valign="middle" >
					            	<xsl:value-of select="."/>
			          			</td>
				          	</xsl:when>
		              </xsl:choose>
			          </xsl:for-each>
			          <xsl:for-each select="td">
			          	<xsl:choose>
				          	<xsl:when test=" position()=3 ">
			          			<td valign="middle" >
					            	<xsl:value-of select="."/>
			          			</td>
				          	</xsl:when>
		              </xsl:choose>
			          </xsl:for-each>
			  			</tr>
		    		</xsl:when>
	        </xsl:choose>
	   		</xsl:for-each>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
		    	<xsl:choose>
		    		<xsl:when test=" position()>1 ">
		    			<tr>
			          <xsl:for-each select="td">
			          	<xsl:choose>
			          		<xsl:when test=" position()=1 ">
				            	<td><xsl:value-of select="."/></td>
				            </xsl:when>
				            <xsl:when test=" position()=2 ">
				            	<td ><xsl:value-of select="."/></td>
				            </xsl:when>
				            <xsl:when test="position() >4 ">
				            	<td  align="right" ><xsl:value-of select="format-number(.,'#,##0.00' )"/></td>
				            </xsl:when>
			          	</xsl:choose>
			          </xsl:for-each>
			          <xsl:for-each select="td">
			          	<xsl:choose>
			          		<xsl:when test=" position()=3 ">
				            	<td align="right">
				            		<xsl:value-of select="format-number(.,'#,##0.00' )"/>
				            	</td>
				            </xsl:when>
			          	</xsl:choose>
			          </xsl:for-each>
			  			</tr>
		    		</xsl:when>
	        </xsl:choose>
	   		</xsl:for-each>
	 		</tbody>
	  </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>