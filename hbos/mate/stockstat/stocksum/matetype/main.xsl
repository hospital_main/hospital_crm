<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<thead>
  		<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=1">
	    			<tr noWrap='true' class='mainHead'>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test=" position() &lt; 3 or position()>4 ">
		          			<th valign="middle" >
				            	<xsl:value-of select="."/>
		          			</th>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test=" position()=3 ">
		          			<th valign="middle" >
				            	<xsl:value-of select="."/>
		          			</th>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test=" position()>1 ">
	    			<tr>
		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		<xsl:when test=" position()=1 ">
			            	<td><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test=" position()=2 ">
			            	<td ><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test="position() >4 ">
			            	<td  align="right" ><xsl:value-of select="format-number(.,'#,##0.00' )"/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		<xsl:when test=" position()=3 ">
			            	<td align="right">
			            		<xsl:value-of select="format-number(.,'#,##0.00' )"/>
			            	</td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>