<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-2"/>
	  <root>
	  	<thead>
		    <tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount">供应商月进货明细</td>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      </tr>
			</thead>
			<tbody>
		  	<xsl:for-each select="/root/tbody/tr">
		  		<xsl:variable name="rowIndex" select="position()"/>
		  		<xsl:variable name="colValue" select="td[1]"/>
		  		<xsl:if test="$rowIndex=1 or td[1] != ../tr[$rowIndex - 1]/td[1]">
		  			<xsl:for-each select="/root/tbody/tr[td[1] = $colValue]">
	          	<xsl:choose>
	          		<xsl:when test=" position()=1 ">
	          			<tr>
	          				<xsl:if test="$rowIndex = 1">
	          				<td style="colspan:4;">
	          					<xsl:value-of select="td[2]"/>
	          				</td>
	          				</xsl:if>
	          				<xsl:if test="$rowIndex >1">
	          				<td style="colspan:4;pagebreak:true">
	          					<xsl:value-of select="td[2]"/>
	          				</td>
	          				</xsl:if>
	          				<td style="display:none"/>
	          				<td style="display:none"/>
	          				<td style="display:none"/>
	          			</tr>
	          			<tr>
						  	  	<td noWrap="true">单据号</td>
						  			<td noWrap="true">制单日期</td>
						        <td noWrap="true">经办人</td>
						  			<td noWrap="true">金额</td>
							  	 </tr>
	          		</xsl:when>
	          		<xsl:when test=" position() &gt; 1">
		            	<tr>
					          <xsl:for-each select="td">
					          	<xsl:choose>
					          		<xsl:when test=" position()=2 or position()=3 or position()=4">
						            	<td><xsl:value-of select="."/></td>
						            </xsl:when>
						            <xsl:when test="position()=5 ">
						            	<td  align="right" ><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						            </xsl:when>
					          	</xsl:choose>
					          </xsl:for-each>
							  	</tr>
		            </xsl:when>
	          	</xsl:choose>
			      </xsl:for-each>
			      <tr>
			      	<td noWrap="true" style="colspan:2">主管院领导：</td>
			      	<td style="display:none"/>
			      	<td noWrap="true" style="colspan:2">科室负责人：</td>
			      	<td style="display:none"/>
			      </tr>
			      <tr>
			      	<td noWrap="true" style="colspan:2">采购：<xsl:value-of select="/root/tbody/tr[$rowIndex+1]/td[6]"/></td>
			      	<td style="display:none"/>
			      	<td noWrap="true" style="colspan:2">会计：</td>
			      	<td style="display:none"/>
			      </tr>
					</xsl:if>
				</xsl:for-each>
			</tbody>
	  </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>