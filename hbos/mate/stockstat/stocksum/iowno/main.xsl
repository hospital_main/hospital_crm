<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<thead>
  		<tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">单据号</th>
  			<th noWrap="true">制单日期</th>
        <th noWrap="true">经办人</th>
  			<th noWrap="true">金额</th>
  	  </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
  			<tr>
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test=" position()=1 or position()=2 or position()=3">
	            	<td><xsl:value-of select="."/></td>
	            </xsl:when>
	            <xsl:when test="position()=4 ">
	            	<td  align="right" ><xsl:value-of select="format-number(.,'#,##0.00' )"/></td>
	            </xsl:when>
	            <xsl:when test="position()=5 ">
	            	<td style="display:none" ><xsl:value-of select="."/></td>
	            </xsl:when>
          	</xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>