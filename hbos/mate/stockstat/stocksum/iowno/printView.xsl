<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-2"/>
	  <root>
	    <thead>
		    <tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      	<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	      </tr>
	      <tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
	      	<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	      </tr>
		 		<tr noWrap='true' class='mainHead'>
	  	  	<td noWrap="true">单据号</td>
	  			<td noWrap="true">制单日期</td>
	        <td noWrap="true">经办人</td>
	  			<td noWrap="true">金额</td>
	  	  </tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	  			<tr>
	          <xsl:for-each select="td">
	          	<xsl:choose>
	          		<xsl:when test=" position()=1 or position()=2 or position()=3">
		            	<td><xsl:value-of select="."/></td>
		            </xsl:when>
		            <xsl:when test="position()=4 ">
		            	<td  align="right" ><xsl:value-of select="format-number(.,'#,##0.00' )"/></td>
		            </xsl:when>
	          	</xsl:choose>
	          </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
	 		<tfoot>
	    	<tr noWrap="true" class="mainHead">
	    		<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
	    		<td style="display:none"/>
	  	  	<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
	  	  	<td style="display:none"/>
	    	</tr>
	    	<tr noWrap="true" class="mainHead">
	    		<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
	    		<td style="display:none"/>
	  	  	<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
	  	  	<td style="display:none"/>
	    	</tr>
	    </tfoot>
	  </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>