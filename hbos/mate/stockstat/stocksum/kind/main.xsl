<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=' '/>  
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th noWrap="true" >序号</th>
				<th noWrap="true" >供应商编码</th>
				<th noWrap="true" >供应商名称</th>
				<th noWrap="true" >类别名称</th>	
				<th noWrap="true" >入库金额</th>
				<th noWrap="true" >结算方式</th>  			  			
				<th noWrap="true" >开户银行</th>  			
				<th noWrap="true" >银行账号</th>  			
				<th noWrap="true" >联系人</th>  			
				<th noWrap="true">联系电话</th>  			
			</tr>
		</thead>
		<tbody>
			<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
			<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td><xsl:value-of select="position()"/>　</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td >
								<a href="#">
								<xsl:attribute name="onclick" >
								javascript:openDialog('../ven/update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:700px;overflow:auto;', '','true')
								</xsl:attribute>
								<xsl:value-of select="."/>
								</a>　
								</td>
							</xsl:when>
							<xsl:when test="position()=4">
								<td  align='right'><xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>　</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/>　</td>
							</xsl:otherwise>			                        
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			<tr>
				<td>合计：</td>
				<td>　</td>
				<td>　</td>
				<td>　</td>
				<td align='right' ><xsl:value-of select="format-number(sum(/root/tbody/tr[td[2]!='']/td[4]),$VHMONEYFORMAT)"/></td>
				<td >　</td>
				<td >　</td>
				<td >　</td>
				<td >　</td>
				<td >　</td>
			</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>