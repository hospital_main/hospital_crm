<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
      <thead>
      <tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	     </tr>
	     <tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	     </tr>
  	 	<tr noWrap='true' class='mainHead'>
				<td noWrap="true" style="fontsize:coltitle;">序号</td>
				<td noWrap="true"  style="fontsize:coltitle;">供应商编码</td>
				<td noWrap="true"  style="fontsize:coltitle;">供应商名称</td>
				<td noWrap="true" style="fontsize:coltitle;" >类别名称</td>	
				<td noWrap="true"  style="fontsize:coltitle;">入库金额</td>
				<td noWrap="true"  style="fontsize:coltitle;">结算方式</td>  			  			
				<td noWrap="true" style="fontsize:coltitle;" >开户银行</td>  			
				<td noWrap="true"  style="fontsize:coltitle;">银行账号</td>  			
				<td noWrap="true"  style="fontsize:coltitle;">联系人</td>  			
				<td noWrap="true" style="fontsize:coltitle;">联系电话</td>  		
			</tr>
  	</thead>
  	<tbody>
			<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
			<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td><xsl:value-of select="position()"/>　</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td >
								<a href="#">
								<xsl:attribute name="onclick" >
								javascript:openDialog('../ven/update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:700px;overflow:auto;', '','true')
								</xsl:attribute>
								<xsl:value-of select="."/>
								</a>　
								</td>
							</xsl:when>
							<xsl:when test="position()=4">
								<td  align='right'><xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>　</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/>　</td>
							</xsl:otherwise>			                        
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			<tr>
				<td>合计：</td>
				<td>　</td>
				<td>　</td>
				<td>　</td>
				<td align='right' ><xsl:value-of select="format-number(sum(/root/tbody/tr[td[2]!='']/td[4]),$VHMONEYFORMAT)"/></td>
				<td >　</td>
				<td >　</td>
				<td >　</td>
				<td >　</td>
				<td >　</td>
			</tr>
		</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>