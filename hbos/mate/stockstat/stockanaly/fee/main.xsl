<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">物资类别</th>
  			<th noWrap="true">采购次数</th>
        <th noWrap="true">采购金额</th>
  			<th noWrap="true">百分比</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <xsl:for-each select="/root/tbody/tr">
	     <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test="position()=2">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
	           	<xsl:when test="position()=3">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=4">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>%
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=5">
		          </xsl:when>
							<xsl:otherwise>
			          <td>
         			    <xsl:value-of select="."/>
        			  </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>