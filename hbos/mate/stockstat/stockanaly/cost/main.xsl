<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">材料编码</th>
  			<th noWrap="true" rowspan="2">材料名称</th>
        <th noWrap="true" rowspan="2">规格型号</th>
  			<th noWrap="true" rowspan="2">计量单位</th>
  			<th noWrap="true" rowspan="2">入库日期</th>
  			<th noWrap="true" colspan="2">实际成本</th>
  			<th noWrap="true" rowspan="2">参考成本</th>
        <th noWrap="true" rowspan="2">差额</th>
  			<th noWrap="true" rowspan="2">差异率</th>
  	  </tr>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">数量</th>
  			<th noWrap="true">总价</th>
      </tr>
  	</thead>
  	<tbody> 
  	  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <xsl:for-each select="/root/tbody/tr">
	     <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	           	<xsl:when test="position()=1 or position()=2 or position()=3 or position()=4 or position()=5">
		           	<td>
         			    <xsl:value-of select="."/>
        			  </td>
		          </xsl:when>
		          <xsl:when test="position()=7 or position()=8 or position()=9">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=10">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>%
	              </td>
		          </xsl:when>
							<xsl:otherwise>
			          <td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>