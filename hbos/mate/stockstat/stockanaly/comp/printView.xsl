<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
	 	  	<tr noWrap='true' class='mainHead'>
	  	  	<td noWrap="true" rowspan="2">供应商名称</td>
	  	  	<td noWrap="true" rowspan="2">采购数量</td>
	  	  	<td noWrap="true" colspan="2">实际采购平均价格</td>
	  			<td style="display:none"/>
	  			<td noWrap="true" colspan="2">招标价格</td>
	  			<td style="display:none"/>
	  			<td noWrap="true" colspan="2">差额</td>
	  			<td style="display:none"/>
	  	  </tr>
	      <tr noWrap="true" class="mainHead">  	
	        <td style='display:none'/>
	        <td style='display:none'/>
	        <td noWrap="true">价格</td>
	  			<td noWrap="true">金额</td>
	  		  <td noWrap="true">招标价格</td>
	  			<td noWrap="true">金额</td>
	  			<td noWrap="true">差价</td>
	  			<td noWrap="true">差额</td>
	  		</tr>
      </thead>	      
      <tbody>
      <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	      <xsl:for-each select="/root/tbody/tr">
	        <tr>
	        	<xsl:for-each select="td">          
	            <xsl:choose>            
	              <xsl:when test="position()= 1">
	              </xsl:when>
	              <xsl:when test="position()=3">
			           	<td  class='numberText' align='right' nowrap='true'>
		                <xsl:value-of select="format-number(.,'#,##0.00')"/>
		              </td>
			          </xsl:when>
		           	<xsl:when test="position()=4 or position()=6 or position()=8">
			           	<td class='numberText' align='right' nowrap='true'>
		                <xsl:value-of select="format-number(.,'#,##0.0000')"/>
		              </td>
			          </xsl:when>
			          <xsl:when test="position()=5 or position()=7 or position()=9">
			           	<td class='numberText' align='right' nowrap='true'>
		                <xsl:value-of select="format-number(.,'#,##0.00')"/>
		              </td>
			          </xsl:when>  
	              <xsl:otherwise>
	              	<td align="left">
	                	<xsl:value-of select="."/>
	                </td>
	              </xsl:otherwise>
	            </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>