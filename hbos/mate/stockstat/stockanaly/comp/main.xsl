<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">供应商名称</th>
  			<th noWrap="true" rowspan="2">采购数量</th>
        <th noWrap="true" colspan="2">实际采购平均价格</th>
  			<th noWrap="true" colspan="2">招标价格</th>
  			<th noWrap="true" colspan="2">差额</th>
  	  </tr>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">价格</th>
  			<th noWrap="true">金额</th>
        <th noWrap="true">招标价格</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">差价</th>
  			<th noWrap="true">差额</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <xsl:for-each select="/root/tbody/tr">
	     <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=3">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
	           	<xsl:when test="position()=4 or position()=6 or position()=8">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=5 or position()=7 or position()=9">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
		          </xsl:when>
							<xsl:when test="position()=1">
							</xsl:when>
							<xsl:otherwise>
			          <td>
         			    <xsl:value-of select="."/>
        			  </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>