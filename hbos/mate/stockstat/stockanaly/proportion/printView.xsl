<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	  		<thead>
	  			<tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			   <tr noWrap="true" class="mainHead">
			   	<td>材料编码</td>
			   	<td>材料名称</td>
			   	<td>规格型号</td>
			   	<td>计量单位</td>
			   	<td>采购数量</td>
			   	<td>百分比</td>
			   	<td>采购金额</td>
			   	<td>百分比</td>
			   </tr>
	  		</thead>
			 	<tbody>
			 	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  	    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
		      <xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<xsl:for-each select="td">          
		            <xsl:choose>            
			            <xsl:when test="position()=7">
				           	<td class='numberText' align='right'>
			                <xsl:value-of select="format-number(.,'#,##0.00')"/>
			              </td>
				          </xsl:when>
		              <xsl:when test="position()=5" >
                    <td class='numberText' nowrap='true' align='right'>
                      <xsl:value-of select="format-number(.,'#,##0.00')"/>       		 	
          		      </td>
                  </xsl:when>
                  <xsl:when test="position()=6 or position()=8">
                    <td class='numberText' nowrap='true' align='right'>
                      <xsl:value-of select="format-number(.,'#,##0.00')"/>%       		 	
          		      </td>
                  </xsl:when>
		            	<xsl:otherwise>
    			          <td nowrap='true'>
    	                <xsl:value-of select="."/>
    	              </td>
    	            </xsl:otherwise>			
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
	      </tbody>
	    
	  </root>
	</xsl:template>
</xsl:stylesheet>