<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">供应商编码</th>
  	  	<th noWrap="true">供应商名称</th>
  	  	<th noWrap="true">材料编码</th>
  			<th noWrap="true">材料名称</th>
  			<th noWrap="true">主要供应商</th>
  			<th noWrap="true">型号</th>
  			<th noWrap="true">单位</th>
        <th noWrap="true">入库价格</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <xsl:for-each select="/root/tbody/tr">
	     <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=8">
		           	<td align='right'>
	                <xsl:value-of select="format-number(.,$VHPRICEFORMAT )"/>
	              </td>
		          </xsl:when>
							<xsl:otherwise>
			          <td>
         			    <xsl:value-of select="."/>
        			  </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>