<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/> 
      <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">科室名称</th>
  			<th noWrap="true">材料编码</th>
  			<th noWrap="true">材料名称</th>
  		  <th noWrap="true">型号</th>
  			<th noWrap="true">计量单位</th>
  			<th noWrap="true">消耗数量</th>
  			<th noWrap="true">消耗金额</th>
  			<th noWrap="true">工作量</th>
  			<th noWrap="true">单位工作量消耗数量</th>
  			<th noWrap="true">单位工作量消耗金额</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=6 or position()=7 or position()=8 or position()=9 or position()=10">
		          	<td align='right'>
         					<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
        			  </td>
							</xsl:when>
				  		<xsl:otherwise>
			          <td >
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>