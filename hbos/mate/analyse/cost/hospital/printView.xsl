<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
  <thead>
		<tr noWrap="true" class="mainHead">
						<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr noWrap="true" class="mainHead">
						<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
					</tr>
				<tr noWrap="true" class="mainHead">
						<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
					</tr>
  	  <tr noWrap='true' class='mainHead'>
  	  	<td noWrap="true" rowspan="2">物资类别</td>
  	  	<td noWrap="true" rowspan="2">本期费用(元)</td>
  	  	<td noWrap="true" colspan="3">与上期比较</td>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  			<td noWrap="true" colspan="3">与去年同期比较</td>
  			<td style="display:none"/>
  			<td style="display:none"/>
  			<td noWrap="true" colspan="3">与预算比较</td>
  			<td style="display:none"/>
  			<td style="display:none"/>
  			<td noWrap="true" colspan="3">与平均比较</td>
  			<td style="display:none"/>
  			<td style="display:none"/>
  	  </tr>
      <tr noWrap="true" class="mainHead">  
        <td style="display:none"/>
        <td style="display:none"/>	
  			<td noWrap="true">上期费用(元)</td>
  			<td noWrap="true">差额(元)</td>
  			<td noWrap="true">差异率</td>
  			<td noWrap="true">同期费用(元)</td>
  			<td noWrap="true">差额(元)</td>
  			<td noWrap="true">差异率</td>
        <td noWrap="true">预算费用(元)</td>
  			<td noWrap="true">差额(元)</td>
  			<td noWrap="true">差异率</td>
        <td noWrap="true">平均费用(元)</td>
  			<td noWrap="true">差额(元)</td>
  			<td noWrap="true">差异率</td>
  	  </tr>
  	</thead>
    <tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1">
		          	<xsl:variable name="islast">
		          	  <xsl:value-of select ="../td[position()=2]"/>
		          	</xsl:variable>
		          	<td>
         					<xsl:if test="$islast='yes'">
         					  <a>
              			  <xsl:attribute name="href" >
              			  	javascript:date_year.value='<xsl:value-of select ="../td[position()=4]"/>';date_month.value='<xsl:value-of select ="../td[position()=5]"/>';mate_type_code.value='<xsl:value-of select ="../td[position()=3]"/>';mate_type.value='<xsl:value-of select ="../td[position()=1]"/>';openDialog('detail/main.html','dialogWidth:800px;dialogHeight:650px');
      			  			  </xsl:attribute>
		                  <xsl:value-of select="."/>
        					  </a>
         					</xsl:if>
         					<xsl:if test="$islast='no'">
         					  <xsl:value-of select="."/>
        					</xsl:if>
        				 
		            </td>
							</xsl:when>
							<xsl:when test="position()=2 or position()=3 or position()=4 or position()=5">
	  					</xsl:when>
	  					<xsl:when test="position()=9 or position()=12 or position()=15 or position()=18">
	  					  <td align='right' class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>%
	              </td>
	  					</xsl:when>
			    		<xsl:otherwise>
			          <td align='right' class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



