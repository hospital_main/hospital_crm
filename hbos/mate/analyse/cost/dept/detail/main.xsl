<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">材料名称</th>
  	  	<th noWrap="true" rowspan="2">本期费用(元)</th>
  	  	<th noWrap="true" colspan="3">与上期比较</th>
  			<th noWrap="true" colspan="3">与去年同期比较</th>
  		  <th noWrap="true" colspan="3">与品均比较</th>
  	  </tr>
      <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">上期费用(元)</th>
  			<th noWrap="true">差额(元)</th>
  			<th noWrap="true">差异率(%)</th>
  			<th noWrap="true">同期费用(元)</th>
  			<th noWrap="true">差额(元)</th>
  			<th noWrap="true">差异率(%)</th>
        <th noWrap="true">平均费用(元)</th>
  			<th noWrap="true">差额(元)</th>
  			<th noWrap="true">差异率(%)</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1">
		          	<xsl:variable name="islast">
		          	  <xsl:value-of select ="../td[position()=2]"/>
		          	</xsl:variable>
		          	<td>
         					<xsl:if test="@islast='yes'">
         					  <a>
              			  <xsl:attribute name="href" >
                		  	javascript:mate_type.text= '<xsl:value-of select="../td[position()=3]"/>';openDialog('detail/main.html','dialogWidth:800px;dialogHeight:650px')
      			  			  </xsl:attribute>
		                  <xsl:value-of select="."/>
        					  </a>
         					</xsl:if>
         					<xsl:if test="@islast='no'">
         					  <xsl:value-of select="."/>
        					</xsl:if>
		            </td>
							</xsl:when>
							<xsl:when test="position()=7 or position()=10 or position()=13">
	  					  <td align="right">
	  					    <xsl:value-of select="."/>
   					    </td>
							</xsl:when>
			    		<xsl:otherwise>
			          <td align='right'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>