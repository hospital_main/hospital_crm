<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">物资类别</th>
  	  	<th noWrap="true" rowspan="2">本期费用(元)</th>
  	  	<th noWrap="true" colspan="3">与上期比较</th>
  			<th noWrap="true" colspan="3">与去年同期比较</th>
  			<th noWrap="true" colspan="3">与预算比较</th>
  			<th noWrap="true" colspan="3">与平均比较</th>
  	  </tr>
      <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">上期费用(元)</th>
  			<th noWrap="true">差额(元)</th>
  			<th noWrap="true">差异率</th>
  			<th noWrap="true">同期费用(元)</th>
  			<th noWrap="true">差额(元)</th>
  			<th noWrap="true">差异率</th>
        <th noWrap="true">预算费用(元)</th>
  			<th noWrap="true">差额(元)</th>
  			<th noWrap="true">差异率</th>
        <th noWrap="true">平均费用(元)</th>
  			<th noWrap="true">差额(元)</th>
  			<th noWrap="true">差异率</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	     <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <!--xsl:when test="position()=1">
		          	<xsl:variable name="islast">
		          	  <xsl:value-of select ="../td[position()=2]"/>
		          	</xsl:variable>
		          	<td>
         					<xsl:if test="$islast='yes'">
         					  <a>
              			  
              			  <xsl:attribute name="href" >
              			  	javascript:date_year.value='<xsl:value-of select ="../td[position()=4]"/>';date_month.value='<xsl:value-of select ="../td[position()=5]"/>';mate_type_code.value='<xsl:value-of select ="../td[position()=3]"/>';mate_type.value='<xsl:value-of select ="../td[position()=1]"/>';openDialog('detail/main.html','dialogWidth:800px;dialogHeight:650px');
      			  			  </xsl:attribute>
		                  <xsl:value-of select="."/>
        					  </a>
         					</xsl:if>
         					<xsl:if test="@islast='no'">
         					  <xsl:value-of select="."/>
        					</xsl:if>
        				 
		            </td>
							</xsl:when-->
							<xsl:when test="position()=1">
		           	<td>
         			    <xsl:value-of select="."/>
        			  </td>
							</xsl:when>
							<xsl:when test="position()=2 or position()=3 or position()=4 or position()=5">
	  					</xsl:when>
	  					<xsl:when test="position()=9 or position()=12 or position()=15 or position()=18">
	  					  <td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>%
	              </td>
	  					</xsl:when>
				  		<xsl:otherwise>
			          <td align='right'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>