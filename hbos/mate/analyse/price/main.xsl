<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">科室</th>
  	   	<th noWrap="true" colspan="3">本期</th>
  			<th noWrap="true" colspan="3">基期</th>
  			<th noWrap="true" rowspan="2">料价变动后<br/>单位工作量消耗(元)</th>
  			<th noWrap="true" rowspan="2">料价变动后<br/>应耗费用(元)</th>
        <th noWrap="true" rowspan="2">料价变动<br/>造成费用增减(元)</th>  	  
  	    <th noWrap="true" rowspan="2">工作量变动<br/>费用增减(元)</th>
  			<th noWrap="true" rowspan="2">非正常性消耗(元)</th>
      </tr>
      <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">费用(元)</th>
  			<th noWrap="true">工作量</th>
  			<th noWrap="true">单位工作量消耗</th>
  			<th noWrap="true">费用(元)</th>
  			<th noWrap="true">工作量</th>
  			<th noWrap="true">单位工作量消耗</th>
  			
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:variable name="islast">
		          	  <xsl:value-of select ="../td[position()=2]"/>
		        </xsl:variable>
	          <xsl:choose>
	            <xsl:when test="position()=1">
	              <!--xsl:variable name="islast">
		          	  <xsl:value-of select ="../td[position()=2]"/>
		          	</xsl:variable-->
		           	<td>
         				  <xsl:value-of select="."/>
        			  </td>
							</xsl:when>
							<xsl:when test="position()=2 or position()=3 or position()=15 or position()=16 or position()=17 or position()=18 or position()=19" >
	  					</xsl:when>
							<xsl:when test="position()=5 or position()=8">
	  					  <td align="right">
	  					    <xsl:value-of select="."/>
   					    </td>
							</xsl:when>
							<xsl:when test="position()=10">
		          	<td align="right">
         					<xsl:if test="$islast='yes'">
         					  <a>
              			  <xsl:attribute name="href" >
              			  	javascript:date_year.value='<xsl:value-of select ="../td[position()=15]"/>';date_month.value='<xsl:value-of select ="../td[position()=16]"/>';date_year1.value='<xsl:value-of select ="../td[position()=17]"/>';date_month1.value='<xsl:value-of select ="../td[position()=18]"/>';dept_code.value='<xsl:value-of select ="../td[position()=3]"/>';dept_name.value='<xsl:value-of select ="../td[position()=1]"/>';work_load.value='<xsl:value-of select ="../td[position()=8]"/>';work_load1.value='<xsl:value-of select ="../td[position()=19]"/>';openDialog('detail.html','dialogWidth:800px;dialogHeight:650px');
      			  			  </xsl:attribute>
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
        					  </a>
         					</xsl:if>
         					<xsl:if test="$islast='no'">
         					  <xsl:value-of select="format-number(.,'#,##0.00')"/>
        					</xsl:if>
        				</td>
							</xsl:when>
			    		<xsl:otherwise>
			          <td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>