<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">材料名称</th>
  	  	<th noWrap="true" rowspan="2">消耗数量</th>
  	  	<th noWrap="true" colspan="3">本期</th>
  			<th noWrap="true" colspan="3">基期</th>
  		</tr>
      <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">单价(元)</th>
  			<th noWrap="true">金额(元)</th>
  			<th noWrap="true">单位工作量消耗(元)</th>
  			<th noWrap="true">单价(元)</th>
  			<th noWrap="true">金额(元)</th>
  			<th noWrap="true">单位工作量消耗(元)</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	     <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1">
		          	<td>
		          	  <xsl:value-of select="."/>
        			  </td>
							</xsl:when>
							<xsl:when test="position()=2">
		          	<td align="right">
		          	  <xsl:value-of select="."/>
        			  </td>
							</xsl:when>
							<xsl:otherwise>
			          <td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>