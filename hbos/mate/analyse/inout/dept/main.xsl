<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2" valign="middle">收费项目编码</th>
  	  	<th noWrap="true" rowspan="2" valign="middle">收费项目名称</th>
  	  	<th noWrap="true" colspan="2">材料领用</th>
  	  	<th style="display:none">材料领用</th>
  	  	<th noWrap="true" colspan="2">材料收入</th>
  	  	<th style="display:none">材料收入</th>
  	  	<th noWrap="true" colspan="2">差异</th>
  	  	<th style="display:none">差异</th>
  	  	<th noWrap="true" colspan="2">差异率</th>
  	  	<th style="display:none">差异率</th>
  	  </tr>
      <tr noWrap="true" class="mainHead">
      	<th style="display:none">收费项目编码</th>
      	<th style="display:none">收费项目名称</th> 	
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1">
		          	<td>
         					<xsl:value-of select="."/>
        			  </td>
							</xsl:when>
							<xsl:when test="position()=2">
		          	<td>
	            		<xsl:value-of select="."/>
        			  </td>
							</xsl:when>
							<xsl:when test="position()=3 or position()=5 or position()=7 ">
	  					  <td align="right">
	  					    <xsl:value-of select="format-number(.,'#,##0.00')"/>
   					    </td>
							</xsl:when>
							<xsl:when test="position()=9 or position()=10">
	  					  <td align="right">
	  					    <xsl:value-of select="format-number(.,'#,##0.00')"/>%
   					    </td>
							</xsl:when>
			    		<xsl:otherwise>
			          <td align='right'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>