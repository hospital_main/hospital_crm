<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">物资类别</th>
  	  	<th noWrap="true" rowspan="2">本期期末余额(元)</th>
  	  	<th noWrap="true" colspan="3">与上期比较</th>
  			<th noWrap="true" colspan="3">与去年同期比较</th>
  			<th noWrap="true" colspan="3">与定额比较</th>
  			<th noWrap="true" colspan="3">与平均比较</th>
  	  </tr>
      <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">上期期末余额(元)</th>
  			<th noWrap="true">差额(元)</th>
  			<th noWrap="true">差异率</th>
  			<th noWrap="true">同期期末余额(元)</th>
  			<th noWrap="true">差额(元)</th>
  			<th noWrap="true">差异率</th>
        <th noWrap="true">储备定额(元)</th>
  			<th noWrap="true">差额(元)</th>
  			<th noWrap="true">差异率</th>
        <th noWrap="true">平均期末余额(元)</th>
  			<th noWrap="true">差额(元)</th>
  			<th noWrap="true">差异率</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1">
		          	<td>
         					<xsl:value-of select="."/>
        			  </td>
							</xsl:when>
							<xsl:when test="position()=5 or position()=8 or position()=11 or position()=14">
	  					  <td align="right">
	  					    <xsl:value-of select="format-number(.,'#,##0.00')"/>%
   					    </td>
							</xsl:when>
			    		<xsl:otherwise>
			          <td align='right'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>