<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
      <thead>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
  	  <tr noWrap='true' class='mainHead'>
  	  	<td noWrap="true">仓库名称</td>
  	  	<td noWrap="true">期末库存(元)</td>
  	  	<td noWrap="true">本期消耗总额(元)</td>
  			<td noWrap="true">报废总额(元)</td>
  			<td noWrap="true">占库存比率</td>
  			<td noWrap="true">占总消耗的比率</td>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr noWrap='true' >
	        <xsl:if test="position()!=last()">
  	        <xsl:for-each select="td">
  	          <xsl:choose>
  	            <xsl:when test="position()=1">
  		            <td noWrap='true' >
  		              <a>
            			    <xsl:attribute name="href" >
              		    	javascript:store_code.value= '<xsl:value-of select="../td[position()=2]"/>';store_name.value= '<xsl:value-of select="../td[position()=1]"/>';b_date_year.value='<xsl:value-of select="../td[position()=3]"/>';b_date_month.value='<xsl:value-of select="../td[position()=4]"/>';
              		    	e_date_year.value='<xsl:value-of select="../td[position()=5]"/>';e_date_month.value='<xsl:value-of select="../td[position()=6]"/>';
              		      openDialog('detail/main.html','dialogWidth:800px;dialogHeight:650px')
    			  			    </xsl:attribute>
                      <xsl:value-of select="."/>
      					    </a>
      					  </td>
  							</xsl:when>
  							<xsl:when test="position()=2 or position()=3 or position()=4 or position()=5 or position()=6">
  		          </xsl:when>
  		          <xsl:when test="position()=10 or position()=11">
  			          <td align='right' noWrap='true'  class="numberText">
  	                <xsl:value-of select="format-number(.,'#,##0.00')"/>%
  	              </td>
  	            </xsl:when>
  		        	<xsl:otherwise>
  			          <td align='right' noWrap='true'  class="numberText">
  	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
  	              </td>
  	            </xsl:otherwise>			                        
  	          </xsl:choose>
  	        </xsl:for-each>
	        </xsl:if>
	        <xsl:if test="position()=last()">
  	        <xsl:for-each select="td">
  	          <xsl:choose>
  	            <xsl:when test="position()=1">
  		            <td noWrap='true' >
  		              <xsl:value-of select="."/>
      					  </td>
  							</xsl:when>
  							<xsl:when test="position()=2 or position()=3 or position()=4 or position()=5 or position()=6">
  		          </xsl:when>
  		          <xsl:when test="position()=10 or position()=11">
  			          <td align='right' noWrap='true'  class="numberText">
  	                <xsl:value-of select="format-number(.,'#,##0.00')"/>%
  	              </td>
  	            </xsl:when>
  		        	<xsl:otherwise>
  			          <td align='right' noWrap='true'  class="numberText">
  	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
  	              </td>
  	            </xsl:otherwise>			                        
  	          </xsl:choose>
  	        </xsl:for-each>
	        </xsl:if>
	      </tr>
	    </xsl:for-each>
   	</tbody>
   	</root>
  </xsl:template>
</xsl:stylesheet>



