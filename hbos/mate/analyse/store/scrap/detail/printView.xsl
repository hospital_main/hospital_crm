<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
      <thead>
      <tr noWrap='true' class='mainHead'>
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
  	  <tr noWrap='true' class='mainHead'>
  	  	<td noWrap="true">物资类别</td>
  	  	<td noWrap="true">期末库存(元)</td>
  	  	<td noWrap="true">本期消耗总额(元)</td>
  			<td noWrap="true">报废总额(元)</td>
  			<td noWrap="true">占库存比率</td>
  			<td noWrap="true">占总消耗的比率</td>
  	  </tr>
  	</thead>
  	<tbody> 
  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	             <xsl:when test="position()=1">
		          	<xsl:variable name="islast">
		          	  <xsl:value-of select ="../td[position()=2]"/>
		          	</xsl:variable>
		          	<td>
         					<xsl:if test="$islast='yes'">
         					  <a href="#">
              			  <xsl:attribute name="onclick" >
                		  	javascript:mate_type.value= '<xsl:value-of select="../td[position()=3]"/>';mate_type_name.value= '<xsl:value-of select="../td[position()=1]"/>';openDialog('detail1/main.html','dialogWidth:800px;dialogHeight:650px')
      			  			  </xsl:attribute>
		                  <xsl:value-of select="."/>
        					  </a>
         					</xsl:if>
         					<xsl:if test="$islast='no'">
         					  <xsl:value-of select="."/>
        					</xsl:if>
		            </td>
							</xsl:when>
							<xsl:when test="position()=2 or position()=3">
		          </xsl:when>
						  <xsl:when test="position()=7 or position()=8">
		            <td align='right' class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>%
	              </td>
		          </xsl:when>
							<xsl:otherwise>
			          <td align='right' class="numberText">
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
   	</root>
  </xsl:template>
</xsl:stylesheet>



