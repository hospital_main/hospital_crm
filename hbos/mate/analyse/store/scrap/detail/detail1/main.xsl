<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">材料编码</th>
  	  	<th noWrap="true">材料名称</th>
  	    <th noWrap="true">期末库存(元)</th>
  	  	<th noWrap="true">本期消耗总额(元)</th>
  			<th noWrap="true">报废总额(元)</th>
  			<th noWrap="true">占库存比率</th>
  			<th noWrap="true">占总消耗的比率</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1 or position()=2">
		            <td>
		              <xsl:value-of select="."/>
    					  </td>
    					</xsl:when>
    					<xsl:when test="position()=6 or position()=7">
		            <td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>%
	              </td>
		          </xsl:when>
    				  <xsl:otherwise>
			          <td align='right'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>