<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">仓库名称</th>
  	  	<th noWrap="true">期初库存</th>
  	  	<th noWrap="true">期末库存</th>
  			<th noWrap="true">本期消耗</th>
  			<th noWrap="true">周转次</th>
  			<th noWrap="true">周转天</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:if test="position()!=last()">
  	        <xsl:for-each select="td">
  	          <xsl:choose>
  	            <xsl:when test="position()=1">
  		            <td>
  		              <a>
              			  <xsl:attribute name="href">
                		  	javascript:mate_whr.code= '<xsl:value-of select="../td[position()=2]"/>';store_name.value= '<xsl:value-of select="../td[position()=1]"/>';b_date_year.value='<xsl:value-of select="../td[position()=3]"/>';b_date_month.value='<xsl:value-of select="../td[position()=4]"/>';
                		  	e_date_year.value='<xsl:value-of select="../td[position()=5]"/>';e_date_month.value='<xsl:value-of select="../td[position()=6]"/>';
                		  	turnover_time1.value='<xsl:value-of select="../td[position()=7]"/>';turnover_time2.value='<xsl:value-of select="../td[position()=8]"/>';
                		  	turnover_day1.value='<xsl:value-of select="../td[position()=9]"/>';turnover_day2.value='<xsl:value-of select="../td[position()=10]"/>';
                		  	openDialog('detail/main.html','dialogWidth:800px;dialogHeight:650px')
      			  			  </xsl:attribute>
                      <xsl:value-of select="."/>
        					  </a>
        					</td>
      					</xsl:when>
  							<xsl:when test="position()=2 or position()=3 or position()=4 or position()=5  or position()=6 or position()=7 or position()=8  or position()=9  or position()=10">
  		          </xsl:when>
  		          <!--xsl:when test="position()=11 or position()=12 or position()=13">
  			          <td align='right'>
  	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
  	              </td>
  	            </xsl:when-->
  	            <xsl:when test="position()=11 or position()=12 or position()=13">
  			          <td align='right'>
  	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
  	              </td>
  	            </xsl:when>
  							<xsl:otherwise>
  			          <td align='right'>
  	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
  	              </td>
  	            </xsl:otherwise>			                        
  	          </xsl:choose>
  	        </xsl:for-each>
	        </xsl:if>
	        <xsl:if test="position()=last()">
  	        <xsl:for-each select="td">
  	          <xsl:choose>
  	            <xsl:when test="position()=1">
  		            <td>
  		              <xsl:value-of select="."/>
        					</td>
      					</xsl:when>
  							<xsl:when test="position()=2 or position()=3 or position()=4 or position()=5  or position()=6 or position()=7 or position()=8  or position()=9  or position()=10">
  		          </xsl:when>
  		          <!--xsl:when test="position()=11 or position()=12 or position()=13">
  			          <td align='right'>
  	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
  	              </td>
  	            </xsl:when-->
  	            <xsl:when test="position()=11 or position()=12 or position()=13">
  			          <td align='right'>
  	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
  	              </td>
  	            </xsl:when>
  							<xsl:otherwise>
  			          <td align='right'>
  	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
  	              </td>
  	            </xsl:otherwise>			                        
  	          </xsl:choose>
  	        </xsl:for-each>
	        </xsl:if>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>