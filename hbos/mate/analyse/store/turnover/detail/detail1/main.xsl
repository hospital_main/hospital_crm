<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">材料代码</th>
  	  	<th noWrap="true">材料名称</th>
  	  	<th noWrap="true">规格型号</th>
  	  	<th noWrap="true">期初库存</th>
  	  	<th noWrap="true">期末库存</th>
  			<th noWrap="true">本期消耗</th>
  			<th noWrap="true">周转次</th>
  			<th noWrap="true">周转天</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1 or position()=2 or position()=3">
		            <td>
		              <xsl:value-of select="."/>
    					  </td>
    					</xsl:when>
    					<xsl:when test="position()=4 or position()=5 or position()=6">
		            <td align='right' class="numberText">
		              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
    					  </td>
    					</xsl:when>
							<xsl:otherwise>
			          <td align='right' class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:otherwise>	                      
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>