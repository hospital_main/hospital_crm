<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">物资类别</th>
  	  	<th noWrap="true">期初库存</th>
  	  	<th noWrap="true">期末库存</th>
  			<th noWrap="true">本期消耗</th>
  			<th noWrap="true">周转次</th>
  			<th noWrap="true">周转天</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	             <xsl:when test="position()=1">
		          	<xsl:variable name="islast">
		          	  <xsl:value-of select ="../td[position()=2]"/>
		          	</xsl:variable>
		          	<td>
         					<xsl:if test="$islast='yes'">
         					  <a href="#">
              			  <xsl:attribute name="onclick" >
                		  	javascript:mate_type.value= '<xsl:value-of select="../td[position()=3]"/>';mate_type_name.value= '<xsl:value-of select="../td[position()=1]"/>';openDialog('detail1/main.html','dialogWidth:800px;dialogHeight:650px')
      			  			  </xsl:attribute>
		                  <xsl:value-of select="."/>
        					  </a>
         					</xsl:if>
         					<xsl:if test="$islast='no'">
         					  <xsl:value-of select="."/>
        					</xsl:if>
		            </td>
							</xsl:when>
							<xsl:when test="position()=2 or position()=3">
		          </xsl:when>
		          <xsl:when test="position()=4 or position()=5 or position()=6">
		          	<td align='right'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
		          </xsl:when>
							<xsl:otherwise>
			          <td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>