<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	    <!--colgroup>		       
	      <col style = 'width:105'/>
	      <col style = 'width:105'/>
	      <col style = 'width:105'/>
	      <col style = 'width:105'/>
	      <col style = 'width:105'/>
	      <col style = 'width:105'/-->
	      <!--xsl:for-each select="/root/tbody/tr[position()=1]/td">
          <xsl:if test="position()>2">
            <col style = 'width:105'/>
          </xsl:if>
        </xsl:for-each-->
	    <!--/colgroup-->
	    <thead>
	    	<tr noWrap="true" class="mainHead">
	    		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	    		<td style="display:none"/>
	    		<td style="display:none"/>
	    		<td style="display:none"/>
	    		<td style="display:none"/>
	    		<td style="display:none"/>
	    	</tr>
	    	<tr noWrap="true" class="mainHead">
	    		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
	    		<td style="display:none"/>
	    		<td style="display:none"/>
	    		<td style="display:none"/>
	    		<td style="display:none"/>
	    		<td style="display:none"/>
	    	</tr>
	    	<tr noWrap="true" class="mainHead">
	    		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	    		<td style="display:none"/>
	    		<td style="display:none"/>
	    		<td style="display:none"/>
	    		<td style="display:none"/>
	    		<td style="display:none"/>
	    	</tr>
		    <tr noWrap='true' class='mainHead'>
		      <td>物资类别</td>
		      <td>期初库存</td>
		      <td>期末库存</td>
		      <td>本期消耗</td>
		      <td>周转次</td>
		      <td>周转天</td>
		    </tr>
		  </thead> 
    	<tbody>
	      <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1">
		        	   <td>
		        	     <xsl:value-of select="."/>
        			   </td>
						  </xsl:when>
							<xsl:when test="position()=2 or position()=3">
		          </xsl:when>
		          <xsl:when test="position()=4 or position()=5 or position()=6">
		          	<td align='right'  class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
		          </xsl:when>
							<xsl:otherwise>
			          <td align='right' class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>