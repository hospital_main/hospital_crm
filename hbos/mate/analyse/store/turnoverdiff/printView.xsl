<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
      <thead>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
  	  <tr noWrap='true' class='mainHead'>
  	  	<td noWrap="true" rowspan="3">物资类别</td>
  	  	<td noWrap="true" colspan="2">本期</td>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" colspan="6">与上期比较</td>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  			<td noWrap="true" colspan="6">与去年同期比较</td>
  			<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  			<td noWrap="true" colspan="6">与标准比较</td>
  			<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
      <tr noWrap="true" class="mainHead">  
        <td style="display:none"/>	
  			<td noWrap="true" rowspan="2">周转次</td>
  			<td noWrap="true" rowspan="2">周转天</td>
  			<td noWrap="true" colspan="3">周转次</td>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  			<td noWrap="true" colspan="3">周转天</td>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  			<td noWrap="true" colspan="3">周转次</td>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  			<td noWrap="true" colspan="3">周转天</td>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  			<td noWrap="true" colspan="3">周转次</td>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  			<td noWrap="true" colspan="3">周转天</td>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  		</tr>
  	  <tr noWrap="true" class="mainHead">  
  	    <td style="display:none"/>	
  	    <td style="display:none"/>	
  	    <td style="display:none"/>	
  			<td noWrap="true">周转次</td>
  			<td noWrap="true">差额(次)</td>
  			<td noWrap="true">百分比</td>
  			<td noWrap="true">周转天</td>
  			<td noWrap="true">差额(天)</td>
  			<td noWrap="true">百分比</td>
  			<td noWrap="true">周转次</td>
  			<td noWrap="true">差额(次)</td>
  			<td noWrap="true">百分比</td>
  			<td noWrap="true">周转天</td>
  			<td noWrap="true">差额(天)</td>
  			<td noWrap="true">百分比</td>
  			<td noWrap="true">周转次</td>
  			<td noWrap="true">差额(次)</td>
  			<td noWrap="true">百分比</td>
  			<td noWrap="true">周转天</td>
  			<td noWrap="true">差额(天)</td>
  			<td noWrap="true">百分比</td>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1">
		           	<td>
         			    <xsl:value-of select="."/>
        			  </td>
							</xsl:when>
							<xsl:when test="(position() mod 3)=0 and position()!=3">
		           	<td align='right' class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>%
	              </td>
							</xsl:when>
							<xsl:otherwise>
			          <td align='right' class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
   	</root>
  </xsl:template>
</xsl:stylesheet>



