<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="3">物资类别</th>
  	  	<th noWrap="true" colspan="2">本期</th>
  	  	<th noWrap="true" colspan="6">与上期比较</th>
  			<th noWrap="true" colspan="6">与去年同期比较</th>
  			<th noWrap="true" colspan="6">与标准比较</th>
  	  </tr>
      <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true" rowspan="2">周转次</th>
  			<th noWrap="true" rowspan="2">周转天</th>
  			<th noWrap="true" colspan="3">周转次</th>
  			<th noWrap="true" colspan="3">周转天</th>
  			<th noWrap="true" colspan="3">周转次</th>
  			<th noWrap="true" colspan="3">周转天</th>
  			<th noWrap="true" colspan="3">周转次</th>
  			<th noWrap="true" colspan="3">周转天</th>
  		</tr>
  	  <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">周转次</th>
  			<th noWrap="true">差额(次)</th>
  			<th noWrap="true">百分比</th>
  			<th noWrap="true">周转天</th>
  			<th noWrap="true">差额(天)</th>
  			<th noWrap="true">百分比</th>
  				<th noWrap="true">周转次</th>
  			<th noWrap="true">差额(次)</th>
  			<th noWrap="true">百分比</th>
  			<th noWrap="true">周转天</th>
  			<th noWrap="true">差额(天)</th>
  			<th noWrap="true">百分比</th>
  			<th noWrap="true">周转次</th>
  			<th noWrap="true">差额(次)</th>
  			<th noWrap="true">百分比</th>
  			<th noWrap="true">周转天</th>
  			<th noWrap="true">差额(天)</th>
  			<th noWrap="true">百分比</th>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1">
		           	<td>
         			    <xsl:value-of select="."/>
        			  </td>
							</xsl:when>
							<xsl:when test="position()=6 or position()=9 or position()=12 or position()=15 or position()=18 or position()=21">
		           	<td align="right">
         			    <xsl:value-of select="format-number(.,'#,##0.00')"/>%
        			  </td>
							</xsl:when>
							<xsl:otherwise>
			          <td align='right'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>