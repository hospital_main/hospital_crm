<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true">入库单号</th>
				<th nowrap="true">入库日期</th>
				<th nowrap="true">供应商</th>
				<th nowrap="true">发票号</th>
				<th nowrap="true">状态</th>
				<th nowrap="true">材料编码</th>
				<th nowrap="true">材料名称</th>
				<th nowrap="true">货位</th>
				<th nowrap="true">金额</th>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          <xsl:when test="position()=1">
	          </xsl:when>
	          <xsl:when test="position()=11">
	          </xsl:when>
	          <xsl:when test="position()=12">
	          </xsl:when>
	          	<xsl:when test="position()=2 and .!='合计'">
	          		<td>
	          			 <xsl:if test="21=../td[11] " >
					           <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../whr/query/mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;','dialogWidth:1150px;dialogHeight:750px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="21!=../td[11] " >
					           <a href="#">
										  <xsl:attribute name="onclick" >
										   javascript:openDialog('update.html?load=&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
	          		</td>
	          	</xsl:when>
	          	<xsl:when test="position()=2 and .='合计'">
	          		<td> 合计
										</td>
	          	</xsl:when>
	          	
	          	
	          	<xsl:when test="position()=7">
	          		<xsl:if test="../td[6]='入库确认'">
		          		<td><a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('outDetail.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:700px;dialogHeight:490px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
									</td>
								</xsl:if>
								<xsl:if test="../td[6]!='入库确认'">
									<td><xsl:value-of select="."/></td>
								</xsl:if>
	          	</xsl:when>
	          	
	            <xsl:when test="position()=10 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
			    		</xsl:when>
		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>