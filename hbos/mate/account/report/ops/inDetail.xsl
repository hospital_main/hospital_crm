<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true">出库单号</th>
				<th nowrap="true">出库日期</th>
				<th nowrap="true">材料名称</th>
				<th nowrap="true">供应商</th>
				<th nowrap="true">数量</th>
				<th nowrap="true">单价</th>
				<th nowrap="true">金额</th>
				<th nowrap="true">库管</th>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	         
	            <xsl:when test="position()=4">
		          
		    </xsl:when>
		     <xsl:when test="position()=10">
		          
		    </xsl:when>
	            <xsl:when test="position()=6 or position()=7 or position()=8 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
		    </xsl:when>
		    <xsl:otherwise>
			<td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>