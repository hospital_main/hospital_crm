<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
	  	<thead>
	  		<tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
	  		<tr class="mainHead">
				<td nowrap="true">物资编号</td>
				<td nowrap="true">物资名称</td>
				<td nowrap="true">领料科室</td>
				<td nowrap="true">总量</td>
				<td nowrap="true">金额</td>
	  		</tr>
	  	</thead>
	  	<tbody> 
	  	  <xsl:for-each select="/root/tbody/tr">
				<tr>
        	<xsl:variable name="pos" select="position()"/>
        	<xsl:variable name="item" select="td[1]"/>
        	<xsl:variable name="cnt" select="count(/root/tbody/tr[td[1]= $item]) "/>     	
				
          <xsl:for-each select="td[position()=1]">
              <xsl:choose>
              		<xsl:when test="position()=1">
		              	<xsl:if test="$pos = 1">
				            	<td>
				            			<xsl:attribute name="rowspan"><xsl:value-of select="$cnt"/></xsl:attribute>
					  	            <xsl:value-of select="."/>
				            	</td>
			            	</xsl:if>
			            	
		            		<xsl:if test="$pos &gt; 1">
		            			<xsl:if test=". != ../../tr[$pos -1]/td[1]">
					            	<td>
					            			<xsl:attribute name="rowspan"><xsl:value-of select="$cnt"/></xsl:attribute>
						  	            <xsl:value-of select="."/>
					            	</td>
		            			</xsl:if>
	            			
	            			<xsl:if test=". = ../../tr[$pos -1]/td[1]">
			            		<td style="display:none">
				  	            	<xsl:value-of select="."/>
			            		</td>
	            			</xsl:if>
	            		</xsl:if>
	            	</xsl:when>

              </xsl:choose>
          </xsl:for-each>
           <xsl:for-each select="td[position()=2]">
	            		<td>
				  	            	<xsl:value-of select="."/>
	            		</td>
          </xsl:for-each>
           <xsl:for-each select="td[position()=3]">
	            		<td>
				  	            	<xsl:value-of select="."/>
	            		</td>
          </xsl:for-each>
           <xsl:for-each select="td[position()=4]">
	            		<td>
					  	         <xsl:value-of select="format-number(.,'#,##0.00')"/>
	            		</td>
          </xsl:for-each>
           <xsl:for-each select="td[position()=5]">
	            		<td>
					  	         <xsl:value-of select="format-number(.,'#,##0.00')"/>
	            		</td>
          </xsl:for-each>
        </tr>
		    </xsl:for-each>
	   	</tbody>
	   	<tfoot>
				<tr>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="align:right;fontsize:coltitle">
						<xsl:value-of select="/root/annex/bottomTitle_head"/>
					</td>
					<td style="align:left;fontsize:coltitle">
						<xsl:value-of select="/root/annex/bottomTitle_value"/>
					</td>
				</tr>		
			</tfoot>
   	</root>
	</xsl:template>
</xsl:stylesheet>