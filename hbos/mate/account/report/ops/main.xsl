<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true">物资编号</th>
				<th nowrap="true">物资名称</th>
				<th nowrap="true">领料科室</th>
				<th nowrap="true">总量</th>
				<th nowrap="true">金额</th>
				<!--th nowrap="true">金额</th-->
  		</tr>
  	</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
        	<xsl:variable name="pos" select="position()"/>
        	<xsl:variable name="item" select="td[1]"/>
        	<xsl:variable name="cnt" select="count(/root/tbody/tr[td[1]= $item]) "/>     	
				
          <xsl:for-each select="td[position()=1]">
              <xsl:choose>
              		<xsl:when test="position()=1">
		              	<xsl:if test="$pos = 1">
				            	<td>
				            			<xsl:attribute name="rowspan"><xsl:value-of select="$cnt"/></xsl:attribute>
					  	            <xsl:value-of select="."/>
				            	</td>
			            	</xsl:if>
			            	
		            		<xsl:if test="$pos &gt; 1">
		            			<xsl:if test=". != ../../tr[$pos -1]/td[1]">
					            	<td>
					            			<xsl:attribute name="rowspan"><xsl:value-of select="$cnt"/></xsl:attribute>
						  	            <xsl:value-of select="."/>
					            	</td>
		            			</xsl:if>
	            			
	            			<xsl:if test=". = ../../tr[$pos -1]/td[1]">
			            		<td style="display:none">
				  	            	<xsl:value-of select="."/>
			            		</td>
	            			</xsl:if>
	            		</xsl:if>
	            	</xsl:when>	       
              </xsl:choose>
          </xsl:for-each>
           <xsl:for-each select="td[position()=2]">
       
	            		<td><a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('inDetail.html?load=&lt;inv_name&gt;<xsl:value-of select="../td[2]"/>&lt;/inv_name&gt;&lt;dept&gt;<xsl:value-of select="../td[3]"/>&lt;/dept&gt;','dialogWidth:700px;dialogHeight:350px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
									</td>
				
          </xsl:for-each>
           <xsl:for-each select="td[position()=3]">
	            		<td>
				  	            	<xsl:value-of select="."/>
	            		</td>
          </xsl:for-each>
           <xsl:for-each select="td[position()=4]">
	            		<td>
					  	         <xsl:value-of select="format-number(.,'#,##0.00')"/>
	            		</td>
          </xsl:for-each>
           <xsl:for-each select="td[position()=5]">
	            		<td>
					  	         <xsl:value-of select="format-number(.,'#,##0.00')"/>
	            		</td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>	
	</xsl:template>
</xsl:stylesheet>