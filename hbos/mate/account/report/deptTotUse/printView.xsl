<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td)"/>
  <xsl:variable name="empname" select="/root/bottom/tr[1]/td[3]"/>
  	<root>
    	<thead>
	  		<tr noWrap="true" >
	      			<td noWrap="true" >
	      				<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
	      			</td>
	      		<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
	      			<td noWrap="true" style="display:none"/>
						</xsl:for-each>
	     </tr>
	     <tr noWrap="true" >
						<td noWrap="true" style="fontsize:subtitle;colspan:colcount">
	      				<xsl:attribute name="style">fontsize:subtitle;colspan:<xsl:value-of select="$colNums"/></xsl:attribute>
	      			</td>
	      		<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;1]">
	      			<td noWrap="true" style="display:none"/>
						</xsl:for-each>

	     </tr>
	     <tr noWrap="true">
	     
					<td noWrap="true">
						<xsl:attribute name="style">fontsize:subtitle;colspan:<xsl:value-of select="ceiling($colNums div 2)"/>;align:left;</xsl:attribute>
					</td>	
						<xsl:for-each select ="/root/tbody/tr[1]/td[position()&lt; ceiling($colNums div 2) ]">
							<td noWrap="true" style="display:none"/>
						</xsl:for-each>
					
					<td noWrap="true">
						<xsl:attribute name="style">fontsize:subtitle;colspan:<xsl:value-of select="$colNums - ceiling($colNums div 2)"/>;align:right;</xsl:attribute>
						<xsl:value-of select="/root/annex/rightTitle"/>
					</td>
					<xsl:for-each select ="/root/tbody/tr[1]/td[position()&lt; ($colNums - ceiling($colNums div 2) ) -1 ]">
						<td noWrap="true" style="display:none"/>
					</xsl:for-each>
	     </tr>
	     <xsl:for-each select="/root/tbody/tr[position()=1]">
  			<tr noWrap='true' class='mainHead'>
          <xsl:for-each select="td">
	          		<td nowrap='true'><xsl:value-of select="."/></td>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
    	</thead>
    	
    	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	      <xsl:variable name="colCnt" select="position()"/>
	      <xsl:variable name="preName" select="../tr[$colCnt]/td[1]"/>
	      
	      <xsl:if test="position() != 1">
		      <xsl:if test="td[1] != ../tr[$colCnt]/td[1]">
		      <tr>
				     <xsl:for-each select="td[position() &gt; 1]">
				     <xsl:variable name="colPos" select="position()"/>
			    	    <xsl:choose>
			    	      <xsl:when test=" position() = 2">
			    	      	<td noWrap="true">
		                  С��
		                </td>
			          	</xsl:when>
			    	      <xsl:when test=" position() = 1">
			    	      	<td>
			          		</td>
			          		<td>
			          		</td>
			          	</xsl:when>
			    	      <xsl:when test="position() &gt; 2">
			    	      	<td noWrap="true" align="right">
			    	      		<xsl:value-of select="format-number(sum(/root/tbody/tr[td[1] = $preName]/td[$colPos + 1]),'#,##0.00')"/>
		                </td>
			          	</xsl:when>
			    	    </xsl:choose>
			    	  </xsl:for-each>
		    	  </tr>
		      </xsl:if>
	      </xsl:if>
	      
	    	<tr>
	    		<xsl:variable name="colName" select="td[1]"/>
	    		<xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[1] = $colName])"/>
	    		<xsl:if test="td[1] != ../tr[$colCnt]/td[1]">
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt + 1"/></xsl:attribute>
	    				<xsl:value-of select="td[1]"/>
	    			</td>
	    		</xsl:if>
	    		<xsl:if test="td[1] = ../tr[$colCnt]/td[1]">
	    			<td style="display:none"/>
	    		</xsl:if>
	    	  <xsl:for-each select="td[position() &gt; 1]">
	    	    <xsl:choose>
	    	      <xsl:when test=" position() = 2">
	    	      	<td>
	          		<xsl:value-of select="."/>
	          		</td>
	          	</xsl:when>
	          	<xsl:when test=" position() &gt; 2">
	    	      	<td noWrap="true" align="right">
	    	      		<xsl:if test=". !='0.00'">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </xsl:if>
                </td>
	          	</xsl:when>
	          	
	          	<xsl:otherwise>
	          		<td><xsl:value-of select="."/></td>
              </xsl:otherwise>
	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
 			<tfoot>
				<tr noWrap='true'>
        <td align="left" style='colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNums"/>    
        </xsl:call-template>
  		</tr>												
		</tfoot>
 	
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
		
</xsl:stylesheet>