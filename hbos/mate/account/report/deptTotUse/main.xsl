<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:for-each select="/root/tbody/tr[position()=1]">
  			<tr noWrap='true' class='mainHead'>
          <xsl:for-each select="td">
	          		<th nowrap='true'><xsl:value-of select="."/></th>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</thead>
  	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	      <xsl:variable name="colCnt" select="position()"/>
	      <xsl:variable name="preName" select="../tr[$colCnt]/td[1]"/>
	      
	      <xsl:if test="position() != 1">
		      <xsl:if test="td[1] != ../tr[$colCnt]/td[1]">
		      <tr>
				     <xsl:for-each select="td[position() &gt; 1]">
				     <xsl:variable name="colPos" select="position()"/>
			    	    <xsl:choose>
			    	      <xsl:when test=" position() = 2">
			    	      	<td noWrap="true">
		                  С��
		                </td>
			          	</xsl:when>
			    	      <xsl:when test=" position() = 1">
			    	      	<td>
			          		</td>
			          	</xsl:when>
			    	      <xsl:when test="position() &gt; 2">
			    	      	<td noWrap="true" align="right">
			    	      		<xsl:value-of select="format-number(sum(/root/tbody/tr[td[1] = $preName]/td[$colPos + 1]),'#,##0.00')"/>
		                </td>
			          	</xsl:when>
			    	    </xsl:choose>
			    	  </xsl:for-each>
		    	  </tr>
		      </xsl:if>
	      </xsl:if>
	      
	    	<tr>
	    		<xsl:variable name="colName" select="td[1]"/>
	    		<xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[1] = $colName])"/>
	    		<xsl:if test="td[1] != ../tr[$colCnt]/td[1]">
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt + 1"/></xsl:attribute>
	    				<xsl:value-of select="td[1]"/>
	    			</td>
	    		</xsl:if>
	    	  <xsl:for-each select="td[position() &gt; 1]">
	    	    <xsl:choose>
	    	      <xsl:when test=" position() = 1">
	    	      	<td noWrap="true">
                  <xsl:value-of select="."/>
                </td>
	          	</xsl:when>
	    	      <xsl:when test=" position() = 2">
	    	      	<td>
	          		<xsl:value-of select="."/>
	          		</td>
	          	</xsl:when>
	    	      <xsl:when test="position() &gt; 2">
	    	      	<td noWrap="true" align="right">
	    	      		<xsl:if test=". !='0.00'">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </xsl:if>
                </td>
	          	</xsl:when>
	          	<xsl:otherwise>
	          		<td><xsl:value-of select="."/></td>
              </xsl:otherwise>
	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>