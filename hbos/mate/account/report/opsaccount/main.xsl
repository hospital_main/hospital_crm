<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true">材料编码</th>
				<th nowrap="true">材料名称</th>
				<th nowrap="true">规格型号</th>
				<th nowrap="true">领料科室</th>
				<th nowrap="true">数量</th>
				<th nowrap="true">金额</th>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=5 or position()=6">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
			    		</xsl:when>
			    		
			    		 <xsl:when test="position()=2">
		           	<td><a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('inDetail.html?load=&lt;id&gt;<xsl:value-of select="../td[2]"/>&lt;/id&gt;&lt;dept&gt;<xsl:value-of select="../td[4]"/>&lt;/dept&gt;','dialogWidth:700px;dialogHeight:350px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
									</td>
			    		</xsl:when>
			    		 <xsl:when test="position()=7">
		            <td align='right'  style="display:none;">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>
	            			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>