<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=" "/>
  <xsl:template match="/">
  <root>
  	<thead>
  		<xsl:for-each select="/root/tbody/tr[position()=1]">
  			<tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:maintitle;colspan:3"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			   <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:center"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
			  	<td style="display:none"/>
			  	<td noWrap="true" style="fontsize:subtitle;colspan:1;align:right">
			  		<xsl:value-of select="/root/annex/rightTitle"/>
			  	</td>
			  	
			  </tr>
			   <tr noWrap="true" class="mainHead">
			  	<td >仓库名称</td>
			  	<td >类别名称</td>
			  	<td>合计</td>
			  </tr>
			  </xsl:for-each>
  	</thead>
  	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	      <xsl:variable name="colName" select="td[position()=5]"/>
	      <xsl:variable name="colPos"  select="position()"/> 
	    	<tr>
	      	<xsl:variable name="newCode" select="td[1]"/>	    		
		      <xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[1] = $newCode and td[5] = $colName])"/>
		      
					<xsl:if test="$colPos = 0 ">
						<td>
							<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
										<xsl:if test="td[5]='14'">
											<xsl:attribute name="style">color:red</xsl:attribute>
										</xsl:if>
										<xsl:if test="td[5]='13'">
											<xsl:attribute name="style">color:green</xsl:attribute>
										</xsl:if>
							<xsl:value-of select="td[2]"/>
						</td>
					</xsl:if>
							      
					<xsl:if test="$colPos != 0 and $newCode != ../tr[$colPos]/td[1]">
						<td>
							<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
										<xsl:if test="td[5]='14'">
											<xsl:attribute name="style">color:red</xsl:attribute>
										</xsl:if>
										<xsl:if test="td[5]='13'">
											<xsl:attribute name="style">color:green</xsl:attribute>
										</xsl:if>
							<xsl:value-of select="td[2]"/>
						</td>
					</xsl:if>		      

					<xsl:if test="$colPos != 0 and $newCode = ../tr[$colPos]/td[1]">
						<td style="display:none">
							
						</td>
					</xsl:if>		      
		      
	    	  <xsl:for-each select="td">
	    	    <xsl:choose>
	    	      	<xsl:when test=" position() = 5 or position() = 1 or position() = 2"/>
								<xsl:when test=" position() &lt; 4">
									<td>
										<xsl:if test="../td[position()=5]='14'">
										<xsl:attribute name="style">color:red</xsl:attribute>
												<xsl:value-of select="."/>
										</xsl:if>
										<xsl:if test="../td[position()=5]='13'">
											<xsl:attribute name="style">color:green</xsl:attribute>
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
								</xsl:when>
									
									<xsl:when test=" position() = 4">
									<td align="right">
										<xsl:if test="../td[position()=5]='14'">
										<xsl:attribute name="style">color:red</xsl:attribute>
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</xsl:if>
										<xsl:if test="../td[position()=5]='13'">
											<xsl:attribute name="style">color:green</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</xsl:if>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:if test="../td[position()=5]='14'">
											<xsl:if test="td[position()=4]='合计'">
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
											</xsl:if>
										</xsl:if>
										<xsl:if test="../td[position()=5]='13'">
											<xsl:attribute name="style">color:green</xsl:attribute>
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
								</xsl:otherwise>

	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
   		<xsl:variable name="trcount" select="count(/root/tbody/tr)"/>
   		

 		</tbody>
 		<tfoot>
			<tr>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="align:right;fontsize:coltitle">
					<xsl:value-of select="/root/annex/bottomTitle_head"/>
				</td>
				<td style="align:left;fontsize:coltitle">
					<xsl:value-of select="/root/annex/bottomTitle_value"/>
				</td>
			</tr>		
		</tfoot>
 		</root>
	</xsl:template>
</xsl:stylesheet>