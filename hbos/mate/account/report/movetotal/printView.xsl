<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
	  	<thead>
	  		<tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
	  		<tr class="mainHead">
		      <td nowrap="true">�Ƴ��ֿ�</td>
					<td nowrap="true">����ֿ�</td>
					<td nowrap="true">���</td>
	  		</tr>
	  	</thead>
	  	<tbody>
	  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
	    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/> 
	  	  <xsl:for-each select="/root/tbody/tr">
		      <tr>
		      	<xsl:variable name="unit_name" select="td[1]" />
		      	<xsl:variable name="cur_pos" select="position()" />
		      	<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$unit_name])" />
		        <xsl:for-each select="td">
		          <xsl:choose>
		           	<xsl:when test="position()=1 ">
									<xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos - 1]/td[1]">
										<td rowspan="{$rowspan}">
											<xsl:value-of select="." />
										</td>
									</xsl:if>
									<xsl:if test="$unit_name = ../../tr[$cur_pos - 1]/td[1]">
										<td style="display:none"><xsl:value-of select="."/>
										</td>
									</xsl:if>
								</xsl:when>
		            <xsl:when test="position()=3 ">
			            <td align='right'>
			              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
			            </td>
				    		</xsl:when>
				    		
				    		<xsl:otherwise>
				          <td align='left'>
		                <xsl:value-of select="."/>
		              </td>
		            </xsl:otherwise>			                        
		          </xsl:choose>
		        </xsl:for-each>
		      </tr>
		    </xsl:for-each>
	   	</tbody>
   	</root>
	</xsl:template>
</xsl:stylesheet>