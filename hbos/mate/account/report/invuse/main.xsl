<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th>物资用途</th>
	  		<th>物资类别</th>
	  		<th>出库金额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:variable name="paravalue" select="/root/tbody/tr[1]/td[ last()]"/>
	    	<xsl:choose>
	    		<xsl:when test="position()>0">
	    			<tr>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			            <xsl:when test="position()=3">
			            	<td align="right"><xsl:value-of select="format-number(.,$paravalue )"/></td>
			            </xsl:when>
			            <xsl:when test="position()=last()">
			            	
			            </xsl:when>
			          	<xsl:otherwise>
			          		<td align="left"><xsl:value-of select="."/></td>
			          	</xsl:otherwise>
		          	</xsl:choose>
		          </xsl:for-each>      
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>