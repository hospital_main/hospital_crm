<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<tr noWrap="true" class="mainHead">
  			<th nowrap="true">序号</th>
				<th nowrap="true">供应商编码</th>
				<th nowrap="true">供应商名称</th>
				<th nowrap="true">应付款余额</th>
				<th nowrap="true">本月增加金额</th>
				<th nowrap="true">本月减少金额</th>
				<th nowrap="true">本月结存金额</th>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	      	<td align='left'>
            <xsl:value-of select="position()"/>
          </td>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
	    <tr>
	    	<td></td>
				<td>合计：</td>
				<td></td>
				<td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),$VHMONEYFORMAT)"/></td>
				<td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[4]),$VHMONEYFORMAT)"/></td>
				<td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),$VHMONEYFORMAT)"/></td>
				<td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),$VHMONEYFORMAT)"/> </td>
			</tr>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>