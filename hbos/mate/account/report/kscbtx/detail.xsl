<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  	    <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<xsl:for-each select="/root/tbody/tr">
  			<xsl:choose>
	    		<xsl:when test=" position() &lt; 3 ">
	    			<xsl:variable name="rows" select="position()"/>
	    			<tr noWrap='true' class='mainHead'>
	    		<xsl:if test=" $rows =1 ">
	          		<th rowspan="2" valign="middle" width='60'>类别编码</th>
	          		<th rowspan="2" valign="middle" width='120'>类别名称</th>
          		</xsl:if>
          		<xsl:if test=" $rows =2 ">
	          		<th style="display:none" >类别编码</th>
	          		<th style="display:none" >类别名称</th>
          		</xsl:if>
		          <xsl:for-each select="td">
  							<xsl:choose>
  							<xsl:when test=" position() > 3 ">
				          	<xsl:variable name="cols" select="position()"/>
				          	<xsl:variable name="cols_name" select="."/>
				          	<xsl:variable name="colNums" select="count(/root/tbody/tr[ $rows ]/td[.= $cols_name]) "/>
			          		<xsl:if test=" $rows=1">
				          		<xsl:if test=" ../td[$cols]!= ../td[$cols -1 ]">
				          			<th valign="middle" >
				          				<xsl:attribute name="colspan" >
				          					<xsl:value-of select="$colNums"/>
						            	</xsl:attribute>
						            	<xsl:if test=" ../td[$cols]= ../../tr[2]/td[$cols ]">
							            	<xsl:attribute name="rowspan" >
					          					<xsl:value-of select="2"/>
							            	</xsl:attribute>
						            	</xsl:if>
						            	<xsl:value-of select="."/>
				          			</th>
					          	</xsl:if>
					          	<xsl:if test=" ../td[$cols] = ../td[$cols -1 ]">
				          			<th style="display:none" >
						            	<xsl:value-of select="."/>
				          			</th>
					          	</xsl:if>
					          </xsl:if>
					          <xsl:if test=" $rows=2">
			          			<th valign="middle" >
					            	<xsl:value-of select="."/>
			          			</th>
					          </xsl:if>
					        </xsl:when>
					      </xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr[position() > 2]">
	    	<tr>
	    	  <xsl:for-each select="td">
	    	    <xsl:choose>
	    	    <xsl:when test=" position() = 1">
	    	      	<td>
					  <xsl:value-of select="."/>
	          		</td>
	          	</xsl:when>
	    	    <xsl:when test=" position() = 2">
	    	      	<td>
	          		<xsl:value-of select="."/>
	          		</td>
	          	</xsl:when>
	    	    <xsl:when test="position() = 3">
	    	      	<td style="display:none">是否末级</td>
	    	    </xsl:when>
	    	    <xsl:when test="position() &gt; 3">
	    	      	<td align="right" noWrap="true">
                   <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                    </td>
	          	</xsl:when>
	          	<xsl:otherwise>
	          		<td><xsl:value-of select="."/></td>
              </xsl:otherwise>
	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>