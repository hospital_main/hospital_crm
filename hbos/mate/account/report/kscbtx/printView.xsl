<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td)-1"/>
  <xsl:variable name="empname" select="/root/bottom/tr[1]/td[3]"/>
  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	<root>
    	<thead>
	  		<tr noWrap="true" >
	      			<td noWrap="true" >
	      				<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="count(/root/tbody/tr[1]/td[position()!=last()])"/></xsl:attribute>
	      			</td>
	      		<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;2]">
	      			<td noWrap="true" style="display:none"/>
						</xsl:for-each>
	     </tr>
	     <tr noWrap="true" >
						<td noWrap="true" style="fontsize:subtitle;colspan:colcount">
	      				<xsl:attribute name="style">fontsize:subtitle;colspan:<xsl:value-of select="count(/root/tbody/tr[1]/td[position()!=last()])"/></xsl:attribute>
	      			</td>
	      		<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;2]">
	      			<td noWrap="true" style="display:none"/>
						</xsl:for-each>

	     </tr>
	     <tr noWrap="true">
	     
					<td noWrap="true">
						<xsl:attribute name="style">fontsize:subtitle;colspan:<xsl:value-of select="ceiling($colNums div 2)"/>;align:left;</xsl:attribute>
					</td>	
						<xsl:for-each select ="/root/tbody/tr[1]/td[position()&lt; ceiling($colNums div 2) ]">
							<td noWrap="true" style="display:none"/>
						</xsl:for-each>
					
					<td noWrap="true">
						<xsl:attribute name="style">fontsize:subtitle;colspan:<xsl:value-of select="$colNums - ceiling($colNums div 2)"/>;align:right;</xsl:attribute>
						<xsl:value-of select="/root/annex/rightTitle"/>
					</td>
					<xsl:for-each select ="/root/tbody/tr[1]/td[position()&lt; ($colNums - ceiling($colNums div 2) ) -1 ]">
						<td noWrap="true" style="display:none"/>
					</xsl:for-each>
	     </tr>
           <xsl:for-each select="/root/tbody/tr">
	  			<xsl:choose>
		    		<xsl:when test=" position() &lt; 3 ">
		    			<xsl:variable name="rows" select="position()"/>
		    			<tr noWrap='true' class='mainHead'>
		    		<xsl:if test=" $rows =1 ">
		          		<td rowspan="2" valign="middle" width='60'>���ұ���</td>
		          		<td rowspan="2" valign="middle" width='120'>��������</td>
	          		</xsl:if>
	          		<xsl:if test=" $rows =2 ">
		          		<td style="display:none" >���ұ���</td>
		          		<td style="display:none" >��������</td>
	          		</xsl:if>
	    	        
	    	        <xsl:for-each select="td">
	    	        <xsl:choose>
	  				<xsl:when test=" position() > 3 "> 
					          	<xsl:variable name="cols" select="position()"/>
					          	<xsl:variable name="cols_name" select="."/>
					          	<xsl:variable name="colNums1" select="count(/root/tbody/tr[ $rows ]/td[.= $cols_name]) "/>
				          		<xsl:if test=" $rows=1">
					          		<xsl:if test=" ../td[$cols]!= ../td[$cols -1 ]">
					          			<td valign="middle" >
					          				<xsl:attribute name="colspan" >
					          					<xsl:value-of select="$colNums1"/>
							            	</xsl:attribute>
							            	<xsl:if test=" ../td[$cols]= ../../tr[2]/td[$cols ]">
								            	<xsl:attribute name="rowspan" >
						          					<xsl:value-of select="2"/>
								            	</xsl:attribute>
							            	</xsl:if>
							            	<xsl:value-of select="."/>
					          			</td>
						          	</xsl:if>
						          	<xsl:if test=" ../td[$cols] = ../td[$cols -1 ]">
					          			<td style="display:none" >
							            	<xsl:value-of select="."/>
					          			</td>
						          	</xsl:if>
						          </xsl:if>
						          <xsl:if test=" $rows=2">
				          			<td valign="middle" >
						            	<xsl:value-of select="."/>
				          			</td>
						          </xsl:if>
	          		</xsl:when>
                    </xsl:choose>
	                </xsl:for-each>
	        </tr>
	        </xsl:when>
	        </xsl:choose>
	    </xsl:for-each> 
	     
    	</thead>
    	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr[position() > 2]">
	      <xsl:variable name="colName" select="position()"/>
	    	<tr>
	    	  <xsl:for-each select="td">
	    	    <xsl:choose>
	    	      <xsl:when test=" position() = 3">
	          	</xsl:when>
	    	      <xsl:when test=" position() = 2">
	    	      	<td>
	          		<xsl:value-of select="."/>
	          		</td>
	          	</xsl:when>
	    	      <xsl:when test="position() &gt; 3">
	    	      	<td noWrap="true">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	          	</xsl:when>
	          	<xsl:otherwise>
	          		<td><xsl:value-of select="."/></td>
              </xsl:otherwise>
	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
 		<tfoot>
 			<tr noWrap='true'>
        <td align="left" style='colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNums"/>    
        </xsl:call-template>
  		</tr>
 		</tfoot>
 	
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
		
</xsl:stylesheet>