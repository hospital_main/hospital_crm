<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true">材料编码</th>
				<th nowrap="true">材料名称</th>
				<th nowrap="true">规格</th>
				<th nowrap="true">计量单位</th>
				<th nowrap="true">数量</th>
				<th nowrap="true">单价</th>
				<th nowrap="true">金额</th>
				<th nowrap="true">类别</th>
				 
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	
	            <xsl:when test="position()=1 ">
		           <td><a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('inDetail.html?load=&lt;id&gt;<xsl:value-of select="../td[9]"/>&lt;/id&gt;','dialogWidth:700px;dialogHeight:350px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
									</td>
			    		</xsl:when>
	           
	            <xsl:when test="position()=5 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position()=6 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position()=7 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position()=9 ">
		            <td align='right' style="display:none;">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
	    <tr>
				<td>合计：</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td align='right' ><xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/></td>
				<td></td>
				 
			</tr>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>