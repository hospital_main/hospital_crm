<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
	  	<thead>
	  		<tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	 
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	 
			  </tr>
	  		<tr class="mainHead">
	      <td nowrap="true">材料编码</td>
				<td nowrap="true">材料名称</td>
				<td nowrap="true">规格</td>
				<td nowrap="true">计量单位</td>
				<td nowrap="true">数量</td>
				<td nowrap="true">单价</td>
				<td nowrap="true">金额</td>
				<td nowrap="true">类别</td>
				 
	  		</tr>
	  	</thead>
	  	<tbody> 
	  	  <xsl:for-each select="/root/tbody/tr">
		      <tr>
		        <xsl:for-each select="td">
		          <xsl:choose>
		          
		          <xsl:when test="position()=5 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0.')"/>
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position()=6 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position()=7 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
			    		</xsl:when>
				    		<xsl:otherwise>
				          <td align='left'>
		                <xsl:value-of select="."/>
		              </td>
		            </xsl:otherwise>			                        
		          </xsl:choose>
		        </xsl:for-each>
		      </tr>
		    </xsl:for-each>
		    <tr>
				<td>合计：</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td align='right' ><xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/></td>
				<td></td> 
			</tr>
	   	</tbody>
	   	<tfoot>
				<tr>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="align:right;fontsize:coltitle">
						<xsl:value-of select="/root/annex/bottomTitle_head"/>
					</td>
					<td style="align:left;fontsize:coltitle">
						<xsl:value-of select="/root/annex/bottomTitle_value"/>
					</td>
				</tr>		
			</tfoot>
   	</root>
	</xsl:template>
</xsl:stylesheet>