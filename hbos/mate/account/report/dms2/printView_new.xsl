<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<root>
    	<thead>
	  		<xsl:for-each select="/root/tbody/tr[position()=1]">
	  			<tr noWrap='true' class='mainHead'>
	          <xsl:for-each select="td">
	          	<xsl:choose>
		          	<xsl:when test=" position() != 3 ">
		          		<td nowrap='true'><xsl:value-of select="."/></td>
		          	</xsl:when>
		          	<xsl:otherwise>
	              </xsl:otherwise>
	            </xsl:choose>
	          </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
    	</thead>
    	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	    	<tr>
	    	  <xsl:for-each select="td">
	    	    <xsl:choose>
	    	      <xsl:when test=" position() = 3">
	          	</xsl:when>
	    	      <xsl:when test=" position() = 2">
	    	      	<td>
	    	      	<xsl:if test=" ../td[3] = 0 ">
	          		</xsl:if>
	          		<xsl:value-of select="."/>
	          		</td>
	          	</xsl:when>
	    	      <xsl:when test="position() &gt; 3">
	    	      	<td align="right" noWrap="true">
                  <xsl:value-of select="."/>
                </td>
	          	</xsl:when>
	          	<xsl:otherwise>
	          		<td><xsl:value-of select="."/></td>
              </xsl:otherwise>
	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>	
</xsl:stylesheet>