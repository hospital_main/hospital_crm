<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
	  		<th>科室名称</th>
	  		<th>物资类别</th>
	  		<th>金额</th>
	  	</tr>
  	</thead>
  	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr">
	      <xsl:variable name="dept_code" select="td[1]" />
				<xsl:variable name="cur_pos" select="position()" />					
				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$dept_code])" />
	    	<tr>
	    	  <xsl:for-each select="td">
	    	  	
	    	    <xsl:choose>
	    	      <xsl:when test="position()=1 or position()=3">
	    	      </xsl:when>
	    	      <xsl:when test="position()=2">
	    	      	<xsl:if test="$cur_pos = 1 or $dept_code != ../../tr[$cur_pos - 1]/td[1]">
	    	      		<td valign='middle' align='center'>
	    	      			<xsl:attribute name="rowspan"><xsl:value-of select="$rowspan"/></xsl:attribute>
	    	      			<xsl:value-of select="."/>
	    	      		</td>
	    	      	</xsl:if>
	    	      </xsl:when>
	    	      <xsl:when test="position()=5">
	    	      	<td align='right'>
	    	      		<xsl:value-of select="format-number(.,'###0.00####')"/>
	    	      	</td>
	    	      </xsl:when>
	    	      <xsl:otherwise>
	    	      	<td>
	    	      			<xsl:value-of select="."/>
	    	      	</td>
	    	      </xsl:otherwise>
	    	       
	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>