<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:for-each select="/root/tbody/tr[position()=1]">
  			<tr noWrap='true' class='mainHead'>
          <xsl:for-each select="td">
          	<xsl:choose>
	          	<xsl:when test=" position() != 3 ">
	          		<th nowrap='true'><xsl:value-of select="."/></th>
	          	</xsl:when>
	          	<xsl:otherwise>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</thead>
  	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	      <xsl:variable name="colName" select="position()"/>
	    	<tr>
	    	  <xsl:for-each select="td">
	    	    <xsl:choose>
	    	      <xsl:when test=" position() = 3">
	          	</xsl:when>
	    	      <xsl:when test=" position() = 2">
	    	      	<td>
	    	      	<xsl:if test=" ../td[3] = 0 ">
	    	      		<img src='/images2/menu/folderopen.png' _exp='-' onclick='expand(this)'/>
	          		</xsl:if>
	          		<xsl:value-of select="."/>
	          		</td>
	          	</xsl:when>
	    	      <xsl:when test="position() &gt; 3">
	    	      	<td noWrap="true">
                  <xsl:value-of select="."/>
                </td>
	          	</xsl:when>
	          	<xsl:otherwise>
	          		<td><xsl:value-of select="."/></td>
              </xsl:otherwise>
	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>