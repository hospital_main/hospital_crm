<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
	  	<thead>
	  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  			<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	  		<tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:7;align:left"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td noWrap="true" style="fontsize:subtitle;colspan:4;align:left"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
	  		<tr noWrap="true" class="mainHead">
	  			<td nowrap="true">序号</td>
					<td nowrap="true">供应商编码</td>
					<td nowrap="true">供应商名称</td>
					<td nowrap="true">应付款余额</td>
					<td nowrap="true">本月增加金额</td>
					<td nowrap="true">本月减少金额</td>
					<td nowrap="true">本月结存金额</td>
					<td nowrap="true">批准付款金额</td>
					<td nowrap="true">实际付款金额</td>
					<td nowrap="true">汇费</td>
					<td nowrap="true">备注</td>
	  		</tr>
	  	</thead>
	  	<tbody> 
	  	  <xsl:for-each select="/root/tbody/tr">
		      <tr>
		      	<td align='left'>
	            <xsl:value-of select="position()"/>
	          </td>
		        <xsl:for-each select="td">
		          <xsl:choose>
		          	<xsl:when test="position()=1">
									<td nowrap="true">
										<xsl:attribute name="value" >
											<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
										</xsl:attribute>
										<xsl:value-of select="."/>
									</td>								
								</xsl:when>
		            <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6">
			            <td align='right'>
			              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
			            </td>
				    		</xsl:when>				
								<xsl:when test="position()=7 or position()=8 or position()=9 ">													
									<td align='right'><xsl:attribute name="value" >&lt;demo2&gt;<xsl:value-of select="."/>&lt;/demo2&gt;</xsl:attribute><xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/></td>
								</xsl:when>
								<xsl:when test="position()=10">
									<td><xsl:attribute name="value" >&lt;demo2&gt;<xsl:value-of select="."/>&lt;/demo2&gt;</xsl:attribute><xsl:value-of select="."/></td>
								</xsl:when>		
				    		<xsl:otherwise>
				          <td align='left'>
		                <xsl:value-of select="."/>
		              </td>
		            </xsl:otherwise>			                        
		          </xsl:choose>
		        </xsl:for-each>
		      </tr>
		    </xsl:for-each>
		    <tr>
		    	<td></td>
					<td>合计：</td>
					<td></td>
					<td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),$VHMONEYFORMAT)"/></td>
					<td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[4]),$VHMONEYFORMAT)"/></td>
					<td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),$VHMONEYFORMAT)"/></td>
					<td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),$VHMONEYFORMAT)"/> </td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
	   	</tbody>
	   	<tfoot>
				<tr>
					<td style="colspan:3">院长</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="colspan:3">主任</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="colspan:5">会计</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>		
			</tfoot>
   	</root>
	</xsl:template>
</xsl:stylesheet>