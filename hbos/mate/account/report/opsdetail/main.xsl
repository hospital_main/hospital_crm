<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true">出库单号</th>
				<th nowrap="true">出库日期</th>
				<th nowrap="true">科室</th>
				<th nowrap="true">状态</th>
				<th nowrap="true">材料编码</th>
				<th nowrap="true">材料名称</th>
				<th nowrap="true">货位</th>
				<th nowrap="true">规格</th>
				<th nowrap="true">数量</th>
				<th nowrap="true">单价</th>
				<th nowrap="true">金额</th>
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          <xsl:when test="position()=1">
	          </xsl:when>
	           <xsl:when test="position()=13">
	          </xsl:when>
	          
	          
	          	<xsl:when test="position()=2 and .!='合计'">
	          		<td><a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('update.html?load=&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a></td>
	          	</xsl:when>
	          	<xsl:when test="position()=2 and .='合计'">
	          		<td> 合计
									</td>
	          	</xsl:when>
	         
	          	<xsl:when test="position()=6">
	          		<xsl:if test="../td[5]='出库确认'">
		          		<td><a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('inDetail.html?load=&lt;id&gt;<xsl:value-of select="../td[13]"/>&lt;/id&gt;','dialogWidth:700px;dialogHeight:490px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
									</td>
								</xsl:if>
								<xsl:if test="../td[5]!='出库确认'">
									<td><xsl:value-of select="."/></td>
								</xsl:if>
	          	</xsl:when>

	            <xsl:when test="position()=11 or position()=12  ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
			    		</xsl:when>
		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>