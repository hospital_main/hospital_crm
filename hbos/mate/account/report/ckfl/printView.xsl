<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td) - 6"/>
  	<root>
		  <!--colgroup>
				<xsl:for-each select="/root/tbody/tr[position()=1]">
					<xsl:for-each select="td">
						<xsl:choose>
			        <xsl:when test="position()=1 or position()=2 or position()=4 or position()=5 or position()=last() ">
							</xsl:when>
							<xsl:when test="position()=3">
								<col style = 'width:200'/>
							</xsl:when>
							<xsl:otherwise>
                <col/>
              </xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</xsl:for-each>
			</colgroup-->
    	<thead>
    		<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNums"/>    
	        </xsl:call-template>
	  		</tr>
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="/root/tbody/tr[position()=1]">
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1 or position()=4 or position()=5 or position()=6 or position()=last() ">
								</xsl:when>
								<xsl:otherwise>
	                            <td><xsl:value-of select="."/></td>
	                            </xsl:otherwise>
                          </xsl:choose>
						</xsl:for-each>
					</xsl:for-each>
				</tr>
			</thead>
    	<tbody>
    		<xsl:variable name="paravalue" select="/root/tbody/tr[1]/td[ last()]"/>
  	    <xsl:for-each select="/root/tbody/tr[position()>1]">
          <tr>
            <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position()=1 or position()=4 or position()=5 or position()=6 or position()= last()">
								</xsl:when>
								<xsl:when test="position()=2">
									<td><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:when test="position()=3">
									<td><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:otherwise>
	                          <td style="border:thin;align:right">
	                	          <xsl:value-of select="format-number(.,$paravalue )"/>
	              	          </td>
	              </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
    			</tr>
     		</xsl:for-each>  	
    	</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>