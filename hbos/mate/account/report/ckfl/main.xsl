<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=1">
	    			<tr noWrap='true' class='mainHead'>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=1 or position()=4 or position()=5 or position()=6 or position()=last() ">
			          		<th style="display:none"><xsl:value-of select="."/></th>
			          	</xsl:when>
			          	<xsl:when test="position()=2">
			            	<th nowrap='true' width='100'><xsl:value-of select="."/></th>
			            </xsl:when>
			          	<xsl:when test="position()=3">
			            	<th nowrap='true' width='200'><xsl:value-of select="."/></th>
			            </xsl:when>
			          	<xsl:otherwise>
		                    <th nowrap='true'><xsl:value-of select="."/></th>
		              </xsl:otherwise>
	              </xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:variable name="paravalue" select="/root/tbody/tr[1]/td[ last()]"/>
	    	<xsl:choose>
	    		<xsl:when test="position()>1">
	    			<tr>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=1 or position()=4 or position()=5 or position()=6 or position()= last()">
			            	<td style="display:none"><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test="position()=2">
			            	<td width='100'><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test="position()=3">
			               <td width='200'>
	    	      	       <xsl:if test=" ../td[4] = 0 ">
	    	      		   <img src='/images2/menu/folderopen.png' _exp='-' onclick='expand(this)'/>
	          		       </xsl:if>
	          		       <xsl:value-of select="."/>
	          		       </td> 
			            </xsl:when>
			          	<xsl:otherwise>
			          		<td align="right"><xsl:value-of select="format-number(.,$paravalue )"/></td>
			          	</xsl:otherwise>
		          	</xsl:choose>
		          </xsl:for-each>      
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>