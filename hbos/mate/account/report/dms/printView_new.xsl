<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td)-1"/>
  <xsl:variable name="empname" select="/root/bottom/tr[1]/td[3]"/>
  	<root>
    	<thead>
	  		<tr noWrap="true" >
	      			<td noWrap="true" >
	      				<xsl:attribute name="style">fontsize:maintitle;colspan:<xsl:value-of select="count(/root/tbody/tr[1]/td[position()!=last()])"/></xsl:attribute>
	      			</td>
	      		<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;2]">
	      			<td noWrap="true" style="display:none"/>
						</xsl:for-each>
	     </tr>
	     <tr noWrap="true" >
						<td noWrap="true" style="fontsize:subtitle;colspan:colcount">
	      				<xsl:attribute name="style">fontsize:subtitle;colspan:<xsl:value-of select="count(/root/tbody/tr[1]/td[position()!=last()])"/></xsl:attribute>
	      			</td>
	      		<xsl:for-each select ="/root/tbody/tr[1]/td[position()&gt;2]">
	      			<td noWrap="true" style="display:none"/>
						</xsl:for-each>

	     </tr>
	     <tr noWrap="true">
	     
					<td noWrap="true">
						<xsl:attribute name="style">fontsize:subtitle;colspan:<xsl:value-of select="ceiling($colNums div 2)"/>;align:left;</xsl:attribute>
					</td>	
						<xsl:for-each select ="/root/tbody/tr[1]/td[position()&lt; ceiling($colNums div 2) ]">
							<td noWrap="true" style="display:none"/>
						</xsl:for-each>
					
					<td noWrap="true">
						<xsl:attribute name="style">fontsize:subtitle;colspan:<xsl:value-of select="$colNums - ceiling($colNums div 2)"/>;align:right;</xsl:attribute>
						<xsl:value-of select="/root/annex/rightTitle"/>
					</td>
					<xsl:for-each select ="/root/tbody/tr[1]/td[position()&lt; ($colNums - ceiling($colNums div 2) ) -1 ]">
						<td noWrap="true" style="display:none"/>
					</xsl:for-each>
	     </tr>
	     <xsl:for-each select="/root/tbody/tr[position()=1]">
  			<tr noWrap='true' class='mainHead'>
          <xsl:for-each select="td">
          	<xsl:choose>
	          	<xsl:when test=" position() != 3 ">
	          		<td nowrap='true'><xsl:value-of select="."/></td>
	          	</xsl:when>
	          	<xsl:otherwise>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
    	</thead>
    	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	      <xsl:variable name="colName" select="position()"/>
	    	<tr>
	    	  <xsl:for-each select="td">
	    	    <xsl:choose>
	    	      <xsl:when test=" position() = 3">
	          	</xsl:when>
	    	      <xsl:when test=" position() = 2">
	    	      	<td>
	          		<xsl:value-of select="."/>
	          		</td>
	          	</xsl:when>
	    	      <xsl:when test="position() &gt; 3">
	    	      	<td noWrap="true">
                  <xsl:value-of select="."/>
                </td>
	          	</xsl:when>
	          	<xsl:otherwise>
	          		<td><xsl:value-of select="."/></td>
              </xsl:otherwise>
	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
 		<tfoot>
 			<tr noWrap='true'>
        <td align="left" style='colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNums"/>    
        </xsl:call-template>
  		</tr>
 		</tfoot>
 	
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
		
</xsl:stylesheet>