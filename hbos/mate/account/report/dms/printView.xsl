<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <xsl:variable name="rowNums"></xsl:variable>
    <xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td) - 4"/>
  	<root>
    	<thead>
    		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  			<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
    		<xsl:variable name="priceFormat" select="/root/tbody/tr[1]/td[last()-1]"/>
    		<xsl:for-each select="/root/tbody/tr">
  	    	<xsl:choose>
  	    		<xsl:when test="position()=1">
  	    			<tr noWrap='true'>
				        <td style='fontsize:maintitle;colspan:colcount'></td>
				  			<xsl:call-template name="repeat">
				  				<xsl:with-param name="times" select="$colNums" />    
				        </xsl:call-template>
				  		</tr>
  	    			<tr noWrap='true' class='mainHead'>
  		          <td nowrap='true'>���ҷ���</td>
  		          <xsl:for-each select="td">
  		          	<xsl:choose>
  			          	<xsl:when test="position()=1 or position()=4 or position()=5 or position()=last()">
  			          	</xsl:when>
  			          	<xsl:otherwise>
  		                <td nowrap='true'><xsl:value-of select="."/></td>
  		              </xsl:otherwise>
  	              </xsl:choose>
  		          </xsl:for-each>
  		  			</tr>
  	    		</xsl:when>
          </xsl:choose>
     		</xsl:for-each>
    	</thead>
    	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	      <xsl:variable name="colName" select="position()"/>
	    	<tr>
	    	  <xsl:for-each select="td">
	    	    <xsl:choose>
	    	      <xsl:when test="position()=1">
	    	        <xsl:choose>
	    	          <xsl:when test="../td[1]!=../../tr[$colName]/td[1]" >
	    	            <td valign="top">
	    	              <xsl:attribute name="rowspan" >
  				              <xsl:value-of select="../td[last()]"/>
  				            </xsl:attribute>
	    	              <xsl:value-of select="."/>
	    	            </td>
	    	          </xsl:when>
	    	          <xsl:when test="../td[1]=../../tr[$colName]/td[1]" >
	    	            <td style="display:none">
	    	              <xsl:value-of select="."/>
	    	            </td>
	    	          </xsl:when>
	    	        </xsl:choose>
	          	</xsl:when>
	          	
	    	      <xsl:when test="position()=4 or position()=5 or position()=last()">
	          	</xsl:when>
	          	<xsl:when test="position()>5 and position() &lt; last()">
	          		<td align="right" noWrap="true">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	          	</xsl:when>
	          	<xsl:otherwise>
                <td nowrap='true'><xsl:value-of select="."/></td>
              </xsl:otherwise>
	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>	
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>