<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:variable name="colName"></xsl:variable>
    <xsl:variable name="rowNum"></xsl:variable>
  	<thead>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=1">
	    			<tr noWrap='true' class='mainHead'>
		          <th nowrap='true'>���ҷ���</th>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=1 or position()=4 or position()=5 or position()=last()">
			          		
			          	</xsl:when>
			          	<xsl:otherwise>
		                <th nowrap='true'><xsl:value-of select="."/></th>
		              </xsl:otherwise>
	              </xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	      <xsl:variable name="colName" select="position()"/>
	    	<tr>
	    	  <xsl:for-each select="td">
	    	    <xsl:choose>
	    	      <xsl:when test="position()=1">
	    	        <xsl:choose>
	    	          <xsl:when test="../td[1]!=../../tr[$colName]/td[1]" >
	    	            <td valign="top">
	    	              <xsl:attribute name="rowspan" >
  				              <xsl:value-of select="../td[last()]"/>
  				            </xsl:attribute>
	    	              <xsl:value-of select="."/>
	    	            </td>
	    	          </xsl:when>
	    	          <xsl:when test="../td[1]=../../tr[$colName]/td[1]" >
	    	            <td style="display:none">
	    	              <xsl:value-of select="."/>
	    	            </td>
	    	          </xsl:when>
	    	        </xsl:choose>
	          	</xsl:when>
	          	
	    	      <xsl:when test="position()=4 or position()=5 or position()=last()">
	          	</xsl:when>
	          	<xsl:when test="position()>5 and position() &lt; last()">
	          		<td class="numberText" noWrap="true">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	          	</xsl:when>
	          	<xsl:otherwise>
                <td nowrap='true'><xsl:value-of select="."/></td>
              </xsl:otherwise>
	    	    </xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>