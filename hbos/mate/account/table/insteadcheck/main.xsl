<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  			<th noWrap="true">材料编码</th>
  			<th noWrap="true">材料名称</th>
  			<th noWrap="true">规格型号</th>
  	  	<th noWrap="true">单位</th>
  			<th noWrap="true">条形码</th>
  			<th noWrap="true">批号</th>
  			<th noWrap="true">单价</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
      </tr>
     </thead>
  	<tbody> 
  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <xsl:for-each select="/root/tbody/tr">
  	  	
	      <tr>
	      	<xsl:if test="position()!=last()">
		        <xsl:for-each select="td">
		          <xsl:choose>
		            <xsl:when test=" position()=7 or position()=8 or position()=9 ">
			            <td align='right' class='moneyCol'>
			              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
			            </td>
				    		</xsl:when>
				    		<xsl:otherwise>
				          <td align='left'>
		                <xsl:value-of select="."/>
		              </td>
		            </xsl:otherwise>
		          </xsl:choose>
		        </xsl:for-each>
	        </xsl:if>
	        <xsl:if test="position()=last()">
	        	<xsl:for-each select="td">
		          <xsl:choose>
		            <xsl:when test="position()=7 or position()=8 ">
			            <td >
			            </td>
				    		</xsl:when>
				    		<xsl:when test="  position()=9 ">
			            <td align='right' class='moneyCol'>
			              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
			            </td>
				    		</xsl:when>
				    		<xsl:otherwise>
				          <td align='left'>
		                <xsl:value-of select="."/>
		              </td>
		            </xsl:otherwise>
		          </xsl:choose>
		        </xsl:for-each>
	        </xsl:if>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>