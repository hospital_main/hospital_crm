<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
  <thead>
	<tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">材料编码</th>
  	  	<th noWrap="true" rowspan="2">材料名称</th>
		<th noWrap="true" rowspan="2">规格型号</th>
		<th noWrap="true" rowspan="2">单位</th>
		<th noWrap="true" rowspan="2">单价</th>
		<th noWrap="true" colspan="2">本期收入</th>
		<th noWrap="true" colspan="2">本期发出</th>
		<th noWrap="true" colspan="2">差额</th>
      </tr>
      <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" >数量</th>
  	  	<th noWrap="true" >金额</th>
		<th noWrap="true" >数量</th>
  	  	<th noWrap="true" >金额</th>
		<th noWrap="true" >数量</th>
  	  	<th noWrap="true" >金额</th>
      </tr>
     </thead>
	  <tbody>
  	   <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>

			
			<xsl:when test=" position()=5">

				<xsl:if test="../td[5]!='合计'">
	          			
	          				<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')" /></td>
	          			
	          		</xsl:if>
				
	          		<xsl:if test="../td[5]='合计'">
	          			
	          				<td align="right"> </td>
	          			
	          		</xsl:if>

	          		
	          	</xsl:when>

	          	<xsl:when test=" position() &gt; 5">	
				<xsl:if test="../td[5]!='合计'">
	          			
	          			<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')" /></td>
	          			
				</xsl:if>
	          	</xsl:when>

				



			<xsl:otherwise>
				<td align='left'>
					<xsl:value-of select="."/>
				</td>
			</xsl:otherwise>


	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>