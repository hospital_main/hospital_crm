<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
			<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
			<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
			<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
		<tr noWrap='true' class='mainHead'>
			<td noWrap="true" rowspan="2">材料编码</td>
			<td noWrap="true" rowspan="2">材料名称</td>
			<td noWrap="true" rowspan="2">规格型号</td>
			<td noWrap="true" rowspan="2">单位</td>
			<td noWrap="true" rowspan="2">单价</td>
			<td noWrap="true" colspan="2">本期收入</td>
			<td style="display:none"/>
			<td noWrap="true" colspan="2">本期发出</td>
			<td style="display:none"/>
			<td noWrap="true" colspan="2">差额</td>
			<td style="display:none"/>
	      </tr>
	      <tr noWrap='true' class='mainHead'>
			<td style="display:none"/>
			<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
			<td noWrap="true" >数量</td>
			<td noWrap="true" >金额</td>
			<td noWrap="true" >数量</td>
			<td noWrap="true" >金额</td>
			<td noWrap="true" >数量</td>
			<td noWrap="true" >金额</td>
	      </tr>
     </thead>
	  <tbody>
  	   <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test=" position()=5">

				<xsl:if test="../td[5]!='合计'">
	          			
	          				<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')" /></td>
	          			
	          		</xsl:if>
				
	          		<xsl:if test="../td[5]='合计'">
	          			
	          				<td align="right"> </td>
	          			
	          		</xsl:if>

	          		
	          	</xsl:when>

	          	<xsl:when test=" position() &gt; 5">	
				<xsl:if test="../td[5]!='合计'">
	          			
	          			<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')" /></td>
	          			
				</xsl:if>
	          	</xsl:when>

			<xsl:otherwise>
				<td align='left'>
					<xsl:value-of select="."/>
				</td>
	            </xsl:otherwise>
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>

	  </root>
	</xsl:template>
</xsl:stylesheet>