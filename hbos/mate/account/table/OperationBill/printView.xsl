<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>      
	      <thead>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
    	    <tr noWrap='true' class='mainHead'>
    	  	<td noWrap="true">单据类型</td>
    	  	<td noWrap="true">发生金额</td>
    			<td noWrap="true">财务入账金额</td>
    			<td noWrap="true">财务未入账金额</td>
          </tr>
        </thead>      
	      <tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test=" position()=2">
          			<td align='right' class='numberText'>
          				<a href="#">
									  <xsl:attribute name="onclick" >
									    javascript:openDialog('detail/main.html?load=&lt;mate_type_code&gt;<xsl:value-of select="../td[8]"/>&lt;/mate_type_code&gt;&lt;from_year_month&gt;<xsl:value-of select="../td[9]"/>&lt;/from_year_month&gt;&lt;to_year_month&gt;<xsl:value-of select="../td[10]"/>&lt;/to_year_month&gt;&lt;store_code&gt;<xsl:value-of select="../td[11]"/>&lt;/store_code&gt;','dialogWidth:850px;dialogHeight:650px',result)
									  </xsl:attribute>
									  <xsl:value-of select="."/> 
									</a>
          			</td>
	          	</xsl:when>
	          	
	          	<xsl:when test=" position()=3">
          			<td align='right' class='numberText'>
          				<a href="#">
									  <xsl:attribute name="onclick" >
									    javascript:openDialog('detail/main.html?load=&lt;mate_type_code&gt;<xsl:value-of select="../td[8]"/>&lt;/mate_type_code&gt;&lt;from_year_month&gt;<xsl:value-of select="../td[9]"/>&lt;/from_year_month&gt;&lt;to_year_month&gt;<xsl:value-of select="../td[10]"/>&lt;/to_year_month&gt;&lt;store_code&gt;<xsl:value-of select="../td[11]"/>&lt;/store_code&gt;','dialogWidth:850px;dialogHeight:650px',result)
									  </xsl:attribute>
									  <xsl:value-of select="."/>
									</a>
          			</td>
	          	</xsl:when>
	          	
	          	<xsl:when test=" position()=4">
          			<td align='right' class='numberText'>
          				<a href="#">
									  <xsl:attribute name="onclick" >
									    javascript:openDialog('detail/main.html?load=&lt;mate_type_code&gt;<xsl:value-of select="../td[8]"/>&lt;/mate_type_code&gt;&lt;from_year_month&gt;<xsl:value-of select="../td[9]"/>&lt;/from_year_month&gt;&lt;to_year_month&gt;<xsl:value-of select="../td[10]"/>&lt;/to_year_month&gt;&lt;store_code&gt;<xsl:value-of select="../td[11]"/>&lt;/store_code&gt;','dialogWidth:850px;dialogHeight:650px',result)
									  </xsl:attribute>
									  <xsl:value-of select="."/>
									</a>
          			</td>
	          	</xsl:when>
			    		
			    		<xsl:when test="position()>4">
			    			<td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
	      
   	</tbody>
	    
	  </root>
	</xsl:template>
</xsl:stylesheet>