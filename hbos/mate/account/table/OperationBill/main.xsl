<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" width="25%">单据类型</th>
  	  	<th noWrap="true" width="25%">发生金额</th>
  			<th noWrap="true" width="25%">财务入账金额</th>
  			<th noWrap="true" width="25%">财务未入账金额</th>
      </tr>
     </thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test=" position()=2">
          			<td align='right' class='numberText'>
          				<xsl:if test="'合计：'!=../td[1]">
	          				<a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('detail/account_all/main.html?load=&lt;bill_type_code&gt;<xsl:value-of select="../td[5]"/>&lt;/bill_type_code&gt;&lt;from_year_month&gt;<xsl:value-of select="../td[6]"/>&lt;/from_year_month&gt;&lt;to_year_month&gt;<xsl:value-of select="../td[7]"/>&lt;/to_year_month&gt;&lt;bill_type_name&gt;<xsl:value-of select="../td[1]"/>&lt;/bill_type_name&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <!--xsl:value-of select="format-number(.,'#,##0.00')"/-->  
										  <xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'合计：'=../td[1]">
										<xsl:value-of select="."/>
									</xsl:if>
          			</td>
	          	</xsl:when>
	          	
	          	<xsl:when test=" position()=3">
          			<td align='right' class='numberText'>
          			  <xsl:if test="'合计：'!=../td[1]">
	          				<a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('detail/account_in/main.html?load=&lt;bill_type_code&gt;<xsl:value-of select="../td[5]"/>&lt;/bill_type_code&gt;&lt;from_year_month&gt;<xsl:value-of select="../td[6]"/>&lt;/from_year_month&gt;&lt;to_year_month&gt;<xsl:value-of select="../td[7]"/>&lt;/to_year_month&gt;&lt;bill_type_name&gt;<xsl:value-of select="../td[1]"/>&lt;/bill_type_name&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'合计：'=../td[1]">
										<xsl:value-of select="."/>
									</xsl:if>
          			</td>
	          	</xsl:when>
	          	
	          	<xsl:when test=" position()=4">
          			<td align='right' class='numberText'>
          			<xsl:if test="'合计：'!=../td[1]">
          				<a href="#">
									  <xsl:attribute name="onclick" >
									    javascript:openDialog('detail/account_no_in/main.html?load=&lt;bill_type_code&gt;<xsl:value-of select="../td[5]"/>&lt;/bill_type_code&gt;&lt;from_year_month&gt;<xsl:value-of select="../td[6]"/>&lt;/from_year_month&gt;&lt;to_year_month&gt;<xsl:value-of select="../td[7]"/>&lt;/to_year_month&gt;&lt;bill_type_name&gt;<xsl:value-of select="../td[1]"/>&lt;/bill_type_name&gt;','dialogWidth:850px;dialogHeight:650px',result)
									  </xsl:attribute>
									  <xsl:value-of select="."/>
									</a>
								</xsl:if>
									<xsl:if test="'合计：'=../td[1]">
										<xsl:value-of select="."/>
									</xsl:if>
          			</td>
	          	</xsl:when>
			    		
			    		<xsl:when test="position()>4">
			    			<td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
	    
	      <!--tr>
	         <td>合计：</td>
	          <td align='right' class='numberText'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[2]),'#,##0.00')"/></td>
	           <td align='right' class='numberText'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),'#,##0.00')"/></td>
	            <td align='right' class='numberText'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[4]),'#,##0.00')"/></td>
	             <td style="display:none"></td>
	      </tr-->
	      
   	</tbody>
  </xsl:template>
</xsl:stylesheet>