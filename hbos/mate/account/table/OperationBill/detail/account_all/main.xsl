<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2" valign="center">日期</th>
  	  	<th noWrap="true" rowspan="2" valign="center">单据号</th>
  	  	<th noWrap="true" rowspan="2" valign="center">单据类型</th>
  	  	<th noWrap="true" rowspan="2" valign="center">业务类型</th>
  			<th noWrap="true" rowspan="2" valign="center">摘要</th>
  			<th noWrap="true" rowspan="2" valign="center">金额</th>
  			<th noWrap="true" colspan="3">会计凭证</th>
      </tr>
      <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">期间</th>
  			<th noWrap="true">日期</th>
  			<th noWrap="true">凭证号</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="(../td[1] = '财务入账金额合计' or ../td[1] = '财务未入账金额合计') and position()!=6 and 10 > position()">
	              <td bgcolor="#99CCFF" align="center">
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:when>
	            
	            <xsl:when test="(../td[1] = '财务入账金额合计' or ../td[1] = '财务未入账金额合计') and position()=6 and 10 > position()">
	              <td bgcolor="#99CCFF" align="center">
	                 <!--xsl:value-of select="format-number(.,'#,##0.00')"/--> 
	                 <xsl:value-of select="."/>
	              </td>
	            </xsl:when>
	            
	          	<xsl:when test=" position()=2">
	          			<td>
	          			  <xsl:if test="'0采购入库'=../td[4] or '3有偿调入'=../td[4] or '4无偿调入'=../td[4] or '7盘盈入库'=../td[4] or '9其他入库'=../td[4] or '11采购退货'=../td[4] or '1自制品入库'=../td[4] or '20委托加工入库'=../td[4] or '23捐赠入库'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../../../whr/query/mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'2科室领用'=../td[4] or '5有偿调出'=../td[4] or '6无偿调出'=../td[4] or '8盘亏出库'=../td[4] or '10其他出库'=../td[4] or '12报废出库'=../td[4] or '15二级库盘点'=../td[4]  or '22科室退库'=../td[4] or '31借库'=../td[4] or '32还库'=../td[4] or '17自制品原材料领用'=../td[4] or '24捐赠出库'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../../../whr/query/mate/whr/out/common/update.html?load=&lt;a&gt;&lt;/a&gt;&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'14移出库'=../td[4] or '13移入库'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../../../whr/query/mate/whr/out/move/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'18代销入库'=../td[4] or '41代销退货'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../../../whr/query/mate/whr/in/instead/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'36代销出库'=../td[4] or '33代销借库'=../td[4] or '34代销还库'=../td[4] or '35代销退库'=../td[4]  " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../../../whr/query/mate/whr/out/instead/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'37代销移入库'=../td[4] or '38代销移出库'=../td[4]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../../../whr/query/mate/whr/out/insteadmove/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<!--<xsl:if test="'1自制品入库'=../td[4] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../../../whr/query/mate/whr/smp/in/update.html?load=&lt;id&gt;<xsl:value-of select="../td[10]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'17自制品原材料领用'=../td[4] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../../../whr/query/mate/whr/smp/out/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>-->
							  	<xsl:if test="'16医生医嘱'=../td[4] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../../../whr/query/mate/whr/second/advice/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'21专购品'=../td[4] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../../../whr/query/mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[11]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>	          			 
	          		</td>
	          	</xsl:when>
	          	
	          	<xsl:when test=" position()=9">
	          			<td>
	          				<a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../td[10]"/>&lt;/vouch_id&gt;')
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
	          			</td>
	          	</xsl:when>
	          	
	          	
	          	<xsl:when test="position()=10">
		           
			    		</xsl:when>
			    		
			    		
	          	
	            <xsl:when test="position()=6">
		            <td align='right' class='moneyCol'>
		              <xsl:value-of select="."/>  
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position()>10">
		            <td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>