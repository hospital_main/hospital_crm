<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>	    	  
	   <thead>
	   	<tr noWrap='true' class='mainHead'>
	   		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   	</tr>
	   	<tr noWrap='true' class='mainHead'>
	   		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   	</tr>
	   	<tr noWrap='true' class='mainHead'>
	   		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   	</tr>
	   	<tr noWrap='true' class='mainHead'>
	   		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   		<td style="display:none"/>
	   	</tr>
  	  <tr noWrap='true' class='mainHead'>
  	  	<td noWrap="true" rowspan="2" valign="center">日期</td>
  	  	<td noWrap="true" rowspan="2" valign="center">单据号</td>
  	  	<td noWrap="true" rowspan="2" valign="center">单据类型</td>
  	  	<td noWrap="true" rowspan="2" valign="center">业务类型</td>
  			<td noWrap="true" rowspan="2" valign="center">摘要</td>
  			<td noWrap="true" rowspan="2" valign="center">金额</td>
  			<td noWrap="true" colspan="3">会计凭证</td>
      </tr>
      <tr noWrap="true" class="mainHead">  	
      		<td noWrap="true"/>
	   		<td noWrap="true"/>
	   		<td noWrap="true"/>
	   		<td noWrap="true"/>
	   		<td noWrap="true"/>
	   		<td noWrap="true"/>
  			<td noWrap="true">期间</td>
  			<td noWrap="true">日期</td>
  			<td noWrap="true">凭证号</td>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="(../td[1] = '财务入账金额合计' or ../td[1] = '财务未入账金额合计') and position()!=6">
	              <td bgcolor="#99CCFF" align="center">
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:when>
	            
	            <xsl:when test="(../td[1] = '财务入账金额合计' or ../td[1] = '财务未入账金额合计') and position()=6">
	              <td bgcolor="#99CCFF" align="center">
	                 <xsl:value-of select="."/> 
	              </td>
	            </xsl:when>
	            
	          	<xsl:when test=" position()=2">
	          			<td>
	          				<a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('account/main.html?load=&lt;mate_inv_code&gt;<xsl:value-of select="../td[2]"/>&lt;/mate_inv_code&gt;&lt;from_year_month&gt;<xsl:value-of select="../td[13]"/>&lt;/from_year_month&gt;&lt;to_year_month&gt;<xsl:value-of select="../td[14]"/>&lt;/to_year_month&gt;&lt;store_code&gt;<xsl:value-of select="../td[15]"/>&lt;/store_code&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
	          			</td>
	          	</xsl:when>
	          	<xsl:when test=" position()=9">
	          			<td>
	          				<a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('account/main.html?load=&lt;mate_inv_code&gt;<xsl:value-of select="../td[9]"/>&lt;/mate_inv_code&gt;&lt;from_year_month&gt;<xsl:value-of select="../td[13]"/>&lt;/from_year_month&gt;&lt;to_year_month&gt;<xsl:value-of select="../td[14]"/>&lt;/to_year_month&gt;&lt;store_code&gt;<xsl:value-of select="../td[15]"/>&lt;/store_code&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
	          			</td>
	          	</xsl:when>
	          	
	            <xsl:when test="position()=6">
		            <td align='right' class='numberText'>
		              <xsl:value-of select="."/>  
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position()>9">
		            <td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	    
	  </root>
	</xsl:template>
</xsl:stylesheet>