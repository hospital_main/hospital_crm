<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">�������</th>
  			<th noWrap="true" colspan="3">����</th>
  			<th noWrap="true" colspan="3">�ۼ�</th>
      </tr>
      <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">Ԥ��</th>
  			<th noWrap="true">Ԥ��ִ��</th>
  			<th noWrap="true">δִ��</th>
  			<th noWrap="true">Ԥ��</th>
  			<th noWrap="true">Ԥ��ִ��</th>
  			<th noWrap="true">δִ��</th>
  		</tr>
  	</thead>
  	<tbody> 
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1">
		            <td >
		              <xsl:value-of select="."/>  
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td align='right' class='moneyCol'>
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>