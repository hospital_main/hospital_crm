<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>      
	      <thead>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>  
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
		  	<tr noWrap='true' class='mainHead'>
		  	  	<td noWrap="true">物资类别编码</td>
  	  			<td noWrap="true">物资类别名称</td>
  	  			<td noWrap="true">仓库名称</td>
  	  			<td noWrap="true">货位编码</td>
  	  			<td noWrap="true">货位名称</td>
  				<td noWrap="true">期初库存金额</td>
  				<td noWrap="true">本期增加金额</td>
  				<td noWrap="true">本期减少金额</td>
  				<td noWrap="true">期末结存余额</td>
		    </tr>
     </thead>   
	 <tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test="position()=1 or position()=2 or position()=5 ">
		            <td align='left'>
		              <xsl:value-of select="."/>
		            </td>
			    </xsl:when>
			    <xsl:when test="position()=6 ">
		            <td align='left'>
		              <xsl:value-of select="."/>
		            </td>
		            <td align='left'>
		              <xsl:value-of select="../td[15]"/>
		            </td>
			    </xsl:when>
	            <xsl:when test=" position()=7 or position()=8 or position()=9 or position()=10">
		            <td align='right' class='moneyCol'>
		              <!--xsl:value-of select="format-number(.,'#,##0.00')"/-->  
		              <xsl:value-of select="."/>
		            </td>
			    </xsl:when>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	  </tbody>
	    
	  </root>
	</xsl:template>
</xsl:stylesheet>