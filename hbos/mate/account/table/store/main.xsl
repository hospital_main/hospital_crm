<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">物资类别编码</th>
  	  	<th noWrap="true">物资类别名称</th>
  			<th noWrap="true">期初库存金额</th>
  			<th noWrap="true">本期增加金额</th>
  			<th noWrap="true">本期减少金额</th>
  			<th noWrap="true">进销误差</th>
  			<th noWrap="true">期末结存余额</th>
      </tr>
     </thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test=" position()=1">
	          		<xsl:if test="../td[8]=1">
	          			<td>
	          				<a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('detail/main.html?load=&lt;mate_type_code&gt;<xsl:value-of select="../td[9]"/>&lt;/mate_type_code&gt;&lt;mate_type_name&gt;<xsl:value-of select="../td[2]"/>&lt;/mate_type_name&gt;&lt;from_year_month&gt;<xsl:value-of select="../td[10]"/>&lt;/from_year_month&gt;&lt;to_year_month&gt;<xsl:value-of select="../td[11]"/>&lt;/to_year_month&gt;&lt;store_code&gt;<xsl:value-of select="../td[12]"/>&lt;/store_code&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
	          			</td>
	          		</xsl:if>
	          		<xsl:if test="../td[8]!=1">
	          			<td>
	          				<xsl:value-of select="."/>
	          			</td>
	          		</xsl:if>
	          	</xsl:when>
	            <xsl:when test=" position()=3 or position()=4 or position()=5 or position()=6 or position()=7">
		            <td align='right' class='moneyCol'>
		               <xsl:value-of select="format-number(.,'#,##0.00')"/> 
		              <!--<xsl:value-of select="."/>--> 
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position()>7">
			    			<td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>