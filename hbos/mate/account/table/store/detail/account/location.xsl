<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">日期</th>
  	  	<th noWrap="true" rowspan="2">单据号</th>
  	  	<th noWrap="true" rowspan="2">单据类型</th>
  	  	<th noWrap="true" rowspan="2">业务类型</th>
  	  	<th noWrap="true" rowspan="2">摘要</th>
  			<th noWrap="true" colspan="3">入库</th>
  			<th noWrap="true" colspan="3">出库</th>
  			<th noWrap="true" colspan="3">结存</th>
  	  </tr>
      <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">数量</th>
  			<th noWrap="true">单价</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">单价</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">单价</th>
  			<th noWrap="true">金额</th>
  	</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=6 or position()=9 or position()=12">
		            <td align='right' class='moneyCol'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position()=7 or position()=8 or position()=10 or position()=11 or position()=13 or position()=14">
		            <td align='right' class='moneyCol'>
		              <xsl:value-of select="."/>  
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>