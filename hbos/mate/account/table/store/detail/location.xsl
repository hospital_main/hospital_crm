<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">物资材料编码</th>
  	  	<th noWrap="true" rowspan="2">物资材料名称</th>
  	  	<th noWrap="true" rowspan="2">规格型号</th>
  	  	<th noWrap="true" rowspan="2">计量单位</th>
  	  	<th noWrap="true" rowspan="2">仓库名称</th>
  	  	<th noWrap="true" rowspan="2">货位名称</th>
  		<th noWrap="true" colspan="2">期初库存</th>
  		<th noWrap="true" colspan="2">本期增加</th>
  		<th noWrap="true" colspan="2">本期减少</th>
  		<th noWrap="true" colspan="2">期末结存</th>
      </tr>
      <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
  	  	<xsl:variable name="mylast" select="last()-position()"/>
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=1 ">
		            <xsl:if test="$mylast!=0">
	          			<td>
	          				<a href="#">
								<xsl:attribute name="onclick" >
									javascript:openDialog('account/location.html?load=&lt;mate_inv_code&gt;<xsl:value-of select="../td[1]"/>&lt;/mate_inv_code&gt;&lt;from_year_month&gt;<xsl:value-of select="../td[17]"/>&lt;/from_year_month&gt;&lt;to_year_month&gt;<xsl:value-of select="../td[18]"/>&lt;/to_year_month&gt;&lt;store_code&gt;<xsl:value-of select="../td[5]"/>&lt;/store_code&gt;&lt;location_code&gt;<xsl:value-of select="../td[7]"/>&lt;/location_code&gt;','dialogWidth:850px;dialogHeight:650px',result)
								</xsl:attribute>
							<xsl:value-of select="."/>
							</a>
	          			</td>
	          		</xsl:if>
	          		<xsl:if test="$mylast=0">
	          			<td>
	          				<xsl:value-of select="."/>
	          			</td>
	          		</xsl:if>
			    </xsl:when>
	          	<xsl:when test=" position()=2 or position()=3 or position()=4 or position()=6 or position()=8 ">
	          		<td align='left'>
		              <xsl:value-of select="."/>
		            </td>
	          	</xsl:when>
	          	<xsl:when test="position()=9 or position()=11 or position()=13 or position()=15">
		            <td align='right' >
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
		            </td>
			    </xsl:when>
	            <xsl:when test="position()=10 or position()=12 or position()=14 or position()=16">
		            <td align='right' class='moneyCol'>
		              <!--xsl:value-of select="format-number(.,'#,##0.00')"/-->  
		              <xsl:value-of select="."/>
		            </td>
			    </xsl:when>
			    <xsl:otherwise>
	            </xsl:otherwise>
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>