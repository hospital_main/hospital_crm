<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
		  <tr noWrap='true' class='mainHead'> 
        <td width='25' ></td>
		    <td width='120'>供应商编码</td>
		    <td>供应商名称</td>
		     <td style="display:none"></td>	
		    <td style="display:none"></td>	
		    <td style="display:none"></td>	
		    <td style="display:none"></td>	
		    <td style="display:none"></td>	
		    <td>数量</td>	
		    <td style="display:none"></td>
		    <td>总金额</td>	
	
		  </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="pos" select="position()"/>
      	<xsl:if test="td[10] = 0">
        <tr>
        	<xsl:attribute name="id">c_<xsl:value-of select="td[1]"/></xsl:attribute>
        	<xsl:attribute name="row"><xsl:value-of select="$pos"/></xsl:attribute>
          <td align='center'>
            <!--<input type='checkbox'><xsl:attribute name="code"><xsl:value-of select="td[1]"/></xsl:attribute></input>-->
          </td>
       <xsl:for-each select="td[position() != last()+1]">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td noWrap='true' >
                   <b><xsl:value-of select="."/></b><a href="#" style="text-decoration: none;">
                   	<xsl:attribute name="onclick">doImg(img_<xsl:value-of select="."/>)</xsl:attribute>
                   	　<img src="down.gif" border="0" d="down" style="cursor:hand">
                   	<xsl:attribute name="id">img_<xsl:value-of select="."/></xsl:attribute><xsl:attribute name="code"><xsl:value-of select="."/></xsl:attribute></img>　</a>
                </td>
              </xsl:when>
            
              
              <xsl:when test="position()=3">
	             
	            </xsl:when>
	              <xsl:when test="position()=4 ">
	              
	            </xsl:when>
	              <xsl:when test="position()=5 ">
	              
	            </xsl:when>
	             <xsl:when test="position()=6 ">
	              
	            </xsl:when>
	             <xsl:when test="position()=10">
	              
	            </xsl:when>
	            <xsl:when test="position()=9">
	              <td  align="right">
                 <b> <xsl:value-of select="format-number(.,'#,##0.00')"/></b>
                </td>
	            </xsl:when>
	            
	             <xsl:when test="position()=8">
	              <td  align="right" style="display:none">
                   <b><xsl:value-of select="."/></b>
                </td>
	            </xsl:when>
	       			<xsl:when test="position()=7">
								<td align="right">
	                <b> <xsl:value-of select="format-number(.,'#,##0.00')"/></b>
	              </td>
							</xsl:when>
							     
              <xsl:otherwise>
                <td noWrap='true'>
                  <b><xsl:value-of select="."/></b>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  	</xsl:if>
  	<xsl:if test="td[10] = 1">
  		
  		<xsl:if test=" $pos =2 or (td[1] = ../tr[$pos - 1]/td[1] and td[1] != ../tr[$pos - 2]/td[1])">
	        	<xsl:text disable-output-escaping="yes">&lt;tr style="display:none" id="tr_</xsl:text><xsl:value-of select="td[1]"/><xsl:text disable-output-escaping="yes">"&gt;
	        	&lt;td&gt;&lt;/td&gt;&lt;td colspan="9"&gt; &lt;table width='80%' style="border:0px solid black;border-collapse:collapse;" &gt;</xsl:text>
	        	<tr>
	        		<b><td class="smallTd" style='display:none'>供应商编码</td>
	        		<td class="smallTd"><b>出库单号</b></td>
	        		<td class="smallTd"><b>出库日期</b></td>
	        		<td class="smallTd"><b>材料编码</b></td>
	        		<td class="smallTd"><b>材料名称</b></td>
	        		<td class="smallTd"><b>使用科室</b></td>
	        		<td class="smallTd"><b>数量</b></td>
	        		<td class="smallTd"><b>单价</b></td>
	        		<td class="smallTd"><b>金额</b></td>
	        		<td class="smallTd"></td></b>
	        	</tr>
		</xsl:if>
			<tr>
				<td class="smallTd2"><a href="#"><xsl:attribute name="onclick">openItem("<xsl:value-of select="td[1]"/>","<xsl:value-of select="td[2]"/>")</xsl:attribute><xsl:value-of select="td[2]"/></a></td>
				<td class="smallTd2" ><xsl:value-of select="td[3]"/></td>
				<td class="smallTd2" ><xsl:value-of select="td[4]"/></td>						
				<td class="smallTd2" ><xsl:value-of select="td[5]"/></td>
				<td class="smallTd2" ><xsl:value-of select="td[6]"/></td>
				<td class="smallTd2" ><xsl:value-of select="td[7]"/></td>
				<td class="smallTd2"><xsl:value-of select="format-number(td[8],'#,##0.00')"/></td>
				<td class="smallTd2"><xsl:value-of select="format-number(td[9],'#,##0.00')"/></td>
				<td class="smallTd2" style='display:none'><xsl:value-of select="td[10]"/></td>	
			</tr>
		
		<xsl:if test="td[1] != ../tr[$pos + 1]/td[1]">
	        	<xsl:text disable-output-escaping="yes">
				&lt;/table&gt;&lt;/td&gt;&lt;/tr&gt;
			</xsl:text>
		</xsl:if>
        </xsl:if>
       
        
        
      </xsl:for-each>  
    </tbody>
  </xsl:template>
</xsl:stylesheet>


