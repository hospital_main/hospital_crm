<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
		  <tr noWrap='true' class='mainHead'> 
		  	<td width='25'><!--<input type='checkbox'/>--></td>
		    <td width='120' >材料编码</td>
		    <td>材料名称</td>
		    <td>规格型号</td>
		    <td colspan="1">计量单位</td>		
		    <td>数量</td>
		    <td>单价</td>
		    <td>金额</td>
		    <td width='200'>供应商</td>
		    <td width='200'>生产厂商</td>
		  </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="pos" select="position()"/>
      	<xsl:if test="td[10] = 0">
        <tr>
        	<xsl:attribute name="id">c_<xsl:value-of select="td[1]"/></xsl:attribute>
        	<xsl:attribute name="row"><xsl:value-of select="$pos"/></xsl:attribute>
          <td align='center'>
            <!--<input type='checkbox'><xsl:attribute name="code"><xsl:value-of select="td[1]"/></xsl:attribute></input>-->
          </td>
       <xsl:for-each select="td[position() != last()]">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td noWrap='true' >
                   <b><xsl:value-of select="."/></b><a href="#" style="text-decoration: none;">
                   	<xsl:attribute name="onclick">doImg(img_<xsl:value-of select="."/>)</xsl:attribute>
                   	　<img src="down.gif" border="0" d="down" style="cursor:hand">
                   	<xsl:attribute name="id">img_<xsl:value-of select="."/></xsl:attribute><xsl:attribute name="code"><xsl:value-of select="."/></xsl:attribute></img>　</a>
                </td>
              </xsl:when>
	       			<xsl:when test="position() &gt; 4 and position() &lt;8">
								<td  class="numberText">
	               <b> <xsl:value-of select="format-number(.,'#,##0.00')"/></b>
	              </td>
							</xsl:when>
							     
              <xsl:otherwise>
                <td noWrap='true'>
                  <b><xsl:value-of select="."/></b>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  	</xsl:if>
  	<xsl:if test="td[10] = 1">
  		
  		<xsl:if test=" $pos =2 or (td[1] = ../tr[$pos - 1]/td[1] and td[1] != ../tr[$pos - 2]/td[1])">
	        	<xsl:text disable-output-escaping="yes">&lt;tr style="display:none" id="tr_</xsl:text><xsl:value-of select="td[1]"/><xsl:text disable-output-escaping="yes">"&gt;
	        	&lt;td&gt;&lt;/td&gt;&lt;td colspan="9"&gt; &lt;table width='80%' style="border:0px solid black;border-collapse:collapse;" &gt;</xsl:text>
	        	<tr>
	        		<td class="smallTd"><b>单据号</b></td>
	        		<td class="smallTd"><b>出库日期</b></td>
	        		<td class="smallTd"><b>领用科室</b></td>
	        		<td class="smallTd"><b>数量</b></td>
	        		<td class="smallTd"><b>单价</b></td>
	        		<td class="smallTd"><b>金额</b></td>
	        	</tr>
		  </xsl:if>
						<tr>
							<td class="smallTd2" align="center"><a href="#"><xsl:attribute name="onclick">openItem("<xsl:value-of select="td[1]"/>","<xsl:value-of select="td[2]"/>")</xsl:attribute><xsl:value-of select="td[2]"/></a></td>
							<td class="smallTd2" align="center"><xsl:value-of select="td[3]"/></td>
							<td class="smallTd2" align="center"><xsl:value-of select="td[4]"/></td>
							<td class="smallTd2" align="center"><xsl:value-of select="format-number(td[5],'#,##0.00')"/></td>		
							<td class="smallTd2" align="center"><xsl:value-of select="format-number(td[6],'#,##0.00')"/></td>
							<td class="smallTd2" align="center"><xsl:value-of select="format-number(td[7],'#,##0.00')"/></td>
		 				</tr>
		  <xsl:if test="td[1] != ../tr[$pos + 1]/td[1]">
	        	<xsl:text disable-output-escaping="yes">
				&lt;/table&gt;&lt;/td&gt;&lt;/tr&gt;
			</xsl:text>
		 </xsl:if>
        </xsl:if>
      </xsl:for-each>  
    </tbody>
  </xsl:template>
</xsl:stylesheet>


