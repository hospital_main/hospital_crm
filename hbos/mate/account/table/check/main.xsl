<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">物资类别</th>
  			<th noWrap="true">期初余额</th>
  			<th noWrap="true">本期收入金额</th>
  			<th noWrap="true">本期支出金额</th>
  			<th noWrap="true">盘盈金额</th>
        <th noWrap="true">盘亏金额</th>
        <th noWrap="true">进销误差</th>
        <th noWrap="true">结存金额</th>           
      </tr>
    </thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=2 or position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8">
		            <td align='right' class='moneyCol'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>