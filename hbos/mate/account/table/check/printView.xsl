<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>	    	   
	      <thead>
	      	<tr noWrap='true' class='mainHead'>
	      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap='true' class='mainHead'>
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap='true' class='mainHead'>
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap='true' class='mainHead'>
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
			 	  <tr noWrap='true' class='mainHead'>
		  	  	<td noWrap="true">物资类别</td>
		  			<td noWrap="true">期初余额</td>
		  			<td noWrap="true">本期收入金额</td>
		  			<td noWrap="true">本期支出金额</td>
		  			<td noWrap="true">盘盈金额</td>
		        <td noWrap="true">盘亏金额</td>
		        <td noWrap="true">进销误差</td>
		        <td noWrap="true">结存金额</td>           
         </tr>
			  </thead>	      
	      <tbody>
		      <xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<xsl:for-each select="td">          
		            <xsl:choose>            
		              <xsl:when test="position()= 1">
		              	<td align="left">
		                	<xsl:value-of select="."/>
		                </td>
		              </xsl:when>  
		              <xsl:otherwise>
		              	<td class="numberText">
		              		<!--xsl:value-of select="format-number(.,'#,##0.00')"/-->
		              		<xsl:value-of select="."/>
		              	</td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
	      </tbody>
	   
	  </root>
	</xsl:template>
</xsl:stylesheet>