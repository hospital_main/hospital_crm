<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=" "/>
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th noWrap="true">材料编码</th>
				<th noWrap="true">材料名称</th>
				<th noWrap="true">规格型号</th>
				<th noWrap="true">批号</th>
				<th noWrap="true">单价</th>
				<th noWrap="true">结存数量</th>
				<th noWrap="true">结存金额</th>
				<th noWrap="true">计量单位</th>
				<th noWrap="true">生产厂商</th>
			</tr>
		</thead>
		<tbody>
		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:choose>
						<xsl:when test="td[11]=2">
							<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=1">
										<td>
				              <xsl:value-of select="."/>  
				            </td>
									</xsl:when>
									<xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=9 or position()=10 ">
										<td/>
									</xsl:when>
									<xsl:when test="position()=7">
										<td align='right' class='numberText'>
				              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
				            </td>
									</xsl:when>
									<xsl:when test="position()=8 ">
										<td align='right' class='numberText'>
				              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
				            </td>
									</xsl:when>
									
			          </xsl:choose>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="td[11]=1">
							<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=1 or position()=11 or position()=12">
									</xsl:when>
									<xsl:when test="position()=7">
										<td align='right' class='numberText'>
				              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
				            </td>
									</xsl:when>
									<xsl:when test="position()=6 ">
										<td align='right' class='numberText'>
				              <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>  
				            </td>
									</xsl:when>
									<xsl:when test="position()=8 ">
										<td align='right' class='numberText'>
				              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
				            </td>
									</xsl:when>
									<xsl:otherwise>
										<td align="left">
											<xsl:value-of select="."/>
										</td>
			            </xsl:otherwise>
			          </xsl:choose>
							</xsl:for-each>
						</xsl:when>
					</xsl:choose>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>