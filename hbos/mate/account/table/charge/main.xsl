<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/> <!--金额的书写方法，对非数据的处理 -->
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" width="30%">科室名称</th>
  			<th noWrap="true" width="30%">物资类别</th>
  			<th noWrap="true" width="40%">金额</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position()=3">
		            <td align='right' class='numberText'>
		              <!--xsl:value-of select="format-number(.,'#,##0.00')"/-->  
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>