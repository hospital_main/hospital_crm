<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">日期</th>
  	  	<th noWrap="true" rowspan="2">单据号</th>
  	  	<th noWrap="true" rowspan="2">单据类型</th>
  	  	<th noWrap="true" rowspan="2">业务类型</th>
  	  	<th noWrap="true" rowspan="2">摘要</th>
  			<th noWrap="true" colspan="3">入库</th>
  			<th noWrap="true" colspan="3">出库</th>
  			<th noWrap="true" colspan="3">结存</th>
  	  </tr>
      <tr noWrap="true" class="mainHead">  	
  			<th noWrap="true">数量</th>
  			<th noWrap="true">单价</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">单价</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">单价</th>
  			<th noWrap="true">金额</th>
  	</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test="position()=2">
	          		<!-- 打开单据明细页面 开始-->
	          		<td>
										<xsl:if test="../td[1]='A'">
											<xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
										</xsl:if>
										<xsl:if test="'采购入库'=../td[4] or '自制品入库'=../td[4] or '有偿调入'=../td[4] or '无偿调入'=../td[4] or '其他入库'=../td[4] or '采购退货'=../td[4] or '委托加工入库'=../td[4] or '捐赠入库'=../td[4] or '盘盈入库'=../td[4]">
											<a href="#">
												<xsl:attribute name="onclick">
										    javascript:openDialog('../../../whr/query/mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[15]"/>&lt;/id&gt;','dialogWidth:[MAXW];dialogHeight:[MAXH]',result)
										  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="'科室领用'=../td[4] or '有偿调出'=../td[4] or '无偿调出'=../td[4]  or '其他出库'=../td[4] or '报废出库'=../td[4] or '二级库盘点'=../td[4] or '自制品原材料领用'=../td[4] or '科室退库'=../td[4] or '捐赠出库'=../td[4] or '借库'=../td[4] or '还库'=../td[4] or '盘亏出库'=../td[4]">
											<a href="#">
												<xsl:attribute name="onclick">
										    javascript:openDialog('../../../whr/query/mate/whr/out/common/update.html?load=&lt;a&gt;&lt;/a&gt;&lt;id&gt;<xsl:value-of select="../td[15]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="'移出库'=../td[4] or '移入库'=../td[4]">
											<a href="#">
												<xsl:attribute name="onclick">
										    javascript:openDialog('../../../whr/query/mate/whr/out/move/update.html?load=&lt;id&gt;<xsl:value-of select="../td[15]"/>&lt;/id&gt;','dialogWidth:920px;dialogHeight:650px',result)
										  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="'期初入库'=../td[4] and  0=../td[18] ">
											<a href="#">
												<xsl:attribute name="onclick">
										    javascript:openDialog('../../../info/init/noconsignee/update.html?load=&lt;id&gt;<xsl:value-of select="../td[15]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="'期初入库'=../td[4] and  1=../td[18] ">
											<a href="#">
												<xsl:attribute name="onclick">
										    javascript:openDialog('../../../info/init/consignee/update.html?load=&lt;id&gt;<xsl:value-of select="../td[15]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="'代销入库'=../td[4] or '代销退货'=../td[4]">
											<a href="#">
												<xsl:attribute name="onclick">
										    javascript:openDialog('../../..//whr/in/instead/update.html?load=&lt;id&gt;<xsl:value-of select="../td[15]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="'代销出库'=../td[4] or '代销借库'=../td[4] or '代销还库'=../td[4] or '代销退库'=../td[4]  ">
											<a href="#">
												<xsl:attribute name="onclick">
										    javascript:openDialog('../../../whr/query/mate/whr/out/instead/update.html?load=&lt;id&gt;<xsl:value-of select="../td[15]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="'代销移入库'=../td[4] or '代销移出库'=../td[4]">
											<a href="#">
												<xsl:attribute name="onclick">
										    javascript:openDialog('../../../whr/query/mate/whr/out/insteadmove/update.html?load=&lt;id&gt;<xsl:value-of select="../td[15]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="'医生医嘱'=../td[4] ">
											<a href="#">
												<xsl:attribute name="onclick">
										    javascript:openDialog('../../../whr/query/mate/whr/second/advice/update.html?load=&lt;id&gt;<xsl:value-of select="../td[15]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<!--21专购品 改为 21专购品入库 21专购品出库  -->
										<xsl:if test="'专购品入库'=../td[4] ">
											<a href="#">
												<xsl:attribute name="onclick">
										    javascript:openDialog('../../../whr/query/mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[15]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="'专购品出库'=../td[4] ">
											<a href="#">
												<xsl:attribute name="onclick">
										    javascript:openDialog('../../../whr/query/mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[15]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										<xsl:if test="'盘盈入库'=../td[4] and 0=../td[18]">
											<a href="#">
												<xsl:attribute name="onclick">
										    javascript:openDialog('../../../whr/query/mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[15]"/>&lt;/id&gt;','dialogWidth:[MAXW];dialogHeight:[MAXH]',result)
										  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</xsl:if>
										
										
						</td>				
				<!-- 打开单据明细页面 结束-->
	          	</xsl:when>	
	            <xsl:when test="position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13 or position()=14">
		            <td align='right' class='moneyCol'>
		              <!--xsl:value-of select="format-number(.,'#,##0.00')"/-->
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position()=15">
			    			<td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>