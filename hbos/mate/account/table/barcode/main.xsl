<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=" "/>
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th noWrap="true">编码</th>
				<th noWrap="true">名称</th>
				<th noWrap="true">规格型号</th>
				<th noWrap="true">计量单位</th>
				<th noWrap="true">批号</th>
				<th noWrap="true">条码</th>
				<th noWrap="true">库存</th>
				<th noWrap="true">单价</th>
				<th noWrap="true">有效日期</th>
			</tr>
		</thead>
	<tbody>
		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=7">
							<td align='right' class='numberText'>
							<xsl:value-of select="format-number(.,'#,##0.00')"/>  
							</td>
						</xsl:when>
						<xsl:when test="position()=8 ">
							<td align='right' class='numberText'>
							<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td align="left">
							<xsl:value-of select="."/>
							</td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>