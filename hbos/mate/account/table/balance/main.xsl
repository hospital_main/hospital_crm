<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="disp" select="/root/tbody/tr[1]/td[last()]"/>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">材料编码</th>
  	  	<th noWrap="true" rowspan="2">材料名称</th>
  	  	<th noWrap="true" rowspan="2">规格型号</th>
  	  	<th noWrap="true" rowspan="2">计量单位</th>
  	  	<xsl:if test="$disp != '1'">
  			<th noWrap="true" colspan="3">期初余额</th>
  			<th noWrap="true" colspan="3">本期收入</th>
  			<th noWrap="true" colspan="3">本期发出</th>
  			<th noWrap="true" colspan="1">进销误差</th>
  			</xsl:if>
  			<th noWrap="true" colspan="3">期末余额</th>
      </tr>
      <tr noWrap="true" class="mainHead">  	
      	<xsl:if test="$disp != '1'">	
  			<th noWrap="true">单价</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">单价</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">单价</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">金额</th>
  			</xsl:if>
  			<th noWrap="true">单价</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">金额</th>
  	  </tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td[position() &lt; last()]">
	          <xsl:choose>
	          	
	          	<xsl:when test="position()=6 or position()=9 or position()=12 ">
	         	 		<xsl:if test="$disp != '1'">
			            <td align='right' class='moneyCol'>
			              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
			            </td>
	          		</xsl:if>
			    		</xsl:when>
	          	<xsl:when test=" position()=7 or position()=10 or position()=13 or position()=14 ">
	          		<xsl:if test="$disp != '1'">
			            <td align='right' class='moneyCol'>
			              <xsl:value-of select="format-number(.,'#,##0.00')"/>   
			            </td>
	          		</xsl:if>
			    		</xsl:when>
			    		<xsl:when test=" position()=5 or position()=8 or position()=11 ">
	          		
	          		<xsl:if test="$disp != '1' and ../td[1]!='合计'">
			            <td align='right' class='moneyCol'>
			              <xsl:value-of select="format-number(.,'#,##0.00')"/>    
			            </td>
	          		</xsl:if>
	          		<xsl:if test="$disp != '1' and ../td[1]='合计'">
									<td/>
								</xsl:if>
	          		
			    		</xsl:when>
			    		<xsl:when test="position()=15">
	            	<xsl:if test="../td[1]='合计'">
									<td/>
								</xsl:if>
								<xsl:if test="../td[1]!='合计'">
									<td align='right' class='moneyCol'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>   
		            	</td>
								</xsl:if>
		            
			    		</xsl:when>
	            <xsl:when test="position()=16">
									<td align='right' class='moneyCol'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
		            	</td>
			    		</xsl:when>
			    		<xsl:when test="position()=17">
		            <td align='right' class='moneyCol'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>