<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
	      	<tr noWrap='true' class='mainHead'>
			   		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>			   	
			   	</tr>
			   	<tr noWrap='true' class='mainHead'>
			   		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>			   	
			   	</tr>
			   	<tr noWrap='true' class='mainHead'>
			   		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>			   	
			   	</tr>
			 	  <tr noWrap='true' class='mainHead'>
		  	  	<td noWrap="true">仓库名称</td>
		  			<td noWrap="true">期初余额</td>
		  			<td noWrap="true">本期收入</td>
		  			<td noWrap="true">本期发出</td>
		  			<td noWrap="true">进销误差</td>
		  			<td noWrap="true">期末余额</td>       
         </tr>
			  </thead>	      
	      <tbody>
		      <xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<xsl:for-each select="td">          
		            <xsl:choose>            
		             	<xsl:when test="position()=1">
		           
			</xsl:when>

	          
	            <xsl:when test="  position()=3 or position()=4 or position()=5 or position()=6 or position()=7">
		            <td align='right' class='moneyCol'>
		              <!--xsl:value-of select="format-number(.,'#,##0.00')"/--> 
		              <xsl:value-of select="."/> 
		            </td>
		</xsl:when>  
		              <xsl:otherwise>
		              	<td class="numberText">
		              		<!--xsl:value-of select="format-number(.,'#,##0.00')"/-->
		              		<xsl:value-of select="."/>
		              	</td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
	      </tbody>
	   
	  </root>
	</xsl:template>
</xsl:stylesheet>