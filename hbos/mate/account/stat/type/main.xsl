<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">物资类别</th>
  			<th noWrap="true">期初余额</th>
  			<th noWrap="true">本期收入</th>
  			<th noWrap="true">本期发出</th>
  			<th noWrap="true">进销误差</th>
  			<th noWrap="true">期末余额</th>
      </tr>
     </thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>


		<xsl:when test="position()=1">
		           
		</xsl:when>

	            <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7">
		            <td align='right' class='moneyCol'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		              <!--xsl:value-of select="."/-->
		            </td>
		</xsl:when>


		 



			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>			                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>