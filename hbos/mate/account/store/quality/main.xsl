<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">是否代销</th>
  	  	<th noWrap="true">仓库名称</th>
  	  	<th noWrap="true">材料编码</th>
  			<th noWrap="true">材料名称</th>
  			<th noWrap="true">规格型号</th>
  			<th noWrap="true">计量单位</th>
  			<th noWrap="true">货位号</th>
  			<th noWrap="true">批号</th>
  			<th noWrap="true">条码</th>
  			<th noWrap="true">结存数量</th>
  			<th noWrap="true">失效日期</th>
  			<th noWrap="true">当前日期</th>
  			<th noWrap="true">天数</th>
  			<th noWrap="true">状态</th>
      </tr>
     </thead>
  	<tbody/> 
  </xsl:template>
</xsl:stylesheet>