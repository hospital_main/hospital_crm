<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>	    	      
	      <thead>
	      	<tr noWrap='true' class='mainHead'>
			   		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>	
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>		   
			   	</tr>
			   	<tr noWrap='true' class='mainHead'>
			   		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>	
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>
			   		<td style="display:none"/>		   
			   	</tr>
			 	  <tr noWrap='true' class='mainHead'>
			 	  	<td noWrap="true">是否代销</td>
		  	  	<td noWrap="true">仓库名称</td>
		  	  	<td noWrap="true">材料编码</td>
		  			<td noWrap="true">材料名称</td>
		  			<td noWrap="true">规格型号</td>
		  			<td noWrap="true">计量单位</td>
		  			<td noWrap="true">货位号</td>
		  			<td noWrap="true">批号</td>
		  			<td noWrap="true">条码</td>
		  			<td noWrap="true">结存数量</td>
		  			<td noWrap="true">失效日期</td>
		  			<td noWrap="true">当前日期</td>
		  			<td noWrap="true">天数</td>
		  			<td noWrap="true">状态</td>    
         </tr>
			  </thead>	      
	      <tbody>
		      <xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<xsl:for-each select="td">          
		            <xsl:choose>            
		              <xsl:when test="position()= 10">
		              	<td class="numberText">
		              		<xsl:value-of select="format-number(.,'#,##0.00')"/>
		              	</td>
		              </xsl:when> 
		              <xsl:when test="position()=13">
		              	<td class="Text">
		              		<xsl:value-of select="format-number(.,'#,##0')"/>
		              	</td>
		              </xsl:when>  
		              <xsl:otherwise>
		              	<td align="left">
		                	<xsl:value-of select="."/>
		                </td>
		              	
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
	      </tbody>
	    
	  </root>
	</xsl:template>
</xsl:stylesheet>