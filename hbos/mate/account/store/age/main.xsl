<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">材料编码</th>
  			<th noWrap="true">材料名称</th>
  			<th noWrap="true">规格型号</th>
  			<th noWrap="true">计量单位</th>
  			<th noWrap="true">货位号</th>
  			<th noWrap="true">批号</th>
  			<th noWrap="true">结存数量</th>
  	    <th noWrap="true">入库日期</th>
  	    <th noWrap="true">当前日期</th>
  	    <th noWrap="true">库龄</th>
  	  </tr>
    </thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=7">
                <td  align="right">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=10">
                <td  align="right">
                  <xsl:value-of select="format-number(.,'#,##0.')"/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
      	  </xsl:for-each>
      	</tr>
      </xsl:for-each>
    </tbody> 
  </xsl:template>
</xsl:stylesheet>