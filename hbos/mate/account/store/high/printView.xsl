<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td noWrap="true" style="fontsize:subtitle;colspan:4;align:left"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
			 	  <tr noWrap='true' class='mainHead'>
		  	  <td noWrap="true">材料编码</td>
	  			<td noWrap="true">材料名称</td>
	  			<td noWrap="true">规格型号</td>
	  			<td noWrap="true">计量单位</td>
	  			<td noWrap="true">结存数量</td>
	  			<td noWrap="true">高限库存量</td>
	  			<td noWrap="true">超储量</td>
         </tr>
			  </thead>	      
	      <tbody>
		      <xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<xsl:for-each select="td">          
		            <xsl:choose>            
		              <xsl:when test="position()= 5 or position()=6 or position()=7">
		              	<td class="numberText">
		              		<xsl:value-of select="format-number(.,'#,##0.00')"/>
		              	</td>
		              </xsl:when>  
		              <xsl:otherwise>
		              	<td align="left">
		                	<xsl:value-of select="."/>
		                </td>
		              	
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
	      </tbody>
	    
	  </root>
	</xsl:template>
</xsl:stylesheet>