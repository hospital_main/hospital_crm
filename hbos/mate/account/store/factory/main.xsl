<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">生产厂商编码</th>
  			<th noWrap="true">生产厂商名称</th>
  			<th noWrap="true">证件类型</th>
  			<th noWrap="true">证件名称</th>
  			<th noWrap="true">证件编号</th>
  			<th noWrap="true">截止日期</th>
  			<th noWrap="true">当前日期</th>
  			<th noWrap="true">天数</th>
  			<th noWrap="true">状态</th>
  	  </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=8 ">
                <td  align="right">
                  <xsl:value-of select="format-number(.,'#,##0')"/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
      	  </xsl:for-each>
      	</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>