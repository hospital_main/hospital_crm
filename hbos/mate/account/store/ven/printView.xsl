<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      		<td style="display:none"/>
	      	</tr>
			 	  <tr noWrap='true' class='mainHead'>
		  	  	<td noWrap="true">供应商编码</td>
		  			<td noWrap="true">供应商名称</td>
		  			<td noWrap="true">证件类型</td>
		  			<td noWrap="true">证件名称</td>
		  			<td noWrap="true">证件编号</td>
		  			<td noWrap="true">截止日期</td>
		  			<td noWrap="true">当前日期</td>
		  			<td noWrap="true">天数</td>
		  			<td noWrap="true">状态</td>
         </tr>
			  </thead>	      
	      <tbody>
		      <xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<xsl:for-each select="td">          
		            <xsl:choose>            
		              <xsl:when test="position()= 8">
		              	<td align="right">
		              		<xsl:value-of select="."/>
		              	</td>
		              </xsl:when>  
		              <xsl:otherwise>
		              	<td align="left">
		                	<xsl:value-of select="."/>
		                </td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
	      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>