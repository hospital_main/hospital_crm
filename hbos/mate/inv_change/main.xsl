<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th>材料编码</th>
				<th>材料名称</th>
				<th>规格型号</th>
				<th>单位</th>
				<th>注册证号</th>
				<th>生产厂商</th>
				<th>收费编码</th>
				<th>收费名称</th>
				<th style="display:none" >收费价格</th>
				<th>变动时间</th>
				<th>变动状态</th>
				<th>原价格</th>
				<th>原价格库存</th>
				<th>新价格</th>
				<th>处理状态</th>
				<th>处理时间</th>
				<th>处理人</th>
				
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                <xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:when test="position()=9">
                <td style="display:none" >
                <xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:when test="position()=12 or position()=14">
                <td align="right">
                		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              
              <xsl:when test="position()=13">
                <td align="right">
                		              <a>
                		              <xsl:attribute name="href">
                		              	javascript:window.inv_out_price_var='<xsl:value-of select="../td[12]"/>';window.inv_code_var='<xsl:value-of select="../td[1]"/>';openDialog('../whr/out/appaudit/main_in.html','dialogWidth:800px;dialogHeight:500px;');
                		              </xsl:attribute>
                		              <xsl:value-of select="format-number(.,'#,##0')"/>
                		              	
                		              </a>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
