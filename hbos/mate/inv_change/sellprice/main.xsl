<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>材料编码</th>
				<th>材料名称</th>
				<th>注册证号</th>
				<th>生产厂商</th>
				<th>入库单号</th>
				<th>原价格</th>
				<th>新价格</th>
				<th>变动时间</th>
				<th>确认人</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                <xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:when test="position()=6 or position()=7">
                <td align="right">
                <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>

              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
