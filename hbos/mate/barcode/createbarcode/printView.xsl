<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
    <thead>
      <tr noWrap="true" class="mainHead"> 
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
	  		<tr noWrap="true" class="mainHead">
		      <td>材料编码</td>
					<td>材料名称</td>
					<td>物资类别</td>
					<td>规格型号</td>
					<td>计量单位</td>
					<td>条形码</td>
	  		</tr>
	  	</thead>
	  	<tbody>
		     <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td >
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
                <xsl:when test="position()=6">
			            <td  style="barcode:CODE39">
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td>
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each> 
	  	</tbody>
	 </root> 	
	</xsl:template>
</xsl:stylesheet>