<?xml version='1.0' encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
  	<thead>
  	  <xsl:for-each select="/root/tbody/tr[1]">
  			<tr noWrap='true' class='mainHead'>
  				<th style='display:none2'><input type='checkbox'/></th>
          <xsl:for-each select="td">
	          		<th nowrap='true'><xsl:value-of select="."/></th>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		
    </thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr[position()!=1]">
	      <tr>
	        <td align='center'>
            <input type='checkbox' TABINDEX='-1'>
              <xsl:attribute name="value" >
               <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
	        <xsl:for-each select="td">
	        	<xsl:variable name="pos" select="position()"/>
	          <xsl:choose>
	            <xsl:when test="position() = 1">
			    <td><a href="#">
				<xsl:attribute name="onclick">
					openOrder("&lt;order_code&gt;<xsl:value-of select="../pk/order_code"/>&lt;/order_code&gt;&lt;date&gt;<xsl:value-of select="../td[2]"/>&lt;/date&gt;")
				</xsl:attribute>
				<xsl:value-of select="."/></a>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td><xsl:value-of select="."/></td>
	            </xsl:otherwise>		                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>