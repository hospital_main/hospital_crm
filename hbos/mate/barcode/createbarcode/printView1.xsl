<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
    <thead>
      <xsl:for-each select="/root/tbody/tr[1]">
      	<tr>
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:center"/>
      		<xsl:for-each select="td[position()>1]">
	          		<td nowrap='true' style="display:none"/>
          </xsl:for-each>
      	</tr>
      	<tr>
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<xsl:for-each select="td[position()>1]">
	          		<td nowrap='true' style="display:none"/>
          </xsl:for-each>
      	</tr>
  			<tr noWrap='true' class='mainHead'>
          <xsl:for-each select="td[position()>0]">
	          		<td nowrap='true'><xsl:value-of select="."/></td>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
	  	</thead>
	  	<tbody>
		     <xsl:for-each select="/root/tbody/tr[position()>1]">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td >
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
                <xsl:when test="position()=last()">
			            <td  style="barcode:CODE39">
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td>
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each> 
	  	</tbody>
	 </root> 	
	</xsl:template>
</xsl:stylesheet>