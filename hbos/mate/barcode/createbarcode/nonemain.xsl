<?xml version='1.0' encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
  	<thead>
  	
   		
    </thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr[position()>1]">
	      <tr>
	      	<xsl:if test="position()=1"><xsl:attribute name="id">firstTrId</xsl:attribute></xsl:if>
	        <td align='center'>
            <input type='checkbox' TABINDEX='-1'>
              <xsl:attribute name="value" >
                <xsl:for-each select="td[position()=2]"><xsl:value-of select="."/></xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
	        <xsl:for-each select="td[position()>1]">
	          <xsl:choose>
	            <xsl:when test=".='T'">
			    			<td tdtype="T">
			    				<input type='text'>
			    					<xsl:attribute name='value'>
			    					</xsl:attribute>
			    					<xsl:attribute name='onblur'>
			    						checkInput(this)
			    					</xsl:attribute>
			    				</input>
		            </td>
			    		</xsl:when>
			    		 <xsl:when test="starts-with(.,'D')">
	            <xsl:variable name="code" select="."></xsl:variable>
			    			<td tdtype="D" >
			    				<input class='inputSelectS' load='mate_barcode_createbarcode_barcode_select' name="column">
			    					<xsl:attribute name='para'>
			    					&lt;a&gt;<xsl:value-of select="."/>&lt;/a&gt;
			    					</xsl:attribute>
			    				</input>
		            </td>
			    		</xsl:when>
			    		<xsl:otherwise>
			          <td tdtype="P">
			          		<xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>		                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>