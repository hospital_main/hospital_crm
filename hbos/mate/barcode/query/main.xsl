<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">日期</th>
  	  	<th noWrap="true">单据类型</th>
  			<th noWrap="true">仓库名称</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">成本单价</th>
  	  	<th noWrap="true">成本金额</th>
  			<th noWrap="true">供应商</th>
  			<th noWrap="true">开嘱医生</th>
  			<th noWrap="true">病人姓名</th>
  	  	<th noWrap="true">住院号</th>
  			<th noWrap="true">资质证件状态</th>
  			<th noWrap="true">单据编号</th>
      </tr>
     </thead>
  	<tbody> 
  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	            <xsl:when test="position() =1">
			    			<td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		
			    		<xsl:when test=" position()=5">
          			<td align='right' class='moneyCol'>
                <xsl:if test="../td[1]='A'">
        	        <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
      	        </xsl:if> 
								  <xsl:value-of select="format-number(.,'#,##0.00')"/>  
          			</td>
	          	</xsl:when>
	          	<xsl:when test="position()=6">
          			<td align='right' class='moneyCol'>
                <xsl:if test="../td[1]='A'">
        	        <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
      	        </xsl:if> 
								  <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>  
          			</td>
	          	</xsl:when>
	          	<xsl:when test="position()=7">
          			<td align='right' class='moneyCol'>
                <xsl:if test="../td[1]='A'">
        	        <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
      	        </xsl:if> 
								  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
          			</td>
	          	</xsl:when>
	          	
	          	<xsl:when test=" position()=8">
          			<td align='left'>
          			  <xsl:if test="../td[1]='A'">
        	          <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
      	          </xsl:if>
          				<a href="#">
									  <xsl:attribute name="onclick" >
									    javascript:openDialog('veninfo.html?load=&lt;ven_code&gt;<xsl:value-of select="../td[15]"/>&lt;/ven_code&gt;','dialogWidth:500px;dialogHeight:520px',result)
									  </xsl:attribute>
									  <xsl:value-of select="."/>
									</a>
          			</td>
	          	</xsl:when>
	          	
	          	<xsl:when test=" position()=13">
          			<td>
            			  <xsl:if test="../td[1]='A'">
          	          <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
        	          </xsl:if> 
        	          
	          			  <xsl:if test="'0采购入库'=../td[3] or '1自制品入库'=../td[3] or '3有偿调入'=../td[3] or '4无偿调入'=../td[3]  or '9其他入库'=../td[3] or '11采购退货'=../td[3] or '20委托加工入库'=../td[3] or '23捐赠入库'=../td[3]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../whr/query/mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  		<xsl:if test="'2科室领用'=../td[3] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'5有偿调出'=../td[3] or '6无偿调出'=../td[3] or '10其他出库'=../td[3] or '12报废出库'=../td[3] or '15二级库盘点'=../td[3] or '17自制品原材料领用'=../td[3] or '22科室退库'=../td[3] or '24捐赠出库'=../td[3] or '31借库'=../td[3] or '32还库'=../td[3]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'14移出库'=../td[3] or '13移入库'=../td[3]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/move/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'39期初入库'=../td[3] and  0=../td[18] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/info/init/noconsignee/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'39期初入库'=../td[3] and  1=../td[18] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/info/init/consignee/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'18代销入库'=../td[3] or '41代销退货'=../td[3]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/in/instead/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'36代销出库'=../td[3] or '33代销借库'=../td[3] or '34代销还库'=../td[3] or '35代销退库'=../td[3]  " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/instead/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'37代销移入库'=../td[3] or '38代销移出库'=../td[3]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/insteadmove/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	
							  	<xsl:if test="'16医生医嘱'=../td[3] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../whr/query/mate/whr/second/advice/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'21专购品出库'=../td[3] or '21专购品入库'=../td[3] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../whr/query/mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'7盘盈入库'=../td[3] and  0=../td[18] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../whr/query/mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'7盘盈入库'=../td[3] and  1=../td[18] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/in/instead/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'8盘亏出库'=../td[3] and  0=../td[18] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'8盘亏出库'=../td[3] and  1=../td[18] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/out/instead/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'42期初未开票入库'=../td[3]  or '43期初未开票退货'=../td[3] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('mate/whr/in/initcommon/update.html?load=&lt;id&gt;<xsl:value-of select="../td[14]"/>&lt;/id&gt;','dialogWidth:1000px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	
	          		</td>
	          	</xsl:when>
			    		
			    		<xsl:when test="position()>13">
			    			<td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		
			    		<xsl:otherwise>
			          <td align='left'>
			            <xsl:if test="../td[1]='A'">
        	          <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
      	          </xsl:if> 
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>		                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
	    
	      
	      
   	</tbody>
  </xsl:template>
</xsl:stylesheet>