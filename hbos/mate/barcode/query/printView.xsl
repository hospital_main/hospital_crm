<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=""/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:center"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:7;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:5;align:left"/>
  	  	<td style="display:none"/>	  	
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:7;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>	  	
  	  	<td style="display:none"/>
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:5;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
				<tr noWrap="true" class="mainHead">
					<td noWrap="true">日期</td>
					<td noWrap="true">单据类型</td>
					<td noWrap="true">仓库名称</td>
					<td noWrap="true">数量</td>
					<td noWrap="true">成本单价</td>
					<td noWrap="true">成本金额</td>
					<td noWrap="true">供应商</td>
					<td noWrap="true">开嘱医生</td>
					<td noWrap="true">病人姓名</td>
					<td noWrap="true">住院号</td>
					<td noWrap="true">资质证件状态</td>
					<td noWrap="true">单据编号</td>
				</tr>
			</thead>
			<tbody>
			  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
        <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1 or position()=14 or position()=15 or position()=16 or position()=17 or position()=18">
								</xsl:when>
								<xsl:when test=" position()=5">
	          			<td align='right' class='numberText'>
	                <xsl:if test="../td[1]='A'">
	        	        <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
	      	        </xsl:if> 
									  <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	          			</td>
		          	</xsl:when>
		          	<xsl:when test="position()=6">
	          			<td align='right' class='numberText'>
	                <xsl:if test="../td[1]='A'">
	        	        <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
	      	        </xsl:if> 
									  <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>  
	          			</td>
		          	</xsl:when>
		          	<xsl:when test="position()=7">
	          			<td align='right' class='numberText'>
	                <xsl:if test="../td[1]='A'">
	        	        <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
	      	        </xsl:if> 
									  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	          			</td>
		          	</xsl:when>
								<xsl:otherwise>
									<td align="left">
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
