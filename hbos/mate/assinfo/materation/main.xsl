<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
        <th noWrap="true">年度</th>
  			<th noWrap="true">物资类别编码</th>
  			<th noWrap="true">物资类别名称</th>
  			<th noWrap="true">储备定额</th>
  			<th noWrap="true">年周转次</th>
  			<th noWrap="true">周转天</th>
       </tr>   		
    </thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">          
            <xsl:variable name="islast">
		          	  <xsl:value-of select ="../td[position()=2]"/>
		        </xsl:variable>
            <xsl:choose>
              <xsl:when test="position()=1">
              	<td nowrap="true">
              		<xsl:attribute name="value" >
                     <xsl:for-each select="../*">
                      <xsl:choose>
                        <xsl:when test="position()=1 or position()=2 or position()=3">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:when>
                      </xsl:choose>
                    </xsl:for-each>
            		  </xsl:attribute>
              		<xsl:value-of select="."/>
              	</td>
              </xsl:when>  
              <xsl:when test="position()=2">
              </xsl:when>  
              <xsl:when test="position()=5">
                <td nowrap="true" align="right">
                    <xsl:attribute name="value" >
                      <xsl:for-each select="../*">
                        <xsl:choose>
                          <xsl:when test="position()=5">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="format-number(.,'###0.00')"/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:when>
                        </xsl:choose>
                      </xsl:for-each>                    
              		  </xsl:attribute>
                		<xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
              </xsl:when>
              <xsl:when test="position()=6">
                <td nowrap="true" align="right">
                  <xsl:attribute name="value" >
                    <xsl:for-each select="../*">
                      <xsl:choose>
                        <xsl:when test="position()=6">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:when>
                      </xsl:choose>
                    </xsl:for-each>                    
            		  </xsl:attribute>
              		<xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=7">
                <td nowrap="true" align="right">
                  <xsl:attribute name="value" >
                    <xsl:for-each select="../*">
                      <xsl:choose>
                        <xsl:when test="position()=7">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:when>
                      </xsl:choose>
                    </xsl:for-each>                    
            		  </xsl:attribute>
              		<xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td nowrap="true">
              		<xsl:value-of select="."/>
              	</td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>