<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	  <thead>
	  	<tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
       <tr noWrap="true" class="mainHead">
       	<td>年度</td>
       	<td>物资类别编码</td>
       	<td>物资类别名称</td>
       	<td>储备定额</td>
       	<td>年周转次</td>
       	<td>周转天</td>
       </tr>
	  </thead>
	    <tbody>
        <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:for-each select="td">          
              <xsl:choose>
                <xsl:when test="position()=1">
                	<td nowrap="true">
                		<xsl:value-of select="."/>
                 	</td>
                </xsl:when>  
                <xsl:when test="position()=2">
                </xsl:when>  
                <xsl:when test="position()=5 or position()=6 or position()=7">
                  <td class="numberText">
                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td nowrap="true">
                		<xsl:value-of select="."/>
                 	</td>
                </xsl:otherwise>
              </xsl:choose>
    			  </xsl:for-each>
  			  </tr>
   		  </xsl:for-each>
  	  </tbody>
     </root>
	</xsl:template>
</xsl:stylesheet>