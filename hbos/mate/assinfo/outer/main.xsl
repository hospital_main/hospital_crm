<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=1">
	    			<tr noWrap='true' class='mainHead'>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=1 ">
			          		<th><xsl:value-of select="."/></th>
			          	</xsl:when>
			          	<xsl:otherwise>
		                <th nowrap='true'><xsl:value-of select="."/></th>
		              </xsl:otherwise>
	              </xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()>1">
	    			<tr>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=1 ">
			            	<td><xsl:value-of select="."/></td>
			            </xsl:when>
			          	<xsl:otherwise>
			          		<td ><xsl:value-of select="."/></td>
			          	</xsl:otherwise>
		          	</xsl:choose>
		          </xsl:for-each>      
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>