<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
	<th>供应商</th>
	<th>报告日期</th>
	<th>质量认证级别</th>
	<th>注册资金</th>
	<th>年营业额</th>
	<th>按时到货率</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	<tr>
	  <xsl:for-each select="td">
	    <xsl:choose>
	      <xsl:when test="position()=4">
	        <td align='right' class='moneyCol'>
	          <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	        </td>
	      </xsl:when>
	      <xsl:otherwise>
	        <td align='left'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:otherwise>			                        
	    </xsl:choose>
	  </xsl:for-each>
	</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>