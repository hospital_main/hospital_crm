<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		  		
      	</tr>
	  		<tr noWrap="true" class="mainHead">
					<td>材料编码</td>
					<td>材料名称</td>
					<td>物资类别</td>
				 	<td>规格型号</td>
				 	<td>拼音码</td>
				 	<td>参考单价</td>
				 	<td>主要供货单位</td>
				 	<td>生产厂商</td>
				 	<td>注册证号</td>
					<td>计量单位</td>
					<!--
			 		<td>安全库存</td>
					<td>最低库存</td>
					<td>最高库存</td>	
					-->				
					<td>是否收费</td>
					<td>是否高值</td>
					<td>是否耐用品</td>
					<td>是否本地</td>
					<td>是否国产</td>
					<td>是否挂网</td>
					<td>参与科室库管理</td>
					<td>物资财务分类</td>
				</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
                </xsl:when>
                 <xsl:when test="position()=2 or position()=3 or position()=11">
                 
                </xsl:when>
               
                <xsl:when test="position()=8">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:otherwise>
                  <td align='left'><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>