<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th>材料编码</th>
				<th>材料名称</th>
				<th>物资类别</th>
			 	<th>规格型号</th>
			 	<th>拼音码</th>
			 	<th>参考单价</th>
			 	<th>主要供货单位</th>
			 	<th>生产厂商</th>
			 	<th>注册证号</th>
				<th>计量单位</th>
				<th>是否收费</th>
				<th>是否高值</th>
				<th>是否耐用品</th>				
				<th>是否本地</th>
				<th>是否国产</th>
				<th>是否挂网</th>
				<th>参与科室库管理</th>
				<th>物资财务分类</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
                </xsl:when>
                 <xsl:when test="position()=2 or position()=3 or position()=11" >
                </xsl:when>
                <xsl:when test="  position()=8">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:otherwise>
                  <td align='left'><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
