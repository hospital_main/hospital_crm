<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>	  		    		
			<th noWrap="true" >仓库代码</th>
			<th noWrap="true">仓库名称</th>
			<th style='display:none' noWrap="true">是否代销</th>
			<th noWrap="true">是否对应</th>	
     	</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>     			
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td nowrap="true">
								<xsl:attribute name="value" >
									<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
								</xsl:attribute>
								<xsl:value-of select="."/>
							</td>								
						</xsl:when>
						<xsl:when test="position()=3">
							<td align='center' style='display:none' ><div>
		            <input type='checkbox' name="is_com">
		      			  <xsl:if test="text() = '1'">
		      			    <xsl:attribute name="checked">1</xsl:attribute>
		      			  </xsl:if>
		    			  </input></div>
		          </td>	
						</xsl:when>
						<xsl:when test="position()=4">
							<td align='center' >
		            <div><input type='checkbox' name = "is_value" >
		              <xsl:attribute name="value" ><xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
		      			  <xsl:if test="text() = '1'">
		      			    <xsl:attribute name="checked">1</xsl:attribute>
		      			  </xsl:if>
		    			  </input></div>
		          </td>	
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
							
			</tr>			
		</xsl:for-each> 		
	  </tbody> 	
 	</xsl:template>
</xsl:stylesheet>


