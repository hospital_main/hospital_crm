<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>        
			<th noWrap="true" >材料代码</th>
			<th noWrap="true">材料名称</th>
			<th noWrap="true">规格型号</th>
			<th noWrap="true">主要供应商</th>
			<th noWrap="true">最高库存</th>
			<th noWrap="true">库存基数</th>
			<th noWrap="true">最低库存</th>
     	</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>     			
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=1">
								<td nowrap="true">
									<xsl:attribute name="value" >
										<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
									</xsl:attribute>
									<xsl:value-of select="."/>
								</td>								
							</xsl:when>
							<xsl:when test="position()=5">
								<td><xsl:attribute name="value" >&lt;demo2&gt;<xsl:value-of select="."/>&lt;/demo2&gt;</xsl:attribute><xsl:value-of select="."/></td>
							</xsl:when>												
							<xsl:when test="position()=6 or position()=7 or position()=8">													
								<td class='numberText'><xsl:attribute name="value" >&lt;demo2&gt;<xsl:value-of select="."/>&lt;/demo2&gt;</xsl:attribute><xsl:value-of select="."/></td>
							</xsl:when>						
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
 	</xsl:template>
</xsl:stylesheet>


