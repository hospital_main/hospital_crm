<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/mate/info/file/inv/main.xsl,v 1.1 2012/03/12 01:54:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:54:35 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th>材料代码</th>
				<th>材料名称</th>
				<th>主要供应商</th>
				<th>货位</th>
				<th>最高库存 </th>
				<th>安全库存</th>
				<th>最低库存</th>
				<th><input type="checkbox" id="allcheckbox" onclick="setCheckAll(this)"/></th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=5 or position()=6 or position()=7">
                  <td align='right'><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:when test="position()=8">
                	<td align="center"><input type="checkbox"/></td>
              	</xsl:when>
                <xsl:when test="position()=9 or position()=10 or position()=11 or position()=12 or position()=13">
                  <td style="display:none"><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
