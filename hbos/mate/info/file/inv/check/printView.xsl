<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
    	<table>
      <thead>
      	<tr>
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr>
      		<td noWrap="true"  style="fontsize:maintitle;colspan:3;text-align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td noWrap="true"  style="fontsize:maintitle;colspan:2"/>
      		<td style="display:none"/>
      	</tr>      	
	  		<tr noWrap="true" class="mainHead">
					<td>材料编码</td>
					<td>材料名称</td>
					<td>规格型号</td>
				 	<td>主要供应商</td>
				 	<td>是否有权</td>
				</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
              <xsl:choose>
		              	
		             <xsl:when test="position()=5">
		             	<xsl:if test=".=1">
		             		<td>是</td>			
		             	</xsl:if>
		             	<xsl:if test=".=0">
		             		<td>否</td>			
		             	</xsl:if>
		             	  
										<td><xsl:value-of select="."/></td>						
								 
								 </xsl:when>
								 
                <xsl:when test="position()=6 or position()=7 or position()=8 or position()=9 or position()=10">
                  
                </xsl:when>
                
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
	  	</table>
    </root>
  </xsl:template>
</xsl:stylesheet>