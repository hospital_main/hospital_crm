<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/mate/info/file/factory/main.xsl,v 1.1 2012/03/12 01:54:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:54:35 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th>生产厂商代码</th>
				<th>生产厂商名称</th>				
				<th>联系电话</th>
				<th>联系人</th>
				<th>手机</th>
				<th>传真</th>
				<th>厂商地址</th>
				<th>是否物流管理</th>
				<th>是否固定资产</th>
				<th>是否无形资产</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <td >
              <xsl:choose>
                <xsl:when test="position()=1">
                  <a tabindex='-1'><xsl:value-of select="."/></a>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
