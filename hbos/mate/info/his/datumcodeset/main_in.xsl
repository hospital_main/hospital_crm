<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>制单日期</th>
				<th>仓库名称</th>
				<th>领料科室名称</th>
				<th>材料编码</th>
				<th>材料名称</th>
				<th>规格型号</th>
				<th>收费价格</th>
				<th>数量</th>
				<th>条形码</th>
				<th>病人姓名</th>
				<th>病人性别</th>
				<th>医生名称</th>
				<th>床位号</th>
				<th>住院号</th>
				
			</tr>
  	</thead>
  	<tbody>
  	
  		<xsl:for-each select="/root/tbody/tr">
  			<tr>
  				<!--td><input type="checkbox"/></td-->
	  			<xsl:for-each select="td">
	  						
	  					 	<td><xsl:value-of select="."/></td>
	  					
	  			</xsl:for-each>
  			</tr>
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>