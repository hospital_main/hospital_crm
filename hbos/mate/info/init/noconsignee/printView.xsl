<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
    <colgroup>           
      <col style = 'width:100'/>
      <col style = 'width:120'/>
      <col style = 'width:120'/>
      <col style = 'width:70'/>
      <col style = 'width:80'/>
      <col style = 'width:80'/>
      <col style = 'width:70'/>
      <col style = 'width:80'/>
      <col style = 'width:80'/>
      <col style = 'width:80'/>
      <col style = 'width:105'/>
      <col style = 'width:100'/>
      <col style = 'width:80'/>
    </colgroup>
    <thead>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <tr noWrap='true' class='mainHead'> 
      
        <th rowspan="2" valign="middle">材料代码</th>
        <th rowspan="2" valign="middle">材料名称</th>
        <th rowspan="2" valign="middle">规格型号</th>
        <th colspan="3">最终核算单元</th>
        <th style='display:none'/>
        <th style='display:none'/>        
        <th colspan="4">包装</th>
        <th style='display:none'/>
        <th style='display:none'/>
        <th style='display:none'/>
        <th rowspan="2" valign="middle">金额</th>
        <th rowspan="2" valign="middle">生产批号</th>
        <th rowspan="2" valign="middle">失效日期</th>
      </tr>
      <tr noWrap='true' class='mainHead'> 
        <th style='display:none'/>
        <th style='display:none'/>
        <th style='display:none'/>
        <th>计量单位</th>
        <th>数量</th>
        <th>单价</th>
        <th>计量单位</th>
        <th>拆包换算率</th>
        <th>件数</th>
        <th>单价</th>
        <th style='display:none'/>
        <th style='display:none'/>
        <th style='display:none'/>
      </tr>
    </thead>     
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">          
            <xsl:choose>            
              <xsl:when test="position()=2 or position()=15 or position()=16 or position()=17">  
              </xsl:when>                 
              <xsl:when test="position()=6  or position()=9 or position()=10 ">
	              <td  align="right" noWrap='true' >
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when> 
	            <xsl:when test=" position()=11 ">
	              <td  align="right" noWrap='true' >
                  <xsl:value-of select="format-number(.,'#,##0.0000')"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=7 ">
		           	<td class="numberText" >
	                <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
	              </td>
		          </xsl:when>
		          <xsl:when test="position()=12">
		           	<td class="numberText">
	                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
	              </td>
		          </xsl:when>  
              <xsl:otherwise>
                <td align="left">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
      <xsl:if test="count(/root/total/td)!=0" >      
        <xsl:for-each select="/root/total">
          <tr SumTitle="合计：">
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td>合计：</td>
                </xsl:when>
                <xsl:when test="position()=2 or position()=15 or position()=16 or position()=17">
                </xsl:when>
                <xsl:when test="position()=12">  
                  <td  class="numberText">
                    <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                  </td>
                </xsl:when>             
                <xsl:otherwise>
                  <td></td>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </tr>
        </xsl:for-each>
      </xsl:if> 
    </tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>