<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th noWrap="true">入库单号</th>
				<th noWrap="true">入库日期</th>
				<th noWrap="true">摘要</th>
				<th noWrap="true">仓库</th>
				<th noWrap="true">业务类型</th>
				<th noWrap="true">制单人</th>
				<th noWrap="true">库管员</th>
				<th noWrap="true">状态</th>
  		</tr>
  	</thead>
  	<tbody>
 		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        		 <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
    	</td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                <a tabindex='-1'><xsl:value-of select="."/></a>
                </td>
              </xsl:when>
     
              <!--如果状态是未审核，改变状态颜色为红色-->
              	<xsl:when test="position()=8">
								<xsl:if test="../td[8]='未审核'">
									<td style='Color:red'>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="../td[8]!='未审核'">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:when>
              
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>