<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		  <tr noWrap='true' class='mainHead'> 
		    <th rowspan="2" valign="middle" width='25'><input type='checkbox'/></th>
		    <th rowspan="2" valign="middle">材料代码</th>
		    <th rowspan="2" valign="middle">材料名称</th>
		    <th rowspan="2" valign="middle">规格型号</th>
		    <th colspan="3">最终核算单元</th>
		    <th colspan="4">包装</th>
		    <th rowspan="2" valign="middle">金额</th>
		    <th rowspan="2" valign="middle">生产批号</th>
		    <th rowspan="2" valign="middle">失效日期</th>
		    <th rowspan="2" valign="middle">条形码</th>
		  </tr>
		  <tr noWrap='true' class='mainHead'> 
		    <th>计量单位</th>
		    <th>数量</th>
		    <th>单价</th>
		    <th>计量单位</th>
		    <th>拆包换算率</th>
		    <th>件数</th>
		    <th>单价</th>
		  </tr>
		</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td noWrap='true'>
                  <a href='#'>
                    <xsl:attribute name="onclick">openDialog('dupdate.html','dialogWidth:400px;dialogHeight:500px', this);</xsl:attribute>
                    <xsl:value-of select="."/>
                  </a>
                </td>
              </xsl:when>
	            <xsl:when test="position()=2 or position()=15 or position()=16 or position()=17">	 
	            </xsl:when>                 
	            <xsl:when test="position()=6  or position()=9 or position()=10 ">
	              <td  align="right" noWrap='true' >
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when> 
	            <xsl:when test=" position()=7 or position()=11 or position()=12">
	              <td  align="right" noWrap='true' >
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>              
              <xsl:otherwise>
                <td noWrap='true'>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
        <xsl:if test="count(/root/total/td)!=0" >      
		      <xsl:for-each select="/root/total">
		      	<tr>
		          <td align='center'></td>
		          <xsl:for-each select="td">
		            <xsl:choose>
		              <xsl:when test="position()=1">
		                <td>合计：</td>
		              </xsl:when>
			            <xsl:when test="position()=2 or position()=15 or position()=16 or position()=17">
			            </xsl:when>
			            <xsl:when test="position()=12">	 
			              <td  align="right">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
			            </xsl:when>	            
		              <xsl:otherwise>
		                <td></td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		        </tr>
		      </xsl:for-each>
      	</xsl:if> 
    </tbody>
  </xsl:template>
</xsl:stylesheet>


