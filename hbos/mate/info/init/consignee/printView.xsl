<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	    <thead>
	     <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
      	<td style="display:none"/>     	
      	<td noWrap="true" style="fontsize:subtitle;colspan:4;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td noWrap="true" style="fontsize:subtitle;colspan:4;align:right"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
      	<td style="display:none"/>    	
      	<td noWrap="true" style="fontsize:subtitle;colspan:8;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      </tr>
		  <tr noWrap='true' class='mainHead'> 
   	    <td >材料代码</td>
		    <td >材料名称</td>
		    <td >规格型号</td>
		    <td>计量单位</td>
		    <td>数量</td>
		    <td>单价</td>
		    <td>件数</td>
		    <td >金额</td>		    
		    <td >生产批号</td>
		    <td >失效日期</td>
      </tr>
    		</thead>     
	      <tbody>
		      <xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<xsl:for-each select="td">          
		            <xsl:choose>            
		               <xsl:when test="position()=2 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17">	 
	                 </xsl:when>                 
	                 <xsl:when test="position()=6 or position()=8">
	              <td align="right" class="moneyCol" noWrap="true">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>
	            <xsl:when test=" position()=7 or position()=9 ">
	              <td align="right" class="moneyCol" noWrap="true">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=17">
	              <td align="right" class="moneyCol" noWrap="true">
                  <xsl:value-of select="."/>
                </td>
	            </xsl:when>
              <xsl:otherwise>
                <td noWrap="true">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
  		   	<xsl:if test="count(/root/total/td)!=0" >      
  		      <xsl:for-each select="/root/total">
  		      	<tr SumTitle="合计：">
  		          <xsl:for-each select="td">
  		            <xsl:choose>
  		              <xsl:when test="position()=1">
  		                <td>合计：</td>
  		              </xsl:when>
  			               <xsl:when test="position()=2 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17">	  
	                  </xsl:when> 
			              <xsl:when test="position()=9">	 
  			              <td  class="numberText">
  		                  <xsl:value-of select="format-number(.,'#,##0.0000')"/>
  		                </td>
  			            </xsl:when>	            
  		              <xsl:otherwise>
  		                <td></td>
  		              </xsl:otherwise>
  		            </xsl:choose>
  		  			  </xsl:for-each>
  		        </tr>
  		      </xsl:for-each>
        	</xsl:if> 
	      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>