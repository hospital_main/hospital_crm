<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
			<th style=""><input type="checkbox" style='display:none'/></th>
			<th noWrap="true" >物资分类编码</th>
			<th noWrap="true">物资分类名称</th>
			<th noWrap="true">财务分类名称</th>
     		</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'  style='display:none'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
    	    </xsl:attribute>
  	  </input>
				</td>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td nowrap="true">
								<xsl:attribute name="value" >
									<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
								</xsl:attribute>
								<xsl:value-of select="."/>
							</td>
						</xsl:when>

						<xsl:when test="position()=3">
							<td nowrap="true">
								
								<xsl:if test=" . = ''">
		            		　
		            					</xsl:if>
		            		
											<xsl:attribute name="ondblclick">
												clearSubj();
											</xsl:attribute>
		            		
								<xsl:if test=". != ''">

								<xsl:value-of select="."/>

								</xsl:if>
								 <a href='#' onclick="setFinaTypeCode(this)">
									设置
								 </a>

							</td>
						</xsl:when>



						


						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>

			</tr>
		</xsl:for-each>
	  </tbody>
 	</xsl:template>
</xsl:stylesheet>
