<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<!--<th>供应商编码</th>-->
				<th>供应商名称</th>
				<th>证件类型</th>
  			<th>证件名称</th>
			<th>状态</th>
  			<th>启始日期</th>
  			<th>有效期</th>
  			<th>截止日期</th>
  			<th>证件编号</th>
				<th>证件备注</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:variable name="ven_code" select="td[1]" />
      	<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$ven_code])"/>
      	<xsl:variable name="cur_pos" select="position()" />
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=1">
            	</xsl:when>
            	<xsl:when test="position()=2">
								<xsl:if test="$cur_pos = 1 ">
									<td nowrap="true" rowspan="{$rowspan}">
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="$cur_pos >1 and $ven_code != ../../tr[$cur_pos - 1]/td[1]">
									<td nowrap="true" rowspan="{$rowspan}">
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="$cur_pos >1 and $ven_code = ../../tr[$cur_pos - 1]/td[1]">
									<td style="display:none"><xsl:value-of select="."/>
								  </td>
								</xsl:if>			
							</xsl:when>
							<xsl:when test="position()=7">
								<td align="right"><xsl:value-of select="."/></td>
            	</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>