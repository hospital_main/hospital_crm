<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
  
      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>

      </tr>
      <tr noWrap="true" class="mainHead">
      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>
      	<td style="display:none"/>

      </tr>
        <tr noWrap="true" class="mainHead"> 
       	
			  	<td>证件编号</td>
				  <td>证件类别</td>
				  <td>供应商</td> 
				  <td>发证机关</td>
				  <td>发证日期</td>
				  <td>起始日期</td>
				  <td>截止日期</td>
				  <td>申请科室</td>
				  <td>状态</td>
				  <td>是否停用</td>
			  </tr>
       </thead>
	  	<tbody> 
	  	  <xsl:for-each select="/root/tbody/tr">
		      <tr>
		        <xsl:for-each select="td">
		          <xsl:choose>
		            <xsl:when test="position()=1">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
	        	    <xsl:otherwise>
									<td>
										<xsl:value-of select="."/>
									</td>
		            </xsl:otherwise>			                        
		          </xsl:choose>
		        </xsl:for-each>
		      </tr>
		    </xsl:for-each>
	   	</tbody>
	  </root>
  </xsl:template>
</xsl:stylesheet>



