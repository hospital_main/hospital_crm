<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
        <th noWrap="true" >材料编码</th>
				<th noWrap="true" >物资名称</th>
				<th noWrap="true" >规格型号</th>
				<th noWrap="true" >证件编号</th>
				<th noWrap="true" >证件名称</th>
				<th noWrap="true" >是否收费</th>
				<th noWrap="true" >申请科室</th>
				<th noWrap="true" >起始日期</th>
				<th noWrap="true" >有效期(月)</th>
				<th noWrap="true" >截止日期</th>
				<th noWrap="true" >参考价格</th>
				<th noWrap="true" >存档位置</th>
				<th noWrap="true" >生产企业</th>
				<th noWrap="true" >物资品牌</th>
				<th noWrap="true" >供应商</th>
				<th noWrap="true" >企业地址</th>
				<th noWrap="true" >联系人</th>
				<th noWrap="true" >联系电话</th>
				<th noWrap="true" >手机</th>
				<th noWrap="true" >状态</th>
      </tr>                
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center' style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      	      </xsl:attribute>
    	    	</input>
          </td>
          <xsl:for-each select="td">
            <td >
              <xsl:choose>
                <xsl:when test="position()=2">
                  <a tabindex='-1'><xsl:value-of select="."/></a>
                </xsl:when>
                <xsl:when test="position()=11">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
		  	  </xsl:for-each>
		  	</tr>
      </xsl:for-each>  	
    </tbody>
  </xsl:template>
</xsl:stylesheet>
