<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
        <th style="display:block"></th>
	<th noWrap="true" >物资材料编码</th>
	<th noWrap="true" >物资材料名称</th>

      </tr>                
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center' style='display:block'>
            <input type='radio' name='select' onclick='chose(this)'>
              <xsl:attribute name="value" ><xsl:value-of select="./td[1]"/></xsl:attribute>
              <xsl:attribute name="text" ><xsl:value-of select="./td[2]"/></xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <td >
             
                <xsl:value-of select="."/>
            
            </td>
  	  </xsl:for-each>
  	</tr>
      </xsl:for-each>  	
    </tbody>
  </xsl:template>
</xsl:stylesheet>
