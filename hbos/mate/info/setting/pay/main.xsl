<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/mate/info/setting/pay/main.xsl,v 1.1 2012/03/12 01:54:46 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:54:46 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th>付款条件编码</th>
				<th>付款条件表示</th>
				<th>信用天数</th>
				<th>优惠天数1</th>
				<th>优惠率1</th>
				<th>优惠天数2</th>
				<th>优惠率2</th>
				<th>优惠天数3</th>
				<th>优惠率3</th>
                <th>优惠天数4</th>
				<th>优惠率4</th>				
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
              </xsl:when>
              <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9">
      	        <td align='right'>
      	          <xsl:value-of select="."/>  
      	        </td>
      	      </xsl:when>                                                
      	      <xsl:otherwise>        	        
      	         <td><xsl:value-of select="."/></td>        	        
      	      </xsl:otherwise>       		      	                        	
            </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
