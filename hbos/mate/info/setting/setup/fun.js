function submitData(obj){
	var res=checkInputs();
	if(res==null||res=="")
		return ;
	if(value_030171=="1"&&value_030184=="0"){
		alert("参数030171为采用时，参数030184不能为否，反之亦然。");
		return false;
	}
	window.xmlhttp.post(obj.name, res,"")
	var str = window.xmlhttp._object.responseText
	if (!window.doMsg(str,"")) {
	  return false;
	}
	//sysDictsUnitinfoCopyParas_select.click();
}
var value_030171="";
var value_030184="";
function checkInputs(){
  var para0311Value = "";
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	var res="";
	for(var i=0;i<trs.length;i++){
		if(trs[i].getAttribute("_editParaType")==null)
			continue;
  		 if(trs[i].childNodes[0].innerText == "030184"){
  		 	 value_030184=trs[i]._editInputValue
  		 }else if(trs[i].childNodes[0].innerText == "030171"){
  		 	 value_030171=trs[i]._editInputValue
  		 }
  		 if(trs[i]._editDataValue==trs[i]._editInputValue)
  			 continue;
    		res+="<record>";
    		
   		  if(checkValue(trs[i],trs[i]._editDataType,trs[i]._editInputValue)){
  			  res+=trs[i]._editPk+"<value>"+trs[i]._editInputValue+"</value>";
    		}
		res+="</record>";
	}
	return res;
}
function checkValue(tr,type,value){
	var msg=tr.cells[0].innerText+" "+tr.cells[1].innerText+" :要求是" ;
	var res=true;
	if(type=="1"&&value!=""){
		msg+=" 文本 ";
		res=true;
	}else if(type=="2"&&IsDate(value)){
		msg+=" 日期 ";
		var d= CDate(value);
		var year=d.getFullYear();
		var month=d.getMonth()+1;
		var day=d.getDate();
		year='0000'+year;year=year.substring(year.length-4)
	  	month='00'+month;month=month.substring(month.length-2)
	  	day='00'+day; day=day.substring(day.length-2)
	 	tr._editInputValue=year+'-'+month+'-'+day;
		res=true;
	}else  if(type=="3"){
		msg+=" 编码规则 ";
		res=isRuleCode(value);
	}else if(type=="4"){
		msg+=" 正整数 ";
		var v=parseInt(value,10);
		if(isNaN(v)||v<0)
			res=false;
	}else if(type=="5"){
		var v=parseInt(value,10);
		if(isNaN(v))
			res=false;
	}
	if(res==false)
		alert(msg);
	return res;
}
function initInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	for(var i=0;i<trs.length;i++)
		initTrInputs(trs[i]);
}
function initTrInputs(tr){
	var pt=tr.getAttribute("_editParaType");
	var dt=tr.getAttribute("_editDataType");
	var dv=tr.getAttribute("_editDataValue");
	var pc=tr.getAttribute("_paraCode");
	var pv=tr.getAttribute("_paraValue");
	
	if(pt==null||dt==null)
		return ;
	var inp="";
	var id=getJsGuid();
	var tag="";
	var chk="";
	tr._editInputId=id;
	if(pt=="1"){//1:该参数为不可见参数 
		inp="<input type='text' style='display:none' id='"+id+"' name='"+id+"' value='"+dv+"'/>";
		tag="input";
	}else if(pt=="2"){//2：该参数为可参不可改的参数(以灰色文本框显示) 
		inp=inp+"<input type='text' readonly=true style='background-color:#EEEEEE;border:1px solid #808080;width:110px'   id='"+id+"' name='"+id+"' value='"+tr.cells[2].innerHTML+"'/>";
	}else if(pt=="3"){//3：可见可以改(文本框)
		inp=inp+"<input type='text'    style='border:1px solid #808080;width:110px' id='"+id+"' name='"+id+"' value='"+tr.cells[2].innerHTML+"'/>";
	}else if(pt=="4"){// 4：可见可以改(下拉框) 
		var ops=tr.cells[3].innerText.split("；");
		if (dt==1){
			inp="<select id='"+id+"' style='width:110px'  name='"+id+"'>";
		}
		if (dt==0){
			inp="<select id='"+id+"' style='width:110px' disabled= 'true' name='"+id+"'>";
		}
		if (tr.cells[0].innerHTML=='030189'){
			inp="<select id='"+id+"' style='width:110px' disabled= 'true' name='"+id+"'>";
		}
		tag="select"
		for(var i=0;i<ops.length;i++){
			var temp = ops[i].split("：");
			if(dv==temp[0])
				chk=" selected ";
			else
				chk="";
   		inp+="<option "+chk+" value='"+temp[0]+"'>"+ops[i]+"</option>";
		}
		inp+="</select>";
	}else if(pt=="5"){//5：可见可以改(单选框)。 
		var ops=tr.cells[3].innerText.split("\/");
		inp="";
		tag="input"
		for(var i=0;i<ops.length;i++){
			if(dv==ops[i])
				chk=" checked ";
			else
				chk="";
			inp+="<input type='radio' "+chk+" name='"+id+"' value='"+ops[i]+"'/>"+ops[i];
			if(i+1!=ops.length)
				inp+="<br/>";
		}
	}
	else
		return ;
	tr.cells[2].innerHTML=inp;
	 
	var inpus=tr.cells[2].getElementsByTagName("input");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	inpus=tr.cells[2].getElementsByTagName("select");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	 
}
function setInputChange(tr,inp){
	inp.onblur=
	inp.onclick=
	inp.onkeyup=function(){tr._editInputValue=this.value};
}