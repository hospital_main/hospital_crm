<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <table>
  	<thead>
  		<tr noWrap="true" class="mainHead" >
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead" >
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead" >
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
  		</tr>
  		
  		<tr noWrap='true' class='mainHead'>        
					<td noWrap="true" >证件编号</td>
					<td noWrap="true">证件类别</td>
					<td noWrap="true">供应商</td>
					<td noWrap="true">发证机关</td>
					<td noWrap="true">发证日期</td>
					<td noWrap="true">种类范围</td>
					<td noWrap="true">开始时间</td>
					<td noWrap="true">结束时间</td>
					<td noWrap="true">审核人</td>
					<td noWrap="true">状态</td>
					<td noWrap="true">是否停用</td>	            
     </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>     			
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
              	<td nowrap="true">
              		<xsl:attribute name="value" >
                     <xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
            		  </xsl:attribute>
              		<xsl:value-of select="."/>
              	</td>
								
              </xsl:when>

              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</table>	
 	</xsl:template>
</xsl:stylesheet>


