<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
  			<th><input type="checkbox"/></th>
				<th>证件编号</th>
				<th>证件类别</th>
				<th>供应商</th>
  			<th>发证机关</th>
  			<th>发证日期</th>
  			<th>种类范围</th>
  			<th>开始日期</th>
  			<th>结束日期</th>
  			<th>审核人</th>
				<th>状态</th>
				<th>是否停用</th>
  		</tr>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>    			
					<td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
					<xsl:for-each select="td">
						
						<xsl:choose>
						<xsl:when test="position()=1">
                <td><a><xsl:attribute name="href">
										javascript:openDialog('update1.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:900px;dialogHeight:500px;overflow:auto;', result)
									</xsl:attribute><xsl:value-of select="."/>
									</a></td>
              </xsl:when> 	   
              <xsl:otherwise>
								<td><xsl:value-of select="."/></td>
						  </xsl:otherwise>
						 	</xsl:choose> 
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 		
	  </tbody> 	
	</xsl:template>
</xsl:stylesheet>