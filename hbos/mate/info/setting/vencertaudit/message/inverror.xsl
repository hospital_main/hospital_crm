<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th noWrap="true">材料编码</th>
				<th noWrap="true">材料名称(库存项名称)</th>
				<th noWrap="true">别名(医嘱项名称)</th>
				<th noWrap="true">单位</th>
				<th noWrap="true">采购价格</th>
				<th noWrap="true">收费价格</th>
				<th noWrap="true">费用子类</th>
				<th noWrap="true">医嘱子类</th>
				<th noWrap="true">是否收费</th>
				<th noWrap="true">同步状态</th>
				<th noWrap="true">错误信息</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=5 or position()=6">
                <td>
                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=22">
                <td>
                 <xsl:value-of select="format-number(.,'#,##0')"/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>