<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>证件类型</th>
				<th>起始日期</th>
				<th>有效期</th>
				<th>结束日期</th>
				<th>证件号码</th>
				<th>证件名称</th>
				<th>备注</th>
				<th>状态</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=4">
                <td><xsl:value-of select="."/></td>
              </xsl:when> 
              <xsl:when test="position()=6">
              	<td><a href="#">
									  <xsl:attribute name="onclick">
										loadFile('<xsl:value-of select="../td[8]"/>');
									  </xsl:attribute><xsl:value-of select="."/>
									</a></td>
              </xsl:when>
              <xsl:when test="position()=7">
              </xsl:when> 
              <xsl:when test="position()=8">
              </xsl:when> 
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>