<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=""/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<!--<td style="display:none"/>-->
      	</tr>
				<tr noWrap="true" class="mainHead">
					<td>采购计划号</td>
					<td>需求计划号</td>
					<td>计划类别</td>
					<td>汇总部门</td>
					<td>汇总日期</td>
					<td>制单人</td>
					<td>状态</td>
					<td style="display:none">执行金额</td>
				</tr>
			</thead>
			<tbody>
	      <xsl:for-each select="/root/tbody/tr">
					<tr>
					  <xsl:for-each select="td">
					    <xsl:choose>
					    	<xsl:when test=" position()=8">
					      </xsl:when>
					      <xsl:when test=" position()=9">
					        <!--<td align='right'>
					          <xsl:value-of select="format-number(.,'#,##0.00')"/>
					        </td>-->
					      </xsl:when>
					      <xsl:otherwise>
					        <td align='left'>
					          <xsl:value-of select="."/>
					        </td>
					      </xsl:otherwise>			                        
				    </xsl:choose>
					  </xsl:for-each>
					</tr>
	      </xsl:for-each>
	    </tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>