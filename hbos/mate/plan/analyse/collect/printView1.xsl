<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=""/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:4;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td noWrap="true" style="fontsize:subtitle;colspan:5;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
				<tr noWrap="true" class="mainHead">
					<td>材料代码</td>
					<td>材料名称</td>
					<td>规格型号</td>
					<td>单位</td>
					<td>当前库存</td>
					<td>采购数量</td>
					<td>计划单价</td>
					<td>金额</td>
					<td>计划到货日期</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=6 or position()=11 or position()=12 or position()=13 or position()=14"/>
								
								<xsl:when test="position()=8 or position()=9">
									<td class="numberText">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </td>
								</xsl:when>
								<xsl:otherwise>
									<td align="left">
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
				<!--xsl:if test="count(/root/tbody/tr)!=0">
					<tr>
						<td>合计：</td>
						<td/>
						<td/>
						<td>
							<xsl:value-of select="format-number(sum(/root/tbody/tr/td[9]),'#,##0.00')"/>
						</td>
						<td/>
						<td>
							<xsl:value-of select="format-number(sum(/root/tbody/tr/td[11]),'#,##0.00')"/>
						</td>
						<td/>
						<td/>
						<td/>
						<td/>
					</tr>
				</xsl:if-->
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
