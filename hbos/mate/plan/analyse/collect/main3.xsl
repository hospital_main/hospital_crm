<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
				<th>需求计划号</th>
				<th>需求科室</th>
				<th>材料代码</th>
				<th>材料名称</th>
				<th>规格型号</th>
				<th>计量单位</th>
				<th>计划数量</th>
				<th>计划单价</th>
				<th>计划金额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				    <xsl:choose>
				      <xsl:when test="position()=7 or position()=8 or position()=9">
				        <td align='right' class='numberText'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
		            </td>
				      </xsl:when>
				      <xsl:otherwise>
				        <td align='left'>
				          <xsl:value-of select="."/>
				        </td>
				      </xsl:otherwise>			                        
				    </xsl:choose>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>