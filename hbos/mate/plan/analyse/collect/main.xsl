<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
				<th>采购计划号</th>
				<th>需求计划号</th>
				<th>计划类别</th>
				<th>汇总部门</th>
				<th>汇总日期</th>
				<th>制单人</th>
				<th>状态</th>
				<th>查看汇总明细</th>
				<th style='display:none'>执行金额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				    <xsl:choose>
				    	<xsl:when test="position()=1">
				        <td>
				        	<a tabindex='-1'>
	                  <xsl:attribute name="href" >
	    	              javascript:openDialog('main1.html?load=&lt;req_id&gt;<xsl:value-of select="../td[1]"/>&lt;/req_id&gt;', 'dialogWidth:900px;dialogHeight:700px', result)
	                  </xsl:attribute>
	                  <xsl:value-of select="."/>
	                </a>
				        </td>
				      </xsl:when>
				      <xsl:when test="position()=2">
				        <td>
				          <a tabindex='-1'>
	                  <xsl:attribute name="href" >
	    	              javascript:openDialog('main2.html?load=&lt;req_id&gt;<xsl:value-of select="../td[2]"/>&lt;/req_id&gt;&lt;req_type&gt;<xsl:value-of select="../td[3]"/>&lt;/req_type&gt;&lt;maker&gt;<xsl:value-of select="../td[6]"/>&lt;/maker&gt;', 'dialogWidth:700px;dialogHeight:500px', result)
	                  </xsl:attribute>
	                  <xsl:value-of select="."/>
	                </a>
				        </td>
				      </xsl:when>
				      <xsl:when test="position()=8">
				        <td>
				        	<a tabindex='-1'>
	                  <xsl:attribute name="href" >
	    	              javascript:openDialog('main3.html?load=&lt;req_id&gt;<xsl:value-of select="../td[2]"/>&lt;/req_id&gt;&lt;req_type&gt;<xsl:value-of select="../td[3]"/>&lt;/req_type&gt;', 'dialogWidth:700px;dialogHeight:500px', result)
	                  </xsl:attribute>查看明细
	                </a>				          
				        </td>
				      </xsl:when>
				      <xsl:when test=" position()=9 ">
				        <!--<td align='right'>
				          <xsl:value-of select="format-number(.,'#,##0.00')"/> 
				        </td>-->
				      </xsl:when>
				      <xsl:otherwise>
				        <td align='left'>
				          <xsl:value-of select="."/>
				        </td>
				      </xsl:otherwise>			                        
				    </xsl:choose>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>