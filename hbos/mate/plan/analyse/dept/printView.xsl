<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      		<td style="display:none"/><td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
	      		<td style="display:none"/><td style="display:none"/>
	      	</tr>
	      	<tr noWrap="true" class="mainHead">
	      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	      		<td style="display:none"/><td style="display:none"/>
	      	</tr>
	 	  	<tr noWrap='true' class='mainHead'>  	  
	        <td>科室编码</td>
	  			<td>科室名称</td>
	  			<td>计划金额</td>
	  		</tr> 		      
      </thead>	      
      <tbody>
	      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				    <xsl:choose>
				      <xsl:when test="position()=4">
				        <td align='right'>
				          <xsl:value-of select="format-number(.,'#,##0.00')"/>
									<xsl:if test="../td[6]=0 and ../td[1]!='合计'">
				          	(其中本科室：<xsl:value-of select="format-number(../td[5],'#,##0.00')"/>)
				          </xsl:if>
				        </td>
				      </xsl:when>
				      <xsl:when test="position()=1 or position()=5 or position()=6">
				        
				      </xsl:when>
				      <xsl:otherwise>
				        <td align='left'>
				          <xsl:value-of select="."/>
				        </td>
				      </xsl:otherwise>			                        
				    </xsl:choose>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>