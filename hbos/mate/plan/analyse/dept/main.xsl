<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>科室编码</th>				
				<th>科室名称</th>
				<th>计划金额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				    <xsl:choose>
				      <xsl:when test="position()=4">
				        <td align='left'>
				          <xsl:value-of select="format-number(.,'#,##0.00')"/>
									<xsl:if test="../td[6]=0 and ../td[1]!='合计'">
				          	(其中本科室：<xsl:value-of select="format-number(../td[5],'#,##0.00')"/>)
				          </xsl:if>
				        </td>
				      </xsl:when>
				      <xsl:when test="position()=1 or position()=5 or position()=6">
				        
				      </xsl:when>
				      <xsl:otherwise>
				        <td align='left'>
				          <xsl:value-of select="."/>
				        </td>
				      </xsl:otherwise>			                        
				    </xsl:choose>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>