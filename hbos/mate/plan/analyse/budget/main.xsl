<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
      	<th rowspan="2" valign="middle">物资类别</th>
      	<th colspan="3">本期</th>
      	<th colspan="3">累计</th>
      </tr>
      <tr noWrap="true" class="mainHead"> 
      	<th>预算</th>
      	<th>采购计划执行</th>
      	<th>未执行</th>
      	<th>预算</th>
      	<th>采购计划执行</th>
      	<th>未执行</th>
      </tr>  		
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<tr>
      	  <xsl:for-each select="td">
      	    <xsl:choose>
      	      <xsl:when test="position()=2 or position()=3 or position()=4 or position()=5 or position()=6 or position()=7">
      	        <td align='right' class='moneyCol'>
      	          <xsl:value-of select="format-number(.,'#,##0.00')"/>  
      	        </td>
      	      </xsl:when>
      	      <xsl:otherwise>
      	        <td align='left'>
      	          <xsl:value-of select="."/>
      	        </td>
      	      </xsl:otherwise>			                        
      	    </xsl:choose>
      	  </xsl:for-each>
      	</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

