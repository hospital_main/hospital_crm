<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:5;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td noWrap="true" style="fontsize:subtitle;colspan:7;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		
      	</tr>
	      <tr noWrap="true" class="mainHead">
	        <td>需求计划号</td>				
					<td>需求科室</td>
					<td>材料代码</td>				
					<td>材料名称</td>
					<td>规格型号</td>
					<td>单位</td>
					<td>计划数量</td>
					<td>执行数量</td>
					<td>计划单价</td>				
					<td>计划金额</td>
					<td>执行金额</td>
					<td>状态</td>
	      </tr>
	    </thead>
	    <tbody>
	      <xsl:for-each select="/root/tbody/tr">
					<tr>
					  <xsl:for-each select="td">
					    <xsl:choose>
					      <xsl:when test="position()=7 or position()=8 or position()=9 or position()=10 or position()=11">
					        <td align='right' class="numberText">
					          <xsl:value-of select="format-number(.,'#,##0.00')"/>  
					        </td>
					      </xsl:when>
					      <xsl:otherwise>
					        <td align='left'>
					          <xsl:value-of select="."/>
					        </td>
					      </xsl:otherwise>			                        
					    </xsl:choose>
					  </xsl:for-each>
					</tr>
	      </xsl:for-each>
	    </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>