<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap="true" class="mainHead">
					<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr noWrap="true" class="mainHead">
					<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr noWrap="true" class="mainHead">
					<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td noWrap="true" >采购计划号</td>
					<td noWrap="true" >编制科室</td>
					<td noWrap="true" >材料编码</td>
					<td noWrap="true" >材料名称</td>
					<td noWrap="true" >规格型号</td>
					<td noWrap="true" >计量单位</td>
					<td noWrap="true" >采购数量</td>
					<td noWrap="true" >科室需求计划号</td>
					<td noWrap="true" >需求科室</td>	
					<td noWrap="true" >需求日期</td>
					<td noWrap="true" >需求数量</td>
					<td noWrap="true" >计划单价</td>
					<td noWrap="true" >金额</td>  			
				</tr>
			</thead>
			<tbody>
				<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
	    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:variable name="unit_name" select="td[1]" />
					<xsl:variable name="kpi" select="td[3]" />
					<xsl:variable name="cur_pos" select="position()" />					
					<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$unit_name])" />
					<xsl:variable name="rowspan2" select="count(/root/tbody/tr[td[3]+td[1] = $kpi+$unit_name ])" />
					<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1 or position()=2">
								<xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos - 1]/td[1]">
									<td rowspan="{$rowspan}">
										<xsl:value-of select="." />
									</td>
								</xsl:if>
								<xsl:if test="$unit_name = ../../tr[$cur_pos - 1]/td[1]">
									<td style="display:none"><xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 ">
								<td>
									<xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos - 1]/td[1] or $kpi != ../../tr[$cur_pos - 1]/td[3]">
										<xsl:attribute name="rowspan">
											<xsl:value-of select="$rowspan2" />
										</xsl:attribute>
									</xsl:if>
									<xsl:if test="$unit_name = ../../tr[$cur_pos - 1]/td[1] and $kpi = ../../tr[$cur_pos - 1]/td[3]">
										<xsl:attribute name="style">display:none</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="." />
		            </td>
							</xsl:when>
							<xsl:when test="position()=7 ">
								<td >
									<xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos - 1]/td[1] or $kpi != ../../tr[$cur_pos - 1]/td[3]">
										<xsl:attribute name="rowspan">
											<xsl:value-of select="$rowspan2" />
										</xsl:attribute>
									</xsl:if>
									<xsl:if test="$unit_name = ../../tr[$cur_pos - 1]/td[1] and $kpi = ../../tr[$cur_pos - 1]/td[3]">
										<xsl:attribute name="style">display:none</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
							</xsl:when>
							<xsl:when test="position()=11 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
			    		</xsl:when>
							<xsl:when test="position()=12 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position()=13 ">
		            <td align='right'>
		              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
		            </td>
			    		</xsl:when>			
							<xsl:otherwise>
								<td>
		            	<xsl:value-of select="."/>
		            </td>
							</xsl:otherwise>
						</xsl:choose>            
					</xsl:for-each>
				</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>