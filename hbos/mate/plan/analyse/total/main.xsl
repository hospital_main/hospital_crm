<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<thead>
  		<xsl:for-each select="/root/tbody/tr">
  			<xsl:variable name="rowindex" select="position()"/>
  			<xsl:variable name="indexvalue" select="/root/tbody/tr[$rowindex]/td[4]"/>
	    	<xsl:choose>
	    		<xsl:when test=" $indexvalue > 0 and $indexvalue!=99 ">
	    			<tr noWrap='true' class='mainHead'>
	    				<xsl:for-each select="td">
		          	<xsl:variable name="colindex" select="position()"/>
		          	<xsl:variable name="col_name" select="."/>
		          	<xsl:variable name="colNums" select="count(/root/tbody/tr[ $rowindex ]/td[.= $col_name]) "/>
		          	<xsl:variable name="rowNums" select="count(/root/tbody/tr[ td[$colindex ] = $col_name ])"/>
		          	<xsl:choose>
		          		<xsl:when test=" $indexvalue=1 and $colindex!=4">
		          			<th noWrap='true' valign="middle">
		          				<xsl:if test="position() &lt; 4">
				          			<xsl:attribute name="rowspan" >
				        					<xsl:value-of select="$rowNums + 1"/>
					            		</xsl:attribute>
				            		</xsl:if>
				            		<xsl:if test="position() &gt; 3">
				          			<xsl:attribute name="rowspan" >
				        					<xsl:value-of select="$rowNums"/>
					            		</xsl:attribute>
				            		</xsl:if>
				            	<xsl:if test=" $colindex=1 ">
				            	</xsl:if>
				            	<xsl:if test=" $colindex!=1 ">
					            	<xsl:if test=" $col_name!= ../td[$colindex - 1 ]">
					          			<xsl:attribute name="colspan" >
					        					<xsl:value-of select="$colNums"/>
						            	</xsl:attribute>
					            	</xsl:if>
					            	<xsl:if test=" $col_name= ../td[$colindex - 1 ]">
					          			<xsl:attribute name="style" >
					        					display:none
						            	</xsl:attribute>
					            	</xsl:if>
					            </xsl:if>
				            	<xsl:value-of select="."/>
				            </th>
			            </xsl:when>
		          		<xsl:when test=" $indexvalue>1 and $colindex!=4 ">
		          			<th noWrap='true' valign="middle">
			          			<xsl:if test=" $col_name!= ../../tr[$rowindex - 1]/td[$colindex ]">
				          			<xsl:attribute name="rowspan" >
				        					<xsl:value-of select="$rowNums"/>
					            	</xsl:attribute>
				            	</xsl:if>
				            	<xsl:if test=" $col_name = ../../tr[$rowindex - 1]/td[$colindex ]">
				            		<xsl:attribute name="style" >
				        					display:none
					            	</xsl:attribute>
				            	</xsl:if>
				            	<xsl:if test=" $col_name!= ../td[$colindex - 1 ]">
				          			<xsl:attribute name="colspan" >
				        					<xsl:value-of select="$colNums"/>
					            	</xsl:attribute>
				            	</xsl:if>
				            	<xsl:if test=" $col_name= ../td[$colindex - 1 ]">
				          			<xsl:attribute name="style" >
				        					display:none
					            	</xsl:attribute>
				            	</xsl:if>
				            	<xsl:value-of select="."/>
				            </th>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
   		<tr noWrap='true' class='mainHead'>
	    				<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 3 and position() != last()]">
	    					<th noWrap='true' valign="middle">
	    						<xsl:if test="( position() mod 2 ) = 1">
	    							需求金额
	    						</xsl:if>
	    						<xsl:if test="( position() mod 2 ) = 0">
	    							执行金额
	    						</xsl:if>
	    					</th>
		         		</xsl:for-each>
		  			</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:variable name="browindex" select="position()"/>
  			<xsl:variable name="bindexvalue" select="/root/tbody/tr[$browindex]/td[4]"/>
	    	<xsl:choose>
	    		<xsl:when test=" $bindexvalue =0 or $bindexvalue=99 ">
	    			<tr>
		          <xsl:for-each select="td">
		          	<xsl:choose>
	          			<xsl:when test=" position() &lt; 3 ">
			            	<td><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test=" position()=3 ">
			            	<td align="right"><xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/></td>
			            </xsl:when>
			          	<xsl:when test="position() >4">
			            	<td align="right" ><xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>