<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>材料代码</th>				
				<th>材料名称</th>
				<th>规格型号</th>
				<th>单价</th>
				<th>计划采购数量</th>
				<th>实际到货数量</th>
				<th>未到货数量</th>
  		</tr>
  	</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				    <xsl:choose>
				      <xsl:when test="position()=4 or position()=5 or position()=6 or position()=7">
				        <td align='right'>
				          <xsl:value-of select="format-number(.,'#,##0.00')"/>  
				        </td>
				      </xsl:when>
				      <xsl:otherwise>
				        <td align='left'>
				          <xsl:value-of select="."/>
				        </td>
				      </xsl:otherwise>			                        
				    </xsl:choose>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
      <tr>
	         <td>合计：</td>
	         <td></td>
	         <td></td>
	         <td></td>
	           <td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00')"/></td>
	            <td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/></td>
	             <td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/></td>
	      </tr>
    </tbody>
	</xsl:template>
</xsl:stylesheet>
