<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
	      <tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
		 	  	<tr noWrap="true" class="mainHead">
    				<td>物资类别</td>				
    				<td>采购计划金额</td>
    				<td>采购计划执行金额</td>
    				<td>未执行计划金额</td>
    				<td>执行百分比</td>
      		</tr>    
	      </thead>	      
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				    <xsl:choose>
				      <xsl:when test=" position() = 1">
          			<td>
          				<a href="#">
									  <xsl:attribute name="onclick" >
									    javascript:openDialog('detail/main.html?load=&lt;mate_type_code&gt;<xsl:value-of select="../td[6]"/>&lt;/mate_type_code&gt;&lt;mate_type_name&gt;<xsl:value-of select="../td[1]"/>&lt;/mate_type_name&gt;','dialogWidth:850px;dialogHeight:650px',result)
									  </xsl:attribute>
									  <xsl:value-of select="."/>  
									</a>
          			</td>
	          	</xsl:when>
				      <xsl:when test="position()= 2 or position() = 3 or position() = 4">
				        <td align='right' class='moneyCol'>
				          <xsl:value-of select="format-number(.,'#,##0.00')"/>  
				        </td>
				      </xsl:when>
				      <xsl:when test="position()= 5 ">
				        <td align='right' class='moneyCol'>
				          <xsl:value-of select="format-number(.,'#,##0.00%')"/>  
				        </td>
				      </xsl:when>
				      <xsl:when test="position()>5">
			    			<td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
				      <xsl:otherwise>
				        <td align='left'>
				          <xsl:value-of select="."/>
				        </td>
				      </xsl:otherwise>			                        
				    </xsl:choose>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
      <tr>
	         <td>合计：</td>
	          <td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[2]),'#,##0.00')"/></td>
	           <td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),'#,##0.00')"/></td>
	            <td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[4]),'#,##0.00')"/></td>
	             <td align='right' class='moneyCol'><xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00%')"/> </td>
	      </tr>
    </tbody>
	    
	  </root>
	</xsl:template>
</xsl:stylesheet>