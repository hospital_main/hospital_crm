<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWarp="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style='display:none'/>
      		<td style='display:none'/>
      		<td style='display:none'/>
      		<td style='display:none'/>
		<td style='display:none'/>
      	</tr>
      	<tr noWarp="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<td style='display:none'/>
      		<td style='display:none'/>
      		<td style='display:none'/>
      		<td style='display:none'/>
		<td style='display:none'/>
      	</tr>
      	<tr noWarp="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
      		<td style='display:none'/>      		
      		<td style='display:none'/>
      		<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
      		<td style='display:none'/>
		<td style='display:none'/>
      	</tr>
      	<tr noWarp="true" class="mainHead">
      		<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
      		<td style='display:none'/>
      		<td style='display:none'/>
      		<td style='display:none'/>
      		<td style='display:none'/>
		<td style='display:none'/>
      	</tr>
        <tr noWrap='true' class='mainHead'>  	  
          <td>材料代码</td>
          <td>材料名称</td>
          <td>报价</td>
          <td>报价日期</td>
          <td>供应商</td>
	  <td>是否停用</td>
	</tr> 		      
      </thead>	      
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
	  <tr>
            <xsl:for-each select="td">          
	      <xsl:choose>            
		<xsl:when test="position()=3">
      	          <td align='right' class='numberText'>
      	            <xsl:value-of select="format-number(.,'#,##0.00')"/>  
      	          </td>
      	        </xsl:when>
		<xsl:otherwise>
		  <td align="left">
		    <xsl:value-of select="."/>
		  </td>
		</xsl:otherwise>
	      </xsl:choose>
            </xsl:for-each>
	  </tr>
        </xsl:for-each>
      </tbody>
      <tfoot>
				<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
      		<td style="display:none"/>
      		<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
      		<td style="display:none"/>      		      
      		<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
		<td style='display:none'/>
      	</tr>
			</tfoot>
    </root>
  </xsl:template>
</xsl:stylesheet>