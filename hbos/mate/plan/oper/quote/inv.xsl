<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap="true" class="mainHead">
			  <th width='40'>选择</th>
  			<th>编码</th>
  			<th>名称</th>
  			<th>型号</th>
  			<th>单位</th>
  			<th>生产厂商</th>
  			<th>供应商</th>
    	</tr>
  	</thead>
  	<tbody>
			<xsl:variable name="code" select="/root/@code"/>
	    <xsl:variable name="name" select="/root/@name"/>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:for-each select="td">
          	<xsl:choose>
        			<xsl:when test="position()=1">
	        			<td>
	            		<input type='radio' name='select' onclick='chose(this)'>
		              	<xsl:attribute name="value" ><xsl:value-of select="../td[2]"/></xsl:attribute>
		              	<xsl:attribute name="text" ><xsl:value-of select="../td[3]"/></xsl:attribute>
		              	<xsl:value-of select="."/>
	            		</input>
	            	</td>
            	</xsl:when>
            	<xsl:when test="position()=2">
            		<td><xsl:value-of select="."/></td>
            	</xsl:when>
            	<xsl:otherwise>
            		<td><xsl:value-of select="."/></td>
            	</xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
