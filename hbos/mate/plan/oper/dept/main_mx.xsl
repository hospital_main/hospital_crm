<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
				<th noWrap="true">材料编码</th>
				<th noWrap="true">材料名称</th>
				<th noWrap="true">规格型号</th>
				<th noWrap="true">单位</th>
				<th noWrap="true" >计划数量</th>
				<th noWrap="true" >单价</th>
				<th noWrap="true" >金额</th>
				<th noWrap="true" >需求日期</th>
				<th noWrap="true">备注</th>
				<th noWrap="true">需求计划单号</th>
				<th noWrap="true">编制部门</th>
				<th noWrap="true">摘要</th>
				<th noWrap="true">编制人</th>
				<th noWrap="true">编制日期</th>
				<th noWrap="true">审核人</th>
				<th noWrap="true">审核日期</th>
				<th noWrap="true">状态</th>
				<th noWrap="true">采购计划号</th>
				<th noWrap="true">执行数量</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=6">
                <td align="right">
									<xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=7">
                <td align="right">
									<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=5 or position()=19">
                <td align="right">
                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>