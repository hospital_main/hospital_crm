<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
      <th style="display:none"><input type="checkbox" /></th>
				<th>材料编码</th>
				<th>材料名称</th>
				<th>规格型号</th>
				<th>计量单位</th>
				<th>上期计划量</th>
				<th>上期耗用量</th>
				<th>本期已报数量</th>
				<th>单价</th>
				<th>存放仓库名称</th>
				<!--th>备注</th-->
  		</tr>
  	</thead>
  	<tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        <td align='center'  style='display:none'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	          <xsl:attribute name="value" >
	            <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	  	      </xsl:attribute>
    	    </input>
        </td>
        <xsl:for-each select="td">  
        	<xsl:if test="position() != 5 and position() != 6 and position() != 7 and position() != 8 and position() != 9  and position() != 11 and position() != 12">      	
	          <td><xsl:value-of select="."/></td>
          </xsl:if>
          <xsl:if test="position() = 6 or position() = 7 or position() = 8 or position() = 9">       	
	          <td align='right'><xsl:value-of select="."/></td>
          </xsl:if>
          <xsl:if test="position() = 12 ">       	
	          <td style="display:none" ><xsl:value-of select="."/></td>
          </xsl:if>
  			</xsl:for-each>
      </tr>
    </xsl:for-each>
	</tbody>  	
	</xsl:template>
</xsl:stylesheet>