<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <colgroup>		       
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:150'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
      </colgroup>
      <thead>
        <tr noWrap='true' class='mainHead'>  	  
          <th noWrap='true' >材料代码</th>
				<th noWrap='true' >材料名称</th>
				<th noWrap='true' >规格型号</th>
				<th noWrap='true' >单位</th>
				<th noWrap='true' >数量</th>
				<th noWrap='true' >需求日期</th>
				</tr> 		      
      </thead>	      
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
	  		<tr>
        	<xsl:for-each select="td">          
	      		<xsl:choose>            
							<xsl:when test="position()= 1">
		  					<td align="left" noWrap='true' >
		    					<xsl:value-of select="."/>
		  					</td>
							</xsl:when>
							<xsl:when test="position()= 5 or position()= 6 or position()= 7">
							</xsl:when> 
							<xsl:when test="position()= 10 or position()= 8 or position()= 9 ">
		  					<td align="left" noWrap='true' class="numberText" >
		  						
		    					<xsl:value-of select="."/>
		  					</td>
							</xsl:when>  
							<xsl:otherwise>
		  					<td align="left" noWrap='true' >
		    					<xsl:value-of select="."/>
		  					</td>
							</xsl:otherwise>
	      		</xsl:choose>
          </xsl:for-each>
	  		</tr>
        </xsl:for-each>
      </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>