<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
		  <tr noWrap='true' class='mainHead'> 
		  	<th width='25'><!--<input type='checkbox'/>--></th>
		    <th width='15%' >材料编码</th>
		    <th>材料名称</th>
		    <th>规格型号</th>
		    <th>计量单位</th>
		    <th>期初库存</th>
		    <th>当前库存</th>
		    <th>需求数量</th>
		    <th>差量</th>
		    <th>单价</th>
		    <th>金额</th>
		  </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="pos" select="position()"/>
      	<xsl:if test="td[11] = 0">
        <tr>
        	<xsl:attribute name="id">c_<xsl:value-of select="td[1]"/></xsl:attribute>
        	<xsl:attribute name="row"><xsl:value-of select="$pos"/></xsl:attribute>
          <td align='center'>
            <!--<input type='checkbox'><xsl:attribute name="code"><xsl:value-of select="td[1]"/></xsl:attribute></input>-->
          </td>
          <xsl:for-each select="td[position() != last()]">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td noWrap='true' >
                   <b><xsl:value-of select="."/></b><a href="#" style="text-decoration: none;">
                   	<xsl:attribute name="onclick">doImg(img_<xsl:value-of select="."/>)</xsl:attribute>
                   	　<img src="down.gif" border="0" d="down" style="cursor:hand">
                   	<xsl:attribute name="id">img_<xsl:value-of select="."/></xsl:attribute><xsl:attribute name="code"><xsl:value-of select="."/></xsl:attribute></img>　</a>
                </td>
              </xsl:when>
              <xsl:when test=" position()=9 ">
	              <td  align="right">
                  <b><xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/></b>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=10">
	              <td  align="right">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>
	            
	       			<xsl:when test="position() &gt; 4 and position() &lt;9">
								<td  class="numberText">
	               <b> <xsl:value-of select="format-number(.,'#,##0.00')"/></b>
	              </td>
							</xsl:when>
							     
              <xsl:otherwise>
                <td noWrap='true'>
                  <b><xsl:value-of select="."/></b>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  	</xsl:if>
  	<xsl:if test="td[11] = 1">
  		
  		<xsl:if test=" $pos =2 or (td[1] = ../tr[$pos - 1]/td[1] and td[1] != ../tr[$pos - 2]/td[1])">
	        	<xsl:text disable-output-escaping="yes">&lt;tr style="display:none" id="tr_</xsl:text><xsl:value-of select="td[1]"/><xsl:text disable-output-escaping="yes">"&gt;
	        	&lt;td&gt;&lt;/td&gt;&lt;td colspan="10"&gt; &lt;table width='80%' style="border:0px solid black;border-collapse:collapse;" &gt;</xsl:text>
	        	<tr>
	        		<td class="smallTd">计划号</td>
	        		<td class="smallTd">申请部门</td>
	        		<td class="smallTd">需求数量</td>
	        		<td class="smallTd">执行数量</td>
	        		<td class="smallTd">单价</td>
	        		<td class="smallTd">金额</td>
	        	</tr>
		</xsl:if>
			<tr>
				<td class="smallTd2" align="center"><a href="#"><xsl:attribute name="onclick">openItem("<xsl:value-of select="td[1]"/>","<xsl:value-of select="td[2]"/>")</xsl:attribute><xsl:value-of select="td[2]"/></a></td>
				<td class="smallTd2" align="center"><xsl:value-of select="td[3]"/></td>
				<td class="smallTd2" align="center"><xsl:value-of select="format-number(td[7],'#,##0.00')"/></td>
				<td class="smallTd2" align="center" edit="true" width="160">
					<xsl:attribute name="code"><xsl:value-of select="td[1]"/></xsl:attribute>
					<xsl:attribute name="row"><xsl:value-of select="$pos"/></xsl:attribute>
					<xsl:value-of select="td[7]"/>
				</td>
				<td class="smallTd2" align="center" width="160"><xsl:value-of select="format-number(td[9],'#,##0.00')"/></td>
				<td class="smallTd2" align="center" width="160"><xsl:value-of select="format-number(td[10],'#,##0.00')"/></td>
			</tr>
		<xsl:if test="td[1] != ../tr[$pos + 1]/td[1]">
	        	<xsl:text disable-output-escaping="yes">
				&lt;/table&gt;&lt;/td&gt;&lt;/tr&gt;
			</xsl:text>
		</xsl:if>
        </xsl:if>
      </xsl:for-each>  
    </tbody>
  </xsl:template>
</xsl:stylesheet>


