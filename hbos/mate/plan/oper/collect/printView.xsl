<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
    <thead>  
    	<tr noWrap="true" class="mainHead">
    		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    	</tr>
    	<tr noWrap="true" class="mainHead">
    		<td noWray="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    	</tr>
      <tr noWrap='true' class='mainHead'> 
        <td noWrap='true' valign="middle">材料编码</td>
        <td noWrap='true' valign="middle">材料名称</td>
        <td noWrap='true' valign="middle">规格型号</td>
        <td noWrap='true' valign="middle">单位</td>
        <td noWrap='true' valign="middle">期初库存</td>
        <td noWrap='true' valign="middle">当前库存</td>
        <td noWrap='true'>需求数量</td>
        <td noWrap='true'>差量</td>
        <td noWrap='true' valign="middle">单价</td>
        <td noWrap='true' valign="middle">金额</td>
      </tr>
    </thead>     
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="td1" select="td[1]"/>
  			<xsl:variable name="curPos" select="position()"/>
  			
       	<xsl:if test="position() = 1" >
	        <tr>
	          <xsl:for-each select="td">          
	            <xsl:choose>            
	              <xsl:when test=" position()=9 ">
		              <td  class="numberText">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	                </td>
		            </xsl:when>
		            <xsl:when test="position()=10">  
		              <td  class="numberText">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </td>
		            </xsl:when>
		            
		       			<xsl:when test="position() &gt; 4 and position() &lt;9">
		              <td  class="numberText">
		                <xsl:value-of select="format-number(.,'#,##0.00')"/>
		              </td>
								</xsl:when>     
	              <xsl:otherwise>
	                <td align="left">
	                  <xsl:value-of select="."/>
	                </td>
	              </xsl:otherwise>
	            </xsl:choose>
	          </xsl:for-each>
        	</tr>
        </xsl:if>
        
       	<xsl:if test="position() > 1" >
       	<xsl:if test="$td1 != ../tr[$curPos -1]/td[1]">
	        <tr>
	          <xsl:for-each select="td">          
	            <xsl:choose>            
	              <xsl:when test=" position()=9 ">
		              <td  class="numberText">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	                </td>
		            </xsl:when>
		            <xsl:when test="position()=10">  
		              <td  class="numberText">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </td>
		            </xsl:when>
		            
		       			<xsl:when test="position() &gt; 4 and position() &lt;9">
		              <td  class="numberText">
		                <xsl:value-of select="format-number(.,'#,##0.00')"/>
		              </td>
								</xsl:when>     
	              <xsl:otherwise>
	                <td align="left">
	                  <xsl:value-of select="."/>
	                </td>
	              </xsl:otherwise>
	            </xsl:choose>
	          </xsl:for-each>
        	</tr>
        </xsl:if>
        </xsl:if>
        
      </xsl:for-each>   
    </tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>