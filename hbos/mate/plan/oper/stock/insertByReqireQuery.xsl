<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th width='25'><input type='checkbox'/></th>
        <th>材料代码</th>
				<th>材料名称</th>
				<th>规格型号</th>
				<th>单位</th>
				<th>数量</th>
				<th>单价</th>
				<th>金额</th>
				<th>需求日期</th>
				<th style="display:none">隐藏单价</th>				
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=5 or position()=6 or position()=7">
	              <td align="right">
                  <xsl:value-of select="."/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=9">
	              <td style="display:none" align="right">
                  <xsl:value-of select="."/>
                </td>
	            </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


