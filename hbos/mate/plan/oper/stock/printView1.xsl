<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=""/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style="width:100"/>
				<col style="width:100"/>
				<col style="width:100"/>
				<col style="width:100"/>
				<col style="width:90"/>
				<col style="width:100"/>
				<col style="width:90"/>
				<col style="width:100"/>
				<col style="width:90"/>
				<col style="width:100"/>
				<col style="width:90"/>
				<col style="width:100"/>
			</colgroup>
			<thead>
				<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  			<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
				<tr noWrap="true" class="mainHead">
					<th>材料代码</th>
					<th>材料名称</th>
					<th>规格型号</th>
					<th>单位</th>
					<th>当前库存</th>
					<th>科室需求数量</th>
					<th>采购数量</th>
					<th>计划单价</th>
					<th>金额</th>
					<th>计划到货日期</th>
					<th>生产厂商</th>
					<th>供应商</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=2 or position()=3 or position()=13 or position()=14  or position()>16 ">
								</xsl:when>	
															
								<xsl:when test="position()=7 or position()=8 or position()=9  ">
									<td class="numberText">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </td>
								</xsl:when>
								<xsl:when test="position()=10 ">
			           	<td class="numberText" >
		                <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
		              </td>
			          </xsl:when>
			          <xsl:when test="position()=11">
			           	<td class="numberText">
		                <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
		              </td>
			          </xsl:when>
								<xsl:otherwise>
									<td align="left">
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
				<xsl:if test="count(/root/tbody/tr)!=0">
					<tr>
						<td>合计：</td>
						<td/>
						<td/>
						<td/>
						<td/>
						<td/>
						<td/>
						<td/>
						
						<td class="numberText">
							<xsl:value-of select="format-number(sum(/root/tbody/tr/td[11]),$VHMONEYFORMAT)"/>
						</td>
						<td/>
						<td/>
						<td/>
						<td/>
					</tr>
				</xsl:if>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
