<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<tr noWrap="true" class="mainHead">
				<th>科室编码</th>
		    <th>科室名称</th>
				<th>材料编码</th>
		    <th>材料名称</th>
		    <th>规格型号</th>
		    <th>计量单位</th>
		    <th>上期数量</th>
		    <th>已通过数量</th>
		    <th>需求数量</th>
		    <th>单价</th>
		    <th>金额</th>
			</tr>
  	</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="pos" select="position()" />
				<xsl:variable name="dept_name" select="td[1]" />
				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$dept_name])" />

        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1 or position() = 2">
								<xsl:if test="$pos = 1 or $dept_name != ../../tr[$pos - 1]/td[1]">
									<td rowspan="{$rowspan}">
										<xsl:value-of select="." />
									</td>
								</xsl:if>
              </xsl:when>
              
	       			<xsl:when test="position() =7 ">
								<td  class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
							</xsl:when>
	       			<xsl:when test="position() =8 ">
								<td  class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
							</xsl:when>
	       			<xsl:when test="position() =9 ">
								<td  class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
							</xsl:when>
              <xsl:when test=" position()=11 ">
	              <td  align="right">
                  <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=10">
	              <td  align="right">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>
              <xsl:when test=" position()=11 ">
	              <td  align="right">
                  <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
                </td>
	            </xsl:when>
              <xsl:otherwise>
                <td noWrap='true'>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>  
    </tbody>
  </xsl:template>
</xsl:stylesheet>


