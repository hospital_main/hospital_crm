<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  		<tr noWrap="true" class="mainHead">
				<th>材料编码</th>
		    <th>材料名称</th>
		    <th>规格型号</th>
		    <th>计量单位</th>
		    <th>需求数量</th>
		    <th>单价</th>
		    <th>金额</th>
			</tr>
  	</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                	<a tabindex='-1'>
	                	<xsl:attribute name="href" >
	    	              javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:700px;overflow:auto;', result)
	                  </xsl:attribute>
	            			<xsl:value-of select="."/>
	            		</a>
	            	</td>
              </xsl:when>
              <xsl:when test=" position()=6 ">
	              <td  align="right">
                  <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=7">
	              <td  align="right">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>
	       			<xsl:when test="position() =5 ">
								<td  class="numberText">
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
							</xsl:when>
              <xsl:otherwise>
                <td noWrap='true'>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>  
    </tbody>
  </xsl:template>
</xsl:stylesheet>


