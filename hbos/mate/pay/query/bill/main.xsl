<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
      	<th>序号</th>
      	<th>单据号</th>
      	<th>单据类型</th>
      	<th>供应商</th>
      	<th>制单日期</th>
      	<th>入库日期</th>
      	<th>金额</th>
      	<th>仓库</th>
      	<th>状态</th>
      </tr>  		
    </thead>
    <tbody>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				    <xsl:choose>
				      <xsl:when test="position()=7">
				        <td align='right' class='moneyCol'>
				          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
				        </td>
				      </xsl:when>
				      <xsl:otherwise>
				        <td align='left'>
				          <xsl:value-of select="."/>
				        </td>
				      </xsl:otherwise>			                        
				    </xsl:choose>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

