<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
	  <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      </tr>
	      <tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      </tr>
	 	  	<tr>
		 	  	<td>序号</td>
	      	<td>单据号</td>
	      	<td>单据类型</td>
	      	<td>供应商</td>
	      	<td>制单日期</td>
	      	<td>入库日期</td>
	      	<td>金额</td>
	      	<td>仓库</td>
	      	<td>状态</td>
        </tr>
      </thead>
	    <tbody>
	    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
	    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	      <xsl:for-each select="/root/tbody/tr">
					<tr>
						
					  <xsl:for-each select="td">
					    <xsl:choose>
					    	
					      <xsl:when test="position()=7">
					        <td align='right'>
					          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
					        </td>
					      </xsl:when>
					      <xsl:otherwise>
					        <td align='left'>
					          <xsl:value-of select="."/>
					        </td>
					      </xsl:otherwise>			                        
					    </xsl:choose>
					  </xsl:for-each>
					</tr>
	      </xsl:for-each>
	      
	    </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>