<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      </tr>
	      <tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      </tr>
	 	  	<tr>
		 	  	<td>序号</td>
	      	<td>供应商编码</td>
	      	<td>供应商名称</td>
	      	<td>本月计划付款额</td>
        </tr>
      </thead>
	    <tbody>
	    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
	    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	      <xsl:for-each select="/root/tbody/tr">
					<tr>
						<td align='left'>
		          <xsl:value-of select="position()"/>
		        </td>
					  <xsl:for-each select="td">
					    <xsl:choose>
					      <xsl:when test="position()=3">
					        <td align='right' class='moneyCol'>
					          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
					        </td>
					      </xsl:when>
					      <xsl:otherwise>
					        <td align='left'>
					          <xsl:value-of select="."/>
					        </td>
					      </xsl:otherwise>			                        
					    </xsl:choose>
					  </xsl:for-each>
					</tr>
	      </xsl:for-each>
	      <tr>
		    	<td colspan="3">合计：</td>
		    	<td style="display:none"></td>
		    	<td style="display:none"></td>
		    	<td align='right' ><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),$VHMONEYFORMAT)"/></td>
		    </tr>
	    </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>