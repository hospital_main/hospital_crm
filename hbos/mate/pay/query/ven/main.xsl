<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
      	<th>序号</th>
      	<th>供应商编码</th>
      	<th>供应商名称</th>
      	<th>本月计划付款额</th>
      </tr>  		
    </thead>
    <tbody>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='left'>
	          <xsl:value-of select="position()"/>
	        </td>
				  <xsl:for-each select="td">
				    <xsl:choose>
				      <xsl:when test="position()=3">
				        <td align='right' class='moneyCol'>
				          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
				        </td>
				      </xsl:when>
				      <xsl:otherwise>
				        <td align='left'>
				          <xsl:value-of select="."/>
				        </td>
				      </xsl:otherwise>			                        
				    </xsl:choose>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
      <tr>
	    	<td colspan="3">合计：</td>
	    	<td style="display:none"></td>
	    	<td style="display:none"></td>
	    	<td align='right' ><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),$VHMONEYFORMAT)"/></td>
	    </tr>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

