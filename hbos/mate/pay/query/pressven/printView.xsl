<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
	      	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		      
		 	  	<tr>
				      	<td>供应商编码</td>
				      	<td>供应商名称</td>
				      	<td>期初金额</td>
				      	<td>本期增加(贷)</td>
				      	<td>本期减少(借)</td>
				      	<td>期末余额</td>
      	
      				</tr>  	 
        </thead>	      
	      <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">
	<tr>
	  <xsl:for-each select="td">
	  <!--
	    <xsl:choose>
	      <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6">
	        <td align='right' class='moneyCol'>
	          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
	        </td>
	      </xsl:when>
	      <xsl:otherwise>
	        <td align='left'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:otherwise>			                        
	    </xsl:choose>
	    -->
	    <td align='left'>
	          <xsl:value-of select="."/>
	        </td>
	  </xsl:for-each>
	</tr>
      </xsl:for-each>
    </root>
  </xsl:template>
</xsl:stylesheet>
