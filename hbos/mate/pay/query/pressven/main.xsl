<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap="true" class="mainHead">
      	<th>供应商编码</th>
      	<th>供应商名称</th>
      	<th>期初金额</th>
      	<th>本期增加(贷)</th>
      	<th>本期减少(借)</th>
      	<th>期末余额</th>
      	
      </tr>  	 		
    </thead>
    <tbody>
    <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">
	<tr>
	  <xsl:for-each select="td">
	    <!-- -->
	    <xsl:choose>
	      <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6">
	        <td align='right' class='moneyCol'>
	          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
	        </td>
	      </xsl:when>
	      <xsl:otherwise>
	        <td align='left'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:otherwise>			                        
	    </xsl:choose>
	  </xsl:for-each>
	</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

