<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
	      	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:1;align:right"/>
		      </tr>
		 	  	<tr>
			 	  	<td>期间</td>
		        <td>摘要</td>
	     	    <td>借方</td>
		        <td>贷方</td>
	          <td>余额</td>
          </tr>
        </thead>	      
	      <tbody>
	      <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
        <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
		      <xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<xsl:for-each select="td">          
		            <xsl:choose>            
		              <xsl:when test="position()=3 or position()=4 or position()= 5">
		              	<td class="numberText">
		              		<xsl:value-of select="format-number(.,'#,##0.00')"/>
		              	</td>
		              </xsl:when>  
		              <xsl:otherwise>
		              	<td align="left">
		                	<xsl:value-of select="."/>
		                </td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
	      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>