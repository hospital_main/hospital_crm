<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
      	<th>期间</th>
      	<th>摘要</th>
      	<th>借方</th>
      	<th>贷方</th>
      	<th>余额</th>
      </tr>  		
    </thead>
    <tbody>
    <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">
	<tr>
	  <xsl:for-each select="td">
	    <xsl:choose>
	      <xsl:when test="position()=3 or position()=4 or position()=5">
	        <td align='right' class='moneyCol'>
	          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
	        </td>
	      </xsl:when>
	      <xsl:otherwise>
	        <td align='left'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:otherwise>			                        
	    </xsl:choose>
	  </xsl:for-each>
	</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

