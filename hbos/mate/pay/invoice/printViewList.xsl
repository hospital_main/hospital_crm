<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
		  <thead>
		  	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:5;align:left"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		  		<tr noWrap="true" class="mainHead">
		        <td>流水号</td>
		        <td>发票号</td>
						<td>发票日期</td>
						<td>采购员</td>
						<td>供货单位</td>
						<td>发票金额</td>
						<td>状态</td>
						<td>审核人</td>
		  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	              <xsl:choose>
	                <xsl:when test="position()=1">
	                  <td><xsl:value-of select="."/></td>
              		</xsl:when>
	                <xsl:when test="position()=6 or position()=7 or position()=8 ">
	                  <td class='numberText'>
  	                		<xsl:value-of select="."/>  
  	              	</td>
	                </xsl:when>
	                <xsl:otherwise>
	                  <td><xsl:value-of select="."/></td>
	                </xsl:otherwise>
	              </xsl:choose>
	          </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
	  	<tfoot>
	  		<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:8"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		    </tr>
	  	</tfoot>
	 </root> 	
	</xsl:template>
</xsl:stylesheet>