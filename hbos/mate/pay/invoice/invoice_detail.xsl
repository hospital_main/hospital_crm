<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <tr noWrap='true' class='mainHead'>
        <th width='25'><input type='checkbox'/></th>
		<!--<th>序号</th>-->
		<th>发票号</th>
		<th>发票金额</th>
		<th>发票日期</th>
      </tr>
    </thead>
    <tbody>
	<xsl:for-each select="/root/tbody/tr">
		<xsl:variable name="rowindex" select="position()"/>
		<tr>
			<td align='center'>
				<input type='checkbox'>
					<xsl:attribute name="check_id">checkbox<xsl:value-of select="$rowindex"/></xsl:attribute>
				</input>
			</td>
			<xsl:for-each select="td">
				<xsl:choose>
					<xsl:when test="position()=1">
						<td align="center"><xsl:value-of select="."/></td>
					</xsl:when>
					<xsl:otherwise>
						<td align="center"><xsl:value-of select="."/></td>
					</xsl:otherwise>
				</xsl:choose>
  			 </xsl:for-each>
  		</tr>
	</xsl:for-each>  	
    </tbody>
  </xsl:template>
</xsl:stylesheet>


