<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>	  		    		
			<th noWrap="true">材料编码</th>
			<th noWrap="true">材料名称</th>
			<th noWrap="true">计量单位</th>
			<th noWrap="true">规格型号</th>
			<th noWrap="true">批号</th>
			<th noWrap="true">条形码</th>
			<th noWrap="true">单价</th>
			<th noWrap="true">数量</th>
			<th noWrap="true">票据金额</th>
			<th noWrap="true">发票号</th>
			<th noWrap="true">发票日期</th>
			<th noWrap="true">优惠金额</th>
			<th noWrap="true">应付金额</th>
			<th noWrap="true" style="display:none">ID</th>
     	</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
			<xsl:for-each select="/root/tbody/tr">
				<tr>    			
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=7">
	              <td align="right" class="moneyCol">
                  <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=9">
	              <td align="right" class="moneyCol">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=last()">
	            	<td align="right" style="display:none"><xsl:value-of select="."/></td>
	            </xsl:when>
							<xsl:otherwise>
								<td align="right"><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 		
	  </tbody> 	
 	</xsl:template>
</xsl:stylesheet>


