<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
		  <thead>
		  	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		  		<tr noWrap="true" class="mainHead">
		        <td>付款单号</td>
		        <td>日期</td>
						<td>供货单位</td>
						<td>发票张数</td>
						<td>付款金额</td>
						<td>发票号</td>
		  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <xsl:variable name="row" select="position()"/>
				<tr>
					<xsl:variable name="colName" select="td[1]"/>
					<xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[1] = $colName])"/>
  				<xsl:variable name="col" select="position()"/>
  				<xsl:for-each select="td">
		  			<xsl:choose>
		  					<xsl:when test="position()= 5">
					    			<td align="right">
					    				<xsl:value-of select="format-number(.,'#,##0.00')"/>
					    			</td>
				    		</xsl:when>
		  				<xsl:when test="(position()= 1 or position()= 2 or position()= 3 or position()= 4) and $row = 1">
					    			<td noWrap="true">
					    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
					    				<xsl:value-of select="."/>
					    			</td>
				    		</xsl:when>
				    		<xsl:when test="(position()= 5) and $row = 1">
					    			<td noWrap="true" align="right">
					    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
					    				<xsl:value-of select="."/>
					    			</td>
				    		</xsl:when>
				    		<xsl:when test="position()=1 and $row > 1">
					    			<xsl:if test=".!= ../../tr[$row -1]/td[1]">
							    			<td noWrap="true">
							    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
							    				<xsl:value-of select="."/>
							    			</td>
							    		</xsl:if>
							    		<xsl:if test=". = ../../tr[$row -1]/td[1]">
							    			<td style="display:none"/>
							    		</xsl:if>
				    		</xsl:when>
				    		<xsl:when test="(position()=2 or position()=3 or position()=4)  and $row > 1">
					    			<xsl:if test="../td[1]!= ../../tr[$row -1]/td[1]">
							    			<td noWrap="true">
							    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
							    				<xsl:value-of select="."/>
							    			</td>
							    		</xsl:if>
							    		<xsl:if test="../td[1] = ../../tr[$row -1]/td[1]">
							    			<td style="display:none"/>
							    		</xsl:if>
				    		</xsl:when>
				    		<xsl:when test="(position()=5)  and $row > 1">
					    			<xsl:if test="../td[1]!= ../../tr[$row -1]/td[1]">
							    			<td noWrap="true" align="right">
							    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
							    				<xsl:value-of select="."/>
							    			</td>
							    		</xsl:if>
							    		<xsl:if test="../td[1] = ../../tr[$row -1]/td[1]">
							    			<td style="display:none"/>
							    		</xsl:if>
				    		</xsl:when>
				    		
		  				
		  				<xsl:otherwise>
		  					<td><xsl:value-of select="."/></td>
		  				</xsl:otherwise>
		  			</xsl:choose>
		  		</xsl:for-each>
					
  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
	  	<tfoot>
	  		<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:6"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		    </tr>
	  	</tfoot>
	 </root> 	
	</xsl:template>
</xsl:stylesheet>