<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
  			<th style='display:none' width='25'><input type='checkbox'/></th>
        <th>付款单号</th>
				<th>日期</th>
				<th>供货单位</th>				
				<th>发票张数</th>
				<th>付款金额</th>
				<th>发票号</th>
				<th>是否做账</th>
  		</tr>
  	</thead>
  	<tbody>
  	
  		<xsl:for-each select="/root/tbody/tr">
  			<xsl:variable name="row" select="position()"/>
				<tr>
				<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:variable name="colName" select="td[1]"/>
					<xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[1] = $colName])"/>
  				<xsl:variable name="col" select="position()"/>
  				<xsl:for-each select="td">
		  			<xsl:choose>		  				
		  				<xsl:when test="(position()= 1 or position()= 2 or position()= 3 or position()= 4) and $row = 1">
					    			<td noWrap="true">
					    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
					    				<xsl:value-of select="."/>
					    			</td>
				    		</xsl:when>
				    		<xsl:when test="position()=1 and $row > 1">
					    			<xsl:if test=".!= ../../tr[$row -1]/td[1]">
							    			<td noWrap="true">
							    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
							    				<xsl:value-of select="."/>
							    			</td>
							    		</xsl:if>
							    		<xsl:if test=". = ../../tr[$row -1]/td[1]">
							    			<td style="display:none"/>
							    		</xsl:if>
				    		</xsl:when>
				    		<xsl:when test="(position()=2 or position()=3 or position()=4)  and $row > 1">
					    			<xsl:if test="../td[1]!= ../../tr[$row -1]/td[1]">
							    			<td noWrap="true">
							    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
							    				<xsl:value-of select="."/>
							    			</td>
							    		</xsl:if>
							    		<xsl:if test="../td[1] = ../../tr[$row -1]/td[1]">
							    			<td style="display:none"/>
							    		</xsl:if>
				    		</xsl:when>				    	
				    		
				    			<xsl:when test="position()=5">
		  					<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		  				</xsl:when>
		  				<xsl:otherwise>
		  					<td><xsl:value-of select="."/></td>
		  				</xsl:otherwise>
		  			</xsl:choose>
		  		</xsl:for-each>
					
  			</tr>
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
