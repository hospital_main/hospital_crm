<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
	      	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:1;align:right"/>
		      </tr>
		 	  	<tr>
			 	  	<td>日期</td>
			 	  	<td>单据号</td>
		        <td>摘要</td>
	     	    <td>借方</td>
		        <td>贷方</td>
	          <td>余额</td>
          </tr>
        </thead>	      
	      <tbody>
    <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">

<xsl:if test="td[3]!='本月合计' and  td[3]!='本年累计'">
      <tr>
	  <xsl:for-each select="td">
	    <xsl:choose>
	      <xsl:when test="position()=2">
	    	<td>
	          <a href="#">
	          	<xsl:attribute name="onclick" >
	          		<xsl:if test="../td[3]='采购发票'">
					javascript:openDialog('../invoice/update_detail.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
				</xsl:if>
				<xsl:if test="../td[3]='入库单'">
					javascript:openDialog('in_detail.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
				</xsl:if>
				<xsl:if test="../td[3]='付款单'">
					javascript:openDialog('../pay/pay/update_detail.html?load=&lt;pay_bill_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/pay_bill_id&gt;','dialogWidth:850px;dialogHeight:650px',result)
				</xsl:if>
			</xsl:attribute>
	          	<xsl:value-of select="."/>
	          </a>
	        </td>
	      </xsl:when>
	      <xsl:when test="position()=4 or position()=5 or position()=6">
	        <td align='right' class='moneyCol'>
	          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
	        </td>
	      </xsl:when>
	      <xsl:when test="position()=last()">
	        <td align='left' style='display:none'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:when>
	      <xsl:otherwise>
	        <td align='left'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:otherwise>			                        
	    </xsl:choose>
	  </xsl:for-each>
	</tr>

</xsl:if>




<xsl:if test="td[3]='本月合计' or  td[3]='本年累计'">
	<tr>
	  <xsl:for-each select="td">
	    <xsl:choose>
	      <xsl:when test="position()=2">
	    	<td>
	          <a href="#">
	          	<xsl:attribute name="onclick" >
	          		<xsl:if test="../td[3]='采购发票'">
					javascript:openDialog('../invoice/update_detail.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
				</xsl:if>
				<xsl:if test="../td[3]='入库单'">
					javascript:openDialog('in_detail.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
				</xsl:if>
				<xsl:if test="../td[3]='付款单'">
					javascript:openDialog('../pay/pay/update_detail.html?load=&lt;pay_bill_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/pay_bill_id&gt;','dialogWidth:850px;dialogHeight:650px',result)
				</xsl:if>
			</xsl:attribute>
	          	<xsl:value-of select="."/>
	          </a>
	        </td>
	      </xsl:when>
	      <xsl:when test="position()=4 or position()=5 or position()=6">
	        <td align='right' class='moneyCol'>
	          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
	        </td>
	      </xsl:when>

	      <xsl:when test="position()=1">
	        <td>
	          
	        </td>
	      </xsl:when>


	      <xsl:when test="position()=last()">
	        <td align='left' style='display:none'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:when>
	      <xsl:otherwise>
	        <td align='left'>
	          <xsl:value-of select="."/>
	        </td>
	      </xsl:otherwise>			                        
	    </xsl:choose>
	  </xsl:for-each>
	</tr>

</xsl:if>

      </xsl:for-each>
    </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>