<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  		<thead>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:11"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:5;align:left"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:6;align:left"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		        <td nowrap='true' valign="center">供应商编码</td>
			<td nowrap='true' valign="center">供应商名称</td>
			<td nowrap='true' valign="center">剩余天数</td>
			<td nowrap='true' valign="center">发票号</td>
			<td nowrap='true' valign="center">发票日期</td>
			<td nowrap='true' valign="center">采购员</td>
			<td nowrap='true' valign="center">状态</td>
			<td nowrap='true' valign="center">审核人</td>
			<td nowrap='true' valign="center">应付金额</td>
			<td nowrap='true' valign="center">优惠金额</td>
			<td nowrap='true' valign="center">已付金额</td>
		      </tr>
	  	</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
		            <tr>
		              <xsl:for-each select="td">
		                <xsl:choose>
											<xsl:when test="position()=9 or position()=10 or position()=11 ">
		                  	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                	</xsl:when>
		                	<xsl:when test="position()=last()">
		                  	<td align="right" style="display:none"><xsl:value-of select="."/></td>
		                	</xsl:when>
				       				<xsl:otherwise>
		                  	<td><xsl:value-of select="."/></td>
		                	</xsl:otherwise>
		               	</xsl:choose>
		             </xsl:for-each>
			    </tr>
	      		</xsl:for-each>
		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>