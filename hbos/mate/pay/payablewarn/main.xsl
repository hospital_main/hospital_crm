<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' valign="center">供应商编码</th>
				<th nowrap='true' valign="center">供应商名称</th>
				<th nowrap='true' valign="center">剩余天数</th>
				<th nowrap='true' valign="center">发票号</th>
				<th nowrap='true' valign="center">发票日期</th>
				
				<th nowrap='true' valign="center">采购员</th>
				<th nowrap='true' valign="center">状态</th>
				<th nowrap='true' valign="center">审核人</th>
				<th nowrap='true' valign="center">应付金额</th>
				<th nowrap='true' valign="center">优惠金额</th>
				<th nowrap='true' valign="center">已付金额</th>
				<th nowrap='true' valign="center" style="display:none">PK</th>
			</tr>
		</thead> 
		<tbody>
	      <xsl:for-each select="/root/tbody/tr">
	            <tr>
	              <xsl:for-each select="td">
	                <xsl:choose>
	                	<xsl:when test="position()=4">
					<td>
					  <a href="#">
					    <xsl:attribute name="onclick" >
						javascript:openDialog('invoice/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
					    </xsl:attribute>
				            <xsl:value-of select="."/>
					  </a>
					</td>
				</xsl:when>
				<xsl:when test="position()=9 or position()=10 or position()=11 ">
	                    		<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	                	</xsl:when>
	                	<xsl:when test="position()=last()">
	                    		<td align="right" style="display:none"><xsl:value-of select="."/></td>
	                	</xsl:when>
			        <xsl:otherwise>
	                     		<td><xsl:value-of select="."/></td>
	                	</xsl:otherwise>
	               	</xsl:choose>
	             </xsl:for-each>
		    </tr>
	      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>