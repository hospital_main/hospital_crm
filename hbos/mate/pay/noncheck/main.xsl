<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
      	<th>仓库</th>
      	<th>入库单号</th>
      	<th>制单日期</th>
      	<th>供货单位</th>
      	<th>采购员</th>
      	<th>应付金额</th>
      	<th>已开票金额</th>
      	<th>未开票金额</th>
      	<th>开户银行</th>
      	<th>银行账号</th>
      	<th style="display:none">业务类型</th>
      	<th style="display:none">入库id</th>
      	<th>发票号</th>
      </tr>  		
    </thead>
    <tbody>
    	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:for-each select="td">
				    <xsl:choose>
	          	<xsl:when test="position()=2">
		          	<td >
	                <xsl:if test="'0'=../td[11] or '11'=../td[11]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:[MAXW];dialogHeight:[MAXH]',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	
							  	<xsl:if test="'21'=../td[11]" >
					          <a href="#" tabindex='-1'>
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:1150px;dialogHeight:750px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										 </a>
							  	</xsl:if>
									<xsl:if test="'42'=../td[11] or '43'=../td[11] " >
					          <a href="#" tabindex='-1'>
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../mate/whr/in/initcommon/update.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:[MAXW];dialogHeight:[MAXH]',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										 </a>
							  	</xsl:if>
	              </td>
		          </xsl:when>
				      <xsl:when test="position()=8 or position()=6 or position()=7">
				        <td align='right' class='moneyCol'>
				          <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
				        </td>
				      </xsl:when>
				      <xsl:when test="position()=11 or position()=12">
				      	<td style="display:none"><xsl:value-of select="."/></td>	
				      </xsl:when>
				      <xsl:otherwise>
				        <td align='left'>
				          <xsl:value-of select="."/>
				        </td>
				      </xsl:otherwise>			                        
				    </xsl:choose>
				  </xsl:for-each>
				</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

