<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
  			<th noWrap="true">日期</th>
  			<th noWrap="true">物资编码</th>
  			<th noWrap="true">物资名称</th>
  	  	<th noWrap="true">类别编码</th>
  			<th noWrap="true">类别名称</th>
  			<th noWrap="true">仓库</th>
  	  	<th noWrap="true">规格</th>
  			<th noWrap="true">数量</th>
  			<th noWrap="true">单位</th>
  			<th noWrap="true">单价</th>
  			<th noWrap="true">金额</th>
  			<th noWrap="true">操作员</th>
  			<th noWrap="true">出库日期</th>
  			<th noWrap="true">科室名称</th>
  			<th noWrap="true">领用人</th>
  			<th noWrap="true">备注</th>
  			<th noWrap="true">供应商</th>
      </tr>
     </thead>
  	<tbody> 
  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <xsl:for-each select="/root/tbody/tr">
  	      <tr>
  	        <xsl:for-each select="td">
  	          <xsl:choose>
  	            <xsl:when test="position()=8">
  		            <td align='right' class='moneyCol'>
  		              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
  		            </td>
  			    		</xsl:when>
  			    		<xsl:when test="position()=10">
  		            <td align='right' class='moneyCol'>
  		              <xsl:value-of select="format-number(.,$VHPRICEFORMAT)"/>  
  		            </td>
  			    		</xsl:when>
  			    		<xsl:when test="position()=11">
  		            <td align='right' class='moneyCol'>
  		              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
  		            </td>
  			    		</xsl:when>
  			    		<xsl:otherwise>
  			          <td align='left'>
  	                <xsl:value-of select="."/>
  	              </td>
  	            </xsl:otherwise>
  	          </xsl:choose>
  	        </xsl:for-each>
  	      </tr>
	        <!--
	        <xsl:when test=" position()=last()">
	          <tr>
    	        <xsl:for-each select="td">
    	          <xsl:choose>
    	          	<xsl:when test="position()=1 ">
	    	          		<td>合计</td>
	    			    		</xsl:when>
    	            <xsl:when test="position()=5">
    		            <td align='right' class='moneyCol'>
    		              <xsl:value-of select="format-number(.,'#,##0.00')"/>  
    		            </td>
    			    		</xsl:when>
    			    		<xsl:when test="position()=8">
    		            <td align='right' class='moneyCol'>
    		              <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
    		            </td>
    			    		</xsl:when>
    			    		<xsl:otherwise>
    			          <td align='left'>
    	              </td>
    	            </xsl:otherwise>			                        
    	          </xsl:choose>
    	        </xsl:for-each>
    	      </tr>
	        </xsl:when>
	        -->
	    </xsl:for-each>
   	</tbody>
  </xsl:template>
</xsl:stylesheet>