<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		  <tr noWrap='true' class='mainHead'> 
		  	<th width='25'><input type='checkbox'/></th>
		    <th>摘要</th>
		    <th>会计科目</th>
		    <th>金额</th>
		    <th>借贷方向</th>
		  </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                  <a href='#'>
                    <xsl:attribute name="onclick">openDialog('charge_dupdate.html','dialogWidth:360px;dialogHeight:260px', this);</xsl:attribute>
                    <xsl:value-of select="."/>
                  </a>
                </td>
              </xsl:when>
	            <xsl:when test="position()=3">
	              <td  align="right">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=4 or position()=6">
	            </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
