<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
	  		<tr noWrap="true" class="mainHead">
	        <td>发票编号</td>
	        <td>开票日期</td>
					<td>摘要</td>
					<td>采购员</td>
					<td>应付金额</td>
					<td>优惠金额</td>
					<td>已付金额</td>
					<td>付款金额</td>
					
	  		</tr>
	  	</thead>
	  	<tbody>
	  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	              <xsl:choose>
	                <xsl:when test="position()=1">
              		</xsl:when>
	                <xsl:when test="position()=6 or position()=7">
	                  <!--td style="display:none" class='numberText'>
  	                	<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
  	              	</td-->
	                </xsl:when>
	                <xsl:when test="position()=8 ">
	                  <td class='numberText'>
  	                	<xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>  
  	              	</td>
	                </xsl:when>
	                <xsl:when test="position()=12 or position()=13 ">
	                </xsl:when>
	                <xsl:otherwise>
	                  <td><xsl:value-of select="."/></td>
	                </xsl:otherwise>
	              </xsl:choose>
	          </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
	  	<tfoot>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/> 
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/> 
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
	  	</tfoot>
	 </root> 	
	</xsl:template>
</xsl:stylesheet>