<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th width='25'><input type='checkbox' check_th='1' /></th>
        <th></th>
        <th>发票编号</th>
        <th>开票日期</th>
				<th>摘要</th>				
				<th>采购员</th>
				<th style="display:none">发票金额</th>
				<th style="display:none">已付款金额</th>		
				<th>应付金额</th>	
				<th>优惠金额</th>
				<th>已付金额</th>
				<th>付款金额</th>	
				<th style="display:none" >bill_id</th>		
				<th style="display:none" >bill_auto_id</th>		
      </tr>
    </thead>
    <tbody>
    <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>
          <td align='center'>
            <input type='checkbox'>
            <xsl:attribute name="check_id">checkbox<xsl:value-of select="$rowindex"/></xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=1">
            		<td onclick="changeDenotation(this)">
            			<xsl:attribute name="id">row<xsl:value-of select="$rowindex"/></xsl:attribute>
	            		<xsl:attribute name="bill_id">
	            			<xsl:value-of select="."/>
	            		</xsl:attribute>+</td>
              </xsl:when>
              <xsl:when test="position()=2">
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
	            <xsl:when test="position()=6">
	              <td style="display:none" align="right" class="moneyCol">
	              	<xsl:attribute name="id">row_pay<xsl:value-of select="$rowindex"/></xsl:attribute>
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=7">
	              <td style="display:none" align="right" class="moneyCol">
	              	<xsl:attribute name="id">row_real<xsl:value-of select="$rowindex"/></xsl:attribute>
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>
	            <xsl:when test=" position()=8 ">
	              <td  align="right" class="moneyCol" >
	              	<xsl:attribute name="id">row_money<xsl:value-of select="$rowindex"/></xsl:attribute>
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>
	            <xsl:when test=" position()=9 ">
	              <td  align="right" class="moneyCol" >
	              	<xsl:attribute name="id">row_favourable<xsl:value-of select="$rowindex"/></xsl:attribute>
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>
	            <xsl:when test=" position()=10 ">
	              <td  align="right" class="moneyCol" >
	              	<xsl:attribute name="id">row_payed_money<xsl:value-of select="$rowindex"/></xsl:attribute>
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>

		    <xsl:when test=" position()=11 ">
	              <td  align="right" class="moneyCol" >
	              	<xsl:attribute name="id">row_real_money<xsl:value-of select="$rowindex"/></xsl:attribute>
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>


              <xsl:when test="position()=12 ">
              	<td style="display:none" >
              		<xsl:value-of select="."/>
              	</td>
	            </xsl:when>
	            <xsl:when test=" position()=13 ">
              	<td style="display:none" >
              		<xsl:attribute name="id">row_auto_id<xsl:value-of select="$rowindex"/></xsl:attribute>
              		<xsl:value-of select="."/>
              	</td>
	            </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			<tr style="display:none">
  				<xsl:attribute name="id">annexrow<xsl:value-of select="$rowindex"/></xsl:attribute>
  				<td style="display:none" align='center'>
          </td>
          <td>
          </td>
          <td colspan="10" ><xsl:attribute name="id">revelation<xsl:value-of select="$rowindex"/></xsl:attribute>
          </td>
  			</tr>
      </xsl:for-each>
      <!--<xsl:if test="count(/root/total/td)!=0" >      
        <xsl:for-each select="/root/total">
        	<tr>
            <td align='center'></td>
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=3">
                  <td>合计：</td>
                </xsl:when>
                <xsl:when test="position()=8">
                  <td  align="right">
                    <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td></td>
                </xsl:otherwise>
              </xsl:choose>
      		  </xsl:for-each>
          </tr>
        </xsl:for-each>
      </xsl:if>-->    	
    </tbody>
  </xsl:template>
</xsl:stylesheet>


