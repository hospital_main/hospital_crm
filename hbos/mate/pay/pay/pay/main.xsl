<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>付款单号</th>
				<th>日期</th>
				<th>采购员</th>
				<th>供货单位</th>				
				<th>开户行</th>				
				<th>银行账号</th>				
				<th>状态</th>
				<th>单据类型</th>
				<th>发票张数</th>
				<th>付款金额</th>
				<xsl:if test="/root/tbody/tr[1]/td[last()] = '1' ">
				<th style='display:none'>预算金额</th>
				<th>是否超预算</th>
				</xsl:if>
				<th>出库情况</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
  			<tr>
  				<xsl:if test="td[4]!='合计'">
  				<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					
					<xsl:for-each select="td">
		  			<xsl:choose>
		  				<xsl:when test="position()=1">
		  					<td><a tabindex='-1'><xsl:value-of select="."/></a></td>
		  				</xsl:when>
		  				<!--如果状态是未审核，改变状态颜色为红色-->
              	<xsl:when test="position()=7">
								<xsl:if test="../td[7]='未审核'">
									<td style='Color:red'>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="../td[7]!='未审核'">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:when>
		  				<xsl:when test="position()=10">
		  					<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		  				</xsl:when>
		  				<xsl:when test="position()=11">
		  					<xsl:if test="/root/tbody/tr[1]/td[last()] = '1' ">
		  						<td align='right' style='display:none'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		  					</xsl:if>
		  				</xsl:when>
		  				<xsl:when test="position()=12">
		  					<xsl:if test="/root/tbody/tr[1]/td[last()] = '1' ">
		  						<td ><xsl:value-of select="."/></td>
		  					</xsl:if>
		  				</xsl:when>
		  				<xsl:when test="position()=13">
		  				</xsl:when>
		  				<xsl:when test="position()=14">
	              <td>
                  <a href="#">
									  <xsl:attribute name="onclick" >
									    javascript:openDialog('outStock_detail.html?load=&lt;pay_bill_no&gt;<xsl:value-of select="../td[1]"/>&lt;/pay_bill_no&gt;','dialogWidth:1200px;dialogHeight:800px',result)
									  </xsl:attribute>
									  <xsl:value-of select="."/>
									</a>
                </td>
	            </xsl:when>
		  				<xsl:otherwise>
		  					<td><xsl:value-of select="."/></td>
		  				</xsl:otherwise>
		  			</xsl:choose>
		  		</xsl:for-each>
		  		
		  		
					</xsl:if>
					
					
					
					
					<xsl:if test="td[4]='合计'">
							<td/>
							<xsl:for-each select="td">
		  			<xsl:choose>
		  				<xsl:when test="position()=1">
		  					<td><a tabindex='-1'><xsl:value-of select="."/></a></td>
		  				</xsl:when>
		  				<xsl:when test="position()=10">
		  					<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		  				</xsl:when>
		  				<xsl:when test="position()=11">
		  					<xsl:if test="/root/tbody/tr[1]/td[last()] = '1' ">
		  						<td align='right' style='display:none'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		  					</xsl:if>
		  				</xsl:when>
		  				<xsl:when test="position()=12">
		  					<xsl:if test="/root/tbody/tr[1]/td[last()] = '1' ">
		  						<td ><xsl:value-of select="."/></td>
		  					</xsl:if>
		  				</xsl:when>
		  				<xsl:when test="position()=13">
		  				</xsl:when>
		  				<xsl:when test="position()=9" >
		  						<td></td>
		  				</xsl:when>
		  				<xsl:otherwise>
		  					<td><xsl:value-of select="."/></td>
		  				</xsl:otherwise>
		  			</xsl:choose>
		  		</xsl:for-each>
						</xsl:if>
  				
  			</tr>
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
