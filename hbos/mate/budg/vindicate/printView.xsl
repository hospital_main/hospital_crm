<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
	      	<tr>
            <td style="fontsize:maintitle;colspan:colcount"></td>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>  
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>  
						<td style='display:none'/>
						<td style='display:none'/>
					</tr>
		 	  	<tr>
			 	<td>类别编码</td>
		        <td>类别名称</td>
		        <td>年度预算</td>
				<td>追加预算</td>
	     	    <td>一月</td>
	     	    <td>追加</td>
		        <td>二月</td>
	     	    <td>追加</td>
		        <td>三月</td>
	     	    <td>追加</td>
	          <td>四月</td>
	     	    <td>追加</td>
            <td>五月</td>
	     	    <td>追加</td>
            <td>六月</td>
	     	    <td>追加</td>
            <td>七月</td>
	     	    <td>追加</td>
            <td>八月</td>
	     	    <td>追加</td>
            <td>九月</td>
	     	    <td>追加</td>
            <td>十月</td>
	     	    <td>追加</td>
            <td>十一月</td>
	     	    <td>追加</td>
            <td>十二月</td>
	     	    <td>追加</td>
          </tr>
        </thead>	      
	      <tbody>
		      		<xsl:for-each select="/root/tbody/tr">
			<tr> 
				<xsl:for-each select="td[position()&lt;29]">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td nowrap="true">
								<xsl:attribute name="value" >
									<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
								</xsl:attribute>
								<xsl:if test="../td[29] = '1'">
									<xsl:value-of select="."/>
								</xsl:if>
								<xsl:if test="../td[29] != '1'">
									<xsl:value-of select="."/>
								</xsl:if>
							</td>								
						</xsl:when>
						<xsl:when test="position() &gt; 2">
							<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
          	
			</tr>			
		</xsl:for-each> 
	      </tbody>
	
	  </root>
	</xsl:template>
</xsl:stylesheet>