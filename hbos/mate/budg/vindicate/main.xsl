<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>	
				<th noWrap="true">财务类别编码</th>
				<th noWrap="true">财务类别名称</th>
				<th noWrap="true">年度预算</th>
				<th noWrap="true">追加预算</th>
				<th noWrap="true">一月</th>
				<th noWrap="true">追加</th>
				<th noWrap="true">二月</th>
				<th noWrap="true">追加</th>
				<th noWrap="true">三月</th>
				<th noWrap="true">追加</th>
				<th noWrap="true">四月</th>
				<th noWrap="true">追加</th>
				<th noWrap="true">五月</th>
				<th noWrap="true">追加</th>
				<th noWrap="true">六月</th>
				<th noWrap="true">追加</th>
				<th noWrap="true">七月</th>
				<th noWrap="true">追加</th>
				<th noWrap="true">八月</th>
				<th noWrap="true">追加</th>
				<th noWrap="true">九月</th>
				<th noWrap="true">追加</th>
				<th noWrap="true">十月</th>
				<th noWrap="true">追加</th>
				<th noWrap="true">十一月</th>
				<th noWrap="true">追加</th>
				<th noWrap="true">十二月</th>
				<th noWrap="true">追加</th>
     	</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr> 
				<xsl:for-each select="td[position()&lt;29]">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td nowrap="true">
								<xsl:attribute name="value" >
									<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
								</xsl:attribute>
								<xsl:if test="../td[29] = '1'">
									<a href='#' onclick="setBudgDeptDetail(this)">
									<xsl:value-of select="."/>
									</a>
								</xsl:if>
								<xsl:if test="../td[29] != '1'">
									<xsl:value-of select="."/>
								</xsl:if>
							</td>								
						</xsl:when>
						<xsl:when test="position() &gt; 2">
							<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
          	
			</tr>			
		</xsl:for-each> 		
	  </tbody> 	
 	</xsl:template>
</xsl:stylesheet>


