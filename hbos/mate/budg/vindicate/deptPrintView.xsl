<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
	      	<tr>
			<td style="fontsize:maintitle;colspan:colcount"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			</tr>
		 <tr>
			<td>部门名称</td>
		        <td>年度预算</td>
			<td>年度追加</td>
			<td>一月</td>	<td>追加</td>
		        <td>二月</td>	<td>追加</td>
		        <td>三月</td>	<td>追加</td>
			<td>四月</td>	<td>追加</td>
			<td>五月</td>	<td>追加</td>
			<td>六月</td>	<td>追加</td>
			<td>七月</td>	<td>追加</td>
			<td>八月</td>	<td>追加</td>
			<td>九月</td>	<td>追加</td>
			<td>十月</td>	<td>追加</td>
			<td>十一月</td>	<td>追加</td>
			<td>十二月</td>	<td>追加</td>
		</tr>
        </thead>
	      <tbody>
		      <xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<xsl:for-each select="td[position() &lt; 28]">
				    <xsl:choose>
					<xsl:when test="position() = 1">
					<td>
						<xsl:value-of select="."/>
					</td>
					</xsl:when>
				      <xsl:otherwise>
					<td align="right">
						<xsl:value-of select="."/>
					</td>
				      </xsl:otherwise>
				    </xsl:choose>
		  		</xsl:for-each>
		  	  </tr>
		   	</xsl:for-each>
	      </tbody>

	  </root>
	</xsl:template>
</xsl:stylesheet>