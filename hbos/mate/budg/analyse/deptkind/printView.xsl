<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
<root>
<thead>
	<xsl:variable name="colNum" select="number(//tr[1]/td[2])"/>
	<xsl:variable name="tcolNum" select="count(/root/tbody/tr[1]/td)"/>
	<tr>
	<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="$tcolNum"/></xsl:attribute></td>
	<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1 ]">
  	<td style='display:none'/>
  	</xsl:for-each>
	</tr>
	<xsl:choose>
		<xsl:when test="$colNum &gt; 1">
		<tr >
		<xsl:for-each select="/root/tbody/tr[2]/td">
			<xsl:if  test="position() &lt; 3">
				<td nowrap='true'>
					<xsl:attribute name="rowspan">2</xsl:attribute>
					<xsl:value-of select="."/>
				</td>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="/root/tbody/tr[1]/td">
			<xsl:if test="(position() &gt; 2) and (((position() - 2) mod $colNum) = 1)">
			<td nowrap='true'>
				<xsl:attribute name="colspan"><xsl:value-of select='$colNum'/></xsl:attribute>
				<xsl:value-of select="."/>
			</td>
			</xsl:if>
			<xsl:if test="(position() &gt; 2) and (((position() - 2) mod $colNum) != 1)">
			<td style='display:none'/>
			</xsl:if>
		</xsl:for-each>
		</tr>
		<tr >
		<td style='display:none'/>
		<td style='display:none'/>
		<xsl:for-each select="/root/tbody/tr[2]">
			<xsl:for-each select="td">
			<xsl:if test="position() &gt; 2">
				<td nowrap='true'>
					<xsl:value-of select="."/>
				</td>
			</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
		</tr>
		</xsl:when>
		<xsl:otherwise>
			<tr >
			<xsl:for-each select="/root/tbody/tr[2]/td[position() &lt; 3]">
			<th nowrap='true'><xsl:value-of select="."/></th>
			</xsl:for-each>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 2]">
			<th nowrap='true'><xsl:value-of select="."/></th>
			</xsl:for-each>
			</tr>
		</xsl:otherwise>
	</xsl:choose>
</thead>
<tbody>
		<xsl:for-each select="/root/tbody/tr[td[1] !='1' and td[1]!='������']">
			<tr>
				<xsl:for-each select="td">
					<xsl:if test="position() =1">
						<td>
							<xsl:if test=".!='0'">
								<xsl:value-of select="."/>
							</xsl:if>
						</td>
					</xsl:if>
						
					<xsl:if test="position() =2">
						<td>
							<xsl:value-of select="."/>
						</td>
					</xsl:if>
					<xsl:if test="position() &gt; 2">
						<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
					</xsl:if>
				</xsl:for-each>

			</tr>
		</xsl:for-each>
</tbody>
</root>
 	</xsl:template>
</xsl:stylesheet>


