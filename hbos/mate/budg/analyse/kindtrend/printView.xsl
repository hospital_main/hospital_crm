<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <thead>
	      	<xsl:variable name="colMerge" select="(//tr[1]/td[last()])"/>
	      	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)"/>
	      	<tr>
            <td style="fontsize:maintitle;colspan:colcount"></td>
  					<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 3 ]">
  						<td style='display:none'/>
  					</xsl:for-each>
					</tr>
					<tr noWrap="true" class="mainHead">
	  				<xsl:for-each select="/root/tbody/tr[td[1] = '1']">
	  					<xsl:for-each select="td[position() &gt; 1 and position()!= last()]">
	  						<xsl:choose>
	  							<xsl:when test="position() &lt; 3">
			  						<td nowrap='true' rowspan="2">
			  							<xsl:value-of select="."/>
			  						</td>
		  						</xsl:when>
		  						<xsl:otherwise>
			  						<xsl:if test=". != ''">
				  						<td nowrap='true'>
				  							<xsl:attribute name="colspan"><xsl:value-of select='$colMerge'/></xsl:attribute>
				  							<xsl:value-of select="."/>
				  						</td>
			  						</xsl:if>
			  						<xsl:if test=". = ''">
				  						<td style='display:none'/>
			  						</xsl:if>
			  					</xsl:otherwise>
			  				</xsl:choose>
	  					</xsl:for-each>
						</xsl:for-each>
					</tr>
		  		<tr noWrap='true' class='mainHead'>
		  				<td style='display:none'/>
		  				<td style='display:none'/>	
		  				<xsl:for-each select="/root/tbody/tr[td[1] = '2']">
		  					<xsl:for-each select="td[position() &gt; 3 and position()!= last()]">
		  						<td nowrap='true'><xsl:value-of select="."/></td>
		  					</xsl:for-each>
							</xsl:for-each>
		     	</tr>
					
        </thead>	      
	      <tbody>
		      <xsl:for-each select="/root/tbody/tr[td[1] !='1' and td[1]!='2']">
		        <tr>
		        	<xsl:for-each select="td[position() &gt; 1 and position()!= last()]">          
		            <xsl:choose>
		            	<xsl:when test="position() = 1 or position() = 2">
		              	<td>
		                	<xsl:value-of select="."/>
		                </td>
		            	</xsl:when>            
		              <xsl:otherwise>
		              	<td align="right">
		                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
		              </xsl:otherwise>
		            </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
	      </tbody>
	
	  </root>
	</xsl:template>
</xsl:stylesheet>