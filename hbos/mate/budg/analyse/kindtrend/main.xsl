<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="colNum" select="(//tr[1]/td[last()])"/>
  		<tr noWrap='true' class='mainHead'>	
  				<xsl:for-each select="/root/tbody/tr[td[1] = '1']">
  					<xsl:for-each select="td[position() &gt; 1 and position()!= last()]">
  						<xsl:choose>
  							<xsl:when test="position() &lt; 3">
		  						<th nowrap='true' rowspan="2">
		  							<xsl:value-of select="."/>
		  						</th>
	  						</xsl:when>
	  						<xsl:otherwise>
		  						<xsl:if test=". != ''">
			  						<th nowrap='true'>
			  							<xsl:attribute name="colspan"><xsl:value-of select='$colNum'/></xsl:attribute>
			  							<xsl:value-of select="."/>
			  						</th>
		  						</xsl:if>
		  					</xsl:otherwise>
		  				</xsl:choose>
  					</xsl:for-each>
					</xsl:for-each>
     	</tr>
  		<tr noWrap='true' class='mainHead'>	
  				<xsl:for-each select="/root/tbody/tr[td[1] = '2']">
  					<xsl:for-each select="td[position() &gt; 3 and position()!= last()]">
  						<th nowrap='true'><xsl:value-of select="."/></th>
  					</xsl:for-each>
					</xsl:for-each>
     	</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr[td[1] !='1' and td[1]!='2']">
			<tr> 
				<xsl:for-each select="td[position() &gt; 1 and position()!= last()]">
					<xsl:if test="position() &lt; 3">
						<td><xsl:value-of select="."/></td>
					</xsl:if>
					<xsl:if test="position() &gt; 2">
						<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
					</xsl:if>
				</xsl:for-each>
          	
			</tr>			
		</xsl:for-each> 		
	  </tbody> 	
 	</xsl:template>
</xsl:stylesheet>


