<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
			<th style=""><!-- <input type="checkbox" /> --></th>
			<th noWrap="true" >物资财务分类编码</th>
			<th noWrap="true">物资财务分类名称</th>
     		</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'  style='display:none'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
    	    </xsl:attribute>
  	    	<xsl:if test="td[3]='0' ">
        		<xsl:attribute name="disabled" />
        	</xsl:if>
  	  		</input>
				</td>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td nowrap="true">
								<xsl:attribute name="value" >
									<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
								</xsl:attribute>
								<xsl:value-of select="."/>
							</td>
						</xsl:when>
						<xsl:when test="position()=3">
							<td style="display:none" >
								<xsl:value-of select="."/>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>

			</tr>
		</xsl:for-each>
	  </tbody>
 	</xsl:template>
</xsl:stylesheet>


