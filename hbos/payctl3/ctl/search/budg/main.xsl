<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th  style='display:none'>类别</th>
				<th>经费归口部门</th>
				<th>经费开支部门</th>
				<th>支出项目编码</th>
				<th>支出项目名称</th>
				<th>经费来源</th>
				<th>年初预算</th>
				<th>期间调整</th>
				<th>预算总额</th>
				<th style='display:none'>归口部门编码</th>
				<th style='display:none'>开支部门编码</th>
				<th style='display:none'>是否末级</th>
			</tr>
		</thead>

		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1 ">
								<td style="display:none"><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=4 ">
								<td >
									<xsl:if test="../td[12]!='0'">
										<xsl:value-of select="."/>
									</xsl:if>
									<xsl:if test="../td[12]='0'">
											<xsl:value-of select="."/>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()=7 ">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=9 ">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=8 ">
								<td align='right'>
									<xsl:if test="../td[12]!='1'">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
									<xsl:if test="../td[12]='1'">
										<a href="#">
											<xsl:attribute name="onclick" >
					    							javascript:openDetail('<xsl:value-of select="../td[position()=4]"/>', '<xsl:value-of select="../td[position()=10]"/>', '<xsl:value-of select="../td[position()=11]"/>');
					  						</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</a>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()=10 or position()=11 or position()=12">
								<td style='display:none'><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



