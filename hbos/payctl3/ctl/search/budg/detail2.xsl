<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>归口部门名称</th>
				<th>开支部门名称</th>
				<th>项目编码</th>
				<th>项目名称</th>
				<th>经费来源</th>
				<th>年初预算</th>
				<th>期间调整</th>
				<th>预算总额</th>
			</tr>
		</thead>

		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=6 or position()=7 or position()=8">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>