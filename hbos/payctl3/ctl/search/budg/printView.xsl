<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
			   		<td style="fontsize:maintitle;colspan:colcount;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
					<tr noWrap='true' class="mainHead">
						<td>经费归口部门</td>
						<td>经费开支部门</td>
						<td>支出项目编码</td>
						<td>支出项目名称</td>
						<td>经费来源</td>
						<td>年初预算</td>
						<td>期间调整</td>
						<td>预算总额</td>
					</tr>
				</thead>
		
				<tbody>
					<xsl:for-each select="/root/tbody/tr">
						<tr>
							<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=1">
										
									</xsl:when>
									<xsl:when test="position()=7 ">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=8 ">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=9 ">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:otherwise>
										<td><xsl:value-of select="."/></td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>



