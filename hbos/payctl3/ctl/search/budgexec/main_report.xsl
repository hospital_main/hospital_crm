<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr class="mainHead">
			<td  align="center" rowspan="3">事件项目编码</td>
			<td  align="center" rowspan="3">事件项目名称</td>
			<td  align="center" rowspan="3">经费归口部门</td>	
			<td  align="center" rowspan="3">经费开支部门</td>	
			<td  align="center" colspan="7">
				年度预算
			</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr class="mainHead">
			<td  align="center" rowspan="2">预算总额</td>	
			<td  align="center" colspan="3">实际报销</td>	
			<td  align="center" colspan="3">实际支付</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</tr>
		<tr class="mainHead">
			<td  align="center" >实际报销</td>
			<td  align="center" >预算余额</td>
			<td  align="center" >执行进度%</td>
			<td  align="center" >实际支付</td>
			<td  align="center" >可用额度</td>
			<td  align="center" >执行进度%</td>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:text disable-output-escaping="yes">
				&lt;tr&gt;
			</xsl:text>
			<td>
				<xsl:value-of select="td[1]"/>
			</td>
			 <td>
				<xsl:value-of select="td[2]"/>
			</td>
			<td>
				<xsl:value-of select="td[3]"/>
			</td>
			<td>
				<xsl:value-of select="td[4]"/>
			</td>
			<td align="right">
				<xsl:value-of select="format-number(td[5],'#,##0.00')"/>
			</td>
			<td align="right">
				<xsl:value-of select="format-number(td[6],'#,##0.00')"/>
			</td>
			<td align="right">
				<xsl:value-of select="format-number(td[7],'#,##0.00')"/>
			</td>
				<td align="right">
				<xsl:value-of select="format-number(td[8],'#,##0.00%')"/>
			</td>
			<td align="right">
				<xsl:value-of select="format-number(td[9],'#,##0.00')"/>
			</td>
			<td align="right">
				<xsl:value-of select="format-number(td[10],'#,##0.00')"/>
			</td>
			<td align="right">
				<xsl:value-of select="format-number(td[11],'#,##0.00%')"/>
			</td>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>