<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>序号</th>
				<th>调整日期</th>
				<th>项目编码</th>
				<th>项目名称</th>
				<th>经费来源</th>
				<th>经费归口部门</th>
				<th>经费开支部门</th>
				<th>调整类别</th>
				<th>年初预算</th>
				<th>当前预算</th>
				<th>期间调整</th>
				<th>预算总额</th>
			</tr>
		</thead>

		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				   <xsl:if test="td[2]!='合计：'">
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					</xsl:if>
			<xsl:if test="td[2]='合计：'">
          <td></td>
          </xsl:if>
					<xsl:for-each select="td[position()!=14]">
							<xsl:choose>
								<xsl:when test="position()=1 ">
									<td >
										<a href="#">
										<xsl:attribute name="onclick">
											openUpdateDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute>
										<xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=12 or position()=11 or position()=10 or position()=9">
									<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
							<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



