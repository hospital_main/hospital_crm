<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
			<tr class='mainHead'>
				<th>摘要</th>	
				<th>支付日期</th>
				<th>经费开支部门</th>
				<th>经费归口部门</th>
				<th>预算项目</th>
				<th>报销金额</th>	
				<th>余额</th>
				<th>是否确认</th>
				<!--th>制单人</th-->
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:if test="position()=1" >
              <td>
                <xsl:value-of select="."/>
  	          </td>
            </xsl:if>
            <xsl:if test="position()=6">  
              <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:if>
            <xsl:if test="position()=7 and ../td[1]='合计：' ">  
              <td align="right"></td>
            </xsl:if>
            <xsl:if test="position()=7 and ../td[1]!='合计：' ">  
              <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:if>
            <xsl:if test="position() !=1 and position()&lt; 6">
              <td><xsl:value-of select="."/></td>
            </xsl:if>
            <xsl:if test="position() =8">
              <td><xsl:value-of select="."/></td>
            </xsl:if>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>