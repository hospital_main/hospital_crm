<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="month_Cols" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1] and td[2]=/root/tbody/tr[1]/td[2]])"/>
		<tr class="mainHead">
			<td  align="center" rowspan="3">经费归口部门</td>	
			<td  align="center" rowspan="3">预算项目编码</td>
			<td  align="center" rowspan="3">预算项目名称</td>
			<td  align="center" rowspan="3">经费来源</td>
	
			<td  align="center" colspan="7">
					年度预算
			 </td>		
		</tr>
		<tr class="mainHead">
			<td  align="center" rowspan="2" >预算总额</td>	
			<td  align="center" colspan="3">实际报销</td>	
			<td  align="center" colspan="3">实际支付</td>
			</tr>
		<tr class="mainHead">
			<td  align="center" >实际报销</td>
			<td  align="center" >预算余额</td>
			<td  align="center" >执行进度</td>
			<td  align="center" >实际支付</td>
			<td  align="center" >可用额度</td>
			<td  align="center" >执行进度</td>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:if test="( position() mod $month_Cols )=1 or $month_Cols = 1">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;
				</xsl:text>
				<td>
					<xsl:value-of select="td[1]"/>
				</td>
				<xsl:if test="td[2]!='合计：'">
				<td>
				
					<a href="#">
						<xsl:attribute name="onclick" >
					    javascript:openDetail('<xsl:value-of select="td[2]"/>','<xsl:value-of select="td[13]"/>');
					  </xsl:attribute>
						<xsl:value-of select="td[2]"/>
					</a>
				</td>
				</xsl:if>
				<xsl:if test="td[2]='合计：'">
					<td>合计 :</td>
				 </xsl:if>
				 
				 <td>
					<xsl:value-of select="td[3]"/>
				</td>
			</xsl:if>
			
			<xsl:if test="(position() mod $month_Cols)!=0">
				<td align="right">
					<xsl:value-of select="td[5]"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[6],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[7],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[8],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[9],'#,##0.00%')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[10],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[11],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[12],'#,##0.00%')"/>
				</td>
			
			</xsl:if>
			
			<xsl:if test="(position() mod $month_Cols)=0 or $month_Cols = 1">
		
				<td align="right">
					<xsl:value-of select="td[5]"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[6],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[7],'#,##0.00')"/>
				</td>
			<xsl:if test="td[2]!='合计：'">
				<td align="right">
					<xsl:value-of select="format-number(td[8],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[9],'#,##0.00%')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[10],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[11],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[12],'#,##0.00%')"/>
				</td>
			</xsl:if>
			<xsl:if test="td[2]='合计：'">
			<td></td>
			</xsl:if>
				<xsl:text disable-output-escaping="yes">
					&lt;/tr&gt;
				</xsl:text>
			</xsl:if>
					
			
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>