<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
	  	<thead>
		  	<tr>
		   		<td style="fontsize:maintitle;colspan:colcount;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
	  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;" width="180">单据编号</td>
					<td style="fontsize:coltitle;" width="180">申请人</td>
					<td style="fontsize:coltitle;" width="180">科室</td>
					<td style="fontsize:coltitle;" width="330">摘要</td>
					<td style="fontsize:coltitle;" width="180">金额</td>
					<td style="fontsize:coltitle;" width="180">制单日期</td>
					<td style="fontsize:coltitle;" width="180">支付状态</td>
					<td style="fontsize:coltitle;" width="180">支付审核人</td>
					<td style="fontsize:coltitle;" width="180">支付日期</td>
	  		</tr>  	
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	            <xsl:choose>
	            	<xsl:when test=" position()=1 ">
	            		<td align='left'><xsl:value-of select="."/></td>
	            	</xsl:when> 
	              <xsl:when test="position()=5">
	                <td align='right' noWrap='true'>
			  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
	              </xsl:when> 
	              <xsl:otherwise>
	                <td align="left"><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	            </xsl:choose>   
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>