<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      
      <tr noWrap='true' class="mainHead">
      	<th><input type="checkbox" onclick="selectAll(this)"/></th>
      	<th nowrap='true' valign='middle'>是否核销</th>
      	<th nowrap='true' valign='middle'>单据编号</th>
      	<th nowrap='true' valign='middle'>申请人</th>
      	<th nowrap='true' valign='middle'>科室</th>
      	<th nowrap='true' valign='middle'>预算项目</th>
      	<th nowrap='true' valign='middle'>说明</th>
      	<th nowrap='true' valign='middle'>金额</th>
      	<th nowrap='true' valign='middle'>制单日期</th>
      </tr>
    </thead>
    
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              
              <xsl:when test="position() = 1">
		            <td align="center">
			            <input ids="1" type="checkbox">
				            <xsl:attribute name="value">
				            	<xsl:value-of select="."/>	
				            </xsl:attribute>
			            </input>
		            </td>
              </xsl:when>
              <xsl:when test="position() = 2">
              	<td align="center">
              		<input ids="2" type="checkbox" onclick="">
	              		<xsl:attribute name="value">
	              			<xsl:value-of select="."/>
	              		</xsl:attribute>
              		</input>
              	</td>
              </xsl:when>
              
              <xsl:when test="position() = 3">
		            <td><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
              	<td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>		
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

