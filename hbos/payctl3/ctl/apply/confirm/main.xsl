<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style="display:none"><input type="checkbox"/></th>
      	<th nowrap='true' valign='middle'>单据编号</th>
      	<th nowrap='true' valign='middle'>经办人</th>
      	<th nowrap='true' valign='middle'>经费归口部门</th>
      	<th nowrap='true' valign='middle'>项目名称</th>
      	<th nowrap='true' valign='middle'>说明</th>
      	<th nowrap='true' valign='middle'>金额</th>
      	<th nowrap='true' valign='middle'>制单日期</th>
      	<th nowrap='true' valign='middle'>确认状态</th>
      	<th nowrap='true' valign='middle'>确认人</th>
      	<th nowrap='true' valign='middle'>确认日期</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          	<td align='center'  style='display:none'>
  	          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
  	          	<xsl:attribute name="chk">
  	          		<xsl:if test="td[10] = '已确认'">1</xsl:if>
  	          		<xsl:if test="td[10] != '已确认'">0</xsl:if>
  	          	</xsl:attribute>
  	            <xsl:attribute name="value" >
  	              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
  	      	    </xsl:attribute>
  	    	  	</input>
            </td>
          <xsl:for-each select="td">
	          <xsl:choose>
              <xsl:when test=" position() = 1 ">
							</xsl:when>
              <xsl:when test="position()=2">
              	<td>
  	          			<a href="#">
							<xsl:attribute name="onclick">
							openDialog('../apply/insert.html?<xsl:value-of select="../pk/pid"/>', 'dialogWidth:1200px;dialogHeight:600px', result)
							</xsl:attribute>
							<xsl:value-of select="."/>
						</a>
  	          	</td>
              </xsl:when>
              <xsl:when test=" position() = 3 ">
								<td align="right" style="display:none"><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test=" position() = 8 ">
								<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
	          </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
      
    </tbody>
  </xsl:template>
</xsl:stylesheet>

