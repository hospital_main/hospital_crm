<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
	  	<thead>
		  	<tr noWrap="true" class="mainHead">
		   		<td style="fontsize:maintitle;colspan:colcount;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
	  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;" >单据编号</td>
					<td style="fontsize:coltitle;" >经办人</td>
					<td style="fontsize:coltitle;" >科室</td>
					<td style="fontsize:coltitle;" >项目名称</td>
					<td style="fontsize:coltitle;" >说明</td>
					<td style="fontsize:coltitle;" >金额</td>
					<td style="fontsize:coltitle;" >制单日期</td>
					<td style="fontsize:coltitle;" >确认状态</td>
					<td style="fontsize:coltitle;" >确认人</td>
					<td style="fontsize:coltitle;" >确认日期</td>
	  		</tr>  	
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	            <xsl:choose>
	            	<xsl:when test=" position()=1 or position()= 3">
	            		
	            	</xsl:when> 
	            		<xsl:when test=" position()=2 ">
	            		<td align='left'><xsl:value-of select="."/></td>
	            	</xsl:when> 
	              <xsl:when test="position()=8">
	                <td align='right' noWrap='true'>
			  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
	              </xsl:when> 
	              <xsl:otherwise>
	                <td align="left"><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	            </xsl:choose>   
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	   		
	 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>