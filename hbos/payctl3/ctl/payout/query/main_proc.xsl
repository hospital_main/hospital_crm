<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
			<tr class='mainHead'>
				<th>经费归口部门</th>	
				<th>预算科目</th>
				<th>执行金额</th>
				<th>预算金额</th>
				<th>预算余额</th>
				<th>执行进度</th>
				<!--th>制单人</th-->
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:if test="position()=1 and .!=''">
              <td>
                <xsl:value-of select="."/>
  	          </td>
            </xsl:if>
            <xsl:if test="position()=3 or position()=4 or position()=5 ">  
              <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:if>
            <xsl:if test="position()=6 ">  
              <td align="right"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
            </xsl:if>
            <xsl:if test="position()=2">
              <td><xsl:value-of select="."/></td>
            </xsl:if>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>