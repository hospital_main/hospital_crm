<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true'>
      	<th nowrap='true' valign='middle' colspan="5">预算情况</th>
      	<th nowrap='true' valign='middle' colspan="3">在途单据情况</th>
      	<th nowrap='true' valign='middle' rowspan="2">可用额度</th>
      </tr>
      <tr noWrap='true'>
      	<th nowrap='true' valign='middle'>支出项目</th>
      	<th nowrap='true' valign='middle'>年初预算</th>
      	<th nowrap='true' valign='middle'>当前预算</th>
      	<th nowrap='true' valign='middle'>执行额</th>
      	<th nowrap='true' valign='middle'>预算结余</th>
      	<th nowrap='true' valign='middle'>已申请未确认</th>
      	<th nowrap='true' valign='middle'>已申请未报销</th>
      	<th nowrap='true' valign='middle'>已制单未报销</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position() = 1">
		            <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position() &gt; 2">
		            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:when test="position() = 2">
              	<td><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
              </xsl:otherwise>
            </xsl:choose>		
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

