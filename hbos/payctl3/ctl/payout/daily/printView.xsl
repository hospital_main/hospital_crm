<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
	  	<thead>
		  	<tr>
		   		<td style="fontsize:maintitle;colspan:colcount;"></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<!--<td style='display:none'/>-->
			</tr>
	  		<tr noWrap="true" class="mainHead">
				<td style="fontsize:coltitle;" width="180">支付日期</td>
				<td style="fontsize:coltitle;" width="80">报销日期</td>
				<td style="fontsize:coltitle;" width="180">预算项目</td>
				<td style="fontsize:coltitle;" width="180">经费来源</td>
				<td style="fontsize:coltitle;" width="180">摘要</td>
				<td style="fontsize:coltitle;" width="180">经费归口部门</td>
				<td style="fontsize:coltitle;" width="330">经费开支部门</td>
				<td style="fontsize:coltitle;" width="180">报销金额</td>
				<td style="fontsize:coltitle;" width="180">预算余额</td>
				<!--<td style="fontsize:coltitle;" width="180">可用额度</td>-->
				<td style="fontsize:coltitle;" width="80">是否确认</td>
				<td style="fontsize:coltitle;" width="80">是否冲销</td>
	  		</tr>  	
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test="position()=8 or position()=9">
	                <td align='right' noWrap='true'>
			  	 	<xsl:value-of select="."/>
				   </td>
	              </xsl:when>
	              <xsl:when test="position()=10">
	              </xsl:when>
	              <xsl:otherwise>
	                <td align="left"><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	            </xsl:choose>   
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>