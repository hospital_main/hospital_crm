<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      
      <tr noWrap='true' class="mainHead">
      	<th nowrap='true' valign='middle'>支出项目编码</th>
      	<th nowrap='true' valign='middle'>支出项目名称</th>
      	<th nowrap='true' valign='middle'>申请金额</th>
      	<th nowrap='true' valign='middle'>摘要</th>
      	<th nowrap='true' valign='middle'>单据编码</th>
      	<th nowrap='true' valign='middle'>单据说明</th>
      	<th nowrap='true' valign='middle'>制单日期</th>
      	<th nowrap='true' valign='middle'>审核状态</th>
      </tr>
    </thead>
    
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position() = 1">
		            <td><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position() = 3">
		            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:when test="position() = 2">
              	<td><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
              	<td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>		
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

