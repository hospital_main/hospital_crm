<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
<xsl:template match="/">
  <thead>
    <tr noWrap="true" class="mainHead">
      <th style="display:none"><input type="checkbox" /></th>
      <th>摘要</th>
      <th>报销人</th>
      <th>经费开支部门</th>
      <th>支出项目</th>
      <th>资金来源</th>
      <th>申报金额</th>
     
    </tr>
  </thead>
  <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        <td align='center'  style='display:none'>
          <input type='checkbox'  TABINDEX='-1' style='font-size:8px;'>
	          <xsl:attribute name="value" >
	            <xsl:for-each select="pk/*"><xsl:value-of select="."/></xsl:for-each>
	  	      </xsl:attribute>
    	    </input>
        </td>
        <xsl:for-each select="td">        	
        	<xsl:choose>
	          <xsl:when test="position()=8">
	            <td align='right'>
                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
              </td>
	          </xsl:when>
	          <xsl:otherwise>
	            <td><xsl:value-of select="."/></td>
	          </xsl:otherwise>
	        </xsl:choose>
  			</xsl:for-each>
      </tr>
    </xsl:for-each>
  </tbody>
</xsl:template>
</xsl:stylesheet>