<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="month_Cols" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1] and td[2]=/root/tbody/tr[1]/td[2]])"/>
		<tr>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="3">支出项目编码</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="3">支出项目名称</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="3">经费来源</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="3">经费归口部门</td>	
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="3">经费开支部门</td>	
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" colspan="7">
				年度预算
			</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
		</tr>
		<tr>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="2">预算总额</td>	
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" colspan="3">实际报销</td>	
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" colspan="3">实际支付</td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			</tr>
		<tr>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >实际报销</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >预算余额</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >执行进度%</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >实际支付</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >可用额度</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >执行进度%</td>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;
				</xsl:text>
				<td>
					<xsl:value-of select="td[1]"/>
				</td>
				 <td>
					<xsl:value-of select="td[2]"/>
				</td>
				<td>
					<xsl:value-of select="td[3]"/>
				</td>
				<td>
					<xsl:value-of select="td[4]"/>
				</td>
				<td>
					<xsl:value-of select="td[5]"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[6],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[7],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[8],'#,##0.00')"/>
				</td>
					<td align="right">
					<xsl:value-of select="format-number(td[9],'#,##0.00%')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[10],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[11],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[12],'#,##0.00%')"/>
				</td>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>