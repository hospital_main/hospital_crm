<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th>开支部门编码</th>
				<th>经费开支部门</th>
				<th>事件项目编码</th>
				<th>事件项目名称</th>
				<th>预算总额</th>
				<th>实际支出</th>
				<th>预算余额</th>
				<th>预警方式</th>
				<th>当前预警线(元)</th>
				<th>当前预警线(比例)</th>
				<th>差额</th>
			</tr>
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() &lt; 5">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=8">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=10">
								<td align="right"><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise>
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



