<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
		<thead>
			<tr>
		   		<td style="fontsize:maintitle;colspan:colcount;"></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr noWrap='true'  class="mainHead">
				<td style="fontsize:coltitle;">开支部门编码</td>
				<td style="fontsize:coltitle;">经费开支部门</td>
				<td style="fontsize:coltitle;">事件项目编码</td>
				<td style="fontsize:coltitle;">事件项目名称</td>
				<td style="fontsize:coltitle;">预算总额</td>
				<td style="fontsize:coltitle;">实际支出</td>
				<td style="fontsize:coltitle;">预算余额</td>
				<td style="fontsize:coltitle;">预警方式</td>
				<td style="fontsize:coltitle;">当前预警线(元)</td>
				<td style="fontsize:coltitle;">当前预警线(比例)</td>
				<td style="fontsize:coltitle;">差额</td>
			</tr>
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() &lt; 5">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position() = 8">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position() = 10">
								<td align="right"><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise>
								<td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>



