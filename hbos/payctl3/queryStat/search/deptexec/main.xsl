<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr class="mainHead">
				<td  align="center" rowspan="2">事件项目编码</td>
				<td  align="center" rowspan="2">事件项目名称</td>
				<td  align="center" colspan="3">年度预算</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr class="mainHead">
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td  align="center">预算金额</td>
				<td  align="center">实际报销</td>
				<td  align="center">执行进度</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<xsl:text disable-output-escaping="yes">
				&lt;tr&gt;
			</xsl:text>
				<td>
					<xsl:value-of select="td[1]"/>
				</td>
				<td>
					<xsl:value-of select="td[2]"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[3],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[4],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[5],'#,##0.00%')"/>
				</td>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>