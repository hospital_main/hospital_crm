<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class="mainHead">
      	<!--th></th-->
      	<th nowrap='true' valign='middle'>行程号</th>
      	<th nowrap='true' valign='middle'>出发日期</th>
      	<th nowrap='true' valign='middle'>时间</th>
      	<th nowrap='true' valign='middle'>地点</th>
      	<th nowrap='true' valign='middle'>到达日期</th>
      	<th nowrap='true' valign='middle'>时间</th>
      	<th nowrap='true' valign='middle'>地点</th>
      	<th nowrap='true' valign='middle'>说明</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position() = 1">
		            
		            <!--td>
			        	<input type="checkbox" >
			        		<xsl:attribute name="value">
			        			<xsl:value-of select="."/>
			        		</xsl:attribute>
			        	</input>
			        </td>
		            
		            <td>
		            <a href="#"><xsl:attribute name="onclick">
		            openDialog('add.html?<xsl:value-of select="."/>', 'dialogWidth:854px;dialogHeight:600px')
		            </xsl:attribute>
		            <xsl:value-of select="."/>
		            </a>
		            </td-->
		            <td><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position() = 2">
              	<td><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
              	<td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>		
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

