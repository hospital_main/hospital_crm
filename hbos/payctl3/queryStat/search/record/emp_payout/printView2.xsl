<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
		<tr>
			<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
		</tr>
		<tr>
			<td style="fontsize:coltitle">单据编号</td>
			<td style="fontsize:coltitle">制单日期</td>
			<td style="fontsize:coltitle">经费归口科室</td>
			<td style="fontsize:coltitle">报销事由</td>
			<td style="fontsize:coltitle">申报金额</td>
			<td style="fontsize:coltitle">预借金额</td>
			<td style="fontsize:coltitle">退补金额</td>
			<td style="fontsize:coltitle">支付状态</td>
			<td style="fontsize:coltitle">有效状态</td>
			<td style="fontsize:coltitle">经办人</td>
			<td style="fontsize:coltitle">制单人</td>
			<td style="fontsize:coltitle">附件张数</td>
		</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test=" position() = 5 or position() = 6 or position() = 7">
							<td align="right"><xsl:value-of select="format-number(.,'##,##0.00')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>          
				 </xsl:for-each>
			</tr>
		</xsl:for-each>5
 	</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>