<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class="mainHead">
      	<th nowrap='true' valign='middle'>行程号</th>
      	<th nowrap='true' valign='middle'>支出项目</th>
      	<th nowrap='true' valign='middle'>支出科目</th>
      	<th nowrap='true' valign='middle'>人/天</th>
      	<th nowrap='true' valign='middle'>报销金额</th>
      	<th nowrap='true' valign='middle'>申请金额</th>
      	<th nowrap='true' valign='middle'>可用额度</th>
      	<th nowrap='true' valign='middle'>是否超额度</th>
      	<th nowrap='true' valign='middle'>摘要</th>
      	<th nowrap='true' valign='middle' width='0' style="display:none">支出项目编码</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position() = 4 or position() = 5 or position() = 6 or position() = 7">
		            <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:when test="position() = 10">
		            <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
              	<td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>		
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

