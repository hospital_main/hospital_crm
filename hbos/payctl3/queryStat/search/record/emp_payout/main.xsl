<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <thead>
            <tr class='mainHead'>
                <th>单据编号</th>
                <th>制单日期</th>
                <th>经费归口科室</th>
                <th>报销事由</th>
                <th>申报金额</th>
                <th>预借金额</th>
                <th>退补金额</th>
                <th>支付状态</th>
                <th>有效状态</th>
                <th>经办人</th>
                <th>制单人</th>
                <th>附件张数</th>
                <th style="display:none">单据类型</th>
            </tr>
        </thead>
		<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<xsl:if test="position()=1">
						<td>
						<xsl:value-of select="."/>
						</td>
					</xsl:if>
					<xsl:if test="position()=5 or position()=6 or position()=7">
						<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
					</xsl:if>
					<xsl:if test="position()!=5 and position()!=6 and position()!=7 and position()!=1 and position() &lt; 13">
						<td><xsl:value-of select="."/></td>
					</xsl:if>
					<xsl:if test="position() &gt; 13">
					</xsl:if>
					<xsl:if test="position()=13">
						<td style="display:none" >
						<xsl:value-of select="."/>;<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
						</td>
					</xsl:if>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
		</tbody>
    </xsl:template>
</xsl:stylesheet>