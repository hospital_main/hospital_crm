<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
    </thead>
    <tbody>
  	
 <tr>
       
          <xsl:for-each select="/root/tbody/tr/td">
            <xsl:choose>
              <xsl:when test="position()=1">
							 <td colspan='2'>单号：<xsl:value-of select="."/></td>
              </xsl:when>
          </xsl:choose>
  			  </xsl:for-each>
          <xsl:for-each select="/root/tbody/tr/td">
            <xsl:choose>
              <xsl:when test="position()=2">
							 <td colspan='2' align='center'><xsl:value-of select="."/></td>
              </xsl:when>
          </xsl:choose>
  			  </xsl:for-each>
          <xsl:for-each select="/root/tbody/tr/td">
            <xsl:choose>
              <xsl:when test="position()=3">
							 <td colspan='2' align='left'>单据类型：<xsl:value-of select="."/></td>
              </xsl:when>
          </xsl:choose>
  			  </xsl:for-each>
  			  
   		</tr>
 
   		<tr>
   		 <td>部门</td>
   		 <xsl:for-each select="/root/tbody/tr/td">
   		 <xsl:choose>
              <xsl:when test="position()=4">
         				<td><xsl:value-of select="."></xsl:value-of></td>
         			</xsl:when>
         	</xsl:choose>
    		 </xsl:for-each>
    		  <td>申请人</td>
   		 <xsl:for-each select="/root/tbody/tr/td">
   		 <xsl:choose>
              <xsl:when test="position()=5">
         				<td><xsl:value-of select="."></xsl:value-of></td>
         			</xsl:when>
         	</xsl:choose>
    		 </xsl:for-each>
    		 
    		 <td>制单人</td>
   		 <xsl:for-each select="/root/tbody/tr/td">
   		 <xsl:choose>
              <xsl:when test="position()=6">
         				<td><xsl:value-of select="."></xsl:value-of></td>
         			</xsl:when>
         	</xsl:choose>
    		 </xsl:for-each>
   		</tr>
   		<tr>
   		 <td>项目名称</td>
   			<xsl:for-each select="/root/tbody/tr/td">
   						<xsl:choose>
				              <xsl:when test="position()=7">
				         				<td  colspan="5"><xsl:value-of select="."></xsl:value-of></td>
				         			</xsl:when>
				         	</xsl:choose>

   				</xsl:for-each>
   		</tr>
   		<tr>
   		<td>说 　明</td>
   			<xsl:for-each select="/root/tbody/tr/td">
   						<xsl:choose>
				              <xsl:when test="position()=8">
				         				<td colspan="5"><xsl:value-of select="."></xsl:value-of></td>
				         			</xsl:when>
				         	</xsl:choose>
   				</xsl:for-each>
   		</tr>
   		<tr align="center" >	
   				<td align="center" style=" font-weight: bold" colspan="6">申 		请 		明			 细</td>
   			
   			</tr>
   		<tr>
   				<td>支出项目</td><td>申请金额</td><td>可用额度 </td><td>是否超额度</td> <td colspan="2">摘　要</td> 
   			</tr>
   		<xsl:for-each select="/root/tbody/tr">
   		<tr>
   				<xsl:for-each select="td[position()=9]">
   						<td>
   							<xsl:value-of select="."></xsl:value-of>
   						</td>
				</xsl:for-each>		
				<xsl:for-each select="td[position()=10]">
   						<td>
   							<xsl:value-of select="."></xsl:value-of>
   						</td>
				</xsl:for-each>	
				<xsl:for-each select="td[position()=11]">
   						<td>
   							<xsl:value-of select="format-number(.,'##,##0.00')"></xsl:value-of>
   						</td>
				</xsl:for-each>	
				<xsl:for-each select="td[position()=12]">
   						<td>
   							<xsl:value-of select="format-number(.,'##,##0.00')"></xsl:value-of>
   						</td>
				</xsl:for-each>	
				<xsl:for-each select="td[position()=13]">
   						<td colspan="2">
   								<xsl:value-of select="."></xsl:value-of>
   						</td>
				</xsl:for-each>		
				</tr>	
   			</xsl:for-each>
   			<tr>
   				<td>申请金额（大写）</td>
   				<xsl:for-each select="/root/tbody/tr/td">
			            <xsl:choose>
			              <xsl:when test="position()=14">
										 <td colspan="2"><xsl:value-of select="."/></td>
			              </xsl:when>
			          </xsl:choose>
  			  </xsl:for-each>
  			     						
   				<td>（小写）</td>
   				<xsl:for-each select="/root/tbody/tr/td">
   						<xsl:choose>
				              <xsl:when test="position()=15">
				         				<td colspan="2"><xsl:value-of select="format-number(.,'##,##0.00')"></xsl:value-of></td>
				         			</xsl:when>
				         	</xsl:choose>
				</xsl:for-each>		

   			</tr>
   			<tr>
            <td height="70" colspan="1">主任签批</td>
            <td   height="70" 	colspan="5" 	>    </td>
          </tr>
          <tr>
             <td height="70" colspan="1">财务签批</td>
             <td   height="70" 	colspan="5" 	>    </td>
          </tr>
          <tr>
            <td height="70" colspan="1">院长签批</td>
					<td   height="70" 	colspan="5">    </td>
          </tr>   	
    </tbody>
  </xsl:template>
</xsl:stylesheet>

