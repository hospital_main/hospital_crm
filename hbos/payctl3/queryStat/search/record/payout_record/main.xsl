<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <thead>
            <tr class='mainHead'>
                <th>单据编号</th>
                <th>制单日期</th>
                <th>经费归口科室</th>
                <th>经费开支科室</th>
                <th>报销事由</th>
                <th>领款人</th>
                <th>申请人</th>
                <th>事件项目</th>
                <th>申报金额</th>
                <th>是否超预算</th>
                <th>支付状态</th>
                <th>有效状态</th>
                <th>制单人</th>
            </tr>
        </thead>
        <tbody>
            <xsl:for-each select="/root/tbody/tr">
                <tr>
                    <xsl:for-each select="td">
                    <xsl:if test="position()=1 and .!=''">
                    <td>
                        <a tabindex='-1'>
                            <xsl:attribute name="href" >#</xsl:attribute>
                            <xsl:attribute name="onclick" >
                            danju_type('<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
                            </xsl:attribute>
                            <xsl:value-of select="."/>
                        </a>
                    </td>
                    </xsl:if>
                    <xsl:if test="position()=9">
                    <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                    </xsl:if>
                    <xsl:if test="position()!=1 and position()!=9 and position()&lt;14">
                    <td><xsl:value-of select="."/></td>
                    </xsl:if>
                    <!--<xsl:if test="position()!=1 and position()&gt;13">
                    <td style="display:none;"><xsl:value-of select="."/></td>
                    </xsl:if>-->
                    </xsl:for-each>
				</tr>
				</xsl:for-each>
            <xsl:if test="count(/root/tbody/tr)>0">
			<tr>
				<td align="left"></td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="left">合计</td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[9]),'#,##0.00')"/></td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="left"></td>
				<td align="left"></td>
            </tr>
			</xsl:if>
        </tbody>
    </xsl:template>
</xsl:stylesheet>