<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
		<tr>
			<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
		</tr>
		<tr>
			<td style="fontsize:coltitle">单据编号</td>
			<td style="fontsize:coltitle">制单日期</td>
			<td style="fontsize:coltitle">经费归口科室</td>
			<td style="fontsize:coltitle">经费开支科室</td>
			<td style="fontsize:coltitle">报销事由</td>
			<td style="fontsize:coltitle">领款人</td>
			<td style="fontsize:coltitle">申请人</td>
			<td style="fontsize:coltitle">事件项目</td>
			<td style="fontsize:coltitle">申报金额</td>
			<td style="fontsize:coltitle">是否超预算</td>
			<td style="fontsize:coltitle">支付状态</td>
			<td style="fontsize:coltitle">有效状态</td>
			<td style="fontsize:coltitle">制单人</td>
		</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:if test="position()=1 and .!=''">
              <td>
  	          	<xsl:value-of select="."/>
			  </td>
            </xsl:if>
            <xsl:if test="position()=9">
              <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:if>
            <xsl:if test="position()!=1 and position()!=9">
              <td><xsl:value-of select="."/></td>
            </xsl:if>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
		<xsl:if test="count(/root/tbody/tr)>0">
	   		<tr>
	   			<td align="left"></td>
	   			<td align="left"></td>
	   			<td align="left"></td>
	   			<td align="left"></td>
	   			<td align="left">合计</td>
	   			<td align="left"></td>
	   			<td align="left"></td>
	   			<td align="left"></td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[9]),'#,##0.00')"/></td>
	   			<td align="left"></td>
	   			<td align="left"></td>
	   			<td align="left"></td>
	   			<td align="left"></td>
	   		</tr>
   		</xsl:if>
 	</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>