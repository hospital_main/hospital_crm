<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">14</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr class='mainHead'>
					<td>单据编号</td>
					<td> 经办人</td>
					<td> 科室</td>
					<td> 核算项目</td>
					<td> 申请说明</td>
					<td> 申请金额</td>
					<td> 实际报销额</td>
					<td> 会计期间</td>
					<td> 制单人</td>
					<td> 制单日期</td>
					<td> 确认状态</td>
					<td> 确认人</td>
					<td> 确认日期 </td>
					<td> 核销状态 </td>
		
			</tr>
			</thead>
			<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:if test="position()=1 and .!=''">
              <td>
  	          		<xsl:value-of select="."/>
  	          </td>
            </xsl:if>
            <xsl:if test="position()=1 and .=''">
              <td>
  	          		<xsl:value-of select="."/>
  	          </td>
            </xsl:if>
            <xsl:if test="position()=6 or position()=7 ">
              <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:if>            
            <xsl:if test="position()!=1 and position()!=6 and position()!=7 ">
              <td><xsl:value-of select="."/></td>
            </xsl:if>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>
