<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
			<tr class='mainHead'>
				<th>单据编号</th>
				<th> 经办人</th>
				<th> 经费归口部门</th>
				<th> 核算项目</th>
				<th> 申请说明</th>
				<th> 申请金额</th>
				<th  style="display:none;"> 实际报销额</th>
				<th> 会计期间</th>
				<th> 制单人</th>
				<th> 制单日期</th>
				<th> 确认状态</th>
				<th> 确认人</th>
				<th> 确认日期 </th>
				<th> 核销状态 </th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:if test="position()=1 and .!=''">
              <td>
                <a tabindex='-1'>
                  <xsl:attribute name="href" >
    	            	javascript:openDialog('ctrl_pay_apply_detail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:900px;dialogHeight:600px', result)
  	          		</xsl:attribute>
  	          		<xsl:value-of select="."/>
  	          	</a>
  	          </td>
            </xsl:if>
            <xsl:if test="position()=1 and .=''">
              <td>
  	          		<xsl:value-of select="."/>
  	          </td>
            </xsl:if>
            <xsl:if test="position()=6 ">
              <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:if>
			  <xsl:if test="position()=7">
				  <td align="right" style="display:none;"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			  </xsl:if>
            <xsl:if test="position()!=1 and position()!=6 and position()!=7">
              <td><xsl:value-of select="."/></td>
            </xsl:if>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>	
 		</tbody>
	</xsl:template>
</xsl:stylesheet>