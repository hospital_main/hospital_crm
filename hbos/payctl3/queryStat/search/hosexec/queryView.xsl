<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/payctl3/queryStat/search/hosexec/queryView.xsl,v 1.1 2012/03/12 01:55:26 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:55:26 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
			<xsl:variable name="month_Cols" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1] and td[2]=/root/tbody/tr[1]/td[2]])"/>
			<tr>
				<td style="colspan:colcount;fontsize:maintitle">全院执行情况</td>
				<td nowrap='true' style="display:none"/>
				<xsl:for-each select="/root/tbody/tr[position() &lt;= $month_Cols]">
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
				</xsl:for-each>
			</tr>
			<tr>
				<td style="fontsize:coltitle" align="center" rowspan="2">科目编码</td>
				<td style="fontsize:coltitle" align="center" rowspan="2">科目名称</td>
				<xsl:for-each select="/root/tbody/tr[position() &lt;= $month_Cols]">
				  <td style="fontsize:coltitle" align="center" colspan="3">
				  	<xsl:if test="td[3]='00'">
							年度预算
						</xsl:if>
				  	<xsl:if test="td[3]!='00'">
							<xsl:value-of select="td[3]"/>月
						</xsl:if>
					</td>
					<td nowrap='true' style="display:none"/>
					<td nowrap='true' style="display:none"/>
				</xsl:for-each>
			</tr>
			<tr>
				<td nowrap='true' style="display:none"/>
				<td nowrap='true' style="display:none"/>
				<xsl:for-each select="/root/tbody/tr[position() &lt;= $month_Cols]">
					<td style="fontsize:coltitle" align="center" >预算金额</td>
					<td style="fontsize:coltitle" align="center" >实际执行</td>
					<td style="fontsize:coltitle" align="center" >执行进度%</td>
				</xsl:for-each>
			</tr>
		</thead>
		<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:if test="( position() mod $month_Cols )=1 or $month_Cols = 1">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;
				</xsl:text>
				<td>
					<xsl:value-of select="td[1]"/>
				</td>
				<td>
					<xsl:value-of select="td[2]"/>
				</td>
			</xsl:if>
			
			<xsl:if test="(position() mod $month_Cols)!=0">
				<td align="right">
					<xsl:value-of select="format-number(td[4],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[5],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[6],'#,##0.00')"/>
				</td>
			</xsl:if>
			
			<xsl:if test="(position() mod $month_Cols)=0 or $month_Cols = 1">
				<td align="right">
					<xsl:value-of select="format-number(td[4],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[5],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[6],'#,##0.00')"/>
				</td>
				<xsl:text disable-output-escaping="yes">
					&lt;/tr&gt;
				</xsl:text>
			</xsl:if>
			
		</xsl:for-each>
	</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>

<!--?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
				<tr>
					<td style="colspan:colcount;fontsize:maintitle">全院执行情况</td>
					<xsl:for-each select="/root/tbody/tr[1]/td[position()&gt;2]">
						<td nowrap='true' style="display:none"/>
					</xsl:for-each>
				</tr>
				<xsl:variable name="cols" select="string-length(/root/tbody/tr[1]/td[1])"/>
				<tr>
				<td class="mainHead" align="center" rowspan="2">科目编码</td>
				<td class="mainHead" align="center" rowspan="2">科目名称</td>
				<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 3]">
				  <td class="mainHead" align="center">
				  	<xsl:if test=".!='' and .='年度预算'">
						<xsl:attribute name="colspan">3</xsl:attribute>
					</xsl:if>
				  	<xsl:if test=".!='' and .!='年度预算'">
						<xsl:attribute name="colspan"><xsl:value-of select="$cols"/></xsl:attribute>
					</xsl:if>
				  	<xsl:if test=".=''">
						<xsl:attribute name="style">display:none</xsl:attribute>
					</xsl:if>
						<xsl:value-of select="."/>
					</td>
				</xsl:for-each>
			</tr>
			<tr>
				<td style="display:none"/>
				<td style="display:none"/>
				<xsl:for-each select="/root/tbody/tr[2]/td[position() &gt; 3]">
				  <td class="mainHead" align="center">
					<xsl:value-of select="."/>
					</td>
				</xsl:for-each>
			</tr>
		</thead>
		<tbody>
		<xsl:for-each select="/root/tbody/tr[ position() &gt; 2]">
			<tr>
			<xsl:for-each select="td[position()!=1]">
				<xsl:choose>
					<xsl:when test="position()=1 or position()=2">
						<td><xsl:value-of select="."/></td>
					</xsl:when>
					<xsl:otherwise>
						<td align="right"><xsl:value-of select="format-number(.,'##,##0.00')"/></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet-->
