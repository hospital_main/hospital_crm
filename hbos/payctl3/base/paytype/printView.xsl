<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>方式编码</td>
				<td nowrap='true'>方式名称</td> 
			</tr>           
		</thead>
		<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">  
					<td align="left">
						<xsl:value-of select="."/>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>