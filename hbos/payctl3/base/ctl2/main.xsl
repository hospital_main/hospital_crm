<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
			
				<!-- <th nowrap="true" align="center">是否控制<input type='checkbox' disabled='true' flag='1' onclick="SetAll(this)"/></th> -->
				<th nowrap="true" align="center">是否控制</th>
				<!--th nowrap="true" align="center">是否明细控制<input type='checkbox'   flag='2' onclick="SetAll(this)"/></th-->
				<th nowrap="true">预算科目编码</th>
				<th nowrap="true">预算科目名称</th>
				<!--
				<th nowrap="true">科室名称</th>
				-->
				<th nowrap="true">控制周期</th>
				<!--th style="display:none1">设置</th>
				<th style="display:none"/-->
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">
					
						<xsl:if test="position() = 1">
							<td align="center" >
								<input type='checkbox' flag='1' onclick="">
								<xsl:if test=".='1'">
									<xsl:attribute name="checked" >true
									</xsl:attribute>
								</xsl:if>
								</input>
							</td>
						</xsl:if>
						
						<!--xsl:if test="position() = 2">
							<td align="center">
								<xsl:if test="../td[6]  = '1'">
									<input type='checkbox' flag='2'>
									<xsl:if test=".='1'">
										<xsl:attribute name="checked">true
										</xsl:attribute>
									</xsl:if>
									</input>
									
								</xsl:if>
								<xsl:if test="../td[6]  != '1'">
									<input type='checkbox' style="display:none" flag='2'/>
								</xsl:if>
							</td>
						</xsl:if-->
						<xsl:if test="position() = 2">
							<td >
								<xsl:value-of select='.'/>
							</td>
						</xsl:if>
						<xsl:if test="position() = 3">
							<td >
								<xsl:value-of select='.'/>
							</td>
						</xsl:if>
						
						
						<xsl:if test="position() = 4">
							<td>
								<select name='aa'>
									<option vlaue="1">
										<xsl:if test=". = '1'">
											<xsl:attribute name="selected">true</xsl:attribute>
										</xsl:if>年
									</option>
									<option vlaue="3">
										<xsl:if test=". = '3'">
											<xsl:attribute name="selected">true</xsl:attribute>
										</xsl:if>半年
									</option>
									<option vlaue="4">
										<xsl:if test=". = '4'">
											<xsl:attribute name="selected">true</xsl:attribute>
										</xsl:if>季
									</option>
									<option vlaue="2">
										<xsl:if test=". = '2'">
											<xsl:attribute name="selected">true</xsl:attribute>
										</xsl:if>月
									</option>
								</select>
							</td>
						</xsl:if>
						<!--xsl:if test="position() = 6">
							<td>
								<xsl:if test=".='1'">
									<a href="#" >
										<xsl:attribute name="onclick">
											openDialog('budgSubjDept.html?<xsl:value-of select='../td[3]'/>||<xsl:value-of select='../td[4]'/>','dialogWidth:680px;dialogHeight:550px;')
										</xsl:attribute>
										设置
									</a>
								</xsl:if>
							</td>
						</xsl:if-->
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
