<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style=''>是否控制</th>
        <th style=''>是否全院</th>
      	<th nowrap='true'>科室编码</th>
      	<th nowrap='true'>科室名称</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <xsl:if test="td[4] = '0'">	
           	 <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="changeEnable(this);setSelect1();">
            </input>
            </xsl:if>
            <xsl:if test="td[4] != '0'">	
           	 <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="changeEnable(this);setSelect1();">
           	 <xsl:attribute name="checked">true</xsl:attribute>
            </input>
            </xsl:if>
            
          </td>
          
          <xsl:for-each select="td">
      
            
            <xsl:if test="position() = 1">  
              <td align="center">
              	<input type='checkbox' flag="2" onclick="setChecked(this)">
	              	<xsl:if test=".='%'">
	              		<xsl:attribute name="checked">true</xsl:attribute>
	                </xsl:if>
	                <xsl:if test="../td[5] = '0'">
	                	<xsl:attribute name="style">display:none</xsl:attribute>
	                </xsl:if>
                </input>
              </td>
            </xsl:if>
            
              <xsl:if test="position() = 2">  
              <td>
                <xsl:value-of select="."/>  
              </td>
            </xsl:if>
            <xsl:if test="position() = 3">  
              <td>
                <xsl:value-of select="."/>  
              </td>
            </xsl:if>
            <xsl:if test="position() = 4 or position() = 5">  
              <td style="display:none">
                <xsl:value-of select="."/>  
              </td>
            </xsl:if>
            <xsl:if test="position() = 5">  
              <td style="display:none">
                <xsl:value-of select="."/>  
              </td>
            </xsl:if>
            
            
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

