<?xml version='1.0' encoding="GBK"?>
<!--xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl"-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:for-each select="/root/para">"<xsl:value-of select="@code"/>" Any <xsl:value-of select="@value"/>(<xsl:value-of select="@para_define"/>)
BEGIN_HELP
<xsl:value-of select="@fun_formate"/>
  
<xsl:value-of select="@fun_note"/>
END_HELP
</xsl:for-each>
  <!--
    <xsl:for-each select="/root/para">"<xsl:value-of select="@code"/>" Any <xsl:value-of select="@value"/>(<xsl:value-of select="@para_define"/>)\nBEGIN_HELP\n<xsl:value-of select="@fun_formate"/>\n<xsl:value-of select="@fun_note"/>\nEND_HELP\n</xsl:for-each>
    -->
  </xsl:template>
</xsl:stylesheet>


