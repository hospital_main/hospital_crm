<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th width="300px" nowrap="true">
					<input type='checkbox' onclick="selectAll(this)"/>是否显示可用额度
				</th>
				<th nowrap="true">控制功能点</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() = 1 or position()=2 or position()=3">
								<td style="display:none">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position() = 4">
								<td align="center">
									<input type='checkbox' >
									<xsl:attribute name="value"><xsl:value-of select="../td[2]"/></xsl:attribute>
										<xsl:if test=".='1'">
											<xsl:attribute name="checked">true</xsl:attribute>
										</xsl:if>
									</input>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
