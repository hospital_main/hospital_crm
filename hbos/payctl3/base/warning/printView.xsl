<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<root>
	<thead>
		<xsl:variable name="month_Cols" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1] and td[2]=/root/tbody/tr[1]/td[2]])"/>
		<tr>
			<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
		<tr>
			<td align="center">经费归口部门名称</td>
			<td align="center">经费开支部门名称</td>
			<td align="center">事件项目编码</td>
			<td align="center">事件项目名称</td>
			<td align="center">预算总额</td>
			<td align="center">预警方式</td>
			<td align="center">年预警线</td>
			<td align="center">数据类型</td>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
			  	<xsl:for-each select="td">
				<xsl:choose>
					<xsl:when test="position()=5">
						<td align='right' noWrap='true'>
							<xsl:value-of select="."/>
						</td>
					</xsl:when>
					<xsl:when test="position()=7">
						<td align='right' noWrap='true'>
							<xsl:value-of select="."/>
						</td>
					</xsl:when>
					<xsl:otherwise>
						<td align="left"><xsl:value-of select="."/></td>
					</xsl:otherwise>
				</xsl:choose>
				  </xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>