function submitData(obj){
	var res=checkInputs();
	if(res==null||res=="")
		return ;
	window.xmlhttp.post(obj.name, res,"")
	var str = window.xmlhttp._object.responseText
	if (!window.doMsg(str,"")) {
	  return false;
	}
	//sysDictsUnitinfoCopyParas_select.click();
}
function checkInputs(){
  var para0311Value = "";
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
    var res = "";
	for(var i=0;i<trs.length;i++){
		if(trs[i].getAttribute("_editParaType")==null)
			continue;
		if(trs[i]._paraCode=='0311'){
		    para0311Value=trs[i]._editInputValue;
			  res+="<record>" + trs[i]._editPk+"<value>"+trs[i]._editInputValue+"</value>";
        } else if (trs[i]._paraCode == '1007') {
            var inputvalue = trs[i].cells[2].getElementsByTagName("input")[0].value;
            if (checkValue(trs[i], trs[i]._editDataType, inputvalue)) {
                if (isNaN(inputvalue)) {
                    alert("数值格式不正确，数字范围1位整数，1位小数（格式：0.1～1.0），请填写正确的数字格式！");
                    break;
                } else if (inputvalue.indexOf('.') >= 0 && inputvalue.substring(inputvalue.indexOf('.') + 1, inputvalue.length).length > 1) {
                    alert("数值格式不正确，数字范围1位整数，1位小数（格式：0.1～1.0），请填写正确的数字格式！");
                    break;
                }
                var destvalue = parseFloat(inputvalue).toFixed(1);
                if (destvalue > 1.0) {
                    alert("数值格式不正确，数字范围1位整数，1位小数（格式：0.1～1.0），请填写正确的数字格式！");
                    break;
                } else if (destvalue <= 0.0) {
                    alert("数值不能为0或负数，数字范围1位整数，1位小数（格式：0.1～1.0），请填写正确的数字格式！");
                    break;
                }
                res += "<record>";
                res += trs[i]._editPk + "<value>" + destvalue + "</value>";
            }
        } else {
		 if((trs[i]._paraCode=='0312' || trs[i]._paraCode=='0313') && para0311Value=='否'){
  			  res+="<record>" + trs[i]._editPk+"<value>否</value>";		    
		  }else {
  		 if(trs[i]._editDataValue==trs[i]._editInputValue)
  			 continue;
  
    		res+="<record>";
   		  if(checkValue(trs[i],trs[i]._editDataType,trs[i]._editInputValue)){
  			  res+=trs[i]._editPk+"<value>"+trs[i]._editInputValue+"</value>";
    		}
    	}
	  }
		res+="</record>";
	}
	return res;
}
function checkValue(tr,type,value){
	var msg=tr.cells[0].innerText+" "+tr.cells[1].innerText+" :要求是" ;
	var res=true;
	if(type=="1"&&value!=""){
		msg+=" 文本 ";
		res=true;
	}else if(type=="2"&&IsDate(value)){
		msg+=" 日期 ";
		var d= CDate(value);
		var year=d.getFullYear();
		var month=d.getMonth()+1;
		var day=d.getDate();
		year='0000'+year;year=year.substring(year.length-4)
	  	month='00'+month;month=month.substring(month.length-2)
	  	day='00'+day; day=day.substring(day.length-2)
	 	tr._editInputValue=year+'-'+month+'-'+day;
		res=true;
	}else  if(type=="3"){
		msg+=" 编码规则 ";
		res=isRuleCode(value);
	}else if(type=="4"){
		msg+=" 正整数 ";
		var v=parseInt(value,10);
		if(isNaN(v)||v<0)
			res=false;
	}else if(type=="5"){
		var v=parseInt(value,10);
		if(isNaN(v))
			res=false;
	}
	if(res==false)
		alert(msg);
	return res;
}
function initInputs(){
	var table=document.getElementById("_mainDataTable");
	//alert(table.outerHTML)
	var trs=table.getElementsByTagName("tr");
	for(var i=0;i<trs.length;i++)
		initTrInputs(trs[i]);
}
function initTrInputs(tr){
	var pt=tr.getAttribute("_editParaType");
	var dt=tr.getAttribute("_editDataType");
	var dv=tr.getAttribute("_editDataValue");
	var pc=tr.getAttribute("_paraCode");
	var pv=tr.getAttribute("_paraValue");
	
	if(pt==null||dt==null)
		return ;
	var inp="";
	var id=getJsGuid();
	var tag="";
	var chk="";
    tr._editInputId = id;

	if(pt=="1"){ //1：编辑框
        if (pc == "1007") {
            inp = "<input type='text' id='" + id + "' name='" + id + "' value='" + dv + "'maxLength='3' size='3'/>";
        } else {
            inp = "<input type='text' id='" + id + "' name='" + id + "' value='" + dv + "'/>";
        }
        tag = "input";
	}else if(pt=="2"){//2：下拉框
		var ops=tr.cells[3].innerText.split("\/");
		/*
		if(pc=="0311"){
	  	inp="<select id='"+id+"' name='"+id+"' onChange='changeValue(this)'>";
	  	g_0311_value = dv ;
	  	g_seleSubj = pv ;
	  	
			if(dv=="是"){
			  seleAcct.innerHTML = "<a href=# onclick=openDialog('setBudgSubj.html','dialogWidth:680px;dialogHeight:650px;')>科目设置</a>";
			}else{
			  seleAcct.innerHTML = "";
			}
	  	
  	}else{
  	*/
		  inp="<select id='"+id+"' name='"+id+"'>";
	  //}
	  
		tag="select"
		for(var i=0;i<ops.length;i++){
			if(dv==ops[i])
				chk=" selected ";
			else
				chk="";

   		inp+="<option "+chk+" value='"+ops[i]+"'>"+ops[i]+"</option>";
		}
		inp+="</select>";
	}else if(pt=="3"){//3：选项按钮
		var ops=tr.cells[3].innerText.split("\/");
		inp="";
		tag="input"
		for(var i=0;i<ops.length;i++){
			if(dv==ops[i])
				chk=" checked ";
			else
				chk="";
			inp+="<input type='radio' "+chk+" name='"+id+"' value='"+ops[i]+"'/>"+ops[i];
			if(i+1!=ops.length)
				inp+="<br/>";
		}
	}else
		return ;
	tr.cells[2].innerHTML=inp;
	var inpus=tr.cells[2].getElementsByTagName("input");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	inpus=tr.cells[2].getElementsByTagName("select");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	
}
function setInputChange(tr,inp){
	//inp.onblur=
	//inp.onclick=
	
	inp.onkeyup=function(){tr._editInputValue=this.value;};

}