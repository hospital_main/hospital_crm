<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style="display:none"><input type="checkbox"/></th>
				<th nowrap='true'>项目编码</th>
				<th nowrap='true'>项目名称</th> 
			</tr>           
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'  style='display:none'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
    			  </xsl:attribute>
  			  </input>
        </td>
				<xsl:for-each select="td">   
					<xsl:choose>
						<xsl:when test="position() = 1">
							<td align="left">
								<a href="#">
									<xsl:attribute name="onclick">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
									</xsl:attribute><xsl:value-of select="."/>
								</a>
							</td>
						</xsl:when>
						<xsl:when test="position() = 2">
							<td align="left">
								<xsl:value-of select="."/>  
							</td>
						</xsl:when>
						<xsl:otherwise>
							
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

