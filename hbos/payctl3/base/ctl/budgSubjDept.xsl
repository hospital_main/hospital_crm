<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style=''>�Ƿ����</th>
      	<th nowrap='true'>���ұ���</th>
      	<th nowrap='true'>��������</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <xsl:if test="td[3] = '0'">	
           	 <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="changeEnable(this)">
            </input>
            </xsl:if>
            <xsl:if test="td[3] != '0'">	
           	 <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="changeEnable(this)">
           	 <xsl:attribute name="checked">true</xsl:attribute>
            </input>
            </xsl:if>
          </td>
          
          <xsl:for-each select="td">
            <xsl:if test="position() &lt; 3">  
              <td>
                <xsl:value-of select="."/>  
              </td>
            </xsl:if>
            <xsl:if test="position() = 3">  
              <td style="display:none">
                <xsl:value-of select="."/>  
              </td>
            </xsl:if>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

