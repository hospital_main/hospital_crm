<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style=''>是否控制</th>
      	<th nowrap='true'>科目编码</th>
      	<th nowrap='true'>科目名称</th>
      	<th nowrap='true'>控制周期</th>
      	
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <xsl:if test="td[3] = '0'">	
           	 <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="changeEnable(this)">
            </input>
            </xsl:if>
            <xsl:if test="td[3] != '0'">	
           	 <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="changeEnable(this)">
           	 <xsl:attribute name="checked">true</xsl:attribute>
            </input>
            </xsl:if>
          </td>
          
          <xsl:for-each select="td">
            <xsl:if test="position() &lt; 3">  
              <td>
                <xsl:value-of select="."/>  
              </td>
            </xsl:if>
            <xsl:if test="position() = 4">  
              	<td>
					<select name='aa'>
						<option value="0"></option>
						<option vlaue="1">
							<xsl:if test=". = '1'">
								<xsl:attribute name="selected">true</xsl:attribute>
							</xsl:if>年
						</option>
						<option vlaue="2">
							<xsl:if test=". = '2'">
								<xsl:attribute name="selected">true</xsl:attribute>
							</xsl:if>月
						</option>
					</select>
                
              </td>
            </xsl:if>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

