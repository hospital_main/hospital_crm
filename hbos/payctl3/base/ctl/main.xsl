<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th nowrap="true">预算科目编码</th>
				<th nowrap="true">预算科目名称</th>
				<th nowrap="true">科室名称</th>
				<th nowrap="true">控制周期</th>
				<th nowrap="true">科目控制类型</th>
				<th nowrap="true">科室控制类型</th>
				<th style="display:none"/>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:if test="position() &lt; 4">
							<td>
								<xsl:value-of select="."/>
							</td>
						</xsl:if>
						<xsl:if test="position() = 4">
							<td>
								<select name='aa' disabled="true">
									<option vlaue="1">
										<xsl:if test=". = '1'">
											<xsl:attribute name="selected">true</xsl:attribute>
										</xsl:if>年
									</option>
									<option vlaue="2">
										<xsl:if test=". = '2'">
											<xsl:attribute name="selected">true</xsl:attribute>
										</xsl:if>月
									</option>
								</select>
							</td>
						</xsl:if>
						
						<xsl:if test="position() = 5">
							<td>
									<select name='budg_acct_ctrl' disabled="true">
									
									<xsl:if test=".='2'"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
									<option vlaue='1'><xsl:if test=". = '1'"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>本级控制</option>
									<option vlaue='2'><xsl:if test=". = '2'"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>上级控制</option>
									<option vlaue='3'><xsl:if test=". = '3'"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>明细控制</option>
									<option vlaue='4'><xsl:if test=". = '4'"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>不控制</option>
									</select>
							</td>
						</xsl:if>
						<xsl:if test="position() = 6">
							<td>
									<select name='budg_acct_ctrl' disabled="true">
									<option vlaue='1'><xsl:if test=". = '1'"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>本级控制</option>
									<option vlaue='2'><xsl:if test=". = '2'"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>上级控制</option>
									<option vlaue='3'><xsl:if test=". = '3'"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>不控制</option>
									</select>
							</td>
						</xsl:if>
						
						<!--
            <xsl:if test="position() &lt; 7">  
              <td>
                  <xsl:value-of select="."/>  
              </td>
            </xsl:if>
            <xsl:if test="position() = 7">  
              <td style="display:none">
                <xsl:value-of select="."/>  
              </td>
            </xsl:if>
            -->
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
