<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th nowrap="true">科目名称</th>
				<th nowrap="true">项目编码</th>
				<th nowrap="true">项目名称</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() = 1">
								<td style="display:none">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position() = 3">
								<td>
								<a>
									<xsl:attribute name="href">
											javascript:loadData("<xsl:for-each select="../*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute>
									<xsl:value-of select="."/>
								</a>
								</td>
							</xsl:when>
							<xsl:otherwise>
							<td>
								<xsl:value-of select="."/>
							</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
