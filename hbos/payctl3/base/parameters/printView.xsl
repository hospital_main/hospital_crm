<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
	<thead>
		<xsl:variable name="month_Cols" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1] and td[2]=/root/tbody/tr[1]/td[2]])"/>
		<tr>
			<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'></td>
		</tr>
		<tr>
			<td align="center">预算调整类别编码</td>
			<td align="center">预算调整类别名称</td>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
        <tr>
	        <xsl:for-each select="td">
	          		<xsl:if test="position()=1 or position()=2">
	                <td align='left' noWrap='true'><xsl:value-of select="."/></td>
	              </xsl:if>
			  			<xsl:value-of select="."/>
	  			</xsl:for-each>
	  		</tr>
		</xsl:for-each>
	</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>