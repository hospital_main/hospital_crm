<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th >选择</th>
				<th nowrap='true' >公式代码</th>
				<th nowrap='true' >公式名称</th>
				<th nowrap='true' >计算公式</th>		
				<th nowrap='true' >公式说明</th>																
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" >
						<input type="radio" TABINDEX="-1" name="radiofactor" onclick="facotrPro(this)"  >
							<xsl:attribute name="formula_code"><xsl:value-of select="td[1]"/></xsl:attribute>
							<xsl:attribute name="formula_name"><xsl:value-of select="td[2]"/></xsl:attribute>
							<xsl:attribute name="formula_chn"><xsl:value-of select="td[4]"/></xsl:attribute>						
							<xsl:attribute name="value"><xsl:for-each select="pk/*"><xsl:value-of select="."/></xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
						<xsl:when test="position()=3" >
								<td align="left">									
																																											
										<xsl:value-of select="." />
															
								</td>
							</xsl:when>
							
							<xsl:when test="position()=2" >
								<td align="left">									
									<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
										<xsl:attribute name="formula_code"><xsl:value-of select="../td[1]"/></xsl:attribute>				
										<xsl:attribute name="formula_name"><xsl:value-of select="../td[2]"/></xsl:attribute>	
										<xsl:attribute name="comment"><xsl:value-of select="../td[4]"/></xsl:attribute>	
										<xsl:attribute name="is_stop"><xsl:value-of select="../td[5]"/></xsl:attribute>	
										<xsl:attribute name="is_weight"><xsl:value-of select="../td[6]"/></xsl:attribute>																																			
										<xsl:value-of select="." />  
									</a>								
								</td>
							</xsl:when>
							
							
							<xsl:when test="position()=4" >
								<td align="left">									
								
										<xsl:attribute name="formula_code"><xsl:value-of select="../td[1]"/></xsl:attribute>				
										<xsl:attribute name="formula_name"><xsl:value-of select="../td[2]"/></xsl:attribute>		
										<xsl:attribute name="is_stop"><xsl:value-of select="../td[5]"/></xsl:attribute>	
										<xsl:attribute name="is_weight"><xsl:value-of select="../td[6]"/></xsl:attribute>																																			
										<xsl:value-of select="." />  
															
								</td>
							</xsl:when>	
							<xsl:when test="position()=5" >
							</xsl:when>	
							<xsl:when test="position()=6" >
							</xsl:when>																									
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
