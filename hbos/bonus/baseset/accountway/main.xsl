<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>核算方式代码</th>
  			<th nowrap='true'>核算方式名称</th>
  			<th nowrap='true'>核算方式说明</th>
  		</tr>
  	</thead>
  	
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <xsl:variable name="img_path" select="td[4]"/>
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          
          <xsl:for-each select="td[position() &lt; 4]">
            <td>
              <xsl:choose>
                <xsl:when test="position()=3">
                	<xsl:if test="not(../td[1]='003')">
	                <a tabindex='-1' href="#" >
	                  <xsl:attribute name="onclick">
	    	            	javascript:viewDialog(&quot;<xsl:value-of select="$img_path"/>&quot;);
	  	          		</xsl:attribute><xsl:value-of select="."/>
	  	          	</a>
                	</xsl:if>
                	<xsl:if test="(../td[1]='003')">
	  	          		<xsl:value-of select="."/>
                	</xsl:if>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>