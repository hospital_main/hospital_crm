<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' align="center">科室代码</th>
				<th nowrap='true' width="35%" align="center">科室名称</th>
				<th nowrap='true' width="35%" align="center">上级科室</th>
				<th nowrap='true' align="center">所属科室分类</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td[position() &lt; 5]">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 4">
								<input type="text" name="perf_unit_code" load="perf_unit_select" required="false" class="inputSelect">
									<xsl:attribute name="initValue"><xsl:value-of select="."/></xsl:attribute>
									<xsl:attribute name="para">&lt;stratagem_sequence_no&gt;<xsl:value-of select="../td[5]"/>&lt;/stratagem_sequence_no&gt;&lt;state_flag&gt;<xsl:value-of select="../td[6]"/>&lt;/state_flag&gt;</xsl:attribute>
									<xsl:attribute name="dept_code"><xsl:value-of select="../td[1]"/></xsl:attribute>
								  <xsl:attribute name="dept_name"><xsl:value-of select="../td[2]"/></xsl:attribute>
								  <xsl:attribute name="super_code"><xsl:value-of select="../td[3]"/></xsl:attribute>
									<xsl:attribute name="stratagem_sequence_no"><xsl:value-of select="../td[5]"/></xsl:attribute>
								</input>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>