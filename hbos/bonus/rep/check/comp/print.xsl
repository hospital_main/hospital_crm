<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
  	<thead>
				<tr>
					<td style="fontsize:maintitle;colspan:6"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>  	
  		<tr noWrap='true' class='mainHead'>
		    <td>行</td>
		  	<td>列</td>
		  	<td>项目编码</td>
		  	<td>项目公式</td>
		  	<td>审核结果</td>
  	  	<td>错误提示</td>
  		</tr>
  	</thead>

  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <td><xsl:value-of select="."/></td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>

