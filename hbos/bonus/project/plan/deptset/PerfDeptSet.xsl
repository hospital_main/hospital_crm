<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style="display:none"><input type="checkbox"/></th>
				<th nowrap='true' align="center">科室代码</th>
				<th nowrap='true' width="35%" align="center">科室名称</th>
				<th nowrap='true' width="35%" align="center">上级科室</th>
				<th nowrap='true' align="center">所属科室分类</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'  style='display:none'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
    			  </xsl:attribute>
  			  </input>
        </td>
        <xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position() = 2 and ../td[7]='2'" >
						<td>
							<a href='#'>
							<xsl:attribute name="onclick">
    	            		 	javascript:openDialog('update.html?load=<xsl:for-each select="../td[position()=1 or position()=2 or position()=4 or position()=8]">&lt;seq_no&gt;<xsl:value-of select="../td[1]"/>&lt;/seq_no&gt;&lt;dept_name&gt;<xsl:value-of select="../td[2]"/>&lt;/dept_name&gt;&lt;dept_type&gt;<xsl:value-of select="../td[4]"/>&lt;/dept_type&gt;&lt;spell&gt;<xsl:value-of select="../td[8]"/>&lt;/spell&gt;</xsl:for-each>', 'dialogWidth:570px;dialogHeight:280px', result)
  	          				</xsl:attribute>
								<xsl:value-of select="."/>
							</a>
						</td>
						</xsl:when>


						<xsl:when test="position() = 3 and .= '00000'  " >
							<td></td>
						</xsl:when>

						<xsl:when test="position() = 4">
							<td>
								<input type="text" name="perf_unit_code" load="perf_unit_select1"  required="false" class="inputSelectS">
									<xsl:attribute name="initValue"><xsl:value-of select="."/></xsl:attribute>
									<xsl:attribute name="para">&lt;stratagem_sequence_no&gt;<xsl:value-of select="../td[5]"/>&lt;/stratagem_sequence_no&gt;&lt;state_flag&gt;<xsl:value-of select="../td[6]"/>&lt;/state_flag&gt;</xsl:attribute>
									<xsl:attribute name="dept_code"><xsl:value-of select="../td[1]"/></xsl:attribute>
								  <xsl:attribute name="dept_name"><xsl:value-of select="../td[2]"/></xsl:attribute>
								  <xsl:attribute name="super_code"><xsl:value-of select="../td[3]"/></xsl:attribute>
									<xsl:attribute name="stratagem_sequence_no"><xsl:value-of select="../td[5]"/></xsl:attribute>
								</input>
							</td>
						</xsl:when>
						<xsl:when test="position() = 5 or position() = 6 or position() = 7 or position() = 8">
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>