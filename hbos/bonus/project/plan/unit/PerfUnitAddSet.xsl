<?xml version="1.0" encoding="gb2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
  <thead>
  <xsl:variable name="TotalRow" select="count(/root/tbody/tr)"/>
  <xsl:variable name="DetailCode" select="/root/tbody/tr[1]/td[5]"/>
  <xsl:variable name="TotalCol" select="count(/root/tbody/tr[td=$DetailCode])"/>
  <xsl:variable name="LoopStep" select="$TotalRow div $TotalCol"/>
   <tr noWrap="true" class="mainHead">
    <th nowrap="true" rowspan="2" valign="middle">科室分类代码</th>
    <th nowrap="true" rowspan="2" valign="middle">科室分类名称</th>
    <xsl:for-each select="/root/tbody/tr">
      <xsl:variable name="cur_pos" select="position()"/>
      <xsl:variable name="kind_code" select="../tr[$cur_pos - 1]/td[4]"/>
      <xsl:variable name="kind_code_t" select="td[4]"/>
     <xsl:if test="position() &lt;= $LoopStep and ( $cur_pos = 1 or $kind_code != $kind_code_t )">
      <th nowrap="true" width="200">
       <xsl:attribute name="colspan"><xsl:value-of select="td[8]"/></xsl:attribute>
       <xsl:value-of select="td[6]"/>
      </th>
     </xsl:if>
    </xsl:for-each>
   </tr>
   <tr noWrap="true" class="mainHead">
    <xsl:for-each select="/root/tbody/tr">
      <xsl:if test="position() &lt;= $LoopStep">
      <th nowrap="true" width="100">
       <xsl:value-of select="td[7]"/>
      </th>
     </xsl:if>
    </xsl:for-each>
   </tr>
  </thead>
		<tbody>  
			 <xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="Step" select="position() mod $LoopStep"/> 
				<xsl:if test="$LoopStep = 1 or $Step = 1">
					<xsl:text disable-output-escaping="yes"><![CDATA[<tr><td noWrap="true">]]></xsl:text>
					<xsl:value-of select="td[2]"/>
					<xsl:text disable-output-escaping="yes"><![CDATA[</td><td noWrap="true">]]></xsl:text>
					<xsl:value-of select="td[3]"/>
					<xsl:text disable-output-escaping="yes"><![CDATA[</td>]]></xsl:text>
				</xsl:if>
				<td align="right">
				  <xsl:attribute name="align">center</xsl:attribute>
				  <xsl:variable name="is_checked" select="td[9]"/>
					<input type='checkbox' name='is_checked' onclick='IsChecked_OnClick();'>
					  <xsl:attribute name="main_sequence_no"><xsl:value-of select="td[1]"/></xsl:attribute>
					  <xsl:attribute name="perf_kind_code"><xsl:value-of select="td[4]"/></xsl:attribute>
					  <xsl:attribute name="perf_kind_detail_code"><xsl:value-of select="td[5]"/></xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="td[9]"/></xsl:attribute>
						<xsl:if test=" $is_checked = 1">
							<xsl:attribute name="checked">true</xsl:attribute>
						</xsl:if>
					</input>
               </td>
				<xsl:if test="$LoopStep = 1 or $Step = 0">
					<xsl:text disable-output-escaping="yes"><![CDATA[</tr>]]></xsl:text>
				</xsl:if>
			</xsl:for-each> 
		</tbody>
	</xsl:template>
</xsl:stylesheet>
	