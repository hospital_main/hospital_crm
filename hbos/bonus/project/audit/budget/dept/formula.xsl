<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th >选择</th>
				<th>指标代码</th>
				<th>指标名称</th>
				<th>说明</th>
				<th>期间类型</th>
				<th>性质</th>
				<th>函数名称</th>
				<th>公式名称</th>
			</tr>
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'>
						<input type='radio' name="selRadio" onclick="selValue(this)">
							<xsl:attribute name="f_code" ><xsl:value-of select="td[1]"/></xsl:attribute>
							<xsl:attribute name="f_text" ><xsl:value-of select="td[2]"/></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<td>
							<xsl:choose>
								<xsl:when test="position()=2">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:when test="position()=7">
									<a href="#">
									<xsl:attribute name="onclick">
										openFormulaPage("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
									</xsl:attribute>
									<xsl:value-of select="."/></a>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



