<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>选择</th>
				<th>指标代码</th>
				<th>指标名称</th>
				<th>说明</th>
				<th>期间类型</th>
				<th>性质</th>
				<th>函数名称</th>
				<th style="display:none">公式代码</th>
				<th>公式名称</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
          			<td align="center" >
						<input type="radio" TABINDEX="-1" name="radiofactor" onclick="selFactor(this)" >
							<xsl:attribute name="factor_code"><xsl:value-of select="td[1]"/></xsl:attribute>
							<xsl:attribute name="factor_name"><xsl:value-of select="td[2]"/></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=7">
								<td style="display:none;">
                					<xsl:value-of select="." />
                				</td>
							</xsl:when>
							<xsl:when test="position()=8">
								<td>
									<a href="#">
										<xsl:attribute name="onclick">
											openPage('../../../../baseset/targetset/hosptargetset/getvaluemset/formula.html?load=&lt;a&gt;<xsl:value-of select="../td[7]"/>&lt;/a&gt;');
										</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>