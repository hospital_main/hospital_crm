<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<tbody>
			<xsl:if test="count(/root/tbody/tr[td[9]=1])&gt;0">
				<tr>
					<td colspan="7" class="table_title_css">全院奖金核算方案</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr>
					<td class="table_mainHead">项目名称</td>
					<td class="table_mainHead" colspan="2">期间类型</td>
					<td class="table_mainHead" style="display:none"/>
					<td class="table_mainHead" colspan="2">性质</td>
					<td class="table_mainHead" style="display:none"/>
					<td class="table_mainHead">函数名称</td>
					<td class="table_mainHead">公式名称</td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[9]=1]">
					<tr>
						<xsl:for-each select="td[position()!=8 and position()!=9 and position()!=10]">
							<td>
								<xsl:choose>
									<xsl:when test="position()=1">
										<xsl:attribute name="title"><xsl:value-of select="../td[8]"/></xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=2 or position()=4">
										<xsl:attribute name="colspan">2</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=3 or position()=5">
										<xsl:attribute name="style">display:none</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=7">
										<a href="#">
											<xsl:attribute name="onclick">
											openFormulaPage("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="count(/root/tbody/tr[td[9]=2])&gt;0">
				<tr>
					<td colspan="8" class="table_title_css">
						<br/>单项目奖金核算方案</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr>
					<td class="table_mainHead">项目名称</td>
					<td class="table_mainHead">是否虚拟</td>
					<td class="table_mainHead">期间类型</td>
					<td class="table_mainHead" colspan="2">性质</td>
					<td class="table_mainHead" style="display:none"/>
					<td class="table_mainHead">函数名称</td>
					<td class="table_mainHead">公式名称</td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[9]=2]">
					<tr>
						<xsl:for-each select="td[position()!=8 and position()!=9 and position()!=10]">
							<td>
								<xsl:choose>
									<xsl:when test="position()=1">
										<xsl:attribute name="title"><xsl:value-of select="../td[8]"/></xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=4">
										<xsl:attribute name="colspan">2</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=5">
										<xsl:attribute name="style">display:none</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=7">
										<a href="#">
											<xsl:attribute name="onclick">
											openFormulaPage("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="count(/root/tbody/tr[td[9]=3])&gt;0">
				<tr>
					<td colspan="8" class="table_title_css">
						<br/>科室类别奖金核算方案</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr>
					<td class="table_mainHead">项目名称</td>
					<td class="table_mainHead">是否虚拟</td>
					<td class="table_mainHead">期间类型</td>
					<td class="table_mainHead" colspan="2">性质</td>
					<td class="table_mainHead" style="display:none"/>
					<td class="table_mainHead">函数名称</td>
					<td class="table_mainHead">公式名称</td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[9]=3]">
					<tr>
						<xsl:for-each select="td[position()!=8 and position()!=9 and position()!=10]">
							<td>
								<xsl:choose>
									<xsl:when test="position()=1">
										<xsl:attribute name="title"><xsl:value-of select="../td[8]"/></xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=4">
										<xsl:attribute name="colspan">2</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=5">
										<xsl:attribute name="style">display:none</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=7">
										<a href="#">
											<xsl:attribute name="onclick">
											openFormulaPage("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="count(/root/tbody/tr[td[9]=4 and td[10]=03])&gt;0">
				<tr>
					<td colspan="8" class="table_title_css">
						<br/>科室奖金核算方案</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr>
					<td class="table_mainHead">项目名称</td>
					<td class="table_mainHead">科室类别</td>
					<td class="table_mainHead">是否虚拟</td>
					<td class="table_mainHead">期间类型</td>
					<td class="table_mainHead">性质</td>
					<td class="table_mainHead">函数名称</td>
					<td class="table_mainHead">公式名称</td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[9]=4 and td[10]=03]">
					<tr>
						<xsl:for-each select="td[position()!=8 and position()!=9 and position()!=10]">
							<td>
								<xsl:choose>
									<xsl:when test="position()=1">
										<xsl:attribute name="title"><xsl:value-of select="../td[8]"/></xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=7">
										<a href="#">
											<xsl:attribute name="onclick">
											openFormulaPage("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="count(/root/tbody/tr[td[9]=4 and td[10]=04])&gt;0">
				<tr>
					<td colspan="8" class="table_title_css">
						<br/>干部奖金核算方案</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr>
					<td class="table_mainHead">项目名称</td>
					<td class="table_mainHead">科室类别</td>
					<td class="table_mainHead">是否虚拟</td>
					<td class="table_mainHead">期间类型</td>
					<td class="table_mainHead">性质</td>
					<td class="table_mainHead">函数名称</td>
					<td class="table_mainHead">公式名称</td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[9]=4 and td[10]=04]">
					<tr>
						<xsl:for-each select="td[position()!=8 and position()!=9 and position()!=10]">
							<td>
								<xsl:choose>
									<xsl:when test="position()=1">
										<xsl:attribute name="title"><xsl:value-of select="../td[8]"/></xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=7">
										<a href="#">
											<xsl:attribute name="onclick">
											openFormulaPage("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="count(/root/tbody/tr[td[9]=5 and td[10]=03])&gt;0">
				<tr>
					<td colspan="8" class="table_title_css">
						<br/>科室奖金分配方案</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr>
					<td class="table_mainHead">项目名称</td>
					<td class="table_mainHead">科室类别</td>
					<td class="table_mainHead">是否虚拟</td>
					<td class="table_mainHead">期间类型</td>
					<td class="table_mainHead">性质</td>
					<td class="table_mainHead">函数名称</td>
					<td class="table_mainHead">公式名称</td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[9]=5 and td[10]=03]">
					<tr>
						<xsl:for-each select="td[position()!=8 and position()!=9 and position()!=10]">
							<td>
								<xsl:choose>
									<xsl:when test="position()=1">
										<xsl:attribute name="title"><xsl:value-of select="../td[8]"/></xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=7">
										<a href="#">
											<xsl:attribute name="onclick">
											openFormulaPage("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="count(/root/tbody/tr[td[9]=5 and td[10]=04])&gt;0">
				<tr>
					<td colspan="8" class="table_title_css">
						<br/>干部奖金分配方案</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr>
					<td class="table_mainHead">项目名称</td>
					<td class="table_mainHead">科室类别</td>
					<td class="table_mainHead">是否虚拟</td>
					<td class="table_mainHead">期间类型</td>
					<td class="table_mainHead">性质</td>
					<td class="table_mainHead">函数名称</td>
					<td class="table_mainHead">公式名称</td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[9]=5 and td[10]=04]">
					<tr>
						<xsl:for-each select="td[position()!=8 and position()!=9 and position()!=10]">
							<td>
								<xsl:choose>
									<xsl:when test="position()=1">
										<xsl:attribute name="title"><xsl:value-of select="../td[8]"/></xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=7">
										<a href="#">
											<xsl:attribute name="onclick">
											openFormulaPage("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</xsl:if>
		</tbody>
	</xsl:template>
</xsl:stylesheet>