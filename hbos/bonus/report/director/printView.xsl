<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNums"/>    
        </xsl:call-template>
  		</tr>
  		<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=1">
	    			<tr noWrap='true' class='mainHead'>
	    				<td>���</td>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()!=6 and position()!=1">
			          		<td ><xsl:value-of select="."/></td>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=6">
			          		<td ><xsl:value-of select="."/></td>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:variable name="paravalue" select="/root/tbody/tr[1]/td[ last()]"/>
	    	<xsl:choose>
	    		<xsl:when test="position()>1">
	    			<tr>
		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		
			          	<xsl:when test="position()!=6 and position() &gt; 5">
			            	<td align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test="position()!=6 and position() &lt;= 5">
			            	<xsl:if test=" . = 'ZZZZZZ' ">
			            	<td></td>
			            	</xsl:if>
			            	<xsl:if test=" . != 'ZZZZZZ' ">
			            	<td  ><xsl:value-of select="."/></td>
			            	</xsl:if>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=6">
			            	<td align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>      
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>