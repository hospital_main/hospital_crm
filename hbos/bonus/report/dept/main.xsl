<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
  		<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test="position()=1">
	    			<tr noWrap='true' class='mainHead'>
	    				<th>���</th>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()!=5 and position()!=1">
			          		<th ><xsl:value-of select="."/></th>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=5">
			          		<th ><xsl:value-of select="."/></th>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:variable name="paravalue" select="/root/tbody/tr[1]/td[ last()]"/>
	    	<xsl:choose>
	    		<xsl:when test="position()>1">
	    			<tr>
		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		
			          	<xsl:when test="position()!=5 and position() &gt; 4">
			            	<td align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test="position()!=5 and position() &lt;= 4">
			            	<xsl:if test=" . = 'ZZZZZZ' ">
			            	<td></td>
			            	</xsl:if>
			            	<xsl:if test=" . != 'ZZZZZZ' ">
			            	<td  ><xsl:value-of select="."/></td>
			            	</xsl:if>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test="position()=5">
			            	<td style="background-color:#99FFCD;font-weight: bold;font-family:'����'" align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>      
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>