<!-- $Header: /cvsroot/HBOS/HBOS3.0/06\040Source/ROOT/hbos/bonus/report/deptsum/printView.xsl,v 
	1.5 2010-03-04 07:43:41 lzk Exp $ $Author: zhoulidong $ $Date: 2010-03-04 07:43:41 
	$ $Revision: 1.1 $ -->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<table>
			<thead>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:choose>
						<xsl:when test="position()=last()">
							<tr noWrap='true' class='mainHead'>
								<td style="colspan:colcount;fontsize:maintitle"></td>
								<xsl:for-each select="td[position()&gt;1]">
									<xsl:choose>
										<xsl:when test="position()=1 or position()=3 ">

										</xsl:when>
										<xsl:otherwise>
											<td nowrap='true'>
												<xsl:value-of select="." />
											</td>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</tr>
							<tr noWrap='true' class='mainHead'>
								<xsl:for-each select="td">
									<xsl:choose>
										<xsl:when test="position()=1 or position()=3 ">

										</xsl:when>
										<xsl:otherwise>
 

												<td nowrap='true'>
													<xsl:value-of select="." />
												</td>
										 

										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</tr>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td align="center">�ϼ�</td>
					<xsl:for-each select="/root/tbody/tr[1]/td">
						<xsl:variable name="c" select="position()" />
						<xsl:if test="$c &gt; 4">
							<td align="right">
								<xsl:value-of
									select="format-number(sum(/root/tbody/tr[position()!=last()]/td[$c]),'##,##0.00')" />
							</td>
						</xsl:if>
					</xsl:for-each>
				</tr>
				<xsl:for-each select="/root/tbody/tr[position()!=last()]">

					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1 or position()=3 ">
								</xsl:when>

								<xsl:when test="position()=2">
									<td align="left">
										<xsl:value-of select="." />
									</td>
								</xsl:when>
								<xsl:when test="position()=4">
									<td align="left">
										<xsl:value-of select="." />
									</td>
								</xsl:when>

								<xsl:otherwise>
									<td align="right">
										<!-- <xsl:value-of select="format-number(.,'##,##0.00')"/> -->
										<xsl:value-of select="." />
									</td>



								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>

				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>