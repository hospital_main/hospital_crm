<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/bonus/report/deptsum/printView2.xsl,v 1.1 2012/03/12 01:46:16 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:46:16 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-2"/>
		<table>
			<thead>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr[position()!=last()]">
					<xsl:for-each select="/root/tbody/tr">
		    	<xsl:choose>
		    		<xsl:when test="position()=last()">
		    			<tr noWrap='true' class='mainHead'>
		    				<td style='colspan:colcount' align="center" >
		    					<xsl:attribute name="colspan" >
          					<xsl:value-of select="$colNum"/>
		            	</xsl:attribute>
				        	<xsl:value-of select="/root/annex/thead_1_1"/>
				        </td>
		    				<xsl:call-template name="repeat">
				  				<xsl:with-param name="times" select="$colNum - 1 "/>    
				        </xsl:call-template>
		    			</tr>
		    			<tr noWrap='true' class='mainHead'>
				    		<xsl:for-each select="td">
			          	<xsl:choose>
				          	<xsl:when test="position()=1 or position()=3 ">
				          	</xsl:when>
				          	<xsl:otherwise>
				          		<td nowrap='true'><xsl:value-of select="."/></td>
			              </xsl:otherwise>
			            </xsl:choose>
			          </xsl:for-each>
		          </tr>
		        </xsl:when>
		      </xsl:choose>
		    </xsl:for-each>
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1 or position()=3 ">
								</xsl:when>
								<xsl:when test="position()=2">
									<td align="left">
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=4">
									<td align="left">
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td align="right">
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>            
						</xsl:for-each>
					</tr>
					<tr>
		        <td style='colspan:colcount' align="center" >
	  					<xsl:attribute name="colspan" >
	    					<xsl:value-of select="$colNum"/>
	          	</xsl:attribute>
		        </td>
	  				<xsl:call-template name="repeat">
		  				<xsl:with-param name="times" select="$colNum - 1 "/>    
		        </xsl:call-template>	
	  			</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>