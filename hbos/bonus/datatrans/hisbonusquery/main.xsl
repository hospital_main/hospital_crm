<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">类型</th>
				<th>核算对象</th>
				<th>考核年度</th>
				<th>考核期间</th>
				<th>绩效奖金</th>
				<th>其他奖金</th>
				<th>合计</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td style="display:none;">
		                			<xsl:value-of select="." />
		                		</td>
							</xsl:when>
							<xsl:when test="position()=2">
								<xsl:choose>
									<xsl:when test="../td[1] = 2">
										<td>--<xsl:value-of select="." /></td>
									</xsl:when>
									<xsl:when test="../td[1] = 3">
										<td>----<xsl:value-of select="." /></td>
									</xsl:when>
									<xsl:otherwise>
										<td><xsl:value-of select="." /></td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:when test="position() = 5 or position() = 6 or position() = 7">
								<td align="right"><xsl:value-of select="format-number(., '#.00')" /></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="." /></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>