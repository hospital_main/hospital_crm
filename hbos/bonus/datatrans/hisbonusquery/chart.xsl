<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:variable name="total" select="/root/tbody/tr[1]/td[5]" />
		<root>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:if test="td[1] = 2">
					<xsl:variable name="typename" select="td[2]" />
					<xsl:variable name="typevalue" select="td[5]"/>
					<xsl:variable name="percent" select="($typevalue div $total)" />
					<item>
						<name><xsl:value-of select="$typename" /></name>
						<value><xsl:value-of select="$percent" /></value>
					</item>
				</xsl:if>
			</xsl:for-each>
			
		</root>
	</xsl:template>
</xsl:stylesheet>