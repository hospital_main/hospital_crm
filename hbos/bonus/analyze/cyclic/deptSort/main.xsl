<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th>期间类型</th>
				<th>考核年度</th>
				<th>考核期间</th>
				<th>待分配奖金额</th>
				<th>待分环比比率</th>
				<th>实分配金额</th>
				<th>实分环比比率</th>
				<th>差额</th>
				<th>差额环比比率</th>
			</tr>
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() = 4 or position() = 6 or position() = 8">
								<td class="numberText">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
							</xsl:when>
							<xsl:when test="position() = 5 or position() = 7 or position() = 9">
								<td align="right">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>