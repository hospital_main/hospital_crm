<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
	  	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
  		<tr noWrap='true' class='mainHead'>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">指标代码</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">指标名称</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">期间类型</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">考核年度</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">考核期间</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">指标值</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">计量单位</td>
  		</tr>
  	</thead>
	<tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:variable name="tdlast" select="./td[1]" />
 				<xsl:variable name="mpos" select="position()-1" />
 				<xsl:variable name="rowspan_coun" select="count(/root/tbody/tr[td[1]=$tdlast])" />
        <tr>
			      <xsl:if test="position()=1">
			      	 <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[1]" />
			   			 </td>  
			      	 <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[2]" />
			   			 </td>  
			      	 <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[3]" />
			   			 </td>  
			      	 <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[4]" />
			   			 </td>  
						</xsl:if> 
						
						<xsl:if test="position()>1">
					    <xsl:if test="$tdlast!=(../tr[$mpos]/td[1])">
			      	
			      	 <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[1]" />
			   			 </td>  
			      	 <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[2]" />
			   			 </td>  
			      	 <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[3]" />
			   			 </td>  
			      	 <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[4]" />
			   			 </td>  
			   			</xsl:if> 
					</xsl:if> 
			      	 <td><xsl:value-of select="td[5]" /></td>  
			      	 <td align='right'>
			      	 		<xsl:value-of select="format-number(td[6],'#,##0.00')" />
			      	 </td>  
			      	 <td><xsl:value-of select="td[7]" /></td>  
        </tr>
      </xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

