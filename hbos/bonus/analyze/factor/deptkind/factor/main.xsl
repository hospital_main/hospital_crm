<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
			<td align="center">科室类别</td>
			<td align="center">期间类型</td>
			<td align="center">考核年度</td>
			<td align="center">考核期间</td>
			<td align="center">指标值</td>
			<td align="center">计量单位</td>
  		</tr>
  	</thead>
	<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position() = 5">
                <td class="numberText" style="background-color:#99FFCC">
                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
                 </td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
            
          </xsl:for-each>
        </tr>
      </xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

