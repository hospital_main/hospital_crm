<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>科室名称</th>
				<th>科室类别</th>
				<xsl:for-each select="/root/tbody/tr">
					<th><xsl:value-of select="td[3]"/></th>
				</xsl:for-each>
			</tr>
		</thead>
		<tbody>
			<tr>
				<xsl:for-each select="/root/tbody/tr">
					<td>
						<xsl:choose>
							<xsl:when test="td[2] = none_code">
								<xsl:choose>
									<xsl:when test="td[1] = 3">
										<td bgColor='#99FFCC'>
											<xsl:value-of select="td[4]"></xsl:value-of>
										</td>
									</xsl:when>
									<xsl:otherwise>
										<td>
											<xsl:value-of select="td[4]"></xsl:value-of>
										</td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<a href="#">
										<xsl:attribute name="onclick">
											openPage('factor_detail.html?load=&lt;b&gt;<xsl:value-of select="td[5]"/>&lt;/b&gt;&lt;c&gt;<xsl:value-of select="td[6]"/>&lt;/c&gt;&lt;d&gt;<xsl:value-of select="td[7]"/>&lt;/d&gt;&lt;e&gt;<xsl:value-of select="td[8]"/>&lt;/e&gt;&lt;f&gt;<xsl:value-of select="td[9]"/>&lt;/f&gt;&lt;g&gt;<xsl:value-of select="td[10]"/>&lt;/g&gt;&lt;h&gt;<xsl:value-of select="td[11]"/>&lt;/h&gt;&lt;i&gt;<xsl:value-of select="td[12]"/>&lt;/i&gt;&lt;j&gt;<xsl:value-of select="td[13]"/>&lt;/j&gt;&lt;k&gt;<xsl:value-of select="td[14]"/>&lt;/k&gt;');
										</xsl:attribute>
										<xsl:value-of select="td[4]"></xsl:value-of>
									</a>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>