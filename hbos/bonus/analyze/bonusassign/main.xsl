<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
		  <xsl:variable name="factor_level" select="/root/tbody/tr[1]/td[2]" />
		  <xsl:variable name="factor_property" select="/root/tbody/tr[1]/td[5]" />
		  <xsl:variable name="first_page" select="/root/tbody/tr[1]/td[6]" />
			<tr noWrap="true" class="mainHead">
			<xsl:if test="$factor_level='B001'">
				<th>核算对象</th>
			</xsl:if>
			<xsl:if test="$factor_level='B002'">
				<th>科室类别</th>
			</xsl:if>
			<xsl:if test="$factor_level='B003'">
				<th>科室类别</th>
				<th>科室名称</th>
			</xsl:if>
				<xsl:variable name="first_dept_name">
					<xsl:choose>
						<xsl:when test="/root/tbody/tr[1]/td[1] = 'dataA'">
							<xsl:choose>
								<xsl:when test="/root/tbody/tr[2]/td[1] = 'dataB'">
									<xsl:value-of select="/root/tbody/tr[2]/td[2]" />
								</xsl:when>
								<xsl:otherwise>
									none
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="/root/tbody/tr[1]/td[1] = 'dataB'">
									<xsl:value-of select="/root/tbody/tr[1]/td[2]" />
								</xsl:when>
								<xsl:otherwise>
									none
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:if test="td[1] = 'dataB' and td[2] = $first_dept_name">
						<th><xsl:value-of select="td[7]"/></th>
					</xsl:if>
				</xsl:for-each>
				
			<xsl:if test="$first_page='0'">
				<th>奖金额</th>
			</xsl:if>
			<xsl:if test="$first_page='1'">
				<th>指标值</th>
			</xsl:if>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="pos" select="position()" />
				<xsl:variable name="prior_deptname">
					<xsl:choose>
						<xsl:when test="$pos = 2">
							none
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="../tr[$pos - 1]/td[2]" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="cur_deptname">
					<xsl:value-of select="td[2]" />
				</xsl:variable>
				<xsl:if test="position() > 1">
					<xsl:if test="$prior_deptname = none or $cur_deptname != $prior_deptname">
						<tr>
							<xsl:if test="$factor_level='B001'">
								<td>全院</td>
							</xsl:if>
							<xsl:if test="$factor_level='B002'">
								<td><xsl:value-of select="td[4]" /></td>
							</xsl:if>
							<xsl:if test="$factor_level='B003'">
								<td><xsl:value-of select="td[4]" /></td>
								<td><xsl:value-of select="td[2]" /></td>
							</xsl:if>
							<xsl:for-each select="/root/tbody/tr">
								<xsl:if test="position() > 1">
									<xsl:if test="td[2] = $cur_deptname">
										<xsl:choose>
											<xsl:when test="td[6] = 'none_code'">
												<xsl:choose>
													<xsl:when test="td[1] = 'dataC'">
														<td align="right" bgColor='#99FFCC'>
															<xsl:value-of select="format-number(td[8], '#.00')" />
														</td>
													</xsl:when>
													<xsl:otherwise>
														<td align="right">
															<xsl:value-of select="format-number(td[8], '#.00')" />
														</td>
													</xsl:otherwise>
												</xsl:choose>
												
											</xsl:when>
											<xsl:otherwise>
												<td align="right">
		<xsl:if test="$factor_property='cal'">												
													<a href="#">
														<xsl:attribute name="onclick">
																
															openPage('main.html?load='
															+'&lt;factor&gt;<xsl:value-of select="td[9]"/>&lt;/factor&gt;'
															+'&lt;firstPage&gt;false&lt;/firstPage&gt;'
															+'&lt;dept_code&gt;<xsl:value-of select="td[3]"/>&lt;/dept_code&gt;'
															);
														</xsl:attribute>
														<xsl:value-of select="td[8]"></xsl:value-of>
													</a>
		</xsl:if>
		<xsl:if test="$factor_property!='cal'">												
														<xsl:value-of select="td[8]"></xsl:value-of>
		</xsl:if>
												</td>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:if>
								</xsl:if>
							</xsl:for-each>
						</tr>
					</xsl:if>
				</xsl:if>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>