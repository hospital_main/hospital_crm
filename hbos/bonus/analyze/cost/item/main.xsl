<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
			<td align="center">科室名称</td>
			<td align="center">直接成本</td>
			<td align="center">分摊公用成本</td>
			<td align="center">分摊管理成本</td>
			<td align="center">分摊医疗辅助成本</td>
			<td align="center">分摊医疗技术成本</td>
			<td align="center">成本金额</td>
  		</tr>
  	</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=7">
              	<td class="numberText" bgcolor="#99FFCC">
               		<xsl:value-of select="format-number(.,'#,##0.00')"/>
               	</td>
              </xsl:when>
              <xsl:when test="position()&gt;1">
              	<td class="numberText">
               		<xsl:value-of select="format-number(.,'#,##0.00')"/>
               	</td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>            
          </xsl:for-each>
        </tr>
      </xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>