<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:variable name="item_property" select="/root/tbody/tr[1]/td[8]" />
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>项目名称</th>
				<th>分配方法</th>
				<th>方法说明</th>
				<th>科室名称</th>
				<th>奖金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="pos" select="position()" />
				<xsl:variable name="prior_prjname">
					<xsl:choose>
						<xsl:when test="$pos = 1">
							none
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="../tr[$pos - 1]/td[1]" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="cur_prjname">
					<xsl:value-of select="td[1]" />
				</xsl:variable>
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() = 1 or position() = 5">
								<xsl:if test="$prior_prjname = none or $cur_prjname != $prior_prjname">
									<td>
										<xsl:attribute name="rowspan">
											<xsl:value-of select="../td[4]" />
										</xsl:attribute>
										<xsl:value-of select="." />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position() = 3">
								<xsl:if test="$prior_prjname = none or $cur_prjname != $prior_prjname">
									<td>
										<xsl:attribute name="rowspan">
										<xsl:value-of select="../td[4]" />
										</xsl:attribute>
										<a href="#">
											<xsl:attribute name="onclick">
												openPage('../../baseset/targetset/hosptargetset/getvaluemset/formula.html?load=&lt;a&gt;<xsl:value-of select="../td[2]"/>&lt;/a&gt;');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position() = 6">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position() = 7">
								<td align="right"><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position() = 8">
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>