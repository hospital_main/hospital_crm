<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			
					<xsl:variable name="factor_desc" select="/root/tbody/tr/td[1]" />
					<item name="{$factor_desc}">
						<xsl:for-each select="/root/tbody/tr">
							<xsl:variable name="deptname" select="td[6]" />
							<xsl:variable name="money" select="td[7]" />
							<item name="{$deptname}" value="{$money}"/>
						</xsl:for-each>
					</item>
		</root>
	</xsl:template>
</xsl:stylesheet>