<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>科室名称</th>
				<th>项目名称</th>
				<th>期间类型</th>
				<th>考核年度</th>
				<th>考核期间</th>
				<th>奖金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="pos" select="position()" />
				<xsl:variable name="dept_name" select="td[1]" />
				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$dept_name])" />

				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() = 1">
								<xsl:if test="$pos = 1 or $dept_name != ../../tr[$pos - 1]/td[1]">
									<td rowspan="{$rowspan}">
										<xsl:value-of select="." />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position() > 1 and position() != last()">
								<td><xsl:value-of select="." /></td>
							</xsl:when>
							<xsl:when test="position() = last()">
								<td align="right" style="background-color:#99FFCC;" >
									<xsl:value-of select="format-number(., '#0.00')"/>
								</td>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>