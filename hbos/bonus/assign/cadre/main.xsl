<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>干部名称</th>
				<th>项目名称</th>
				<th>期间类型</th>
				<th>考核年度</th>
				<th>考核期间</th>
				<th>奖金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="pos" select="position()" />
				<xsl:variable name="prior_deptname">
					<xsl:choose>
						<xsl:when test="$pos = 1">
							none
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="../tr[$pos - 1]/td[1]" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="cur_deptname">
					<xsl:value-of select="td[1]" />
				</xsl:variable>
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() = 1">
								<xsl:if test="$prior_deptname = 'none' or $cur_deptname != $prior_deptname">
									<td>
										<xsl:attribute name="rowspan">
											<xsl:value-of select="../td[2]" />
										</xsl:attribute>
										<xsl:value-of select="." />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position() > 2 and position() != last()">
								<td><xsl:value-of select="." /></td>
							</xsl:when>
							<xsl:when test="position() = last()">
								<td align="right" style="background-color:#99FFCC;" >
									<xsl:value-of select="format-number(., '#0.00')"/>
								</td>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>