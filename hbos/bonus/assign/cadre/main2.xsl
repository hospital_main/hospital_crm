<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>干部名称</th>
				<th>科室名称</th>
				<xsl:variable name="first_dept_name">
					<xsl:choose>
						<xsl:when test="/root/tbody/tr[1]/td[1] = 'dataA'">
							<xsl:choose>
								<xsl:when test="/root/tbody/tr[2]/td[1] = 'dataB'">
									<xsl:value-of select="/root/tbody/tr[2]/td[2]" />
								</xsl:when>
								<xsl:otherwise>
									none
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="/root/tbody/tr[1]/td[1] = 'dataB'">
									<xsl:value-of select="/root/tbody/tr[1]/td[2]" />
								</xsl:when>
								<xsl:otherwise>
									none
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:if test="td[1] = 'dataB' and td[2] = $first_dept_name">
						<th><xsl:value-of select="td[4]"/></th>
					</xsl:if>
				</xsl:for-each>
				<th>奖金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="pos" select="position()" />
				<xsl:variable name="prior_deptname">
					<xsl:choose>
						<xsl:when test="$pos = 2">
							none
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="../tr[$pos - 1]/td[2]" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="cur_deptname">
					<xsl:value-of select="td[2]" />
				</xsl:variable>
				<xsl:if test="td[1] = 'dataB' or td[1] ='dataC'">
					<xsl:if test="$prior_deptname = none or $cur_deptname != $prior_deptname">
						<tr>
							<td>
								<xsl:value-of select="td[2]" />
							</td>
							<td>
								<xsl:value-of select="td[3]" />
							</td>
							<xsl:for-each select="/root/tbody/tr">
								<xsl:if test="td[1] = 'dataB' or td[1] ='dataC'">
									<xsl:if test="td[2] = $cur_deptname">
										<xsl:choose>
											<xsl:when test="td[1] = 'dataC'">
												<td align="right" style="background-color:#99FFCC;" >
													<xsl:value-of select="format-number(td[5], '#0.00')"/>
												</td>
											</xsl:when>
											<xsl:otherwise>
												<td align="right">
													<xsl:value-of select="format-number(td[5], '#0.00')"/>
												</td>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:if>
								</xsl:if>
							</xsl:for-each>
						</tr>
					</xsl:if>
				</xsl:if>
			</xsl:for-each>
			<tr style="display:none;">
				<td>
					<a href="#" id="adata">
						<xsl:attribute name="period_type">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[2]" />
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="year">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[3]" />
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="period">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[4]" />
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="item_name">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[5]" />
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="factor_prop">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[6]" />
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="fun_name">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[7]" />
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="formula">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[8]" />
							</xsl:if>
						</xsl:attribute>
						
					</a>
				</td>
			</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>