<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="lzk_hc" select="count(/root/tbody/tr[td[12]=/root/tbody/tr[1]/td[12]])"/>
		<tr class="mainHead">
			<th >科室名称</th>
			<th >干部名称</th>
			<th >期间类型</th>
			<th >考核年度</th>
			<th >考核期间</th>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $lzk_hc]">
				<th >
					<xsl:value-of select="td[5]"/>
				</th>
			</xsl:for-each>
		</tr>
	</thead>
	<tbody>
		<xsl:variable name="lzk_bc" select="count(/root/tbody/tr[td[12]=/root/tbody/tr[1]/td[12]])"/>
		<xsl:variable name="lzk_opt" select="/root/tbody/tr[1]/td[5]"/>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:if test="( position() mod $lzk_bc )=1 or $lzk_bc = 1">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;
				</xsl:text>
				<td>
					<xsl:value-of select="td[1]"/>
				</td>
				<td>
					<xsl:value-of select="td[12]"/>
				</td>
				<td>
					<xsl:value-of select="td[2]"/>
				</td>
				<td>
					<xsl:value-of select="td[3]"/>
				</td>
				<td>
					<xsl:value-of select="td[4]"/>
				</td>
			</xsl:if>
				<td>
						<xsl:if test=" td[5]='分配额' ">
							<xsl:attribute name="style">text-align:right;background-color:#99FFCD;</xsl:attribute>
						</xsl:if>
						<xsl:if test=" td[5]!='分配额' ">
							<xsl:attribute name="style">text-align:right;</xsl:attribute>
						</xsl:if>
						<xsl:attribute name="pk1"><xsl:value-of select="td[7]"/></xsl:attribute>
						<xsl:attribute name="distribute_audit_no"><xsl:value-of select="td[11]"/></xsl:attribute>
						
						<xsl:value-of select="format-number(td[6],'#,##0.00')" disable-output-escaping="yes"/>
				</td>
			
			<xsl:if test="(position() mod $lzk_bc)=0 or $lzk_bc = 1">
				<xsl:text disable-output-escaping="yes">
					&lt;/tr&gt;
				</xsl:text>
			</xsl:if>
		</xsl:for-each>
		
		<xsl:if test="count(/root/tbody/tr)>0">
		<tr>
			<td>
			<xsl:attribute name="colspan">
					<xsl:value-of select="$lzk_bc + 3"/>
			</xsl:attribute>
			合计
			</td>
			<td id="sumTdId">
				<xsl:attribute name="style">text-align:right;</xsl:attribute>
				<xsl:value-of select="format-number(sum(/root/tbody/tr[td[5]='合计']/td[6]),'#,##0.00')"/>
			</td>
			<td id="vouchTdId">
				<xsl:attribute name="style">text-align:right;background-color:#99FFCD;</xsl:attribute>
				<xsl:value-of select="format-number(sum(/root/tbody/tr[td[5]='分配额']/td[6]),'#,##0.00')"/>
			</td>
			</tr>
			</xsl:if>
			
	</tbody>
	</xsl:template>
</xsl:stylesheet>

