<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>指标代码</th>
				<th>指标名称</th>
				<th>说明</th>
				<th>期间类型</th>
				<th>性质</th>
				<th>状态</th>
			</tr>
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
							<!--xsl:if test="'已完成'!=td[6]"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
							<xsl:if test="'已完成'!=td[6]"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
							<xsl:if test="'已完成'=td[6]"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>-->
						</input>
					</td>
					<xsl:for-each select="td">
						<td>
							<xsl:choose>
								<xsl:when test="position()=2">
									<a href="#">
									<xsl:attribute name="onclick">
										openUpdatePage("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
									</xsl:attribute>
									<xsl:attribute name="title">
										<xsl:value-of select="../td[3]"/>
									</xsl:attribute>
									<xsl:value-of select="."/></a>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



