<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">收入项目类</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">本科开单本科执行金额</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">本科开单本科执行权重</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">本科开单他科执行金额</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">本科开单他科执行权重</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">本科执行他科开单金额</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">本科执行他科开单权重</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">收入金额</td>
  		</tr>
  	</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=8">
              	<td class="numberText" bgcolor="#99FFCC">
               		<xsl:value-of select="format-number(.,'#,##0.00')"/>
               	</td>
              </xsl:when>
              <xsl:when test="position()&gt;1">
              	<td class="numberText">
               		<xsl:value-of select="format-number(.,'#,##0.00')"/>
               	</td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>            
          </xsl:for-each>
        </tr>
      </xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>