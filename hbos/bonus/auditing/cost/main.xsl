<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
			<td align="center">科室代码</td>
			<td align="center">科室名称</td>
			<td align="center">期间类型</td>
			<td align="center">考核年度</td>
			<td align="center">考核期间</td>
			<td align="center">成本金额</td>
  		</tr>
  	</thead>
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=6">
              	<td class="numberText" bgcolor="#99FFCC">
									<a href="#">
									<xsl:attribute name="onclick">
										javascript:openDialog('detail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:880px;dialogHeight:660px')
									</xsl:attribute>
               		<xsl:value-of select="format-number(.,'#,##0.00')"/>
               	</a>
               	</td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>            
          </xsl:for-each>
        </tr>
      </xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>