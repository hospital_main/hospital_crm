<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>�������</th>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:choose>
						<xsl:when test="td[2] = 1">
							<th><xsl:value-of select="td[1]"/></th>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>ȫԺ</td>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:choose>
						<xsl:when test="td[2] = 2">
							<xsl:choose>
								<xsl:when test="td[3] = 1">
									<td bgcolor="#99FFCC" align="right">
										<xsl:value-of select="td[1]"/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td align="right"><xsl:value-of select="td[1]"/></td>
								</xsl:otherwise>
							</xsl:choose>							
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>