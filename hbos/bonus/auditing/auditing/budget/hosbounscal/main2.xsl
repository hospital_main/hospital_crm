<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>核算对象</th>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:choose>
						<xsl:when test="td[1] = 'dataB'">
							<th><xsl:value-of select="td[2]"/></th>
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
				<th>奖金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:variable name="dataB" select="count(/root/tbody/tr[td[1]='dataB'])"/>
			<xsl:variable name="dataC" select="count(/root/tbody/tr[td[1]='dataC'])"/>
			<xsl:if test="$dataB > 0 or $dataC > 0">
				<tr>
					<td>全院</td>
					<xsl:for-each select="/root/tbody/tr">
						<xsl:choose>
							<xsl:when test="td[1] = 'dataB'">
								<td align="right">
									<xsl:value-of select="format-number(td[3], '#.00')" />
								</td>
							</xsl:when>
							<xsl:when test="td[1] = 'dataC'">
								<td bgcolor="#99FFCC" align="right">
									<xsl:value-of select="format-number(td[3], '#.00')" />
								</td>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:if>
			<tr style="display:none;">
				<td>
					<a href="#" id="adata">
						<xsl:attribute name="period_type">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[2]" />
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="year">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[3]" />
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="period">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[4]" />
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="factor_prop">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[5]" />
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="fun_name">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[6]" />
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="formula">
							<xsl:if test="/root/tbody/tr[1]/td[1] = 'dataA'">
								<xsl:value-of select="/root/tbody/tr[1]/td[7]" />
							</xsl:if>
						</xsl:attribute>
					</a>
				</td>
			</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>