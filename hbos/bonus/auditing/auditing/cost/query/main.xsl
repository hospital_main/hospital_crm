<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="dyn_Cols" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<tr class="mainHead">
			<td align="center" >��������</td>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $dyn_Cols]">
				<xsl:if test=" td[2]!=''">
					<td align="center" >
						<xsl:value-of select="td[2]"/>
					</td>
				</xsl:if>
			</xsl:for-each>
			<td align="center" >�����</td>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:if test="( position() mod $dyn_Cols )=1 or $dyn_Cols = 1">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;
				</xsl:text>
				<td>
					<xsl:value-of select="td[1]"/>
				</td>
		
			</xsl:if>
			<xsl:if test=" td[2]!=''">
				<td align="right">
				<xsl:value-of select="format-number(td[3],'#,##0.00')"/>
				</td>
			</xsl:if>
			<xsl:if test="(position() mod $dyn_Cols)=0 or $dyn_Cols = 1">
				<td align="right" style="background-color:#99FFCC">
				<xsl:value-of select="format-number(td[4],'#,##0.00')"/>
				</td>
				<xsl:text disable-output-escaping="yes">
					&lt;/tr&gt;
				</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

