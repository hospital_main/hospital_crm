<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<td align="center">干部名称</td>
				<td align="center">项目名称</td>
				<td align="center">期间类型</td>
				<td align="center">考核年度</td>
				<td align="center">考核期间</td>
				<td align="center">奖金额</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:variable name="unit_name" select="td[1]"/>
					<xsl:variable name="cur_pos" select="position()"/>
					<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$unit_name])" />
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos - 1]/td[1]">
									<td>
										<xsl:attribute name="rowspan">
											<xsl:value-of select="$rowspan"/>
										</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test="$unit_name = ../../tr[$cur_pos - 1]/td[1]">
									<td style="display:none">
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=6">
								<td class="numberText" bgcolor="#99FFCC">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>