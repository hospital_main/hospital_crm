<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
  	  		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  	</tr>
  	  	<tr noWrap="true" class="mainHead">
  	  		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  	</tr>
        <tr noWrap='true' class='mainHead'>
        	<td>科室代码</td>
        	<td>科室名称</td>
        	<td>科室奖金</td>
        	<td>科室预留奖金</td>
        	<td>科室职工奖金</td>
		</tr>
	  </thead>
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
	  	  <tr>
            <xsl:for-each select="td">
	      	  <xsl:choose>
	      	  	<xsl:when test="position() = 1 or position() = 2 or position() = 3">
      	        </xsl:when>
				<xsl:when test="position() = 6 or position() = 7 or position() = 8">
      	          <td align="right">
		    		<xsl:attribute name="class">numberText</xsl:attribute>
              		<xsl:value-of select="format-number(.,'#,##0.00')"/>
		  		  </td>
      	        </xsl:when>
				<xsl:otherwise>
		  		  <td align="left">
		    		<xsl:value-of select="."/>
		  		  </td>
				</xsl:otherwise>
	      	  </xsl:choose>
            </xsl:for-each>
	  	  </tr>
        </xsl:for-each>
      </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>