<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="lzk_hc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<tr class="mainHead">
			<th align="center">
				<input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="all_click(result)"></input>
			</th>
			<xsl:for-each select="/root/tbody/tr[1]/td">
				<td align="center">
					<xsl:value-of select="."/>
				</td>
			</xsl:for-each>
		</tr>
	</thead>
	<tbody>

		<xsl:variable name="lzk_bc" select="count(/root/tbody/tr[1]/td)"/>
		<xsl:for-each select="/root/tbody/tr[position()&gt;1 and position()!=last()]">
			<tr>
				<td align='center'  style='display:block'>
		          	  	<input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick='check_click(result)'>
		          	  		<xsl:if test=" td[last()]='��'"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
		          	  	</input>
		        	</td>
		        	<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test=" position() &lt;5 or position()=last()">
							<td><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:when test="position()=last() - 1">
							<td style="text-align:right;background-color:#99FFCD;"><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:otherwise>
							<td style="text-align:right;"><xsl:attribute name="pk1"><xsl:value-of select="substring-before(.,'|')"/></xsl:attribute><xsl:value-of select="format-number(substring-after(.,'|'),'#,##0.00')"/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
		<xsl:for-each select="/root/tbody/tr[position()=last()]">
			<tr>
				<td align='center'  style='display:block'>
		          	  	��
		        	</td>
		        	<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test=" position()=1">
							<td><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td) - 2"/></xsl:attribute>
							�ϼ�
							</td>
						</xsl:when>
						<xsl:when test="position()=last()">
							<td>��</td>
						</xsl:when>
						<xsl:when test="position()=last() - 1">
							<td style="text-align:right;background-color:#99FFCD;"><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:otherwise>
							<td style="display:none">��</td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

