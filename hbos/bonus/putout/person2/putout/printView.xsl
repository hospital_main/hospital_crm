<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
  	  		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  		<xsl:for-each select="/root/tbody/tr[1]/td">
		      <xsl:choose>
		        <xsl:when test="position() &lt; $colNum">
				  <td style='display:none'/>
		        </xsl:when>
		      </xsl:choose>
		  	</xsl:for-each>
  	  	</tr>
  	  	<tr noWrap="true" class="mainHead">
  	  		<td noWrap="true" style="fontsize:subtitle;colspan:4;align:left;"/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td noWrap="true" style="fontsize:subtitle;colspan:colcount-4;align:right;"/>
  	  		<xsl:for-each select="/root/tbody/tr[1]/td">
		      <xsl:choose>
		        <xsl:when test="position() &lt; $colNum - 4 ">
				  <td style='display:none'/>
		        </xsl:when>
		      </xsl:choose>
		  	</xsl:for-each>
  	  	</tr>
        <tr noWrap='true' class='mainHead'>
        	<xsl:for-each select="/root/tbody/tr[1]/td">
		    	<td><xsl:value-of select="."/></td>
		  	</xsl:for-each>
		</tr>
	  </thead>
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
        <xsl:if test="position() &gt; 1">
	  	  <tr>
            <xsl:for-each select="td">
	      	  <xsl:choose>            
				<xsl:when test="position() &gt; 4">
      	          <td align="right">
		    		<xsl:attribute name="class">numberText</xsl:attribute>
              		<xsl:value-of select="format-number(.,'#,##0.00')"/>
		  		  </td>
      	        </xsl:when>
				<xsl:otherwise>
		  		  <td align="left">
		    		<xsl:value-of select="."/>
		  		  </td>
				</xsl:otherwise>
	      	  </xsl:choose>
            </xsl:for-each>
	  	  </tr>
	  	</xsl:if>
        </xsl:for-each>
      </tbody>
      <tfoot>
      	<tr noWrap='true'>
      		<td style='fontsize:foot;border:none;colspan:colcount;align:right;' />
      		<xsl:for-each select="/root/tbody/tr[1]/td">
		      <xsl:choose>
		        <xsl:when test="position() &lt; $colNum">
				  <td style="display:none" />
		        </xsl:when>
		      </xsl:choose>
		  	</xsl:for-each>
      	</tr>
      </tfoot>
    </root>
  </xsl:template>
</xsl:stylesheet>