<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
    <root>
  	<thead>
		<tr noWrap="true" class="mainHead">
  	  		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  		<xsl:for-each select="/root/tbody/tr[1]/td">
		      <xsl:choose>
		        <xsl:when test="position() &lt; $colNum - 5">
				  <td style='display:none'/>
		        </xsl:when>
		      </xsl:choose>
		  	</xsl:for-each>
  	  	</tr>
		<tr noWrap="true" class="mainHead">
  	  		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
  	  		<xsl:for-each select="/root/tbody/tr[1]/td">
		      <xsl:choose>
		        <xsl:when test="position() &lt; $colNum - 5">
				  <td style='display:none'/>
		        </xsl:when>
		      </xsl:choose>
		  	</xsl:for-each>
  	  	</tr>
  		<tr noWrap="true" class="mainHead">
  			<xsl:for-each select="/root/tbody/tr[1]/td">
		      <xsl:choose>
		        <xsl:when test="position()=3 or position()=4 or position()=6 or position()=8 or position()=10 or position()=11">
					<td rowspan="2" valign="middle">
						<xsl:value-of select="."/>
					</td>
		        </xsl:when>
		        <xsl:when test="position() &gt; 11">
		        	<xsl:variable name="cols" select="position()"/>
		          	<xsl:variable name="cols_name" select="."/>
		          	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td[.=$cols_name])"/>
				    <xsl:if test=" ../td[$cols]!= ../td[$cols -1]">
				    	<td valign="middle" >
				    		<xsl:attribute name="colspan" >
				    			<xsl:value-of select="$colNums"/>
				    		</xsl:attribute>
						    <xsl:value-of select="."/>
						</td>
				    </xsl:if>
				    <xsl:if test=" ../td[$cols] = ../td[$cols -1]">
						<td style="display:none" >
							<xsl:value-of select="."/>
						</td>
				    </xsl:if>
		        </xsl:when>
		      </xsl:choose>
		  	</xsl:for-each>
		</tr>
		<tr noWrap="true" class="mainHead">
  			<xsl:for-each select="/root/tbody/tr[2]/td">
		      <xsl:choose>
		        <xsl:when test="position()=3 or position()=4 or position()=6 or position()=8 or position()=10 or position()=11">
					<td style="display:none">
						<xsl:value-of select="."/>
					</td>
		        </xsl:when>
		        <xsl:when test="position() &gt; 11">
					<td valign="middle" >
						<xsl:value-of select="."/>
					</td>
		        </xsl:when>
		      </xsl:choose>
		  	</xsl:for-each>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:if test="position() &gt; 2">
	    		<tr>
	    		  <xsl:variable name="rows" select="position()"/>
	    		  <xsl:variable name="rowName" select="concat(td[3],td[5],td[7])"/>
	    		  <xsl:variable name="rowCnt" select="count(/root/tbody/tr[concat(td[3],td[5],td[7]) = $rowName])"/>
	    		  <xsl:if test="concat(../tr[$rows - 1]/td[3],../tr[$rows - 1]/td[5],../tr[$rows - 1]/td[7]) != $rowName">
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[3]"/>
	    			</td>
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[4]"/>
	    			</td>
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[6]"/>
	    			</td>
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[8]"/>
	    			</td>
				  </xsl:if>
				  <xsl:if test="concat(../tr[$rows - 1]/td[3],../tr[$rows - 1]/td[5],../tr[$rows - 1]/td[7]) = $rowName">
	    			<td noWrap="true" style="display:none">
	    				<xsl:value-of select="td[3]"/>
	    			</td>
	    			<td noWrap="true" style="display:none">
	    				<xsl:value-of select="td[4]"/>
	    			</td>
	    			<td noWrap="true" style="display:none">
	    				<xsl:value-of select="td[6]"/>
	    			</td>
	    			<td noWrap="true" style="display:none">
	    				<xsl:value-of select="td[8]"/>
	    			</td>
				  </xsl:if>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			            <xsl:when test="position() = 10">
			            	<td>
			            		<xsl:value-of select="."/>
			            	</td>
			            </xsl:when>
			            <xsl:when test="position() &gt; 10">
			            	<td align="right">
			            		<xsl:attribute name="class">numberText</xsl:attribute>
              					<xsl:value-of select="format-number(.,'#,##0.00')"/>
			            	</td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		  		</tr>
	    	</xsl:if>
   		</xsl:for-each>
      </tbody>
      <tfoot>
      	<tr noWrap='true'>
      		<td style='fontsize:foot;border:none;colspan:colcount;align:right;' />
  	  		<xsl:for-each select="/root/tbody/tr[1]/td">
		      <xsl:choose>
		        <xsl:when test="position() &lt; $colNum - 5">
				  <td style='display:none'/>
		        </xsl:when>
		      </xsl:choose>
		  	</xsl:for-each>
      	</tr>
      </tfoot>
    </root>
  </xsl:template>
</xsl:stylesheet>