<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th nowrap='true'>工资项目编码</th>
				<th nowrap='true'>工资项目名称</th> 
			</tr>   
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>														<xsl:if test="td[3]=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
						</input>
					</td>
				<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() = 3">
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

