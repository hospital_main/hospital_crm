<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
  	  		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  	</tr>
  	  	<tr noWrap="true" class="mainHead">
  	  		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  		<td style='display:none'/>
  	  	</tr>
        <tr noWrap='true' class='mainHead'>
        	<td>绩效科室编码</td>
        	<td>绩效科室名称</td>
        	<td>发放方式</td>
        	<td>职工金额</td>
        	<td>是否提交</td>
        	<td>提交人</td>
        	<td>是否审核</td>
        	<td>审核人</td>
		</tr>
	  </thead>
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
	  	  <tr>
            <xsl:for-each select="td">
	      	  <xsl:choose>            
				<xsl:when test="position() = 4">
      	          <td align="right">
		    		<xsl:attribute name="class">numberText</xsl:attribute>
              		<xsl:value-of select="format-number(.,'#,##0.00')"/>
		  		  </td>
      	        </xsl:when>
				<xsl:otherwise>
		  		  <td align="left">
		    		<xsl:value-of select="."/>
		  		  </td>
				</xsl:otherwise>
	      	  </xsl:choose>
            </xsl:for-each>
	  	  </tr>
        </xsl:for-each>
      </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>