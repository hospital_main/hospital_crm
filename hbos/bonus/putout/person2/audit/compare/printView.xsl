<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
  	<root>
  	<thead>
  		<tr noWrap="true" class="mainHead">
  	  		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  		<xsl:for-each select="/root/tbody/tr[1]/td">
		      <xsl:choose>
		        <xsl:when test="position() &lt; $colNum ">
				  <td style='display:none'/>
		        </xsl:when>
		      </xsl:choose>
		  	</xsl:for-each>
  	  </tr>
			<tr noWrap="true" class="mainHead">
  	  		<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
  	  		<xsl:for-each select="/root/tbody/tr[1]/td">
		      <xsl:choose>
		        <xsl:when test="position() &lt; $colNum ">
				  <td style='display:none'/>
		        </xsl:when>
		      </xsl:choose>
		  	</xsl:for-each>
  	  </tr>
  		<tr noWrap="true" class="mainHead">
  			<td rowspan="2">绩效科室编码</td>
        <td rowspan="2">绩效科室名称</td>
        <td colspan="2">科室奖金</td>
        <td style='display:none'/>															
  			<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td[position() &gt; 4])"/>
  			<td >
  				<xsl:attribute name="colspan" ><xsl:value-of select="$colNums"/></xsl:attribute>
  				职工奖金
  			</td>
  		</tr>
  		<tr noWrap="true" class="mainHead">
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td>科室金额</td> 
  			<td>科室职工金额</td> 
  			<td>合计</td>
  			<xsl:for-each select="/root/tbody/tr[1]/td">
  				<xsl:if test="position() &gt; 5">
  					<td><xsl:value-of select="."/></td>
  				</xsl:if>
		  	</xsl:for-each>
		</tr>
  	</thead>
  	<tbody>
	   <xsl:for-each select="/root/tbody/tr">
	    	<xsl:if test="position() &gt; 1">
	    		<tr>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			            <xsl:when test="position() &gt; 2">
			            	<td>
			            		<xsl:attribute name="class">numberText</xsl:attribute>
              					<xsl:value-of select="format-number(.,'#,##0.00')"/>
			            	</td>
			            </xsl:when>
			            <xsl:otherwise>
		  		  				<td>
		    							<xsl:value-of select="."/>
		  		  				</td>
									</xsl:otherwise>
		          	</xsl:choose>
		          </xsl:for-each>
		  		</tr>
	    	</xsl:if>
   		</xsl:for-each>
  	</tbody>
  	<tfoot>
      	<tr noWrap='true'>
      		<td style='fontsize:foot;border:none;colspan:colcount;align:right;' />
  	  		<xsl:for-each select="/root/tbody/tr[1]/td">
		      <xsl:choose>
		        <xsl:when test="position() &lt; $colNum ">
				  <td style='display:none'/>
		        </xsl:when>
		      </xsl:choose>
		  	</xsl:for-each>
      	</tr>
      </tfoot>
  	</root>
	</xsl:template>
</xsl:stylesheet>