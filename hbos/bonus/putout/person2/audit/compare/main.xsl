<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
  			<th rowspan="2">绩效科室编码</th>
        <th rowspan="2">绩效科室名称</th>
        <th colspan="2">科室奖金</th>
  			<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td[position() &gt; 4])"/>
  			<th >
  				<xsl:attribute name="colspan" ><xsl:value-of select="$colNums"/></xsl:attribute>
  				职工奖金
  			</th>
  		</tr>
  		<tr noWrap="true" class="mainHead">
  			<th>科室金额</th> 
  			<th>科室职工金额</th> 
  			<th>合计</th>
  			<xsl:for-each select="/root/tbody/tr[1]/td">
  				<xsl:if test="position() &gt; 5">
  					<th><xsl:value-of select="."/></th>
  				</xsl:if>
		  	</xsl:for-each>
		</tr>
  	</thead>
  	<tbody>
	   <xsl:for-each select="/root/tbody/tr">
	    	<xsl:if test="position() &gt; 1">
	    		<tr>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			            <xsl:when test="position() &gt; 2">
			            	<td>
			            		<xsl:attribute name="class">numberText</xsl:attribute>
              					<xsl:value-of select="format-number(.,'#,##0.00')"/>
			            	</td>
			            </xsl:when>
			            <xsl:otherwise>
		  		  				<td>
		    							<xsl:value-of select="."/>
		  		  				</td>
									</xsl:otherwise>
		          	</xsl:choose>
		          </xsl:for-each>
		  		</tr>
	    	</xsl:if>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>