<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="lzk_hc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<tr>
			<th align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">
			<input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="all_click(result)"></input>
			</th>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">职工名称</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">科室名称</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">期间类型</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">考核年度</td>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">考核期间</td>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $lzk_hc]">
				<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center">
					<xsl:value-of select="td[6]"/>
				</td>
			</xsl:for-each>
			<td align="center" style="background-color:#FCB724;font-weight: bold;font-family:'宋体'">是否发放</td>
		</tr>
	</thead>
	<tbody>

		<xsl:variable name="lzk_bc" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<xsl:variable name="lzk_opt" select="/root/tbody/tr[1]/td[6]"/>
		<xsl:for-each select="/root/tbody/tr">
			
			<xsl:if test="( position() mod $lzk_bc )=1 or $lzk_bc = 1">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;
				</xsl:text>
				
			<td align='center'  style='display:block'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick='check_click(result)'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
        </td>
          
				<td>
					<xsl:value-of select="td[1]"/>
				</td>
				<td>
					<xsl:value-of select="td[2]"/>
				</td>
				<td>
					<xsl:value-of select="td[3]"/>
				</td>
				<td>
					<xsl:value-of select="td[4]"/>
				</td>
				<td>
					<xsl:value-of select="td[5]"/>
				</td>
			</xsl:if>
				<td>
						<xsl:if test=" td[6]='合计' ">
							<xsl:attribute name="style">text-align:right;background-color:#99FFCD;</xsl:attribute>
						</xsl:if>
						<xsl:if test=" td[6]!='合计' ">
							<xsl:attribute name="style">text-align:right;</xsl:attribute>
						</xsl:if>
						<xsl:attribute name="pk1"><xsl:value-of select="td[8]"/></xsl:attribute>
						<xsl:value-of select="td[7]" disable-output-escaping="yes"/>

						<!--xsl:attribute name="style">text-align:right;</xsl:attribute>
						<xsl:attribute name="pk1"><xsl:value-of select="td[8]"/></xsl:attribute>
						<xsl:value-of select="td[7]" disable-output-escaping="yes"/-->
				</td>
			
			<xsl:if test="(position() mod $lzk_bc)=0 or $lzk_bc = 1">
				
				<td>
					<xsl:attribute name="style">text-align:left;</xsl:attribute>
					<xsl:value-of select="td[5]"/>
				</td>
				
				<xsl:text disable-output-escaping="yes">
					&lt;/tr&gt;
				</xsl:text>
			</xsl:if>
		</xsl:for-each>
		
		<tr>
			<td>
			<xsl:attribute name="colspan">
					<xsl:value-of select="$lzk_bc + 4"/>
			</xsl:attribute>
			合计
			</td>
			<td id="putoutTdId">
			<xsl:attribute name="style">text-align:right;background-color:#99FFCD;</xsl:attribute>
			</td>
			<td/>
			</tr>
			
	</tbody>
	</xsl:template>
</xsl:stylesheet>

