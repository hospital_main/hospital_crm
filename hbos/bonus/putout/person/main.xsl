<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:for-each select="/root/tbody/tr[1]">
				<tr noWrap="true" class="mainHead">
					<th noWrap="true">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;" onclick="all_click(result)"/>
					</th>
					<xsl:for-each select="td[position()!=1]">
						<th noWrap="true">
							<xsl:value-of select="."/>
						</th>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position()!=1]">
				<tr>
					<xsl:if test=" td[last()]='��' ">
						<td align="center" style="display:block">
							<input type="checkbox" TABINDEX="-1" style="font-size:8px;" onclick="check_click(result)">
            </input>
						</td>
					</xsl:if>
					<xsl:if test=" td[last()]='��' ">
						<td align="center" style="display:block">
							<input type="checkbox" TABINDEX="1" disabled="true" style="font-size:8px;" onclick="check_click(result)"/>
						</td>
					</xsl:if>
					<xsl:for-each select="td[position()!=1]">
						<xsl:choose>
							<xsl:when test="position() &gt; 5 and position() &lt; last()-1">
								<td>
									<xsl:attribute name="style">text-align:center;</xsl:attribute>
									<xsl:attribute name="pk1"><xsl:value-of select="substring-before(., '|||')"/></xsl:attribute>
									<input type="text">										
										<xsl:if test="../td[last()]='��'">
											<xsl:attribute name="readonly">true</xsl:attribute>
										</xsl:if>
										<xsl:attribute name="class">inputDecimal</xsl:attribute>
										<xsl:attribute name="extent">100</xsl:attribute>
										<xsl:attribute name="Point">4</xsl:attribute>
										<xsl:attribute name="name"><xsl:value-of select="substring-before(., '|||')"/></xsl:attribute>
										<xsl:attribute name="value"><xsl:value-of select="substring-after(., '|||')"/></xsl:attribute>
									</input>
								</td>
							</xsl:when>
							<xsl:when test="position() = last()-1">
								<td>
									<xsl:attribute name="style">text-align:right;background-color:#99FFCD;</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			<tr noWrap="true">
				<td>
					<xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td[position()!=1])-1"/></xsl:attribute>
			  			�ϼ�
			   	</td>
				<td>
					<xsl:attribute name="style">text-align:right;background-color:#99FFCD;</xsl:attribute>
					<xsl:value-of select="format-number(sum(/root/tbody/tr[position()!=1]/td[last()-1]),'#,##0.00')"/>
				</td>
				<td></td>
			</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>