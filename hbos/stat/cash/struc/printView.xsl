<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/stat/cash/struc/printView.xsl,v 1.1 2012/03/12 01:56:19 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:56:19 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
			</colgroup>
			<thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'  valign='middle'>科目性质</th>
      	<th nowrap='true'  valign='middle'>科目编码</th>
      	<th nowrap='true'  valign='middle'>科目名称</th>
      	<th nowrap='true'  valign='middle'>分析期</th>
      	<th nowrap='true'  valign='middle'>结构(%)</th>
      	<th nowrap='true'  valign='middle'>对比期</th>
      	<th nowrap='true'  valign='middle'>结构(%)</th>
      	<th nowrap='true'  valign='middle'>结构增减(%)</th>
      </tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position()=1 ">
                      </xsl:when>
                      <xsl:when test="position()=6 or position()=8 or position()=9 ">
                        <td align='right' class="numberText"><xsl:value-of select="format-number(.,'###0.00')"/></td>
                      </xsl:when>
                      <xsl:when test="position()>4 ">
                        <td align='right' class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                      </xsl:when>
                      <xsl:otherwise>
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                    </xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
