<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true' rowspan='2' valign='middle'>科目</th>
      	<th nowrap='true' rowspan='2' valign='middle'>本期预算</th>
      	<th nowrap='true' rowspan='2' valign='middle'>本期追加</th>
      	<th nowrap='true' rowspan='2' valign='middle'>本期总预算</th>
      	<th nowrap='true' rowspan='2' valign='middle'>本期完成</th>
      	<th nowrap='true' colspan='2' valign='middle'>完成预算</th>
      	<th nowrap='true' rowspan='2' valign='middle'>对比期完成</th>
      	<th nowrap='true' colspan='2' valign='middle'>增减</th>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>差额</th>
      	<th nowrap='true'>百分比</th>
      	<th nowrap='true'>金额</th>
      	<th nowrap='true'>百分比</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position()=7 or position()=10">
                        <td align='right'><xsl:value-of select="."/></td>
                      </xsl:when>
                      <xsl:when test="position()>1 ">
                        <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                      </xsl:when>
                      <xsl:otherwise>
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                    </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

