<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true' rowspan='2' valign='middle'>指标类别编码</th>
      	<th nowrap='true' rowspan='2' valign='middle'>指标类别名称</th>
      	<th nowrap='true' rowspan='2' valign='middle'>指标编码</th>
      	<th nowrap='true' rowspan='2' valign='middle'>指标名称</th>
      	<th nowrap='true' rowspan='2' valign='middle'>指标单位</th>
      	<th nowrap='true' rowspan='2' valign='middle'>分析期</th>
      	<th nowrap='true' rowspan='2' valign='middle'>对比期</th>
      	<th nowrap='true' colspan='2' valign='middle'>增减</th>
      	
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>金额</th>
      	<th nowrap='true'>百分比</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <td>
                  <xsl:value-of select="."/>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

