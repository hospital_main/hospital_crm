<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true' valign='middle'>指标类别编码</th>
      	<th nowrap='true' valign='middle'>指标类别名称</th>
      	<th nowrap='true' valign='middle'>指标编码</th>
      	<th nowrap='true' valign='middle'>指标名称</th>
      	<th nowrap='true' valign='middle'>指标单位</th>
      	<xsl:for-each select="/root/tbody/tr[1]/td">
      	  <xsl:if test="position() > 5 and last() - position() >0"> 
      	  	<th nowrap='true' valign='middle'><xsl:value-of select="."/></th>
      	  </xsl:if>
   	  	</xsl:for-each>
   	  	<th nowrap='true' valign='middle'>平均值</th>
      	
 		</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:if test="position() > 1"> 
        <tr>
          <xsl:for-each select="td">
            <td>
                  <xsl:value-of select="."/>
            </td>
          </xsl:for-each>
        </tr>
        </xsl:if> 
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

