<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
	  	<tr>
	   		<td style="fontsize:maintitle;colspan:colcount;"></td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr>
					<td rowspan="2">指标编码</td>
					<td rowspan="2">计划指标</td>
					<td colspan="4">本年计划</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td rowspan="2">指标类型</td>
			</tr>
			<tr>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td>全院</td>
				<td>科室汇总</td>
				<td>差额</td>
				<td>差异率%</td>
				<td style="display:none"></td>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test=" position() = 1 or position() = 2 or position() = 7">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	              <xsl:otherwise>
			<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	              </xsl:otherwise>
	          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>