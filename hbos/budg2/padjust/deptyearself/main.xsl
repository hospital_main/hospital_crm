<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>调整序号</th>
				<th>调整日期</th>
				<th>调整文号</th>
				<th>预算下达</th>
				<th>调整说明</th>
				<th style="display:none"></th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' align="left">
		              <a tabindex="-2">
			                <xsl:attribute name="href" >
			                  javascript:openDialog('index.html?load=&lt;adjust_xh&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/adjust_xh&gt;&lt;is_elast&gt;<xsl:value-of select="../td[position()=6]"/>&lt;/is_elast&gt;', 'dialogWidth:800px;dialogHeight:600px', result)
			                </xsl:attribute>
	                  	<xsl:value-of select="."/>
	                  </a>
			            </td>
                </xsl:when>
                <xsl:when test="position() = 6">
                	<td style="display:none">
                		<xsl:value-of select="."/>
                	</td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
