<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>指标编码</th>
				<th>指标名称</th>
				<th>计量单位</th>
				<th>年初预算</th>
				<th>单位调整</th>
				<th>科室汇总</th>
				<th>差额</th>
				<th>调整后预算</th>
				<th>累计调整</th>
				<th>调整比例(%)</th>
				<th>职能审核状态</th>
				<th>职能审核信息</th>
				<th>单位审核状态</th>
				<th>单位审核信息</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' align="left">
		              	<a tabindex="-2" href="#">
			                <xsl:attribute name="onclick">
			                  javascript:openDialog('dept.html?load=&lt;p1&gt;<xsl:value-of select="../td[1]"/>&lt;/p1&gt;&lt;p2&gt;<xsl:value-of select="../td[2]"/>&lt;/p2&gt;&lt;p3&gt;<xsl:value-of select="../td[5]"/>&lt;/p3&gt;','dialogWidth:1000px;dialogHeight:400px',result)
			                </xsl:attribute>
	                  	<xsl:value-of select="."/>
	                  </a>
			            </td>
                </xsl:when>
                <xsl:when test="position() = 4 or position() = 5 or position() = 6 or position() = 7 or position() = 8 or position() = 9 or position() = 10 ">
                	<td class="numberText">
                		<xsl:value-of select="format-number(.,'#,##0.00')"/>
                		</td>
                </xsl:when>
	              <xsl:when test=" position() = 11 or position() = 12 or position() = 13 or position() = 14 ">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
