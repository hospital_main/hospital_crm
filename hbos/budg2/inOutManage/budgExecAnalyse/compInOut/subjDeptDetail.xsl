<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th rowspan="2">选择</th>
				<th nowrap="true" rowspan="2" dataIndex="0">科室编码</th>
				<th nowrap="true" rowspan="2" dataIndex="0">科室名称</th>
				<th nowrap="true" colspan="3" dataIndex="0">年度预算</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">1月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">2月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">3月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">4月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">5月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">6月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">7月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">8月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">9月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">10月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">11月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">12月</th>
				<th style="display:none"/>
				<th style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th style="display:none"/>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","年度预算",4)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","年度预算",5)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","1月",7)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","1月",8)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","2月",10)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","2月",11)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","3月",13)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","3月",14)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","4月",16)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","4月",17)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","5月",19)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","5月",20)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","6月",22)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","6月",23)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","7月",25)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","7月",26)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","8月",28)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","8月",29)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","9月",31)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","9月",32)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","10月",34)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","10月",35)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","11月",37)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","11月",38)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("0","12月",40)
					  </xsl:attribute>
						预算总额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1","12月",41)
					  </xsl:attribute>
						实际执行
					</a>
				</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <td align='center'  noWrap='true'>
           <button class="graphButton">
            	 <xsl:attribute name="index"><xsl:value-of select='$CurTrPos'/></xsl:attribute>
            	 <xsl:attribute name="onclick">openGraph(this)</xsl:attribute>
            	 ___
    			  </button>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true'>
			            	<xsl:if test="../td[1]!='0'">
	                  	<xsl:value-of select="."/>
			            	</xsl:if>
			            </td>
                </xsl:when>
                <xsl:when test="position()=2">
			            <td  noWrap='true'>
	                  	<xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' align="right">
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
