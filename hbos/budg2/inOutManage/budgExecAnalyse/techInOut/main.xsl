<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>科室编码</th>
  			<th nowrap='true'>科室名称</th>
  			<th nowrap='true'>本月执行</th>   			
  			<th nowrap='true'>年度预算金额</th> 
  			<th nowrap='true'>截止本月累计执行</th> 
  			<th nowrap='true'>执行进度%</th> 
  		</tr>
  	</thead>

  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<tr>
          <xsl:for-each select="td[position() &lt; 7]">
          	<xsl:if test="position() = 1 or position() = 2 ">
	         		<td>
	         			<xsl:if test="../td[7] = '0'">
	         				<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
		         			<xsl:value-of select="."/>
								</xsl:if>
								<xsl:if test="../td[7] = '1'">
										  <xsl:value-of select="."/>
								</xsl:if>
	         		</td>
         		</xsl:if>
          	<xsl:if test="position() &gt; 2">
	         		<td align="right">
	         			<xsl:if test="../td[7] = '0'">
	         				<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
		         			<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:if>
								<xsl:if test="../td[7] = '1'">
		         			<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:if>
	         		</td>
         		</xsl:if>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>