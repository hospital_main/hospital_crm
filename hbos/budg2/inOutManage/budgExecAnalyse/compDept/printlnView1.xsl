<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<table>
  	<thead>
  		<tr noWrap="true" class="mainHead">
  			<td style="fontsize:maintitle;colspan:colcount"></td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td nowrap="true" rowspan="2" dataIndex="0">科室编码</td>
				<td nowrap="true" rowspan="2" dataIndex="0">科室名称</td>
				<td nowrap="true" colspan="3" dataIndex="0">年度预算</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" colspan="3" dataIndex="0">1月</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" colspan="3" dataIndex="0">2月</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" colspan="3" dataIndex="0">3月</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" colspan="3" dataIndex="0">4月</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" colspan="3" dataIndex="0">5月</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" colspan="3" dataIndex="0">6月</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" colspan="3" dataIndex="0">7月</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" colspan="3" dataIndex="0">8月</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" colspan="3" dataIndex="0">9月</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" colspan="3" dataIndex="0">10月</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" colspan="3" dataIndex="0">11月</td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" colspan="3" dataIndex="0">12月</td>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td style="display:none"/>
				<td style="display:none"/>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
				<td nowrap="true" dataIndex="0">预算总额</td>
				<td nowrap="true" dataIndex="0">实际执行</td>
				<td nowrap="true" dataIndex="0">执行进度%</td>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true'>
			            	<xsl:if test="../td[1]!='0'">
	                  	<xsl:value-of select="."/>
			            	</xsl:if>
			            </td>
                </xsl:when>
                <xsl:when test="position()=2">
			            <td  noWrap='true'>
	                  	<xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' align="right">
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
