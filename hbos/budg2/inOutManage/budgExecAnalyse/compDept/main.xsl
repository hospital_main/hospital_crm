<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true">科室编码</th>
				<th nowrap="true">科室名称</th>
				<th nowrap="true">年度预算</th>
				<th nowrap="true">本月执行</th>
				<th nowrap="true">截止本月累计执行</th>
				<th nowrap="true">差额</th>
				<th nowrap="true">执行进度%</th>
				<!--
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">1月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">2月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">3月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">4月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">5月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">6月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">7月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">8月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">9月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">10月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">11月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">12月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				-->
			</tr>
			<!--
			<tr noWrap="true" class="mainHead">
				<th style="display:none"/>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
			</tr>
			-->
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
           <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1 or position() = 2">
			            <td  noWrap='true'>
	                  	<xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' align="right" >
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
