<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<thead>
	<tr noWrap="true" class="mainHead">
		<th rowspan="2">科目编码</th>
		<th rowspan="2">科目名称</th>
		<th rowspan="2">费用标准</th>
		<th colspan="2">本期预算</th>
		<th colspan="2">实际执行</th>
		<th colspan="2">差异</th>
	</tr>
	<tr noWrap="true" class="mainHead">
		<th>工作量</th>
		<th>预算总额</th>
		<th>工作量</th>
		<th>执行总额</th>
		<th>工作量</th>
		<th>差额</th>
	</tr>
</thead>
<tbody>
<xsl:for-each select="/root/tbody/tr">
	<tr>
		<xsl:for-each select="td">
		<xsl:choose>
		<xsl:when test="position() &lt; 3">
			<td><xsl:value-of select="."/></td>
		</xsl:when>
		<xsl:when test="position() &gt; 2">
			<td align="right"><xsl:value-of select="format-number(., '#,##0.00')"/></td>
		</xsl:when>
		</xsl:choose>
		</xsl:for-each>
	</tr>
</xsl:for-each>
</tbody>
</xsl:template>
</xsl:stylesheet>