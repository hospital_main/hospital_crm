<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>科目编码</th>
				<th>科目名称</th>
				<th>年初预算</th>
				<th>单位调整</th>
				<th>科室汇总</th>
				<th>差额</th>
				<th>差异率(%)</th>
				<th>累计调整</th>
				<th>调整后预算</th>
				<th>调整比例(%)</th>
				<th>职能审核</th>
				<th>职能审核信息</th>
				<th>单位审核</th>
				<th>单位审核信息</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' align="left">
		              	<a tabindex="-2" href="#">
			                <xsl:attribute name="onclick">
			                  javascript:openPage('dept.html?load=&lt;p1&gt;<xsl:value-of select="../td[1]"/>&lt;/p1&gt;&lt;p2&gt;<xsl:value-of select="../td[2]"/>&lt;/p2&gt;&lt;p3&gt;<xsl:value-of select="../td[4]"/>&lt;/p3&gt;')
			                </xsl:attribute>
	                  	<xsl:value-of select="."/>
	                  </a>
			            </td>
                </xsl:when>
                <xsl:when test="position()=7">
			            <td noWrap='true' align="right">
                  	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:when>
                <xsl:when test="position()=11">
			            <td noWrap='true' align="left">
			            <xsl:if test="../td[11]='审核通过'">
			            <xsl:attribute name="style">
			                  color:green
			            </xsl:attribute>
			            </xsl:if>
			            <xsl:if test="../td[11]='审核未通过'">
			            <xsl:attribute name="style">
			                  color:red
			            </xsl:attribute>
			            </xsl:if>
                  	<xsl:value-of select="."/>
			            </td>
                </xsl:when>
                <xsl:when test="position()=12">
                  <td  noWrap='true' align="center">
		              	<a tabindex="-2" href="#">
			                <xsl:attribute name="onclick">
			                  javascript:openPage('dept.html?load=&lt;p1&gt;<xsl:value-of select="../td[1]"/>&lt;/p1&gt;&lt;p2&gt;<xsl:value-of select="../td[2]"/>&lt;/p2&gt;&lt;p3&gt;<xsl:value-of select="../td[4]"/>&lt;/p3&gt;')
			                </xsl:attribute>
	                  	<xsl:value-of select="."/>
	                  </a>
			            </td>
                </xsl:when>
                 <xsl:when test="position()=13">
			            <td noWrap='true' align="left">
			            <xsl:if test="../td[13]='审核通过'">
			            <xsl:attribute name="style">
			                  color:green
			            </xsl:attribute>
			            </xsl:if>
			            <xsl:if test="../td[13]='审核未通过'">
			            <xsl:attribute name="style">
			                  color:green
			            </xsl:attribute>
			            </xsl:if>
                  	<xsl:value-of select="."/>
			            </td>
                </xsl:when>
                 <xsl:when test="position()=14">
                  <td  noWrap='true' align="center">
		              	<a tabindex="-2" href="#">
			                <xsl:attribute name="onclick">
			                  javascript:openPage('dept.html?load=&lt;p1&gt;<xsl:value-of select="../td[1]"/>&lt;/p1&gt;&lt;p2&gt;<xsl:value-of select="../td[2]"/>&lt;/p2&gt;&lt;p3&gt;<xsl:value-of select="../td[4]"/>&lt;/p3&gt;')
			                </xsl:attribute>
	                  	<xsl:value-of select="."/>
	                  </a>
			            </td>
                </xsl:when>
                <xsl:when test="position()&gt;=3">
			            <td noWrap='true' align="right">
                  	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:when>
                
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
