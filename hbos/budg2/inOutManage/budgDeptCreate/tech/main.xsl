<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>���ұ���</th>
  			<th nowrap='true'>��������</th>
  			<th nowrap='true'>Ԥ������</th> 
  		</tr>
  	</thead>

  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<tr>
          <xsl:for-each select="td[position() &lt; 4 ]">
          	<xsl:if test="position() = 1 ">
	         		<td>
	         			<xsl:if test="../td[4] = '0'">
	         				<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
		         			<xsl:value-of select="."/>
								</xsl:if>
								<xsl:if test="../td[4] = '1'">
			    				<a href="#">
										  <xsl:attribute name="onclick" >
				    	              javascript:openDialog('detail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:700px;overflow:auto;', result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
									</a>
								</xsl:if>
	         		</td>
         		</xsl:if>
          	<xsl:if test="position() = 2 ">
	         		<td>
	         			<xsl:if test="../td[4] = '0'">
	         				<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
								</xsl:if>
	         			<xsl:value-of select="."/>
	         		</td>
         		</xsl:if>
          	<xsl:if test="position() &gt; 2">
	         		<td align="right">
	         			<xsl:if test="../td[4] = '0'">
	         				<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
								</xsl:if>
	         			<xsl:value-of select="format-number(.,'#,##0.00')"/>
	         		</td>
         		</xsl:if>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>