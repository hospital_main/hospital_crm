<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
	  	<tr>
	   		<td style="fontsize:maintitle;colspan:colcount;"></td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr>
					<td>科目编码</td>
					<td>科目名称</td>
					<td>历史执行</td>
					<td>本年度预算</td>
					<td>差额</td>
					<td>差异率</td>
					<td>参考方法</td>
					
			</tr>

  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test="  position() = 1 or position() = 2 or position() = 3 or position() = 4 or position() = 5 or position() = 6 or position() = 7 ">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	              <xsl:otherwise>
	              </xsl:otherwise>
	          </xsl:choose>
  		</xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>