<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap="true" class="mainHead">
					<td style="fontsize:maintitle;colspan:colcount"></td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr>
					<td rowspan='2'>月份</td>
					<td rowspan='2'>初始余额</td>
					<td colspan='4'>资金流入预算</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td colspan='4'>资金流出预算</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td rowspan='2'>资金流量</td>
					<td rowspan='2'>可用额度</td>
					<td rowspan='2'>资金存量</td>
				</tr>
				<tr>
					<td style="display:none"/>
					<td style="display:none"/>
					<td >收入预算</td>
					<td >本期应收计划</td>
					<td >本期收款计划</td>
					<td >现金流入</td>
					<td >支出预算</td>
					<td >本期应付计划</td>
					<td >本期付款计划</td>
					<td >现金流出</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test=" position() =1 ">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>

		</root>
	</xsl:template>
</xsl:stylesheet>
