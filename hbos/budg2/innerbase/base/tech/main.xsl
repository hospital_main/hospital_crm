<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>���ұ���</th>
  			<th nowrap='true'>��������</th>
  			<th nowrap='true'>�ٴ����ұ���</th>
  			<th nowrap='true'>�ٴ���������</th>
  			
  		</tr>
  	</thead>

  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
       	<xsl:variable name="colCnt" select='position()'/>
        <tr>
        	<xsl:variable name="dept_name" select='td[1]'/>
        	<xsl:variable name="rowCnt" select='count(/root/tbody/tr[td[1] = $dept_name])'/>
					
					<xsl:if test="$colCnt = 1">
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[1]"/>
	    			</td>
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[2]"/>
	    				<a href="#">
								  <xsl:attribute name="onclick" >
		    	              javascript:openDialog('budgDeptList.html?load=<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:700px;overflow:auto;', result)
								  </xsl:attribute>
								  ����
							</a>
	    			</td>
					</xsl:if>
					<xsl:if test="$colCnt &gt; 1">
			    		<xsl:if test="td[1] != ../tr[$colCnt - 1 ]/td[1]">
			    			<td noWrap="true">
			    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			    				<xsl:value-of select="td[1]"/>
			    			</td>
			    			<td noWrap="true">
			    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			    				<xsl:value-of select="td[2]"/>
			    				<a href="#">
										  <xsl:attribute name="onclick" >
				    	              javascript:openDialog('budgDeptList.html?load=<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:700px;overflow:auto;', result)
										  </xsl:attribute>
										  ����
									</a>
			    			</td>
			    		</xsl:if>
			    		<xsl:if test="td[1] = ../tr[$colCnt - 1 ]/td[1]">
			    			<td style="display:none"/>
			    			<td style="display:none"/>
			    		</xsl:if>
			     </xsl:if>
        	
          <xsl:for-each select="td[position() &gt; 2]">
         		<td><xsl:value-of select="."/></td>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>