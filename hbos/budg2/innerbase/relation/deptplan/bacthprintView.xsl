<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
	  	<tr>
	   		<td style="fontsize:maintitle;colspan:colcount;"></td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr>
					<td rowspan="2">科室名称</td>
					<td rowspan="2">上年执行</td>
					<td colspan="4">本年科室计划</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
			</tr>
			<tr>
				<td style="display:none"></td>
				<td style="display:none"/>
				<td>单位下达</td>
				<td>科室建议</td>
				<td>差额</td>
				<td>差异率</td>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test=" position() = 2 or position() = 3 or position() = 4 or position() = 5 or position() = 6 or position() = 7">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	              <xsl:otherwise>
	              </xsl:otherwise>
	          </xsl:choose>
  		</xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>