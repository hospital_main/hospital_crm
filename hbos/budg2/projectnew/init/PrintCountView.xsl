<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		
			<thead>
				<tr noWrap='true' >
					
					<td style='fontsize:maintitle;colspan:colcount'>物资消耗52定额</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap='true'>
				  	<td nowrap='true'>项目编码</td>
				  	<td nowrap='true'>项目名称</td>
				  	<td nowrap='true'>项目类别</td>
				  	<td nowrap='true'>项目级别</td>
				  	<td nowrap='true'>项目年度</td>
				  	<td nowrap='true'>预算总额</td>
				  	<td nowrap='true'>申请人</td>
				  	<td nowrap='true'>负责人</td>
				  	<td nowrap='true'>项目用途</td>
				  	<td nowrap='true'>项目状态</td>
				  	<td nowrap='true'>申请科室</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
					<xsl:for-each select="td">
							<xsl:choose>
          	 	<xsl:when test="position()=1">
			            <th style="display:none" noWrap='true'></th>
	             </xsl:when>
	               <xsl:when test="position()=7">
										<td align='left'><xsl:value-of select="format-number(.,'###,##0.00')"/></td>
                </xsl:when>
	             <xsl:otherwise>
	              	<td align='left'><xsl:value-of select="."/></td>
	             </xsl:otherwise>
	             </xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
			<tfoot>
 			<tr noWrap='true'>
	        	<td style='fontsize:subtitle;colspan:colcount;align:right'></td>
  			</tr>
 			</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
