<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
  		
        <th style="display:none"><input type="checkbox"/></th>
        		<th>单据号码</th>
				<th>日    期</th>
				<th>项    目</th>
				<th>摘    要</th>
				<th>支出项目</th>
				<th>报销金额</th>
				<th>资金来源</th>
				<th>报 销 人</th>
				<th>报销科室</th>
				<th>票据号码</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      	      </xsl:attribute>
    	    </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=6">
			        <td align="left">
						<xsl:value-of select="format-number(.,'###,##0.00')"/>
					</td>
                </xsl:when>
               
			  	<xsl:otherwise>
			        <td align="left">
						<xsl:value-of select="."/>
					</td>
                </xsl:otherwise>
              </xsl:choose>
  		  </xsl:for-each>
  		</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
