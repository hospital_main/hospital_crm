<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		
			<thead>
				<tr noWrap='true' >
					
					<td style='fontsize:maintitle;colspan:colcount'></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					
				</tr>
				<tr noWrap='true'>
			        <td style='fontsize:subtitle;colspan:colcount;'></td>
		  		</tr>
				<tr noWrap='true'>
				  	<td nowrap='true'>执行日期</td>
				  	<td nowrap='true'>摘　　要</td>
				  	<td nowrap='true'>科　　室</td>
				  	<td nowrap='true'>职　　工</td>
				  	<td nowrap='true'>项目编码</td>
				  	<td nowrap='true'>项目名称</td>
				  	<td nowrap='true'>负 责 人</td>
				  	<td nowrap='true'>支出项目</td>
				  	<td nowrap='true'>资金来源</td>
				  	<td nowrap='true'>支出金额</td>
				  	<td nowrap='true'>票据号码</td>
				  	<td nowrap='true'>报 销 单</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
							<xsl:when test="position()=11 ">
							<td align="right">
								<xsl:value-of select="format-number(.,'###,##0.00')"/>
							</td>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
								<xsl:when test="position()!=1 and position()!=14 and position()!=15 ">
									<td align="left">
									<xsl:value-of select="."/>
									</td>
								</xsl:when>
								</xsl:choose>
							
							</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
			<tfoot>
 			<tr noWrap='true'>
	        	<td style='fontsize:subtitle;colspan:colcount;align:right'></td>
  			</tr>
 			</tfoot>
		</root>
	</xsl:template>
</xsl:stylesheet>
