<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true" rowspan="2" dataIndex="0" valign="middle">科目编码</th>
				<th nowrap="true" rowspan="2" dataIndex="0" valign="middle">科目名称</th>
				<th nowrap="true" colspan="3" dataIndex="0">支出预算</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">项目预算</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">合计</th>
				<th style="display:none"/>
				<th style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=2 or position() = 1">
			            <td  noWrap='true'>
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' align="right">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
