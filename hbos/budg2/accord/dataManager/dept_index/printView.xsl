<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
  		<tr noWrap="true" class="mainHead">
  		<td style="fontsize:coltitle;" width="180">科室名称</td>
  		<td style="fontsize:coltitle;" width="180">计划指标</td>
			<td style="fontsize:coltitle;" width="180">上年执行</td>
			<td style="fontsize:coltitle;" width="280">增长比例%</td>
			<td style="fontsize:coltitle;" width="330">本年计划</td>
  		</tr>  	
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test=" position() = 1 ">
	              </xsl:when>
	              <xsl:otherwise>
	              	<td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>