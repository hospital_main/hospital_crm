<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
				<tr noWrap="true" class="mainHead">
				  <th>ѡ��</th>
					<th>���ұ���</th>
					<th>��������</th>
					<th>�·�</th>
					<th>Ԥ���Ŀ����</th>					
					<th>Ԥ���Ŀ����</th>
					<th>���</th>
					<th>����</th>
				</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            </input>
          </td>
					<xsl:for-each select="td">
					  <xsl:choose>
					    <xsl:when test="position() = 6">
					      <td align="right">
					        <xsl:value-of select="format-number(.,'#,##0.00')" />
					      </td>
					    </xsl:when>
					    <xsl:otherwise>
    						<td noWrap="true">
    							<xsl:value-of select="."/>
    						</td>
    					</xsl:otherwise>
    				 </xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
