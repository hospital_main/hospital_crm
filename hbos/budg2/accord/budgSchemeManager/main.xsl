<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style='display:block' width='25'><input type='checkbox'/></th>
				<th>预置方案编号</th>
				<th>预置方案名称</th>
				<th>预置方案说明</th>
				<th>上传说明文档</th>
				<th>删除说明文档</th>
			</tr>
		</thead>
		<tbody>
	<xsl:for-each select="/root/tbody/tr">
	<tr>
          <td align='center'  noWrap='true'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      	      </xsl:attribute>
    	    </input>
          </td>
	  <xsl:for-each select="td">
		<xsl:choose>
		<xsl:when test="position()=1">
			<td align="center" noWrap="true">
				<xsl:value-of select="."/>
			</td>
		</xsl:when>
		<xsl:when test="position()=2">
			<td noWrap="true">
				<xsl:value-of select="."/>
			</td>
		</xsl:when>
		<xsl:when test="position()=3">
			<td align="center" noWrap="true">
				<a tabindex="-2" target="_new">
		                <xsl:attribute name="href" >
		                   /upload/<xsl:value-of select="../td[4]"/>
		                </xsl:attribute><xsl:value-of select="../td[3]"/>
				</a>
			</td>
		</xsl:when>
		<xsl:when test="position()=5">
			<td align="center" noWrap="true">
				<a tabindex="-2">
		                <xsl:attribute name="href" >
		                  javascript:openDialog('docInsert.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:360px;dialogHeight:240px', result)
		                </xsl:attribute><xsl:value-of select="."/>
				</a>
			</td>
			<xsl:if test="../td[4]=''">
				<td align="center" noWrap="true"></td>
			</xsl:if>
			<xsl:if test="../td[4]!= ''">
				<td align="center" noWrap="true">
					<a tabindex="-2">
		                	<xsl:attribute name="href" >
		                  	javascript:delDoc('<xsl:for-each select="../pk/*"><xsl:value-of select="."/></xsl:for-each>')
		                	</xsl:attribute>删除说明文档
					</a>
				</td>
			</xsl:if>
		</xsl:when>
		</xsl:choose>
	  </xsl:for-each>
	</tr>
	</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
