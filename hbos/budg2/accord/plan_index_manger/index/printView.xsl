<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
		<thead>
			<tr>
				<td style="fontsize:maintitle;colspan:colcount;"></td>
				<td style='display:none'></td>
				<td style='display:none'></td>
				<td style='display:none'></td>
				<td style='display:none'></td>
				<td style='display:none'></td>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td>年度</td>
				<td>操作员</td>
				<td>科室名称</td>
				<td>指标编号</td>
				<td>指标名称</td>
				<td>计量单位</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<td>
							<xsl:value-of select="."/>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
