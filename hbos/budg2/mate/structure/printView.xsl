<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/budg2/mate/structure/printView.xsl,v 1.2 2015/03/17 03:39:34 yaoqingsong Exp $
 $Author: yaoqingsong $
 $Date: 2015/03/17 03:39:34 $
 $Revision: 1.2 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">		
		<root>		
		<thead>
			<tr>
   			<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">13</xsl:attribute>
				</td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
  		<tr noWrap='true' class='mainHead'>
  			<td>财务编码</td>
  			<td>财务类别</td>
  			<td>预计物资采购量</td>
  			<td>本年门(急)诊人次</td>
				<td>门急诊人均耗用量</td>
				<td>门急诊耗用小计</td>
				<td>本年住院病人数</td>
				<td>住院病人人均耗用量</td>
				<td>住院耗用小计</td>
				<td>本年物资耗用量</td>
  			<td>物资留存比例(%)</td>
  			<td>预计期末物资库存量</td>
  			<td>期初物资库存量</td>  				  				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()!=1 and position()!=2">
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
