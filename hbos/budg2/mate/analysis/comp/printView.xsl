<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">12</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td valign="center" rowspan="3">财务类别编码</td>
					<td valign="center" rowspan="3">财务类别名称</td>
					<td  align="center" colspan="5">年度预算执行</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td  align="center" colspan="5">本期预算执行</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr>
										<td style="display:none"/>
						<td style="display:none"/>
						<td  align="center" colspan="3">预算</td>
												<td style="display:none"/>
						<td style="display:none"/>
						<td  valign="center" rowspan="2">物资执行</td>
						<td  valign="center" rowspan="2">执行进度(%)</td>

						<td  align="center" colspan="3">预算</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td  valign="center" rowspan="2">物资执行</td>
						<td  valign="center" rowspan="2">执行进度(%)</td>
				</tr>
				<tr>

						<td style="display:none"/>
						<td style="display:none"/>

						<td  align="center" >合计</td>
						<td  align="center" >年初预算</td>
						<td  align="center" >调整额</td>
						<td style="display:none"/>
						<td style="display:none"/>

						<td  align="center" >合计</td>
						<td  align="center" >期初预算</td>
						<td  align="center" >调整额</td>
						<td style="display:none"/>
						<td style="display:none"/>

				</tr>	
			</thead>
			
			<tbody>
	      <xsl:for-each select="/root/tbody/tr">
	        <tr>
	        	<xsl:for-each select="td">   
	            <xsl:choose>
	              <xsl:when test="position()=1 or position()=2">
	                <td align='right' noWrap='true'>
			  						<xsl:value-of select="."/>
									</td>
	              </xsl:when> 
	              <xsl:otherwise>
	                <td align="left"><xsl:value-of  select="format-number(.,'#,##0.00')"/></td>
	              </xsl:otherwise>
	            </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	   		
      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>
