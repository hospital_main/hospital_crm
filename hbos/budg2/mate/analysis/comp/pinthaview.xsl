<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
  	<tr>
  		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
			</tr>
		<tr>
			<td style="fontsize:coltitle;" width="180" rowspan="3" align="center">科室名称</td>
			<td style="fontsize:coltitle;" width="180" rowspan="3" align="center">科室名称</td>
			<td  width="180" colspan="5" align="center">年度预算执行</td>
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td width="180" colspan="5" align="center">本期预算执行</td>
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="diplay:none" />
		</tr>		
		<tr>	
			<td style="diplay:none" />
			<td style="fontsize:coltitle;" width="180" colspan="3" align="center">预算</td>
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="fontsize:coltitle;" width="180" rowspan="2" align="center">物资执行</td>
			<td style="fontsize:coltitle;" width="180" rowspan="2" align="center">执行进度(%)</td>
			<td style="fontsize:coltitle;" width="180" colspan="3" align="center">预算</td>
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="fontsize:coltitle;" width="180" rowspan="2" align="center">物资执行</td>
			<td style="fontsize:coltitle;" width="180" rowspan="2" align="center">执行进度(%)</td>
		</tr>	
		<tr>

			<td style="diplay:none" />
			<td style="fontsize:coltitle;" width="180" align="center" >合计</td>
			<td style="fontsize:coltitle;" width="180" align="center">年初预算</td>
			<td style="fontsize:coltitle;" width="180" align="center">调整额</td>
			<td style="diplay:none" />
			<td style="diplay:none" />
			<td style="fontsize:coltitle;" width="180" align="center">合计</td>
			<td style="fontsize:coltitle;" width="180" align="center">期初预算</td>
			<td style="fontsize:coltitle;" width="180" align="center">调整额</td>
			<td style="diplay:none" />
			<td style="diplay:none" />
		</tr>		
		</thead>  	 		

  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
            
	              <xsl:when test="position()=1 or position() = 2">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
                      <xsl:when test="	 position() = 3 or position() = 4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12">
                        <td align='right'>
                          <xsl:value-of select="format-number(.,'#,##0.00')"/>
                        </td>
                      </xsl:when>
	              <xsl:otherwise>
	              </xsl:otherwise>
	          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>