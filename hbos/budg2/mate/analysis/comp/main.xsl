<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
		<tr noWrap='true' class='mainHead'>
			<th valign="center" rowspan="3">财务类别编码</th>
			<th valign="center" rowspan="3">财务类别名称</th>
			<th align="center" colspan="5">年度预算执行</th>
			<th align="center" colspan="5">本期预算执行</th>
		</tr>
		<tr class='mainHead'>
				<th align="center" colspan="3">预算</th>
				<th align="center" rowspan="2">物资执行</th>
				<th align="center" rowspan="2">执行进度(%)</th>
				<th align="center" colspan="3">预算</th>
				<th align="center" rowspan="2">物资执行</th>
				<th align="center" rowspan="2">执行进度(%)</th>
		</tr>
		<tr class='mainHead'>
				<th align="center" >合计</th>
				<th align="center" >年初预算</th>
				<th align="center" >调整额</th>
				<th align="center" >合计</th>
				<th align="center" >期初预算</th>
				<th align="center" >调整额</th>
		</tr>		</thead>  	
   
   
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=2">
									<td noWrap="true">
										<a tabindex="-2">
			                <xsl:attribute name="href" >
			                  javascript:openDialog('detail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:600px')
			                </xsl:attribute><xsl:value-of select="."/></a>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td align="right">
										<xsl:value-of select="format-number(., '#,##0.00')"/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



