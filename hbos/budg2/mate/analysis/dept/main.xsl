<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
		<tr noWrap='true' class='mainHead'>
			<th valign="center" rowspan="3">财务类别编码</th>
			<th valign="center" rowspan="3">财务类别名称</th>
			<th align="center" colspan="5">年度预算执行</th>
			<th align="center" colspan="5">本期预算执行</th>
		</tr>
		<tr class='mainHead'>
				<th align="center" colspan="3">预算</th>
				<th align="center" rowspan="2">物资执行</th>
				<th lign="center" rowspan="2">执行进度(%)</th>
				<th lign="center" colspan="3">预算</th>
				<th lign="center" rowspan="2">物资执行</th>
				<th lign="center" rowspan="2">执行进度(%)</th>
		</tr>
		<tr class='mainHead'>
				<th lign="center" >合计</th>
				<th lign="center" >年初预算</th>
				<th lign="center" >调整额</th>
				<th lign="center" >合计</th>
				<th lign="center" >期初预算</th>
				<th lign="center" >调整额</th>
		</tr>		</thead>  	
   
   
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1 or position()=2">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td align="right">
										<xsl:value-of select="format-number(., '#,##0.00')"/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



