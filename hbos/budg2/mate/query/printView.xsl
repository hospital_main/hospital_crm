<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">15</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr noWrap='true' class='mainHead'>
						<td nowrap='true' valign='middle'>财务分类编码</td>
		      	<td nowrap='true' valign='middle'>财务分类名称</td>
		      	<td nowrap='true' valign='middle'>年度预算</td>
		      	<td nowrap='true' valign='middle'>1月</td>
		      	<td nowrap='true' valign='middle'>2月</td>
		      	<td nowrap='true' valign='middle'>3月</td>
		      	<td nowrap='true' valign='middle'>4月</td>
		      	<td nowrap='true' valign='middle'>5月</td>
		      	<td nowrap='true' valign='middle'>6月</td>
		      	<td nowrap='true' valign='middle'>7月</td>
		      	<td nowrap='true' valign='middle'>8月</td>
		      	<td nowrap='true' valign='middle'>9月</td>
		      	<td nowrap='true' valign='middle'>10月</td>
		      	<td nowrap='true' valign='middle'>11月</td>
		      	<td nowrap='true' valign='middle'>12月</td>
				</tr>
			
			</thead>
			
			<tbody>
	      <xsl:for-each select="/root/tbody/tr">
	        <tr>
	        	<xsl:for-each select="td">   
	            <xsl:choose>
	              <xsl:when test="position()=1">
	              </xsl:when> 
	              <xsl:when test="position()=2 or position()=3">
	                <td align='right' noWrap='true'>
			  						<xsl:value-of select="."/>
									</td>
	              </xsl:when> 
	              <xsl:otherwise>
	                <td align="left"><xsl:value-of  select="format-number(.,'#,##0.00')"/></td>
	              </xsl:otherwise>
	            </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	   		
      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>
