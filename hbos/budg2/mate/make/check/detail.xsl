<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="2">财务类别名称</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="2">科目预算</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" colspan="4">定额计算消耗</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="2">差额分配</td>
		</tr>
		<tr>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >计划指标名称</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >单位消耗定额</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >工作量</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >消耗总额</td>
		</tr>
	</thead>
	<tbody>
		<!--第一列 同值合并
		第二列 同值合并
		第七列 第二列减 第六列的合
		-->
		<xsl:for-each select="/root/tbody/tr">
			<xsl:variable name="vId" select="td[1]"/>
    	<xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[1]=$vId])"/>
    	<xsl:variable name="rowPos" select="position()"/>
			<tr>
					<xsl:for-each select="td">
							<xsl:choose>
								<!--开始-->
								<xsl:when test="position()=1">
									<xsl:if test="$rowPos = 1">
		                <td align="center">
	                	<xsl:if test="$vId != ''">
			                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
		                </xsl:if>
		                
		                <xsl:value-of select="."/>
		  	          	</td>
		  	          </xsl:if>

	                <xsl:if test="$rowPos &gt; 1">
	                	<xsl:if test="$vId != ../../tr[$rowPos - 1 ]/td[1]">
	                		<td align="center">
		                	<xsl:if test="$vId != ''">
				                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			                </xsl:if>
			                <xsl:value-of select="."/>
			  	          	</td>
		  	          	</xsl:if>

	                	<xsl:if test="$vId = ../../tr[$rowPos - 1 ]/td[1]">
		                	<xsl:if test="$vId != ''">
			                <td align="center" style="display:none" >
			  	          	</td>
			  	          	</xsl:if>
		                	<xsl:if test="$vId = ''">
			                <td align="center" >
			                	<xsl:value-of select="."/>
			  	          	</td>
			  	          	</xsl:if>
		  	          	</xsl:if>
		  	          </xsl:if>
		  	        </xsl:when>
								<!--结束-->
								<xsl:when test="position()=2">
									<xsl:if test="$rowPos = 1">
		                <td class="numberText">
	                	<xsl:if test="$vId != ''">
			                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
		                </xsl:if>
		                
		                <xsl:value-of select="format-number(.,'#,##0.00')"/>
		  	          	</td>
		  	          </xsl:if>

	                <xsl:if test="$rowPos &gt; 1">
	                	<xsl:if test="$vId != ../../tr[$rowPos - 1 ]/td[1]">
	                		<td class="numberText">
		                	<xsl:if test="$vId != ''">
				                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			                </xsl:if>
			                <xsl:value-of select="format-number(.,'#,##0.00')"/>
			  	          	</td>
		  	          	</xsl:if>

	                	<xsl:if test="$vId = ../../tr[$rowPos - 1 ]/td[1]">
		                	<xsl:if test="$vId != ''">
			                <td align="center" style="display:none" >
			  	          	</td>
			  	          	</xsl:if>
		                	<xsl:if test="$vId = ''">
			                <td class="numberText">
			                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			  	          	</td>
			  	          	</xsl:if>
		  	          	</xsl:if>
		  	          </xsl:if>
		  	        </xsl:when>
								<xsl:when test="position()=3">
									<td><xsl:value-of select="."/></td>
								</xsl:when>
								<!--第七列 -->
								<xsl:when test="position()=7">
									<xsl:if test="$rowPos = 1">
		                <td class="numberText">
		                	<xsl:if test="$vId != ''">
				                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			                </xsl:if>
											 <xsl:value-of select="format-number(.,'#,##0.00')"/>
		  	          	</td>
		  	          </xsl:if>

	                <xsl:if test="$rowPos &gt; 1">
	                	<xsl:if test="$vId != ../../tr[$rowPos - 1 ]/td[1]">
			                <td class="numberText">
		                	<xsl:if test="$vId != ''">
				                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			                </xsl:if>
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
			  	          	</td>
		  	          	</xsl:if>

	                	<xsl:if test="$vId = ../../tr[$rowPos - 1 ]/td[1]">
		                	<xsl:if test="$vId != ''">
				                <td align="center" style="display:none" >
				  	          	</td>
			                </xsl:if>
			                <xsl:if test="$vId = ''">
				                <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			                </xsl:if>
		  	          	</xsl:if>
		  	          </xsl:if>
								</xsl:when>
								<!--第七列结束-->
								<xsl:otherwise>
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
			</tr>			
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

