<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
		<xsl:variable name="tot_Cols" select="(count(/root/tbody/tr[td[2]=/root/tbody/tr[1]/td[2]]))* 4 +2"/>
		<xsl:variable name="month_Cols" select="count(/root/tbody/tr[td[2]=/root/tbody/tr[1]/td[2]])"/>
	  	<tr>
	   		<td style="fontsize:maintitle;">
	   		<xsl:attribute name="colspan">
	   			<xsl:value-of select="$tot_Cols"/>
	   		</xsl:attribute>
	   		</td>
				<td style="display:none"/>
				<xsl:for-each select="/root/tbody/tr[td[2]=/root/tbody/tr[1]/td[2]]">
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				
				</xsl:for-each>
			</tr>
			<tr>
				<td rowspan="2">科目编码</td>
				<td rowspan="2">科目名称</td>
				<xsl:for-each select="/root/tbody/tr[position() &lt;= $month_Cols]">
				  <td align="center" colspan="4">
				  	<xsl:if test="td[3]='0'">
							年度预算
						</xsl:if>
				  	<xsl:if test="td[3]!='0' and td[8]&gt;0">
							<xsl:value-of select="td[3]"/>月
						</xsl:if>
				  	<xsl:if test="td[3]!='0' and td[8]=0">
							<xsl:value-of select="td[3]"/>月
						</xsl:if>
					</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</xsl:for-each>
		</tr>
		<tr>
					<td style="display:none"/>
					<td style="display:none"/>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $month_Cols]">
				<td align="center" >科目预算</td>
				<td align="center" >物资汇总</td>
				<td align="center" >差异</td>
				<td align="center" >差异率</td>
			</xsl:for-each>
		</tr>

  	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:if test="( position() mod $month_Cols )=1 or $month_Cols = 1">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;
				</xsl:text>
				<td>
					<xsl:value-of select="td[1]"/>
				</td>
				<td>
					<xsl:value-of select="td[2]"/>
				</td>
			</xsl:if>
			
			<xsl:if test="(position() mod $month_Cols)!=0">
				<td align="right">
					<xsl:value-of select="format-number(td[4],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[5],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[6],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[7],'#,##0.00')"/>
				</td>
			</xsl:if>
			
			<xsl:if test="(position() mod $month_Cols)=0 or $month_Cols = 1">
				<td align="right">
					<xsl:value-of select="format-number(td[4],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[5],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[6],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[7],'#,##0.00')"/>
				</td>
				<xsl:text disable-output-escaping="yes">
					&lt;/tr&gt;
				</xsl:text>
			</xsl:if>
			
		</xsl:for-each>
	</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>