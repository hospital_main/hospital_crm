<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		
			<thead>
				<tr noWrap='true' >
					
					<td style='fontsize:maintitle;colspan:colcount'>物资消耗定额</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap='true' >
				  	<td nowrap='true'>科室编码</td>
				  	<td nowrap='true'>科室名称</td>
				  	<td nowrap='true'>财务分类编码</td>
				  	<td nowrap='true'>财务分类名称</td>
				  	<td nowrap='true'>计划指标名称</td>
				  	<td nowrap='true'>单位消耗定额</td>
				  	<td nowrap='true'>是否参与预算</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:variable name="xh" select="position()"/>
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
							<xsl:when test="position()=1">
								<td align="right">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()=2">
								<td>
									<xsl:for-each select="/root/tbody/tr/pk">
										<xsl:choose>
											<xsl:when test="position()=$xh">
												<xsl:value-of select="dept_name"/>
											</xsl:when>
										</xsl:choose>
									</xsl:for-each>
								</td>
							</xsl:when>
							<xsl:when test="position()=4 or position()=5 or  position()=7"> 
								<td><xsl:value-of select="substring-after(.,'|||')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
