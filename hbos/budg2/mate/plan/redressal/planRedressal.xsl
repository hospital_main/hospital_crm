<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
			<th>调整序号</th>
			<th>编制部门</th>
			<th>摘要</th>
			<th>编制人</th>
			<th>编制日期</th>
			<th>审核人</th>
			<th>审核日期</th>
			<th>状态</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
  			<xsl:variable name="row" select="position()"/>
  			<tr>
  				<xsl:for-each select="td">
  					  <xsl:variable name="cspan" select="/root/tbody/tr[$row]/td[1]"/>
  						<xsl:choose>
  							<xsl:when test="position()=2">
  								<td align='center'>
  									<a href="#">
  											<xsl:attribute name="onclick">
  												openUpdateDlg('<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="$cspan"/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>')
  											</xsl:attribute>
  									<xsl:value-of select="."/></a>	
  								</td>
  							</xsl:when>
								<xsl:when test="position()>2">
  								<td align='left'><xsl:value-of select="."/></td>
  							</xsl:when>
  						</xsl:choose>
  				</xsl:for-each>
  		  </tr>
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>