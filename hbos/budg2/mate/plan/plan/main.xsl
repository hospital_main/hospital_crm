<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/"> 
  	<thead>
  		<tr noWrap="true" class="mainHead">
        	<th style='display:none' width='25'><input type='checkbox'/></th>
			<th>需求计划单号</th>
			<th>编制部门</th>
			<th>摘要</th>
			<th>编制人</th>
			<th>编制日期</th>
			<th>审核人</th>
			<th>审核日期</th>
			<th>状态</th>
  		</tr>
  	</thead>
  	<tbody/>
	</xsl:template>
</xsl:stylesheet>