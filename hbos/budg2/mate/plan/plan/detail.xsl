<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        	<th style='display:none' width='25'><input type='checkbox'/></th>
			<th>需求计划单号</th>
			<th>编制部门</th>
			<th>摘要</th>
			<th>材料名称</th>
			<th>数量</th>
			<th>单价</th>
			<th>总价</th>
			<th>状态</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
  			<tr>
  				<td align='center' noWrap='true'>
            				<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              					<xsl:attribute name="value" >
                					<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              					</xsl:attribute>
    			 	 	</input>
          			</td>
  				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td align='center'>
							<a href="#">
							<xsl:attribute name="onclick">
								openUpdateDlg('<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>')
							</xsl:attribute>
							<xsl:value-of select="."/></a></td>
						</xsl:when>
			  			<xsl:when test="position()=5 or position()=6 or position()=7">
		                        		<td align='right'>
		                          			<xsl:value-of select="format-number(.,'#,##0.00')"/>
		                          		</td>
		                          	</xsl:when>
		                          	<xsl:otherwise>
			           			 <td align='left' noWrap='true'><xsl:value-of select="."/></td>
			  			</xsl:otherwise>
		  			</xsl:choose>
	  			</xsl:for-each>
  			</tr>
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>