<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		
			<thead>
				<tr noWrap='true' >
					
					<td style='fontsize:maintitle;colspan:colcount'></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap='true'>
				  	<td nowrap='true'>项目编码</td>
				  	<td nowrap='true'>项目名称</td>
				  	<td nowrap='true'>负责人</td>
				  	<td nowrap='true'>年初余额</td>
				  	<td nowrap='true'>预算增加</td>
				  	<td nowrap='true'>预算调整</td>
				  	<td nowrap='true'>预算总额</td>
				  	<td nowrap='true'>执行金额</td>
				  	<td nowrap='true'>剩余额度</td>
				  	<td nowrap='true'>执行进度</td>
				  	<td nowrap='true'>预警线</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
								<td><xsl:value-of select="."/></td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
