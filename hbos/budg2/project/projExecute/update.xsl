<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th noWrap='true' >申领序号</th>
  			<th noWrap='true' >预算明细编号</th>
  			<th noWrap='true' >预算明细名称</th>
  			<th noWrap='true' >预算明细分类</th>
  			<th noWrap='true' >预算科目</th>
  			<th noWrap='true' >支出经济分类</th>
  			<th noWrap='true' >预算额度</th>
  			<th noWrap='true' >支票号</th>
  			<th noWrap='true' >本次申领</th>
  			<th noWrap='true' >申领日期</th>
  			<th noWrap='true' >使用人</th>
  			<th noWrap='true' >本次执行</th>
  			<th noWrap='true' >执行日期</th>
  			<th noWrap='true' >说明</th>
  			<th noWrap='true' >状态</th>
  		</tr>
  	</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	 
           <xsl:for-each select="td">
           	<xsl:choose>
           		          		
           		<xsl:when test="position()=7 or position()=9 or position()=12" >
           			<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
           		</xsl:when>
           		<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
		          </xsl:otherwise>
		         </xsl:choose>
           </xsl:for-each>
         </tr>
         
      </xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>



