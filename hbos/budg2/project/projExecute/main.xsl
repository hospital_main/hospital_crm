<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th noWrap='true' >选择</th>
        <th noWrap='true' >项目编号</th>
  			<th noWrap='true' >项目名称</th>
  			<th noWrap='true' >支出功能分类</th>
  			<th noWrap='true' >预算年度</th>
  			<th noWrap='true' >业务状态</th>
  			<th noWrap='true' >预算批复</th>
  			<th noWrap='true' >调整额度</th>
  			<th noWrap='true' >可用额度</th>
  			<th noWrap='true' >已申领额度</th>
  			<th noWrap='true' >已执行额度</th>
  			<th noWrap='true' >本次执行合计</th>
  			<th noWrap='true' >剩余额度</th>
  		</tr>
  	</thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	 <td align="center">
        	 	<img src="/images2/menu/folderclose.png" closeimg="/images2/menu/folderclose.png" openimg="/images2/menu/folderopen.png" onclick="clickimg(this)" >
        	 		<xsl:attribute name="proj_code"><xsl:value-of select="./td[2]"/></xsl:attribute>
        	 		<xsl:attribute name="proj_id"><xsl:value-of select="./td[14]"/></xsl:attribute>
        	 	</img>
        	 	
        	 </td>
           <xsl:for-each select="td">
           	<xsl:choose>
           		<xsl:when test="position()=1 or position()=14">
           		
           		</xsl:when>
           		<xsl:when test="position()=2">
           			<td>
           				<a >
	           				<xsl:attribute name="href" >
	                      javascript:openDialog('update.html?load=&lt;proj_id&gt;<xsl:value-of select="../td[14]"/>&lt;/proj_id&gt;','dialogWidth:1000px;dialogHeight:900px')
	                    </xsl:attribute>
           					<xsl:value-of select="."/>
           				</a>
           			</td>
           		</xsl:when>
           		
           		<xsl:when test="position()>6" >
           			<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
           		</xsl:when>
           		<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
		          </xsl:otherwise>
		         </xsl:choose>
           </xsl:for-each>
         </tr>
         <tr style="display:none;" >
         	<xsl:attribute name="id">tr<xsl:value-of select="./td[2]"/></xsl:attribute>
         	<td colspan="13" >
         		<div width="100%">
	         		<xsl:attribute name="id">div<xsl:value-of select="./td[14]"/></xsl:attribute>
	         		<xsl:attribute name="proj_code"><xsl:value-of select="./td[2]"/></xsl:attribute>
	         		<xsl:attribute name="proj_id"><xsl:value-of select="./td[14]"/></xsl:attribute>
	         		<xsl:attribute name="manu_type"><xsl:value-of select="./td[1]"/></xsl:attribute>
	         		<font color="red">正在打开请稍候.......</font>
         		</div>
         	</td>
         </tr>
      </xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>



