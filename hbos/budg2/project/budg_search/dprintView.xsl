<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
  		<tr noWrap="true" class="mainHead">
  				<td style="fontsize:coltitle;" width="180">明细项目编号</td>
					<td style="fontsize:coltitle;" width="180">明细项目名称</td>
					<td style="fontsize:coltitle;" width="180">明细项目类别名称</td>
					<td style="fontsize:coltitle;" width="230">支出经济科目</td>
					<td style="fontsize:coltitle;" width="230">资金来源</td>
					<td style="fontsize:coltitle;" width="230">年初预算</td>
  		</tr>  	
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=6">
                <td align='right' noWrap='true'>
		  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
              </xsl:when> 
              <xsl:otherwise>
                <td align="left"><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<tr>   			
   			<td align="center" colspan="5">合计</td>
   			<td></td>
   			<td></td>
   			<td></td>
   			<td></td>
   			<td align="right">
   				<xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/>
   			</td>
   		</tr>   		
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>