<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th>序号</th>
        <th>项目编码</th>
        <th>项目名称</th>
        <th>项目年度</th>
        <th>项目功能分类</th>
        <th>项目业务状态</th>
        <th>年初预算金额</th>
        <th>调整</th>
        <th>实际执行金额</th>
        <th>剩余金额</th>
        <th>执行进度</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose> 
              <xsl:when test="position()=3">
                <td>
                	<xsl:if test="../td[1]!=''">
	                  <a>
	                    <xsl:attribute name="href" >
	                      javascript:openDialog('projView.html?load=&lt;proj_id&gt;<xsl:value-of select="../td[12]"/>&lt;/proj_id&gt;','dialogWidth:800px;dialogHeight:900px')
	                    </xsl:attribute>
	                    <xsl:value-of select="."/>
	                  </a>
                  </xsl:if>
                  <xsl:if test="../td[1]=''">
                  	<xsl:value-of select="."/>
                  </xsl:if>
                </td>
              </xsl:when>
              <xsl:when test="position()=7 or position()=8 or position()=9 or position()=10">
                <td align='right' noWrap='true'>
		  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
              </xsl:when> 
              <xsl:when test="position()=11">
                <td align='right' noWrap='true'>
		  						<xsl:value-of select="format-number(.,'#,##0.00')"/>%
								</td>
              </xsl:when> 
              <xsl:when test="position()=12">
                <td style="display:none">
		  						<xsl:value-of select="."/>
								</td>
              </xsl:when> 
              <!--xsl:when test="position()=14">
                <td>
                  <a>
                    <xsl:attribute name="href" >
                      javascript:openDialog('main2.html?load=&lt;apply_id&gt;<xsl:value-of select="../td[13]"/>&lt;/apply_id&gt;','dialogWidth:700px;dialogHeight:500px')
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                  </a>
                </td>
              </xsl:when-->
              <xsl:otherwise>
                <td align="center"><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>