<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th>顺序号</th>
  			<th>开始时间</th>
  			<th>结束时间</th>
  			<th>审批人编码</th>
  			<th>审批人姓名</th>
  			<th>科室编码</th>
  			<th>科室名称</th>
  			<th>审批意见</th>
  			<th>修改意见</th>
  			<th>审批状态</th>
  			<th>并审模式</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <td><xsl:value-of select="."/></td>
   			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>