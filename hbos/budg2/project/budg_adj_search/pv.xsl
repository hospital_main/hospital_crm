<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">9</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr noWrap='true' class='mainHead'>
						<td noWrap='true' rowspan="2">项目编码</td>
						<td noWrap='true' rowspan="2">项目名称</td>
						<td noWrap='true' rowspan="2">科室名称</td>
						<td noWrap='true' rowspan="2">项目分类</td>
						<td noWrap='true' rowspan="2">项目性质</td>
						<td noWrap='true' rowspan="2">功能分类</td>
						<td noWrap='true' colspan="3">预算总额</td>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' style="display:none"></td>
         </tr>
			 	  <tr noWrap='true' class='mainHead'>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' >合计</td>
						<td noWrap='true' >年初预算</td>
						<td noWrap='true' >调整数</td>
         </tr>
			</thead>
			<tbody>
	      <xsl:for-each select="/root/tbody/tr">
	        <tr>
	        	<xsl:for-each select="td">   
	            <xsl:choose>
	              <xsl:when test="position()=7 or position()=8 or position()=9">
	                <td align='right' noWrap='true'>
			  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
	              </xsl:when> 
	              <xsl:otherwise>
	                <td align="left"><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	            </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
   		<tr>
   			<td align="center" colspan="6">合计</td>
   			<td style="display:none"/>
   			<td style="display:none"/>
   			<td style="display:none"/>
   			<td style="display:none"/>
   			<td style="display:none"/>
   			<td align="left"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/></td>
   			<td align="left"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/></td>
   			<td align="left"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[9]),'#,##0.00')"/></td>
   		</tr>
      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>
