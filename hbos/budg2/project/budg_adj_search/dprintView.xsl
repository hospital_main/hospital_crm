<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <colgroup>		       
		      <col style = 'width:120mm'/>
		      <col style = 'width:220mm'/>
		      <col style = 'width:280mm'/>
		      <col style = 'width:100mm'/>
		      <col style = 'width:100mm'/>
		      <col style = 'width:80mm'/>
		      <col style = 'width:80mm'/>
		      <col style = 'width:80mm'/>
		    </colgroup>
	      <thead>
	      	<tr>
			   		<td style="fontsize:maintitle;colspan:colcount;"></td>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>
						<td style='display:none'/>		
					</tr>
			 	  <tr noWrap='true' class='mainHead'>
						<td noWrap='true' rowspan="2">明细项目编号</td>
						<td noWrap='true' rowspan="2">明细项目名称</td>
						<td noWrap='true' rowspan="2">明细项目类别名称</td>
						<td noWrap='true' rowspan="2">支出经济科目</td>
						<td noWrap='true' rowspan="2">资金来源</td>
						<td noWrap='true' colspan="3">合计</td>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' style="display:none"></td>
         </tr>
			 	  <tr noWrap='true' class='mainHead'>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' style="display:none"></td>
						<td noWrap='true' >合计</td>
						<td noWrap='true' >年初预算</td>
						<td noWrap='true' >调整预算</td>
         </tr>
			  </thead>
	    <tbody>
	      <xsl:for-each select="/root/tbody/tr">
	        <tr>
	        	<xsl:for-each select="td">   
	            <xsl:choose>
	              <xsl:when test="position()=6 or position()=7 or position()=8">
	                <td align='right' noWrap='true'>
			  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
	              </xsl:when> 
	              <xsl:otherwise>
	                <td align="left"><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	            </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	   		<tr>
	   			<td align="center" colspan="5">合计</td>
	   			<td noWrap='true' style="display:none"></td>
					<td noWrap='true' style="display:none"></td>
					<td noWrap='true' style="display:none"></td>
					<td noWrap='true' style="display:none"></td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/></td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/></td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/></td>
	   		</tr>	   		
      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>