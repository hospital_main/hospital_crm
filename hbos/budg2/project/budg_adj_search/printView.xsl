<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <colgroup>		       
		      <col style = 'width:120mm'/>
		      <col style = 'width:220mm'/>
		      <col style = 'width:80mm'/>
		      <col style = 'width:100mm'/>
		      <col style = 'width:100mm'/>
		      <col style = 'width:120mm'/>
		      <col style = 'width:80mm'/>
		      <col style = 'width:80mm'/>
		      <col style = 'width:80mm'/>
		    </colgroup>
	      <thead>
			 	  <tr noWrap='true' class='mainHead'>
						<th noWrap='true' rowspan="2">项目编码</th>
						<th noWrap='true' rowspan="2">项目名称</th>
						<th noWrap='true' rowspan="2">科室名称</th>
						<th noWrap='true' rowspan="2">项目分类</th>
						<th noWrap='true' rowspan="2">项目性质</th>
						<th noWrap='true' rowspan="2">功能分类</th>
						<th noWrap='true' colspan="3">预算总额</th>
						<th noWrap='true' style="display:none"></th>
						<th noWrap='true' style="display:none"></th>
         </tr>
			 	  <tr noWrap='true' class='mainHead'>
						<th noWrap='true' style="display:none"></th>
						<th noWrap='true' style="display:none"></th>
						<th noWrap='true' style="display:none"></th>
						<th noWrap='true' style="display:none"></th>
						<th noWrap='true' style="display:none"></th>
						<th noWrap='true' style="display:none"></th>
						<th noWrap='true' >合计</th>
						<th noWrap='true' >年初预算</th>
						<th noWrap='true' >调整数</th>
         </tr>
			  </thead>
	    <tbody>
	      <xsl:for-each select="/root/tbody/tr">
	        <tr>
	        	<xsl:for-each select="td">   
	            <xsl:choose>
	              <xsl:when test="position()=7 or position()=8 or position()=9">
	                <td align='right' noWrap='true'>
			  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
	              </xsl:when> 
	              <xsl:otherwise>
	                <td align="left"><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	            </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
   		<tr>
   			<td align="center" colspan="6">合计</td>
   			<td align="left"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/></td>
   			<td align="left"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/></td>
   			<td align="left"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[9]),'#,##0.00')"/></td>
   		</tr>
      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>