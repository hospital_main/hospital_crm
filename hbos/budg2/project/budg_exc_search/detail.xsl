<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th rowspan="2">明细项目编码</th>
        <th rowspan="2">明细项目名称</th>
        <th rowspan="2">明细项目类别名称</th>
        <th rowspan="2">支出经济科目</th>
        <th rowspan="2">资金来源</th>
        <th rowspan="2">上年结转</th>
        <th colspan="3">本年预算</th>
        <th colspan="3">预算执行</th>
        <th rowspan="2">可用预算余额</th>
        <th rowspan="2">进度比例(%)</th>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
        <th>合计</th>
        <th>年初预算</th>
        <th>调整预算</th>
        <th>合计</th>
        <th>截至上月实际执行</th>
        <th>本月已执行</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13">
                <td align='right' noWrap='true'>
		  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
              </xsl:when> 
              <xsl:when test="position()=14">
                <td align='right' noWrap='true'>
		  						<xsl:value-of select="format-number(.,'#,##0.00')"/>%
								</td>
              </xsl:when> 
              <xsl:otherwise>
                <td align="left"><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<tr>
   			<td align="center" colspan="5">合计</td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[9]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[10]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[11]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[12]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[13]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number((sum(/root/tbody/tr/td[10]) div sum(/root/tbody/tr/td[7])) * 100,'#,##0.00')"/>%</td>
   		</tr>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>