<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
		<tr noWrap='true' class='mainHead'>
				<td style="fontsize:coltitle;" width="180">明细项目编号</td>
				<td style="fontsize:coltitle;" width="180">明细项目名称</td>
				<td style="fontsize:coltitle;" width="180">明细项目类别名称</td>
				<td style="fontsize:coltitle;" width="180">支出经济科目</td>
				<td style="fontsize:coltitle;" width="180">资金来源</td>
				<td style="fontsize:coltitle;" width="180">上年结转</td>
				<td style="fontsize:coltitle;" width="180"></td>
				<td style="fontsize:coltitle;" width="180">本年预算</td>
				<td style="fontsize:coltitle;" width="180"></td>
				<td style="fontsize:coltitle;" width="180"></td>
				<td style="fontsize:coltitle;" width="180">预算执行</td>
				<td style="fontsize:coltitle;" width="180"></td>
				<td style="fontsize:coltitle;" width="180">可用预算金额</td>
				<td style="fontsize:coltitle;" width="180">进度比例(%)</td>
         </tr>
		 <tr noWrap='true' class='mainHead'>
				<td style="fontsize:coltitle;" width="180"></td>
				<td style="fontsize:coltitle;" width="180"></td>
				<td style="fontsize:coltitle;" width="180"></td>
				<td style="fontsize:coltitle;" width="180"></td>
				<td style="fontsize:coltitle;" width="180"></td>
				<td style="fontsize:coltitle;" width="180"></td>
				<td style="fontsize:coltitle;" width="180">合计</td>
				<td style="fontsize:coltitle;" width="180">年初预算</td>
				<td style="fontsize:coltitle;" width="180">调整预算</td>
				<td style="fontsize:coltitle;" width="180">合计</td>
				<td style="fontsize:coltitle;" width="180">截至上月实际执行</td>
				<td style="fontsize:coltitle;" width="180">本月已执行</td>
				<td style="fontsize:coltitle;" width="180"></td>
				<td style="fontsize:coltitle;" width="180"></td>
         </tr>
  	</thead>  	
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=2 or position()=1">
			            <td  noWrap='true'>
	                  		<xsl:value-of select="."/>
			            </td>
                </xsl:when>
                <xsl:when test="position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13">
			            <td align="right" noWrap='true' style="display:none">
							<xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:when>
				<xsl:when test="position() = 14">
						<td align="right" noWrap='true' style="display:none">
							<xsl:value-of select="format-number(.,'#,##0.00')"/>%
			            </td>
				</xsl:when>
			  	<xsl:otherwise>
			            <td  align="left" noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>        
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
		<tr>
   			<td align="center" colspan="5">合计</td>
				<td noWrap='true' style="display:none"></td>
				<td noWrap='true' style="display:none"></td>
				<td noWrap='true' style="display:none"></td>
				<td noWrap='true' style="display:none"></td>
			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[9]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[10]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[11]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[12]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[13]),'#,##0.00')"/></td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[14]),'#,##0.00')"/>%</td>
   		</tr>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>