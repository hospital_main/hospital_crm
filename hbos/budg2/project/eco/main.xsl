<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th>序号</th>
        <th>项目编码</th>
        <th>项目名称</th>
        <th>项目年度</th>
        <th>项目功能分类</th>
        <th>项目业务状态</th>
        <th>会计科目</th>
        <th>经济分类</th>
        <th>财政资金来源</th>
        <th>上年结转</th>
        <th>年初预算金额</th>
        <th>调整数</th>
        <th>实际执行金额</th>
        <th>执行进度</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td align='center' noWrap='true'>
                	<xsl:if test="../td[3]!='小计' and ../td[3]!='合计'">
		  							<xsl:value-of select="."/>
		  						</xsl:if>
                	<xsl:if test="../td[3]='小计' or ../td[3]='合计'">
		  							
		  						</xsl:if>
								</td>
              </xsl:when> 
              <xsl:when test="position()=2">
                <td>
                	<xsl:if test="../td[3]!='小计' and ../td[3]!='合计'">
	                  <a>
	                    <xsl:attribute name="href" >
	                      javascript:openDialog('update.html?load=&lt;proj_id&gt;<xsl:value-of select="../td[15]"/>&lt;/proj_id&gt;','dialogWidth:900px;dialogHeight:900px')
	                    </xsl:attribute>
	                    <xsl:value-of select="."/>
	                  </a>
                  </xsl:if>
                	<xsl:if test="../td[3]='小计' or ../td[3]='合计'">
                  	
                  </xsl:if>
                </td>
              </xsl:when>
              <xsl:when test="position()=10 or position()=11 or position()=12 or position()=13">
                <td align='right' noWrap='true'>
		  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
              </xsl:when> 
              <xsl:when test="position()=14">
                <td align='right' noWrap='true'>
		  						<xsl:value-of select="format-number(.,'#,##0.00')"/>%
								</td>
              </xsl:when> 
              <xsl:when test="position()=15 or position()=16">
                <td style="display:none">
		  						<xsl:value-of select="."/>%
								</td>
              </xsl:when>
              <xsl:otherwise>
                <td align="left"><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>