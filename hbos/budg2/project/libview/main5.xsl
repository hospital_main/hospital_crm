<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  	<tr noWrap='true' class='mainHead'>
        <th width="100">可行性指标</th>
        <th width="100">预期目标</th>
        <th width="200">实际效果</th>
        <th width="200">说明</th>
  	</tr>
  	</thead>
  	<tbody>
  	  <xsl:variable name="real_effect">real_effect</xsl:variable>
  	  <xsl:variable name="memo">memo</xsl:variable>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=3">
                <td align='center'>
                  <input type="text"  extent="200" name="memo" class="inputTextA"><!--extent="200"-->
                    <xsl:attribute name="id">
    	                <xsl:value-of select="$real_effect"/><xsl:value-of select="../td[5]"/>
    	              </xsl:attribute>
    	              <xsl:attribute name="value">
    	                <xsl:value-of select="."/>
    	              </xsl:attribute>
                  </input>
                </td>
              </xsl:when>
              <xsl:when test="position()=4">
                <td align='center'>
                  <input type="text" extent="200" name="memo" class="inputTextA">
                    <xsl:attribute name="id">
    	                <xsl:value-of select="$memo"/><xsl:value-of select="../td[5]"/>
    	              </xsl:attribute>
    	              <xsl:attribute name="value">
    	                <xsl:value-of select="."/>
    	              </xsl:attribute>
                  </input>
                </td>
              </xsl:when>
              <xsl:when test="position()>4">
                <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>