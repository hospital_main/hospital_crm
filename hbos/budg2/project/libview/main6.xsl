<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th>�ĵ�����</th>
        <th>�ĵ�����</th>
  		</tr>
  	</thead>
  	<tbody>
  	  <xsl:variable name="content">content</xsl:variable>
  	  <xsl:variable name="doc_name">doc_name</xsl:variable>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td align='center'>
                  <input type="text" extent="200" name="memo" class="inputTextA">
                    <xsl:attribute name="id">
    	                <xsl:value-of select="$doc_name"/><xsl:value-of select="../td[3]"/>
    	              </xsl:attribute>
    	              <xsl:attribute name="value">
    	                <xsl:value-of select="."/>
    	              </xsl:attribute>
                  </input>
                </td>
              </xsl:when>
              <xsl:when test="position()=2">
                <td align='center'>
                  <input type="text" extent="200" name="memo" class="inputTextA">
                    <xsl:attribute name="id">
    	                <xsl:value-of select="$content"/><xsl:value-of select="../td[3]"/>
    	              </xsl:attribute>
    	              <xsl:attribute name="value">
    	                <xsl:value-of select="."/>
    	              </xsl:attribute>
                  </input>
                </td>
              </xsl:when>
              <xsl:when test="position()>2">
                <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>