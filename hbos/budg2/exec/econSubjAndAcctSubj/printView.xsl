
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
					<td style="fontsize:maintitle;colspan:colcount;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td>经济分类编码</td>
					<td>经济分类名称</td>
					<td>科目编码</td>
					<td>科目名称</td>
				 </tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test=" position() = 1 ">
								</xsl:when>
								<xsl:otherwise>
								<td  align="center" class="centerText">
										<xsl:value-of select="."/>
								</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
