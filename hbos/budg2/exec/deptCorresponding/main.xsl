<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true' onclick="check(this)">选择</th>
  			<th nowrap='true'>科室编码</th>
  			<th nowrap='true'>科室名称</th>
  			<th nowrap='true'>对应科室编码</th>
  			<th nowrap='true'>对应科室名称</th>
  			<th nowrap='true'>数据来源类型</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  >
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">          
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                	<a tabindex='-1'>
	                  <xsl:attribute name="href" >
	    	            	javascript:openDialog('update.html?load=&lt;id&gt;<xsl:value-of select="../td[1]"/>&lt;/id&gt;&lt;dept_map_code&gt;<xsl:value-of select="../td[3]"/>&lt;/dept_map_code&gt;', 'dialogWidth:350px;dialogHeight:350px', result)
	  	          		</xsl:attribute>
	  	          		<xsl:value-of select="."/>
  	          		</a>
                </td>
              </xsl:when>
							<xsl:when test="position()>6">
                <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>