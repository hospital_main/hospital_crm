<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>会计科目</th>
  			<th nowrap='true'>科室名称</th>
  			<th nowrap='true'>数据来源</th>
  			<th nowrap='true'>数据标记</th>
  			<th nowrap='true'>账套</th>
  			<th nowrap='true'>错误原因</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <td><xsl:value-of select="."/></td>         
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>