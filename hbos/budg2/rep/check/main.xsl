<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
      	<th nowrap='true'>项目编码</th>
      	<th nowrap='true'>项目公式</th>
      	<th nowrap='true'>审核结果</th>
      	<th nowrap='true'>错误提示</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:if test="td[position()=3]!='正确'">
            <xsl:attribute name="style"> color :red</xsl:attribute>            
          </xsl:if>
          <xsl:for-each select="td">
            <td><xsl:value-of select="."/></td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

