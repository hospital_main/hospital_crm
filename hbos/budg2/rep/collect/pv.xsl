<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">4</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td nowrap='true'>汇总表编码</td>
      		<td nowrap='true'>汇总表名称</td>
      		<td nowrap='true'>汇总说明</td>
      		<td nowrap='true'>汇总设置</td>
  		  </tr>
			</thead>
			<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td[position() != 1]">
                <td><xsl:value-of select="."/></td>
  			  </xsl:for-each>
  			  <td>设置</td>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>
