<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th nowrap='true'>ѡ��</th>
		  	<th nowrap='true'>����</th>
		  	<th nowrap='true'>����</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="value"><xsl:value-of select="td[1]"/>:<xsl:value-of select="td[3]"/>:<xsl:value-of select="td[4]"/></xsl:variable>
        <tr value="{$value}" onclick="selComp(this);">
        	<xsl:if test="position() = 1">
        		<xsl:attribute name="id">firstComp</xsl:attribute>
        	</xsl:if>
        	<td align="center"><input type="radio" name="radio" id="comp_{td[1]}" onclick="radioClick(this);" value="{$value}"/></td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position() &gt; 2">
                </xsl:when>
                <xsl:when test="position() = 1">
                  <xsl:if test="../td[4]=0">
                  <td><xsl:value-of select="substring-after(.,'_')"/></td>
                  </xsl:if>
                  <xsl:if test="../td[4]=1">
                  <td><xsl:value-of select="."/></td>
                  </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                	<td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>