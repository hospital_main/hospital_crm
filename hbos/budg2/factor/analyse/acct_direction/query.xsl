<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="cspan" select="/root/tbody/tr[1]/td[3]"/>   
			<xsl:for-each select="/root/tbody/tr">
				<xsl:if test="position()=1">
					<tr noWrap="true" class="mainHead">
						<th noWrap="true" rowspan="2" valign="center">科目编码</th>
						<th noWrap="true" rowspan="2" valign="center">科目名称</th>
						<xsl:for-each select="td">
							<xsl:if test=" position() &gt; 3 and ($cspan = 1 or ( (position() - 3) mod $cspan ) = 0)">
								<th noWrap="true">
									<xsl:attribute name="colspan" >
	      			      <xsl:value-of select="$cspan"/>
    	  			    </xsl:attribute>
									<xsl:value-of select="substring-before(.,'-')"/>
								</th>
							</xsl:if>
						</xsl:for-each>
					</tr>
					<tr noWrap="true" class="mainHead">						
						<xsl:for-each select="td">
							<xsl:if test="position()>3">
								<th noWrap="true">
									<xsl:value-of select="substring-after(.,'-')"/>
								</th>
							</xsl:if>
						</xsl:for-each>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:if test="position()>1">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position() = 1">
								</xsl:when>
								<xsl:when test="position() &gt; 3">
									<td align="right"><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:otherwise>
									<td noWrap="true">
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
