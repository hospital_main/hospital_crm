<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
			<xsl:variable name="cspan" select="/root/t2head/tr[1]/th[2]"/> 
			<xsl:for-each select="/root/t2head/tr">
	  		<tr noWrap="true" class="mainHead">
	  			<th nowrap='true' rowspan="2" valign="middle">���ұ���</th>
	  			<th nowrap='true' rowspan="2" valign="middle">��������</th>
					<xsl:for-each select="th">
						<xsl:if test=" (position()>2 and $cspan =1) or ( (position() - 2) mod $cspan ) = 1">
							<th noWrap="true">
								<xsl:attribute name="colspan" >
      			      <xsl:value-of select="$cspan"/>
  	  			    </xsl:attribute>
								<xsl:value-of select="substring-before(.,'-')"/>
							</th>
						</xsl:if>
					</xsl:for-each>
	  		</tr>
				<tr noWrap="true" class="mainHead">						
					<xsl:for-each select="th">
						<xsl:if test="position()>2">
							<th noWrap="true">
								<xsl:value-of select="substring-after(.,'-')"/>
							</th>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
					<xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1 or position()=2">
								<td><xsl:value-of select="."/></td>
              </xsl:when>
		  			  <xsl:otherwise>
		            <td  noWrap='true' align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		            </td>
              </xsl:otherwise>
            </xsl:choose>
					</xsl:for-each>
				</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
