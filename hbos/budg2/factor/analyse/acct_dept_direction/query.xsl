<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="cspan" select="/root/tbody/tr[1]/td[2]"/>   
			<xsl:for-each select="/root/tbody/tr">
				<xsl:if test="position()=1">
					<tr noWrap="true" class="mainHead">
						<th noWrap="true" rowspan="2" valign="center">科室编码</th>
						<th noWrap="true" rowspan="2" valign="center">科室名称</th>
						<xsl:for-each select="td">
							<xsl:if test=" position() &gt; 2 and ($cspan = 1 or ( (position() - 2) mod $cspan ) = 1)">
								<th noWrap="true">
									<xsl:attribute name="colspan" >
	      			      <xsl:value-of select="$cspan"/>
    	  			    </xsl:attribute>
									<xsl:value-of select="substring-before(.,'-')"/>
								</th>
							</xsl:if>
						</xsl:for-each>
					</tr>
					<tr noWrap="true" class="mainHead">
						<xsl:for-each select="td">
							<xsl:if test="position()>2">
								<th noWrap="true">
									<xsl:value-of select="substring-after(.,'-')"/>
								</th>
							</xsl:if>
						</xsl:for-each>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:if test="position() = 2">
					<tr>
						<td></td>	
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position() &gt; 2">
									<td align="right"><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:when test="position() = 1">
									<td>合计</td>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:if>			
				<xsl:if test="position() &gt; 2">
					<tr>					
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position() &gt; 2">
									<td align="right"><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:otherwise>
									<td noWrap="true">
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
