<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th nowrap="true" rowspan="3" valign="center">科目代码</th>
				<th nowrap="true" rowspan="3" valign="center">科目名称</th>
				<th nowrap="true" rowspan="3" valign="center">行次</th>
				<th nowrap="true" colspan="6">全年预算及执行情况对比</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>				
				<th nowrap="true" colspan="6">本期预算及执行情况对比</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th nowrap="true" colspan="3">全年预算金额</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th nowrap="true" rowspan="2" valign="center">实际执行金额</th>
				<th nowrap="true" rowspan="2" valign="center">实际执行率</th>
				<th nowrap="true" rowspan="2" valign="center">全年余额</th>
				<th nowrap="true" colspan="3">本期预算金额</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th nowrap="true" rowspan="2" valign="center">本期执行金额</th>
				<th nowrap="true" rowspan="2" valign="center">本期执行率</th>
				<th nowrap="true" rowspan="2" valign="center">本期余额</th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th nowrap="true" >合计</th>
				<th nowrap="true">年初</th>
				<th nowrap="true" >调整</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>				
				<th nowrap="true" >合计</th>
				<th nowrap="true">年初</th>
				<th nowrap="true" >调整</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="r" select="position()"/>
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1 or position()=2">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=3">
								<td><xsl:value-of select="$r"/></td>
							</xsl:when>
							<xsl:when test="position()=8 or position()=14">
								<td align="right"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
