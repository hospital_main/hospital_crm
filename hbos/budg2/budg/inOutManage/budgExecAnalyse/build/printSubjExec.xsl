<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>

		<thead>
				<tr>
				<td style="fontsize:maintitle;colspan:colcount;"></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td nowrap="true" rowspan="3" valign="center">科目代码</td>
				<td nowrap="true" rowspan="3" valign="center">科目名称</td>
				<td nowrap="true" rowspan="3" valign="center">行次</td>
				<td nowrap="true" colspan="6">全年预算及执行情况对比</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>				
				<td nowrap="true" colspan="6">本期预算及执行情况对比</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td nowrap="true" colspan="3">全年预算金额</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td nowrap="true" rowspan="2" valign="center">实际执行金额</td>
				<td nowrap="true" rowspan="2" valign="center">实际执行率</td>
				<td nowrap="true" rowspan="2" valign="center">全年余额</td>
				<td nowrap="true" colspan="3">本期预算金额</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td nowrap="true" rowspan="2" valign="center">本期执行金额</td>
				<td nowrap="true" rowspan="2" valign="center">本期执行率</td>
				<td nowrap="true" rowspan="2" valign="center">本期余额</td>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td nowrap="true" >合计</td>
				<td nowrap="true">年初</td>
				<td nowrap="true" >调整</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>				
				<td nowrap="true" >合计</td>
				<td nowrap="true">年初</td>
				<td nowrap="true" >调整</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
		</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="r" select="position()"/>
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1 or position()=2">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=3">
								<td><xsl:value-of select="$r"/></td>
							</xsl:when>
							<xsl:when test="position()=8 or position()=14">
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
