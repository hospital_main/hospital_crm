<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>			
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>			
		</tr>
  		<tr noWrap="true" class="mainHead">
				<td style="fontsize:coltitle;" nowrap="true" rowspan="2" >科室编码</td>
				<td style="fontsize:coltitle;" nowrap="true" rowspan="2">科室名称</td>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2" >年度预算</td>
				<td style='display:none'/>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2">1月</td>
				<td style='display:none'/>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2">2月</td>
				<td style='display:none'/>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2">3月</td>
				<td style='display:none'/>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2">4月</td>
				<td style='display:none'/>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2">5月</td>
				<td style='display:none'/>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2">6月</td>
				<td style='display:none'/>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2">7月</td>
				<td style='display:none'/>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2">8月</td>
				<td style='display:none'/>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2">9月</td>
				<td style='display:none'/>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2">10月</td>
				<td style='display:none'/>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2">11月</td>
				<td style='display:none'/>
				<td style="fontsize:coltitle;" nowrap="true" colspan="2">12月</td>
				<td style='display:none'/>
  		</tr>
			<tr noWrap="true" class="mainHead">
				<td style="fontsize:coltitle" nowrap="true">预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
				<td style="fontsize:coltitle" nowrap="true" >预算额</td>
				<td style="fontsize:coltitle" nowrap="true" >比重(%)</td>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true'>
			            	<xsl:if test="../td[1]!='0'">
	                  	<xsl:value-of select="."/>
			            	</xsl:if>
			            </td>
                </xsl:when>
                <xsl:when test="position()=2">
			            <td  noWrap='true'>
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' align="right">
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  	</root>
	</xsl:template>
</xsl:stylesheet>
