<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="aa" select="count(/root/tbody/tr[1]/td)"/>
			<xsl:if test="$aa = 0">
				<tr noWrap="true" class="mainHead">
					<th>科室编码</th>
					<th>科室名称</th>
					<th>收入预算</th>
					<th>支出预算</th>
					<th>预算结余</th>
					<th>结余率</th>
				</tr>
			</xsl:if>
			<xsl:if test="$aa = 14">
				<tr noWrap="true" class="mainHead">
					<th rowspan="2">科室编码</th>
					<th rowspan="2">科室名称</th>
					<th colspan="3">收入预算</th>
					<th colspan="3">支出预算</th>
					<th colspan="3">预算结余</th>
					<th colspan="2">结余率</th>
				</tr>
				<tr noWrap="true" class="mainHead">
					<th>上年执行数</th>
					<th>本年预算数</th>
					<th>差异数</th>
					<th>上年执行数</th>
					<th>本年预算数</th>
					<th>差异数</th>
					<th>上年结余</th>
					<th>本年结余</th>
					<th>差异数</th>
					<th>上年结余率</th>
					<th>本年结余率</th>
				</tr>
			</xsl:if>
			<xsl:if test="$aa = 7">
				<tr noWrap="true" class="mainHead">
					<th>科室编码</th>
					<th>科室名称</th>
					<th>收入预算</th>
					<th>支出预算</th>
					<th>预算结余</th>
					<th>结余率</th>
				</tr>
			</xsl:if>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:variable name="bb" select="td[1]"/>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td noWrap="true" style="display:none">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()=2 or position()=3">
								<td noWrap="true">
									<xsl:if test="$bb = '0'">
										<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()> 3 ">
								<td noWrap="true" align="right">
									<xsl:if test="$bb = '0'">
										<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="format-number(.,'##,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
