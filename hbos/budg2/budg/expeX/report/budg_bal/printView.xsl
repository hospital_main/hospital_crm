<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<root>
	<thead>
			
	
			<xsl:variable name="aa" select="count(/root/tbody/tr[1]/td)"/>
			<tr>
				<xsl:if test="$aa = 14">
				<td style="fontsize:maintitle;colspan:colcount;"></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				</xsl:if>
				<xsl:if test="$aa = 7">
				<td style="fontsize:maintitle;colspan:colcount;"></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				
				</xsl:if>
				
			</tr>
			
			<xsl:if test="$aa = 14">
				<tr noWrap="true" class="mainHead">
					<td rowspan="2">科室编码</td>
					<td rowspan="2">科室名称</td>
					<td colspan="3">收入预算</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td colspan="3">支出预算</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td colspan="3">预算结余</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td colspan="2">结余率</td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap="true" class="mainHead">
					<td style="display:none"></td>	
					<td style="display:none"></td>	
					<td style="fontsize:coltitle;" width="180">上年执行数</td>
					<td style="fontsize:coltitle;" width="180">本年预算数</td>
					<td style="fontsize:coltitle;" width="180">差异数</td>
					<td style="fontsize:coltitle;" width="180">上年执行数</td>
					<td style="fontsize:coltitle;" width="180">本年预算数</td>
					<td style="fontsize:coltitle;" width="180">差异数</td>
					<td style="fontsize:coltitle;" width="180">上年结余</td>
					<td style="fontsize:coltitle;" width="180">本年结余</td>
					<td style="fontsize:coltitle;" width="180">差异数</td>
					<td style="fontsize:coltitle;" width="180">上年结余率</td>
					<td style="fontsize:coltitle;" width="150">本年结余率</td>
				</tr>
			</xsl:if>
			<xsl:if test="$aa = 7">
				<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;" width="180">科室编码</td>
					<td style="fontsize:coltitle;" width="180">科室名称</td>
					<td style="fontsize:coltitle;" width="180">收入预算</td>
					<td style="fontsize:coltitle;" width="180">支出预算</td>
					<td style="fontsize:coltitle;" width="180">预算结余</td>
					<td style="fontsize:coltitle;" width="150">结余率</td>
				</tr>
			</xsl:if>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								
							</xsl:when>
							<xsl:when test="position()=2 or position()=3">
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()> 3 ">
								<td noWrap="true" align="right">
									<xsl:value-of select="format-number(.,'##,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>
