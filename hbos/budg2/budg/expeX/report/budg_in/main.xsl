<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="dispLast" select="/root/tbody/tr[1]/td[2]"/>
			<xsl:variable name="dispDiff" select="/root/tbody/tr[1]/td[3]"/>
			<xsl:if test="$dispLast &lt; 1">
				<tr noWrap="true" class="mainHead">
					<th rowspan="2">科室名称</th>
					<th colspan="3">门诊预算收入</th>
					<th colspan="3">住院预算收入</th>
					<th rowspan="2">预算合计</th>
				</tr>
				<tr noWrap="true" class="mainHead">
					<th>门诊人次</th>
					<th>诊次费用</th>
					<th>预算金额</th>
					<th>住院床日</th>
					<th>床日费用</th>
					<th>预算金额</th>
				</tr>
			</xsl:if>
			<xsl:if test="($dispLast &gt; 0)">
				<tr noWrap="true" class="mainHead">
					<th rowspan="3">科室名称</th>
					<xsl:if test="$dispDiff &lt; 1">
						<th colspan="6">门诊预算收入</th>
						<th colspan="6">住院预算收入</th>
					</xsl:if>
					<xsl:if test="$dispDiff &gt; 0">
						<th colspan="9">门诊预算收入</th>
						<th colspan="9">住院预算收入</th>
					</xsl:if>
					<th colspan="3" rowspan="2">合计</th>
				</tr>
				<xsl:if test="$dispDiff &lt; 1">
					<tr noWrap="true" class="mainHead">
						<th colspan="2">门诊人次</th>
						<th colspan="2">诊次费用</th>
						<th colspan="2">金额</th>
						<th colspan="2">住院床日</th>
						<th colspan="2">床日费用</th>
						<th colspan="2">金额</th>
					</tr>
				</xsl:if>
				<xsl:if test="$dispDiff &gt; 0">
					<tr noWrap="true" class="mainHead">
						<th colspan="3">门诊人次</th>
						<th colspan="3">诊次费用</th>
						<th colspan="3">金额</th>
						<th colspan="3">住院床日</th>
						<th colspan="3">床日费用</th>
						<th colspan="3">金额</th>
					</tr>
				</xsl:if>
				<tr noWrap="true" class="mainHead">
					<th noWrap="true">上年</th>
					<th noWrap="true">今年</th>
					<xsl:if test="$dispDiff &gt; 0">
						<th noWrap="true">差异率(%)</th>
					</xsl:if>
					<th noWrap="true">上年</th>
					<th noWrap="true">今年</th>
					<xsl:if test="$dispDiff &gt; 0">
						<th noWrap="true">差异率(%)</th>
					</xsl:if>
					<th noWrap="true">上年</th>
					<th noWrap="true">今年</th>
					<xsl:if test="$dispDiff &gt; 0">
						<th noWrap="true">差异率(%)</th>
					</xsl:if>
					<th noWrap="true">上年</th>
					<th noWrap="true">今年</th>
					<xsl:if test="$dispDiff &gt; 0">
						<th noWrap="true">差异率(%)</th>
					</xsl:if>
					<th noWrap="true">上年</th>
					<th noWrap="true">今年</th>
					<xsl:if test="$dispDiff &gt; 0">
						<th noWrap="true">差异率(%)</th>
					</xsl:if>
					<th noWrap="true">上年</th>
					<th noWrap="true">今年</th>
					<xsl:if test="$dispDiff &gt; 0">
						<th noWrap="true">差异率(%)</th>
					</xsl:if>
					<th noWrap="true">上年</th>
					<th noWrap="true">今年</th>
					<xsl:if test="$dispDiff &gt; 0">
						<th noWrap="true">差异率(%)</th>
					</xsl:if>
				</tr>
			</xsl:if>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:variable name="bb" select="td[1]"/>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=4">
								<td noWrap="true" align="left">
									<xsl:if test="$bb = '0'">
										<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position() &gt; 4">
								<xsl:if test = "((position()-4) mod 3) = 1 and $dispLast=1">
									<td noWrap="true" align="right">
										<xsl:if test="$bb = '0'">
											<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:if>
								<xsl:if test="((position()-4) mod 3) = 2">
									<td noWrap="true" align="right">
										<xsl:if test="$bb = '0'">
											<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:if>
								<xsl:if test="((position()-4) mod 3) = 0 and $dispDiff=1">
									<td noWrap="true" align="right">
										<xsl:if test="$bb = '0'">
											<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:if>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
