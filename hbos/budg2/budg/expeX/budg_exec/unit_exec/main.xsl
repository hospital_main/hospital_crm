<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th rowspan="2" valign="middle">选择</th>
				<th nowrap="true" rowspan="2" dataIndex="0" valign="middle">科目编码</th>
				<th nowrap="true" rowspan="2" dataIndex="0" valign="middle">科目名称</th>
				<th nowrap="true" colspan="3" dataIndex="0">年度预算</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">1月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">2月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">3月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">4月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">5月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">6月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">7月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">8月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">9月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">10月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">11月</th>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" colspan="3" dataIndex="0">12月</th>
				<th style="display:none"/>
				<th style="display:none"/>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th style="display:none"/>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
				<th nowrap="true" dataIndex="0">预算总额</th>
				<th nowrap="true" dataIndex="0">实际执行</th>
				<th nowrap="true" dataIndex="0">执行进度%</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <td align='center'  noWrap='true'>
            <button class="graphButton">
            	 <xsl:attribute name="index"><xsl:value-of select='$CurTrPos'/></xsl:attribute>
            	 <xsl:attribute name="onclick">openGraph(this)</xsl:attribute>
            	 ___
    			  </button>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true'>
			            	<xsl:if test="../td[47]='0'">
	                  	<xsl:value-of select="."/>
			            	</xsl:if>
			            	<xsl:if test="../td[47]!='0'">
			            	</xsl:if>
			            </td>
                </xsl:when>
                <xsl:when test="position()=2">
			            <td  noWrap='true'>
			            	<xsl:if test="../td[47]='2'">
	                  	<xsl:value-of select="."/>
			            	</xsl:if>
			            	<xsl:if test="../td[47]='0'">
											<a href="#">
												<xsl:attribute name="onclick">
											    view('subjDeptDetail_byManage.html?load=&lt;comp_code&gt;<xsl:value-of select="../td[43]"/>&lt;/comp_code&gt;&lt;copy_code&gt;<xsl:value-of select="../td[44]"/>&lt;/copy_code&gt;&lt;budg_year&gt;<xsl:value-of select="../td[45]"/>&lt;/budg_year&gt;&lt;budg_totalMoney_type&gt;<xsl:value-of select="../td[46]"/>&lt;/budg_totalMoney_type&gt;&lt;isType&gt;<xsl:value-of select="../td[47]"/>&lt;/isType&gt;&lt;st_name&gt;<xsl:value-of select="../td[2]"/>&lt;/st_name&gt;&lt;st_code&gt;<xsl:value-of select="../td[1]"/>&lt;/st_code&gt;')
											  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
			            	</xsl:if>
			            	<xsl:if test="../td[47]='1'">
											<a href="#">
												<xsl:attribute name="onclick">
											    view('subjDeptDetail_byManage.html?load=&lt;comp_code&gt;<xsl:value-of select="../td[43]"/>&lt;/comp_code&gt;&lt;copy_code&gt;<xsl:value-of select="../td[44]"/>&lt;/copy_code&gt;&lt;budg_year&gt;<xsl:value-of select="../td[45]"/>&lt;/budg_year&gt;&lt;budg_totalMoney_type&gt;<xsl:value-of select="../td[46]"/>&lt;/budg_totalMoney_type&gt;&lt;isType&gt;<xsl:value-of select="../td[47]"/>&lt;/isType&gt;&lt;st_name&gt;<xsl:value-of select="../td[2]"/>&lt;/st_name&gt;&lt;st_code&gt;<xsl:value-of select="../td[42]"/>&lt;/st_code&gt;')
											  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
			            	</xsl:if>
			            </td>
                </xsl:when>
                <xsl:when test="position()=42 or position()=43 or position()=44 or position()=45 or position()=46 or position()=47">
			            <td  noWrap='true' style="display:none">
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' align="right" >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
