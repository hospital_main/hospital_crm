<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style=''>日期</th>
				<th style=''>凭证号</th>
				<th style=''>摘要</th>
				<th style=''>科目编码</th>
				<th nowrap='true'>科目名称</th>
				<th nowrap='true'>借方金额</th>
				<th nowrap='true'>贷方金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=6 or position()=7">
								<td class="numberText">
									<xsl:if test=". != 0 ">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>