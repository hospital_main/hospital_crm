<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th rowspan="2" valign="center">部门编码</th>
				<th rowspan="2" valign="center">部门名称</th>
				<!--th colspan="3">收入</th-->
				<th colspan="3">支出</th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<!--th>预算数</th>
				<th>执行数</th>
				<th>执行进度%</th-->
				<th>预算数</th>
				<th>执行数</th>
				<th>执行进度%</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() = 1 or position() = 2 ">
								<td noWrap="true">
										<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap="true" align="right">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
