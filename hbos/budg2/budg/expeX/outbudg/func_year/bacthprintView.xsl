<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
	  	<tr>
	   		<td style="fontsize:maintitle;colspan:colcount;"></td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr>
					<td rowspan="2">科目编码</td>
					<td rowspan="2">科目名称</td>
					<td colspan="4">本年预算</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td rowspan="2">指标属性</td>
			</tr>
			<tr>
				<td style="display:none"></td>
				<td style="display:none"/>
				<td>全院</td>
				<td>科室汇总</td>
				<td>差额</td>
				<td>差异率</td>
				<td style="display:none"/>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test="  position() = 1 or position() = 2 or position() = 3 or position() = 4 or position() = 5 or position() = 6 or position() = 7">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	              <xsl:otherwise>
	              </xsl:otherwise>
	          </xsl:choose>
  		</xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>