<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
	  	<tr>
	   		<td style="fontsize:maintitle;colspan:colcount;"></td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr>
					<td rowspan="2">����</td>
					<td rowspan="2">Ԥ�����</td>
					<td colspan="4">����Ԥ��</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
			</tr>
			<tr>
			
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td>��λ�´�(E)</td>
				<td>����Ԥ��(E)</td>
				<td>���</td>
				<td>������(%)</td>
				
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test="  position() = 1 ">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	              <!--xsl:when test=" position() =4 ">
	                <td><xsl:value-of select="substring-after(.,'|||')"/></td>
	              </xsl:when-->
	              <xsl:otherwise>
	              	<td noWrap="true" align="right">
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</td>
	              </xsl:otherwise>
	          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>