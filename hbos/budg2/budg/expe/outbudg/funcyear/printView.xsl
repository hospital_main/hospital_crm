<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
  	<root>
  	<thead>
		<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
	  <tr noWrap="true" class="mainHead">
	   	<td rowspan="2">科目编码</td>
			<td rowspan="2">科目名称</td>
			<td rowspan="2">预算基础</td>
			<td rowspan="2">本年预算</td>
			<td colspan="4">审批结果</td>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
		</tr>+
		<tr>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td>职能审批</td>
			<td>职能建议</td>
			<td>单位审批</td>
			<td>单位建议</td>	
		</tr>
  	</thead>
  	<tbody>
	<xsl:for-each select="/root/tbody/tr">
		<tr noWrap="true">
			<xsl:for-each select="td">
			<xsl:choose>
				<xsl:when test="position()=9 or position()=10 or position()=11 or position()=12 or position()=13">

				</xsl:when>
				<xsl:when test="position()=3 and position()=4">
				<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				</xsl:when>
				<xsl:otherwise>
				<td><xsl:value-of select="."/></td>
				</xsl:otherwise>
			</xsl:choose>
         		</xsl:for-each>
  		</tr>  		
   	</xsl:for-each>
 	</tbody>
 	</root>
  </xsl:template>
</xsl:stylesheet>