<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>科目编码</th>
				<th>科目名称</th>     
				<th>上年执行</th>
				<th>本年度预算</th>
				<th>差额</th>
				<th>差额率(%)</th>
				<th>参考方法</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6">
                   <td align='right'>
                      <xsl:value-of select="format-number(.,'#,##0.00')"/>
                   </td>
                </xsl:when>

                <xsl:when test="position()=8 or position()=9 or position()=10">
			            <td  noWrap='true' style="display:none">
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
