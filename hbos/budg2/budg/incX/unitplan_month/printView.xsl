<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
 		<tr noWrap="true" class="mainHead">
			<td style="fontsize:coltitle;" width="180">指标编码</td>
			<td style="fontsize:coltitle;" width="180">指标名称</td>
			<td style="fontsize:coltitle;" width="180">计量单位</td>
			<td style="fontsize:coltitle;" width="180">月计划汇总</td>
			<td style="fontsize:coltitle;" width="180">1月</td>
			<td style="fontsize:coltitle;" width="180">2月</td>
			<td style="fontsize:coltitle;" width="180">3月</td>
			<td style="fontsize:coltitle;" width="180">4月</td>
			<td style="fontsize:coltitle;" width="180">5月</td>
			<td style="fontsize:coltitle;" width="180">6月</td>
			<td style="fontsize:coltitle;" width="180">7月</td>
			<td style="fontsize:coltitle;" width="180">8月</td>
			<td style="fontsize:coltitle;" width="180">9月</td>
			<td style="fontsize:coltitle;" width="180">10月</td>
			<td style="fontsize:coltitle;" width="180">11月</td>
			<td style="fontsize:coltitle;" width="180">12月</td>
 		</tr>  	
  	</thead>  	
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test=" position() = 1 or position() = 2 or position() = 3 ">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
                      <xsl:when test=" position() = 4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17 or position()=18 ">
                        <td align='right'>
                          <xsl:value-of select="format-number(.,'#,##0.00')"/>
                        </td>
                      </xsl:when>
	              <xsl:otherwise>
	              </xsl:otherwise>
	          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>