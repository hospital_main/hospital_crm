<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">5</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr noWrap="true" class="mainHead">
					<td>科室名称</td>
					<!--th>上年收入</th-->
					<td>单位预算</td>
					<td>科室预算</td>
					<td>差额</td>
					<td>差异率</td>
				</tr>
			</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
					  <xsl:choose>
					    <xsl:when test="position() = 1" >
	    				  <td><xsl:value-of select="."/></td>
  					  </xsl:when>
  					  <xsl:otherwise>
	    				  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
  					  </xsl:otherwise>
  					</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
