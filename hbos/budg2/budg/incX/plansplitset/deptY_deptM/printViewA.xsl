<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>

			
		</tr>
 		<tr noWrap="true" class="mainHead">
			<td style="fontsize:coltitle;" width="180">���ұ���</td>
			<td style="fontsize:coltitle;" width="180">��������</td>

 		</tr>  	
  	</thead>  	
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>


	              <xsl:when test=" position() = 1 or position() = 2 or position() = 3 ">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
                      
	              <xsl:otherwise>
	              </xsl:otherwise>
	          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>