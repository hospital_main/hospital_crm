<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="/root/tbody/tr[position()=1]">
						<xsl:for-each select="td">	
							<xsl:choose>
								<xsl:when test="position()=1">
									<td align="center" colspan="colcount">
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td style="display:none"></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</xsl:for-each>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="/root/tbody/tr[position()=1]">
						<xsl:for-each select="td">	
							<xsl:choose>
								<xsl:when test="position()=1">
									<td><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</xsl:for-each>
				</tr>
			</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	        <tr>
	          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test="position()=1">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	              <xsl:otherwise>
	                <td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
