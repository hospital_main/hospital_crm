<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>调整序号</th>
				<th>调整日期</th>
				<th>调整文号</th>
				<th>预算下达</th>
				<th>调整说明</th>
				<th>预算信息</th>
			</tr>
		</thead>  	
   
   
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
							<xsl:if test="'0'=td[7]">
								<xsl:attribute name="disabled">true</xsl:attribute>
							</xsl:if>
						</input>
					</td>
					<xsl:for-each select="td[position()!=12 and position()!=7]">
						<td >
							<xsl:choose>
								<xsl:when test="position()=1 or position()=6">
										<a href="#">
										<xsl:attribute name="onclick">
											openUpdateDlg(<xsl:value-of select="position()"/>,"<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute>
										<xsl:value-of select="."/></a>
								</xsl:when>
								<xsl:otherwise>
										<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



