<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style="display:none"><input type='checkboxs'/></th>
      	<th><input type="checkbox" id="allcheckbox" onclick="setCheckAll(this)"/></th>
      	<th nowrap='true' valign='middle'>指标编码</th>
      	<th nowrap='true' valign='middle'>指标名称</th>
      	<th nowrap='true' valign='middle'>计量单位</th>
      	<th nowrap='true' valign='middle'>年度计划</th>
      	<th nowrap='true' valign='middle'>1月</th>
      	<th nowrap='true' valign='middle'>2月</th>
      	<th nowrap='true' valign='middle'>3月</th>
      	<th nowrap='true' valign='middle'>4月</th>
      	<th nowrap='true' valign='middle'>5月</th>
      	<th nowrap='true' valign='middle'>6月</th>
      	<th nowrap='true' valign='middle'>7月</th>
      	<th nowrap='true' valign='middle'>8月</th>
      	<th nowrap='true' valign='middle'>9月</th>
      	<th nowrap='true' valign='middle'>10月</th>
      	<th nowrap='true' valign='middle'>11月</th>
      	<th nowrap='true' valign='middle'>12月</th>

      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
                    <xsl:choose>

                    <xsl:when test="position()=1">
			                <td align="center"><input type="checkbox"/></td>
                    </xsl:when>


                      <xsl:when test="position()=2 or position()=3 or position()=4">
                        <td align='left'>
                          <xsl:value-of select="."/>
                        </td>
                      </xsl:when>
                      <xsl:when test="position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17">
                        <td align='right'>
                          <xsl:value-of select="format-number(.,'#,##0.00')"/>
                        </td>
                      </xsl:when>
                      <xsl:otherwise>
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                    </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

