<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th><input type="checkbox" id="allcheckbox" onclick="setCheckAll(this)"/></th>
				<th>科目编码</th>
				<th>科目名称</th>
				<th align="right">年度预算</th>
				<th align="right">月预算汇总</th>
				<th align="right">差额</th>
				<th align="right">1月</th>
				<th align="right">2月</th>
				<th align="right">3月</th>
				<th align="right">4月</th>
				<th align="right">5月</th>
				<th align="right">6月</th>
				<th align="right">7月</th>
				<th align="right">8月</th>
				<th align="right">9月</th>
				<th align="right">10月</th>
				<th align="right">11月</th>
				<th align="right">12月</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <td align='center' noWrap='true'>
						<input type="checkbox"/>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true'>
			            	<xsl:if test="../td[19]!='1'">
			            	<xsl:value-of select="."/>
			            	</xsl:if>
			            	<xsl:if test="../td[19]='1'">
			            	</xsl:if>
			            </td>
                </xsl:when>
                <xsl:when test="position()=2">
			            <td  noWrap='true'>
	                  	<xsl:value-of select="."/>
			            </td>
                </xsl:when>
                <xsl:when test="position()=18 or position()=19 ">
			            <td  noWrap='true' style="display:none">
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' align="right">
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
