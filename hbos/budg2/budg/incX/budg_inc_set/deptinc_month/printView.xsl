<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
		<tr>
   			<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
	  	<tr noWrap="true" class="mainHead">
	   	<td>科目编码</td>
			<td>科目名称</td>
			<td>年度预算</td>
			<td>月预算汇总</td>
			<td>差额</td>
			<td>1月</td>
			<td>2月</td>
			<td>3月</td>
			<td>4月</td>
			<td>5月</td>
			<td>6月</td>
			<td>7月</td>
			<td>8月</td>
			<td>9月</td>
			<td>10月</td>
			<td>11月</td>
			<td>12月</td>
		</tr>
  	</thead>
  	<tbody>
	<xsl:for-each select="/root/tbody/tr">
		<tr noWrap="true">
			<xsl:for-each select="td">
			<xsl:choose>
				<xsl:when test="position()=18 or position()=19 or position()=20">

				</xsl:when>
				<xsl:when test="position()!=1 and position()!=2">
				<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				</xsl:when>
				<xsl:otherwise>
				<td><xsl:value-of select="."/></td>
				</xsl:otherwise>
			</xsl:choose>
         		</xsl:for-each>
  		</tr>
   	</xsl:for-each>
 	</tbody>
 	</root>
  </xsl:template>
</xsl:stylesheet>