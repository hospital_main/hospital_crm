<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th rowspan="2">选择</th>
				<th nowrap="true" rowspan="2" dataIndex="0">科室编码</th>
				<th nowrap="true" rowspan="2" dataIndex="0">科室名称</th>
				<th nowrap="true" colspan="2" dataIndex="0">年度预算</th>
				<th style="display:none"/>
				<th nowrap="true" colspan="2" dataIndex="0">1月</th>
				<th style="display:none"/>
				<th nowrap="true" colspan="2" dataIndex="0">2月</th>
				<th style="display:none"/>
				<th nowrap="true" colspan="2" dataIndex="0">3月</th>
				<th style="display:none"/>
				<th nowrap="true" colspan="2" dataIndex="0">4月</th>
				<th style="display:none"/>
				<th nowrap="true" colspan="2" dataIndex="0">5月</th>
				<th style="display:none"/>
				<th nowrap="true" colspan="2" dataIndex="0">6月</th>
				<th style="display:none"/>
				<th nowrap="true" colspan="2" dataIndex="0">7月</th>
				<th style="display:none"/>
				<th nowrap="true" colspan="2" dataIndex="0">8月</th>
				<th style="display:none"/>
				<th nowrap="true" colspan="2" dataIndex="0">9月</th>
				<th style="display:none"/>
				<th nowrap="true" colspan="2" dataIndex="0">10月</th>
				<th style="display:none"/>
				<th nowrap="true" colspan="2" dataIndex="0">11月</th>
				<th style="display:none"/>
				<th nowrap="true" colspan="2" dataIndex="0">12月</th>
				<th style="display:none"/>
  		</tr>
			<tr noWrap="true" class="mainHead">
				<th style="display:none"/>
				<th style="display:none"/>
				<th style="display:none"/>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("年度预算",4)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("1月",6)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("2月",8)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("3月",10)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("4月",12)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("5月",14)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("6月",16)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("7月",18)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("8月",20)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("9月",22)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("10月",24)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("11月",26)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
				<th nowrap="true" dataIndex="0">
					<a href="#">
						<xsl:attribute name="onclick">
					    viewGraph("12月",28)
					  </xsl:attribute>
						预算额
					</a>
				</th>
				<th nowrap="true" dataIndex="0">比重(%)</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <td align='center'  noWrap='true'>
            <button class="graphButton">
            	 <xsl:attribute name="index"><xsl:value-of select='$CurTrPos'/></xsl:attribute>
            	 <xsl:attribute name="onclick">openGraph(this)</xsl:attribute>
    			  </button>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true'>
			            	<xsl:if test="../td[1]!='0'">
	                  	<xsl:value-of select="."/>
			            	</xsl:if>
			            </td>
                </xsl:when>
                <xsl:when test="position()=2">
			            <td  noWrap='true'>
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' align="right">
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
