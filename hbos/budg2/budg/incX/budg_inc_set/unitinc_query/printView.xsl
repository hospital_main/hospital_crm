<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>			
		</tr>
 		<tr noWrap="true" class="mainHead">
			<td style="fontsize:coltitle;" width="180">科目编码</td>
			<td style="fontsize:coltitle;" width="180">科目名称</td>
			<td style="fontsize:coltitle;" width="180">年度预算</td>
			<td style="fontsize:coltitle;" width="180">1月</td>
			<td style="fontsize:coltitle;" width="180">2月</td>
			<td style="fontsize:coltitle;" width="180">3月</td>
			<td style="fontsize:coltitle;" width="180">4月</td>
			<td style="fontsize:coltitle;" width="180">5月</td>
			<td style="fontsize:coltitle;" width="180">6月</td>
			<td style="fontsize:coltitle;" width="180">7月</td>
			<td style="fontsize:coltitle;" width="180">8月</td>
			<td style="fontsize:coltitle;" width="180">9月</td>
			<td style="fontsize:coltitle;" width="180">10月</td>
			<td style="fontsize:coltitle;" width="180">11月</td>
			<td style="fontsize:coltitle;" width="180">12月</td>
 		</tr>  	
  	</thead>  	
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true'>
			            	<xsl:value-of select="."/>
			            </td>
                </xsl:when>
                <xsl:when test="position()=2">
			            <td  noWrap='true'>
			            	<xsl:value-of select="."/>
			            </td>
                </xsl:when>
                <xsl:when test="position()=16 or position()=17 or position()=18 or position()=19 or position()=20 or position()=21">
			            <td  noWrap='true' style="display:none">
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' align="right">
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>