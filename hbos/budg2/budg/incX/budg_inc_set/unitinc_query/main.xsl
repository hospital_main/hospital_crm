<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th><input type="checkbox" id="allcheckbox" onclick="setCheckAll(this)"/></th>
				<th>科目编码</th>
				<th>科目名称</th>
				<th>年度预算</th>
				<th>1月</th>
				<th>2月</th>
				<th>3月</th>
				<th>4月</th>
				<th>5月</th>
				<th>6月</th>
				<th>7月</th>
				<th>8月</th>
				<th>9月</th>
				<th>10月</th>
				<th>11月</th>
				<th>12月</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <td align='center' noWrap='true'>
						<input type="checkbox"/>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true'>
			            	<xsl:if test="../td[21]='0'">
	                  	<xsl:value-of select="."/>
			            	</xsl:if>
			            	<xsl:if test="../td[21]!='0'">
			            	</xsl:if>
			            </td>
                </xsl:when>
                <xsl:when test="position()=2">
			            <td  noWrap='true'>
			            	<xsl:if test="../td[21]='2'">
	                  	<xsl:value-of select="."/>
			            	</xsl:if>
			            	<xsl:if test="../td[21]='0'">
											<a href="#">
												<xsl:attribute name="onclick">
											    view('subjDeptDetail.html?load=&lt;comp_code&gt;<xsl:value-of select="../td[17]"/>&lt;/comp_code&gt;&lt;copy_code&gt;<xsl:value-of select="../td[18]"/>&lt;/copy_code&gt;&lt;budg_year&gt;<xsl:value-of select="../td[19]"/>&lt;/budg_year&gt;&lt;budg_totalMoney_type&gt;<xsl:value-of select="../td[20]"/>&lt;/budg_totalMoney_type&gt;&lt;isType&gt;<xsl:value-of select="../td[21]"/>&lt;/isType&gt;&lt;st_name&gt;<xsl:value-of select="../td[2]"/>&lt;/st_name&gt;&lt;st_code&gt;<xsl:value-of select="../td[1]"/>&lt;/st_code&gt;')
											  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
			            	</xsl:if>
			            	<xsl:if test="../td[21]='1'">
											<a href="#">
												<xsl:attribute name="onclick">
											    view('subjDeptDetail.html?load=&lt;comp_code&gt;<xsl:value-of select="../td[17]"/>&lt;/comp_code&gt;&lt;copy_code&gt;<xsl:value-of select="../td[18]"/>&lt;/copy_code&gt;&lt;budg_year&gt;<xsl:value-of select="../td[19]"/>&lt;/budg_year&gt;&lt;budg_totalMoney_type&gt;<xsl:value-of select="../td[20]"/>&lt;/budg_totalMoney_type&gt;&lt;isType&gt;<xsl:value-of select="../td[21]"/>&lt;/isType&gt;&lt;st_name&gt;<xsl:value-of select="../td[2]"/>&lt;/st_name&gt;&lt;st_code&gt;<xsl:value-of select="../td[16]"/>&lt;/st_code&gt;')
											  </xsl:attribute>
												<xsl:value-of select="."/>
											</a>
			            	</xsl:if>
			            </td>
                </xsl:when>
                <xsl:when test="position()=16 or position()=17 or position()=18 or position()=19 or position()=20 or position()=21">
			            <td  noWrap='true' style="display:none">
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' align="right">
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
