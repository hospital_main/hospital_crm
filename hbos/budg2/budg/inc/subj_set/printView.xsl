<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;" width="180">科目编码</td>
					<td style="fontsize:coltitle;" width="280">科目名称</td>
					<td style="fontsize:coltitle;" width="330">分解方法设置</td>
					<td style="fontsize:coltitle;" width="330">是否独立核算</td>
					<td style="fontsize:coltitle;" width="330">调节比例\比例系数</td>
  		</tr>  	
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test="position() = 1 or position() = 2 or position() = 3">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	              <xsl:when test="position()=3 or position()=4">
	                <td><xsl:value-of select="substring-after(.,'|||')"/></td>
	              </xsl:when>
	              <xsl:when test="position()=5">
	                <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	              </xsl:when>
	              <xsl:otherwise>
	              </xsl:otherwise>
	          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>