<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>指标编码</th>
				<th>指标名称</th>
				<th>计量单位</th>
				<th>年初预算</th>
				<th>调整预算</th>
				<th>1月</th>
				<th>2月</th>
				<th>3月</th>
				<th>4月</th>
				<th>5月</th>
				<th>6月</th>
				<th>7月</th>
				<th>8月</th>
				<th>9月</th>
				<th>10月</th>
				<th>11月</th>
				<th>12月</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' align="left">
		              <a tabindex="-2" href="#">
			                <xsl:attribute name="onclick" >
			                  javascript:openDialog('indexMonthDetail.html?load=&lt;idx_code&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/idx_code&gt;&lt;idx_name&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/idx_name&gt;&lt;adjust_xh &gt;'+adjust_xh.value+'&lt;/adjust_xh&gt;', 'dialogWidth:1024px;dialogHeight:768px', result)
			                </xsl:attribute>
	                  	<xsl:value-of select="."/>
	                  </a>
			            </td>
                </xsl:when>
                <xsl:when test="position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17">
			            <td  noWrap='true' align="right">
	                  	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
