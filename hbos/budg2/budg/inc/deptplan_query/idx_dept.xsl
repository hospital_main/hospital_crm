<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
        <th rowspan="2" valign="middle" ><input type="checkbox" id="allcheckbox"/></th>
        <th rowspan="2" valign="middle" >科室编码</th>
        <th rowspan="2" valign="middle" >科室名称</th>
		    <th colspan="2">年度计划</th>
		    <th colspan="2">1月</th>
		    <th colspan="2">2月</th>
		    <th colspan="2">3月</th>
		    <th colspan="2">4月</th>
		    <th colspan="2">5月</th>
		    <th colspan="2">6月</th>
		    <th colspan="2">7月</th>
		    <th colspan="2">8月</th>
		    <th colspan="2">9月</th>
		    <th colspan="2">10月</th>
		    <th colspan="2">11月</th>
		    <th colspan="2">12月</th>		    		    
  		</tr>
  		<tr noWrap="true" class="mainHead"> 
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>		    		    
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>
		    <th  valign="middle">计划量</th>
		    <th  valign="middle">比重</th>			    		    
		  </tr>  		
  	</thead>
  	    
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
                    <xsl:choose>
                    <xsl:when test="position()=1">
			                <td align="center"><input type="checkbox"/></td>
                    </xsl:when>
                <!-- 
                 <xsl:when test="position()=2">
			            <td  noWrap='true' >
		              <a tabindex="-2">
		                <xsl:attribute name="href" >
		                  javascript:view('idx_dept.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;')
		                </xsl:attribute><xsl:value-of select="."/></a>
			            </td>
                </xsl:when>-->
                      
                 <!--     <xsl:when test="position()=3 ">
                        <td align='center'>
                          <xsl:value-of select="."/>
                        </td>
                      </xsl:when>-->
                      <xsl:when test="position()=29 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17">
                        <td align='right'>
                          <xsl:value-of select="format-number(.,'#,##0.00')"/>
                        </td>
                      </xsl:when>
                      <xsl:when test="position()=18 or position()=19 or position()=20 or position()=21 or position()=22 or position()=23 or position()=24 or position()=25 or position()=26 or position()=27 or position()=28">
                        <td align='right'>
                          <xsl:value-of select="format-number(.,'#,##0.00')"/>
                        </td>
                      </xsl:when> 
                                           
                      <xsl:otherwise>
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                    </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

