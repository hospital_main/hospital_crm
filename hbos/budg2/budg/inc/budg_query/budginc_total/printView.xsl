<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<thead>
				<xsl:variable name="dispLast" select="/root/tbody/tr[1]/td[2]"/>
				<xsl:variable name="dispDiff" select="/root/tbody/tr[1]/td[3]"/>
				<xsl:if test="$dispLast &lt; 1">
					<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">8</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
					</tr>
				</xsl:if>
				<xsl:if test="$dispLast &gt; 0 and $dispDiff &lt; 1">
					<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">15</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
					</tr>
				</xsl:if>
				<xsl:if test="$dispLast &gt; 0 and $dispDiff &gt; 0">
					<tr>
						<td style="fontsize:maintitle;">
							<xsl:attribute name="colspan">22</xsl:attribute>
						</td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
					</tr>
				</xsl:if>
				<xsl:if test="$dispLast &lt; 1">
					<tr noWrap="true" class="mainHead">
						<td rowspan="2">科室名称</td>
						<td colspan="3">门诊预算收入</td>
						<td style="display:none"></td>
						<td style="display:none"></td>
						
						<td colspan="3">住院预算收入</td>
						<td style="display:none"></td>
						<td style="display:none"></td>
						<td rowspan="2">预算合计</td>
					</tr>
					<tr noWrap="true" class="mainHead">
					<td style="display:none"></td>
						<td>门诊人次</td>
						<td>诊次费用</td>
						<td>预算金额</td>
						<td>住院床日</td>
						<td>床日费用</td>
						<td>预算金额</td>
						<td style="display:none"></td>
					</tr>
				</xsl:if>
				<xsl:if test="($dispLast &gt; 0)">
					<tr noWrap="true" class="mainHead">
						<td rowspan="3">科室名称</td>
						<xsl:if test="$dispDiff &lt; 1">
							<td colspan="6">门诊预算收入</td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td colspan="6">住院预算收入</td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
						</xsl:if>
						<xsl:if test="$dispDiff &gt; 0">
							<td colspan="9">门诊预算收入</td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td colspan="9">住院预算收入</td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td style="display:none"></td>
						</xsl:if>
						<td colspan="3" rowspan="2">合计</td>
						<td style="display:none"></td>
						<td style="display:none"></td>
					</tr>
					<xsl:if test="$dispDiff &lt; 1">
						<tr noWrap="true" class="mainHead">
						<td style="display:none"></td>
							<td colspan="2">门诊人次</td>
							<td style="display:none"></td>
							<td colspan="2">诊次费用</td>
							<td style="display:none"></td>
							<td colspan="2">金额</td>
							<td style="display:none"></td>
							<td colspan="2">住院床日</td>
							<td style="display:none"></td>
							<td colspan="2">床日费用</td>
							<td style="display:none"></td>
							<td colspan="2">金额</td>
							<td style="display:none"></td>
						</tr>
					</xsl:if>
					<xsl:if test="$dispDiff &gt; 0">
						<tr noWrap="true" class="mainHead">
						<td style="display:none"></td>
							<td colspan="3">门诊人次</td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td colspan="3">诊次费用</td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td colspan="3">金额</td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td colspan="3">住院床日</td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td colspan="3">床日费用</td>
							<td style="display:none"></td>
							<td style="display:none"></td>
							<td colspan="3">金额</td>
							<td style="display:none"></td>
							<td style="display:none"></td>
						</tr>
					</xsl:if>
					<tr noWrap="true" class="mainHead">
						<td style="display:none"></td>
						<td noWrap="true">上年</td>
						<td noWrap="true">今年</td>
						<xsl:if test="$dispDiff &gt; 0">
							<td noWrap="true">差异率(%)</td>
						</xsl:if>
						<td noWrap="true">上年</td>
						<td noWrap="true">今年</td>
						<xsl:if test="$dispDiff &gt; 0">
							<td noWrap="true">差异率(%)</td>
						</xsl:if>
						<td noWrap="true">上年</td>
						<td noWrap="true">今年</td>
						<xsl:if test="$dispDiff &gt; 0">
							<td noWrap="true">差异率(%)</td>
						</xsl:if>
						<td noWrap="true">上年</td>
						<td noWrap="true">今年</td>
						<xsl:if test="$dispDiff &gt; 0">
							<td noWrap="true">差异率(%)</td>
						</xsl:if>
						<td noWrap="true">上年</td>
						<td noWrap="true">今年</td>
						<xsl:if test="$dispDiff &gt; 0">
							<td noWrap="true">差异率(%)</td>
						</xsl:if>
						<td noWrap="true">上年</td>
						<td noWrap="true">今年</td>
						<xsl:if test="$dispDiff &gt; 0">
							<td noWrap="true">差异率(%)</td>
						</xsl:if>
						<td noWrap="true">上年</td>
						<td noWrap="true">今年</td>
						<xsl:if test="$dispDiff &gt; 0">
							<td noWrap="true">差异率(%)</td>
						</xsl:if>
					</tr>
				</xsl:if>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:variable name="bb" select="td[1]"/>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=4">
								<td noWrap="true" align="left">
									<xsl:if test="$bb = '0'">
										<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position() &gt; 4">
								<xsl:if test = "((position()-4) mod 3) = 1 and $dispLast=1">
									<td noWrap="true" align="right">
										<xsl:if test="$bb = '0'">
											<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:if>
								<xsl:if test="((position()-4) mod 3) = 2">
									<td noWrap="true" align="right">
										<xsl:if test="$bb = '0'">
											<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:if>
								<xsl:if test="((position()-4) mod 3) = 0 and $dispDiff=1">
									<td noWrap="true" align="right">
										<xsl:if test="$bb = '0'">
											<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:if>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
