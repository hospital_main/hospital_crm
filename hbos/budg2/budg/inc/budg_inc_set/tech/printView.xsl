<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  		<xsl:variable name='cCnt' select='count(/root/tbody/tr[1]/td)' />
  	<thead>
	  	<tr>
	   		<td style="fontsize:maintitle;colspan:colcount;"><xsl:attribute name="colspan"><xsl:value-of select="$cCnt"/></xsl:attribute></td>
	   		<!--xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
					<td style='display:none'/>
				</xsl:for-each-->
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
  		<tr noWrap='true' class='mainHead'>
  			<td nowrap='true'>科室编码</td>
  			<td nowrap='true'>科室名称</td>
  			<td nowrap='true'>预算收入</td> 
  			<!--xsl:if test='$cCnt &gt; 3 '> 			
	  			<td nowrap='true'>服务科室编码</td>
	  			<td nowrap='true'>服务科室名称</td>
	  			<td nowrap='true'>预算服务收入</td>
	  			<td nowrap='true'>所占比例</td>
  			</xsl:if-->
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
       	<xsl:variable name="colCnt" select='position()'/>
        <tr>
        	<xsl:variable name="dept_name" select='td[1]'/>
        	<xsl:variable name="rowCnt" select='count(/root/tbody/tr[td[1] = $dept_name])'/>
					
					<xsl:if test="$colCnt = 1">
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[1]"/>
	    			</td>
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[2]"/>
	    			</td>
	    			<td noWrap="true" align="right">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="format-number(td[3],'#,##0.00')"/>
	    			</td>
					</xsl:if>
					<xsl:if test="$colCnt &gt; 1">
			    		<xsl:if test="td[1] != ../tr[$colCnt - 1 ]/td[1]">
			    			<td noWrap="true">
			    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			    				<xsl:value-of select="td[1]"/>
			    			</td>
			    			<td noWrap="true">
			    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			    				<xsl:value-of select="td[2]"/>
			    			</td>
			    			<td noWrap="true" align="right">
			    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			    				<xsl:value-of select="format-number(td[3],'#,##0.00')"/>
			    			</td>
			    		</xsl:if>
			     </xsl:if>
        	
          <!--xsl:for-each select="td[position() &gt; 3]">
          	<xsl:if test="position() = 1 or position() = 2">
	         		<td>
	         			<xsl:value-of select="."/>
	         		</td>
         		</xsl:if>
          	<xsl:if test="position() &gt; 2">
	         		<td align="right">
	         			<xsl:value-of select="format-number(.,'#,##0.00')"/>
	         		</td>
         		</xsl:if>
          </xsl:for-each-->
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>