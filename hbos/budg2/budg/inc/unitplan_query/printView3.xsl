<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>		
   	</tr> 
 		<tr noWrap="true" class="mainHead">
			<td style="fontsize:coltitle;" rowspan="2" width="180">科室编码</td>
			<td style="fontsize:coltitle;" rowspan="2" width="180">科室名称</td>
			
			<td style="fontsize:coltitle;" colspan="2" width="180">年度计划</td>		
 		</tr>  
 		<tr noWrap="true" class="mainHead">
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
 		</tr>  
  	</thead>  	
  	
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>            	            
	              <xsl:when test="position()=1 or position()=2">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	              
                <xsl:when test="position()=3 or position()=4">
                   <td align='right'>
                       <xsl:value-of select="format-number(.,'#,##0.00')"/>
                   </td>
                </xsl:when>                      
	              <xsl:otherwise>
	              </xsl:otherwise>
	          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>