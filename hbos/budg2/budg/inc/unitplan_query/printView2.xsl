<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>			
			<td style='display:none'/>
   	</tr> 
 		<tr noWrap="true" class="mainHead">
			<td style="fontsize:coltitle;" rowspan="2" width="180">科室编码</td>
			<td style="fontsize:coltitle;" rowspan="2" width="180">科室名称</td>
			
			<td style="fontsize:coltitle;" colspan="2" width="180">年度计划</td>
			<td style="fontsize:coltitle;display:none"></td>
			<td style="fontsize:coltitle;" colspan="2" width="180">1月</td>
			<td style="fontsize:coltitle;display:none"></td>			
			<td style="fontsize:coltitle;" colspan="2" width="180">2月</td>
			<td style="fontsize:coltitle;display:none"></td>			
			<td style="fontsize:coltitle;" colspan="2" width="180">3月</td>
			<td style="fontsize:coltitle;display:none"></td>			
			<td style="fontsize:coltitle;" colspan="2" width="180">4月</td>
			<td style="fontsize:coltitle;display:none"></td>			
			<td style="fontsize:coltitle;" colspan="2" width="180">5月</td>
			<td style="fontsize:coltitle;display:none"></td>			
			<td style="fontsize:coltitle;" colspan="2" width="180">6月</td>
			<td style="fontsize:coltitle;display:none"></td>			
			<td style="fontsize:coltitle;" colspan="2" width="180">7月</td>
			<td style="fontsize:coltitle;display:none"></td>			
			<td style="fontsize:coltitle;" colspan="2" width="180">8月</td>
			<td style="fontsize:coltitle;display:none"></td>			
			<td style="fontsize:coltitle;" colspan="2" width="180">9月</td>
			<td style="fontsize:coltitle;display:none"></td>			
			<td style="fontsize:coltitle;" colspan="2" width="180">10月</td>
			<td style="fontsize:coltitle;display:none"></td>			
			<td style="fontsize:coltitle;" colspan="2" width="180">11月</td>
			<td style="fontsize:coltitle;display:none"></td>			
			<td style="fontsize:coltitle;" colspan="2" width="180">12月</td>
			<td style="fontsize:coltitle;display:none"></td>			
 		</tr>  
 		<tr noWrap="true" class="mainHead">
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>
			<td style="fontsize:coltitle;" width="180">计划量</td>
			<td style="fontsize:coltitle;" width="180">比重</td>			
 		</tr>  
  	</thead>  	
  	
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>            	            
	              <xsl:when test="position()=1 or position()=2">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	              
                <xsl:when test="position()=29 or position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17">
                   <td align='right'>
                       <xsl:value-of select="format-number(.,'#,##0.00')"/>
                   </td>
                </xsl:when>
                <xsl:when test="position()=18 or position()=19 or position()=20 or position()=21 or position()=22 or position()=23 or position()=24 or position()=25 or position()=26 or position()=27 or position()=28">
                   <td align='right'>
                      <xsl:value-of select="format-number(.,'#,##0.00')"/>
                   </td>
                </xsl:when>
                      
	              <xsl:otherwise>
	              </xsl:otherwise>
	          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>