<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
      <colgroup>		       
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:280'/>
      </colgroup>
    	<thead>
    		<tr noWrap='true' class='mainHead'>
    			<th nowrap='true'>会计科目</th>
    			<th nowrap='true'>科室名称</th>
    			<th nowrap='true'>数据来源</th>
    			<th nowrap='true'>数据标记</th>
    			<th nowrap='true'>账套</th>
    			<th nowrap='true'>错误原因</th>
    		</tr>
    	</thead>
    	<tbody>
  	    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=5">
                  <td align="right">
                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td align='center'><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
    			</tr>
     		</xsl:for-each>  	
    	</tbody>
    </root>
	</xsl:template>
</xsl:stylesheet>