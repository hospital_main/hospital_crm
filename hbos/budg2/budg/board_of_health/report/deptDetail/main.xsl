<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th rowspan="2" style="display:none">预算年度</th>
				<th rowspan="2">经济分类编码</th>
				<th rowspan="2">经济分类</th>
				<th rowspan="2">功能分类</th>
				<th rowspan="1" colspan="4">年初预算数</th>
				<th rowspan="1" colspan="4">批复数</th>
				<th rowspan="1" colspan="4">差额</th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th>合计</th>
				<th>财政资金</th>
				<th>预算外</th>
				<th>其他资金</th>
				<th>合计</th>
				<th>财政资金</th>
				<th>预算外</th>
				<th>其他资金</th>
				<th>合计</th>
				<th>财政资金</th>
				<th>预算外</th>
				<th>其他资金</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
          <td align='center'  noWrap='true'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td noWrap="true" style="display:none">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()=2">
								<td noWrap="true">
									<a tabindex='-1'><xsl:value-of select="."/></a>
								</td>
							</xsl:when>
							<xsl:when test="position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16">
								<td align="right">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
