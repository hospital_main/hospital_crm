<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
    <thead>
    	 <tr noWrap='true' class='mainHead' >
  		<td  style="colspan:15;fontsize:maintitle"></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
    </tr>
     <tr noWrap='true' class='mainHead' >
						<td rowspan="2">经济分类编码</td>
						<td rowspan="2">经济分类</td>
						<td rowspan="2">功能分类</td>
						<td rowspan="1" colspan="4">年初预算数</td>
						<td style="display:none"></td>
						<td style="display:none"></td>
						<td style="display:none"></td>
						<td rowspan="1" colspan="4">批复数</td>
						<td style="display:none"></td>
						<td style="display:none"></td>
						<td style="display:none"></td>
						<td rowspan="1" colspan="4">差额</td>
						<td style="display:none"></td>
						<td style="display:none"></td>
						<td style="display:none"></td>
    </tr>
    <tr noWrap='true' class='mainHead'>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td>合计</td>
					<td>财政资金</td>
					<td>预算外</td>
					<td>其他资金</td>
					<td>合计</td>
					<td>财政资金</td>
					<td>预算外</td>
					<td>其他资金</td>
					<td>合计</td>
					<td>财政资金</td>
					<td>预算外</td>
					<td>其他资金</td>	 
    </tr>
   </thead>
   
	    <tbody>
	      <xsl:for-each select="/root/tbody/tr">
	        <tr>
	        	<xsl:for-each select="td">          
	            <xsl:choose>           
	              <xsl:when test="position()=1">
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()=2">
								<td noWrap="true">
									<a tabindex='-1'><xsl:value-of select="."/></a>
								</td>
							</xsl:when>
							<xsl:when test="position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15">
								<td class="numberText" align="right">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>	
	            </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
      </tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>
