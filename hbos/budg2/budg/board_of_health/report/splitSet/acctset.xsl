
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none" width='25'><input type='checkboxs'/></th>
				<th><input type="checkbox" id="allcheckbox" onclick="setCheckAll(this)"/></th>
				<th>会计科目代码</th>
				<th>会计科目名称</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td style='display:none' align='center'  noWrap='true'  >
            <input type='checkboxs' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <!--<xsl:when test="position()=1">
			            <td  noWrap='true' >
	                  <a tabindex='-1'><xsl:value-of select="."/></a>
			            </td>
                </xsl:when>-->
                <xsl:when test="position()=1">
			            <td align="center"><input type="checkbox"/></td>
                </xsl:when>  
                <xsl:when test="position()=4 or position()=5 or position()=6 or position()=7 or position()=8">
                  <td style="display:none"><xsl:value-of select="."/></td>
                </xsl:when>                
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
