<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;" rowspan="2">预算年度</td>
					<td style="fontsize:coltitle;" rowspan="2">科目编码</td>
					<td style="fontsize:coltitle;" rowspan="2">会计科目名称</td>
					<td style="fontsize:coltitle;" rowspan="2">功能分类</td>
					<td style="fontsize:coltitle;" colspan="4">预算数</td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td rowspan="2">批复文号</td>
  		</tr>
  		<tr noWrap="true" class="mainHead">
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td rowspan="2">批复文号</td>
					<td>合计</td>
					<td>财政资金</td>
					<td>预算外资金</td>
					<td>其他资金</td>
					<td style="display:none"></td>
  		</tr>  	
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>           
	              <xsl:when test="position()=1">
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()=2">
								<td noWrap="true">
									<a tabindex='-1'><xsl:value-of select="."/></a>
								</td>
							</xsl:when>
							<xsl:when test="position()=5 or position()=6 or position()=7 or position()=8">
								<td align="right">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>	
	            </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>