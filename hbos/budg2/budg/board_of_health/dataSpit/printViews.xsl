<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td) - 1"/>
	  <root>
	    <thead>
	    	<tr>
	    		<td style='fontsize:maintitle;colspan:colcount'></td>
	    		<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNums"/>    
	        </xsl:call-template>
	    	</tr>	  
	    	<tr>
	    		<td style='fontsize:subtitle;colspan:colcount;align:left'></td>
	    		<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNums"/>    
	        </xsl:call-template>
	    	</tr>	  	
	      <tr>
	      	<xsl:for-each select="/root/tbody/tr[position() = 1]">
	      		<xsl:for-each select="td">
	      			<td nowrap='true'><xsl:value-of select="."/></td>
	      		</xsl:for-each>
	      	</xsl:for-each>
	      </tr>
	      <tr>
	      	<xsl:for-each select="/root/tbody/tr[position() = 2]">
	      		<xsl:for-each select="td">
	      			<td nowrap='true'><xsl:value-of select="."/></td>
	      		</xsl:for-each>
	      	</xsl:for-each>
	      </tr>
	    </thead>
	    <tbody>
	      <xsl:for-each select="/root/tbody/tr[position() > 2]">
	        <tr>
	          <xsl:for-each select="td">
	            
              <xsl:choose>

              	<xsl:when test="position()>3">
              		<td align="right">
              			<xsl:value-of select="format-number(.,'#,##0.00')"/>
              		</td>
              	</xsl:when>
              	<xsl:otherwise>
              		<td><xsl:value-of select="."/></td>
              	</xsl:otherwise>
              </xsl:choose>
	            
	          </xsl:for-each>
	        </tr>
	      </xsl:for-each>
	    </tbody>
    </root>
  </xsl:template>
  
  <xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>