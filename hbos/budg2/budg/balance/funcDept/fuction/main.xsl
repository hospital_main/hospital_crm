<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>调整序号</th>
				<th>调整日期</th>
				<th>调整文号</th>
				<th>预算下达</th>
				<th>调整月份</th>
				<th>编制模式</th>
				<th>调整说明</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' align="left">
		              <a tabindex="-2">
			                <xsl:attribute name="href" >
			                  javascript:openDialog('main2.html?load=&lt;adjust_xh&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/adjust_xh&gt;', 'dialogWidth:1204px;dialogHeight:768px', result)
			                </xsl:attribute>
	                  	<xsl:value-of select="."/>
	                  </a>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
