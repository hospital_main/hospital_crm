<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	
  	<thead>
  	<tr noWrap="true" class="mainHead">
			<td style="fontsize:maintitle;colspan:colcount"></td>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			
		</tr>
 		<tr noWrap="true" class="mainHead">
			<td nowrap="true" rowspan="2" dataIndex="0">科室</td>
			<td nowrap="true" rowspan="2" dataIndex="0">预算基础</td>
			<td nowrap="true" colspan="4" dataIndex="0">本年预算</td>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			
			<td nowrap="true" rowspan="2" dataIndex="0">审查状态</td>
			<td nowrap="true" rowspan="2" dataIndex="0">审查意见</td>
			
		</tr>
		<tr noWrap="true" class="mainHead">
			<td style="display:none"/>
			<td style="display:none"/>
			<td nowrap="true" dataIndex="0">单位下达</td>
			<td nowrap="true" dataIndex="0">科室预算</td>
			<td nowrap="true" dataIndex="0">差额</td>
			<td nowrap="true" dataIndex="0">差异率(%)</td>
			<td style="display:none"/>
			<td style="display:none"/>
			
			
		</tr>
  	
  	</thead>  
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
	            	
	              <xsl:when test=" position() = 2 or position() = 3 or position() = 4 or position() = 5 or position() = 6">
	                <td><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	              </xsl:when>
	              <xsl:when test=" position() = 8">
	                <td><xsl:value-of select="substring-after(.,'|||')"/></td>
	              </xsl:when>
	             <xsl:when test=" position() = 7">
	                
	              </xsl:when>
	              <xsl:otherwise>
	              	<td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>	
  	<!--tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' align="left">
		              <a tabindex="-2">
			                <xsl:attribute name="href" >
			                  javascript:openDialog('main2.html?load=&lt;adjust_xh&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/adjust_xh&gt;', 'dialogWidth:1204px;dialogHeight:768px', result)
			                </xsl:attribute>
	                  	<xsl:value-of select="."/>
	                  </a>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody-->
 		</root>
	</xsl:template>
</xsl:stylesheet>