<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>��������</th>
				<xsl:for-each select="/root/tbody/tr[td[1]='4_th']">
				    <th><xsl:value-of select="td[2]"/></th>
				  </xsl:for-each>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[td[1]!='4_th']">
				<tr>
				<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td noWrap="true">
										<xsl:value-of select="."/>
								</td>
							</xsl:when>
							
							<xsl:otherwise>
								<td noWrap="true" align="right">
									<xsl:value-of select="format-number(.,'#,###.00')"/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
