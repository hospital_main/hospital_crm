<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
 <thead>
	<xsl:variable name="colNum" select="number(3)"/>
		<tr class='mainHead'>
		<xsl:for-each select="/root/tbody/tr[1]/td">
			<xsl:if  test="position() &lt; 3">
				<th nowrap='true'>
					<xsl:attribute name="rowspan">2</xsl:attribute>
					<xsl:value-of select="."/>
				</th>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="/root/tbody/tr[1]/td">
			<xsl:if test="(position() &gt; 2) and (((position() - 2) mod $colNum) = 1)">
			<th nowrap='true'>
				<xsl:attribute name="colspan"><xsl:value-of select='$colNum'/></xsl:attribute>
				<xsl:value-of select="."/>
			</th>
			</xsl:if>
		</xsl:for-each>
		</tr>
		<tr class='mainHead'>
		<xsl:for-each select="/root/tbody/tr[2]">
			<xsl:for-each select="td">
			<xsl:if test="position() &gt; 2">
				<th nowrap='true'>
					<xsl:value-of select="."/>
				</th>
			</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
		</tr>
</thead>
<tbody>
		<xsl:for-each select="/root/tbody/tr[position() &gt; 2]"><!-- td[1]!='��Ŀ����' -->
			<tr>
				<xsl:for-each select="td">
					<xsl:if test="position() &lt; 3 and . != '0'">
						<td><xsl:value-of select="."/></td>
					</xsl:if>
					<xsl:if test="position() &lt; 3 and . = '0'">
						<td></td>
					</xsl:if>
					<xsl:if test="position() &gt; 2">
						<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
					</xsl:if>
				</xsl:for-each>

			</tr>
		</xsl:for-each>
</tbody>

 	</xsl:template>
</xsl:stylesheet>


