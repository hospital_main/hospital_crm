<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th nowrap='true'>科室编码</th>
				<th nowrap='true'>科室名称</th>
				<th nowrap='true'>科室建议数</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td[position()&lt;6 ]">   
						<xsl:choose>
							<xsl:when test="position() = 1">
								<td align="center" noWrap="true" width='100'>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position() = 3">
								<td align="center" noWrap="true" width='300'>
									<input type="text"  name="defaultValue" style="border:1px solid gray;width:200px">
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
									</input>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

