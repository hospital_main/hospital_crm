<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>ѡ��</th>
      	<th nowrap='true'>����</th>
      	<th nowrap='true'>����</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<td align='center'  noWrap='true'>
            <input type='radio' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
      			  <xsl:attribute name="onclick">
      			  	doChoose(this);
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
                <xsl:when test="position()=1">
  	            	<xsl:value-of select="."/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

