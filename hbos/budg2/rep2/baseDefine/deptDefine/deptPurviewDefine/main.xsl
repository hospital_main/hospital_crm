<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
        <th width='25'><input type='checkbox' onclick="checkInit(this)"/></th>
				<th nowrap='true'>���ұ���</th>
				<th nowrap='true'>��������</th> 
			</tr>           
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
        <td align='center'  noWrap='true'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
    			  </xsl:attribute>
    			  <xsl:attribute name="onclick">
    			  	doChoose(this);
    			  </xsl:attribute>
    			  <xsl:if test="td[5]='1'">
	    			  <xsl:attribute name="checked">
	    			  	true
	    			  </xsl:attribute>
    			  </xsl:if>
  			  </input>
        </td>
				<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position() = 1">  
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position() = 3 or position() = 4 or position() = 5">  
								<td style="display:none">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>  
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

