<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
	  	<thead>
		  	<tr>
		   		<td style="fontsize:maintitle;colspan:colcount;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
	  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;" width="180">数据集编码</td>
					<td style="fontsize:coltitle;" width="280">数据集名称</td>
					<td style="fontsize:coltitle;" width="430">数据集语句</td>
					<td style="fontsize:coltitle;" width="330">数据集说明</td>
	  		</tr>  	
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test=" position() = 1 ">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	              <xsl:otherwise>
	              	<td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          </xsl:choose>          
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>