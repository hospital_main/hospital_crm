<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>字段编码</th>
      	<th nowrap='true'>字段名称</th>
      	<th nowrap='true'>数据类型</th>
      	<th nowrap='true'>数据表</th>
      	<th nowrap='true'>字段类型</th>
      	<th nowrap='true'>参数对象</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
                <xsl:when test="position()=1">
  	            	<xsl:value-of select="."/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

