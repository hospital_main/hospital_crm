<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>项目编码</td>
				<td nowrap='true'>项目名称</td> 
				
			</tr>           
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
					<xsl:choose>
						<xsl:when test="position() = 1">
							<td align="left">
								<xsl:value-of select="."/>
							</td>
						</xsl:when>
						<xsl:when test="position() = 2">
							<td align="left">
								<xsl:value-of select="."/>  
							</td>
						</xsl:when>
						<xsl:otherwise>
							
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>

