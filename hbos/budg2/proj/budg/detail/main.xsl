<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>项目编码</th>
  			<th nowrap='true'>项目名称</th>
  			<th nowrap='true'>申请科室</th>
  			<th nowrap='true'>项目负责人</th>
  			<th nowrap='true'>明细名称</th>
  			<th nowrap='true'>资金来源</th>
  			<th nowrap='true'>会计科目</th>
  			<th nowrap='true'>年初预算</th>
  			<th nowrap='true'>调整预算</th>
  			<th nowrap='true'>总预算</th>
  			<th nowrap='true'>执行金额</th>
  			<th nowrap='true'>剩余金额</th>
  			<th nowrap='true'>执行进度%</th>  			
  		</tr>
  	</thead>

  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
       	<xsl:variable name="colCnt" select='position()'/>
        <tr>
        	<xsl:variable name="dept_name" select='td[1]'/>
        	<xsl:variable name="rowCnt" select='count(/root/tbody/tr[td[1] = $dept_name])'/>
					
					<xsl:if test="$colCnt = 1">
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[1]"/>
	    			</td>
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[2]"/>
	    			</td>
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[3]"/>
	    			</td>
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[4]"/>
	    			</td>
					</xsl:if>
					<xsl:if test="$colCnt &gt; 1">
			    		<xsl:if test="td[1] != ../tr[$colCnt - 1 ]/td[1]">
			    			<td noWrap="true">
			    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			    				<xsl:value-of select="td[1]"/>
			    			</td>
			    			<td noWrap="true">
			    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			    				<xsl:value-of select="td[2]"/>
			    			</td>
			    			<td noWrap="true">
			    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			    				<xsl:value-of select="td[3]"/>
			    			</td>
			    			<td noWrap="true">
			    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			    				<xsl:value-of select="td[4]"/>
			    			</td>
			    		</xsl:if>
			    		<xsl:if test="td[1] = ../tr[$colCnt - 1 ]/td[1]">
			    			<td style="display:none"/>
			    			<td style="display:none"/>
			    			<td style="display:none"/>
			    			<td style="display:none"/>
			    		</xsl:if>
			     </xsl:if>
        	
          <xsl:for-each select="td[position() &gt; 4]">
          	<xsl:if test="position() = 1 or position() = 2 or position() = 3">
          		<xsl:if test=".!='合计'">
	         			<td><xsl:value-of select="."/></td>
         			</xsl:if>
          		<xsl:if test=". ='合计' and position() = 1 ">
	         			<td align="center" colspan="3"><xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute><xsl:value-of select="."/></td>
         			</xsl:if>
         		</xsl:if>

          	<xsl:if test="position() &gt; 3">
         			<td align="right">
         				<xsl:if test="../td[5] = '合计'">
         					<xsl:attribute name="bgcolor">#CCFFFF</xsl:attribute>
         				</xsl:if>
         				<xsl:value-of select="format-number(.,'#,##0.00')"/>
         			</td>
         		</xsl:if>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>