<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	
  	<thead>
  	<tr noWrap="true" class="mainHead">
			<td style="fontsize:maintitle;colspan:colcount">科室项目执行分析</td>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			
		</tr>
		<tr>
			<td>科室编码</td>
			<td>科室名称</td>
			<td>年度余额</td>
			<td>预算增加</td>
			<td>预算调整</td>
			<td>预算总额</td>
			<td>执行金额</td>
			<td>剩余额度</td>
			<td>执行进度(%)</td>
			
		</tr>
		
  	</thead>  
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
           <xsl:for-each select="td">
              <xsl:choose>
              	 <xsl:when test="position()=1">
		              <td>
		                <a href="#">
										 
										  <xsl:attribute name="onclick">
											javascript:openDialog('deptView.html?load=&lt;dept_id&gt;<xsl:value-of select="../td[1]"/>&lt;/dept_id&gt;','dialogWidth:900px;dialogHeight:480px')
											</xsl:attribute>
		               		<xsl:value-of select="."/>
										</a>
		              </td>
		            </xsl:when>
              	
                <xsl:when test="position() = 2">
			            <td  noWrap='true'>
	                  	<xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' align="right" >
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
 		</tbody>	
    <tfoot>
 			<tr noWrap='true'>
	        	<td style='fontsize:subtitle;colspan:colcount;align:right'></td>
  	  </tr>
 			</tfoot>
 		</root>
	</xsl:template>
</xsl:stylesheet>