<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th>项目编码</th>
				<th>项目名称</th>
				<th>负责人</th>
				<th>年度余额</th>
				<th>预算增加</th>
				<th>预算调整</th>
				<th>预算总额</th>
				<th>执行金额</th>
				<th>剩余额度</th>
				<th>执行进度(%)</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">	    	
        <tr>
          <xsl:for-each select="td">          	
          	 <xsl:choose>
	             <xsl:when test="position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9">
			            <td  noWrap='true' align="right">
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		              </td>
	             </xsl:when>
	              <xsl:when test="position()=10">
			            <td  noWrap='true' align="right">
		              <xsl:value-of select="."/>
		              </td>
	             </xsl:when> 
	             <xsl:otherwise>
	              	<td align='left'><xsl:value-of select="."/></td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  	<tfoot>
 			<tr noWrap='true'>
	        	<td style='fontsize:subtitle;colspan:colcount;align:right'></td>
  			</tr>
 			</tfoot>
	</xsl:template>
</xsl:stylesheet>
