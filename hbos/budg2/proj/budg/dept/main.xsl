<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>���ұ���</th>
  			<th nowrap='true'>��������</th>
  			<th nowrap='true'>������</th>
  			<th nowrap='true'>Ԥ������</th>
				<th nowrap='true'>Ԥ�����</th>
				<th nowrap='true'>Ԥ���ܶ�</th>
				<th nowrap='true'>ִ�н��</th>
				<th nowrap='true'>ʣ����</th>
				<th nowrap='true'>ִ�н���(%)</th> 			
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
           <xsl:for-each select="td">
              <xsl:choose>
              	 <xsl:when test="position()=1">
		              <td>
		                <a href="#">
										 
										  <xsl:attribute name="onclick">
												openDetail('&lt;dept_id&gt;<xsl:value-of select="../td[1]"/>&lt;/dept_id&gt;')
											</xsl:attribute>
											
		               		<xsl:value-of select="."/>
										</a>
		              </td>
		            </xsl:when>
              	
                <xsl:when test="position() = 2">
			            <td  noWrap='true'>
	                  	<xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' align="right" >
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>