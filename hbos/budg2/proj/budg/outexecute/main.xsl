<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="colNum" select="/root/tbody/tr[1]/td[last()]"/>
			<xsl:for-each select="/root/tbody/tr[position()=1]">
				<tr noWrap='true' class='mainHead'>
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test=" position() = 2 ">
	          		<th rowspan='2' nowrap='true' ><xsl:value-of select="."/></th>
	          	</xsl:when>
	          	<xsl:when test=" position() = 3 ">
	          		<th rowspan='2' nowrap='true' ><xsl:value-of select="."/></th>
	          	</xsl:when>
	          	<!--and (position() - 5) mod $colNum = 0-->
	          	<xsl:when test=" position() &gt; 4 and position() &lt; last()  and (position() - 5) mod $colNum = 0">
	          		<th nowrap='true'>
	          			<xsl:attribute name="colspan"><xsl:value-of select="$colNum"/></xsl:attribute>
	          			<xsl:value-of select="."/>
	          		</th>
	          	</xsl:when>
	          
	          	<xsl:otherwise>
	          		
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<xsl:for-each select="/root/tbody/tr[position()=2]">
				<tr noWrap='true' class='mainHead'>
          <xsl:for-each select="td">
          	<xsl:choose>
	          	<xsl:when test=" position() &gt; 4 and position() &lt; last()">
	          		<th nowrap='true' ><xsl:value-of select="."/></th>
	          	</xsl:when>
	          	<xsl:otherwise>
	          		
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
				<tr>
						<xsl:if test="td[2] = '�ϼ�'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[2] != '�ϼ�'">
	          <td align='center'  noWrap='true'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value">
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
	    			  </input>
	          </td>
	        </xsl:if>
					<xsl:for-each select="td">
					
					<xsl:choose>
						  <xsl:when test="position()=2">
						  	<td  noWrap='true' >
				            
		                <a tabindex='-1'>
		                <xsl:attribute name="onclick">
		                openPage('&lt;id&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/id&gt;');
		                </xsl:attribute>
		                <xsl:attribute name="href">
		                  #
		                </xsl:attribute><xsl:value-of select="."/></a>
				            </td>
				            
								
							</xsl:when>
							<xsl:when test="position()=3">
								<td align='left'>	
									<a tabindex="-1">
										<xsl:value-of select="."/>
									</a>
								</td>
							</xsl:when>
						
						
							<xsl:when test="position() &gt; 4 and position() != last()">
								<td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
						
							<xsl:otherwise>
							</xsl:otherwise>
						
						</xsl:choose>
					</xsl:for-each>
					
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
