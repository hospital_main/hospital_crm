<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	
  	<thead>
  	<tr noWrap="true" class="mainHead">
			<td style="fontsize:maintitle;colspan:colcount">项目执行分析</td>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			
		</tr>
		<tr>
			<td>项目编码</td>
			<td >项目名称</td>
			<td >负责人</td>
			<td>年初余额</td>
			<td>预算增加</td>
			<td>预算调整</td>
			<td>预算总额</td>
			<td>执行金额</td>
			<td>剩余金额</td>
			<td>执行进度(%)</td>
			
		</tr>
		
  	</thead>  
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test=" position() =1 or position() =2 or position() =3 ">
	                <td align="left"><xsl:value-of select="."/></td>
	              </xsl:when>
	             
	              <xsl:otherwise>
	              	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	              </xsl:otherwise>
	          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>	
      <tfoot>
 			<tr noWrap='true'>
	        	<td style='fontsize:subtitle;colspan:colcount;align:right'></td>
  			</tr>
 		</tfoot>
 		</root>
	</xsl:template>
</xsl:stylesheet>