<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	
  	<thead>
  	<tr noWrap="true" class="mainHead">
			<td style="fontsize:maintitle;colspan:colcount">项目执行分析</td>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			
		</tr>
		<tr>
  			<th style="display:none">""</th>
        <td>资金来源</td>
				<td>支出编码</td>
				<td>支出项目</td>
				<td>年初余额</td>
				<td>预算增加</td>
				<td>预算调整</td>
				<td>预算总额</td>
				<td>执行金额</td>
				<td>剩余额度</td>
				<td>执行进度(%)</td>
			
		</tr>
		
  	</thead>  
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">	    	
	    	<xsl:variable name="unit_name" select="td[2]" />
	    	<xsl:variable name="cur_pos" select="position()-1" />
	    	<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[2]= $unit_name ])" />
        <tr>
          <xsl:for-each select="td">          	
          	 <xsl:choose>
          	 	<xsl:when test="position()=1">
			            <th style="display:none" noWrap='true' ></th>
	             </xsl:when>
	             <xsl:when test="position()=2">
			            <xsl:if test="$cur_pos = 0 or $unit_name != ../../tr[$cur_pos]/td[2] ">													
										<td rowspan="{$rowspan}">
											<xsl:value-of select="." />									
										</td>
									</xsl:if>								 
								<xsl:if test="$unit_name = ../../tr[$cur_pos]/td[2]">
									<td style="display:none"><xsl:value-of select="."/></td>
								</xsl:if>	
	             </xsl:when>	             
	             <xsl:when test="position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10">
			            <td  noWrap='true' align="right">
		              <xsl:value-of select="format-number(.,'#,##0.00')"/>
		              </td>
	             </xsl:when>
	             <xsl:when test="position()=11">
			            <td  noWrap='true' align="right">
		              <xsl:value-of select="."/>
		              </td>
	             </xsl:when> 
	             <xsl:otherwise>
	              	<td align='left'><xsl:value-of select="."/></td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each> 
 		</tbody>	
    	<tfoot>
 			<tr noWrap='true'>
	        	<td style='fontsize:subtitle;colspan:colcount;align:right'></td>
  			</tr>
 			</tfoot>
 		</root>
	</xsl:template>
</xsl:stylesheet>