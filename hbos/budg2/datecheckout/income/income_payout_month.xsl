<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
			<th>科室标识</th>
			<th>科室名称</th>
			<th>月份</th>
			<th>人数累计</th>
			<th>费用累计</th>
			<th>合计</th>
			<th>月度预算值</th>
			<th>差额</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
  			<tr>
					<xsl:for-each select="td">
						<xsl:choose>
						<xsl:when test="position()=4 or position()=5 or position()=6 or position()=7 or position()=8">
							<td align='right'><xsl:value-of select="format-number(.,'#,###,##0.00')"/></td>
						</xsl:when>
						<xsl:when test="position()=1 or position()=3">
							<td align='right'><xsl:value-of select="."/></td>
						</xsl:when>	
						<xsl:otherwise>
							<td align='left'><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
					</xsl:for-each>
  			</tr>
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>