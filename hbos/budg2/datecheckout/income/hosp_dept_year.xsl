<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
			<th>计划指标名称</th>
			<th>年度计划值</th>
			<th>科室计划值</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
  			<tr>
					<xsl:for-each select="td">
						<xsl:choose>
						<xsl:when test="position()=3 or position()=4">
							<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>	
						<xsl:when test="position()>1">
							<td align='center'><xsl:value-of select="."/></td>
						</xsl:when>	
					</xsl:choose>
					</xsl:for-each>
  			</tr>
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>