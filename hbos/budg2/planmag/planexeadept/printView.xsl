<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
  		<tr noWrap="true" class="mainHead">
        <td rowspan="2" valign="middle">指标编码</td>
        <td rowspan="2" valign="middle">指标名称</td>
        <td rowspan="2" valign="middle">计量单位</td>        
		    <td colspan="3">年度计划</td>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td colspan="3">1月</td>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td colspan="3">2月</td>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td colspan="3">3月</td>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td colspan="3">4月</td>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td colspan="3">5月</td>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td colspan="3">6月</td>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td colspan="3">7月</td>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td colspan="3">8月</td>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td colspan="3">9月</td>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td colspan="3">10月</td>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td colspan="3">11月</td>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td colspan="3">12月</td>
				<td style='display:none'/>
				<td style='display:none'/>
  		</tr>
  		<tr noWrap="true" class="mainHead"> 
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>	
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>	
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>		
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>		
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>	
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>	
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>		
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>		
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>		    
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>	
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>		
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>		
		    <td valign="middle">计划量</td>
		    <td valign="middle">实际执行</td>
		    <td valign="middle">执行进度%</td>		    		    
		  </tr> 
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
		            <xsl:when test=" position() = 1 ">
		            </xsl:when>
	              <xsl:when test=" position() &gt; 4 ">
	                <td><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	              </xsl:when>
	              <xsl:otherwise>
	               	<td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>