<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
  	<tr>
   		<td style="fontsize:maintitle;colspan:colcount;"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>

		</tr>
  		<tr noWrap="true" class="mainHead">
			<td style="fontsize:coltitle;" width="180" rowspan="2">指标编码</td>
			<td style="fontsize:coltitle;" width="280" rowspan="2">指标名称</td>
			<td style="fontsize:coltitle;" width="300" rowspan="2">计量单位</td>
			<td style="fontsize:coltitle;" width="200" colspan="4">初始预算</td>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="fontsize:coltitle;" width="200" colspan="3">平衡后数据</td>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="fontsize:coltitle;" width="200" colspan="3">审批结果</td>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
  		</tr>
  		<tr>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
			<td>单位计划</td>
			<td>科室汇总</td>
			<td>差额</td>
			<td>差异率%</td>
			<td>平衡数据</td>
			<td>与单位计划差</td>
			<td>与科室汇总差</td>
			<td>单位审批</td>
			<td>单位建议</td>
			<td>职能审批</td>
			<td>职能建议</td>
		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            <xsl:choose>
	              <xsl:when test="position() = 1 or position() = 2 or position() = 3  or position() = 12 or position() = 14">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	             	<xsl:when test=" position() = 13  or position() = 11">
	                <td><xsl:value-of select="substring-after(.,'|||')"/></td>
	              </xsl:when>
	              <xsl:when test="position() &gt; 3 or position() &lt; 11">
	                <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	              </xsl:when>
	              <xsl:otherwise>
	              </xsl:otherwise>
	          </xsl:choose>
  	</xsl:for-each>
  	</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>