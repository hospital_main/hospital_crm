<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>项目名称</th>
				<th>计算序</th>
				<th>编制方法</th>
				<th>计算方法</th>
				<th>参考方法</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        <xsl:for-each select="td">
	<xsl:choose>
		<xsl:when test="position() &lt; 3">
			<td><xsl:value-of select="."/></td>
		</xsl:when>
		<xsl:when test="position() = 3 or position() = 4">
			<td><xsl:value-of select="substring-after(.,'|||')"/></td>
		</xsl:when>
		<xsl:when test="position() = 6">
			<td><xsl:value-of select="."/></td>
		</xsl:when>
	</xsl:choose>
  	</xsl:for-each>
  	</tr>
   	</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
