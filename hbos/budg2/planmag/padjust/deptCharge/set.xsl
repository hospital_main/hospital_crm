<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th><input type="checkbox" onclick="selectAllCheckBox(this)"/></th>
				<th>模板编号</th>
				<th>模板名称</th>
				<th>模板内容</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
		<td align='center' >
			<input type='checkbox' >
			<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
    			</input>
		</td>
                </xsl:when>
		<xsl:otherwise>
		<td ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  	</xsl:for-each>
	      <td align="center"><a href="#" onclick="showDetail(this)">查看</a></td>
  	</tr>
   	</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
