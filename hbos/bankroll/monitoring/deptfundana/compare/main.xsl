<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th rowspan='2'>部门编码</th>
				<th rowspan='2'>部门名称</th>
				<th colspan='3'>基期</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th colspan='3'>对比期</th>			
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th colspan='4'>差异</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th >计划</th>
				<th >执行</th>
				<th >进度(%)</th>
				<th >计划</th>
				<th >执行</th>
				<th >进度(%)</th>
				<th >计划差异</th>
				<th >计划差异率(%)</th>
				<th >执行差异</th>
				<th >执行差异率(%)</th>
			</tr>
		</thead>
		<tbody>
		<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
											 		openInfo('<xsl:value-of select="." />','<xsl:value-of select="../td[2]" />')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="../td[2]!= '合计' and position() = 5 and ../td[3] = 0.00 and ../td[4] != '0.00'">
									<td align='right'></td>
								</xsl:when>
								<xsl:when test="../td[2]!= '合计' and position() = 8 and ../td[6] = 0.00 and ../td[7] != '0.00'">
									<td align='right'></td>
								</xsl:when>
								<xsl:when test="../td[2]!= '合计' and position() = 10 and ../td[3] = 0.00 and ../td[9] != '0.00'">
									<td align='right'></td>
								</xsl:when>
								<xsl:when test="../td[2]!= '合计' and position() = 12 and ../td[4] = 0.00 and ../td[11] != '0.00'">
									<td align='right'></td>
								</xsl:when>
								<xsl:when test="position()&gt; 2">
									<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
