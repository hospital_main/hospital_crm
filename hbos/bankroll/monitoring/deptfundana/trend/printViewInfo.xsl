<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
			<xsl:variable name="headRow" select="count(/root/tbody/tr[td[1]='类别编码'])" />
			<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-2" />
				<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
		  			<xsl:call-template name="repeat">
		  				<xsl:with-param name="times" select="$colNum"/>  
		        </xsl:call-template>
	  			</tr>
				<xsl:for-each select="/root/tbody/tr[position() &lt; ($headRow + 1)]">
				<xsl:variable name="curRow" select="position()" />	
					<tr noWrap="true" class="mainHead">
					<xsl:for-each select="td">
							<xsl:variable name="curcode" select="."/>
			      	<xsl:variable name="curCol" select="position()" />	
			      	<xsl:variable name="colspan" select="count(/root/tbody/tr[$curRow]/td[.=$curcode and .!='计划' and .!='执行'and .!='进度(%)'])" />
			      	<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[$curCol] = $curcode])" />
							<xsl:choose>
								
								<xsl:when test=". = '计划' or .='执行' or .='进度(%)'">
										<td>
											<xsl:value-of select="."/>
										</td>
								</xsl:when>
								<xsl:when test="position() = 3">
								</xsl:when>
								<xsl:when test=". != string(../td[$curCol - 1]) and . != string(/root/tbody/tr[$curRow - 1]/td[$curCol])">
										<td colspan='{$colspan}' rowspan='{$rowspan}'>
											<xsl:value-of select="."/>
										</td>
								</xsl:when>
								<xsl:otherwise>
									<td style='display:none;'></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; $headRow]">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position() = 3">
								</xsl:when>
								<xsl:when test="position() &gt; 3">
										<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
