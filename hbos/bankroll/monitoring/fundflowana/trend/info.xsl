<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="headRow" select="count(/root/tbody/tr[td[1]='项目编码'])" />
				<xsl:for-each select="/root/tbody/tr[position() &lt; ($headRow + 1)]">
				<xsl:variable name="curRow" select="position()" />	
				<tr noWrap="true" class="mainHead">
					<xsl:if test="position() = 1">
        		<th rowspan= '{$headRow}' style="display:none">
						<input type="checkbox"/>
						</th>
        	</xsl:if>
					<xsl:for-each select="td">
							<xsl:variable name="curcode" select="."/>
			      	<xsl:variable name="curCol" select="position()" />	
			      	<xsl:variable name="colspan" select="count(/root/tbody/tr[$curRow]/td[.=$curcode and .!='计划' and .!='执行'and .!='进度(%)'])" />
			      	<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[$curCol] = $curcode])" />
							<xsl:choose>
								<xsl:when test="position() = 3 or position() = 4">
								</xsl:when>
								
								<xsl:when test=". != string(../td[$curCol - 1]) and . != string(/root/tbody/tr[$curRow - 1]/td[$curCol])">
										<th colspan='{$colspan}' rowspan='{$rowspan}'>
											<xsl:value-of select="."/>
										</th>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; $headRow]">
				<tr>
					<xsl:if test="td[2] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[2] != '合计'">
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
        	</xsl:if>
					<xsl:for-each select="td">						
							<xsl:choose>
							
								<xsl:when test="position() = 1">
										<td>
										<xsl:value-of select="." /></td>
								</xsl:when>
								<xsl:when test="position() = 3 or position() = 4">
								</xsl:when>
								<xsl:when test="position() &gt; 3">
				        		<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
				        </xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
