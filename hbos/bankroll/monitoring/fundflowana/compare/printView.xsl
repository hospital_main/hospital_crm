<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		
      
      	</tr>
			<tr noWrap="true" class="mainHead">
				<td rowspan='2'>部门编码</td>
				<td rowspan='2'>部门名称</td>
				<td rowspan='2'>主管部门</td>
				<td colspan='3'>基期</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan='3'>对比期</td>			
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan='4'>差异</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td >计划</td>
				<td >执行</td>
				<td >进度(%)</td>
				<td >计划</td>
				<td >执行</td>
				<td >进度(%)</td>
				<td >计划差异</td>
				<td >计划差异率(%)</td>
				<td >执行差异</td>
				<td >执行差异率(%)</td>
			</tr>
		</thead>
		<tbody>
		<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=4">
								</xsl:when>
								<xsl:when test="position()=5">
								</xsl:when>
								<xsl:when test="../td[2]!= '流入合计' and ../td[2]!= '流出合计' and position() = 8 and ../td[6] = 0.00 and ../td[7] != '0.00'">
									<td align='right'></td>
								</xsl:when>
								<xsl:when test="../td[2]!= '流入合计' and ../td[2]!= '流出合计' and position() = 11 and ../td[9] = 0.00 and ../td[10] != '0.00'">
									<td align='right'></td>
								</xsl:when>
								<xsl:when test="../td[2]!= '流入合计' and ../td[2]!= '流出合计' and position() = 13 and ../td[6] = 0.00 and ../td[12] != '0.00'">
									<td align='right'></td>
								</xsl:when>
								<xsl:when test="../td[2]!= '流入合计' and ../td[2]!= '流出合计' and position() = 15 and ../td[7] = 0.00 and ../td[14] != '0.00'">
									<td align='right'></td>
								</xsl:when>
								<xsl:when test="position()&gt; 5">
									<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
