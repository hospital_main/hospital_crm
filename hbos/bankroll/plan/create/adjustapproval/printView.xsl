<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td) - 1"/>
		<root>
		<thead>
			<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  	</tr>		 
			<tr noWrap='true' class='mainHead'>
			<td >调整编号 </td>
			<td noWrap="true">编制日期 </td>
			<td noWrap="true">编制部门 </td>
			<td noWrap="true">资金类别 </td>
			<td noWrap="true">计划编号</td>
			<td noWrap="true">调整金额 </td>
			<td noWrap="true">审批金额 </td>	
			<td noWrap="true">计划说明</td>	
			<td noWrap="true">状态 </td>
			<td noWrap="true">制单人 </td>
			<td noWrap="true">审核人 </td>
			<td noWrap="true">确认人 </td>
				</tr>
		</thead>
		<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
							<xsl:when test="position()=6 or position()=7 ">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
								</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>