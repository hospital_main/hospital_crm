<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th >业务部门 </th>
			<th noWrap="true">资金项目 </th>
			<th noWrap="true">辅助账 </th>
			<th noWrap="true">计划金额 </th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=4">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
						</xsl:choose>
					</xsl:for-each></tr>
				</xsl:for-each></tbody>
		</xsl:template>
	
	</xsl:stylesheet>