//业务处理
var g_isHasDetail = false;
var d_fund_item_code = "";
var d_fund_item_name = "";
	  
function getDetailData(temp_str){
	//g_isHasDetail = checkIsHasDetail_u(temp_str);
	//清空原有明细数据
	clearDetailData();
	g_isHasDetail = checkIsHasDetail_u(temp_str);
	agreeHandler(temp_str);
	if(g_isHasDetail == true) {
		detail.TableEditEnable = true;
	}
	//控制明细列是否可编辑
	g_isHasDetail = false;
}


function clearDetailData(){

	detail_init();
	
}

function checkIsHasDetail_u(temp_str){

	var srcTree = new window.ActiveXObject("Microsoft.XMLDOM");
	srcTree.async=false;
	//alert(temp_str);
	srcTree.loadXML(temp_str);
		
	var total_count = srcTree.getElementsByTagName("tr").length;
	g_isHasDetail = (total_count > 0 ? true : false);
	return g_isHasDetail;
}


//协议类业务处理
function agreeHandler(temp_str){
			var detailObj = new Object();
			var srcTree = new window.ActiveXObject("Microsoft.XMLDOM");
			srcTree.async=false;
			srcTree.loadXML(temp_str);
			detail.AllowAddNewRow=true;
		
			for(var i=0; i< srcTree.getElementsByTagName("tr").length; i++){
				
				var t_tr = srcTree.getElementsByTagName("tr").item(i);
				
				 detailObj.bill_id = t_tr.getElementsByTagName("bill_id").item(0).text;
				 detailObj.bill_code = t_tr.getElementsByTagName("bill_code").item(0).text;
				 detailObj.bill_date = t_tr.getElementsByTagName("bill_date").item(0).text;
				 detailObj.dept_name = t_tr.getElementsByTagName("dept_code").item(0).text;
				 detailObj.dept_code = detailObj.dept_name.split("|||")[0];
				 detailObj.check_name = t_tr.getElementsByTagName("check_type").item(0).text;
				 detailObj.check_code = detailObj.check_name.split("|||")[0];
				 detailObj.plan_money = t_tr.getElementsByTagName("plan_money").item(0).text;
				 detailObj.check_money = detailObj.plan_money;
			
				//协议类业务
				if(g_acc_type == '2'){
					detailObj.bill_date = detailObj.bill_date.replace("-", "/").replace("-", "/");;   
					var a = new Date(detailObj.bill_date);
					a = a.valueOf();
					a = a + g_acc_date * 24 * 60 * 60 * 1000;
					a = new Date(a);
					
					detailObj.bill_date = a.getFullYear() 
											+ "-" 
											+ ((a.getMonth()+1) > 9 ? (a.getMonth()+1) : ("0" + (a.getMonth() + 1 ) )  ) 
											+ "-" 
											+ (a.getDate() > 9 ? a.getDate() : ("0" + a.getDate())) ;
				}
				
				addToDetail(detailObj);	
			
				temp_str = "";
			}
			detail.AllowAddNewRow=flag_isAdd;

}

function addToDetail(detailObj){
	
		if(d_fund_item_name == ""){
					getDefaultFundItem();
		}
	
		//业务编号,计划日期,业务部门,,资金项目,,"+ g_check_code +",,计划金额,审批金额,备注	
		var str_xml = "<tr>";
		str_xml += ("<td>"+ detailObj.bill_code +"</td>");
		str_xml += ("<td>"+ detailObj.bill_date +"</td>"); 
		str_xml += ("<td>"+ detailObj.dept_name +"</td>"); 
		str_xml += ("<td>"+ detailObj.dept_code +"</td>"); 
		str_xml += ("<td>"+ d_fund_item_name +"</td>"); 
		str_xml += ("<td>"+ d_fund_item_code +"</td>"); 
		str_xml += ("<td>"+ detailObj.check_name +"</td>"); 
		str_xml += ("<td>"+ detailObj.check_code +"</td>"); 
		str_xml += ("<td>"+ detailObj.plan_money +"</td>"); 
		str_xml += ("<td>"+ detailObj.plan_money +"</td>"); 
		str_xml += ("<td></td>"); 
		str_xml += ("<td>"+ detailObj.bill_id +"</td>"); 
				
		str_xml += "</tr>"; 
		detail.AddTableRowData(str_xml);
		
		str_xml = ""; 
}

function insertData(obj){
		//query.submit(obj);
		//保存前校验数据
		if(detail.GetRowCount() <= 0 || detail.GetCellData(1,1) == ""){
			alert("明细数据不能为空！");
			return ;
		}
		
		if(!checkBudgIsPass())
			return;
			
		if(multi.submit(obj,'true')){
					
					var strXml = window.xmlhttp._object.responseXml;
					var temp_plan_id = jQuery(strXml).find("mdctn_main_pk").eq(0).find("plan_id:first").text();
					var temp_plan_code = jQuery(strXml).find("mdctn_main_pk").eq(0).find("plan_code:first").text();
				
					plan_id.value = temp_plan_id;
					bill_no.value = temp_plan_code;

		}
}

function saveData(obj){
		if(detail.GetRowCount() <= 0 || detail.GetCellData(1,1) == ""){
			alert("明细数据不能为空！");
			return ;
		}
		if(!checkBudgIsPass())
			return;
		
		multi.submit(obj,'true');
			
}

function checkBudgIsPass(){
		//预算控制方式是1，不控制 return true
		//预算控制方式是2，提示控制
		//预算控制方式是3，严格控制
		
		var xmlData = "<data>"+multi.sub_assemble().replace(/</g,"&lt;").replace(/>/g,"&gt;")+"</data>";
		var para = "<a>"+acct_year.value+"</a><b>"+sys_dept.value+"</b><c>"+fund_type.value+"</c><d>"+plan_id.value+"</d>";
		window.xmlhttp.post("bankrollPlanCreate_budgIsExceed", xmlData + para, '?isCheck=false');
		var resText = window.xmlhttp._object.responseText;
		var result_type = $(window.xmlhttp._object.responseXML).find("td:eq(0)").text();
		var result_msg = $(window.xmlhttp._object.responseXML).find("td:eq(1)").text();
		
		if(result_type == '2'){
			if(!confirm("资金项目【"+result_msg+"】超出年度资金项目预算是否继续保存?")){
						return false;	
			}	
		}
		if(result_type == '3'){
			alert("保存失败：资金项目【"+result_msg+"】超出年度资金项目预算！");
			return false;
		}
		return true;
		
}

function checkIsHasDetail(){
		if(detail.GetRowCount() <= 0 || detail.GetCellData(1,1) == ""){
			
			return false;
		}
		return true;
		
}


//自动生成业务
function autoCreateFun(){
	var detailObj = new Object();
	//0901对于门诊预交金，从acct_charge_pre取上月数据，doc_type=0
	//0903对于住院预交金，从acct_charge_pre取上月数据，doc_type=1
	//0902对于门诊结算，从acct_charge_acc取上月数据，doc_type=0并且acct_pay_type. pay_attr=1、2
	//0904对于住院结算，从acct_charge_acc取上月数据，doc_type=1并且acct_pay_type. pay_attr=1、2
	var srcTree = new window.ActiveXObject("Microsoft.XMLDOM");
	srcTree.async=false;
	
	if(g_fun_code == '0901' || g_fun_code == '0903'){
			window.xmlhttp.post("bankrollPlanCreateHisPre_select", "<g_fun_code>" + g_fun_code + "</g_fun_code><acct_year>" + acct_year.value + "</acct_year><acct_month>" + acct_month.value + "</acct_month>", "?isCheck=false");	
			
	}
	if(g_fun_code == '0902' || g_fun_code == '0904'){
			window.xmlhttp.post("bankrollPlanCreateHisAcc_select", "<g_fun_code>" + g_fun_code + "</g_fun_code><acct_year>" + acct_year.value + "</acct_year><acct_month>" + acct_month.value + "</acct_month>", "?isCheck=false");	
		
	}
	
	srcTree.load(xmlhttp._object.responseXml);
	var trs = srcTree.getElementsByTagName("tr");
	

	if(trs.length > 0){
		
			detail.AllowAddNewRow=true;
			if(checkIsHasDetail()){
					detailInit_other();
			}
			for(var i = 0; i < trs.length; i++){
				
				detailObj.bill_id =  trs.item(i).getElementsByTagName("td").item(0).firstChild.nodeValue;
				detailObj.bill_code =  trs.item(i).getElementsByTagName("td").item(1).firstChild.nodeValue;
				detailObj.bill_date =  trs.item(i).getElementsByTagName("td").item(2).firstChild.nodeValue;
				detailObj.plan_money =  trs.item(i).getElementsByTagName("td").item(3).firstChild.nodeValue;
				detailObj.check_money = detailObj.plan_money;
				detailObj.dept_name = sys_dept.value+"|||"+sys_dept.text.split("  ")[1];
				detailObj.dept_code = sys_dept.value;
				detailObj.check_name = "";
				detailObj.check_code = "";
			
				addToDetail(detailObj);	
			}
			detail.AllowAddNewRow=flag_isAdd;
			if(checkIsHasDetail() == true) {
				detail.TableEditEnable = true;
			
			}		
	}

}
	

		  var printDs={};
		  var flagHead = 0;
		  function printNew(){
  			if(bill_no.value =='系统自动生成'){
					alert('请先保存数据后再打印！');
					return false;
		  	}
    		printDs.begin=function(){
	          flagHead = 0
	          flag=0;
	          trs_t=print_post();
		    };
				printDs.getNextHeadData=getNextHeadData;
				printDs.getNextRowData=getNextRowData;
				printDs.end=function(){};
				printCellByTemplate("bankrollPlanCreateCreate_totalPrintPreview",printDs);//在\ROOT\base\scripts\sub.js 中打开窗口 
    	}

	  
		//获得表头、表尾数据
	  function getNextHeadData(){ 
			if(flagHead > 0) 
				return null;
			flagHead++;
			subBody.load = 'bankrollPlanCreateCreate_getPrintHead'
			subBody.para = '<a>'+plan_id.value+'</a>'
			subBody.post();
			return subBody.getOneDim()
	  }
	  //获得表体数据
	  function getNextRowData(){
	  	var a = new Array();
	  	if(flag>=trs_t.length)
	  	  return null;
	  	var tds = null;
	  	tds=trs_t[flag].getElementsByTagName("td");
	  	for(var j=0;j<tds.length;j++){
	  			a.push(tds[j].text);
	  	}
	  	flag++;
	  	return a;
	  }
	  function print_post() {
	  	window.xmlhttp.post("bankrollPlanCreateCreate_getPrintBody","<a>"+plan_id.value+"</a>", "?isCheck=false");
			var resXml=window.xmlhttp._object.responseXML;		
			var trs=resXml.getElementsByTagName("tr");
			return trs;
	  }
	  
	  //取资金项目默认值
	  
	  function getDefaultFundItem(){
	  	subBody.load = 'bankrollPlanCreateFundType_select';
			subBody.para = '<a>'+fund_type.value+'</a><b></b>';
			subBody.post();
			var rtnData = subBody.getOneDim();
			if(rtnData){
					
					d_fund_item_name = rtnData[0]+"|||"+rtnData[1];
					d_fund_item_code = rtnData[0];
			}
		
	  	
	  }

