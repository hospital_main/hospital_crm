//业务处理
//手动选择
var g_isHasDetail = false;
var d_fund_item_code = "";
var d_fund_item_name = "";

function setAdjustDetail(tmp_plan_id){
	
	var srcTree = new window.ActiveXObject("Microsoft.XMLDOM");
	srcTree.async=false;
	window.xmlhttp.post("bankrollPlanCreateAdjustPlanDetail_select", "<plan_id>"+tmp_plan_id+"</plan_id>", "?isCheck=false");	
	
	srcTree.load(xmlhttp._object.responseXml);
	var trs = srcTree.getElementsByTagName("tr");
	
	if(trs.length > 0){
				detail_init();
			var detail_para = new Array();
			for(var i = 0; i < trs.length; i++){
				
				detail_para[0] = trs.item(i).getElementsByTagName("td").item(0).text;
				detail_para[1] = trs.item(i).getElementsByTagName("td").item(1).text;
				detail_para[2] = trs.item(i).getElementsByTagName("td").item(2).text;
				detail_para[3] = trs.item(i).getElementsByTagName("td").item(3).text;
				detail_para[4] = trs.item(i).getElementsByTagName("td").item(4).text;
				detail_para[5] = trs.item(i).getElementsByTagName("td").item(5).text;
				detail_para[6] = trs.item(i).getElementsByTagName("td").item(6).text;
				detail_para[7] = trs.item(i).getElementsByTagName("td").item(7).text;
				detail_para[8] = trs.item(i).getElementsByTagName("td").item(9).text;
				detail_para[9] = 0;
				detail_para[10] = 0;
				detail_para[11] = trs.item(i).getElementsByTagName("td").item(10).text;
				detail_para[12] = trs.item(i).getElementsByTagName("td").item(11).text;
				detail_para[13] = trs.item(i).getElementsByTagName("td").item(12).text;
				
				addToDetail(detail_para);	
			}	
			
			if(checkIsHasDetail()){
					if(g_input_type == "1" && g_integrate_type == "1"){
						detail.TableEditEnable = true;
						setDetailCheckBox();
					}
					if(g_input_type == "2"){
						detail.TableEditEnable = true;
						detail.AllowAddNewRow=true;
						//detail.MDAddNewRow=true;
						setDetailCheckBox();
					}
					if(g_input_type == "1" && g_integrate_type == "3"){
						detail.TableEditEnable = true;
						setDetailCheckBox();
					}
					
					
			}
	}	
}
//从计划中取的不可使用，添加的可以使用
function setDetailCheckBox(){
		for(var i=1;i<=detail.GetRowCount();i++){
			
			if(detail.GetCellData(i,14) != "" && detail.GetCellData(i,14) != "0"){
				detail.CheckBoxEnable(i,false);
			}
		}	
}


function addToDetail(detail_para){
		if(d_fund_item_name == ""){
					getDefaultFundItem();
		}
		
		//业务编号,计划日期,业务部门,,资金项目,,"+ g_check_code +",,计划金额,审批金额,备注	
		var str_xml = "<tr>";
		str_xml += ("<td>"+ detail_para[0] +"</td>");
		str_xml += ("<td>"+ detail_para[1] +"</td>"); 
		str_xml += ("<td>"+ detail_para[2] +"</td>"); 
		str_xml += ("<td>"+ detail_para[3] +"</td>"); 
		str_xml += ("<td>"+ (detail_para[4] == "" ? d_fund_item_name :  detail_para[4]) +"</td>"); //资金项目名称
		str_xml += ("<td>"+ (detail_para[5] == "" ? d_fund_item_code :  detail_para[5]) +"</td>"); //资金项目编码
		str_xml += ("<td>"+ detail_para[6] +"</td>"); 
		str_xml += ("<td>"+ detail_para[7] +"</td>"); 
		str_xml += ("<td>"+ detail_para[8] +"</td>"); 
		str_xml += ("<td>"+ detail_para[9] +"</td>"); 
		str_xml += ("<td>"+ detail_para[10] +"</td>"); 
		str_xml += ("<td>"+ detail_para[11] +"</td>"); 
		str_xml += ("<td>"+ detail_para[12] +"</td>");
		str_xml += ("<td>"+ detail_para[13] +"</td>"); 
				
		str_xml += "</tr>"; 
		detail.AddTableRowData(str_xml);
		
		str_xml = ""; 
}

function insertData(obj){
		//query.submit(obj);
		//保存前校验数据
		if(detail.GetRowCount() <= 0 || detail.GetCellData(1,1) == ""){
			alert("明细数据不能为空！");
			return ;
		}
		
		if(!checkBudgIsPass())
			return;
		
		if(multi.submit(obj,'true')){
					
					var strXml = window.xmlhttp._object.responseXml;
					var temp_adjust_id = jQuery(strXml).find("mdctn_main_pk").eq(0).find("adjust_id:first").text();
					var temp_adjust_code = jQuery(strXml).find("mdctn_main_pk").eq(0).find("adjust_code:first").text();
				
					adjust_id.value = temp_adjust_id;
					adjust_code.value = temp_adjust_code;

		}
}

function saveData(obj){
		if(detail.GetRowCount() <= 0 || detail.GetCellData(1,1) == ""){
			alert("明细数据不能为空！");
			return ;
		}
		if(!checkBudgIsPass())
			return;
			
		multi.submit(obj,'true');
}

function checkBudgIsPass(){
		//预算控制方式是1，不控制 return true
		//预算控制方式是2，提示控制
		//预算控制方式是3，严格控制
		
		var xmlData = "<data>"+multi.sub_assemble().replace(/</g,"&lt;").replace(/>/g,"&gt;")+"</data>";
		var para = "<a>"+acct_year.value+"</a><b>"+sys_dept.value+"</b><c>"+fund_type.value+"</c><d>"+adjust_id.value+"</d>";
		window.xmlhttp.post("bankrollPlanAdjust_budgIsExceed", xmlData + para, '?isCheck=false');
		var resText = window.xmlhttp._object.responseText;
		var result_type = $(window.xmlhttp._object.responseXML).find("td:eq(0)").text();
		var result_msg = $(window.xmlhttp._object.responseXML).find("td:eq(1)").text();
		
		if(result_type == '2'){
			if(!confirm("资金项目【"+result_msg+"】超出年度资金项目预算是否继续保存?")){
						return false;	
			}	
		}
		if(result_type == '3'){
			alert("保存失败：资金项目【"+result_msg+"】超出年度资金项目预算！");
			return false;
		}
		return true;
		
}

function checkIsHasDetail(){
		if(detail.GetRowCount() <= 0 || detail.GetCellData(1,1) == ""){
			
			return false;
		}
		return true;
		
}

 			var printDs={};
		  var flagHead = 0;
		  function printNew(){
  			if(adjust_code.value =='系统自动生成'){
					alert('请先保存数据后再打印！');
					return false;
		  	}
    		printDs.begin=function(){
	          flagHead = 0
	          flag=0;
	          trs_t=print_post();
		    };
				printDs.getNextHeadData=getNextHeadData;
				printDs.getNextRowData=getNextRowData;
				printDs.end=function(){};
				printCellByTemplate("bankrollPlanCreateAdjust_totalPrintPreview",printDs);//在\ROOT\base\scripts\sub.js 中打开窗口 
    	}

	//获得表头、表尾数据
	  function getNextHeadData(){ 
			if(flagHead > 0) 
				return null;
			flagHead++;
			subBody.load = 'bankrollPlanCreateAdjust_getPrintHead'
			subBody.para = '<a>'+adjust_id.value+'</a>'
			subBody.post();
			return subBody.getOneDim()
	  }
	  //获得表体数据
	  function getNextRowData(){
	  	var a = new Array();
	  	if(flag>=trs_t.length)
	  	  return null;
	  	var tds = null;
	  	tds=trs_t[flag].getElementsByTagName("td");
	  	for(var j=0;j<tds.length;j++){
	  			a.push(tds[j].text);
	  	}
	  	flag++;
	  	return a;
	  }
	  function print_post() {
	  	window.xmlhttp.post("bankrollPlanCreateAdjust_getPrintBody","<a>"+adjust_id.value+"</a>", "?isCheck=false");
			var resXml=window.xmlhttp._object.responseXML;		
			var trs=resXml.getElementsByTagName("tr");
			return trs;
	  }

 //取资金项目默认值
	  function getDefaultFundItem(){
	  	subBody.load = 'bankrollPlanCreateFundType_select';
			subBody.para = '<a>'+fund_type.value+'</a><b></b>';
			subBody.post();
			var rtnData = subBody.getOneDim();
			if(rtnData){
					
					d_fund_item_name = rtnData[0]+"|||"+rtnData[1];
					d_fund_item_code = rtnData[0];
			}
		
	  	
	  }