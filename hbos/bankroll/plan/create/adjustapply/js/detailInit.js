
function detailInit_other(){
	
		flag_isEdit = (All_isOperate == true ?  flag_isEdit: false);
		flag_isAdd = (All_isOperate == true ?  flag_isAdd: false);
	
		if(isFromAjustApproval){
			flag_isEdit = true;
			flag_isAdd = false;
		}
	
		detail.StartInit();
		detail.HaveFirstSelCol = true;
		detail.isDataAutoSubmit = false;
		detail.TableEditEnable = flag_isEdit;//flag_isEdit;
		detail.VerifyStartValue=true;
		detail.AllowAddNewRow=flag_isAdd;

		detail.SetHeaderFromXML(creatActiveXHeadXML("业务编号,计划日期,业务部门,,资金项目,,"+ g_check_code +",,计划审批金额,调整金额,审批金额,备注,,"));
		detail.SetTableColumn(1,"type=text;edit=verify;align=left;width=100;");//业务编号
		detail.SetTableColumn(2,"type=text;edit=verify;verify=true;align=left;width=100;");//计划日期
		detail.SetTableColumn(3,"type=py;header=部门编码,部门名称;sqlid=bankrollPlanCreateSysdept_select;edit=verify;verify=true;align=left;width=100;required=true");//业务部门名称
		detail.SetTableColumn(4,"type=text;edit=true;align=left;width=0;");//业务部门编码(隐藏)
		detail.SetTableColumn(5,"type=py;header=编码,名称;sqlid=bankrollPlanCreateFundType_select;param=<a>" + fund_type.value  + "</a>;edit=verify;verify=true;align=left;width=100;required=true");//资金项目名称
		detail.SetTableColumn(6,"type=text;edit=true;align=left;width=0;");//资金项目编码(隐藏)
		
		/*
		if(g_check_code == '供应商'){
				detail.SetTableColumn(7,"type=py;header=编码,名称;sqlid=bankrollPlanCreateVendor_select;param=<a>" + g_check_sql  + "</a>;edit=verify;verify=true;align=left;width=100;required=true");//为空的辅助账名称(隐藏)
		}else if(g_check_code == '客户'){
				detail.SetTableColumn(7,"type=py;header=编码,名称;sqlid=bankrollPlanCreateCustomer_select;param=<a>" + g_check_sql  + "</a>;edit=verify;verify=true;align=left;width=100;required=true");//为空的辅助账名称(隐藏)
		}else if(g_check_code == '项目'){
				detail.SetTableColumn(7,"type=py;header=编码,名称;sqlid=bankrollPlanCreateProject_select;param=<a>" + g_check_sql  + "</a>;edit=verify;verify=true;align=left;width=100;required=true");//为空的辅助账名称(隐藏)
		}else{
				detail.SetTableColumn(7,"type=text;edit=true;align=left;width=0;");//为空的辅助账名称(隐藏)	
		}*/
		if(g_check_code != ''){
				detail.SetTableColumn(7,"type=py;header=编码,名称;sqlid=bankrollPlanCreateCheckDict_select;param=<a>" + acct_year.value  + "</a><b>" + g_check_code  + "</b><c>" + g_check_sql  + "</c>;edit=verify;verify=true;align=left;width=100;required=true");//为空的辅助账名称(隐藏)
		}else{
				detail.SetTableColumn(7,"type=text;edit=true;align=left;width=0;");//为空的辅助账名称(隐藏)	
		}
		
		
		detail.SetTableColumn(8,"type=text;edit=true;align=left;width=0;");//为空的辅助账编码(隐藏)
		detail.SetTableColumn(9,"type=number;edit=verify;verify=true;format=0,000.00;width=100;required=true");//计划金额
		detail.SetTableColumn(10,"type=number;edit=verify;verify=true;format=0,000.00;width=100;required=true");//调整金额
		
		if(isFromAjustApproval){
			detail.SetTableColumn(11,"type=number;edit=verify;verify=true;format=0,000.00;width=100;");	//审批金额
		}else{
			detail.SetTableColumn(11,"type=number;edit=false;format=0,000.00;width=100;");	//审批金额
		}
		detail.SetTableColumn(12,"type=text;edit=verify;verify=true;align=left;width=100;");//备注
		detail.SetTableColumn(13,"type=text;edit=true;align=left;width=0;");//业务Id
		detail.SetTableColumn(14,"type=text;edit=true;align=left;width=0;");//明细Id
		
		detail.MDAddNewRow=true;
		detail.AllowUserResize=true;

		detail.EndInit();
}