<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th width='25'><input type='checkbox'/></th>
			<th >调整编号 </th>
			<th noWrap="true">编制日期 </th>
			<th noWrap="true">编制部门 </th>
			<th noWrap="true">资金类别 </th>
			<th noWrap="true">计划编号</th>
			<th noWrap="true">调整金额 </th>
			<th noWrap="true">审批金额 </th>	
			<th noWrap="true">计划说明</th>	
			<th noWrap="true">状态 </th>
			<th noWrap="true">制单人 </th>
			<th noWrap="true">审核人 </th>
			<th noWrap="true">确认人 </th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					
				<tr>
					<xsl:if test="td[1]!='合计'">
 						<td align='center'  style=''>
            		<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              		<xsl:attribute name="value" >&lt;adjust_id&gt;<xsl:value-of select="pk/adjust_id"/>&lt;/adjust_id&gt;</xsl:attribute>
    			 </input> 
    			 </td>
				</xsl:if>
				<xsl:if test="td[1]='合计'" >
					<td></td>
					</xsl:if>
				 
          	
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=1 and ../td[1]!='合计'">
								<td noWrap='true'>
                	<a tabindex='-1' ><xsl:value-of select="."/></a>
                </td>
							</xsl:when>
							<xsl:when test="position()=5 and ../td[1]!='合计'">
								<td  noWrap='true' >
							  	<a tabindex="-1">
							    <xsl:attribute name="href" >
							      javascript:viewPlan('&lt;plan_id&gt;<xsl:value-of select="../pk/plan_id"/>&lt;/plan_id&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:when test="position()=6 or position()=7 ">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
						</xsl:choose>
					</xsl:for-each></tr>
				</xsl:for-each></tbody>
		</xsl:template>
	
	</xsl:stylesheet>