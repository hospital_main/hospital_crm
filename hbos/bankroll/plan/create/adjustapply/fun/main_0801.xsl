<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th width='25'></th>
			<th noWrap="true">合同编号</th>
			<th noWrap="true">合同名称</th>
			<th noWrap="true">签订日期</th>
			<th noWrap="true">客户</th>
			<th noWrap="true">期号</th>	
			<th noWrap="true">资金来源</th>
			<th noWrap="true">收款日期</th>
			<th noWrap="true">收款方式</th>
			<th noWrap="true">收款金额</th>
			<th noWrap="true">已收金额</th>
			<th noWrap="true">未收金额</th>
			</tr>
		</thead>
			<tbody>
				
					<xsl:for-each select="/root/tbody/tr">
		        <tr>
		        	<td align='center'  style=''>
            		<input type='radio' name='select_code'>
              		<xsl:attribute name="value" >
              		<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              		</xsl:attribute>
			    			</input> 
			    		</td>
		          <xsl:for-each select="td">
		            <xsl:choose>
		            	
									<xsl:when test="position()=8 or position()=9 or position()=10">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
								
		              <xsl:otherwise>
		                <td><xsl:value-of select="."/></td>
		              </xsl:otherwise>
		          	</xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>          
			</tbody>
		</xsl:template>
	
	</xsl:stylesheet>