<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:decimal-format NaN=''/>
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th noWrap="true">指标编码</th>
			<th noWrap="true">指标名称</th>
			<th noWrap="true">职能科室</th>
			<th noWrap="true">期初余额</th>
			<th noWrap="true">本期应收</th>
			<th noWrap="true">本期收款</th>
			<th noWrap="true">期末余额</th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">	
				<tr>
					<xsl:for-each select="./td">
						<xsl:choose>
							
							<xsl:when test="position()=4 or position()=5 or position()=6 or position()=7">
								<td align='right'>
								<xsl:value-of select="format-number(.,'###,##0.00')" />
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
				</xsl:for-each>
			</tbody>
		</xsl:template>
	
	</xsl:stylesheet>