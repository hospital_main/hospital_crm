<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true' >
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap="true" class="mainHead">
					<td noWrap="true">月份</td>
					<td noWrap="true">期初余额</td>
					<td noWrap="true">资金流入量</td>
					<td noWrap="true">资金流出量</td>
					<td noWrap="true">净现金流量</td>
					<td noWrap="true">期末余额</td>
					<td noWrap="true">理想余额</td>
					<td noWrap="true">资金余缺</td>
			  </tr>
	  	</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=3 or position()=4 or position()=5">
								<td align="right"> 
									<xsl:value-of select="format-number(.,'###,##0.00')" />
								</td>
							</xsl:when>
							<xsl:when test="position()=2 or position()=6 or position()=7 or position()=8">
								<xsl:if test="../td[1] = '合计'">
									<td></td>
								</xsl:if>
								<xsl:if test="../td[1] != '合计'">
									<td align="right"> 
										<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>