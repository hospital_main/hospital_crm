<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:decimal-format NaN=''/>
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th noWrap="true">月份</th>
			<th noWrap="true">期初余额</th>
			<th noWrap="true">资金流入量</th>
			<th noWrap="true">资金流出量</th>
			<th noWrap="true">净现金流量</th>
			<th noWrap="true">期末余额</th>
			<th noWrap="true">理想余额</th>
			<th noWrap="true">资金余缺</th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">	
				<tr>
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=3 or position()=4 or position()=5">
								<td align="right"> 
									<xsl:value-of select="format-number(.,'###,##0.00')" />
								</td>
							</xsl:when>
							<xsl:when test="position()=2 or position()=6 or position()=7 or position()=8">
								<xsl:if test="../td[1] = '合计'">
									<td></td>
								</xsl:if>
								<xsl:if test="../td[1] != '合计'">
									<td align="right"> 
										<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
				</xsl:for-each>
			</tbody>
		</xsl:template>
	
	</xsl:stylesheet>