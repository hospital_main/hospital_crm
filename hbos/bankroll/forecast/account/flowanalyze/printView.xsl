<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-2"/>
  	<root>
    	<thead>
	    	<tr noWrap='true' >
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap="true" class="mainHead">
					<td noWrap="true">指标编码</td>
					<td noWrap="true">指标名称</td>
					<td noWrap="true">方向</td>
					<td noWrap="true">职能科室</td>
					<td noWrap="true">资金流量</td>
					<td noWrap="true">流量占比(%)</td>
					<td noWrap="true" >flag</td>
			  </tr>
	  	</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=5">
								<td align='right'>
								<xsl:value-of select="format-number(.,'###,##0.00')" />
								</td>
							</xsl:when>
							<xsl:when test="position()=6">
								<xsl:if test="../td[7] = '3' ">
									<td>
									</td>
								</xsl:if>
								<xsl:if test="../td[7] != '3' ">
									<td align='right'>
									<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:if>
							</xsl:when>
							
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>