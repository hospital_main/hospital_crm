<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:decimal-format NaN=''/>
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th noWrap="true">指标编码</th>
			<th noWrap="true">指标名称</th>
			<th noWrap="true">方向</th>
			<th noWrap="true">职能科室</th>
			<th noWrap="true">资金流量</th>
			<th noWrap="true">流量占比(%)</th>
			<th noWrap="true" style="display:none">flag</th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">	
				<tr>
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=5">
								<td align='right'>
								<xsl:value-of select="format-number(.,'###,##0.00')" />
								</td>
							</xsl:when>
							<xsl:when test="position()=6">
								<xsl:if test="../td[7] = '3' ">
									<td>
									</td>
								</xsl:if>
								<xsl:if test="../td[7] != '3' ">
									<td align='right'>
									<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=7">
								<td style="display:none">
									<xsl:value-of select="." />
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
				</xsl:for-each>
			</tbody>
		</xsl:template>
	
	</xsl:stylesheet>