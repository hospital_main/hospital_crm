<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-3"/>
  	<root>
    	<thead>
	    	<tr noWrap='true' >
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap="true" class="mainHead">
					<td noWrap="true">预测日期</td>
					<td noWrap="true">科室名称</td>
					<td noWrap="true">预测指标</td>
					<td noWrap="true">单据编码</td>
					<td noWrap="true">流入金额</td>
					<td noWrap="true">流出金额</td>
					<td noWrap="true">资金余额</td>
					<td noWrap="true" >dire</td>
					<td noWrap="true" >flag</td>
			  </tr>
	  	</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=5">
								<xsl:if test="(../td[8] = '' and ../td[9] = '0' ) or ../td[8] = '1' ">
									<td align='right'>
									</td>
								</xsl:if>
								<xsl:if test="../td[8] = '0' or ../td[9] = '2' or ../td[9] = '3' ">
									<td align='right'>
									<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=6">
								<xsl:if test="(../td[8] = '' and ../td[9] = '0' ) or ../td[8] = '0' ">
									<td align='right'>
									</td>
								</xsl:if>
								<xsl:if test="../td[8] = '1' or ../td[9] = '2' or ../td[9] = '3' ">
									<td align='right'>
									<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=7">
								<xsl:if test=" ../td[9] = '2' or ../td[9] = '3'">
									<td align='right'>
									</td>
								</xsl:if>
								<xsl:if test=" ../td[9] = '0' or ../td[9] = '1' ">
									<td align='right'>
									<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:if>
							</xsl:when>
							
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>