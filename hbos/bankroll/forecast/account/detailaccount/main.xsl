<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:decimal-format NaN=''/>
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th noWrap="true">预测日期</th>
			<th noWrap="true">科室名称</th>
			<th noWrap="true">预测指标</th>
			<th noWrap="true">单据编码</th>
			<th noWrap="true">流入金额</th>
			<th noWrap="true">流出金额</th>
			<th noWrap="true">资金余额</th>
			<th noWrap="true" style="display:none">dire</th>
			<th noWrap="true" style="display:none">flag</th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">	
				<tr>
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=5">
								<xsl:if test="(../td[8] = '' and ../td[9] = '0' ) or ../td[8] = '1' ">
									<td align='right'>
									</td>
								</xsl:if>
								<xsl:if test="../td[8] = '0' or ../td[9] = '2' or ../td[9] = '3' ">
									<td align='right'>
									<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=6">
								<xsl:if test="(../td[8] = '' and ../td[9] = '0' ) or ../td[8] = '0' ">
									<td align='right'>
									</td>
								</xsl:if>
								<xsl:if test="../td[8] = '1' or ../td[9] = '2' or ../td[9] = '3' ">
									<td align='right'>
									<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=7">
								<xsl:if test=" ../td[9] = '2' or ../td[9] = '3'">
									<td align='right'>
									</td>
								</xsl:if>
								<xsl:if test=" ../td[9] = '0' or ../td[9] = '1' ">
									<td align='right'>
									<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=8 or position()=9">
								<td style="display:none">
									<xsl:value-of select="." />
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
				</xsl:for-each>
			</tbody>
		</xsl:template>
	
	</xsl:stylesheet>