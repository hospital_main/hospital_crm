	var modCode,isExpectSet,detailFlag=false,isExpect;//detailFlag为false没有明细
	
	function init(){
			var insert=getPageArg("insert");
			if(insert){
				update_type.value="insert";
				changeInputType();
				changeDir();
			}else{
				updateInit();
			}
	}
	
	function updateInit(){
		tar_code.readOnly=true;
		multi.load="bankrollForecastBasesetIndex_select";
		multi.onAfterLoad="afterLoad()";
		multi.init();
	}
	function  afterLoad(){
			contract_type_td.style.display="none";
			type_code_td.style.display="none";
			acct_subj_td.style.display="none";
			showDetailFlag();
		  	showMainInput(modCode);
		  	cash_item.para="<direction>"+direction.value+"</direction>";
		  	fun_code.para="<direction>"+direction.value+"</direction>";
		  	fun_code.refresh();
		  	
	}
	function changeDir(){
		cash_item.para="<direction>"+direction.value+"</direction>";
		cash_item.refresh();
		cash_item.setValue('|||');
		fun_code.para="<direction>"+direction.value+"</direction>";
		fun_code.refresh();
		 if(modCode=='13'){
			type_code.para="<dept>"+dept_code.value+"</dept><dir>"+direction.value+"</dir>";
			type_code.refresh();
			type_code.selectItem(0);
		}else if(modCode=='14'){
			acct_subj.para="<dir>"+direction.value+"</dir>";
			acct_subj.refresh();
			acct_subj.selectItem(0);
		}
		cash_item.selectItem(0);
	}
	function changeFun(){
		contract_type_td.style.display="none";
		type_code_td.style.display="none";
		acct_subj_td.style.display="none";
		type_code.required="false";
		contract_type.required="false";
		acct_subj.required="false";
		type_code.setValue("|||");
		contract_type.setValue("|||");
		acct_subj.setValue("|||");
		is_expect.setEnabled(true);
		if(!postFun())return;
		showMainInput(modCode);
		is_expect.setValue(isExpect);
		changeExpect();
		is_expect.setEnabled(false);
	}
	function changeDept(){
		if(modCode=='07' || modCode=='08'){
			contract_type.para="<dept>"+dept_code.value+"</dept><mod>"+modCode+"</mod>";
			contract_type.refresh();
		}else if(modCode=='13'){
			type_code.para="<dept>"+dept_code.value+"</dept><dir>"+direction.value+"</dir>";
			type_code.refresh();
		}
	}
	
	function changeInputType(){
		if(input_type.value=='2'){
			fun_code.required="false";
			fun_code.setValue("");
			fun_code.setEnabled(false);
		}else if(input_type.value=='1'){
			fun_code.setEnabled(true);
			fun_code.required="true";
			fun_code.selectItem(0);
		}
		changeFun();
		showDetailFlag();
	}
	
	function changeExpect(){
		unit_id.required="false";
		unit_id.setValue("");	
		unit_id.setEnabled(false);
		if(is_expect.value=="1"){
			if(input_type.value=='2'){
				unit_id.setEnabled(true);
				unit_id.required="true";
				unit_id.selectItem(0);
			}else if(input_type.value=='1'){
				if(isExpectSet=='1'){
					unit_id.setEnabled(true);
					unit_id.required="true";
					unit_id.selectItem(0);
				}
			}
		}
		showDetailFlag();
	}
	
	function showDetailFlag(){
		detailFlag=false;
		postFun();
		if(is_expect.value=="1"){
			if(input_type.value=='2'){
				detailFlag=true;
			}else if(input_type.value=='1'){
				if(isExpectSet=='1'){
					detailFlag=true;
				}
			}
		}
		if(detailFlag){
			if(editDiv.style.display=='none')editDiv.style.display="block";
			unit_id.setEnabled(true);
			unit_id.required="true";
			var row=detail.GetRowCount() ;
			if(row<= 0 || detail.GetCellData(1,1) == ""){
				detail_init();
			}
		}else {
			unit_id.setEnabled(false);
			unit_id.required="false";
			editDiv.style.display="none";
			detail_init();
		}
	}
	function showMainInput(code){
		if(code=='07' || code=='08'){
			contract_type.para="<dept>"+dept_code.value+"</dept><mod>"+modCode+"</mod>";
			//contract_type.refresh();
			contract_type_td.style.display="";
			contract_type.required="true";;
		}else if(code=='13'){
			type_code.para="<dept>"+dept_code.value+"</dept><dir>"+direction.value+"</dir>";
			//type_code.refresh();
			type_code_td.style.display="";
			type_code.required="true";
		}else if(code=='14'){
			acct_subj.para="<dir>"+direction.value+"</dir>";
			//acct_subj.refresh();
			acct_subj_td.style.display="";
			acct_subj.required="true";
		}
		if(input_type.value=='2'){
			fun_code.required="false";
			fun_code.setValue("");
			fun_code.setEnabled(false);
		}else if(input_type.value=='1'){
			fun_code.setEnabled(true);
			fun_code.required="true";
			is_expect.setEnabled(false);
		}
	}
	
	function postFun(){
		subBody.para="<fun_code>"+fun_code.value+"</fun_code>";
		subBody.load="bankrollForecastBasesetIndex_fun_mod_code";
		subBody.post();
		var p=subBody.getOneDim();
		if(!p){
			modCode='';
			isExpectSet='';
			isExpect='';
			return false;
		}
		modCode=p[0];
		isExpectSet=p[1];
		isExpect=p[2];
		return true;
		
	}
	function saveData(obj){
		
		if(detailFlag){
			var row=detail.GetRowCount() ;
			if(row<= 0 || detail.GetCellData(1,1) == ""){
				alert("明细数据不能为空！");
				return ;
			}
			var sum=0;
			var cell="";
			for(var i=1;i<=row;i++){
				cell=detail.GetCellData(i,2);
				if( detail.GetCellData(i,1))sum+=parseFloat(cell);
				if(i>1){
					if(parseInt(detail.GetCellData(i,1)) <= parseInt(detail.GetCellData(i-1,1))){
						alert("第"+i+"行明细间隔期必须大于前一行间隔期");
						return;
					}
				}
			}
			if(sum!=100.00){
				alert("结算比例（%）之和必须等于100.00");
				return ;
			}
		}	
		var a=multi.submit(obj,'true');
		if(a){
			update_type.value="update";
			tar_code.readOnly=true;
		}
	}

	function detail_init(){
			detail.StartInit();
			detail.HaveFirstSelCol = true;
			detail.isDataAutoSubmit = false;
			detail.TableEditEnable = true;
			detail.VerifyStartValue=true;
			detail.AllowAddNewRow=true;
			detail.MDAddNewRow=true;
			detail.AllowUserResize=true;
			detail.SetHeaderFromXML(creatActiveXHeadXML("间隔期,结算比例（%）,现金比例（%）"));
			detail.SetTableColumn(1,"type=number;edit=true;verify=true;align=left; required=true;format=0;width=200;");//间隔期
			detail.SetTableColumn(2,"type=number;edit=true;verify=true;align=left; required=true;format=0.00;width=200;");//结算比例
			detail.SetTableColumn(3,"type=number;edit=true;verify=true; required=true;align=left;format=0.00;width=200;");//现金比例
			
			
			detail.EndInit();
	}
	function CheckGridValue(TableID, RowIndex, ColIndex, CellValue){
		var res='1',msg='';
			
		if(ColIndex>1){
			if(parseFloat(CellValue)<=0 || parseFloat(CellValue)>100){
				res='0';
				msg="百分比例必须大于0且小于等于100";
			}
		}else  {
			if(parseInt(CellValue)<=0){
				res='0';
				msg="间隔期必须大于0";
			}
		}
		detail.SetVerifyResult(res,msg);
		if(ColIndex==2 ){
			if(detail.GetCellData(RowIndex,3)=='')
			detail.SetCellData(RowIndex,3,'100.00');
		}
	}


	function DoPageDataAction(TableID, RowIndex, UpdateType, sPkValue){
			detail.DoPageResultData(RowIndex, UpdateType, "")
	}