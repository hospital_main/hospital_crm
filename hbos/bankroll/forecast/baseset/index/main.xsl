<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th width='25'><input type='checkbox'/></th>
			<th  noWrap="true">指标编码 </th>
			<th noWrap="true">指标名称 </th>
			<th noWrap="true">资金方向 </th>
			<th noWrap="true">职能科室 </th>
			<th noWrap="true">数据方式 </th>
			<th noWrap="true">取数业务 </th>
			<th noWrap="true">现金流量项目 </th>
			<th noWrap="true">结算预期 </th>
			<th noWrap="true">间隔单位 </th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					
					<tr>
 						<td align='center'  style=''>
            					<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              				<xsl:attribute name="value" >&lt;tar_code&gt;<xsl:value-of select="pk/tar_code"/>&lt;/tar_code&gt;</xsl:attribute>
    			  			</input>  
          			</td>
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td  noWrap='true' >
							  	<a tabindex="-1"><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
						</xsl:choose>
					</xsl:for-each></tr>
				</xsl:for-each></tbody>
		</xsl:template>
	
	</xsl:stylesheet>