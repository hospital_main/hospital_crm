<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td) - 1"/>
		<root>
		<thead>
			<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  	</tr>		 
			<tr noWrap='true' class='mainHead'>
				<td  noWrap="true">指标编码 </td>
			<td noWrap="true">指标名称 </td>
			<td noWrap="true">资金方向 </td>
			<td noWrap="true">职能科室 </td>
			<td noWrap="true">数据方式 </td>
			<td noWrap="true">取数业务 </td>
			<td noWrap="true">现金流量项目 </td>
			<td noWrap="true">结算预期 </td>
			<td noWrap="true">间隔单位 </td>
				</tr>
		</thead>
		<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td><xsl:value-of select="."></xsl:value-of></td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>