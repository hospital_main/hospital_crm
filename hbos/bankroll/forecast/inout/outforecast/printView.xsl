<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-5"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap="true" class="mainHead">
				<td>职能科室</td>
				<td>预测指标</td>
				<td>单据编码</td>
				<td>单据日期</td>
				<td>付款日期</td>
				<td>付款金额</td>
				<td>数据类型</td>
			</tr>
	  	</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=6">
							<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:when test="position() >=8">
							<td align='right'></td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
					
				</xsl:for-each>
				</tr>
			</xsl:for-each>
			 <xsl:if test="count(/root/tbody/tr)>0">
	   		<tr>
	   			<td align="left">合计</td>
	   			<td align="left"></td>
	   			<td align="left"></td> 
	   			<td align="left"></td>
	   			<td align="left"></td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/></td>
	   			
	   		</tr>
   		</xsl:if> 	

			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>