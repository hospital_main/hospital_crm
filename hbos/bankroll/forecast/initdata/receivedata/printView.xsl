<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:template match="/">
		<root>
			<thead>
				
				<tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	
  	  	
  	  </tr>   	
				<tr noWrap='true' class='mainHead'>
				  	<td nowrap='true'>职能科室</td>
				  	<td nowrap='true'>预测指标</td>
				  	<td nowrap='true'>编制方式</td>
				  	<td nowrap='true'>单据编码</td>
				  	<td nowrap='true'>单据日期</td>
				  	<td nowrap='true'>单据金额</td>
				  	<td nowrap='true'>应收金额</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td[position()=1 or position()=3 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 ]">							
							<xsl:choose>
								<xsl:when test="position()=1 and ../td[1]='合 计'">
								<td  align = 'left'><xsl:value-of select="../td[1]"/></td>
							</xsl:when>	
								<xsl:when test="position()=6 or position()=7">
								<td align="right"><xsl:value-of select="."/></td>
							</xsl:when>	
							<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
							</xsl:choose>							
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
