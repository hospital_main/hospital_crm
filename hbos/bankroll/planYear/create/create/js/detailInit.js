

function detailInit_other(){
		
		flag_isEdit = (All_isOperate == true ?  flag_isEdit: false);
		flag_isAdd = (All_isOperate == true ?  flag_isAdd: false);
		
		if(isFromBudgApproval){
			flag_isEdit = true;
			flag_isAdd = false;
		}
	
		detail.StartInit();
		detail.HaveFirstSelCol = true;
		detail.isDataAutoSubmit = false;
		detail.TableEditEnable = flag_isEdit//flag_isEdit;
		detail.VerifyStartValue=true;
		detail.AllowAddNewRow=flag_isAdd;

		detail.SetHeaderFromXML(creatActiveXHeadXML("资金项目,,预算金额,审批金额,备注"));
		
		detail.SetTableColumn(1,"type=py;header=编码,名称;sqlid=bankrollPlanCreateFundType_select;param=<a>" + fund_type.value  + "</a>;edit=verify;verify=true;align=left;width=100;required=true");//资金项目名称
		detail.SetTableColumn(2,"type=text;edit=true;align=left;width=0;");//资金项目编码(隐藏)
		
		detail.SetTableColumn(3,"type=number;edit=verify;verify=true;format=0,000.00;width=100;required=true");//预算金额
		
		if(isFromBudgApproval){
			detail.SetTableColumn(4,"type=number;edit=verify;verify=true;format=0,000.00;width=100;");	//审批金额
		}else{
			detail.SetTableColumn(4,"type=number;edit=false;format=0,000.00;width=100;");	//审批金额
		}
		
		detail.SetTableColumn(5,"type=text;edit=verify;verify=true;align=left;width=100;");//备注
		detail.MDAddNewRow=true;
		detail.AllowUserResize=true;

		detail.EndInit();
}