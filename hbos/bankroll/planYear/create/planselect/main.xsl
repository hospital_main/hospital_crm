<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th >预算编号 </th>
			<th noWrap="true">编制日期 </th>
			<th noWrap="true">编制部门 </th>
			<th noWrap="true">资金类别 </th>
			<th noWrap="true">状态 </th>
			<th noWrap="true">性质 </th>
			<th noWrap="true">预算说明 </th>
			<th noWrap="true">资金项目 </th>
			<th noWrap="true">预算金额 </th>
			<th noWrap="true">审批金额 </th>	
			<th noWrap="true">备注</th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					
				<tr>
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=1 and ../td[1]!='合计'">
								<td  noWrap='true' >
							  	<a tabindex="-1">
							    <xsl:attribute name="href" >
							      javascript:view('<xsl:value-of select="../pk/budg_id"/>','<xsl:value-of select="../td[6]"/>')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:when test="position()=9 or position()=10 ">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
						</xsl:choose>
					</xsl:for-each></tr>
				</xsl:for-each></tbody>
		</xsl:template>
	
	</xsl:stylesheet>