//业务处理
//手动选择
var g_isHasDetail = false;
var d_fund_item_code = "";
var d_fund_item_name = "";

function setAdjustDetail(tmp_budg_id){
	
	var srcTree = new window.ActiveXObject("Microsoft.XMLDOM");
	srcTree.async=false;
	window.xmlhttp.post("bankrollBudgCreateAdjustBudgDetail_select", "<budg_id>"+tmp_budg_id+"</budg_id>", "?isCheck=false");	
	
	srcTree.load(xmlhttp._object.responseXml);
	var trs = srcTree.getElementsByTagName("tr");
	
	if(trs.length > 0){
				detail_init();
			var detail_para = new Array();
			for(var i = 0; i < trs.length; i++){
				
				detail_para[0] = trs.item(i).getElementsByTagName("td").item(0).text;
				detail_para[1] = trs.item(i).getElementsByTagName("td").item(1).text;
				detail_para[2] = trs.item(i).getElementsByTagName("td").item(3).text;
				detail_para[3] = 0;
				detail_para[4] = 0;
				detail_para[5] = trs.item(i).getElementsByTagName("td").item(4).text;
				detail_para[6] = trs.item(i).getElementsByTagName("td").item(5).text;
				
				
				addToDetail(detail_para);	
			}	
			
			if(checkIsHasDetail()){
					detail.TableEditEnable = true;
					detail.AllowAddNewRow=true;
					setDetailCheckBox();
					
					
			}
	}	
}
//从预算中取的不可使用，添加的可以使用
function setDetailCheckBox(){
		for(var i=1;i<=detail.GetRowCount();i++){
			
			if(detail.GetCellData(i,7) != "" && detail.GetCellData(i,7) != "0"){
				detail.CheckBoxEnable(i,false);
			}
		}	
}


function addToDetail(detail_para){
		
		//资金项目,,预算金额,审批金额,备注,明细ID	
		var str_xml = "<tr>";
		str_xml += ("<td>"+ detail_para[0] +"</td>"); //资金项目名称
		str_xml += ("<td>"+ detail_para[1] +"</td>"); //资金项目编码
		str_xml += ("<td>"+ detail_para[2] +"</td>"); 
		str_xml += ("<td>"+ detail_para[3] +"</td>"); 
		str_xml += ("<td>"+ detail_para[4] +"</td>"); 
		str_xml += ("<td>"+ detail_para[5] +"</td>"); 
		str_xml += ("<td>"+ detail_para[6] +"</td>"); 
				
		str_xml += "</tr>"; 
		detail.AddTableRowData(str_xml);
		
		str_xml = ""; 
}

function insertData(obj){
		//query.submit(obj);
		//保存前校验数据
		if(detail.GetRowCount() <= 0 || detail.GetCellData(1,1) == ""){
			alert("明细数据不能为空！");
			return ;
		}
		
		if(multi.submit(obj,'true')){
					
					var strXml = window.xmlhttp._object.responseXml;
					var temp_adjust_id = jQuery(strXml).find("mdctn_main_pk").eq(0).find("adjust_id:first").text();
					var temp_adjust_code = jQuery(strXml).find("mdctn_main_pk").eq(0).find("adjust_code:first").text();
				
					adjust_id.value = temp_adjust_id;
					adjust_code.value = temp_adjust_code;

		}
}

function saveData(obj){
		if(detail.GetRowCount() <= 0 || detail.GetCellData(1,1) == ""){
			alert("明细数据不能为空！");
			return ;
		}
		
		multi.submit(obj,'true');
}

function checkIsHasDetail(){
		if(detail.GetRowCount() <= 0 || detail.GetCellData(1,1) == ""){
			
			return false;
		}
		return true;
		
}

 			var printDs={};
		  var flagHead = 0;
		  function printNew(){
  			if(adjust_code.value =='系统自动生成'){
					alert('请先保存数据后再打印！');
					return false;
		  	}
    		printDs.begin=function(){
	          flagHead = 0
	          flag=0;
	          trs_t=print_post();
		    };
				printDs.getNextHeadData=getNextHeadData;
				printDs.getNextRowData=getNextRowData;
				printDs.end=function(){};
				printCellByTemplate("bankrollBudgCreateAdjust_totalPrintPreview",printDs);//在\ROOT\base\scripts\sub.js 中打开窗口 
    	}

	//获得表头、表尾数据
	  function getNextHeadData(){ 
			if(flagHead > 0) 
				return null;
			flagHead++;
			subBody.load = 'bankrollBudgCreateAdjust_getPrintHead'
			subBody.para = '<a>'+adjust_id.value+'</a>'
			subBody.post();
			return subBody.getOneDim()
	  }
	  //获得表体数据
	  function getNextRowData(){
	  	var a = new Array();
	  	if(flag>=trs_t.length)
	  	  return null;
	  	var tds = null;
	  	tds=trs_t[flag].getElementsByTagName("td");
	  	for(var j=0;j<tds.length;j++){
	  			a.push(tds[j].text);
	  	}
	  	flag++;
	  	return a;
	  }
	  function print_post() {
	  	window.xmlhttp.post("bankrollBudgCreateAdjust_getPrintBody","<a>"+adjust_id.value+"</a>", "?isCheck=false");
			var resXml=window.xmlhttp._object.responseXML;		
			var trs=resXml.getElementsByTagName("tr");
			return trs;
	  }

 