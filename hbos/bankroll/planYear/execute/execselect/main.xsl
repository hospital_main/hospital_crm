<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				
				<th>执行编号</th>
				<th>编制日期</th>
				<th>编制部门</th>
				<th>资金类别</th>
				<th>状态</th>
				<!--<th>计划日期</th>
				<th>执行日期</th>-->
				<th>业务部门</th>		
				<th>资金项目</th>
				<th>辅助项</th>
				<th>业务编码</th>
				<th>计划金额</th>
				<th>本月执行</th>
				<th>累计执行</th>
				<th>差异</th>
				<th>差异率(%)</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td>
										<xsl:value-of select="."/>
									</td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openUpdatePage('<xsl:value-of select="../pk/exec_id"/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								
								<xsl:when test="position() =10 or position()=11 or position()=12 or position()=13" >
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								
								<xsl:when test="position()=14" >
										<td align='right'>
											<xsl:value-of select="." />
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
