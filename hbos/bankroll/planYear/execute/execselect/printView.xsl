<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap="true" class="mainHead">
				<td>执行编号</td>
				<td>编制日期</td>
				<td>编制部门</td>
				<td>资金类别</td>
				<td>状态</td>
				<!--<td>计划日期</td>
				<td>执行日期</td>-->
				<td>业务部门</td>		
				<td>资金项目</td>
				<td>辅助项</td>
				<td>业务编码</td>
				<td>计划金额</td>
				<td>本月执行</td>
				<td>累计执行</td>
				<td>差异</td>
				<td>差异率(%)</td>
			</tr>
	  	</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td>
										<xsl:value-of select="."/>
									</td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openUpdatePage('<xsl:value-of select="../pk/exec_id"/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() =10 or position()=11 or position()=12 or position()=13" >
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=14" >
										<td align='right'>
											<xsl:value-of select="." />
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>