<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="checkcode" select="/root/tbody/tr/td[8]"/>
      <tr noWrap="true" class="mainHead">
      	<th>业务部门</th>
      	<th>资金项目</th>
      	<xsl:if test="$checkcode != '' ">
					<th>
						<xsl:value-of select="$checkcode"/>  	
					</th>
				</xsl:if>
				<xsl:if test="$checkcode = '' ">
					<th style="display:none">
						
					</th>
				</xsl:if>
      	<th>计划金额</th>
      	<th>执行金额</th>
      	<th>差异</th>
      	<th>执行进度(%)</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=3">
            		<xsl:if test="$checkcode != '' ">
									<td>
										<xsl:value-of select="."/>  	
									</td>
								</xsl:if>
								<xsl:if test="$checkcode = '' ">
									<td style="display:none">
									</td>
								</xsl:if>
              </xsl:when>
              <xsl:when test="position()=4 or position()=5 or position()=6 or position()=7">
                <td align='right' class='moneyCol'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	              </td>
              </xsl:when>
              <xsl:when test="position()=8">
                <td style="display:none">
	                <xsl:value-of select="."/> 
	              </td>
              </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  	      </xsl:for-each>
  	    </tr>
      </xsl:for-each>  	
    </tbody>
  </xsl:template>
</xsl:stylesheet>