<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>单据编码</th>
				<th nowrap='true'>制单日期</th>
				<th nowrap='true'>投资单位</th>
				<th nowrap='true'>投资类别</th>
				<th nowrap='true'>币种</th>
				<th nowrap='true'>汇率</th>
				<th nowrap='true'>投资金额</th>
				<th nowrap='true'>本币金额</th>
				<th noWrap="true">起息日期</th>
				<th noWrap="true">到期日期</th>
				<th noWrap="true">利率方式</th>
				<th noWrap="true">利率</th>
				<th noWrap="true">利息金额</th>
				<th noWrap="true">利息本币</th>
				<th noWrap="true">剩余天数</th>
      </tr>
     
    </thead>
    <tbody>
    	
   		<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td><xsl:value-of select="."/></td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										
										<a href='#'><xsl:attribute name='onclick'>openDialog('../../../bankroll/invest/management/bill/update.html?load=&lt;invest_id&gt;<xsl:value-of select='../pk/invest_id' />&lt;/invest_id&gt;&lt;other&gt;1&lt;/other&gt;', 'dialogWidth:950px;dialogHeight:340px')</xsl:attribute><xsl:value-of select="." /></a>
	
									</td>
									</xsl:if>
								</xsl:when>
							<xsl:when test="(position()=6) and .!=''">
									<td align='right'>
										<xsl:value-of select="format-number(.,'###,##0.0000000000')" />
									</td>
								</xsl:when>
								
								<xsl:when test="position() &gt; 6 and position() &lt; 9">
									<td align='right'>
										<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=12 " >
									<td align='right'>
										<xsl:value-of select="." />
									</td>
								</xsl:when>
								<xsl:when test="position()=13 and ../td[1]!='合计'">
									<td align='right'>
										<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=13 and ../td[1]='合计'">
									<td align='right'></td>
								</xsl:when>
								<xsl:when test="position()=14 and ../td[1]!='合计'">
									<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
										<xsl:when test="position()=14 and ../td[1]='合计'">
										<td align='right'></td>
								</xsl:when>
								<xsl:when test="position()=15 and ../td[1]!='合计'">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=15 and ../td[1]='合计'">
										<td align='right'></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>        
    </tbody>
  </xsl:template>
</xsl:stylesheet>

