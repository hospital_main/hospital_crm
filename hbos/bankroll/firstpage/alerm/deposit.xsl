<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>单据编号</th>
				<th>存款日期</th>
				<th>开户银行</th>
				<th>定期账号</th>
				<th>币种</th>
				<th>汇率</th>
				<th>存款金额</th>
				<th>本币金额</th>	
				<th>起息日期</th>
				<th>利率方式</th>
				<th>天数</th>
				<th>利率</th>
				<th>存期</th>
				<th>到期日期</th>
				<th>利息金额</th>
				<th>利息本币</th>
				<th>剩余天数</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td>
										<xsl:value-of select="."/>
									</td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											<xsl:attribute name='onclick'>
openDialog('../../regular/init/inslip/update.html?load=&lt;is_insert&gt;2&lt;/is_insert&gt;&lt;deposit_id&gt;<xsl:value-of select='../pk/deposit_id'/>&lt;/deposit_id&gt;', 'dialogWidth:650px;dialogHeight:470px')

											</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=7 or position()=8 or position()=15 or position()=16" >
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="(position()=6) and ../td[1]!='合计'" >
									<td align='right'>
										<xsl:value-of select="format-number(.,'###,##0.0000000000')" />
									</td>
								</xsl:when>
								<xsl:when test="(position()=6) and ../td[1]='合计'" >
									<td align='right'>
									</td>
								</xsl:when>
								<xsl:when test="position()=11  or position()=12 or position()=17" >
									<td align='right'>
										<xsl:value-of select="." />
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
