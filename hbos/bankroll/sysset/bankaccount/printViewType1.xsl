<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
	  		<tr noWrap="true" class="mainHead">
					<td>账号编码</td>
					<td>账号名称</td>
					<td>开户银行</td>
				 	<td>开户日期</td>
				 	<td>期初结息日</td>
				 	<td>账号科目</td>
				 	<td>币种</td>
				 	<td>期初积数</td>
				 	<td>利率方式</td>
				 	<td>天数</td>
				 	<td>利率</td>
				 	<td>是否协定</td>
				 	<td>基本存款</td>
				 	<td>协定利率方式</td>
				 	<td>协定天数</td>
				 	<td>协定利率</td>
					<td>是否销户</td>
				</tr>
	  	</thead>
	  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1 and .!='合计'">
                <td>
                	<xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position()=8">
                <td align="right"><xsl:value-of select="format-number(.,'###,##0.00')"/></td>
              </xsl:when>
              <xsl:when test="position()=10">
                <td align="right"><xsl:value-of select="format-number(.,'###0')"/></td>
              </xsl:when>
              <xsl:when test="position()=11">
                <td align="right"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position()=13 and ../td[1]!='合计'">
                <td align="right"><xsl:value-of select="format-number(.,'###0.00')"/></td>
              </xsl:when>
              <xsl:when test="position()=13 and ../td[1]='合计'">
                <td align="right"></td>
              </xsl:when>
              <xsl:when test="position()=15">
                <td align="right"><xsl:value-of select="format-number(.,'###0')"/></td>
              </xsl:when>
              <xsl:when test="position()=16">
                <td align="right"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>