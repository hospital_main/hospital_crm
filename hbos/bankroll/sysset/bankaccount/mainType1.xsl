<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
                           
				<th>账号编码</th>
				<th>账号名称</th>
				<th>开户银行</th>
			 	<th>开户日期</th>
			 	<th>期初结息日</th>
			 	<th>账号科目</th>
			 	<th>币种</th>
			 	<th>期初积数</th>
			 	<th>利率方式</th>
			 	<th>天数</th>
			 	<th>利率</th>
			 	<th>是否协定</th>
			 	<th>基本存款</th>
			 	<th>协定利率方式</th>
			 	<th>协定天数</th>
			 	<th>协定利率</th>
				<th>是否销户</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:if test='td[1]="合计"'>
        		<td></td>
        	</xsl:if>
        	<xsl:if test='td[1]!="合计"'>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          </xsl:if>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1 and .!='合计'">
                <td><a href='#'>
                	<xsl:attribute name='onclick'>
                		openDialog('updateType1.html?&lt;account_code&gt;<xsl:value-of select="."/>&lt;/account_code&gt;','dialogWidth:700px;dialogHeight:400px', result)
                	</xsl:attribute>
                	<xsl:value-of select="."/></a></td>
              </xsl:when>
              <xsl:when test="position()=8">
                <td align="right"><xsl:value-of select="format-number(.,'###,##0.00')"/></td>
              </xsl:when>
              <xsl:when test="position()=10">
                <td align="right"><xsl:value-of select="format-number(.,'###0')"/></td>
              </xsl:when>
              <xsl:when test="position()=11">
                <td align="right"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position()=13 and ../td[1]!='合计'">
                <td align="right"><xsl:value-of select="format-number(.,'###0.00')"/></td>
              </xsl:when>
              <xsl:when test="position()=13 and ../td[1]='合计'">
                <td align="right"></td>
              </xsl:when>
              <xsl:when test="position()=15">
                <td align="right"><xsl:value-of select="format-number(.,'###0')"/></td>
              </xsl:when>
              <xsl:when test="position()=16">
                <td align="right"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
