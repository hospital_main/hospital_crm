<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
	  		<tr noWrap="true" class="mainHead">
					<td>类别编码</td>
					<td>类别名称</td>
					<td>资金方向</td>
				 	<td>职能科室</td>
				 	<td>数据方式</td>
				 	<td>计划取数业务</td>
				 	<td>计划执行业务</td>
				 	<td>计划控制方式</td>
				 	<td>结算取数业务</td>
				 	<td>辅助账</td>
				 	<td>现金流量项目</td>
				 	<td>账龄天数</td>
				 	<td>合同类别</td>
				 	<td>自动凭证</td>
				 	<td>凭证方式</td>
				</tr>
	  	</thead>
	  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position()=12">
                <td align="right"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>