<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th>类别编码</th>
				<th>类别名称</th>
				<th>资金方向</th>
			 	<th>职能科室</th>
			 	<th>数据方式</th>
			 	<th>计划取数业务</th>
			 	<th>计划执行业务</th>
			 	<th>计划控制方式</th>
			 	<th>结算取数业务</th>
			 	<th>辅助账</th>
			 	<th>现金流量项目</th>
			 	<th>账龄天数</th>
			 	<th>合同类别</th>
			 	<th>自动凭证</th>
			 	<th>凭证方式</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
              </xsl:when>
              <xsl:when test="position()=12">
                <td align="right"><xsl:value-of select="format-number(.,'###,##0')"/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
