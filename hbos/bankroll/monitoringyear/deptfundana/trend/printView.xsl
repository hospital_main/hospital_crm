<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		
      	</tr>
			<tr noWrap="true" class="mainHead">
				<td >类别编码</td>
				<td >类别名称</td>
				<td >主管部门</td>
				<td >年度预算</td>
				<td >预算占比（%）</td>
				<td >月份累计计划</td>
				<td >月份占比（%）</td>
				<td >差异</td>
				<td >执行进度（%）</td>
				
			</tr>
			
		</thead>
		<tbody>
		<xsl:for-each select="/root/tbody/tr">
				
				<tr>
					<xsl:for-each select="./td">
						<xsl:choose>
							
							<xsl:when test="(../td[2]!='流入合计' and ../td[2]!='流出合计' and ../td[2]!='净流量' and (position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9))">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
							</xsl:when>
							<xsl:when test="(../td[2]='流入合计' or ../td[2]='流出合计' or ../td[2]='净流量') and (position()=5 or position()=7 ) ">
								<td align='right'></td>
							</xsl:when>
							<xsl:when test="(../td[2]='流入合计' or ../td[2]='流出合计' or ../td[2]='净流量') and (position()=4 or position()=6 or position()=8 or position()=9) ">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
						</xsl:choose>
					</xsl:for-each></tr>
				</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
