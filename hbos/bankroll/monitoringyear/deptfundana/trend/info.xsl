<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th >资金项目 </th>
			<th noWrap="true">预算金额 </th>
			<th noWrap="true">年度占比（%） </th>
			<th noWrap="true">月份累计计划  </th>
			<th noWrap="true">月份占比（%） </th>
			<th noWrap="true">差异 </th>
			<th noWrap="true">执行进度（%） </th>	
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
				
				<tr>
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=2 or position()=3 or position()=4 or position()=5 or position()=6 or position()=7">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
							</xsl:when>
							
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
						</xsl:choose>
					</xsl:for-each></tr>
				</xsl:for-each></tbody>
		</xsl:template>
	
	</xsl:stylesheet>