<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th >类别编码 </th>
			<th noWrap="true">类别名称 </th>
			<th noWrap="true">主管部门 </th>
			<th noWrap="true">年度预算 </th>
			<th noWrap="true">预算占比（%） </th>
			<th noWrap="true">月份累计计划  </th>
			<th noWrap="true">月份占比（%） </th>
			<th noWrap="true">差异 </th>
			<th noWrap="true">执行进度（%） </th>	
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
				
				<tr>
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td  noWrap='true' >
							  	<a tabindex="-1">
							    <xsl:attribute name="href" >
							      javascript:view('<xsl:value-of select="."/>')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:when test="../td[2]='流入合计' and position()=4">
								<td align='right'><a>
								<xsl:attribute name="href" >
							      javascript:openGraph(true,4);
							    </xsl:attribute><xsl:value-of select="format-number(.,'###,##0.00')" /></a></td>
							</xsl:when>
							<xsl:when test="../td[2]='流出合计' and position()=4">
								<td align='right'><a>
								<xsl:attribute name="href" >
							      javascript:openGraph(false,4);
							    </xsl:attribute><xsl:value-of select="format-number(.,'###,##0.00')" /></a></td>
							</xsl:when>
							<xsl:when test="../td[2]='流入合计' and position()=6">
								<td align='right'><a>
								<xsl:attribute name="href" >
							      javascript:openGraph(true,6);
							    </xsl:attribute><xsl:value-of select="format-number(.,'###,##0.00')" /></a></td>
							</xsl:when>
							<xsl:when test="../td[2]='流出合计' and position()=6">
								<td align='right'><a>
								<xsl:attribute name="href" >
							      javascript:openGraph(false,6);
							    </xsl:attribute><xsl:value-of select="format-number(.,'###,##0.00')" /></a></td>
							</xsl:when>
							<xsl:when test="(../td[2]!='流入合计' and ../td[2]!='流出合计' and ../td[2]!='净流量' and (position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9))">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
							</xsl:when>
							<xsl:when test="(../td[2]='流入合计' or ../td[2]='流出合计' or ../td[2]='净流量') and (position()=5 or position()=7 ) ">
								<td align='right'></td>
							</xsl:when>
							<xsl:when test="(../td[2]='流入合计' or ../td[2]='流出合计' or ../td[2]='净流量') and (position()=4 or position()=6 or position()=8 or position()=9) ">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
						</xsl:choose>
					</xsl:for-each></tr>
				</xsl:for-each>
</tbody>
		</xsl:template>
	
	</xsl:stylesheet>