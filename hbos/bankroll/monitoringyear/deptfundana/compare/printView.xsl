<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		
      	</tr>
			<tr noWrap="true" class="mainHead">
				<td >项目</td>
				<td >年度预算</td>
				<td >月份累计计划</td>
				<td >差异</td>
				<td >差异率（%）</td>
				<td >执行占比（%）</td>
			</tr>
		</thead>
		<tbody>
		<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()&gt; 1">
									<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
								<xsl:when test="position() = 5 and ../td[2] = 0.00 and ../td[4] != '0.00'">
									<td align='right'></td>
								</xsl:when>
								<xsl:when test="position() = 6 and ../td[2] = 0.00 and ../td[3] != '0.00'">
									<td align='right'></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
