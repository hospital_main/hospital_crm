<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				
				<th>类别编码</th>
				<th>类别名称</th>
				<th>主管部门</th>
				<th>年度预算</th>
				<th>预算占比(%)</th>
				<th>累计执行</th>
				<th>执行占比(%)</th>
				<th>差异</th>		
				<th>执行进度(%)</th>
				<th style="display:none">flag</th>
				<th style="display:none">dire</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test="../td[10] = '1'">
										<td>
										<xsl:value-of select="."/>
									</td>
									</xsl:if>
									<xsl:if test="../td[10] = '0'">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openUpdatePage('<xsl:value-of select="."/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=4">
									<xsl:if test="../td[10] = '0' or ../td[11] = '3'">
										<td align="right">
											<xsl:value-of select="format-number(.,'###,##0.00')" />
										</td>
									</xsl:if>
									<xsl:if test="../td[10] = '1' and ../td[11] != '3'">
										<td align="right">
										<a href="#">
											<xsl:attribute name="onclick" >
                					show('<xsl:value-of select="position()" />','<xsl:value-of select="../td[11]" />')
											</xsl:attribute>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
										</a>
										</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=6">
									<xsl:if test="../td[10] = '0' or ../td[11] = '3'">
										<td align="right">
											<xsl:value-of select="format-number(.,'###,##0.00')"/>
										</td>
									</xsl:if>
									<xsl:if test="../td[10] = '1' and ../td[11] != '3'">
										<td align="right">
										<a href="#">
											<xsl:attribute name="onclick" >
											 	show('<xsl:value-of select="position()" />','<xsl:value-of select="../td[11]" />')
											</xsl:attribute>
											<xsl:value-of select="format-number(.,'###,##0.00')"/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=5 or position()=7 or position() =8 or position() =9" >
									<td align='right'>
										<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=10 or position() =11" >
									<td style="display:none">
										<xsl:value-of select="." />
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
