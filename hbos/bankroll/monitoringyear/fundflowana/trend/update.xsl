<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="checkcode" select="/root/tbody/tr/td[8]"/>
      <tr noWrap="true" class="mainHead">
      	
      	<th>资金项目</th>
      	<th>年度预算</th>
      	<th>执行金额</th>
      	<th>差异</th>
      	<th>执行进度(%)</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=2 or position()=3 or position()=4 or position()=5">
                <td align='right' class='moneyCol'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	              </td>
              </xsl:when>
             
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  	      </xsl:for-each>
  	    </tr>
      </xsl:for-each>  	
    </tbody>
  </xsl:template>
</xsl:stylesheet>