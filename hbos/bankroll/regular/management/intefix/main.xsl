<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>单据编码</th>
				<th>单据日期</th>
				<th>开户银行</th>
				<th>定期账号</th>
				<th>定期存单</th>
				
				<th>定期取款</th>
				
				<th>起息日期</th>
				<th>截止日期</th>
				<th>币种</th>
				<th>计息本金</th>
				<th>计息本币</th>	
					
				<th>定期利率方式</th>
				<th>定期利率</th>
				<th>定期天数</th>
				<th>定期利息金额</th>
				<th>定期利息本币</th>
				
				<th>活期利率方式</th>
				<th>活期天数</th>
				<th>活期利率</th>
				<th>活期利息金额</th>
				<th>活期利息本币</th>
				
				<th>利息摘要</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>状态</th>
				<th>凭证</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:if test="td[1] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
        	</xsl:if>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td>
										<xsl:value-of select="."/>
									</td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openInterestPage('<xsl:value-of select="../pk/interest_id"/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=5" >
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
				      					openDepositPage('<xsl:value-of select="../pk/deposit_id"/>')
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=6" >
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
				      					openWithdrawPage('<xsl:value-of select="../pk/withdraw_id"/>')
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=10 or position()=11" >
									<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=15 or position()=16 or position()=20 or position()=21">
									<xsl:if test="../td[1] = '合计'">
										<td>
										 
										</td>
									</xsl:if>
									<xsl:if test="../td[1] != '合计'">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
										</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=13 or position()=14 or position()=18 or position()=19 " >
									<xsl:if test="../td[1] = '合计'">
										<td>
										</td>
									</xsl:if>
									<xsl:if test="../td[1] != '合计'">
										<td align='right'>
											<xsl:value-of select="." />
										</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=26">
									<td  noWrap='true' >
								  	<a tabindex="-1">
								    <xsl:attribute name="href" >
								      javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;')
								    </xsl:attribute><xsl:value-of select="."/></a>
								  </td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
