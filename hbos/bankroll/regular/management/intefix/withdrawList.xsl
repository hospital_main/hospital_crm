<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>选择</th>
				<th>单据编码</th>
				<th>单据日期</th>
				<th>开户银行</th>
				<th>定期账号</th>
				<th>币种</th>
				<th>取款金额</th>
				<th>本币金额</th>
				<th style="display:none">withdraw_id</th>
				<th style="display:none">deposit_id</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'>
	  	  			<input type='radio' name='select_withdraw_id'>
	              <xsl:attribute name="value" ><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
	      			</input>
				   </td>
					<xsl:for-each select="td">
						<xsl:choose>
							
							<xsl:when test="position()=6 or position()=7">
            		<td align='right'>
									<xsl:value-of select="format-number(.,'###,##0.00')" />
			  				</td>
	            </xsl:when>
	            <xsl:when test="position()>= 8 ">
            		<td style="display:none;">
									<xsl:value-of select="." />
			  				</td>
	            </xsl:when>
						  <xsl:otherwise>
								<td>
		            	<xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
