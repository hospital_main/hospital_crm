<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap="true" class="mainHead">
				<td>单据编码</td>
				<td>单据日期</td>
				<td>开户银行</td>
				<td>定期账号</td>
				<td>定期存单</td>
				<td>定期取款</td>
				
				<td>起息日期</td>
				<td>截止日期</td>
				<td>币种</td>
				<td>计息本金</td>
				<td>计息本币</td>	
					
				<td>定期利率方式</td>
				<td>定期利率</td>
				<td>定期天数</td>
				<td>定期利息金额</td>
				<td>定期利息本币</td>
				
				<td>活期利率方式</td>
				<td>活期天数</td>
				<td>活期利率</td>
				<td>活期利息金额</td>
				<td>活期利息本币</td>
				
				<td>利息摘要</td>
				<td>制单人</td>
				<td>审核人</td>
				<td>状态</td>
				<td>凭证</td>
			</tr>
	  	</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td>
										<xsl:value-of select="."/>
									</td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openInterestPage('<xsl:value-of select="../pk/interest_id"/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=5" >
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
				      					openDepositPage('<xsl:value-of select="../pk/deposit_id"/>')
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=6" >
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
				      					openWithdrawPage('<xsl:value-of select="../pk/withdraw_id"/>')
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=10 or position()=11" >
									<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=15 or position()=16 or position()=20 or position()=21">
									<xsl:if test="../td[1] = '合计'">
										<td>
										 
										</td>
									</xsl:if>
									<xsl:if test="../td[1] != '合计'">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
										</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=13 or position()=14 or position()=18 or position()=19 " >
									<xsl:if test="../td[1] = '合计'">
										<td>
										</td>
									</xsl:if>
									<xsl:if test="../td[1] != '合计'">
										<td align='right'>
											<xsl:value-of select="." />
										</td>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>