<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>业务日期</th>
				<th>单据编号</th>
				<th>币种</th>
				<th>存款金额</th>
				<th>取本金额</th>	
				<th>利息金额</th>
				<th>余额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=2">
									<xsl:if test="contains(.,'DQCK')">
										<td>
											<a href="#">
												<xsl:attribute name="onclick" >
	                				openDepositPage('<xsl:value-of select="../pk/deposit_id"/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</td>
									</xsl:if>
									<xsl:if test="contains(.,'DQQK')">
										<td>
											<a href="#">
												<xsl:attribute name="onclick" >
	                				openWithdrawPage('<xsl:value-of select="../pk/withdraw_id"/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</td>
									</xsl:if>
									
								</xsl:when>
								<xsl:when test="position()=4" >
									<xsl:if test="contains(../td[2],'DQCK')">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
					  				</td>
									</xsl:if>
									<xsl:if test="contains(../td[2],'DQQK')">
										<td>
										</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=5 or position()=6" >
									<xsl:if test="contains(../td[2],'DQCK')">
										<td>
										</td>
									</xsl:if>
									<xsl:if test="contains(../td[2],'DQQK')">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
					  				</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=7" >
									<td align='right'>
										<xsl:value-of select="format-number(.,'###,##0.00')" />
				  				</td>
								</xsl:when>
								
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
