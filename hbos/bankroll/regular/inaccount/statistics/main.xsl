<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>业务编号</th>
				<th>业务日期</th>
				<th>开户银行</th>
				<th>账户号</th>
				<th>到期日期</th>
				<th>存期</th>
				<th>币种</th>
				<th>存款金额</th>
				<th>取款金额</th>	
				<th>续存转出</th>			
				<th>利息</th>
				<th>余额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:if test="td[1] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
        	</xsl:if>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td>
										<xsl:value-of select="."/>
									</td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openDepositPage('<xsl:value-of select="../pk/deposit_id"/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=6" >
									<td align='right'>
										<xsl:value-of select="." />
									</td>
								</xsl:when>
								<xsl:when test="position()=8 or position()=10 or position()=11 or position()=12" >
									<td align='right'>
										<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=9" >
									<xsl:if test="../td[1] = '合计'">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
										</td>
									</xsl:if>
									<xsl:if test="../td[1] != '合计'">
									<td align='right'>
										<a href="#">
										 	<xsl:attribute name="onclick" >
	            					openWithdrawPage('<xsl:value-of select="../pk/deposit_id"/>')
											</xsl:attribute>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
