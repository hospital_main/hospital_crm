<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>单据编号</th>
				<th>单据日期</th>
				<th>开户银行</th>
				<th>账号</th>
				<th>币种</th>
				<th>取款金额</th>
				<th>本币金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=6 or position()=7">
            		<td align='right'>
									<xsl:value-of select="format-number(.,'###,##0.00')" />
			  				</td>
	            </xsl:when>
						  <xsl:otherwise>
								<td>
		            	<xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
