<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap="true" class="mainHead">
				<td>业务编号</td>
				<td>业务日期</td>
				<td>开户银行</td>
				<td>账户号</td>
				<td>到期日期</td>
				<td>存期</td>
				<td>币种</td>
				<td>存款金额</td>
				<td>取款金额</td>	
				<td>续存转出</td>			
				<td>利息</td>
				<td>余额</td>
			</tr>
	  	</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=6" >
									<td align='right'>
										<xsl:value-of select="." />
									</td>
								</xsl:when>
								<xsl:when test="position()=8 or position()=9 or position()=10 or position()=11 or position()=12" >
									<td align='right'>
										<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>