<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap="true" class="mainHead">
				<td>单据编号</td>
				<td>单据日期</td>
				<td>开户银行</td>
				<td>定期账号</td>
				<td>起息日期</td>
				<td>币种</td>
				<td>存款金额</td>
				<td>本币金额</td>	
				<td>利率方式</td>
				<td>天数</td>
				<td>利率</td>	
				<td>存期</td>
				<td>到期日期</td>	
				<td>利息金额</td>
				<td>利息本币</td>	
				<td>经手人</td>
				<td>存款摘要</td>
				<td>存单号</td>
				<td>制单人</td>
				<td>审核人</td>
				<td>状态</td>
				<td>凭证</td>
			</tr>
	  	</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td>
										<xsl:value-of select="."/>
									</td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=7 or position()=8  or position()=14 or position()=15" >
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=9 or position()=10 or position()=11 or position()=12 " >
										<td align='right'>
											<xsl:value-of select="." />
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>