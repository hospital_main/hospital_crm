<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>单据编号</th>
				<th>取款日期</th>
				<th>开户银行</th>
				<th>定期账号</th>
				<th>存款单</th>
				<th>到期日期</th>		
				<th>存期</th>
				<th>币种</th>
				<th>取款金额</th>
				<th>本币金额</th>	
				<th>经手人</th>	
				<th>取款摘要</th>
				<th>状态</th>
				<th>凭证</th>	
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:if test="td[1] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
        	</xsl:if>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td>
										<xsl:value-of select="."/>
									</td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openWithdrawPage('<xsl:value-of select="../pk/withdraw_id"/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
									
								<xsl:when test="position()=5" >
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
				      					openDepositPage('<xsl:value-of select="../pk/deposit_id"/>')
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=7" >
										<td align='right'>
											<xsl:value-of select="." />
									</td>
								</xsl:when>
								<xsl:when test="position()=9 or position()=10" >
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=14">
									<td  noWrap='true' >
								  	<a tabindex="-1">
								    <xsl:attribute name="href" >
								      javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;')
								    </xsl:attribute><xsl:value-of select="."/></a>
								  </td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
