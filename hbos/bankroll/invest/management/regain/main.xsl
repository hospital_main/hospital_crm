<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>单据编号</th>
				<th>单据日期</th>
				<th>投资编号</th>
				<th>投资单位</th>
				<th>职能部门</th>
				<th>收款方式</th>
				<th>票据号码</th>
				<th>收回摘要</th>
				<th>币种</th>			
				<th>汇率</th>
				<th>收回投资</th>
				<th>收回本币</th>
				<th>状态</th>
				<th>制表人</th>
				<th>审核人</th>
				<th>凭证</th>
			</tr>
		</thead>
		<tbody>
		<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:if test="td[1] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">
					<td align="center">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
        	</xsl:if>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td><xsl:value-of select="."/></td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
											 		javascript:openPage('<xsl:value-of select='../pk/return_id' />');
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=3">
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
											 	javascript:openInvestPage('<xsl:value-of select='../pk/invest_id' />');
											 		</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
									</xsl:when>
								<xsl:when test="position()=10">
									<xsl:if test="../td[1]= '合计'">
										<td></td>
									</xsl:if>
									<xsl:if test="../td[1] != '合计'">
												<td><xsl:value-of select="."/></td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=11 or position()=12">
									<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
								<xsl:when test="position()=16">
								<td  noWrap='true' >
							  <a>
							    <xsl:attribute name="href" >
							       javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
