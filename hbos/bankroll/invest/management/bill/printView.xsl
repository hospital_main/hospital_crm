<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      
      	</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap="true">单据编号</td>
				<td nowrap="true">单据日期</td>
				<td nowrap="true">投资类别</td>
				<td nowrap="true">投资单位</td>
				<td nowrap="true">职能部门</td>
				<td nowrap="true">付款方式</td>
				<td nowrap="true">票据号码</td>
				<td nowrap="true">起息日期</td>
				<td nowrap="true">到期日期</td>
				<td nowrap="true">利率方式</td>
				<td nowrap="true">利率</td>
				<td nowrap="true">利息金额</td>
				<td nowrap="true">利息本币</td>
				<td nowrap="true">投资摘要</td>
				<td nowrap="true">币种</td>			
				<td nowrap="true">汇率</td>
				<td nowrap="true">投资总额</td>
				<td nowrap="true">总额本币</td>
				<td nowrap="true">现金总额</td>
				<td nowrap="true">现金本币</td>
				<td nowrap="true">状态</td>
				<td nowrap="true">制表人</td>
				<td nowrap="true">审核人</td>	
				<td nowrap="true">凭证</td>		
		  	
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=16">
									<xsl:if test="../td[1]= '合计'">
										<td></td>
									</xsl:if>
									<xsl:if test="../td[1] != '合计'">
												<td><xsl:value-of select="."/></td>
									</xsl:if>
								</xsl:when>
						<xsl:when test="position()=12 or position()=13 or position()=17 or position()=18 or position()=19 or position()=20">
									<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
		        <xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
				  </xsl:choose>		
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>

