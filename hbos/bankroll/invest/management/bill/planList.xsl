<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th></th>
				<th>计划编号</th>
				<th>编制部门</th>
				<th>编制日期</th>
				<th>业务编号</th>
				<th>计划日期</th>
				<th>业务科室</th>
				<th>资金项目</th>
				<th>投资单位</th>
				<th>计划金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<td align='center'>
	  	  			<input type='radio' name='select_code'>
	              <xsl:attribute name="value" ><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
	      			</input>
				   </td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=9">
	            		<td align='right'>
		                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
				  				</td>
	            </xsl:when>
						 <xsl:otherwise>
								<td>
		                 <xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
