<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:6;fontsize:maintitle'></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					
				</tr>
				<tr noWrap='true' class='mainHead'>
		  		<td noWrap="true">投资类别</td>
					<td noWrap="true">投资单位</td>
					<td noWrap="true">币种</td>
					<td noWrap="true">汇率</td>
					<td noWrap="true">年初余额</td>
					<td noWrap="true">年初本币</td>
					
     		</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">							
							<xsl:choose>
								<xsl:when test="position()=5 or position()=6">
	                <td class='numberText' noWrap='true' align='right'>
	              	  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </td>
	              </xsl:when>
							<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
							</xsl:choose>							
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
