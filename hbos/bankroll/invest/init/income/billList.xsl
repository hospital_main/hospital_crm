<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th></th>
				<th>单据编号</th>
				<th>投资单位</th>
				<th>投资类别</th>
				<th>到期日期</th>
				<th>币种</th>
				<th>投资金额</th>
				<th>投资本币</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<td align='center'>
	  	  			<input type='radio' name='select_code'>
	              <xsl:attribute name="value" ><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
	      			</input>
				   </td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=6 or position()=7">
	            		<td align='right'>
		                 <xsl:value-of select="format-number(.,'#,##0.00')"/>
				  				</td>
	            </xsl:when>
						 <xsl:otherwise>
								<td>
		                 <xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
