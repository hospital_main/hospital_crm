<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap="true">单据编号</td>
				<td nowrap="true">单据日期</td>
				<td nowrap="true">投资编号</td>
				<td nowrap="true">投资单位</td>
				<td nowrap="true">职能部门</td>
				<td nowrap="true">收款方式</td>
				<td nowrap="true">票据号码</td>
				<td nowrap="true">收回摘要</td>
				<td nowrap="true">币种</td>			
				<td nowrap="true">汇率</td>
				<td nowrap="true">收回投资</td>
				<td nowrap="true">收回本币</td>
				<td nowrap="true">状态</td>
				<td nowrap="true">制表人</td>
				<td nowrap="true">审核人</td>
				
			
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=10">
									<xsl:if test="../td[1]= '合计'">
										<td></td>
									</xsl:if>
									<xsl:if test="../td[1] != '合计'">
												<td><xsl:value-of select="."/></td>
									</xsl:if>
								</xsl:when>
						<xsl:when test="position()=11 or position()=12">
							<td align='right'>
	              <xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
	          </xsl:when>
		        <xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
				  </xsl:choose>		
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>

