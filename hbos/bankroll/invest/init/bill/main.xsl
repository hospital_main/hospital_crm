<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>单据编号</th>
				<th>单据日期</th>
				<th>投资类别</th>
				<th>投资单位</th>
				<th>职能部门</th>
				<th>付款方式</th>
				<th>票据号码</th>
				<th>起息日期</th>
				<th>到期日期</th>
				<th>利率方式</th>
				<th>利率</th>
				<th>利息金额</th>
				<th>利息本币</th>
				<th>投资摘要</th>
				<th>币种</th>			
				<th>汇率</th>
				<th>投资总额</th>
				<th>总额本币</th>
				<th>现金总额</th>
				<th>现金本币</th>
				<th>状态</th>
				<th>制表人</th>
				<th>审核人</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:if test="td[1] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
        	</xsl:if>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td><xsl:value-of select="."/></td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
											 		javascript:openPage('<xsl:value-of select='../pk/invest_id' />');
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=16">
									<xsl:if test="../td[1]= '合计'">
										<td></td>
									</xsl:if>
									<xsl:if test="../td[1] != '合计'">
												<td><xsl:value-of select="."/></td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=12 or position()=13 or position()=17 or position()=18 or position()=19 or position()=20">
									<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
