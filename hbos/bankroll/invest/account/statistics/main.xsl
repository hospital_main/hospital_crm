<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
         <th nowrap='true'>单据编码</th>
         <th nowrap='true'>制单日期</th>
         <th nowrap='true'>投资单位</th>
         <th nowrap='true'>投资类别</th>
         <th nowrap='true'>币种</th>
         <th nowrap='true'>汇率</th>
         <th nowrap='true'>投资金额</th>
         <th nowrap='true'>收回金额</th>
         <th nowrap='true'>结欠本金</th>
         <th nowrap='true'>应收利息</th>
         <th nowrap='true'>收取利息</th>
         <th nowrap='true'>结欠利息</th>
         <th nowrap='true'>收回进度(%)</th>
         <th nowrap='true'>结欠本利和</th>
		  	
      </tr>
     
    </thead>
  <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td><xsl:value-of select="."/></td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
											 		javascript:openInvestPage('<xsl:value-of select='../pk/invest_id' />');
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=8 and ../td[1] != '合计'">
										<td align='right'>
											<a href="#"><xsl:attribute name="onclick" >
openDialog('returnList.html?load=&lt;id&gt;<xsl:value-of select='../pk/invest_id' />&lt;/id&gt;', 'dialogWidth:850px;dialogHeight:500px')
											</xsl:attribute><xsl:value-of select="format-number(.,'###,##0.00')" /></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=11 and ../td[1] != '合计'">
										<td align='right'>
											<a href="#"><xsl:attribute name="onclick" >
openDialog('interestList.html?load=&lt;id&gt;<xsl:value-of select='../pk/invest_id' />&lt;/id&gt;', 'dialogWidth:850px;dialogHeight:500px')
											</xsl:attribute><xsl:value-of select="format-number(.,'###,##0.00')" /></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=6">
										<td align='right'><xsl:value-of select="."/></td>
									
								</xsl:when>
								<xsl:when test="position()>=7">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
