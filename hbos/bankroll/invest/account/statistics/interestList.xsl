<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead"><td colspan='6' align='left'>投资收益单列表</td></tr>
			<tr noWrap="true" class="mainHead">
					<th noWrap="true">单据编号</th>
					<th noWrap="true">单据日期</th>
					<th noWrap="true">收款方式</th>
					<th noWrap="true">币种</th>
					<th noWrap="true">收益金额</th>
					<th noWrap="true">收益本币</th>
				</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position() &gt; 4">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
