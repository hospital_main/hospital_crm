<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
			<tr noWrap='true' class='mainHead'>
      
		  	<td nowrap='true' rowspan='2'>投资类别编码</td>
		  	<td nowrap='true' rowspan='2'>投资类别名称</td>
	
		  	<td nowrap='true' colspan='2'>期初金额</td>
		  	<td style="display:none"/>
		  	<td nowrap='true' colspan='2'>本期投资</td>
		  	<td style="display:none"/>
		  	<td nowrap='true' colspan='2'>本期收回</td>
		  	<td style="display:none"/>
		  	<td nowrap='true' colspan='2'>期末余额</td>
		  	<td style="display:none"/>
		  	
		  	
      </tr>
      <tr noWrap='true' class='mainHead'>
     
		 		<td style="display:none"/>
		 			<td style="display:none"/>
		 			
		 				
		  	
		  	<td nowrap='true'>原币</td>
		  	
		  	<td nowrap='true'>本币</td>
		  	
		  	<td nowrap='true'>原币</td>
		  
		  	<td nowrap='true'>本币</td>
		  	
	
		  	<td nowrap='true'>原币</td>
		  
		  	<td nowrap='true'>本币</td>
		  	<td nowrap='true'>原币</td>
		  
		  	<td nowrap='true'>本币</td>
		  	
      </tr> 
		</thead>
		<tbody>
			
   			<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
           
                <td><xsl:value-of select="."/></td>
              
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>      
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
