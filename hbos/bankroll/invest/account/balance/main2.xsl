<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>投资类别编码</th>
         <th nowrap='true'>投资类别名称</th>
          <th nowrap='true'><a href='#'><xsl:attribute name='onclick'>show('3')</xsl:attribute>期初金额</a></th>
           <th nowrap='true'><a href='#'><xsl:attribute name='onclick'>show('4')</xsl:attribute>本期投资</a></th>
            <th nowrap='true'><a href='#'><xsl:attribute name='onclick'>show('5')</xsl:attribute>本期收回</a></th>
             <th nowrap='true'><a href='#'><xsl:attribute name='onclick'>show('6')</xsl:attribute>期末余额</a></th>
        
	
		  	
      </tr>
     
    </thead>
    <tbody>
    
   		<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position() = 1">
										<td><a href='#'>
										<xsl:attribute name='onclick'>
											openCom('<xsl:value-of select="." />')	
										</xsl:attribute>
										<xsl:value-of select="." /></a></td>
							</xsl:when>
							<xsl:when test="position()=4 or position()=5 or position()=6 or position()=3">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
						
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>        
    </tbody>
  </xsl:template>
</xsl:stylesheet>

