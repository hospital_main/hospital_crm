<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true"  style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
		
			
			<tr noWrap='true' class='mainHead'>
			 	 <td nowrap='true'>单据编码</td>
         <td nowrap='true'>制单日期</td>
         <td nowrap='true'>投资单位</td>
         <td nowrap='true'>投资类别</td>
         <td nowrap='true'>币种</td>
         <td nowrap='true'>汇率</td>
         <td nowrap='true'>投资金额</td>
         <td nowrap='true'>本币金额</td>
         <td nowrap='true'>投资收益</td>
         <td nowrap='true'>收益本币</td>
         <td nowrap='true'>投资收益率(%)</td>
			</tr>          
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=8 or position()=9 or position()=10 or position()=11 or position()= 7 ">
								<td align='right'>
										<xsl:value-of select="format-number(.,'###,##0.00')" />
								</td>
							</xsl:when>
							<xsl:when test="position()=6">
									<td align='right'><xsl:value-of select="."/></td>									
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  
    
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
