<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="headRow" select="count(/root/tbody/tr[1]/td)" />	
			<xsl:for-each select="/root/tbody/tr[position() &lt; 3]">
				<xsl:variable name="curRow" select="position()" />	
				<tr noWrap="true" class="mainHead">
					<xsl:for-each select="td">
							<xsl:variable name="curcode" select="."/>
			      	<xsl:variable name="curCol" select="position()" />	
			      	<xsl:variable name="colspan" select="count(/root/tbody/tr[$curRow]/td[.=$curcode and .!='ԭ��' and .!='����'])" />
			      	<xsl:variable name="rowspan" select="count(/root/tbody/tr[position() &lt; 3 and td[$curCol] = $curcode])" />
							<xsl:choose>
								<xsl:when test=". != string(../td[$curCol - 1]) and . != string(/root/tbody/tr[$curRow - 1]/td[$curCol])">
										<td colspan='{$colspan}' rowspan='{$rowspan}'>
											<xsl:value-of select="."/>
										</td>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=3 and .!=''">
									<xsl:if test='starts-with(.,"TZD")'>
										<td align='left'>
											<a href='#'>
												<xsl:attribute name='onclick'>javascript:openInvestPage('<xsl:value-of select='../pk/id' />');</xsl:attribute>
											
												<xsl:value-of select="." /></a>
												
										</td>
									</xsl:if>
									<xsl:if test='starts-with(.,"TZSH")'>
										<td align='left'>
											<a href='#'>
												<xsl:attribute name='onclick'>javascript:openPage('<xsl:value-of select='../pk/id' />');</xsl:attribute>
												<xsl:value-of select="." />
											</a>
										</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position() &gt; 6 and (($headRow = 10 and position() != 9) or ($headRow = 14 and position() != 12))">
										<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
