<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
		<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)"/>
		<thead>
			<tr>
				<td style='fontsize:maintitle;colspan:colcount'></td>
				<xsl:call-template  name="repeat">
					<xsl:with-param  name="times" select="$colNum - 1"/>  
				</xsl:call-template>
			</tr>
			<xsl:for-each select="/root/tbody/tr[position() &lt; 3]">
				<xsl:variable name="curRow" select="position()" />	
				<tr noWrap="true" class="mainHead">
					<xsl:for-each select="td">
							<xsl:variable name="curcode" select="."/>
			      	<xsl:variable name="curCol" select="position()" />	
			      	<xsl:variable name="colspan" select="count(/root/tbody/tr[$curRow]/td[.=$curcode and .!='ԭ��' and .!='����'])" />
			      	<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[ position() &lt; 3 and $curCol] = $curcode])" />
							<xsl:choose>
								<xsl:when test=". = 'ԭ��' or .='����'">
										<td>
											<xsl:value-of select="."/>
										</td>
								</xsl:when>
								<xsl:when test=". != string(../td[$curCol - 1]) and . != string(/root/tbody/tr[$curRow - 1]/td[$curCol])">
										<td colspan='{$colspan}' rowspan='{$rowspan}'>
											<xsl:value-of select="."/>
										</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								
								<xsl:when test=".!='' and position() &gt; 6 and (($colNum = 10 and position() != 9) or ($colNum = 14 and position() != 12))">
										<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
