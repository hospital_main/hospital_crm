<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true"  style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		

      		
      	</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true' colspan='2'><xsl:value-of select="/root/tbody/tr[position()=1]/td[1]"/>年</td>
        <td style="display:none"/>
		  	<td nowrap='true' rowspan='2'>单据编号</td>
		  	<td nowrap='true' rowspan='2'>投资类型</td>
		  	<td nowrap='true' rowspan='2'>投资单位</td>
		  	<td nowrap='true' rowspan='2'>摘要</td>
		  	
		  	<td nowrap='true' rowspan='2'>借方</td>
		  	
		  	<td nowrap='true' rowspan='2'>贷方</td>
		  	
		  	<td nowrap='true' rowspan='2'>方向</td>
		  	<td nowrap='true' rowspan='2'>余额</td>
		 
				
			</tr>
			
			<tr noWrap='true' class='mainHead'>
			
				<td nowrap='true'>月</td>
        <td nowrap='true'>日</td>
       
			</tr>          
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position()=2]">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=7">
								<td align='right'></td>
							</xsl:when>
							<xsl:when test="position()=8">
								<td align='right'></td>
							</xsl:when>
						
							<xsl:when test="position()=11">
								
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  
    	<xsl:for-each select="/root/tbody/tr[position()>2]">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	
						
							<xsl:when test="position()=11">
								
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>      
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
