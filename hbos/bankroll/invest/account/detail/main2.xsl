<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true' colspan='2'><xsl:value-of select="/root/tbody/tr[position()=1]/td[1]"/>年</th>
        
		  	<th nowrap='true' rowspan='2'>单据编号</th>
		  	<th nowrap='true' rowspan='2'>投资类型</th>
		  	<th nowrap='true' rowspan='2'>投资单位</th>
		  	<th nowrap='true' rowspan='2'>摘要</th>
		  	
		  	<th nowrap='true' rowspan='2'>借方</th>
		  	
		  	<th nowrap='true' rowspan='2'>贷方</th>
		  	
		  	<th nowrap='true' rowspan='2'>方向</th>
		  	<th nowrap='true' rowspan='2'>余额</th>
		  	
      </tr>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>月</th>
        <th nowrap='true'>日</th>
		  	
		  
		  	
		  
		
      </tr>
    </thead>
    <tbody>
<xsl:for-each select="/root/tbody/tr[position()=2]">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=7">
								<td align='right'></td>
							</xsl:when>
							<xsl:when test="position()=8">
								<td align='right'></td>
							</xsl:when>
							<xsl:when test="position()=10">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=11">
								
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  
    	<xsl:for-each select="/root/tbody/tr[position()>2]">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=3">
								<xsl:choose>
            		<xsl:when test="../td[11]='1'">
            			<td><a>
							    <xsl:attribute name="href" >
							      javascript:openInvestPage('<xsl:value-of select='../pk/bill_id' />');
							    </xsl:attribute><xsl:value-of select="."/></a></td>
								</xsl:when>
								<xsl:when test="../td[11]='2'">
									<td><a>
						          <xsl:attribute name="href" >
						    	    javascript:openPage('<xsl:value-of select='../pk/bill_id' />');
						  	      </xsl:attribute><xsl:value-of select="."/></a></td>
								</xsl:when>
								<xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              	</xsl:otherwise>
              	</xsl:choose>
							</xsl:when>
							<xsl:when test="position()=7">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=8">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=10">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=11">
								
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>        
    </tbody>
  </xsl:template>
</xsl:stylesheet>

