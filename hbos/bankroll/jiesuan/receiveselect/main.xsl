<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th >单据编号 </th>
			<th noWrap="true">收款日期</th>
			<th noWrap="true">职能部门</th>
			<th noWrap="true">业务部门</th>
			<th noWrap="true">资金类别</th>
			<th noWrap="true">资金项目</th>
			<th noWrap="true">资金来源</th>
			<th noWrap="true">付款银行</th>
			<th noWrap="true">付款单位</th>
			<th noWrap="true">付款账户</th> 
			<th noWrap="true">付款地址</th>
			<th noWrap="true">收款银行</th>
			<td noWrap="true">收款账户</td>
			<th noWrap="true">收款方式</th>
			<th noWrap="true">票据号码</th>	
			<th noWrap="true">币种</th>
			<th noWrap="true">汇率</th>
			<th noWrap="true">收款金额</th>	
			<th noWrap="true">收款本币</th>
			<th noWrap="true">收款用途</th>
			<th noWrap="true">收款说明</th>
			<th noWrap="true">业务员</th>
			<th noWrap="true">附件张数</th>	
			<!--<th noWrap="true">项目信息</th>-->
			<th noWrap="true">状态</th>
			<th noWrap="true">制表人</th>
			<th noWrap="true">审核人</th>
			<th noWrap="true">确认人</th>
			<th noWrap="true">业务功能</th>
			<th noWrap="true">凭证</th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
          	
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=1 and ../td[1]!='合计'">
								<td  noWrap='true' >
							  	<a tabindex="-1">
							    <xsl:attribute name="href" >
							      javascript:view('&lt;receive_id&gt;<xsl:value-of select="../pk/receive_id"/>&lt;/receive_id&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:when test="position()=18 or position()=19">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
							</xsl:when>
							<xsl:when test="position()=17 or position()=23">
								<td align='right'><xsl:value-of select="." /></td>
							</xsl:when>
							<xsl:when test="position()=29">
								<td  noWrap='true' >
							  	<a tabindex="-1">
							    <xsl:attribute name="href" >
							      javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
						</xsl:choose>
					</xsl:for-each></tr>
				</xsl:for-each>
			</tbody>
	</xsl:template>
	
	</xsl:stylesheet>