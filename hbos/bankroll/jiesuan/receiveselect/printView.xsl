<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
    		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
    		<td style="display:none"/>
      		
      </tr>
			<tr noWrap='true' class='mainHead'>
				<td >单据编号 </td>
				<td noWrap="true">收款日期</td>
				<td noWrap="true">职能部门</td>
				<td noWrap="true">业务部门</td>
				<td noWrap="true">资金类别</td>
				<td noWrap="true">资金项目</td>
				<td noWrap="true">资金来源</td>
				<td noWrap="true">付款银行</td>
				<td noWrap="true">付款单位</td>
				<td noWrap="true">付款账户</td> 
				<td noWrap="true">付款地址</td>
				<td noWrap="true">收款银行</td>
				<td noWrap="true">收款账户</td>
				<td noWrap="true">收款方式</td>
				<td noWrap="true">票据号码</td>	
				<td noWrap="true">币种</td>
				<td noWrap="true">汇率</td>
				<td noWrap="true">收款金额</td>	
				<td noWrap="true">收款本币</td>
				<td noWrap="true">收款用途</td>
				<td noWrap="true">收款说明</td>
				<td noWrap="true">业务员</td>
				<td noWrap="true">附件张数</td>	
				<!--<td noWrap="true">项目信息</td>-->
				<td noWrap="true">状态</td>
				<td noWrap="true">制表人</td>
				<td noWrap="true">审核人</td>
				<td noWrap="true">确认人</td>
				<td noWrap="true">业务功能</td>
				<td noWrap="true">凭证</td>		
		  	
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
        
				<xsl:for-each select="./td">
					<xsl:choose>
						
						<xsl:when test="position()=18 or position()=19">
							<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
						</xsl:when>
						<xsl:when test="position()=17 or  position()=23">
							<td align='right'><xsl:value-of select="." /></td>
						</xsl:when>
						
						<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
					</xsl:choose>
				</xsl:for-each></tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>

