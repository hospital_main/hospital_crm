<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap="true" class="mainHead">
				<td>单据编号</td>
				<td>付款日期</td>
				<td>职能部门</td>
				<td>资金类别</td>
				<td>付款银行</td>
				<td>付款账户</td>
				<td>辅助账</td>			
				<td>收款单位</td>
				<td>收款银行</td>
				<td>收款账户</td>
				<td>收款地址</td>
				<td>付款方式</td>
				<td>票据号码</td>
				<td>币种</td>
				<td>汇率</td>
				<td>付款金额</td>
				<td>付款本币</td>
				<td>付款用途</td>
				<td>业务员</td>
				<td>附件张数</td>
				<td>状态</td>			
				<td>制表人</td>
				<td>审核人</td>
				<td>确认人</td>
				<td>业务功能</td>
				<td>业务编号</td>
				<td>凭证</td>
			</tr>
	  	</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				<xsl:for-each select="td">
					<xsl:choose>
					<xsl:when test="position()=16 or position()=17">
					<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
					</xsl:when>
					<xsl:otherwise>
					<td><xsl:value-of select="."/></td>
					</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				</tr>
			</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>