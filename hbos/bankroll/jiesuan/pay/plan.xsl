<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead> 
			<tr noWrap='true' class='mainHead'>
			<th width='25'>选择</th>
			<th noWrap="true">计划编号 </th>
			<th noWrap="true">业务编号 </th>
			<th noWrap="true">业务日期</th>
			<th noWrap="true">业务部门 </th>
			<th noWrap="true">资金项目 </th>
			<xsl:if test="/root/tbody/tr[1]/td[1] != ''">
					<th noWrap="true"><xsl:value-of select="/root/tbody/tr[1]/td[1]"/> </th>
			</xsl:if>	
			
			<th noWrap="true">计划金额</th>
			<th noWrap="true">已付金额 </th>
			<th noWrap="true">未付金额 </th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						 <td align='center'  style=''>
            	<input type='radio'  name='plan' onClick='choose(this)' style='font-size:8px;'>
              	<xsl:attribute name="value" >
              		<xsl:for-each select="pk/*">"<xsl:value-of select="name()"/>":"<xsl:value-of select="."/>",</xsl:for-each>
              	</xsl:attribute>
    			  	</input>  
          	</td>
					<xsl:for-each select="./td[position() &gt; 1]">
						<xsl:choose>
							<xsl:when test="position()=6">
								<xsl:if test=". = ''">
								</xsl:if>
								<xsl:if test=". != ''">
										<td ><xsl:value-of select="."/></td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=7 or position()=8 or position()=9">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
						</xsl:choose>
					</xsl:for-each></tr>
				</xsl:for-each></tbody>
		</xsl:template>
	
	</xsl:stylesheet>