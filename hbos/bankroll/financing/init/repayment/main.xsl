<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th width='25'><input type='checkbox'/></th>
			<th >单据编号 </th>
			<th noWrap="true">制单日期 </th>
			<th noWrap="true">筹资单位 </th>
			<th noWrap="true">筹资单据 </th>
			<th noWrap="true">职能部门 </th>
			<th noWrap="true">还款方式 </th>
			<th noWrap="true">票据号码 </th>
			<th noWrap="true">还款摘要 </th>
			<th noWrap="true">币种 </th>
			<th noWrap="true">汇率 </th>	
			<th noWrap="true">还款金额</th>
			<th noWrap="true">还款本币 </th>	
			<th noWrap="true">状态 </th>
			<th noWrap="true">制单人 </th>
			<th noWrap="true">审核人 </th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					
					<tr>
						 
          	<xsl:if test="td[1]!='合计'">
 						<td align='center'  style=''>
            	<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              	<xsl:attribute name="value" >
              		&lt;return_id&gt;<xsl:value-of select="pk/return_id"/>&lt;/return_id&gt;
              	</xsl:attribute>
    			  	</input>  
          	</td>
				</xsl:if>
				<xsl:if test="td[1]='合计'" >
					<td></td>
					</xsl:if>
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=1 and ../td[1]!='合计'">
								<td  noWrap='true' >
							  	<a tabindex="-1">
							    <xsl:attribute name="href" >
							      javascript:view('&lt;return_id&gt;<xsl:value-of select="../pk/return_id"/>&lt;/return_id&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:when test="position()=4 and ../td[1]!=''">
								<td  noWrap='true' >
							  	<a tabindex="-1">
							    <xsl:attribute name="href" >
							      javascript:openFinance('<xsl:value-of select="../pk/finance_id"/>')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
								<xsl:when test="position()=10">
								<td align='right'><xsl:value-of select="." /></td>
							</xsl:when>
							<xsl:when test="position()=11 or position()=12">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
						</xsl:choose>
					</xsl:for-each></tr>
				</xsl:for-each></tbody>
		</xsl:template>
	
	</xsl:stylesheet>