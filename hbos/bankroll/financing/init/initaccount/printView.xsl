<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-6"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap='true' class='mainHead'>
		  		<td noWrap="true">筹资类别</td>
					<td noWrap="true">筹资单位</td>
					<td noWrap="true">币种</td>
					<td noWrap="true">汇率</td>
					<td noWrap="true">年初余额</td>
					<td noWrap="true">年初本币</td>
					<td noWrap="true">累计借方</td>
					<td noWrap="true">借方本币</td>
					<td noWrap="true">累计贷方</td>
					<td noWrap="true">贷方本币</td>
		      <td noWrap="true">期末余额</td> 
		      <td noWrap="true">期末本币</td>
     		</tr>
  		</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">            
	            <xsl:choose>
	              <xsl:when test="position()=1">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when> 
	              <xsl:when test="position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12">
	                <td class='numberText' noWrap='true' align='right'>
	              	  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </td>
	              </xsl:when>
	              <xsl:otherwise>
	                <td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          	</xsl:choose>          
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>