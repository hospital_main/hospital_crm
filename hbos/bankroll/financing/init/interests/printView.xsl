<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr> 
	  		<tr noWrap='true' class='mainHead'>
				<td noWrap="true">单据编号</td>
				<td noWrap="true">单据日期</td>
	      <td noWrap="true">筹资单位</td>
				<td noWrap="true">筹资编号</td>
				<td noWrap="true">职能部门</td>
				<td noWrap="true">支付方式</td>
				<td noWrap="true">票据号码</td>
				<td noWrap="true">利息摘要</td>
				<td noWrap="true">币种</td>
				<td noWrap="true">汇率</td>
				<td noWrap="true">利息金额</td>
				<td noWrap="true">利息本币</td>
				<td noWrap="true">状态</td>
				<td noWrap="true">制表人</td>
	      <td noWrap="true">审核人</td>     
      </tr>
	  	</thead>
	  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr[position()=1]">
        <tr>   
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
							  <td  noWrap='true'>合计</td>
							</xsl:when>
							<xsl:when test="position()=10">
								<td align='right'></td>
							</xsl:when>	
							<xsl:when test="position()=11 or position()=12">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
		    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	        <tr>	        	
	          <xsl:for-each select="td">
	            <xsl:choose>	
	             <xsl:when test="position()=10">
								<td align='right'><xsl:value-of select="."/></td>
							 </xsl:when>	
							 <xsl:when test="position()=11 or position()=12">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							 </xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
	          	</xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>