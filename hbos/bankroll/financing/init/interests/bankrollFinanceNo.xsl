<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>      
			<tr noWrap="true" class="mainHead">        
				<th>选择</th>
				<th>单据编号</th>
				<th>筹资单位</th>
				<th>筹资类别</th>
				<th>制单日期</th>
				<th>到期日期</th>
				<th>起息日期</th>
				<th>币种</th>
				<th>筹资总额</th>
				<th>利息金额</th>
				<th style="display:none">标示</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
		   		       		<td align='center'>
			    	  	  			<input type='radio' name='select' onclick='choose(this)'>
						              <xsl:attribute name="value" >
				                 	 <xsl:value-of select="../pk/id"/>
						      			  </xsl:attribute>
						      			  <xsl:attribute name="dept" >
				                 	 				<xsl:value-of select="../pk/dept"/>
						      			  </xsl:attribute>
						      			</input>
				   					</td>
					   				<td align='center'>
						    	    <xsl:value-of select="."/>
		                </td>
              	</xsl:when>
								<xsl:otherwise>
									<td align='center'><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
