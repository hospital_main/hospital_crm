<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		  <tr noWrap='true' class='mainHead'>
	      <th><input type='checkbox'/></th>
				<th noWrap="true">单据编号</th>
				<th noWrap="true">单据日期</th>
	      <th noWrap="true">筹资单位</th>
				<th noWrap="true">筹资编号</th>
				<th noWrap="true">职能部门</th>
				<th noWrap="true">支付方式</th>
				<th noWrap="true">票据号码</th>
				<th noWrap="true">利息摘要</th>
				<th noWrap="true">币种</th>
				<th noWrap="true">汇率</th>
				<th noWrap="true">利息金额</th>
				<th noWrap="true">利息本币</th>
				<th noWrap="true">状态</th>
	      <th noWrap="true">制表人</th>
	      <th noWrap="true">审核人</th>
      </tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr[position()=1]">
        <tr>
          <td noWrap='true'></td>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
							  <td noWrap='true' align='center'>合计</td>
							</xsl:when>
							<xsl:when test="position()=10 ">
								<td align='right' noWrap='true'></td>
							</xsl:when>
							<xsl:when test="position()=11 or position()=12">
								<td align='right' noWrap='true'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td noWrap='true'><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
	    <xsl:for-each select="/root/tbody/tr[position()!=1]">
        <tr>
          <td align='center'  style=''>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
							  <td  noWrap='true'>
							  <a tabindex="-1">
							    <xsl:attribute name="href">
							      javascript:view('insert.html?load=&lt;id&gt;<xsl:value-of select="../pk/interest_id"/>&lt;/id&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:when test="position()=4">
							  <td  noWrap='true'>
							  <a tabindex="-1">
							    <xsl:attribute name="href">
							      javascript:viewOther('../../init/bill/insert.html?load=&lt;id&gt;<xsl:value-of select="../pk/finance_id"/>&lt;/id&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:when test="position()=10 or position()=11">
								<td align='right' noWrap='true'><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=11 or position()=12">
								<td align='right' noWrap='true'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td noWrap='true'><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


