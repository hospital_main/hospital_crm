<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th width='25'>选择</th>
			<th >单据编码 </th>
			<th noWrap="true">筹资单位 </th>
			<th noWrap="true">筹资类别</th>
			<th noWrap="true">到息日期 </th>
			<th noWrap="true">起息日期 </th>	
			<th noWrap="true">币种 </th>
			<th noWrap="true">筹资总额</th>
			<th noWrap="true">利息金额 </th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						 <td align='center'  style=''>
            	<input type='radio'  name='finance_id' onClick='choose(this)' style='font-size:8px;'>
              	<xsl:attribute name="value" >
              		<xsl:for-each select="pk/*"><xsl:value-of select="."/></xsl:for-each>
              	</xsl:attribute>
    			  	</input>  
          	</td>
					<xsl:for-each select="./td">
						<xsl:choose>
							<xsl:when test="position()=7 or position()=8 ">
								<td align='right'><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
						</xsl:choose>
					</xsl:for-each></tr>
				</xsl:for-each></tbody>
		</xsl:template>
	
	</xsl:stylesheet>