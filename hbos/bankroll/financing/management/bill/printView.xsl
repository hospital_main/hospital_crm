<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr> 
	  		<tr noWrap='true' class='mainHead'>
				<td noWrap="true">单据编码</td>
				<td noWrap="true">制单日期</td>
	      		<td noWrap="true">筹资类别</td>
				<td noWrap="true">筹资单位</td>
				<td noWrap="true">职能部门</td>
				<td noWrap="true">收款方式</td>
				<td noWrap="true">票据号码</td>
				<td noWrap="true">起息日期</td>		
				<td noWrap="true">到期日期</td>
				<td noWrap="true">利率方式</td>
				<td noWrap="true">利率</td>
				<td noWrap="true">筹资摘要</td>
				<td noWrap="true">币种</td>
				<td noWrap="true">汇率</td>
				<td noWrap="true">筹资总额</td>
				<td noWrap="true">本币金额</td>
				<td noWrap="true">利息金额</td>
				<td noWrap="true">利息本币</td>
		      <td noWrap="true">手续费</td>
		      <td noWrap="true">其他费用</td>
		      <td noWrap="true">状态</td>
		      <td noWrap="true">制表人</td>
		      <td noWrap="true">审核人</td>
	     	 <td noWrap="true">凭证</td>
      </tr>
	  	</thead>
	  	<tbody>
	  		<xsl:for-each select="/root/tbody/tr[position()=1]">
        <tr>   
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
							  <td  noWrap='true'>合计</td>
							</xsl:when>
							 <xsl:when test="position()=14 or position()=20 or position()=19">
								<td align='right' noWrap='true'></td>
							</xsl:when>
							<xsl:when test="position()=18 or position()=15 or position()=16 or position()=17">
								<td align='right' noWrap='true'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
		    <xsl:for-each select="/root/tbody/tr[position()!=1]">
	        <tr>	        	
	          <xsl:for-each select="td">
	            <xsl:choose>	
						<xsl:when test="position()=20 or position()=15 or position()=16 or position()=17  or position()=18 or position()=19">
								<td align='right' noWrap='true'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
	          	</xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>