<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">        
				<th>选择</th>
				<th>单据编码</th>
				<th>担保单位</th>
				<th>担保方式</th>
				<th>单据日期</th>
				<th>到期日期</th>
				<th>币种</th>
				<th>担保金额</th>
				<th>本币金额</th>
				<th style="display:none">标示</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
		   		       		<td align='center'>
			    	  	  			<input type='radio' name='select' onclick='choose(this)'>
						              <xsl:attribute name="value" >
				                 	 <xsl:value-of select="../pk/id"/>
						      			  </xsl:attribute>
						      			</input>
				   					</td>
					   				<td align='center'>
						    	    <xsl:value-of select="."/>
		                </td>
              	</xsl:when>
								<xsl:otherwise>
									<td align='center'><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
