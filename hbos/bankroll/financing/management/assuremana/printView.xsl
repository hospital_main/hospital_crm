<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td) - 1"/>
		<root>
		<thead>
			<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  	</tr>		 
			<tr noWrap='true' class='mainHead'>
				<td >单据编码 </td>
			<td noWrap="true">制单日期 </td>
			<td noWrap="true">担保单位 </td>
			<td noWrap="true">到期日期 </td>
			<td noWrap="true">币种 </td>
			<td noWrap="true">汇率 </td>	
			<td noWrap="true">担保总额</td>
			<td noWrap="true">本币金额 </td>
			<td noWrap="true">担保方式</td>
			<td noWrap="true">还款来源</td>
			<td noWrap="true">单据说明 </td>	
			<td noWrap="true">状态 </td>
			<td noWrap="true">制单人 </td>
			<td noWrap="true">审核人 </td>
				</tr>
		</thead>
		<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
							<xsl:when test="position()=6">
								<td align='right'><xsl:value-of select="." /></td>
							</xsl:when>
							<xsl:when test="position()=7 or position()=8 ">
								<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
								</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>