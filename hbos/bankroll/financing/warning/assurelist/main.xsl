<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
					<th noWrap="true">单据编码</th>
					<th noWrap="true">制单日期</th>
					<th noWrap="true">担保单位</th>
					<th noWrap="true">担保类别</th>
					<th noWrap="true">币种</th>
					<th noWrap="true">汇率</th>
					<th noWrap="true">担保金额</th>
					<th noWrap="true">本币金额</th>
					<th noWrap="true">到期日期</th>
					<th noWrap="true">剩余天数</th>
				</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1 and .!='合计'">
										<td align='left'>
											<a href='#'><xsl:attribute name='onclick'>openDialog('../../init/assuremana/update.html?load=&lt;warrant_id&gt;<xsl:value-of select='../pk/warrant_id' />&lt;/warrant_id&gt;', 'dialogWidth:700px;dialogHeight:400px')</xsl:attribute><xsl:value-of select="." /></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=6">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.0000000000')" />
									</td>
								</xsl:when>
								<xsl:when test="position() &gt; 6 and position() &lt; 9">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=10 and ../td[1]!='合计'">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=10 and ../td[1]='合计'">
										<td align='right'></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
