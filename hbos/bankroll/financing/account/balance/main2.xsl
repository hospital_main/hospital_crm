<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
					<th rowspan='2' noWrap="true">筹资类别编码</th>
					<th rowspan='2' noWrap="true">筹资类别名称</th>
					<th colspan='2' noWrap="true">期初余额</th>
					<th colspan='2' noWrap="true">筹资金额</th>
					<th colspan='2' noWrap="true">还款金额</th>
					<th colspan='2' noWrap="true">期末余额</th>
				</tr>
				<tr noWrap="true" class="mainHead">
					<th noWrap="true">本币</th>
					<th noWrap="true">原币</th>
					<th noWrap="true">本币</th>
					<th noWrap="true">原币</th>
					<th noWrap="true">本币</th>
					<th noWrap="true">原币</th>
					<th noWrap="true">本币</th>
					<th noWrap="true">原币</th>
				</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
										<td>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
