		<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="headRow" select="count(/root/tbody/tr[position() &lt; 3 and td[1]='筹资类别编码'])" />
				<xsl:for-each select="/root/tbody/tr[position() &lt; ($headRow + 1)]">
				<xsl:variable name="curRow" select="position()" />	
				<tr noWrap="true" class="mainHead">
					<xsl:for-each select="td">
							<xsl:variable name="curcode" select="."/>
			      	<xsl:variable name="curCol" select="position()" />	
			      	<xsl:variable name="colspan" select="count(/root/tbody/tr[$curRow]/td[.=$curcode and .!='原币' and .!='本币'])" />
			      	<xsl:variable name="rowspan" select="count(/root/tbody/tr[position() &lt; 3 and td[$curCol] = $curcode])" />
							<xsl:choose>
								<xsl:when test="$headRow = 1 and position() &gt; 2">
										<td colspan='{$colspan}' rowspan='{$rowspan}'>
											<a href='#'><xsl:attribute name='onclick'>show('<xsl:value-of select="position()" />')</xsl:attribute><xsl:value-of select="."/></a>
										</td>
								</xsl:when>
								<xsl:when test="$headRow = 2 and $curRow = 2 and position() &gt; 2">
										<td colspan='{$colspan}' rowspan='{$rowspan}'>
											<a href='#'><xsl:attribute name='onclick'>show('<xsl:value-of select="position()" />')</xsl:attribute><xsl:value-of select="."/></a>
										</td>
								</xsl:when>
								<xsl:when test=". != string(../td[$curCol - 1]) and . != string(/root/tbody/tr[$curRow - 1]/td[$curCol])">
										<td colspan='{$colspan}' rowspan='{$rowspan}'>
											<xsl:value-of select="."/>
										</td>
								</xsl:when>
								<xsl:otherwise>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; $headRow]">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position() = 1">
										<td><a href='#'>
										<xsl:attribute name='onclick'>
											openCom('<xsl:value-of select="." />')	
										</xsl:attribute>
										<xsl:value-of select="." /></a></td>
								</xsl:when>
								<xsl:when test="position() &gt; 2">
										<td align='right'><xsl:value-of select="format-number(.,'###,##0.00')" /></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
