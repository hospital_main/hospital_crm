<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
					<th colspan="2">2014年</th>
					<th rowspan='2' noWrap="true">单据编号</th>
					<th rowspan='2' noWrap="true">筹资类型</th>
					<th rowspan='2' noWrap="true">筹资单位</th>
					<th rowspan='2' noWrap="true">摘要</th>
					<th colspan='2' noWrap="true">借方</th>
					<th colspan='2' noWrap="true">贷方</th>
					<th rowspan='2' noWrap="true">方向</th>
					<th colspan='2' noWrap="true">余额</th>
				</tr>
			<tr  noWrap="true" class="mainHead">
					<th noWrap="true">月</th>
					<th noWrap="true">日</th>
					<th noWrap="true">原币</th>
					<th noWrap="true">本币</th>
					<th noWrap="true">原币</th>
					<th noWrap="true">本币</th>
					<th noWrap="true">原币</th>
					<th noWrap="true">本币</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
										<td>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
