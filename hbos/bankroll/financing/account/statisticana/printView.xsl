<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
		<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
		<thead>
			<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:14'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  	</tr>
			<tr noWrap="true" class="mainHead">
					<td noWrap="true">单据编码</td>
					<td noWrap="true">制单日期</td>
					<td noWrap="true">筹资单位</td>
					<td noWrap="true">筹资类别</td>
					<td noWrap="true">币种</td>
					<td noWrap="true">汇率</td>
					<td noWrap="true">筹资总额</td>
					<td noWrap="true">还款金额</td>
					<td noWrap="true">结欠本金</td>
					<td noWrap="true">应付利息</td>
					<td noWrap="true">支付利息</td>
					<td noWrap="true">结欠利息</td>
					<td noWrap="true">还款进度(%)</td>
					<td noWrap="true">结欠本利和</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=6 and .!='' ">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,###0.0000000000')" />
									</td>
								</xsl:when>
								<xsl:when test="position() &gt; 6">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,###0.00')" />
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
