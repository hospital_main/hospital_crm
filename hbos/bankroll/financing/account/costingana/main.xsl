<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
					<th noWrap="true">单据编码</th>
					<th noWrap="true">制单日期</th>
					<th noWrap="true">筹资单位</th>
					<th noWrap="true">筹资类别</th>
					<th noWrap="true">币种</th>
					<th noWrap="true">汇率</th>
					<th noWrap="true">筹资总额</th>
					<th noWrap="true">本币金额</th>
					<th noWrap="true">手续费</th>
					<th noWrap="true">其他费用</th>
					<th noWrap="true">利息</th>
					<th noWrap="true">筹资成本</th>
					<th noWrap="true">筹资成本率(%)</th>
				</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1 and .!='合计'">
										<td align='left'>
											<a href='#'><xsl:attribute name='onclick'>openDialog('../../init/bill/insert.html?load=&lt;id&gt;<xsl:value-of select='../pk/finance_id' />&lt;/id&gt;', 'dialogWidth:950px;dialogHeight:340px')</xsl:attribute><xsl:value-of select="." /></a>
									</td>
								</xsl:when>
								<xsl:when test="position()=6">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.0000000000')" />
									</td>
								</xsl:when>
								<xsl:when test="position() &gt; 6">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,##0.00')" />
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
