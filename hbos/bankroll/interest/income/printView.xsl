<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      	<tr noWrap="true" class="mainHead">
					<td colspan='6'></td>
					<td style='display:none'></td>
					<td style='display:none'></td>
				 	<td style='display:none'></td>
				 	<td style='display:none'></td>
				 	<td style='display:none'></td>
				</tr>
	  		<tr noWrap="true" class="mainHead">
					<td>业务类别</td>
					<td>业务编码</td>
					<td>业务名称</td>
				 	<td>科目编码</td>
				 	<td>科目名称</td>
				 	<td>金额</td>
				</tr>
	  	</thead>
	  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=6">
                <td align="right"><xsl:value-of select="format-number(.,'###,##0.00')"/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each> 	
  	</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>