<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
			<tr noWrap='true' class='mainHead'>
				<td noWrap='true' >单据编号</td>
				<td noWrap='true' >单据日期</td>
				<td noWrap='true' >开户银行</td>
				<td noWrap='true' >活期账号</td>
				<td noWrap='true' >起息日期</td>
				<td noWrap='true' >截止日期</td>
				<td noWrap='true' >币种</td>
				<td noWrap='true' >利率方式</td>
				<td noWrap='true' >利率</td>
				<td noWrap='true' >积数</td>
				<td noWrap='true' >利息金额</td>
				<td noWrap='true' >利息本币</td>			
				<td noWrap='true' >利息摘要</td>
				<td noWrap='true' >制单人</td>
				<td noWrap='true' >审核人</td>
				<td noWrap='true' >是否协定</td>
				<td noWrap='true' >状态</td>
				<td noWrap='true' >凭证</td>
		  	
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<xsl:choose>
						
						<xsl:when test="position()=11 or position()=12 ">
							<td align='right'>
	              <xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
	          </xsl:when>
		        <xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
				  </xsl:choose>		
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>

