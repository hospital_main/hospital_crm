<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:for-each select="/root/t2head/tr[1]">
				<tr  noWrap="true" class="mainHead">
					<xsl:for-each select="td">

						<th align="center">
							<xsl:value-of select="."/>
						</th>

					</xsl:for-each>
				</tr>
			</xsl:for-each>

		</thead>
		<tbody>

			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">

						<td align="left">
							<xsl:value-of select="."/>
						</td>

					</xsl:for-each>
				</tr>
			</xsl:for-each>

		</tbody>
	</xsl:template>
</xsl:stylesheet>
