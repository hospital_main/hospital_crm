<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    <xsl:variable name="colNum" select="count(//tr[1]/td)-1"/>
		<xsl:variable name="cellend" select="//tr[1]/td[$colNum]"/>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th width='40'>ѡ��</th>
        <xsl:if test=" $cellend != '1' ">
      	<th nowrap='true'>��λ����</th>
      	<th nowrap='true'>��λ����</th>
      	
      	</xsl:if>
      	<xsl:if test=" $cellend != '0' ">
      	<th nowrap='true'>���ױ���</th>
      	<th nowrap='true'>��������</th>
      	</xsl:if>
      	
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:'>
            <input type='checkbox' name='select'>
            </input>
          </td>
          <xsl:for-each select="td">
           	<xsl:choose>
              <xsl:when test="position()=$colNum">
             	 <td align='center'  style='display:none'>
          			<xsl:value-of select="//tr[1]/td[$colNum]"/>
       				 </td>
	       		  </xsl:when>
              <xsl:when test="position()=($colNum+1)">
             	 <td align='center'  style='display:none'>
          			<xsl:value-of select="."/>
       				 </td>
	       		  </xsl:when>
            	<xsl:otherwise>
			         	<td>
									<xsl:value-of select="."/>
								</td>
						  </xsl:otherwise>
           </xsl:choose>
                
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

