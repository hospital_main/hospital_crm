<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th>资产卡片号</th>
        <th>资产名称</th>
				<th>资产型号</th>
				<th>资产原值</th>
				<th>设备单价</th>
				<th>附件编码</th>
				<th>附件名称</th>
				<th>附件型号</th>
				<th>附件金额</th>
				<th>开始日期</th>
        <th>分摊月数</th>
				<th>已分摊月数</th>
				<th>本次分摊金额</th>
				<th>部门编码</th>
        <th>部门名称</th>
				<th>经费来源</th>						
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:for-each select="td">
           	<xsl:choose>
	          	
	            <xsl:when test="position()=4">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=5">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=9">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=11">
	              <td align='right'>
	              	<xsl:value-of select="."/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=12">
	              <td align='right'>
	              	<xsl:value-of select="."/>
								
	              </td>
	            </xsl:when>
	            
	            <xsl:when test="position()=13">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


