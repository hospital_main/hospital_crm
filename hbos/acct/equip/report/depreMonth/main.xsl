<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th>卡片编号</th>
        <th>资产编码(院内)</th>
        <th>资产名称</th>
	<th>资产分类</th>
	<th>资产原值</th>
	<th>月折旧率(%)</th>
	<th>本月折旧</th>
	<th>累计折旧</th>					
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:for-each select="td">
           	<xsl:choose>
	          	
	            <xsl:when test="position()=5">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=6">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=7">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=8">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:when>
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


