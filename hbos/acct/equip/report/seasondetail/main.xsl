<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th rowspan="2" valign="middle">卡片号</th>
        <th rowspan="2" valign="middle">仪器名称</th>
        <th rowspan="2" valign="middle">规格</th>
				<th rowspan="2" valign="middle">型号</th>
				<th rowspan="2" valign="middle">厂家名称</th>				
				<th rowspan="2" valign="middle">国别</th>
				<th rowspan="2" valign="middle">合同编号</th>
				<th rowspan="2" valign="middle">数量</th>
				<th colspan="2" valign="middle" >价格</th>
				<th style="display:none"></th>
				<th colspan="2" valign="middle">经费来源</th>
				<th style="display:none"></th>
				<th rowspan="2" valign="middle">购置日期</th>
				<th rowspan="2" valign="middle">科室名称</th>
      </tr>
  		<tr noWrap="true" class="mainHead">
  		  <th style="display:none"></th>
  		  <th style="display:none"></th>
  		  <th style="display:none"></th>
  		  <th style="display:none"></th>
  		  <th style="display:none"></th>
  		  <th style="display:none"></th>
  		  <th style="display:none"></th>
  		  <th style="display:none"></th>
  			<th valign="middle" >外币金额</th>
  			<th valign="middle" >原值</th>
  			<th valign="middle" >外币来源</th>
  			<th valign="middle" >经费来源</th>
  			<th style="display:none"></th>
  		  <th style="display:none"></th>
  		</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:for-each select="td">
           	<xsl:choose>
	            <xsl:when test="position()=7">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0')"/>
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=8 or position()=9 or position()=10">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:when>
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


