<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
     <thead>
    	 
    	<tr noWrap="true" class="mainHead">
    			<td noWrap="true" style="fontsize:maintitle;colspan:3;align:center;border:none"/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    	    			<td style='display:none'/>
    			<td style='display:none'/>
    		
    			
    	</tr>
    	<tr noWrap="true" class="mainHead">
    			<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left;border:none"/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			     			<td style='display:none'/>
    			<td style='display:none'/>
    	</tr>
    	<tr noWrap="true" class="mainHead">
    			<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left;border:none"/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			     			<td style='display:none'/>
    			<td style='display:none'/>
    	</tr>
  		<tr noWrap='true' class='mainHead'>
        <td style='border:1'>部门编码</td>
        <td style='border:1'>部门名称</td>
				<td style='border:1'>折旧金额</td>
		        <td style='border:1'>计提原值</td>
				<td style='border:1'>累计折旧</td>	 
										
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:for-each select="td">
           	<xsl:choose>
	            <xsl:when test="position()=3">
	              <td class='numberText' noWrap='true' align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:otherwise>
		            <td>
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			
      </xsl:for-each>
      
    </tbody>
    <tfoot>
    
 		    <tr noWrap='true'>
		        <td style='fontsize:foot;colspan:colcount;align:left;border:none'></td>
		  			<xsl:call-template name="repeat">
		  				<xsl:with-param name="times" select="$colNum"/>    
		        </xsl:call-template>
  	  	</tr>
    		
    	</tfoot>
   </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>