<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-2"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap='true'>
	        <td style='fontsize:subtitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
				<xsl:variable name="colPosition" select="2"/>
				<tr noWrap='true' class='mainHead'>
					<td nowrap='true' rowspan="2">统计分类</td>
					<xsl:for-each select="/root/tbody/tr[1]/td">    		      
						<xsl:choose>
							<xsl:when test="position() &gt; $colPosition and position() mod 2 = 0 ">
								<td nowrap='true' colspan="2"><xsl:value-of select="."/></td>
								<td style="display:none" ></td>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td style="display:none" ></td>
					<xsl:for-each select="/root/tbody/tr[1]/td">    		      
						<xsl:if test="position() &gt; $colPosition ">
							<xsl:if test=" position() mod 2 = 0 ">
								<td nowrap='true'>金额</td>
							</xsl:if>
							<xsl:if test=" position() mod 2 = 1 ">
								<td nowrap='true'>数量</td>
							</xsl:if>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<xsl:if test="position() &gt; 2">
						<tr>
							<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position() = 2 ">
										<td><xsl:value-of select="."/></td>
									</xsl:when>
									<xsl:when test="position()>2 and position() mod 2 = 1 ">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0')"/></td>
									</xsl:when>
									<xsl:when test="position()>2 and position() mod 2 = 0 ">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
								</xsl:choose>
							</xsl:for-each>
						</tr>
					</xsl:if>	 
				</xsl:for-each>
			</tbody>
	 		<tfoot>
		    <tr noWrap='true'>
		        <td style='fontsize:foot;colspan:colcount;align:left;border:none'></td>
		  			<xsl:call-template name="repeat">
		  				<xsl:with-param name="times" select="$colNum"/>    
		        </xsl:call-template>
		  	</tr>
	  	</tfoot>
		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>