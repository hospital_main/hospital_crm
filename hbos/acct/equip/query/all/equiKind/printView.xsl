<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead> 
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum - 1"/>    
        </xsl:call-template>
  		</tr>
  		<tr noWrap="true" class="mainHead">
    			<td noWrap="true" style="fontsize:subtitle;colspan:20;align:left;border:none"/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style="display:none"></td>
					<td style="display:none"></td>
    			 
    	</tr>
  		<tr noWrap="true" class="mainHead">
				<td rowspan="2">������</td>
				<td rowspan="2">�������</td>
				<td colspan="9">�ڿ�</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan="8">����</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td rowspan="2">��ĩ�ܼ�</td>
				<!--td rowspan="2" style="display:none">�ۼ��۾�</td-->
			</tr>
			<tr noWrap="true" class="mainHead">
			  <td style="display:none"></td>
			  <td style="display:none"></td>
				<td>�ڳ����</td>
				<td>�����</td>
				<td>�˿�</td>
				<td>��ӯ</td>
				<td>ת�Ƽ���</td>
				<td>�ɹ��˻�</td>
				<td>���ý��</td>
				<td>�̿�</td>
				<td>��ĩ���</td>
				<td>�ڳ����</td>
				<!--td style="display:none">�����</td-->
				<td>ת������</td>
				<td>��ӯ</td>
				<td>�˿�</td>
				<td>�ɹ��˻�</td>
				<td>���ý��</td>
				<td>�̿�</td>
				<td>��ĩ���</td>
				<td style="display:none"></td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<!--td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td-->
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
								 <td align='left'>	
									<a tabindex="-1">
									 
										<xsl:value-of select="."/>
									 
									</a>
								 </td>
								</xsl:when>
								<xsl:when test="position()=3">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position() &gt; 3 and position() &lt; 13">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=13">
								 <!--td align='right' style='display:none'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td-->
								</xsl:when>
								<xsl:when test="position() &gt; 13">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								
								<xsl:otherwise>
								 <td align='left'>	
									<xsl:value-of select="."/>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		<tfoot>
 			<tr noWrap='true'>
        <td align="left" style='colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum - 1"/>    
        </xsl:call-template>
  		</tr>
 		</tfoot>
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>