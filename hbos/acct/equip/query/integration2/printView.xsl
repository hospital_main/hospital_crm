<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<root>
    	<thead>
    		<tr noWrap='true'>
    			<td style="fontsize:maintitle;colspan:colcount"></td>
		        <xsl:for-each select="/root/t2head/tr[1]/td[position() &gt; 2]">
				<td style="display:none">
				</td>
			</xsl:for-each>
	  	</tr>
	    	<tr noWrap="true" class="mainHead">
			<xsl:for-each select="/root/t2head/tr[1]/td[position() != 1]">
				<td nowrap="true">
					<xsl:value-of select="."/>
				</td>
			</xsl:for-each>
		</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<xsl:variable name="c" select="position()"/>

					<xsl:if test="/root/t2head/tr[2]/td[$c +1 ] != '2'">
						<td align="left">
							<xsl:value-of select="."/>
						</td>
					</xsl:if>
					<xsl:if test="/root/t2head/tr[2]/td[$c +1 ]='2'">
						<td align="right">
							<xsl:if test=". != '�ϡ�����'">
								<xsl:value-of select="format-number(.,'##,##0.00')"/>
							</xsl:if>
							<xsl:if test=". = '�ϡ�����'">
								<xsl:value-of select="."/>
							</xsl:if>
						</td>
					</xsl:if>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>