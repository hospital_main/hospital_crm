<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<xsl:for-each select="/root/t2head/tr[1]/td">
					<th nowrap="true">
						<xsl:value-of select="."/>
					</th>
				</xsl:for-each>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:variable name="c" select="position()"/>
						<xsl:if test="string-length($c)&gt;0">
							<xsl:if test="substring( /root/t2head/tr[1]/td[$c] ,string-length( /root/t2head/tr[1]/td[$c] ),1)=' '">
								<xsl:if test=". != ''">
								<td align="right">
									<xsl:value-of select="format-number(.,'##,##0.00')"/>
								</td>
								</xsl:if>
								<xsl:if test=". = ''"><td><xsl:text>��</xsl:text></td></xsl:if>
							</xsl:if>
							
							<xsl:if test="substring( /root/t2head/tr[1]/td[$c] ,string-length( /root/t2head/tr[1]/td[$c] ),1)!=' '">
								<xsl:if test=". = ''"><td><xsl:text>��</xsl:text></td></xsl:if>
								<xsl:if test=". != ''">
								<td>
									<xsl:if test="$c=1">
         					  <a href="#">
              			  <xsl:attribute name="onclick" >
              			  	javascript:openDialog('../../fixcard/add/update.html?load=&lt;card_arch_no&gt;<xsl:value-of select="."/>&lt;/card_arch_no&gt;','dialogWidth:1024px;dialogHeight:800px');
      			  			  </xsl:attribute>
        					  </a>
         					</xsl:if>
									<xsl:value-of select="."/>
								</td>
								</xsl:if>
							</xsl:if>
						</xsl:if>
	<xsl:if test="string-length($c)=0">
							<td/>
						</xsl:if>
						</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>