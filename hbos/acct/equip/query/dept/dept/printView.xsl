<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	<xsl:variable name="colSpanNum" select="/root/tbody/tr[1]/td[last()]"/>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum - 4"/>    
        </xsl:call-template>
  		</tr>
  		<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum - 4"/>    
        </xsl:call-template>
  		</tr>
  		<xsl:for-each select="/root/tbody/tr[position()=1]">
	  		<tr noWrap="true" class="mainHead">
					<xsl:for-each select="td">
          	<xsl:choose>
	          	<xsl:when test=" position() = 3 ">
	          		<td rowspan='2' nowrap='true' ><xsl:value-of select="."/></td>
	          	</xsl:when>
	          	<xsl:when test=" position() &gt; 4 and position() &lt; last() - 1 and (position() - 5) mod $colSpanNum = 0 ">
	          		<td nowrap='true' >
	          			<xsl:attribute name="colspan"><xsl:value-of select="$colSpanNum"/></xsl:attribute>
	          			<xsl:value-of select="."/>
	          		</td>
	          	</xsl:when>
	          	<xsl:when test=" position() &gt; 4 and position() &lt; last() - 1 and (position() - 5) mod $colSpanNum != 0 ">
	          		<td style="display:none"></td>
	          	</xsl:when>
	          	<xsl:when test=" position() = last() - 1 ">
	          		<td rowspan='2' nowrap='true' >�ܼ�</td>
	          	</xsl:when>
	          	<xsl:otherwise>
	          		
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
				</tr>
			</xsl:for-each>
			<xsl:for-each select="/root/tbody/tr[position()=2]">
	  		<tr noWrap='true' class='mainHead'>
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test=" position() = 3 ">
          			<td style="display:none"></td>
	          	</xsl:when>
	          	<xsl:when test=" position() &gt; 4 and position() &lt; last() - 1">
	          		<td nowrap='true' ><xsl:value-of select="."/></td>
	          	</xsl:when>
	          	<xsl:when test=" position() = last() - 1">
              	<td style="display:none"></td>
	          	</xsl:when>
	          	<xsl:otherwise>
	          		
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
  		</xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
				<tr>
					<!--td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td-->
					<xsl:for-each select="td">
					
					<xsl:choose>
						
							<xsl:when test="position()=3">
								<td align='left'>	
									<a tabindex="-1">
										<xsl:value-of select="."/>
									</a>
								</td>
							</xsl:when>
						
						
							<xsl:when test="position() &gt; 4 and position() &lt; last()">
								<td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
						
						
							<xsl:otherwise>
							
							</xsl:otherwise>
						
						</xsl:choose>
					</xsl:for-each>
					
				</tr>
			</xsl:for-each>
		</tbody>
		<tfoot>
 			<tr noWrap='true'>
        <td align="left" style='colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum - 4"/>    
        </xsl:call-template>
  		</tr>
 		</tfoot>
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>