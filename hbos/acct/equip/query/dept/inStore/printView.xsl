<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>  
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:11'></td>
				<td style="display:none"></td>
			  <td style="display:none"></td>
			  <td style="display:none"></td>
				<td style="display:none"></td>
			  <td style="display:none"></td>
			  <td style="display:none"></td>
				<td style="display:none"></td>
			  <td style="display:none"></td>
			  <td style="display:none"></td>
				<td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td rowspan="2">类别编码</td>
				<td rowspan="2">类别名称</td>
				<td rowspan="2">期初余额</td>
				<td colspan="4">本期增加</td>
				<td style="display:none"></td>
			  <td style="display:none"></td>
			  <td style="display:none"></td>
				<td colspan="3">本期减少</td>
				<td style="display:none"></td>
			  <td style="display:none"></td> 
				<td rowspan="2">期末余额</td>
			</tr>
			<tr noWrap="true" class="mainHead">
			  <td style="display:none"></td>
			  <td style="display:none"></td>
			  <td style="display:none"></td>
				<td>入库增加</td>
				<td>科室退库</td>
				<td>采购退货</td>
				<td>增加合计</td>
				<td>科室领用</td>
				<td>处置减少</td>
				<td>减少合计</td>
			  <td style="display:none"></td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
								 <td align='left'>	
										<xsl:value-of select="."/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=3">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=4">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=5">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=6">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=7">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=8">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=9">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=10">
								 <td align='right' >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=11">
								 <td align='right' >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=11">
								 <td align='right' style="display:none">	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:otherwise>
								 <td align='left'>	
									<xsl:value-of select="."/>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>

		<tfoot>
			<tr> 
				<td   style="colspan:11">制表人：</td> 
			</tr>		
		</tfoot>		
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>