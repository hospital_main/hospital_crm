<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th>资产代码</th>
        <th>资产名称</th>
        <th>规格</th>
        <th>型号</th>
				<th>数量</th>
				<th>单价</th>
				<th>金额</th>				
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:for-each select="td">
           	<xsl:choose>
	            <xsl:when test="position()=5">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0')"/>
	              </td>
	            </xsl:when>
	          	<xsl:when test="position()=6">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=7">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:when>
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet> 
