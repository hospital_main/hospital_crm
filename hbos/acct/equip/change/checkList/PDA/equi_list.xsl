
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        
				<th>盘点明细号</th>
				<th>资产卡片号</th>
				<th>资产名称</th>
				<th>品牌规格</th>
				<th>资产原值</th>
				<th>购置日期</th>		
				<th>状态</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>       
                        
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' >
	                  <a tabindex='-1' ><xsl:value-of select="."/></a>
			            </td>
                </xsl:when>
                <xsl:when test="position()=5">
                	<td>
                		<xsl:value-of select="format-number(.,'#,##0.00')"/>
                	</td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>  	
	</xsl:template>
</xsl:stylesheet>
