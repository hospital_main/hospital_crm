<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>选择</th>
				<th>合同编号</th>
				<th>合同名称</th>
				<th>签定日期</th>
				<th>供应商</th>
				<th>合同总金额</th>
				<th>合同简介</th>
				<th>币种</th>
				<th>状态</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
		   		       		<td align='center'>
			    	  	  			<input type='radio' name='select' onclick='choose(this)'>
						              <xsl:attribute name="value" >
				                 	 <xsl:value-of select="../pk/id"/>
						      			  </xsl:attribute>
						      			</input>
				   					</td>
					   				<td align='center'>
						   				<a href="#">
						    	    <xsl:attribute name='onclick'>
						    	      javascript:openDialog('equiContractEquip.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;', 'dialogWidth:850px;dialogHeight:650px', result)
						    	    </xsl:attribute>
						    	    <xsl:value-of select="."/>
						    	    </a>
					   				<!--a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('equiContractEquip.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;', 'dialogWidth:850px;dialogHeight:650px', result)
		                </xsl:attribute><xsl:value-of select="."/></a-->
		                </td>
              	</xsl:when>
              	
								<xsl:when test="position()=5 ">
                	<td align="center"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
								
								<xsl:otherwise>
									<td align='center'><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
