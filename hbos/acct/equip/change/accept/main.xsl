<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
  			<th style="display:none"><input type="checkbox"/></th>
  			<th>资产验收单号</th>
  			<th>资产合同编号</th>
  			<th>验收日期</th>
  			<th>摘要</th>
  			<th>需求科室</th>
  			<!--th>验收设备</th>
  			<th>验收配件</th>-->
  			<th>状态</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
  			
	  			<tr>
		  			<td align="center" style="display:none">
		  				<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
		  					<xsl:attribute name="value">
		  						<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		  					</xsl:attribute>
		  				</input>
		  			</td>
		  			<xsl:for-each select="td">
		  				<xsl:choose>
		  					<xsl:when test="position()=1">
		  						<td><a tabindex='-2'><xsl:value-of select="."/></a></td>
		  					</xsl:when>
		  					<xsl:when test="position()=2">
		  						<td>
		  							<a href="#">
		  								<xsl:attribute name="onclick">
		  									javascript:openDialog('../../contract/manager/update.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px;scroll:1',result)
		  								</xsl:attribute>
		  								<xsl:value-of select="."/>
		  							</a>
		  						</td>
		  					</xsl:when>
		  					<!--xsl:when test="position()=6">
		  						<td>
		  							<a href="#">
		  								<xsl:attribute name="onclick">
		  									javascript:openDialog('equip/main.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:850px;dialogHight:450px',result)
		  								</xsl:attribute>
		  								<xsl:value-of select="."/>
		  							</a>
		  						</td>
		  					</xsl:when>
		  					<<xsl:when test="position()=7">
		  						<td>
		  							<a href="#">
		  								<xsl:attribute name="onclick">
		  									javascript:openDialog('fittings/main.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:850px;dialogHight:450px',result)
		  								</xsl:attribute>
		  								<xsl:value-of select="."/>
		  							</a>
		  						</td>
		  					</xsl:when>-->
		  					<xsl:otherwise>
		  						<td>
		  							<xsl:value-of select="."/>
		  						</td>
		  					</xsl:otherwise>
		  				</xsl:choose>
		  			</xsl:for-each>
	  			</tr>
  			
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>