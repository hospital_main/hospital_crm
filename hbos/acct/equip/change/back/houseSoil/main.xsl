
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>退货单号</th>
				<th>退货日期</th>
				<th>往来单位</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>状态</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  noWrap='true'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <td  noWrap='true' >
              <xsl:choose>
                <xsl:when test="position()=1">
                  <a tabindex='-1' href='#'>
		                <xsl:attribute name="onclick" >
		                  updateItem('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
		                </xsl:attribute>
		                <xsl:value-of select="."/></a>
                </xsl:when>
                <!--<xsl:when test="position()=6">
                  <a tabindex="-2">
                    <xsl:attribute name="href" >
                      javascript:openDialog('equi/main.html?&lt;root&gt;&lt;equi_back_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/equi_back_no&gt;&lt;state&gt;<xsl:value-of select="../td[position()=7]"/>&lt;/state&gt;&lt;/root&gt;', 'dialogWidth:850px;dialogHeight:610px', null)
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                  </a>
                </xsl:when>-->
                
                
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
