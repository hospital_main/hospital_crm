<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th width='25'><input type='checkbox'/></th>
        <th>设备编码</th>
        <th>设备名称</th>
        <th>型号规格</th>
				<th>生产厂家</th>
				<th>单位</th>
				<th>单价</th>
				<th>数量</th>
				<th>金额</th>				
      </tr>
    </thead>
 <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1 or position()=6 or position()=7">
                <td>                 
                    <xsl:attribute name="onclick">openDialog('dupdate.html','dialogWidth:400px;dialogHeight:340px', this);</xsl:attribute>
                    <xsl:value-of select="."/>
                </td>
              </xsl:when>
	            <xsl:when test="position()=3 or position()=4 or position()=5 or position()=8 or position()=9">	
	            	<td>
	            		
	            	</td> 
	            </xsl:when>                 
	            <xsl:when test="position()=5 or position()=6 or position()=7 ">
	              <td  align="right" class="moneyCol">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
