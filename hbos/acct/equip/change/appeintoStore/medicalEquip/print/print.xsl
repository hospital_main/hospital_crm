<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>选择</th>
  			<th nowrap='true'>打印项目</th>
  			<th nowrap='true'>选择</th>
  			<th nowrap='true'>打印项目</th>
  		</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr/td">
			<xsl:if test="( position() mod 4 )=1">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;
				</xsl:text>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="( position() mod 4 )=1 or ( position() mod 4 )=3">
				  <xsl:if test="position()=7 or position()=9">
  					<td>
  					<input type="checkbox" TABINDEX='-1' style='font-size:8px;'>
  					<xsl:attribute name="value">
  						<xsl:value-of select="."/>
  					</xsl:attribute>
  					</input>
  					</td>
					</xsl:if>

				  <xsl:if test="position()!=7 and position()!=9">
  					<td>
  					<input type="checkbox" TABINDEX='-1' style='font-size:8px;' checked="true">
  					<xsl:attribute name="value">
  						<xsl:value-of select="."/>
  					</xsl:attribute>
  					</input>
  					</td>
					</xsl:if>

				</xsl:when>
				<xsl:otherwise>
					<td><xsl:value-of select="."/></td>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="(position() mod 4)=0">
				<xsl:text disable-output-escaping="yes">
					&lt;/tr&gt;
				</xsl:text>
			</xsl:if>
		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>