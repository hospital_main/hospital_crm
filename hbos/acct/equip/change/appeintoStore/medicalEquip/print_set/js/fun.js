function submitData(obj){
	var res=checkInputs();
	if(res==null||res=="")
		return false;
	window.xmlhttp.post(obj.name, res,"")
	var str = window.xmlhttp._object.responseText
	if (!window.doMsg(str,"")) {
	  return false;
	}
	//sysDictsUnitinfoCopyParas_select.click();
	return true;
}
function checkInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	var res="";
	for(var i=0;i<trs.length;i++){
		if(trs[i].getAttribute("_editParaType")==null)
			continue;
		if(trs[i]._editDataValue==trs[i]._editInputValue)
			continue;
		res+="<record>";
		if(checkValue(trs[i],trs[i]._editParaType,trs[i]._editInputValue))
			res+=trs[i]._editPk+"<value>"+trs[i]._editInputValue+"</value>";
		else{
			return null;
		}
		res+="</record>";
	}
	return res;
}
function checkValue(tr,type,value){
	var msg=tr.cells[0].innerText+" "+tr.cells[1].innerText+" :要求是" ;
	var res=true;
	if(type=="1"&&value!=""){
		msg+=" 文本 ";
		res=true;
	}else if(type=="2"&&value!=""){
		msg+=" 正整数 ";
		var n=parseInt(value,10);
		if(isNaN(n)||n<0||n!=value){
			res=false;
		}
	}else  if(type=="3"&&value!=""){
		msg+=" 数值 ";
		var n=parseFloat(value,10);
		if(isNaN(n)||n!=value){
			res=false;
		}
	}else if(type=="4"&&value!=""){
		msg+=" 列行坐标 ";
		var vv=value.split(",");
		if(vv.length!=2)
			res=false;
		else{
			var v0=parseInt(vv[0],10);
			var v1=parseInt(vv[1],10);
			if(isNaN(v0)||isNaN(v1)||v0<0||v1<0||v0!=vv[0]||v1!=vv[1])
				res=false;
		}
	}else if(type=="5"){
	
	}
	if(res==false)
		alert(msg);
	return res;
}
function initInputs(tableObj){
	var table=document.getElementById("_mainDataTable");
	if(tableObj)
		table=tableObj;
	var trs=table.getElementsByTagName("tr");
	for(var i=0;i<trs.length;i++)
		initTrInputs(trs[i]);
}
function initTrInputs(tr){
	var pt=tr.getAttribute("_editParaType");
	var dv=tr.getAttribute("_editDataValue");
	if(pt==null)
		return ;
	var inp="";
	var id=getJsGuid();
	var tag="";
	var chk="";
	tr._editInputId=id;
	if(pt=="1"||pt=="2"||pt=="3"||pt=="4"){
		inp="<input type='text' id='"+id+"' name='"+id+"' value='"+dv+"'/>";
		tag="input";
	}else if(pt=="5"){
		var ops=tr.cells[3].innerText.split("\/");
		inp="<select id='"+id+"' name='"+id+"' >";
		tag="select"
		for(var i=0;i<ops.length;i++){
			if(dv==ops[i])
				chk=" selected ";
			else
				chk="";
			inp+="<option "+chk+" value='"+ops[i]+"'>"+ops[i]+"</option>";
		}
		inp+="</select>";
	}else
		return ;
	tr.cells[2].innerHTML=inp;
	var inpus=tr.cells[2].getElementsByTagName("input");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	inpus=tr.cells[2].getElementsByTagName("select");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	
}
function setInputChange(tr,inp){
	inp.onblur=
	inp.onclick=
	inp.onkeyup=function(){tr._editInputValue=this.value};
}