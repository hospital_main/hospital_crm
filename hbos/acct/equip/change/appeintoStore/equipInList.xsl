<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th width='25'><input type='checkbox'/></th>
        <th nowrap='true'>设备编码</th>
      	<th nowrap='true'>设备名称</th>
        <th nowrap='true'>型号规格</th>
      	<th nowrap='true'>产地厂家</th>
      	<th nowrap='true'>单位</th>
      	<th nowrap='true'>单价</th>
      	<th nowrap='true'>数量</th>
      	<th nowrap='true'>金额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
            	
              <xsl:when test="position()=1">
                <td>
                  <a href='#'>
                    <xsl:attribute name="onclick">openDialog('equipInUpdateNew.html','dialogWidth:800px;dialogHeight:600px', this);</xsl:attribute>
                    <xsl:value-of select="../td[position()=4]"/>
                  </a>
                </td>
              </xsl:when>
               <xsl:when test="position()=2">
                <td>
                    <xsl:value-of select="../td[position()=25]"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=3">
                <td>
                    <xsl:value-of select="../td[position()=27]"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=4">
                <td>
                    <xsl:value-of select="../td[position()=28]"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=5">
                <td>
                    <xsl:value-of select="../td[position()=26]"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=6">
                <td align='right' class='moneyCol'>
	                <xsl:value-of select="format-number(../td[position()=12],'#,##0.00')"/>  
	              </td>
              </xsl:when>
              <xsl:when test="position()=7">
                <td>
                    <xsl:value-of select="../td[position()=13]"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=8">
                
                <td align='right' class='moneyCol'>
	                <xsl:value-of select="format-number(../td[position()=29],'#,##0.00')"/>  
	              </td>
              </xsl:when>
              <xsl:otherwise>
              	<td style="display:none">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


