<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>维保合同付款</th>
				<th>维保合同设备</th>
				<th>原始合同号</th>
				<th>合同名称</th>
				<th>签定日期</th>
				<th>开始日期</th>
				<th>结束日期</th>
				<th>上次付款日期</th>
				<th>供应商</th>
				<th>对方负责人</th>
				<th>合同总金额</th>
				<!--
				<th>分期付款</th>
				<th>附带文档</th>
				-->
				<th>状态</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" style="display:none">
						<input type="text" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
									<td><a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('pay.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/contract_no&gt;', 'dialogWidth:750px;dialogHeight:550px', result)
		                </xsl:attribute><xsl:value-of select="."/></a></td>
								</xsl:when>
								<xsl:when test="position()=2">
									<td><a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('card.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/contract_no&gt;', 'dialogWidth:750px;dialogHeight:550px', result)
		                </xsl:attribute><xsl:value-of select="."/></a></td>
								</xsl:when>
								<xsl:when test="position()=11 ">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
