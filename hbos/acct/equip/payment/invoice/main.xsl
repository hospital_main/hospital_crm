<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>发票单号</th>
				<th>原始发票号</th>
				<th>开票日期</th>
				<th>供应商</th>
				 
				<th>资产名称</th>
				<th>规格</th>
				<th>型号</th>
				<th>入库单号</th>
				<th>发票金额</th>
				<th>开票金额</th>
				<th>付款金额</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>状态</th>
				<th>备注</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
			<xsl:for-each select="/root/tbody/tr">
				<tr>    
		<xsl:if test="td[1] = '合计'">
        		       <td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">			
					<td align='center' style='display:none'>
				            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
				              <xsl:attribute name="value" >
				                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
				      			  </xsl:attribute>
				    			  </input>
				          </td>
          </xsl:if>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
	              <xsl:if test=". = '合计'">
				        		<td noWrap='true'><xsl:value-of select="."/></td>
				        	</xsl:if>
				        	 <xsl:if test=". != '合计'">
	              
	              <td>
                  <a href="#">
									  <xsl:attribute name="onclick" >
									    javascript:openDialog('update.html?load=&lt;id&gt;<xsl:value-of select="../pk"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
									  </xsl:attribute>
									  <xsl:value-of select="."/>
									</a>
                </td>
                </xsl:if>
	            </xsl:when>
							<xsl:when test="position()=9">
	              <td align="right" class="moneyCol">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=10">
	              <td align="right" class="moneyCol">
                  <xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=11">
	              <td align="right" class="moneyCol">
                  <xsl:value-of select="."/>
                </td>
	            </xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 		
	  </tbody> 	
	</xsl:template>
</xsl:stylesheet>