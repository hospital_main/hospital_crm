
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th width='25'><input type='checkbox'/></th>
				<th>增加单号</th>
				<th>增加日期</th>
				<th>业务类型</th>
				<th>往来单位</th>
				<th>资产名称</th>
				<th>类别</th>
				<th>规格</th>
				<th>型号</th>
				<th>库房</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>状态</th>
				<th>金额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:if test="td[1] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">
	          <td align='center'  noWrap='true'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
	    			  </input>
	          </td>
	        </xsl:if>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
	                <xsl:if test=". = '合计'">
				        		<td noWrap='true'><xsl:value-of select="."/></td>
				        	</xsl:if>
				        	 <xsl:if test=". != '合计'">
				            <td  noWrap='true' >
				            
		                <a tabindex='-1'>
		                <xsl:attribute name="onclick">
		                openPage('&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;');
		                </xsl:attribute>
		                <xsl:attribute name="href" >
		                  #
		                </xsl:attribute><xsl:value-of select="."/></a>
				            </td>
				        	</xsl:if>
                </xsl:when>
                <xsl:when test="position()=13">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
