<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th nowrap='true'>卡片编码</th>
				<th nowrap='true'>资产名称</th>
				<th nowrap='true'>类别</th>
				<th nowrap='true'>入库日期</th>
				<th nowrap='true'>规格</th>
				<th nowrap='true'>型号</th>
				<th nowrap='true'>数量</th>
				<th nowrap='true'>供应商</th>
				<th nowrap='true'>序列号</th>
				<th nowrap='true'>入库单号</th>
				<th nowrap='true'>资产用途</th>
				<th nowrap='true'>科室信息</th>
				<th nowrap='true'>仓库</th>
				
				<th nowrap='true'>合同金额</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
			<xsl:for-each select="/root/tbody/tr">
				<tr>    
		<xsl:if test="td[1] = '合计'">
        		       <td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">			
					<td align='center' style='display:none'>
				            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
				              <xsl:attribute name="value" >
  							<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
  						</xsl:attribute>
				    			  </input>
				          </td>
          </xsl:if>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
	                                       <xsl:if test=". = '合计'">
				        		<td noWrap='true'><xsl:value-of select="."/></td>
				        	</xsl:if>
				        	 <xsl:if test=". != '合计'">
				            <td  noWrap='true' >
				            
		                <a tabindex='-1'>
		                <xsl:attribute name="onclick">
		                openPage('&lt;card_arch_no&gt;<xsl:value-of select="."/>&lt;/card_arch_no&gt;');
		                </xsl:attribute>
		                <xsl:attribute name="href" >
		                  #
		                </xsl:attribute><xsl:value-of select="."/></a>
				            </td>
				        	</xsl:if>
	            </xsl:when>
							<xsl:when test="position()=10">
	              <td >
                  <xsl:value-of select="."/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=11">
	              <td >
                  <xsl:value-of select="."/>
                </td>
	            </xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 		
	  </tbody> 	
	</xsl:template>
</xsl:stylesheet>