<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
		  <thead>
		  	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:colcount;align:left"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:1;align:left"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:1;align:left"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>         
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:1;align:left"/>
		      </tr>
		  		<tr noWrap="true" class="mainHead">
		        <td>入库单号</td>
						<td>仓库</td>
						<td>入库日期</td>
						<td>采购员</td>
						<td>应开票据金额</td>
		  		</tr>
	  	</thead>
	  	<tbody>
	  	<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
      <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	              <xsl:choose>
	                <xsl:when test="position()=1">
              		</xsl:when>
	                <xsl:when test="position()=6 or position()=7 or position()=8 ">
	                  <td class='numberText'>
  	                		<xsl:value-of select="format-number(.,'#,##0.00')"/>  
  	              	</td>
	                </xsl:when>
	                <xsl:otherwise>
	                  <td><xsl:value-of select="."/></td>
	                </xsl:otherwise>
	              </xsl:choose>
	          </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
	  	<tfoot>
	  		<tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
	      	<td style="display:none"/>
	      	<td noWrap="true" style="fontsize:foot;colspan:2;align:left"/>
	      	<td style="display:none"/>
	      	<td noWrap="true" style="fontsize:foot;colspan:1;align:left"/>
		    </tr>
	  	</tfoot>
	 </root> 	
	</xsl:template>
</xsl:stylesheet>