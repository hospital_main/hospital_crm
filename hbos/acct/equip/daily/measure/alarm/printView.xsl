<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
					<td>计量计划编码</td>
					<td>资产卡片号</td>
					<td>资产名称</td>
					<td>规格</td>
					<td>型号</td>
					<td>产地厂家</td>
					<td>计量类别</td>
	  			<td>使用科室</td>
					<td>计划计量日期</td>
					<td>计量说明</td>
	  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	            <td>
	              <xsl:choose>
	                <xsl:when test="position()=1">
	                  <xsl:value-of select="."/>
	                </xsl:when>
	                <xsl:otherwise>
	                  <xsl:value-of select="."/>
	                </xsl:otherwise>
	              </xsl:choose>
	            </td>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>