<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>选择</th>
      	<th nowrap='true'>质控项目</th>
      	<th nowrap='true'>是否执行</th>
      	<th nowrap='true'>是否正常</th>
      	<th nowrap='true'>质控说明</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <xsl:if test="td[6] = '0' ">
              <input type='checkbox' style='font-size:8px;' >
                <xsl:attribute name="value" >
                  <xsl:value-of select="td[1]"/>
                </xsl:attribute>
              </input>
            </xsl:if>
            <xsl:if test="td[6] = '1' ">
              <input type='checkbox' style='font-size:8px;' checked='true' >
                <xsl:attribute name="value" >
                  <xsl:value-of select="td[1]"/>
                </xsl:attribute>
              </input>
            </xsl:if>
          </td>
          <xsl:for-each select="td[position() &gt; 1 and position() &lt; 6]">
            <td align="center">
              <xsl:value-of select="."/>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

