<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th nowrap='true'>维修记录号</th>
				<th nowrap='true'>资产卡片号</th>
				<th nowrap='true'>资产名称</th>
				<th nowrap='true'>规格</th>
				<th nowrap='true'>型号</th>
				<th nowrap='true'>制单日期</th>
				<th nowrap='true'>维修工时</th>
				<th nowrap='true'>维修级别</th>
				<th nowrap='true'>供货单位</th> 
        <th nowrap='true'>维修总金额</th>
				<th nowrap='true'>状态</th>
        <th nowrap='true'>制单人</th>
        <th nowrap='true'>审核人</th>
      </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        <xsl:if test="td[2] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[2] != '合计'">
		          <td align='center'  style='display:none'>
		            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
		              <xsl:attribute name="value" >
		                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		      			  </xsl:attribute>
		    			  </input>
		          </td>
           </xsl:if>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
              </xsl:when>  
              
           <!--xsl:when test="position()=2">
                <td align='center'><a href='#' onclick='mopen(this)'><xsl:value-of select="."/></a></td>
              </xsl:when-->
           <xsl:when test="position()=7 ">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
           </xsl:when>
           <xsl:when test="position()=10 ">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
           </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

