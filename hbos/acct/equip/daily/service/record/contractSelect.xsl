<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>选择</th>
      	<th nowrap='true'>合同编号</th>
      	<th nowrap='true'>合同名称</th>
      	<th nowrap='true'>签订日期</th>
      	<th nowrap='true'>供应商</th>
      	<th nowrap='true'>合同总金额</th>
      	<th nowrap='true'>合同摘要</th>
      	<th nowrap='true'>付款</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='radio' style='font-size:8px;' name="a" onclick="selAccount(this)">
              <xsl:attribute name="contract_no" ><xsl:value-of select="td[1]"/></xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td[position()>=1]">
            <!--<td>
              <xsl:value-of select="."/>
            </td>-->
            <xsl:choose>
            <xsl:when test="position()=5 ">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:when>
            <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
            </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

