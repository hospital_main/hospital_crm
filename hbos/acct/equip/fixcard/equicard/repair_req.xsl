<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th>ѡ��</th>
        <th></th>
        <th>ά�����뵥�ݺ�</th>
        <th>���벿��</th>
				<th>��������</th>				
				<th>������</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>
          <td align='center'>
            <input type='radio' name='select' onclick='OptionChose(this)'>
            <xsl:attribute name="value"><xsl:value-of select="td[1]"/></xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=1">
            		<td align="center" onclick="changeDenotation(this)" style="cursor:hand" >
            		  <xsl:attribute name="id">row<xsl:value-of select="$rowindex"/></xsl:attribute>
	            		<xsl:attribute name="apply_no">
	            			<xsl:value-of select="."/>
	            		</xsl:attribute>+</td>
              </xsl:when>
              <xsl:otherwise>
                <td align="center">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			<tr style="display:none">
  				<xsl:attribute name="id">annexrow<xsl:value-of select="$rowindex"/></xsl:attribute>
  				<td align='center'>
          </td>
          <td>
          </td>
          <td colspan="4" ><xsl:attribute name="id">revelation<xsl:value-of select="$rowindex"/></xsl:attribute>
          </td>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


