<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			
		   
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
  				<td align='center'  style='display:none'>
  					<input type='checkbox' TABINDEX='-1'>
  						<xsl:attribute name="value" >
  							<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
  						</xsl:attribute>
  						<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
  					</input>
  				</td>
				<xsl:for-each select="td[position()!=1]">   
          <xsl:choose>
            <xsl:when test="position()=1">
              <td>
                <a tabindex='-1'><xsl:value-of select="."/></a>
              </td>
            </xsl:when>
            <xsl:otherwise>  
     					<td>
    	  	  		<xsl:value-of select="."/>
           		</td>
           	</xsl:otherwise>
          </xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>


