<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>选择</th>
      	<th nowrap='true'>申请单据号</th>
      	<th nowrap='true'>资产卡片号</th>
      	<th nowrap='true'>是否附件</th>
      	<th nowrap='true'>资产编码</th>
      	<th nowrap='true'>资产名称</th>
      	<th nowrap='true'>申请日期</th>
      	<th nowrap='true'>故障说明</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='radio' style='font-size:8px;' name="a" onclick="selAccount(this)">
              <xsl:attribute name="detail_id" ><xsl:value-of select="td[1]"/></xsl:attribute>
              <xsl:attribute name="card_no" ><xsl:value-of select="td[3]"/></xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td[position()>1]">
            <td>
              <xsl:value-of select="."/>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

