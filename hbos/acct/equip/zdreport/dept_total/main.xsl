<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th rowspan="2" valign="middle">���ұ���</th>
        <th rowspan="2" valign="middle" >��������</th>
		    <th colspan="4">ԭֵ</th>
		    <th colspan="4">�ۼ��۾�</th>
		    <th colspan="2" valign="middle" >��ֵ</th>
  		</tr>
  		<tr noWrap="true" class="mainHead"> 
  			<th valign="middle">�ڳ�</th>
		    <th valign="middle">�跽</th>
		    <th valign="middle">����</th>
		    <th valign="middle">���</th>	
		    <th valign="middle">�ڳ�</th>
		    <th valign="middle">�跽</th>
		    <th valign="middle">����</th>
		    <th valign="middle">���</th>	
		    <th valign="middle">�ڳ�</th>
		    <th valign="middle">��ĩ</th>		    		    
		  </tr>  		
  	</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      <xsl:variable name='CurTrPos' select='position()'/>
        <tr>          
          <xsl:for-each select="td">
          	<xsl:choose>							
              <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12">
                <td align='right'>
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              
              <xsl:otherwise>
                <td align='left'><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>