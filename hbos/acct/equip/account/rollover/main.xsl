<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>年度</th>
				<th>月份</th>
				<th>起始日期</th>
				<th>结束日期</th>
				<th>是否计提</th>
				<th>是否分摊</th>
				<th>是否结转</th>
				<th>结转时间</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
             <td align="center">
               <xsl:value-of select="."/>
             </td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>