<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>序号</th>
				<th>配件名称</th>
				<th>设备名称</th>
				<th>是否附件</th>
				<th>规格型号</th>
				<th>产地厂家</th>
				<th>使用科室</th>
				<th>数量</th>
				<th>计量单位</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=10"></xsl:when> 
                <xsl:otherwise>
                  <td >
	                  <p>
	                  <font>
		                  <xsl:attribute name="size">2.5</xsl:attribute>
		                  <xsl:if test="../td[10]='1'">
					        			<xsl:attribute name="style">color:green</xsl:attribute>
					        		 </xsl:if>
					        		<xsl:value-of select="."/>
	                  </font>
	                  </p>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>