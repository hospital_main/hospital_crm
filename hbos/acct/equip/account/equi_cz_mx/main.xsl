<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
  			<th>序号</th>
  			<th>部门/仓库编码</th>				
				<th>部门/仓库名称</th>		
				<th>卡片编码</th>				
				<th>资产名称</th>				
				<th>资产类别</th>				
				<th>规格</th>
				<th>型号</th>				
				<th>增加日期</th>
				<th>处置日期</th>				
				<th>原值</th>
				<th>累计折旧</th>				
				<th>现值</th>				
				<th>处理收入</th>				
				<th>处置金额</th>				
				<th>业务类型</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<td><xsl:value-of select="position()"/></td>
          <xsl:for-each select="td">
              <xsl:choose>							
              <xsl:when test="position()=10 or position()=11 or position()=12 or position()=13 or position()=14">
                <td align='right'>
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>              
              <xsl:otherwise>
                <td align='left'><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>

