<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>合同编号</th>
				<th>合同名称</th>
				<th>原始合同编号</th>
				<th>签订日期</th>
				<th>付款期号</th>
				<th>合同付款金额</th>
				<th>合同结算方式</th>
				<th>未付金额</th>
				<th>付款单号</th>
				<th>付款日期</th>
				<th>付款金额</th>
				<th>供应商</th>
				  
			</tr>
			 
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					 
					<xsl:for-each select="td">
						<td>
							<xsl:choose>
								<xsl:when test="position()=1">
									<a tabindex="-1">
										<xsl:value-of select="."/>
									</a>
								</xsl:when>
								<xsl:when test="position()=6 or position()=8 or position()=11">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
