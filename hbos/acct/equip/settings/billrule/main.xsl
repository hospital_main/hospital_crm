<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
  		<xsl:variable name="self" select="/root/tbody/tr[1]/td[8]"/>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>单据名称</th>
				<xsl:if test="$self='0'">
				  <th nowrap='true'>标准前缀</th>
				</xsl:if>
				<xsl:if test="$self!='0'">
				  <th nowrap='true'>自定义前缀</th>
				</xsl:if>
				<th nowrap='true'>月结是否初始化</th>
				<th nowrap='true'>使用归属前缀</th>
				<th nowrap='true'>使用年前缀</th>
				<th nowrap='true'>使用月前缀</th>
				<th nowrap='true'>使用日前缀</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:attribute name="_editPk"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
				<xsl:attribute name="_change">0</xsl:attribute>
				<xsl:attribute name="_self"><xsl:value-of select="td[8]"/></xsl:attribute>
				<xsl:attribute name="_pref"><xsl:value-of select="td[2]"/></xsl:attribute>
				<xsl:attribute name="_cyc"><xsl:value-of select="td[3]"/></xsl:attribute>
				<xsl:attribute name="_att"><xsl:value-of select="td[4]"/></xsl:attribute>
				<xsl:attribute name="_year"><xsl:value-of select="td[5]"/></xsl:attribute>
				<xsl:attribute name="_month"><xsl:value-of select="td[6]"/></xsl:attribute>
				<xsl:attribute name="_day"><xsl:value-of select="td[7]"/></xsl:attribute>
				
				<xsl:for-each select="td[position()&lt;8]">   
						<td>
			  			<xsl:value-of select="."/>  
						</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

