<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
	      <th noWrap="true" width="30%">资产编码</th>
				<th noWrap="true" width="60%">资产名称</th>
				<th style="display:none" width="10%"><input type="checkbox"/></th>
       </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
	          <xsl:for-each select="td[position() &lt; 3]">
	             <td><xsl:value-of select="."/></td>
	          </xsl:for-each>
	          <td align='center'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      	      </xsl:attribute>
	    	   </input>
	          </td>			
  	</tr>
  			
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


