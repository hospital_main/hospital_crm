<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style='display:none' width='25'><input type='checkbox'/></th>
				<th>申请标题名称</th>
				<th>申请单编号</th>
				<th>购置年度</th>
				<th>申请科室</th>
				<th>申请人</th>
				<th>申请人联系方式</th>
				<th>申请日期</th>
				<th>资产/附件</th>
				<th>名称</th>
				<th>规格</th>
				<th>型号</th>
				<th>计量单位</th>
				<th>生产厂家</th>
				<th>申请数量</th>
				<th>建议单价</th>
				<th>资金来源</th>
				<th>预算名称</th>
				<th>需求日期</th>
				<th>希望到货日期</th>
				<th>备注</th>
				<th>制单人</th>
				<th>计划号</th>
				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  noWrap='true'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
			   				<td align='left'>
				    	    <xsl:value-of select="."/>
                </td>
            	</xsl:when>
							<xsl:when test="position()=14 ">
              	<td align="right"><xsl:value-of select="format-number(.,'#,##0')"/></td>
           		</xsl:when>
           		<xsl:when test="position()=15 ">
              	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
           		</xsl:when>
							<xsl:otherwise>
								<td align='left'><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
