<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
       	<th noWrap="true" >合同编号</th>
	      <th noWrap="true">付款期号</th>
				<th noWrap="true">发票号 </th>
				<th noWrap="true">往来单位</th>
				<th noWrap="true">应付日期</th>
				<th noWrap="true">付款方式</th>
				<th noWrap="true">货币种类</th>
				<th noWrap="true">应付金额</th>
				<th noWrap="true">已付金额</th>
				<th noWrap="true">状态</th>
          		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          
          <xsl:for-each select="td">
            <xsl:choose>

                 <xsl:when test="position()=1">
			            <td  noWrap='true' >
		              <a tabindex="-2">
		                <xsl:attribute name="href">
		                  javascript:view('payment/contractView.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;')
		                </xsl:attribute><xsl:value-of select="."/></a>
			            </td>

                </xsl:when>
                  <xsl:when test="position()=2">
			            <td  noWrap='true' >
		              <a tabindex="-2">
		                <xsl:attribute name="href">
		                  javascript:viewPAY('payment/payPromiseView.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/id&gt;')
		                </xsl:attribute><xsl:value-of select="."/></a>
			            </td>

                </xsl:when>
                <xsl:when test="position()=3">
			            <td  noWrap='true' >
		              <a tabindex="-2">
		                <xsl:attribute name="href">
		                  javascript:viewInvoice('payment/invoiceView.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=3]"/>&lt;/id&gt;')
		                </xsl:attribute><xsl:value-of select="."/></a>
			            </td>
                </xsl:when>

              <xsl:when test="position() = 8 or position() = 9">
                <td align='right' class='moneyCol'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	              </td>
              </xsl:when>              
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


