<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
       	<th noWrap="true">资产卡片号</th>
	      <th noWrap="true">资产名称</th>
				<th noWrap="true">规格</th>
				<th noWrap="true">型号</th>
				<th noWrap="true">计量单位</th>
				<th noWrap="true">使用科室</th>
				<th noWrap="true">转正方式</th>
				<th noWrap="true">已入账</th>
				<th noWrap="true">转正数量</th>
				<th noWrap="true">转正日期</th>
      </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          
          <xsl:for-each select="td">
            <xsl:choose>

                 <xsl:when test="position()=1">
			            <td  noWrap='true' >
		              	<xsl:value-of select="."/>
			            </td>

                </xsl:when>
                  <xsl:when test="position()=2">
                  <td  noWrap='true' >
			            	<xsl:value-of select="."/>
			            </td>

                </xsl:when>
                <xsl:when test="position()=3">
			            <td  noWrap='true' >
		              	<xsl:value-of select="."/>
			            </td>
                </xsl:when>

              <xsl:when test="position() = 8 or position() = 9">
                <td align='right' class='moneyCol'>
	                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
	              </td>
              </xsl:when>              
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


