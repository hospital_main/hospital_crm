<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
                   <th style='display:none'><input type='checkbox'/></th>
                                <th noWrap="true">申购单号</th>
				<th noWrap="true">设备编码</th>
				<th noWrap="true">设备名称</th>
				<th noWrap="true">购置数量</th>
				<th noWrap="true">需人民币</th>
				<th noWrap="true">审批步骤</th>
				<th noWrap="true">是否通过</th>
				<th noWrap="true">执行状态</th>
				<th noWrap="true">提交日期 </th>
				<th noWrap="true">提交人</th>
				<th noWrap="true">附带文档</th>
				<th noWrap="true">期望日期</th>
				
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	     
        <tr >
					         <td align='center'  noWrap='true'  style='display:none'>
					            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
					              <xsl:attribute name="value" >
					                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					      	      </xsl:attribute>
					    	   </input>
					          </td>
					          <xsl:for-each select="td">
					              <xsl:choose>
					 
							   <xsl:when test="position()=1">
				             <td  noWrap='true' >
		                  <a tabindex='-1'><xsl:value-of select="."/></a>
				             </td>
                  </xsl:when>
					          <xsl:when test="position()=5">
									            <td align='right'>
						              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
													    </td>
						        </xsl:when>
						        <xsl:when test="position()=6">
													<td  >
													<a href="#">
													<xsl:attribute name="onclick" >
													   javascript:openDialog('../step/update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:400px;overflow:auto;', '','true')
													</xsl:attribute>
													<xsl:value-of select="."/>
													</a>　
													</td>
							</xsl:when>
						        
						         <xsl:when test="position()=8">
													<td  >
													<a href="#">
													<xsl:attribute name="onclick" >
													   javascript:openDialog('../exec/update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:400px;overflow:auto;', '','true')
													</xsl:attribute>
													<xsl:value-of select="."/>
													</a>　
													</td>
							 </xsl:when>
							 <xsl:when test="position()=11">
														<td><a tabindex="-1">
															<xsl:attribute name="href">
							                  javascript:openDialog('../contractdoc_main.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/contract_no&gt;', 'dialogWidth:750px;dialogHeight:550px', result)
							                </xsl:attribute>查看</a></td>
							</xsl:when>
							 <xsl:when test="position()=13">
													<td align='right' style="display:none">
														<xsl:value-of select="."/>
													</td>
													</xsl:when>
							<xsl:otherwise>
								            <td  noWrap='true' >
							                  <xsl:value-of select="."/>
								            </td>
					                </xsl:otherwise>
					              </xsl:choose>
					  			  </xsl:for-each>
  	 </tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>




