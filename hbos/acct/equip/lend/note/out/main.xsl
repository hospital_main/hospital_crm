<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		 <tr noWrap='true' class='mainHead'>
  		 		<th nowrap='true'>卡片编码</th>
					<th nowrap='true'>资产名称</th>
          <th nowrap='true'>规格</th>
					<th nowrap='true'>型号</th>
					<th nowrap='true'>责任科室</th>	
          <th nowrap='true'>合同号</th>
					<th nowrap='true'>业务类型</th>
					<th nowrap='true'>租借单位</th>
					<th nowrap='true'>开始日期</th>
					<th nowrap='true'>结束日期</th>
					<th nowrap='true'>资产原值</th>
					<th nowrap='true'>折旧年限</th>				
					<th nowrap='true'>折旧金额</th>					
					<th nowrap='true'>净值</th>
     		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">            
            <xsl:choose>
              <!-- xsl:when test="position()=1">
								<td><a tabindex="-2">
											<xsl:attribute name="href">
		                  javascript:openDialog('../../../fixcard/maintain/update.html?load=&lt;card_arch_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/card_arch_no&gt;', 'dialogWidth:800px;dialogHeight:600px', result)
		                	</xsl:attribute><xsl:value-of select="."/>
		                </a>
		            </td>
              </xsl:when>  
              <xsl:when test="position()=6">
								<td><a tabindex="-2">
											<xsl:attribute name="href">
		                  javascript:openDialog('../../out/contract/update.html?load=&lt;lend_no&gt;<xsl:value-of select="../td[position()=6]"/>&lt;/lend_no&gt;', 'dialogWidth:800px;dialogHeight:600px', result)
		                	</xsl:attribute><xsl:value-of select="."/>
		                </a>
		            </td>
              </xsl:when -->  
              <xsl:when test="position()=11 ">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             	</xsl:when>
             	<xsl:when test="position()=13 ">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             	</xsl:when>
             	<!--xsl:when test="position()=14 ">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             	</xsl:when-->
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

