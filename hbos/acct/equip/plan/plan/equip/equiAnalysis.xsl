<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		 <tr noWrap='true' class='mainHead'>
				<!--th noWrap="true" >序号</th-->
	      <th noWrap="true" rowspan='2' valign='middle'>资产编码</th>
				<th noWrap="true" rowspan='2' valign='middle'>资产名称</th>
				<th noWrap="true" rowspan='2' valign='middle'>资产卡片号</th>
				<th noWrap="true" rowspan='2' valign='middle'>规格</th>
				<th noWrap="true" rowspan='2' valign='middle'>型号</th>
        <th noWrap="true" rowspan='2' valign='middle'>资产原值</th>
        <th noWrap="true" colspan='4' valign='middle'>费用</th>
        <th noWrap="true" colspan='3' valign='middle'>收益</th>
        <th noWrap="true" rowspan='2' valign='middle'>投入/产出比率</th>
       </tr>
       <tr noWrap='true' class='mainHead'>
        <th noWrap="true">保养费用</th>
        <th noWrap="true">维修费用</th>
        <th noWrap="true">检测计量费用</th>
        <th noWrap="true">累计折旧</th>
        <th noWrap="true">运行机时</th>
        <th noWrap="true">使用频率</th>
        <th noWrap="true">收入</th>
       </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          
          <xsl:for-each select="td">
          <xsl:choose>
              
                <xsl:when test="position()=6 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
             		<xsl:when test="position()=7 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when> 
             		<xsl:when test="position()=8 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when> 
             		<xsl:when test="position()=9 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when> 
             		<xsl:when test="position()=10 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when> 
             		<xsl:when test="position()=11 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when> 
             		<xsl:when test="position()=12 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when> 
             		<xsl:when test="position()=13 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when> 
             		<xsl:when test="position()=14 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/>%</td>
             		</xsl:when> 
             		<!--xsl:when test="position()=7 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
             		<xsl:when test="position()=8 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
             		<xsl:when test="position()=9 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
             		<xsl:when test="position()=10 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when-->
	              <xsl:otherwise>
	                <td align="center"><xsl:value-of select="."/></td>
	              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


