<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style=''><input type='checkbox'/></th>
				<th noWrap="true" >计划号</th>
	      <th noWrap="true">购置年月</th>
	      <th noWrap="true">计划类型</th>
	      <th noWrap="true">编制人</th>
				<th noWrap="true">编制日期 </th>
				<th noWrap="true">摘要</th>
				<th noWrap="true">计划总额</th>
        <th noWrap="true">资产金额</th>
				<!--th noWrap="true">附件金额</th-->
				<!--<th noWrap="true">附带文档</th>-->
				<th noWrap="true">审核人</th>
				<th noWrap="true">审核日期</th>
        <th noWrap="true">状态</th>
     </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style=''>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>

               <xsl:when test="position()=1">
			            <td  noWrap='true' >
		              <a tabindex="-1">
		               <!--xsl:value-of select="."/></a-->
		               <xsl:attribute name="href" >
		                  javascript:view('update.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;')
		                </xsl:attribute><xsl:value-of select="."/></a>
			            </td>

                </xsl:when>
                 <xsl:when test="position()=7 ">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		 </xsl:when>
             		 <xsl:when test="position()=8 ">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		 </xsl:when>
             		 <xsl:when test="position()=9 ">
                <!--td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td-->
             		 </xsl:when>
                 <!--<xsl:when test="position()=7">
			            <td  noWrap='true' >
		              <a tabindex="-2">
		                <xsl:attribute name="href" >
		                  javascript:view('equip/main.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;')
		                </xsl:attribute>管理</a>
			            </td>

                </xsl:when>
                  <xsl:when test="position()=8">
			            <td  noWrap='true' >
		              <a tabindex="-2">
		                <xsl:attribute name="href" >
		                  javascript:view('appe/main.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;')
		                </xsl:attribute>管理</a>
			            </td>

                </xsl:when>
              <xsl:when test="position()=9">
                 <td  noWrap='true' >
		              <a tabindex="-2">
		                <xsl:attribute name="href" >
		                  javascript:view('doc/main.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;')
		                </xsl:attribute>管理</a>
			            </td>

              </xsl:when>-->

              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


