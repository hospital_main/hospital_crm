<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
		  <tr noWrap='true' class='mainHead'>
				<td noWrap="true" >申请单号</td>
	      <td noWrap="true">购置年月</td>
				<td noWrap="true">制单日期 </td>
				<td noWrap="true">申请科室</td>
				<td noWrap="true">申请人</td>
				<td noWrap="true">摘要</td>
				<td noWrap="true">申请总金额</td>
	      <td noWrap="true">资产金额</td>
				<td noWrap="true">附件金额</td>
	      <td noWrap="true">状态</td>
	      <td noWrap="true">备注</td>
	      <td noWrap="true">制单人</td>
	      <td noWrap="true">计划号</td>
	      <td noWrap="true">资金来源</td>
      </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
							  <td  noWrap='true' >
							  <a tabindex="-1">
							    <xsl:attribute name="href" >
							      javascript:view('update.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:when test="position()=7 or position()=8 or position()=9 ">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>