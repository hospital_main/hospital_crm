<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		  <tr noWrap='true' class='mainHead'>
				<th noWrap="true" >序号</th>
	      <th noWrap="true">资产编码</th>
				<th noWrap="true">资产名称</th>
				<th noWrap="true">资产卡片号</th>
				<th noWrap="true">规格</th>
				<th noWrap="true">型号</th>
				<th noWrap="true">产地厂家</th>
        <th noWrap="true">原值</th>
        <th noWrap="true">卡片数量</th>
				<th noWrap="true">开始使用日期</th>
        <th noWrap="true">累计折旧</th>
        <th noWrap="true">在用创收</th>
        <th noWrap="true">维修费用</th>
        </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style=''>
           <!--<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
    			  -->
    			  <xsl:value-of select="position()"/>
          </td>
          <xsl:for-each select="td">
          <xsl:choose>
              <xsl:when test="position()=3 ">
                <td  noWrap='true' align="center">
                    <a href="#">
						    	    <xsl:attribute name='onclick'>
						    	      javascript:openDialog('../../../fixcard/add/update.html?load=&lt;is_read&gt;true&lt;/is_read&gt;&lt;card_arch_no&gt;<xsl:value-of select="../td[position()=3]"/>&lt;/card_arch_no&gt;&lt;type&gt;<xsl:value-of select="../pk/*"/>&lt;/type&gt;', 'dialogWidth:850px;dialogHeight:650px', result)
						    	       <!--javascript:openDialog('../../../fixcard/add/update.html?load=&lt;card_arch_no&gt;<xsl:value-of select="../td[position()=3]"/>&lt;/card_arch_no&gt;&lt;type&gt;<xsl:value-of select="../../pk/type"/>&lt;/type&gt;', 'dialogWidth:850px;dialogHeight:650px', result)-->
						    	    </xsl:attribute>
						    	    <xsl:value-of select="."/>
						    	    </a>
		              
			            </td>
             		 </xsl:when>
                <xsl:when test="position()=7 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
             		 
             		<xsl:when test="position()=10 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
             		<xsl:when test="position()=11 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
             		<xsl:when test="position()=12 ">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
	              <xsl:otherwise>
	                <td align="center"><xsl:value-of select="."/></td>
	              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


