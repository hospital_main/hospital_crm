<%@ page contentType="text/html; charset=gb2312" language="java" import="com.viewhigh.core.web.OperateHelper,com.viewhigh.core.dao.*" errorPage="" %>
<html xmlns:vh>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
		<link href="" rel="stylesheet" type="text/css">
		<script language='javascript' id='scriptID'></script>
		<script language='javascript' src='/base/scripts/CreateObjects.js'></script>
		<script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>
		<script language='javascript'>
		  function init(){
		  	//alert(location.href);
		  }
		  
		  function printthis()
		  {
		  	var names = document.getElementById("hiddenCName").value.split(';');
		  	var colcount = document.getElementById("hiddenColCount").value;
				var idx=0;
		  	var xmlStr = '<?xml version="1.0" encoding="UTF-16"?>';
		  	xmlStr += "<root>";
		  	xmlStr += "<table>";
		  	xmlStr += "<thead>";
		  	xmlStr += "<tr noWrap='true'><td style='fontsize:maintitle;colspan:colcount'></td><td style='display:none'></td>";
		  	for(var z=2;z<colcount*2;z++)
		  	{
		  		xmlStr += "<td style='display:none'></td>";
		  	}
		  	
		  	xmlStr += "</tr>";
		  	xmlStr += "</thead>";
		  	
		  	xmlStr += "<tbody>";
		  	var ttbody = query.firstChild;
		  	for(var i=0;i<ttbody.childNodes.length;i++)
		  	{
		  		var trr = ttbody.childNodes[i];
		  		var cs = trr.cells;
		  		xmlStr += "<tr>";
		  		var j=0;
		  		for(;j<cs.length;j++)
		  		{
		  			if(j%2==0 && cs[j].innerText.trim()!="")
		  			{
		  				xmlStr += "<td>" + cs[j].innerText + "</td>";
		  				
		  				if(cs[j+1].colSpan!=1)
		  					xmlStr += "<td colspan='"+cs[j+1].colSpan+"'>";
		  				else
		  					xmlStr += "<td>";
		  					
		  				var oobj = document.getElementById(names[idx]);
		  				
		  				if(oobj.className == "inputSelectS")
		  				{
		  					xmlStr += oobj.text;
		  				}
		  				else
	  					{
	  						xmlStr += oobj.value;
	  					}

		  				xmlStr += "</td>";
		  				idx++ ;
		  				j++;
		  			}
		  			else if(j<colcount*2)
	  				{
	  					xmlStr += "<td></td>";
	  					xmlStr += "<td></td>";
	  					j++
	  				}
		  		}
		  		while(j<colcount*2)
		  		{
	  					xmlStr += "<td style='display:none'></td>";
	  					j++;
		  		}
		  		xmlStr += "</tr>"
		  	}
		  	
		  	var d = document.getElementById("detail_frm");
		  	if(d!=null)
		  		xmlStr += d.contentWindow.getDetailXml(colcount);
		  	
		  	xmlStr += "</tbody>"
		  	xmlStr += "</table>"
		  	xmlStr += "</root>"
		  	
		  	var data=new Object();
				data["thead_1_1"]=document.getElementById(names[0]).value;
		  	printStringDataToCell(xmlStr,data,false,false,location.href);
		  }
		  
		  function importz(){
			  var fs = getDict("equip_ws_getExcelPosition","<type>"+document.getElementById("billType").value+"</type><d>"+document.getElementById("dataid").value+"</d>");
			  objTable.OpenExcelFile();
			  var ps=fs.getElementsByTagName("para");
				for(var i=0;i<ps.length;i++){
					var c=ps[i].getAttribute("code");
					var v=ps[i].getAttribute("value");
					if(v!="")
					{
						var cc = v.split(",");
						setControlValue(c,objTable.GetExcelCellContext(cc[0],cc[1]));
					}
				}
				
				objTable.CloseExcelFile();
		  }
		  
		  function setControlValue(n,v)
		  {
    		var obj = document.getElementById(n);;
      	if(obj.className == "inputSelectS")
      	{
      		obj.setValue(v);
      	}
      	else if(obj.className == "inputCalendar")
      	{
      		obj.value = v;
      	}
      	else
    		{
	    		if(obj.tagName == "SELECT")
	    		{
	    			setSelectValue(obj,v);
	    		}
	    		else
	    			obj.value = v;
    		}
		  }
		  
		  function setSelectValue(obj,v)
		  {
  			for (var i=0; i< obj.options.length; i++)
  			{
  				if(obj.options(i).text == v)
  					obj.selectedIndex = i;
  			}
		  }

		  function fieldcheck(){
				var names = document.getElementById("hiddenCName").value.split(';');
				for(var i=0;i<names.length-1;i++)
				{
        	if(!check(document.getElementById(names[i]))){
            return false;
          }
        }
        return true;
      }

      function check(obj)
      {
      	if(obj.className == "inputSelectS" || obj.className == "inputCalendar")
      	{
      		if(!obj.check())
      			return false;
      	}

  			if(obj._r == 'true')
  			{
    			if(obj.tagName == "SELECT")
    			{
    				if(obj.selectedIndex < 0)
    				{
	    				alert("存在选择框不允许为空值");
	    				return false;
	    			}
    			}
    			else
    			{
    				if(obj.value == "")
    				{
	    				alert("存在输入框不允许为空值");
	    				return false;
	    			}
    			}
    		}
	    		    		
    		if(obj._t == "inputDecimal" && obj.value != "")
    		{
    			var intDec = /^\d+([.]\d+)?$/;
    			var r2 = intDec.test(obj.value);
    			if(!r2)
    			{
    				alert("存在数值输入格式错误");
    				return false;
    			}
    		}
      	return true;
    	}
    	
    	function btn_submit(v)
    	{
    		if(fieldcheck())
    		{
	    		document.getElementById("b_status").value = v;
	    		document.getElementById("f").submit();
	    	}
    	}
    	<%
    		if(request.getParameter("msg") !=null )
    		{
    			if(request.getParameter("msg").equals("addok"))
    				out.write("alert('新增数据成功')");
    			else if(request.getParameter("msg").equals("updateok"))
    				out.write("alert('更新数据成功')");
    			else
    				out.write("");
    		}
    	%>
		</script>
	</head>
	<body class='subBody' id='subBody' onload='init();' style="overflow:auto;scroll:auto;overflow-x:hidden;">
	<%
		//工作流 billType参数有值
		//数据 audioid,dataid参数有值
		String billType=request.getParameter("type");//工作流程类型 ws_WorkStream表autoid
		String audioid = request.getParameter("audioid");//审核单据ID ws_DataAudit表autoid
		String dataid = request.getParameter("dataid");//数据格式ID ws_DataDefine表autoid
		String billId = request.getParameter("bid");//单据Id ws_DataStorage表autoid
		String comp_Code=request.getParameter("comp");
		String copy_code=request.getParameter("zt");
		String UID=request.getParameter("uid");
		String read=request.getParameter("read");//当存在这个参数，并且值为1时，强制为只读
		String hidebtn=request.getParameter("sb");//是否显示按钮
	
		if(hidebtn==null || hidebtn.equals("0"))
			hidebtn = "";
		else
			hidebtn = "none";
		
		boolean is_read = false;
		boolean is_Add = (billId.equals("0"));//仅是新增
	
		RelationalParameter param = null;
		RelationalResult result = null;
		String[] values = null;	
		
		int colcount = 3;
		String DelegateUrl = "";
		
		if(billType!=null)//如果是增加附属数据的情况，则不需要此次查询
		{
			param = new RelationalParameter("equi_Ws_GetDataDefine",null,new String[]{billType});
			result = OperateHelper.executeProc(request,param);
			values = (String[])result.getResultValue().get(0);
	
			colcount = Integer.parseInt(values[3]);
			
			if(colcount<1)
				colcount = 3;
				
			DelegateUrl = values[4];
		}
		
		if(!is_Add)
		{
			param = new RelationalParameter("equi_Ws_GetUpdateDataInfo",null,new String[]{billId});
			result = OperateHelper.executeProc(request,param);
			values = (String[])result.getResultValue().get(0);
			
			is_read = !values[values.length-1].equals("-1");
		}
		
		if(read!=null && read.equals("1"))
		{
			is_read = true;
		}
	%>
	<form id="f" name='f' action="billsave.jsp" method="post">
	<%
		if(dataid==null)
		{
			param=new RelationalParameter("equi_Ws_FieldList",null,new String[]{billType,"1"});
		}
		else
		{
			param=new RelationalParameter("equi_Ws_FieldList",null,new String[]{dataid,"0"});
		}
			
		result=OperateHelper.executeProc(request,param);
		
		StringBuffer sb = new StringBuffer(1024);
		StringBuffer consb = new StringBuffer(64);
		
		String ConstStr = " style='border:1 solid #838383;";
		
		sb.append("<table id='query' class='lineCtn'>");	
		
		int rowCellIdx = 0;
    for(int i=0;i<result.getResultValue().size();i++){
			boolean istextarea = false;
      if(rowCellIdx%colcount==0)
      {
      	rowCellIdx = 0;
        sb.append("<tr>");
      }
        
    	String[] UnitRow=(String[])result.getResultValue().get(i);
			consb.append(UnitRow[1] + ";");
			
			String oneCStr = (UnitRow[7].equals("1")?" _r='true' required='true' ":"") + ConstStr ;
			if(is_read==false && UnitRow[7].equals("1"))
				oneCStr += "background-color:#DBFCFF;";
    	
    	if(UnitRow[5].equals("1"))
    	{
    		if(rowCellIdx == 0)
    		{
    			sb.append("<td align='right'>" + UnitRow[0] + "</td><td align='left' colspan='"+String.valueOf(colcount*2-1)+"'>");
    			oneCStr += "width:400px;";
    			rowCellIdx = colcount ;
    			istextarea = true;
    		}
    		else
  			{
  				while(rowCellIdx<colcount)
  				{
  					++rowCellIdx;
  					sb.append("<td>&nbsp;</td><td>&nbsp;</td>");
  				}
  				sb.append("</tr>");
    			sb.append("<tr><td align='right'>" + UnitRow[0] + "</td><td align='left' colspan='"+String.valueOf(colcount*2-1)+"'>");
    			oneCStr += "width:400px;";
    			rowCellIdx = colcount ;
    			istextarea = true;
  			}
    	}
    	else
    	{
	      sb.append("<td align='right'>" + UnitRow[0] + "</td><td align='left'>");
	      ++rowCellIdx;
		  }

	    if(UnitRow[2].equals("3"))
	    {
	    	if(!is_Add && !values[i].equals(""))
	    		values[i] = values[i].substring(0,10);
	    	sb.append("<input type='text' name='"+UnitRow[1]+"' id='"+UnitRow[1]+"' " + oneCStr + "' class='inputCalendar' value='"+((!is_Add)?values[i]:"")+"'/>");
	    }
	    else if(UnitRow[2].equals("2"))
	    	sb.append("<input type='text' name='"+UnitRow[1]+"' id='"+UnitRow[1]+"' " + oneCStr + "' _t='inputDecimal' value='"+((!is_Add)?values[i]:"")+"'/>");
	    else if(UnitRow[2].equals("4"))//只读的自动编码
	    	sb.append("<input type='text' name='"+UnitRow[1]+"' id='"+UnitRow[1]+"' style='border:1 solid #838383;background-color:#EDEDED' readonly value='"+((!is_Add)?values[i]:"")+"'/>");
	    else
	    {
    		if(!UnitRow[3].equals(""))
    			sb.append("<input type='text' name='"+UnitRow[1]+"' id='"+UnitRow[1]+"' " + oneCStr + "' class='inputSelectS' load='acct_"+UnitRow[3]+"' initValue='"+((!is_Add)?values[i]:"")+"'/> ");
    		else if(!UnitRow[4].equals(""))
  			{
      		sb.append("<SELECT name='"+UnitRow[1]+"' id='"+UnitRow[1]+"' " + oneCStr + "'>");
      		String[] chooseItems = UnitRow[4].split(";");
      		if(is_Add)
      		{
			    	for(int j=0;j<chooseItems.length;j++)
			    	{
	      			sb.append("<OPTION VALUE='"+chooseItems[j]+"'>"+chooseItems[j]);
	      		}
	      	}
	      	else
	      	{
			    	for(int j=0;j<chooseItems.length;j++)
			    	{
	      			sb.append("<OPTION VALUE='"+chooseItems[j]+"' "+(chooseItems[j].equals(values[i])?"selected":"")+">"+chooseItems[j]);
	      		}
	      	}
      		sb.append("</SELECT>");
  			}
  			else
  			{
  				if(istextarea==true)
  					sb.append("<textarea name='"+UnitRow[1]+"' id='"+UnitRow[1]+"' " + oneCStr + "' cols='50' rows='4'>"+((!is_Add)?values[i]:"")+"</textarea>");
	  			else
	  				sb.append("<input type='text' name='"+UnitRow[1]+"' id='"+UnitRow[1]+"' " + oneCStr + "' value='"+((!is_Add)?values[i]:"")+"'/>");
  			}
    	}
	    	
	    sb.append("</td>");

      if(rowCellIdx%colcount==0)
      {
        sb.append("</tr>");
      }

      //++rowCellIdx;
    }
    
    sb.append("</table>");
    
		sb.append("<div style='display:none'>");
		sb.append("<input type='text' id='billType' name='billType' value='" + ((billType==null)?"":billType) + "'/>");
		sb.append("<input type='text' name='billId' value='" + billId + "'/>");
		sb.append("<input type='text' name='audioid' value='" + ((audioid==null)?"":audioid) + "'/>");
		sb.append("<input type='text' id='dataid' name='dataid' value='" + ((dataid==null)?"":dataid) + "'/>");
		sb.append("<input type='text' name='comp_Code' value='" + comp_Code + "'/>");
		sb.append("<input type='text' name='copy_code' value='" + copy_code + "'/>");
		sb.append("<input type='text' name='UID' value='" + UID + "'/>");
		if(is_Add)//状态信息
			sb.append("<input type='text' id='b_status' name='b_status' value='-1'/>");
		else
			sb.append("<input type='text' id='b_status' name='b_status' value='" + values[values.length-1] + "'/>");
		sb.append("</div>");

		if(hidebtn.equals("none"))
		{
			sb.append("<div align='center'>");
			sb.append("<input name='btn_print' type='button' value='打印' onclick='printthis();'></input>");
			sb.append("</div>");
		}
		else
		{
			sb.append("<div align='center' style='display:" + hidebtn + "'>");
			
	    if(is_read == false)
	    {
		    if(audioid==null || audioid.equals("") || audioid.equals("0"))//是附属数据时不需要草稿状态
		    {
					sb.append("<input name='btn_savecg' type='button' value='草稿保存' onclick='btn_submit(-1);'></input>");
				}
				sb.append("<input name='btn_save' type='button' value='提交' onclick='btn_submit(0);'></input>");
				sb.append("<input name='btn_import' type='button' value='导入Excel' onclick='importz();'></input>");
	    }
	    else
	    {
		    if(audioid==null || audioid.equals("") || audioid.equals("0"))
		    {
					sb.append("<input name='btn_savecg' type='button' DISABLED  value='草稿保存' onclick='btn_submit(-1);'></input>");
				}
				sb.append("<input name='btn_save' type='button' DISABLED value='提交' onclick='btn_submit(0);'></input>");
				sb.append("<input name='btn_import' type='button' DISABLED value='导入Excel' onclick='importz();'></input>");
	    }
			sb.append("<input name='btn_print' type='button' value='打印' onclick='printthis();'></input>");
			sb.append("</div>");
		}
    
		if(!DelegateUrl.equals(""))
		{
	    sb.append("<div class='titleHeader'>附属数据</div>");
	    sb.append("<iframe name='detail_frm' id='detail_frm' style='width:100%;height:250px' src='common/"+DelegateUrl+".html?load=<dsid>"+billId+"</dsid>'></iframe>");
		}
		
		sb.append("<input type='hidden' name='hiddenCName' id='hiddenCName' value='" + consb.toString() + "'/>");
		sb.append("<input type='hidden' name='hiddenColCount' id='hiddenColCount' value='" + String.valueOf(colcount) + "'/>");
    out.write(sb.toString());
		out.flush();
	%>
		</form>
    <div style="display:none">
  	  <script language="javascript">
  	    CreateObjectControl("objTable", "VHTable", "100%", "300", null);
  	  </script>
  	</div>
  </body>
</html>		