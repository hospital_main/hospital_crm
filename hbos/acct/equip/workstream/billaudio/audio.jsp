<%@ page contentType="text/html; charset=gb2312" language="java" import="com.viewhigh.core.web.OperateHelper,com.viewhigh.core.dao.*" errorPage="" %>
<%
String did = request.getParameter("idx");
String structid = request.getParameter("struct");
String serialflag = request.getParameter("isSerial");
String mergeIdx = request.getParameter("mergeIdx");

String audioId = request.getParameter("audioid");
String read = request.getParameter("read");
String showbtn=request.getParameter("sb");
String disableFlag = "";

if(audioId==null)
	audioId="0";

if(showbtn==null || showbtn.equals("0"))
	showbtn = "0";
else
	showbtn = "1";

if(read==null)
{
	if(audioId.equals("0"))//新增，初始化不允许操作
	{
		read="1";
		disableFlag="DISABLED";
	}
	else
	{
		read="0";
	}
}
else
{
	if(!read.equals("0"))
	{
		read="1";
		disableFlag="DISABLED";
	}
}

RelationalParameter param = null;
RelationalResult result = null;

param = new RelationalParameter("equi_Ws_GetDataInfo",null,new String[]{did});
result = OperateHelper.executeProc(request,param);
String[] DataInfo = (String[])result.getResultValue().get(0);

param = new RelationalParameter("equi_Ws_GetAudioStep",null,new String[]{did,DataInfo[0],DataInfo[3]});
result = OperateHelper.executeProc(request,param);

if(result.getResultValue().size()>0)
{
String[] StepInfo = (String[])result.getResultValue().get(0);

String Items = "";
if(!StepInfo[3].equals("0") || !StepInfo[4].equals("0"))
	Items += "附属数据=0;";
	
if(StepInfo[2].equals("1"))
	Items += "附件=1;";
%>
<html xmlns:vh>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <link href="" rel="stylesheet" type="text/css">
    <script language="javascript" id="scriptID"></script>
    <script language="javascript" src="/base/scripts/tag.js"></script>
    <script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>
    <script language='javascript'>
    var imgload = false;
    function init()
    {
    	uid.value = getUserID();
    	basic_frm.document.location.href="../billbasic.jsp?type=<% out.write(DataInfo[0]); %>&comp="+getCompCode()+"&zt="+getCopyCode()+"&uid="+getUserID()+"&bid=<% out.write(did); %>&sb=1";
    <%
    	if(!Items.equals(""))
    	{
    		out.write("try {	initTag(tab1); }catch (e) {}");
    		
    		if(!StepInfo[3].equals("0"))
    		{
    			out.write("LoadDetail('"+StepInfo[3]+"');");
    		}
    		if(!StepInfo[4].equals("0"))
    		{
    		}
			}
		%>
    }
    function LoadDetail(wstype)
    {
    	detail_frm.document.location.href="../billbasic.jsp?type="+wstype+"&comp="+getCompCode()+"&zt="+getCopyCode()+"&uid="+getUserID()+"&bid=0&read=<% out.write(read); %>";
    }
    
    function history()
    {
    	openDialog('../billscan/ws.jsp?idx=<% out.write(did); %>&wsid=<%=request.getParameter("wsid")%>','dialogWidth:760px;dialogHeight:660px',null,'true')
    }

		function tag_click(tagid) {
			if(tagid == 0)
			{
				<%
    		if(!StepInfo[3].equals("0"))
    		{
    			out.write("document.getElementById(\"detail_frm\").style.display='';");
    		}
    		if(!StepInfo[4].equals("0"))
    		{
    			out.write("document.getElementById(\"data_frm\").style.display='';");
    		}
    		out.write("document.getElementById(\"div2\").style.display='none';");
				%>
			}
		  else if(tagid == 1)
			{
				<%
    		if(!StepInfo[3].equals("0"))
    		{
    			out.write("document.getElementById(\"detail_frm\").style.display='none';");
    		}
    		if(!StepInfo[4].equals("0"))
    		{
    			out.write("document.getElementById(\"data_frm\").style.display='none';");
    		}
    		out.write("document.getElementById(\"div2\").style.display='';");
				%>
		  	if(!imgload)
		  	{
		  		refreshPic();
		  		imgload = true;
		  	}
		  }
	  }
    function Expand(obj)
    {
    	if(obj.title == "隐藏")
    	{
    		document.getElementById("basic_div").style.display = "none";
    		obj.title = "展开";
    		obj.src="\\images2\\ws\\end.gif";
    	}
    	else
  		{
  			document.getElementById("basic_div").style.display = "";
  			obj.title = "隐藏";
  			obj.src="\\images2\\ws\\top.gif";
  		}
    }
    
  	vhFile.onload = upFile;
  	var vhStatus = "";
	 	function selectFile(){
			vhFile.choose();
			doc_file.value = vhFile.clientFileName;
		}
		
		function upFile(){
			if(vhFile.status != "0"){
				alert(vhFile.message);
				return;
			}
			if(vhStatus=="insert"){
  			var fn=vhFile.realFileName;
  			if(fn.indexOf("\\")>0)
  				fn=fn.substr(fn.lastIndexOf("\\")+1);
  			var tempPath = vhFile.uploadROOT + fn;
  			serverFile.value = tempPath;
  			equi_ws_attach_upload.click();
  		}
		}
		
		function save(btn){
		  if(doc_file.value==""){
		    alert("请选择需要上传的文件！")
		    return;
		  }
		  vhStatus = "insert";
			vhFile.insert();
		}
			
		function refreshPic(){
 			doc_file.value = "";
 			if(audio_autoid.value=="0") return;
		  var p = getDict("equi_ws_attachName","<a>"+ds_autoid.value+"</a><b>"+audio_autoid.value+"</b>");
		  var picList = p.getElementsByTagName("para");
		  var tempHtml = "<table id='picList' class='lineCtn' scrolling='yes'>"
		  for(var i=0;i<picList.length;i++){
		    if(i%6==0 && i!=0)tempHtml+="</tr>";
		    if(i%6==0)tempHtml += "<tr>";
		    var xxx = picList[i].getAttribute("code").split(';');
		    var vvv = picList[i].getAttribute("value");
		    
		    var fileext = xxx[0].substr(xxx[0].lastIndexOf("."));
		    if(".jpg.gif.bmp.jpeg.png".indexOf(fileext)==-1)
		    	fileext = "/images2/ws/down.png";
		    else
		    	fileext = xxx[0];
		    tempHtml += "<td><table><tr><td><a href='" + window.prefix + xxx[0] + "'><img border='0' src='" + fileext + "' height='90' width='90' name='pic_" + xxx[1] + "' /></a></td></tr><tr><td><input type=CheckBox  name='pic"+xxx[1]+"' onclick='checkBoxClick(this);'/>" + vvv + "</td></tr></table></td>";
		  }
      tempHtml += "</tr></table>";
      td9.innerHTML = tempHtml;
		}
		
		function deleteFile(obj){
		  vhStatus = "delete";
		  var xxx = picFile.value.split(';');
		  for(var i=0;i<xxx.length;i++){
		    if(xxx[i]!=""){
		      vhFile.clientFileName = xxx[i];
		      vhFile.serverFileName = xxx[i];
		      vhFile.del();
		    }
		  }
		  deletePic.submit(obj);
		  picFile.value=';';
		  picId.value = ',';
		  window.setTimeout(refreshPic,300);
		}
    function checkBoxClick(obj){
    	var b=obj.name.substring(3,obj.name.length);
 			var fn
 			eval("fn=pic_" + b + ".src");
 			if(fn.indexOf("/")>0)
 				fn=fn.substr(fn.lastIndexOf("/")+1);

    	if(obj.checked)
    	{
    	  picId.value+=b+",";
  			picFile.value += fn + ';';
      }
      else
    	{
    		picId.value=picId.value.replace(","+b+",",",");
    		picFile.value = picFile.value.replace(";" + fn + ";" , ";");
    	}	
    }	
    function agree(flag)
    {
    	signflag.value=flag;
	  	if(audio.submit(equi_ws_audio))
	  	{
	  		alert("操作成功");
	  		var xxx = window.xmlhttp._object.responseText;
			  xxx = xxx.substr(xxx.indexOf('<pid>') + 5 ,xxx.indexOf('</pid>')-(xxx.indexOf('<pid>') + 5));
	  		audioid.value = xxx;
<%	  		
if(!Items.equals(""))
{
	if(StepInfo[2].equals("1"))
	{
	%>
audio_autoid.value = xxx;
fileSelect.disabled=false;
btn_uploadFile.disabled=false;
equi_ws_pic_delete.disabled=false;
	<%
	}
	if(!StepInfo[3].equals("0"))
	{
	%>
detail_frm.document.location.href="../billbasic.jsp?type=<% out.write(StepInfo[3]); %>&comp="+getCompCode()+"&zt="+getCopyCode()+"&uid="+getUserID()+"&bid=0&audioid="+xxx;
	<%
	}
	if(!StepInfo[4].equals("0"))
	{
	%>
data_frm.aduio_id.value = audio_autoid.value;
data_frm.btn_add.disabled=false;
data_frm.btn_delete.disabled=false;
	<%
	}
}
%>
	  	}
    }
    
    function uninit()
    {
    	dialogArguments.loadbill();
    }
    </script>
    <title>单据审核</title>
  </head>
  <body class="subBody" id="subBody" onLoad="init();" onunload="uninit();";>
  	<div align="right"><img src="\images2\ws\open.png" onclick="history()" title="审核历史"/>&nbsp;<img src="\images2\ws\top.gif" onclick="Expand(this)" title="隐藏"/></div>
  	<div id="basic_div">
	  	<div class="titleHeader">浏览单据</div>
	    <iframe name="basic_frm" id="basic_frm" style="width:100%;height:350px" src="about:blank"></iframe>
    </div>
    <div class="titleHeader">审核信息</div>
		<table id="audio" class="lineCtn">
		 <tr>
			<td><input required="true" type=text label="　审核意见" class="inputTextarea" name="m_Text" cols="60" rows="5"/></td>
		 </tr>
		 <tr>
			<td>审核人签字：<input type=text class="inputTextA" name="d_sign" required="true"/></td>
			<td style="display:none"><input type=text class="inputTextA" name="signflag"/></td>
			<td style="display:none"><input type=text class="inputTextA" name="structid" value="<% out.write(structid); %>"/></td>
			<td style="display:none"><input type=text class="inputTextA" name="uid"/></td>
			<td style="display:none"><input type=text class="inputTextA" name="audioid" value="<% out.write(audioId); %>"/></td>
			<td style="display:none"><input type=text class="inputTextA" name="dsid" value="<% out.write(did); %>"/></td>
		 </tr>
		 <tr>
			<td align="left">
				<input type=text class="inputTextA" name="serialflag" style="display:none" value="<% out.write(serialflag); %>"/>
				<input type=text class="inputTextA" name="mergeIdx" style="display:none" value="<% out.write(mergeIdx); %>"/>
			  <button class="tableBtn" onClick="agree(1)" name="equi_ws_audio">同意确认</button>&nbsp;&nbsp;
			  <button class="tableBtn" onClick="agree(0)" name="equi_ws_audio_back">不同意退回</button>&nbsp;&nbsp;
			  <button class="tableBtn" onClick="window.close();" name="winclose">关闭</button> 
			</td> 
		 </tr>
		</table>      
		<br><br>
    <%
    	if(!Items.equals(""))
    	{
    %>
    
    <table id="tab1" cellSpacing=0 cellPadding=0 style="display:block;margin-left:0" width="100%" attrib="tabpage" tabItems="<% out.write(Items.substring(0,Items.length()-1)); %>">
    </table>
    
    <%
    	if(!StepInfo[3].equals("0"))
    	{
    %>
    <iframe name="detail_frm" id="detail_frm" style="width:100%;height:350px;display:none" src="about:blank"></iframe>
    <%
    	}
    	if(!StepInfo[4].equals("0"))
    	{
    %>
    <iframe name="data_frm" id="data_frm" style="width:100%;height:350px;display:none" src="attachData.html?load=<aduioid>-1</aduioid><dataid><% out.write(StepInfo[4]); %></dataid><read><% out.write(read); %></read><sb><% out.write(showbtn); %></sb>"></iframe>
    <%
    	}
    	if(StepInfo[2].equals("1"))
    	{
    %>
	  <div id="div2" style="overflow:scroll;scroll:auto;overflow-x:hidden;height:350px;display:none">
			<table id="query" class='lineCtn'>
			  <tr>
			  	<td>
			  		<input type='text' class='inputTextA' name="ds_autoid" value="<% out.write(did); %>" style="display:none"/>
			  		<input type='text' class='inputTextA' name="audio_autoid" value="<% out.write(audioId); %>" style="display:none"/>
			  		名称:<input type="text" class="inputTextA" name="doc_name" maxInput="30">
			  	</td>
			    <td align="left">
			 	    附件：<input type="text" class="inputTextA" required="true" readonly name="doc_file" maxInput="30">
			      <button class='pageBtn' accessKey="B" <% out.write(disableFlag); %> name='fileSelect' onclick="selectFile();">选择</button>
			      <button class="pageBtn" accessKey="U" <% out.write(disableFlag); %> name="btn_uploadFile" onclick="save(this)">上传</button>
			      <input type='text' class='inputTextA' name="serverFile" style="display:none"/>
			      <button class='pageBtn' name="equi_ws_attach_upload" onclick="query.submit(this);refreshPic();" style="display:none" />
			    </td>
			  </tr>
			</table>
			<div id="td9"></div>
			<table id="deletePic" class='lineCtn'>
			  <td style="display:none"><input type='text' class='inputTextA' name="picId" value=','/></td>
			  <td style="display:none"><input type='text' class='inputTextA' name="picFile" value=';'></td>
			  <td align="center"><button class='pageBtn' <% out.write(disableFlag); %> accessKey="D" name='equi_ws_pic_delete' onclick="deleteFile(this);">删除</button></td>
			</table>
		</div>
		<%
			}
		}
		%>
  </body>
</html>

<%
}
else
{
out.write("<script language='javascript'>alert('流程配置可能存在问题，系统检索不到相关数据');window.close();</script>");
}
%>