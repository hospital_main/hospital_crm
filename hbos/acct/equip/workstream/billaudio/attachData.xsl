<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:for-each select="/root/tbody/tr">
  			<xsl:if test="position()=1">
		  		<tr noWrap="true" class="mainHead">
		        <th width='25'><input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="checkall(this)"/></th>
		        <xsl:for-each select="td">
							<th><xsl:value-of select="."/></th>
						</xsl:for-each>
		  		</tr>
	  		</xsl:if>
  		</xsl:for-each>  	
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <xsl:if test="position() &gt; 1">
        	<tr>
	          <td align='center'  noWrap='true'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
	    			  </input>
	          </td>                
	          <xsl:for-each select="td">
	              <xsl:choose>
	                <xsl:when test="position()=1">
				            <td  noWrap='true' >
		                  <!--<a href='javascript:void(0)' onclick="loadData(this)"><xsl:value-of select="."/></a>-->
		                  <xsl:value-of select="."/>
				            </td>
	                </xsl:when>
				  			  <xsl:otherwise>
				            <td  noWrap='true' >
			                  <xsl:value-of select="."/>
				            </td>
	                </xsl:otherwise>
	              </xsl:choose>
	  			  </xsl:for-each>
  				</tr>
  			</xsl:if>
   		</xsl:for-each>  	
  	</tbody>  	
	</xsl:template>
</xsl:stylesheet>
