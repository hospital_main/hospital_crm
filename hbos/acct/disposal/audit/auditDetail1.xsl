<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
        <xsl:variable name="natureCode" select="/root/tbody/tr[1]/td[15]"/>
        <xsl:variable name="listProj" select="/root/tbody/tr[1]/td[3]"/>
        <xsl:variable name="disCheck" select="/root/tbody/tr[1]/td[17]"/>
    		<tr noWrap='true' class='mainHead'>
    		  <xsl:if test="$disCheck='0'">
    			  <th noWrap='true' style="display:none">对账</th>
    			</xsl:if>   
    			<th noWrap='true'>凭证日期</th>
    			<th noWrap='true'>凭证号</th>
    			<xsl:if test="$listProj != '-1'">
      			<th noWrap='true'>往来项目</th>  
    			</xsl:if>  			
    			<xsl:if test="$listProj = '-1'">
      			<th noWrap='true' style="display:none">往来项目</th>  
    			</xsl:if>  			
    			<th noWrap='true'>合同号</th>
    			<th noWrap='true'>单据号</th>
    			<xsl:if test="$natureCode='04'">
      			<th noWrap='true'>到期日期</th>
    			</xsl:if>
    			<xsl:if test="$natureCode!='04'">
      			<th noWrap='true' style="display:none">到期日期</th>
    			</xsl:if>
    			<th noWrap='true'>摘要</th>
    			<th noWrap='true'>贷方金额</th>
    			<th noWrap='true'>已核销金额</th>
    			<th noWrap='true'>核销序列号</th>
    		</tr>
    	</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
  		    <xsl:for-each select="td">
             <xsl:choose>
              <xsl:when test="position()=2">
                <xsl:if test="$listProj!='-1'">
                  <td align='left' noWrap='true'>
        						<xsl:value-of select="."/>
                  </td>                    
                </xsl:if>
                <xsl:if test="$listProj='-1'">
                  <td align='left' noWrap='true' style="display:none">
        						<xsl:value-of select="."/>
                  </td>                    
                </xsl:if>
              </xsl:when>
              <xsl:when test="position()=6">
                <xsl:if test="$natureCode='04'">
                  <td align='left' noWrap='true'>
        						<xsl:value-of select="."/>
                  </td>  
                </xsl:if>
                <xsl:if test="$natureCode!='04'">
                  <td align='left' noWrap='true' style="display:none">
        						<xsl:value-of select="."/>
                  </td>  
                </xsl:if>
              </xsl:when>
  		        <xsl:when test="position()=8">
  		            <td align="right"><xsl:value-of select="format-number(.,'#,#00.00')"/></td>
  		        </xsl:when>
  		        <xsl:when test="position()=9">
  		            <td style='Color:red' align="right"><xsl:value-of select="format-number(.,'#,#00.00')"/></td>
  		        </xsl:when>
  		        <xsl:when test="position()>10">
                  <td style="display:none">
      							<xsl:value-of select="."/>
                  </td>
              </xsl:when>
                <xsl:otherwise>
                  <td align='left' noWrap='true'>
      							<xsl:value-of select="."/>
                  </td>  
                </xsl:otherwise>
              </xsl:choose>
  	  		</xsl:for-each>
  	  	</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
