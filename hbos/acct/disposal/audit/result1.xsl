<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
    <table width='100%' class='mainTable' border='1' borderColor='#292954'>
    	<thead>
        <xsl:variable name="natureCode" select="/root/tbody/tr[1]/td[15]"/>
        <xsl:variable name="listProj" select="/root/tbody/tr[1]/td[3]"/>
        <xsl:variable name="disCheck" select="/root/tbody/tr[1]/td[16]"/>
    		<tr noWrap='true' class='mainHead'>
    		  <xsl:if test="$disCheck='0'">
    			  <th noWrap='true' style="display:none">对账</th>
    			</xsl:if>    			
    		  <xsl:if test="$disCheck!='0'">
    			  <th noWrap='true'>选择</th>
    			</xsl:if>    			
    			<th noWrap='true'>凭证日期</th>
    			<th noWrap='true'>凭证号</th>
    			<xsl:if test="$listProj != '-1'">
      			<th noWrap='true'>往来项目</th>  
    			</xsl:if>  			
    			<xsl:if test="$listProj = '-1'">
      			<th noWrap='true' style="display:none">往来项目</th>  
    			</xsl:if>  			
    			<th noWrap='true'>合同号</th>
    			<th noWrap='true'>单据号</th>
    			<xsl:if test="$natureCode='04'">
      			<th noWrap='true'>到期日期</th>
    			</xsl:if>
    			<xsl:if test="$natureCode!='04'">
      			<th noWrap='true' style="display:none">到期日期</th>
    			</xsl:if>
    			<th noWrap='true'>摘要</th>
    			<th noWrap='true'>借方金额</th>
    			<th noWrap='true'>已核销金额</th>
    			<th noWrap='true'>核销序列号</th>
    		</tr>
    	</thead>
    	<tbody style="overflow-y:auto" id="tbody1">
		    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:if test="$disCheck!='0'">
              <td align='center'>
                <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="doItemCheckL(this)">
                  <xsl:attribute name="value" >
                    <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
          			  </xsl:attribute>
          			  <xsl:attribute name="value2" >
                    <xsl:for-each select="pk/*"><xsl:value-of select="."/></xsl:for-each>
          			  </xsl:attribute>
          			  <xsl:if test="td[11]='1'">
  								  <xsl:attribute name="checked"/>
  							  </xsl:if>
        			  </input>
              </td>
            </xsl:if>
            <xsl:if test="$disCheck='0'">
              <td align='center' style="display:none">
                <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="doItemCheckL(this)">
                  <xsl:attribute name="value" >
                    <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
          			  </xsl:attribute>
          			  <xsl:attribute name="value2" >
                    <xsl:for-each select="pk/*"><xsl:value-of select="."/></xsl:for-each>
          			  </xsl:attribute>
          			  <xsl:if test="td[11]='1'">
  								  <xsl:attribute name="checked"/>
  							  </xsl:if>
        			  </input>
              </td>
            </xsl:if>
          	<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=2">
                  <xsl:if test="../td[14]!=''">
                  <td noWrap="true">
                    <a href="#">
  									  <xsl:attribute name="onclick">
  									    javascript:openVouchDlg("&lt;vouch_id&gt;"+<xsl:value-of select="../td[14]"/>+"&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;")
  									  </xsl:attribute>
  									</a>
      							<xsl:value-of select="."/>
                  </td>
                  </xsl:if>
                  <xsl:if test="../td[14]=''">
                  <td>
      							<xsl:value-of select="."/>
      							</td>
                  </xsl:if>
               </xsl:when>
                <xsl:when test="position()=3">
                  <xsl:if test="$listProj!='-1'">
                    <td align='left' noWrap='true'>
        							<xsl:value-of select="."/>
                    </td>                    
                  </xsl:if>
                  <xsl:if test="$listProj='-1'">
                    <td align='left' noWrap='true' style="display:none">
        							<xsl:value-of select="."/>
                    </td>                    
                  </xsl:if>
                </xsl:when>
                <xsl:when test="position()=4">
                  <td noWrap="true">
								    <a href="#">
  							  	  <xsl:attribute name="onclick">
  							  	    openPact('<xsl:value-of select="."/>','<xsl:for-each select="../pk/*"><xsl:value-of select="."/></xsl:for-each>')
  							  	  </xsl:attribute>
  							  		<xsl:value-of select="."/>
								  	</a>
								  </td>
                </xsl:when>
                <xsl:when test="position()=5">
                  <td noWrap="true">
  							  		<xsl:value-of select="."/>
								  </td>
                </xsl:when>
                <xsl:when test="position()=8 or position()=9 ">
                  <td align='right'>
      							<xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:when>
                <xsl:when test="position()=6">
                  <xsl:if test="$natureCode='04'">
                    <td align='left' noWrap='true'>
        							<xsl:value-of select="."/>
                    </td>  
                  </xsl:if>
                  <xsl:if test="$natureCode!='04'">
                    <td align='left' noWrap='true' style="display:none">
        							<xsl:value-of select="."/>
                    </td>  
                  </xsl:if>
                </xsl:when>
                <xsl:when test="position()>10">
                  <td style="display:none">
      							<xsl:value-of select="."/>
                  </td>
                </xsl:when>
                 <xsl:when test="position()=7">
                  <td style="width:70">
      							<xsl:value-of select="."/>
                  </td>
                </xsl:when>
								<xsl:when test="position()=10">
								  <xsl:if test="../td[10]!=''">
                    <td noWrap="true">
  								    <a href="#">
    							  	  <xsl:attribute name="onclick">
    							  	    openAuditDetail1("&lt;sequence_no&gt;<xsl:value-of select="../td[10]"/>&lt;/sequence_no&gt;")
    							  	  </xsl:attribute>
    							  		<xsl:value-of select="."/>
  								  	</a>
  								  </td>
									</xsl:if>
                  <xsl:if test="../td[10]=''">
                    <td>
      							  <xsl:value-of select="."/>
      							</td>
                  </xsl:if>
								</xsl:when>
                <xsl:otherwise>
                  <td align='left' noWrap='true'>
      							<xsl:value-of select="."/>
                  </td>  
                </xsl:otherwise>
              </xsl:choose>              
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
     		<xsl:if test="count(/root/tbody/tr) &gt; 0">
     		<tr>  			
    		  <xsl:if test="$disCheck!='0'">	  
   					<td colspan="2" align="center">合计</td>
    			</xsl:if> 
    		  <xsl:if test="$disCheck='0'">	  
   					<td colspan="1" align="center">合计</td>
    			</xsl:if> 
   			
   			<xsl:if test="$listProj!='-1'">
          <td align='left' noWrap='true'>
          </td>                    
        </xsl:if>
        <xsl:if test="$listProj='-1'">
          <td align='left' noWrap='true' style="display:none">
          </td>                    
        </xsl:if>
        
        <td></td>
        
   			<xsl:if test="$natureCode='04'">
          <td align='left' noWrap='true'>
						
          </td>  
        </xsl:if>
        <xsl:if test="$natureCode!='04'">
          <td align='left' noWrap='true' style="display:none">
          </td>  
        </xsl:if>
     		<td colspan="3"/>
     		<td align='right'>
      				<xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/>
         </td>
     		</tr>
     		</xsl:if>
   		</tbody>
    </table>
	</xsl:template>
</xsl:stylesheet>