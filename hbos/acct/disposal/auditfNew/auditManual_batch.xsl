<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <thead>
            <tr noWrap='true' class='mainHead'>
                <th style='display:none' width='25'><input type='checkbox'/></th>
                <th noWrap='true'>凭证日期</th>
                <th noWrap='true'>凭证号</th>
                <th noWrap='true'>摘要</th>
                <th noWrap='true'>总金额</th>
                <th noWrap='true'>未核销金额</th>
                <th noWrap='true'>本次核销金额</th>
            </tr>
        </thead>
        <tbody>
            <xsl:for-each select="/root/tbody/tr">
                <tr>
                    <td style='display:none'>
                        <input type='checkbox' TABINDEX='-1' style='font-size:8px;display:none'>
                            <xsl:attribute name="value" >
                                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
                            </xsl:attribute>
                        </input>
                    </td>

                    <xsl:variable name="pp" select="position()"/>
                    <xsl:for-each select="td">
                        <xsl:choose>
                            <xsl:when test="position()=4 or position()=5">
                                <td align="right">
                                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
                                </td>
                            </xsl:when>
                            <xsl:when test="position()=2">
                                <td><a tabindex='-1'>
                                    <!--<xsl:attribute name="href" >-->
                                        <!--javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;');-->
                                    <!--</xsl:attribute>-->
                                    <xsl:value-of select="."/>
                                </a></td>
                            </xsl:when>
                            <xsl:when test="position()=6">
                                <td >
                                    <input type="text" class="inputDecimal" point="2" maxInput="12" extent="120" required='true' style="text-align:right "><!--disabled="false"-->
                                        <xsl:attribute name="max"><xsl:value-of select="."/> </xsl:attribute>
                                        <xsl:attribute name="name">ips<xsl:value-of select="$pp"/></xsl:attribute>
                                        <xsl:attribute name="id">ip<xsl:value-of select="$pp"/></xsl:attribute>
                                        <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
                                    </input>
                                </td>
                            </xsl:when>
                            <xsl:otherwise>
                                <td><xsl:value-of select="."/></td>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                </tr>
            </xsl:for-each>
        </tbody>
    </xsl:template>
</xsl:stylesheet>