<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
    <table width='100%' class='mainTable' border='1' borderColor='#292954'>
    	<thead>
        <xsl:variable name="natureCode" select="/root/tbody/tr[1]/td[14]"/>
        <xsl:variable name="listProj" select="/root/tbody/tr[1]/td[3]"/>
        <xsl:variable name="disCheck" select="/root/tbody/tr[1]/td[15]"/>
    		<tr noWrap='true' class='mainHead'>
    		  <xsl:if test="$disCheck='0'">
    			  <th noWrap='true' style="display:none">对账</th>
    			</xsl:if>    			
    		  <xsl:if test="$disCheck!='0'">
    			  <th noWrap='true'><input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="doAllCheck(this)"/></th>
    			</xsl:if>    			
    			<th noWrap='true'>凭证日期</th>
    			<th noWrap='true'>凭证号</th>
    			<xsl:if test="$listProj != '-1'">
      			<th noWrap='true'>往来项目</th>  
    			</xsl:if>  			
    			<xsl:if test="$listProj = '-1'">
      			<th noWrap='true' style="display:none">往来项目</th>  
    			</xsl:if>  			
    			<th noWrap='true'>合同号</th>
    			<th noWrap='true'>单据号</th>
    			<xsl:if test="$natureCode='04' or $natureCode='05'">
      			<th noWrap='true'>到期日期</th>
    			</xsl:if>
    			<xsl:if test="$natureCode!='04' and $natureCode!='05'">
      			<th noWrap='true' style="display:none">到期日期</th>
    			</xsl:if>
    			<th noWrap='true'>摘要</th>
    			<th noWrap='true'>总金额</th>
                <th noWrap='true'>未核销金额</th>
			<!--	<th noWrap='true'>预算标志</th>-->
				<th noWrap='true'>核销状态</th>
    		</tr>
    	</thead>
    	<tbody style="overflow-y:auto" id="tbody1">
		    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:if test="$disCheck!='0'">
              <td align='center'>
                <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="doItemCheck(this)">
                  <xsl:attribute name="value" >
                    <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
          			  </xsl:attribute>
          			  <xsl:if test="td[14]='1'">
  								  <xsl:attribute name="checked"/>
  							  </xsl:if>
        			  </input>
              </td>
            </xsl:if>
            <xsl:if test="$disCheck='0'">
              <td align='center' style="display:none">
                <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="doItemCheck(this)">
                  <xsl:attribute name="value" >
                    <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
          			  </xsl:attribute>
          			  <xsl:if test="td[14]='1'">
  								  <xsl:attribute name="checked"/>
  							  </xsl:if>
        			  </input>
              </td>
            </xsl:if>
          	<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=2">
                  <xsl:if test="../td[13]!=''">
                  <td noWrap="true" >
                  	
                    <a href="#">
  									  <xsl:attribute name="onclick">
  									    javascript:openVouchDlg("&lt;vouch_id&gt;"+<xsl:value-of select="../td[13]"/>+"&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;")
  									  </xsl:attribute>
  									</a>
      							<xsl:value-of select="."/>
      							
                  </td>
                  </xsl:if>
                  <xsl:if test="../td[13]=''">
                  <td>
      							<xsl:value-of select="."/>
      							</td>
                  </xsl:if>
               </xsl:when>
                <xsl:when test="position()=3">
                  <xsl:if test="$listProj!='-1'">
                    <td align='left' noWrap='true' style="width:70">
        							<xsl:value-of select="."/>
                    </td>                    
                  </xsl:if>
                  <xsl:if test="$listProj='-1'">
                    <td align='left' noWrap='true' style="display:none">
        							<xsl:value-of select="."/>
                    </td>                    
                  </xsl:if>
                </xsl:when>
                <xsl:when test="position()=4">
                  <td noWrap="true">
								    <a href="#">
  							  	  <xsl:attribute name="onclick">
  							  	    openPact('<xsl:value-of select="."/>','<xsl:for-each select="../pk/*"><xsl:value-of select="."/></xsl:for-each>')
  							  	  </xsl:attribute>
  							  		<xsl:value-of select="."/>
								  	</a>
								  </td>
                </xsl:when>
                <xsl:when test="position()=5">
                  <td noWrap="true">
  							  	<xsl:value-of select="."/>
								  </td>
                </xsl:when>
                <xsl:when test="position()=8 or position()=9">
                  <td align='right'>
      							<xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:when>

                <xsl:when test="position()=6">
                  <xsl:if test="$natureCode='04' or $natureCode='05' ">
                    <td align='left' noWrap='true'>
        							<xsl:value-of select="."/>
                    </td>  
                  </xsl:if>
                  <xsl:if test="$natureCode!='04' and $natureCode!='05' ">
                    <td align='left' noWrap='true' style="display:none">
        							<xsl:value-of select="."/>
                    </td>  
                  </xsl:if>
                </xsl:when>
                  <xsl:when test="position()=7">
                  <td style="width:70">
                      <xsl:value-of select="."/>
                  </td>
              </xsl:when>

         <!--     <xsl:when test="position()=15">
                  <td align="center">
                  <xsl:choose>
                      <xsl:when test="../td[15]=1">
                              <a href="#">
                                  <xsl:attribute name="onclick">
                                      javascript:openVouchCheckSet("&lt;vouch_id&gt;"+<xsl:value-of select="../td[12]"/>+"&lt;/vouch_id&gt;")
                                  </xsl:attribute>
                                  收支类
                              </a>
                      </xsl:when>
                      <xsl:otherwise>
                          非收支类
                      </xsl:otherwise>
                  </xsl:choose>
                  </td>

              </xsl:when>-->
                  <xsl:when test="position()=16">
                      <td align="center">
                          <xsl:choose>
                              <xsl:when test="../td[17] &gt; 0">
                                  <a href="#">
                                      <xsl:attribute name="onclick">
                                          javascript:openVouchCancelDetail("&lt;vouch_id&gt;"+<xsl:value-of select="../pk/acct_check_id"/>+"&lt;/vouch_id&gt;")
                                      </xsl:attribute>
                                      <xsl:choose>
                                          <xsl:when test="../td[17] = 2">
                                              已核销
                                          </xsl:when>
                                          <xsl:otherwise>
                                              核销中
                                          </xsl:otherwise>
                                      </xsl:choose>
                                  </a>
                              </xsl:when>
                              <xsl:otherwise>
                                  未核销
                              </xsl:otherwise>
                          </xsl:choose>
                      </td>

                  </xsl:when>


                <xsl:when test="position()>8">
                  <td style="display:none">
      							<xsl:value-of select="."/>
                  </td>
                </xsl:when>

                <xsl:otherwise>
                  <td align='left' noWrap='true'>
      							<xsl:value-of select="."/>
                  </td>  
                </xsl:otherwise>
              </xsl:choose>              
    			  </xsl:for-each>



    			</tr>
     		</xsl:for-each>
     		<xsl:if test="count(/root/tbody/tr) &gt; 0">
     		<tr>
     			<xsl:if test="$disCheck!='0'">
            <td align='left' noWrap='true' style="width:70"></td>                    
          </xsl:if>
          <xsl:if test="$listProj!='-1'">
     				<td colspan="7" align="center">合　　　　　　　　计</td>
     			</xsl:if>
     			<xsl:if test="$listProj='-1'">
     				<td colspan="6" align="center">合　　　　　　　　计</td>
     			</xsl:if>
     			<td align='right'>
      				<xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/>
                  	</td>

                <td align='right'>
                    <xsl:value-of select="format-number(sum(/root/tbody/tr/td[9]),'#,##0.00')"/>
                </td>
     		</tr>
     		</xsl:if>
   		</tbody>
    </table>
	</xsl:template>
</xsl:stylesheet>