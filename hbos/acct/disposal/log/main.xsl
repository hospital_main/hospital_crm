<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
			<tr noWrap='true' class='mainHead' >
				<th rowspan="2" >会计科目</th>
				<th colspan="4">借</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th colspan="4">贷</th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th rowspan="2" >核销金额</th>
				<th rowspan="2" >核销序列号</th>
				
			</tr>
  		<tr noWrap='true' class='mainHead'>
  			<th style="display:none" >会计科目</th>
  			<th nowrap='true'>凭证类型</th>
  			<th nowrap='true'>凭证号</th>
  			<th nowrap='true'>摘要</th>
  			<th nowrap='true'>金额</th>
  			<th nowrap='true'>凭证类型</th>
  			<th nowrap='true'>凭证号</th>
  			<th nowrap='true'>摘要</th>
  			<th nowrap='true'>金额</th>
  			<th style="display:none">核销金额</th>  	
  			<th style="display:none">核销序列号</th> 
  			
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
         <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=3">
            				<td><a href='#'>
			                  <xsl:attribute name="onclick" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../td[12]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');return false;
			  	          	  </xsl:attribute><xsl:value-of select="."/>
			  	          	</a></td>
			  	    </xsl:when>
			  	    <xsl:when test="position()=7">
            				<td><a href='#'>
			                  <xsl:attribute name="onclick" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../td[13]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');return false;
			  	          	  </xsl:attribute><xsl:value-of select="."/>
			  	          	</a></td>
			  	    </xsl:when>
			  	    <xsl:when test="position()=12 or position()=13">
                  
              </xsl:when>
							<xsl:when test="position()=5 or position()=9 or position()=10">
                  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>