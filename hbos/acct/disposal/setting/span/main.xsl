<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>区间描述</th>
  			<th nowrap='true'>起始天数</th>
  			<th nowrap='true'>终止天数</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:choose>
          
          
            <xsl:when test="position()=1">
              <xsl:for-each select="td">
                <xsl:choose>
                  <xsl:when test="position()=2">
                    <td>
                      <input type="text">
                 				<xsl:attribute name="value">
    			                <xsl:value-of select="."/>
    			              </xsl:attribute>
    			              <xsl:attribute name="class">
    			                inputTextA
    			              </xsl:attribute>
    			              <xsl:attribute name="name">
    			                a
    			              </xsl:attribute>
    			              <xsl:attribute name="readonly">
    			                true
    			              </xsl:attribute>
                			</input>
                    </td>
                  </xsl:when>
                  <xsl:when test="position()=3">
                    <td>
                      <input type="text">
                 				<xsl:attribute name="value">
    			                <xsl:value-of select="."/>
    			              </xsl:attribute>
    			              
    			              <xsl:attribute name="name">
    			                a
    			              </xsl:attribute>
    			              <xsl:attribute name="onFocus">
    			                javascript:this.select();
    			              </xsl:attribute>
    			              <xsl:attribute name="onKeyPress">
    			              javascript:alert(window.event.keyCode)
    			                //javascript:getFocus(this.parentNode.parentNode,<xsl:value-of select="position()"/>);
    			              </xsl:attribute>
                			</input>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td><xsl:value-of select="."/></td>
                  </xsl:otherwise>
    	          </xsl:choose>
      			  </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
              <xsl:for-each select="td">
                <xsl:choose>
                  <xsl:when test="position()=2 or position()=3">
                    <td>
                      <input type="text">
                 				<xsl:attribute name="value">
    			                <xsl:value-of select="."/>
    			              </xsl:attribute>
    			              
    			              <xsl:attribute name="name">
    			                a
    			              </xsl:attribute>
    			              <xsl:attribute name="onFocus">
    			                javascript:this.select();
    			              </xsl:attribute>
    			              <xsl:attribute name="onKeyPress">
    			                javascript:getFocus(this.parentNode.parentNode,<xsl:value-of select="position()"/>);
    			              </xsl:attribute>
                			</input>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td><xsl:value-of select="."/></td>
                  </xsl:otherwise>
    	          </xsl:choose>
      			  </xsl:for-each>
            </xsl:otherwise>
            
            
	        </xsl:choose>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>