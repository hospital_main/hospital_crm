<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th nowrap='true'>科目编码</th>
		  	<th nowrap='true'>科目名称</th>
		  	<th nowrap='true'>方向</th>
		  	<th nowrap='true'>总账期初余额</th>
		  	<th nowrap='true'>往来初始余额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position() = 1">
                	<xsl:if test="../td[6] = 1">
		                <td><a tabindex='-1'><xsl:attribute name="href">

		                	javascript:openSubjDlg('<xsl:value-of select="../td[1]"/>', '<xsl:value-of select="../td[2]"/>', '<xsl:value-of select="../td[3]"/>');
 										</xsl:attribute><xsl:value-of select="."/></a></td>
 								</xsl:if>	
 								<xsl:if test="../td[6] = 0">
		                <td><xsl:value-of select="."/></td>
 								</xsl:if>		
                </xsl:when>
                <xsl:when test="position() = 4 or position() = 5">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:when test="position() = 6">
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>