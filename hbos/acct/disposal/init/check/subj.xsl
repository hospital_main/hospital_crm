<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="TotalCol" select="count(/root/tbody/tr[1]/td)"/> 
      <tr noWrap='true' class='mainHead'>
	    	<xsl:for-each select="/root/tbody/tr[1]/td">
	          <xsl:if test="(position() &lt;= $TotalCol - 2) and (position() mod 2 = 0)">
	          	<th nowrap='true'><xsl:value-of select="."/></th>
	          </xsl:if>
	      </xsl:for-each>
		  	<th nowrap='true'>�����ڳ����</th>
		  	<th nowrap='true'>������ʼ���</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="(position() &lt; $TotalCol - 2) and (position() mod 2 = 1) ">
                	<td><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:when test="(position() = last()) or (position() = last() - 1) ">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:otherwise>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>