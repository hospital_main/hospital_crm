<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/disposal/init/set/printView.xsl,v 1.1 2012/03/12 01:44:38 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:38 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
		<thead>
			<tr noWrap='true' class='mainHead'>
				<td style='fontsize:maintitle'>
				  <xsl:attribute name="colspan">
				    <xsl:value-of select="$colNum"/>
				  </xsl:attribute>
				  ������ʼ��
				</td>
				<xsl:for-each select="/root/tbody/tr[1]/td[position()>1]">
				  <xsl:if test="position()!=1">
    				<td style="display:none"></td>
  				</xsl:if>
				</xsl:for-each>
			</tr>

			<tr noWrap='true' class='mainHead'>
				<td style='fontsize:subtitle'>
				  <xsl:attribute name="colspan">
				    <xsl:value-of select="$colNum"/>
				  </xsl:attribute>
				  ������ʼ��
				</td>
				<xsl:for-each select="/root/tbody/tr[1]/td[position()>1]">
				  <xsl:if test="position()!=1">
    				<td style="display:none"></td>
  				</xsl:if>
				</xsl:for-each>
			</tr>
			<tr noWrap='true' class='mainHead'>
			  <xsl:for-each select="/root/tbody/tr[1]/td[position()>1]">
  				<td nowrap='true'><xsl:value-of select="."/></td>
				</xsl:for-each>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position()>1]">
				<tr>
					<xsl:for-each select="td[position()>1]">
					  <td>
					   <!-- <xsl:if test="substring-after(.,'|||') != ''">
					    <xsl:value-of select="substring-after(.,'|||')"/>
					    </xsl:if>
					    <xsl:if test="substring-after(.,'|||') = ''">
					    -->
					    <xsl:value-of select="translate(.,'|||',' ')"/>
					    <!--
					    </xsl:if>
					    -->
					  </td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
