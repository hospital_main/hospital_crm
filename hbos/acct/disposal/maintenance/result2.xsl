<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
    <table width='100%' class='mainTable' border='1' borderColor='#292954'>
    	<thead>
    		<tr noWrap='true' class='mainHead'>
    			<th style="display:none" noWrap='true'>对帐</th>
    			<th noWrap='true'>凭证日期</th>
    			<th noWrap='true'>凭证号</th>
    			<th noWrap='true'>单据号</th>
    			<th noWrap='true'>到期日期</th>
    			<th noWrap='true'>摘要</th>
    			<th noWrap='true'>贷方金额</th>
    		</tr>
    	</thead>
    	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <td style="display:none" align='center'>
              <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
                <xsl:attribute name="value" >
                  <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
        			  </xsl:attribute>
        			  <xsl:if test="td[7]='1'">
								  <xsl:attribute name="checked"/>
							  </xsl:if>
      			  </input>
            </td>
          	<xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=2">
                  <td>
                    <a href="#">
  									  <xsl:attribute name="onclick">
  									    javascript:openVouchDlg("&lt;vouch_id&gt;"+<xsl:value-of select="../td[8]"/>+"&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;")
  									  </xsl:attribute>
  									</a>
      							<xsl:value-of select="."/>
                  </td>
                </xsl:when>
                <xsl:when test="position()>6">
                  <td style="display:none">
      							<xsl:value-of select="."/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td align='left' noWrap='true'>
      							<xsl:value-of select="."/>
                  </td>  
                </xsl:otherwise>
              </xsl:choose>              
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
    </table>
	</xsl:template>
</xsl:stylesheet>