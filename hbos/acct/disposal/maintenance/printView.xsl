<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
		  <colgroup>		       
				<col style = 'width:100'/>
				<col style = 'width:100'/>
				<col style = 'width:100'/>
				<col style = 'width:250'/>
				<col style = 'width:100'/>
			</colgroup>  		
    	<thead>
    		<tr noWrap='true' class='mainHead'>
	    		<td style='colspan:5;fontsize:maintitle'>往来核销维护</td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
      	</tr>
      	<tr noWrap='true' class='mainHead'>
	    		<td style='colspan:5;fontsize:subtitle'></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
      	</tr>
  	  	<tr noWrap='true' class='mainHead'>
    			<td nowrap='true'>操作日期</td>
    			<td nowrap='true'>借方对账数量</td>
    			<td nowrap='true'>贷方对账数量</td>
    			<td nowrap='true'>对账备注</td>
    			<td nowrap='true'>检测通过</td>
  	  	</tr>
    	</thead>
    	<tbody>
  	    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:for-each select="td">
              <xsl:choose>
  							<xsl:when test="position()=5">
                  <xsl:if test=".='1'">
                    <td>是</td>
                  </xsl:if>
                  <xsl:if test=".='0'">
                    <td><font color="#FF0000">否</font></td>
                  </xsl:if>
                </xsl:when>
                <xsl:when test="position()>5">
                  <td style="display:none"><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>  	
    	</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>