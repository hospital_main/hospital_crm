<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>摘要</th>
  			<th nowrap='true'>凭证日期</th>
  			<th nowrap='true'>凭证号</th>
  			<th nowrap='true'>往来项目</th>
  			<th nowrap='true'>方向</th>
  			<th nowrap='true'>发生额</th>
  			<th nowrap='true'>未核销金额</th>  			
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
         <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                  <a href="#">
									  <xsl:attribute name="onclick">
									    javascript:openDetail("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
									  </xsl:attribute>
									  <xsl:value-of select="."/>
									</a>
                </td>
              </xsl:when>
							<xsl:when test="position()=3">
                  <td>
                  	<xsl:choose>           
            			<xsl:when test="../td[8]='' or ../td[8]='null' or ../td[8]='NULL'">
										<xsl:value-of select="."/>
		              </xsl:when>
		              <xsl:otherwise>
                  	<a href="#">
									  <xsl:attribute name="onclick">
									    javascript:openVouchDlg("&lt;vouch_id&gt;<xsl:value-of select="../td[8]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;")
									  </xsl:attribute>
										<xsl:value-of select="."/>
									</a>
		              </xsl:otherwise>
            		</xsl:choose>
									</td>
              </xsl:when>
							<xsl:when test="position()=6 or position()=7">
                  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:when test="position()>7">
                <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>