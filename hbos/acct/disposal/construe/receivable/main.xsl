<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="rowNum" select="count(//tr)-2"/> 
      <xsl:for-each select="/root/tbody/tr">
        <xsl:choose>
          <xsl:when test="position()=2">            
            <tr noWrap='true' class='mainHead'>
              <xsl:for-each select="td">
                <xsl:choose>
                  <xsl:when test="position()=1">
                    <th style="display:none">
                      <xsl:value-of select="."/>
                    </th>
                  </xsl:when>
                  <xsl:otherwise>
                    <th><xsl:value-of select="."/></th>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </tr>            
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
    </thead>
    
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <xsl:choose>
          <xsl:when test="position() &gt; 2 and position() &lt;= $rowNum">
            <tr>
              <xsl:for-each select="td">
              	<xsl:variable name="colindex" select="position()"/>
                <xsl:choose>
                  <xsl:when test="position()=1">
                    <td style="display:none">
                      <xsl:value-of select="."/>
                    </td>
                  </xsl:when>
                 <xsl:when test="position()=4">
                   <xsl:if test=".!='0' and .!='0.00'">
                   		<td align="right">
	                      <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                    </td>
	              		</xsl:if>
                    <xsl:if test=".='0' or .='0.00'">
                    	<td></td>
                    </xsl:if>
                  </xsl:when>
                  <xsl:when test="position()>4">
                   <xsl:if test="/root/tbody/tr[1]/td[$colindex]!=''">
                   		<td align="right">
	                     <a href="#" onclick="win_Open(this);" name="target_value"> 	
														<xsl:attribute name="area"><xsl:value-of select="/root/tbody/tr[1]/td[$colindex]"/></xsl:attribute>
														<xsl:attribute name="run_subj"><xsl:value-of select="../td[2]"/></xsl:attribute>
												  	<xsl:value-of select="format-number(.,'#,##0.00')"/>
												</a>
	                    </td>
	              		</xsl:if>
	              		<xsl:if test="/root/tbody/tr[1]/td[$colindex]=''">
                   		<td align="right">
												  	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	                    </td>
	              		</xsl:if>
                    <xsl:if test=".='0' or .='0.00'">
                    	<td></td>
                    </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                    <td><xsl:value-of select="."/></td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </tr>
          </xsl:when>
          <xsl:when test="position() = $rowNum+1">
          	<tr>
              <xsl:for-each select="td">
                <xsl:choose>
                  <xsl:when test="position()=1">
                    <td style="display:none">
                      <xsl:value-of select="."/>
                    </td>
                  </xsl:when>
                  <xsl:when test="position()>3">
                   <xsl:if test=".!='0' and .!='0.00'">
                   		<td align="right">
	                      <xsl:value-of select="."/>
	                    </td>
	              		</xsl:if>
                    <xsl:if test=".='0' or .='0.00'">
                    	<td></td>
                    </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                    <td><xsl:value-of select="."/></td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </tr>
          </xsl:when>
          <xsl:when test="position() = $rowNum+2">
          	<tr>
              <xsl:for-each select="td">
                <xsl:choose>
                  <xsl:when test="position()=1">
                    <td style="display:none">
                      <xsl:value-of select="."/>
                    </td>
                  </xsl:when>
                  <xsl:when test="position()>3">
                   <xsl:if test=".!='0' and .!='0.00'">
                   		<td align="right">
	                      <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                    </td>
	              		</xsl:if>
                    <xsl:if test=".='0' or .='0.00'">
                    	<td></td>
                    </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                    <td><xsl:value-of select="."/></td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </tr>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>