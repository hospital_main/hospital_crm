<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
		  <colgroup>
				<xsl:for-each select="/root/tbody/tr[position()=1]">
					<xsl:for-each select="td[position()>1]">
						<col/>
					</xsl:for-each>
				</xsl:for-each>
			</colgroup>
    	<thead>
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="/root/tbody/tr[position()=1]">
						<xsl:for-each select="td[position()>1]">
							<th><xsl:value-of select="."/></th>
						</xsl:for-each>
					</xsl:for-each>
				</tr>
			</thead>
    	<tbody>
  	    <xsl:for-each select="/root/tbody/tr[position()>1]">
          <tr>
            <xsl:for-each select="td[position()>1]">
              <xsl:choose>              						
  							<xsl:when test="position()>=4 and position() mod 2=0">
                  <td class="numberText">
                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
    			</tr>
     		</xsl:for-each>  	
    	</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>