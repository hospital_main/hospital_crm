<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' valign="center">应付项目</th>				
				<th nowrap='true' valign="center">应付金额</th>
				<th nowrap='true' valign="center">预警天数</th>
				<th nowrap='true' valign="center">计划付款日期</th>
			</tr>
		</thead> 
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
      			<xsl:variable name="colCount" select="last()" />
            <tr>
            	<xsl:choose>
	            	<xsl:when test="position()=$colCount">
	            		<xsl:for-each select="td">
	            			<xsl:choose>
	            				<xsl:when test="position()=1">
		                		<td><xsl:value-of select="."/></td>
		                	</xsl:when>	
	            				<xsl:when test="position()=2">
	                			<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	                		</xsl:when>
	                		<xsl:when test="position()=3">
	                			<td></td>
	                		</xsl:when>
	                		<xsl:otherwise>
	                    	<td align="right"><xsl:value-of select="."/></td>
	                		</xsl:otherwise>
	                	</xsl:choose>
	                </xsl:for-each>
	              </xsl:when>
		             <xsl:when test="position()!=$colCount">
		              <xsl:for-each select="td">
		                <xsl:choose>	
											<xsl:when test="position()=2">
		                    <td align="right">
		                    	<a href="#">
		                    		<xsl:attribute name="onclick">
		                    			javascript:openDetail("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>");			
		                    		</xsl:attribute>
		                    		<xsl:value-of select="format-number(.,'#,##0.00')"/>
		                    	</a>
		                    </td>
		                	</xsl:when>
		                	<xsl:when test="position()=1">
		                		<td><xsl:value-of select="."/></td>
		                	</xsl:when>	
											<xsl:otherwise>
		                    <td align="right"><xsl:value-of select="."/></td>
		                	</xsl:otherwise>
		               	</xsl:choose>
		             </xsl:for-each>
		             </xsl:when>
             </xsl:choose>
					</tr>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>