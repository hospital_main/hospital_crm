<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' valign="center">凭证日期</th>
				<th nowrap='true' valign="center">账龄天数</th>
				<th nowrap='true' valign="center">凭证号</th>
				<th nowrap='true' valign="center">摘要</th>
				<th nowrap='true' valign="center">核销状态</th>
				<th nowrap='true' valign="center">合同号</th>
				<th nowrap='true' valign="center">到期日期</th>
				<th nowrap='true' valign="center">金额</th>
				<th nowrap='true' valign="center">核销金额</th>
				<th nowrap='true' valign="center">余额</th>
			</tr>
		</thead> 
		 	
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
            <tr>
              <xsl:for-each select="td">
                <xsl:choose>
                	<xsl:when test="position()=3">
										<td>
										 <xsl:if test=" ../pk/vouch_id!=0">
							        <a href="#">
												<xsl:attribute name="onclick">
													javascript:openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;+&lt;/edit_mask&gt;");
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
											</xsl:if>
											<xsl:if test=" ../pk/vouch_id=0">
												<xsl:value-of select="."/>
											</xsl:if>
											</td>
									</xsl:when>
									<xsl:when test="position()=8 or position()=9 or position()=10 ">
                    <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                	</xsl:when>
									<xsl:otherwise>
                    <td><xsl:value-of select="."/></td>
                	</xsl:otherwise>
               	</xsl:choose>
             </xsl:for-each>
					</tr>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>