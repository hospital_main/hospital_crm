<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
    	<thead>
    		<xsl:variable name="rowNum" select="count(//tr)-4"/> 
    		<xsl:for-each select="/root/tbody/tr">
					<xsl:if test="position()=2">
						<tr>
							<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)-1"/></xsl:attribute>
								Ӧ���������
							</td>
							<xsl:for-each select="td">
								<xsl:if test="position()!=1 and position()!=last()">
									<td style='width:120;display:none'/>
								</xsl:if>
							</xsl:for-each>
						</tr>
						<tr>
							<td style="fontsize:subtitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)-1"/></xsl:attribute></td>
							<xsl:for-each select="td">
								<xsl:if test="position()!=1 and position()!=last()">
									<td style='width:120;display:none'/>
								</xsl:if>
							</xsl:for-each>
						</tr>
						<tr noWrap='true' class='mainHead'>
							<xsl:for-each select="td">
								<xsl:if test="position()!=1">
									<td style='width:120'><xsl:value-of select="."/></td>
								</xsl:if>
							</xsl:for-each>
						</tr>
					</xsl:if>
				</xsl:for-each>
				
			</thead>
			<tbody>
  	    <xsl:for-each select="/root/tbody/tr[position()>2]">
  	    	<xsl:choose>
  	    		<xsl:when test="position() = $rowNum+1">
          	<tr>
	            <xsl:for-each select="td[position()>1]">
	              <xsl:choose>
	  							<xsl:when test="position()>=4">
	  								<xsl:if test=".='0'">
	                    	<td></td>
	                    </xsl:if>
	                    <xsl:if test=".!='0'">
		                    <td><xsl:value-of select="."/></td>
	                    </xsl:if>
	                </xsl:when>
	                <xsl:otherwise>
	                  <td><xsl:value-of select="."/></td>
	                </xsl:otherwise>
	              </xsl:choose>
	            </xsl:for-each>
	    			</tr>
          </xsl:when>
          <xsl:when test="position() != $rowNum+1">
          	<tr>
	            <xsl:for-each select="td[position()>1]">
	              <xsl:choose>
	  							<xsl:when test="position()=3">
                    <td class="numberText">
		                    <xsl:value-of select="."/>
		                </td>
	                </xsl:when>
	  							<xsl:when test="position()>=4">
	  								<xsl:if test=".='' or .='0' or .='0.00'">
	                    	<td></td>
	                    </xsl:if>
	                    <xsl:if test=".!='' and .!='0.0' and .!='0.00'">
		                    <td class="numberText">
		                    	<xsl:value-of select="format-number(.,'#,##0.00')"/>
		                 		</td>
	                    </xsl:if>
	                </xsl:when>
	                <xsl:otherwise>
	                  <td><xsl:value-of select="."/></td>
	                </xsl:otherwise>
	              </xsl:choose>
	            </xsl:for-each>
	    			</tr>
          </xsl:when>
    			</xsl:choose>
     		</xsl:for-each>
    	</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>