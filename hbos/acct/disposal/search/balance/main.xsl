<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true' rowspan="2" >往来编码</th>
  			<th nowrap='true' rowspan="2" >往来名称</th>
  			<th nowrap='true' rowspan="2" >年初余额</th>
  			<th nowrap='true' rowspan="2" >本月借方</th>
  			<th nowrap='true' rowspan="2" >累计借方</th>
  			<th nowrap='true' rowspan="2" >本月贷方</th>
  			<th nowrap='true' rowspan="2" >累计贷方</th>
  			<th nowrap='true' colspan="2">余额</th>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
  		  <th nowrap='true'>方向</th>
  		  <th nowrap='true'>金额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>  
              <xsl:when test="position()=2">
                <xsl:if test=".!='' and .!='合计'">
                   <td>
                     <a href="#">
          					  <xsl:attribute name="onclick">
          					    javascript:openDetailDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
          					  </xsl:attribute>
					           
                     <xsl:value-of select="."/></a>
                   </td>
                </xsl:if>
                <xsl:if test=".='' or .='合计'">
                <td><xsl:value-of select="."/></td>
                </xsl:if>
              </xsl:when>            						
							<xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=9">
                <td align="right">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>