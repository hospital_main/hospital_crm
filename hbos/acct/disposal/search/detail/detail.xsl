<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  		  <th nowrap='true'>病人编码</th>
  		  <th nowrap='true'>病人名称</th>
  		  <th nowrap='true'>收费项目</th>
  		  <th nowrap='true'>日期</th>
  		  <th nowrap='true'>入账金额</th>
  		  <th nowrap='true'>收到未入账金额</th>
  		  <th nowrap='true'>His应收金额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=5 or position()=6 or position()=7 ">
              	<td align="right">
				 					<xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>