<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>		
    	<thead>
    		<tr noWrap='true' class='mainHead'>
	    		<td style='colspan:colcount;fontsize:maintitle'></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<xsl:if test="$colNum = 16">
			  	<td style="display:none"></td>
			  	</xsl:if>
      	</tr>
      	<tr noWrap='true' class='subHead'>
	    		<td style='colspan:colcount;fontsize:subtitle'></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<xsl:if test="$colNum = 16">
			  	<td style="display:none"></td>
			  	</xsl:if>
      	</tr>
      	
    	<tr noWrap='true' class='mainHead'>
  		  <td id="yearText" colspan="2" valign="middle" nowrap='true'></td>
  		  <td style="display:none"></td>
  		  <td rowspan="2" valign="middle" nowrap='true'>凭证号</td>
	      <xsl:if test="$colNum = 16"><td rowspan="2" valign="middle" nowrap='true'>项目</td></xsl:if> 		  
  		  <td rowspan="2" valign="middle" nowrap='true'>摘要</td>
  		  <td rowspan="2" valign="middle" nowrap='true'>核销状态</td>
  		  <td rowspan="2" valign="middle" nowrap='true'>合同号</td>
  		  <td rowspan="2" valign="middle" nowrap='true'>单据号</td>
  			<td rowspan="2" valign="middle" nowrap='true'>到期日期</td>
  			<td rowspan="2" valign="middle" nowrap='true'>借方</td>
  			<td rowspan="2" valign="middle" nowrap='true'>贷方</td>
  			<td rowspan="2" valign="middle" nowrap='true'>方向</td>
  			<td rowspan="2" valign="middle" nowrap='true'>余额</td>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
  		  <td nowrap='true'>月</td>
  		  <td nowrap='true'>日</td>
  		  <td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<xsl:if test="$colNum = 16">
	  	<td style="display:none"></td>
	  	</xsl:if>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
  		</tr>
    	</thead>
    	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
		    	<xsl:variable name="rowNum" select="position()"/>
		    	<xsl:if test="$colNum = 16">
			    	<xsl:if test="$rowNum =1 or /root/tbody/tr[$rowNum - 1]/td[4] != ./td[4]">
							<tr noWrap='true' align='left'>
								<td align='left' style="colspan:colcount;align:left">
									<xsl:if test="$rowNum !=1">
										<xsl:attribute name="style">pagebreak:true;colspan:colcount</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="./td[4]"/>
								</td>
							</tr>
						</xsl:if>
					</xsl:if>
	        <tr>
	          <xsl:for-each select="td">
	            <xsl:choose>
				  			<xsl:when test="position()= $colNum - 3 or position()= $colNum - 5 or position()= $colNum - 6 ">
	                <td align="right">     
			            <xsl:if test=".!=''">
			            	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </xsl:if>                  
	                </td>
	              </xsl:when>
	              <xsl:when test="position() = $colNum - 1 or position() = $colNum or position() = $colNum -2">
	              </xsl:when>
	              <xsl:when test="position()=$colNum - 12 ">
              	<td>
              		<xsl:if test="$colNum = 16">
               			<xsl:if test= "../td[$colNum -11 ]='本年累计' or ../td[$colNum -11 ] = '本月合计' "></xsl:if>
               		</xsl:if>
               		<xsl:if test="$colNum = 16">
               			<xsl:if test= "../td[$colNum -11 ] != '本年累计' and ../td[$colNum -11 ] != '本月合计' " ><xsl:value-of select="."/></xsl:if>
               		</xsl:if>
               		<xsl:if test="$colNum != 16">
               			<xsl:value-of select="."/>
               		</xsl:if>
               </td>
              </xsl:when>
	              <xsl:otherwise>
	                <td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	            </xsl:choose>
	          </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
    	</tbody>
    	<tfoot>
 			<tr noWrap='true'>
        		<td align="left" style='fontsize:foot;colspan:colcount'></td>
        		<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<xsl:if test="$colNum = 16">
			  	<td style="display:none"></td>
			  	</xsl:if>
  			</tr>
 		</tfoot>
 		</root>
	</xsl:template>
</xsl:stylesheet>
