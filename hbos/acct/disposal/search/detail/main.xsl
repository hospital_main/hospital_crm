<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="ACCTYEAR" select="/root/annex/ACCTYEAR"/>
	  	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
  		<tr noWrap='true' class='mainHead'>
  		  <th id="yearText" colspan="2" valign="middle" nowrap='true'>
  		  	<xsl:value-of select="$ACCTYEAR" />
  		  </th>
  		  <th rowspan="2" valign="middle" nowrap='true'>凭证号</th>
	      <xsl:if test="$colNum = 16">
	  		  <th rowspan="2" valign="middle" nowrap='true'>项目</th>
	      </xsl:if> 		  
  		  <th rowspan="2" valign="middle" nowrap='true'>摘要</th>
  		  <th rowspan="2" valign="middle" nowrap='true'>核销状态</th>
  		  <th rowspan="2" valign="middle" nowrap='true'>合同号</th>
  		  <th rowspan="2" valign="middle" nowrap='true'>单据号</th>
  			<th rowspan="2" valign="middle" nowrap='true'>到期日期</th>
  			<th rowspan="2" valign="middle" nowrap='true'>借方</th>
  			<th rowspan="2" valign="middle" nowrap='true'>贷方</th>
  			<th rowspan="2" valign="middle" nowrap='true'>方向</th>
  			<th rowspan="2" valign="middle" nowrap='true'>余额</th>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
  		  <th nowrap='true'>月</th>
  		  <th nowrap='true'>日</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=3">
                <td>
                  <a href="#">
									  <xsl:attribute name="onclick">
									    javascript:openVouchDlg("&lt;vouch_id&gt;<xsl:value-of select="../td[$colNum - 2]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;")
									  </xsl:attribute>
									<xsl:value-of select="."/>
									</a>
                </td>
              </xsl:when>
			  			<xsl:when test="position()= $colNum - 5 or position()= $colNum - 6 ">
                <td align="right">
				 					<xsl:if test=".!=0"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if>
                </td>
              </xsl:when>
              <xsl:when test="position()=$colNum - 9 ">
              	<td noWrap="true">
							    <a href="#">
							  	  <xsl:attribute name="onclick">
							  	    openPact('<xsl:value-of select="."/>','<xsl:for-each select="../pk/*"><xsl:value-of select="."/></xsl:for-each>')
							  	  </xsl:attribute>
							  		<xsl:value-of select="."/>
							  	</a>
							  </td>
              </xsl:when>
              <xsl:when test="position()=$colNum - 12 ">
              	<td>
              		<xsl:if test="$colNum = 16">
               			<xsl:if test= "../td[$colNum -11 ]='本年累计' or ../td[$colNum -11 ] = '本月合计' "></xsl:if>
               		</xsl:if>
               		<xsl:if test="$colNum = 16">
               			<xsl:if test= "../td[$colNum -11 ] != '本年累计' and ../td[$colNum -11 ] != '本月合计' " ><xsl:value-of select="."/></xsl:if>
               		</xsl:if>
               		<xsl:if test="$colNum != 16">
               			<xsl:value-of select="."/>
               		</xsl:if>
               </td>
              </xsl:when>
              
              <xsl:when test="position()= $colNum - 3 ">
                <td align="right">
				 					<xsl:if test=".!=0 and ../td[$colNum]!='供应商' and ../td[$colNum]!='病人'"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if>
				 					<xsl:if test=".!=0 and (../td[$colNum]='供应商' or ../td[$colNum]='病人') and ../td[$colNum - 2]!=''">
					 					<!--a href='#'>
		                  <xsl:attribute name="onclick" >
		    	            javascript:openWin('<xsl:value-of select="../td[$colNum - 1]"/>','<xsl:value-of select="."/>');
		  	          	  </xsl:attribute-->
		  	          	  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		  	          	<!--/a-->
	  	          	</xsl:if>
	  	          	<xsl:if test=".!=0 and (../td[$colNum]='供应商' or ../td[$colNum]='病人') and ../td[$colNum - 2]=''">
		  	          	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	  	          	</xsl:if>
	  	          	<xsl:if test=". =0 and ../td[$colNum -4 ]='平' ">
		  	          	Q
	  	          	</xsl:if>
                </td>
              </xsl:when>
              <xsl:when test="position() = $colNum - 1 or position() = $colNum or position() = $colNum -2 ">
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>