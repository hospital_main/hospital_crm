<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>		
    	<thead>
    		<tr noWrap='true' class='mainHead'>
	    		<td style='colspan:colcount;fontsize:maintitle'>往来明细账查询</td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<xsl:if test="$colNum = 15">
			  	<td style="display:none"></td>
			  	</xsl:if>
      	</tr>
      	<tr noWrap='true' class='mainHead'>
	    		<td style='colspan:colcount;fontsize:subtitle'></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<xsl:if test="$colNum = 15">
			  	<td style="display:none"></td>
			  	</xsl:if>
      	</tr>
    	<tr noWrap='true' class='mainHead'>
  		  <td id="yearText" colspan="2" valign="middle" nowrap='true'>
  		  </td>
  		  <td style="display:none"></td>
  		  <td rowspan="2" valign="middle" nowrap='true'>凭证号</td>
	      <xsl:if test="$colNum = 15">
	  		  <td rowspan="2" valign="middle" nowrap='true'>项目</td>
	      </xsl:if> 		  
  		  <td rowspan="2" valign="middle" nowrap='true'>摘要</td>
  		  <td rowspan="2" valign="middle" nowrap='true'>核销状态</td>
  		  <td rowspan="2" valign="middle" nowrap='true'>合同号</td>
  			<td rowspan="2" valign="middle" nowrap='true'>到期日期</td>
  			<td rowspan="2" valign="middle" nowrap='true'>借方</td>
  			<td rowspan="2" valign="middle" nowrap='true'>贷方</td>
  			<td rowspan="2" valign="middle" nowrap='true'>方向</td>
  			<td rowspan="2" valign="middle" nowrap='true'>余额</td>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
  		  <td nowrap='true'>月</td>
  		  <td nowrap='true'>日</td>
  		  <td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<xsl:if test="$colNum = 15">
	  	<td style="display:none"></td>
	  	</xsl:if>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
	  	<td style="display:none"></td>
  		</tr>
    	</thead>
    	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	            <xsl:choose>
				  <xsl:when test="position()= $colNum - 3 or position()= $colNum - 5 or position()= $colNum - 6 ">
	                <td align="right">     
			            <xsl:if test=".!=''">
			            	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </xsl:if>                  
	                </td>
	              </xsl:when>
	              <xsl:when test="position() = $colNum - 1 or position() = $colNum or position() = $colNum -2">
	              </xsl:when>
	              <xsl:otherwise>
	                <td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	            </xsl:choose>
	          </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
    	</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>
