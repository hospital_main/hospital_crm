<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<root>
  	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
  	<xsl:variable name="colRnum" select="count(//tr)"/>
  	<xsl:variable name="count" select="0"/>
  	<xsl:variable name="SHOWDUIZHAO" select="/root/annex/SHOWDUIZHAO"/>		
    	<thead>
    	<tr noWrap='true' class='mainHead'>
	    		<td style='colspan:colcount;fontsize:maintitle;'>核销清册查询</td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
		  	  <xsl:if test="$colNum = 26">
          	<td style="display:none"></td>
          </xsl:if> 
					<xsl:if test="$SHOWDUIZHAO = '1'">
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>s
	  			</xsl:if>
			  	
      	</tr>
      	<tr noWrap='true' class='mainHead'>
	    		<td style='colspan:colcount;fontsize:subtitle'></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<xsl:if test="$colNum = 26">
  		      <td style="display:none"></td>
  		    </xsl:if> 
					<xsl:if test="$SHOWDUIZHAO = '1'">
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
				  	<td style="display:none"></td>
	  			</xsl:if>
			  	
      	</tr>
  		<tr noWrap='true' class='mainHead'>
  		  <td id="yearText" colspan="3" valign="center" nowrap='true'>日期</td>
  		  <td style="display:none"></td>
  		  <td style="display:none"></td>
  		  <td rowspan="2" valign="center" nowrap='true'>凭证号</td>
	      <xsl:if test="$colNum = 26">
	  		  <td rowspan="2" valign="center" nowrap='true'>项目</td>
	      </xsl:if> 	
  		  <td rowspan="2" valign="middle" nowrap='true'>摘要</td>
  		  <td rowspan="2" valign="center" nowrap='true'>核销状态</td>
  		  <td rowspan="2" valign="center" nowrap='true'>合同号</td>
  		  <td rowspan="2" valign="center" nowrap='true'>单据号</td>
  			<td rowspan="2" valign="center" nowrap='true'>到期日期</td>
  			<td rowspan="2" valign="center" nowrap='true'>借方金额</td>
  			<td rowspan="2" valign="center" nowrap='true'>贷方金额</td>
  			<td rowspan="2" valign="center" nowrap='true'>已核销金额</td>
				<xsl:if test="$SHOWDUIZHAO = '1'">
  			<td colspan="9" valign="middle" nowrap='true'>对照信息</td>
  			</xsl:if>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
  			<td nowrap='true'>年</td>
  		  <td nowrap='true'>月</td>
  		  <td nowrap='true'>日</td>
  		   <td style="display:none"></td>
  		   <td style="display:none"></td>
  		   <td style="display:none"></td>
  		   <td style="display:none"></td>
  		   <td style="display:none"></td>
  		   <td style="display:none"></td>
  		   <td style="display:none"></td>
  		   <td style="display:none"></td>
  		   <td style="display:none"></td>
  		   <xsl:if test="$colNum = 26">
  		     <td style="display:none"></td>
  		   </xsl:if> 	
				<xsl:if test="$SHOWDUIZHAO = '1'">
			  	<td valign="center" nowrap='true'>核销日期</td>
			  	<td valign="center" nowrap='true'>凭证日期</td>
			  	<td valign="center" nowrap='true'>凭证号</td>
			  	<td valign="center" nowrap='true'>摘要</td>
			  	<td valign="center" nowrap='true'>合同号</td>
			  	<td valign="center" nowrap='true'>单据号</td>
			  	<td valign="center" nowrap='true'>到期日期</td>
			  	<td valign="center" nowrap='true'>借方金额</td>
			  	<td valign="center" nowrap='true'>贷方金额</td>
  			</xsl:if>
  		</tr>
    	</thead>
    	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	      <xsl:variable name="tdlast" select="./td[$colNum]" />
 				<xsl:variable name="mpos" select="position() - 1" />
 				<xsl:variable name="rowspan_coun" select="count(/root/tbody/tr[td[$colNum]=$tdlast])" />
	      		<xsl:if test="position() = 1">
	      			<tr>
	      			<td>
			       			<xsl:value-of select="td[1]" />
			   			 </td>  
			         <td>
			       			<xsl:value-of select="td[2]" />
			   			 </td>  
			         <td>
			       			<xsl:value-of select="td[3]" />
			   			 </td> 
			   			 <td>
											<xsl:value-of select="td[4]"/>
			   			  </td> 
	      				<xsl:if test="$colNum = 26">
					  		  <td>
					       			<xsl:value-of select="td[$colNum - 21]" />
					   			 </td> 
					   		</xsl:if>
				         <td>
				       			<xsl:value-of select="td[$colNum - 20]" />
				   			 </td> 
				         <td>
				       			<xsl:value-of select="td[$colNum - 19]" />
				   			 </td>
				   			 <td>
				       			<xsl:value-of select="td[$colNum - 18]" />
				   			 </td>
				         <td>
				       			<xsl:value-of select="td[$colNum - 17]" />
				   			 </td> 
				         <td>
				       			<xsl:value-of select="td[$colNum - 16]" />
				   			 </td> 
				         <td align="right">
				       			<xsl:value-of select="format-number(td[$colNum - 15],'#,##0.00')" />
				   			 </td> 
				         <td align="right">
				       			<xsl:value-of select="format-number(td[$colNum - 14],'#,##0.00')" />
				   			 </td> 
				         <td align="right">
								 					<xsl:if test="td[$colNum - 13]!=0"><xsl:value-of select="format-number(td[$colNum - 13],'#,##0.00')"/></xsl:if>
				   			 </td> 
				   			 <xsl:if test="$SHOWDUIZHAO = '1'">
				         <td><xsl:value-of select="td[$colNum - 10]" /></td> 
				         <td><xsl:value-of select="td[$colNum - 9]" /></td> 
				         <td><xsl:value-of select="td[$colNum - 8]"/></td> 
				         <td><xsl:value-of select="td[$colNum - 7]" /></td> 
				         <td><xsl:value-of select="td[$colNum - 6]" /></td> 
				         <td><xsl:value-of select="td[$colNum - 5]" /></td>
				         <td><xsl:value-of select="td[$colNum - 4]" /></td>
				         <td align="right"><xsl:if test="td[$colNum - 3]!=0"><xsl:value-of select="format-number(td[$colNum - 3],'#,##0.00')"/></xsl:if></td> 
				         <td align="right"><xsl:if test="td[$colNum - 2]!=0"><xsl:value-of select="format-number(td[$colNum - 2],'#,##0.00')"/></xsl:if></td> 
				   		</xsl:if>
				   		</tr>
						</xsl:if>
	      		<xsl:if test="position() &gt; 1">
	      		<xsl:if test="$tdlast!=(../tr[$mpos]/td[$colNum])">
	      			<tr>
			      	 <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[1]" />
			   			 </td>  
			         <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[2]" />
			   			 </td>  
			         <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[3]" />
			   			 </td> 
			   			 <td rowspan="{$rowspan_coun}">
											<xsl:value-of select="td[4]"/>
			   			  </td> 
	      				<xsl:if test="$colNum = 26">
					  		  <td rowspan="{$rowspan_coun}">
					       			<xsl:value-of select="td[$colNum - 21]" />
					   			 </td> 
					   		</xsl:if>
				         <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 20]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 19]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 18]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 17]" />
				   			 </td>
				   			 <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 16]" />
				   			 </td>  
				         <td rowspan="{$rowspan_coun}" align="right">
				       			<xsl:value-of select="format-number(td[$colNum - 15],'#,##0.00')" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}" align="right">
				       			<xsl:value-of select="format-number(td[$colNum - 14],'#,##0.00')" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}" align="right">
								 					<xsl:if test="td[$colNum - 13]!=0"><xsl:value-of select="format-number(td[$colNum - 13],'#,##0.00')"/></xsl:if>
				   			 </td> 
						   	<xsl:if test="$SHOWDUIZHAO = '1'">
						   			 <td><xsl:value-of select="td[$colNum - 10]" /></td>
						         <td><xsl:value-of select="td[$colNum - 9]" /></td> 
						         <td><xsl:value-of select="td[$colNum - 8]" /></td> 
						         <td><xsl:value-of select="td[$colNum - 7]"/></td> 
						         <td><xsl:value-of select="td[$colNum - 6]" /></td> 
						         <td><xsl:value-of select="td[$colNum - 5]" /></td> 
						         <td><xsl:value-of select="td[$colNum - 4]" /></td> 
						         <td align="right"><xsl:value-of select="td[$colNum - 3]" /></td> 
						         <td align="right"><xsl:value-of select="td[$colNum - 2]" /></td> 
								</xsl:if> 
				   		</tr>
						</xsl:if> 
						<xsl:if test="$tdlast=(../tr[$mpos]/td[$colNum])">
				   		<tr>
  		       <td></td>
  		       <td></td>
  		       <td></td>
  		       <td></td>
  		       <td></td>
  		       <td></td>
  		       <td></td>
  		       <td></td>
  		       <td></td>
	  		     <td></td>
	  		     <td></td>
				  	<xsl:if test="$colNum = 26">
	  		     <td></td>
	  		    </xsl:if> 
						   	<xsl:if test="$SHOWDUIZHAO = '1'">
						   			 <td><xsl:value-of select="td[$colNum - 10]" /></td>
						         <td><xsl:value-of select="td[$colNum - 9]" /></td> 
						         <td><xsl:value-of select="td[$colNum - 8]" /></td> 
						         <td><xsl:value-of select="td[$colNum - 7]"/></td> 
						         <td><xsl:value-of select="td[$colNum - 6]" /></td> 
						         <td><xsl:value-of select="td[$colNum - 5]" /></td> 
						         <td><xsl:value-of select="td[$colNum - 4]" /></td> 
						         <td align="right"><xsl:if test="td[$colNum - 3]!=0"><xsl:value-of select="format-number(td[$colNum - 3],'#,##0.00')"/></xsl:if></td> 
						         <td align="right"><xsl:if test="td[$colNum - 2]!=0"><xsl:value-of select="format-number(td[$colNum - 2],'#,##0.00')"/></xsl:if></td> 
								</xsl:if> 
				   		</tr>
						</xsl:if> 
					</xsl:if>
   		</xsl:for-each>  	
  		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>