<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:variable name="SHOWDUIZHAO" select="/root/annex/SHOWDUIZHAO"/>
	  	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
  		<tr noWrap='true' class='mainHead'>
  		  <th  colspan="3" valign="middle" nowrap='true'>日期</th>
  		  <th rowspan="2" valign="middle" nowrap='true'>凭证号</th>
	      <xsl:if test="$colNum = 24">
	  		  <th rowspan="2" valign="middle" nowrap='true'>项目</th>
	      </xsl:if> 	
  		  <th rowspan="2" valign="middle" nowrap='true'>摘要</th>
  		  <th rowspan="2" valign="middle" nowrap='true'>核销状态</th>
  		  <th rowspan="2" valign="middle" nowrap='true'>单据号</th>
  			<th rowspan="2" valign="middle" nowrap='true'>到期日期</th>
  			<th rowspan="2" valign="middle" nowrap='true'>借方金额</th>
  			<th rowspan="2" valign="middle" nowrap='true'>贷方金额</th>
  			<th rowspan="2" valign="middle" nowrap='true'>已核销金额</th>
				<xsl:if test="$SHOWDUIZHAO = '1'">
  			<th colspan="8" valign="middle" nowrap='true'>对照信息</th>
  			</xsl:if>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>年</th>
  		  <th nowrap='true'>月</th>
  		  <th nowrap='true'>日</th>
			<xsl:if test="$SHOWDUIZHAO = '1'">
  		  <th valign="middle" nowrap='true'>核销日期</th>
  		  <th valign="middle" nowrap='true'>凭证日期</th>
  		  <th valign="middle" nowrap='true'>凭证号</th>
  		  <th valign="middle" nowrap='true'>摘要</th>
  		  <th valign="middle" nowrap='true'>单据号</th>
  			<th valign="middle" nowrap='true'>到期日期</th>
  			<th valign="middle" nowrap='true'>借方金额</th>
  			<th valign="middle" nowrap='true'>贷方金额</th>
  			</xsl:if>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	      <xsl:variable name="tdlast" select="./td[$colNum]" />
 				<xsl:variable name="mpos" select="position()-1" />
 				<xsl:variable name="rowspan_coun" select="count(/root/tbody/tr[td[$colNum]=$tdlast])" />
        <tr>
			      <xsl:if test="position()=1">
			      	 <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[1]" />
			   			 </td>  
			         <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[2]" />
			   			 </td>  
			         <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[3]" />
			   			 </td> 
			   			 <td rowspan="{$rowspan_coun}">
			         	<xsl:choose>           
	            			<xsl:when test="td[$colNum - 11]=''">
											<xsl:value-of select="td[4]"/>
			              </xsl:when>
			              <xsl:otherwise>
	                  <a href="#">
										  <xsl:attribute name="onclick">
										    javascript:openVouchDlg("&lt;vouch_id&gt;<xsl:value-of select="td[$colNum - 11]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;")
										  </xsl:attribute>
											<xsl:value-of select="td[4]"/>
										</a>
			              </xsl:otherwise>
	            		</xsl:choose>
			   			  </td> 
	      				<xsl:if test="$colNum = 24">
					  		  <td rowspan="{$rowspan_coun}">
					       			<xsl:value-of select="td[$colNum - 19]" />
					   			 </td> 
					   		</xsl:if>
				         <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 18]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 17]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 16]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 15]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}" align="right">
				       			<xsl:value-of select="td[$colNum - 14]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}" align="right">
				       			<xsl:value-of select="td[$colNum - 13]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}" align="right">
				       			<xsl:choose>           
		              			<xsl:when test="td[$colNum - 10]!=''">
				                	<a href="#">
													  <xsl:attribute name="onclick">
													    javascript:openDetail("&lt;acct_check_id&gt;<xsl:value-of select="td[$colNum - 10]"/>&lt;/acct_check_id&gt;")
													  </xsl:attribute>
								 						<xsl:if test="td[$colNum - 12]!=0"><xsl:value-of select="format-number(td[$colNum - 12],'#,##0.00')"/></xsl:if>
													</a>
				              </xsl:when>
				              <xsl:otherwise>
								 					<xsl:if test="td[$colNum - 12]!=0"><xsl:value-of select="format-number(td[$colNum - 12],'#,##0.00')"/></xsl:if>
				              </xsl:otherwise>
		            		</xsl:choose>
				   			 </td> 
				   			 <xsl:if test="$SHOWDUIZHAO = '1'">
				         <td><xsl:value-of select="td[$colNum - 9]" /></td> 
				         <td><xsl:value-of select="td[$colNum - 8]" /></td> 
				         <td>
				         		<xsl:choose>           
	            			<xsl:when test="td[$colNum - 1]=''">
											<xsl:value-of select="td[$colNum - 7]"/>
			              </xsl:when>
			              <xsl:otherwise>
	                  <a href="#">
										  <xsl:attribute name="onclick">
										    javascript:openVouchDlg("&lt;vouch_id&gt;<xsl:value-of select="td[$colNum - 1]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;")
										  </xsl:attribute>
											<xsl:value-of select="td[$colNum - 7]"/>
										</a>
			              </xsl:otherwise>
	            		</xsl:choose>
	            </td> 
				         <td><xsl:value-of select="td[$colNum - 6]" /></td> 
				         <td><xsl:value-of select="td[$colNum - 5]" /></td> 
				         <td><xsl:value-of select="td[$colNum - 4]" /></td> 
				         <td align="right"><xsl:if test="td[$colNum - 3]!=0"><xsl:value-of select="format-number(td[$colNum - 3],'#,##0.00')"/></xsl:if></td> 
				         <td align="right"><xsl:if test="td[$colNum - 2]!=0"><xsl:value-of select="format-number(td[$colNum - 2],'#,##0.00')"/></xsl:if></td> 
				   			 </xsl:if>
						</xsl:if> 
						
		        <xsl:if test="position()>1">
					    <xsl:if test="$tdlast!=(../tr[$mpos]/td[$colNum])">
			      	 <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[1]" />
			   			 </td>  
			         <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[2]" />
			   			 </td>  
			         <td rowspan="{$rowspan_coun}">
			       			<xsl:value-of select="td[3]" />
			   			 </td> 
			   			 <td rowspan="{$rowspan_coun}">
			         	<xsl:choose>           
	            			<xsl:when test="td[$colNum - 11]=''">
											<xsl:value-of select="td[4]"/>
			              </xsl:when>
			              <xsl:otherwise>
	                  <a href="#">
										  <xsl:attribute name="onclick">
										    javascript:openVouchDlg("&lt;vouch_id&gt;<xsl:value-of select="td[$colNum - 11]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;")
										  </xsl:attribute>
											<xsl:value-of select="td[4]"/>
										</a>
			              </xsl:otherwise>
	            		</xsl:choose>
			   			  </td> 
	      				<xsl:if test="$colNum = 24">
					  		  <td rowspan="{$rowspan_coun}">
					       			<xsl:value-of select="td[$colNum - 19]" />
					   			 </td> 
					   		</xsl:if>
				         <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 18]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 17]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 16]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}">
				       			<xsl:value-of select="td[$colNum - 15]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}" align="right">
				       			<xsl:value-of select="td[$colNum - 14]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}" align="right">
				       			<xsl:value-of select="td[$colNum - 13]" />
				   			 </td> 
				         <td rowspan="{$rowspan_coun}" align="right">
				       			<xsl:choose>           
		              			<xsl:when test="td[$colNum - 10]!=''">
				                	<a href="#">
													  <xsl:attribute name="onclick">
													    javascript:openDetail("&lt;acct_check_id&gt;<xsl:value-of select="td[$colNum - 10]"/>&lt;/acct_check_id&gt;")
													  </xsl:attribute>
								 						<xsl:if test="td[$colNum - 12]!=0"><xsl:value-of select="format-number(td[$colNum - 12],'#,##0.00')"/></xsl:if>
													</a>
				              </xsl:when>
				              <xsl:otherwise>
								 					<xsl:if test="td[$colNum - 12]!=0"><xsl:value-of select="format-number(td[$colNum - 12],'#,##0.00')"/></xsl:if>
				              </xsl:otherwise>
		            		</xsl:choose>
				   			 </td> 
						</xsl:if> 
				   			 <xsl:if test="$SHOWDUIZHAO = '1'">
				         <td><xsl:value-of select="td[$colNum - 9]" /></td> 
				         <td><xsl:value-of select="td[$colNum - 8]" /></td> 
				         <td>
				         		<xsl:choose>           
	            			<xsl:when test="td[$colNum - 1]=''">
											<xsl:value-of select="td[$colNum - 7]"/>
			              </xsl:when>
			              <xsl:otherwise>
	                  <a href="#">
										  <xsl:attribute name="onclick">
										    javascript:openVouchDlg("&lt;vouch_id&gt;<xsl:value-of select="td[$colNum - 1]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;")
										  </xsl:attribute>
											<xsl:value-of select="td[$colNum - 7]"/>
										</a>
			              </xsl:otherwise>
	            		</xsl:choose>
				         </td> 
				         <td><xsl:value-of select="td[$colNum - 6]" /></td> 
				         <td><xsl:value-of select="td[$colNum - 5]" /></td> 
				         <td><xsl:value-of select="td[$colNum - 4]" /></td> 
				         <td align="right"><xsl:if test="td[$colNum - 3]!=0"><xsl:value-of select="format-number(td[$colNum - 3],'#,##0.00')"/></xsl:if></td> 
				         <td align="right"><xsl:if test="td[$colNum - 2]!=0"><xsl:value-of select="format-number(td[$colNum - 2],'#,##0.00')"/></xsl:if></td> 
						</xsl:if> 
					</xsl:if> 
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>