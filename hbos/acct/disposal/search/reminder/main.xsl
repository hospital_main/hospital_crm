<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<thead>
  		<tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true">部门</th>
  	  	<th noWrap="true">职工</th>
  			<th noWrap="true">日期</th>
        <th noWrap="true">凭证号</th>
  			<th noWrap="true">摘要</th>
  			<th noWrap="true">借款金额</th>
  			<th noWrap="true">还款金额</th>
  			<th noWrap="true">核销状态</th>
  	  </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
  			<tr>
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test="position()=1 or position()=2 ">
          			<td>
	          			<xsl:if test="../td[6]!='合计' and ../td[6]!='余额' ">
	          				<xsl:value-of select="."/>
	          			</xsl:if>
          			</td>
          		</xsl:when>
          		<xsl:when test="position()=3">
          		</xsl:when>
          		<xsl:when test="position()=5">
          			<xsl:variable name="vouch_id" select='../pk/vouch_id'/>
          			<xsl:if test="$vouch_id !=''">
									<td>
										<a href="#">
											<xsl:attribute name="onclick">openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;&lt;/edit_mask&gt;")</xsl:attribute>
											<xsl:value-of select="."/> 
										</a>
									</td>
								</xsl:if>
          			<xsl:if test="$vouch_id=''">
									<td>
											<xsl:value-of select="."/> 
									</td>
								</xsl:if>
								
							</xsl:when>
	            <xsl:when test="position()=7 or position()=8 ">
	            	<td  align="right" ><xsl:value-of select="format-number(.,'#,##0.00' )"/></td>
	            </xsl:when>
	            <xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
          	</xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>