<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-2"/>
	  <root>
	    <thead>
		    <tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	      	<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	      </tr>
	      <tr noWrap="true" class="mainHead">
	  	  	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
	  	  	<td style="display:none"/>
	  	  	<td noWrap="true" style="fontsize:subtitle;colspan:2;align:left"/>
	  	  	<td style="display:none"/>
	  	  	<td noWrap="true" style="fontsize:subtitle;colspan:4;align:left"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  </tr>
	  	  <tr noWrap="true" class="mainHead">
	  	  	<td noWrap="true" style="fontsize:subtitle;colspan:8;align:left"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  </tr>
		 		<tr noWrap='true' class='mainHead'>
	  			<td noWrap="true">部门</td>
	  	  	<td noWrap="true">职工</td>
	  			<td noWrap="true">日期</td>
	        <td noWrap="true">凭证号</td>
	  			<td noWrap="true">摘要</td>
	  			<td noWrap="true">借款金额</td>
	  			<td noWrap="true">还款金额</td>
	  			<td noWrap="true">核销状态</td>
	  	  </tr>
	  	</thead>
		  <tbody>
		    <xsl:for-each select="/root/tbody/tr">
  			<tr>
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test="position()=1 or position()=2 ">
          			<td>
	          			<xsl:if test="../td[6]!='合计' and ../td[6]!='余额' ">
	          				<xsl:value-of select="."/>
	          			</xsl:if>
          			</td>
          		</xsl:when>
          		<xsl:when test="position()=3">
          		</xsl:when>
          		<xsl:when test="position()=5">
								<td>
									<a href="#">
										<xsl:attribute name="onclick">openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;&lt;/edit_mask&gt;")</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
								</td>
							</xsl:when>
	            <xsl:when test="position()=7 or position()=8 ">
	            	<td  align="right" ><xsl:value-of select="format-number(.,'#,##0.00' )"/></td>
	            </xsl:when>
	            <xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
          	</xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
	 		</tbody>
	  </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>