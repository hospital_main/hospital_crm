<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
    <table width='100%' class='mainTable' border='1' borderColor='#292954'>
    	<thead>
    		<tr noWrap='true' class='mainHead'>
    			<th noWrap='true'>凭证日期</th>
    			<th noWrap='true'>凭证号</th>
    			<th noWrap='true'>单据号</th>
    			<th noWrap='true'>摘 要</th>
    			<th noWrap='true'>贷方金额</th>
    			<th noWrap='true'>已对帐</th>
    			<th noWrap='true'>未对帐</th>
    		</tr>
    	</thead>
    	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
          <tr>
          	<xsl:attribute name="run_acct_id"><xsl:value-of select="td[position()=7]"/></xsl:attribute>
            <xsl:attribute name="con"><xsl:value-of select="td[position()=8]"/></xsl:attribute>
            <xsl:attribute name="is_check"><xsl:value-of select="td[position()=6]"/></xsl:attribute>
            <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position()=5">
              		<td align='right' noWrap='true'>
			  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
	        			</xsl:when>
	        			<xsl:when test="position()=6">
	                <td align='center' noWrap='true' value="">
	                  <xsl:if test=". = 1">√</xsl:if>
	                </td>
                </xsl:when>
                <xsl:when test="position()&gt;=1 and position()&lt;=4">
                  <td align='center' noWrap='true' value="">
										<xsl:value-of select="."/>
                  </td>
                </xsl:when>
              </xsl:choose>
    			  </xsl:for-each>
            <td align='center' noWrap='true' value="">
              <input type='checkbox'/>
            </td>
    			</tr>
     		</xsl:for-each>
   		</tbody>
    </table>
	</xsl:template>
</xsl:stylesheet>




<!--
xslt语法
引用value用.
引用属性用@

-->
