<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  		  <th nowrap='true'>选择</th>
  			<!--th nowrap='true'>应收科目</th-->
  			<th nowrap='true'>坏账提取科目</th>
  			<th nowrap='true'>管理费用科目</th>
  			<th nowrap='true'>凭证日期</th>
  			<th noWrap='true'>应收期末余额</th>
  			<th noWrap='true'>坏账准备期末余额</th>
  			<th noWrap='true'>提取坏账金额</th>
  			<th nowrap='true'>凭证号</th>
  		</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'  style='display:none'>
					<input type='checkbox' TABINDEX='-1'>
						<xsl:attribute name="value" >
							<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
						</xsl:attribute>
					</input>
					</td>
				<xsl:for-each select="td">
					<xsl:choose>
					  <xsl:when test="position()=4 or position()=5 or position()=7">
					    <td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
					  </xsl:when>
					  <xsl:when test="position()=6">
					  </xsl:when>
						<xsl:when test="position()=8">
								<td><a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="."/>
			  	          	</a></td>
						</xsl:when>
						<xsl:when test="position()>8">
							<td style="display:none">
								<xsl:value-of select="."/>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>