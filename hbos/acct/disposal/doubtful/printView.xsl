<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
  	<thead>
   	<tr>
   		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">8</xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
  		<tr noWrap='true' class='mainHead'>
  			<!--th nowrap='true'>应收科目</th-->
  			<td nowrap='true'>坏账提取科目</td>
  			<td nowrap='true'>管理费用科目</td>
  			<td nowrap='true'>凭证日期</td>
  			<td noWrap='true'>应收期末余额</td>
  			<td noWrap='true'>坏账准备期末余额</td>
  			<td noWrap='true'>提取比例(%)</td>
  			<td noWrap='true'>提取坏账金额</td>
  			<td nowrap='true'>凭证号</td>
  		</tr>
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<xsl:choose> 
					  <xsl:when test="position()=4 or position()=5 or position()=6 or position()=7">
					    <td align="right">
								<xsl:value-of select="format-number(.,'#,#00.00')"/>					    
							</td>
					  </xsl:when>       
						<xsl:when test="position()=8">
							<td>
								<xsl:value-of select="."/>
							</td>
						</xsl:when>
						<xsl:when test="position()>8">
							<td style="display:none">
								<xsl:value-of select="."/>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>            
				</xsl:for-each>
			</tr>
		</xsl:for-each>  	
	</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>