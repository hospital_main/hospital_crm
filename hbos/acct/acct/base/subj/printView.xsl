<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">22</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
      	<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
		  <tr noWrap='true' class='mainHead'>
      	<td nowrap='true' rowspan='2' valign='middle'>科目编码</td>
      	<td nowrap='true' rowspan='2' valign='middle'>科目名称</td>
      	<td nowrap='true' rowspan='2' valign='middle'>项目编码</td>
      	<td nowrap='true' rowspan='2' valign='middle'>项目名称</td>
      	<td nowrap='true' rowspan='2' valign='middle'>年度预算</td>
      	<td nowrap='true' rowspan='2' valign='middle'>预算追加</td>
      	<td nowrap='true' rowspan='2' valign='middle'>总预算</td>
      	<td nowrap='true' colspan='15' valign='middle'>完成预算</td>
      	<td style='display:none'/>
      	<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
      	<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
      </tr>
      <tr noWrap='true' class='mainHead'>
        <td style='display:none'/>
      	<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
      	<td nowrap='true'>完成合计</td>
      	<td nowrap='true'>差额</td>
      	<td nowrap='true'>百分比(%)</td>
      	<td nowrap='true'>01月</td>
      	<td nowrap='true'>02月</td>
      	<td nowrap='true'>03月</td>
      	<td nowrap='true'>04月</td>
      	<td nowrap='true'>05月</td>
      	<td nowrap='true'>06月</td>
      	<td nowrap='true'>07月</td>
      	<td nowrap='true'>08月</td>
      	<td nowrap='true'>09月</td>
      	<td nowrap='true'>10月</td>
      	<td nowrap='true'>11月</td>
      	<td nowrap='true'>12月</td>
      </tr>
			</thead>
			<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position()=10">
                        <td align='right'>
                          <xsl:value-of select="format-number(.,'#,##0.00')"/>
                        </td>
                      </xsl:when>
                      <xsl:otherwise>
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                    </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
