<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true' rowspan='2' valign='middle'>科目编码</th>
      	<th nowrap='true' rowspan='2' valign='middle'>科目名称</th>
      	<th nowrap='true' rowspan='2' valign='middle'>项目编码</th>
      	<th nowrap='true' rowspan='2' valign='middle'>项目名称</th>
      	<th nowrap='true' rowspan='2' valign='middle'>年度预算</th>
      	<th nowrap='true' rowspan='2' valign='middle'>预算追加</th>
      	<th nowrap='true' rowspan='2' valign='middle'>总预算</th>
      	<th nowrap='true' colspan='15' valign='middle'>完成预算</th>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>完成合计</th>
      	<th nowrap='true'>差额</th>
      	<th nowrap='true'>百分比(%)</th>
      	<th nowrap='true'>01月</th>
      	<th nowrap='true'>02月</th>
      	<th nowrap='true'>03月</th>
      	<th nowrap='true'>04月</th>
      	<th nowrap='true'>05月</th>
      	<th nowrap='true'>06月</th>
      	<th nowrap='true'>07月</th>
      	<th nowrap='true'>08月</th>
      	<th nowrap='true'>09月</th>
      	<th nowrap='true'>10月</th>
      	<th nowrap='true'>11月</th>
      	<th nowrap='true'>12月</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position()>4">
                        <td align='right'>
                          <xsl:value-of select="format-number(.,'#,##0.00')"/>
                        </td>
                      </xsl:when>
                      <xsl:otherwise>
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                    </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

