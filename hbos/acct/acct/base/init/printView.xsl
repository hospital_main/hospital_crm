<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/acct/base/init/printView.xsl,v 1.1 2012/03/12 01:44:15 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:15 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		
		<thead>
			<tr noWrap='true' class='mainHead'>
				<td style='colspan:6;fontsize:maintitle'>科目初始账</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td style=''>科目编码</td>
				<td nowrap='true'>科目名称</td>
				<td nowrap='true'>方向</td>
				<td nowrap='true'>币种</td>
				<td nowrap='true'>计量</td>
				<td nowrap='true'>年初余额</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position() &gt; 6]"> 
						<xsl:choose>
							<xsl:when test="position()=6">
								<td class="numberText" align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td align='left'><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
