<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'> 			
  			<th nowrap='true'>选择</th>
  			<th nowrap='true'>旧科目编码</th>
  			<th nowrap='true'>旧科目名称</th>
  			<th nowrap='true'>方向</th>
  			<th nowrap='true'>金额</th>
  			<th nowrap='true'>新科目编码</th>
  			<th nowrap='true'>新科目名称</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
         <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                  <a href="#">
									  <xsl:attribute name="onclick">
									    javascript:openDialog('set.html?load=&lt;cheque_id&gt;<xsl:value-of select="../td[1]"/>&lt;/cheque_id&gt;','dialogWidth:900px;dialogHeight:600px')
									  </xsl:attribute>
                 		<xsl:value-of select="."/>
									</a>
                </td>
              </xsl:when>
              <xsl:when test="position()=4">
                <td align="center">
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>