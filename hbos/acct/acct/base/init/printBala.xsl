<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/acct/base/init/printBala.xsl,v 1.1 2012/03/12 01:44:15 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:15 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
		<root>
		<colgroup>
			<col style = 'width:100mm'/>
			<col style = 'width:300mm'/>
			<col style = 'width:150mm'/>
			<col style = 'width:150mm'/>
			<xsl:if test=" $colNum &gt; 4 ">
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
			</xsl:if>
		</colgroup>
		<thead>
			<tr noWrap='true' class='mainHead'>
				<td rowspan="2" valign="center">科目编码</td>
				<td rowspan="2" valign="center">科目名称</td>
				<td colspan="2">年初余额</td>
				<td style="display:none"></td>
				<xsl:if test=" $colNum &gt; 4 ">
					<td colspan="2">本年累计</td>
					<td style="display:none"></td>
					<td colspan="2">期初余额</td>
					<td style="display:none"></td>
				</xsl:if>
			</tr>
	  		<tr noWrap='true' class='mainHead'>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
				<td>借方</td>
				<td>贷方</td>
				<xsl:if test=" $colNum &gt; 4 ">
					<td>借方</td>
					<td>贷方</td>
					<td>借方</td>
					<td>贷方</td>
				</xsl:if>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()!=1 and position()!=2">
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
