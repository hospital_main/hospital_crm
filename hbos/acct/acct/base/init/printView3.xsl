<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN="0"/>
  <xsl:template match="/">
  	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td)-1 "/>
  	<xsl:variable name="rowNums" select="count(/root/tbody/tr)"/>
  	<root>
    	<thead>
    		<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNums"/>    
	        </xsl:call-template>
	  		</tr>
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="/root/tbody/tr[position()=1]">
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
									<td><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:otherwise>
	                <td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
              </xsl:choose>
						</xsl:for-each>
					</xsl:for-each>
				</tr>
			</thead>
    	<tbody>
  	    <xsl:for-each select="/root/tbody/tr[position()>1]">
          <tr>
            <xsl:for-each select="td">
              <xsl:choose>
								<xsl:when test="position()=1">
									<td><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:otherwise>
	                <td><xsl:value-of select="."/>
	                </td>
	              </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
    			</tr>
     		</xsl:for-each> 
    	</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
	<xsl:template name="repeat2">  
		<xsl:param name="times2" select="0"/>  
		<xsl:if test="$times2 > 0">  
			<td></td>
			<xsl:call-template  name="repeat2">  
				<xsl:with-param  name="times2" select="$times2 - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
	<xsl:template name="calculate_score"> 
		<xsl:param name="pre_sum" /> 
		<xsl:param name="last_node" />
		<xsl:variable name="ass_score" select="number(translate(/root/tbody/tr[ $last_node - 1 ]/td[last()],',',''))" />
		<xsl:variable name="curr_sum" select="number($pre_sum + $ass_score)" />
		<xsl:if test ="$last_node = 2">
			<xsl:value-of select="format-number( $pre_sum ,'#,##0.00')" />
		</xsl:if>
		<xsl:choose> 
			<xsl:when test="number($last_node -1) &gt; 1 "> 
				<xsl:call-template name="calculate_score"> 
					<xsl:with-param name="pre_sum">
						<xsl:value-of select="$curr_sum" />
					</xsl:with-param> 
					<xsl:with-param name="last_node">
						<xsl:value-of select="number($last_node -1)" />
					</xsl:with-param> 
				</xsl:call-template> 
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>