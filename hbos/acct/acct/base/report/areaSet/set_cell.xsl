<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap='true' class='mainHead'>
        <th>�к�</th>
  		<th>����</th>
   	 </tr>
   </thead>
   <tbody>
   <xsl:for-each select="/root/tbody/tr">
    <xsl:variable name='CurTrPos' select='position()'/>
   <tr>          
   		<xsl:for-each select="td">
			<xsl:choose>
              <xsl:when test="position() = 2">
              	<td align='left'>
              	<input type="text" class='inputSelect' TABINDEX='-1' load='report_acctdef_col_define_list'  extent='250' required='true'>
              	<xsl:attribute name='name' >colproperty_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
              	<xsl:attribute name='value'><xsl:value-of select="."/></xsl:attribute>
              	</input>
				</td>
              </xsl:when>
              <xsl:otherwise>
                <td align='center'><xsl:value-of select="."/></td>
              </xsl:otherwise>
        	</xsl:choose>
    	</xsl:for-each>
    </tr>
    </xsl:for-each>
	</tbody>
   </xsl:template>
</xsl:stylesheet>
