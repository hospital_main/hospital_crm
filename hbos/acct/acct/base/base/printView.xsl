<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/acct/base/base/printView.xsl,v 1.1 2012/03/12 01:44:15 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:15 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:200mm'/>
				<col style = 'width:200mm'/>
				<col style = 'width:200mm'/>
				<col style = 'width:100mm'/>
			</colgroup>
			<thead>
		<tr>
			<td style="fontsize:maintitle;colspan:7"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
      <tr noWrap='true' class='mainHead'>
      	<td nowrap='true'>分析指标编码</td>
      	<td nowrap='true'>分析指标名称</td>
      	<td nowrap='true'>指标单位</td>
      	<td nowrap='true'>基本分析指标类别</td>
      	<td nowrap='true'>五性分析指标类别</td>
      	<td nowrap='true'>备注</td>
      	<td nowrap='true'>是否停用</td>
      </tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position() = 5">
								<td>
										<xsl:value-of select="."/>
								</td>
								</xsl:when>
								<xsl:otherwise>
								<td>
										<xsl:value-of select="."/>
								</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
