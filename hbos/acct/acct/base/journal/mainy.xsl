<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="total" select="count(/root/tbody/tr[1]/td)"/>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>凭证编号</th>
				<th>凭证日期</th>
				<th>附件</th>
				<th>摘要</th>
				<th>科目编码</th>
				<th>科目名称</th>
				<th>借方金额</th>
				<th>贷方金额</th>
				<th>制单人</th>
				<xsl:if test="$total = 16+1">
						<th>签字人</th>
				</xsl:if>
				<th>审核人</th>
				<th>记账人</th>
				<th>标错信息</th>
			</tr>
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
	          <td align='center'  style='display:none'>
	          <xsl:if test="td[1]!= '合计'">
						<input type='checkbox' TABINDEX='-1' align='center'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
						</input>
						</xsl:if>
						<xsl:if test="td[1]='合计'">
							<td align='center'></td>
						</xsl:if>
					</td>
					<xsl:for-each select="td[position()!=($total - 16 + 13) and position()!=($total - 16+14) and position()!=($total - 15+14) and position()!=($total - 15+15)]">
						
							<xsl:choose>
								<xsl:when test="position()=1">
									<td>
									<xsl:if test=". != '合计'">
							        <a href="#">
												<xsl:attribute name="onclick">
													openMainVouch("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
											<xsl:if test="../td[$total - 16+13]!=''"><img src="chong.gif"/></xsl:if>
											<xsl:if test="../td[$total - 16+14]='1'"><img src="fei.gif"/></xsl:if>
											<xsl:if test="../td[$total - 16+16]='1'"><img src="error.png"/></xsl:if>
									
					         </xsl:if>
	               <xsl:if test=". = '合计'">
						        <xsl:value-of select="."/>  
					       </xsl:if>
						   </td>
								</xsl:when>
								<xsl:when test="position()=6">
									<td>
									<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=4">
									<td style="width:300px; overflow:hidden">
										<div style="width:300px; word-break:break-all;">
											<xsl:value-of select="."/> 
										</div> 
									</td>
								</xsl:when>
								<xsl:when test="position()=5">
								<td>
								  <xsl:if test="../td[$total - 16 +15]='1'">
  								  <a href="#">
    								  <xsl:attribute name="onclick">
    								    openDetailItems("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
    								  </xsl:attribute>
    									<xsl:value-of select="."/>
  									</a>
									</xsl:if>
									<xsl:if test="../td[$total - 16 + 15]='0'">
  									<xsl:value-of select="."/>
  								</xsl:if>
								</td>
								</xsl:when>
								<xsl:when test="position()=7 or position()=8">
									<td>
									<xsl:if test=". != 0">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
