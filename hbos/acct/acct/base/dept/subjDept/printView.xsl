<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>

			<thead>
				<tr>
					<td style="fontsize:maintitle;colspan:29"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
				<tr noWrap='true' class='mainHead'>
			  	<td nowrap='true'>科目编码</td>
			  	<td nowrap='true'>科目名称</td>
			  	<td nowrap='true'>方向</td>
			  	<td nowrap='true'>预算金额</td>
			  	<td nowrap='true'>追加金额</td>
			  	<td nowrap='true'>1月</td>
			  	<td nowrap='true'>追加</td>
			  	<td nowrap='true'>2月</td>
			  	<td nowrap='true'>追加</td>
			  	<td nowrap='true'>3月</td>
			  	<td nowrap='true'>追加</td>
			  	<td nowrap='true'>4月</td>
			  	<td nowrap='true'>追加</td>
			  	<td nowrap='true'>5月</td>
			  	<td nowrap='true'>追加</td>
			  	<td nowrap='true'>6月</td>
			  	<td nowrap='true'>追加</td>
			  	<td nowrap='true'>7月</td>
			  	<td nowrap='true'>追加</td>
			  	<td nowrap='true'>8月</td>
			  	<td nowrap='true'>追加</td>
			  	<td nowrap='true'>9月</td>
			  	<td nowrap='true'>追加</td>
			  	<td nowrap='true'>10月</td>
			  	<td nowrap='true'>追加</td>
			  	<td nowrap='true'>11月</td>
			  	<td nowrap='true'>追加</td>
			  	<td nowrap='true'>12月</td>
			  	<td nowrap='true'>追加</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
                 <xsl:when test="position() &lt; 6">
								 </xsl:when>
								 <xsl:otherwise>
									<td><xsl:value-of select="."/></td>
							 	</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
