<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
					<td style="fontsize:maintitle;colspan:6"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
				<tr noWrap='true' class='mainHead'>
			  	<td nowrap='true'>科目编码</td>
			  	<td nowrap='true'>科目名称</td>
			  	<td nowrap='true'>方向</td>
			  	<td nowrap='true'>预算金额</td>
			  	<td nowrap='true'>追加金额</td>
			  	<td nowrap='true'>是否分解</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td[position() &gt; 5 and position() &lt; 12]">
							<xsl:if test="position() = 4 or position() = 5">
								<td align="right">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:if>
							<xsl:if test="position() != 4 and position() !=5">
								<td>
										<xsl:value-of select="."/>
								</td>
							</xsl:if>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
