<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox' value=''/></th>
        <th>选择</th>
      	<th nowrap='true'>指标编码</th>
      	<th nowrap='true'>指标名称</th>
      	<th nowrap='true'>公式</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
				<td align='center'>
							<input type='radio' name="selRadio" onclick="selValue(this)">
								<xsl:attribute name="_code" ><xsl:value-of select="td[1]"/></xsl:attribute>
								<xsl:attribute name="_name" ><xsl:value-of select="td[2]"/></xsl:attribute>
								<xsl:attribute name="formula" ><xsl:value-of select="td[3]"/></xsl:attribute>
							</input>
				</td>
          <xsl:for-each select="td">
            <td>
              <xsl:value-of select="."/>
            </td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

