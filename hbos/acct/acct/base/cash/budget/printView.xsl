<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:250mm'/>
				<col style = 'width:250mm'/>
			</colgroup>
			<thead>
				<tr>
					<td style="fontsize:maintitle;colspan:2"></td>
					<td style='display:none'/>
				</tr>
				<tr noWrap='true' class='mainHead'>
			  	<td style="width:150">现金科目</td>
			  	<td style="width:150">预算值</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td align="right"><xsl:value-of select="."/></td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
