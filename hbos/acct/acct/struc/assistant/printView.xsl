<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN =' '/>
	<xsl:template match="/">
	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
	<root>
		<thead>
		    <xsl:for-each select="/root/tbody/tr">
		      <xsl:if test="position()=1">
		      	<tr noWrap='true' class='mainHead'>
		      		<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td[position()!=1])"/></xsl:attribute></td>
		          	<xsl:for-each select="td[position()&gt;2]">
		           	 <td style='display:none'/>
		    	    </xsl:for-each>
		       	</tr>
		       	<tr noWrap='true' class='mainHead'>
		      		<td style="fontsize:subtitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td[position()!=1])"/></xsl:attribute></td>
		          	<xsl:for-each select="td[position()&gt;2]">
		           	 <td style='display:none'/>
		    	    </xsl:for-each>
		       	</tr>
		        <tr noWrap='true' class='mainHead'>
		          <xsl:for-each select="td">
		            <xsl:if test="position()=3 or position()=2">
		    	        <td nowrap='true' rowspan='2' valign="center"><xsl:value-of select="."/></td>
		    	      </xsl:if>
		            <xsl:if test="position()>3 and string-length(.)>0">
		    	        <td nowrap='true' colspan='2' valign="center"><xsl:value-of select="."/></td>
		    	        <td style='display:none'/>
		    	      </xsl:if>
		    	    </xsl:for-each>
		       	</tr>
		      </xsl:if>
		      <xsl:if test="position()=2">
		        <tr noWrap='true' class='mainHead'>
		        <xsl:for-each select="td">
		        	<xsl:if test="position()=3 or position()=2">
		    	        <td style='display:none'/>
		    	      </xsl:if>
		            <xsl:if test="position()>3">
		    	        <td nowrap='true' valign="center"><xsl:value-of select="."/></td>
		    	      </xsl:if>
		    	    </xsl:for-each>
		      	</tr>
		   	  </xsl:if>
		    </xsl:for-each>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
      <xsl:if test="position()>=3">
                <tr>
           <xsl:for-each select="td">
             <xsl:choose>
               <xsl:when test="position()=1">
               </xsl:when>
               <xsl:when test="position()=2">
                <xsl:if test="../td[3]!='�ϼ�'">
                 <td align='left'>
                  <xsl:value-of select="."/>
                  </td>
                 </xsl:if>
                <xsl:if test="../td[3]='�ϼ�'">
                 <td align='left'>
                  </td>
                 </xsl:if>
               </xsl:when>

               <xsl:when test="position()=3">
                 <td align='left'>
                  <xsl:value-of select="."/>
                  </td>
               </xsl:when>
               
               <xsl:otherwise>
                 <xsl:if test=".!=''">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                 </xsl:if>
                 <xsl:if test=".=''">
                  <td align='right'>0.00</td>
                 </xsl:if>
               </xsl:otherwise>
             </xsl:choose>
       		</xsl:for-each>
                </tr>
      </xsl:if>
    </xsl:for-each>
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>



