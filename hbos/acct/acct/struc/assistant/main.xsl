<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    <xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)" />
    <xsl:for-each select="/root/tbody/tr">
      <xsl:if test="position()=1">
        <tr noWrap='true' class='mainHead'>
  				<th valign="center" rowspan='2'>选择</th>
          <xsl:for-each select="td">
            <xsl:if test="position()=3 or position()=2 ">
    	        <th nowrap='true' rowspan='2' valign="center">
    	          <xsl:value-of select="."/>
    	        </th>
    	      </xsl:if>
    	      
            <xsl:if test="position()=4">
    	        <th nowrap='true' colspan='2' valign="center">
        	    <a href="#">
          	    <xsl:attribute name='onclick'>
          	      javascript:pieGraph(<xsl:value-of select="position()" />)
        	      </xsl:attribute>
   	            <xsl:value-of select="."/>
      	      </a>
    	        </th>
    	      </xsl:if>
    	         	      
            <xsl:if test="position()>5 and string-length(.)>0">
            <xsl:if test=". !='增减'">
    	        <th nowrap='true' colspan='2' valign="center">
        	    <a href="#">
          	    <xsl:attribute name='onclick'>
          	      javascript:pieGraph(<xsl:value-of select="position()" />)
        	      </xsl:attribute>
   	            <xsl:value-of select="."/>
      	      </a>
    	        </th>
    	        </xsl:if>
            <xsl:if test=". ='增减'">
    	        <th nowrap='true' colspan='2' valign="center">
   	            <xsl:value-of select="."/>
    	        </th>
    	        </xsl:if>

    	      </xsl:if>
    	    </xsl:for-each>
       	</tr>
      </xsl:if>
      
      <xsl:if test="position()=2">
        <tr noWrap='true' class='mainHead'>
        <xsl:for-each select="td">
            <xsl:if test="position()>3">
    	        <th nowrap='true' valign="center"><xsl:value-of select="."/></th>
    	      </xsl:if>
    	    </xsl:for-each>
      	</tr>
   	  </xsl:if>
    </xsl:for-each>
    </thead>
    
    <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <xsl:if test="position()>=3">
         <tr>
          <xsl:if test="td[3]!='合计'">
 					<td align='center' >
 						<input type='checkbox' TABINDEX='-1'>
 							<xsl:attribute name="value" >
 								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
 							</xsl:attribute>
 								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
 						</input>
 					</td>
 					</xsl:if>
          <xsl:if test="td[3]='合计'">
 					<td align='center' >
 					</td>
 					</xsl:if>
           <xsl:for-each select="td">
             <xsl:choose>
               <xsl:when test="position()=1">
               </xsl:when>
               <xsl:when test="position()=2">
                <xsl:if test="../td[3]!='合计'">
                 <td align='left'>
                  <xsl:value-of select="."/>
                  </td>
                 </xsl:if>
                <xsl:if test="../td[3]='合计'">
                 <td align='left'>
                  </td>
                 </xsl:if>
               </xsl:when>

               <xsl:when test="position()=3">
                 <td align='left'>
                  <xsl:value-of select="."/>
                  </td>
               </xsl:when>
               
               <xsl:otherwise>
                 <xsl:if test=".!=''">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                 </xsl:if>
                 <xsl:if test=".=''">
                  <td align='right'>0.00</td>
                 </xsl:if>
               </xsl:otherwise>
             </xsl:choose>
       		</xsl:for-each>
         </tr>
      </xsl:if>
    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>



