<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    <xsl:for-each select="/root/tbody/tr">
      <xsl:if test="position()=1">
        <tr noWrap='true' class='mainHead'>
          <xsl:for-each select="td">
            <xsl:if test="position()=3 or position()=2">
    	        <th nowrap='true' rowspan='2' valign="center"><xsl:value-of select="."/></th>
    	      </xsl:if>
            <xsl:if test="position()>3 and string-length(.)>0">
    	        <th nowrap='true' colspan='2' valign="center"><xsl:value-of select="."/></th>
    	      </xsl:if>
    	    </xsl:for-each>
       	</tr>
      </xsl:if>
      <xsl:if test="position()=2">
        <tr noWrap='true' class='mainHead'>
        <xsl:for-each select="td">
            <xsl:if test="position()>3">
    	        <th nowrap='true' valign="center"><xsl:value-of select="."/></th>
    	      </xsl:if>
    	    </xsl:for-each>
      	</tr>
   	  </xsl:if>
    </xsl:for-each>
    </thead>
    <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <xsl:if test="position()>=3">
                <tr>
                  <xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position()=1">
                      </xsl:when>
                      <xsl:when test="position()=2 or position()=3">
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:when>
                      <xsl:when test="position()=4 or position()=6 or position()=8">
                        <xsl:if test=".=0">
                          <td align='right'></td>
                        </xsl:if>
                        <xsl:if test=".!=0">
                        <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                        </xsl:if>
                      </xsl:when>
                      <xsl:when test="position()=5">
                        <xsl:if test="../td[4]=0">
                          <td align='right'></td>
                        </xsl:if>
                        <xsl:if test="../td[4]!=0">
                          <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                        </xsl:if>
                      </xsl:when>
                      <xsl:when test="position()=7">
                        <xsl:if test="../td[6]=0">
                          <td align='right'></td>
                        </xsl:if>
                        <xsl:if test="../td[6]!=0">
                          <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                        </xsl:if>
                      </xsl:when>
                      <xsl:when test="position()=9 ">
                        <xsl:if test="../td[8]=0">
                          <td align='right'></td>
                        </xsl:if>
                        <xsl:if test="../td[8]!=0">
                          <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                        </xsl:if>
                      </xsl:when>
                      <xsl:otherwise>
                        <td align='center'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                    </xsl:choose>
              		</xsl:for-each>
                </tr>
      </xsl:if>
    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>



