<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
		<colgroup>
			<col style = 'width:100mm'/>
			<col style = 'width:200mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:200mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
		</colgroup>

    <thead>
    	<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">10</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr>
				<td style="fontsize:subtitle;"><xsl:attribute name="colspan">10</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
      <tr noWrap='true' class='mainHead'>
      	<td nowrap='true' rowspan='2' valign='middle'>指标类别编码</td>
      	<td nowrap='true' rowspan='2' valign='middle'>指标类别名称</td>
      	<td nowrap='true' rowspan='2' valign='middle'>五性分析指标类别</td>
      	<td nowrap='true' rowspan='2' valign='middle'>指标编码</td>
      	<td nowrap='true' rowspan='2' valign='middle'>指标名称</td>
      	<td nowrap='true' rowspan='2' valign='middle'>指标单位</td>
      	<td nowrap='true' rowspan='2' valign='middle'>分析期</td>
      	<td nowrap='true' rowspan='2' valign='middle'>对比期</td>
      	<td nowrap='true' colspan='2' valign='middle'>增减</td>
      	<td style='display:none'/>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<td style='display:none'/>
      	<td style='display:none'/>
		<td style='display:none'/>
		<td style='display:none'/>
		<td style='display:none'/>
		<td style='display:none'/>
		<td style='display:none'/>
		<td style='display:none'/>
      	<td nowrap='true'>金额</td>
      	<td nowrap='true'>百分比</td>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
	          		<xsl:when test="position() = 10"><td align="right"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td></xsl:when>
								<xsl:when test="position() = 7 or position() = 8 or position() = 9 "><td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td></xsl:when>
								<xsl:otherwise><td><xsl:value-of select="."/></td></xsl:otherwise>
						</xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>

  </xsl:template>
</xsl:stylesheet>

