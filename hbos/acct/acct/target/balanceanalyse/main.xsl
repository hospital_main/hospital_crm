<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th valign="center" rowspan="2">选择</th>
      	<th nowrap='true' rowspan='2' valign='middle'>指标类别编码</th>
      	<th nowrap='true' rowspan='2' valign='middle'>指标类别名称</th>
      	<th nowrap='true' rowspan='2' valign='middle'>五性分析指标类别</th>
      	<th nowrap='true' rowspan='2' valign='middle'>指标编码</th>
      	<th nowrap='true' rowspan='2' valign='middle'>指标名称</th>
      	<th nowrap='true' rowspan='2' valign='middle'>指标单位</th>
      	<th nowrap='true' rowspan='2' valign='middle'>分析期</th>
      	<th nowrap='true' rowspan='2' valign='middle'>对比期</th>
      	<th nowrap='true' colspan='2' valign='middle'>增减</th>

      </tr>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>金额</th>
      	<th nowrap='true'>百分比</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<td align='center' >
				<input type='checkbox' TABINDEX='-1'>
					<xsl:attribute name="value" >
						<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					</xsl:attribute>
						<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
				</input>
			</td>
	          <xsl:for-each select="td">
	          	<xsl:choose>
	          		<xsl:when test="position() = 10"><td align="right"><xsl:value-of select="format-number(.,'#,##0.00%')"/></td></xsl:when>
	          		<xsl:when test="position() = 7 or position() = 8 or position() = 9 "><td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td></xsl:when>
								<xsl:otherwise><td><xsl:value-of select="."/></td></xsl:otherwise>
						</xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

