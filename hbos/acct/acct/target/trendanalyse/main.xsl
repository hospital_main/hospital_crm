<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<th valign="center" >选择</th>
      	<th nowrap='true' valign='middle'>指标类别编码</th>
      	<th nowrap='true' valign='middle'>指标类别名称</th>
      	<th nowrap='true' valign='middle'>五性分析指标类别</th>
      	<th nowrap='true' valign='middle'>指标编码</th>
      	<th nowrap='true' valign='middle'>指标名称</th>
      	<th nowrap='true' valign='middle'>指标单位</th>
      	<xsl:for-each select="/root/tbody/tr[1]/td">
      	  <xsl:if test="position() > 6 and last() - position() >0"> 
      	  	<th nowrap='true' valign='middle'><xsl:value-of select="."/></th>
      	  </xsl:if>
   	  	</xsl:for-each>
   	  	<th nowrap='true' valign='middle'>平均值</th>
      	
 		</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:if test="position() > 1"> 
        <tr>
        	<td align='center' >
				<input type='checkbox' TABINDEX='-1'>
					<xsl:attribute name="value" >
						<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					</xsl:attribute>
						<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
				</input>
			</td>
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test="position() > 6"><td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td></xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."/></td></xsl:otherwise>
					</xsl:choose>
          </xsl:for-each>
        </tr>
        </xsl:if> 
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

