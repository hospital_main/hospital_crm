<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
   <thead>
   		<tr>
				<td style="fontsize:maintitle;">
					<xsl:attribute name="colspan">
						<xsl:if test="count(/root/tbody/tr)&gt;0">
							<xsl:value-of select="count(/root/tbody/tr[1]/td)"/>
						</xsl:if>
						<xsl:if test="count(/root/tbody/tr)=0">
							7
						</xsl:if>
					</xsl:attribute>
				</td>
					
				<td style='display:none' nowrap='true'/>
				<td style='display:none' nowrap='true'/>
				<td style='display:none' nowrap='true'/>
				<td style='display:none' nowrap='true'/>
				<td style='display:none' nowrap='true'/>
				<td style='display:none' nowrap='true'/>
				<xsl:for-each select="/root/tbody/tr[1]/td">
		      	  <xsl:if test="position() > 6 and last() - position() >0"> 
		      	  	<td style='display:none' nowrap='true'/>
		      	  </xsl:if>
		   	  	</xsl:for-each>
			</tr>
			<tr>
				<td style="fontsize:subtitle;" nowrap='true'><xsl:attribute name="colspan">
					<xsl:if test="count(/root/tbody/tr)&gt;0">
						<xsl:value-of select="count(/root/tbody/tr[1]/td)"/>
					</xsl:if>
					<xsl:if test="count(/root/tbody/tr)=0">
						7
					</xsl:if>
				</xsl:attribute></td>
				<td style='display:none' nowrap='true'/>
				<td style='display:none' nowrap='true'/>
				<td style='display:none' nowrap='true'/>
				<td style='display:none' nowrap='true'/>
				<td style='display:none' nowrap='true'/>
				<td style='display:none' nowrap='true'/>
				<xsl:for-each select="/root/tbody/tr[1]/td">
		      	  <xsl:if test="position() > 6 and last() - position() >0"> 
		      	  	<td style='display:none' nowrap='true'/>
		      	  </xsl:if>
		   	  	</xsl:for-each>
			</tr>
      <tr noWrap='true' class='mainHead'>
      	<td nowrap='true' valign='middle'>指标类别编码</td>
      	<td nowrap='true' valign='middle'>指标类别名称</td>
      	<td nowrap='true' valign='middle'>五性分析指标类别</td>
      	<td nowrap='true' valign='middle'>指标编码</td>
      	<td nowrap='true' valign='middle'>指标名称</td>
      	<td nowrap='true' valign='middle'>指标单位</td>
      	<xsl:for-each select="/root/tbody/tr[1]/td">
      	  <xsl:if test="position() > 6 and last() - position() >0"> 
      	  	<td nowrap='true' valign='middle'><xsl:value-of select="."/></td>
      	  </xsl:if>
   	  	</xsl:for-each>
   	  	<td width='100' valign='middle'>平均值</td>
 		</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:if test="position() > 1"> 
        <tr>
          <xsl:for-each select="td">
            <td nowrap='true'>
                  <xsl:value-of select="."/>
            </td>
          </xsl:for-each>
        </tr>
        </xsl:if> 
      </xsl:for-each>
    </tbody>
   </root> 
  </xsl:template>
</xsl:stylesheet>

