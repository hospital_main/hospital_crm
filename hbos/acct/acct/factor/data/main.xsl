<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th>指标编码</th>
				<th>指标名称</th>
				<th>指标单位</th>
				<th>指标数据</th>
			</tr>    
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:attribute name="d_code"><xsl:value-of select="td[1]"/></xsl:attribute>
					<xsl:attribute name="d_name"><xsl:value-of select="td[2]"/></xsl:attribute>
					<xsl:attribute name="d_formula"><xsl:value-of select="td[5]"/></xsl:attribute>
					<xsl:attribute name="d_last"><xsl:value-of select="td[6]"/></xsl:attribute>
					<xsl:if test="td[7]=1"><xsl:attribute name="style">display:none</xsl:attribute></xsl:if>
					<xsl:for-each select="td[position()&lt;5]">
						<td >
							<xsl:choose>
								<xsl:when test="position()=4">
									<input type="text" maxlength="15" style="text-align:right" onblur="updateParent(this)">
										<xsl:attribute name="pk" >
											<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
										</xsl:attribute>
										<xsl:if test="../td[6]='0'"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
										<xsl:attribute name="oldValue"><xsl:value-of select="."/></xsl:attribute>
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
									</input>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="width">30%</xsl:attribute>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



