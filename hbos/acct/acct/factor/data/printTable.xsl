<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
		<colgroup>
			<col style = 'width:100mm'/>
			<col style = 'width:200mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:200mm'/>
			<col style = 'width:100mm'/>
			<xsl:for-each select="/root/tbody/tr[1]/td">
      	  	<xsl:if test="position() > 5 and last() - position() >0"> 
      	  		<col style = 'width:100mm'/>
      	  	</xsl:if>
   	  		</xsl:for-each>

			<col style = 'width:100mm'/>
		</colgroup>
   <thead>
   		<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">4</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr>
				<td style="fontsize:subtitle;"><xsl:attribute name="colspan">4</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr>
				<td>指标编码</td>
				<td>指标名称</td>
				<td>指标单位</td>
				<td>指标数据</td>
			</tr>
    </thead>
    <tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position()&lt;5]">
						<td >
							<xsl:choose>
								<xsl:when test="position()=4">
									<xsl:attribute name="align">right</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="width">30%</xsl:attribute>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
   </root> 
  </xsl:template>
</xsl:stylesheet>

