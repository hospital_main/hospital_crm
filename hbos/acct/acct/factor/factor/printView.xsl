<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>

	<thead>
   		<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">6</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr>
				<td style="fontsize:subtitle;"><xsl:attribute name="colspan">6</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr><tr>
					<td colspan="6"  style="colspan:6;fontsize:coltitle">分析数据</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr style="colspan:10;fontsize:subtitle">
					<td >指标编码</td>
					<td >指标名称</td>
					<td >单位</td>
					<td >分析期间</td>
					<td >对比期间</td>
					<td >增减额度</td>
				</tr>
    </thead>
    <tbody> 
			<xsl:if test="count(/root/tbody/tr[td[9]=1])&gt;0">
				
				<xsl:for-each select="/root/tbody/tr[td[9]=1]">
					<tr>
						<xsl:for-each select="td[position()!=7 and position()!=8 and position()!=9]">
							<td>
								<xsl:choose>
									<xsl:when test="position()=4 or position()=5 or position()=6 ">
										<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="style">text-align:left;</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="count(/root/tbody/tr[td[9]=2])&gt;0">
				<tr>
					<td colspan="6" ><br/>计算步骤(连环替代法)</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr  >
					<td >指标编码</td>
					<td >指标名称</td>
					<td >单位</td>
					<td >计算过程</td>
					<td >计算结果</td>
					<td >计算步骤</td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[9]=2]">
					<tr>
						<xsl:for-each select="td[position()!=7 and position()!=8 and position()!=9]">
							<td>
								<xsl:choose>
									<xsl:when test=" position()=5 ">
									
										<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:when>
									<xsl:when test="position()=6 ">
										<xsl:attribute name="style">text-align:center;</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="style">text-align:left;</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="count(/root/tbody/tr[td[9]=3])&gt;0">
				<tr>
					<td colspan="6"><br/>分析报告</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>	
				</tr>
				<tr>
					<td colspan="6"  >分析报告</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[9]=3]">
						<tr>
								<td colspan="6"><xsl:value-of select="td[1]"/></td>					
								<td style="display:none"/>
								<td style="display:none"/>
								<td style="display:none"/>
								<td style="display:none"/>
								<td style="display:none"/>	
						</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="count(/root/tbody/tr[td[9]=4])&gt;0">
				<tr>
					<td colspan="6"><br/>计算步骤(差额计算法)</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr>
					<td >指标编码</td>
					<td >指标名称</td>
					<td >单位</td>
					<td >增减额度</td>
					<td >计算过程</td>
					<td >影响值</td>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[9]=4]">
					<tr>
						<xsl:for-each select="td[position()!=7 and position()!=8 and position()!=9]">
							<td>
								<xsl:choose>
									<xsl:when test="position()=4 ">
										<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:when>
									<xsl:when test="position()=5 ">
										<xsl:attribute name="style">text-align:left;</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test=" position()=6 and ../td[8]!='1' ">
										<xsl:attribute name="style">text-align:right;</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:when>
									<xsl:when test=" position()=6 and ../td[8]='1' ">
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="style">text-align:left;</xsl:attribute>
										<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="count(/root/tbody/tr[td[9]=5])&gt;0">
				<tr>
					<td colspan="6"><br/>分析报告</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>	
				</tr>
				<tr>
					<td colspan="6"   >分析报告</td>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<xsl:for-each select="/root/tbody/tr[td[9]=5]">
						<tr>
								<td colspan="6"><xsl:value-of select="td[1]"/></td>					
								<td style="display:none"/>
								<td style="display:none"/>
								<td style="display:none"/>
								<td style="display:none"/>
								<td style="display:none"/>	
						</tr>
				</xsl:for-each>
			</xsl:if>
		</tbody>
    </root>

  </xsl:template>
</xsl:stylesheet>

