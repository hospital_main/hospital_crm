<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th noWrap="true" rowspan="2">因素编码</th>
				<th noWrap="true" rowspan="2">因素名称</th>
				<th noWrap="true" rowspan="2">单位</th>
				<th noWrap="true" colspan="2">分析期间</th>
				<th style="display:none"></th>
				<th noWrap="true" colspan="2">对比期间</th>
				<th style="display:none"></th>
				<th noWrap="true" colspan="2">增减幅度</th>
				<th style="display:none"></th>
				<th noWrap="true" rowspan="2">影响程度(%)</th>
			</tr>    
			<tr noWrap='true' class='mainHead'>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th noWrap="true">数值</th>
				<th noWrap="true">百分比(%)</th>
				<th noWrap="true">数值</th>
				<th noWrap="true">百分比(%)</th>
				<th noWrap="true">数值</th>
				<th noWrap="true">百分比(%)</th>
				<th style="display:none"></th>  
			</tr>
		</thead>  	      
 
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<td >
							<xsl:choose>
								<xsl:when test="position()&gt;3">
									<xsl:attribute name="align">right</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



