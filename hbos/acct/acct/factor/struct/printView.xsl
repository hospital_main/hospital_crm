<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/acct/factor/struct/printView.xsl,v 1.1 2012/03/12 01:44:15 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:15 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
		<thead>
			<tr>
				<td style="colspan:10;fontsize:maintitle">凭证汇总</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td style="colspan:10;fontsize:subtitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td noWrap="true" rowspan="2">因素编码</td>
				<td noWrap="true" rowspan="2">因素名称</td>
				<td noWrap="true" rowspan="2">单位</td>
				<td noWrap="true" colspan="2">分析期间</td>
				<td style="display:none"></td>
				<td noWrap="true" colspan="2">对比期间</td>
				<td style="display:none"></td>
				<td noWrap="true" colspan="2">增减幅度</td>
				<td style="display:none"></td>
				<td noWrap="true" rowspan="2">影响程度(%)</td>
			</tr>    
			<tr noWrap='true' class='mainHead'>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td noWrap="true">数值</td>
				<td noWrap="true">百分比(%)</td>
				<td noWrap="true">数值</td>
				<td noWrap="true">百分比(%)</td>
				<td noWrap="true">数值</td>
				<td noWrap="true">百分比(%)</td>
				<td style="display:none"></td>  
			</tr>
		</thead>  	  
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<td >
							<xsl:choose>
								<xsl:when test="position()&gt;3">
									<xsl:attribute name="align">right</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
