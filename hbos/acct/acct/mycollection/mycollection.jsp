<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.io.IOException" %>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="javax.servlet.jsp.JspException"%>

<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gb2312"%>
<%@ taglib prefix="my" uri="/WEB-INF/taglib/viewhigh-html.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk">
<title>我的收藏</title>
<link href="" rel="stylesheet" type="text/css">
<script language='javascript' id='scriptID'></script>
<script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>
<script language="JavaScript" src="../../../../base/scripts/sub.js" ></script>
	<script src='/base/scripts/timerYears.js'></script>
<script type="text/javascript">
	//alert(window.parent.document.location.href);
	var currentDelDiv; //当前的delDiv
	var curTd; //包含当前delDiv的td
	var X; //创建delDiv时的鼠标位置X
	var Y; //创建delDiv时的鼠标位置Y
	var delDivWidth = 100; //delDiv的宽度
	var delDivHeight = 20; //delDiv的高度
	var xError = 10; //删除delDiv时的x误差
	var yError = 10; //删除delDiv时的y误差
	var curImg; //右键单击的img
	var curName; //当前的功能名称
	/**
		创建删除Div,用做删除某个收藏夹的内容
	*/
	function createDel(img,url,name){

		var currentX = window.event.clientX; //鼠标绝对位置X
		var currentY = window.event.clientY; //鼠标绝对位置Y
		if(!currentDelDiv){
			var delDiv = document.createElement("div"); //创建删除div
			delDiv.style.position = 'absolute';
			delDiv.style.top = currentY + 'px';
			delDiv.style.left = currentX + 'px';
			delDiv.style.background = 'white';
			delDiv.style.width = delDivWidth + "px";
			delDiv.style.height = delDivHeight + "px";
			delDiv.style.lineHeight = delDivHeight + 'px';
			delDiv.style.cursor = 'hand';
			delDiv.style.textAlign = 'center';
			delDiv.style.zIndex = '10';
			var mcButton = document.createElement("button");
			mcButton.style.position = "relative";
			mcButton.style.top = "0px";
			mcButton.style.left = "0px";
			mcButton.setAttribute("id","mcButton");
			mcButton.className = 'button';
			mcButton.innerText = '删除' + name + '';
			mcButton.onclick = function(){if(confirm('确定要从收藏夹中删除\"' + name + '\"么?')){window.document.location.href = url;}};
			currentDelDiv = delDiv;
			curTd = document.body;
			X = currentX;
			Y = currentY;
			curImg = img;
			curName = name;
			delDiv.appendChild(mcButton);
			document.body.appendChild(delDiv);
		}
	}
	/**
		删除delDiv
	*/
	function delCurDiv(){
		if(curTd && currentDelDiv){
			curTd.removeChild(currentDelDiv);
			curTd = null;
			currentDelDiv = null;
		}
		curImg.alt = '点击进入  ' + curName;
	}
	/**
		确定何时删除delDiv
	*/
	function confirmDel(){
		var currentX = window.event.clientX; //当前鼠标绝对位置X
		var currentY = window.event.clientY; //当前鼠标绝对位置Y

		if(currentX < X - xError || currentX > X+delDivWidth+xError || currentY < Y-yError || currentY > Y+delDivHeight+yError){
			delCurDiv();
		}
	}
</script>
<style type="text/css">
.button{
	border-right = "#7b9ebd 1px solid";
	padding-right = "2px";
	border-top = "#7b9ebd 1px solid";
	padding-left = "2px";
	font-size = "12px"
	filter = "progid:DXImageTransform.Microsoft. " +
		"Gradient(GradientType=0, StartColorStr=#ffffff, EndColorStr=#cecfde)";

	border-left = "#7b9ebd 1px solid";
	cursor = "hand";
	color = "black";
	padding-top = "2px";
	border-bottom = "#7b9ebd 1px solid";
}
</style>
<style type="text/css">
body {
	font: normal 11px auto "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
	color: #4f6b72;
	background: white;
}

a {
	color: #c75f3e;
	text-decoration: none;
}

#mytable {
	width: 800px;
	padding: 0;
	margin: 0;
	background: white;
	text-align: center;
}

td {
	font-size:11px;
	padding: 6px 6px 6px 12px;
	color: #4f6b72;
	text-align: center;
}
.customtd{
	font-size:18px;
	font-weight: bolder;
	padding: 6px 6px 6px 12px;
	color: #4f6b72;
	text-align: center;
}

td.alt {
	background: #F5FAFA;
	color: #797268;
}

span{
	text-align: right;
	color: red;
	font-weight: normal;
	font-size: 20px;
	cursor: hand;
}
</style>
<script language="JavaScript">
if (window.Event)
	document.captureEvents(Event.MOUSEUP);
function nocontextmenu() {
	event.cancelBubble = true;
	event.returnValue = false;
	return false;
}
function norightclick(e) {
	if (window.Event) {
		if (e.which == 2 || e.which == 3)
			return false;
	}else if (event.button == 2 || event.button == 3){
		event.cancelBubble = true;
		event.returnValue = false;
		return false;
	}
}
document.oncontextmenu = nocontextmenu;
document.onmousedown = norightclick;
</script>

</head>
<body onmousemove="confirmDel();">

	<div class='menuIdList' style="display: none"></div>

 <%
	    String strhtm="";
		request.setCharacterEncoding("gb2312");
		if(request.getAttribute("list") == null){
	%>
		<script type="text/javascript" language="javascript">
			window.document.location.href = "<%= request.getContextPath()%>/MyCollection?action=list&comp_code="
				+ getCurCompCode() + "&cp=" + getCurCopyCode() + "&userid=" + getUserID() + "&modulename=" + window.parent._getCurModule();
		</script>
	<%
		}else{

			// 后台java代码过来 begin

			StringBuffer sb = new StringBuffer();
			List<?> result = null;
			result = (List<?>) request.getAttribute("list");

			sb.append("<table id='mytable' cellspacing='0' align='center' border = '0'>");
			if (result.size() == 0) {
				sb.append("<tr><td class='customtd'>");
				sb.append("目前没有任何功能在收藏夹中,右键点击左边树功能可添加.");
				sb.append("</td></tr>");
			}
			Iterator<?> iterator = result.iterator();
			Map<?, ?> someInfo = null;
			int loop = 0;
			while (iterator.hasNext()) {
				loop++;
				someInfo = (Map<?, ?>) iterator.next();
				int id = ((Integer) someInfo.get("id")).intValue();
				String fullname=(String) someInfo.get("fullname");
				String name = (String) someInfo.get("name");
				String imagename = (String) someInfo.get("imagename");
				String spanid = (String) someInfo.get("spanid");
				String tempStr = request.getContextPath().equalsIgnoreCase("/") ? ""
						: request.getContextPath();
				String delUrl = tempStr + "/MyCollection?action=delone&id=" + id;
				if (loop % 3 != 1) {
					sb.append("<td name='"+fullname+"' class='row' ");
				} else {
					sb.append("<tr><td name='"+fullname+"' class='row' ");
				}
				sb.append(">");

				sb.append("<img src='" + tempStr
						+ "/hbos/acct/acct/mycollection/images/");
				sb.append(imagename);
				sb.append("' width='70' height='70' style='cursor: hand'  alt=\"点击进入&nbsp;&nbsp;" + name + "\" ");
				sb.append("oncontextmenu=\"createDel(this,'");
				sb.append(delUrl);
				sb.append("','");
				sb.append(name);
				sb.append("');this.alt=''\" /><br>");
				sb.append("<a href='#'  onmouseover=\"this.style.textDecoration='underline';this.style.color='red'\" "
						+ "onmouseout=\"this.style.textDecoration='none';this.style.color='#c75f3e'\" >"
						+ name + "</a>");

				sb.append("<span onclick=\"if(confirm('确定要从收藏夹中删除" + name + "么?')){window.location.href='"
						+ delUrl
						+ "';}\" onmouseover='this.style.fontWeight=900' onmouseout='this.style.fontWeight=\"normal\"' >×</span>");
				if (loop % 3 == 0) {
					sb.append("</td></tr>");
				} else {
					sb.append("</td>");
				}
			}
			sb.append("</table>");
			strhtm=sb.toString();

			//try {
				//out.print(sb.toString());
				//out.flush();
			//} catch (IOException e) {
				//e.printStackTrace();
		//	}
			// 后台java代码过来 end
		}
	%>

	<div id='collection' ><%=strhtm%></div>
<!-- 重新获取新的菜单顺序id-->

<script type="text/javascript" language="javascript">

	var $myHeaderDiv = $(".menuIdList");
	var menuArray={};
	$(function () {
		var leftSpan=window.parent.document.getElementById('leftIFrame').contentWindow.document.getElementById('mainMainMenuTable') .getElementsByTagName("span");
		var leftSpanS ="<root>"
		for(var i=0;i<leftSpan.length;i++){
			var currentName = leftSpan[i].innerText;
			var currentSpanId = leftSpan[i];
			var fullName;
			var tr=leftSpan[i];
			while(tr.tagName.toLowerCase()!="tr")
				tr=tr.parentNode;
			if(tr.getAttribute("treeItemPara")!=null&&
					tr.getAttribute("treeItemPara")!=""&&
					tr.getAttribute("treeItemOneTabLabel")!="我的收藏"){
				fullName = tr.getAttribute("treeItemPk");
				fullName = fullName.replace("<fullName>","");
				fullName = fullName.replace("</fullName>",""); //节点功能的全称,包括父节点
			}
			leftSpanS +="<para id= \""+leftSpan[i].id+ "\"   name =\""+leftSpan[i].innerHTML+ "\"  fullname=\""+fullName+"\" />"

		}

		leftSpanS +="</root>";
		$myHeaderDiv.html(leftSpanS);
		//alert("leftSpanS: "+leftSpanS);
		var vXml= new ActiveXObject("Microsoft.XMLDOM");
		vXml.async = false;
		vXml.loadXML(leftSpanS);
		var list1 = vXml.getElementsByTagName("para");
		for(var i=0;i<list1.length;i++) {
			   var totalName=list1[i].getAttribute('fullname')+"-"+list1[i].getAttribute('name');
				menuArray[totalName] = list1[i].getAttribute('id');
		}

		var list2=document.getElementsByTagName("td");
		for(var i=0;i<list2.length;i++) {
			(function(i){
				var key=list2[i].getAttribute("name")+"-"+list2[i].getElementsByTagName("a")[0].innerText;
				list2[i].getElementsByTagName("img")[0].onclick=function(){
					window.parent.document.getElementById('leftIFrame').contentWindow.document.getElementById(menuArray[key]).click() ;
				}

				list2[i].getElementsByTagName("a")[0].onclick=function(i){
					window.parent.document.getElementById('leftIFrame').contentWindow.document.getElementById(menuArray[key]).click() ;
				}
			})(i)



		}


	})
</script>



</body>
</html>
