<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>我的收藏</title>
<style type="text/css">
.btn {
	BORDER-RIGHT: #7b9ebd 1px solid;  
	PADDING-RIGHT: 2px; 
	BORDER-TOP: #7b9ebd 1px solid; 
	PADDING-LEFT: 2px; 
	FONT-SIZE: 12px; 
	FILTER: progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr=#ffffff, EndColorStr=#cecfde); 
	BORDER-LEFT: #7b9ebd 1px solid; 
	CURSOR: hand; 
	COLOR: black; 
	PADDING-TOP: 2px; 
	BORDER-BOTTOM: #7b9ebd 1px solid;
}
</style>
<script type="text/javascript">
	function back(){
		var mc = window.parent.document.getElementById("mc");
		mc.src = "./mycollection.jsp";
	}
</script>
</head>
<body>
	<div style="text-align: center;">
		<button id="back" name="back" onclick="back();" type="button" class="btn">返回到我的收藏</button>
	</div>
</body>
</html>