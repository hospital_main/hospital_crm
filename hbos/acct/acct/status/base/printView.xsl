<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN =' '/>
	<xsl:template match="/">
	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
	<root>
		<thead>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:if test="position()=1">
					<tr>
						<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)-2"/></xsl:attribute></td>
						<xsl:for-each select="td">
							<xsl:if test="position()!=2 and position()!=3 and position()!=last()">
								<td nowrap="true" style='display:none'/>
							</xsl:if>
						</xsl:for-each>
					</tr>
					<tr>
						<td style="fontsize:subtitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)-2"/></xsl:attribute></td>
						<xsl:for-each select="td">
							<xsl:if test="position()!=2 and position()!=3 and position()!=last()">
								<td nowrap="true" style='display:none'/>
							</xsl:if>
						</xsl:for-each>
					</tr>
					<tr noWrap='true' class='mainHead'>
						<xsl:for-each select="td">
                <xsl:if test="position() = 1">
              		<td rowspan="2" nowrap="true" valign="center"><xsl:value-of select="."/></td>
              	</xsl:if>
                <xsl:if test="position()=2 or position()=3">
            	  </xsl:if>
            	  <xsl:if test="position() = 4">
                	<td nowrap="true" colspan="2"><xsl:value-of select="."/></td>
                </xsl:if>
                <xsl:if test="position() > 4 and (position() mod 2 = 0)">
                	<td nowrap="true" style='display:none'/>
                </xsl:if>
                <xsl:if test="position() > 4 and (position() mod 2 = 0)">
                	<td nowrap="true" colspan="2"><xsl:value-of select="."/></td>
                </xsl:if>
						</xsl:for-each>
					</tr>
				</xsl:if>
				<xsl:if test="position()=2">
            <tr noWrap='true' class='mainHead'>
              <xsl:for-each select="td">
            	 	<xsl:if test="position()!=2 and position()!=3">
            			<td noWrap="true" ><xsl:value-of select="."/></td>
            		</xsl:if>
            	</xsl:for-each>
            </tr>
      </xsl:if>
			</xsl:for-each>
			<!-- pengjin update bug HBOSTHIRDNEW-1857  -->			
			<!--xsl:for-each select="/root/tbody/tr">
				<xsl:if test="position()=1">
					<tr>
						<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute></td>
						<xsl:for-each select="td">
							<xsl:if test="position()!=2 and position()!=3 and position()!=last()">
								<td style='display:none'/>
							</xsl:if>
						</xsl:for-each>
					</tr>
					<tr>
						<td style="fontsize:subtitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute></td>
						<xsl:for-each select="td">
							<xsl:if test="position()!=2 and position()!=3 and position()!=last()">
								<td style='display:none'/>
							</xsl:if>
						</xsl:for-each>
					</tr>
					<tr noWrap='true' class='mainHead'>
						<td rowspan="2">��Ŀ����</td>
						<xsl:for-each select="td[position() &gt; 3]">
							<xsl:if test="position()!=2 and position()!=3">
								<td nowrap='true'><xsl:value-of select="."/></td>
							</xsl:if>
						</xsl:for-each>
					</tr>
					<tr noWrap='true' class='mainHead'>
						<td style='display:none'/>
						<xsl:for-each select="/root/tbody/tr[2]/td[position() &gt; 3]">
							<xsl:if test="position()!=2 and position()!=3">
								<td nowrap='true'><xsl:value-of select="."/></td>
							</xsl:if>
						</xsl:for-each>
					</tr>
				</xsl:if>
			</xsl:for-each-->
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:if test="position()>2">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
									<td align='left'><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:when test="position()=2 or position()=3">
								</xsl:when>
								<xsl:otherwise>
									<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>



