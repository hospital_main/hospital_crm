<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    <xsl:for-each select="/root/tbody/tr">
      <xsl:if test="position()=1">
            <tr noWrap='true' class='mainHead'>
              <xsl:for-each select="td">
                <xsl:if test="position()!=2 and position()!=3">
            	    <th nowrap='true'><xsl:value-of select="."/></th>
            	  </xsl:if>
            	</xsl:for-each>
            </tr>
      </xsl:if>
    </xsl:for-each>
    </thead>
          <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <xsl:if test="position()>1">
                <tr>
                  <xsl:for-each select="td">
                    <td><xsl:value-of select="."/>11</td>
                	</xsl:for-each>
                </tr>
      </xsl:if>
    </xsl:for-each>
          </tbody>
  </xsl:template>
</xsl:stylesheet>



