<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
				<th valign="center" rowspan='2'>选择</th>
    	  <th nowrap='true' rowspan='2' valign="center">项目名称</th>
    	  <th nowrap='true' colspan='2' valign="center">
    	    <a href="#">
    	    <xsl:attribute name='onclick'>
    	      javascript:pieGraph(2)
    	      </xsl:attribute>
    	      对比期
    	      </a>
    	   </th>
    	  <th nowrap='true' colspan='2' valign="center">
    	    <a href="#">
    	    <xsl:attribute name='onclick'>
    	      javascript:pieGraph(4)
    	      </xsl:attribute>
    	      分析期
    	      </a>
    	  </th>
    	  <th nowrap='true' colspan='2'>增减</th>
    	</tr>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>金额</th>
      	<th nowrap='true'>结构(%)</th>
      	<th nowrap='true'>金额</th>
      	<th nowrap='true'>结构(%)</th>
      	<th nowrap='true'>金额</th>
      	<th nowrap='true'>结构(%)</th>
      </tr>
    </thead>
    <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
					<td align='center' >
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
						</input>
					</td>
        <xsl:for-each select="td">
          <xsl:choose>
            <xsl:when test="position()=1">
              <td align='left'><xsl:value-of select="."/></td>
            </xsl:when>
            <xsl:when test="position()=2 or position()=3">
            </xsl:when>
            <xsl:when test="position()=4">
              <td align='right'><xsl:value-of select="format-number(../td[6],'#,##0.00')"/></td>
            </xsl:when>
            <xsl:when test="position()=6">
              <td align='right'><xsl:value-of select="format-number(../td[4],'#,##0.00')"/></td>
            </xsl:when>
            <xsl:when test="position()=8">
              <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:when>
            <xsl:when test="position()=5">
              <td align='right'>
                <xsl:if test=".!=0">
                  <xsl:value-of select="format-number(../td[7],'###0.00')"/>
                </xsl:if>
              </td>
            </xsl:when>
            <xsl:when test="position()=7">
              <td align='right'>
                <xsl:if test=".!=0">
                  <xsl:value-of select="format-number(../td[5],'###0.00')"/>
                </xsl:if>
              </td>
            </xsl:when>
            <xsl:when test="position()=9">
              <td align='right'>
                <xsl:if test=".!=0">
                  <xsl:value-of select="format-number(.,'###0.00')"/>
                </xsl:if>
              </td>
            </xsl:when>
            <xsl:otherwise>
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </tr>
    </xsl:for-each>
          </tbody>
  </xsl:template>
</xsl:stylesheet>



