<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN =' '/>
	<xsl:template match="/">
	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
	<root>
		<thead>
			<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">7</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr>
				<td style="fontsize:subtitle;"><xsl:attribute name="colspan">7</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr noWrap='true' class='mainHead'>
	    	  <td nowrap='true' rowspan='2' valign="center">项目名称</td>
	    	  <td nowrap='true' colspan='2' valign="center">对比期</td>
	    	  <td style='display:none'/>
	    	  <td nowrap='true' colspan='2' valign="center">分析期</td>
	    	  <td style='display:none'/>
	    	  <td nowrap='true' colspan='2'>增减</td>
	    	  <td style='display:none'/>
	    	</tr>
	      <tr noWrap='true' class='mainHead'>
	      <td style='display:none'/>
	      	<td nowrap='true'>金额</td>
	      	<td nowrap='true'>结构(%)</td>
	      	<td nowrap='true'>金额</td>
	      	<td nowrap='true'>结构(%)</td>
	      	<td nowrap='true'>金额</td>
	      	<td nowrap='true'>结构(%)</td>
	      </tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
            <xsl:when test="position()=1">
              <td align='left'><xsl:value-of select="."/></td>
            </xsl:when>
            <xsl:when test="position()=2 or position()=3">
            </xsl:when>
            <xsl:when test="position()=4">
              <td align='right'><xsl:value-of select="format-number(../td[6],'#,##0.00')"/></td>
            </xsl:when>
            <xsl:when test="position()=6">
              <td align='right'><xsl:value-of select="format-number(../td[4],'#,##0.00')"/></td>
            </xsl:when>
            <xsl:when test="position()=8">
              <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:when>
            <xsl:when test="position()=5">
              <td align='right'>
                <xsl:if test=".!=0">
                  <xsl:value-of select="format-number(../td[7],'###0.00')"/>
                </xsl:if>
              </td>
            </xsl:when>
            <xsl:when test="position()=7">
              <td align='right'>
                <xsl:if test=".!=0">
                  <xsl:value-of select="format-number(../td[5],'###0.00')"/>
                </xsl:if>
              </td>
            </xsl:when>
            <xsl:when test="position()=9">
              <td align='right'>
                <xsl:if test=".!=0">
                  <xsl:value-of select="format-number(.,'###0.00')"/>
                </xsl:if>
              </td>
            </xsl:when>
            <xsl:otherwise>
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:otherwise>
          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>



