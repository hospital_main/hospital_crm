<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
    	  <th nowrap='true' rowspan='2' valign="center">项目名称</th>
    	  <th nowrap='true' colspan='2' valign="center">分析期</th>
    	  <th nowrap='true' colspan='2' valign="center">对比期</th>
    	  <th nowrap='true' colspan='2'>增减</th>
    	</tr>
      <tr noWrap='true' class='mainHead'>
      	<th nowrap='true'>金额</th>
      	<th nowrap='true'>结构(%)</th>
      	<th nowrap='true'>金额</th>
      	<th nowrap='true'>结构(%)</th>
      	<th nowrap='true'>金额</th>
      	<th nowrap='true'>结构(%)</th>
      </tr>
    </thead>
    <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        <xsl:for-each select="td">
          <xsl:choose>
            <xsl:when test="position()=1">
              <td align='left'><xsl:value-of select="."/></td>
            </xsl:when>
            <xsl:when test="position()=2 or position()=3">
            </xsl:when>
            <xsl:when test="position()=4 or position()=6 or position()=8">
              <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:when>
            <xsl:when test="position()=5 or position()=7 or position()=9">
              <td align='right'>
                <xsl:if test=".!=0">
                  <xsl:value-of select="format-number(.,'###0.00')"/>
                </xsl:if>
              </td>
            </xsl:when>
            <xsl:otherwise>
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </tr>
    </xsl:for-each>
          </tbody>
  </xsl:template>
</xsl:stylesheet>



