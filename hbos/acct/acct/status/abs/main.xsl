<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    <xsl:for-each select="/root/tbody/tr">
      <xsl:if test="position()=1">
            <tr noWrap='true' class='mainHead'>
      				<th valign="center" >ѡ��</th>
              <xsl:for-each select="td">
                <xsl:if test="position()!=2 and position()!=3">
            	    <th nowrap='true'><xsl:value-of select="."/></th>
            	  </xsl:if>
            	</xsl:for-each>
            </tr>
      </xsl:if>
    </xsl:for-each>
    </thead>
    <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <xsl:if test="position()>1">
                <tr>
									<td align='center' >
										<input type='checkbox' TABINDEX='-1'>
											<xsl:attribute name="value" >
												<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
											</xsl:attribute>
												<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
										</input>
									</td>
                  <xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position()=1">
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:when>
                      <xsl:when test="position()!=2 and position()!=3">
                      	<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                      </xsl:when>
                    </xsl:choose>
                	</xsl:for-each>
                </tr>
      </xsl:if>
    </xsl:for-each>
          </tbody>
  </xsl:template>
</xsl:stylesheet>



