<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN =' '/>
	<xsl:template match="/">
	<root>
		<thead>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:if test="position()=1">
					<tr>
						<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)-2"/></xsl:attribute></td>
						<xsl:for-each select="td">
							<xsl:if test="position()!=2 and position()!=3 and position()!=last()">
								<td style='width:120;display:none'/>
							</xsl:if>
						</xsl:for-each>
					</tr>
					<tr>
						<td style="fontsize:subtitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)-2"/></xsl:attribute></td>
						<xsl:for-each select="td">
							<xsl:if test="position()!=2 and position()!=3 and position()!=last()">
								<td style='width:120;display:none'/>
							</xsl:if>
						</xsl:for-each>
					</tr>
					<tr noWrap='true' class='mainHead'>
						<xsl:for-each select="td">
							<xsl:if test="position()!=2 and position()!=3">
								<td style='width:120'><xsl:value-of select="."/></td>
							</xsl:if>
						</xsl:for-each>
					</tr>
				</xsl:if>
			</xsl:for-each>		
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:if test="position()>1">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
									<td style='width:120' align='left'><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:when test="position()!=2 and position()!=3">
									<td style='width:120' align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>



