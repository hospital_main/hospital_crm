<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/acct/budg/dept/deptbalance/printView.xsl,v 1.1 2012/03/12 01:44:15 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:15 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
			</colgroup>
			<thead>
				<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">11</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr>
				<td style="fontsize:subtitle;"><xsl:attribute name="colspan">11</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
      <tr noWrap='true' class='mainHead'>
      	<td nowrap='true' rowspan='2' valign='middle'>部门</td>
      	<td nowrap='true' rowspan='2' valign='middle'>科目</td>
      	<td nowrap='true' rowspan='2' valign='middle'>本期预算</td>
      	<td nowrap='true' rowspan='2' valign='middle'>本期追加</td>
      	<td nowrap='true' rowspan='2' valign='middle'>本期总预算</td>
      	<td nowrap='true' rowspan='2' valign='middle'>本期完成</td>
      	<td nowrap='true' colspan='2' valign='middle'>完成预算</td>
      	<td style='display:none'/>
      	<td nowrap='true' rowspan='2' valign='middle'>对比期完成</td>
      	<td nowrap='true' colspan='2' valign='middle'>增减</td>
      	<td style='display:none'/>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td style='display:none'/>
      	<td nowrap='true'>差额</td>
      	<td nowrap='true'>百分比(%)</td>
      	<td style='display:none'/>
      	<td nowrap='true'>金额</td>
      	<td nowrap='true'>百分比(%)</td>
      </tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position()=8 or position()=11">
                        <td align='right' class="numberText"><xsl:value-of select="."/></td>
                      </xsl:when>
                      <xsl:when test="position()>2 ">
                        <td align='right' class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                      </xsl:when>
                      <xsl:otherwise>
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                    </xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
