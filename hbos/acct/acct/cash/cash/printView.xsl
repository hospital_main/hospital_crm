<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/acct/cash/cash/printView.xsl,v 1.1 2012/03/12 01:44:15 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:15 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
			</colgroup>
			<thead>
			<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">6</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr>
				<td style="fontsize:subtitle;"><xsl:attribute name="colspan">6</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
      <tr noWrap='true' class='mainHead'>
      	<td nowrap='true'  valign='middle'>科目性质</td>
      	<td nowrap='true'  valign='middle'>科目编码</td>
      	<td nowrap='true'  valign='middle'>科目名称</td>
      	<td nowrap='true'  valign='middle'>本年预算</td>
      	<td nowrap='true'  valign='middle'>本期发生</td>
      	<td style="width:120"  valign='middle'>本年累计发生</td>
      </tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position()=1 ">
                      </xsl:when>
                      <xsl:when test="position()>4 ">
                        <td align='right' class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                      </xsl:when>
                      <xsl:otherwise>
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                    </xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
