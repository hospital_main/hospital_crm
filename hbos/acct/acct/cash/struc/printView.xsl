<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/acct/cash/struc/printView.xsl,v 1.2 2014/04/30 05:30:39 yaoqingsong Exp $
 $Author: yaoqingsong $
 $Date: 2014/04/30 05:30:39 $
 $Revision: 1.2 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
			</colgroup>
			<thead>
				<tr>
				<td style="fontsize:maintitle;"><xsl:attribute name="colspan">8</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr>
				<td style="fontsize:subtitle;"><xsl:attribute name="colspan">8</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
      <tr noWrap='true' class='mainHead'>
      	<td nowrap='true'  valign='middle'>科目性质</td>
      	<td nowrap='true'  valign='middle'>科目编码</td>
      	<td nowrap='true'  valign='middle'>科目名称</td>
      	<td nowrap='true'  valign='middle'>对比期</td>
      	<td nowrap='true'  valign='middle'>结构(%)</td>
      	<td nowrap='true'  valign='middle'>分析期</td>
      	<td nowrap='true'  valign='middle'>结构(%)</td>
      	<td nowrap='true'  valign='middle'>结构增减(%)</td>
      </tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
                    <xsl:choose>
                      <xsl:when test="position()=1 ">
                      </xsl:when>
                      <xsl:when test="position()=5 ">
                        <td align='right'><xsl:value-of select="format-number(../td[7],'#,##0.00')"/></td>
                      </xsl:when>
                      <xsl:when test="position()=6 ">
                        <td align='right'>
                          <xsl:if test="../td[8]!=0">
                          <xsl:value-of select="format-number(../td[8],'#,##0.00')"/>
                          </xsl:if>
                         </td>
                      </xsl:when>
                      <xsl:when test="position()=7 ">
                        <td align='right'><xsl:value-of select="format-number(../td[5],'#,##0.00')"/></td>
                      </xsl:when>
                      <xsl:when test="position()=8 ">
                        <td align='right'>
                          <xsl:if test="../td[6]!=0">
                          <xsl:value-of select="format-number(../td[6],'#,##0.00')"/>
                          </xsl:if>
                        </td>
                      </xsl:when>
                      <xsl:when test="position()=9">
                        <td align='right'>
                          <xsl:if test=".!=0">
                          <xsl:value-of select="format-number(.,'###0.00')"/>
                          </xsl:if>
                        </td>
                      </xsl:when>
                      <xsl:when test="position()>4 ">
                        <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                      </xsl:when>
                      <xsl:otherwise>
                        <td align='left'><xsl:value-of select="."/></td>
                      </xsl:otherwise>
                    </xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
