<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-2"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
  	  	<td noWrap="true" rowspan="2">科目</td>
  	  	<td noWrap="true" rowspan="2">物资类别</td>
  	  	<td noWrap="true" colspan="2">期初额</td>
  	  	<td style="display:none"></td>
  			<td noWrap="true" colspan="2">借方（本期增加）</td>
  			<td style="display:none"></td>
  			<td noWrap="true" colspan="2">贷方（本期减少）</td>
  			<td style="display:none"></td>
  			<td noWrap="true" colspan="2">期末余额</td>
  			<td style="display:none"></td>
  	  </tr>
      <tr noWrap="true" class="mainHead">
      	<td style="display:none"></td>
      	<td style="display:none"></td>
      	<td noWrap="true">会计账</td>  	
  			<td noWrap="true">实物账</td>
  			<td noWrap="true">会计账</td>  	
  			<td noWrap="true">实物账</td>
  			<td noWrap="true">会计账</td>  	
  			<td noWrap="true">实物账</td>
  			<td noWrap="true">会计账</td>  	
  			<td noWrap="true">实物账</td>
  	  </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
				<tr>
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test="position()=2">
          			<td><xsl:value-of select="../td[1]"/><xsl:value-of select="."/></td>
          		</xsl:when>
          		<xsl:when test="position()=3">
          			<td>
          				<a href="#">
									  <xsl:attribute name="onclick" >
									  	javascript:openDialog('detail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;mate_type_name&gt;<xsl:value-of select="../td[3]"/>&lt;/mate_type_name&gt;','dialogWidth:850px;dialogHeight:650px',result)
									  </xsl:attribute>
									  <xsl:value-of select="."/>
									</a>
								</td>
          		</xsl:when>
          		<xsl:when test="position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 ">
          			<td align='right' class='moneyCol'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
          		</xsl:when>
	          </xsl:choose>
          </xsl:for-each>
        </tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>