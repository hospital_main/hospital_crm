<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<thead>
      <tr noWrap="true" class="mainHead">
  			<th noWrap="true">ժҪ</th>  	
  			<th noWrap="true">���</th>
  			<th noWrap="true">ժҪ</th>  	
  			<th noWrap="true">���</th>
  	  </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
				<tr>
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test="position()=1 or position()=3">
          			<td><xsl:value-of select="."/></td>
          		</xsl:when>
          		<xsl:when test="position()=2 or position()=4 ">
          			<td align="right">
          				<xsl:if test=".!='0'">
          					<xsl:value-of select="format-number(.,'#,##0.00')"/>
          				</xsl:if>
          				<xsl:if test=".='0'">
          				</xsl:if>
          			</td>
          		</xsl:when>
	          </xsl:choose>
          </xsl:for-each>
        </tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>