<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  	  	<th noWrap="true" rowspan="2">科目</th>
  	  	<th noWrap="true" rowspan="2">物资类别</th>
  	  	<th noWrap="true" colspan="2">期初额</th>
  	  	<th style="display:none"></th>
  			<th noWrap="true" colspan="2">借方（本期增加）</th>
  			<th style="display:none"></th>
  			<th noWrap="true" colspan="2">贷方（本期减少）</th>
  			<th style="display:none"></th>
  			<th noWrap="true" colspan="2">期末余额</th>
  			<th style="display:none"></th>
  	  </tr>
      <tr noWrap="true" class="mainHead">
      	<th style="display:none"></th>
      	<th style="display:none"></th>
      	<th noWrap="true">会计账</th>  	
  			<th noWrap="true">实物账</th>
  			<th noWrap="true">会计账</th>  	
  			<th noWrap="true">实物账</th>
  			<th noWrap="true">会计账</th>  	
  			<th noWrap="true">实物账</th>
  			<th noWrap="true">会计账</th>  	
  			<th noWrap="true">实物账</th>
  	  </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
				<tr>
          <xsl:for-each select="td">
          	<xsl:variable name="ps" select="position()"/>
          	<xsl:choose>
          		<xsl:when test="position()=1">
          			<td style="display:none"></td>
          		</xsl:when>
          		<xsl:when test="position()=2">
          			<td><xsl:value-of select="../td[1]"/><xsl:value-of select="."/></td>
          		</xsl:when>
          		<xsl:when test="position()=3">
          			<td>
          				<a href="#">
									  <xsl:attribute name="onclick" >
openDialog('detail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;mate_type_name&gt;<xsl:value-of select="../td[3]"/>&lt;/mate_type_name&gt;','dialogWidth:850px;dialogHeight:650px',result)
</xsl:attribute>
									  <div class="textshot"><xsl:value-of select="."/>
</div>
									</a>
								</td>
          		</xsl:when>
          		
          		<xsl:when test="position()=4 or position()=6 or position()=8 or position()=10  ">
          			<td align='right' class='moneyCol'>
          				<xsl:if test="format-number(.,'#,##0.00') !=format-number(../td[$ps + 1],'#,##0.00')"><xsl:attribute name="bgColor">red</xsl:attribute></xsl:if>
          				<xsl:value-of select="format-number(.,'#,##0.00')"/>
          			</td>
          		</xsl:when>
          		<xsl:when test="position()=5 or position()=7 or position()=9 or position()=11 ">
          			<td align='right' class='moneyCol'>
          				<xsl:if test="format-number(.,'#,##0.00') !=format-number(../td[$ps - 1],'#,##0.00')"><xsl:attribute name="bgColor">red</xsl:attribute></xsl:if>
          				<xsl:value-of select="format-number(.,'#,##0.00')"/>
          			</td>
          		</xsl:when>
	          </xsl:choose>
          </xsl:for-each>
        </tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>