<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
    <table width='100%' class='mainTable' border='1' borderColor='#292954' >
    	<thead>
    		<tr noWrap='true' class='mainHead'>
    			<th noWrap='true'>日期</th>
    			<th noWrap='true'>单据号</th>
     			<th noWrap='true'>是否生成凭证</th>  
    			<th noWrap='true'>摘要</th>
    			<th noWrap='true'>增加</th>
      		<th noWrap='true'>减少</th>
    			<th noWrap='true'>余额</th>
    		</tr>
    	</thead>
    	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
          <tr>
          	<xsl:for-each select="td[position() &lt; 8]">
	          	<xsl:choose>
	          		<xsl:when test="position()=2">
              		<td>
		          			<xsl:if test="'0401'= ../td[last()-1] or '0402'= ../td[last()-1] or '0404'= ../td[last()-1] or '0405'= ../td[last()-1] or '0408'= ../td[last()-1] or '0410'= ../td[last()-1] or '0412'= ../td[last()-1] or '0419'= ../td[last()-1] or '0422'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0451'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test=" '0413'= ../td[last()-1] or '0416'= ../td[last()-1]  " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/smp/in/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test=" '0411'= ../td[last()-1] or '0409'= ../td[last()-1] or '0407'= ../td[last()-1] or '0406'= ../td[last()-1] or '0403'=../td[last()-1] or '0421'= ../td[last()-1] or '0423'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/out/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0414'=../td[last()-1] or '0415'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/out/move/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0417'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/second/advice/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0418'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/smp/out/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0420'=../td[last()-1] or '0423'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0424'=../td[last()-1] or '0425'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/pay/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
	              	</td>
	          		</xsl:when>
	          		<xsl:when test="position() &gt; 4">
              		<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	          		</xsl:when>
	          		<xsl:otherwise>
              		<td><xsl:value-of select="."/></td>
              	</xsl:otherwise>
              </xsl:choose>
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
    </table>
	</xsl:template>
</xsl:stylesheet>