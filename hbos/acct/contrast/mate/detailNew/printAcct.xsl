<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
    <table width='100%' class='mainTable' border='1' borderColor='#292954' >
    	<thead>
 	    	<tr>
					<td style="fontsize:maintitle;colspan:7"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
 	    	<tr>
					<td style="fontsize:subtitle;colspan:7"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
    		<tr noWrap='true' class='mainHead'>
    			<td noWrap='true'>凭证日期</td>
    			<td noWrap='true'>凭证号</td>
     			<td noWrap='true'>是否自动凭证</td>  
    			<td noWrap='true'>摘要</td>
    			<td noWrap='true'>借方</td>
      		<td noWrap='true'>贷方</td>
    			<td noWrap='true'>余额</td>
    		</tr>
    	</thead>
    	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
          <tr>
          	<xsl:for-each select="td">
	          	<xsl:choose>
	          		<xsl:when test="position()=2">
              		<td>
	              			<xsl:value-of select="."/>
	              	</td>
	          		</xsl:when>
	          		<xsl:when test="position() &gt; 4">
              		<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	          		</xsl:when>
	          		<xsl:otherwise>
              		<td><xsl:value-of select="."/></td>
              	</xsl:otherwise>
              </xsl:choose>
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
    </table>
	</xsl:template>
</xsl:stylesheet>