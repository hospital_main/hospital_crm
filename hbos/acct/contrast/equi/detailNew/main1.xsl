<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
    <table width='100%' class='mainTable' border='1' borderColor='#292954'>
    	<thead>
    		<tr noWrap='true' class='mainHead'>
    			<th noWrap='true'>日期</th>
					<th noWrap='true'>凭证号</th>
					<th noWrap='true'>是否自动凭证</th>
					<th noWrap='true'>摘要</th>
					<th noWrap='true'>借方</th>
					<th noWrap='true'>贷方</th>
					<th noWrap='true'>余额</th>    
    		</tr>
    	</thead>
    	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:for-each select="td">
              <xsl:choose>
              <xsl:when test="position()=2">
              		<td align='left' noWrap='true'>
              		<a href="#">
								<xsl:attribute name="onclick" >
              					javascript:openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;+&lt;/edit_mask&gt;");
								</xsl:attribute>
								<xsl:value-of select="."/>
								</a>　
									</td>
	        			</xsl:when>
              	<xsl:when test="position()>= 5">
              		<td align='right' noWrap='true'>
			  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
	        			</xsl:when>
	        			<xsl:otherwise>
	        			<td>
	        				<xsl:value-of select="."/>
	        				</td>
	        			</xsl:otherwise>
              </xsl:choose>
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
    </table>
	</xsl:template>
</xsl:stylesheet>




<!--
xslt语法
引用value用.
引用属性用@

-->
