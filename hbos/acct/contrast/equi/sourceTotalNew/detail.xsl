<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th noWrap='true'>ժҪ</th>
				<th noWrap='true'>���</th>
				<th noWrap='true'>ժҪ</th>
				<th noWrap='true'>���</th>
			</tr>
		</thead>
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:variable name="ps" select="position()"/>
						<td>
							<xsl:choose>
								<xsl:when test="position()=2 or position()=4">
									<xsl:attribute name="align">right</xsl:attribute>
									<xsl:if test=".!=''"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



