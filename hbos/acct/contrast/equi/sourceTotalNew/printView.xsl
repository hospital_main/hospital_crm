<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/contrast/equi/total/printView.xsl,v 1.1 2012/03/12 01:44:37 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:37 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
		<thead>
			<tr>
				<td style="colspan:10;fontsize:maintitle">固定资产总账对账</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td style="colspan:10;fontsize:subtitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td noWrap='true' rowspan="2">科目</td>
				<td noWrap='true' rowspan="2">资产类别</td>
				<td noWrap='true' colspan="2">期初额</td>
				<td style="display:none"></td>
				<td noWrap='true' colspan="2">本期增加</td>
				<td style="display:none"></td>
				<td noWrap='true' colspan="2">本期减少</td>
				<td style="display:none"></td>
				<td noWrap='true' colspan="2">期末余额</td>    
				<td style="display:none"></td>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td noWrap='true'>会计账</td>    
				<td noWrap='true'>实物账</td>
				<td noWrap='true'>会计账</td>    
				<td noWrap='true'>实物账</td>
				<td noWrap='true'>会计账</td>    
				<td noWrap='true'>实物账</td>
				<td noWrap='true'>会计账</td>    
				<td noWrap='true'>实物账</td>
			</tr>
		</thead>
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:variable name="ps" select="position()"/>
						<td>
							<xsl:choose>
								<xsl:when test="position()=2">
									<xsl:if test="../td[1]!=''">
										<a href="#">
										<xsl:attribute name="onclick">openDetail("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;+subjField,sumField,prePageBtn,nextPageBtn,vouchCheckBtn2&lt;/edit_mask&gt;")</xsl:attribute>
										<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="../td[1]=''">
										<xsl:value-of select="."/>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=3 or position()=5 or position()=7 or position()=9">
									<xsl:attribute name="align">right</xsl:attribute>
									<xsl:if test=".!=../td[$ps + 1]"><xsl:attribute name="bgColor">red</xsl:attribute></xsl:if>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:when test="position()=4 or position()=6 or position()=8 or position()=10">
									<xsl:attribute name="align">right</xsl:attribute>
									<xsl:if test=".!=../td[$ps - 1]"><xsl:attribute name="bgColor">red</xsl:attribute></xsl:if>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
