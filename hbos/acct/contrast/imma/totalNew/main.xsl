<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th noWrap='true' rowspan="2">科目</th>
				<th noWrap='true' rowspan="2">资产类别</th>
				<th noWrap='true' colspan="2">期初额</th>
				<th style="display:none"></th>
				<th noWrap='true' colspan="2">本期增加</th>
				<th style="display:none"></th>
				<th noWrap='true' colspan="2">本期减少</th>
				<th style="display:none"></th>
				<th noWrap='true' colspan="2">期末余额</th>    
				<th style="display:none"></th>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<th style="display:none"></th>
				<th style="display:none"></th>
				<th noWrap='true'>会计账</th>    
				<th noWrap='true'>实物账</th>
				<th noWrap='true'>会计账</th>    
				<th noWrap='true'>实物账</th>
				<th noWrap='true'>会计账</th>    
				<th noWrap='true'>实物账</th>
				<th noWrap='true'>会计账</th>    
				<th noWrap='true'>实物账</th>
			</tr>
		</thead>
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:variable name="ps" select="position()"/>
						<td>
							<xsl:choose>
								<xsl:when test="position()=2">
									<xsl:if test="../td[1]!=''">
										<a href="#">
										<xsl:attribute name="onclick">openDetail("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;type_name&gt;<xsl:value-of select="."/>&lt;/type_name&gt;&lt;b_money&gt;<xsl:value-of select="../td[4]"/>&lt;/b_money&gt;&lt;j_money&gt;<xsl:value-of select="../td[6]"/>&lt;/j_money&gt;&lt;d_money&gt;<xsl:value-of select="../td[8]"/>&lt;/d_money&gt;&lt;e_money&gt;<xsl:value-of select="../td[10]"/>&lt;/e_money&gt;")</xsl:attribute>
										<div class="textshot"><xsl:value-of select="."/></div>
										</a>
									</xsl:if>
									<xsl:if test="../td[1]=''">
										<xsl:value-of select="."/>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=3 or position()=5 or position()=7 or position()=9">
									<xsl:attribute name="align">right</xsl:attribute>
									<xsl:if test=".!=../td[$ps + 1]"><xsl:attribute name="bgColor">red</xsl:attribute></xsl:if>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:when test="position()=4 or position()=6 or position()=8 or position()=10">
									<xsl:attribute name="align">right</xsl:attribute>
									<xsl:if test=".!=../td[$ps - 1]"><xsl:attribute name="bgColor">red</xsl:attribute></xsl:if>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



