<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
    <table width='100%' class='mainTable' border='1' borderColor='#292954'>
    	<thead>
    	<tr>
				<td style="colspan:colcount;fontsize:maintitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td style="colspan:colcount;fontsize:subtitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
    		<tr noWrap='true' class='mainHead'>
    		<td style="border:thin;fontsize:coltitle">日期</td>
				<td style="border:thin;fontsize:coltitle">凭证号</td>
				<td style="border:thin;fontsize:coltitle">是否自动凭证</td>
				<td style="border:thin;fontsize:coltitle">摘要</td>
				<td style="border:thin;fontsize:coltitle">借方</td>
				<td style="border:thin;fontsize:coltitle">贷方</td>
				<td style="border:thin;fontsize:coltitle">余额</td>
    		</tr>
    	</thead>
    	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:for-each select="td">
              <xsl:choose>
              <xsl:when test="position()=1">
              <td>
              		<xsl:if test=".!='9999' and .!='0'">
              		<xsl:value-of select="."/>
              		</xsl:if>
              </td>
              </xsl:when>

              <xsl:when test="position()=2">
              		<td align='right' noWrap='true'>
              		<a href="#">
								<xsl:attribute name="onclick" >
								 javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');
								</xsl:attribute>
								<xsl:value-of select="."/>
								</a>　
									</td>
	        			</xsl:when>
              	<xsl:when test="position()>= 5">
              		<td align='right' noWrap='true'>
			  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
	        			</xsl:when>
	        			<xsl:otherwise>
	        			<td>
	        				<xsl:value-of select="."/>
	        				</td>
	        			</xsl:otherwise>
              </xsl:choose>
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
    </table>
	</xsl:template>
</xsl:stylesheet>




<!--
xslt语法
引用value用.
引用属性用@

-->
