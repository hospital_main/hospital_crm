<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
    <table width='100%' class='mainTable' border='1' borderColor='#292954'>
    	<thead>
    		<tr noWrap='true' class='mainHead'>
    			<th noWrap='true'>日期</th>
					<th noWrap='true'>单据号</th>
					<th noWrap='true'>是否已生凭证</th>
					<th noWrap='true'>摘要</th>
					<th noWrap='true'>增加</th>
					<th noWrap='true'>减少</th>
					<th noWrap='true'>余额</th>    
    		</tr>
    	</thead>
    	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:for-each select="td">
              <xsl:choose>
              <xsl:when test="position()=1">
              <td>
              		<xsl:if test=".!='9999' and .!='0'">
              		<xsl:value-of select="."/>
              		</xsl:if>
              </td>	
              </xsl:when>
              <xsl:when test="position()=last()">
              <td style="display:none">
              		
              </td>	
              </xsl:when>
              <xsl:when test="position()=2">
              		<td align='left' noWrap='true'>
              		<xsl:if test="'101'=../td[last()] or '102'=../td[last()] or '103'=../td[last()] or '111'=../td[last()] or '112'=../td[last()] or '113'=../td[last()] or '107'=../td[last()] or '114'=../td[last()]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openPage('<xsl:value-of select="../td[2]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
									<xsl:if test="'105'=../td[last()]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openPage('<xsl:value-of select="../td[2]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'104'=../td[last()] or '108'=../td[last()]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../immaequip/change/back/medicalEquip/update.html?load=&lt;id&gt;<xsl:value-of select="../td[2]"/>&lt;/id&gt;','dialogWidth:1200px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'555'=../td[last()]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../immaequip/change/storetodept/update.html?load=&lt;id&gt;<xsl:value-of select="../td[2]"/>&lt;/id&gt;','dialogWidth:1200px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
											 
								  	</xsl:if>
								  	<xsl:if test="'333'=../td[last()] or '原值增加'=../td[last()] or '原值减少'=../td[last()]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../immaequip/change/primchange/update.html?load=&lt;doc_no&gt;<xsl:value-of select="../td[2]"/>&lt;/doc_no&gt;','dialogWidth:1200px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
											 
								  	</xsl:if>
								  	<xsl:if test="'201'=../td[last()] or '202'=../td[last()] or '203'=../td[last()] or '204'=../td[last()] or '205'=../td[last()] or '206'=../td[last()] or '207'=../td[last()] or '211'=../td[last()] or '213'=../td[last()] or '215'=../td[last()]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openReducePage('<xsl:value-of select="../td[2]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
									</td>
	        			</xsl:when>
              	<xsl:when test="position()>= 5">
              		<td align='right' noWrap='true'>
			  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
	        			</xsl:when>
	        			<xsl:otherwise>
	        			<td>
	        				<xsl:value-of select="."/>
	        				</td>
	        			</xsl:otherwise>
              </xsl:choose>
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
    </table>
	</xsl:template>
</xsl:stylesheet>




<!--
xslt语法
引用value用.
引用属性用@

-->
