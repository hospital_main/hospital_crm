<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:5;fontsize:maintitle'>配套资金收入维护</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					
				</tr>
				<tr noWrap='true' class='mainHead'>
				  	<td nowrap='true'>批复日期</td>
				  	<td nowrap='true'>单据编码</td>
				  	<td nowrap='true'>项目编码</td>
				  	<td nowrap='true'>项目名称</td>
				  	<td nowrap='true'>批复金额</td>
				  	<td nowrap='true'>批复文号</td>
				  	<td nowrap='true'>批复说明</td>
				  	
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
							<xsl:when test="position()=5">
								<td align="right">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>								
								<td >
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
