<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>制单日期</th>
				<th>课题编码</th>
				<th>课题名称</th>
				<th>负责人</th>
				<th>单据编码</th>
				<th>批复金额</th>
				<th>批复文号</th>
				<th>批复说明</th>
				
			</tr>
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
	          <td align='center'  style='display:none'>
	          
						<input type='checkbox' TABINDEX='-1' align='center'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
						</input>
						
					</td>
					<xsl:for-each select="td">
						<td>
							<xsl:choose>
								<xsl:when test="position()=6">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:attribute name="align">right</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:otherwise>
										<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
