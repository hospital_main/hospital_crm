<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true" rowspan="2">项目编码</th>
				<th nowrap="true" rowspan="2">项目名称</th>
				<th nowrap="true" rowspan="2">负责人</th>
				<th nowrap="true" colspan="4">合计</th>
				<th nowrap="true" colspan="4">外拨资金</th>
				<th nowrap="true" colspan="4">配套资金</th>
			</tr>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true">期初余额</th>
				<th nowrap="true">本期收入</th>
				<th nowrap="true">本期支出</th>
				<th nowrap="true">期末余额</th>
				<th nowrap="true">期初余额</th>
				<th nowrap="true">本期收入</th>
				<th nowrap="true">本期支出</th>
				<th nowrap="true">期末余额</th>
				<th nowrap="true">期初余额</th>
				<th nowrap="true">本期收入</th>
				<th nowrap="true">本期支出</th>
				<th nowrap="true">期末余额</th>
			</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
          	<td>
							<xsl:choose>
								<xsl:when test="position()!=1 and position()!=2 and position()!=3">
									<xsl:if test=". != 0">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
										<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
          	</td>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
