<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
		  <colgroup>		       
				<col style = 'width:70'/>
				<col style = 'width:80'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
			</colgroup>  		
    	<thead>
    		<tr noWrap='true' class='mainHead'>
    		<td style='colspan:15;fontsize:maintitle'>科教项目备查簿</td>
    			<td style="display:none"></td>
    		  <td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
  	  	</tr>
	  		<tr noWrap="true" class="mainHead">
					<td nowrap="true" rowspan="2">项目编码</td>
					<td nowrap="true" rowspan="2">项目名称</td>
					<td nowrap="true" rowspan="2">负责人</td>
					<td nowrap="true" colspan="4">合计</td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
					<td nowrap="true" colspan="4">外拨资金</td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
					<td nowrap="true" colspan="4">配套资金</td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
				</tr>
	  		<tr noWrap="true" class="mainHead">
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
					<td nowrap="true">期初余额</td>
					<td nowrap="true">本期收入</td>
					<td nowrap="true">本期支出</td>
					<td nowrap="true">期末余额</td>
					<td nowrap="true">期初余额</td>
					<td nowrap="true">本期收入</td>
					<td nowrap="true">本期支出</td>
					<td nowrap="true">期末余额</td>
					<td nowrap="true">期初余额</td>
					<td nowrap="true">本期收入</td>
					<td nowrap="true">本期支出</td>
					<td nowrap="true">期末余额</td>
				</tr>
    	</thead>
    	<tbody>
  	    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td class="centerText">
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>            
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>