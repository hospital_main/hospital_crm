<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <tbody>
  		<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test=" position() &lt; 3 ">
	    			<xsl:variable name="rows" select="position()"/>
	    			<tr noWrap='true' class='mainHead'>
		          <xsl:for-each select="td">
		          	<xsl:variable name="cols" select="position()"/>
		          	<xsl:variable name="cols_name" select="."/>
		          	<xsl:variable name="colNums" select="count(/root/tbody/tr[ $rows ]/td[.= $cols_name]) "/>
		          	<xsl:choose>
			          	<xsl:when test=" position() &lt; 10 and position() &gt; 2 and position() !=7  and position() !=last() ">
				          			<th >
						            	<xsl:value-of select="."/>
				          			</th>
			          	</xsl:when>
			          	<xsl:when test=" position()>9 ">
			          		<xsl:if test=" $rows=1">
				          		<xsl:if test=" ../td[$cols]!= ../td[$cols+1]">
				          			<th valign="middle">
				          				<xsl:attribute name="colspan" >
				          				<xsl:value-of select="$colNums"/>
						            	</xsl:attribute> 
						            	<xsl:value-of select="."/>
				          			</th>
					          	</xsl:if>
					          	<xsl:if test=" ../td[$cols] = ../td[$cols -1 ]">
				          			<th style="display:none" >
						            	<xsl:value-of select="."/>
				          			</th>
					          	</xsl:if>
					          </xsl:if>	
										<xsl:if test=" $rows=2">
					          	<xsl:if test="position() !=last()">
				          			<th>
						            	<xsl:value-of select="."/>
				          			</th>
					          	</xsl:if> 
					          </xsl:if>					          	
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
	    		<xsl:when test=" position() &gt; 2 ">		          
	    			<tr noWrap='true'>
		          <xsl:for-each select="td"> 		          	
		          	<xsl:choose>
		              <xsl:when test="position()=6">
		                <td><a href='#'>
		                  <xsl:attribute name="onclick" >
		    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../td[7]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');return false;
		  	          	  </xsl:attribute><xsl:value-of select="../td[6]"/>
		  	          	</a></td>
		              </xsl:when>
			          	<xsl:when test=" position() &lt;  3 or position() =last() or position() =7   ">
				          			<td style="display:none">
						            	<xsl:value-of select="."/>
				          			</td>
			          	</xsl:when> 
			          	<xsl:when test=" position() &gt;  2  and  position() &lt;  13 ">
				          			<td>
						            	<xsl:value-of select="."/>
				          			</td>
			          	</xsl:when> 
			          	<xsl:otherwise>
					          <xsl:if test=". =0">
				          			<td/>
					          </xsl:if>
					          <xsl:if test=". !=0">
			      					<td align="right">
					            	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			          			</td>
					          </xsl:if>
			          	</xsl:otherwise>      	
		          	</xsl:choose>
	    				</xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

