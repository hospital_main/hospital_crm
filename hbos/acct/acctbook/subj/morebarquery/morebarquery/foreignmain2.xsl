<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>    
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	       <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=4">
			                <td><a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="../td[5]"/>
			  	          	</a></td>
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()=8 or position()=9 or position()=10 or position()=11">
    			            <td class='numberText'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=13">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[12]='ƽ'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:when test="position()=14">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[12]='ƽ'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
			            <xsl:when test="position() &gt; 14 ">
					      	<td class='numberText'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			            </xsl:when>
		                <xsl:otherwise>
		                	<xsl:if test = ".=''" >
		                		<td><xsl:text>��</xsl:text></td>
		                	</xsl:if>
		                	<xsl:if test = ".!=''">
		              			<td><xsl:value-of select="."/></td>
		              		</xsl:if>
		                </xsl:otherwise>
		              </xsl:choose>
	  			  </xsl:for-each>
		  	  </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

