<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:for-each select="/root/tbody/tr">
      		<xsl:if test="position() =1 ">
		      <tr noWrap='true' class='mainHead'>
	          <xsl:for-each select="td">
		          <xsl:choose>
		            <xsl:when test="position()=1">
				      	<th nowrap='true' colspan="3">期间</th>
				      	<th nowrap='true' rowspan="2" valign="center">凭证号</th>
				      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
				      	<th nowrap='true' rowspan="2" valign="center">汇率</th>
				      	<th nowrap='true' colspan="2" valign="center">借方</th>
				      	<th nowrap='true' colspan="2" valign="center">贷方</th>
				      	<th nowrap='true' rowspan="2" valign="center">方向</th>
				      	<th nowrap='true' colspan="2" valign="center">余额</th>
		            </xsl:when>
		            <xsl:when test="position() = 4 and  . != '' and . != '0'">
		              <th nowrap='true'>
		               	<xsl:attribute name="colspan" >
	      			      <xsl:value-of select="."/>
    	  			    </xsl:attribute>借方分析
			          </th>
		            </xsl:when>
		            <xsl:when test="position() = 5 and .!= '' and . != '0'">
		              <th nowrap='true'>
		               	<xsl:attribute name="colspan" >
	      			      <xsl:value-of select="."/>
    	  			    </xsl:attribute>贷方分析
    	  			  </th>
		            </xsl:when>
		         </xsl:choose>
	          </xsl:for-each>
		      </tr>
        	</xsl:if>
      		<xsl:if test="position() =2 ">
		      <tr noWrap='true' class='mainHead'>
		          <xsl:for-each select="td">
		            <xsl:if test="position() = 1 ">
				      	<th nowrap='true'>年</th>
				      	<th nowrap='true'>月</th>
				      	<th nowrap='true'>日</th>
				      	<th nowrap='true'>原币</th>
				      	<th nowrap='true'>本币</th>
				      	<th nowrap='true'>原币</th>
				      	<th nowrap='true'>本币</th>
				      	<th nowrap='true'>原币</th>
				      	<th nowrap='true'>本币</th>
		            </xsl:if>
		            <xsl:if test="position() &gt; 14 ">
				      	<th nowrap='true'><xsl:value-of select="."/></th>
		            </xsl:if>
		          </xsl:for-each>
	          </tr>
        	</xsl:if>
    	</xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 5">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=4">
			                <td><a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="../td[5]"/>
			  	          	</a></td>
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()=8 or position()=9 or position()=10 or position()=11">
    			            <td class='numberText'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=13">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[12]='平'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:when test="position()=14">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[12]='平'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
			            <xsl:when test="position() &gt; 14 ">
					      	<td class='numberText'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			            </xsl:when>
		                <xsl:otherwise>
		                	<xsl:if test = ".=''" >
		                		<td><xsl:text>　</xsl:text></td>
		                	</xsl:if>
		                	<xsl:if test = ".!=''">
		              			<td><xsl:value-of select="."/></td>
		              		</xsl:if>
		                </xsl:otherwise>
		              </xsl:choose>
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

