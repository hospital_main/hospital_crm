<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:for-each select="/root/tbody/tr">
      		<xsl:if test="position() =1 ">
		      <tr noWrap='true' class='mainHead'>
	          <xsl:for-each select="td">
		          <xsl:choose>
		            <xsl:when test="position()=1">
				      	<th nowrap='true' colspan="2"><xsl:value-of select="."/>年</th>
				      	<th nowrap='true' rowspan="2" valign="center">凭证号</th>
				      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
				      	<th nowrap='true' rowspan="2" valign="center">借方</th>
				      	<th nowrap='true' rowspan="2" valign="center">贷方</th>
				      	<th nowrap='true' rowspan="2" valign="center">方向</th>
				      	<th nowrap='true' rowspan="2" valign="center">余额</th>
		            </xsl:when>
		            <xsl:when test="position() = 2 and  . != '' and . != '0'">
		              <th nowrap='true'>
		               	<xsl:attribute name="colspan" >
	      			      <xsl:value-of select="."/>
    	  			    </xsl:attribute>余额分析
			          </th>
		            </xsl:when>
		         </xsl:choose>
	          </xsl:for-each>
		      </tr>
        	</xsl:if>
      		<xsl:if test="position() =2 ">
		      <tr noWrap='true' class='mainHead'>
		          <xsl:for-each select="td">
		            <xsl:if test="position() = 1 ">
				      	<th nowrap='true'>月</th>
				      	<th nowrap='true'>日</th>
		            </xsl:if>
		            <xsl:if test="position() &gt; 9 ">
				      	<th nowrap='true'><xsl:value-of select="."/></th>
		            </xsl:if>
		          </xsl:for-each>
	          </tr>
        	</xsl:if>
    	</xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 2">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=3">
			                <td><a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="../td[4]"/>
			  	          	</a></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=6 or position()=7 or position()=9">
    			            <td class='numberText'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
			            <xsl:when test="position() &gt; 9 ">
					      	<td class='numberText'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			            </xsl:when>
		                <xsl:otherwise>
		                	<xsl:if test = ".=''" >
		                		<td><xsl:text>　</xsl:text></td>
		                	</xsl:if>
		                	<xsl:if test = ".!=''">
		              			<td><xsl:value-of select="."/></td>
		              		</xsl:if>
		                </xsl:otherwise>
		              </xsl:choose>
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

