<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>  
  <xsl:template match="/">
  	<thead>
  	  <tr noWrap='true' class='mainHead'>
        <th style="display:none"><input type="checkbox"/></th>
				<th>设置名称 </th>
				<th>设置科目 </th>
				<th>打印模板设置 </th>
				<th>打印模板设置(外币) </th>
      </tr>
     </thead>
  	<tbody> 
 	  <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            
              <xsl:choose>
                <xsl:when test="position()=1">
                <td >
                  <a tabindex='-1'><xsl:value-of select="."/></a>
                 </td>
                </xsl:when>
                <xsl:when test="position()=3">
                <td >
                  <a href="#">
										<xsl:attribute name="onclick">
											openSetPage("<xsl:value-of select="../td[5]"/>",0)
										</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
									</td>
                </xsl:when>
                <xsl:when test="position()=4">
                <td >
                  <a href="#">
										<xsl:attribute name="onclick">
											openSetPage("<xsl:value-of select="../td[5]"/>",1)
										</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
									</td>
                </xsl:when>
                <xsl:when test="position()=5">
                </xsl:when>
                <xsl:otherwise>
                <td >
                  <xsl:value-of select="."/>
                 </td>
                </xsl:otherwise>
              </xsl:choose>
  		  </xsl:for-each>
  	    </tr>
   	  </xsl:for-each> 
   	</tbody>
  </xsl:template>
</xsl:stylesheet>