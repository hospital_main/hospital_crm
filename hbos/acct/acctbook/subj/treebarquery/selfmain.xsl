<?xml version='1.0' encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">

    <thead>

    	<xsl:for-each select="/root/tbody/tr">

      		<xsl:if test="position() =1 ">

	          <xsl:for-each select="td">

	            <xsl:if test="position() = 2 ">

				      <tr noWrap='true' class='mainHead'>

				      	<th nowrap='true' colspan="3"><xsl:value-of select="."/>期间</th>

				      	<th nowrap='true' rowspan="2" valign="center">凭证号</th>

				      	<th nowrap='true' rowspan="2" valign="center">摘要</th>

				      	<th nowrap='true' rowspan="2" valign="center">借方</th>

				      	<th nowrap='true' rowspan="2" valign="center">贷方</th>

				      	<th nowrap='true' rowspan="2" valign="center">方向</th>

				      	<th nowrap='true' rowspan="2" valign="center">余额</th>

				      </tr>

				      <tr noWrap='true' class='mainHead'>

				      	<th nowrap='true'>年</th>

				      	<th nowrap='true'>月</th>

				      	<th nowrap='true'>日</th>

				      </tr>

	            </xsl:if>

	          </xsl:for-each>

        	</xsl:if>

    	</xsl:for-each>

    </thead>



    <tbody>

      <xsl:for-each select="/root/tbody/tr">

      	<xsl:variable name="colCount" select="position()" />

	      <xsl:if test="position() &gt; 0">

		      <tr>

		          <xsl:for-each select="td">

		              <xsl:choose>

		                <xsl:when test="position()=1 or position()=2 or position()=3">

		                  <td align="center"><xsl:value-of select="."/></td>

		                </xsl:when>

		                <xsl:when test="position()=3">
			                <!-- <td><a href='#'>

			                  <xsl:attribute name="onclick" >

			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');return false;

			  	          	  </xsl:attribute><xsl:value-of select="../td[4]"/>

			  	          	</a></td> -->

		                </xsl:when>

		                <xsl:when test="position()=4">
		                </xsl:when>
										<xsl:when test="position()=5">
											<td><a href='#'>

			                  <xsl:attribute name="onclick" >

			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../td[4]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;');return false;

			  	          	  </xsl:attribute><xsl:value-of select="."/>

			  	          	</a></td>
		                </xsl:when>

		                <xsl:when test="position()=7">

			                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>

		                </xsl:when>

		                <xsl:when test="position()=8">

			                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>

		                </xsl:when>

		                <xsl:when test="position()=11 or position()=12 ">

	                	</xsl:when>

		                <xsl:when test="position()=10">

		                	<xsl:if test="(../td[10]='13' or ../td[10]='07') ">

			                <td align='right' ><a href='#'>

			                  <xsl:attribute name="onclick" >

			    	            javascript:openWin('<xsl:value-of select="../td[10]"/>');

			  	          	  </xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>

			  	          	</a></td>

			  	          	</xsl:if>

			  	          	<xsl:if test="../td[10]!='13' and ../td[10]!='07'">

			  	          		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>

			  	          	</xsl:if>

		                </xsl:when>

		                <xsl:otherwise>

		                  <td><xsl:value-of select="."/></td>

		                </xsl:otherwise>

		              </xsl:choose>

	  			  </xsl:for-each>

		  	  </tr>

	  	  </xsl:if>

      </xsl:for-each>

    </tbody>

  </xsl:template>

</xsl:stylesheet>



