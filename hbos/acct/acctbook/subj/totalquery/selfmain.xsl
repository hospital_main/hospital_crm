<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="SHOWPZH" select="/root/annex/SHOWPZH"/>
    	<xsl:for-each select="/root/tbody/tr">
    		<xsl:if test="position() =1 ">
          <xsl:for-each select="td">
            <xsl:if test="position() = 2 ">
			      <tr noWrap='true' class='mainHead'>
			      	<th nowrap='true' colspan="2"><xsl:value-of select="."/>期间</th>
			      	<xsl:choose>	            
                <xsl:when test="$SHOWPZH = 'true' or $SHOWPZH = true">  
			      			<th nowrap='true' rowspan="2" valign="center">凭证号</th>
                </xsl:when> 
                <xsl:otherwise>
              	</xsl:otherwise>  
           		</xsl:choose> 
			      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
			      	<th nowrap='true' rowspan="2" valign="center">借方</th>
			      	<th nowrap='true' rowspan="2" valign="center">贷方</th>
			      	<th nowrap='true' rowspan="2" valign="center">方向</th>
			      	<th nowrap='true' rowspan="2" valign="center">余额</th>
			      </tr>
			      <tr noWrap='true' class='mainHead'>
			      	<th nowrap='true'>年</th>
			      	<th nowrap='true'>月</th>
			      </tr>
            </xsl:if>
          </xsl:for-each>
      	</xsl:if>
    	</xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 0">
     		 <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>		               
		                <xsl:when test="position()=2">
		                  <td><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:when test="position()=3">
		                	<xsl:choose>	            
				                <xsl:when test="$SHOWPZH = 'true' or $SHOWPZH = true">  
				                <td align='left'><xsl:value-of select="." disable-output-escaping="yes"/>
	    			            </td>
				                </xsl:when> 
				                <xsl:otherwise>
			                	</xsl:otherwise>  
		             		  </xsl:choose> 
		                </xsl:when>
		                <xsl:when test="position()=8">
		              	<xsl:choose>	
		                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[7]='平'">
    			            	<td align='right'>Q</td>
    			          	</xsl:when>
			                <xsl:otherwise>
			                  <td><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			                </xsl:otherwise>
		             		 </xsl:choose>
		                </xsl:when>
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>
	  			  </xsl:for-each>
	  	  	</tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

