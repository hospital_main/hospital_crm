<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
    <thead>
       	<xsl:for-each select="/root/tbody/tr">
      		<xsl:if test="position() =1 ">
	          <xsl:for-each select="td">
	            <xsl:if test="position() = 2 ">
	           <tr noWrap='true' class='mainHead'>
			      	<td nowrap='true' colspan="10" style="fontsize:maintitle"></td>0
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      </tr>
	           <tr noWrap='true' class='mainHead'>
			      	<td nowrap='true' colspan="10" style="fontsize:maintitle"></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      </tr>
	          <tr noWrap='true' class='mainHead'>
			      	<td nowrap='true' colspan="10" style="fontsize:subtitle"></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      	<td style='display:none'></td>
			      </tr>
			      <tr noWrap='true' class='mainHead'>
			      	<td nowrap='true' colspan="2">xxxx期间</td>
			      	<td style='display:none'></td>
			      	<td nowrap='true' rowspan="2" valign="center">摘要</td>
			      	<td nowrap='true' colspan="2">借方</td>
			      	<td style='display:none'></td>
			      	<td nowrap='true' colspan="2">贷方</td>
			      	<td style='display:none'></td>
			      	<td nowrap='true' rowspan="2" valign="center">方向</td>
			      	<td nowrap='true' colspan="2">余额</td>
			      	<td style='display:none'></td>
			      </tr>
			      <tr noWrap='true' class='mainHead'>
			      	<td nowrap='true'>月</td>
			      	<td nowrap='true'>日</td>
			      	<td style='display:none'></td>
			      	<td nowrap='true'>原币</td>
			      	<td nowrap='true'>本币</td>
			      	<td nowrap='true'>原币</td>
			      	<td nowrap='true'>本币</td>
			      	<td style='display:none'></td>
			      	<td nowrap='true'>原币</td>
			      	<td nowrap='true'>本币</td>
			      </tr>
	            </xsl:if>  
	          </xsl:for-each>
        	</xsl:if>
    	</xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 0">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>     
		                <xsl:when test="position()=1">  
		                  <td style="align:right;"><xsl:value-of select="../td[2]"/></td>
		                </xsl:when>       
		                <xsl:when test="position()=2">
		                	<td></td>  
		                </xsl:when> 
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position()=5 or position()=6 or position()=7 or position()=8">
    			            <td class='numberText'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=10">
    			            <xsl:choose>	
		                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[9]='平'">
    			            	<td align='right'>Q</td>
    			          	</xsl:when>
			                <xsl:otherwise>
			                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			                </xsl:otherwise>
		             		 </xsl:choose>
		                </xsl:when>
		                <xsl:when test="position()=11">
    			            <xsl:choose>	
		                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[9]='平'">
    			            	<td align='right'>Q</td>
    			          	</xsl:when>
			                <xsl:otherwise>
			                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			                </xsl:otherwise>
		             		 </xsl:choose>
		                </xsl:when>
		                <xsl:otherwise>
		                  <td style="align:left"><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



