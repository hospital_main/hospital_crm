<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
    <colgroup>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:if test="position() =1">
      	  <xsl:for-each select="td">
      	    <xsl:if test="position() &gt;1">
	            <col style = 'width:120'/>
	          </xsl:if>
	        </xsl:for-each>
	      </xsl:if>
      </xsl:for-each>
    </colgroup>
    <thead>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:if test="position() =1 ">
          <tr noWrap="true" class="mainHead">	        	
            <xsl:for-each select="td">
              <xsl:if test="position() &gt;1 ">          
                <th noWrap="true"><xsl:value-of select="."/></th>
              </xsl:if>  
            </xsl:for-each>
          </tr>
        </xsl:if>
    	</xsl:for-each>
    </thead>
    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:if test="position() &gt; 1">
	        <tr>
	          <xsl:for-each select="td">	            
              <xsl:choose>
                <xsl:when test="position()=1">	                  
                </xsl:when>
                <xsl:when test="position()=2">
                  <td><xsl:value-of select="."/></td>	                  
                </xsl:when>
                <xsl:when test="position()=3">
			            <xsl:variable name="tgvalue">
			              <xsl:value-of select="."/>
			            </xsl:variable>	                	
                	<xsl:if test="$tgvalue = 0">
                		<td></td>
                	</xsl:if>	                	 
                	<xsl:if test="$tgvalue != 0">
                		<td align='right' class='moneyCol'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                	</xsl:if>
	              </xsl:when>	          
                <xsl:otherwise>
                  <td align='right' class='moneyCol'>
		                <xsl:value-of select="format-number(.,'#,##0.00')"/>  
		              </td>
                </xsl:otherwise>
              </xsl:choose>	            
	  			  </xsl:for-each>
	  			</tr>
  			</xsl:if>
   		</xsl:for-each>  	
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



