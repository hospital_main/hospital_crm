<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
			      <tr noWrap='true' class='mainHead'>
			      	<th id='yearText' nowrap='true' colspan='3'>期间</th>
			      	<th nowrap='true' rowspan='2' valign="center" width="60">凭证号</th>
			      	<th nowrap='true' rowspan='2' valign="center">摘要</th>
			      	<th nowrap='true' rowspan='2' valign="center">借方</th>
			      	<th nowrap='true' rowspan='2' valign="center">贷方</th>
			      	<th nowrap='true' rowspan='2' valign="center">方向</th>
			      	<th nowrap='true' rowspan='2' valign="center">余额</th>
			      </tr> 
			      <tr noWrap='true' class='mainHead'>
			        <th nowrap='true'>年</th> 
			        <th nowrap='true'>月</th> 
				      <th nowrap='true'>日</th> 
				     </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="colCount" select="position()" />
	      <xsl:if test="position() &gt; 0">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=2 or position()=3">
		                  <td align="center"><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
			                <td><xsl:value-of select="../td[5]" disable-output-escaping="yes"/>
			  	          	</td>
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()=7">
			                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=8">
			                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=11 or position()=12 ">
	                	</xsl:when>
		                <xsl:when test="position()=10">
		                	<xsl:if test="(../td[10]='13' or ../td[10]='07') ">
			                <td align='right' ><a href='#'>
			                  <xsl:attribute name="onclick" >
			    	            javascript:openWin('<xsl:value-of select="../td[11]"/>');
			  	          	  </xsl:attribute>
			  	          	  <xsl:choose>	
			                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[9]='平'">
	    			            	Q
	    			          	</xsl:when>
				                <xsl:otherwise>
				                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
				                </xsl:otherwise>
			             		 </xsl:choose>
			  	          	</a>
			  	          	</td>
			  	          	</xsl:if>
			  	          	<xsl:if test="../td[10]!='13' and ../td[10]!='07'">
			  	          	  <xsl:choose>	
			                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[9]='平'">
	    			            	<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
				                 	<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
			  	          	</xsl:if>
		                </xsl:when>
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

