<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
			      <tr noWrap='true' class='mainHead'>
			      	<th id='yearText' nowrap='true' colspan="3">期间</th>
			      	<th nowrap='true' rowspan="3" valign="center" width="60">凭证号</th>
			      	<th nowrap='true' rowspan="3" valign="center">摘要</th>
			      	<th nowrap='true' rowspan="3" valign="center">汇率</th>
			      	<th nowrap='true' colspan="28">借方</th>
			      	<th nowrap='true' colspan="28">贷方</th>
			      	<th nowrap='true' rowspan="3" valign="center">方向</th>
			      	<th nowrap='true' colspan="28">余额</th>
			      </tr>
			      <tr noWrap='true' class='mainHead'>
			      	<th nowrap='true' rowspan="2" valign="center">年</th>
			      	<th nowrap='true' rowspan="2" valign="center">月</th>
			      	<th nowrap='true' rowspan="2" valign="center">日</th>
			      	<th nowrap='true' colspan="14" valign="center">原币</th>
			      	<th nowrap='true' colspan="14" valign="center">本币</th>
			      	<th nowrap='true' colspan="14" valign="center">原币</th>
			      	<th nowrap='true' colspan="14" valign="center">本币</th>
			      	<th nowrap='true' colspan="14" valign="center">原币</th>
			      	<th nowrap='true' colspan="14" valign="center">本币</th>
			      </tr>
			      <tr noWrap='true' class='mainHead'>
			      	<th nowrap='true' style='display:none'></th>
			      	<th nowrap='true' style='display:none'></th>
			      	<th nowrap='true' style='display:none'></th>
			      </tr>
    </thead>
      <tbody>
      <xsl:for-each select="/root/tbody/tr">
          <xsl:if test="position() &gt; 0">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                 <xsl:when test="position()=2 or position()=3">
		                  <td align="center"><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
			                <td><xsl:value-of select="../td[5]"/>
			  	          	</td>
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()=7">
		                  <td align='right'><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:when test="position()=8 or position()=9">
			                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=10 or position()=11">
			                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=13">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[12]='平'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:when test="position()=14">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[12]='平'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
		  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
