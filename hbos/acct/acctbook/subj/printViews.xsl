<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
    <thead>
    	<xsl:for-each select="/root/tbody/tr">
      		<xsl:if test="position() =1 ">
	          <xsl:for-each select="td">
	            <xsl:if test="position() = 5 ">
	           <tr noWrap='true' class='mainHead'>
			      	<td nowrap='true' colspan="8" style="fontsize:maintitle"></td>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      </tr>
			      <tr noWrap='true' class='mainHead'>
			      	<td nowrap='true' colspan="8" style="fontsize:subtitle"></td>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      </tr>
			      <tr noWrap='true' class='mainHead'>
			      	<td nowrap='true' colspan="8" style="fontsize:subtitle"></td>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      </tr>
			      <tr noWrap='true' class='mainHead'>
			      	<td nowrap='true' colspan="2" style="fontsize:coltitle">xxxx年</td>
			      	<td style='display:none'/>
			      	<td nowrap='true' rowspan="2" style="fontsize:coltitle" valign="center" width="60">凭证号</td>
			      	<td nowrap='true' rowspan="2" style="fontsize:coltitle" valign="center">摘要</td>
			      	<td nowrap='true' rowspan="2" style="fontsize:coltitle" valign="center">借方</td>
			      	<td nowrap='true' rowspan="2" style="fontsize:coltitle" valign="center">贷方</td>
			      	<td nowrap='true' rowspan="2" style="fontsize:coltitle" valign="center">方向</td>
			      	<td nowrap='true' rowspan="2" style="fontsize:coltitle" valign="center">余   额</td>
			      </tr>
			      <tr noWrap='true' class='mainHead'>
			      	<td nowrap='true' style="fontsize:coltitle">月</td>
			      	<td nowrap='true' style="fontsize:coltitle">日</td>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      	<td style='display:none'/>
			      </tr>
	            </xsl:if>
	          </xsl:for-each>
        	</xsl:if>
    	</xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 0">
	      	
					<xsl:variable name="rowNum" select="position()"/>
					<tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=7">
			                <td class='numberText' style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=8">
			                <td class='numberText' style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=10">
			                <xsl:choose>	
			                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[9]='平'">
	    			            	<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
				                 	<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:when test="position()=last()"> 
		                </xsl:when>
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



