<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
    <thead>
    	<xsl:for-each select="/root/tbody/tr">
      		<xsl:if test="position() =1 ">
	          <xsl:for-each select="td">
	            <xsl:if test="position() = 2 ">
	            	<tr noWrap='true' class='mainHead'>
	      				<td style="colspan:8;fontsize:maintitle;"></td>
	      				<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      </tr>
				      <tr class='mainHead'> 
	      				<td style="colspan:8;fontsize:maintitle;"></td>
	      				<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      </tr>
				      <tr class='mainHead'>
	      				<td style="colspan:8;fontsize:maintitle;"></td>
	      				<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      	<td style='display:none'></td>
				      </tr>
				      <tr class='mainHead'> 
				      	<td colspan="2" valign="center">xxxx期间</td>
				      	<td style='display:none'></td>
				      	<td rowspan="2" valign="center">凭证号</td>
				      	<td rowspan="2" valign="center">摘要</td>
				      	<td rowspan="2" valign="center">借方</td>
				      	<td rowspan="2" valign="center">贷方</td>
				      	<td rowspan="2" valign="center">方向</td>
				      	<td rowspan="2" valign="center">余额</td>
				      </tr> 
				      <tr class='mainHead'> 
				      	<td rowspan="1" valign="center">月</td>
				      	<td rowspan="1" valign="center">日</td>
				      	</tr> 
	            </xsl:if>
	          </xsl:for-each>
        	</xsl:if>
    	</xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 0">
     		 <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">  
		                  <td style="align:right;"><xsl:value-of select="../td[2]"/></td>
		                </xsl:when>       
		                <xsl:when test="position()=2">
		                	<td></td>  
		                </xsl:when> 
		                <xsl:when test="position()=3">
    			            	<td align='left'><xsl:value-of select="." disable-output-escaping="yes"/></td>
		                </xsl:when>
		                <xsl:when test="position()=5 or position()=6">
    			            <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=8">
    			           <xsl:choose>	
		                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[7]='平'">
    			            	<td align='right'>Q</td>
    			          	</xsl:when>
			                <xsl:otherwise>
			                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			                </xsl:otherwise>
		             		 </xsl:choose>
		                </xsl:when>
		                <xsl:otherwise>
		                  <td style="align:left;"><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>
	  			  </xsl:for-each>
	  	  	</tr>
	  	  </xsl:if>
      </xsl:for-each>
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



