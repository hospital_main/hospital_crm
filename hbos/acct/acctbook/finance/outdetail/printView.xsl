<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
	<thead>
		<xsl:variable name="idx2" select="/root/tbody/tr[1]/td[2]"/>    
		<xsl:variable name="idx3" select="/root/tbody/tr[1]/td[3]"/>    
		<xsl:for-each select="/root/tbody/tr">
			<xsl:if test="position() =1 ">
				<tr>
					<td style="fontsize:maintitle">
						<xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)-1"/></xsl:attribute>
					</td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<xsl:for-each select="/root/tbody/tr[1]/td">
						<xsl:if test="position() &gt; 9 ">
							<td style='display:none'/>
						</xsl:if>
					</xsl:for-each>
				</tr>
				<tr>
					<td style="fontsize:subtitle"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)-1"/></xsl:attribute></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<xsl:for-each select="/root/tbody/tr[1]/td">
						<xsl:if test="position() &gt; 9 ">
							<td style='display:none'/>
						</xsl:if>
					</xsl:for-each>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td nowrap='true' colspan="2"><xsl:value-of select="."/>年</td>
								<td style='display:none'/>
								<td nowrap='true' rowspan="2" valign="center">凭证号</td>
								<td nowrap='true' rowspan="2" valign="center">摘要</td>
								<td nowrap='true' rowspan="2" valign="center">借方</td>
								<td nowrap='true' rowspan="2" valign="center">贷方</td>
								<td nowrap='true' rowspan="2" valign="center">方向</td>
								<td nowrap='true' rowspan="2" valign="center">余额</td>
							</xsl:when>
							<xsl:when test="position() = 2  and . &gt; 0">
								<td nowrap='true' >
									<xsl:attribute name="colspan" >
										<xsl:value-of select="."/>
									</xsl:attribute>借方分析
								</td>
								<xsl:for-each select="/root/tbody/tr[1]/td">
									<xsl:if test="position() &lt; $idx2">
										<td style='display:none'></td>
									</xsl:if>
								</xsl:for-each>
							</xsl:when>
							<xsl:when test="position() = 3 and . &gt; 0">
								<td nowrap='true'>
									<xsl:attribute name="colspan" >
										<xsl:value-of select="."/>
									</xsl:attribute>贷方分析
								</td>
								<xsl:for-each select="/root/tbody/tr[1]/td">
									<xsl:if test="position() &lt; $idx3">
										<td style='display:none'/>
									</xsl:if>
								</xsl:for-each>
							</xsl:when>
						</xsl:choose>	            
					</xsl:for-each>
				</tr>
			</xsl:if>
			<xsl:if test="position() =2 ">
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="td">
						<xsl:if test="position() = 1 ">
							<td nowrap='true'>月</td>
							<td nowrap='true'>日</td>
							<td style='display:none'/>
							<td style='display:none'/>
							<td style='display:none'/>
							<td style='display:none'/>
							<td style='display:none'/>
							<td style='display:none'/>
						</xsl:if>  
						<xsl:if test="position() &gt; 9 ">
							<td nowrap='true'><xsl:value-of select="."/></td>
						</xsl:if>  
					</xsl:for-each>
				</tr>
			</xsl:if>
		</xsl:for-each>
	</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 2">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position()=6 or position()=7 or position()=9">
    			            <td  style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
			            <xsl:when test="position() &gt; 9 ">
					      	<td  style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			            </xsl:when>  
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



