<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th nowrap="true">科目编码</th>
				<th nowrap="true">科目名称</th>
				<th nowrap="true">医疗业务成本</th>
				<th nowrap="true">管理费用</th>
				<th nowrap="true">合计</th>
			</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
          	
		     <xsl:choose>
          <xsl:when test="position()=6">
          </xsl:when>
          <xsl:when test="position()=3">
          	<xsl:if test=".=0">
          		<td></td>
          	</xsl:if>
          	<xsl:if test=".!=0">
            <td align='right'><a href='#'>
	                    <xsl:attribute name="onclick">openDialog('main_p.html?load=&lt;typ&gt;yl&lt;/typ&gt;<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:950px;dialogHeight:800px;overflow:auto;', this);</xsl:attribute>
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </a></td>
	           </xsl:if>
          </xsl:when>
          <xsl:when test="position()=4">
          	<xsl:if test=".=0">
          		<td></td>
          	</xsl:if>
          	<xsl:if test=".!=0">
            <td align='right'><a href='#'>
	                    <xsl:attribute name="onclick">openDialog('main_p.html?load=&lt;typ&gt;gl&lt;/typ&gt;<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:950px;dialogHeight:800px;overflow:auto;', this);</xsl:attribute>
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </a></td>
	           </xsl:if>
          </xsl:when>
          <xsl:when test="position()=5">
          	<xsl:if test=".=0">
          		<td></td>
          	</xsl:if>
          	<xsl:if test=".!=0">
            <td align='right'><a href='#'>
	                    <xsl:attribute name="onclick">openDialog('main_p.html?load=&lt;typ&gt;all&lt;/typ&gt;<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:950px;dialogHeight:800px;overflow:auto;', this);</xsl:attribute>
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </a></td>
	           </xsl:if>
          </xsl:when>
          <xsl:otherwise>
		         <td align='left'>
              <xsl:value-of select="."/>  
            </td>
          </xsl:otherwise>   	
		     </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each> 
    	<tr>
     		<td></td>
     		<td align="center">合计</td>
     		<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr[td[6]='1']/td[3]),'#,##0.00')"/></td>
     		<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr[td[6]='1']/td[4]),'#,##0.00')"/></td>
     		<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr[td[6]='1']/td[5]),'#,##0.00')"/></td>
     	</tr> 	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
