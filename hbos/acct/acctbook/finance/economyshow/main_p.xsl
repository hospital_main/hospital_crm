<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th>凭证编号</th>
				<th>发生日期</th>
				<th>摘要</th>
				<th>科目编码</th>
				<th>科目名称</th>
				<th>借方金额</th>
				<th>制单人</th>
			</tr>
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position()!=11 and position()!=12 and position()!=13]">
						<td>
							<xsl:choose>
								<xsl:when test="position()=6">
									<xsl:if test=". != 0">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
										<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
