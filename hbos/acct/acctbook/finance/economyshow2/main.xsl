<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th nowrap="true">年</th>
				<th nowrap="true">月</th>
				<th nowrap="true">日</th>
				<th nowrap="true">凭证号</th>
				<th nowrap="true">摘要</th>
				<th nowrap="true">支出功能分类科目</th>
				<th nowrap="true">支出经济分类科目</th>
				<th nowrap="true">借方</th>
				<th nowrap="true">贷方</th>
				<th nowrap="true">借/贷</th>
				<th nowrap="true">余额</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
          	
		     <xsl:choose>
          <xsl:when test="position()=1">
          </xsl:when>
          
          <xsl:when test="position()=13">
          </xsl:when>
          
          <xsl:when test="position()=9 or position()=10 or position()=12">
          <td align="right">
           	<xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">
				<xsl:attribute name="class">numberText</xsl:attribute>
				<xsl:value-of select="format-number(.,'#,##0.00')"/>
			</xsl:if>
			
          </td>
          </xsl:when>
          
          <xsl:when test="position()=4">
          		<xsl:if test=".=32 or .=33">
          			<td></td>
          		</xsl:if>
          		
          		<xsl:if test=".!=33 and .!=32">
          			<td>
              		<xsl:value-of select="."/>  
            		</td>
          		</xsl:if>          	
          </xsl:when>
          
          
          <xsl:when test="position()=5">
          		<xsl:if test="../pk/vouch_id!=''">
	                 <td >
										<a href="#">
											<xsl:attribute name="onclick">openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;&lt;/edit_mask&gt;")</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
							</xsl:if>
          		
          		<xsl:if test="../pk/vouch_id=''">
          			<td>
              		<xsl:value-of select="."/>  
            		</td>
          		</xsl:if> 
          		         	
          </xsl:when>
          
          <xsl:otherwise>
		         <td align='left'>
              <xsl:value-of select="."/>  
            </td>
          </xsl:otherwise>   	
		     </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each> 
	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
