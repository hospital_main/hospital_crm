<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/acctbook/finance/economyshow2/printView.xsl,v 1.3 2013/01/25 06:42:39 chenhaifeng Exp $
 $Author: chenhaifeng $
 $Date: 2013/01/25 06:42:39 $
 $Revision: 1.3 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
		<thead>
			<tr>
				<td style="colspan:11;fontsize:maintitle">财政补助备查簿</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td style="colspan:11;fontsize:subtitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td nowrap="true">年</td>
				<td nowrap="true">月</td>
				<td nowrap="true">日</td>
				<td nowrap="true">凭证号</td>
				<td nowrap="true">摘要</td>
				<td nowrap="true">支出功能分类科目</td>
				<td nowrap="true">支出经济分类科目</td>
				<td nowrap="true">借方</td>
				<td nowrap="true">贷方</td>
				<td nowrap="true">借/贷</td>
				<td nowrap="true">余额</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
          <xsl:when test="position()=1">
          </xsl:when>
          
          <xsl:when test="position()=13">
          </xsl:when>
          <xsl:when test="position()=4">
          		<xsl:if test=".=32 or .=33">
          			<td></td>
          		</xsl:if>
          		
          		<xsl:if test=".!=33 and .!=32">
          			<td>
              		<xsl:value-of select="."/>  
            		</td>
          		</xsl:if>          	
          </xsl:when>
          
          <xsl:when test="position()=9 or position()=10 or position()=12">
          <td align="right">
           	<xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">
			<xsl:attribute name="class">numberText</xsl:attribute>
			<xsl:value-of select="format-number(.,'#,##0.00')"/>
			</xsl:if>
          </td>
          </xsl:when>
          
          <xsl:when test="position()=5">
          		<xsl:if test="../td[position()=11]='贷'">
											<td align='left'>
													<a href='#'>
				                    <xsl:attribute name="onclick">openDialog('detail.html?load=&lt;typ&gt;all&lt;/typ&gt;<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:950px;dialogHeight:800px;overflow:auto;', this);</xsl:attribute>
				                    <xsl:value-of select="."/>
	                  			</a>
	                  	</td>
							</xsl:if>
          		
          		<xsl:if test="../td[position()=11]!='贷'">
          			<td>
              		<xsl:value-of select="."/>  
            		</td>
          		</xsl:if> 
          		         	
          </xsl:when>
	  
          <xsl:otherwise>
		         <td align='left'>
              <xsl:value-of select="."/>  
            </td>
          </xsl:otherwise>  
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4" style="align:left;fontsize:coltitle">会计主管：</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td  colspan="2" style="align:left;fontsize:coltitle">制表人：</td>
				<td style="display:none"></td>
				<td  colspan="2" style="align:left;fontsize:coltitle">审核人：</td>
				<td style="display:none"></td>
				<td  colspan="3" style="align:left;fontsize:coltitle">记账人：</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>		
		</tfoot>
		</table>
	</xsl:template>
</xsl:stylesheet>
