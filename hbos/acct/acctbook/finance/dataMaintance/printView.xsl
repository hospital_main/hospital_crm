<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:8;fontsize:maintitle'>财政基本补助支出维护</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap='true' class='mainHead'>
				  	<td nowrap='true'>发生日期</td>
				  	<td nowrap='true'>凭证类型</td>
				  	<td nowrap='true'>凭证号</td>
				  	<td nowrap='true'>摘要</td>
				  	<td nowrap='true'>科目</td>
				  	<td nowrap='true'>功能分类</td>
				  	<td nowrap='true'>经济分类</td>
				  	<td nowrap='true'>支出金额</td>
				  	<td nowrap='true'>制单人</td>
				  	<td nowrap='true'>导入标识</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
							<xsl:when test="position()=1 or position()=3 or position()=4">
								<td align="right">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()=2 or position()=5 or position()=6 or position()=7"> 
								<td><xsl:value-of select="substring-after(.,' ')"/></td>
							</xsl:when>
							
							<xsl:otherwise>								
								<td >
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
