<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <xsl:variable name="colNum" select="count(//tr[1]/td)"/> 
  <xsl:variable name="foreignCur" select="//tr[1]/td[3]"/> 
  <root>
    <colgroup>
     	<col style = 'width:40'/>
    	<col style = 'width:40'/>
    	<col style = 'width:120'/>
        <xsl:for-each select="/root/tbody/tr[1]/td">
          <xsl:if test="position() &gt; 4  and position() &lt;= $colNum - 12 - $foreignCur">
		    <col style = 'width:120'/>
          </xsl:if>  
        </xsl:for-each>    	
    	<col style = 'width:120'/>
    	<col style = 'width:120'/>
	  	<xsl:if test="$foreignCur = 1 ">
	      	<col style = 'width:80'/>
	    </xsl:if> 
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
    	<col style = 'width:40'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/> 
    </colgroup>
    <thead>
    	<tr>
					<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<xsl:for-each select="/root/tbody/tr[1]/td">
			          <xsl:if test="position() &gt; 4  and position() &lt;= $colNum - 12 - $foreignCur">
					    <td style='display:none'/>
			          </xsl:if>  
			        </xsl:for-each> 
			        <xsl:if test="$foreignCur = 1 ">
				      	<td style='display:none'/>
				    </xsl:if> 
				</tr>
				<tr>
					<td style="fontsize:subtitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<xsl:for-each select="/root/tbody/tr[1]/td">
			          <xsl:if test="position() &gt; 4  and position() &lt;= $colNum - 12 - $foreignCur">
					    <td style='display:none'/>
			          </xsl:if>  
			        </xsl:for-each> 
			        <xsl:if test="$foreignCur = 1 ">
				      	<td style='display:none'/>
				    </xsl:if> 
				</tr>
		  <tr noWrap='true' class='mainHead'>
          <xsl:for-each select="/root/tbody/tr[1]/td">
            <xsl:if test="position() = 2 ">
			  <td nowrap='true' colspan="2"><xsl:value-of select="."/>年</td>
			  <td nowrap='true' style='display:none'/>
	      	  <td nowrap='true' rowspan="2" valign="center">凭证号</td>
            </xsl:if>
            <xsl:if test="position() &gt; 4  and position() &lt;= $colNum - 12 - $foreignCur">
			  <td nowrap='true' rowspan="2" valign="center"><xsl:value-of select="."/></td>
            </xsl:if>  
          </xsl:for-each>
	      	<td nowrap='true' rowspan="2" valign="center">科目</td>
	      	<td nowrap='true' rowspan="2" valign="center">摘要</td>
		  	<xsl:if test="$foreignCur = 1 ">
		      	<td nowrap='true' rowspan="2" valign="center">汇率</td>
		    </xsl:if> 
	      	<td nowrap='true' colspan="3" valign="center">收入</td>
	      	<td nowrap='true' style='display:none'/>
	      	<td nowrap='true' style='display:none'/>
	      	<td nowrap='true' colspan="3" valign="center">发出</td>
	      	<td nowrap='true' style='display:none'/>
	      	<td nowrap='true' style='display:none'/>
	      	<td nowrap='true' rowspan="2" valign="center">方向</td>
	      	<td nowrap='true' colspan="3" valign="center">结余</td>
	      	<td nowrap='true' style='display:none'/>
	      	<td nowrap='true' style='display:none'/>
	      </tr>
	      <tr noWrap='true' class='mainHead'>
	      	<td nowrap='true'>月</td>
	      	<td nowrap='true'>日</td>
	      	<td nowrap='true' style='display:none'/>
	        <xsl:for-each select="/root/tbody/tr[1]/td">
	          <xsl:if test="position() &gt; 4  and position() &lt;= $colNum - 12 - $foreignCur">
			    <td nowrap='true' style='display:none'/>
	          </xsl:if>  
	        </xsl:for-each>
	      	<td nowrap='true' style='display:none'/>
	      	<td nowrap='true' style='display:none'/>
		  	<xsl:if test="$foreignCur = 1 ">
		      	<td nowrap='true' style='display:none'/>
		    </xsl:if> 	      	
	      	<td nowrap='true'>数量</td>
	      	<td nowrap='true'>单价</td>		      	
	      	<td nowrap='true'>金额</td>
	      	<td nowrap='true'>数量</td>
	      	<td nowrap='true'>单价</td>		      	
	      	<td nowrap='true'>金额</td>
	      	<td nowrap='true' style='display:none'/>
	      	<td nowrap='true'>数量</td>
	      	<td nowrap='true'>单价</td>		      	
	      	<td nowrap='true'>金额</td>    			      	
	      </tr>     
    </thead>
    <tbody>
    <xsl:if test="$foreignCur != 1 ">
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position() &gt; 7 and position() &lt; 14">
		                	<td class="numberText" style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position() &gt; 14">
		                	<td class="numberText" style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>		                
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
    </xsl:if>
    
    <xsl:if test="$foreignCur = 1 ">
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position() &gt; 8 and position() &lt; 15">
		                	<td class="numberText" style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position() &gt; 15">
		                	<td class="numberText" style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>		                
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
    </xsl:if>    
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



