<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">    
    <thead>
	  <xsl:variable name="colNum" select="count(//tr[1]/td)"/> 
	  <xsl:variable name="foreignCur" select="//tr[1]/td[3]"/> 
		  <tr noWrap='true' class='mainHead'>
          <xsl:for-each select="/root/tbody/tr[1]/td">
            <xsl:if test="position() = 2 ">
			  <th nowrap='true' colspan="2"><xsl:value-of select="."/>年</th>
	      	  <th nowrap='true' rowspan="2" valign="center">凭证号</th>
            </xsl:if>
            <xsl:if test="position() &gt; 4  and position() &lt;= $colNum - 12 - $foreignCur">
			  <th nowrap='true' rowspan="2" valign="center"><xsl:value-of select="."/></th>
            </xsl:if>  
          </xsl:for-each>
	      	<th nowrap='true' rowspan="2" valign="center">科目</th>
	      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
		  	<xsl:if test="$foreignCur = 1 ">
		      	<th nowrap='true' rowspan="2" valign="center">汇率</th>
		    </xsl:if> 
	      	<th nowrap='true' colspan="3" valign="center">收入</th>
	      	<th nowrap='true' colspan="3" valign="center">发出</th>
	      	<th nowrap='true' rowspan="2" valign="center">方向</th>
	      	<th nowrap='true' colspan="3" valign="center">结余</th>	          
	      </tr>
	      <tr noWrap='true' class='mainHead'>
	      	<th nowrap='true'>月</th>
	      	<th nowrap='true'>日</th>
	      	<th nowrap='true'>数量</th>
	      	<th nowrap='true'>单价</th>		      	
	      	<th nowrap='true'>金额</th>
	      	<th nowrap='true'>数量</th>
	      	<th nowrap='true'>单价</th>		      	
	      	<th nowrap='true'>金额</th>
	      	<th nowrap='true'>数量</th>
	      	<th nowrap='true'>单价</th>		      	
	      	<th nowrap='true'>金额</th>    			      	
	      </tr>
    </thead>
    <tbody>
    <xsl:if test="$foreignCur != 1 ">
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=3">
			                <td><a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="../td[4]"/>
			  	          	</a></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position() &gt; 7 and position() &lt; 14">
		                	<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position() &gt; 14">
		                	<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>		                
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
    </xsl:if>
    
    <xsl:if test="$foreignCur = 1 ">
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=3">
			                <td><a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="../td[4]"/>
			  	          	</a></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position() &gt; 8 and position() &lt; 15">
		                	<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position() &gt; 15">
		                	<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>		                
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
    </xsl:if>    
    </tbody>
  </xsl:template>
</xsl:stylesheet>