<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
    <colgroup>
     	<col style = 'width:40'/>
    	<col style = 'width:40'/>
    	<col style = 'width:120'/>
    	<col style = 'width:120'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
    	<col style = 'width:40'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/> 
    </colgroup>
    <thead>
    	<xsl:for-each select="/root/tbody/tr[1]/td">
            <xsl:if test="position() = 2 ">
            		<tr>
					<td style="fontsize:maintitle;colspan:12"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
				<tr>
					<td style="fontsize:subtitle;colspan:12"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
			      <tr noWrap='true' class='mainHead'>
			      	<td nowrap='true' colspan="2"><xsl:value-of select="."/>年</td>
			      	<td nowrap='true' style='display:none'></td>
			      	<td nowrap='true' rowspan="2" valign="center">会计科目</td>
			      	<td nowrap='true' rowspan="2" valign="center">摘要</td>
			      	<td nowrap='true' colspan="2">收入</td>
			      	<td nowrap='true' style='display:none'></td>
			      	<td nowrap='true' colspan="2">发出</td>
			      	<td nowrap='true' style='display:none'></td>
			      	<td nowrap='true' rowspan="2" valign="center">方向</td>
			      	<td nowrap='true' colspan="3">余额</td>
			      	<td nowrap='true' style='display:none'></td>
			      	<td nowrap='true' style='display:none'></td>
			      </tr>
			      <tr noWrap='true' class='mainHead'>
			      	<td nowrap='true'>月</td>
			      	<td nowrap='true'>日</td>
			      	<td nowrap='true' style='display:none'></td>
			      	<td nowrap='true' style='display:none'></td>
			      	<td nowrap='true'>数量</td>
			      	<td nowrap='true'>金额</td>
			      	<td nowrap='true'>数量</td>
			      	<td nowrap='true'>金额</td>
			      	<td nowrap='true' style='display:none'></td>
			      	<td nowrap='true'>数量</td>
			      	<td nowrap='true'>单价</td>
			      	<td nowrap='true'>金额</td>
			      </tr>
            </xsl:if>  
    	</xsl:for-each>      
    </thead>
    <tbody>
        <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position() &gt; 4 and position() &lt; 9">
    			            <td class="numberText" style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position() &gt; 9">
    			            <td class="numberText" style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



