<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:for-each select="/root/tbody/tr[1]/td">
            <xsl:if test="position() = 2 ">
			      <tr noWrap='true' class='mainHead'>
			      	<th nowrap='true' colspan="2"><xsl:value-of select="."/>年</th>
			      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
			      	<th nowrap='true' colspan="2">收入</th>
			      	<th nowrap='true' colspan="2">发出</th>
			      	<th nowrap='true' rowspan="2" valign="center">方向</th>
			      	<th nowrap='true' colspan="3">结存</th>
			      </tr>
			      <tr noWrap='true' class='mainHead'>
			      	<th nowrap='true'>月</th>
			      	<th nowrap='true'>日</th>
			      	<th nowrap='true'>数量</th>
			      	<th nowrap='true'>金额</th>
			      	<th nowrap='true'>数量</th>
			      	<th nowrap='true'>金额</th>
			      	<th nowrap='true'>数量</th>
			      	<th nowrap='true'>单价</th>
			      	<th nowrap='true'>金额</th>
			      </tr>
            </xsl:if>  
    	</xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
     		 <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position() &gt; 3 and position() &lt; 8">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>		                
		                <xsl:when test="position() &gt; 8">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>		                
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
	  	  	</tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
