<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
	    <xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)"/>    
    	<xsl:for-each select="/root/tbody/tr[1]/td">
            <xsl:if test="position() = 2 ">
			      <tr noWrap='true' class='mainHead'>
			      	<th nowrap='true' colspan="2"><xsl:value-of select="."/>年</th>
			      	<th nowrap='true' rowspan="2" valign="center">凭证号</th>
			      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
  		            <xsl:if test="$colNum = 16 ">
				      	<th nowrap='true' rowspan="2" valign="center">汇率</th>
		            </xsl:if>  
			      	<th nowrap='true' colspan="3">收入</th>
			      	<th nowrap='true' colspan="3">发出</th>
			      	<th nowrap='true' rowspan="2" valign="center">方向</th>
			      	<th nowrap='true' colspan="3">结存</th>
			      </tr>
			      <tr noWrap='true' class='mainHead'>
			      	<th nowrap='true'>月</th>
			      	<th nowrap='true'>日</th>
			      	<th nowrap='true'>数量</th>
			      	<th nowrap='true'>单价</th>
			      	<th nowrap='true'>金额</th>
			      	<th nowrap='true'>数量</th>
			      	<th nowrap='true'>单价</th>
			      	<th nowrap='true'>金额</th>
			      	<th nowrap='true'>数量</th>
			      	<th nowrap='true'>单价</th>
			      	<th nowrap='true'>金额</th>
			      </tr>
            </xsl:if>  
    	</xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
     		 <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=3">
			                <td><a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="../td[4]"/>
			  	          	</a></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position() &gt; 5 and position() &lt; $colNum - 3">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>	
		                <xsl:when test="position() &gt; $colNum - 3">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>			                
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
	  	  	</tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
