<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td[.!=''])"/>
  <root>
    <thead>
    	<tr noWrap='true'>
	    	<td style="fontsize:maintitle">
					<xsl:attribute name="colspan">
						<xsl:value-of select=" $colNum -9   "/>
					</xsl:attribute>
				</td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum - 10"/>    
        </xsl:call-template>
  		</tr>
  		<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test=" position() &lt; 2 ">
	    			<tr noWrap='true' class='mainHead'>
		          <xsl:for-each select="td">
		          	<xsl:variable name="colIndex" select="position()"/>
		          	<xsl:choose>
		          		<xsl:when test=" position()=1 or position()=3 or position()=5 or position()=7 or position()=9 or position()=11 or position()=13 or position()=15 or position()=18 ">
		          		</xsl:when>
		          		<xsl:when test=" position()=2  ">
		          			<td>
				            	<xsl:value-of select="."/>
		          			</td>
		          		</xsl:when>
		          		<xsl:when test="  position()=4 or position()=6 or position()=8 or position()=10 or position()=12 or position()=14 or position()=16 ">
		          			<xsl:if test=" ../td[ $colIndex - 1 ]!='' ">
		          			<td>
				            	<xsl:value-of select="."/>
		          			</td>
		          			</xsl:if>
		          		</xsl:when>
			          	<xsl:when test=" position() > 18 ">
			          		<xsl:if test=" . !='' ">
			          			<td>
					            	<xsl:value-of select="."/>
			          			</td>
		          			</xsl:if>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
	          	<xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test=" position()=17 ">
			          		<td>
						          <xsl:value-of select="."/>
				          	</td>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test=" position()>1 ">
	    			<tr>
		          <xsl:for-each select="td">
		          	<xsl:variable name="colIndex1" select="position()"/>
		          	<xsl:choose>
		          		<xsl:when test=" position()=1 or position()=3 or position()=5 or position()=7 or position()=9 or position()=11 or position()=13 or position()=15 or position()=18 ">
			            </xsl:when>
			            <xsl:when test=" position()=2 ">
			            	<td width="80"><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test="  position()=4 or position()=6 or position()=8 or position()=10 or position()=12 or position()=14 or position()=16  ">
			            	<xsl:if test=" ../../tr[1]/td[ $colIndex1 - 1 ]!=''  ">
			          			<td width="80">
					            	<xsl:value-of select="."/>
			          			</td>
		          			</xsl:if>
		          			<xsl:if test=" ../../tr[1]/td[ $colIndex1 - 1 ]=''  ">
		          			</xsl:if>
			            </xsl:when>
			            <xsl:when test="position() >18 ">
			            	<xsl:if test=" ../../tr[1]/td[ $colIndex1  ] !='' ">
			          			<xsl:if test=".='0.00' or .='NULL' or .='' " >
												<td align="right" ></td>
											</xsl:if>
											<xsl:if test=".!='0.00' and .!='NULL' and .!='' " >
				            		<td align="right" ><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				            	</xsl:if>
		          			</xsl:if>
			            	<xsl:if test=" ../../tr[1]/td[ $colIndex1  ] !=''">
		          			</xsl:if>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		<xsl:when test=" position()=17 ">
			            	<xsl:if test=".='0.00' or .='NULL' or .='' " >
											<td align="right" ></td>
										</xsl:if>
										<xsl:if test=".!='0.00' and .!='NULL' and .!='' " >
			            		<td align="right" ><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			            	</xsl:if>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
 		</tbody>
  </root>
  </xsl:template>
  <xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>



