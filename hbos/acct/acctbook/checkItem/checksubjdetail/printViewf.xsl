<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <xsl:variable name="colNum" select="count(//tr[1]/td)-10"/>
  <root>
    <thead>
    	<tr>
			<td style="fontsize:maintitle">
				<xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)-2"/></xsl:attribute>
			</td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			 <xsl:for-each select="//tr[1]/td">
	        <xsl:if test="position() &gt; 5  and position() &lt;= $colNum">
			  <td style='display:none'/>
	        </xsl:if>  
	      </xsl:for-each>
		</tr>
		<tr>
			<td style="fontsize:subtitle">
				<xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)-2"/></xsl:attribute>
			</td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<xsl:for-each select="//tr[1]/td">
	        <xsl:if test="position() &gt; 5  and position() &lt;= $colNum">
			  <td style='display:none'/>
	        </xsl:if>  
	      </xsl:for-each>
		</tr>
      <tr noWrap='true' class='mainHead'>
       	<xsl:for-each select="/root/tbody/tr[1]/td">
	        <xsl:if test="position() = 3">
		      	<td nowrap='true' colspan="2"><xsl:value-of select="."/>年</td>
		      	<td nowrap='true' style='display:none'/>
				  	<td nowrap='true' rowspan="2" valign="center">凭证号</td>		
	        </xsl:if>  
	        <xsl:if test="position() &gt; 5  and position() &lt;= $colNum">
			  		<td nowrap='true' rowspan="2" valign="center"><xsl:value-of select="."/></td>
	        </xsl:if>  
    	</xsl:for-each>			      	
      	<td nowrap='true' rowspan="2" valign="center">科目</td>
      	<td nowrap='true' rowspan="2" valign="center">摘要</td>
      	<td nowrap='true' rowspan="2" valign="center">汇率</td>
      	<td nowrap='true' colspan="2">借方</td>
      	<td nowrap='true' style='display:none'/>
      	<td nowrap='true' colspan="2">贷方</td>
      	<td nowrap='true' style='display:none'/>
      	<td nowrap='true' rowspan="2" valign="center">方向</td>
      	<td nowrap='true' colspan="2">余额</td>
      	<td nowrap='true' style='display:none'/>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<td nowrap='true'>月</td>
      	<td nowrap='true'>日</td>
      	<td nowrap='true' style='display:none'/>
       	<xsl:for-each select="/root/tbody/tr[1]/td">
	        <xsl:if test="position() &gt; 5  and position() &lt;= $colNum">
			  		<td nowrap='true' style='display:none'/>
	        </xsl:if>  
    	</xsl:for-each>				      	
	    	<td nowrap='true' style='display:none'/>
	    	<td nowrap='true' style='display:none'/>
	    	<td nowrap='true' style='display:none'/>
      	<td nowrap='true'>原币</td>
      	<td nowrap='true'>本币</td>
      	<td nowrap='true'>原币</td>
      	<td nowrap='true'>本币</td>
      	<td nowrap='true' style='display:none'/>
      	<td nowrap='true'>原币</td>
      	<td nowrap='true'>本币</td>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=4">
			                <td><a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="../td[5]"/>
			  	          	</a></td>
		                </xsl:when>
		                <xsl:when test="position()=5 or position()=1">
		                </xsl:when>
		                <xsl:when test="position()=$colNum+3">
		                  <td align="right"><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:when test="position()=$colNum+4 or position()=$colNum+5 or position()=$colNum+6 or position()=$colNum+7">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>	
		                <xsl:when test="position()=$colNum+9">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[$colNum + 8]='平'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:when test="position()=$colNum+10">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[$colNum + 8]='平'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		             
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



