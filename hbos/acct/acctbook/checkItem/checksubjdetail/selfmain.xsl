<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
       	<xsl:variable name="colNum" select="count(//tr[1]/td)-6"/>   
		  <tr noWrap='true' class='mainHead'>
			<xsl:for-each select="/root/tbody/tr[1]/td">
	        <xsl:if test="position() = 3 ">
				  <th nowrap='true' colspan="3">期间</th>
				  <th nowrap='true' rowspan="2" valign="center">凭证号</th>		  
	            </xsl:if>
	            <xsl:if test="position() &gt; 5  and position() &lt;= $colNum">
				  <th nowrap='true' rowspan="2" valign="center"><xsl:value-of select="."/></th>
	            </xsl:if>  
            </xsl:for-each>
          <th nowrap='true' rowspan="2" valign="center">科目</th>
	      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
	      	<th nowrap='true' rowspan="2" valign="center">借方</th>
	      	<th nowrap='true' rowspan="2" valign="center">贷方</th>
	      	<th nowrap='true' rowspan="2" valign="center">方向</th>
	      	<th nowrap='true' rowspan="2" valign="center">余额</th>	          
	      </tr>
	      <tr noWrap='true' class='mainHead'>
	      	<th nowrap='true'>年</th>
	      	<th nowrap='true'>月</th>
	      	<th nowrap='true'>日</th>
	      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=4">
			                <td><a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="../td[5]"/>
			  	          	</a></td>
		                </xsl:when>
		                <xsl:when test="position()=5">
		                </xsl:when>
		                <xsl:when test="position()=$colNum+3 or position()=$colNum+4">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>	
		                <xsl:when test="position()=$colNum+6">
		                		<xsl:choose>	
			                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[$colNum + 5]='平'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>	
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
