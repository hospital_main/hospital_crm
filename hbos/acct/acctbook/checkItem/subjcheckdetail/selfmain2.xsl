<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    </thead>
    <tbody>
    <xsl:variable name="colNum" select="count(//tr[1]/td)-5"/> 
      <xsl:for-each select="/root/tbody/tr">

		       <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=3">
			                <td><a href='#'>
			                  <xsl:attribute name="onclick" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');return false;
			  	          	  </xsl:attribute><xsl:value-of select="../td[4]"/>
			  	          	</a></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=$colNum+1 or position()=$colNum+2">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=$colNum+4">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[$colNum + 3]='ƽ'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:when test="position()= $colNum + 5">
		                    
		                </xsl:when>
		                <xsl:when test="position()=2">
			                <td ><xsl:value-of select="substring-before(../td[1],'-')"/></td>
			                <td ><xsl:value-of select="substring-after(../td[1],'-')"/></td>
			                <td ><xsl:value-of select="substring-after(substring-after(../td[2],'-'),'-')"/></td>
		                </xsl:when>		   
		                <xsl:when test="position()=1">
		                    
		                </xsl:when>             
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>

      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

