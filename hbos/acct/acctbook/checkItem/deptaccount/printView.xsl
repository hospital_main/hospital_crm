<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
    <thead>
    	<tr>
			<td style="fontsize:maintitle;colspan:6"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
		<tr>
			<td style="fontsize:subtitle;colspan:6"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
		<tr noWrap='true' class='mainHead'>
			<td>部门编码</td>
			<td>部门名称</td>
			<td>期初余额</td>
			<td>借方发生</td>
			<td>贷方发生</td>
			<td>余额</td>
			</tr>
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() = 1 or position() = 2">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position() = 3 or position() = 6">
								<td align="right" ><xsl:value-of select="format-number(.,'#,##0.00' )" /></td>
							</xsl:when>
							<xsl:when test="position() = 4" >
								<td align="right" >
									<xsl:if test="../td[1]!='合计'">
										<a href="#">
											<xsl:attribute name="onclick">
												javascript:openDialog('detail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;flag&gt;05&lt;/flag&gt;','dialogWidth:850px;dialogHeight:650px',result)
												</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00' )" />
										</a>
									</xsl:if>
									<xsl:if test="../td[1] = '合计' ">
										<xsl:value-of select="format-number(.,'#,##0.00' )" />
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position() = 5">
								<td align="right" >
									<xsl:if test="../td[1]!='合计'">
										<a href="#">
											<xsl:attribute name="onclick">
												javascript:openDialog('detail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;flag&gt;04&lt;/flag&gt;','dialogWidth:850px;dialogHeight:650px',result)
												</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00' )" />
										</a>
									</xsl:if>
									<xsl:if test="../td[1] = '合计' ">
										<xsl:value-of select="format-number(.,'#,##0.00' )" />
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td align="right" ><xsl:value-of select="." /></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



