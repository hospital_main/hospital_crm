<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap='true' class='mainHead'>
			<th>日期</th>
			<th>凭证类型</th>
			<th>凭证号</th>
			<th>摘要</th>
			<th>金额</th>
		</tr>           
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position() = 3 ">
							<td>
								<a tabindex='-1' href='#' >
                  <xsl:attribute name="onclick" >
    	            javascript:openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;");
  	          	  </xsl:attribute><xsl:value-of select="."/>
  	          	</a>
							</td>
						</xsl:when>
						<xsl:when test="position() = 5 ">
							<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="." /></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

