<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
	    <xsl:variable name="colNum" select="count(//tr[1]/td)-5"/> 
    	
				      <tr noWrap='true' class='mainHead'>
				      	<th nowrap='true' colspan="2">期间</th>
				      	<th nowrap='true' rowspan="2" valign="center">会计科目</th>
				      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
				      	<th nowrap='true' rowspan="2" valign="center">借方</th>
				      	<th nowrap='true' rowspan="2" valign="center">贷方</th>
				      	<th nowrap='true' rowspan="2" valign="center">方向</th>
				      	<th nowrap='true' rowspan="2" valign="center">余额</th>
				      </tr>
				      <tr noWrap='true' class='mainHead'>
				      	<th nowrap='true'>年</th>
				      	<th nowrap='true'>月</th>
				      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 0">
      	<xsl:variable name="rowNum" select="position()"/>   
     		 <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="(position()=$colNum+2 or position()=$colNum+3) and $rowNum>1">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="(position()=$colNum+2 or position()=$colNum+3) and $rowNum=1">
    			            <td class="numberText"></td>
		                </xsl:when>
		                <xsl:when test="position()=$colNum+5">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[$colNum+4]='平'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
	  	  	</tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
