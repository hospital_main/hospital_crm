<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
    	<xsl:variable name="cell10" select="//tr[1]/td[10]"/>    	
    <thead>
    	
    	
		<!--第一行第10个单元个没有数据，则说明是外币，否则是本币-->    	
    	<xsl:if test=" $cell10 != '' ">
    		<xsl:variable name="colPosition" select="9"/>
    		<tr noWrap='true' class='mainHead'>
	    		<xsl:for-each select="/root/tbody/tr[1]/td">
			    	<xsl:choose>
			            <xsl:when test="position()=2">
					      	<th nowrap='true' colspan="2"><xsl:value-of select="."/>年</th>
					      	<th nowrap='true' style="display:none"></th>
					      	<th nowrap='true' rowspan="2" valign="center">凭证号</th>
					      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
					      	<th nowrap='true' rowspan="2" valign="center">借方</th>
					      	<th nowrap='true' rowspan="2" valign="center">贷方</th>
					      	<th nowrap='true' rowspan="2" valign="center">方向</th>
					      	<th nowrap='true' rowspan="2" valign="center">余额</th>
			            </xsl:when>
			            <xsl:when test="position() &gt; $colPosition and position() mod 2 = 0 ">
	  		              <th nowrap='true' colspan="2"><xsl:value-of select="."/></th>
			            </xsl:when>
			        </xsl:choose>
			    </xsl:for-each>
        	</tr>
		    <tr noWrap='true' class='mainHead'>
				<th nowrap='true'>月</th>
				<th nowrap='true'>日</th>	
				<th nowrap='true' style="display:none"></th>
				<th nowrap='true' style="display:none"></th>
				<th nowrap='true' style="display:none"></th>
				<th nowrap='true' style="display:none"></th>
				<th nowrap='true' style="display:none"></th>
				<th nowrap='true' style="display:none"></th>	    
	        	<xsl:for-each select="/root/tbody/tr[2]/td">
			        <xsl:if test="position() &gt; $colPosition ">
						<th nowrap='true'><xsl:value-of select="."/><xsl:if test=". = ''">　</xsl:if></th>
			        </xsl:if>  
			    </xsl:for-each>
	        </tr>
    	</xsl:if>

    	<xsl:if test=" $cell10 = '' ">
    		<xsl:variable name="colPosition2" select="10"/>
    		<tr noWrap='true' class='mainHead'>
	    		<xsl:for-each select="/root/tbody/tr[1]/td">
			    	<xsl:choose>
			            <xsl:when test="position()=2">
					      	<th nowrap='true' colspan="2"><xsl:value-of select="."/>年</th>
					      	<th nowrap='true' rowspan="2" valign="center">凭证号</th>
					      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
					      	<th nowrap='true' rowspan="2" valign="center">汇率</th>
					      	<th nowrap='true' rowspan="2" valign="center">借方</th>
					      	<th nowrap='true' rowspan="2" valign="center">贷方</th>
					      	<th nowrap='true' rowspan="2" valign="center">方向</th>
					      	<th nowrap='true' rowspan="2" valign="center">余额</th>
			            </xsl:when>
			            <xsl:when test="position() &gt; $colPosition2 and position() mod 2 = 1 ">
	  		              <th nowrap='true' colspan="2"><xsl:value-of select="."/><xsl:if test=". = ''">　</xsl:if></th>
			            </xsl:when>
			        </xsl:choose>
			    </xsl:for-each>
        	</tr>
		    <tr noWrap='true' class='mainHead'>
				<th nowrap='true'>月</th>
				<th nowrap='true'>日</th>		    
	        	<xsl:for-each select="/root/tbody/tr[2]/td">
			        <xsl:if test="position() &gt; $colPosition2 ">
						<th nowrap='true'><xsl:value-of select="."/><xsl:if test=". = ''">　</xsl:if></th>
			        </xsl:if>  
			    </xsl:for-each>
	        </tr>
    	</xsl:if>
    </thead>
    <tbody>
    <xsl:if test=" $cell10 != '' ">
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 2">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=3">
			                <td><a tabindex='-1' href="#">
			                  <xsl:attribute name="onclick" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="../td[4]"/></a><xsl:if test="../td[4] = ''"></xsl:if></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=6 or position()=7 ">
    			            <td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=9">
	                	<xsl:choose>	
		                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[8]='平'">
		                		<td align='right'>Q</td>
    			          	</xsl:when>
			                <xsl:otherwise>
		                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			                </xsl:otherwise>
		             		 </xsl:choose>
		             		 </xsl:when>	
		                <xsl:when test="position() &gt; 9">
    			            <td class="numberText" align="right"><xsl:value-of select="."/><xsl:if test=". = ''">　</xsl:if></td>
		                </xsl:when>	
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/><xsl:if test=". = ''">　</xsl:if></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>	  	 
      </xsl:for-each>
	</xsl:if>
	
    <xsl:if test=" $cell10 = '' ">
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 2">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=3">
			                <td><a tabindex='-1' href="#">
			                  <xsl:attribute name="onclick" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="../td[4]"/></a><xsl:if test="../td[4] = ''"></xsl:if></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=6 or position()=7 or position()=8 or position()=10">
    			            <td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=10">
	                	<xsl:choose>	
		                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[9]='平'">
		                		<td align='right'>Q</td>
    			          	</xsl:when>
			                <xsl:otherwise>
		                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			                </xsl:otherwise>
		             		 </xsl:choose>
		             		 </xsl:when>	
		                <xsl:when test="position()&gt; 10">
    			            <td class="numberText" align="right"><xsl:value-of select="."/><xsl:if test=". = ''">　</xsl:if></td>
		                </xsl:when>	
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/><xsl:if test=". = ''">　</xsl:if></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>	  	 
      </xsl:for-each>
	</xsl:if>	
    </tbody>
  </xsl:template>
</xsl:stylesheet>

