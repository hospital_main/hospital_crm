<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <xsl:variable name="colNum" select="count(//tr[1]/td)"/> 
  <root>
    <colgroup>
     	<col style = 'width:40'/>
    	<col style = 'width:40'/>
    	<col style = 'width:120'/>
    	<col style = 'width:120'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
    	<col style = 'width:40'/>
    	<col style = 'width:80'/> 
      <xsl:for-each select="//tr[1]/td">
        <xsl:if test="position() &gt; 9  and position() &lt;= $colNum">
		  <col style = 'width:80'/>
        </xsl:if>  
      </xsl:for-each>
    </colgroup>
    <thead>    	  
    	<tr>
			<td style="fontsize:maintitle">
				<xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute>
			</td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<xsl:for-each select="//tr[1]/td">
		        <xsl:if test="position() &gt; 9  and position() &lt;= $colNum">
				  <td style='display:none'/>
		        </xsl:if>  
		      </xsl:for-each>
		</tr>
		<tr>
			<td style="fontsize:subtitle">
				<xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute>
			</td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<xsl:for-each select="//tr[1]/td">
		        <xsl:if test="position() &gt; 9  and position() &lt;= $colNum">
				  <td style='display:none'/>
		        </xsl:if>  
		      </xsl:for-each>
		</tr>
		<xsl:variable name="colPosition" select="9"/>
		<tr noWrap='true' class='mainHead'>
    		<xsl:for-each select="/root/tbody/tr[1]/td">
		    	<xsl:choose>
		            <xsl:when test="position()=2">
				      	<td nowrap='true' colspan="2"><xsl:value-of select="."/>年</td>
				      	<td nowrap='true' style='display:none'></td>
				      	<td nowrap='true' rowspan="2" valign="center">凭证号</td>
				      	<td nowrap='true' rowspan="2" valign="center">摘要</td>
				      	<td nowrap='true' rowspan="2" valign="center">借方</td>
				      	<td nowrap='true' rowspan="2" valign="center">贷方</td>
				      	<td nowrap='true' rowspan="2" valign="center">方向</td>
				      	<td nowrap='true' rowspan="2" valign="center">余额</td>
		            </xsl:when>
		            <xsl:when test="position() &gt; $colPosition and position() mod 2 = 0 ">
  		              <td nowrap='true' colspan="2"><xsl:value-of select="."/></td>
  		              <td nowrap='true' style='display:none'></td>
		            </xsl:when>
		        </xsl:choose>
		    </xsl:for-each>
    	</tr>
	    <tr noWrap='true' class='mainHead'>
			<td nowrap='true'>月</td>
			<td nowrap='true'>日</td>
			<td nowrap='true' style='display:none'></td>
			<td nowrap='true' style='display:none'></td>
			<td nowrap='true' style='display:none'></td>
			<td nowrap='true' style='display:none'></td>
			<td nowrap='true' style='display:none'></td>
			<td nowrap='true' style='display:none'></td>
        	<xsl:for-each select="/root/tbody/tr[2]/td">
		        <xsl:if test="position() &gt; $colPosition ">
					<td nowrap='true'><xsl:value-of select="."/></td>
		        </xsl:if>  
		    </xsl:for-each>
        </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 2">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=3">
		                </xsl:when>
		                <xsl:when test="position()=6 or position()=7 or position()=9">
    			            <td style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>	
		                <xsl:when test="position()&gt; 9">
    			            <td style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>	
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



