<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <xsl:variable name="colNum" select="count(//tr[1]/td)-5"/> 
  <root>
    <thead> 
    	<tr>
			<td style="fontsize:maintitle">
				<xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute>
			</td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<xsl:for-each select="/root/tbody/tr[1]/td">
		     	<xsl:if test="position() &gt; 2 and position() &lt;= $colNum">
			    	<td style='display:none'/>
	        	</xsl:if>
		    </xsl:for-each>
		</tr>
		<tr>
			<td style="fontsize:subtitle">
				<xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute>
			</td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<xsl:for-each select="/root/tbody/tr[1]/td">
		     	<xsl:if test="position() &gt; 2 and position() &lt;= $colNum">
			    	<td style='display:none'/>
	        	</xsl:if>
		    </xsl:for-each>
		</tr>
	  <tr noWrap='true' class='mainHead'>
      <xsl:for-each select="/root/tbody/tr[1]/td">
        <xsl:if test="position() = 1 ">
		 		 <td nowrap='true' colspan="2">xxxx期间</td>
		  	 <td nowrap='true' style='display:none'></td>
        </xsl:if>
        <xsl:if test="position() &gt; 2  and position() &lt;= $colNum">
		  <td nowrap='true' rowspan="2" valign="center"><xsl:value-of select="."/></td>
        </xsl:if>  
      </xsl:for-each>
      	<td nowrap='true' rowspan="2" valign="center">摘要</td>
      	<td nowrap='true' rowspan="2" valign="center">借方</td>
      	<td nowrap='true' rowspan="2" valign="center">贷方</td>
      	<td nowrap='true' rowspan="2" valign="center">方向</td>
      	<td nowrap='true' rowspan="2" valign="center">余额</td>	          
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<td nowrap='true'>月</td>
      	<td nowrap='true'>日</td>
      	<xsl:for-each select="/root/tbody/tr[1]/td">
	        <xsl:if test="position() &gt; 2  and position() &lt;= $colNum">
		      	<td nowrap='true' style='display:none'></td>
	        </xsl:if>        	
      	</xsl:for-each>
      	<td nowrap='true' style='display:none'></td>
      	<td nowrap='true' style='display:none'></td>
      	<td nowrap='true' style='display:none'></td>
      	<td nowrap='true' style='display:none'></td>
      	<td nowrap='true' style='display:none'></td>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
     		 <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>  
		                <xsl:when test="position()=1">  
		                  <td style="align:right;"><xsl:value-of select="../td[2]"/></td>
		                </xsl:when>       
		                <xsl:when test="position()=2">
		                	<td></td>  
		                </xsl:when> 
		                <xsl:when test="position()=$colNum+2 or position()=$colNum+3">
    			            <td style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=$colNum+5">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[$colNum+4]='平'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
	  	  	</tr>
	  	  </xsl:if>
      </xsl:for-each>	
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



