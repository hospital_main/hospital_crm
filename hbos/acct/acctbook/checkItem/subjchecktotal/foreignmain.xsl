<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="colNum" select="count(//tr[1]/td)-8"/>    
    	<xsl:for-each select="/root/tbody/tr">
      		<xsl:if test="position() =1 ">
			  <tr noWrap='true' class='mainHead'>
	          <xsl:for-each select="td">
	            <xsl:if test="position() = 1 ">
				      	<th nowrap='true' colspan="2">日期</th>
	            </xsl:if>
	            <xsl:if test="position() &gt; 2  and position() &lt;= $colNum">
				  <th nowrap='true' rowspan="2" valign="center"><xsl:value-of select="."/></th>
	            </xsl:if>  
	          </xsl:for-each>
		      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
		      	<th nowrap='true' colspan="2">借方</th>
		      	<th nowrap='true' style='display:none'></th>
		      	<th nowrap='true' colspan="2">贷方</th>
		      	<th nowrap='true' style='display:none'></th>
		      	<th nowrap='true' rowspan="2" valign="center">方向</th>
		      	<th nowrap='true' colspan="2">余额</th>
		      	<th nowrap='true' style='display:none'></th>
		      </tr>
		      <tr noWrap='true' class='mainHead'>
		      	<th nowrap='true'>年</th>
		      	<th nowrap='true'>月</th>
		      	<th nowrap='true'>原币</th>
		      	<th nowrap='true'>本币</th>
		      	<th nowrap='true'>原币</th>
		      	<th nowrap='true'>本币</th>
		      	<th nowrap='true'>原币</th>
		      	<th nowrap='true'>本币</th>
		      </tr>
        	</xsl:if>
    	</xsl:for-each>
    </thead>
      <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=$colNum+2 or position()=$colNum+3">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=$colNum+4 or position()=$colNum+5">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=$colNum+7">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[$colNum+6]='平'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:when test="position()=$colNum+8">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[$colNum+6]='平'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
