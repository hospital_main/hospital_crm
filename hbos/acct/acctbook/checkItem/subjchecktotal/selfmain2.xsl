<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    </thead>
    <tbody>
    	<xsl:variable name="colNum" select="count(//tr[1]/td)-5"/>    
      <xsl:for-each select="/root/tbody/tr">
	       <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=$colNum+2 or position()=$colNum+3">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=$colNum+5">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[$colNum+4]='ƽ'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
	  	  	</tr>
	  	  
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

