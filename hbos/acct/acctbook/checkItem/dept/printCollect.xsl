<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
    <thead>
    	<tr>
    		<td style="fontsize:maintitle;colspan:colcount"></td>
		<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 3]">
			<td style='display:none'/>
		</xsl:for-each>
	</tr>
	<tr>
		<td style="fontsize:subtitle;colspan:9;colspan:colcount"></td>
		<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 3]">
			<td style='display:none'/>
		</xsl:for-each>
	</tr>
	<tr>
		<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 2]">
			<td style="fontsize:coltitle;">
				<xsl:value-of select="."/>
			</td>
		</xsl:for-each>
	</tr>
    </thead>
    <tbody>
		<xsl:for-each select="/root/tbody/tr[position() &gt; 1]">
			<tr>
				<xsl:for-each select="td[position() &gt; 2]">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td bgcolor="#FFDDA6">
								<xsl:value-of select="."/>
							</td>
				                </xsl:when>	                
				                <xsl:otherwise>
				                	<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



