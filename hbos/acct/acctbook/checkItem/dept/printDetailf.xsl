<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <xsl:variable name="colNum" select="count(//tr[1]/td)"/>
  <root>
    <thead>
    	<tr>
			<td style="fontsize:maintitle" colspan="13"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
		<tr>
			<td style="fontsize:subtitle;colspan:13"></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
		<tr noWrap='true' class='mainHead'>
				<td nowrap='true' colspan="2"><xsl:value-of select="/root/tbody/tr[1]/td[2]"/></td>
				<td style='display:none'></td>
				<td nowrap='true' rowspan="2">凭证号</td>
				<td nowrap='true' rowspan="2">摘要</td>
				<td nowrap='true' rowspan="2">科目</td>
				<td nowrap='true' rowspan="2">汇率</td>
				<td nowrap='true' colspan="2">借方</td>
				<td style='display:none'></td>
				<td nowrap='true' colspan="2">贷方</td>
				<td style='display:none'></td>
				<td nowrap='true' rowspan="2">方向</td>
				<td nowrap='true' colspan="2">余额</td>
				<td style='display:none'></td>
			</tr>
			<tr class='mainHead'>
				<td nowrap='true'>月</td>
				<td nowrap='true'>日</td>
				<td style='display:none'></td>
				<td style='display:none'></td>
				<td style='display:none'></td>
				<td style='display:none'></td>
				<td nowrap='true'>原币</td>
				<td nowrap='true'>本币</td>
				<td nowrap='true'>原币</td>
				<td nowrap='true'>本币</td>
				<td style='display:none'></td>
				<td nowrap='true'>原币</td>
				<td nowrap='true'>本币</td>
			</tr>
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position()!=1 and position()!=2]"> 
						<xsl:variable name="sss" select="../td[4]!='66' and ../td[4]!='99'"/>
						<xsl:choose>
							<xsl:when test="position()=2">
								<td><xsl:if test=".!='66' and .!='99'"><xsl:value-of select="."/></xsl:if></td>
							</xsl:when>
							<xsl:when test="position()=3">
								<td><a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="."/>
			  	          	</a></td>
							</xsl:when>
							<xsl:when test="position() &lt; 6">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=6 and ../td[1]!='1'">
								<td class="numberText" align="right"><xsl:if test="'true'=$sss"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if></td>
							</xsl:when>
							<xsl:when test="(position()=7 or position()=9) and ../td[1]!='1'">
								<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="(position()=12 ) and ../td[1]!='1'">
								<td class="numberText" align="right"><xsl:if test="'true'=$sss"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if></td>
							</xsl:when>
							<xsl:when test="position()=11">
								<td class="numberText" align="right"><xsl:if test="'true'=$sss"><xsl:value-of select="."/></xsl:if></td>
							</xsl:when>
							<xsl:when test="position()=8 or position()=10 or position()=11">
								<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=13">
								<td class="numberText" align="right"><xsl:if test="'true'=$sss"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if></td>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



