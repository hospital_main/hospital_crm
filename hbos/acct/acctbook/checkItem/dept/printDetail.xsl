<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <xsl:variable name="colNum" select="count(//tr[1]/td)"/>
  <xsl:variable name="list1" select="substring-before(/root/tbody/tr[1]/td[1],';')"/>  
  <xsl:variable name="list2" select="substring-after(/root/tbody/tr[1]/td[1],';')"/>  
  <root>
    <thead>
    	<xsl:if test="count(/root/tbody/tr)&gt;0">
    	<tr>
    	<xsl:if test="$list1='1' and $list2='1'">
  			<td style="fontsize:maintitle;"><xsl:attribute name="colspan">6</xsl:attribute></td>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
			</xsl:if>
    	<xsl:if test="$list1='1' and $list2='0'">
  			<td style="fontsize:maintitle;"><xsl:attribute name="colspan">9</xsl:attribute></td>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
			</xsl:if>
    	<xsl:if test="$list1='0' and $list2='1'">
  			<td style="fontsize:maintitle;"><xsl:attribute name="colspan">8</xsl:attribute></td>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
			</xsl:if>
    	<xsl:if test="$list1='0' and $list2='0'">
  			<td style="fontsize:maintitle;"><xsl:attribute name="colspan">13</xsl:attribute></td>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
			</xsl:if>

		</tr>
		<tr>
    	<xsl:if test="$list1='1' and $list2='1'">
  			<td style="fontsize:subtitle;"><xsl:attribute name="colspan">6</xsl:attribute></td>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
			</xsl:if>
    	<xsl:if test="$list1='1' and $list2='0'">
  			<td style="fontsize:subtitle;"><xsl:attribute name="colspan">9</xsl:attribute></td>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
			</xsl:if>
    	<xsl:if test="$list1='0' and $list2='1'">
  			<td style="fontsize:subtitle;"><xsl:attribute name="colspan">8</xsl:attribute></td>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
			</xsl:if>
    	<xsl:if test="$list1='0' and $list2='0'">
  			<td style="fontsize:subtitle;"><xsl:attribute name="colspan">13</xsl:attribute></td>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
  			<td style='display:none'/>
			</xsl:if>

		</tr>
		</xsl:if>
		<tr noWrap='true' class='mainHead'>
				<td nowrap='true' colspan="2"><xsl:value-of select="/root/tbody/tr[1]/td[2]"/></td>
				<td style='display:none'></td>
				<td nowrap='true' rowspan="2">凭证号</td>
				<td nowrap='true' rowspan="2">摘要</td>
				<td nowrap='true' rowspan="2">科目</td>
				<xsl:if test="$list1='0'">
  				<td nowrap='true' rowspan="2">汇率</td>
				</xsl:if>
				
				<td nowrap='true' rowspan="2">借方</td>
				<xsl:if test="$list1='0'">
    			<td style='display:none'></td>
    		</xsl:if>
    		
    		<xsl:if test="$list2='0'">
  				<td nowrap='true' rowspan="2">贷方</td>
  				<xsl:if test="$list1='0'">
  				  <td style='display:none'></td>
  				</xsl:if>
  				<td nowrap='true' rowspan="2">方向</td>
  				<td nowrap='true' rowspan="2">余额</td>
				<td style='display:none'></td>
				</xsl:if>
			</tr>
			<tr class='mainHead'>
				<td nowrap='true'>月 </td>
				<td nowrap='true'>日 </td>
				<td style='display:none'></td>
				<td style='display:none'></td>
				<td style='display:none'></td>
				<xsl:if test="substring-before(/root/tbody/tr[1]/td[1],';')='1'">
				  <xsl:if test="$list2='0'">
  					<td style='display:none'></td>
					  <td style='display:none'></td>
					</xsl:if>
				</xsl:if>
				
				<xsl:if test="substring-before(/root/tbody/tr[1]/td[1],';')!='1'">
					<td style='display:none'></td>
					<td nowrap='true'>原币</td>
					<td nowrap='true'>本币</td>
 					<td nowrap='true'>原币</td>
 					<td nowrap='true'>本币</td>
				</xsl:if>
				
				<xsl:if test="$list2='0'">
  				<xsl:if test="substring-before(/root/tbody/tr[1]/td[1],';')='1'">
  					<td style='display:none'></td>
  				</xsl:if>
  				<xsl:if test="substring-before(/root/tbody/tr[1]/td[1],';')!='1'">
  					<td nowrap='true'>原币</td>
  					<td nowrap='true'>本币</td>
  				</xsl:if>
				</xsl:if>

			</tr>
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position()!=1 and position()!=2]"> 
						<xsl:variable name="sss" select="../td[4]!='66' and ../td[4]!='99'"/>
						<xsl:choose>
							<xsl:when test="position()=2">
								<td><xsl:if test=".!='66' and .!='99'"><xsl:value-of select="."/></xsl:if></td>
							</xsl:when>
							<xsl:when test="position()=3">
								<td><a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="."/>
			  	          	</a></td>
							</xsl:when>
							<xsl:when test="position() &lt; 6">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=6 and substring-before(../td[1],';')!='1'">
								<td class="numberText" align="right"><xsl:if test="'true'=$sss"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if></td>
							</xsl:when>
							<xsl:when test="(position()=7 or position()=9) and substring-before(../td[1],';')!='1'">
								<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="(position()=12 ) and substring-before(../td[1],';')!='1'">
								<td class="numberText" align="right"><xsl:if test="'true'=$sss"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if></td>
							</xsl:when>
							<xsl:when test="position()=11 and substring-after(../td[1],';')!='1'">
								<td class="numberText" align="right"><xsl:if test="'true'=$sss"><xsl:value-of select="."/></xsl:if></td>
							</xsl:when>
							
							<xsl:when test="position()=8">
								<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							
							<xsl:when test="(position()=10 or position()=11) and substring-after(../td[1],';')!='1'">
								<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							
							<xsl:when test="position()=13 and substring-after(../td[1],';')!='1'"> 
								<td class="numberText" align="right"><xsl:if test="'true'=$sss"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if></td>
							</xsl:when>
							
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



