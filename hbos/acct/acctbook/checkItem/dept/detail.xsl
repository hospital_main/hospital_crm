<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' colspan="2"><xsl:value-of select="/root/tbody/tr[1]/td[2]"/></th>
				<th style='display:none'></th>
				<th nowrap='true' rowspan="2">凭证号</th>
				<th nowrap='true' rowspan="2">摘要</th>
				<th nowrap='true' rowspan="2">科目</th>
				<!--
				<xsl:if test="substring-before(/root/tbody/tr[1]/td[1],';')='1'">
				-->
					<th nowrap='true' rowspan="2">借方</th>
					<xsl:if test="substring-after(/root/tbody/tr[1]/td[1],';')!='1'">
  					<th nowrap='true' rowspan="2">贷方</th>
					</xsl:if>
				<!--
				</xsl:if>
				<xsl:if test="substring-before(/root/tbody/tr[1]/td[1],';')!='1'">
					<th nowrap='true' rowspan="2">汇率</th>
					<th nowrap='true' colspan="2">借方</th>
					<th style='display:none'></th>
					<xsl:if test="substring-after(/root/tbody/tr[1]/td[1],';')!='1'">
  					<th nowrap='true' colspan="2">贷方</th>
  					<th style='display:none'></th>
  				</xsl:if>
				</xsl:if>
				-->
				<xsl:if test="substring-after(/root/tbody/tr[1]/td[1],';')!='1'">
  				<th nowrap='true' rowspan="2">方向</th>
  				<!--
  				<xsl:if test="substring-before(/root/tbody/tr[1]/td[1],';')='1'">
  				-->
  					<th nowrap='true' rowspan="2">余额</th>
  				</xsl:if>
				<!--
  				<xsl:if test="substring-before(/root/tbody/tr[1]/td[1],';')!='1'">
  					<th nowrap='true' colspan="2">余额</th>
  					<th style='display:none'></th>
  				</xsl:if>
				</xsl:if>
				-->
			</tr>
			<tr class='mainHead'>
				<th nowrap='true'>月</th>
				<th nowrap='true'>日</th>
				<th style='display:none'></th>
				<th style='display:none'></th>
				<th style='display:none'></th>
				<xsl:if test="substring-before(/root/tbody/tr[1]/td[1],';')='1'">
					<th style='display:none'></th>
					<th style='display:none'></th>
				</xsl:if>
				<!--
				<xsl:if test="substring-before(/root/tbody/tr[1]/td[1],';')!='1'">
					<th style='display:none'></th>
					<th nowrap='true'>原币</th>
					<th nowrap='true'>本币</th>
 					<th nowrap='true'>原币</th>
 					<th nowrap='true'>本币</th>
				</xsl:if>
				<th style='display:none'></th>
				<xsl:if test="substring-before(/root/tbody/tr[1]/td[1],';')='1'">
					<th style='display:none'></th>
				</xsl:if>
				<xsl:if test="substring-before(/root/tbody/tr[1]/td[1],';')!='1'">
					<th nowrap='true'>原币</th>
					<th nowrap='true'>本币</th>
				</xsl:if>
				-->
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position()!=1 and position()!=2]"> 
						<xsl:variable name="sss" select="../td[4]!='66' and ../td[4]!='99'"/>
						<xsl:choose>
							<xsl:when test="position()=2">
								<td><xsl:if test=".!='66' and .!='99'"><xsl:value-of select="."/></xsl:if></td>
							</xsl:when>
							<xsl:when test="position()=3">
								<td><a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;');
			  	          	  </xsl:attribute><xsl:value-of select="."/>
			  	          	</a></td>
							</xsl:when>
							<xsl:when test="position() &lt; 6">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=6 and substring-before(../td[1],';')!='1'">
								<td class="numberText" align="right"><xsl:if test="'true'=$sss"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if></td>
							</xsl:when>
							<xsl:when test="(position()=7 or position()=9) and substring-before(../td[1],';')!='1'">
								<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="(position()=12 ) and substring-before(../td[1],';')!='1'">
								<td class="numberText" align="right"><xsl:if test="'true'=$sss"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if></td>
							</xsl:when>
							<xsl:when test="position()=11 and substring-after(../td[1],';')!='1'">
								<td class="numberText" align="right"><xsl:if test="'true'=$sss"><xsl:value-of select="."/></xsl:if></td>
							</xsl:when>
							
							<xsl:when test="position()=8">
								<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							
							<xsl:when test="(position()=10 or position()=11) and substring-after(../td[1],';')!='1'">
								<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							
							<xsl:when test="position()=13 and substring-after(../td[1],';')!='1'"> 
								<td class="numberText" align="right"><xsl:if test="'true'=$sss"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if></td>
							</xsl:when>
							
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>