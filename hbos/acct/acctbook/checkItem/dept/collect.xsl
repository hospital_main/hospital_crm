<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr class="mainHead">
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 2]">
				<td align="center">
					<xsl:value-of select="."/>
				</td>
			</xsl:for-each>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr[position() &gt; 1]">
			<tr>
				<xsl:for-each select="td[position() &gt; 2]">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td>
								<xsl:value-of select="."/>
							</td>
              </xsl:when>	                
              <xsl:otherwise>
              	<td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

