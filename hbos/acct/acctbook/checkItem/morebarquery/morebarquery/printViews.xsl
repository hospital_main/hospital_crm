<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
    <colgroup>
     	<col style = 'width:40'/>
     	<col style = 'width:40'/>
    	<col style = 'width:40'/>
    	<col style = 'width:120'/>
    	<col style = 'width:120'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
    	<col style = 'width:40'/>
    	<col style = 'width:80'/>
    	<col style = 'width:80'/>
	      <xsl:for-each select="//tr[1]/td">
	        <xsl:if test="position() &gt; 10">
			  <col style = 'width:120'/>
	        </xsl:if>  
	      </xsl:for-each>
    </colgroup>
    <thead>
    	<tr>
			<td style="fontsize:maintitle">
				<xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute>
			</td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<xsl:for-each select="//tr[1]/td">
	        <xsl:if test="position() &gt; 10">
			  <td style='display:none'/>
	        </xsl:if>  
	      </xsl:for-each>
		</tr>
		<tr>
			<td style="fontsize:subtitle">
				<xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute>
			</td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<xsl:for-each select="//tr[1]/td">
	        <xsl:if test="position() &gt; 10">
			  <td style='display:none'/>
	        </xsl:if>  
	      </xsl:for-each>
		</tr>
      <tr noWrap='true' class='mainHead'>
  		<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
		<xsl:variable name="idx2" select="/root/tbody/tr[1]/td[4]"/>    
		<xsl:variable name="idx3" select="/root/tbody/tr[1]/td[6]"/>        
    	<xsl:for-each select="/root/tbody/tr[1]/td">
          <xsl:choose>
            <xsl:when test="position()=2">
		      	<td nowrap='true' rowspan="2"><xsl:value-of select="."/>年</td>
		      	<td nowrap='true' rowspan="2"><xsl:value-of select="."/>月</td>
		      	<td nowrap='true' rowspan="2"><xsl:value-of select="."/>日</td>
		      	<td nowrap='true' rowspan="2" valign="center">凭证号</td>
		      	<td nowrap='true' rowspan="2" valign="center">摘要</td>
		      	<td nowrap='true' rowspan="2" valign="center">借方</td>
		      	<td nowrap='true' rowspan="2" valign="center">贷方</td>
		      	<td nowrap='true' rowspan="2" valign="center">方向</td>
		      	<td nowrap='true' rowspan="2" valign="center">余额</td>
            </xsl:when>
            <xsl:when test="position() = 4 ">
              <td nowrap='true'>
               	<xsl:attribute name="colspan" >
  			      <xsl:value-of select="."/>
  			    </xsl:attribute>借方分析
	          </td>
		      <xsl:for-each select="/root/tbody/tr[1]/td">
			       <xsl:if test="position() &gt; 11 and position() &lt;= 10+$idx2 ">
			       		<td nowrap='true' style='display:none'></td>
		           </xsl:if>  
		      </xsl:for-each>
            </xsl:when>
            <xsl:when test="position() = 6 ">
              <td nowrap='true'>
               	<xsl:attribute name="colspan" >
  			      <xsl:value-of select="."/>
  			    </xsl:attribute>贷方分析
  			  </td>
			  <xsl:for-each select="/root/tbody/tr[1]/td">
			       <xsl:if test="position() &gt; 11+$idx2 and position() &lt;= $colNum">
			       		<td nowrap='true' style='display:none'></td>
		           </xsl:if>  
		      </xsl:for-each>
            </xsl:when>
         </xsl:choose>	            
      </xsl:for-each>
      </tr>
      <tr noWrap='true' class='mainHead'>
          <xsl:for-each select="/root/tbody/tr[2]/td">
            <xsl:if test="position() = 1 ">
		      	<td nowrap='true' style='display:none'></td>
		      	<td nowrap='true' style='display:none'></td>
		      	<td nowrap='true' style='display:none'></td>
		      	<td nowrap='true' style='display:none'></td>
		      	<td nowrap='true' style='display:none'></td>
		      	<td nowrap='true' style='display:none'></td>
		      	<td nowrap='true' style='display:none'></td>
		      	<td nowrap='true' style='display:none'></td>
		      	<td nowrap='true' style='display:none'></td>		      	
            </xsl:if>  
            <xsl:if test="position() &gt; 10 ">
		      	<td nowrap='true'><xsl:value-of select="."/></td>
            </xsl:if>  
          </xsl:for-each>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 2">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=8 or position()=7">
		                  <td class="numberText" style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()&gt;9">
		                  <td class="numberText" style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>		      
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



