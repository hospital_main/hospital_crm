<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
    	<xsl:for-each select="/root/tbody/tr[1]/td">
          <xsl:choose>
            <xsl:when test="position()=3">
		      	<th nowrap='true' rowspan="2">年</th>
		      	<th nowrap='true' rowspan="2">月</th>
		      	<th nowrap='true' rowspan="2">日</th>
		      	<th nowrap='true' rowspan="2" valign="center">凭证号</th>
		      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
		      	<th nowrap='true' rowspan="2" valign="center">借方</th>
		      	<th nowrap='true' rowspan="2" valign="center">贷方</th>
		      	<th nowrap='true' rowspan="2" valign="center">方向</th>
		      	<th nowrap='true' rowspan="2" valign="center">余额</th>
            </xsl:when>
            <xsl:when test="position() = 4 ">
              <th nowrap='true'>
               	<xsl:attribute name="colspan" >
  			      <xsl:value-of select="."/>
  			    </xsl:attribute>借方分析
	          </th>
	          <xsl:variable name="cc" select="."/>
	          <xsl:for-each select="/root/tbody/tr[1]/td[position() &lt; $cc]">
	          	<th nowrap='true' style="display:none"></th>
	          </xsl:for-each>
            </xsl:when>
            <xsl:when test="position() = 6 ">
              <th nowrap='true'>
               	<xsl:attribute name="colspan" >
  			      <xsl:value-of select="."/>
  			    </xsl:attribute>贷方分析
  			  </th>
  					<xsl:variable name="cc" select="."/>
	          <xsl:for-each select="/root/tbody/tr[1]/td[position() &lt; $cc]">
	          	<th nowrap='true' style="display:none"></th>
	          </xsl:for-each>
            </xsl:when>
         </xsl:choose>	            
      </xsl:for-each>
      </tr>
      <tr noWrap='true' class='mainHead'>
          <xsl:for-each select="/root/tbody/tr[2]/td">
            <xsl:if test="position() = 1 ">
		      	<th nowrap='true' style="display:none"></th>
            <th nowrap='true' style="display:none"></th>
            <th nowrap='true' style="display:none"></th>
            </xsl:if>  
            <th nowrap='true' style="display:none"></th>
            <th nowrap='true' style="display:none"></th>
            <th nowrap='true' style="display:none"></th>
            <th nowrap='true' style="display:none"></th>
            <th nowrap='true' style="display:none"></th>
            <th nowrap='true' style="display:none"></th>
            <xsl:if test="position() &gt; 10 ">
		      	<th nowrap='true'><xsl:value-of select="."/></th>
            </xsl:if>  
          </xsl:for-each>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <tr>
	          <xsl:for-each select="td">
	              <xsl:choose>
	                <xsl:when test="position()=4">
		                <td><a tabindex='-1' href="#">
		                  <xsl:attribute name="onclick" >
		    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');
		  	          	  </xsl:attribute><xsl:value-of select="../td[5]"/>
		  	          	</a><xsl:if test=". = ''">　</xsl:if></td>
	                </xsl:when>
	                <xsl:when test="position()=5">
	                </xsl:when>
	                <xsl:when test="position()=8 or position()=7">
	                  <td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	                </xsl:when>
	                <xsl:when test="position()=10">
	                	<xsl:choose>	
		                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[9]='平'">
		                		<td align='right'>Q</td>
    			          	</xsl:when>
			                <xsl:otherwise>
		                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			                </xsl:otherwise>
		             		 </xsl:choose>
	                </xsl:when>
	                <xsl:when test="position()&gt;10">
	                  <td class="numberText" align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	                </xsl:when>
	                
	                <xsl:otherwise>
	                  <td><xsl:value-of select="."/><xsl:if test=". = ''">　</xsl:if></td>
	                </xsl:otherwise>
	              </xsl:choose>		            
  			  </xsl:for-each>
	  	  </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

