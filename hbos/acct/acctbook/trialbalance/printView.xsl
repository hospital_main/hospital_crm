<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
  	<colgroup>		       
			<col style = 'width:110mm'/>	
			<col style = 'width:150mm'/>
			<col style = 'width:130mm'/>
			<col style = 'width:130mm'/>
			<col style = 'width:130mm'/>	
			<col style = 'width:130mm'/>	
			<col style = 'width:130mm'/>
			<col style = 'width:130mm'/>	
		</colgroup>
    <thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">8</xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
		<tr>
			<td style="fontsize:subtitle;"><xsl:attribute name="colspan">8</xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
     <tr noWrap='true' class='mainHead' >
  		<td rowspan="2" valign="center">科目编码</td>
		<td rowspan="2" valign="center">科目名称</td>
		<td colspan="2">期初余额</td>
		<td style="display:none"></td>
		<td colspan="2">本期发生额</td>
		<td style="display:none"></td>
		<td colspan="2">期末余额</td>
		<td style="display:none"></td>
    </tr>
    <tr noWrap='true' class='mainHead'>
    	<td style="display:none"></td>
    	<td style="display:none"></td>
    	<td>借方</td>
	<td>贷方</td>
	<td>借方</td>
	<td>贷方</td>
	<td>借方</td>
	<td>贷方</td>
    </tr>
   </thead>
   
   <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>                  
          <xsl:for-each select="td">
            <td>
               <xsl:choose>
                <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8">
		    <xsl:if test=". != '0.0000'">
			<xsl:attribute name="style">align:right</xsl:attribute>
			<xsl:value-of select="format-number(.,'#,##0.00')"/>
			</xsl:if>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="."/>
		</xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
     </xsl:for-each>   
 	</tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>
