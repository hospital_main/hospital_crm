<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
		<th rowspan="2" valign="center">科目编码</th>
		<th rowspan="2" valign="center">科目名称</th>
		<th colspan="2">期初余额</th>
		<th colspan="2">本期发生额</th>
		<th colspan="2">期末余额</th>
		</tr>
  		<tr noWrap='true' class='mainHead'>
		<th>借方</th>
		<th>贷方</th>
		<th>借方</th>
		<th>贷方</th>
		<th>借方</th>
		<th>贷方</th>
		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
  			<tr>
          <xsl:for-each select="td">
            <td >
			
		 <xsl:choose>
                <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8">
		    <xsl:if test=". != '0.0000'">
			<xsl:attribute name="class">numberText</xsl:attribute>
			<xsl:value-of select="format-number(.,'#,##0.00')"/>
			</xsl:if>
		</xsl:when>
		
		 <xsl:when test="position()=1">
	  		<xsl:if test="(../td[2])!=''">
				<xsl:value-of select="."/>
			  </xsl:if>
			  <xsl:if test="(../td[2])=''">
			  	<span style="font-size:13px;font-weight:bold;">
				<xsl:value-of select="."/>
				</span>
			  </xsl:if>
			</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="."/>
		</xsl:otherwise>
              </xsl:choose>
			  
            </td>
  			  </xsl:for-each>  				
  			</tr>
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>