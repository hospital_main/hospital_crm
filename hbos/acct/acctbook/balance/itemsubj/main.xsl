<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
		<xsl:variable name="cellend" select="//tr[1]/td[$colNum]"/>
  		<tr noWrap='true' class='mainHead'>	
		<th rowspan="2" valign="center">科目代码</th>
		<th rowspan="2" valign="center">科目名称</th>
		<th colspan="2">期初余额</th>
		<th colspan="2">本期发生额</th>
		<xsl:if test=" $cellend != '0' ">
		<th colspan="2">本年累计额</th>
		</xsl:if>
		<th colspan="2">期末余额</th>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
		<th>借方</th>
		<th>贷方</th>
		<th>借方</th>
		<th>贷方</th>
		<xsl:if test=" $cellend != '0' ">
		<th>借方</th>
		<th>贷方</th>
		</xsl:if>
		<th>借方</th>
		<th>贷方</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
               <xsl:when test="position()=$colNum">
            	 </xsl:when>
            	 
               <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 ">
        		    <td>
        		      <xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">
              			<xsl:attribute name="class">numberText</xsl:attribute>
              			<xsl:value-of select="format-number(.,'#,##0.00')"/>
            			</xsl:if>
          			</td>
	            </xsl:when>
	            
          		<xsl:otherwise>
            		<td>
            			<xsl:value-of select="."/>
          			</td>
          		</xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

