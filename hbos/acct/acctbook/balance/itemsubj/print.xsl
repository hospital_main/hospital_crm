<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<root>
    	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
		  <xsl:variable name="cellend" select="//tr[1]/td[$colNum - 1]"/>
		  <xsl:variable name="cellen" select="//tr[1]/td[$colNum]"/>
			<colgroup>		       
				<col style = 'width:110mm'/>	
				<col style = 'width:110mm'/>	
				<col style = 'width:110mm'/>	
				<col style = 'width:110mm'/>	
				<col style = 'width:110mm'/>	
				<col style = 'width:110mm'/>
				<col style = 'width:110mm'/>
				<col style = 'width:110mm'/>
				<col style = 'width:110mm'/>
				<col style = 'width:110mm'/>
				<col style = 'width:110mm'/>
				<col style = 'width:110mm'/>
			</colgroup>
  	<thead>
  		<tr>
          <td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="8 + $cellend + $cellend+ $cellen + $cellen" /></xsl:attribute></td>
					<td style='display:none'/>
					<xsl:if test=" $cellen != '0' ">
  					<td style='display:none'/>
  					<td style='display:none'/>
					</xsl:if>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
		  			<xsl:if test=" $cellend != '0' ">
  					<td style='display:none'/>
  					<td style='display:none'/>
					</xsl:if>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
				<tr>
					<td style="fontsize:subtitle;"><xsl:attribute name="colspan"><xsl:value-of select="8 + $cellend + $cellend+ $cellen + $cellen" /></xsl:attribute></td>
					<td style='display:none'/>
					<xsl:if test=" $cellen != '0' ">
  					<td style='display:none'/>
  					<td style='display:none'/>
					</xsl:if>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
		  			<xsl:if test=" $cellend != '0' ">
  					<td style='display:none'/>
  					<td style='display:none'/>
					</xsl:if>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
  		<tr noWrap='true' class='mainHead'>
  		<td rowspan="2">科目代码</td>
  		<td rowspan="2">科目名称</td>
  		<xsl:if test=" $cellen != '0' ">
    		<td colspan="2">年初余额</td>
    		<td style="display:none"/>
  		</xsl:if>
  		<td colspan="2">期初余额</td>
  		<td style="display:none"/>
  		<td colspan="2">本期发生额</td>
  		<td style="display:none"/>
  		<xsl:if test=" $cellend != '0' ">
    		<td colspan="2">本年累计额</td>
    		<td style="display:none"/>
  		</xsl:if>
  		<td colspan="2">期末余额</td>
  		<td style="display:none"/>
 		</tr>
  		<tr noWrap='true' class='mainHead'>
		<td style="display:none"/>
		<td style="display:none"/>
		<xsl:if test=" $cellen != '0' ">
  		<td>借方</td>
  		<td>贷方</td>
  		 </xsl:if>
		<td>借方</td>
		<td>贷方</td>
		<td>借方</td>
		<td>贷方</td>
		<xsl:if test=" $cellend != '0' ">
  		<td>借方</td>
  		<td>贷方</td>
    		</xsl:if>
		<td>借方</td>
		<td>贷方</td>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                <xsl:choose>
                <xsl:when test="position()=1 or position()=2">
		            <td><xsl:value-of select="."/></td>
                </xsl:when>		
                <xsl:when test="position()=$colNum">
                </xsl:when>     
                <xsl:when test="position()=$colNum - 1">
                </xsl:when>                                 
                <xsl:otherwise>
                  <td class="numberText" style="align:right">
          		      <xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">
                      <xsl:value-of select="format-number(.,'#,##0.00')"/>
                    </xsl:if>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>

