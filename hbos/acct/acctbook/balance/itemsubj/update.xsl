<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="total" select="count(/root/tbody/tr[1]/td)"/>
			<tr noWrap='true' class='mainHead'>
				<th>凭证编号</th>
				<th>凭证日期</th>
				<th>附件</th>
				<th>摘要</th>
				<th>科目编码</th>
				<th>科目名称</th>
				<th>借方金额</th>
				<th>贷方金额</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>记账人</th>
				
			</tr>
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position()!=($total - 14 + 12) and position()!=($total - 14+13) and position()!=($total - 14+14)]">
						<td>
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". != '合计'">
											<a href="#">
												<xsl:attribute name="onclick">													
													openMainVouch("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
													
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
											<xsl:if test="../td[$total - 14+12]!=''"><img src="chong.gif"/></xsl:if>
											<xsl:if test="../td[$total - 14+13]='1'"><img src="fei.gif"/></xsl:if>
									</xsl:if>
									<xsl:if test=". = '合计'">
										<xsl:value-of select="."/>  
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=4  or position()=6">
									<xsl:value-of select="."/>
								</xsl:when>
								<!--
								<xsl:when test="position()=5">
								  <xsl:if test="../td[$total - 14 +14]='1'">
  								  <a href="#">
    								  <xsl:attribute name="onclick">
    								    openDetailItems("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
    								  </xsl:attribute>
    									<xsl:value-of select="."/>
  									</a>
									</xsl:if>
									<xsl:if test="../td[$total - 14 + 14]='0'">
  									<xsl:value-of select="."/>
  								</xsl:if>
								</xsl:when>
								-->
								<xsl:when test="position()=7 or position()=8">
									<xsl:if test=". != 0">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
			
								<xsl:otherwise>
										<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
