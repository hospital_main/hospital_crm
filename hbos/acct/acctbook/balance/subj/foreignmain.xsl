<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  	<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
		<xsl:variable name="cellend" select="//tr[1]/td[$colNum]"/>
		<xsl:variable name="cellend2" select="//tr[1]/td[$colNum - 1]"/>
		<xsl:variable name="cellend3" select="//tr[1]/td[$colNum - 2]"/>
		<xsl:variable name="cellend4" select="//tr[1]/td[$colNum - 3]"/>
		<xsl:variable name="cellend5" select="//tr[1]/td[$colNum - 4]"/>
  		<tr noWrap='true' class='mainHead'>
		<th rowspan="3" valign="center">科目代码</th>
		<th rowspan="3" valign="center">科目名称</th>
		<xsl:if test=" $cellend5 != '0' ">
		<th colspan="4">年初余额</th>
		</xsl:if>
		<xsl:if test=" $cellend4 != '0' ">
		<th colspan="4">期初余额</th>
		</xsl:if>
		<xsl:if test=" $cellend3 != '0' ">
		<th colspan="4">本期发生额</th>
		</xsl:if>
		<xsl:if test=" $cellend2 != '0' ">
		<th colspan="4">累计发生额</th>
		</xsl:if>
		<xsl:if test=" $cellend != '0' ">
		<th colspan="4">期末余额</th>
		</xsl:if>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
  		<xsl:if test=" $cellend5 != '0' ">
		<th colspan="2">借方</th>
		<th colspan="2">贷方</th>
		</xsl:if>
		<xsl:if test=" $cellend4 != '0' ">
		<th colspan="2">借方</th>
		<th colspan="2">贷方</th>
		</xsl:if>
		<xsl:if test=" $cellend3 != '0' ">
		<th colspan="2">借方</th>
		<th colspan="2">贷方</th>
		</xsl:if>
		<xsl:if test=" $cellend2 != '0' ">
		<th colspan="2">借方</th>
		<th colspan="2">贷方</th>
		</xsl:if>
		<xsl:if test=" $cellend != '0' ">
		<th colspan="2">借方</th>
		<th colspan="2">贷方</th>
		</xsl:if>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
  		<xsl:if test=" $cellend5 != '0' ">
		<th>原币</th>
		<th>本币</th>
		<th>原币</th>
		<th>本币</th>
		</xsl:if>
		<xsl:if test=" $cellend4 != '0' ">
		<th>原币</th>
		<th>本币</th>
		<th>原币</th>
		<th>本币</th>
		</xsl:if>
		<xsl:if test=" $cellend3 != '0' ">
		<th>原币</th>
		<th>本币</th>
		<th>原币</th>
		<th>本币</th>
		</xsl:if>
		<xsl:if test=" $cellend2 != '0' ">
		<th>原币</th>
		<th>本币</th>
		<th>原币</th>
		<th>本币</th>
		</xsl:if>
		<xsl:if test=" $cellend != '0' ">
		<th>原币</th>
		<th>本币</th>
		<th>原币</th>
		<th>本币</th>
		</xsl:if>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                <xsl:choose>
                <xsl:when test="position()=$colNum or position()=$colNum - 1 or position()=$colNum - 2 or position()=$colNum - 3 or position()=$colNum - 4">
								</xsl:when>
						    
						    <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 
						   or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17 or position()=18 or position()=19 or position()=20 or position()=21 or position()=22">
								    <td>
									    <xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">
												<xsl:attribute name="class">numberText</xsl:attribute>
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
											</xsl:if>
											
										</td>
								</xsl:when>	
								<!--
								 <xsl:when test="../td[$colNum - 7]='平' and position() > 2">
								 	<td>
								 			<xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">
												<xsl:attribute name="class">numberText</xsl:attribute>
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
											</xsl:if>
																					
								 			<xsl:if test="( $cellend != '0' and (position() = $colNum - 6 or position() = $colNum - 5 ) and ( . = '0.0000' or .='0.00' or .='0' ))">
								 				<xsl:attribute name="class">numberText</xsl:attribute>
								 				Q
											</xsl:if>	
									</td>
								 	</xsl:when>								 
								 	-->
								<xsl:otherwise>
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

