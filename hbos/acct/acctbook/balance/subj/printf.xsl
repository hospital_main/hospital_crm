<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<root>
     <xsl:variable name="colNum" select="count(//tr[1]/td)"/>
   		<xsl:variable name="cellend" select="//tr[1]/td[$colNum]"/>
   		<xsl:variable name="cellend2" select="//tr[1]/td[$colNum - 1]"/>
   		<xsl:variable name="cellend3" select="//tr[1]/td[$colNum - 2]"/>
   		<xsl:variable name="cellend4" select="//tr[1]/td[$colNum - 3]"/>
   		<xsl:variable name="cellend5" select="//tr[1]/td[$colNum - 4]"/>
  	<thead>
      	<tr>
          <td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="2 + $cellend5 *4 + $cellend4 * 4 + $cellend3* 4  + $cellend2 * 4 + $cellend * 4"/></xsl:attribute></td>
					<td style='display:none'/>
  			<xsl:if test=" $cellend5 != '0' ">
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</xsl:if>
  			<xsl:if test=" $cellend4 != '0' ">
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>					
				</xsl:if>
  			<xsl:if test=" $cellend3 != '0' ">
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>					
				</xsl:if>
  			<xsl:if test=" $cellend2 != '0' ">
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>					
				</xsl:if>
  			<xsl:if test=" $cellend != '0' ">
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>	
					<td style='display:none'/>					   
				</xsl:if>
				</tr>
				<tr>
					<td style="fontsize:subtitle;"><xsl:attribute name="colspan"><xsl:value-of select="2 + $cellend5 *4 + $cellend4 * 4 + $cellend3* 4  + $cellend2 * 4 + $cellend * 4"/></xsl:attribute></td>
					<td style='display:none'/>
  			<xsl:if test=" $cellend5 != '0' ">
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>					
				</xsl:if>
  			<xsl:if test=" $cellend4 != '0' ">
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>					
				</xsl:if>
  			<xsl:if test=" $cellend3 != '0' ">
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>					
				</xsl:if>
  			<xsl:if test=" $cellend2 != '0' ">
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>					
				</xsl:if>
  			<xsl:if test=" $cellend != '0' ">
					<td style='display:none'/>
					<td style='display:none'/> 
					<td style='display:none'/>	
					<td style='display:none'/>					  
				</xsl:if>
				</tr>
  	<tr noWrap='true' class='mainHead'>
		<td rowspan="3">科目代码</td>
		<td rowspan="3">科目名称</td>
		<xsl:if test=" $cellend5 != '0' ">
  		<td colspan="4">年初余额</td>
  		<td style="display:none"/>
  		<td style="display:none"/>
  		<td style="display:none"/>
  	</xsl:if>
		<xsl:if test=" $cellend4 != '0' ">
  		<td colspan="4">期初余额</td>
  		<td style="display:none"/>
  		<td style="display:none"/>
  		<td style="display:none"/>
  	</xsl:if>
		<xsl:if test=" $cellend3 != '0' ">
  		<td colspan="4">本期发生额</td>
  		<td style="display:none"/>
  		<td style="display:none"/>
  		<td style="display:none"/>
  	</xsl:if>
		<xsl:if test=" $cellend2 != '0' ">
  		<td colspan="4">累计发生额</td>
  		<td style="display:none"/>
  		<td style="display:none"/>
  		<td style="display:none"/>
  	</xsl:if>
		<xsl:if test=" $cellend != '0' ">  	  	
  		<td colspan="4">期末余额</td>
  		<td style="display:none"/>
  		<td style="display:none"/>
  		<td style="display:none"/>
  	</xsl:if>
  	  
  	</tr>
  	<tr noWrap='true' class='mainHead'>
		<td style="display:none"/>
    <td style="display:none"/>
		<xsl:if test=" $cellend5 != '0' ">        		
		<td colspan="2">借方</td>
		<td style="display:none"/>
		<td colspan="2">贷方</td>
		<td style="display:none"/>
  	</xsl:if>
		<xsl:if test=" $cellend4 != '0' ">		
		<td colspan="2">借方</td>
		<td style="display:none"/>
		<td colspan="2">贷方</td>
		<td style="display:none"/>
  	</xsl:if>
		<xsl:if test=" $cellend3 != '0' ">		
		<td colspan="2">借方</td>
		<td style="display:none"/>
		<td colspan="2">贷方</td>
		<td style="display:none"/>
  	</xsl:if>
		<xsl:if test=" $cellend2 != '0' ">		
		<td colspan="2">借方</td>
		<td style="display:none"/>
		<td colspan="2">贷方</td>
		<td style="display:none"/>
  	</xsl:if>
		<xsl:if test=" $cellend != '0' ">		
		<td colspan="2">借方</td>
		<td style="display:none"/>
		<td colspan="2">贷方</td>
		<td style="display:none"/>
  	</xsl:if>
  	</tr>
  	<tr noWrap='true' class='mainHead'>
  	<td style="display:none"/>
		<td style="display:none"/>
  	<xsl:if test=" $cellend5 != '0' ">
		<td>原币</td>
		<td>本币</td>
		<td>原币</td>
		<td>本币</td>
		</xsl:if>
		<xsl:if test=" $cellend4 != '0' ">
		<td>原币</td>
		<td>本币</td>
		<td>原币</td>
		<td>本币</td>
		</xsl:if>
		<xsl:if test=" $cellend3 != '0' ">
		<td>原币</td>
		<td>本币</td>
		<td>原币</td>
		<td>本币</td>
		</xsl:if>
		<xsl:if test=" $cellend2 != '0' ">
		<td>原币</td>
		<td>本币</td>
		<td>原币</td>
		<td>本币</td>
		</xsl:if>
		<xsl:if test=" $cellend != '0' ">
		<td>原币</td>
		<td>本币</td>
		<td>原币</td>
		<td>本币</td>
		</xsl:if>
  		</tr>  	
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                <xsl:choose>
                <xsl:when test="position()=$colNum or position()=$colNum - 1 or position()=$colNum - 2 or position()=$colNum - 3 or position()=$colNum - 4">
                </xsl:when>
								<!--
						    <xsl:when test=" $cellend != '0' and  position()=$colNum - 7">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>	
								-->
						    <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 
						   or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17 or position()=18 or position()=19 or position()=20 or position()=21 or position()=22">
								    <td>
									    <xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">
												<xsl:attribute name="class">numberText</xsl:attribute>
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
											</xsl:if>
										</td>
								</xsl:when>	
								<!--
								 <xsl:when test="../td[$colNum - 7]='平' and position() > 2">
								 	<td>
								 			<xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">
												<xsl:attribute name="class">numberText</xsl:attribute>
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
											</xsl:if>
																					
								 			<xsl:if test="( $cellend != '0' and (position() = $colNum - 6 or position() = $colNum - 5 ) and ( . = '0.0000' or .='0.00' or .='0' ))">
								 				<xsl:attribute name="class">numberText</xsl:attribute>
								 				Q
											</xsl:if>	
									</td>
								 	</xsl:when>		
								-->						 								
								<xsl:otherwise>
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>

