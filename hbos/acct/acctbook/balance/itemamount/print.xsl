<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<root>
  	  <xsl:variable name="colNum" select="count(//tr[1]/td)"/>
	    <xsl:variable name="cellend" select="//tr[1]/td[$colNum]"/>
			<colgroup>		       
				<col style = 'width:110mm'/>	
				<col style = 'width:110mm'/>	
				<col style = 'width:110mm'/>	
				<col style = 'width:110mm'/>	
				<col style = 'width:110mm'/>	
				<col style = 'width:110mm'/>
				<col style = 'width:110mm'/>
     		<xsl:if test=" $cellend != '0' ">
  				<col style = 'width:110mm'/>
  				<col style = 'width:110mm'/>	
  				<col style = 'width:110mm'/>	
  				<col style = 'width:110mm'/>	
				</xsl:if>
				<col style = 'width:110mm'/>	
				<col style = 'width:110mm'/>	
				<col style = 'width:110mm'/>
				<col style = 'width:110mm'/>
				<col style = 'width:110mm'/>
				<col style = 'width:110mm'/>
			</colgroup>
  	<thead>
  		<tr>
        		<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="13 + $cellend + $cellend + $cellend + $cellend"/></xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
     		<xsl:if test=" $cellend != '0' ">
  				<td style='display:none'/>
  				<td style='display:none'/>
  				<td style='display:none'/>
  				<td style='display:none'/>
  			</xsl:if>
				<td style='display:none'/>
				<td style='display:none'/>  
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/> 
			</tr>
			<tr>
				<td style="fontsize:subtitle;"><xsl:attribute name="colspan"><xsl:value-of select="13 + $cellend + $cellend + $cellend + $cellend"/></xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
     		<xsl:if test=" $cellend != '0' ">
  				<td style='display:none'/>
  				<td style='display:none'/>
  				<td style='display:none'/>
  				<td style='display:none'/>
  			</xsl:if>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>   
			</tr>
  		<tr noWrap='true' class='mainHead'>
		<td rowspan="2">科目代码</td>
		<td rowspan="2">科目名称</td>
		<td rowspan="2">方向</td>
		<td colspan="3">期初结存</td>
		<td style="display:none"/>
        <td style="display:none"/>
		<td colspan="2">本期收入</td>
		<td style="display:none"/>
		<td colspan="2">本期发出</td>
		<td style="display:none"/>
 		<xsl:if test=" $cellend != '0' ">
  		<td colspan="2">本年累计收入</td>
  		<td style="display:none"/>
  		<td colspan="2">本年累计发出</td>
  		<td style="display:none"/>
		</xsl:if>
		<td colspan="3">期末结存</td>
		<td style="display:none"/>
        <td style="display:none"/>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
		<td style="display:none"/>
        <td style="display:none"/>
		<td style="display:none"/>
		<td>数量</td>
		<td>单价</td>
		<td>金额</td>
		<td>数量</td>
		<td>金额</td>
		<td>数量</td>
		<td>金额</td>
 		<xsl:if test=" $cellend != '0' ">
  		<td>数量</td>
  		<td>金额</td>
  		<td>数量</td>
  		<td>金额</td>
		</xsl:if>
		<td>数量</td>
		<td>单价</td>
		<td>金额</td>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                <xsl:choose>
                <xsl:when test="position()=1 or position()=2 or position()=3">
			            <td><xsl:value-of select="."/></td>
	                </xsl:when>		                                
	                <xsl:otherwise>
	                  <td class="numberText" style="align:right">
            		      <xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">
    	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
  	                  </xsl:if>
	                  </td>
	                </xsl:otherwise>
	              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>

