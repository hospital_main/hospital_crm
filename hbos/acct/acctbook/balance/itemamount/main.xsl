<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  	   <xsl:variable name="colNum" select="count(//tr[1]/td)"/>
	     <xsl:variable name="cellend" select="//tr[1]/td[$colNum]"/>
  		 <tr noWrap='true' class='mainHead'>
      		<th rowspan="2" valign="center">科目代码</th>
      		<th rowspan="2" valign="center">科目名称</th>
      		<th rowspan="2" valign="center">方向</th>
      		<th colspan="3">期初结存</th>
      		<th colspan="2">本期收入</th>
      		<th colspan="2">本期发出</th>
      		<xsl:if test=" $cellend != '0' ">
        		<th colspan="2">本年累计收入</th>
        		<th colspan="2">本年累计发出</th>
      		</xsl:if>
      		<th colspan="3">期末结存</th>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
    		<th>数量</th>
    		<th>单价</th>
    		<th>金额</th>
    		<th>数量</th>
    		<th>金额</th>
    		<th>数量</th>
    		<th>金额</th>
    		<xsl:if test=" $cellend != '0' ">
      		<th>数量</th>
      		<th>金额</th>
      		<th>数量</th>
      		<th>金额</th>
    		</xsl:if>
    		<th>数量</th>
    		<th>单价</th>
    		<th>金额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
             <xsl:choose>
                <xsl:when test="position()=$colNum">
	            	</xsl:when>
                <xsl:when test="position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9 
                    or position()=10 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17">
          		    <td>
          		      <xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">
                			<xsl:attribute name="class">numberText</xsl:attribute>
                			<xsl:value-of select="format-number(.,'#,##0.00')"/>
                		</xsl:if>
            			</td>
            		</xsl:when>
            		<xsl:otherwise>
               		<td>
               			<xsl:value-of select="."/>
             			</td>
             		</xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

