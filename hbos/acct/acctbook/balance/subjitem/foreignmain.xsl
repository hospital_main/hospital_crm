<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
		<xsl:variable name="cellend" select="//tr[1]/td[$colNum - 2]"/>	
		<xsl:variable name="cellend1" select="//tr[1]/td[$colNum]"/>
		<xsl:variable name="cellend2" select="//tr[1]/td[$colNum - 1]"/>
		<!--第一行最后个单元个有数据，则说明是包括本年累计-->    	
   		<tr noWrap='true' class='mainHead'>
    		<xsl:for-each select="/root/tbody/tr[1]/td">
		    	<xsl:choose>
		            <xsl:when test="position() &lt; $colNum - $cellend">
				      	<th nowrap='true' rowspan="3" valign="center"><xsl:value-of select="."/></th>
		            </xsl:when>
		      </xsl:choose>
		  </xsl:for-each>
		   <xsl:if test=" $cellend1 != '0'  ">
				<th nowrap='true' colspan="4" valign="center">年初余额</th>
		  </xsl:if>
				<th nowrap='true' colspan="4" valign="center">期初余额</th>
		       	<th nowrap='true' colspan="4" valign="center">本期发生额</th>
		   <xsl:if test=" $cellend2 != '0'  ">
				<th nowrap='true' colspan="4" valign="center">本年累计额</th>
		  </xsl:if>
		        <th nowrap='true' colspan="4" valign="center">期末余额</th>
        </tr>
	  	<tr noWrap='true' class='mainHead'>
			<xsl:for-each select="/root/tbody/tr[1]/td">
		    	<xsl:choose>
		            <xsl:when test="position() &lt; $colNum - $cellend">
				      	<th nowrap='true' style='display:none'></th>
		            </xsl:when>
		      	</xsl:choose>
		  	</xsl:for-each>
	  	 <xsl:if test=" $cellend1 != '0' ">
			<th colspan="2">借方</th>
			<th colspan="2">贷方</th>
		 </xsl:if>
			<th colspan="2">借方</th>
			<th colspan="2">贷方</th>
			<th colspan="2">借方</th>
			<th colspan="2">贷方</th>
		<xsl:if test=" $cellend2 != '0' ">
			<th colspan="2">借方</th>
			<th colspan="2">贷方</th>
		</xsl:if>
			<th colspan="2">借方</th>
			<th colspan="2">贷方</th>
	  	</tr>
	  	<tr noWrap='true' class='mainHead'>
			<xsl:for-each select="/root/tbody/tr[1]/td">
		    	<xsl:choose>
		            <xsl:when test="position() &lt; $colNum - $cellend">
				      	<th nowrap='true' style='display:none'></th>
		            </xsl:when>
		      	</xsl:choose>
		  	</xsl:for-each>
	  	 <xsl:if test=" $cellend1 != '0' ">
			<th>原币</th>
			<th>本币</th>
			<th>原币</th>
			<th>本币</th>
		 </xsl:if>
			<th>原币</th>
			<th>本币</th>
			<th>原币</th>
			<th>本币</th>
			<th>原币</th>
			<th>本币</th>
			<th>原币</th>
			<th>本币</th>
		<xsl:if test=" $cellend2 != '0' ">
			<th>原币</th>
			<th>本币</th>
			<th>原币</th>
			<th>本币</th>
		</xsl:if>
			<th>原币</th>
			<th>本币</th>
			<th>原币</th>
			<th>本币</th>
	  	</tr>
    </thead>
      <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
		      <tr>
				<xsl:for-each select="td">
					<xsl:if test="position() &lt; $colNum - 1">
		              <xsl:choose>
		                <xsl:when test="position() &lt; $colNum - $cellend">
                          <td><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:otherwise>
		                <td>
    			          <xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">
              				<xsl:attribute name="class">numberText</xsl:attribute>
              				<xsl:value-of select="format-number(.,'#,##0.00')"/>
            				</xsl:if>
            			</td>
		                </xsl:otherwise>
		              </xsl:choose>	
		             </xsl:if>
			    </xsl:for-each>
			  </tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
<!--
<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
		<xsl:variable name="cellend" select="//tr[1]/td[$colNum]"/>		
		<xsl:if test=" $cellend != '0' ">
     		<tr noWrap='true' class='mainHead'>
	    		<xsl:for-each select="/root/tbody/tr[1]/td">
			    	<xsl:choose>
			            <xsl:when test="position() &lt; $colNum - 15">
					      	<th nowrap='true' rowspan="3" valign="center"><xsl:value-of select="."/></th>			            
			            </xsl:when>
			            <xsl:when test="position() = $colNum - 15">
					      	<th nowrap='true' colspan="4" valign="center">期初余额</th>
					      	<th nowrap='true' colspan="4" valign="center">本期发生额</th>
					      	<th nowrap='true' colspan="4" valign="center">本年累计额</th>
					      	<th nowrap='true' colspan="4" valign="center">期末余额</th>
			            </xsl:when>
			        </xsl:choose>
			    </xsl:for-each>
        	</tr>
		  	<tr noWrap='true' class='mainHead'>
				<th colspan="2">借方</th>
				<th colspan="2">贷方</th>
				<th colspan="2">借方</th>
				<th colspan="2">贷方</th>
				<th colspan="2">借方</th>
				<th colspan="2">贷方</th>
				<th colspan="2">借方</th>
				<th colspan="2">贷方</th>
		  	</tr>
	<tr noWrap='true' class='mainHead'>	  	
		<th>原币</th>
		<th>本币</th>
		<th>原币</th>
		<th>本币</th>	

		<th>原币</th>
		<th>本币</th>
		<th>原币</th>
		<th>本币</th>	

		<th>原币</th>
		<th>本币</th>
		<th>原币</th>
		<th>本币</th>	

		<th>原币</th>
		<th>本币</th>
		<th>原币</th>
		<th>本币</th>							
		</tr>
		</xsl:if>		
		<xsl:if test=" $cellend = '0' ">
     		<tr noWrap='true' class='mainHead'>
	    		<xsl:for-each select="/root/tbody/tr[1]/td">
			    	<xsl:choose>
			            <xsl:when test="position() &lt; $colNum - 11">
					      	<th nowrap='true' rowspan="3" valign="center"><xsl:value-of select="."/></th>			            
			            </xsl:when>
			            <xsl:when test="position() = $colNum - 11">
					      	<th nowrap='true' colspan="4" valign="center">期初余额</th>
					      	<th nowrap='true' colspan="4" valign="center">本期发生额</th>
					      	<th nowrap='true' colspan="4" valign="center">期末余额</th>
			            </xsl:when>
			        </xsl:choose>
			    </xsl:for-each>
        	</tr>
		  	<tr noWrap='true' class='mainHead'>
				<th colspan="2">借方</th>
				<th colspan="2">贷方</th>
				<th colspan="2">借方</th>
				<th colspan="2">贷方</th>
				<th colspan="2">借方</th>
				<th colspan="2">贷方</th>
		  	</tr>
	<tr noWrap='true' class='mainHead'>	  	
		<th>原币</th>
		<th>本币</th>
		<th>原币</th>
		<th>本币</th>	

		<th>原币</th>
		<th>本币</th>
		<th>原币</th>
		<th>本币</th>	

		<th>原币</th>
		<th>本币</th>
		<th>原币</th>
		<th>本币</th>	
						
		</tr>		  	
		</xsl:if>		
    </thead>
      <tbody>
	<xsl:if test=" $cellend != '0' ">
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">      
		      <tr>
				<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position() &lt; $colNum - 15">
                          <td><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:otherwise>
		                <td>
    			          <xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">    			          
              			<xsl:attribute name="class">numberText</xsl:attribute>
              			<xsl:value-of select="format-number(.,'#,##0.00')"/>              			
            				</xsl:if>
            				</td>
		                </xsl:otherwise>
		              </xsl:choose>						
			    </xsl:for-each>
			  </tr>
	  	  </xsl:if>	  	 	  
      </xsl:for-each>
     </xsl:if>	
    
	<xsl:if test=" $cellend = '0' ">
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">      
		      <tr>
				<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position() &lt; $colNum - 11">
		                  <td><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:otherwise>
		                <td>
    			          <xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">    			          
              			<xsl:attribute name="class">numberText</xsl:attribute>
              			<xsl:value-of select="format-number(.,'#,##0.00')"/>              			
            				</xsl:if>
            				</td>
		                </xsl:otherwise>
		              </xsl:choose>						
			    </xsl:for-each>
			  </tr>
	  	  </xsl:if>	  	 	  
      </xsl:for-each>
     </xsl:if>	     
    </tbody>
  </xsl:template>
</xsl:stylesheet>
-->