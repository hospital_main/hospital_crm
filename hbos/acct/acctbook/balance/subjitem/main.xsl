<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
		<xsl:variable name="cellend" select="//tr[1]/td[$colNum]"/>		
		<!--第一行最后个单元个有数据，则说明是包括本年累计-->    	
		<xsl:if test=" $cellend != '0' ">
     		<tr noWrap='true' class='mainHead'>
	    		<xsl:for-each select="/root/tbody/tr[1]/td">
			    	<xsl:choose>
			            <xsl:when test="position() &lt; $colNum - 7">
					      	<th nowrap='true' rowspan="2" valign="center"><xsl:value-of select="."/></th>			            
			            </xsl:when>
			            <xsl:when test="position() = $colNum - 7">
					      	<th nowrap='true' colspan="2" valign="center">期初余额</th>
					      	<th nowrap='true' colspan="2" valign="center">本期发生额</th>
					      	<th nowrap='true' colspan="2" valign="center">本年累计额</th>
					      	<th nowrap='true' colspan="2" valign="center">期末余额</th>
			            </xsl:when>
			        </xsl:choose>
			    </xsl:for-each>
        	</tr>
		  	<tr noWrap='true' class='mainHead'>
				<th>借方</th>
				<th>贷方</th>
				<th>借方</th>
				<th>贷方</th>
				<th>借方</th>
				<th>贷方</th>
				<th>借方</th>
				<th>贷方</th>
		  	</tr>
		</xsl:if>
		
		<xsl:if test=" $cellend = '0' ">
     		<tr noWrap='true' class='mainHead'>
	    		<xsl:for-each select="/root/tbody/tr[1]/td">
			    	<xsl:choose>
			            <xsl:when test="position() &lt; $colNum - 5">
					      	<th nowrap='true' rowspan="2" valign="center"><xsl:value-of select="."/></th>			            
			            </xsl:when>
			            <xsl:when test="position() = $colNum - 5">
					      	<th nowrap='true' colspan="2" valign="center">期初余额</th>
					      	<th nowrap='true' colspan="2" valign="center">本期发生额</th>
					      	<th nowrap='true' colspan="2" valign="center">期末余额</th>
			            </xsl:when>
			        </xsl:choose>
			    </xsl:for-each>
        	</tr>
		  	<tr noWrap='true' class='mainHead'>
				<th>借方</th>
				<th>贷方</th>
				<th>借方</th>
				<th>贷方</th>
				<th>借方</th>
				<th>贷方</th>
		  	</tr>
		</xsl:if>		
    </thead>
      <tbody>
	<xsl:if test=" $cellend != '0' ">
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">      
		      <tr>
				<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position() &lt; $colNum - 7">
                           <td><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:otherwise>
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>						
			    </xsl:for-each>
			  </tr>
	  	  </xsl:if>	  	 	  
      </xsl:for-each>
     </xsl:if>	
     
	<xsl:if test=" $cellend = '0' ">
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">      
		      <tr>
				<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position() &lt; $colNum - 5">
		                  <td><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:otherwise>
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>						
			    </xsl:for-each>
			  </tr>
	  	  </xsl:if>	  	 	  
      </xsl:for-each>
     </xsl:if>	     
    </tbody>
  </xsl:template>
</xsl:stylesheet>
