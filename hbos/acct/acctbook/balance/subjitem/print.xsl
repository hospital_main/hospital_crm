<?xml version='1.0' encoding="GBK"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">  <xsl:template match="/">		<xsl:variable name="colNum" select="count(//tr[1]/td)"/>		<xsl:variable name="cellend" select="//tr[1]/td[$colNum - 2]"/>			<xsl:variable name="cellend1" select="//tr[1]/td[$colNum]"/>		<xsl:variable name="cellend2" select="//tr[1]/td[$colNum - 1]"/>	<root>	<colgroup>    		<xsl:for-each select="/root/tbody/tr[1]/td">		    	<xsl:choose>		            <xsl:when test="position() &lt; $colNum - $cellend">				      	<col style = 'width:120'/>		            </xsl:when>		      </xsl:choose>		  	</xsl:for-each>		   	<xsl:if test=" $cellend1 != '0'  ">				<col style = 'width:80'/>				<col style = 'width:80'/>		  	</xsl:if>				<col style = 'width:80'/>				<col style = 'width:80'/>				<col style = 'width:80'/>				<col style = 'width:80'/>		   	<xsl:if test=" $cellend2 != '0'  ">				<col style = 'width:80'/>				<col style = 'width:80'/>		  	</xsl:if>		        <col style = 'width:80'/>				<col style = 'width:80'/>	</colgroup>	<thead>			<tr>            	<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="$colNum - 2"/></xsl:attribute></td>				<xsl:for-each select="/root/tbody/tr[1]/td">		    	  <xsl:choose>		            <xsl:when test="position() &lt; $colNum - $cellend - 1">				      	<td style='display:none'/>		            </xsl:when>		      	  </xsl:choose>		  		</xsl:for-each>		  		<xsl:if test=" $cellend1 != '0'  ">					<td style='display:none'/>					<td style='display:none'/>		  		</xsl:if>					<td style='display:none'/>					<td style='display:none'/>					<td style='display:none'/>					<td style='display:none'/>		   		<xsl:if test=" $cellend2 != '0'  ">					<td style='display:none'/>					<td style='display:none'/>		  		</xsl:if>		        	<td style='display:none'/>					<td style='display:none'/>			</tr>			<tr>            	<td style="fontsize:subtitle;"><xsl:attribute name="colspan"><xsl:value-of select="$colNum - 2"/></xsl:attribute></td>				<xsl:for-each select="/root/tbody/tr[1]/td">		    	  <xsl:choose>		            <xsl:when test="position() &lt; $colNum - $cellend - 1">				      	<td style='display:none'/>		            </xsl:when>		      	  </xsl:choose>		  		</xsl:for-each>		  		<xsl:if test=" $cellend1 != '0'  ">					<td style='display:none'/>					<td style='display:none'/>		  		</xsl:if>					<td style='display:none'/>					<td style='display:none'/>					<td style='display:none'/>					<td style='display:none'/>		   		<xsl:if test=" $cellend2 != '0'  ">					<td style='display:none'/>					<td style='display:none'/>		  		</xsl:if>		        	<td style='display:none'/>					<td style='display:none'/>			</tr>     		<tr noWrap='true' class='mainHead'>    			<xsl:for-each select="/root/tbody/tr[1]/td">		    	<xsl:choose>		            <xsl:when test="position() &lt; $colNum - $cellend">				      	<td nowrap='true' rowspan="2" valign="center"><xsl:value-of select="."/></td>		            </xsl:when>		      	</xsl:choose>		  		</xsl:for-each>		   		<xsl:if test=" $cellend1 != '0'  ">					<td nowrap='true' colspan="2" valign="center">年初余额</td>					<td nowrap='true' style='display:none'></td>		  		</xsl:if>					<td nowrap='true' colspan="2" valign="center">期初余额</td>					<td nowrap='true' style='display:none'></td>		       		<td nowrap='true' colspan="2" valign="center">本期发生额</td>		       		<td nowrap='true' style='display:none'></td>		   		<xsl:if test=" $cellend2 != '0'  ">					<td nowrap='true' colspan="2" valign="center">本年累计额</td>					<td nowrap='true' style='display:none'></td>		  		</xsl:if>		         	<td nowrap='true' colspan="2" valign="center">期末余额</td>		         	<td nowrap='true' style='display:none'></td>        	</tr>		  	<tr noWrap='true' class='mainHead'>				<xsl:for-each select="/root/tbody/tr[1]/td">		    	<xsl:choose>		            <xsl:when test="position() &lt; $colNum - $cellend">				      	<td nowrap='true' style='display:none'></td>		            </xsl:when>		      	</xsl:choose>		  		</xsl:for-each>	  	 		<xsl:if test=" $cellend1 != '0' ">					<td>借方</td>					<td>贷方</td>		 		</xsl:if>					<td>借方</td>					<td>贷方</td>					<td>借方</td>					<td>贷方</td>				<xsl:if test=" $cellend2 != '0' ">					<td>借方</td>					<td>贷方</td>				</xsl:if>					<td>借方</td>					<td>贷方</td>		  	</tr>    </thead>	<tbody>      <xsl:for-each select="/root/tbody/tr">	      <xsl:if test="position() &gt; 1">		      <tr>				<xsl:for-each select="td">					<xsl:if test="position() &lt; $colNum - 1">		              <xsl:choose>		                <xsl:when test="position() &lt; $colNum - $cellend">                          <td><xsl:value-of select="."/></td>		                </xsl:when>		                <xsl:otherwise>		                <td>    			          <xsl:if test=". != '0.0000' and .!='0.00' and .!='0'">              				<xsl:attribute name="class">numberText</xsl:attribute>              				<xsl:value-of select="format-number(.,'#,##0.00')"/>            				</xsl:if>            			</td>		                </xsl:otherwise>		              </xsl:choose>			             </xsl:if>			    </xsl:for-each>			  </tr>	  	  </xsl:if>      </xsl:for-each>	</tbody> 	</root>  </xsl:template></xsl:stylesheet><!--<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  		<xsl:variable name="colNum" select="count(//tr[1]/td)"/>
		<xsl:variable name="cellend" select="//tr[1]/td[$colNum]"/>		
		<root>
	    <colgroup>
			<xsl:if test=" $cellend != '0' ">
	    		<xsl:for-each select="/root/tbody/tr[1]/td">
			    	<xsl:choose>
			            <xsl:when test="position() &lt; $colNum - 7">
					      	<col style = 'width:120'/>
			            </xsl:when>
			            <xsl:when test="position() = $colNum - 7">
							<col style = 'width:80'/>
							<col style = 'width:80'/>
			            </xsl:when>
			        </xsl:choose>
			    </xsl:for-each>
			</xsl:if>
			
			<xsl:if test=" $cellend = '0' ">
	    		<xsl:for-each select="/root/tbody/tr[1]/td">
			    	<xsl:choose>
			            <xsl:when test="position() &lt; $colNum - 5">
					      	<col style = 'width:120'/>
			            </xsl:when>
			        </xsl:choose>
			    </xsl:for-each>
			</xsl:if>	
				    
			<col style = 'width:80'/>
			<col style = 'width:80'/>
			<col style = 'width:80'/>
			<col style = 'width:80'/>
			<col style = 'width:80'/>
			<col style = 'width:80'/>
	    </colgroup>
       <thead>
		<xsl:if test=" $cellend != '0' ">
			<tr>
            		<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/> 
					<xsl:if test=" $cellend != '0' ">
			    		<xsl:for-each select="/root/tbody/tr[1]/td">
					    	<xsl:choose>
					            <xsl:when test="position() &lt; $colNum - 7">
							      	<td style='display:none'/>
					            </xsl:when>
					            <xsl:when test="position() = $colNum - 7">
									<td style='display:none'/>
									<td style='display:none'/>
					            </xsl:when>
					        </xsl:choose>
					    </xsl:for-each>
					</xsl:if>
					
					<xsl:if test=" $cellend = '0' ">
			    		<xsl:for-each select="/root/tbody/tr[1]/td">
					    	<xsl:choose>
					            <xsl:when test="position() &lt; $colNum - 5">
							      	<td style='display:none'/>
					            </xsl:when>
					        </xsl:choose>
					    </xsl:for-each>
					</xsl:if>	
				</tr>
				<tr>
					<td style="fontsize:subtitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/> 
					<xsl:if test=" $cellend != '0' ">
			    		<xsl:for-each select="/root/tbody/tr[1]/td">
					    	<xsl:choose>
					            <xsl:when test="position() &lt; $colNum - 7">
							      	<td style='display:none'/>
					            </xsl:when>
					            <xsl:when test="position() = $colNum - 7">
									<td style='display:none'/>
									<td style='display:none'/>
					            </xsl:when>
					        </xsl:choose>
					    </xsl:for-each>
					</xsl:if>
					
					<xsl:if test=" $cellend = '0' ">
			    		<xsl:for-each select="/root/tbody/tr[1]/td">
					    	<xsl:choose>
					            <xsl:when test="position() &lt; $colNum - 5">
							      	<td style='display:none'/>
					            </xsl:when>
					        </xsl:choose>
					    </xsl:for-each>
					</xsl:if>	
				</tr>
     		<tr noWrap='true' class='mainHead'>
	    		<xsl:for-each select="/root/tbody/tr[1]/td">
			    	<xsl:choose>
			            <xsl:when test="position() &lt; $colNum - 7">
					      	<td nowrap='true' rowspan="2" valign="center"><xsl:value-of select="."/></td>			            
			            </xsl:when>
			            <xsl:when test="position() = $colNum - 7">
					      	<td nowrap='true' colspan="2" valign="center">期初余额</td>
					      	<td nowrap='true' style='display:none'></td>
					      	<td nowrap='true' colspan="2" valign="center">本期发生额</td>
					      	<td nowrap='true' style='display:none'></td>
					      	<td nowrap='true' colspan="2" valign="center">本年累计额</td>
					      	<td nowrap='true' style='display:none'></td>
					      	<td nowrap='true' colspan="2" valign="center">期末余额</td>
					      	<td nowrap='true' style='display:none'></td>
			            </xsl:when>
			        </xsl:choose>
			    </xsl:for-each>
        	</tr>
		  	<tr noWrap='true' class='mainHead'>
	    		<xsl:for-each select="/root/tbody/tr[1]/td">	
			    	<xsl:choose>
			            <xsl:when test="position() &lt; $colNum - 7">
					      	<td nowrap='true' style='display:none'></td>
			            </xsl:when>
			        </xsl:choose>	    		
			    </xsl:for-each>	    			  	
			  	<td nowrap='true'>借方</td>
			  	<td nowrap='true'>贷方</td>
			  	<td nowrap='true'>借方</td>
			  	<td nowrap='true'>贷方</td>
			  	<td nowrap='true'>借方</td>
			  	<td nowrap='true'>贷方</td>
			  	<td nowrap='true'>借方</td>
			  	<td nowrap='true'>贷方</td>
		  	</tr>
		</xsl:if>
		
		<xsl:if test=" $cellend = '0' ">
			<tr>
            		<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/> 
					<xsl:if test=" $cellend != '0' ">
			    		<xsl:for-each select="/root/tbody/tr[1]/td">
					    	<xsl:choose>
					            <xsl:when test="position() &lt; $colNum - 7">
							      	<td style='display:none'/>
					            </xsl:when>
					            <xsl:when test="position() = $colNum - 7">
									<td style='display:none'/>
									<td style='display:none'/>
					            </xsl:when>
					        </xsl:choose>
					    </xsl:for-each>
					</xsl:if>
					
					<xsl:if test=" $cellend = '0' ">
			    		<xsl:for-each select="/root/tbody/tr[1]/td">
					    	<xsl:choose>
					            <xsl:when test="position() &lt; $colNum - 5">
							      	<td style='display:none'/>
					            </xsl:when>
					        </xsl:choose>
					    </xsl:for-each>
					</xsl:if>	
				</tr>
				<tr>
					<td style="fontsize:subtitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td)"/></xsl:attribute></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/> 
					<xsl:if test=" $cellend != '0' ">
			    		<xsl:for-each select="/root/tbody/tr[1]/td">
					    	<xsl:choose>
					            <xsl:when test="position() &lt; $colNum - 7">
							      	<td style='display:none'/>
					            </xsl:when>
					            <xsl:when test="position() = $colNum - 7">
									<td style='display:none'/>
									<td style='display:none'/>
					            </xsl:when>
					        </xsl:choose>
					    </xsl:for-each>
					</xsl:if>
					
					<xsl:if test=" $cellend = '0' ">
			    		<xsl:for-each select="/root/tbody/tr[1]/td">
					    	<xsl:choose>
					            <xsl:when test="position() &lt; $colNum - 5">
							      	<td style='display:none'/>
					            </xsl:when>
					        </xsl:choose>
					    </xsl:for-each>
					</xsl:if>	
				</tr>
     		<tr noWrap='true' class='mainHead'>
	    		<xsl:for-each select="/root/tbody/tr[1]/td">
			    	<xsl:choose>
			            <xsl:when test="position() &lt; $colNum - 5">
					      	<td nowrap='true' rowspan="2" valign="center"><xsl:value-of select="."/></td>			            
			            </xsl:when>
			            <xsl:when test="position() = $colNum - 5">
					      	<td nowrap='true' colspan="2" valign="center">期初余额</td>
					      	<td nowrap='true' style='display:none'></td>
					      	<td nowrap='true' colspan="2" valign="center">本期发生额</td>
					      	<td nowrap='true' style='display:none'></td>
					      	<td nowrap='true' colspan="2" valign="center">期末余额</td>
			            </xsl:when>
			        </xsl:choose>
			    </xsl:for-each>
        	</tr>
		  	<tr noWrap='true' class='mainHead'>
	    		<xsl:for-each select="/root/tbody/tr[1]/td">	
			    	<xsl:choose>
			            <xsl:when test="position() &lt; $colNum - 5">
					      	<td nowrap='true' style='display:none'></td>
			            </xsl:when>
			        </xsl:choose>	    		
			    </xsl:for-each>	    			  			  	
			  	<td nowrap='true'>借方</td>
			  	<td nowrap='true'>贷方</td>
			  	<td nowrap='true'>借方</td>
			  	<td nowrap='true'>贷方</td>
			  	<td nowrap='true'>借方</td>
			  	<td nowrap='true'>贷方</td>
		  	</tr>
		</xsl:if>	
    </thead>
      <tbody>
  	<xsl:if test=" $cellend != '0' ">
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">      
		      <tr>
				<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position() &lt; $colNum - 7">
                           <td><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:otherwise>
    			            <td class="numberText" style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>						
			    </xsl:for-each>
			  </tr>
	  	  </xsl:if>	  	 	  
      </xsl:for-each>
     </xsl:if>	
     
	<xsl:if test=" $cellend = '0' ">
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">      
		      <tr>
				<xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position() &lt; $colNum - 5">
		                  <td><xsl:value-of select="."/></td>
		                </xsl:when>
		                <xsl:otherwise>
    			            <td class="numberText" style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:otherwise>
		              </xsl:choose>						
			    </xsl:for-each>
			  </tr>
	  	  </xsl:if>	  	 	  
      </xsl:for-each>
     </xsl:if>	
    </tbody>
 	</root>
  </xsl:template>
</xsl:stylesheet>-->