<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th nowrap='true' colspan='2'>
		  	<xsl:for-each select="/root/tbody/tr[1]">
				<xsl:for-each select="td">
					<xsl:if test="position()=1">
					<xsl:value-of select="."/>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>

		  	年</th>
		  	<th nowrap='true' rowspan='2' valign='middle'>凭证号</th>
		  	<th nowrap='true' rowspan='2' valign='middle'>摘要</th>
		  	<th nowrap='true' rowspan='2' valign='middle'>现金流量科目</th>
		  	<th nowrap='true' rowspan='2' valign='middle'>现金流量类型</th>
		  	<th nowrap='true' rowspan='2' valign='middle'>现金流量项目</th>
		  	<th nowrap='true' rowspan='2' valign='middle'>流入金额</th>
		  	<th nowrap='true' rowspan='2' valign='middle'>流出金额</th>
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<th nowrap='true'>月</th>
		  	<th nowrap='true'>日</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 4 or position() = 1">
                <td align="center">
                	<xsl:attribute name="style" >display:none;</xsl:attribute><xsl:value-of select="."/>
  	          	</td>
  	          	</xsl:when>
				<xsl:when test="position() = 5">
                <td align="center">
                <a tabindex='-1'>
                  <xsl:attribute name="href" >
    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../td[4]"/>&lt;/vouch_id&gt;');
  	          		</xsl:attribute><xsl:value-of select="."/></a>
  	          	</td>
                </xsl:when>
                <xsl:when test="position() = 10 or position() = 11">
			            <td align="right">
		                <xsl:if test="text() != '0'">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </xsl:if>
      			      </td>
                </xsl:when>
                <xsl:otherwise>
                	<td>
                  <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>