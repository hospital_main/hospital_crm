<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
		<colgroup>
			<col style = 'width:50mm'/>
			<col style = 'width:50mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:200mm'/>
			<col style = 'width:200mm'/>
			<col style = 'width:200mm'/>
			<col style = 'width:200mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:0mm'/>
			<col style = 'width:0mm'/>
			
		</colgroup>
  
    <thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">9</xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
		<tr>
			<td style="fontsize:subtitle;"><xsl:attribute name="colspan">9</xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
      <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true' colspan='2'>
		  	<xsl:for-each select="/root/tbody/tr[1]">
				<xsl:for-each select="td">
					<xsl:if test="position()=1">
					<xsl:value-of select="."/>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>

		  	年</td><td style='display:none'/>
		  	<td nowrap='true' rowspan='2' valign='middle'>凭证号</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>摘要</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>现金流量科目</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>现金流量类型</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>现金流量项目</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>流入金额</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>流出金额</td>
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true'>月</td>
		  	<td nowrap='true'>日</td>
		  	<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 1 or position() = 4">
              		
              	</xsl:when>
              	<xsl:when test="position() = 4 or position() = 1">
                <td align="center">
                	<xsl:attribute name="style" >display:none;</xsl:attribute><xsl:value-of select="."/>
  	          	</td>
  	          	</xsl:when>
				<xsl:when test="position() = 10 or position() = 11">
			            <td align="right" style="align:right">
		                <xsl:if test="text() != '0'">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </xsl:if>
      			      </td>
                </xsl:when>
                <xsl:otherwise>
                	<td>
                  <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>