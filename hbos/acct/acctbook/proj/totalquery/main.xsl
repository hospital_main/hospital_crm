<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>年度</th>
  			<th nowrap='true'>月份</th>
  			<th nowrap='true'>摘要</th>
  			<th nowrap='true'>借方</th>
  			<th nowrap='true'>贷方</th>
  			<th nowrap='true'>方向</th>
  			<th nowrap='true'>余额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>   						
							<xsl:when test=" position()=4 or position()=5 or position()=7">
                  <xsl:if test=".='0.0000' or .='0'">
                    <td></td>
                  </xsl:if>               
                  <xsl:if test=".!='0.0000' and .!='0'">
		                <td align="right">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
                  </xsl:if> 
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>