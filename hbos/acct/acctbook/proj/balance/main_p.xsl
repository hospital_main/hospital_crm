<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th>凭证日期</th>
				<th>凭证号</th>
				<th>摘要</th>
				<th>科目名称</th>
				<th>借方金额</th>
				<th>贷方金额</th>
			</tr>
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=2">
								<td>
					        <a href="#">
										<xsl:attribute name="onclick">
											 javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');return false;
										</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
							</td>
							</xsl:when>
								<xsl:when test="position()=5">
									<td  align='right'>
									<xsl:if test="format-number(.,'#,##0.00') != '0.00'">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
									</td>
								</xsl:when>
								<xsl:when test="position()=6">
									<td  align='right'>
									<xsl:if test="format-number(.,'#,##0.00') != '0.00'">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
