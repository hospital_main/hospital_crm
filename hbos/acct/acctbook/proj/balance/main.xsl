<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>项目编码</th>
  			<th nowrap='true'>项目名称</th>
  			<th nowrap='true'>负责人</th>
  			<th nowrap='true'>期初数</th>
  			<th nowrap='true'>收入</th>
  			<th nowrap='true'>支出</th>
  			<th nowrap='true'>结余</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=4 or position()=7">
                  <xsl:if test="format-number(.,'#,##0.00')='0.00'">
                    <td></td>
                  </xsl:if>               
                  <xsl:if test="format-number(.,'#,##0.00')!='0.00' and ( position()=4 or position()=7)">
		                <td align="right">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
                  </xsl:if>        
              </xsl:when>
							<xsl:when test="position() = 5">
                  <xsl:if test="format-number(.,'#,##0.00')!='0.00'">
           				 <td  align="right"><a href='#'>
	                    <xsl:attribute name="onclick">openDialog('main_p.html?load=&lt;sz&gt;sr&lt;/sz&gt;<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:950px;dialogHeight:800px;overflow:auto;', this);</xsl:attribute>
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </a></td>
                  </xsl:if> 
                  <xsl:if test="format-number(.,'#,##0.00')='0.00'">
                    <td></td>
                  </xsl:if>       
              </xsl:when>
							<xsl:when test="position() = 6">
                  <xsl:if test="format-number(.,'#,##0.00')!='0.00'">
           				 <td  align="right"><a href='#'>
	                    <xsl:attribute name="onclick">openDialog('main_p.html?load=&lt;sz&gt;zc&lt;/sz&gt;<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:950px;dialogHeight:800px;overflow:auto;', this);</xsl:attribute>
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </a></td>
                  </xsl:if>   
                  <xsl:if test="format-number(.,'#,##0.00')='0.00'">
                    <td></td>
                  </xsl:if>                       
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<tr>
				<td>合计</td>
				<td></td>
				<td></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr[*]/td[4]),'#,##0.00')"/></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr[*]/td[5]),'#,##0.00')"/></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr[*]/td[6]),'#,##0.00')"/></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr[*]/td[7]),'#,##0.00')"/></td>
			</tr>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>