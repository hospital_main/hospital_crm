<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>项目编码</th>
  			<th nowrap='true'>项目名称</th>
  			<th nowrap='true'>负责人</th>
  			<th nowrap='true' style="display:none">科目名称</th>
  			<th nowrap='true'>期初余额</th>
  			<th nowrap='true'>借方发生</th>
  			<th nowrap='true'>贷方发生</th>
  			<th nowrap='true'>借方累计</th>
  			<th nowrap='true'>贷方累计</th>
  			<th nowrap='true'>期末余额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>   	
							<xsl:when test="position()=5 ">
              </xsl:when>
							<xsl:when test="position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10">
                  <xsl:if test=".='0.0000' or .='0'">
                    <td></td>
                  </xsl:if>               
                  <xsl:if test=".!='0.0000' and .!='0' and ( position()=5 or position()=8 or position()=9 or position()=10 )">
		                <td align="right">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
                  </xsl:if>
                  <xsl:if test=".!='0.0000' and .!='0' and  position()=6">
           				 <td><a href='#'>
	                    <xsl:attribute name="onclick">openDialog('main_p.html?load=&lt;jd&gt;jf&lt;/jd&gt;<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:950px;dialogHeight:800px;overflow:auto;', this);</xsl:attribute>
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </a></td>
                  </xsl:if>
                  <xsl:if test=".!='0.0000' and .!='0' and position()=7 ">
           				 <td><a href='#'>
	                    <xsl:attribute name="onclick">openDialog('main_p.html?load=&lt;jd&gt;df&lt;/jd&gt;<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:950px;dialogHeight:800px;overflow:auto;', this);</xsl:attribute>
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </a></td>
                  </xsl:if>                      
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>