<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>日期</th>
  			<th nowrap='true'>凭证号</th>
  			<th nowrap='true'>科目编码</th>
  			<th nowrap='true'>科目名称</th>
  			<th nowrap='true'>摘要</th>
  			<th nowrap='true'>借方</th>
  			<th nowrap='true'>贷方</th>
  			<th nowrap='true'>方向</th>
  			<th nowrap='true'>余额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>  
		              <xsl:when test="position()=2">
		                <td><a href='#'>
		                  <xsl:attribute name="onclick" >
		    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../td[10]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');return false;
		  	          	  </xsl:attribute><xsl:value-of select="."/>
		  	          	</a></td>
		              </xsl:when> 						
							<xsl:when test="position()=6 or position()=7 or position()=9">
                  <xsl:if test=".='0.0000' or .='0'">
                    <td></td>
                  </xsl:if>               
                  <xsl:if test=".!='0.0000' and .!='0'">
		                <td align="right">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                </td>
                  </xsl:if>                  
              </xsl:when>					
							<xsl:when test="position()=10">
									<td style="display:none"></td>                
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>