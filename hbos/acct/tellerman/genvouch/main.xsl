<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">	   
		<thead>
			<xsl:variable name="colNum" select="count(//tr[1]/td)"/> 
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th nowrap='true'>确认日期</th>
				<th nowrap='true'>摘要</th>
				<xsl:if test="$colNum = 8">
					<th nowrap='true'>结算方式</th>
					<th nowrap='true'>票据号</th>
				</xsl:if>
				<th nowrap='true'>对方科目</th>
				<th nowrap='true'>借方金额</th>	
				<th nowrap='true'>贷方金额</th>	
				<th nowrap='true'>制单人</th>									
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1' class="tableCheckbox">
						<xsl:attribute name="value" >
							<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
						</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<td>
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:when test="position()=$colNum - 1 or position()=$colNum - 2">
									<xsl:if test=".!=0">
										<xsl:attribute name="class" >numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'##,###.00')"/>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>

