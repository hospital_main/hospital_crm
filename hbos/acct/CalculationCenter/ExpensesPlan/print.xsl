<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				
			</colgroup>
  	<thead>
  		<tr noWrap='true' class='mainHead'>
		        <th>计划编号</th>
		  	<th>制单日期</th>
		  	<th>制表人</th>
		  	<th>单位审核人</th>
		  	<th>中心审核人</th>
  	  
  		</tr>
  		
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                <td><xsl:value-of select="."/></td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>

