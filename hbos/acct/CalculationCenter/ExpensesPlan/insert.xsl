<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		  <tr noWrap='true' class='mainHead'> 
		  	<th width='25'><input type='checkbox'/></th>
		  	<th>序号</th>
		    <th >材料代码</th>
		    <th >材料名称</th>
		    <th >规格型号</th>
		    <th>计量单位</th>
		    <th>数量</th>
		    <th>单价</th>
		    <th>件数</th>
		    <th >金额</th>		    
		    <th >生产批号</th>
		    <th >失效日期</th>
		  </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <td align='right'>
            <xsl:value-of select="position()"/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=2">
                <td>
                   <xsl:value-of select="."/>
                </td>
              </xsl:when>
	            <xsl:when test="position()=1 or position()=3 or position()=9 or position()=10 or position()=12 or position()=16 or position()=17 or position()=18">	
	              <td style="display:none">
	                <xsl:value-of select="."/>
	              </td> 
	            </xsl:when>                 
	            <xsl:when test="position()=7 or position()=8 or position()=11 or position()=13">
	              <td  align="right" class="moneyCol" noWrap="true">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>
              <xsl:otherwise>
                <td noWrap="true">
                  <xsl:value-of select="."/>
                </td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>