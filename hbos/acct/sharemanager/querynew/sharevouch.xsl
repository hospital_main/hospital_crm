<?xml version='1.0' encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="total" select="count(/root/tbody/tr[1]/td)"/>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>序号</th>

				<th>财务会计凭证号</th>
				<th>预算会计凭证号</th>
				<th>日期</th>

			</tr>
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
	        <td align='center'  style='display:none'>
	          <xsl:if test="td[1]!= '合计'">
							<input type='checkbox' TABINDEX='-1' align='center'>
								<xsl:attribute name="value" >
									<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
								</xsl:attribute>
									<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
							</input>
						</xsl:if>
						<xsl:if test="td[1]='合计'">
							<input type='checkbox' TABINDEX='-1' align='center'>
								<xsl:attribute name="disabled" >true</xsl:attribute>
							</input>
						</xsl:if>
					</td>
					<xsl:for-each select="td[($total = 22 and position() &lt;= 14) or ($total = 21 and position()&lt;= 13)]">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
									<xsl:if test=". != '合计'">
						        <a href="#">
											<xsl:attribute name="onclick">
												openMainVouch("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
										<xsl:if test=". != '' and ../td[($total = 22 and position() = 15) or ($total = 21 and position() = 14)]!=''"><img src="chong.gif"/></xsl:if>
										<xsl:if test=". != '' and ../td[($total = 22 and position() = 16) or ($total = 21 and position() = 15)]='1'"><img src="fei.gif"/></xsl:if>
										<xsl:if test=". != '' and ../td[($total = 22 and position() = 18) or ($total = 21 and position() = 17)]='1'"><img src="err.gif"/></xsl:if>
					        </xsl:if>
	               	<xsl:if test=". = '合计'">
						        <xsl:value-of select="."/>
						      </xsl:if>
							  </td>
							</xsl:when>
							<xsl:when test="position()=2">
								<td>
									<xsl:if test=". != ''">
						        <a href="#">
											<xsl:attribute name="onclick">
												openMainVouch("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
										<xsl:if test="../td[($total = 22 and position() = 15) or ($total = 21 and position() = 14)]!=''"><img src="chong.gif"/></xsl:if>
										<xsl:if test="../td[($total = 22 and position() = 16) or ($total = 21 and position() = 15)]='1'"><img src="fei.gif"/></xsl:if>
										<xsl:if test="../td[($total = 22 and position() = 18) or ($total = 21 and position() = 17)]='1'"><img src="err.gif"/></xsl:if>
					        </xsl:if>
	               	<xsl:if test=". = ''">
						        <xsl:value-of select="."/>  
						      </xsl:if>
							  </td>
							</xsl:when>
							<xsl:when test="position()=5">
								<td style="width:300px; overflow:hidden">
									<div style="width:300px; word-break:break-all;">
										<xsl:value-of select="."/> 
									</div> 
								</td>
							</xsl:when>
							<xsl:when test="position()=6 or position()=7">
								<td>
								<xsl:if test=". != 0">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="($total = 22 and position()=13) or ($total = 21 and position()=12) ">
								<td>
									<xsl:if test=". != ''">
						        <a href="#">
											<xsl:attribute name="onclick">
												openVouchdiff("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
					        </xsl:if>
	               	<xsl:if test=". = ''">
						        <xsl:value-of select="."/>  
						      </xsl:if>
							  </td>
							</xsl:when>
							<xsl:when test="($total = 22 and position()>14) or ($total = 21 and position()>13) ">
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
