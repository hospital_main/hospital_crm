<?xml version="1.0" encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	    <thead>
	    <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>
	  	  <tr noWrap='true' class='mainHead'>
	  			<td>会计凭证编号</td>
	  			<td>预算凭证编号</td>
					<td>凭证日期</td>
					<td>附件</td>
					<td>摘要</td>
					<td>借方金额</td>
					<td>贷方金额</td>
					<td>制单人</td>
					<td>审核人</td>
					<td>记账人</td>
					<td>标错信息</td>
			        <td>辅助项</td>
	      </tr>
      </thead>
	  	<tbody> 
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td[position()!=12 and position()!=14]">
							<td>
								<xsl:choose>
									<xsl:when test="position()=1">
										<xsl:if test=". != '合计'">
											<xsl:value-of select="."/>
						         </xsl:if>
		               <xsl:if test=". = '合计'">
							        <xsl:value-of select="."/>  
						       </xsl:if>
									</xsl:when>
									<xsl:when test="position()=6 or position()=7">
										<xsl:if test=". != 0">
											<xsl:attribute name="align">right</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</xsl:if>
									</xsl:when>
									<xsl:when test="position()>12 ">
							    </xsl:when>
									<xsl:otherwise>
											<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>