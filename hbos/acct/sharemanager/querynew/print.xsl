<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/root">
    <xsl:for-each select="table">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()!=last()">
        <div style='PAGE-BREAK-AFTER: always'></div>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="*">
    <hr class='noprint' width='100%'/>
    <div style='border:0 groove gray;overflow:hidden;width:686;height:320;padding:0'>

    <table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0'>
      <tr valign='top'>
        <td>
          <table border='0' cellpadding='0' cellspacing='0' class="tablePrintTitle2">
            <colgroup>
              <col style = "width:250"/>
              <col style = "width:186"/>
              <col style = "width:110"/>
              <col style = "width:90"/>
              <col style = "width:50"/>
            </colgroup>
            <tr height='5' valign='top'>
              <td colspan="5" style='font-size:1'>　</td>
            </tr>
            <tr height='20' valign='top'>
              <td colspan="5" class="tablePrintTitle1">记账凭证</td>
            </tr>
            <tr height='12' valign='top'>
              <td style="text-align:left" nowrap="nowrap"><xsl:value-of select="top/td[position()=1]"/></td>
              <td style="text-align:center" nowrap="nowrap">凭证日期:<xsl:value-of select="top/td[position()=3]"/></td>
              <td>凭证号:<xsl:value-of select="top/td[position()=2]"/></td>
              <td>附单据号:<xsl:value-of select="top/td[position()=4]"/></td>
              <td align='right'><xsl:value-of select="top/td[position()=5]"/></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr valign='top'>
        <td>
          <table class="printTable" bgColor="white" borderColor="black" border="1" >
            <colgroup>
              <col style = "width:180"/>
              <col style = "width:216"/>
              <col style = "width:145"/>
              <col style = "width:145"/>
            </colgroup>
            <thead>
              <tr nowrap="true" style="height:28">
                <th>摘要</th>
                <th>会计科目</th>
                <th>借方金额</th>
                <th>贷方金额</th>
              </tr>
            </thead>
            <tbody>
              <xsl:for-each select="tr">
                <tr style="height:29">
                  <xsl:for-each select="td">
                    <xsl:if test="position()=1 or position()=2">
                      <td class="normalText">
                        <div>
                          <xsl:attribute name='style'>
                            overflow:hidden;
                            height:26;
                            width:
                              <xsl:if test="position()=1">180</xsl:if>
                              <xsl:if test="position()=2">216</xsl:if>
                          </xsl:attribute>
                          <xsl:value-of select="." disable-output-escaping="yes"/>                      
                        </div>                        
                      </td>
                    </xsl:if>
                    <xsl:if test="position()=3 or position()=4">
                      <td class="numberText"><xsl:if test=". != 0"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if></td>
                    </xsl:if>
                  </xsl:for-each>
                </tr>
              </xsl:for-each>
              
              
              <xsl:call-template name="blank">
                <xsl:with-param name="trIdx" select="6 - count(tr)"/>                
              </xsl:call-template>
              
              <tr style="height:28">
                <td class="normalText" colspan='2'><xsl:value-of select="sum/td[1]"/><xsl:value-of select="sum/td[2]"/></td>
                <td style='display:none'></td>
                <td class="numberText"><xsl:value-of select="format-number(sum/td[3],'#,##0.00')"/></td>
                <td class="numberText"><xsl:value-of select="format-number(sum/td[3],'#,##0.00')"/></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr valign='top'>
        <td width='100%'>
          <table border='0' cellpadding='0' cellspacing='1' width='100%'>
            <colgroup>
              <col style = "width:137"/>
              <col style = "width:137"/>
              <col style = "width:137"/>
              <col style = "width:137"/>
              <col style = "width:137"/>
            </colgroup>
            <tr height='12'>
              <td class="tablePrintTitle2" style="text-align:left" nowrap="nowrap" width='23%'>财务主管:<xsl:value-of select="bottom/td[position()=5]"/></td>
              <td class="tablePrintTitle2" style="text-align:left" nowrap="nowrap" width='23%'>记账:<xsl:value-of select="bottom/td[position()=3]"/></td>
              <td class="tablePrintTitle2" style="text-align:left" nowrap="nowrap" width='23%'>复核:<xsl:value-of select="bottom/td[position()=1]"/></td>
              <!--<td class="tablePrintTitle2" style="text-align:left" nowrap="nowrap" width='20%'>出纳:<xsl:value-of select="bottom/td[position()=4]"/></td>-->
              <td class="tablePrintTitle2" style="text-align:right" nowrap="nowrap" width='11%'>制单:<xsl:value-of select="bottom/td[position()=2]"/></td>
            </tr>
            <tr height='12'>
              <td></td>
              <td></td>
              <td style='display:none'></td>
              <td class="tablePrintTitle2" style="text-align:right" nowrap="nowrap" colspan='3'>打印日期：<xsl:value-of select="bottom/td[position()=6]"/></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td style='font-size:1'>　</td>
      </tr>
    </table>
    </div>
  </xsl:template>
  
  
  
  <xsl:template name="blank">
    <xsl:param name="trIdx"/>
    
    <xsl:if test="$trIdx > 0">
      <tr style="height:29">
        <td></td><td></td><td></td><td></td>
      </tr>
    </xsl:if>
    
    <xsl:if test="$trIdx > 1">      
      <xsl:call-template name="blank">
        <xsl:with-param name="trIdx" select="$trIdx - 1"/>                
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>