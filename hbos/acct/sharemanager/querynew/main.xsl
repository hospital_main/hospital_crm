<?xml version='1.0' encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="total" select="count(/root/tbody/tr[1]/td)"/>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th style='display:none' >序号</th>
				<th>待摊项目编号</th>
				<th>添加待摊日期</th>
				<th>待摊项目名称</th>
				<th>财务会计凭证号</th>
				<th>预算会计凭证号</th>
				<th>待摊期限开始</th>
				<th>待摊期限截止</th>
				<th>待摊金额</th>
				<th>分期数</th>
				<th>已摊期数</th>
				<th>平均每期摊销金额</th>
				<th>剩余待摊金额</th>
				<th>摊销状态</th>
			</tr>
		</thead>  	
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=2">

								<td>
									<a href="#">
										<xsl:attribute name="onclick">
											javascript:openDialog('update.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;', 'dialogWidth:850px;dialogHeight:390px', result)
										</xsl:attribute>
										<xsl:value-of select="."/>
									</a>


								</td>
							</xsl:when>
							<xsl:when test="position()=5 or position()=6">

								<td>
									<a href="#">
										<xsl:attribute name="onclick">
											javascript:openVouchDlg('<xsl:for-each select="../pk/vouch_id">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
										</xsl:attribute>
										<xsl:value-of select="."/>
									</a>

								</td>
							</xsl:when>

							<xsl:when test="position()=14">
								<td>
									<a href="#">
										<xsl:attribute name="onclick">
											javascript:openDialog('sharemain.html?load=<xsl:for-each select="../pk/prepaid_id">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:650px;dialogHeight:420px');
										</xsl:attribute>
										<xsl:value-of select="."/>
									</a>

								</td>
							</xsl:when>


							<xsl:when test="position()=1">
								<td style='display:none'><xsl:value-of select="."/></td>
							</xsl:when>

							<xsl:when test="position()=9 or position()=12 or position()=13">
								<td><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
