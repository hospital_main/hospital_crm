<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>
  <xsl:template match="/">
    <thead>
        <xsl:variable name="total" select="count(/root/tbody/tr[1]/td)"/>
  	  <tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
            <th style="display:none">凭证编码</th>
        	<th>财务会计凭证名称</th>
        	<th>预算会计凭证名称</th>
            <th>日期</th>
            <th>金额</th>
    	</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
            <td align='center'  style=''>
                <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
                    <xsl:attribute name="value" >
                        <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
                    </xsl:attribute>
                </input>
            </td>

          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=2 or position()=3">
                  <td>
                      <a href="#">
                          <xsl:attribute name="onclick">
                              javascript:openVouchDlg('<xsl:for-each select="../pk/vouch_id">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
                          </xsl:attribute>
                          <xsl:value-of select="."/>
                      </a>
                  </td>
              </xsl:when>
              <xsl:when test="position()=1">
                  <td style="display:none">
                          <xsl:value-of select="."/>
                  </td>
              </xsl:when>
                <xsl:when test="position()=6">
                    <td align='right' class='moneyCol'>
                        <xsl:value-of select="format-number(.,'#,##0.00')"/>
                    </td>
                </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
