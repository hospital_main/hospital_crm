<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' valign="center" rowspan="3">科目编码</th>
				<th nowrap='true' valign="center" rowspan="3">科目名称</th>
				<th nowrap='true' colspan="4">科目总账</th>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' colspan="2">借方</th>
				<th nowrap='true' colspan="2">贷方</th>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>账表金额</th>
				<th nowrap='true'>明细金额</th>
				<th nowrap='true'>账表金额</th>
				<th nowrap='true'>明细金额</th>
			</tr>
		</thead>       
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=5 or position()=6 or position()=3 or position()=4">
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>