<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th>币别</th>
				<th>名称</th>
				<th>平均汇率</th>
				<th>期末汇率</th>
			</tr>    
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1 or position()=2">
									<td >
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=3 ">
									<td >
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=4">
									<td align="right" >
									<input type="text" maxlength="12" TABINDEX="-1" style="font-size:12px;border:1px solid gray;" size="20">
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
										<xsl:attribute name="name"><xsl:value-of select="../td[1]"/></xsl:attribute>	
										<xsl:attribute name="point"><xsl:value-of select="../td[5]"/></xsl:attribute>			
										<xsl:attribute name="comp_code"><xsl:value-of select="../td[6]"/></xsl:attribute>
										<xsl:attribute name="copy_code"><xsl:value-of select="../td[7]"/></xsl:attribute>
										<xsl:attribute name="acct_year"><xsl:value-of select="../td[8]"/></xsl:attribute>
										<xsl:attribute name="acct_month"><xsl:value-of select="../td[9]"/></xsl:attribute>
									</input>
									</td>
								</xsl:when>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



