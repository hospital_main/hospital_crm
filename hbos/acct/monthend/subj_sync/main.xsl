<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap='true' class='mainHead' >
			<th width="10%">待摊会计科目编码</th> 
			<th width="20%">待摊会计科目名称</th>
			<th width="10%">医疗支出科目编码</th>
			<th width="20%">医疗支出科目名称</th> 
			<th width="10%">公共卫生科目编码</th>
			<th width="20%">公共卫生科目名称</th>
    </tr>
   </thead>
   <tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          
          <xsl:for-each select="td">
              <xsl:choose>
                
                <xsl:when test="position()=1">
                  <td align='center'>
                   <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td align='center'>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
