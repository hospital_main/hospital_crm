<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>部门编码</th>
				<th nowrap='true'>部门名称</th>
				<th nowrap='true'>分摊比例</th>
			</tr>   
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
					<xsl:choose>
						<xsl:when test="position()=1">
              	<td nowrap="true">
              		<xsl:attribute name="value" >
										<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
									</xsl:attribute>
              		<xsl:value-of select="."/>
              	</td>
              </xsl:when>
            <xsl:when test="position() = 2">
							<td>
								<xsl:value-of select="."/>
							</td>
						</xsl:when>
						<xsl:when test="position()=3">
              <td nowrap="true" align="right">
                <xsl:attribute name="value" >
                  <xsl:for-each select="../*">
                    <xsl:choose>
                      <xsl:when test="position()=6">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:when>
                    </xsl:choose>
                  </xsl:for-each>                    
          		  </xsl:attribute>
            		<xsl:value-of select="format-number(.,'#,##0.00')"/>
              </td>
            </xsl:when>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

