<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>

			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:6;fontsize:maintitle'>出纳账明细表</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<!-- <td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td> -->
				</tr>
				<tr noWrap='true' class='mainHead'>
				  	<td nowrap='true'>日期</td>
				  	<td nowrap='true'>摘要</td>
				    <td nowrap='true'>附件张数</td>
				  	<td nowrap='true'>收入金额</td>
				  	<td nowrap='true'>支出金额</td>
				  	<td nowrap='true'>余额</td>
				  	<!-- <td nowrap='true'>制单人</td>
				  	<td nowrap='true'>审核人</td>
				  	<td nowrap='true'>凭证类型</td>
				  	<td nowrap='true'>凭证号</td> -->
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td[position() != 4 and position() &lt; 8]">
							<xsl:choose>
							<xsl:when test="position()=5 or position()=6 or position()=7">
								<td align="right">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
