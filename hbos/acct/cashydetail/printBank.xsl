<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:6;fontsize:maintitle'>银行账登记</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap='true' class='mainHead'>
				  	<td nowrap='true'>日期</td>
				  	<td nowrap='true'>摘要</td>
				  	<td nowrap='true'>附件张数</td>
				  	<td nowrap='true'>收入金额</td>
				  	<td nowrap='true'>支出金额</td>
				  	<td nowrap='true'>余额</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td[position() != 3 and position() !=4 and position() !=6 and position() &lt; 10]">
							<xsl:choose>
							<xsl:when test="position()=7 or position()=8 or position()=9">
								<td align="right">
									<xsl:value-of select="format-number(.,'###,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td align="left">
									<xsl:value-of select="."/>
								</td>
								
							</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
