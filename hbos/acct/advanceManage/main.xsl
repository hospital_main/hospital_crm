<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
		  <tr noWrap='true' class='mainHead'>
	      <th style=''><input type='checkbox'/></th>
					<td>预提项目编号</td>
					<td>添加预提日期</td>
					<td>预提项目名称</td>
					<td>预提期限开始</td>
					<td>预提期限截止</td>
					<td>预提固定金额</td>
					<td>分期数</td>
					<td>已提期数</td>
					<td>平均每期预提金额</td>
					<td>已预提总金额</td>
					<td>预提状态</td>
      </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:if test="td[1] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">
          <td align='center'  style=''>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          </xsl:if>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                <a href="#">
						<xsl:attribute name="onclick" >
							openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:850px;dialogHeight:562px')	
						</xsl:attribute>
						<xsl:value-of select="."/>
					    </a>
                </td>
              </xsl:when>
              <xsl:when test="position()=6 or position()=9 or position()=10">
                <td>
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>  
                </td>
              </xsl:when>
              <xsl:when test="position()=11">
                <td>
                	<a href="#">
						<xsl:attribute name="onclick" >
							openDialog('detail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:700px;dialogHeight:562px')	
						</xsl:attribute>
						<xsl:value-of select="."/>
					</a>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


