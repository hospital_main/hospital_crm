<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		  <tr noWrap='true' class='mainHead'>
	      <th style=''><input type='checkbox'/></th>
				<th noWrap="true">序号</th>
	      <th noWrap="true">财务会计凭证号</th>
				<th noWrap="true">预算会计凭证号</th>
				<th noWrap="true">日期</th>
        </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style=''>
           <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
          <xsl:choose>
							<xsl:when test="position()=2 or position()=3">
								<td>
									<a href="#">
										<xsl:attribute name="onclick">openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;")</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
								</td>
							</xsl:when>
              <xsl:otherwise>
                <td align="center"><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


