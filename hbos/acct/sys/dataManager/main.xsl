<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>单位</th>
				<th nowrap='true'>账套</th>
				<th nowrap='true'>模块</th>
				<th nowrap='true'>年度</th>
				<th nowrap='true'>备份日期</th>
				<th nowrap='true'>备份描述</th>
				<th nowrap='true'>备份文件</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<td>
							<xsl:choose>
								<xsl:when test="position() = 7">
	          				<a href='#' >
	          					<xsl:attribute name="onclick">
	          						downLoad('<xsl:value-of select="."/>')
	          					</xsl:attribute>
											<xsl:value-of select="."/>
										</a>										
								</xsl:when>
								<xsl:otherwise>
										<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>