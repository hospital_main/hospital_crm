<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
  		<th>会计科目编码</th>
  		<th>会计科目名称</th>
  		<th>显示科目余额</th>
  		<th>显示辅助核算余额</th>
	
    </tr>
   </thead>
   <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>          
          <td align='center'  style='display:none'>
            <xsl:if test='pk/checkbox_value &gt; 0'>
  						<input type='checkbox' TABINDEX='-1' checked='true'>
  							<xsl:attribute name="value" >
  								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
  							</xsl:attribute>
  								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
  						</input>
						</xsl:if>
            <xsl:if test='pk/checkbox_value &lt; 1'>
  						<input type='checkbox' TABINDEX='-1'>
  							<xsl:attribute name="value" >
  								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
  							</xsl:attribute>
  								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
  						</input>
						</xsl:if>
					</td>
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
                <xsl:when test="position()=1">
        					<a tab="-1"><xsl:value-of select="."/></a>
        				</xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
          <td align='center'  style='display:none'>
            <xsl:if test='pk/subj_value &gt; 0'>
  						<input type='checkbox' TABINDEX='-2' checked='true'>
  								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
  						</input>
						</xsl:if>
            <xsl:if test='pk/subj_value &lt; 1'>
  						<input type='checkbox' TABINDEX='-2'>
  								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
  						</input>
						</xsl:if>
					</td>
          <td align='center'  style='display:none'>
          	<xsl:if test='pk/check_value &gt; 0'>
  						<input type='checkbox' TABINDEX='-3' checked='true'>
  								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
  						</input>
						</xsl:if>
            <xsl:if test='pk/check_value &lt; 1'>
  						<input type='checkbox' TABINDEX='-3'>
  								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
  						</input>
						</xsl:if>
					</td>
        </tr>
     </xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
