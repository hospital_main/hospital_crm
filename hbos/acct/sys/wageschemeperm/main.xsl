<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
  		<th>用户编码</th>
  		<th>用户名称</th>
    </tr>
   </thead>
   <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>          
          <td align='center'  style='display:none'>
            <xsl:if test='pk/c &gt; 0'>
  						<input type='checkbox' TABINDEX='-1' checked='true'>
  							<xsl:attribute name="value" >
  								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
  							</xsl:attribute>
  								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
  						</input>
			</xsl:if>
            <xsl:if test='pk/c &lt; 1'>
  						<input type='checkbox' TABINDEX='-1'>
  							<xsl:attribute name="value" >
  								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
  							</xsl:attribute>
  								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
  						</input>
			</xsl:if>
		  </td>
          <xsl:for-each select="td">
            <td>
              <xsl:value-of select="."/>
            </td>
          </xsl:for-each>
        </tr>
     </xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
