<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap='true' class='mainHead'>
  		<th>资金来源编码</th>
  		<th>资金来源名称</th>
			<th>对应科目编码名称</th>
    </tr>
   </thead>
   <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>          
          <xsl:for-each select="td">
          	<xsl:if test="position() != 3">
	            <td>
	                <xsl:value-of select="."/>
	            </td>
            </xsl:if>
          	<xsl:if test="position() = 3">
	            <td>
	                <xsl:value-of select="."/>
	                <a href='#' onclick="setSubj(this)">
	                设置
	                </a>
	            </td>
            </xsl:if>
          </xsl:for-each>
        </tr>
     </xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>
