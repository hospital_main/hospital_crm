<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:for-each select="/root/tbody/tr">
    		<xsl:choose>
	    		<xsl:when test=" position() =2 ">
			  		<tr noWrap='true' class='mainHead'>
				  		<th>选择</th>
				  		<th>类别编码</th>
				  		<th>类别名称</th>
				  		<xsl:for-each select="td">
				  			<xsl:choose>
				  				<xsl:when test=" position() > 5 ">
						  			<th>
						  				<xsl:value-of select="."/>
						  			</th>
					  			</xsl:when>
				  			</xsl:choose>
				  		</xsl:for-each>
		    		</tr>
		    	</xsl:when>
		    </xsl:choose>
    	</xsl:for-each>
   </thead>
   <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:choose>
	    		<xsl:when test=" position()>2 ">
		        <tr>
		        	<td align='center'>
		        		<xsl:if test="td[4]='1' ">
			            <input type='checkbox' TABINDEX='-1' style='font-size:8px;display:none;'>
			              <xsl:attribute name="value" >
			                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
			              </xsl:attribute>
			              <xsl:attribute name="onclick" >
			                setCheck(this);
			              </xsl:attribute>
			              <xsl:if test="td[3]=1 ">
			              <xsl:attribute name="checked" >
			              </xsl:attribute>
			              </xsl:if>
			            </input>
	              </xsl:if>
		        		<xsl:if test="td[4]!='1' ">
			            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
			              <xsl:attribute name="value" >
			                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
			              </xsl:attribute>
			              <xsl:attribute name="onclick" >
			                setCheck(this);
			              </xsl:attribute>
			              <xsl:if test="td[3]=1 ">
			              <xsl:attribute name="checked" >
			              </xsl:attribute>
			              </xsl:if>
			            </input>
	            	</xsl:if>
		          </td>          
		          <xsl:for-each select="td">
		          	<xsl:variable name="colindex" select="position()"/>
		          	<xsl:if test="position() &lt; 3">
			            <td>
			              <xsl:value-of select="."/>
			            </td>
		            </xsl:if>
		          	<xsl:if test="position() &gt; 5">
			            <td>
			            	<xsl:attribute name="type" >
			            		<xsl:value-of select="/root/tbody/tr[1]/td[$colindex]"/>
			            	</xsl:attribute>
		            		<xsl:if test=" . = ''">
		            		　
		            		</xsl:if>
		            		
											<xsl:attribute name="ondblclick">
												clearSubj();
											</xsl:attribute>
		            		
		            		<xsl:if test=". != ''">
		                	<xsl:value-of select="."/>
		                </xsl:if>
		                <a href='#' onclick="setSubj(this)">
		                设置
		                </a>
			            </td>
		            </xsl:if>
		          </xsl:for-each>
		        </tr>
		      </xsl:when>
		    </xsl:choose>  
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
