<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
	  		<th noWrap="true" width="5%"></th>
				<th noWrap="true" width="30%">科目编码</th>
				<th noWrap="true" width="55%">科目名称</th>
			</tr>
  	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'>
							<input type='checkbox' name="selRadio" onclick="selValue(this)">
								<xsl:attribute name="acct_subj_code" ><xsl:value-of select="td[1]"/></xsl:attribute>
								<xsl:attribute name="acct_subj_name" ><xsl:value-of select="td[2]"/></xsl:attribute>
							</input>
				</td>
				<xsl:for-each select="td">  
				<td>
					<xsl:choose>
							<xsl:when test="position() = 1">
							  <xsl:value-of select="."/>
							</xsl:when>
              <xsl:when test="position() = 2">
							  <xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
					</xsl:choose>
				</td>    
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>