<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap='true' class='mainHead'>

      <th width="5%"><input type='checkbox'/></th>
			<th width="5%">序号</th>
         <th width="10%">财务会计科目类别</th>
			<th width="10%">财务会计科目编码</th>
			<th width="10%">财务会计科目名称</th>
			<th width="10%">预算会计科目类别</th> 
			<th width="10%">预算会计科目编码</th>
			<th width="10%">预算会计科目名称</th>
			<th width="30%">预算会计科目全称名称</th>
            <th width="5%">是否常用</th>
    </tr>
   </thead>
   <tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <td align='right'>
            <a tab="-1"><xsl:value-of select="position()"/></a>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                
                <xsl:when test="position()=1">
                  <td align="left">
                   <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
