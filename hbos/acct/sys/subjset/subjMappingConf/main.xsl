<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
			  		<tr noWrap='true' class='mainHead'>
				  		<th>对应类别编码</th>
				  		<th>对应类别名称</th>
				  		<th>是否分零差率</th>
				  		<th>是否分组</th>
				  		<th>是否分资金来源</th>
				  		<th>是否分仓库</th>
		    		</tr>
   </thead>
   <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<tr>
      		<td>
      			<xsl:value-of select="td[1]"/>
      		</td>
      		<td>
      			<xsl:value-of select="td[2]"/>
      		</td>
      		<td>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
							<xsl:if test="td[3]='1' ">
								<xsl:attribute name="checked" >
								</xsl:attribute>
							</xsl:if>
							<xsl:if test="td[7]!='3' ">
								<xsl:attribute name="disabled" >
								</xsl:attribute>
							</xsl:if>
						</input>
					</td>
					<td>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
							<xsl:if test="td[4]='1' ">
								<xsl:attribute name="checked" >
								</xsl:attribute>
							</xsl:if>
							<xsl:attribute name="disabled" >
							</xsl:attribute>
						</input>
					</td>
					<td>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
							<xsl:if test="td[5]='1' ">
								<xsl:attribute name="checked" >
								</xsl:attribute>
							</xsl:if>
							<xsl:attribute name="disabled" >
							</xsl:attribute>
						</input>
					</td>
					<td>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
							<xsl:if test="td[6]='1' ">
								<xsl:attribute name="checked" >
								</xsl:attribute>
							</xsl:if>
							<xsl:if test="td[7]!='6' ">
								<xsl:attribute name="disabled" >
								</xsl:attribute>
							</xsl:if>
						</input>
					</td>
				</tr> 
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
