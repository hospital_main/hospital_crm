<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<!--th style="display:none"><input type="checkbox"/></th-->
				<th noWrap="true" width="20%">类别编码</th>
				<th noWrap="true" width="20%">类别名称</th>
				<th noWrap="true" width="30%">财务会计科目</th>
				<th noWrap="true" width="30%">预算会计科目</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	  	<xsl:variable name="rowindex" select="position()"/>
	        <tr>
	        	<!--td align='center'  style='display:none'>
            	<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            	  <xsl:attribute name="value" >
            	    <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      				  </xsl:attribute>
    			  	</input>
          	</td--> 
	         <xsl:for-each select="td[position()>1]">
	          <td>
	           <xsl:choose>
							<xsl:when test="position() = 3">
								<table width="100%">
									<tr>
										<td width="80%">
											<xsl:attribute name="ondblclick">
												clearSubj();
											</xsl:attribute>
											<xsl:value-of select="."/>
										</td>
										<td>
										 <xsl:if test="../td[1]='0'">
										 	<xsl:attribute name="code" >
				            		<xsl:value-of select="/root/tbody/tr[$rowindex]/td[2]"/>
				            	</xsl:attribute>
										  [<a >
											<xsl:attribute name="onclick">
												setting(1,this);
											</xsl:attribute>
											<xsl:attribute name="href">
												#
											</xsl:attribute>
											设置
											</a>]
											</xsl:if>
										</td>
									</tr>
								</table>
							</xsl:when>
							<xsl:when test="position() = 4">
								<table width="100%">
									<tr>
										<td width="80%">
											
											<xsl:attribute name="ondblclick">
												clearSubj();
											</xsl:attribute>
											<xsl:value-of select="."/>
										</td>
										<td>
										 <xsl:if test="../td[1]='0'">
											<xsl:attribute name="code" >
				            		<xsl:value-of select="/root/tbody/tr[$rowindex]/td[2]"/>
				            	</xsl:attribute>
										  [<a >
											<xsl:attribute name="onclick" >
												setting(2,this);
											</xsl:attribute>
											<xsl:attribute name="href">
												#
											</xsl:attribute>
											设置
											</a>]
											</xsl:if>
										</td>
									</tr>
								</table>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
					</xsl:choose> 
					 
	       </td>
	     
		   </xsl:for-each>
		   
  		</tr>
   	    </xsl:for-each>
 	</tbody>
  </xsl:template>
</xsl:stylesheet>