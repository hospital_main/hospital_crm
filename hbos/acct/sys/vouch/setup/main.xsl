<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none2'></th>
				<th nowrap='true'>类别编码</th>
				<th nowrap='true'>类别名称</th>
				<th nowrap='true'>科目编码</th>
				<th nowrap='true'>科目全称</th>
			</tr>    
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'>
						<input type='checkbox' onclick="updateCheckBox(this)" setdis="0">
							<xsl:attribute name="value"><xsl:value-of select="td[1]"/></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<td>
							<xsl:value-of select="."/>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>

