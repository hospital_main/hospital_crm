<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>编码</th>
  			<th nowrap='true'>名称</th>
  			<th nowrap='true'>级别</th>
  			<th nowrap='true'>是否选择</th>
  		</tr>
  	</thead>
  	
  	<tbody>
  		<xsl:variable name="select">select</xsl:variable>
  		<xsl:for-each select="/root/tbody/tr">
        <xsl:variable name="row"><xsl:value-of select="position()"/></xsl:variable>
  		
        <tr>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                    <xsl:value-of select="../td[7]"/>
                	<xsl:choose>
	                	<xsl:when test="../td[5]=0">
	                		<img src="../../../../../base/themes/blue/images/treetable/folderopen.png" border="0"/>
	                	</xsl:when>
	                	<xsl:otherwise>
	                		<img src="../../../../../base/themes/blue/images/treetable/file.png" border="0"/>
	                	</xsl:otherwise>
                	</xsl:choose>
                	<xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:when test="position()=4">
                <td align='center'><div>                	
                	<input type="checkbox" TABINDEX='-1' style='font-size:8px;'>
                		
             			<xsl:attribute name="value">
			            <xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
			            </xsl:attribute>			              
			            <xsl:if test="../td[4]=1">
							<xsl:attribute name="checked">true</xsl:attribute>
						</xsl:if>
						<xsl:if test="../td[8]='1'">
							<xsl:attribute name="disabled">true</xsl:attribute>
						</xsl:if>
						<xsl:attribute name="id">
						  	<xsl:value-of select="$select"/><xsl:value-of select="../td[6]"/>
						</xsl:attribute>
						<xsl:attribute name="onclick">
							javascript:checkSelect(this.parentNode.parentNode.parentNode,this);edit_flag=true;
						</xsl:attribute>

						
					</input></div>

                </td>
              </xsl:when>
              <xsl:when test="position()>4">
                <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>