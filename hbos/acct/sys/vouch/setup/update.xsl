<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th width='25'><input type='checkbox'/></th>
      	<th nowrap='true'>摘要</th>
      	<th nowrap='true'>科目</th>
      	<th nowrap='true'>方向</th>
      	
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox'/>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=2">
                <td>
                  <a href='#'>
                    <xsl:attribute name="onclick">openDialog('dupdate.html','dialogWidth:480px;dialogHeight:250px', this);</xsl:attribute>
                    <xsl:value-of select="."/>
                  </a>
                </td>
              </xsl:when>
              <xsl:when test="position()=1 or position()=3">
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


