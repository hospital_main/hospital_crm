<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>选择</th>
				<th>科目编码</th>
				<th>科目名称</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				<xsl:for-each select="td">
				<xsl:choose>
					<xsl:when test="position()=1">
				      <td align='center'>
    	  	  			<input type='radio' name='select' onclick='choose(this)'>
			              <xsl:attribute name="subj_code" >
	                 	 <xsl:value-of select="../pk/acct_subj_code"/>
			      			  </xsl:attribute>
			      			  <xsl:attribute name="subj_name" >
	                 	 <xsl:value-of select="../pk/acct_subj_name"/>
			      			  </xsl:attribute>
			      			</input>
	   					</td>
	            <td  noWrap='true' >
	            <xsl:value-of select="."/>
	            </td>
					</xsl:when>
					<xsl:otherwise>
						<td><xsl:value-of select="."/></td>
					</xsl:otherwise>
				</xsl:choose>					
				</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
