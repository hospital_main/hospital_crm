<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th nowrap='true'>序号</th>
		  	<th nowrap='true'>摘要</th>
		  	<th nowrap='true'>借贷方向</th>
		  	<th nowrap='true'>设置</th>
		  	<th nowrap='true'>科目编码</th>
		  	<th nowrap='true'>科目名称</th>
      </tr>
    </thead>
    <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
      <xsl:for-each select="td">
          <xsl:choose>
					  <xsl:when test=" position()=4">
					  	<xsl:if test="'1' = ../pk/is_auto_create">
									<td nowrap="true">
										<a href='#'>
											<xsl:attribute name="onclick" >
												javascript:openItem("<xsl:for-each select='../pk/mod_code'><xsl:value-of select='.'/></xsl:for-each>");
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
							</xsl:if>
					  	<xsl:if test="'0' = ../pk/is_auto_create">
									<td nowrap="true">
										<a href='#'>
										<xsl:attribute name="onclick" >
											javascript:openDialog("set.html?load=<xsl:for-each select='../pk/*'>&lt;<xsl:value-of select='name()'/>&gt;<xsl:value-of select='.'/>&lt;/<xsl:value-of select='name()'/>&gt;</xsl:for-each>","dialogWidth:600px;dialogHeight:600px",result);
										</xsl:attribute>
										<xsl:value-of select="."/>
										</a>
									</td>
							</xsl:if>
					  </xsl:when>
            <xsl:otherwise>
              <td><xsl:value-of select="."/></td>
            </xsl:otherwise>
          </xsl:choose>
      </xsl:for-each>
      </tr>
    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>