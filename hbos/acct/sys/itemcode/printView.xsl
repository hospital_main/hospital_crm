<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  <root>
    <thead>
    	 <tr noWrap='true' class='mainHead' >
  		<td  style="colspan:17;fontsize:maintitle">会计科目列表</td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
    </tr>
     <tr noWrap='true' class='mainHead' >
  		<td rowspan="1" colspan="7">会计科目</td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td rowspan="2">现金流量</td>
  		<td colspan="9">辅助核算</td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
  		<td style='display:none'></td>
    </tr>
    <tr noWrap='true' class='mainHead'>
    		<td>编码</td>
    		<td>名称</td>
  	  	<td>全称</td>
  	  	<td>类别</td>
  	  	<td>性质</td>
  	  	<td>往来类别</td>
  	  	<td>方向</td>
  	  	<td style='display:none'></td>
    		<td>是否</td>
    		<td>核算1</td>
    		<td>核算2</td>
    		<td>核算3</td> 
    		<td>核算4</td>
    		<td>核算5</td>
    		<td>核算6</td>
    		<td>核算7</td>
    		<td>核算8</td>  
    </tr>
   </thead>
   
   <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>                  
          <xsl:for-each select="td">
            <td>
               <xsl:value-of select="."/>
            </td>
          </xsl:for-each>
        </tr>
     </xsl:for-each>   
 	</tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>
