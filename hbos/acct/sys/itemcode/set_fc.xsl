<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap='true' class='mainHead'>
     	<th>ѡ��</th>
  		<th>���ֱ���</th>
  		<th>��������</th>
  		<th>�Ƿ���ĩ����</th>
  	</tr>
   </thead>
   <tbody>
   <xsl:for-each select="/root/tbody/tr">
    <xsl:variable name='CurTrPos' select='position()'/>
   <tr>

   		<xsl:for-each select="td">
			<xsl:choose>
              <xsl:when test="position() = 1">
              	<td align='center'>
              	<input type='radio' TABINDEX='-1'>
              	<xsl:attribute name="name" >selectedid_1</xsl:attribute>
              	<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
              	</input>
				</td>
              </xsl:when>

              <xsl:when test="position() = 4">
              	<td align='center'>
              	<input type='checkbox' TABINDEX='-1'  class='vhcheckBox'>
              	<xsl:attribute name="name" >is_qmth_<xsl:value-of select="$CurTrPos"/></xsl:attribute>
              	<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
              	<xsl:if test=".='1'">
              	<xsl:attribute name="checked">true</xsl:attribute>
              	</xsl:if>
              	</input>
				</td>
              </xsl:when>

              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
        	</xsl:choose>
    	</xsl:for-each>
    </tr>
    </xsl:for-each>
	</tbody>
   </xsl:template>
</xsl:stylesheet>
