<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    <tr noWrap='true' class='mainHead' >
		<th rowspan="2" width="80"  align="center">科目编码</th>
		<th rowspan="2" width="180" align="center" >科目名称</th>
		<th rowspan="2" width="100" align="center" >科目余额</th>
		<th rowspan="2" width="40"  align="center" >方向</th>
		<th colspan="3" width="360" align="center" >来源信息</th>
		<th rowspan="2" width="40"  align="center" >备注</th>
    </tr>
    <tr noWrap='true' class='mainHead'>
    <th rowspan="1"  width="100"  align="center" >来源科目编码</th>
		<th rowspan="1"  width="180" align="center" >来源科目名称</th>
    <th rowspan="1" width="100"  align="center" >期末余额</th>
    </tr>
   </thead>
   <tbody>
      <xsl:for-each select="/root/tbody/tr">
          <xsl:variable name="new_Subj_code" select="td[1]"/>
          <xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[1]=$new_Subj_code])"/>
          <xsl:variable name="rowPos" select="position()"/>
        <tr>          
          <xsl:for-each select="td">
              <xsl:choose>
                  <xsl:when test="position() = 1">
                      <xsl:if test="$rowPos = 1">
                          <td align="left">
                              <xsl:if test="$new_Subj_code != ''">
                                  <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
                              </xsl:if>
                              <xsl:value-of select="."/>
                          </td>
                      </xsl:if>

                      <xsl:if test="$rowPos &gt; 1">
                          <xsl:if test="$new_Subj_code != ../../tr[$rowPos - 1 ]/td[1]">
                              <td align="left">
                                  <xsl:if test="$new_Subj_code != ''">
                                      <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
                                  </xsl:if>
                                  <xsl:value-of select="."/>
                              </td>
                          </xsl:if>

                          <xsl:if test="$new_Subj_code = ../../tr[$rowPos - 1 ]/td[1]">
                              <xsl:if test="$new_Subj_code != ''">
                                  <td align="center" style="display:none" >
                                  </td>
                              </xsl:if>
                              <xsl:if test="$new_Subj_code = ''">
                                  <td align="left" >
                                      <xsl:value-of select="../td[1]"/>
                                  </td>
                              </xsl:if>
                          </xsl:if>
                      </xsl:if>
                  </xsl:when>

                  <xsl:when test="position() = 2">
                      <xsl:if test="$rowPos = 1">
                          <td align="left">
                              <xsl:if test="$new_Subj_code != ''">
                                  <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
                              </xsl:if>
                              <xsl:value-of select="."/>
                          </td>
                      </xsl:if>

                      <xsl:if test="$rowPos &gt; 1">
                          <xsl:if test="$new_Subj_code != ../../tr[$rowPos - 1 ]/td[1]">
                              <td align="left">
                                  <xsl:if test="$new_Subj_code != ''">
                                      <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
                                  </xsl:if>
                                  <xsl:value-of select="."/>
                              </td>
                          </xsl:if>

                          <xsl:if test="$new_Subj_code = ../../tr[$rowPos - 1 ]/td[1]">
                              <xsl:if test="$new_Subj_code != ''">
                                  <td align="left" style="display:none" >
                                  </td>
                              </xsl:if>
                              <xsl:if test="$new_Subj_code = ''">
                                  <td align="left" >
                                      <xsl:value-of select="."/>
                                  </td>
                              </xsl:if>
                          </xsl:if>
                      </xsl:if>
                  </xsl:when>


                  <xsl:when test="position() = 3">
                      <xsl:if test="$rowPos = 1">
                          <td align="right">
                              <xsl:if test="$new_Subj_code != ''">
                                  <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
                              </xsl:if>
                             <xsl:value-of select="format-number(.,'#,##0.00')"/>
                          </td>
                      </xsl:if>

                      <xsl:if test="$rowPos &gt; 1">
                          <xsl:if test="$new_Subj_code != ../../tr[$rowPos - 1 ]/td[1]">
                              <td align="right">
                                  <xsl:if test="$new_Subj_code != ''">
                                      <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
                                  </xsl:if>
                                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                              </td>
                          </xsl:if>

                          <xsl:if test="$new_Subj_code = ../../tr[$rowPos - 1 ]/td[1]">
                              <xsl:if test="$new_Subj_code != ''">
                                  <td align="right" style="display:none" >
                                  </td>
                              </xsl:if>
                              <xsl:if test="$new_Subj_code = ''">
                                  <td align="right" >
                                      <xsl:value-of select="format-number(.,'#,##0.00')"/>
                                  </td>
                              </xsl:if>
                          </xsl:if>
                      </xsl:if>
                  </xsl:when>
                  <xsl:when test="position()=7">
                      <td align="right" >
                      	<xsl:if test=". != ''">
                         <xsl:value-of select="format-number(.,'#,##0.00')"/>
                         </xsl:if>
                      </td>
                  </xsl:when>
                  <xsl:when test="position() = 8">
                      <xsl:if test="$rowPos = 1">
                          <td align="center">
                              <xsl:if test="$new_Subj_code != ''">
                                  <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
                              </xsl:if>
                             <a href="#">
                                <xsl:attribute name="onclick">
                                     javascript:openDialog('insert.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:650px;dialogHeight:320px');
                                </xsl:attribute>
                                <xsl:value-of select="."/>
                             </a>
                          </td>
                      </xsl:if>

                      <xsl:if test="$rowPos &gt; 1">
                          <xsl:if test="$new_Subj_code != ../../tr[$rowPos - 1 ]/td[1]">
                              <td align="center">
                                  <xsl:if test="$new_Subj_code != ''">
                                      <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
                                  </xsl:if>
                                  <a href="#">
                                   <xsl:attribute name="onclick">
                                     javascript:openDialog('insert.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:650px;dialogHeight:320px');
                                   </xsl:attribute>
                                   <xsl:value-of select="."/>
                                  </a>
                              </td>
                          </xsl:if>

                          <xsl:if test="$new_Subj_code = ../../tr[$rowPos - 1 ]/td[1]">
                              <xsl:if test="$new_Subj_code != ''">
                                  <td align="center" style="display:none" >
                                  </td>
                              </xsl:if>
                              <xsl:if test="$new_Subj_code = ''">
                                  <td align="center" >
                                      <a href="#">
                                        <xsl:attribute name="onclick">
                                         javascript:openDialog('insert.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:650px;dialogHeight:320px');
                                        </xsl:attribute>
                                        <xsl:value-of select="."/>
                                      </a>
                                  </td>
                              </xsl:if>
                          </xsl:if>
                      </xsl:if>
                  </xsl:when>
                   
                <xsl:otherwise>
                    <td align="left" ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
     </xsl:for-each>
    
 	</tbody>
	</xsl:template>
</xsl:stylesheet>
