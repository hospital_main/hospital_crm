<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  	    <xsl:variable name="colNum" select="count(//tr[1]/td)"/>
  		<tr noWrap='true' class='mainHead'>
		<th rowspan="2" valign="center">科目编码</th>
		<th rowspan="2" valign="center">科目名称</th>
		<th colspan="2">年初余额</th>
		<xsl:if test=" $colNum &gt; 4 ">
			<th colspan="2">本年累计</th>
			<th colspan="2">期初余额</th>
		</xsl:if>
		</tr>
  		<tr noWrap='true' class='mainHead'>
		<th>借方</th>
		<th>贷方</th>
		<xsl:if test=" $colNum &gt; 4 ">
			<th>借方</th>
			<th>贷方</th>
			<th>借方</th>
			<th>贷方</th>
		</xsl:if>
		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
  			<tr>
		        <xsl:for-each select="td">
		           <xsl:choose>
					  <xsl:when test="position() &gt; 2 ">
						  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				    </xsl:when>
				     <xsl:when test="position()=1">
		          	<td>
		          		<xsl:if test="(../td[2])!=''">
         					<xsl:value-of select="."/>
         				  </xsl:if>
     
         				  <xsl:if test="(../td[2])=''">
         				  	<span style="font-size:13px;font-weight:bold;">
         					<xsl:value-of select="."/>
         					</span>
         				  </xsl:if>
        			  </td>
							</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
		           </xsl:choose>
		  		</xsl:for-each>  				
  			</tr>
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>