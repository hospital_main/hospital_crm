<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
     <tr noWrap='true' class='mainHead' >

        <th style='display:none' rowspan="2"><input type='checkbox'/></th>
  		<th rowspan="1" colspan="8">会计科目</th>
  		
		<th rowspan="2" width="80">现金流量</th>
		<th rowspan="1" style="display:none">外币</th>
		<th rowspan="1" width="0" style="display:none">核算数量</th>
		<th colspan="9" width="80">辅助核算</th>
		  	
    </tr>
    <tr noWrap='true' class='mainHead'>
    		<th rowspan="1" width="80">编码</th>
  		<th rowspan="1" width="80">名称</th>
	  	<th rowspan="1" width="80">全称</th>
	  	<th rowspan="1" width="80">类别</th>
	  	<th rowspan="1" width="120">会计核算类别</th>
	  	<th rowspan="1" width="80">性质</th> 
	  	<th rowspan="1" width="80">往来类别</th>
	  	<th rowspan="1" width="80">方向</th>
    		<th width="80">是否</th>
    		<th width="80">核算1</th>
		<th width="80">核算2</th>
		<th width="80">核算3</th> 
		<th width="80">核算4</th>
		<th width="80">核算5</th>
		<th width="80">核算6</th>
		<th width="80">核算7</th>
		<th width="80">核算8</th>  
    </tr>
   </thead>
   <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>          
          <td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
							<!--<xsl:if test="td[11]='1'">-->
								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
							<!--</xsl:if>
							<xsl:if test="td[11]!='1'">
								<xsl:attribute name="style" >font-size:8px;display:none</xsl:attribute>
							</xsl:if>-->
						</input>
					</td>

          
          
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
                <xsl:when test="position()=1">
					<a tab="-1"><xsl:value-of select="."/></a>
				</xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
     </xsl:for-each>
    
 	</tbody>
	</xsl:template>
</xsl:stylesheet>
