<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
		<th rowspan="2">部门</th>
		<th rowspan="2">项目</th>
		<th colspan="2">方向</th>
		<th colspan="2">年初余额</th>
		<th colspan="2">借方累计</th>
		<th colspan="2">贷方累计</th>
		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
  			<tr>
          <xsl:for-each select="td">
            <td >
			
		       <xsl:value-of select="."/>
			  
            </td>
  			  </xsl:for-each>  				
  			</tr>
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>