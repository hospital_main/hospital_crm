<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="total" select="count(/root/tbody/tr[1]/td)"/>
    <tr noWrap='true' class='mainHead'>
    	<th style='display:none' rowspan="2"><input type='checkbox'/></th>
    	<th>调整凭证号</th>    	
	  	<th>摘要</th>
    	<th>科目编码</th>
  		<th>科目名称</th>
	  	<th>借方金额</th>
	  	<th>贷方金额</th>
    </tr>
   </thead>
   <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>          
          <td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
						</input>
					</td>
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
              
                <xsl:when test="position()=1">
				        	<a tab="-1"><xsl:value-of select="."/></a>
				        </xsl:when>
				        <xsl:when test="position()=5 or position()=6 ">
								
								<xsl:if test=". != ''">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:if>
								
							</xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr>
     </xsl:for-each>
    
 	</tbody>
	</xsl:template>
</xsl:stylesheet>
