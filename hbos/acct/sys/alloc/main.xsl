<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style=''>部门编码</th>
				<th nowrap='true'>部门名称</th>
				<th nowrap='true'>分摊比例</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=3">
								<td>
									<input class="inputDecimal" name="abc" point="2">
										<xsl:attribute name="deptCode"><xsl:value-of select="../td[1]"/></xsl:attribute>
										<xsl:attribute name="oldValue"><xsl:value-of select="."/></xsl:attribute>
										<xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
									</input>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td width="50%"><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>