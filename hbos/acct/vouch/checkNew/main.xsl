<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
			<xsl:variable name="chkState" select="/root/tbody/tr[1]/td[10]"/>
			
  		<tr noWrap='true' class='mainHead'>
  		  <xsl:if test="$chkState!=''"> 
        <th style='display:none'><input type='checkbox'/></th>
        </xsl:if>
  		  <xsl:if test="$chkState=''"> 
        </xsl:if>
  			<th noWrap='true'>凭证日期</th>
  			<th noWrap='true'>财务会计凭证号</th>
  			<th noWrap='true'>预算会计凭证号</th> 
  			<th noWrap='true'>附件</th>
  			<th noWrap='true'>制单人</th>
  			<th noWrap='true'>审核人</th>
  			<th noWrap='true'>记账人</th> 
  			<th noWrap='true'>标错人</th> 
  			<th noWrap='true'>标错信息</th> 
  		</tr>
  	</thead>
  	<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				  <xsl:if test="$chkState!=''">
  					<td align='center'  style='display:none'>
  						<input type='checkbox' TABINDEX='-1'>
  							<xsl:attribute name="value" >
  								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
  							</xsl:attribute>
  						</input>
  					</td>
					</xsl:if>
				  <xsl:if test="$chkState =''">
  					<td align='center'  style='display:none'>
  					</td>
					</xsl:if>
					<xsl:for-each select="td[position()&lt; 10]">
						<td >
							<xsl:choose>
								<xsl:when test="position()=2">
									<a href="#">
										<xsl:attribute name="onclick">getCond("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;+subjField,sumField,prePageBtn,nextPageBtn,vouchCheckBtn2&lt;/edit_mask&gt;")</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
									<xsl:if test=". !='' and ../td[last()-1]='1'"><img src="err.gif"/></xsl:if>
								</xsl:when>
								<xsl:when test="position()=3">
									<a href="#">
										<xsl:attribute name="onclick">getCond("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;+subjField,sumField,prePageBtn,nextPageBtn,vouchCheckBtn2&lt;/edit_mask&gt;")</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
									<xsl:if test=". !='' and ../td[last()-1]='1'"><img src="err.gif"/></xsl:if>
								</xsl:when>
								<xsl:when test="position()=4">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



