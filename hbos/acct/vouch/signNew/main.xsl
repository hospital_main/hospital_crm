<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>财务会计凭证号</th>
				<th>预算会计凭证号</th>
				<th>凭证日期</th>
				<th>附件</th>
				<th>摘要</th>
				<th>借方金额</th>
				<th>贷方金额</th>
				<th>制单人</th>
				<th>签字状态</th>
				<th>签字人</th>
				<th>审核人</th>
				<th>记账人</th>
			</tr>
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
	          <td align='center'  style='display:none'>
	          <xsl:if test="td[1]!= '合计'">
						<input type='checkbox' TABINDEX='-1' align='center'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
						</input>
						</xsl:if>
						<xsl:if test="td[1]='合计'">
							<input type='checkbox' TABINDEX='-1' align='center'>
								<xsl:attribute name="disabled" >true</xsl:attribute>
							</input>
						</xsl:if>
					</td>
					<xsl:for-each select="td[position() &lt;13 ]">
						<td>
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". != '合计'">
							        <a href="#">
												<xsl:attribute name="onclick">
													javascript:openVouchDlg("<xsl:for-each select="../pk/vouch_id">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;+&lt;/edit_mask&gt;")
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
					            <xsl:if test="../td[13]='1'"><img src="error.png"/></xsl:if>
					         </xsl:if>
	               <xsl:if test=". = '合计'">
						        <xsl:value-of select="."/>  
					       </xsl:if>
								</xsl:when>
								<xsl:when test="position()=5 ">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:when test="position()=6 or position()=7">
									<xsl:if test=". != 0">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
										<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
