<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style="colspan:colcount;fontsize:maintitle"></td>
					<td nowrap='true' style="display:none"></td>
					<td nowrap='true' style="display:none"></td>
					<td nowrap='true' style="display:none"></td>
					<td nowrap='true' style="display:none"></td>
					<td nowrap='true' style="display:none"></td>
					<td nowrap='true' style="display:none"></td>
					<td nowrap='true' style="display:none"></td>
					<td nowrap='true' style="display:none"></td>
					<td nowrap='true' style="display:none"></td>
					<td nowrap='true' style="display:none"></td>
				</tr>   
				<tr noWrap='true' class='mainHead'>
					<th nowrap='true'>财务会计凭证号</th>
					<th nowrap='true'>预算会计凭证号</th>
					<td nowrap='true'>凭证日期</td> 
					<td nowrap='true'>附件</td> 
					<td nowrap='true'>摘要</td> 
					<th nowrap='true'>财务会计科目</th> 
					<th nowrap='true'>预算会计科目</th> 
					<td nowrap='true'>借方金额</td>
					<td nowrap='true'>贷方金额</td> 
					<td nowrap='true'>制单人</td> 
					<td nowrap='true'>审核人</td> 
					<td nowrap='true'>记账人</td>  
					<td nowrap='true'>作废</td> 
				</tr>   
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">   
							<td>
								<xsl:choose>
									<xsl:when test="position() = 1 ">
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position() = 8 or position()=9">
										<xsl:if test=".!=0">
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</xsl:if>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="."/>  
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
