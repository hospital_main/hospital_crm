<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>顺序号</th>
				<th nowrap='true'>财务会计科目编号</th> 
				<th nowrap='true'>财务会计科目</th> 
				<th nowrap='true'>预算会计科目编号</th> 
				<th nowrap='true'>预算会计科目</th> 
				<th nowrap='true'>借贷方向</th> 
				<th nowrap='true' style='display:none'>部门</th> 
				<th nowrap='true'>人员类别</th> 
				<th nowrap='true'>公式</th> 
				<th nowrap='true'>取借贷平衡数</th> 
			</tr>     
		</thead>       
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
						<xsl:choose>
							<xsl:when test="position()=7">
								<td>
                	<xsl:attribute name="style" >display:none;</xsl:attribute><xsl:value-of select="."/>
  	          	</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>  
							</xsl:otherwise>
						</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

