<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>财务会计凭证号</th>
				<th>预算会计凭证号</th>
				<th>日期</th>
				<th>附件</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>记账人</th>
				<xsl:if test="substring( /root/tbody/tr[1]/td[8] ,1,1)!='-'">
					<th style='display:none'>单据号</th>
				</xsl:if>
			</tr>                   
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
							<xsl:if test="td[9]='1'">
								<xsl:attribute name="disabled">true</xsl:attribute>
							</xsl:if>
						</input>
					</td>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1 or position()=2">
									<td>
										<a href="#">
											<xsl:attribute name="onclick">openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;")</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=8">
								<xsl:if test="substring( /root/tbody/tr[1]/td[8] ,1,1)!='-'">
	          			<td style='display:none'>
		          			<xsl:if test="'0401'= ../td[last()-1] or '0404'= ../td[last()-1] or '0405'= ../td[last()-1] or '0408'= ../td[last()-1] or '0410'= ../td[last()-1] or '0412'= ../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/barcode/query/mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0402'= ../td[last()-1] or '0402'= ../td[last()-1] or '0406'= ../td[last()-1] or '0407'= ../td[last()-1] or '0409'= ../td[last()-1] or '0411'= ../td[last()-1] or '0413'= ../td[last()-1] or '0416'= ../td[last()-1] or '0419'= ../td[last()-1] or '0421'= ../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/barcode/query/mate/whr/smp/in/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0403'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/barcode/query/mate/whr/out/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0414'=../td[last()-1] or '0415'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/barcode/query/mate/whr/out/move/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0417'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/barcode/query/mate/whr/second/advice/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0418'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/barcode/query/mate/whr/smp/out/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0420'=../td[last()-1] or '0423'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/barcode/query/mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0424'=../td[last()-1] or '0425'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/barcode/query/mate/pay/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0501'=../td[last()-1] or '0502'=../td[last()-1] or '0503'=../td[last()-1] or '0506'=../td[last()-1] or '0507'=../td[last()-1] or '0508'=../td[last()-1] or '0509'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openPage('<xsl:value-of select="../td[last()]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0505'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openPage('<xsl:value-of select="../td[last()]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0504'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../equip/change/back/medicalEquip/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0519'=../td[last()-1] or '0511'=../td[last()-1] or '0512'=../td[last()-1] or '0514'=../td[last()-1] or '0515'=../td[last()-1] or '0516'=../td[last()-1] or '0517'=../td[last()-1] or '0518'=../td[last()-1] or '0520'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openReducePage('<xsl:value-of select="../td[last()]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0513'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openPKPage('<xsl:value-of select="../td[last()]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0523'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openJCJLPage('<xsl:value-of select="../td[last()]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0524'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openZCKCPage('<xsl:value-of select="../td[last()]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0525'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openZCBYPage('<xsl:value-of select="../td[last()]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
		          		</td>
		          		</xsl:if>
		          	</xsl:when>
								<xsl:when test="position()=9 or position()=last() or position()=last()-1">
								</xsl:when>
								<xsl:otherwise>
									<td >
									<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



