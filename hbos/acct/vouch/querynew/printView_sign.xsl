<?xml version="1.0" encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	    <thead>
	    <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
        <td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
  	  </tr> 	
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
        <td style="display:none"/>
		  <td style="display:none"/>
		  <td style="display:none"/>
  	  </tr>   	
	  	  <tr noWrap='true' class='mainHead'>
	  			<td>会计凭证号</td>
			  	<td>预算凭证编号</td>
					<td>凭证日期</td>
					<td>附件</td>
					<td>摘要</td>
					<td>财务会计借方金额</td>
					<td>财务会计贷方金额</td>
          <td>预算会计借方金额</td>
					<td>预算会计贷方金额</td>
					<td>制单人</td>
					<td>签字人</td>
					<td>审核人</td>
					<td>记账人</td>
					<td>标错信息</td>
					<td>是否标记</td>
	      </tr>
      </thead>
	  	<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=1">
					          <td>
										<xsl:if test=". != '合计'">
											<xsl:value-of select="."/>
						         </xsl:if>
		                <xsl:if test=". = '合计'">
							        <xsl:value-of select="."/>
						        </xsl:if>
                    </td>
									</xsl:when>
                  <xsl:when test="position()=6 or position()=7 or position()=8 or position()=9">
										<td noWrap='true' align="right">
										  <xsl:if test=". != 0">
											  <xsl:value-of select="format-number(.,'#,##0.00')"/>
										  </xsl:if>
                    </td>
									</xsl:when>
                  <xsl:when test="position()=16">
                  </xsl:when>
                  <xsl:when test="position()&gt;16 ">
							    </xsl:when>
                  <xsl:otherwise>
                    <td>
									      <xsl:value-of select="."/>
								    </td>
									</xsl:otherwise>
                </xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>