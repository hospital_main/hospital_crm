<?xml version='1.0' encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="total" select="count(/root/tbody/tr[1]/td)"/>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th nowrap='true' valign='middle'>财务会计凭证号</th>
		  	<th nowrap='true' valign='middle'>预算会计凭证号</th>
		  	<th nowrap='true' valign='middle'>凭证日期</th>
		  	<th nowrap='true' valign='middle'>盈余与预算结余差异类</th>
		  	<th nowrap='true' valign='middle'>盈余与预算结余差异项</th>
		  	<th nowrap='true' valign='middle'>流入金额</th>
		  	<th nowrap='true' valign='middle'>流出金额</th>
			</tr>
		</thead>  	
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 0">
     		 <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		              	<xsl:when test="position() = 1 or position() = 2">
                <td align="center">
                	<xsl:attribute name="style" >display:none;</xsl:attribute><xsl:value-of select="."/>
  	          	</td>
  	          	</xsl:when>
		              	<xsl:when test="position() = 3 ">
		              		<xsl:if test=".='总合计'">
				            		  <td noWrap="true" align='left'  style="background-color:lightblue;font-weight:bold">
				            		      <xsl:value-of select="."/>
				            		   </td>
				            	</xsl:if>
				            	<xsl:if test=".!='总合计'">
				                <td align="left">
				                <a tabindex='-1'>
				                  <xsl:attribute name="href" >
				    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../td[1]"/>&lt;/vouch_id&gt;');
				  	          		</xsl:attribute><xsl:value-of select="."/></a>
				  	          	</td>
			  	          	</xsl:if>
			              </xsl:when>
			              <xsl:when test="position() = 4 ">
			                <td align="left">
			                 <a tabindex='-1'>
				                  <xsl:attribute name="href" >
				    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../td[1]"/>&lt;/vouch_id&gt;');
				  	          		</xsl:attribute><xsl:value-of select="."/></a>
			  	          	</td>
			              </xsl:when>
		                	                
		                <xsl:when test="position() &gt; 7">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>		                
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
	  	  	</tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>
