<?xml version='1.0' encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <xsl:variable name="colNum" select="count(//tr[1]/td)"/> 
    	<xsl:for-each select="/root/tbody/tr[td[last()]='0']">
    	  <tr noWrap='true' class='mainHead'>
	          <xsl:for-each select="td[position() &lt; last()]">
				      	<th nowrap='true'><xsl:value-of select="."/></th>
	          </xsl:for-each>
        	</tr>
    	</xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[td[last()]='1']">
        <tr>
  		    <xsl:for-each select="td[position() &lt; last()]">
  		    	<xsl:variable name="colindex" select=" position() "/>
  		    	<xsl:variable name="colname" select="/root/tbody/tr[1]/td[$colindex]"/>
  		        <xsl:if test="position() = last()">
  		        <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
  		        </xsl:if>
  		        <xsl:if test="position() &lt; last() ">
  		          <td>
  		          	<xsl:if test=" $colname = '��ͬ���'">
	  		          	<a href='#'>
	                    <xsl:attribute name="onclick">openPact('<xsl:value-of select="."/>');</xsl:attribute>
	                    <xsl:value-of select="."/>
	                  </a>
	                </xsl:if>
	                <xsl:if test=" $colname != '��ͬ���'">
	  		          	<xsl:value-of select="."/>
	                </xsl:if>
  		          </td>
  		        </xsl:if>
  	  		</xsl:for-each>
  	  	</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
