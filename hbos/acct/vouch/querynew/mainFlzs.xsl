<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="total" select="count(/root/tbody/tr[1]/td)"/>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>凭证编号</th>
				<th>凭证日期</th>
				<th>附件</th>
				<th>摘要</th>
				<th>科目编码</th>
				<th>科目名称</th>
				<th>借方金额</th>
				<th>贷方金额</th>
				<th>制单人</th>
				<xsl:if test="$total =24">
					<th>签字人</th>
				</xsl:if>
				<th>审核人</th>
				<th>记账人</th>
				<th>标错信息</th>
				<th>是否标记</th>
				<th>时间差异</th>
				<th>是否关联</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<xsl:if test="td[1]!= '合计'">
							<input type='checkbox' TABINDEX='-1' align='center'>
								<xsl:attribute name="value" >
									<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
								</xsl:attribute>
								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
							</input>
						</xsl:if>
						<xsl:if test="td[1]='合计'">
							<td align='center'></td>
						</xsl:if>
					</td>
					<xsl:for-each select="td[($total = 24 and position() &lt;= 15) or ($total = 23 and position()&lt;= 14) ]">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
									<xsl:if test=". != '合计'">
										<a href="#">
											<xsl:attribute name="onclick">
												openMainVouch("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
										<xsl:if test=". != '' and ../td[($total = 24 and position() = 16) or ($total = 23 and position() = 15)]!=''"><img src="chong.gif"/></xsl:if>
										<xsl:if test=". != '' and ../td[($total = 24 and position() = 17) or ($total = 23 and position() = 16)]='1'"><img src="fei.gif"/></xsl:if>
										<xsl:if test=". != '' and ../td[($total = 24 and position() = 19) or ($total = 23 and position() = 18)]='1'"><img src="err.gif"/></xsl:if>
									</xsl:if>
									<xsl:if test=". = '合计'">
										<xsl:value-of select="."/>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()=4">
								<td style="width:300px; overflow:hidden">
									<div style="width:300px; word-break:break-all;">
										<xsl:value-of select="."/>
									</div>
								</td>
							</xsl:when>
							<xsl:when test="position()=7 or position()=8">
								<td>
									<xsl:if test=". != 0">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="($total = 24 and position()=14) or ($total = 23 and position()=13) ">
								<xsl:choose>
								<xsl:when test="../td[1]='合计'">
									<td>
									</td>
								</xsl:when>
								<xsl:otherwise>
								<td>
									<xsl:if test=". != '' and .!='无需标记'">
										<a href="#">
											<xsl:attribute name="onclick">
												openVouchdiff("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test=". = '' or . = '无需标记'">
										<xsl:value-of select="."/>
									</xsl:if>
								</td>
								</xsl:otherwise>
								</xsl:choose>

							</xsl:when>
							<xsl:when test="($total = 24 and position()=15) or ($total = 23 and position()=14) ">

								<xsl:choose>
									<xsl:when test="../td[1]='合计'">
										<td>
										</td>
									</xsl:when>
									<xsl:otherwise>
										<td>
											<xsl:choose>
												<xsl:when test="$total = 24 and ../td[24]=1 or $total = 23 and ../td[23]=1">
													<a href="#">
														<xsl:attribute name="onclick">
															javascript:openVouchCheckSet("&lt;vouch_id&gt;"+<xsl:value-of select="../pk/vouch_id"/>+"&lt;/vouch_id&gt;")
														</xsl:attribute>
														收支类
													</a>
												</xsl:when>
												<xsl:otherwise>
													非收支类
												</xsl:otherwise>
											</xsl:choose>
										</td>
									</xsl:otherwise>
								</xsl:choose>



								<td>
									<xsl:if test=". ='是'">
										<a href="#">
											<xsl:attribute name="onclick">
												openVouchCon("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test=". = '否'">
										<xsl:value-of select="."/>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="($total = 24 and position()>15) or ($total = 23 and position()>14) "></xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>

					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
