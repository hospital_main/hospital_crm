<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	    <thead>
	  	    <tr noWrap='true' class='mainHead'>
	  			<td align='center'>会计年度</td>
					<td align='center'>会计期间</td>
					<td align='center'>日期</td>
					<td align='center'>凭证类型</td>
					<td align='center'>凭证号</td>
					<td align='center'>制单人</td>
					<td align='center'>审核人</td>
					<td align='center'>记账人</td>
					<td align='center'>出纳人</td>
					<td align='center'>是否签字</td>
					
					<td align='center'>附件张数</td>
					<td align='center'>分录序号</td>
					<td align='center'>科目摘要</td>
					<td align='center'>科目代码</td>
					<td align='center'>科目名称</td>
					<td align='center'>借方金额</td>
					<td align='center'>贷方金额</td>
					<td align='center'>辅助序号</td>
					<td align='center'>辅助摘要</td>
					<td align='center'>辅助核算</td>
					
					<td align='center'>辅助金额</td>
					<td align='center'>现金流量编码</td>
					<td align='center'>现金流量名称</td>
					<td align='center'>结算方式编码</td>
					<td align='center'>结算方式名称</td>
					<td align='center'>票据号码</td>
					<td align='center'>合同号</td>
					<td align='center'>单据号</td>
					<td align='center'>到期日期</td>
					<td align='center'>时间差异</td>
					<td align='center'>是否核销</td>
	      </tr>
      </thead>
	  	<tbody> 
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=16 or position()=17 or (position()=21 and ../td[18] != 0 )">
									<td align="right">
										
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:when>
								<xsl:when test="position()=21 and ../td[18] = 0 ">
									<td align="right">
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td align='left'><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>