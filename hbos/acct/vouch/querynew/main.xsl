<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="total" select="count(/root/tbody/tr[1]/td)"/>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none' rowspan="2"><input type='checkbox'/></th>
				<th rowspan="2">会计凭证号</th>
				<!--<th>预算会计凭证号</th>-->
				<th rowspan="2">凭证日期</th>
				<th rowspan="2">附件</th>
				<th rowspan="2">摘要</th>
				<th colspan="2">财务会计</th>
				<th style="display:none">财务会计</th>
				<th colspan="2">预算会计</th>
				<th style="display:none" >预算会计</th>

				<th rowspan="2">制单人</th>
				<xsl:if test="$total = 25">
					<th rowspan="2">签字人</th>
				</xsl:if>
				<th rowspan="2">审核人</th>
				<th rowspan="2">记账人</th>
				<th rowspan="2">标错信息</th>
				<th rowspan="2">是否标记</th>
				<th rowspan="2">时间差异</th>
				<th rowspan="2">是否关联</th>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td style='display:none'></td>
				<td style="display:none">会计凭证号</td>
				<!--<th>预算会计凭证号</th>-->
				<td style="display:none">凭证日期</td>
				<td style="display:none">附件</td>
				<td style="display:none">摘要</td>
				<td>借方金额</td>
				<td>贷方金额</td>
				<td>借方金额</td>
				<td>贷方金额</td>
				<td style="display:none">制单人</td>
				<xsl:if test="$total = 25">
					<td style="display:none">签字人</td>
				</xsl:if>
				<td style="display:none">审核人</td>
				<td style="display:none">记账人</td>
				<td style="display:none">标错信息</td>
				<td style="display:none">是否标记</td>
				<td style="display:none">时间差异</td>
				<td style="display:none">是否关联</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
	        <td align='center'  style='display:none'>
	          <xsl:if test="td[1]!= '合计'">
							<input type='checkbox' TABINDEX='-1' align='center'>
								<xsl:attribute name="value" >
									<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
								</xsl:attribute>
									<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
							</input>
						</xsl:if>
						<xsl:if test="td[1]='合计'">
							<input type='checkbox' TABINDEX='-1' align='center'>
								<xsl:attribute name="disabled" >true</xsl:attribute>
							</input>
						</xsl:if>
					</td>
					<xsl:for-each select="td[($total = 25 and position() &lt;= 16) or ($total = 24 and position()&lt;= 15) ]">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
									<xsl:if test=". != '合计'">
						        <a href="#">
											<xsl:attribute name="onclick">
												openMainVouch("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
										<xsl:if test=". != '' and ../td[($total = 25 and position() = 17) or ($total = 24 and position() = 16)]!=''"><img src="chong.gif"/></xsl:if>
										<xsl:if test=". != '' and ../td[($total = 25 and position() = 18) or ($total = 24 and position() = 17)]='1'"><img src="fei.gif"/></xsl:if>
										<xsl:if test=". != '' and ../td[($total = 25 and position() = 20) or ($total = 24 and position() = 19)]='1'"><img src="err.gif"/></xsl:if>
					        </xsl:if>
	               	<xsl:if test=". = '合计'">
						        <xsl:value-of select="."/>
						      </xsl:if>
							  </td>
							</xsl:when>
							<xsl:when test="position()=2">
								<!--<td>
									<xsl:if test=". != ''">
						        <a href="#">
											<xsl:attribute name="onclick">
												openMainVouch("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
										<xsl:if test="../td[($total = 21 and position() = 14) or ($total = 20 and position() = 13)]!=''"><img src="chong.gif"/></xsl:if>
										<xsl:if test="../td[($total = 21 and position() = 15) or ($total = 20 and position() = 14)]='1'"><img src="fei.gif"/></xsl:if>
										<xsl:if test="../td[($total = 21 and position() = 17) or ($total = 20 and position() = 16)]='1'"><img src="err.gif"/></xsl:if>
					        </xsl:if>
	               	<xsl:if test=". = ''">
						        <xsl:value-of select="."/>
						      </xsl:if>
							  </td>-->
							</xsl:when>
							<xsl:when test="position()=5">
								<td style="width:300px; overflow:hidden">
									<div style="width:300px; word-break:break-all;">
										<xsl:value-of select="."/>
									</div>
								</td>
							</xsl:when>
							<xsl:when test="position()=6 or position()=7 or position()=8 or position()=9">
								<td>
								<xsl:if test=". != 0">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="($total = 25 and position()=15) or ($total = 24 and position()=14) ">
								<xsl:choose>
								<xsl:when test="../td[1]='合计'">
									<td>
									</td>
								</xsl:when>
								<xsl:otherwise>
								<td>
									<xsl:if test=". != '' and .!='无需标记'">
						        <a href="#">
											<xsl:attribute name="onclick">
												openVouchdiff("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
					        </xsl:if>
	               	<xsl:if test=". = '' or . = '无需标记'">
						        <xsl:value-of select="."/>
						      </xsl:if>
							  </td>
								</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:when test="($total = 25 and position()=16) or ($total = 24 and position()=15) ">

								<xsl:choose>
									<xsl:when test="../td[1]='合计'">
										<td>
										</td>
									</xsl:when>
									<xsl:otherwise>
										<td>
											<xsl:choose>
												<xsl:when test="$total = 25 and ../td[25]=1 or $total = 24 and ../td[24]=1">
													<a href="#">
														<xsl:attribute name="onclick">
															javascript:openVouchCheckSet("&lt;vouch_id&gt;"+<xsl:value-of select="../pk/vouch_id"/>+"&lt;/vouch_id&gt;")
														</xsl:attribute>
														收支类
													</a>
												</xsl:when>
												<xsl:otherwise>
													非收支类
												</xsl:otherwise>
											</xsl:choose>
										</td>
									</xsl:otherwise>
								</xsl:choose>



								<td>
									<xsl:if test=". ='是'">
						        <a href="#">
											<xsl:attribute name="onclick">
												openVouchCon("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
					        </xsl:if>
	               	<xsl:if test=". = '否'">
						        <xsl:value-of select="."/>
						      </xsl:if>
							  </td>
							</xsl:when>
							<xsl:when test="($total = 25 and position()>16) or ($total = 24 and position()>15) ">
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>

					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
