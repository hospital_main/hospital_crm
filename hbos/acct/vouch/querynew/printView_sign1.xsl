<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap="true" class="mainHead">
					<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr noWrap="true" class="mainHead">
					<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
					<td style="display:none"/>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td>凭证编号</td>
					<td>凭证日期</td>
					<td>附件</td>
					<td>摘要</td>
					<td>科目编码</td>
					<td>科目名称</td>
					<td>借方金额</td>
					<td>贷方金额</td>
					<td>制单人</td>
					<td>签字人</td>
					<td>审核人</td>
					<td>记账人</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td[position()!=12 and position()!=13 and position()!=14]">
							<xsl:choose>
								<xsl:when test="position()=1">
									<td>
										<xsl:if test=". != '合计'">
											<xsl:value-of select="."/>
											<xsl:if test="../td[12]!=''"><img src="chong.gif"/></xsl:if>
											<xsl:if test="../td[13]='1'"><img src="fei.gif"/></xsl:if>
										</xsl:if>
										<xsl:if test=". = '合计'">
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
								</xsl:when>
								<xsl:when test="position()=6">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=4">
									<td style="width:300px; overflow:hidden">
										<div style="width:300px; word-break:break-all;">
											<xsl:value-of select="."/>
										</div>
									</td>
								</xsl:when>
								<xsl:when test="position()=5">
									<td>
										<xsl:if test="../td[14]='1'">
											<xsl:value-of select="."/>
										</xsl:if>
										<xsl:if test="../td[14]!='1'">
											<xsl:value-of select="."/>
										</xsl:if>
									</td>
								</xsl:when>
								<xsl:when test="position()=7 or position()=8">
									<td>
										<xsl:if test=". != 0">
											<xsl:attribute name="align">right</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</xsl:if>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>