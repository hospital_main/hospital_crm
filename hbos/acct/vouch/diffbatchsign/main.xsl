<?xml version='1.0' encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="total" select="count(/root/tbody/tr[1]/td)"/>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th nowrap='true' valign='middle'>凭证号</th>
		  	<th nowrap='true' valign='middle'>凭证日期</th>
		  	<th nowrap='true' valign='middle'>摘要</th>
				<th nowrap='true' valign='middle'>财务借方金额</th>
		  	<th nowrap='true' valign='middle'>财务贷方金额</th>
		  	<th nowrap='true' valign='middle'>预算借方金额</th>
		  	<th nowrap='true' valign='middle'>预算贷方金额</th>
		  	<th nowrap='true' valign='middle'>差异类</th>
		  	<th nowrap='true' valign='middle'>差异项目</th>
		  	<th nowrap='true' valign='middle'>差异金额</th>
		  	<th nowrap='true' valign='middle'>是否标记</th>
			</tr>
		</thead>  	
		<tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 0">
     		 <tr>
     		 	<td align='center'  style='display:none'>
	          <xsl:if test="td[1]!='合计'">
							<input type='checkbox' TABINDEX='-1' align='center'>
								<xsl:attribute name="value" >
									<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
								</xsl:attribute>
									<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
							</input>
						</xsl:if>
						<xsl:if test="td[1]='合计'">
							<input type='checkbox' TABINDEX='-1' align='center'>
								<xsl:attribute name="disabled" >true</xsl:attribute>
							</input>
						</xsl:if>
					</td>
		          <xsl:for-each select="td">
		              <xsl:choose>

			              <xsl:when test="position() = 1 ">
							  <xsl:choose>
							  <xsl:when test="../td[1]='合计'">
								  <td>
									  合计
								  </td>
							  </xsl:when>
							  <xsl:otherwise>
			                <td align="left">
			                 <a tabindex='-1'>
				                  <xsl:attribute name="href" >
				    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
											  </xsl:attribute><xsl:value-of select="."/></a>
			  	          	</td>
							  </xsl:otherwise>
							  </xsl:choose>
			              </xsl:when>
		                	                
		                <xsl:when test="position() = 4 or position() = 5 or position() = 6 or position() = 7 or position() = 10">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>	
		                
		            <xsl:when test="position()=11">
						<xsl:choose>
						<xsl:when test="../td[1]='合计'">
							<td>
							</td>
						</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:if test=". != '' and .!='无需标记'">
						        <a href="#">
											<xsl:attribute name="onclick">
												openVouchdiff("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
					        </xsl:if>
	               	<xsl:if test=". = '' or . = '无需标记'">
						        <xsl:value-of select="."/>
						      </xsl:if>
							  </td>
							</xsl:otherwise>
						</xsl:choose>
							</xsl:when>

		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
	  	  	</tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
	</xsl:template>
</xsl:stylesheet>
