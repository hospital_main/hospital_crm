<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style=''>日期</th>
				<th nowrap='true'>科室代码</th>
				<th nowrap='true'>科室名称</th>
				<th nowrap='true'>会计科目代码</th>
				<th nowrap='true'>会计科目名称</th>
				<th nowrap='true'>已发生金额</th>
				<th nowrap='true'>预算科目代码</th>
				<th nowrap='true'>预算科目名称</th>
				<th nowrap='true'>本年预算额度</th>
				<th nowrap='true'>本年剩余预算额度</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">   
						<xsl:choose>
		                <xsl:when test="position()=6">
		                 		 <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		               <xsl:when test="position()=9">
		                 		 <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=10">
		                	 
		                 		 <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                 	 
		                </xsl:when>
		                		<xsl:otherwise>
				                 <td align='center'><xsl:value-of select="."/></td>
				                </xsl:otherwise>
			             </xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>