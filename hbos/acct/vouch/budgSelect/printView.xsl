<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		
      	</tr>
			<tr noWrap='true' class='mainHead'>     
				<td nowrap='true'>日期</td>
        <td nowrap='true'>科室代码</td>
		  	<td nowrap='true'>科室名称</td>
		  	<td nowrap='true'>会计科目代码</td>
		  	<td nowrap='true'>会计科目名称</td>
		  	<td nowrap='true'>已发生金额</td>
		  	<td nowrap='true'>预算科目代码</td>
		  	<td nowrap='true'>预算科目名称</td>
		  	<td nowrap='true'>本年预算额度</td>
		  	<td nowrap='true'>本年剩余预算额度</td>
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=6 or position()=9 or position()=10">
									<td align='right'><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
		              <td align='left'><xsl:value-of select="."/></td>
				  		</xsl:otherwise>
						</xsl:choose>	
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
