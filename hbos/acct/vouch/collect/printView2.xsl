
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
	<table>
		<thead>
			<tr noWrap="true" class="mainHead">
				<td style="fontsize:maintitle;colspan:colcount" />
				<td style="display:none" />
				<td style="display:none" />
				<td style="display:none" />
			</tr>
			<tr noWrap="true" class="mainHead">
				<td style="fontsize:subtitle;colspan:colcount;align:left" />
				<td style="display:none" />
				<td style="display:none" />
				<td style="display:none" />
			</tr>
			<tr noWrap="true" class="mainHead">
				<td style="fontsize:subtitle;colspan:colcount;align:left" />
				<td style="display:none" />
				<td style="display:none" />
				<td style="display:none" />
			</tr>
			<tr>
				<td style="fontsize:coltitle">科目编码</td>
				<td style="fontsize:coltitle">科目名称</td>
				<td style="fontsize:coltitle">借方金额</td>
				<td style="fontsize:coltitle">贷方金额</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<xsl:if test="../td[5]='1'">
									<td><xsl:value-of select="."/></td>
								</xsl:if>
								<xsl:if test="../td[5]!='1'">
									<td style="colspan:2;"><xsl:value-of select="../td[2]"/></td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=2">
								<xsl:if test="../td[5]='1'">
									<td><xsl:value-of select="."/></td>
								</xsl:if>
								<xsl:if test="../td[5]!='1'">
									<td style="display:none;"></td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=3 or position()=4">
								<td style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=5">
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</table>
	</xsl:template>
</xsl:stylesheet>