<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/vouch/collect/printView.xsl,v 1.2 2012/10/19 04:16:47 linaikun Exp $
 $Author: linaikun $
 $Date: 2012/10/19 04:16:47 $
 $Revision: 1.2 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
		<thead>
			<tr>
				<td style="colspan:5;fontsize:maintitle">凭证汇总</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>								<td style="display:none"></td>
			</tr>
			<tr>
				<td style="colspan:5;fontsize:subtitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>								<td style="display:none"></td>
			</tr>
			<tr>
				<td style="border:thin;fontsize:coltitle">科目编码</td>
				<td style="border:thin;fontsize:coltitle">科目名称</td>
				<td style="border:thin;fontsize:coltitle">借方金额</td>
				<td style="border:thin;fontsize:coltitle">贷方金额</td>								<td style="border:thin;fontsize:coltitle">凭证数</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position()!=last()]"> 
						<xsl:choose>
							<xsl:when test="position()=3 or position()=4">
								<td style="border:thin;align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=5">
								<td style="border:thin;align:right"><xsl:value-of select="."/></td>
							</xsl:when>

							<xsl:otherwise>
								<td style="border:thin"><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			<tr>
				<td></td>
				<td align="center">合计</td>
				<td><xsl:value-of select="format-number(sum(/root/tbody/tr[td[last()]='1']/td[3]),'#,##0.00')"/></td>
				<td><xsl:value-of select="format-number(sum(/root/tbody/tr[td[last()]='1']/td[4]),'#,##0.00')"/></td>
				<td></td>
				
			</tr>
		</tbody>
		<tfoot>
			<tr>
		
				<td style="colspan:5;align:right;fontsize:coltitle">制表人：</td>				
			</tr>		
		</tfoot>
		</table>
	</xsl:template>
</xsl:stylesheet>
