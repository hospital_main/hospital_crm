<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style=''>科目编码</th>
				<th nowrap='true'>科目名称</th>
				<th nowrap='true'>借方金额</th>
				<th nowrap='true'>贷方金额</th>
				
				<th nowrap='true'>凭证数</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position() != last()]"> 
						<xsl:choose>
							<xsl:when test="position()=2 and ../td[1]=''">
								<td align="left"><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=3 or position()=4">
								<td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							
							<xsl:when test="position()=5">
								<td align="right"><xsl:value-of select="."/></td>
							</xsl:when>
							
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			<tr>
				<td></td>
				<td align="center">合计</td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr[td[last()]='1']/td[3]),'#,##0.00')"/></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr[td[last()]='1']/td[4]),'#,##0.00')"/></td>
				
				<td></td>
			</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>