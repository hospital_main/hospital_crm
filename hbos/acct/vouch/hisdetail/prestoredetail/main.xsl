<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
  			<th noWrap='true'>���˱���</th>
  			<th noWrap='true'>��������</th>
  			<th noWrap='true'>�ڳ����</th>
  			<th noWrap='true'>Ԥ�ս��</th>
  			<th noWrap='true'>�������</th>
  			<th noWrap='true'>��ĩ���</th>
  		</tr>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td[position()&lt;7]">						
						<xsl:choose>
							<xsl:when test="position() = 3">
								<td>
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:when test="position() = 4">
								<td align='right'>
									<xsl:if test="../td[1] != ''">
										<a href="#">
										<xsl:attribute name="onclick">getDetail(1, "<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</a>
									</xsl:if>
									<xsl:if test="../td[1] = ''">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position() = 5">
								<td align='right'>
									<xsl:if test="../td[1] = ''">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
									<xsl:if test="../td[1] != ''">
										<a href="#">
										<xsl:attribute name="onclick">getDetail(2, "<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</a>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position() = 6">
								<td>
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



