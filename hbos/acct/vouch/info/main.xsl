<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style=''>凭证总数</th>
				<th nowrap='true'>正常凭证</th>
				<th nowrap='true'>作废凭证</th>
				<th nowrap='true'>已审核凭证</th>
				<th nowrap='true'>未审核凭证</th>
				<th nowrap='true'>已记账凭证</th>
				<th nowrap='true'>未记账凭证</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">   
						<td><xsl:value-of select="."/></td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>