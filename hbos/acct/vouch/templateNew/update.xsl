<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th width='25'><input type='checkbox'/></th>
      	<th nowrap='true'>摘要</th>
      	<th nowrap='true'>财务科目代码</th>
      	<th nowrap='true'>财务科目名称</th>
      	<th nowrap='true'>辅助信息</th>
        <th nowrap='true'>预算科目代码</th>
      	<th nowrap='true'>预算科目名称</th>
      	<th nowrap='true'>辅助信息</th>
      	<th nowrap='true'>金额方向</th>
      	<th nowrap='true'>金额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr onclick="selectOneSubj(this)">
          <td align='center'>
            <input type='checkbox'>
              <xsl:if test="@chose='true'">
                <xsl:attribute name="checked">true</xsl:attribute>
              </xsl:if>
            </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                  <a href='#'>
                    <xsl:attribute name="onclick">openDialog('dupdate.html?load=&lt;checkInfo&gt;<xsl:value-of select="../td[6]"/>&lt;/checkInfo&gt;&lt;itemId&gt;<xsl:value-of select="../td[7]"/>&lt;/itemId&gt;&lt;budgcheckInfo&gt;<xsl:value-of select="../td[12]"/>&lt;/budgcheckInfo&gt;&lt;budgitemId&gt;<xsl:value-of select="../td[13]"/>&lt;/budgitemId&gt;','dialogWidth:800px;dialogHeight:600px', this);</xsl:attribute>
                    <xsl:value-of select="."/>
                  </a>
                </td>
              </xsl:when>
              <xsl:when test="position()=2 or position()=3 ">
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:when test="position()=4 ">
                <td>
                  <xsl:value-of select="substring(.,2,string-length(.))"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=6 or position()=5 or position()=7">
                <td style="display:none">
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:when test="position()=8">
                <td>
                  
                    <!-- <xsl:attribute name="onclick">openDialog('dupdate.html?load=&lt;checkInfo&gt;<xsl:value-of select="../td[6]"/>&lt;/checkInfo&gt;&lt;itemId&gt;<xsl:value-of select="../td[7]"/>&lt;/itemId&gt;','dialogWidth:800px;dialogHeight:600px', this);</xsl:attribute>  -->
                    <xsl:value-of select="."/>
                  
                </td>
              </xsl:when>
                <xsl:when test="position()=9">
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
               <xsl:when test="position()=10">
                <td>
                  <xsl:value-of select="substring(.,2,string-length(.))"/>
                </td>
              </xsl:when>
              <xsl:when test="position()=14 ">
                <td>
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:when test="position()=15 ">
                <td>
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


