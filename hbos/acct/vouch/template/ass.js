///////////////////////////
var temp_templet_id=0;
var temp_templet_budgid=0;
var temp_curr_tr=null;
var g_asst_summ="";
var g_asst_subj="";
var g_asst_did="";
function genTempId(){
	var p=getValuePairBySql("acct_vouch_prepare_maintemple","");
	temp_templet_id=p[0];
}
function getTempDetailId(){
	var p=getValuePairBySql("acct_vouch_prepare_maintemple_detail","<tid>"+temp_templet_id+"</tid>");
	return p[0];
}

function selectOneSubj(obj){
	if(temp_curr_tr!=null)
		temp_curr_tr.style["background"]="white";
	temp_curr_tr=obj;
	temp_curr_tr.style["background"]="#eeeeee";
	g_asst_summ=temp_curr_tr.cells[1].innerText;
	g_asst_subj=temp_curr_tr.cells[2].innerText;
	g_asst_did=temp_curr_tr.cells[5].innerText;
	var p=getValuePairBySql("acct_vouch_get_is_check","<comp_code>"+getCompCode()+"</comp_code><copy_code>"+getCopyCode()+"</copy_code><vouch_year>"+getAcctYear()+"</vouch_year><acct_subj>"+g_asst_subj+"</acct_subj>");
	disableAsstBtn(p[0]=="0"?true:false);
	if(p[0]=="0"){
		detailTable.innerHTML="";
		return ;
	}
	window.xmlhttp.post('acctVouchEDITemp_updat_item_load_head', "<did>"+g_asst_did+"</did><subj>"+g_asst_subj+"</subj><def>6</def>", '?isCheck=false');
	var resText = window.xmlhttp._object.responseText;
	if(resText.indexOf("<error>")>0){
		window.doMsg(resText);
		return false;
	}
	var htmlStr='<div id="vouch_info_asset" style="overflow:auto; height:80;border-top-width: 1px;border-top-style: solid;border-top-color: #000000;">';
	htmlStr+='	<table width="100%"  border="0" cellpadding="1" cellspacing="1" bgcolor="#000000">';
	htmlStr+='<tr><td align="left" bgcolor="#FFFFFF"  >科目：'+temp_curr_tr.cells[2].innerText+'&nbsp;'+temp_curr_tr.cells[3].innerText+'</td></tr>';
	var trs=window.xmlhttp._object.responseXML.getElementsByTagName("tr");
	var tds;
	for(var i=0;i<trs.length;i++){
		tds=trs[i].getElementsByTagName("td");
		for(var j=0;j<tds.length;j++){
			if(tds[j].text=='') continue;
			ccc=tds[j].text.split("|||");
			htmlStr+='<tr><td align="left" bgcolor="#FFFFFF" >'+ccc[0]+'：('+ccc[1]+')'+ccc[2]+'</td></tr>';
		}
	}
	htmlStr+="</table></div>"
	detailTable.innerHTML=htmlStr;
}
function resetTableState(){
	temp_curr_tr=null;
	disableAsstBtn(true);
	genTempId();
}
function disableAsstBtn(d){
	asstBtn.disabled=d;
}
function openAsst(){
	if(g_asst_did==''){
		alert('请先选择科目');
		return;
		}
	openDialog('asst.html', 'dialogWidth:800px;dialogHeight:350px;')
}
////////////