<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<xsl:if test="count(/root/tbody/tr)&lt;1">
					<th>单据号</th>
					<th>单据日期</th>
					<th>制单人</th>
					<th>审核人</th>
					<th>凭证号</th>
				</xsl:if>
				<xsl:for-each select="/root/tbody/tr[1]/td[position()!=1 and position() &lt; last()-1 ]">
					<th><xsl:value-of select="."/>
				</th>
				</xsl:for-each>
			</tr>           
		</thead> 	
		<tbody>
			<xsl:variable name="LINKFLAG" select="/root/annex/LINKFLAG"/>
			<xsl:for-each select="/root/tbody/tr[position()!=1]">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
							<xsl:if test="td[last()-2]!=''">
								<xsl:attribute name="disabled">true</xsl:attribute>
							</xsl:if>
						</input>
					</td>
					<xsl:for-each select="td[position()!=1]">
						<xsl:variable name="cols" select="position()"/>
							<xsl:choose>
								<xsl:when test="position()=1">
	          			<td>
								  	<xsl:if test="'0101'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    openPact('<xsl:value-of select="."/>','<xsl:for-each select="../pk/mod_code"><xsl:value-of select="."/></xsl:for-each>')
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0102'= ../td[last()-1] or '0103'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    openDialog('../../../pact/paypact/claimcon/update.html?load=&lt;b&gt;<xsl:value-of select="."/>&lt;/b&gt;','dialogWidth:1050px;dialogHeight:450px')
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0104'= ../td[last()-1] or '0105'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    openDialog('../../../pact/paypact/secupaycon/update.html?load=&lt;deposit_code&gt;<xsl:value-of select="."/>&lt;/deposit_code&gt;','dialogWidth:1050px;dialogHeight:450px')
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0106'= ../td[last()-1] or '0107'= ../td[last()-1] or '0108'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    openDialog('../../../pact/paypact/paydocu/update.html?load=&lt;pay_id&gt;<xsl:value-of select="../pk/id"/>&lt;/pay_id&gt;','dialogWidth:1000px;dialogHeight:570px')
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0109'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    openDialog('../../../pact/paypact/paychange/paychange.html?load=&lt;change_code&gt;<xsl:value-of select="."/>&lt;/change_code&gt;','dialogWidth:900px;dialogHeight:600px')
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0201'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    openPact('<xsl:value-of select="."/>','<xsl:for-each select="../pk/mod_code"><xsl:value-of select="."/></xsl:for-each>')
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0202'= ../td[last()-1] or '0203'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    openDialog('../../../pact/collpact/claimcon/update.html?load=&lt;b&gt;<xsl:value-of select="."/>&lt;/b&gt;','dialogWidth:1050px;dialogHeight:450px')
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0204'= ../td[last()-1] or '0205'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    openDialog('../../../pact/collpact/secupaycon/update.html?load=&lt;deposit_code&gt;<xsl:value-of select="."/>&lt;/deposit_code&gt;','dialogWidth:1050px;dialogHeight:450px')
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0206'= ../td[last()-1] or '0207'= ../td[last()-1] or '0208'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    openDialog('../../../pact/collpact/paydocu/update.html?load=&lt;receive_id&gt;<xsl:value-of select="../pk/id"/>&lt;/receive_id&gt;','dialogWidth:1000px;dialogHeight:570px')
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0209'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    openDialog('../../../pact/collpact/paychange/paychange.html?load=&lt;change_code&gt;<xsl:value-of select="."/>&lt;/change_code&gt;','dialogWidth:900px;dialogHeight:600px')
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
		          		</td>
		          	</xsl:when>
								<xsl:when test="position()=last()-2">
									<td >
										<a href="#">
											<xsl:attribute name="onclick">openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;&lt;/edit_mask&gt;")</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="starts-with(.,'合同编号_') = true()">
									<td >
										<xsl:if test="$LINKFLAG =1" >
											<a href="#">
												<xsl:attribute name="onclick">openPact('<xsl:value-of select="substring-after(.,'_')"/>','<xsl:for-each select="../pk/mod_code"><xsl:value-of select="."/></xsl:for-each>')</xsl:attribute>
												<xsl:value-of select="substring-after(.,'_')"/>
											</a>
										</xsl:if>
										<xsl:if test="$LINKFLAG !=1" >
											<xsl:value-of select="substring-after(.,'_')"/>
										</xsl:if>
									</td>
								</xsl:when>
								<xsl:when test="starts-with(.,'金额_') = true()">
									<td align="right" >
										<xsl:value-of select="format-number(substring-after(.,'_'),'#,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="starts-with(.,'使用年数_') = true()">
									<td align="right" >
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=last() or position()=last()-1">
								</xsl:when>
								<xsl:otherwise>
									<td >
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>