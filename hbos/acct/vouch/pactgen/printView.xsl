<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
			<tr noWrap='true' class='mainHead'>				
				<xsl:if test="count(/root/tbody/tr)&lt;1">
					<td>单据号</td>
					<td>单据日期</td>
					<td>制单人</td>
					<td>审核人</td>
					<td>凭证号</td>
				</xsl:if>
				<xsl:for-each select="/root/tbody/tr[1]/td[position()!=1 and position() &lt; last()-1 ]">
					<td><xsl:value-of select="."/>
				</td>
				</xsl:for-each>
			</tr>           
		</thead> 	
		<tbody>
			<xsl:variable name="LINKFLAG" select="/root/annex/LINKFLAG"/>
			<xsl:for-each select="/root/tbody/tr[position()!=1]">
				<tr>
					<xsl:for-each select="td[position()!=1]">
						<xsl:variable name="cols" select="position()"/>
							<xsl:choose>
								<xsl:when test="position()=1">
	          			<td>
	          				<xsl:value-of select="."/>
		          		</td>
		          	</xsl:when>
								<xsl:when test="position()=last()-2">
									<td >
											<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="starts-with(.,'合同编号_') = true()">
									<td >
										<xsl:value-of select="substring-after(.,'_')"/>
									</td>
								</xsl:when>
								<xsl:when test="starts-with(.,'金额_') = true()">
									<td align="right" >
										<xsl:value-of select="format-number(substring-after(.,'_'),'#,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="starts-with(.,'使用年数_') = true()">
									<td align="right" >
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=last() or position()=last()-1">
								</xsl:when>
								<xsl:otherwise>
									<td >
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
