<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
	  	<thead>
	  		<tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>			  	 
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
			  <tr noWrap="true" class="mainHead">
			  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>			  	 
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  	<td style="display:none"/>
			  </tr>
	  		<tr class="mainHead">
		      <td nowrap='true'>日期</td>
		      <td nowrap='true'>凭证类型</td>
					<td nowrap='true'>凭证号</td>
					<td nowrap='true'>制单人</td>
					<td nowrap='true'>审核人</td>
					<td nowrap='true'>记账人</td>
					<td nowrap='true'>附件张数</td>
	  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">            
	            <xsl:choose>
	              <xsl:when test="position()=1">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>  
	              <xsl:when test="position() = 7 ">
	                <td align="right" nowrap='true'>
	                  <xsl:value-of select="format-number(.,'#,##0')"/>
	      		      </td>
	              </xsl:when>
	              <xsl:otherwise>
	                <td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          </xsl:choose>          
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	   		<tr>
					<td>合计：</td>
					<td colspan="5" ><xsl:value-of select="format-number(count(/root/tbody/tr/td[2]),'#,##0')"/>张</td>
					<td style="display:none" ></td>
					<td style="display:none" ></td>
					<td style="display:none" ></td>
					<td style="display:none" ></td>
					<td style="display:none" ></td>
				</tr>
	 		</tbody>
   	</root>
	</xsl:template>
</xsl:stylesheet>