<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th nowrap='true'>操作员</th>
				<th nowrap='true'>凭证数量</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><xsl:value-of select="."/></td>
              </xsl:when>  
              <xsl:when test="position() = 2 ">
                <td class='numberText' nowrap='true'>
                  <xsl:value-of select="format-number(.,'#,##0')"/>
      		      </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<tr>
				<td>合计：</td>
				<td class='numberText' ><xsl:value-of select="format-number(sum(/root/tbody/tr/td[2]),'#,##0')"/>张</td>
			</tr>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

