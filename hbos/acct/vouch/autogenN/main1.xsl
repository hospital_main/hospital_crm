<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<xsl:if test="count(/root/tbody/tr)&lt;1">
					<th>单据号</th>
					<th>单据日期</th>
					<th>制单人</th>
					<th>审核人</th>
					<th>财务会计凭证号</th>
				</xsl:if>
				<xsl:for-each select="/root/tbody/tr[1]/td[position()!=1 and position() &lt; last()-1 ]">
					<th><xsl:value-of select="."/>
					</th>
				</xsl:for-each>
			</tr>           
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr[position()!=1]">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
							<xsl:if test="td[last()-2]!=''">
								<xsl:attribute name="disabled">true</xsl:attribute>
							</xsl:if>
						</input>
					</td>
					<xsl:for-each select="td[position()!=1]">
						<xsl:variable name="cols" select="position()"/>
							<xsl:choose>
								<xsl:when test="position()=1">
	          			<td>
								  	
									<xsl:if test="'0541'=../td[last()-1] or '0542'=../td[last()-1] or '0543'=../td[last()-1] or '0544'=../td[last()-1] or '0545'=../td[last()-1] or '0546'=../td[last()-1] or '0547'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openCGPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'1701'=../td[last()-1] or '1702'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openCGImmaPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'0551'=../td[last()-1] or '0552'=../td[last()-1] or '0553'=../td[last()-1] or '0554'=../td[last()-1] or '0555'=../td[last()-1] or '0556'=../td[last()-1] or '0557'=../td[last()-1] or '0558'=../td[last()-1] or '0559'=../td[last()-1] or '0560'=../td[last()-1] or '0561'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openJSPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'1711'=../td[last()-1] or '1712'=../td[last()-1] or '1713'=../td[last()-1] or '1714'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openJSImmaPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'1731'=../td[last()-1] or '1732'=../td[last()-1] or '1741'=../td[last()-1] or '1751'=../td[last()-1] or '1752'=../td[last()-1] or '1761'=../td[last()-1] or '1762'=../td[last()-1] or '1763'=../td[last()-1]" >
										<xsl:value-of select="."/>
									</xsl:if>
									<!-- 固定资产科室领用 -->
									<xsl:if test="'0581'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCKSLYPage('<xsl:for-each select="../pk/id"><xsl:value-of select="."/></xsl:for-each>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									
									<!-- 固定资产科室退库 -->
									<xsl:if test="'0582'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCKSTKPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									
									<!-- 固定资产付款单 -->
									<xsl:if test="'0571'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCFKPage('<xsl:value-of select="../td[2]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									
									<!-- 固定资产原值调整 -->
									<xsl:if test="'0575'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openYZBDPage('<xsl:value-of select="../td[2]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									
									<!-- 支出控制支出申请 -->
									<xsl:if test="'1001'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCKZJKDPage('<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 支出控制报销单(无预借)、报销单(有预借) -->
									<xsl:if test="'1002'=../td[last()-1] or '1003'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCKZBXDPage('<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
		          		</td>
		          	</xsl:when>
								<xsl:when test="position()=last()-2">
									<td >
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="/root/tbody/tr[1]/td[( $cols+ 1)] = '金额'  " >
									<td align="right" >
										<xsl:value-of select="format-number(.,'#,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="/root/tbody/tr[1]/td[($cols+ 1 )] = '使用年数'  " >
									<td align="right" >
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=last() or position()=last()-1">
								</xsl:when>
								<xsl:otherwise>
									<td >
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



