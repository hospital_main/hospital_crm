<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<xsl:if test="count(/root/tbody/tr)&lt;1">
					<th>单据号</th>
					<th>单据日期</th>
					<th>制单人</th>
					<th>审核人</th>
					<th>财务会计凭证号</th>
					<th>预算会计凭证号</th>
				</xsl:if>
				<xsl:for-each select="/root/tbody/tr[1]/td[position()!=1 and position() &lt; last()-1 ]">
					<th><xsl:value-of select="."/>
				</th>
				</xsl:for-each>
			</tr>           
		</thead> 	
		<tbody>
			<xsl:variable name="LINKFLAG" select="/root/annex/LINKFLAG"/>
			<xsl:for-each select="/root/tbody/tr[position()!=1]">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
							<xsl:if test="td[last()-2]!=''">
								<xsl:attribute name="disabled">true</xsl:attribute>
							</xsl:if>
						</input>
					</td>
					<xsl:for-each select="td[position()!=1]">
						<xsl:variable name="cols" select="position()"/>
							<xsl:choose>
								<xsl:when test="position()=1">
	          			<td>
	          			<xsl:if test="'1801'= ../td[last()-1] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../fund/moneycome/moneycome1/update.html?load=&lt;account_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/account_id&gt;', 'dialogWidth:950px;dialogHeight:700px', null)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
	          			<xsl:if test="'1803'= ../td[last()-1] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('update.html?load=&lt;pay_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/pay_id&gt;', 'dialogWidth:950px;dialogHeight:700px', null)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
								  <xsl:if test="'0401'= ../td[last()-1] or '0402'= ../td[last()-1]  or '0404'= ../td[last()-1] or '0405'= ../td[last()-1] or '0406'= ../td[last()-1] or '0407'= ../td[last()-1] or '0408'= ../td[last()-1] or '0410'= ../td[last()-1] or '0412'= ../td[last()-1] or '0422'= ../td[last()-1] or '0420'= ../td[last()-1]  " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openMatePage(<xsl:value-of select="../td[last()]"/>)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<!--<xsl:if test="'0402'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:1150px;dialogHeight:850px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>--><!--D:\H-ERP3.1_ALL\ROOT\hbos\mate\whr\out\common\update.html-->
								  	<xsl:if test="'0403'=../td[last()-1] or '0409'= ../td[last()-1] or '0411'= ../td[last()-1] or '0413'= ../td[last()-1] or '0414'= ../td[last()-1] or '0415'= ../td[last()-1] or '0419'= ../td[last()-1] or '0421'= ../td[last()-1] or '0423'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/whr/out/common/update.html?load=&lt;a&gt;&lt;/a&gt;&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:950px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<!-- 注释掉原来物流管理移库的单据链接 HJJ comment out -->
								  	<xsl:if test="'0430'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/whr/out/move/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	
								  	<!-- 物流管理采购发票 -->
								  	<xsl:if test="'0416'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/pay/invoice/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0417'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/barcode/query/mate/whr/second/advice/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0418'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
												  javascript:openDialog('../../../mate/whr/out/common/update.html?load=&lt;a&gt;&lt;/a&gt;&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<!--
								  	<xsl:if test="'0420'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/barcode/query/mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>--><!--D:\H-ERP3.1_ALL\ROOT\hbos\mate\pay\pay\pay\update.html-->
								  	<xsl:if test="'0424'=../td[last()-1] or '0425'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../mate/pay/pay/pay/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0501'=../td[last()-1] or '0502'=../td[last()-1] or '0503'=../td[last()-1] or '0506'=../td[last()-1] or '0507'=../td[last()-1] or '0508'=../td[last()-1] or '0509'=../td[last()-1] or '0511'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openPage('<xsl:value-of select="../td[2]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0504'=../td[last()-1] or '0505'=../td[last()-1] or '0510'=../td[last()-1]  " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../equip/change/back/medicalEquip/update.html?load=&lt;id&gt;<xsl:value-of select="../td[2]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0521'=../td[last()-1] or '0522'=../td[last()-1] or '0523'=../td[last()-1] or '0524'=../td[last()-1] or '0525'=../td[last()-1] or '0527'=../td[last()-1]or '0528'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openReducePage('<xsl:value-of select="../td[2]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0513'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openPKPage('<xsl:value-of select="../td[2]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	
								  	<!--<xsl:if test="'0572'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openZCKCPage('<xsl:value-of select="../td[2]"/>');
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>-->
									<xsl:if test="'0541'=../td[last()-1] or '0542'=../td[last()-1] or '0543'=../td[last()-1] or '0544'=../td[last()-1] or '0545'=../td[last()-1] or '0546'=../td[last()-1] or '0547'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openCGPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'1701'=../td[last()-1] or '1702'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openCGImmaPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'0551'=../td[last()-1] or '0552'=../td[last()-1] or '0553'=../td[last()-1] or '0554'=../td[last()-1] or '0555'=../td[last()-1] or '0556'=../td[last()-1] or '0557'=../td[last()-1] or '0558'=../td[last()-1] or '0559'=../td[last()-1] or '0560'=../td[last()-1] or '0561'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openJSPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'1711'=../td[last()-1] or '1712'=../td[last()-1] or '1713'=../td[last()-1] or '1714'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openJSImmaPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'1731'=../td[last()-1] or '1732'=../td[last()-1] or '1733'=../td[last()-1] or '1734'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openImmaPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'1751'=../td[last()-1] or '1752'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openImmaYZBDPage('<xsl:value-of select="../td[2]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'1741'=../td[last()-1] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('../../../immaequip/change/back/medicalEquip/update.html?load=&lt;id&gt;<xsl:value-of select="../td[2]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'1742'=../td[last()-1] or '1743'=../td[last()-1] or '1744'=../td[last()-1] or '1745'=../td[last()-1] " >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openImmaReducePage('<xsl:value-of select="../td[2]"/>');
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'1753'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openImmaZCKSLYPage1('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'1754'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openImmaZCKSTKPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'1756'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openImmaZCZYPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
							  	<xsl:if test="'1760'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openImmaZCWXPage('<xsl:value-of select="../td[2]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
							  	<xsl:if test="'1761'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openImmaZCFKPage('<xsl:value-of select="../td[2]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'1762'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openImmaYFKPage('<xsl:value-of select="../td[11]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test="'1763'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openImmaYZWBHTPage('<xsl:value-of select="../td[12]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 物流管理耐用品流转（库-科）||（科-库） -->
									<xsl:if test="'0426'=../td[last()-1] or '0427'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openMATNYPPage('<xsl:for-each select="../pk/id"><xsl:value-of select="."/></xsl:for-each>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 物流管理库房耐用品报废 -->
									<xsl:if test="'0428'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openMATKFNYPage('<xsl:for-each select="../pk/id"><xsl:value-of select="."/></xsl:for-each>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 物流管理科室耐用品报废 -->
									<xsl:if test="'0429'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openMATKSNYPage('<xsl:for-each select="../pk/id"><xsl:value-of select="."/></xsl:for-each>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 固定资产科室领用 -->
									<xsl:if test="'0581'=../td[last()-1] "  >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCKSLYPage('<xsl:for-each select="../pk/id"><xsl:value-of select="."/></xsl:for-each>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									
									<!-- 固定资产科室退库 -->
									<xsl:if test="'0582'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCKSTKPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 固定资产科室领用 -->
									<xsl:if test="'0583'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCKSLYPage1('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									
									<!-- 固定资产科室退库 -->
									<xsl:if test="'0584'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCKSTKPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 固定资产转移 -->
									<xsl:if test="'0585'=../td[last()-1] or '0586'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCZYPage('<xsl:value-of select="../td[2]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 固定资产转移 -->
									<xsl:if test="'0512'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCFPPage('<xsl:for-each select="../pk/id"><xsl:value-of select="."/></xsl:for-each>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 固定资产付款单 -->
									<xsl:if test="'0571'=../td[last()-1]  " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCFKPage('<xsl:value-of select="../td[2]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<xsl:if test=" '0579'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCFKPageInit('<xsl:value-of select="../td[2]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 固定资产资产维修 -->
									<xsl:if test="'0572'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCWXPage('<xsl:value-of select="../td[2]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 固定资产检测计量 -->
									<xsl:if test="'0573'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openJCJLPage('<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 固定资产资产保养 -->
									<xsl:if test="'0574'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCBYPage('<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 固定资产原值调整 -->
									<xsl:if test="'0575'=../td[last()-1] or '0576'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openYZBDPage('<xsl:value-of select="../td[2]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 固定资产预付款 -->
									<xsl:if test="'0577'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openYFKPage('<xsl:value-of select="../td[10]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 固定资产维保合同付款 -->
									<xsl:if test="'0578'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openYZWBHTPage('<xsl:value-of select="../td[12]"/>','<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 支出控制支出申请 -->
									<xsl:if test="'1001'=../td[last()-1] " >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCKZJKDPage('<xsl:value-of select="../td[last()]"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 支出控制报销单(无预借)、报销单(有预借) -->
									<xsl:if test="'1002'=../td[last()-1] or '1003'=../td[last()-1]" >
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openZCKZBXDPage('<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
									<!-- 资金管理付款单 -->
								  	<xsl:if test="'2201'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../bankroll/jiesuan/pay/update.html?load=&lt;pay_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/pay_id&gt;&lt;add&gt;3&lt;/add&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
									<!-- 资金管理收款单 -->
								  	<xsl:if test="'2202'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../bankroll/jiesuan/receive/update.html?load=&lt;receive_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/receive_id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<!-- 资金管理筹资单 -->
								  	<xsl:if test="'2203'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openCZDPage('&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;&lt;add&gt;3&lt;/add&gt;');
												</xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
									<!-- 资金管理筹资利息单 -->
								  	<xsl:if test="'2204'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openCZLXPage('&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;');
												</xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<!-- 资金管理筹资还款单 -->
								  	<xsl:if test="'2205'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openCZHKPage('&lt;return_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/return_id&gt;');
												</xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
									<!-- 资金管理投资单 -->
								  	<xsl:if test="'2206'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openTZDPage('&lt;invest_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/invest_id&gt;');
												</xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<!-- 资金管理投资收益单 -->
								  	<xsl:if test="'2207'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openTZSYPage('&lt;income_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/income_id&gt;');
												</xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
									<!-- 资金管理投资收回单 -->
								  	<xsl:if test="'2208'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openTZSHPage('&lt;return_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/return_id&gt;');
												</xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<!-- 资金管理定期存款单 -->
								  	<xsl:if test="'2209'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDepositPage('<xsl:value-of select="../td[last()]"/>');
												</xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<!-- 资金管理定期取款单 -->
								  	<xsl:if test="'2210'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openWithdrawPage('<xsl:value-of select="../td[last()]"/>');
												</xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<!-- 资金管理定期利息 -->
								  	<xsl:if test="'2211'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDQLXPage('<xsl:value-of select="../td[last()]"/>');
												</xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<!-- 资金管理活期利息 -->
								  	<xsl:if test="'2212'= ../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openHQLXPage('<xsl:value-of select="../td[last()]"/>');
												</xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
		          		</td>
		          	</xsl:when>
								<xsl:when test="position()=last()-3 or position()=last()-2">
									<td >
										<a href="#">
											<xsl:attribute name="onclick">openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;&lt;/edit_mask&gt;")</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="starts-with(.,'合同编号_') = true()">
									<td >
										<xsl:if test="$LINKFLAG =1" >
											<a href="#">
												<xsl:attribute name="onclick">openPact('<xsl:value-of select="substring-after(.,'_')"/>','<xsl:for-each select="../pk/mod_code"><xsl:value-of select="."/></xsl:for-each>')</xsl:attribute>
												<xsl:value-of select="substring-after(.,'_')"/>
											</a>
										</xsl:if>
										<xsl:if test="$LINKFLAG !=1" >
											<xsl:value-of select="substring-after(.,'_')"/>
										</xsl:if>
									</td>
								</xsl:when>
								<xsl:when test="starts-with(.,'金额_') = true()">
									<td align="right" >
										<xsl:value-of select="format-number(substring-after(.,'_'),'#,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="starts-with(.,'使用年数_') = true()">
									<td align="right" >
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=last() or position()=last()-1">
								</xsl:when>
								<xsl:otherwise>
									<td >
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>