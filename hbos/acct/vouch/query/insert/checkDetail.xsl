<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="colNum" select="count(//tr[1]/td)-6"/>    
    	<xsl:for-each select="/root/tbody/tr">
      		<xsl:if test="position() =1 ">
    		  <tr noWrap='true' class='mainHead'>
	          <xsl:for-each select="td">
	            <xsl:if test="position() = 2 ">
				  			<th nowrap='true' colspan="2"><xsl:value-of select="."/>年</th>
		      	  	<th nowrap='true' rowspan="2" valign="center">凭证号</th>
	            </xsl:if>
	            <xsl:if test="position() &gt; 4  and position() &lt;= $colNum">
				  			<th nowrap='true' rowspan="2" valign="center">项目</th>
	            </xsl:if>  

	          </xsl:for-each>
		      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
		      	<th nowrap='true' rowspan="2" valign="center">借方</th>
		      	<th nowrap='true' rowspan="2" valign="center">贷方</th>
		      	<th nowrap='true' rowspan="2" valign="center">方向</th>
		      	<th nowrap='true' rowspan="2" valign="center">余额</th>	          
		      </tr>
		      <tr noWrap='true' class='mainHead'>
		      	<th nowrap='true'>月</th>
		      	<th nowrap='true'>日</th>
		      </tr>
        	</xsl:if>
    	</xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=1">
			                <td>
			  	          	  <xsl:value-of select="substring(.,6,string-length(.))"/>
			  	          	</td>
		                </xsl:when>
		                <xsl:when test="position()=2">
			                <td>
			  	          	  <xsl:value-of select="substring(.,9,string-length(.))"/>
			  	          	</td>
		                </xsl:when>
		                <xsl:when test="position()=3">
			                <td>
			  	          	  <xsl:value-of select="../td[4]"/>
			  	          	</td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=last()">
		                </xsl:when>
		                <xsl:when test="position()=$colNum+2 or position()=$colNum+3 or position()=$colNum+5">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>		                
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

