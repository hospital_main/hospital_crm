<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	    <thead>
	    <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
  	  </tr> 	
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:colcount"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  </tr>   	
	  	  <tr noWrap='true' class='mainHead'>
	  			<td>凭证编号</td>
					<td>凭证日期</td>
					<td>附件</td>
					<td>摘要</td>
					<td>科目编码</td>
					<td>科目名称</td>
					<td>借方金额</td>
					<td>贷方金额</td>
					<td>制单人</td>
					<td>签字人</td>
					<td>审核人</td>
					<td>记账人</td>
	      </tr>
      </thead>
	  	<tbody> 
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td[position()!=13 and position()!=14 and position()!=15]">
							<td>
								<xsl:choose>
									<xsl:when test="position()=1">
										<xsl:if test=". != '合计'">
											<xsl:value-of select="."/>
											<xsl:if test="../td[13]!=''"><img src="chong.gif"/></xsl:if>
											<xsl:if test="../td[14]='1'"><img src="fei.gif"/></xsl:if>
						         </xsl:if>
		               <xsl:if test=". = '合计'">
							        <xsl:value-of select="."/>  
						       </xsl:if>
									</xsl:when>
									<xsl:when test="position()=4  or position()=6">
										<xsl:value-of select="."/>
									</xsl:when>
									<xsl:when test="position()=5">
									   <xsl:if test="../td[14]='1'">
	    								<xsl:value-of select="."/>
										</xsl:if>
										<xsl:if test="../td[14]!='1'">
	  									<xsl:value-of select="."/>
	  								</xsl:if>
									</xsl:when>
									<xsl:when test="position()=7 or position()=8">
										<xsl:if test=". != 0">
											<xsl:attribute name="align">right</xsl:attribute>
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</xsl:if>
									</xsl:when>
									<xsl:otherwise>
											<xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>