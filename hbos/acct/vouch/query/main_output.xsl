<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
  <xsl:variable name="comName" select="/root/@comName"/>
  <root>
    <colgroup>
 	    <col style='width:50mm'/>
 	    <col style='width:60mm'/>
 	    <col style='width:40mm'/>
 	    <col style='width:40mm'/>
    </colgroup>

    <xsl:for-each select='/root/table'>
      <tr height='50'><td border='none' colspan='4' style='text-align:center;'>记帐凭证</td></tr>
      <tr height='22'>
        <td border='none' colspan='4'>编制单位：<xsl:value-of select='top/td[1]'/></td>
      </tr>
      <tr height='22'>
        <td border='none'>凭证号：<xsl:value-of select='top/td[2]'/></td>
        <td border='none' colspan='2'>凭证日期：<xsl:value-of select='top/td[3]'/></td>
        <td border='none'>附单据号：<xsl:value-of select='top/td[4]'/>　页号：<xsl:value-of select='top/td[5]'/></td>
      </tr>
      <tr height='25'>
        <th>摘要</th>
        <th>会计科目</th>
        <th>借方金额</th>
        <th>贷方金额</th>
      </tr>
      <xsl:for-each select='tr'>
        <tr height='22'>
          <xsl:for-each select='td'>
            <xsl:if test='position()=3 or position()=4'>
              <td style='text-align:right'>
                <xsl:value-of select="."/>
              </td>
            </xsl:if>
            <xsl:if test='position()=1 or position()=2'>
              <td>
                <xsl:value-of select="."/>
              </td>
            </xsl:if>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
      <tr height='25'>
        <th>主管：<xsl:value-of select='bottom/td[5]'/></th>
        <th>记账：<xsl:value-of select='bottom/td[3]'/></th>
        <th>复核：<xsl:value-of select='bottom/td[1]'/></th>
        <th>制单：<xsl:value-of select='bottom/td[2]'/></th>
      </tr>
    </xsl:for-each>
  </root>
  </xsl:template>
</xsl:stylesheet>