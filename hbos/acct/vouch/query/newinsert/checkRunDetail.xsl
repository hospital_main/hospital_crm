<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
				<th>ѡ��</th>
    		<xsl:for-each select="/root/tbody/tr[td[last()]='0']">
          <xsl:for-each select="td[position() &lt; last() - 1 ]">
			      	<th nowrap='true'><xsl:value-of select="."/></th>
          </xsl:for-each>
    	</xsl:for-each>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr[td[last()]='1']">
	      <tr>     			
					<td align='center'  style='display:none'>
						<xsl:if test="td[last()-1]='1'">
						<input type='checkbox' TABINDEX='-1' checked='true' >
							<xsl:attribute name="onclick2">
								javascript:checkMoney()
							</xsl:attribute>
							<xsl:attribute name="onchange">
								javascript:checkMoney()
							</xsl:attribute>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
						</input>
						</xsl:if>
						<xsl:if test="td[last()-1]!='1'">
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="onclick2">
								javascript:checkMoney()
							</xsl:attribute>
							<xsl:attribute name="onchange">
								javascript:checkMoney()
							</xsl:attribute>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
								<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
						</input>
						</xsl:if>
					</td>

  		    <xsl:for-each select="td[position() &lt; last()-1]">	    		
  		        <xsl:if test="position() = last()  or position() = last() - 1 or position() = last() - 3 ">
	  		        <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
  		        </xsl:if>
  		        <xsl:if test=" position() = last() - 2 ">
	  		        <td><xsl:value-of select="."/></td>
  		        </xsl:if>
  		        <xsl:if test="position() &lt; last() - 3 ">
  		          <td><xsl:value-of select="."/> </td>
  		        </xsl:if>
  	  		</xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</xsl:template>
</xsl:stylesheet>