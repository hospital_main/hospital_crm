<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<xsl:if test="count(/root/tbody/tr)&lt;1">
					<th>单据号</th>
					<th>单据日期</th>
					<th>制单人</th>
					<th>审核人</th>
					<th>凭证号</th>
				</xsl:if>
				<xsl:for-each select="/root/tbody/tr[1]/td[position()!=1 and position() &lt; last()-1 ]">
					<th><xsl:value-of select="."/></th>
				</xsl:for-each>
			</tr>           
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr[position()!=1]">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
							<xsl:if test="td[last()-2]!=''">
								<xsl:attribute name="disabled">true</xsl:attribute>
							</xsl:if>
						</input>
					</td>
					<xsl:for-each select="td[position()!=1]">
							<xsl:choose>
								<xsl:when test="position()=1">
	          			<td>
		          			<xsl:if test="'0401'= ../td[last()-1] or '0404'= ../td[last()-1] or '0405'= ../td[last()-1] or '0408'= ../td[last()-1] or '0410'= ../td[last()-1] or '0412'= ../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/in/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0402'= ../td[last()-1] or '0402'= ../td[last()-1] or '0406'= ../td[last()-1] or '0407'= ../td[last()-1] or '0409'= ../td[last()-1] or '0411'= ../td[last()-1] or '0413'= ../td[last()-1] or '0416'= ../td[last()-1] or '0419'= ../td[last()-1] or '0421'= ../td[last()-1]" >hbos\mate\barcode\query\mate\whr\smp\in
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/smp/in/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0403'=../td[last()-1]" >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/out/common/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0414'=../td[last()-1] or '0415'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/out/move/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0417'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/second/advice/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0418'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/smp/out/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0420'=../td[last()-1] or '0423'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/whr/in/special/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
								  	<xsl:if test="'0424'=../td[last()-1] or '0425'=../td[last()-1] " >
						          <a href="#">
											  <xsl:attribute name="onclick" >
											    javascript:openDialog('../../../../mate/barcode/query/mate/pay/update.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
											  </xsl:attribute>
											  <xsl:value-of select="."/>
											</a>
								  	</xsl:if>
		          		</td>
		          	</xsl:when>
								<xsl:when test="position()=last()-2">
									<td >
									<!--
										<a href="#">
											<xsl:attribute name="onclick">openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;&lt;/edit_mask&gt;")</xsl:attribute>
										</a>	-->
											<xsl:value-of select="."/>
										
									</td>
								</xsl:when>
								<xsl:when test="position()=last() or position()=last()-1">
								</xsl:when>
								<xsl:otherwise>
									<td >
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



