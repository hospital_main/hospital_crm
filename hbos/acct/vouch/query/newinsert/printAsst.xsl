<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <colgroup>		       
        <col style = 'width:80'/>
        <col style = 'width:150'/>
      </colgroup>
      <thead> 
      	<tr class="mainHead" >
      		<td colspan="2"/>
      		<td style="display:none"/>
      	</tr> 
      	<tr class="mainHead">  
					<td>
			    	科目名称
			    </td>
					<td>
						辅助核算信息
					</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
		      <xsl:variable name="colName" select="td[1]"/>
		      <xsl:variable name="colPos"  select="position()"/> 
					<tr>
			      <xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[1] = $colName])"/>
						<xsl:for-each select="td[position() &gt; 1]"> 
								<xsl:if test="position() = 1">
									<xsl:if test="$colPos = 1">
										<td>
											<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
											<xsl:value-of select="."/>
										</td>
									</xsl:if>
									<xsl:if test="$colPos &gt; 1">
										<xsl:if test=" $colName != ../../tr[$colPos -1 ]/td[1]">
											<td>
												<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
												<xsl:value-of select="."/>
											</td>
										</xsl:if>
										<xsl:if test=" $colName = ../../tr[$colPos -1]/td[1]">
											<td style="display:none">
											</td>
										</xsl:if>
									</xsl:if>
								</xsl:if>
								
								<xsl:if test="position() = 2">
									<td>
										<xsl:value-of select="."/>
									</td>
							</xsl:if>
						</xsl:for-each>
					</tr>
				</xsl:for-each>	
			</tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>