<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <xsl:variable name="colNum" select="count(//tr[1]/td)"/> 
  		<xsl:variable name="ISTEMP" select="/root/annex/ISTEMP"/>
    	<xsl:for-each select="/root/tbody/tr[td[last() - 1]='0']">
    	  <tr noWrap='true' class='mainHead'>
	          <xsl:for-each select="td[position() &lt; last() - 1]">
				      	<th nowrap='true'><xsl:value-of select="."/></th>
	          </xsl:for-each>
        	</tr>
    	</xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[td[last() - 1]='1']">
      	
        <tr>
  		    <xsl:for-each select="td[position() &lt; last()]">
  		    		<xsl:if test="position() = 1">
	              <td align="left">
								<a href="#">
									<xsl:attribute name="onclick">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>","<xsl:value-of select="format-number(../td[last() - 3],'#,##0.00')"/>","<xsl:value-of select="format-number(../td[last() - 2],'#,##0.00')"/>")
									</xsl:attribute>
									<xsl:value-of select="."/>
								</a>
	              </td>
							</xsl:if>  				
  		        <xsl:if test="position() = 2 ">
  		        <td align="left">
  		        <xsl:choose>           
            			<xsl:when test="../td[last()]='' or ../td[last()]='null' or ../td[last()]='NULL'">
										<xsl:value-of select="."/>
		              </xsl:when>
		              <xsl:when test="$ISTEMP=1">
											<xsl:value-of select="."/>
		              </xsl:when>
		              <xsl:otherwise>
	                  	<a href="#">
										  <xsl:attribute name="onclick">
										    javascript:openVouchDlg("&lt;vouch_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;")
										  </xsl:attribute>
											<xsl:value-of select="."/>
											</a>
		              </xsl:otherwise>
            		</xsl:choose>
            		</td>
  		        </xsl:if>    		
  		        <xsl:if test="position() = last() - 1 or position() = last() - 2 ">
  		        <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
  		        </xsl:if>
  		        <xsl:if test="position() &lt; last() -2 and position() &gt; 2 ">
  		          <td><xsl:value-of select="."/> </td>
  		        </xsl:if>
  		      
  	  		</xsl:for-each>
  	  	</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
