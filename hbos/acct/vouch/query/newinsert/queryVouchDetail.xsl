<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<xsl:variable name="colNum" select="count(//tr[1]/td)-5"/>    
    	<xsl:for-each select="/root/tbody/tr">
      		<xsl:if test="position() =1 ">
    		  <tr noWrap='true' class='mainHead'>
    		  	<xsl:for-each select="td">
    		  	  <xsl:if test="position() = 2 ">
				  		<th nowrap='true' colspan="3">期间</th>
		      	  <th nowrap='true' rowspan="2" valign="center">凭证号</th>
		      	  </xsl:if> 
	            		<xsl:if test="position() &gt; 4  and position() &lt;= $colNum - 1">
				  <th nowrap='true' rowspan="2" valign="center"><xsl:value-of select="."/></th>
	            		</xsl:if>  
	          	</xsl:for-each>
		      	<th nowrap='true' rowspan="2" valign="center">摘要</th>
		      	<th nowrap='true' rowspan="2" valign="center">借方</th>
		      	<th nowrap='true' rowspan="2" valign="center">贷方</th>
		      	<th nowrap='true' rowspan="2" valign="center">方向</th>
		      	<th nowrap='true' rowspan="2" valign="center">余额</th>	          
		  </tr>
		  <tr noWrap='true' class='mainHead'>
	      	<th nowrap='true'>年</th>
	      	<th nowrap='true'>月</th>
	      	<th nowrap='true'>日</th>
	      </tr>
       </xsl:if>
    	</xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
	      <xsl:if test="position() &gt; 1">
		      <tr>
		          <xsl:for-each select="td">
		              <xsl:choose>
		                <xsl:when test="position()=3">
			                <td><a href='#'>
			                  <xsl:attribute name="onclick" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;');return false;
			  	          	  </xsl:attribute><xsl:value-of select="../td[4]"/>
			  	          	</a></td>
		                </xsl:when>
		                <xsl:when test="position()=4">
		                </xsl:when>
		                <xsl:when test="position()=$colNum+1 or position()=$colNum+2">
    			            <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		                </xsl:when>
		                <xsl:when test="position()=$colNum+4">
		                	<xsl:choose>	
			                	<xsl:when test="(.='' or .='0.00' or .=null or .='null') and ../td[$colNum + 3]='平'">
			                		<td align='right'>Q</td>
	    			          	</xsl:when>
				                <xsl:otherwise>
			                		<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
				                </xsl:otherwise>
			             		 </xsl:choose>
		                </xsl:when>
		                <xsl:when test="position()= $colNum + 5">
		                    
		                </xsl:when>
		                <xsl:when test="position()=2">
			                <td ><xsl:value-of select="substring-before(../td[1],'-')"/></td>
			                <td >
			                	<xsl:if test="../td[last()]=2">
			                		<xsl:value-of select="substring-before(substring-after(../td[2],'-'),'-')"/>
			                	</xsl:if>
			                	<xsl:if test="../td[last()]!=2">
			                		<xsl:value-of select="substring-after(../td[1],'-')"/>
			                	</xsl:if>
			                </td>
			                <td ><xsl:value-of select="substring-after(substring-after(../td[2],'-'),'-')"/></td>
		                </xsl:when>		   
		                <xsl:when test="position()=1">
		                    
		                </xsl:when>             
		                <xsl:otherwise>
		                  <td><xsl:value-of select="."/></td>
		                </xsl:otherwise>
		              </xsl:choose>		            
	  			  </xsl:for-each>
		  	  </tr>
	  	  </xsl:if>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

