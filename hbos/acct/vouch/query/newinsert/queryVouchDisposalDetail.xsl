<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th noWrap='true'>凭证号</th>
  			<th noWrap='true'>日期</th>
  			<th noWrap='true'>摘要</th>
  			<th noWrap='true'>借方发生额</th>
  			<th noWrap='true'>贷方发生额</th>
  			<th noWrap='true'>未核销金额</th>
				<th noWrap='true'>合同编号</th>
				<th noWrap='true'>单据号</th>
  		</tr>
  	</thead>
  	<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<td >
							<xsl:choose>
								<xsl:when test="position()=4 or position()=5 or position()=6">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</xsl:when>
								<xsl:when test="position()=7">
									<a href='#'>
                    <xsl:attribute name="onclick">openPact('<xsl:value-of select="."/>','<xsl:value-of select="../td[9]"/>');</xsl:attribute>
                    <xsl:value-of select="."/>
                  </a>
								</xsl:when>
								<xsl:when test="position()=8">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:when test="position()=9">
									<xsl:attribute name="style">display:none</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



