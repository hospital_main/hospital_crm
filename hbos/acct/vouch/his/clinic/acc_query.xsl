<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
 	<thead>
 		<tr noWrap='true' class='mainHead'>
			<th noWrap="true">收款员编码</th>
			<th noWrap="true">收款员姓名</th>
			<th noWrap="true">结算编码</th>
			<th noWrap="true">结算名称</th>
			<th noWrap="true">病人类别编码</th>
			<th noWrap="true">病人类别名称</th>
			<th noWrap="true">金额</th>
			<xsl:for-each select="/root/tbody/tr[td[1]='0']">
				<xsl:for-each select="td[position() &gt; 3]">
					<th noWrap="true"><xsl:value-of select="."/></th>
				</xsl:for-each>	
			</xsl:for-each>	
		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr[td[1]!='0']">
        <tr> 
	         <xsl:for-each select="td[position() &gt; 0]">
	          <xsl:if test="position() &lt; 7">
	          	<td>
								<xsl:value-of select="."/>
		        	</td>
						</xsl:if>
	          <xsl:if test="position()= 7">
	          	<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
		        	</td>
						</xsl:if>
		   		</xsl:for-each>
	  		</tr>
      </xsl:for-each>
 	</tbody>
  </xsl:template>
</xsl:stylesheet>