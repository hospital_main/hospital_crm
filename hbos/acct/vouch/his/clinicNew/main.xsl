<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
 	<thead>
 		<tr noWrap='true' class='mainHead'>
			<th noWrap="true">���ұ���</th>
			<th noWrap="true">��������</th>
			<xsl:for-each select="/root/tbody/tr[td[1]='0']">
				<xsl:for-each select="td[position() &gt; 3]">
					<th noWrap="true"><xsl:value-of select="."/></th>
				</xsl:for-each>	
			</xsl:for-each>	
		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr[td[1]!='0']">
        <tr> 
	         <xsl:for-each select="td[position() &gt; 1]">
	          <xsl:if test="position() &lt; 3">
	          	<td>
								<xsl:value-of select="."/>
		        	</td>
						</xsl:if>
	          <xsl:if test="position() &gt; 2">
	          	<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
		        	</td>
						</xsl:if>
		   		</xsl:for-each>
	  		</tr>
      </xsl:for-each>
 	</tbody>
  </xsl:template>
</xsl:stylesheet>