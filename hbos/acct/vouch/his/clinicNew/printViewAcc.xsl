<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
		<thead>
			<tr>
				<td style="fontsize:maintitle"><xsl:attribute name="colspan"><xsl:value-of select="7"/>住院收费</xsl:attribute></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td style="fontsize:subtitle"><xsl:attribute name="colspan"><xsl:value-of select="7"/></xsl:attribute></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
	 		<tr noWrap='true' class='mainHead'>
				<td noWrap="true">收款员编码</td>
				<td noWrap="true">收款员姓名</td>
				<td noWrap="true">结算编码</td>
				<td noWrap="true">结算名称</td>
				<td noWrap="true">病人类别编码</td>
				<td noWrap="true">病人类别名称</td>
				<td noWrap="true">金额</td>
			</tr>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=7">
								<td align="right">
									<xsl:value-of  select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
 	</tbody>

		</table>
	</xsl:template>
</xsl:stylesheet>
