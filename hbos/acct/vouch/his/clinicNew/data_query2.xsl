<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
 	<thead>
 		<tr noWrap='true' class='mainHead'>
			<th  width='25'><input type='checkbox'/></th>
			<th noWrap="true">收款员编码</th>
			<th noWrap="true">收款员姓名</th>
			<th noWrap="true" width="80">是否生成凭证</th>
			<xsl:for-each select="/root/tbody/tr[td[1]='0']">
				<xsl:for-each select="td[position() &gt; 4]">
					<th noWrap="true"><xsl:value-of select="."/></th>
				</xsl:for-each>	
			</xsl:for-each>	
		</tr>
  	</thead>
  	<tbody>	         
	    <xsl:for-each select="/root/tbody/tr[td[1]!='0']">
      <tr> 
			  <td align='center'>
			    <xsl:if test="td[2]!='' and td[4] = '否'">
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      		    </xsl:attribute>
	    	    </input>
	    	  </xsl:if>
	    	  <xsl:if test="td[2]!='' and td[4] = '是'">
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;' disabled='disabled'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      		    </xsl:attribute>
	    	    </input>
	    	  </xsl:if>
	      </td>          	
        <xsl:for-each select="td[position() &gt; 1]"> 
          <xsl:if test="position() &lt;= 3">
          	<td><xsl:value-of select="."/></td>
					</xsl:if>
          <xsl:if test="position() &gt; 3">
          	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
					</xsl:if>
	   		</xsl:for-each>
  		</tr>
    </xsl:for-each>
 	</tbody>
  </xsl:template>
</xsl:stylesheet>