<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/vouch/his/balance/printView.xsl,v 1.3 2015/04/08 06:02:34 yaoqingsong Exp $
 $Author: yaoqingsong $
 $Date: 2015/04/08 06:02:34 $
 $Revision: 1.3 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
		<thead>
			<tr>
				<td style="colspan:8;fontsize:maintitle">住院结算</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td style="colspan:8;fontsize:subtitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td noWrap="true">收款员编码</td>
				<td noWrap="true">收款员名称</td>
				<td noWrap="true">结算编码</td>
				<td noWrap="true">结算名称</td>
				<td noWrap="true">病人类别编码</td>
				<td noWrap="true">病人类别名称</td>
				<td noWrap="true">收费金额</td>
				<td noWrap="true">是否生成凭证</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=7">
								<td style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="translate(.,'|||','')"/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
