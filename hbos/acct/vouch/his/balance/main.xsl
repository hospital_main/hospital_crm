<?xml version='1.0' encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">

		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none' width='25'><input type='checkbox'/></th>
				<th noWrap="true">收款员编码</th>
				<th noWrap="true">收款员名称</th>
				<th noWrap="true">结算编码</th>
				<th noWrap="true">结算名称</th>
				<th noWrap="true">病人类别编码</th>
				<th noWrap="true">病人类别名称</th>
				<th noWrap="true">收费金额</th>
				<th noWrap="true">是否生成凭证</th>
				<!--th noWrap="true">凭证号</th-->
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'>  
					  <xsl:if test="td[1]!='' and td[8] = '是'">          
		            <input type='checkbox' TABINDEX='-1' style='font-size:8px;' disabled='disabled'>
		              <xsl:attribute name="value" >
		                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		      		  </xsl:attribute>		      		  
			    	  </input>
			    	</xsl:if>
			    	<xsl:if test="td[1]!='' and td[8] = '否'">          
		            <input type='checkbox' TABINDEX='-1' style='font-size:8px;' >
		              <xsl:attribute name="value" >
		                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		      		  </xsl:attribute>		      		  
			    	  </input>
			    	</xsl:if>
			    </td>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=7">
								<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
  </xsl:template>

</xsl:stylesheet>
