<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
						<td style="fontsize:maintitle;"><xsl:attribute name="colspan">11</xsl:attribute></td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
				</tr>
				<tr noWrap='true' class='mainHead'>
  			<td noWrap='true'>收费日期</td>
  			<td noWrap='true'>收款员编码</td>
  			<td noWrap='true'>收款员名称</td>
  			<td noWrap='true'>支付方式</td>
  			<td noWrap='true'>卡号</td>
  			<td noWrap='true'>病历号</td>
  			<td noWrap='true'>病人姓名</td>
  			<td noWrap='true'>收费金额</td>
  			<td noWrap='true'>票据号</td>
  			<td noWrap='true'>住院号</td>
  			<td noWrap='true'>状态</td>
  			<td noWrap='true'>凭证号</td>
  		</tr>
			</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
						<xsl:choose>
							<xsl:when test="position() = 8">
								<td align="right">									
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>	
							<xsl:otherwise>
								<td align="left"><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>
