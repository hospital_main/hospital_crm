<?xml version='1.0' encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">

		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none' width='25'><input type='checkbox'/></th>
				<th noWrap="true">收款员编码</th>
				<th noWrap="true">收款员名称</th>
				<th noWrap="true">支付编码</th>
				<th noWrap="true">支付名称</th>
				<th noWrap="true">收费金额</th>
				<th noWrap="true">是否生成凭证</th>
				<th noWrap="true">操作</th>
				<!--th noWrap="true">凭证号</th-->
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'>  
					  <xsl:if test="td[1]!='' and td[6]='是'">          
		            <input type='checkbox' TABINDEX='-1' style='font-size:8px;' disabled='disabled'>
		              <xsl:attribute name="value" >
		                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		      		  </xsl:attribute>
			    	</input>
			    	</xsl:if>
			    	<xsl:if test="td[1]!='' and td[6]='否'">          
		            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
		              <xsl:attribute name="value" >
		                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		      		  </xsl:attribute>
			    	</input>
			    	</xsl:if>
			    </td>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=5">
								<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							
							<xsl:when test="position()=7 and ../td[1]!=''">
				            <td>
				            	<a tabindex='-1'>
						           <xsl:attribute name="onclick" >
						           javascript:preDetailClick("<xsl:value-of select="../td[1]"/>,<xsl:value-of select="../td[3]"/>")
					  	          </xsl:attribute>
					  	          <xsl:attribute name="href" >#</xsl:attribute>
					  	          <xsl:value-of select="."/>
				                </a>		                	
				            </td>
	                		</xsl:when> 
							
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
  </xsl:template>

</xsl:stylesheet>
