<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/vouch/his/prestore/printView.xsl,v 1.3 2013/01/30 05:26:42 pengjin Exp $
 $Author: pengjin $
 $Date: 2013/01/30 05:26:42 $
 $Revision: 1.3 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
		<thead>
			<tr>
				<td style="colspan:5;fontsize:maintitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td style="colspan:5;fontsize:subtitle"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td style="border:thin;fontsize:coltitle">收款员编码</td>
				<td style="border:thin;fontsize:coltitle">收款员名称</td>
				<td style="border:thin;fontsize:coltitle">支付编码</td>
				<td style="border:thin;fontsize:coltitle">支付名称</td>
				<td style="border:thin;fontsize:coltitle">收费金额</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=5">
								<td style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="translate(.,'|||','')"/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
