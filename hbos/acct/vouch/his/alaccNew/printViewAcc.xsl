<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/vouch/his/alacc/printViewAcc.xsl,v 1.1 2015/05/05 14:53:22 yaoqingsong Exp $
 $Author: yaoqingsong $
 $Date: 2015/05/05 14:53:22 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
			<thead>
				<tr>
					<td colspan="7" style="fontsize:maintitle"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr>
					<td colspan="7" style="fontsize:subtitle"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
		 		<tr noWrap='true' class='mainHead'>
					<td noWrap="true">收款员编码</td>
					<td noWrap="true">收款员名称</td>
					<td noWrap="true">结算编码</td>
					<td noWrap="true">结算名称</td>
					<td noWrap="true">病人类别编码</td>
					<td noWrap="true">病人类别名称</td>
					<td noWrap="true">收费金额</td>
				</tr>
	  	</thead>
	  	<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td"> 
							<xsl:choose>
								<xsl:when test="position()=7">
									<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
