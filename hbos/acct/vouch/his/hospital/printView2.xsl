<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/vouch/his/hospital/printView2.xsl,v 1.2 2015/04/08 02:55:05 yaoqingsong Exp $
 $Author: yaoqingsong $
 $Date: 2015/04/08 02:55:05 $
 $Revision: 1.2 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
		<thead>
			<xsl:variable name="colCnt" select="count(/root/tbody/tr[1]/td[position() &gt; 1])"/>
			<tr>
				<td style="fontsize:maintitle"><xsl:attribute name="colspan"><xsl:value-of select="$colCnt"/>住院收费</xsl:attribute></td>
				<xsl:for-each select="/root/tbody/tr[td[1]='0']">
					<xsl:for-each select="td[position() &gt; 2]">
						<td style="display:none"></td>
					</xsl:for-each>	
				</xsl:for-each>	
			</tr>
			<tr>
				<td style="fontsize:subtitle"><xsl:attribute name="colspan"><xsl:value-of select="$colCnt"/></xsl:attribute></td>
				<xsl:for-each select="/root/tbody/tr[td[1]='0']">
					<xsl:for-each select="td[position() &gt; 2]">
						<td style="display:none"></td>
					</xsl:for-each>	
				</xsl:for-each>	
			</tr>
	 		<tr noWrap='true' class='mainHead'>
				<td noWrap="true">收款员编码</td>
				<td noWrap="true">收款员姓名</td>
				<td noWrap="true">是否生成凭证</td>
				<xsl:for-each select="/root/tbody/tr[td[1]='0']">
					<xsl:for-each select="td[position() &gt; 4]">
						<td noWrap="true"><xsl:value-of select="."/></td>
					</xsl:for-each>	
				</xsl:for-each>	
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr[td[1]!='0']">
        <tr> 
	         <xsl:for-each select="td[position() &gt; 1]">
	          <xsl:if test="position() &lt; 4">
	          	<td>
								<xsl:value-of select="."/>
		        	</td>
						</xsl:if>
	          <xsl:if test="position() &gt; 3">
	          	<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
		        	</td>
						</xsl:if>
		   		</xsl:for-each>
	  		</tr>
      </xsl:for-each>
 	</tbody>

		</table>
	</xsl:template>
</xsl:stylesheet>
