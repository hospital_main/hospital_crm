<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th noWrap='true'>凭证日期</th>
  			<th noWrap='true'>凭证编号</th>
  			<!--th noWrap='true'>新凭证号</th-->
  			<th noWrap='true'>附件</th>
  			<th noWrap='true'>制单人</th>
  		</tr>
  	</thead>
  	<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position()&lt;5]">
						<td >
							<xsl:choose>
								<xsl:when test="position()=3 ">
									<xsl:attribute name="class">numberText</xsl:attribute>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



