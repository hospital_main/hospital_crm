<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<root>
			<thead>
				<tr>
						<td style="fontsize:maintitle;"><xsl:attribute name="colspan">6</xsl:attribute></td>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
						<td style="display:none"/>
					
				</tr>
				<tr noWrap='true' class='mainHead'>
  			<td noWrap='true'>单据号</td>
  			<td noWrap='true'>付款日期</td>
  			<td noWrap='true'>供应商</td>
  			<td noWrap='true'>支付方式</td>
  			<td noWrap='true'>金额</td>
  			<td noWrap='true'>付款人</td>
  		</tr>
			</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
						<xsl:choose>
							<xsl:when test="position() = 5">
								<td align="right">									
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>	
							<xsl:otherwise>
								<td align="left"><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>
