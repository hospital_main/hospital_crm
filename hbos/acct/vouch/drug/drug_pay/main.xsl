<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style='display:'><input type='checkbox'/></th>
  			<th noWrap='true'>单据号</th>
  			<th noWrap='true'>付款日期</th>
  			<th noWrap='true'>供应商</th>
  			<th noWrap='true'>支付方式</th>
  			<th noWrap='true'>金额</th>
  			<th noWrap='true'>付款人</th>
  			<th noWrap='true'>是否生成</th>
  		</tr>
  	</thead>
  	<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:'>
						<input type='checkbox' TABINDEX='-1' >
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
							<xsl:if test="td[last()]!=''">
								<xsl:attribute name="disabled">true</xsl:attribute>
							</xsl:if>
						</input>
						
					</td>
					<xsl:for-each select="td">						
						<xsl:choose>
							<xsl:when test="position() = 5">
								<td align="right">									
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>	
							<xsl:otherwise>
								<td align="left"><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



