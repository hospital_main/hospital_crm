<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/vouch/drug/printView.xsl,v 1.1 2012/05/09 08:47:55 pengjin Exp $
 $Author: pengjin $
 $Date: 2012/05/09 08:47:55 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<colgroup>
			<col style = 'width:50mm'/>
			<col style = 'width:150mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
		</colgroup>
		<thead>
			<tr noWrap='true' class='mainHead'>
				<td>凭证号</td>
				<td>凭证类型</td>
				<td>日期</td>
				<td>附件</td>
				<td>制单人</td>
				<td>审核人</td>
				<td>记账人</td>
			</tr>
		</thead>
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position()!=9]">
						<xsl:choose>
							<xsl:when test="position()=8">
							</xsl:when>
							<xsl:otherwise>
								<td >
								<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
