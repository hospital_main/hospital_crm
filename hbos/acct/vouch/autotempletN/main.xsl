<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
      	<xsl:variable name="flag" select="/root/tbody/tr[1]/td[10]" />
      	<xsl:if test="'0' = $flag">
		  		<th style="display:none" nowrap='true'>Ƭ��</th>
		  	</xsl:if>
		  	<xsl:if test="'1' = $flag">
		  		<th nowrap='true'>Ƭ��</th>
		  	</xsl:if>
		  	<th nowrap='true'>���</th>
		  	<th nowrap='true'>ժҪ</th>
		  	<th nowrap='true'>�������</th>
		  	<th nowrap='true'>����</th>
		  	<th nowrap='true'>��Ŀ����</th>
		  	<th nowrap='true'>��Ŀ����</th>
		  	<th style="display:none" nowrap='true'>is_auto_create</th>
		  	<th style="display:none" nowrap='true'>is_group</th>
		  	<th nowrap='true'>Ԥ���Ŀ����</th>
		  	<th nowrap='true'>Ԥ������</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="unit_name" select="td[1]" />
      	<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[1]=$unit_name])"/>
      	<xsl:variable name="cur_pos" select="position()" />
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test="$cur_pos = 1 ">
										<xsl:if test="'1' = $flag">
											<td nowrap="true" rowspan="{$rowspan}">
												<xsl:value-of select="."/>
											</td>
										</xsl:if>
										<xsl:if test="'0' = $flag">
											<td style="display:none" rowspan="{$rowspan}">
												<xsl:value-of select="."/>
											</td>
										</xsl:if>
									</xsl:if>
									<xsl:if test="$cur_pos >1 and $unit_name != ../../tr[$cur_pos - 1]/td[1]">
										<xsl:if test="'1' = $flag">
											<td nowrap="true" rowspan="{$rowspan}">
												<xsl:value-of select="."/>
											</td>
										</xsl:if>
										<xsl:if test="'0' = $flag">
											<td style="display:none" rowspan="{$rowspan}">
												<xsl:value-of select="."/>
											</td>
										</xsl:if>
									</xsl:if>
									<xsl:if test="$cur_pos >1 and $unit_name = ../../tr[$cur_pos - 1]/td[1]">
										<td style="display:none"><xsl:value-of select="."/>
									  </td>
									</xsl:if>			
								</xsl:when>
								<xsl:when test="position()=3">
									<td>
										<!--<input type="text" name="col4" >
											<xsl:attribute name="value" >
												<xsl:value-of select="."/>
											</xsl:attribute>
											<xsl:attribute name="pkvalue" >
												<xsl:for-each select='../pk/*'>&lt;<xsl:value-of select='name()'/>&gt;<xsl:value-of select='.'/>&lt;/<xsl:value-of select='name()'/>&gt;</xsl:for-each>
											</xsl:attribute>
										</input>-->
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test=" position()=5">
	          			<td>
										<xsl:if test="'1'=../td[8] " >
											<a href="#">
												<xsl:choose>
												<xsl:when test="'100'=../pk/rela_type">
												   <xsl:attribute name="onclick" >
										javascript:openDialog("../../../../hbos/fund/base/fundkjsubj/main.html?load=<xsl:for-each select='../pk/*'>&lt;<xsl:value-of select='name()'/>&gt;<xsl:value-of select='.'/>&lt;/<xsl:value-of select='name()'/>&gt;</xsl:for-each>",'dialogWidth:900px;dialogHeight:600px',result);init();
											</xsl:attribute>
												  </xsl:when>
													<xsl:when test="'99'=../pk/rela_type">
												   <xsl:attribute name="onclick" >
										javascript:openDialog("../../../../hbos/fund/base/finausage/main.html?load=<xsl:for-each select='../pk/*'>&lt;<xsl:value-of select='name()'/>&gt;<xsl:value-of select='.'/>&lt;/<xsl:value-of select='name()'/>&gt;</xsl:for-each>",'dialogWidth:900px;dialogHeight:600px',result);init();
											</xsl:attribute>
												  </xsl:when>
													<xsl:when test="'98'=../pk/rela_type">
												   <xsl:attribute name="onclick" >
										javascript:openDialog("../../../../hbos/bankroll/sysset/bankaccount/mainType1.html",'dialogWidth:900px;dialogHeight:600px',result);init();
											</xsl:attribute>
												  </xsl:when>
												  <xsl:when test="'97'=../pk/rela_type">
												   <xsl:attribute name="onclick" >
										javascript:openDialog("../../../../hbos/bankroll/sysset/bankaccount/mainType2.html",'dialogWidth:900px;dialogHeight:600px',result);init();
											</xsl:attribute>
												  </xsl:when>
													<xsl:when test="'96'=../pk/rela_type">
												   <xsl:attribute name="onclick" >
										javascript:openDialog("../../../../hbos/bankroll/invest/basisset/type/main.html",'dialogWidth:900px;dialogHeight:600px',result);init();
											</xsl:attribute>
												  </xsl:when>
													<xsl:when test="'95'=../pk/rela_type">
												   <xsl:attribute name="onclick" >
										javascript:openDialog("../../../../hbos/bankroll/financing/basisset/type/main.html",'dialogWidth:900px;dialogHeight:600px',result);init();
											</xsl:attribute>
												  </xsl:when>
												  <xsl:when test="'94'=../pk/rela_type">
												   <xsl:attribute name="onclick" >
										javascript:openDialog("../../../../hbos/bankroll/sysset/fundproject/project/main2.html",'dialogWidth:900px;dialogHeight:600px',result);init();
											</xsl:attribute>
												  </xsl:when>
												  <xsl:when test="'93'=../pk/rela_type">
												   <xsl:attribute name="onclick" >
										javascript:openDialog("../../../../hbos/fund/base/payout/main.html",'dialogWidth:900px;dialogHeight:600px',result);init();
											</xsl:attribute>
												  </xsl:when>
												  <xsl:when test="'92'=../pk/rela_type">
												   <xsl:attribute name="onclick" >
										javascript:openDialog("../../../../hbos/bdgt/baseSet/base/subj/budgset.html",'dialogWidth:900px;dialogHeight:600px',result);init();
											</xsl:attribute>
												  </xsl:when>
												  <xsl:when test="'91'=../pk/rela_type">
												   <xsl:attribute name="onclick" >
										javascript:openDialog("../../../../hbos/payctl3/base/paytype/main.html",'dialogWidth:900px;dialogHeight:600px',result);init();
											</xsl:attribute>
												  </xsl:when>
												  <xsl:otherwise>
												    <xsl:attribute name="onclick" >
										javascript:openDialog("auto.html?load=<xsl:for-each select='../pk/*'>&lt;<xsl:value-of select='name()'/>&gt;<xsl:value-of select='.'/>&lt;/<xsl:value-of select='name()'/>&gt;</xsl:for-each>",'dialogWidth:900px;dialogHeight:600px',result);init();
											</xsl:attribute>
												  </xsl:otherwise>
												</xsl:choose>																					
											<xsl:value-of select="."/></a>
								  	</xsl:if>
								  	<xsl:if test="'0'=../td[8] " >
									  	<xsl:if test="'0'=../td[9]" >
							          <a href="#">
							          	<xsl:choose>
												  <xsl:when test="'92'=../pk/rela_type">
												   <xsl:attribute name="disabled" >false</xsl:attribute>
												  </xsl:when>
												  <xsl:when test="'91'=../pk/rela_type">
												   <xsl:attribute name="onclick" >
										javascript:openDialog("../../../../hbos/payctl3/base/paytype/main.html",'dialogWidth:700px;dialogHeight:400px',result);init();
											</xsl:attribute>
												  </xsl:when>
												  <xsl:otherwise>
												    <xsl:attribute name="onclick" >
										javascript:openDialog("update.html?load=<xsl:for-each select='../pk/*'>&lt;<xsl:value-of select='name()'/>&gt;<xsl:value-of select='.'/>&lt;/<xsl:value-of select='name()'/>&gt;</xsl:for-each>",'dialogWidth:700px;dialogHeight:400px',result);init();
											</xsl:attribute>
												  </xsl:otherwise>
												</xsl:choose>	
												  
												  <xsl:value-of select="."/>
												</a>
									  	</xsl:if>
									  	<xsl:if test="'1'=../td[9]" >
							          <a href="#">
												  <xsl:choose>
												  <xsl:when test="'92'=../pk/rela_type">
												   <xsl:attribute name="disabled" >false</xsl:attribute>
												  </xsl:when>
												  <xsl:when test="'91'=../pk/rela_type">
												   <xsl:attribute name="onclick" >
										javascript:openDialog("../../../../hbos/payctl3/base/paytype/main.html",'dialogWidth:700px;dialogHeight:400px',result);init();
											</xsl:attribute>
												  </xsl:when>
												  <xsl:otherwise>
												    <xsl:attribute name="onclick" >
										javascript:openDialog("update1.html?load=<xsl:for-each select='../pk/*'>&lt;<xsl:value-of select='name()'/>&gt;<xsl:value-of select='.'/>&lt;/<xsl:value-of select='name()'/>&gt;</xsl:for-each>",'dialogWidth:700px;dialogHeight:400px',result);init();
											</xsl:attribute>
												  </xsl:otherwise>
												</xsl:choose>	
												  <xsl:value-of select="."/>
												</a>
									  	</xsl:if>
								  	</xsl:if>
								  </td>
							  </xsl:when>
							  <xsl:when test=" position()=8 or position()=9 or position()=10">
							  	<td style="display:none" ><xsl:value-of select="."/></td>
							  </xsl:when>
							  <!--<xsl:when test=" position()=8 or position()=9 or position()=10">
							  	<td><xsl:value-of select="../pk/rela_type"/></td>
							  </xsl:when>-->
							 
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>