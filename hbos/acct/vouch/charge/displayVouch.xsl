<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead> 
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th> 
      	<th nowrap='true'>凭证号</th>
      	<th nowrap='true'>凭证日期</th>
      	<th nowrap='true'>凭证类型</th>
      	<th nowrap='true'>制单人</th>
      	<th nowrap='true'>审核人</th>
      	<th nowrap='true' style="display:none"> </th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' name='select'>
            </input>
          </td>
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test="position()=last() ">
		          			<th  style="display:none" >
				            	<xsl:value-of select="."/>
		          			</th>
	          	</xsl:when>
			        <xsl:otherwise> 
			         	<td>
									<xsl:value-of select="."/>
								</td> 
			        </xsl:otherwise> 
          	</xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

