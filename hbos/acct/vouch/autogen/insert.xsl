<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th nowrap='true'>提取年月</th>
				<th nowrap='true'>所在部门</th>
				<th nowrap='true'>资产卡片号</th>
				<th nowrap='true'>资产编码</th>
				<th nowrap='true'>资产名称</th>
				<th nowrap='true'>型号规格</th>
				<th nowrap='true'>启用日期</th>
				<th nowrap='true'>资产原值</th>
				<th nowrap='true'>本月计提</th>
				<th nowrap='true'>累计计提</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><xsl:value-of select="."/></td>
              </xsl:when>  
              <xsl:when test="position() = 9 ">
                <td class='numberText' nowrap='true'>
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
      		      </td>
              </xsl:when>
              <xsl:when test="position() = 8 or position() = 10">
                <td class='numberText' nowrap='true'>
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>       		 	
      		      </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

