<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
		<thead>
			<tr noWrap='true' class='mainHead'>
				<xsl:if test="count(/root/tbody/tr)&lt;1">
					<td>单据号</td>
					<td>单据日期</td>
					<td>制单人</td>
					<td>审核人</td>
					<td>凭证号</td>
				</xsl:if>
				<xsl:for-each select="/root/tbody/tr[1]/td[position()!=1 and position() &lt; last()-1 ]">
					<td><xsl:value-of select="."/></td>
				</xsl:for-each>
			</tr>           
		</thead>  	
		<tbody> 
			<tr>
				<td/>
				<xsl:for-each select="/root/tbody/tr[1]/td[position()!=1 and position() != last() and position() != last() -1]">
					<xsl:if test=". = '金额'">
						<xsl:variable name="pos" select="position()"/>
						<td align="center">
							<xsl:attribute name="colspan"><xsl:value-of select=" $pos - 2 "/></xsl:attribute>
							合计
						</td>
						<xsl:for-each select="/root/tbody/tr[1]/td[position() != 1 and position() &lt; ($pos - 1)]">
							<td style="display:none"/>
						</xsl:for-each>
						<td align="right">
							<xsl:value-of select="format-number(sum(/root/tbody/tr[position()>1]/td[$pos + 1]),'#,##0.00')"/>
						</td>
						<xsl:for-each select="/root/tbody/tr[1]/td[position()!=1 and position() &gt; $pos and position() != last() and position() != last() -1]">
							<td/>
						</xsl:for-each>
					</xsl:if>
				</xsl:for-each>
			</tr>

			<xsl:for-each select="/root/tbody/tr[position()!=1]">
				<tr>
					<xsl:for-each select="td[position()!=1]">
						<xsl:variable name="cols" select="position()"/>
							<xsl:choose>
								<xsl:when test="position()=1">
	      					<td >
										<xsl:value-of select="."/>
									</td>
		          	</xsl:when>
		          	<xsl:when test="/root/tbody/tr[1]/td[( $cols+ 1)] = '金额'  " >
									<td align="right" >
										<xsl:value-of select="format-number(.,'#,##0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="/root/tbody/tr[1]/td[($cols+ 1 )] = '使用年数'  " >
									<td align="right" >
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=last()-2">
									<td >
										<a href="#">
											<xsl:attribute name="onclick">openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;&lt;/edit_mask&gt;")</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=last() or position()=last()-1">
								</xsl:when>
								<xsl:otherwise>
									<td >
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
