<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style=''>凭证号</th>
				<th nowrap='true'>日期</th>
				<th nowrap='true'>科目</th>
				<th nowrap='true'>摘要</th>
				<th nowrap='true'>借方金额</th>
				<th nowrap='true'>贷方金额</th>
				<th nowrap='true'>现金项目</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<td>
							<xsl:choose>
								<xsl:when test="position()=1">
										<a href="#">
											<xsl:attribute name="onclick">
											  javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
											  </xsl:attribute>
											<xsl:value-of select="."/>
										</a>
								</xsl:when>
								<xsl:when test="position()=3">
										<a href="#">
											<xsl:attribute name="onclick">
											  javascript:openDialog('../query/newinsert/cash.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;'+escape('<xsl:value-of select="."/>')+'&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>',"dialogWidth:800px;dialogHeight:550px;")
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
								</xsl:when>
								
								<xsl:when test="position()=5 or position()=6">
									<xsl:if test=". != '0.0000'">
										<xsl:attribute name="class">numberText</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</xsl:when>
								
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>