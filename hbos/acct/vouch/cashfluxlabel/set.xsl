<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
			<th noWrap="true">科目编码</th>
			<th noWrap="true">科目名称</th>
			<th noWrap="true">现金项目编码</th>
			<th noWrap="true">现金项目名称</th>
			
		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	        <tr> 
	         <xsl:for-each select="td">
	           <xsl:choose>
							<xsl:when test="position() = 3">
							<td width="20%">
								<table width="100%">
									<tr>
										<td>
											<xsl:attribute name="ondblclick">
												clearSubj();
											</xsl:attribute>
											<xsl:value-of select="."/>
										</td>
										<td>
										  [<a >
											<xsl:attribute name="onclick">
												setting();
											</xsl:attribute>
											<xsl:attribute name="href">
												#
											</xsl:attribute>
											设置
											</a>]
										</td>
									</tr>
								</table>
								</td>
							</xsl:when>
							<xsl:when test="position() = 4">
							<td width="30%">
								<xsl:value-of select="."/> 
								</td>
							</xsl:when>
							<xsl:otherwise>
							<td>
								<xsl:value-of select="."/>  
							</td>
							</xsl:otherwise>
					</xsl:choose>    
		   </xsl:for-each>
		   
  		</tr>
   	    </xsl:for-each>
 	</tbody>
  </xsl:template>
</xsl:stylesheet>