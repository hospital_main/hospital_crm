<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>资产编码</th>
				<th>资产名称</th>
				<th>规格型号</th>
				<th>计量单位</th>
				<th>期初库存金额</th>
				<th>本期增加金额</th>
				<th>本期减少金额</th>
				<th>期末结存金额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">                        
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
								  <xsl:value-of select="."/>
								</td >
							</xsl:when>
							<xsl:when test="position()=5 or position()=6 or position()=7 or position()=8">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td ><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>                                   
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>


