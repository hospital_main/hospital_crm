<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th>资产卡片号</th>
        <th>资产编号</th>
				<th>资产名称</th>				
				<th>型号规格</th>
				<th>产地厂家</th>
				<th>计量单位</th>
				<th>原值</th>
				<th>卡片数量</th>
				<th>入库日期</th>
				<th>启用日期</th>
				<th>所在部门</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
             <td align="center">
               <xsl:value-of select="."/>
             </td>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


