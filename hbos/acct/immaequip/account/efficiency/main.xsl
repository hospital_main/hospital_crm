<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        
		    <th rowspan="2" valign="middle" >资产编码</th>
		    <th rowspan="2" valign="middle" >资产名称</th>
		    <th rowspan="2" valign="middle" >资产卡片号</th>
		    <th rowspan="2" valign="middle" >在用科室</th>
		    <th rowspan="2" valign="middle" >资产原值</th>
		    <th colspan="5" valign="middle" >费用</th>
		    <th colspan="2" valign="middle" >收益</th>
		    <th rowspan="2" valign="middle" >投入/产出比率</th>
  		</tr>
  		<tr noWrap="true" class="mainHead"> 
		    <th  valign="middle" class="style4">保养费用</th>
		    <th  valign="middle" class="style4">维修费用</th>
		    <th  valign="middle" class="style4">检测计量费用</th>
		    <th  valign="middle" class="style4">累计折旧</th>
		    <th  valign="middle" class="style4">运行机时</th>
		    <th  valign="middle" class="style4">使用人次</th>
		    <th  valign="middle" class="style4">收入</th>
		  </tr>  		
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td>
                    <a tabindex='-1'><xsl:value-of select="."/></a>
                  </td>
                </xsl:when>
                <xsl:when test="position()=3 or position()=4">
	                  <td align="left"><xsl:value-of select="."/></td>
	                </xsl:when>
                <xsl:when test="position()=5 or position()=6 or position()=8 or position()=7 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13">
	                  <td align="right">
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </td>
	              </xsl:when>
                <xsl:otherwise>
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>


