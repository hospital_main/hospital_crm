<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>类别编码</th>
				<th>类别名称</th>
				<th>原值</th>
				<th>本期计提</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td[position() &lt; 5]">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td>
                  	<xsl:if test="../td[5] = '1'">
                  	<a tabindex="-2">
                    	<xsl:attribute name="href" >
                      	javascript:openDialog('querydepre.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:800px;dialogHeight:600px') 
                      	<!-- javascript:openDialog('querydepre.html?load=&lt;root&gt;&lt;year1&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/year1&gt;&lt;month1&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/month1&gt;&lt;year2&gt;<xsl:value-of select="../td[position()=3]"/>&lt;/year2&gt;&lt;month2&gt;<xsl:value-of select="../td[position()=4]"/>&lt;/month2&gt;&lt;equi_kind_code&gt;<xsl:value-of select="../td[position()=5]"/>&lt;/equi_kind_code&gt;&lt;equi_kind_name&gt;<xsl:value-of select="../td[position()=6]"/>&lt;/equi_kind_name&gt;&lt;/root&gt;', 'dialogWidth:900px;dialogHeight:600px',name="equi_depreciation_select",result) -->                     	   
                    	</xsl:attribute>
                    	<xsl:value-of select="."/>
                  	</a>
                  	</xsl:if>
                  	<xsl:if test="../td[5] != '1'">
                    	<xsl:value-of select="."/>
                  	</xsl:if>
                  </td >
                </xsl:when> 
                <xsl:when test="position()=3">
									<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td >
								</xsl:when>
								<xsl:when test="position()=4">
									<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td >
								</xsl:when>
                <xsl:otherwise>
                  <td ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
