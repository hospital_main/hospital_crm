<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		<tr noWrap='true'>
        <td style='fontsize:subtitle;colspan:colcount;align:left'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		<tr noWrap="true" class="mainHead">
        <!--td style="display:none"><input type="checkbox"/></td-->
		    <td rowspan="2" valign="middle" >日期</td>
		    <td rowspan="2" valign="middle" >摘要</td>
		    <td colspan="2" valign="middle">借</td>
		    <td style="display:none"></td>
		    <td colspan="2" valign="middle">贷</td>
		    <td style="display:none"></td>
		    <td colspan="2" valign="middle" >余额</td>
		    <td style="display:none"></td>
  		</tr>
  		<tr noWrap="true" class="mainHead">
  		  <td style="display:none"></td>
  		  <td style="display:none"></td> 
		    <td  valign="middle" class="style4">数量</td>
		    <td  valign="middle" class="style4">金额</td>
		    <td  valign="middle" class="style4">数量</td>
		    <td  valign="middle" class="style4">金额</td>
		    <td  valign="middle" class="style4">数量</td>
		    <td  valign="middle" class="style4">金额</td>
		  </tr>  		
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <!--td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td-->
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td>
                    <a tabindex='-1'><xsl:value-of select="."/></a>
                  </td>
                </xsl:when>
                <xsl:when test="position()=3 or position()=5 or position()=7">
	                  <td align="right"><xsl:value-of select="."/></td>
	                </xsl:when>
                <xsl:when test="position()=4 or position()=6 or position()=8">
	                  <td align="right">
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </td>
	              </xsl:when>
                <xsl:otherwise>
                  <td>
                    <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
  </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>