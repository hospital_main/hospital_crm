<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>文档类别</th>
				<th>文档名称</th>
				<th>科室</th>
				<th>责任人</th>
				<th>存放位置</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  noWrap='true'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' >
	                  <a tabindex='-1' href='#'>
		                <xsl:attribute name="onclick" >
		                  view('equipInDocUpdate.html?load=&lt;id&gt;<xsl:value-of select="../pk/id"/>&lt;/id&gt;');
		                </xsl:attribute><xsl:value-of select="."/></a>
			            </td>
                </xsl:when>
                <xsl:when test="position()=2">
			            <td  noWrap='true' >
		              <xsl:choose>
		                <xsl:when test="../td[position() = 6] != ''">
			                  <a tabindex='-1'>
				                <xsl:attribute name="href" >
				                  /upload/<xsl:value-of select="../td[position()=6]"/>
				                </xsl:attribute>
				                <xsl:value-of select="."/></a>
			                </xsl:when>
						  			  <xsl:otherwise>
												<xsl:value-of select="."/>
			                </xsl:otherwise>
			              </xsl:choose>
			            </td>
                </xsl:when>
                <xsl:when test="position()=6">
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
