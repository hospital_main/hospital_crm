<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th width='40'>选择</th>
  			<th>设备编码</th>
  			<th>设备名称</th>
  			<th>型号规格</th>
  			<th>产地厂家</th>
  			<th>单位</th>
  			<th>数量</th>
  			<th>单价</th>
  			<th>交货日期</th>
  		</tr>
  	</thead>
  	<tbody>
  	<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
		   		       		<td align='center'>
			    	  	  			<input type='radio' name='select' onclick='choose(this)'>
						              <xsl:attribute name="value" >
				                 	 <xsl:value-of select="../pk/id"/>
						      			  </xsl:attribute>
			       						</input>
				   					</td>
					   				<td><xsl:value-of select="."/></td>
              	</xsl:when>
                <xsl:otherwise>
					   				<td><xsl:value-of select="."/></td>
              	</xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
  		</tr>
   	</xsl:for-each>
 	</tbody>
	</xsl:template>
</xsl:stylesheet>
