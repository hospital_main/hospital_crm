<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>验收项目</th>
				<th>规定技术指标</th>
				<th>允许误差范围</th>
				<th>项目验收结果</th>
				<th>是否正常</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          
          <xsl:for-each select="td">
          <xsl:choose>
          	<xsl:when test="position()=1">
          		<td style="display:none">
                <xsl:value-of select="."/>
          		</td>
          	</xsl:when>
          	<xsl:when test="position()=2">
          		<td  noWrap='true' >
                <a tabindex='-1' href="#">
                <xsl:attribute name="onclick" >
                  viewDetail('update2.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
                </xsl:attribute><xsl:value-of select="."/></a>
          		</td>
          	</xsl:when>
	            <xsl:otherwise>
		  						<td>
		  							<xsl:value-of select="."/>
		  						</td>
		  					</xsl:otherwise>
  			  </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
