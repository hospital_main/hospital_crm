<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
  			<th style="display:none"><input type="checkbox"/></th>
  			<th>设备编码</th>
  			<th>设备名称</th>
  			<th>型号规格</th>
  			<th>产地厂家</th>
  			<th>单位</th>
  			<th>单价</th>
  			<th>验收数量</th>
  			<th>缺损程度</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
  			
	  			<tr>
		  			<td align="center" style="display:none">
		  				<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
		  					<xsl:attribute name="value">
		  						<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		  					</xsl:attribute>
		  				</input>
		  			</td>
		  			<xsl:for-each select="td">
		  				<xsl:choose>
		  					<xsl:when test="position()=1">
			            <td  noWrap='true' >
	                  <a tabindex='-1' href="#">
		                <xsl:attribute name="onclick" >
		                  viewDetail('update1.html?load=&lt;equi_code&gt;<xsl:value-of select="../pk/equi_code"/>&lt;/equi_code&gt;&lt;accept_equi_id&gt;<xsl:value-of select="../pk/accept_equi_id"/>&lt;/accept_equi_id&gt;');
		                </xsl:attribute><xsl:value-of select="."/></a>
			            </td>
                </xsl:when>
                <xsl:when test="position()=9">
                	<td style='display:none'>
                		<xsl:value-of select="."/>
                	</td>
                </xsl:when>
		  					<xsl:otherwise>
		  						<td>
		  							<xsl:value-of select="."/>
		  						</td>
		  					</xsl:otherwise>
		  				</xsl:choose>
		  			</xsl:for-each>
	  			</tr>
  			
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>