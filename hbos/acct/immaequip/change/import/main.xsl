<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
  			<xsl:for-each select="/root/t2head/tr[1]/td">
  				<th ><xsl:value-of select="."/></th>
  			</xsl:for-each>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
  			
	  			<tr>
		  			<xsl:for-each select="td">
		  				<xsl:variable name="c" select="position()"/>
		  				<xsl:if test="substring( /root/t2head/tr[1]/td[$c] ,string-length( /root/t2head/tr[1]/td[$c] ),1)=' '">
							<td align="right">
								<xsl:value-of select="format-number(.,'##,##0.00')"/>
							</td>
						</xsl:if>
						<xsl:if test="substring( /root/t2head/tr[1]/td[$c] ,string-length( /root/t2head/tr[1]/td[$c] ),1)!=' '">
							<td>
								<xsl:value-of select="."/>
							</td>
						</xsl:if>
		  			</xsl:for-each>
	  			</tr>
  			
  		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>