<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>				
				<th width="25">
					<input type="checkbox" onclick="checkInit(this)"/>
				</th>
				<th nowrap='true'>资产编号</th>
				<th nowrap='true'>资产名称</th>
				<th nowrap='true'>原值</th>
				<th nowrap='true'>累计折旧</th>
				<th nowrap='true'>净值</th>
				<th nowrap='true'>变动日期</th>				
				<th nowrap='true'>减少方式</th>
				<th nowrap='true'>清理费用</th>			
				<th nowrap='true'>清理收入</th>
				<th nowrap='true'>备注</th>
				<th nowrap='true'>凭证号</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
				<tr>														
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td noWrap="true">										
									<xsl:if test="../td[14]='0'">
	                  	<xsl:value-of select="."/>
			            </xsl:if>
			        		<xsl:if test="../td[14]='1'">
			        				<input type="checkbox" TABINDEX='-1' style='font-size:8px;'>
             					<xsl:attribute name="value">
			            				<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
			        				</xsl:attribute>												
											</input>									
									</xsl:if>																	
								</td>
							</xsl:when>							
							<xsl:when test="position()=2">								
								<td noWrap="true">
									<xsl:if test="../td[14]='0'">
	                  	<xsl:value-of select="."/>
			            	</xsl:if>
									<xsl:if test="../td[14]='1'">
										<a tabindex="-1">
											<xsl:attribute name="href">    	            			
    	            				javascript:showWin('update_detail.html?load=&lt;equi_arch_no&gt;<xsl:value-of select="../td[2]"/>&lt;/equi_arch_no&gt;&lt;tableid&gt;<xsl:value-of select="../td[13]"/>&lt;/tableid&gt;');
  	          				</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="position()=4 or position()=5 or position()=6 or position()=9 or position()=10">
								<td noWrap="true" align='right'>									
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:when test="position()=12">
								<td noWrap="true">									
									<xsl:value-of select="."/>									
								</td>
							</xsl:when>
							<xsl:when test="position()=13">
								<td noWrap="true" style="display:none">									
									<xsl:value-of select="."/>									
								</td>
							</xsl:when>
							<xsl:when test="position()=14">
								<td noWrap="true" style="display:none">									
									<xsl:value-of select="."/>									
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap="true">
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>