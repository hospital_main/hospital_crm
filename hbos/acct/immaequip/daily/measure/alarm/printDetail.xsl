<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
    <colgroup>
      <col style = 'width:205'/>
      <col style = 'width:105'/>
      <col style = 'width:105'/>
      <col style = 'width:105'/>
    </colgroup>
    <thead> 		
    </thead>
  	<tbody>
  	  <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td >计划编号</td><td><xsl:value-of select="td[1]"/></td>
          <td >计划名称</td><td><xsl:value-of select="td[2]"/></td>
  			</tr>
  			<tr>
          <td >设备号</td><td><xsl:value-of select="td[3]"/></td>
          <td >设备序列号</td><td><xsl:value-of select="td[4]"/></td>
  			</tr>
  			<tr>
          <td >设备名称</td><td><xsl:value-of select="td[5]"/></td>
          <td >型号</td><td><xsl:value-of select="td[6]"/></td>
  			</tr>
  			<tr>
          <td >单位</td><td><xsl:value-of select="td[7]"/></td>
          <td >厂商</td><td><xsl:value-of select="td[8]"/></td>
  			</tr>
  			<tr>
          <td >使用科室</td><td><xsl:value-of select="td[9]"/></td>
          <td >申请日期</td><td><xsl:value-of select="td[10]"/></td>
  			</tr>
  			<tr>
          <td >申请科室</td><td><xsl:value-of select="td[11]"/></td>
          <td >申请人</td><td><xsl:value-of select="td[12]"/></td>
  			</tr>
  			<tr>
          <td >计量周期</td><td><xsl:value-of select="td[13]"/></td>
          <td >计量类别</td><td><xsl:value-of select="td[14]"/></td>
  			</tr>
  			<tr>
          <td >计量单位</td><td><xsl:value-of select="td[15]"/></td>
          <td >计划计量日期</td><td><xsl:value-of select="td[16]"/></td>
  			</tr>
  			<tr>
          <td >计量说明</td><td colspan="3"><xsl:value-of select="td[17]"/></td>
          <td style="display:none"></td><td style="display:none"></td>
  			</tr>
			</xsl:for-each>
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



