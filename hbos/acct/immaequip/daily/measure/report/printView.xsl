<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
				<td>计量记录号</td> 
				<td>资产卡号</td>               
				<td>资产名称</td>             
				<td>规格型号</td>             
				<td>生产厂家</td>
				<td>资产序列号</td>                 
				<td>使用部门</td>         
				<td>计量周期</td>             
				<td>计划计量日期</td>         
				<td>实际计量日期</td>         
				<td>计量费用</td>             
				<td>附带文档</td>           
				<td>状态</td>                 
				<td>供应商</td>               
				<td>计量单位</td>             
				<td>计量证书号</td>       
	  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	            <td >
	              <xsl:choose>
	                <xsl:when test="position()=1">
	                  <xsl:value-of select="."/>
	                </xsl:when>
	                <xsl:when test="position()=11">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </xsl:when>
	                <xsl:when test="position()=12">
										管理
	                </xsl:when>
	                <xsl:otherwise>
	                  <xsl:value-of select="."/>
	                </xsl:otherwise>
	              </xsl:choose>
	            </td>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>