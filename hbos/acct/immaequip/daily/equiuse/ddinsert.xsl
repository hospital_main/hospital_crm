<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
		<th><input type="checkbox" onclick="setAll(this)"/></th>
		<th nowrap="true">资产卡片号</th>
		<th nowrap="true">资产名称</th>
		<th nowrap="true">资产编码</th>
		<th nowrap="true">规格</th>
		<th nowrap="true">型号</th>
		<th nowrap="true">生产厂家</th>
		<th nowrap="true">类型</th>
		<th nowrap="true">原值</th>
		<th nowrap="true">购置日期</th>
		
		 
    	</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' />
          </td>
          <xsl:for-each select="td">
		         
            		 <xsl:choose>
            		<xsl:when test="position()=8 ">
	                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	              </xsl:when>  
	              <xsl:otherwise>
		  						<td>
		  							<xsl:value-of select="."/>
		  						</td>
		  					</xsl:otherwise>
		       </xsl:choose>
  	      </xsl:for-each>
  	</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
