function submitData(obj){
	var res=checkInputs();
	if(res==null||res=="")
		return ;
	window.xmlhttp.post(obj.name, res,"")
	var str = window.xmlhttp._object.responseText
	if (!window.doMsg(str,"")) {
	  return false;
	}
	//sysDictsUnitinfoCopyParas_select.click();
}
function checkInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	var res="";
	for(var i=0;i<trs.length;i++){
		if(trs[i].getAttribute("_editParaType")==null)
			continue;
			
  		 if(trs[i]._editDataValue==trs[i]._editInputValue)
  			 continue;
 
    		res+="<record>";
   		  if(checkValue(trs[i],trs[i]._editDataType,trs[i]._editInputValue)){
   		    var temp = trs[i]._editInputValue;
  			  res+=trs[i]._editPk+"<value>"+temp+"</value>";
    		}else{
    			return "";
    			}
		res+="</record>";
	}
	return res;
}
function checkValue(tr,type,value){
	var msg=tr.cells[0].innerText+" "+tr.cells[1].innerText+" :要求是" ;
	var res=true;
	if(type=="4"){
		msg+=" 文本 ";
		res=true;
	}else if(type=="20"&&IsDate(value)){
		msg+=" 日期 ";
		var d= CDate(value);
		var year=d.getFullYear();
		var month=d.getMonth()+1;
		var day=d.getDate();
		year='0000'+year;year=year.substring(year.length-4)
	  	month='00'+month;month=month.substring(month.length-2)
	  	day='00'+day; day=day.substring(day.length-2)
	 	tr._editInputValue=year+'-'+month+'-'+day;
		res=true;
	}else  if(type=="30"){
		msg+=" 编码规则 ";
		res=isRuleCode(value);
	}else if(type=="1"&&value!=""){
		msg+=" 正整数 ";
		var v=parseInt(value,10);
		var t=''+v;
		var r=''+value;
		if(isNaN(v)||v<0||t.length!=r.length)
			res=false;
	}else if(type=="5"){
		var v=parseInt(value,10);
		if(isNaN(v))
			res=false;
	}
	if(res==false){
		alert(msg);
		return;
	}
	return res;
}
function initInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	for(var i=0;i<trs.length;i++)
		initTrInputs(trs[i]);
}
function initTrInputs(tr){
	var pt=tr.getAttribute("_editParaType");
	var dt=tr.getAttribute("_editDataType");
	var dv=tr.getAttribute("_editDataValue");
	var pc=tr.getAttribute("_paraCode");
	var pv=tr.getAttribute("_paraValue");
	var cc=tr.getAttribute("_canChange")

	if(pt==null||dt==null)
		return ;
	var inp="";
	var id=getJsGuid();
	var tag="";
	var chk="";
	tr._editInputId=id;
	
	
	
	if(cc=='1'){
		inp="<input type='text' id='"+id+"' name='"+id+"' value='"+dv+"'/>";
		tag="input";
	}else {

	var ops=tr.cells[3].innerText.split("；");
		  inp="<select id='"+id+"' name='"+id+"'>";
	  
		tag="select"
		for(var i=0;i<ops.length;i++){
		  var temp = ops[i].split("：");
			if(dv==temp[0])
				chk=" selected ";
			else
				chk="";

   		inp+="<option "+chk+" value='"+temp[0]+"'>"+ops[i]+"</option>";
		}
		inp+="</select>";
	}
	tr.cells[2].innerHTML=inp;
	
	var inpus=tr.cells[2].getElementsByTagName("input");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	inpus=tr.cells[2].getElementsByTagName("select");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}

}
function setInputChange(tr,inp){
	inp.onblur=
	inp.onclick=
  inp.onkeyup=function(){if(tr._canChange!='0'){tr._editInputValue=this.value}else{this.value=tr._editInputValue}};
}