<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	
  		<tr noWrap="true" class="mainHead">
    			<td noWrap="true" style="fontsize:maintitle;colspan:19;align:center;border:none"/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			
    	</tr>
  		<tr noWrap='true' class='mainHead'>
  		  <td style='border:1'>资产编号</td>
				<td style='border:1'>资产名称</td>
				<td style='border:1'>规格</td>
				<td style='border:1'>型号</td>
				<td style='border:1'>品牌</td>
				<td style='border:1'>单价</td>
				<td style='border:1'>数量</td>
				<td style='border:1'>入库单号</td>
				<td style='border:1'>入库日期</td>
				<td style='border:1'>库房</td>	
				<td style='border:1'>资产类别</td>
				<td style='border:1'>资产性质</td>			
				<td style='border:1'>往来单位</td>
				<td style='border:1'>入库类型</td>
				<td style='border:1'>制单人</td>
				<td style='border:1'>金额</td>
				<td style='border:1'>领用科室</td>
				<td style='border:1'>发票号</td>
				<td style='border:1'>备注</td>
      </tr>
     </thead>
  	<tbody>
  	 <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>
        	
        	<xsl:for-each select="td">
           	<xsl:choose>
	          	<xsl:when test="position()=6">
	              <td align='right'>
        		      <xsl:if test=".!='0' and .!='0.00' and .!='0.0000' and .!=''">
              			<xsl:attribute name="class">numberText</xsl:attribute>
               			<xsl:value-of select="format-number(.,'#,##0.00')"/>
             			</xsl:if> 
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=7">
	              <td align='right'>
        		      <xsl:if test=".!='0' and .!='0.00' and .!='0.0000' and .!=''">
              			<xsl:attribute name="class">numberText</xsl:attribute>
               			<xsl:value-of select="format-number(.,'#,##0.00')"/>
             			</xsl:if> 
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=16">
	              <td align='right'>
        		      <xsl:if test=".!='0' and .!='0.00' and .!='0.0000' and .!=''">
              			<xsl:attribute name="class">numberText</xsl:attribute>
               			<xsl:value-of select="format-number(.,'#,##0.00')"/>
             			</xsl:if> 
	              </td>
	            </xsl:when>
	           
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			
      </xsl:for-each>
    </tbody>
    <tfoot>
    
 		    <tr noWrap='true'>
		        <td style='fontsize:foot;colspan:colcount;align:left;border:none'></td>
		  			<xsl:call-template name="repeat">
		  				<xsl:with-param name="times" select="$colNum"/>    
		        </xsl:call-template>
  	  	</tr>
    		
    	</tfoot>
   </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>