<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th>序号</th>
        <th>供应商编号</th>
				<th>供应商名称</th>
				<th>金额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>
        <xsl:attribute name="vCode"><xsl:value-of select="td[1]"/></xsl:attribute>
					<!--xsl:if test="td[2]=''"><xsl:attribute name="style">display:none</xsl:attribute></xsl:if-->
					<xsl:for-each select="td[position()&lt;12]">
        		<xsl:choose>
           	  <xsl:when test="position()=2">
								<td  >
								<a href="#">
								<xsl:attribute name="onclick" >
								   javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:800px;dialogHeight:700px;overflow:auto;', '','true')
								</xsl:attribute>
								<xsl:value-of select="."/>
								</a>　
								</td>
							</xsl:when>
	          	<xsl:when test="position()=4">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
	            </xsl:when>
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


