<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<xsl:for-each select="/root/t2head/tr[1]/td[position() != 1]">
					<xsl:variable name="c" select="position()"/>
					<th nowrap="true">
						<xsl:value-of select="."/>
						<xsl:if test="/root/t2head/tr[6]/td[$c + 1] != '0'">
							<xsl:if test="/root/t2head/tr[6]/td[$c + 1] = '1'">
								<img src="up.gif" border="0" style="cursor:hand">
									<xsl:attribute name="onclick">
										doOrder("<xsl:value-of select="/root/t2head/tr[5]/td[$c + 1]"/> DESC");
									</xsl:attribute>
								</img>
							</xsl:if>
							<xsl:if test="/root/t2head/tr[6]/td[$c + 1] = '2'">
								<img src="down.gif" border="0" style="cursor:hand">
									<xsl:attribute name="onclick">
										doOrder("<xsl:value-of select="/root/t2head/tr[5]/td[$c + 1]"/> ASC");
									</xsl:attribute>
								</img>
							</xsl:if>
						</xsl:if>
						<xsl:if test="/root/t2head/tr[6]/td[$c + 1] = '0'">
							<img src="up2.gif" border="0" style="cursor:hand">
								<xsl:attribute name="onclick">
									doOrder("<xsl:value-of select="/root/t2head/tr[5]/td[$c + 1]"/> ASC");
								</xsl:attribute>
							</img>
						</xsl:if>
					</th>
				</xsl:for-each>
			</tr>
		</thead>
		
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:variable name="c" select="position()"/>

						<xsl:if test="/root/t2head/tr[2]/td[$c +1 ] != '2'">
							<td align="left">
								<xsl:if test="/root/t2head/tr[3]/td[$c +1 ]='0'">
									<xsl:value-of select="."/>
								</xsl:if>
								<xsl:if test="/root/t2head/tr[3]/td[$c +1 ]='1'">
									<xsl:if test=". != '合　　计'">
										<a href="#">
											<xsl:attribute name="onclick" >javascript:doOpenLink("<xsl:value-of select="."/>");
										</xsl:attribute><xsl:value-of select="."/></a>
									</xsl:if>
									<xsl:if test=". = '合　　计'">
										<xsl:value-of select="."/>
									</xsl:if>
								</xsl:if>
							</td>
						</xsl:if>
						<xsl:if test="/root/t2head/tr[2]/td[$c +1 ]='2'">
							<td align="right">
								<xsl:if test="/root/t2head/tr[3]/td[$c +1 ]='0'">
									<xsl:value-of select="format-number(.,'##,##0.00')"/>
								</xsl:if>
								<xsl:if test="/root/t2head/tr[3]/td[$c +1 ]='1'">
									<xsl:if test=". != '合　　计'">
										<a href="#">
											<xsl:attribute name="onclick" >javascript:doOpenLink("<xsl:value-of select="."/>");
										</xsl:attribute><xsl:value-of select="format-number(.,'##,##0.00')"/></a>
									</xsl:if>
									<xsl:if test=". = '合　　计'">
										<xsl:value-of select="."/>
									</xsl:if>
								</xsl:if>
							</td>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>