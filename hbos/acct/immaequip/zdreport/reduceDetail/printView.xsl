 <?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>  
	    	<tr noWrap='true'> 
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>  
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
    			<td noWrap="true" style="fontsize:subtitle;colspan:8;align:left;border:none"/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
    			 
    	</tr>
	  		<tr noWrap="true" class="mainHead">
			  <td>使用部门</td>
				<td>资产常用名称</td>
				<td>资产财务分类</td>
				<td>条形码</td>
				<td>设备卡号</td>
				<td>金额</td>
				<td>购置日期</td>
				<td>注销日期</td>
				<td>注销人</td>
				<td>经费来源</td>
				<td>型号</td>
				<td>规格</td>
	  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	            <td  noWrap='true' >
	              <xsl:choose>
	                <xsl:when test="position()=1">
			              <xsl:value-of select="."/>
	                </xsl:when>
	                <xsl:otherwise>
	                  <xsl:value-of select="."/>
	                </xsl:otherwise>
	              </xsl:choose>
	            </td>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
	  	<tfoot>
    
 		    <tr noWrap='true'>
		        <td style='fontsize:foot;colspan:colcount;align:left;border:none'></td>
		  			<xsl:call-template name="repeat">
		  				<xsl:with-param name="times" select="$colNum"/>    
		        </xsl:call-template>
  	  	</tr>
    		
    	</tfoot>
   </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>