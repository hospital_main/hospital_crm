<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>  
			<tr noWrap="true" class="mainHead">
				<th rowspan="3">类别</th> 
				<th colspan="2">期初余额</th>
				<th colspan="5">本期增加</th>
				<th colspan="5">本期减少</th>
				<th colspan="2">期末余额</th>
				<!--th rowspan="2">累计折旧</th-->
			</tr>
			<tr noWrap="true" class="mainHead">
				<th rowspan="2">台数</th>
				<th rowspan="2">金额</th>
				<th rowspan="2">采购增加</th>
				<th rowspan="2">无偿增加</th>
				<th rowspan="2">其他增加</th>
				<th colspan="2">合计</th>
				<th rowspan="2">报废减少</th>
				<th rowspan="2">无偿减少</th>
				<th rowspan="2">其他减少</th>
				<th colspan="2">合计</th>
				<th rowspan="2">台数</th>
				<th rowspan="2">金额</th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th>台数</th>
				<th>金额</th>
				<th>台数</th>
				<th>金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<!--td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td-->
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
								 <td align='left'>	
									<a tabindex="-1">
									 
										<xsl:value-of select="."/>
									 
									</a>
								 </td>
								</xsl:when>
								<xsl:when test="position()=2">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=3">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=4">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=5">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=6">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=7">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=8">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=9">
								 <td align='right'  >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=10">
								 <td align='right' >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=11">
								 <td align='right' >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=12">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=13">
								 <td align='right' >	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when>
								<xsl:when test="position()=14">
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								<xsl:otherwise>
								 <td align='right'>	
									<xsl:value-of select="."/>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
