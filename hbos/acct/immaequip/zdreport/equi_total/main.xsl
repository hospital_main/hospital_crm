<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th rowspan="2" valign="middle">期间</th>
        <th rowspan="2" valign="middle" >摘要</th>
		    <th colspan="3">原值</th>
		    <th colspan="3">累计摊销</th>
		    <th rowspan="2" valign="middle" >净值</th>
  		</tr>
  		<tr noWrap="true" class="mainHead"> 
		    <th valign="middle">借方</th>
		    <th valign="middle">贷方</th>
		    <th valign="middle">余额</th>	
		    <th valign="middle">借方</th>
		    <th valign="middle">贷方</th>
		    <th valign="middle">余额</th>		    		    
		  </tr>  		
  	</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      <xsl:variable name='CurTrPos' select='position()'/>
        <tr>          
          <xsl:for-each select="td">
          	<xsl:choose>							
              <xsl:when test="position()=3 or position()=4 or position()=5 or position()=6 or position()=7 or position()=8 or position()=9">
                <td align='right'>
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              
              <xsl:otherwise>
                <td align='left'><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>