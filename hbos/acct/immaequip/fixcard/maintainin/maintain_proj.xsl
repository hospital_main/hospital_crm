<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
  		<xsl:variable name="self" select="/root/tbody/tr[1]/td[7]"/>
			<tr noWrap='true' class='mainHead'>
			  <th noWrap='true'>选择</th>
				<th nowrap='true'>保养项目</th>
				<th nowrap='true'>是否执行</th>
				<th nowrap='true'>是否正常</th>
				<th nowrap='true'>保养说明</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:attribute name="_editPk"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
				<xsl:attribute name="_change">0</xsl:attribute>
				<xsl:attribute name="_is_exec"><xsl:value-of select="td[3]"/></xsl:attribute>
				<xsl:attribute name="_is_well"><xsl:value-of select="td[4]"/></xsl:attribute>
				<xsl:attribute name="_desc"><xsl:value-of select="td[5]"/></xsl:attribute>
				<xsl:if test="td[6] = '0'">
  				<td>
    					<input type='checkbox' TABINDEX='-1' style="align:center">
    						<xsl:attribute name="value" >
    							<xsl:value-of select="td[1]"/>
    						</xsl:attribute>
    					</input>
          </td>
        </xsl:if>
				<xsl:if test="td[6] = '1'">
  				<td>
    					<input type='checkbox' TABINDEX='-1' style="align:center" checked='true'>
    						<xsl:attribute name="value" >
    							<xsl:value-of select="td[1]"/>
    						</xsl:attribute>
    					</input>
          </td>
        </xsl:if>
				<xsl:for-each select="td[position() &gt; 1]"> 
				  <xsl:if test="position() &lt; 5">  
						<td>
			  			<xsl:value-of select="."/>  
						</td>
					</xsl:if>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>


