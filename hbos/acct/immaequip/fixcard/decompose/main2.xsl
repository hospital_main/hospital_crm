<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<tr noWrap='true' class='mainHead'>
			<xsl:for-each select="/root/tbody/tr[position()=1]">
			  <xsl:for-each select="td[position()!=1]">
			    <th nowrap='true'><xsl:value-of select="."/></th>
			  </xsl:for-each>
			</xsl:for-each>
		</tr>     
	</thead>

	<tbody>
		<xsl:for-each select="/root/tbody/tr[position()!=1]">
			<tr>
  				<td align='center'  style='display:none'>
  					<input type='checkbox' TABINDEX='-1'>
  						<xsl:attribute name="value" >
  							<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
  						</xsl:attribute>
  						<xsl:attribute name="style" >font-size:8px;</xsl:attribute>
  					</input>
  				</td>
				<xsl:for-each select="td[position()!=1]">   
          <xsl:choose>
            <xsl:when test="position()=1">
              <td>
                <a tabindex='-1'><xsl:value-of select="."/></a>
              </td>
            </xsl:when>
            <xsl:otherwise>  
     					<td>
    	  	  		<xsl:value-of select="."/>
           		</td>
           	</xsl:otherwise>
          </xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

