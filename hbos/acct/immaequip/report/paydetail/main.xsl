<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th>资产卡片号</th>
        <th>资产编号</th>
				<th>资产名称</th>
				<th>资产属性</th>				
				<th>型号规格</th>
				<th>产地厂家</th>
				<th>购置日期</th>
				<th>所在部门</th>
				<th>资产状态</th>
				<th>经费来源</th>
				<th>部门类别</th>
				<th>原值</th>
				<th>单价</th>
				<th>数量</th>
				<th>金额</th>
				<th>付款日期</th>
				<th>归属</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>
        	
        	<xsl:for-each select="td">
           	<xsl:choose>
	          	<xsl:when test="position()=12">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=13">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=14">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=15">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


