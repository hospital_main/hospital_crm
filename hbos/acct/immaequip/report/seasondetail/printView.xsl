<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:13'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>
	  		<tr noWrap='true'>
	        <td style='fontsize:subtitle;colspan:13'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>
				<tr noWrap='true' class='mainHead'>
	        <td rowspan="2" valign="middle" style="fontsize:coltitle">卡片号</td>
	        <td rowspan="2" valign="middle" style="fontsize:coltitle">仪器名称</td>
	        <td rowspan="2" valign="middle" style="fontsize:coltitle">规格</td>
					<td rowspan="2" valign="middle" style="fontsize:coltitle">型号</td>
					<td rowspan="2" valign="middle" style="fontsize:coltitle">厂家名称</td>				
					<td rowspan="2" valign="middle" style="fontsize:coltitle">国别</td>
					<td rowspan="2" valign="middle" style="fontsize:coltitle">合同编号</td>
					<td rowspan="2" valign="middle" style="fontsize:coltitle">数量</td>
					<td colspan="2" valign="middle" style="fontsize:coltitle">价格</td>
					<td style="display:none"></td>
					<td colspan="2" valign="middle" style="fontsize:coltitle">经费来源</td>
					<td style="display:none"></td>
					<td rowspan="2" valign="middle" style="fontsize:coltitle">购置日期</td>
					<td rowspan="2" valign="middle" style="fontsize:coltitle">科室名称</td>
	      </tr>
	  		<tr noWrap="true" class="mainHead">
	  		  <td style="display:none"></td>
	  		  <td style="display:none"></td>
	  		  <td style="display:none"></td>
	  		  <td style="display:none"></td>
	  		  <td style="display:none"></td>
	  		  <td style="display:none"></td>
	  		  <td style="display:none"></td>
	  		  <td style="display:none"></td>
	  			<td valign="middle" style="fontsize:coltitle">外币金额</td>
	  			<td valign="middle" style="fontsize:coltitle">原值</td>
	  			<td valign="middle" style="fontsize:coltitle">外币来源</td>
	  			<td valign="middle" style="fontsize:coltitle">经费来源</td>
	  			<td style="display:none"></td>
	  		  <td style="display:none"></td>
	  		</tr>
	    </thead>
	    <tbody>
	      <xsl:for-each select="/root/tbody/tr">
	        <tr>
	        	<xsl:for-each select="td">
	           	<xsl:choose>
		            <xsl:when test="position()=8">
		              <td align='right'>
		              	<xsl:value-of select="format-number(.,'#,##0')"/>
		              </td>
		            </xsl:when>
		            <xsl:when test="position()=9 or position()=10 ">
		              <td align='right'>
		              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
		              </td>
		            </xsl:when>
		            <xsl:otherwise>
			            <td align="left">
			              <xsl:value-of select="."/>
			            </td>
		            </xsl:otherwise>
	            </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	      </xsl:for-each>
	    </tbody>
	 		<tfoot>
		    <tr noWrap='true'>
	        <td style='fontsize:foot;colspan:13;align:left;border:none'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
		  	</tr>
	  	</tfoot>
		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>