<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
          <td>付款单号</td>
					<td>付款日期</td>
					<td>供应商</td>
					<td>支付方式</td>
					<td>发票号码</td>					
					<td>合同编码</td>					
					<td>付款金额</td>
					<td>付款类型</td>
					<td>制单人</td>
					<td>审核人</td>
					<td>状态</td>
	  		</tr>
	  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
              </xsl:when>  
              <xsl:when test="position()=6">
								<td><a tabindex="-2">
											<xsl:attribute name="href">
		                  javascript:openDialog('../../out/contract/update.html?load=&lt;lend_no&gt;<xsl:value-of select="../td[position()=6]"/>&lt;/lend_no&gt;', 'dialogWidth:800px;dialogHeight:600px', result)
		                	</xsl:attribute><xsl:value-of select="."/>
		                </a>
		            </td>
              </xsl:when>  
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>