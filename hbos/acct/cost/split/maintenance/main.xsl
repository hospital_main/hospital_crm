<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th width='15' style='display:none'><input type='checkbox'/></th>
				<th width='100'>财务会计凭证号</th>
				<th width='100'>预算会计凭证号</th>
				<th>日期</th>
				<th>附件</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>记账人</th>
			</tr>                   
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
							<xsl:if test="td[9]='1'">
								<xsl:attribute name="disabled">true</xsl:attribute>
							</xsl:if>
						</input>
					</td>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
									<td>
										<a href="#">
											<xsl:attribute name="onclick">openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;")</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=4">
									<td align="right">
										<xsl:value-of select="."/>
									</td>
								</xsl:when>						
								<xsl:when test="position()>=8">
								</xsl:when>
								<xsl:otherwise>
									<td >
									<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



