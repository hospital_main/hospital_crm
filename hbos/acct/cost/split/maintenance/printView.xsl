<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:200mm'/>
				<col style = 'width:200mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
			</colgroup>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td width="200" >财务会计凭证号</td>
					<td width="200">预算会计凭证号</td>
					<td>日期</td>
					<td>附件</td>
					<td>制单人</td>
					<td>审核人</td>
					<td>记账人</td>
				</tr>
			</thead>
			<tbody> 
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position>=8">
								</xsl:when>
								<xsl:when test="position() = 4">
									<td>
										<xsl:attribute name="align">right</xsl:attribute>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
