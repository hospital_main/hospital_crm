<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none' width='25'><input type='checkbox'/></th>
				<th nowrap='true'>成本单号</th>
				<th nowrap='true'>支付日期</th>
				<th nowrap='true'>摘要</th>
				<th nowrap='true'>成本项目名称</th>
				<th nowrap='true'>金额</th>
				<th nowrap='true'>财务会计凭证类型</th>
				<th nowrap='true'>预算会计凭证类型</th>
				<th nowrap='true'>借方财务会计科目（医疗）</th>
				<th nowrap='true'>借方预算会计科目</th>
        <th nowrap='true'>借方财务会计科目（管理）</th>
				<th nowrap='true'>借方预算会计科目</th>
				<th nowrap='true'>贷方财务会计科目</th>
				<th nowrap='true'>贷方预算会计科目</th>
				<th nowrap='true'>财务会计凭证</th>
				<th nowrap='true'>预算会计凭证</th>
				<th nowrap='true'>拆分状态</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center' style='display:none'>
						<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
									<a tabindex='-1'><xsl:value-of select="."/></a>
								</td>
							</xsl:when>
							<xsl:when test="position()=5">
								<td align="right">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
              <xsl:when test="position()=14 or position()=15">
                  <td>
                      <a href="javascript:void(0);">
                          <xsl:attribute name="onclick">
                              javascript:openVouchDlg('<xsl:for-each select="../td[17]">&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;</xsl:for-each>');
                          </xsl:attribute>
                          <xsl:value-of select="."/>
                      </a>
                  </td>
              </xsl:when>
							<xsl:when test="position()=17">
							</xsl:when>
							<xsl:otherwise>
								<td align='left'>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>