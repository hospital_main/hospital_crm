<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th>月份</th>
		  	<th>摘要</th>
		  	<th>计划金额</th>
		  	<th>报账金额</th>
		  	<th>余额</th>
      </tr>
    </thead>
     <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
				<xsl:when test="position() = 3 or position() = 4 or position() = 5">
			          <td align="right">
		                <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </td>
                </xsl:when>
                <xsl:otherwise>
                	<td>
                  <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
