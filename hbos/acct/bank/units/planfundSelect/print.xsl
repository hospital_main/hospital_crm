<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				
			</colgroup>
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<td style='colspan:6;fontsize:maintitle'>用款计划查询</td>
		    <td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
  			<td style='colspan:6;fontsize:subtitle'></td>
		    <td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
		    <td>月份</td>
		  	<td>摘要</td>
		  	<td>计划金额</td>
		  	<td>报账金额</td>
		  	<td>余额</td>
  	  
  		</tr>
  		
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                <td><xsl:value-of select="."/></td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>

