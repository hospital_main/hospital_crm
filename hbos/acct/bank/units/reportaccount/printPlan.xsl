<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <colgroup>		       
        <col style = 'width:100'/>
        <col style = 'width:100'/>
        <col style = 'width:150'/>
        <col style = 'width:100'/>
        
      </colgroup>
      <thead>
        <tr noWrap='true' class='mainHead'>  	  
          <th noWrap='true' >摘要</th>
				<th noWrap='true' >支出科目</th>
				<th noWrap='true' >项目名称</th>
				<th noWrap='true' >报账金额</th>
				</tr> 		      
      </thead>	      
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
	  		<tr>
        	<xsl:for-each select="td">          
	      		<xsl:choose>            
							
							<xsl:when test="position()= 4">
		  					<td align="left" noWrap='true' >
		  						<xsl:attribute name="class">numberText</xsl:attribute>
		    					<xsl:value-of select="format-number(.,'#,##0.00')"/>
		  					</td>
							</xsl:when>  
							<xsl:otherwise>
		  					<td align="left" noWrap='true' >
		    					<xsl:value-of select="."/>
		  					</td>
							</xsl:otherwise>
	      		</xsl:choose>
          </xsl:for-each>
	  		</tr>
        </xsl:for-each>
      </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>