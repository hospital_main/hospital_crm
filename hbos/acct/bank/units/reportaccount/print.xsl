<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				<col style = 'width:110'/>	
				
			</colgroup>
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<td style='colspan:5;fontsize:maintitle'>报账单</td>
		    <td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
  			<td style='colspan:5;fontsize:subtitle'></td>
		    <td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
		    <td>单据编号</td>
		  	<td>制单日期</td>
		  	<td>制单人</td>
		  	<td>单位审核人</td>
		  	<td>中心审核人</td>
  	  
  		</tr>
  		
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
                <td><xsl:value-of select="."/></td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</root>
	</xsl:template>
</xsl:stylesheet>

