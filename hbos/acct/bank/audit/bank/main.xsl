<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
	      <th style='display:none'><input type='checkbox'/></th>
		  	<th nowrap='true'>日期</th>
		  	<th nowrap='true'>摘要</th>
		  	<th nowrap='true'>结算类型</th>
		  	<th nowrap='true'>票据号</th>
		  	<th nowrap='true'>对方科目</th>
		  	<th nowrap='true'>收入金额</th>
		  	<th nowrap='true'>支出金额</th>
		  	<th nowrap='true'>制单人</th>
		  	<th nowrap='true'>审核人</th>
		  	<th nowrap='true'>凭证号</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            	<xsl:if test="td[13] = 1">
              	<xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
              <xsl:attribute name="value">
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=10">
	                <td><xsl:if test="text() != ''">
		                <a tabindex='-1'>
		                  <xsl:attribute name="href" >
		    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../td[12]"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+readOnly,onlyOne&lt;/edit_mask&gt;');
		  	          		</xsl:attribute><xsl:value-of select="."/>_<xsl:value-of select="../td[11]"/>
		  	          	</a>
	  	          	</xsl:if></td>
                </xsl:when>
                <xsl:when test="position()=11 or position()=12 or position() = 13">
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>