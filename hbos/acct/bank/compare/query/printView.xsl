<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/bank/compare/query/printView.xsl,v 1.2 2012/06/29 07:05:34 linaikun Exp $
 $Author: linaikun $
 $Date: 2012/06/29 07:05:34 $
 $Revision: 1.2 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:100mm'/>
				<col style = 'width:80mm'/>
				<col style = 'width:80mm'/>
				<col style = 'width:80mm'/>
				<col style = 'width:80mm'/>
				<col style = 'width:80mm'/>
				<col style = 'width:80mm'/>
				<col style = 'width:80mm'/>
				<col style = 'width:80mm'/>
				<col style = 'width:80mm'/>
			</colgroup>
			<thead>
			 <tr noWrap='true' class='mainHead'>
			 <td style='colspan:10;fontsize:maintitle'>对账列表</td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true' rowspan='2' valign='middle'>日期</td>
		  	<td nowrap='true' colspan='3'>借方发生额</td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td nowrap='true' colspan='3'>贷方发生额</td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td nowrap='true' colspan='3'>余额</td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<td style="display:none"></td>
		  	<td nowrap='true'>出纳</td>
		  	<td nowrap='true'>会计</td>
		  	<td nowrap='true'>差额</td>
		  	<td nowrap='true'>出纳</td>
		  	<td nowrap='true'>会计</td>
		  	<td nowrap='true'>差额</td>
		  	<td nowrap='true'>出纳</td>
		  	<td nowrap='true'>会计</td>
		  	<td nowrap='true'>差额</td>
      </tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
                <xsl:when test="position() = 1">
                  <td><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:otherwise>
			            <td align="right">
		                <xsl:if test="text() != '0'">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </xsl:if>
	    			      </td>
                </xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>				      <xsl:if test="count(/root/tbody/tr) &gt; 0">		<tr>			<td align="center">合计</td>			<td align='right'>				<xsl:if test="sum(/root/tbody/tr/td[2]) != 0">					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[2]),'#,##0.00')"/>				</xsl:if>			</td>			<td align='right'>				<xsl:if test="sum(/root/tbody/tr/td[3]) != 0">					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),'#,##0.00')"/>				</xsl:if>			</td>			<td align='right'>				<xsl:if test="sum(/root/tbody/tr/td[4]) != 0">					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[4]),'#,##0.00')"/>				</xsl:if>			</td>			<td align='right'>				<xsl:if test="sum(/root/tbody/tr/td[5]) != 0">					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00')"/>				</xsl:if>			</td>			<td align='right'>				<xsl:if test="sum(/root/tbody/tr/td[6]) != 0">					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/>				</xsl:if>			</td>			<td align='right'>				<xsl:if test="sum(/root/tbody/tr/td[7]) != 0">					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/>				</xsl:if>			</td>			<td></td>			<td></td>			<td></td>		</tr>	  </xsl:if>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
