<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		  	<th nowrap='true' rowspan='2' valign='middle'>日期</th>
		  	<th nowrap='true' colspan='3'>借方发生额</th>
		  	<th nowrap='true' colspan='3'>贷方发生额</th>
		  	<th nowrap='true' colspan='3'>余额</th>
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<th nowrap='true'>出纳</th>
		  	<th nowrap='true'>会计</th>
		  	<th nowrap='true'>差额</th>
		  	<th nowrap='true'>出纳</th>
		  	<th nowrap='true'>会计</th>
		  	<th nowrap='true'>差额</th>
		  	<th nowrap='true'>出纳</th>
		  	<th nowrap='true'>会计</th>
		  	<th nowrap='true'>差额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position() = 1">
                  <td><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:otherwise>
			            <td align="right">
		                <xsl:if test="text() != '0'">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </xsl:if>
	    			      </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>      <xsl:if test="count(/root/tbody/tr) &gt; 0">
		<tr>
			<td align="center">合计</td>
			<td align='right'>
				<xsl:if test="sum(/root/tbody/tr/td[2]) != 0">
					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[2]),'#,##0.00')"/>
				</xsl:if>
			</td>
			<td align='right'>
				<xsl:if test="sum(/root/tbody/tr/td[3]) != 0">
					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),'#,##0.00')"/>
				</xsl:if>
			</td>
			<td align='right'>
				<xsl:if test="sum(/root/tbody/tr/td[4]) != 0">
					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[4]),'#,##0.00')"/>
				</xsl:if>
			</td>
			<td align='right'>
				<xsl:if test="sum(/root/tbody/tr/td[5]) != 0">
					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00')"/>
				</xsl:if>

			</td>
			<td align='right'>
				<xsl:if test="sum(/root/tbody/tr/td[6]) != 0">
					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/>
				</xsl:if>
			</td>
			<td align='right'>
				<xsl:if test="sum(/root/tbody/tr/td[7]) != 0">
					<xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/>
				</xsl:if>
			</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	  </xsl:if>
    </tbody>
  </xsl:template>
</xsl:stylesheet>