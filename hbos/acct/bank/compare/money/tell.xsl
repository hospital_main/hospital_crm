<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
	      <th style='display:none'>选择</th>
		  	<th nowrap='true'>日期</th>
		  	<th nowrap='true'>摘要</th>
		  	<th nowrap='true'>对方科目</th>
		  	<th nowrap='true'>收支方向</th>
		  	<th nowrap='true'>金额</th>
		  	<th nowrap='true'>凭证明细号</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="cruTr" select="position()"/>
        <tr>
          <td align='center' style='display:none'>
            <input type='checkbox' tabindex='-1'>
              <xsl:attribute name="id" >tell_<xsl:value-of select="$cruTr"/></xsl:attribute>
              <xsl:attribute name="value">
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position() = 5">
                  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:when test="position() = 6">
                  <td>
	                  <input type='text' value='{text()}' size='10' maxlength='10'>
				              <xsl:attribute name="id">detail_<xsl:value-of select="$cruTr"/></xsl:attribute>
				              <xsl:if test="../td[7] != '0'">
				              <xsl:attribute name="readonly">true</xsl:attribute>
				              </xsl:if>
	                  </input>
                  </td>
                </xsl:when>
                <xsl:when test="position() = 7">
                </xsl:when>
                <xsl:otherwise>
			            <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>