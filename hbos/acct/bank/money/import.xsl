<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th width='25'><input type='checkbox' onclick="checkInit(this)"/></th>
				<th>单据编号</th>
				<th>制单日期</th>
				<th>经费归口科室</th>
				<!--th>摘要</th-->
				<th>报销事由</th>
				<th>实报金额</th>
				<th>支付方式</th>
				<th>领款人</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>支付人</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  noWrap='true'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true'>
	                  		<!--a href="#" -->

			                  <!--xsl:attribute name="onclick">
							  	openDialog("p3insert.html?<xsl:value-of select="../pk/pay_id"/>^<xsl:value-of select="../td[12]"/>","dialogWidth:1024px;dialogHeight:700px");
							  </xsl:attribute-->
							  <xsl:value-of select="."/>

	                  	  	<!--/a-->

			            </td>
                </xsl:when>
                <xsl:when test="position()=5">
			            <td align='right' noWrap='true'>
	                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			           	</td>
                </xsl:when>
                <xsl:when test="position()=7 or position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17">
			            <td  noWrap='true' style="display:none">
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
