<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style="display:none"><input type="checkbox" checked='checked'/></th>
				<th nowrap='true'>出纳日期</th>
				<th nowrap='true'>摘要</th>
				<th nowrap='true'>附件张数</th> 
				<th nowrap='true'>票据号</th> 
				<th nowrap='true'>对方科目</th>
				<th nowrap='true'>收入金额</th> 
				<th nowrap='true'>支出金额</th>   
			</tr>           
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center' style='display:none'>
          <input type='checkbox'  TABINDEX='-1' style='font-size:8px;'  checked='checked'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
    	    </xsl:attribute>
  	  </input>
          </td>
		<xsl:for-each select="td">   
					<xsl:choose>
					  <xsl:when test="position() = 1 or position() = 2 or position() = 3 or position() = 4 or position() = 5">
					     <td align="left">
						<xsl:value-of select="."/>  
					     </td>
					  </xsl:when>
					 <xsl:when test="position() = 6 or position() = 7">
					     <td align="left">
						  <xsl:attribute name="class">numberText</xsl:attribute>
						  <xsl:value-of select="format-number(.,'#,##0.00')"/>
					     </td>
					  </xsl:when>
					<xsl:otherwise>
							
					</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

