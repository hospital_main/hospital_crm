<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:14;fontsize:maintitle'>现金账登记</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td> 
					<td style="display:none"></td>
					<td style="display:none"></td> 
				</tr>
				
				<tr noWrap='true' class='mainHead'>
				  	<td nowrap='true'>出纳日期</td>
				  	<td nowrap='true'>摘要</td>
				  	<td nowrap='true'>附件张数</td>
				  	<td nowrap='true'>票据号</td>
				  	<td nowrap='true'>对方科目</td>
				  	<td nowrap='true'>收入金额</td>
				  	<td nowrap='true'>支出金额</td>
				  	<td nowrap='true'>余额</td>
				  	<td nowrap='true'>制单人</td>
				  	<td nowrap='true'>确认人</td>
				  	<td nowrap='true'>确认日期</td> 
				  	<td nowrap='true'>凭证号</td>
				  	<td nowrap='true'>业务类型</td> 
				  	<td nowrap='true'>单据号</td>  
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
							<xsl:when test="position()=7 or position()=8 or position()=6">
								<td align="right">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()=5"> 
								<td><xsl:value-of select="substring-after(.,'|||')"/></td>
							</xsl:when>
							<xsl:when test="position()=12 or position()=14 or position()=15 or position()=16 or position()=17 ">
							</xsl:when>
							<xsl:when test="position()=11 or position()=13 or position()=18 or position()=19 ">
								<td >
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="translate(.,'|||',' ')"/></td>
							</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
