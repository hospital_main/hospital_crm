<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>ժҪ���</th>
				<th nowrap='true'>ժҪ</th> 
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="pk/*">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 1">
								<a href='#' >
									<xsl:attribute name="onclick">
										javascript:loadData("&lt;summary_id&gt;<xsl:value-of select="../../pk/summary_id"/>&lt;/summary_id&gt;")
									</xsl:attribute><xsl:value-of select="."/>
								</a>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

