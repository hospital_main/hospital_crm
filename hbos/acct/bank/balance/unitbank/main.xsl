<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/bank/balance/unitbank/main.xsl,v 1.1 2012/03/12 01:44:26 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:26 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:130mm'/>
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>	
			</colgroup>
			<thead>
				<tr noWrap='true' class='mainHead'>
			  	<th nowrap='true'>业务日期</th>
			  	<th nowrap='true'>摘要</th>
			  	<th nowrap='true'>结算方式</th>
			  	<th nowrap='true'>票据号</th>
			  	<th nowrap='true'>借方金额</th>
			  	<th nowrap='true'>贷方金额</th>
			  	<th nowrap='true'>对帐</th>
			  	<th nowrap='true'>是否导入</th>			  				  	
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position() = 5">
								</xsl:when>
								<xsl:otherwise>
								<td>
										<xsl:value-of select="."/>
								</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
