<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/bank/balance/unitbank/printView.xsl,v 1.1 2012/03/12 01:44:26 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:26 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:120mm'/>	
				<col style = 'width:120mm'/>
				<col style = 'width:120mm'/>
				<col style = 'width:120mm'/>
				<col style = 'width:120mm'/>
				<col style = 'width:120mm'/>
				<col style = 'width:120mm'/>	
				<col style = 'width:120mm'/>	
				<col style = 'width:120mm'/>	
			</colgroup>
			<thead>
				<tr noWrap='true' class='mainHead'>
	  			<td style='colspan:11;fontsize:maintitle'>单位银行账</td>
			    <td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	
  			</tr>
  			<tr noWrap='true' class='mainHead'>
	  			<td style='colspan:11;fontsize:subtitle'></td>
			    <td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	
  			</tr>
				<tr noWrap='true' class='mainHead'>
			  	<td nowrap='true'>日期</td>
			  	<td nowrap='true'>凭证类型</td>
			  	<td nowrap='true'>凭证号</td>
			  	<td nowrap='true'>摘要</td>
			  	<td nowrap='true'>结算方式</td>
			  	<td nowrap='true'>票据号</td>
			  	<td nowrap='true'>借方金额</td>
			  	<td nowrap='true'>贷方金额</td>
			  	<td nowrap='true'>余额</td>
			  	<td nowrap='true'>是否对账</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
							<xsl:when test="position()=2"> 
								<xsl:if test="contains( . ,'|||')">
									<td><xsl:value-of select="substring-after(.,'|||')"/></td>
								</xsl:if>
								<xsl:if test="not(contains( . ,'|||'))">
									<td><xsl:value-of select="."/></td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=5"> 
								<td><xsl:value-of select="substring-after(.,'|||')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="translate(.,'|||','')"/></td>
							</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
