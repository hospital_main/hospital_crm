<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'></th>
				<th nowrap='true'>对账</th>
				<th nowrap='true'>日期</th>
				<th nowrap='true'>结算方式</th>
				<th nowrap='true'>票据号</th>
				<th nowrap='true'>摘要</th>
				<th nowrap='true'>借方金额</th>
				<th nowrap='true'>贷方金额</th>
				<th nowrap='true'></th>
				<th nowrap='true'></th>
				<th nowrap='true'>对账</th>
				<th nowrap='true'>日期</th>
				<th nowrap='true'>结算方式</th>
				<th nowrap='true'>票据号</th>
				<th nowrap='true'>凭证号</th>
				<th nowrap='true'>摘要</th>
				<th nowrap='true'>借方金额</th>
				<th nowrap='true'>贷方金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td[position() &gt; 6]"> 
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
									<input type="checkbox" pos="l">
										<xsl:if test="../td[1]=0"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
										<xsl:if test="../td[2]=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
										<xsl:attribute name="oldValue"><xsl:value-of select="../td[2]"/></xsl:attribute>
										<xsl:attribute name="rowId"><xsl:value-of select="../td[3]"/></xsl:attribute>
										<xsl:attribute name="leftMM"><xsl:value-of select="../td[12]"/></xsl:attribute>
										<xsl:attribute name="rightMM"><xsl:value-of select="../td[13]"/></xsl:attribute>
									</input>
								</td>
								<td align="left"><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=8">
								<td></td>
								<td>
									<input type="checkbox" pos="r">
										<xsl:if test="../td[4]=0"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
										<xsl:if test="../td[5]=1"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
										<xsl:attribute name="oldValue"><xsl:value-of select="../td[5]"/></xsl:attribute>
										<xsl:attribute name="rowId"><xsl:value-of select="../td[6]"/></xsl:attribute>
										<xsl:attribute name="leftMM"><xsl:value-of select="../td[20]"/></xsl:attribute>
										<xsl:attribute name="rightMM"><xsl:value-of select="../td[21]"/></xsl:attribute>
									</input>
								</td>
								<td align="left"><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=12">
								<td><a href="#">
								<xsl:attribute name="onclick">
									openVouchDlg("&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+readOnly,onlyOne&lt;/edit_mask&gt;")
								</xsl:attribute>
								<xsl:value-of select="."/></a></td>
							</xsl:when>
							<xsl:when test="position()=6 or position()=7 or position()=14 or position()=15">
								<td class="numberText" align="right"><xsl:if test=".!=0"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:if></td>
							</xsl:when>
							<xsl:otherwise>
								<td align="left"><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>