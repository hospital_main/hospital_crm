<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
    <table width='100%' class='mainTable' border='1' borderColor='#292954'>
    	<thead>
        <xsl:variable name="disCheck" select="/root/tbody/tr[1]/td[1]"/>
    		<tr noWrap='true' class='mainHead'>
    		  <xsl:if test="$disCheck='0'">
    			  <th noWrap='true' style="display:none">对账</th>
    			</xsl:if>    			
    		  <xsl:if test="$disCheck!='0'">
    			  <th noWrap='true'>对账</th>
    			</xsl:if>    			
    			<th noWrap='true'>日期</th>
    			<th noWrap='true'>结算方式</th>
    			<th noWrap='true'>票据号</th>
    			<th noWrap='true'>摘要</th>
    			<th noWrap='true'>借方金额</th>
    			<th noWrap='true'>贷方金额</th>
    		</tr>
    	</thead>
    	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:if test="$disCheck!='0'">
              <td align='center'>
                <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
                  <xsl:attribute name="value" >
                    <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
          			  </xsl:attribute>
          			  <xsl:if test="td[2]='1'">
  								  <xsl:attribute name="checked"/>
  							  </xsl:if>
        			  </input>
              </td>
            </xsl:if>
            <xsl:if test="$disCheck='0'">
              <td align='center' style="display:none">
                <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
                  <xsl:attribute name="value" >
                    <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
          			  </xsl:attribute>
          			  <xsl:if test="td[2]='1'">
  								  <xsl:attribute name="checked"/>
  							  </xsl:if>
        			  </input>
              </td>
            </xsl:if>
          	<xsl:for-each select="td[position()>2]">
              <xsl:choose>
                <xsl:when test="position()=6 or position()=5">
                  <td align="right">
      							<xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td align='left' noWrap='true'>
      							<xsl:value-of select="."/>
                  </td>  
                </xsl:otherwise>
              </xsl:choose>              
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
    </table>
	</xsl:template>
</xsl:stylesheet>