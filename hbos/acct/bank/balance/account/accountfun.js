var maxDate;
var minDate;
var submitXml;
var curAcctSubjCode;
var money_type1;
var money_type2;
var quce_code;
var que_code;

function init()
{
	comp_code.value = getCompCode();
	copy_code.value = getCopyCode();
	comp_year.value = getAcctYear();
	
	acct_subj.value = getPageArg("acct_subj_name");
	subj_date.value = getCurrentDate();

	var cheque_type1 = getPageArg("cheque_type1");
	money_type1 = getPageArg("money_type1");
	var min_count1 = getPageArg("min_count1");
	var max_count1 = getPageArg("max_count1");
	var acct_check1 = getPageArg("acct_check1");
	var acct_subj_code = getPageArg("acct_subj_code");
	var auto_dovalue = getPageArg("auto_dovalue");
	quce_code = getPageArg("quce_code");//结算方式
	que_code = getPageArg("que_code");//票据号
	var to_money = getPageArg("to_money");
	var cheque_type2 = getPageArg("cheque_type2");
	money_type2 = getPageArg("money_type2");
	var min_count2 = getPageArg("min_count2");
	var max_count2 = getPageArg("max_count2");
	var acct_check2 = getPageArg("acct_check2");
	var begin_date1 = getPageArg("begin_date1");
	var end_date1 = getPageArg("end_date1");
	var begin_date2 = getPageArg("begin_date2");
	var end_date2 = getPageArg("end_date2");

	subj_code.value = acct_subj_code;
	
	var sCommon = "<a>" + comp_code.value + "</a><b>" + copy_code.value + "</b><c>" + comp_year.value + "</c><d>" + acct_subj_code +  "</d>";
	var p1 = sCommon + "<e>" + begin_date1 + "</e><f>" + end_date1 + "</f><g>" + cheque_type1 + "</g><h>" + money_type1 + "</h><i>" + min_count1 + "</i>";
	p1 += "<j>" + max_count1 + "</j><k>" + acct_check1 + "</k>";
	var sTitle = creatActiveXHeadXML("对账,日期,结算方式,票据号,摘要,借方金额,贷方金额");
	t1.HaveFirstSelCol = true
	t1.AllowUserResize = true;
	t1.SetHeaderFromXML(sTitle);
	t1.SetTableColumn(1, "width=60");
	t1.SetTableColumn(2, "width=70");
	t1.SetTableColumn(3, "width=80");
	t1.SetTableColumn(4, "width=70");
	t1.SetTableColumn(5, "width=70");
	t1.SetTableColumn(6, "width=70");
	t1.SetTableColumn(7, "width=70");
	t1.SetTableFromHttp("acctBankBalanceAccount_load_t1", p1);
	
 	var p2 = sCommon + "<e>" + begin_date2 + "</e><f>" + end_date2 + "</f><g>" + cheque_type2 + "</g><h>" + money_type2 + "</h><i>" + min_count2 + "</i>";
	p2 += "<j>" + max_count2 + "</j><k>" + acct_check2 + "</k>";
	sTitle = creatActiveXHeadXML("对账,日期,结算方式,票据号,凭证号,摘要,借方金额,贷方金额,凭证id");
	t2.HaveFirstSelCol = true
	t2.AllowUserResize = true;
	t2.SetHeaderFromXML(sTitle);
	t2.SetTableColumn(1, "width=60");
	t2.SetTableColumn(2, "width=70");
	t2.SetTableColumn(3, "width=80");
	t2.SetTableColumn(4, "width=60");
	t2.SetTableColumn(5, "width=100;type=link");
	t2.SetTableColumn(6, "width=70");
	t2.SetTableColumn(7, "width=70");
	t2.SetTableColumn(8, "width=70");		
	t2.ColumnHide(9);
	t2.SetTableFromHttp("acctBankBalanceAccount_load_t2", p2);

	if (auto_dovalue == "1") window.setTimeout(function(){AutoCheckValue()}, 100);

}

function AutoCheckValue()
{
	var i;
	var j;
	var RowCount = t1.GetRowCount();

	if(money_type1!="" || money_type2!=""){
	
	if(money_type1=="0"){
	for (i = 1; i <= RowCount; i++)
	{
		
		if (t1.GetCellData(i, 1) == "已对帐")
		{
			t1.CheckBoxEnable(i, false);
		}
		else
		{

			for (j = 1; j <=t2.GetRowCount(); j++)
			{
        var SelT2 = ";" +  t2.GetSelectedRow();
        var SelT1 = ";" +  j;
				if (t2.GetCellData(j, 1) == "已对帐")
					{
						t2.CheckBoxEnable(j, false);
					}
				else{
	
				if(quce_code==1 && que_code==0){
				  if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 3) == t2.GetCellData(j, 3)) && (t1.GetCellData(i, 6)!= 0) && (t2.GetCellData(j, 8)!= 0) && (t1.GetCellData(i, 6) == t2.GetCellData(j, 8)))
				 {
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				 }
				}
				if(quce_code==0 && que_code==1){
				  if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 4) == t2.GetCellData(j, 4)) && (t1.GetCellData(i, 6)!= 0) && (t2.GetCellData(j, 8)!= 0) && (t1.GetCellData(i, 6) == t2.GetCellData(j, 8)))
  				 {
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				 }
				}
				if(quce_code==0 && que_code==0){
				if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 6)!= 0) && (t2.GetCellData(j, 8)!= 0) && (t1.GetCellData(i, 6) == t2.GetCellData(j, 8)))
				 {
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				 }
				}
				if(quce_code==1 && que_code==1){
				  if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 3) == t2.GetCellData(j, 3) && t1.GetCellData(i, 4) == t2.GetCellData(j, 4)) && (t1.GetCellData(i, 6)!= 0) && (t2.GetCellData(j, 8)!= 0) && (t1.GetCellData(i, 6) == t2.GetCellData(j, 8)))
				 {
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				 }
				}
				
			    }
			}
		}
	  }
	}else{
	
	  for (i = 1; i <= RowCount; i++)
	{
		
		if (t1.GetCellData(i, 1) == "已对帐")
		{
			t1.CheckBoxEnable(i, false);
		}
		else
		{
			for (j = 1; j <= t2.GetRowCount(); j++)
			{//alert("i==="+i+"|||j==="+j);
			        var SelT2 = ";" +  t2.GetSelectedRow();
			        var SelT1 = ";" +  j;
				if (t2.GetCellData(j, 1) == "已对帐")
					{
						t2.CheckBoxEnable(j, false);
					}
				else{
                                 if(quce_code==0 && que_code==0){
				 if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 7)!= 0) && (t1.GetCellData(i, 7) == t2.GetCellData(j, 7)))
				{
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				}
			      }
			      if(quce_code==1 && que_code==1){
			       if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 3) == t2.GetCellData(j, 3) && t1.GetCellData(i, 4) == t2.GetCellData(j, 4)) && (t1.GetCellData(i, 7)!= 0) && (t1.GetCellData(i, 7) == t2.GetCellData(j, 7)))
				{
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				}
			      }
			      
			      if(quce_code==1 && que_code==0){
			       if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 3) == t2.GetCellData(j, 3)) && (t1.GetCellData(i, 7)!= 0) && (t1.GetCellData(i, 7) == t2.GetCellData(j, 7)))
				{
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				}
			      }
			      
			      if(quce_code==0 && que_code==1){
			       if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 4) == t2.GetCellData(j, 4)) && (t1.GetCellData(i, 7)!= 0) && (t1.GetCellData(i, 7) == t2.GetCellData(j, 7)))
				{
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				}
			      }
			      
			    }
			}
		}
	   }
	 }
    }else{
      for (i = 1; i <= RowCount; i++)
	{
		
		if (t1.GetCellData(i, 1) == "已对帐")
		{
			t1.CheckBoxEnable(i, false);
		}
		else
		{
			for (j = 1; j <=t2.GetRowCount(); j++)
			{//alert("i==="+i+"|||j==="+j);
			        var SelT2 = ";" +  t2.GetSelectedRow();
			        var SelT1 = ";" +  j;
				if (t2.GetCellData(j, 1) == "已对帐")
					{
						t2.CheckBoxEnable(j, false);
					}
				else{
				 if(quce_code==0 && que_code==0){
				 if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 6)!= 0) && (t2.GetCellData(j, 8)!= 0) && (t1.GetCellData(i, 6) == t2.GetCellData(j, 8)))
				 {
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				 }
                                 
				 if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 7)!= 0) && (t2.GetCellData(j, 7)!=0) && (t1.GetCellData(i, 7) == t2.GetCellData(j, 7)))
				{
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				}
			      }
			      if(quce_code==1 && que_code==1){
			        if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 3) == t2.GetCellData(j, 3) && t1.GetCellData(i, 4) == t2.GetCellData(j, 4)) && (t1.GetCellData(i, 6)!= 0) && (t2.GetCellData(j, 8)!= 0) && (t1.GetCellData(i, 6) == t2.GetCellData(j, 8)))
				 {
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				 }
                                 
				 if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 3) == t2.GetCellData(j, 3) && t1.GetCellData(i, 4) == t2.GetCellData(j, 4)) && (t1.GetCellData(i, 7)!= 0) && (t2.GetCellData(j, 7)!=0) && (t1.GetCellData(i, 7) == t2.GetCellData(j, 7)))
				{
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				}
			      }
			      if(quce_code==1 && que_code==0){
			        if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 3) == t2.GetCellData(j, 3)) && (t1.GetCellData(i, 6)!= 0) && (t2.GetCellData(j, 8)!= 0) && (t1.GetCellData(i, 6) == t2.GetCellData(j, 8)))
				 {
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				 }
                                 
				 if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 3) == t2.GetCellData(j, 3)) && (t1.GetCellData(i, 7)!= 0) && (t2.GetCellData(j, 7)!=0) && (t1.GetCellData(i, 7) == t2.GetCellData(j, 7)))
				{
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				}
			      }
			      
			      if(quce_code==0 && que_code==1){
			        if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 4) == t2.GetCellData(j, 4)) && (t1.GetCellData(i, 6)!= 0) && (t2.GetCellData(j, 8)!= 0) && (t1.GetCellData(i, 6) == t2.GetCellData(j, 8)))
				 {
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				 }
                                 
				 if (((SelT2.indexOf(SelT1) == -1)) && (t1.GetCellData(i, 4) == t2.GetCellData(j, 4)) && (t1.GetCellData(i, 7)!= 0) && (t2.GetCellData(j, 7)!=0) && (t1.GetCellData(i, 7) == t2.GetCellData(j, 7)))
				{
						t1.SetCheckBoxValue(i, "Checked");
						t2.SetCheckBoxValue(j, "Checked");
						break;
				}
			      }
			      
			    }
			}
		}
	   }
    }
    
}

function DoColLink(TableID, RowIndex, ColIndex, CellValue)
{
	var vid = t2.GetCellData(RowIndex, 9);
	openVouchDlg("<vouch_id>"+ vid +"</vouch_id><edit_mask>+onlyOne,readOnly</edit_mask>");
}

function SaveClick()
{
	if (t1.GetRowCount() != 0 && t2.GetRowCount() != 0)
	{
		var t1Sel = t1.GetSelectedRow();
		var t2Sel = t2.GetSelectedRow();
		var sTmpAarry1, sTmpAarry2;
		var cnt1, cnt2;

		if (t1Sel != "" && t2Sel != "")
		{
			var t1Total = 0;
			sTmpAarry1 = t1Sel.split(";");
			var cnt1 = sTmpAarry1.length - 1;
			for(var i = 0; i <cnt1; i++)
			{
				t1Total = parseFloat(t1Total) + parseFloat(t1.GetCellData(sTmpAarry1[i], 6));
			}
			
			var t2Total = 0;
			sTmpAarry2 = t2Sel.split(";");
			cnt2 = sTmpAarry2.length - 1;
			for(var i = 0; i <cnt2; i++)
			{
				t2Total = parseFloat(t2Total) + parseFloat(t2.GetCellData(sTmpAarry2[i], 8));
			}
			
			if (t1Total != t2Total)
			{
				alert("对账单借方金额总和必须和单位账贷方金额总和相等！");
				return;
			}
			
			t1Total = 0;
			for (var i = 0; i <cnt1; i++)
			{
				t1Total = parseFloat(t1Total) + parseFloat(t1.GetCellData(sTmpAarry1[i], 7));
				
			}
			
			t2Total = 0;
			for (var i = 0; i <cnt2; i++)
			{
				t2Total = parseFloat(t2Total) + parseFloat(t2.GetCellData(sTmpAarry2[i], 7));
			}
                        
			if (t1Total != t2Total)
			{
				alert("对账单贷方金额总和必须和单位账借方金额总和相等！");
				return;
			}
			//alert(cnt1);
			subBody.load ="acctBankBalanceAccount_save";
			subBody.para = "<x>" + comp_code.value + "</x><y>" + copy_code.value + "</y><z>" + comp_year.value + "</z><a>" + subj_code.value + "</a><b>" + subj_date.value + "</b><c>" + cnt1 + "</c><d>" + cnt2 + "</d><e>" + subj_memo.value + "</e>";
			subBody.post();
			var a = subBody.getOneDim();
			if (a)
			{
				check_id.value = a[0];
				t1.DeleteSelectedRow("acctBankBalanceAccount_save1", "<cid>" + a[0] + "</cid>");
				t2.DeleteSelectedRow("acctBankBalanceAccount_save2", "<cid>" + a[0] + "</cid>");
			}
		}
				
	}
}

