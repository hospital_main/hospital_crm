<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/bank/balance/maintenance/printView.xsl,v 1.1 2012/03/12 01:44:26 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:26 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:150mm'/>	
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>
				<col style = 'width:150mm'/>	
				<col style = 'width:150mm'/>	
			</colgroup>
			<thead>
				<tr noWrap='true' class='mainHead'>
	  			<td style='colspan:5;fontsize:maintitle'>银行对账单</td>
			    <td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
  			</tr>
  			<tr noWrap='true' class='mainHead'>
	  			<td style='colspan:5;fontsize:subtitle'></td>
			    <td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	
  			</tr>
				<tr noWrap='true' class='mainHead'>
			  	<td nowrap='true'>操作日期</td>
			  	<td nowrap='true'>对账单笔数</td>
			  	<td nowrap='true'>银行账笔数</td>
			  	<td nowrap='true'>对账备注</td>
			  	<td nowrap='true'>检测通过</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
					<td><xsl:value-of select="."/></td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
