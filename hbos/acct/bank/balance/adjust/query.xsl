<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>银行账项目</td>
			  	<td nowrap='true'>金额</td>
			  	<td nowrap='true'>银行对账单项目</td>
			  	<td nowrap='true'>金额</td>
			</tr>
		</thead>  	
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() mod 2 = 0">
							<td align="right">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
							</xsl:when>
							<xsl:otherwise>
							<td>
									<xsl:value-of select="."/>
							</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



