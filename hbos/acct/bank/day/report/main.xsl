<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th noWrap='true' width="25%">项 目</th>
  			<th noWrap='true' class='moneyCol'>昨日库存</th>
  			<th noWrap='true' class='moneyCol'>今日收入</th>
  			<th noWrap='true' class='moneyCol'>今日支出</th>
  			<th noWrap='true' class='moneyCol'>今日库存</th>
  		</tr>
  	</thead>
  	<tbody>
  	<xsl:for-each select="/root/tbody/tr">
  		<tr>
  			<xsl:for-each select="td">
  				<td  nowarp='true'>
  					<xsl:choose>
  						<xsl:when test="position()=2">
		               <div align='right' >
		               	<xsl:value-of select="format-number(.,'#,##0.00')"/>
		               </div>
  						</xsl:when>
  						<xsl:when test="position()=3">
		               <div align='right' >
		               <xsl:value-of select="format-number(.,'#,##0.00')"/>
		               </div>
  						</xsl:when>
  						<xsl:when test="position()=4">
		               <div align='right'>
		               <xsl:value-of select="format-number(.,'#,##0.00')"/>
		               </div>
  						</xsl:when>
  						<xsl:when test="position()=5">
		               <div align='right'>
		               <xsl:value-of select="format-number(.,'#,##0.00')"/>
		               </div>
  						</xsl:when>
              <xsl:otherwise>
                <div width="50"><xsl:value-of select="."/></div>
              </xsl:otherwise>  						
  					</xsl:choose>
  				</td>
  			</xsl:for-each>
  		</tr>
  	</xsl:for-each>
  	</tbody>
	</xsl:template>
</xsl:stylesheet>



