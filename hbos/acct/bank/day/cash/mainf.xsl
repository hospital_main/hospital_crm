<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
  		<xsl:variable name="ACCTYEAR" select="/root/annex/ACCTYEAR"/>
      <tr noWrap='true' class='mainHead'>
		  	<th id='yearText' nowrap='true' colspan='2'><xsl:value-of select="$ACCTYEAR"/>年</th>
		  	<th nowrap='true' rowspan='2' valign='middle'>凭证号</th>
		  	<th nowrap='true' rowspan='2' valign='middle'>摘要</th>
		  	<th nowrap='true' rowspan='2' valign='middle'>汇率</th>
		  	<th nowrap='true' style="display:none" rowspan='2' valign='middle'>现金科目</th>
		  	<th nowrap='true' colspan='2' valign='middle'>借方金额</th>
		  	<th nowrap='true' colspan='2' valign='middle'>贷方金额</th>
		  	<th nowrap='true' rowspan='2' valign='middle'>方向</th>
		  	<th nowrap='true' colspan='2' valign='middle'>余额</th>
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<th style="display:none" nowrap='true'>年</th>
		  	<th nowrap='true'>月</th>
		  	<th nowrap='true'>日</th>
		  	<th style="display:none" nowrap='true'></th>
		  	<th style="display:none" nowrap='true'></th>
		  	<th style="display:none" nowrap='true'></th>
		  	<th style="display:none" nowrap='true'></th>
		  	<th nowrap='true'>本币</th>
		  	<th nowrap='true'>原币</th>
		  	<th nowrap='true'>本币</th>
		  	<th nowrap='true'>原币</th>
		  	<th style="display:none" nowrap='true'></th>
		  	<th nowrap='true'>本币</th>
		  	<th nowrap='true'>原币</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position() = 1 ">
			            <td style="display:none" align="left">
	                  <xsl:value-of select="."/>
      			      </td>
                </xsl:when>
                <xsl:when test="position() = 4">
                <td align="center">
                <a tabindex='-1'>
                  <xsl:attribute name="href" >
    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;');
  	          		</xsl:attribute><xsl:value-of select="../td[15]"/></a>
  	          	</td>
                </xsl:when>
                <xsl:when test="position() =6">
			            <td align="right">
			            	<xsl:if test="../td[5] != '昨日结转'">
	                  	<xsl:value-of select="."/>
	                  </xsl:if>
      			      </td>
                </xsl:when>
                <xsl:when test="position() = 7">
                  <td style="display:none">
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                <xsl:when test="position() = 8 or position() = 9 or position() = 10 or position() = 11">
			            <td align="right">
		                <xsl:if test="text() != '0'">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </xsl:if>
      			      </td>
                </xsl:when>
                <xsl:when test="position() = 13">
			            <td align="right">
	                  <xsl:choose>	
		                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[12]='平'">
	  			            	Q
	  			          	</xsl:when>
			                <xsl:otherwise>
			                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			                </xsl:otherwise>
	             		 </xsl:choose>
      			      </td>
                </xsl:when>
                <xsl:when test="position() = 14">
			            <td align="right">
	                  <xsl:choose>	
		                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[12]='平'">
	  			            	Q
	  			          	</xsl:when>
			                <xsl:otherwise>
			                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			                </xsl:otherwise>
	             		 </xsl:choose>
      			      </td>
                </xsl:when>
                <xsl:when test="position() = 15">
                </xsl:when>
                <xsl:otherwise>
                	<td>
                  <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>