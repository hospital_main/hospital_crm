<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/bank/day/cash/printViewf1.xsl,v 1.1 2012/03/12 01:44:26 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:26 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
			<tr noWrap='true' class='mainHead'>
			  <td style='colspan:15;fontsize:maintitle'>现金日记账</td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true' colspan='2'>xxxx年</td>
		  	<td style="display:none"></td>
		  	<td nowrap='true' rowspan='2' valign='middle'>凭证号</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>摘要</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>汇率</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>现金科目</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>对方科目</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>对方金额</td>
		  	<td nowrap='true' colspan='2' valign='middle'>借方金额</td>
		  	<td style="display:none"></td>
		  	<td nowrap='true' colspan='2' valign='middle'>贷方金额</td>
		  	<td style="display:none"></td>
		  	<td nowrap='true' rowspan='2' valign='middle'>方向</td>
		  	<td nowrap='true' colspan='2' valign='middle'>余额</td>
		  	<td style="display:none"></td>
      </tr>
     <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true'>月</td>
		  	<td nowrap='true'>日</td>
		  	<td style="display:none" nowrap='true'></td>
		  	<td style="display:none" nowrap='true'></td>
		  	<td style="display:none" nowrap='true'></td>
		  	<td style="display:none" nowrap='true'></td>
		  	<td style="display:none" nowrap='true'></td>
		  	<td style="display:none" nowrap='true'></td>
		  	<td nowrap='true'>本币</td>
		  	<td nowrap='true'>原币</td>
		  	<td nowrap='true'>本币</td>
		  	<td nowrap='true'>原币</td>
		  	<td style="display:none" nowrap='true'></td>
		  	<td nowrap='true'>本币</td>
		  	<td nowrap='true'>原币</td>
      </tr>
			</thead>
			<tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="vId" select="td[18]"/>
      	<xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[18]=$vId])"/>
      	<xsl:variable name="rowPos" select="position()"/>
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position() = 1 ">
                </xsl:when>
                <xsl:when test="position() = 4">
	                <xsl:if test="$rowPos = 1">
		                <td align="center" valign="center">
	                	<xsl:if test="$vId != ''">
			                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
		                </xsl:if>
		                <a tabindex='-1'>
		                  <xsl:attribute name="href" >
		    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;');
		  	          		</xsl:attribute><xsl:value-of select="../td[17]"/></a>
		  	          	</td>
		  	          </xsl:if>

	                <xsl:if test="$rowPos &gt; 1">
	                	<xsl:if test="$vId != ../../tr[$rowPos - 1 ]/td[18]">
			                <td align="center" valign="center">
		                	<xsl:if test="$vId != ''">
				                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			                </xsl:if>
			                <a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;');
			  	          		</xsl:attribute><xsl:value-of select="../td[17]"/></a>
			  	          	</td>
		  	          	</xsl:if>

	                	<xsl:if test="$vId = ../../tr[$rowPos - 1 ]/td[18]">
		                	<xsl:if test="$vId != ''">
			                <td align="center" style="display:none" >
			  	          	</td>
			  	          	</xsl:if>
		                	<xsl:if test="$vId = ''">
			                <td align="center" >
			                	<xsl:value-of select="../td[17]"/>
			  	          	</td>
			  	          	</xsl:if>
		  	          	</xsl:if>
		  	          </xsl:if>
                </xsl:when>
                <xsl:when test="position() =6">
			            <td align="right">
			            	<xsl:if test="../td[5] != '昨日结转'">
	                  	<xsl:value-of select="."/>
	                  </xsl:if>
      			      </td>
                </xsl:when>
                <xsl:when test="position() = 8 ">
			            <td align="left"  valign="center">
	                  <xsl:value-of select="."/>
      			      </td>
                </xsl:when>

                <xsl:when test="position() = 9 ">
			            <td align="right">
		                <xsl:if test="text() != '0'">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </xsl:if>
      			      </td>
                </xsl:when>

               <xsl:when test="position() = 10 or position() = 11 or position() = 12 or position() = 13">
	                <xsl:if test="$rowPos = 1">
		                <td align="right">
			                <xsl:if test="text() != '0'">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                  </xsl:if>
		  	          	</td>
		  	          </xsl:if>

	                <xsl:if test="$rowPos &gt; 1">
	                	<xsl:if test="$vId != ../../tr[$rowPos - 1 ]/td[18]">
			                <td align="right">
		                	<xsl:if test="$vId != ''">
				                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			                </xsl:if>
				                <xsl:if test="text() != '0'">
			                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			                  </xsl:if>
			  	          	</td>
		  	          	</xsl:if>

	                	<xsl:if test="$vId = ../../tr[$rowPos - 1 ]/td[18]">
		                	<xsl:if test="$vId != ''">
				                <td align="center" style="display:none" >
				  	          	</td>
			                </xsl:if>
			                <xsl:if test="$vId = ''">
				                <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			                </xsl:if>
		  	          	</xsl:if>
		  	          </xsl:if>

                </xsl:when>
                <xsl:when test="position() = 15">
	                <xsl:if test="$rowPos = 1">
		                <td align="right">
			                 <xsl:choose>	
			                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[14]='平'">
		  			            	Q
		  			          	</xsl:when>
				                <xsl:otherwise>
				                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
				                </xsl:otherwise>
		             		 </xsl:choose>
		  	          	</td>
		  	          </xsl:if>

	                <xsl:if test="$rowPos &gt; 1">
	                	<xsl:if test="$vId != ../../tr[$rowPos - 1 ]/td[18]">
			                <td align="right">
		                	<xsl:if test="$vId != ''">
				                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			                </xsl:if>
				                <xsl:choose>	
			                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[14]='平'">
		  			            	Q
		  			          	</xsl:when>
				                <xsl:otherwise>
				                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
				                </xsl:otherwise>
		             			 </xsl:choose>
			  	          	</td>
		  	          	</xsl:if>

	                	<xsl:if test="$vId = ../../tr[$rowPos - 1 ]/td[18]">
		                	<xsl:if test="$vId != ''">
				                <td align="center" style="display:none" >
				  	          	</td>
			                </xsl:if>
			                <xsl:if test="$vId = ''">
				                <td align="right">
					                <xsl:choose>	
				                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[14]='平'">
			  			            	Q
			  			          	</xsl:when>
					                <xsl:otherwise>
					                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
					                </xsl:otherwise>
			             		 	</xsl:choose>	
				                </td>
			                </xsl:if>
		  	          	</xsl:if>
		  	          </xsl:if>

                </xsl:when>
                
                <xsl:when test="position() = 16">
	                <xsl:if test="$rowPos = 1">
		                <td align="right">
			                 <xsl:choose>	
			                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[14]='平'">
		  			            	Q
		  			          	</xsl:when>
				                <xsl:otherwise>
				                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
				                </xsl:otherwise>
		             		 </xsl:choose>
		  	          	</td>
		  	          </xsl:if>

	                <xsl:if test="$rowPos &gt; 1">
	                	<xsl:if test="$vId != ../../tr[$rowPos - 1 ]/td[18]">
			                <td align="right">
		                	<xsl:if test="$vId != ''">
				                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			                </xsl:if>
				                <xsl:choose>	
			                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[14]='平'">
		  			            	Q
		  			          	</xsl:when>
				                <xsl:otherwise>
				                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
				                </xsl:otherwise>
		             			 </xsl:choose>
			  	          	</td>
		  	          	</xsl:if>

	                	<xsl:if test="$vId = ../../tr[$rowPos - 1 ]/td[18]">
		                	<xsl:if test="$vId != ''">
				                <td align="center" style="display:none" >
				  	          	</td>
			                </xsl:if>
			                <xsl:if test="$vId = ''">
				                <td align="right">
					                <xsl:choose>	
				                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[14]='平'">
			  			            	Q
			  			          	</xsl:when>
					                <xsl:otherwise>
					                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
					                </xsl:otherwise>
			             		 	</xsl:choose>	
				                </td>
			                </xsl:if>
		  	          	</xsl:if>
		  	          </xsl:if>

                </xsl:when>

                <xsl:when test="position() = 17 or position() = 18">
                </xsl:when>

                <xsl:otherwise>
	                <xsl:if test="$rowPos = 1">
		                <td align="left" valign="center">
		                	<xsl:if test="$vId != ''">
				                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			                </xsl:if>
											<xsl:value-of select="."/>
		  	          	</td>
		  	          </xsl:if>

	                <xsl:if test="$rowPos &gt; 1">
	                	<xsl:if test="$vId != ../../tr[$rowPos - 1 ]/td[18]">
			                <td align="left" valign="center">
		                	<xsl:if test="$vId != ''">
				                <xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
			                </xsl:if>
												<xsl:value-of select="."/>
			  	          	</td>
		  	          	</xsl:if>

	                	<xsl:if test="$vId = ../../tr[$rowPos - 1 ]/td[18]">
		                	<xsl:if test="$vId != ''">
				                <td align="center" style="display:none" >
				  	          	</td>
			                </xsl:if>
			                <xsl:if test="$vId = ''">
				                <td><xsl:value-of select="."/></td>
			                </xsl:if>
		  	          	</xsl:if>
		  	          </xsl:if>

                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
