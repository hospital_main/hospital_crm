<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/bank/day/cash/printView.xsl,v 1.1 2012/03/12 01:44:26 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:26 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
			<tr noWrap='true' class='mainHead'>
			  <td style='colspan:8;fontsize:maintitle'>现金日记账</td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true' colspan='2'>xxxx年</td>
		  	<td style="display:none"></td>
		  	<td nowrap='true' rowspan='2' valign='middle'>凭证号</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>摘要</td>
		  	<!--td nowrap='true' rowspan='2' valign='middle'>现金科目</td-->
		  	<td nowrap='true' rowspan='2' valign='middle'>借方金额</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>贷方金额</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>方向</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>余额</td>
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true'>月</td>
		  	<td nowrap='true'>日</td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
      </tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
                <xsl:when test="position() = 1 ">
                </xsl:when>
                <xsl:when test="position() = 4">
									<td><xsl:value-of select="../td[11]"/></td>
                </xsl:when>
                <xsl:when test="position() = 6">
                </xsl:when>
                <xsl:when test="position() = 7 or position() = 8">
			            <td align="right">
		                <xsl:if test="text() != '0'">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </xsl:if>
      			      </td>
                </xsl:when>
                <xsl:when test="position() = 10">
			            <td align="right">
		                <xsl:choose>	
		                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[9]='平'">
	  			            	Q
	  			          	</xsl:when>
			                <xsl:otherwise>
			                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			                </xsl:otherwise>
	             		 </xsl:choose>
      			      </td>
                </xsl:when>
                <xsl:when test="position() = 11">
                </xsl:when>
								<xsl:otherwise>
								<td>
										<xsl:value-of select="."/>
								</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
