<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/bank/day/bank/printView1.xsl,v 1.2 2012/09/21 07:14:08 linaikun Exp $
 $Author: linaikun $
 $Date: 2012/09/21 07:14:08 $
 $Revision: 1.2 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
  		<xsl:variable name="SHOWDZXX" select="/root/annex/SHOWDZXX"/>
			<tr noWrap='true' class='mainHead'>
				<xsl:choose>	            
          <xsl:when test="$SHOWDZXX = '1' or $SHOWDZXX = 1">  
			  		<td style='colspan:14;fontsize:maintitle'>银行日记账</td>
		  			<td style="display:none"></td>
          </xsl:when> 
          <xsl:otherwise>
			  		<td style='colspan:13;fontsize:maintitle'>银行日记账</td>
        	</xsl:otherwise>  
   		 </xsl:choose> 
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true' colspan='2'>xxxx年</td>
		  	<td style="display:none"></td>
		  	<td nowrap='true' rowspan='2' valign='middle'>凭证号</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>摘要</td>
      	<xsl:choose>	            
          <xsl:when test="$SHOWDZXX = '1' or $SHOWDZXX = 1">  
		  	<td nowrap='true' rowspan='2' valign='middle'>对账</td>
          </xsl:when> 
          <xsl:otherwise>
        	</xsl:otherwise>  
   		 </xsl:choose> 
		  	<!--td nowrap='true' rowspan='2' valign='middle'>银行科目</td-->
		  	<td nowrap='true' rowspan='2' valign='middle'>对方科目</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>对方金额</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>票据类型</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>票据号</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>借方金额</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>贷方金额</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>方向</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>余额</td>
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true'>月</td>
		  	<td nowrap='true'>日</td>
      </tr>
			</thead>
			<tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="vId" select="td[17]"/>
      	<xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[17]=$vId])"/>
      	<xsl:variable name="rowPos" select="position()"/>
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position() = 1 ">
                </xsl:when>
                <xsl:when test="position() = 4">
	                <xsl:if test="$rowPos = 1">
		                <td align="center" valign="center">
		                <a tabindex='-1'>
		                  <xsl:attribute name="href" >
		    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;');
		  	          		</xsl:attribute><xsl:value-of select="../td[16]"/></a>
		  	          	</td>
		  	          </xsl:if>

	                <xsl:if test="$rowPos &gt; 1">

			                <td align="center" valign="center">
			                <a tabindex='-1'>
			                  <xsl:attribute name="href" >
			    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;');
			  	          		</xsl:attribute><xsl:value-of select="../td[16]"/></a>
			  	          	</td>

		  	          </xsl:if>
                </xsl:when>
                <xsl:when test="position() = 6">
					      	<xsl:choose>	            
					          <xsl:when test="$SHOWDZXX = '1' or $SHOWDZXX = 1">  
											<td align="center"><xsl:value-of select="."/></td>
					          </xsl:when> 
					          <xsl:otherwise>
					        	</xsl:otherwise>  
					   		 </xsl:choose> 
                </xsl:when>
                <xsl:when test="position() = 7">
                </xsl:when>
                <xsl:when test="position() = 8 ">
			            <td align="left"  valign="center">
	                  <xsl:value-of select="."/>
      			      </td>
                </xsl:when>

                <xsl:when test="position() = 9 ">
			            <td align="right">
		                <xsl:if test="text() != '0'">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </xsl:if>
      			      </td>
                </xsl:when>

                <xsl:when test="position() = 12 or position() = 13">
	                <xsl:if test="$rowPos = 1">
		                <td align="right" valign="center">
			                <xsl:if test="text() != '0'">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
		                  </xsl:if>
		  	          	</td>
		  	          </xsl:if>

	                <xsl:if test="$rowPos &gt; 1">

			                <td align="right" valign="center">
				                <xsl:if test="text() != '0'">
			                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
			                  </xsl:if>
			  	          	</td>

		  	          </xsl:if>

                </xsl:when>
                <xsl:when test="position() = 15">
	                <xsl:if test="$rowPos = 1">
		                <td align="right">
			                <xsl:choose>	
		                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[14]='平'">
	  			            	Q
	  			          	</xsl:when>
			                <xsl:otherwise>
			                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			                </xsl:otherwise>
	             		 </xsl:choose>
		  	          	</td>
		  	          </xsl:if>

	                <xsl:if test="$rowPos &gt; 1">

			                <td align="right">
				                 <xsl:choose>	
		                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[14]='平'">
	  			            	Q
	  			          	</xsl:when>
			                <xsl:otherwise>
			                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			                </xsl:otherwise>
	             		 </xsl:choose>
			  	          	</td>

		  	          </xsl:if>

                </xsl:when>
                <xsl:when test="position() = 16 or position() = 17">
                </xsl:when>

                <xsl:otherwise>
	                <xsl:if test="$rowPos = 1">
		                <td align="left">
											<xsl:value-of select="."/>
		  	          	</td>
		  	          </xsl:if>

	                <xsl:if test="$rowPos &gt; 1">
			                <td align="left">
												<xsl:value-of select="."/>
			  	          	</td>

		  	          </xsl:if>

                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
