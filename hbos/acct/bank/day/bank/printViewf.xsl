<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/bank/day/bank/printViewf.xsl,v 1.1 2012/03/12 01:44:26 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:26 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<thead>
  		<xsl:variable name="SHOWDZXX" select="/root/annex/SHOWDZXX"/>
			<tr noWrap='true' class='mainHead'>
				<xsl:choose>	                  
          <xsl:when test="$SHOWDZXX = '1' or $SHOWDZXX = 1">  
			  		<td style='colspan:15;fontsize:maintitle'>银行日记账</td>
		  			<td style="display:none"></td>
          </xsl:when> 
          <xsl:otherwise>
			  		<td style='colspan:14;fontsize:maintitle'>银行日记账</td>
        	</xsl:otherwise>  
   		 </xsl:choose> 
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true' colspan='2'>xxxx年</td>
		  	<td style="display:none"></td>
		  	<td nowrap='true' rowspan='2' valign='middle'>凭证号</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>摘要</td>
				<xsl:choose>	                  
          <xsl:when test="$SHOWDZXX = '1' or $SHOWDZXX = 1">  
		  			<td nowrap='true' rowspan='2' valign='middle'>对账</td>
          </xsl:when> 
          <xsl:otherwise>
        	</xsl:otherwise>  
   		 </xsl:choose> 
		  	<td nowrap='true' rowspan='2' valign='middle'>汇率</td>
		  	<!--td nowrap='true' rowspan='2' valign='middle'>现金科目</td-->
		  	<td nowrap='true' rowspan='2' valign='middle'>票据类型</td>
		  	<td nowrap='true' rowspan='2' valign='middle'>票据号</td>
		  	<td nowrap='true' colspan='2' valign='middle'>借方金额</td>
		  	<td style="display:none"></td>
		  	<td nowrap='true' colspan='2' valign='middle'>贷方金额</td>
		  	<td style="display:none"></td>
		  	<td nowrap='true' rowspan='2' valign='middle'>方向</td>
		  	<td nowrap='true' colspan='2' valign='middle'>余额</td>
		  	<td style="display:none"></td>
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true'>月</td>
		  	<td nowrap='true'>日</td>
		  	<td style="display:none" nowrap='true'></td>
		  	<td style="display:none" nowrap='true'></td>
				<xsl:choose>	                  
          <xsl:when test="$SHOWDZXX = '1' or $SHOWDZXX = 1">  
		  			<td style="display:none" nowrap='true'></td>
          </xsl:when> 
          <xsl:otherwise>
        	</xsl:otherwise>  
   		 </xsl:choose> 
		  	<td style="display:none" nowrap='true'></td>
		  	<td style="display:none" nowrap='true'></td>
		  	<td style="display:none" nowrap='true'></td>
		  	<td nowrap='true'>本币</td>
		  	<td nowrap='true'>原币</td>
		  	<td nowrap='true'>本币</td>
		  	<td nowrap='true'>原币</td>
		  	<td style="display:none" nowrap='true'></td>
		  	<td nowrap='true'>本币</td>
		  	<td nowrap='true'>原币</td>
      </tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
                <xsl:when test="position() = 1 ">
                </xsl:when>
                <xsl:when test="position() = 4">
									<td><xsl:value-of select="../td[18]"/></td>
                </xsl:when> 
                <xsl:when test="position() = 6">
								<xsl:choose>	                  
				          <xsl:when test="$SHOWDZXX = '1' or $SHOWDZXX = 1">  
											<td align="center"><xsl:value-of select="."/></td>
				          </xsl:when> 
				          <xsl:otherwise>
				        	</xsl:otherwise>  
				   		 </xsl:choose> 
                </xsl:when>
                <xsl:when test="position() = 7">
								<td align="right">
											<xsl:value-of select="."/>
								</td>
                </xsl:when>
                <xsl:when test="position() = 8">
                </xsl:when>
                <xsl:when test="position() = 11 or position() = 12 or position() = 13 or position() = 14">
			            <td align="right">
		                <xsl:if test="text() != '0'">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </xsl:if>
      			      </td>
                </xsl:when>
                <xsl:when test="position() = 16">
			            <td align="right">
		                <xsl:choose>	
		                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[15]='平'">
	  			            	Q
	  			          	</xsl:when>
			                <xsl:otherwise>
			                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			                </xsl:otherwise>
	             		 </xsl:choose>
      			      </td>
                </xsl:when>
                <xsl:when test="position() = 17">
			            <td align="right">
		                <xsl:choose>	
		                	<xsl:when test="(.='' or format-number(.,'#,##0.00')='0.00' or .=null or .='null') and ../td[15]='平'">
	  			            	Q
	  			          	</xsl:when>
			                <xsl:otherwise>
			                 	<xsl:value-of select="format-number(.,'#,##0.00')"/>
			                </xsl:otherwise>
	             		 </xsl:choose>
      			      </td>
                </xsl:when>
                <xsl:when test="position() = 18">
                </xsl:when>
								<xsl:otherwise>
								<td>
										<xsl:value-of select="."/>
								</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
