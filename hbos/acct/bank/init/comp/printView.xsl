<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/bank/init/comp/printView.xsl,v 1.1 2012/03/12 01:44:26 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:26 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>	
				<col style = 'width:100mm'/>
			</colgroup>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:6;fontsize:maintitle'>单位未达账列表</td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
					<td style="display:none"></td>
				</tr>
				<tr noWrap='true' class='mainHead'>
			  	<td nowrap='true'>业务日期</td>
			  	<td nowrap='true'>结算方式</td>
			  	<td nowrap='true'>票据号</td>
			  	<td nowrap='true'>摘要</td>
			  	<td nowrap='true'>借方金额</td>
			  	<td nowrap='true'>贷方金额</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td><xsl:value-of select="translate(.,'|||','')"/></td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
