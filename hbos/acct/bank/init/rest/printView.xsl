<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/acct/bank/init/rest/printView.xsl,v 1.1 2012/03/12 01:44:26 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:44:26 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>
				<col style = 'width:200mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:200mm'/>
				<col style = 'width:100mm'/>
			</colgroup>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:4;fontsize:maintitle'>余额调节表</td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:4;fontsize:subtitle'></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
				</tr>
				<tr noWrap='true' class='mainHead'>
			  	<td nowrap='true'>项目</td>
			  	<td nowrap='true'>金额</td>
			  	<td nowrap='true'>项目</td>
			  	<td nowrap='true'>金额</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position() mod 2 = 0">
								<td align="right">
									<xsl:if test="text() != 0">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:if>
								</td>
								</xsl:when>
								<xsl:otherwise>
								<td>
										<xsl:value-of select="."/>
								</td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
