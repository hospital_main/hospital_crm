<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>银行名称</th>
  			<th nowrap='true'>银行账户</th>
  			<th nowrap='true'>支票类型</th>
  			<th nowrap='true'>登记日期</th>
  			<th nowrap='true'>支票号码</th>
  			<th nowrap='true'>领用人</th>
  			<th nowrap='true'>领用日期</th>
  			<th nowrap='true'>对方单位</th>
  			<th nowrap='true'>金额</th>
  			<th nowrap='true'>制单人</th>
  			<th nowrap='true'>核销人</th>
  			<th nowrap='true'>核销日期</th>
  			<th nowrap='true'>作废人</th>
  			<th nowrap='true'>作废日期</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>              
              <xsl:when test="position()=5">
                <td align="right">
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>							
							<xsl:when test="position()=9">
                <td align="right">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>