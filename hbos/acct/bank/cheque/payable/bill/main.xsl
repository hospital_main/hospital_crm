<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true' width='30'>选择</th>
  			<th nowrap='true' width='60'>票据编码</th>
  			<th nowrap='true' width='60'>票据类型</th>
  			<th nowrap='true' width='60'>收款单位</th>
  			<th nowrap='true' width='60'>承兑银行</th>
  			<th nowrap='true' width='60'>签发日期</th>
  			<th nowrap='true' width='80'>票面利率(%)</th>
  			<th nowrap='true' width='60'>到期日期</th>
  			<th nowrap='true' width='60'>科室名称</th>
  			<th nowrap='true' width='60'>业务员</th>
  			<th nowrap='true' width='60'>票据面值</th>
  			<th nowrap='true' width='60'>结算金额</th>
  			<th nowrap='true' width='30'>余额</th>
  			<!--<th nowrap='true' width='60'>凭证号</th>-->
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:if test="td[1]!='合计' and td[14]!='1' and td[15] !='1'">
	          <td align='center'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
	    			  </input>
	          </td>
    			</xsl:if>
  			  <xsl:if test="td[1]='合计' or td[14]='1' or td[15]='1'">
  			  	<td/>
  			  </xsl:if>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1"> 
              	<xsl:if test="../td[14] !='1' and ../td[15] !='1'">
				          <td><a>
				            <xsl:attribute name="href" >
				    	      	javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:350px;dialogHeight:400px', result)
				  	    		</xsl:attribute><xsl:value-of select="."/></a>
				  	    	</td>  
				  	    </xsl:if>
              	<xsl:if test="../td[14] = '1' or ../td[15]='1'">
              		<td><xsl:value-of select="."/></td>
				  	    </xsl:if>
              </xsl:when> 
              <xsl:when test="position()=4">
                <td align="center">
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:when test="(position()=6 ) and ../td[1]='合计' ">
                <td/>
              </xsl:when>
							<xsl:when test="position()=10 or position()=11 or position()=12">
                <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              
              <xsl:when test="position()=13">
                <td style="display:none"><a href='#'>
                  <xsl:attribute name="onclick" >
    	            javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="."/>&lt;/vouch_id&gt;&lt;edit_mask&gt;+&lt;/edit_mask&gt;');return false;
  	          	  </xsl:attribute><xsl:value-of select="../td[13]"/>
  	          	</a></td>
              </xsl:when>
              <xsl:when test="position()>13">
                <td style="display:none"></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>