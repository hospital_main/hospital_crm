<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
		  <colgroup>		       
				<col style = 'width:70'/>
				<col style = 'width:80'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:70'/>
			</colgroup>  		
    	<thead>
    		<tr noWrap='true' class='mainHead'>
    		<td style='colspan:12;fontsize:maintitle'>应付票据</td>
    			<td style="display:none"></td>
    		  <td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
  	  	</tr>
  	  	<tr noWrap='true' class='mainHead'>
	  			<td nowrap='true'>票据编码</td>
	  			<td nowrap='true'>票据类型</td>
	  			<td nowrap='true'>收款单位</td>
	  			<td nowrap='true'>承兑银行</td>
	  			<td nowrap='true'>签发日期</td>
	  			<td nowrap='true'>票面利率(%)</td>
	  			<td nowrap='true'>到期日期</td>
	  			<td nowrap='true'>科室名称</td>
	  			<td nowrap='true'>业务员</td>
	  			<td nowrap='true'>票据面值</td>
	  			<td nowrap='true'>结算金额</td>
	  			<td nowrap='true'>余额</td>
  	  	</tr>
    	</thead>
    	<tbody>
  	    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=4">
                  <td class="centerText">
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                <xsl:when test="position()=6  and ../td[1]='合计' ">
	                <td/>
	              </xsl:when>
	              <xsl:when test="position()=6  and ../td[1]!='合计' ">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
  							<xsl:when test="position()=10 or position()=11 or position()=12">
                  <td class="numberText"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:when test="position()>12">
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>            
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>