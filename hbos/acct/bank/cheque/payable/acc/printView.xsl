<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  	<root>
  	<!--
		  <colgroup>		       
				<col style = 'width:70'/>
				<col style = 'width:80'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:70'/>
			</colgroup>  	-->	
    	<thead>
    		<tr noWrap='true' class='mainHead'>
    			<td style='colspan:15;fontsize:maintitle'>应付票据</td>
    			<td style="display:none"></td>
    		  <td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<td style="display:none"></td>
    			<!--<td style="display:none"></td>-->
  	  	</tr>
  	  	<tr noWrap='true' class='mainHead'>
	  			<td nowrap='true'>票据编码</td>
	  			<td nowrap='true'>结算序号</td>
	  			<td nowrap='true'>结算方式</td>
	  			<td nowrap='true'>结算日期</td>
	  			<td nowrap='true'>结算金额</td>
	  			<td nowrap='true'>利息</td>
	  			<td nowrap='true'>费用</td>
	  			<td nowrap='true'>票据类型</td>
	  			<td nowrap='true'>收款单位</td>
	  			<td nowrap='true'>承兑银行</td>
	  			<td nowrap='true'>签发日期</td>
	  			<td nowrap='true'>票面利率(%)</td>
	  			<td nowrap='true'>到期日期</td>
	  			<td nowrap='true'>票据面值</td>
	  			<td nowrap='true'>余额</td>
	  			<!--<td nowrap='true'>凭证</td>-->
  	  	</tr>
    	</thead>
    	<tbody>
  	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1"> 
              	<xsl:if test="../td[16] !='1' and ../td[1]!='合计'">
				          <td><a>
				            <xsl:attribute name="href" >
				    	      	javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:600px;dialogHeight:450px', result)
				  	    		</xsl:attribute><xsl:value-of select="."/></a>
				  	    	</td>  
				  	    </xsl:if>
              	<xsl:if test="../td[16]='1' or ../td[1]='合计'">
              		<td><xsl:value-of select="."/></td>
				  	    </xsl:if>
              </xsl:when> 
							<xsl:when test="position()=5 or position()=6 or position()=7 or position()=14 or position()=15">
                <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:when test="position()=12">
              	<xsl:if test=" ../td[1]='合计'">
              		<td/>
              	</xsl:if>
              	<xsl:if test=" ../td[1]!='合计'">
              		<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              	</xsl:if>
              </xsl:when>
              <xsl:when test="position()=16">
              </xsl:when>
              <xsl:when test="position()>16">
                <td style="display:none"></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>