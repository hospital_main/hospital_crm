<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th style='display:none'><input type='checkbox'/></th>
  			<th nowrap='true'>编号</th>
  			<th nowrap='true'>日期</th>
  			<th nowrap='true'>银行名称</th>
  			<th nowrap='true'>账户信息</th>
  			<th nowrap='true'>收款单位</th>
  			<th nowrap='true'>收款银行</th>
  			<th nowrap='true'>收款账号</th>
  			<th nowrap='true'>汇款金额</th>
  			<th nowrap='true'>用途</th>
  			<th nowrap='true'>制单人</th>
  			<th nowrap='true'>作废</th>
  			<th nowrap='true' style="display:none" >凭证</th>
  		</tr>            
  	</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'  style='display:none'>
					<input type='checkbox' TABINDEX='-1'>
						<xsl:attribute name="value" >
							<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
						</xsl:attribute>
					</input>
				</td>
				<xsl:for-each select="td">
					<xsl:choose>            
						<xsl:when test="position()=1">
							<td><a href="#">
								<xsl:attribute name="onclick">
									javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:700px;dialogHeight:330px',result)
								</xsl:attribute>
								<xsl:value-of select="."/></a>
								<xsl:if test="../td[11]='是'"><img src="../../../vouch/query/fei.gif"/></xsl:if></td>
						</xsl:when>  						
						<xsl:when test="position()=8">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
						</xsl:when>
						<xsl:when test="position()=last()">
							<td align="right" style="display:none"  >
								<a href="#"><xsl:attribute name="onclick">openVouchDlg("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;edit_mask&gt;+onlyOne,readOnly&lt;/edit_mask&gt;")</xsl:attribute>
								<xsl:value-of select="."/></a>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>