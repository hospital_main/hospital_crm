<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>	
    	<thead>
    		<tr noWrap='true' class='mainHead'>
				 <td style='colspan:11;fontsize:maintitle'>银行电汇</td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<!--<td style="display:none"></td>-->
     		</tr>
  	  	<tr noWrap='true'>
    		<td nowrap='true'>编号</td>
  			<td nowrap='true'>日期</td>
  			<td nowrap='true'>银行名称</td>
  			<td nowrap='true'>账户信息</td>
  			<td nowrap='true'>收款单位</td>
  			<td nowrap='true'>收款银行</td>
  			<td nowrap='true'>收款账号</td>
  			<td nowrap='true'>汇款金额</td>
  			<td nowrap='true'>用途</td>
  			<td nowrap='true'>制单人</td>
  			<td nowrap='true'>作废</td>
  			<!--<td nowrap='true' style="display:none" >凭证</td>-->
  	  	</tr>
    	</thead>
    	<tbody>
  	    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:for-each select="td">
            <xsl:choose>            
						<xsl:when test="position()=1">
							<td><xsl:value-of select="."/></td>
						</xsl:when>  			
						<xsl:when test="position()=8">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
						</xsl:when>
						<xsl:when test="position()=last()">
							<td align="right" style="display:none">
								<xsl:value-of select="."/>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
  			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>