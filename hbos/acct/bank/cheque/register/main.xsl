<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th nowrap='true'>选择</th>
  			<th nowrap='true'>银行名称</th>
  			<th nowrap='true'>银行账户</th>
  			<th nowrap='true'>支票类型</th>
  			<th nowrap='true'>登记日期</th>
  			<th nowrap='true'>起始号码</th>
  			<th nowrap='true'>结束号码</th>
  			<th nowrap='true'>总张数</th>
  			<th nowrap='true'>未用张数</th>
  			<th nowrap='true'>已领用张数</th>
  			<th nowrap='true'>已核销张数</th>
  			<th nowrap='true'>已作废张数</th>
  			<th nowrap='true'>登记人</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                  <a href="#">
									  <xsl:attribute name="onclick">
									    javascript:openDialog('update.html?load=&lt;cheque_id&gt;<xsl:value-of select="../td[13]"/>&lt;/cheque_id&gt;','dialogWidth:350px;dialogHeight:380px')
									  </xsl:attribute>
                  <xsl:value-of select="."/>
									</a>
                </td>
              </xsl:when>
              <xsl:when test="position()=4">
                <td align="center">
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
							<xsl:when test="position()=5 or position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11">
                <td align="right"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position()>12">
                <td style="display:none">
                  <xsl:value-of select="."/>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>