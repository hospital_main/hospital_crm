<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
		  <colgroup>		       
				
				
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:80'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
				<col style = 'width:70'/>
			</colgroup>  		
    	<thead>
    		<tr noWrap='true' class='mainHead'>
				 <td style='colspan:14;fontsize:maintitle'>支票领用</td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
			  	<td style="display:none"></td>
     		</tr>
  	  	<tr noWrap='true' class='mainHead'>
    			<td nowrap='true'>银行名称</td>
    			<td nowrap='true'>银行账户</td>
    			<td nowrap='true'>支票类型</td>
    			<td nowrap='true'>支票号码</td>
    			<td nowrap='true'>领用日期</td>
    			<td nowrap='true'>领用金额</td>
    			<td nowrap='true'>领用人</td>
    			<td nowrap='true'>收款人</td>
    			<td nowrap='true'>对方单位</td>
    			<td nowrap='true'>制单人</td>
    			<td nowrap='true'>核销人</td>
    			<td nowrap='true'>核销日期</td>
    			<td nowrap='true'>作废人</td>
    			<td nowrap='true'>作废日期</td>
  	  	</tr>
    	</thead>
    	<tbody>
  	    <xsl:for-each select="/root/tbody/tr">
          <tr>
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                </xsl:when>
                <xsl:when test="position()=5">
                  <td class="numberText">
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when>
  							<xsl:when test="position()=7">
                  <td class="numberText">
                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:when>                
                <xsl:when test="position()=15">
                  <td><xsl:value-of select="."/></td>
                </xsl:when>   
                <xsl:when test="position()>15">
                  <td></td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
    			  </xsl:for-each>
    			</tr>
     		</xsl:for-each>
   		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>