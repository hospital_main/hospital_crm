<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
		<colgroup>
			<col style = 'width:250mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
		</colgroup>

    <thead>
    	<tr noWrap='true' class='mainHead'>
    		<td style='colspan:6;fontsize:maintitle'>库存日报</td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	
      </tr>
      <tr noWrap='true' class='mainHead'>
    		<td style='colspan:6;fontsize:subtitle'></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	<td style="display:none"></td>
		  	
      </tr>
      <tr noWrap='true' class='mainHead'>
		  	<td>项目</td>
		  	<td>昨日库存</td>
		  	<td>本日收入</td>
		  	<td>本日支出</td>
		  	<td>本日库存</td>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 6 or position() = 7">
                <td align="center">
                	<xsl:attribute name="style" >display:none;</xsl:attribute><xsl:value-of select="."/>
  	          	</td>
  	          	</xsl:when>
				<xsl:when test="position() = 2 or position() = 3 or position() = 5 or position() = 4">
                    <xsl:if test=".=0">
                      <td align='right'></td>
                    </xsl:if>
                    <xsl:if test=".!=0">
				          		<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                	<td>
                  <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>