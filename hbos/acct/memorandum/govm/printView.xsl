<?xml version="1.0" encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	    <thead>
	    <tr noWrap="true" class="mainHead">
	  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
	  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
  	  </tr>
	  	  <tr noWrap='true' class='mainHead'>
	  			<td>����ƾ֤���</td>
	  			<td>Ԥ��ƾ֤���</td>
				<td>ƾ֤����</td>
				<td>ժҪ</td>
				<td>�跽���</td>
				<td>�������</td>
				<td>����״̬</td>
	      </tr>
      </thead>
	  	<tbody> 
				<xsl:for-each select="/root/tbody/tr">
					<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=5 or position()=6">
								<td style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
			              <xsl:when test="position()=3 or position()=4">
			                <td align="left"><xsl:value-of select="."/></td>
			              </xsl:when>
			              <xsl:when test="position()>7">
			                <td style="display:none">
			                  <xsl:value-of select="."/>
			                </td>
			              </xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
				</xsl:for-each>
			</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>