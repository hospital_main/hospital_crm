<?xml version="1.0" encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	    <thead>
	    <tr noWrap="true" class="mainHead">
	  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
  	  </tr>
  	  <tr noWrap="true" class="mainHead">
	  	  	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
	  	  	<td style="display:none"/>
  	  </tr>
	  	  <tr noWrap='true' class='mainHead'>
	  			<td>财务凭证编号</td>
	  			<td>预算凭证编号</td>
				<td>摘要</td>
				<td>借方金额</td>
				<td>贷方金额</td>
				<td>种类</td>
				<td>号数</td>
				<td>出票日期</td>
				<td>到期日</td>
				<td>票面金额</td>
				<td>交易合同号</td>
				<td>收款人姓名或单位名称</td>
				<td>收款日期</td>
				<td>金额</td>
				<td>票据状态</td>
	      </tr>
      </thead>
	  	<tbody> 
				<xsl:for-each select="/root/tbody/tr">
					<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=4 or position()=5">
								<td style="align:right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							 <xsl:when test="position()=4 or position()=5 or position()=10 or position()=14">
				                	<td align="right"><xsl:value-of select="."/></td>
				              </xsl:when>
			              <xsl:when test="position()>15">
				                <td style="display:none">
				                  <xsl:value-of select="."/>
				                </td>
			              </xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
				</xsl:for-each>
			</tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>