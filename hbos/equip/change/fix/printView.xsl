<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap='true' class='mainHead'>
	        <td noWrap="true">安装单号</td>
					<td noWrap="true">安装日期 </td>
					<td noWrap="true">摘要</td>
					<td noWrap="true">资产名称</td>
					<td noWrap="true">安装部门</td>
					<td noWrap="true">合同编号</td>
					<td noWrap="true">安装单位</td>
					<td noWrap="true">安装费用</td>
					<td noWrap="true">状态</td>
					<td noWrap="true">附带文档</td>
				</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">            
	            <xsl:choose>
			           <xsl:when test="position()=1">
			            <td  noWrap='true' >
		              	<xsl:value-of select="."/>
			            </td>
	              </xsl:when>
	              <xsl:when test="position()=8">
	                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	              </xsl:when>
	              <xsl:otherwise>
	                <td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          	</xsl:choose>
	  			  </xsl:for-each>
	  			  <td>
	            管理
	         	</td>    
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>