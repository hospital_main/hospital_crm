<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN =' '/>
  <xsl:template match="/">
  <root>
    <colgroup>
      <col style = 'width:105mm'/>
      <col style = 'width:105mm'/>
      <col style = 'width:105mm'/>
      <col style = 'width:105mm'/>
      <col style = 'width:105mm'/>
      <col style = 'width:105mm'/>
      <col style = 'width:105mm'/>
      <col style = 'width:105mm'/>
    </colgroup>
    <thead>	
    </thead>
  	<tbody>
   		<tr><td colspan="8">合同设备列表</td></tr>
   		<tr noWrap="true" class="mainHead">
				<td>设备编码</td>
				<td>设备名称</td>
				<td>型号规格</td>
				<td>产地厂家</td>
				<td>单位</td>
				<td>数量</td>
				<td>单价</td>
				<td>交货日期</td>
  		</tr>
   		<xsl:for-each select="/root/tbody/tr[td[1]='e']">
	        <tr>
	          <xsl:for-each select="td[position()!=1]">
	            <td>
	              <xsl:choose>
	                <xsl:when test="position()=1">
	                  <a tabindex='-1'><xsl:value-of select="."/></a>
	                </xsl:when>
	                <xsl:otherwise>
	                  <xsl:value-of select="."/>
	                </xsl:otherwise>
	              </xsl:choose>
	            </td>
	  			  </xsl:for-each>
	  			</tr>
		  </xsl:for-each>
   		<tr><td colspan="8">合同配件列表</td></tr>
   		<tr noWrap="true" class="mainHead">
				<td>配件编码</td>
				<td>配件名称</td>
				<td>型号规格</td>
				<td>产地厂家</td>
				<td>单位</td>
				<td>数量</td>
				<td>单价</td>
				<td>交货日期</td>
			</tr>
   		<xsl:for-each select="/root/tbody/tr[td[1]='i']"> 
		        <tr>
		          <xsl:for-each select="td[position()!=1]">
		            <td>
		              <xsl:choose>
		                <xsl:when test="position()=1">
		                  <a tabindex='-1'><xsl:value-of select="."/></a>
		                </xsl:when>
		                <xsl:otherwise>
		                  <xsl:value-of select="."/>
		                </xsl:otherwise>
		              </xsl:choose>
		            </td>
		  			  </xsl:for-each>
		  			</tr>
		   		</xsl:for-each>
  	</tbody>
  </root>
  </xsl:template>
</xsl:stylesheet>



