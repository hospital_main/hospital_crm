<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	  <root>
	      <colgroup>		       
		      <col style = 'width:120mm'/>
		      <col style = 'width:80mm'/>
		      <col style = 'width:150mm'/>
		      <col style = 'width:150mm'/>
		      <col style = 'width:80mm'/>
		      <col style = 'width:85mm'/>
		      <col style = 'width:85mm'/>
		      <col style = 'width:85mm'/>
		    </colgroup>
	      <thead>
			 	  <tr noWrap='true' class='mainHead'>
						<th noWrap='true' >设备编码</th>
						<th noWrap='true' >设备名称</th>
						<th noWrap='true' >型号规格</th>
						<th noWrap='true' >产地厂家</th>
						<th noWrap='true' >单位</th>
						<th noWrap='true' >单价</th>
						<th noWrap='true' >数量</th>
						<th noWrap='true' >金额</th>
         </tr>
			  </thead>	      
	      <tbody>
		      <xsl:for-each select="/root/tbody/tr">    
		        <tr>
		        	<xsl:for-each select="td">		            
		        		<xsl:if test="position()&lt;=8">	            
		              	<td noWrap='true'  align="left">
		                	<xsl:value-of select="."/>
		                </td>
		            </xsl:if>
		  			  </xsl:for-each>
		  			</tr>		
		   		</xsl:for-each>
	      </tbody>
	  </root>
	</xsl:template>
</xsl:stylesheet>