<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>变动单号</th>
				<th>变动日期</th>
				<th>科室/库房</th>
				<th>卡片号</th>
				<th>资产名称</th>
				<th>规格</th>
				<th>型号</th>
				<th>供应商</th>
				<th>原值</th>
				<th>变动原值</th>
				<th>新原值</th>
				<th>变动累计折旧</th>
				<th>旧使用年限</th>
				<th>新使用年限</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>状态</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:if test='td[1] != "合计"'>
        	<td align='center'  noWrap='true'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
	    			  </input>
	        </td>
	        </xsl:if>
	        <xsl:if test='td[1] = "合计"'>
        	<td></td>
	        </xsl:if>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position()=1">
	                <xsl:if test=". = '合计'">
				        		<td noWrap='true'><xsl:value-of select="."/></td>
				        	</xsl:if>
			        	 	<xsl:if test=". != '合计'">
			            <td  noWrap='true' >
	                <a href='#'>
		                <xsl:attribute name="onclick">
		                	openDialog('update.html?load=&lt;doc_no&gt;<xsl:value-of select="."/>&lt;/doc_no&gt;','dialogWidth:960px;dialogHeight:500px');
		                </xsl:attribute>
		                <xsl:value-of select="."/>
	                </a>
			            </td>
				        	</xsl:if>
                </xsl:when>
                <xsl:when test="position()=11 or position()=12 or position()=10 or position()=9">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
	               <xsl:when test="position()=13 or position() = 14">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,###')"/>
								    </td>
	              </xsl:when>              
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
