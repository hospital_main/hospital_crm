<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum - 1"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
				<td>变动单号</td>
				<td>变动日期</td>
				<td>科室/库房</td>
				<td>卡片号</td>
				<td>资产名称</td>
				<td>规格</td>
				<td>型号</td>
				<td>供应商</td>
				<td>原值</td>
				<td>变动原值</td>
				<td>新原值</td>
				<td>变动累计折旧</td>
				<td>旧使用年限</td>
				<td>新使用年限</td>
				<td>制单人</td>
				<td>审核人</td>
				<td>状态</td>
	  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
              <xsl:choose>
                 <xsl:when test="position()=11 or position()=12 or position()=10 or position()=9">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
	               <xsl:when test="(position()=13 or position() = 14) and . != ''">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,###')"/>
								    </td>
	              </xsl:when> 
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>