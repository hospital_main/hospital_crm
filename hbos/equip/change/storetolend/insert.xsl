<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/equip/change/storetolend/insert.xsl,v 1.1 2012/03/12 01:47:59 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:47:59 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  	  
        <tr noWrap="true" class="mainHead">
          <th style="display:none"><input type="checkbox"/></th>				
  				<th>设备编码</th>
  				<th>设备名称</th>
  				<th>型号规格</th>
  				<th>产地厂家</th>
  				<th>单位</th>
  				<th>单价</th>
  				<th>数量</th>
  				<th>金额</th>
  			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  noWrap='true'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td  noWrap='true' >
                  <!--<a tabindex='-1'>
		                <xsl:attribute name="onclick" >
		                  updateItem('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
		                </xsl:attribute>
                  <xsl:value-of select="."/></a>-->
                   <xsl:value-of select="."/>
                  </td>
                </xsl:when>
                <xsl:when test="position()=6 or position()=8">
                  <td align="right" class="moneyCol" ><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:when test="position()=7">
                  <td align="right"  ><xsl:value-of select="."/></td>
                </xsl:when>
                <!--<xsl:when test="position()=9">
                  <td>
                  <a href="#">                  
                    <xsl:attribute name="onclick" >
                      viewDetail('<xsl:value-of select="../td[11]"/>', '<xsl:value-of select="../td[1]"/>', '<xsl:value-of select="../td[7]"/>', 
                      'equip/main.html?&lt;root&gt;&lt;equi_back_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/equi_back_no&gt;&lt;state&gt;<xsl:value-of select="../td[position()=7]"/>&lt;/state&gt;&lt;/root&gt;');
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                  </a>
                  </td>
                </xsl:when>
                <xsl:when test="position()=10">
                  <td>
                  <a href="#">                  
                    <xsl:attribute name="onclick" >
                      viewFittings('<xsl:value-of select="../td[11]"/>', '<xsl:value-of select="../td[1]"/>','appe/main.html?&lt;root&gt;<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;/root&gt;');
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                  </a>
                  </td>
                </xsl:when>-->
                
                <xsl:when test="position()=9 or position()=10 or position()=11 or position()=12 or position()=13">
                  
                </xsl:when>
                <xsl:when test="position()=11">
                </xsl:when>
                <xsl:otherwise>
                  <td>
                  <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
