
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th width='25'><input type='checkbox'/></th>
				<th>资产编码</th>
				<th>资产名称</th>
				<th>资产卡片号</th>
				<th>自定义卡片号</th>
				<th>规格</th>
				<th>型号</th>
				<th>产地厂家</th>
				<th>计量单位</th>
				<th>卡片数量</th>
				<th>资产原值</th>
				<th>资产条形码</th>
				<th>资产序列号</th>
				<th>资产性质</th>
				<th>入库日期</th>
				<th>购买日期</th> 
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  noWrap='true'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' >
	                  <a tabindex='-1'><xsl:value-of select="."/></a>
			            </td>
                </xsl:when>
               
                <xsl:when test="position()=9">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0')"/>
								    </td>
	              </xsl:when>
	              <xsl:when test="position()=10">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
