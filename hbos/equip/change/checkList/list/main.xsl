<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>业务类型</th>
				<th>盘点单据号</th>
				<th>单据日期</th>
				<th>卡片号</th>
				<th>科室</th>
				<th>仓库</th>	
				<th>资产编码</th>
				<th>资产名称</th>	
				<th>规格</th>
				<th>型号</th>
				<th>品牌</th>
				<th>国家</th>
				<th>盈亏数量</th>
				<th>单价</th>
				<th>盈亏金额</th>
				<th>说明</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>               
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=15 or position() = 13 or position() = 14">
			            <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>  	
	</xsl:template>
</xsl:stylesheet>
