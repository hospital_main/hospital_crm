<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
  
    <thead>
    	<tr>
   		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">15</xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
    <tr noWrap='true' class='mainHead'>
    	<td>业务类型</td>
				<td>盘点单据号</td>
				<td>单据日期</td>
				<td>卡片号</td>
				<td>科室</td>
				<td>仓库</td>	
				<td>资产编码</td>
				<td>资产名称</td>	
				<td>规格</td>
				<td>型号</td>
				<td>品牌</td>
				<td>国家</td>
				<td>盈亏数量</td>
				<td>单价</td>
				<td>盈亏金额</td>
				<td>说明</td>
    </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>               
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=15 or position() = 13 or position() = 14">
			            <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each> 
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>