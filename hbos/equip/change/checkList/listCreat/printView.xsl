<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
		<colgroup>
			<col style = 'width:150mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<!--<col style = 'width:100mm'/>-->
		</colgroup>
  
    <thead>
    	<tr>
   		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">14</xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<!--<td style='display:none'/>-->
		</tr>
			<tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="colspan:colcount"/>
  	  <td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<!--<td style='display:none'/>-->
  	  </tr>
    <tr noWrap='true' class='mainHead'>
    	<td nowrap='true' valign='middle'>科室名称</td>
	  	<td nowrap='true' valign='middle'>资产卡片号</td>
	  	<!--<td nowrap='true' valign='middle'>附件编码</td>-->
	  	<td nowrap='true' valign='middle'>资产编码</td>
	  	<td nowrap='true' valign='middle'>资产名称</td>
	  	<td nowrap='true' valign='middle'>规格</td>
	  	<td nowrap='true' valign='middle'>型号</td>
	  	<td nowrap='true' valign='middle'>产地厂家</td>
	  	<td nowrap='true' valign='middle'>计量单位</td>
	  	<td nowrap='true' valign='middle'>单价</td>
	  	<td nowrap='true' valign='middle'>账面数量</td>
	  	<td nowrap='true' valign='middle'>盘点数量</td>
	  	<td nowrap='true' valign='middle'>入库日期</td>
	  	<td nowrap='true' valign='middle'>放置地点</td>
    </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 13">
	                <td align="right">
	                	<xsl:value-of select="."/>
	  	          	</td>
  	          	</xsl:when>
  	          	<xsl:when test="position() = 9">
	                <td align="right">
	                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	  	          	</td>
  	          	</xsl:when>
  	          	<xsl:when test="position() = 10 or position() = 11">
	                <td align="right">
	                	<xsl:value-of select="format-number(.,'#,##0')"/>
	  	          	</td>
  	          	</xsl:when>
                <xsl:otherwise>                	
	                <td align="left">
	                	<xsl:value-of select="."/>
	                	<!--xsl:value-of select="substring-after(.,'|||')"/-->
	  	          	</td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    <tfoot>
 			<tr noWrap='true'>
        <td align="left" style='fontsize:foot;colspan:colcount'></td>
        <xsl:for-each select="/root/tbody/tr[1]/td[position() != 1]">
        	<td style="display:none"></td>
        </xsl:for-each>	
  		</tr>
 		</tfoot>
    </root>
  </xsl:template>
</xsl:stylesheet>