<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
			<xsl:for-each select="/root/t2head/tr">
				<xsl:variable name="curRow" select="position()" />
				<tr noWrap="true" class="mainHead">
					<xsl:for-each select="th">
						<xsl:variable name="curcode" select="."/>
          	<xsl:variable name="curCol" select="position()" />	
          	<xsl:variable name="colspan" select="count(/root/t2head/tr[1]/th[.=$curcode])" />
          	<xsl:variable name="rowspan" select="count(/root/t2head/tr[th[$curCol] = $curcode])" />
						<xsl:choose>
        			<xsl:when test="$curRow = 1 and .!=string(../../tr[$curRow - 1]/th[$curCol]) and .!=string(../th[$curCol - 1])">
	          			<th colspan="{$colspan}" rowspan="{$rowspan}" nowrap='true' ><xsl:value-of select="."/></th>
	          	</xsl:when> 
	          	<xsl:when test="$curRow = 2 and position() &gt; 4">
	          			<th nowrap='true' ><xsl:value-of select="."/></th>
	          	</xsl:when>          		         	
          		<xsl:otherwise>
            	</xsl:otherwise>
            </xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
	</thead>
    
    <tbody>
	    <xsl:for-each select="/root/tbody/tr">
		    <tr>
			    <xsl:for-each select="td"> 
					<xsl:choose>
						<xsl:when test="position() &lt; 4">
						 <td align='left'>	
							<xsl:value-of select="."/>
						 </td>
						</xsl:when>
						<xsl:otherwise>
						 <td align='right'>	
							<xsl:value-of select="format-number(.,'#,##0.00')"/>
						 </td>
						</xsl:otherwise>
					</xsl:choose>
			    </xsl:for-each>
		    </tr>
	    </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

