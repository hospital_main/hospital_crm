<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>  
			<tr noWrap="true" class="mainHead">
				<th rowspan="2">使用科室</th>
				<th colspan="3">房屋</th>
				<th colspan="3">一般设备</th>
				<th colspan="3">专业设备</th>
				<th colspan="3">其他</th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th>原值</th>
				<th>月折旧</th>
				<th>累计折旧</th>
				<th>原值</th>
				<th>月折旧</th>
				<th>累计折旧</th>
				<th>原值</th>
				<th>月折旧</th>
				<th>累计折旧</th>
				<th>原值</th>
				<th>月折旧</th>
				<th>累计折旧</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				 <xsl:choose>
				    <xsl:when test=" substring-before(./td[1],'合计')!='' or substring-before(./td[1],'小计')!=''">
             <tr>
              <xsl:for-each select="td">
							<xsl:choose>
								
								<xsl:when test="position()=1">
									  
								  <td align='left'>	
									 <span style="color:black;font-weight:bold"><xsl:value-of select="."/> </span>
								 </td>
								</xsl:when>
								<xsl:otherwise>
								 <td align='right'>	
									 <span style="color:black;font-weight:bold"><xsl:value-of select="format-number(.,'#,##0.00')"/> </span>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
				</xsl:when>
				    <xsl:when test=" substring-before(./td[1],'合计')=''">
             <tr>
              <xsl:for-each select="td">
							<xsl:choose>
								
								<xsl:when test="position()=1">
									  
								  <td align='left'>	
									 <xsl:value-of select="."/>
								 </td>
								</xsl:when>
								<xsl:otherwise>
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
				</xsl:when>
			 </xsl:choose>        
			  
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
