<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td rowspan="2">类别编码</td>
				<td rowspan="2">类别名称</td>
				<td colspan="4">原值</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan="4">累计折旧</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan="2">净值</td>
				<td style="display:none"></td>
			</tr>
			<tr noWrap="true" class="mainHead">
			  <td style="display:none"></td>
			  <td style="display:none"></td>
				<td>期初</td>
				<td>借方</td>
				<td>贷方</td>
				<td>余额</td>
	 			<td>期初</td>
				<td>借方</td>
				<td>贷方</td>
				<td>余额</td>
				<td>期初</td>
				<td>期末</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1 or position()=2">
								  <td align='left'>	
									<xsl:value-of select="."/>
								 </td>
								</xsl:when>
								<xsl:otherwise>
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		 <tfoot>
 		    <tr noWrap='true'>
		      <td style='fontsize:foot;colspan:12;align:left;border:none'></td>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
  	  	</tr>		
    	</tfoot>
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>