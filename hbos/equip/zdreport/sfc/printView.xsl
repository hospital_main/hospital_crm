<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		<tr noWrap="true" class="mainHead">
				<td rowspan="2">资产编码</td>
				<td rowspan="2">资产名称</td>
				<td colspan='2'>上月结存</td>
				<td style="display:none"></td>
				<td colspan='2'>本期购入</td>
				<td style="display:none"></td>
				<td colspan='2'>本期支出</td>
				<td style="display:none"></td>
				<td colspan='2'>期末结存</td>
				<td style="display:none"></td>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td>数量</td>
				<td>金额</td>
				<td>数量</td>
				<td>金额</td>
				<td>数量</td>
				<td>金额</td>
				<td>数量</td>
				<td>金额</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=1 or position()=2">
									 <td align='left'>	
										<xsl:value-of select="."/>
									 </td>
									</xsl:when>
									<xsl:when test="position()=3 or position()=5 or position() = 7 or position() = 9">
									 <td align='right'>	
										<xsl:value-of select="format-number(.,'#,##0')"/>
									 </td>
									</xsl:when>
									<xsl:when test="position()=4 or position()=6 or position() = 8 or position() = 10">
									 <td align='right'>	
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									 </td>
									</xsl:when>
									<xsl:otherwise>
									 <td align='right'>	
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									 </td>
									</xsl:otherwise>
								</xsl:choose>
						</xsl:for-each>
					</tr>
			</xsl:for-each>
		</tbody>
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>