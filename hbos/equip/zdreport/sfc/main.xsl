<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>  
			<tr noWrap="true" class="mainHead">
				<th rowspan="2">资产编码</th>
				<th rowspan="2">资产名称</th>
				<th colspan='2'>上月结存</th>
				<th colspan='2'>本期购入</th>
				<th colspan='2'>本期支出</th>
				<th colspan='2'>期末结存</th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th>数量</th>
				<th>金额</th>
				<th>数量</th>
				<th>金额</th>
				<th>数量</th>
				<th>金额</th>
				<th>数量</th>
				<th>金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td>
								<xsl:choose>
									<xsl:when test="position() = 3 or position() = 5 or position() = 7 or position() = 9">
									 <span style="text-align:right;width:100%;"><xsl:value-of select="format-number(.,'#,##0')"/></span>
									</xsl:when>
									<xsl:when test="position() = 4 or position() = 6 or position() = 8 or position() = 10">
									 <span style="text-align:right;width:100%;"><xsl:value-of select="format-number(.,'#,##0.00')"/></span>
									</xsl:when>
									<xsl:otherwise>
									 <xsl:value-of select="."/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</xsl:for-each>
					</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
