<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th rowspan="2" valign="middle">日期</th>
        <th rowspan="2" valign="middle" >卡片号</th>
        <th rowspan="2" valign="middle" >资产名称</th>
        <th rowspan="2" valign="middle" >摘要</th>
        <th rowspan="2" valign="middle" >部门</th>
		    <th colspan="3">原值</th>		    
		    <th rowspan="2" valign="middle" >数量</th>
  		</tr>
  		<tr noWrap="true" class="mainHead"> 
		    <th valign="middle">借方</th>
		    <th valign="middle">贷方</th>
		    <th valign="middle">余额</th>		    		    
		  </tr>  		
  	</thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      <xsl:variable name='CurTrPos' select='position()'/>
        <tr>          
          <xsl:for-each select="td">
          	<xsl:choose>							
              <xsl:when test="position()=6 or position()=7">
                <td align='right'>
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
              </xsl:when>
              <xsl:when test="position()= 8 or position() = 9">
              	<td align='right'>
              		<xsl:if test="position()= 8 and . != ''">
            				<xsl:value-of select="format-number(.,'#,##0.00')"/>
          				</xsl:if> 
          				<xsl:if test="position()= 9 and . != ''">
            				<xsl:value-of select="format-number(.,'#,##0')"/>
          				</xsl:if>                                 
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td align='left'><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>