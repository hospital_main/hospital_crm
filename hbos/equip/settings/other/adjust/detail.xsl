<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <thead>
            <tr noWrap='true' class='mainHead'>
                <th nowrap='true'>资产编号</th>
                <th nowrap='true'>资产名称</th>
                <th nowrap='true'>卡片编号</th>
                <th nowrap='true'>资产类别（原）</th>
                <th nowrap='true'>资产类别（新）</th>
                <th nowrap='true'>财务分类（原）</th>
                <th nowrap='true'>财务分类（新）</th>
                <th nowrap='true'>调整单号</th>
                <th nowrap='true'>单据状态</th>
                <th nowrap='true'>制单人</th>
                <th nowrap='true'>制单日期</th>
            </tr>
        </thead>
        <tbody>
            <xsl:for-each select="/root/tbody/tr">
                <tr>

                    <xsl:for-each select="td">
                        <xsl:choose>
                            <xsl:when test="position()=1">
                                <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
                            </xsl:when>
                            <xsl:otherwise>
                                <td><xsl:value-of select="."/></td>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                </tr>
            </xsl:for-each>
        </tbody>
    </xsl:template>
</xsl:stylesheet>

