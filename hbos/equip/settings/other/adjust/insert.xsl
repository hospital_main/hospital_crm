<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
                            <th><input type='checkbox'/></th>
         
					<th nowrap='true'>资产编号</th>
					<th nowrap='true'>资产名称</th>
					<!--<th nowrap='true'>卡片编号</th>-->
            <th nowrap='true'  style="display:none">资产类别编码（原）</th>
					<th nowrap='true'>资产类别（原）</th>
                    <th nowrap='true'  style="display:none">资产类别编码（新）</th>
					<th nowrap='true'>资产类别（新）</th>
            <th nowrap='true' style="display:none">资产类别编码（原）</th>
					<th nowrap='true'>财务分类（原）</th>
                    <th nowrap='true' style="display:none">资产类别编码（新）</th>
                    <th nowrap='true'>财务分类（新）</th>
     		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                    <!--<a href="#">-->
                        <!--<xsl:attribute name="onclick" >-->
                            <!--javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")-->
                        <!--</xsl:attribute><xsl:value-of select="."/></a>-->
                    <a tabindex='-1'><xsl:value-of select="."/></a>
                </td>
              </xsl:when>
                <xsl:when test="position()=3  or position()=5 or position()=7 or position()=9">
                    <td  style="display:none" ><xsl:value-of select="."/></td>
                </xsl:when>
              <xsl:otherwise>

                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

