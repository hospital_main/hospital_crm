<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
                            <th><input type='checkbox'/></th>
         
					<th nowrap='true'>资产编号</th>
					<th nowrap='true'>资产名称</th>
					<!--<th nowrap='true'>卡片编号</th>-->
            <th nowrap='true' style="display:none">资产类别编码(</th>
					<th nowrap='true'>资产类别</th>
            <th nowrap='true' style="display:none">财务分类编码</th>
					<th nowrap='true'>财务分类</th>
            <th nowrap='true'>是否调整</th>
            <th nowrap='true'>调整单号</th>
     		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
                <xsl:attribute name="value" >
                    <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
                </xsl:attribute>
                <xsl:if test="td[7] = '1'">
                    <xsl:attribute name="disabled">	true</xsl:attribute>
                </xsl:if>
    			  </input>
          </td>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
              </xsl:when>
                <xsl:when test="position()=3 or position()=5">
                    <td  style="display:none"><xsl:value-of select="."/></td>
            </xsl:when>
                <xsl:when test="position()=7">
                    <xsl:if test="../td[7]='0'"><td align="right"> 否</td></xsl:if>
                    <xsl:if test="../td[7]='1'"><td align="right"> 是</td></xsl:if>
                </xsl:when>
                <!--    <xsl:when test="position()=6">
                    <td align="right"><xsl:value-of select="format-number(.,'#,##0')"/></td>
                  </xsl:when>
                  <xsl:when test="position()=7">
                    <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                  </xsl:when>  -->
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

