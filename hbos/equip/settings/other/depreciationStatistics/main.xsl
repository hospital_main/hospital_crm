<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
        <xsl:variable name="dept_td3" select="/root/tbody/tr[1]/td[3]"/>
  		<tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
            <th nowrap='true'>财务分类名称</th>
            <th nowrap='true'>资金来源</th>
            <xsl:if test="$dept_td3 = ''">
            <th nowrap='true' style="display:none">所在部门</th>
            </xsl:if>
            <xsl:if test="$dept_td3!= ''">
                <th nowrap='true'>所在部门</th>
            </xsl:if>
            <th nowrap='true'>资产原值</th>
            <th nowrap='true'>补提折旧金额</th>
            <th nowrap='true'>累计计提(前)</th>
            <th nowrap='true'>累计计提(后)</th>
        </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
              </xsl:when>
                <xsl:when test="position()=3">
                    <xsl:if test="$dept_td3 = ''">
                    <td style="display:none;"><a tabindex='-1'><xsl:value-of select="."/></a></td>
                    </xsl:if>
                    <xsl:if test="$dept_td3!= ''">
                        <td ><a tabindex='-1'><xsl:value-of select="."/></a></td>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="position() = 4 or position() = 5 or position() = 6 or position() = 7">
                    <td class='numberText' nowrap='true'>
                        <xsl:value-of select="format-number(.,'#,##0.00')"/>
                    </td>
                </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

