<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead' style="width:500px">
        <th style='display:none'><input type='checkbox'/></th>
	                <th nowrap='true' >卡片编号</th>
									<th nowrap='true' >资产编码</th>
									<th nowrap='true' >资产名称</th>
                                    <th nowrap='true' >是否补提</th>
                                    <th nowrap='true' >原有折旧年限</th>
                                    <th nowrap='true' >现有折旧年限</th>

          		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
                <xsl:if test="td[4]=1">
                    <xsl:attribute name="checked">true</xsl:attribute>
                </xsl:if>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/><xsl:value-of select="td[3]"/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td width="130"><a tabindex='-1'><xsl:value-of select="."/></a></td>
              </xsl:when>
                <xsl:when test="position()=4">
                    <xsl:if test=". = '' or . = 0">
                        <td width="130">不补提</td>
                    </xsl:if>
                    <xsl:if test=". != '' and . = 1">
                        <td width="130">补提</td>
                    </xsl:if>

                </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

