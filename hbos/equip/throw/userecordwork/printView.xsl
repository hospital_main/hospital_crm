<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap='true' class='mainHead'>
	  			<td>资产卡片</td>
	  			<td>资产名称</td>
	  			<td>使用日期</td>
	  			<td>病人编码</td>
	  			<td>病人姓名</td>
	  			<td>物资材料</td>
	  			<td>材料名称</td>
	  			<td>材料规格型号</td>
	  			<td>耗材数量</td>
	  			<td>价格</td>
	  			<td>耗材金额</td>
	  			<td>资产类别</td>
	  			<td>资产规格</td>
	  			<td>资产型号</td>
	  			<td>所属科室</td>
     		</tr>
  		</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">            
	            <xsl:choose>
	              <xsl:when test="position()=1">
	                <td><xsl:value-of select="."/></td>
	              </xsl:when>
	              <xsl:when test="position()=9">
	                <td class='numberText' noWrap='true' align='right'>
	              	  <xsl:value-of select="format-number(.,'#,##0')"/>
	                </td>
	              </xsl:when>
	              <xsl:when test="position()=10 or position()=11">
	                <td class='numberText' noWrap='true' align='right'>
	              	  <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </td>
	              </xsl:when>
	              <xsl:otherwise>
	                <td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          	</xsl:choose>          
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>