<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
                            <th><input type='checkbox'/></th>
         
					<th nowrap='true'>资产卡片号</th>
					<th nowrap='true'>资产名称</th>
					<th nowrap='true'>原值</th>
					<th nowrap='true'>转正方式</th>					
					<th nowrap='true'>转正数量</th>					
					<!--th nowrap='true'>使用日期</th-->
					<!--th nowrap='true'>材料编码</th-->	
					<th nowrap='true'>材料编码</th>
					<th nowrap='true'>材料名称</th>
					<th nowrap='true'>耗材数量</th>
					<th nowrap='true'>耗材金额</th>
					<th nowrap='true'>资产类别</th>
					<th nowrap='true'>规格</th>					
					<th nowrap='true'>型号</th>					
					<th nowrap='true'>所属科室</th>
					<th nowrap='true'>存放仓库</th>					
					<th nowrap='true'>供应商</th>	
									
					<th nowrap='true'>购入日期</th>
     		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
              </xsl:when>  
              <xsl:when test="position()=5">
                <td align="right"><xsl:value-of select="format-number(.,'#,##0')"/></td>
              </xsl:when>  
              <xsl:when test="position()=8">
                <td align="right"><xsl:value-of select="format-number(.,'#,##0')"/></td>
              </xsl:when>  
              <xsl:when test="position()=9">
                <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>  
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

