<%@ page contentType="text/html; charset=gb2312" language="java" import="com.viewhigh.core.web.OperateHelper,com.viewhigh.core.dao.*" errorPage="" %>
<html xmlns:vh>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<link href="" rel="stylesheet" type="text/css">
		<script language='javascript' id='scriptID'></script>
		<script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>
		<script language='javascript'>
			var fun_result = "";
			var attCode = "";
			var typeFlag = "0";
			var atDeptTemp = "";
			var storeCodeTemp = "";

			function initBudgno() {
                budg_no.load = 'equi_budg_proj_dict';
                if(top.cardNo!="" && top.cardNo!='undefined') {
                    var p=getValueArrayBySql("equi_proj_year",'<cardNo>'+ top.cardNo +'</cardNo>');
                    if(p==null)
                        return;
                    budg_no.para = '<year>'+ p[0] +'</year>';
                } else {
                    budg_no.para = '<year>'+ getAcctYear() +'</year>';
				}
                budg_no.refresh;
            }

		  function init(){
		  	//调整select组件浮动层位置
		  	 $('.inputSelect').each(
				   	function(){
				   		$(this).prev().prev().remove()
				   		$(this).prev().css({'margin-left':'9px','margin-top':'3px','height':'15px'})
				   	}
			   )
		    //comp_code.value=getCompCode();
		   // copy_code.value=getCopyCode();
		    in_date.value=getLoginDate();
		    amount.value=1;
		    loadData();

		    if (parent.data_source=='firstpage'){

					parent.setdetaildata();

				}

		    changeEqui2();

		   // changeDepre(is_depre.value);
		    changeMeasure(is_measure.value);
		    changeType_before();
		    changeType();
		    typeFlag='1';
		    amount.disabled = true;

              initBudgno();
		    if (equi_arch_no.value != "") {
		    	equi_primmoney.disabled = true;
		    	install_money.disabled = true;
		    	trans_money.disabled = true;
		    	tex_money.disabled = true;

                if(is_autowrite.value =="1"){
                    equi_card_vouch_no.disabled = true;
                }
		      setTempValue();
		    }
		    //changeEqui();

		    changeAlready_depre();
		    if(create_user.value==''){
		     var a =getValuePairBySql("equipTransfer_userName","<user_id>"+getUserID()+"</user_id>");
  		if(a!=null)
		  	{
		  	 var maker=a[0];
		  	 if(create_user.value==''){
		  	 create_user.value = maker;
		  	}
		    }
			 }

			 if(is_app.value=="0"){


			  			main_arch_no.disabled=true;

			  		}
			  		else{
			  			main_arch_no.disabled=false;
			  			}
		  }
      function round(v,e){

		var t=1;

		for(;e>0;t*=10,e--);

		for(;e<0;t/=10,e++);

		return Math.round(v*t)/t;

		}


			function changeEqui2(){
			  if (equi_code.value!=''){
				  subBody.load = "equi_code_info";
			    subBody.para = "<card>"+equi_code.value+"</card>";
			    subBody.post();
			    var d = new Array();
			    d=subBody.getOneDim();
			    if(d!=null){
		    		equi_name.value=d[0];
		    	}
		    	if (equi_kind_code) {
		    	  equi_kind_code.setEnabled(false);
		    	}
		    	if (equi_kind_type) {
		    	  equi_kind_type.setEnabled(false);
		    	}
			  }
			}

		  function loadData(){
		  	if(subBody.hideMsg){

		  	}else{
		  		window.setTimeout(loadData,10);
		  		return;
		  	}

		    if(top.cardNo!="" && top.cardNo!='undefined'){

		      subBody.load = "equi_card_detail";
		      subBody.para = "<card>"+top.cardNo+"</card>";
		      subBody.post();
		      var d = subBody.getOneDim();

		      loadControl(d);
		    }else{
		    	equi_kind_type.setValue(top.cardType);

        }
		  }

		  function loadControl(values)
		  {

		  	var minCount = 0;
		  	var names = document.getElementById("hiddenCName").value.split(';');

		  	if(values.length>names.length-1)
		  		minCount = names.length-1;
		  	else
		  		minCount = values.length;

		  	for(var i=0;i < minCount; i++)
		  	{
		  		setControlValue(names[i],values[i]);
		  	}
		  }

		  function setControlValue(n,v)
		  {
      	if(document.getElementsByName(n).length==0)
      		return;

		  	var obj = document.getElementsByName(n)[0];
      	if(obj.className == "inputSelectS")
      	{
      		obj.setValue(v);
      	}
      	else if(obj.className == "inputSelect")
      	{
      		obj.setValue(v);
      	}
      	else if(obj.className == "inputCalendar")
      	{
      		if(v.indexOf(':') > 0){
      			v = v.substr(0,v.indexOf(' '));
      		}

      		obj.value = v;
      	}
      	else
    		{
	    		if(obj.tagName == "INPUT")
	    				obj.value = v;
	    		if(obj.tagName == "SELECT")
	    		{
	    			setSelectValue(obj,v);
	    		}
    		}
		  }

		  function setSelectValue(obj,v)
		  {
  			for (var i=0; i< obj.options.length; i++)
  			{
  				if(obj.options(i).value == v)
  					obj.selectedIndex = i;
  			}
		  }


			function changeis_code(){
				subBody.load="equi_card_get_costMonth_paraValue";
				subBody.para="";
				subBody.post();
				var d = subBody.getOneDim();
				if(d!=null){

				   if(d[0]==0)
				   {
				   	 if(is_cost.value=='1'){
								if(equi_primmoney.value==''){
									alert('请输入设备单价!');

								}
								if(in_date.value==''){
									alert('请输入入库日期!');
									return;
								}

								changePmoney();
							}
							else{
								cost_month.value='';
							}

						cost_month.disabled=true;
						cost_month.style.backgroundColor='#EDEDED';
				   }
				   else
				   {
				     if(equi_arch_no.value=="")
				     {
				     	 if(is_cost.value=='1'){

												if(in_date.value==''){
													alert('请输入入库日期!');
													return;
												}
												var p = getValuePairBySql("equipCard_getCostMonth","<equi_code>"+equi_code.value+"</equi_code>");

				  	            if(p!=null)
													 {
													  	var t=p[0].split(',');

												      cost_month.value=t[0];
												      after_cost_month.value=t[1];

											     }
											  if(in_date.value!=""){
																subBody.load="equi_get_acctmonth"
																subBody.para="<a>" + in_date.value +"</a>";
																subBody.post();
																var d = subBody.getOneDim();
																try{cost_start_ym.value=d[0]}catch(e){alert('没有对应会计期间设置!');return;}
												  }

													 	  cost_month.disabled=false;
												      cost_month.style.backgroundColor='';

											}
											else
											{
												  		cost_month.value='';
															cost_month.disabled=true;
															cost_month.style.backgroundColor='#EDEDED';
											}
				     	}
				     else
				     	{
						   	 subBody.load="equi_card_state"
								 subBody.para="<a>" + equi_arch_no.value +"</a>";
								 subBody.post();
								 var aa = subBody.getOneDim();
								 if(aa!=null){

								 	subBody.load="equi_card_is_rework"
								  subBody.para="";
								  subBody.post();
								  var bb = subBody.getOneDim();

								  if(bb!=null){
										if(aa[0]<2||(aa[0]>2 && bb[0]==1))
											{
										   		if(is_cost.value=='1'){

														if(in_date.value==''){
															alert('请输入入库日期!');
															return;
														}
														var p = getValuePairBySql("equipCard_getCostMonth","<equi_code>"+equi_code.value+"</equi_code>");

						  	            if(p!=null)
															 {
															  	var t=p[0].split(',');

														      cost_month.value=t[0];
														      after_cost_month.value=t[1];

													     }
													  if(in_date.value!=""){
																		subBody.load="equi_get_acctmonth"
																		subBody.para="<a>" + in_date.value +"</a>";
																		subBody.post();
																		var d = subBody.getOneDim();
																		try{cost_start_ym.value=d[0]}catch(e){alert('没有对应会计期间设置!');return;}
														  }

															 	  cost_month.disabled=false;
														      cost_month.style.backgroundColor='';

													}
													else
													{
														  		cost_month.value='';
																	cost_month.disabled=true;
																	cost_month.style.backgroundColor='#EDEDED';
													}
						          }
									  }
									}
							}
				   }
		}



			}
			function changePmoney(){
				changePrim();
					/*
					if(is_cost.value=='1'){
					if(equi_primmoney.value!=''){

						subBody.load="equi_card_cost_month";
						subBody.para="<a>" + equi_primmoney.value + "</a>";
						subBody.post();
						var d = subBody.getOneDim();
						//alert(d);
						if(d[0]=='error_month'){
								alert('请设置相应资产的分摊规则！');
								return false;
							}
						try{cost_month.value=d[0]; }catch(e){alert(cost_month.value=d[0]);}
					}
					if(in_date.value!=""){
						subBody.load="equi_get_acctmonth"
						subBody.para="<a>" + in_date.value +"</a>";
						subBody.post();
						var d = subBody.getOneDim();

						try{cost_start_ym.value=d[0]}catch(e){alert('没有对应会计期间设置!');return;}
					}
				}
				*/
			}
			function changeWork(){

		    if (work_num_ini){

		    	work_num_sum.value=work_num_ini.value==''?0:work_num_ini.value;
		    }
		     	  }
			function changePrim(){
		  	var equi_fj=0
		    var equi_amount=0
		    var equi_price=0
		    var equi_install=0
		    var equi_trans=0
		    var equi_tex=0
		    var doc_no=''

		    if (typeof(parent.equi_in_doc_no)=='object')
		    	doc_no=parent.equi_in_doc_no.value
		    else
		    	doc_no=parent.equi_in_doc_no
				/*
		  	if(equi_arch_no.value != "") {
			  	subBody.load="equipChangeIn_equi_prim";
					subBody.para="<a>"+doc_no+"</a><b>"+equi_arch_no.value+"</b>";
			    subBody.post();
				  var prim = subBody.getOneDim();
				  if (prim!=null)
			    	equi_fj=prim[0];
				}*/

		    if (amount){
		    	equi_amount=amount.value==''?0:amount.value;
		    }
		    if (prim_money){
		    	equi_price=equi_primmoney.value==''?0:equi_primmoney.value
		    	if (equi_primmoney.value=='noUse')
		    	equi_price=0
		    }
		    if (install_money){
		    	equi_install=install_money.value==''?0:install_money.value
		    	if (install_money.value=='noUse')
		    	equi_install=0
		    	}
		    if (trans_money){
		    	equi_trans=trans_money.value==''?0:trans_money.value
		    	if (trans_money.value=='noUse')
		    	equi_trans=0
		    	}
		    if (tex_money){
		    	equi_tex=tex_money.value==''?0:tex_money.value
		    	if (tex_money.value=='noUse')
		    	equi_tex=0
		    	}

		  	prim_money.value=parseFloat(equi_fj)+parseFloat(equi_amount)*parseFloat(equi_price)+parseFloat(equi_install)+parseFloat(equi_trans)+parseFloat(equi_tex)
		  	equi_card_money.value = prim_money.value;
		    var already=already_depre.value
		    var prim=prim_money.value
		    var fo_odd_money=fore_odd_money.value

		    cur_money.value=parseFloat(prim.replace('','0'))-parseFloat(already.replace('','0'))
		  }
		  /**引用页面提交后,调用方法*/
		  function assemble_detail(){
		  	fun_result = "";
        if(equi_in_type.value=="1" && at_dept.value==""){
        	alert("请填写使用部门！");
          return "ERROR";
        }
        if(equi_in_type.value=="0" && store_code.value==""){
        	alert("请填写仓库！");
          return "ERROR";
        }
        if(is_measure.value=="1" && measure_value.value==""){
        	alert("请填写计量频率！");
          return "ERROR";
        }
        // is_depre.value=="1"
        if(use_years_defined.value=="" ){
        	alert("请填写折旧年数！");
        	use_years_defined.focus();
          return "ERROR";
        }
        if(use_years_defined.value<1){
        	alert("请正确填写计提年限！");
          return "ERROR";
        }

        if(is_depre.value=="1" && equi_depre_code.value=='02'){
         if(work_num.value<1){
        	alert("请正确填写总工作量！");
          return "ERROR";
        }
      }
/*
        if(is_import.value=="1" && is_taxfree.value!=1 && is_taxfree.value!=2){
        	alert("请选择是否免税！");
          return "ERROR";
        }
*/

				var names = document.getElementById("hiddenCName").value.split(';');
				for(var i=0;i<names.length-1;i++)
				{
          if(names[i] == "equi_arch_no")
          {
          	if(equi_arch_no.value=="")
          	{
            	fun_result+= "noUse" + "\\" ;
            	continue;
            }
					}

        	if(!check(names[i])){
            return "ERROR";
          }
        }
        if (equi_primmoney.value > 9999999999.99) {
		      alert("设备单价不能大于9999999999.99");
		      return "ERROR";
		    }
        if (prim_money.value > 9999999999.99) {
		      alert("原值不能大于9999999999.99");
		      return "ERROR";
		    }
        fun_result=fun_result.substring(0,fun_result.length-1);
        return fun_result;
      }
      //对页面控件进行验证
      function check(objName)
      {
      	if(document.getElementsByName(objName).length==0)
      	{
      		fun_result+= "\\" ;
      		return true;
      	}

      	var obj = document.getElementsByName(objName)[0];
      	if(obj.className == "inputSelectS" || obj.className == "inputCalendar")
      	{
      		fun_result+= obj.value + "\\" ;
      		return obj.check();
      	}
      	else
    		{
	    		if(obj.tagName == "SELECT")
	    		{
	    			if(obj.selectedIndex==-1)
	    				fun_result += "\\" ;
	    			else
	    				fun_result+= obj.options[obj.selectedIndex].value + "\\" ;
	    		}
	    		else
	    		{
	    			var str = obj.value.replace(/\\/g,'&lt;');
	    			fun_result += str + "\\" ;
	    		}

    			if(obj._required == 'true')
    			{
	    			if(obj.tagName == "INPUT" && obj.value.trim() == "")
	    			{
	    				alert("存在必填项未指定值");
	    				return false;
	    			}
	    			if(obj.tagName == "SELECT" && obj.selectedIndex < 0)
	    			{
	    				alert("存在必填项未指定值");
	    				return false;
	    			}
	    		}

	    		if(obj._t == "inputInteger" && obj.value != "")
	    		{
	    			var intReg = /^[0-9]\d*$/;
	    			var r = intReg.test(obj.value);

	    			if(!r)
	    			{
	    				alert("请填写正确格式的整数信息");
	    			}
	    			return r;
	    		}

	    		if(obj._t == "inputDecimal" && obj.value != "")
	    		{
	    			if(isNaN(obj.value)){
	    				alert("请填写正确格式的数据信息");
	    				return false;
	    			}
	    			//var intDec = /^\d+([.]\d+)?$/;
	    			//var r2 = intDec.test(obj.value);
	    			//if(!r2)
	    			//{
	    			//	alert("请填写正确格式的数据信息");
	    			//}
	    			return true;
	    		}
    		}
      	return true;
    	}
      function getEquiCode(){
			  return equi_code.value;
			}
			function getEquiName(){
			  return equi_name.value;
			}

			function getPrim_money(){
				return prim_money.value;
			}

			function getFore_odd_money(){
				return fore_odd_money.value;
			}
		function changeDepreCode(obj){
			  if(obj=="02"){//工作量法

			  	work_num._required = "true";
			  	work_num.style.backgroundColor='#DBFCFF';
			    work_unit._required = "true";
			    work_unit.style.backgroundColor='#DBFCFF';
			  }
			  else{
			  	work_num._required = "false";
			    work_num.style.backgroundColor='';
			    work_unit._required = "false";
			    work_unit.style.backgroundColor='';
			  	}
			}
	 function changeAlready_depre(){
	 	    //alert(fore_odd_money.value);
	 	    var already=already_depre.value
		    var prim=prim_money.value
		    var fo_odd_money=fore_odd_money.value
		   if(fore_odd_money.value =='noUse'){
			   cur_money.value=0;
			 }else{
			   cur_money.value=(parseFloat(prim.replace('','0'))-parseFloat(already.replace('','0'))-parseFloat(fo_odd_money.replace('','0'))).toFixed(2)
			 }
	 }
			function changeDepre(obj){
			  if(obj=="1"){//是否折旧选择"是"
			  	use_years_defined._required = "true";
			  	use_years_defined.style.backgroundColor='#DBFCFF';
			    equi_depre_code._required = "true";
			    equi_depre_code.style.backgroundColor='#DBFCFF';
			    subBody.load = "equi_card_equi_depre_code";
			    subBody.para = "<card>"+equi_code.value+"</card>";
			    subBody.post();
			    var vs = new Array();
   				vs=subBody.getOneDim();
   				if(vs!=null && vs !='')
    			{
    				 equi_depre_code.value=vs[0];
    			}
    			  if(equi_depre_code.value=="02"){//工作量法

			  	work_num._required = "true";
			  	work_num.style.backgroundColor='#DBFCFF';
			    work_unit._required = "true";
			    work_unit.style.backgroundColor='#DBFCFF';
			  }
			    if (use_years_defined){
		    		if(use_years_defined.value==''){
		    			subBody.load = "equi_card_depre_years";
					    subBody.para = "<card>"+equi_code.value+"</card>";
					    subBody.post();
					    var res = new Array();
		   				res=subBody.getOneDim();
		   				if(res!=null)
		    			{
		    				use_years_defined.value=res[0];
		    			}
		    		}
		    	}
			  }else{

			  	use_years_defined._required = "true";
			  	use_years_defined.style.backgroundColor='#DBFCFF';
			    equi_depre_code._required = "false";
			    equi_depre_code.style.backgroundColor='';
			    work_num._required = "false";
			  	work_num.style.backgroundColor='';
			    work_unit._required = "false";
			    work_unit.style.backgroundColor='';
			  }
			}

			function changeEqui(){

			  var tempValue = equi_code.text;

			  if (equi_code.value!=''){
				  subBody.load = "equi_code_info_card";
			    subBody.para = "<card>"+equi_code.value+"</card>";
			    subBody.post();
			    var d = new Array();
			    d=subBody.getOneDim();
			    if(d!=null){
			    	if(equi_name)

				    	equi_name.value=d[0];

		    		if(card_selfname){
		    			//if (card_selfname.value=='')
		    				card_selfname.value=d[0];
		    		}

		    		if(equi_model){
		    			if (equi_model.value=='')
		    				equi_model.value=d[1];
		    		}
		    		if(equi_spec){
		    			if (equi_spec.value=='')
		    				equi_spec.value=d[2];
		    		}

		    		if(unit_code){
		    			//if (unit_code.value=='')
		    			unit_code.setValue(d[3]);
		    			//unit_code.value=d[3];
		    		}
		    		if(equi_kind_code){
		    			//if (unit_code.value=='')
          if(d[11]!=''){
            equi_kind_code.setValue(d[11]+'|||'+d[13]);
          }
		    		}
		    		if(is_depre){
		    			if(d[4]!=''){
		    			   is_depre.value=d[4];
		    		  }
		    		  else
		    		  {
		    		     is_depre.value=0;
		    		   }
		    		}
		    		if(equi_depre_code){

		    			if(d[5]!=''){
		    			  equi_depre_code.value=d[5];
		    			 }
		    			 else
		    			 {
		    			 	 equi_depre_code.value='01';
		    			 	}
		    		}
		    		if(use_years_defined){
		    			  use_years_defined.value=d[6];
		    		}
		    		if(equi_no_defined){
		    			  equi_no_defined.value=d[7];
		    		}
		    		subBody.load="equi_card_get_costMonth_paraValue";
						subBody.para="";
						subBody.post();
						var a = subBody.getOneDim();
						if(a!=null){

							a[0]=1;

						   if(a[0]==1)
						   {
						   	  if(is_cost){
						   	  	is_cost.value=d[8];

					    			/*if(d[7]!=''){
					    			  is_cost.value=d[8];
					    			}
					    			else
					    			{
					    				is_cost.value=0;
					    			 }
					    			 */
					    		}
					    		if(cost_month){

					    			   cost_month.value=d[9];

					    		}
					    		if(after_cost_month){

					    			  after_cost_month.value=d[10];

					    		}
					    		if(is_cost.value==1)
					    		{
					    		  if(in_date.value!=""){
											subBody.load="equi_get_acctmonth"
											subBody.para="<a>" + in_date.value +"</a>";
											subBody.post();
											var d = subBody.getOneDim();
											try{cost_start_ym.value=d[0]}catch(e){alert('没有对应会计期间设置!');return;}
										}

					    		}if (equi_kind_code) {
						    	  equi_kind_code.setEnabled(false);
						    	}if (equi_kind_type) {
						    	  equi_kind_type.setEnabled(false);
						    	}
					    		else
									  	{
									  		cost_start_ym.value='';
									  	}
						   }
				    }
		    	}
			  }

			 if(is_depre.value=="1"  && equi_depre_code.value=="02"){
			  	work_num._required = "true";
			  	work_num.style.backgroundColor='#DBFCFF';
			    work_unit._required = "true";
			    work_unit.style.backgroundColor='#DBFCFF';
			  }
			}

			function changePurEqui(){
			 // var tempValue = pur_no.text
			  if (pur_no.value!=''){

				  subBody.load = "equi_pur_info";
			    subBody.para = "<pur>"+pur_no.value+"</pur>";
			    subBody.post();
			    var d = new Array();
			    d=subBody.getOneDim();
			    if(d!=null){
			    	if(equi_name)
			    	equi_name.value=d[0];
		    		if(card_selfname){

		    				card_selfname.value=d[0];
		    		}
		    		/*if(equi_power){

		    			equi_power.setValue(d[5]);---科研卡号

		    		}*/
		    		if(equi_model){

		    				equi_model.value=d[1];
		    		}
		    		if(equi_spec){

		    				equi_spec.value=d[2];
		    		}
		    		if(equi_power){

		    			equi_power.value=d[3];

		    		}

		    		if(nation_code){
		    			if(d[4]!=''){
		    			  nation_code.value=d[4];
		    			 }
		    			 else
		    			 {
		    			 	 nation_code.value='001';
		    			 	}
		    		}

            if(amount){

		    			amount.value=d[6];

		    		}
		    		if(at_dept){

		    			at_dept.setValue(d[7]);

		    		}
		    		if(buy_code){

		    			buy_code.value=d[8];

		    		}
		    		if(prim_money){

		    			prim_money.value=d[9];

		    		}
		    		if(equi_primmoney){

		    			equi_primmoney.value=d[10];

		    		}

		    	}
		    	 equi_in_type.value='1';
			  }
			}

		  function changeMeasure(obj){

		    if(obj=='1')
		    {
		    	subBody.load = "equi_measure_value_info";
			    subBody.para="<a>" + equi_arch_no.value +"</a>";
			    subBody.post();
			    var d = new Array();
			    d=subBody.getOneDim();
		    	if(d!=null && d[2]!=0){
		    	  measure_value._required='true';
		    	  measure_value.value=d[1];
		    	  measure_cycle.value=d[2];
		    	  measure_value.style.backgroundColor='#DBFCFF';
		    	}else{
		    	  measure_value._required='true';
		    	  measure_value.value=1;
		    	  measure_cycle.value='0';
		        measure_value.style.backgroundColor='#DBFCFF';
		    	}
		    }
		    else
		    {
		      measure_value._required='false';
		      measure_value.value='';
		      measure_value.style.backgroundColor='';
		    }
		  }

        function changeImport(){

		  	/*
			  	if(is_import.value=='0'){
				    is_taxfree.disabled=true;
				    is_taxfree.value='0';

			    }
			    else{
			    	is_taxfree.value='1';
				    //is_taxfree.refresh();
				    is_taxfree.disabled=false;
			  	}
			  */

		  }
			function changeThrow(){
			if (equi_arch_no.value != "") {
		        alert("已保存卡片不能修改是否投放");
		         if (is_throw.value=='0') {
		          is_throw.value='1';
		        } else {
		          is_throw.value='0';
		        }
		        return;
			}
			}
		  function changeType(){
		    if (typeFlag != "0") {
		      if (equi_arch_no.value != "") {
		        alert("已保存卡片不能修改位置类型");
		        if (equi_in_type.value=='0') {
		          equi_in_type.value='1';
		        } else {
		          equi_in_type.value='0';
		        }
		        return;
		      }
		    }
			  	if(equi_in_type.value=='0'){

				    at_dept.setEnabled(false);
				    at_dept.required='false';
			    	at_dept.setValue('|||');
				    store_code.required='true';
				    store_code.refresh();
				    store_code.setEnabled(true);
				    //use_state.value='2';
			    }
			    else{
			    	//alert(1)
			    	store_code.setEnabled(false)
			    	store_code.setValue('|||');
				    store_code.required='false';
				    store_code.refresh();
				    at_dept.setEnabled(true)
				    at_dept.required='true';
			  	}
		  }

		  function changeAppe(v){
		  		//alert(v)
		  	if(v=="1"){

			  	main_arch_no._required='true';
			  	main_arch_no.disabled=false;
			  	//$("#main_arch_no").removeAttr("disabled");

		  	}else{
		  		main_arch_no._required = "false";
		  			main_arch_no.value="";
		  	main_arch_no.disabled=true;
		  	}
		  }

		  function changeType_before(){
		  	if(subBody.hideMsg){
		  	}else{
		  		window.setTimeout(changeType_before,10);
		  		return;
		  	}
		    subBody.load = "equi_card_state";
		    subBody.para = "<card>"+top.cardNo+"</card>";

		    subBody.post();
		    var d = new Array();
		    d=subBody.getOneDim();
		    if(d!=null)
		    {

				    if(d[0]==0)
				    {
					  	if(equi_in_type.value=='0'){
						    at_dept.setEnabled(false)
						    store_code.setEnabled(true)
						    store_code.required='true';
						    at_dept.required='false';
					    }
					    else{
					    	  store_code.setEnabled(false)
						      at_dept.setEnabled(true)
						      store_code.required='false';
						    	at_dept.required='true';
					  	}
				   }
				   else
		   	  {

		   		 at_dept.setEnabled(false)
		   		 store_code.setEnabled(false)

//					 is_taxfree.disabled=true;

		   	  }
		   }
		   else
		   	{
		   		if(equi_in_type.value=='0'){
						    at_dept.setEnabled(false)
						    store_code.setEnabled(true)
						   // use_state.value = '2';  //默认在库
					    }
					    else{
					    	  store_code.setEnabled(false)
						      at_dept.setEnabled(true)
						     // use_state.value = '1';  //默认在用
					  	   }

					/*
					  if(is_import.value=='0'){
						   is_taxfree.disabled=true;
						  }
					    else{
					    	   is_taxfree.disabled=false;
					  	   }
				 */
		   	}
		  }
		   function throw_code(){
		          if(is_throw.value=='1'){
								if(change_type.value==''){
									alert('请输入转正方式!');
									return;
								}
								if(change_num.value==''){
									alert('请输入转正数量!');
									return;
								}


							}

		        }
		  function changeAtDept() {
		    if (equi_arch_no.value != "") {
		      alert("已保存卡片不能修改使用部门");
		      at_dept.setValue(atDeptTemp);
		    }
		  }

		  function changeStoreCode() {
		    if (equi_arch_no.value != "") {
		      alert("已保存卡片不能修改仓库");
		      store_code.setValue(storeCodeTemp);
		    }
		  }

		  function setTempValue() {
		    if(equi_in_type.value=='0'){
		      storeCodeTemp = store_code.value;
		    } else {
		      atDeptTemp = at_dept.value;
		    }
		   // equi_primmoney.disabled = true;
		   // amount.disabled = true;
		  }

		</script>
		<style type="text/css">
		input{ width:140px; margin-left:5px;}
		select{ width:140px;margin-left:5px;}
		input.inputSelectS{ margin:0}
		</style>
	</head>
	<body class="subBody" id="subBody" onLoad="init()" style="overflow:scroll;scroll:auto;overflow-x:hidden;">
	<%      String cardType=request.getParameter("type");

		String comp_Code=request.getParameter("comp");
		String copy_code=request.getParameter("zt");
		String UID=request.getParameter("uid");
		String readStr = request.getParameter("read");
		String depreRead = request.getParameter("depreRead");
		String isCS = request.getParameter("isCS"); 
		if("1".equals(depreRead))
			depreRead="disabled = true";
		else
				depreRead="";
	  
	  if("1".equals(isCS))
			isCS="1";
		else
			isCS="";
			
		String attCode = request.getParameter("attCode");
		if(null==attCode||attCode=="undefined"){
			attCode="";
		}

		boolean is_read = false;
		RelationalParameter param=null;
		RelationalResult result=null;
		if(readStr.equals("6")){
			is_read = false;
                param=new RelationalParameter("equi_CardDisplayProperty",null,new String[]{cardType});
		result=OperateHelper.executeProc(request,param);}
		if(readStr.equals("0")){
			is_read = false;
                param=new RelationalParameter("equi_CardDisplayProperty",null,new String[]{cardType});
		result=OperateHelper.executeProc(request,param);}
		if(readStr.equals("1")){
		        is_read = true;
                param=new RelationalParameter("equi_CardDisplayProperty",null,new String[]{cardType});
		result=OperateHelper.executeProc(request,param);}

		String ConstStr = "";
		int displayNum = 0;
		StringBuffer sb = new StringBuffer(1024);
		StringBuffer sbName = new StringBuffer(128);

		sb.append("<table id='query' class='lineCtn' style='background:none'>");

		for(int i=0;i<result.getResultValue().size();i++)
		{
			String[] OneRow=(String[])result.getResultValue().get(i);
			String[] OneControl = OneRow[0].split(";");

			if(OneControl[0].equals("measure_cycle")){

			}
      else if(OneControl[0].equals("measure_value")){
      	sbName.append("measure_value;measure_cycle;");
      }
    	else{
    		sbName.append(OneControl[0]+";");
    	}
     //  if((OneControl[3].equals("1") && OneControl[2].equals("1")))
      if(OneControl[2].equals("1"))
      {
	      if(displayNum%3==0)
	        sb.append("<tr>");

	      if(!OneControl[0].equals("measure_cycle")){
	        sb.append("<td align='right'>");
  	      sb.append( OneControl[4] + "：");
  		    ++displayNum ;
		      sb.append("</td>");
		      sb.append("<td>");
		     // if(is_read==true && OneControl[5].equals("0"))
		      if(is_read==true && OneControl[5].equals("0"))
		      	ConstStr = "DISABLED style='background-color:#EDEDED;border:1 solid #838383;";
		      else
		      	ConstStr = "style='border:1 solid #838383;";
	      }

	      //根据字段名称来判断CLASS及其他属性
        if(OneControl[0].equals("measure_cycle")){
          //sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"'/>"); 
        }
      	else if(OneControl[0].equals("equi_arch_no")){//资产卡片号
        	sb.append("<input type='text' name='"+OneControl[0]+"' readonly style='background-color:#EDEDED;border:1 solid #838383;'/>");
				}
        else if(OneControl[0].equals("state")){//状态
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"'/>");
				}
        else if(OneControl[0].equals("equi_name")){//资产名称
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+" margin-left:5px' readonly style='background-color:#EDEDED;border:1 solid #838383;'/>");
				}
		else if(OneControl[0].equals("buy_date")){//购置日期
          sb.append("<input type='text' class='inputCalendar' name='"+OneControl[0]+"' "+ConstStr+"'/>");
				}
				else if(OneControl[0].equals("equi_code")){//资产编码
         // sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"'/>");
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelectS'  defaultValue='false' required='true' code='false' load='equi_dict_list2' para='<type>" + cardType + "</type>' onchange='changeEqui()' editor='equi_code_dict'/> ");
        }
                               else if(OneControl[0].equals("at_dept")){//在用科室
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelectS' required='true' load='equip_dept_code_write_spell_new' para='<a>"+UID+"</a>' onchange='changeAtDept();'/> ");
				}
				else if(OneControl[0].equals("apply_dept")){//申请科室
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelectS' load='dict_dept_code_spell' para='' /> ");
				}
				else if(OneControl[0].equals("buy_emp")){//采购员
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelectS' load='stock_emp_spell' para='' /> ");
				}
				else if(OneControl[0].equals("store_code")){//仓库equip_store_name_write_spell  equip_store_code_write_spell
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelectS'  load='equip_store_name_write_spell' para='<userID>" + UID + "</userID><b>"+attCode+"</b>'; /> ");
				}
				else if(OneControl[0].equals("equi_kind_code")){//资产分类
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelectS' required='true' code='true' load='equi_kind_dict_list' para='<type>" + cardType + "</type>' /> ");
				}
				else if(OneControl[0].equals("equi_factory")){//生产厂家
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelectS' load='dict_equi_factory' />");
				}
				else if(OneControl[0].equals("ven_code")){//供应商
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"'  class='inputSelectS' load='equip_ven_code_spell' />");
				}
					else if(OneControl[0].equals("is_cost")){//是否分摊
          sb.append("<SELECT name='"+OneControl[0]+"'"+ConstStr+"' ><OPTION VALUE='0'>否<OPTION VALUE='1' >是</SELECT>");
				}
				/*
				else if(OneControl[0].equals("is_cost")){//是否分摊
          sb.append("<SELECT name='"+OneControl[0]+"'"+ConstStr+"' onchange='changeis_code()'><OPTION VALUE='0'>否<OPTION VALUE='1' >是</SELECT>");
				}
				*/
				else if(OneControl[0].equals("budg_no")){//预算编号
				System.out.println(OneControl[0]);
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"'  class='inputSelectS' load='equi_budg_proj_dict' para='<year></year>' />");
				}

				else if(OneControl[0].equals("unit_code")){//计量单位
					sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelectS' load='dict_equi_unit' extent='140'/>");
					/*RelationalParameter param2=new RelationalParameter("equi_UnitInfo",null,new String[]{""});
					RelationalResult result2=OperateHelper.executeProc(request,param2);
          sb.append("<SELECT name='"+OneControl[0]+"' "+ConstStr+"'>");
          for(int j=0;j<result2.getResultValue().size();j++){
          	String[] UnitRow=(String[])result2.getResultValue().get(j);
          	sb.append("<OPTION VALUE='"+UnitRow[0]+"'>"+UnitRow[1]);
          }
          sb.append("</SELECT>");*/
				}
				 else if(OneControl[0].equals("change_type")){//转正方式
					sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelectS' load='equi_changetype_throw' extent='80'/>");
					 
				}
				else if(OneControl[0].equals("equi_usage_code")){//设备用途
					RelationalParameter param3=new RelationalParameter("equi_EquipUsage",null,new String[]{comp_Code,copy_code});
					RelationalResult result3=OperateHelper.executeProc(request,param3);
          //sb.append("<SELECT  onchange='usestate_code()' name='"+OneControl[0]+"' "+ConstStr+"'>");  //新会计制度取消一次性计提   zhould
         sb.append("<SELECT   name='"+OneControl[0]+"' "+ConstStr+"'>");
          for(int j=0;j<result3.getResultValue().size();j++){
          	String[] UnitRow=(String[])result3.getResultValue().get(j);
          	sb.append("<OPTION VALUE='"+UnitRow[0]+"'>"+UnitRow[1]);
          }
          sb.append("</SELECT>");
				}
				else if(OneControl[0].equals("card_type")){//卡片类别
					RelationalParameter param7=new RelationalParameter("equi_CardType",null,new String[]{comp_Code,copy_code});
					RelationalResult result7=OperateHelper.executeProc(request,param7);
          sb.append("<SELECT name='"+OneControl[0]+"' "+ConstStr+"'>");
          for(int j=0;j<result7.getResultValue().size();j++){
          	String[] UnitRow=(String[])result7.getResultValue().get(j);
          	sb.append("<OPTION VALUE='"+UnitRow[0]+"'>"+UnitRow[1]);
          }
          sb.append("</SELECT>");
				}
				else if(OneControl[0].equals("buy_code")){//采购类型

          RelationalParameter param9=new RelationalParameter("equi_cardPur_kind",null,new String[]{comp_Code,copy_code});
					RelationalResult result9=OperateHelper.executeProc(request,param9);
          sb.append("<SELECT name='"+OneControl[0]+"' "+ConstStr+"'><OPTION VALUE='0'> ");
          for(int j=0;j<result9.getResultValue().size();j++){
          	String[] UnitRow=(String[])result9.getResultValue().get(j);
          	sb.append("<OPTION VALUE='"+UnitRow[0]+"'>"+UnitRow[1]);
          }
          sb.append("</SELECT>");
				}

	else if(OneControl[0].equals("is_accept")){  //是否验收
        	sb.append("<SELECT name='"+OneControl[0]+"'  "+ConstStr+"'><OPTION VALUE='0'>否<OPTION VALUE='1'>是</SELECT>");
				}
        else if(OneControl[0].equals("is_measure")){  //是否计量
        	sb.append("<SELECT name='"+OneControl[0]+"' onchange='changeMeasure(this.value)' "+ConstStr+"'><OPTION VALUE='0'>否<OPTION VALUE='1'>是</SELECT>");
				}
				else if(OneControl[0].equals("measure_value")){//计量频率和周期
          sb.append("<input type='text' name='"+OneControl[0]+"' _t='inputInteger'  "+ConstStr+"width:90;'/>&nbsp;");
          sb.append("<SELECT name='measure_cycle' "+ConstStr+"width:40'><OPTION VALUE='0'>年<OPTION VALUE='1'>月<OPTION VALUE='2'>日</SELECT>");
				}
        else if(OneControl[0].equals("is_import")){//是否进口
        	sb.append("<SELECT name='"+OneControl[0]+"' onchange='changeImport(this.value)' "+ConstStr+"background-color:#DBFCFF'><OPTION VALUE='0'>否<OPTION VALUE='1'>是</SELECT>");
				}
				else if(OneControl[0].equals("is_taxfree")){//
        	sb.append("<SELECT name='"+OneControl[0]+"' ' "+ConstStr+"'><OPTION VALUE='0'>否<OPTION VALUE='1'>是</SELECT>");

				}
				else if(OneControl[0].equals("nation_code")){//国家
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelectS'  load='dict_equi_nation' /> ");
        }
      	else if(OneControl[0].equals("run_type")){//业务类型
					RelationalParameter param6=new RelationalParameter("equi_run_type",null,new String[]{""});
					RelationalResult result6=OperateHelper.executeProc(request,param6);
          sb.append("<SELECT name='"+OneControl[0]+"' "+ConstStr+"'>");
          for(int j=0;j<result6.getResultValue().size();j++){
          	String[] UnitRow=(String[])result6.getResultValue().get(j);
          	sb.append("<OPTION VALUE='"+UnitRow[0]+"'>"+UnitRow[1]);
          }
          sb.append("</SELECT>");
				}
	 	   else if(OneControl[0].equals("lend_run_type")){//租借状态
					sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"'  class='inputSelectS' load='lend_out_type_spell'/>");
					
				}
				else if(OneControl[0].equals("is_depre")){//是否计提
					sb.append("<SELECT name='"+OneControl[0]+"' onchange='changeDepre(this.value)' "+ConstStr+"background-color:#DBFCFF'><OPTION VALUE='0'>否<OPTION VALUE='1'>是</SELECT>");
				}
				else if(OneControl[0].equals("is_oldequi")){//是否旧设备
					if(isCS.equals("1"))
					{
						sb.append("<SELECT name='"+OneControl[0]+"' "+ConstStr+"'><OPTION VALUE='1'>是<OPTION VALUE='0'>否</SELECT>");
					}
					else{
						sb.append("<SELECT name='"+OneControl[0]+"' "+ConstStr+"'><OPTION VALUE='0' checked>否<OPTION VALUE='1'>是</SELECT>");
					}
					
				}
				else if(OneControl[0].equals("property_form")){//是否有产权
					sb.append("<SELECT name='"+OneControl[0]+"' "+ConstStr+"'><OPTION VALUE='1'>是<OPTION VALUE='0'>否</SELECT>");
     		   }
     		   else if(OneControl[0].equals("equi_card_money")){//卡片汇总金额
         			 sb.append("<input  type='text' name='"+OneControl[0]+"'  _t='inputDecimal' readonly/>");
        }
        
				else if(OneControl[0].equals("equi_depre_code")){//计提方法
					RelationalParameter param5=new RelationalParameter("equi_Depre",null,new String[]{""});
					RelationalResult result5=OperateHelper.executeProc(request,param5);

          sb.append("<SELECT name='"+OneControl[0]+"' onchange='changeDepreCode(this.value)' "+ConstStr+"'>");
          for(int j=0;j<result5.getResultValue().size();j++){
          	String[] UnitRow=(String[])result5.getResultValue().get(j);
          	sb.append("<OPTION VALUE='"+UnitRow[0]+"'>"+UnitRow[1]);
          }
          sb.append("</SELECT>");
				}
				else if(OneControl[0].equals("card_bz")){//币种
					RelationalParameter param8=new RelationalParameter("equi_Curr",null,new String[]{""});
					RelationalResult result8=OperateHelper.executeProc(request,param8);
          sb.append("<SELECT name='"+OneControl[0]+"' "+ConstStr+"'>");
          for(int j=0;j<result8.getResultValue().size();j++){
          	String[] UnitRow=(String[])result8.getResultValue().get(j);
          	sb.append("<OPTION VALUE='"+UnitRow[0]+"'>"+UnitRow[1]);
          }
          sb.append("</SELECT>");
				}
				else if(OneControl[0].equals("use_year")){//已用年限
          sb.append("<input type='text' name='"+OneControl[0]+"' _t='inputInteger' "+ConstStr+"'/>");
				}
				else if(OneControl[0].equals("lend_in_id")){//已用年限
          sb.append("<input type='text' name='"+OneControl[0]+"' _t='inputInteger' "+ConstStr+"'/>");
				}
        else if(OneControl[0].equals("do_year")){//可用年限
          sb.append("<input type='text' name='"+OneControl[0]+"' _t='inputInteger' "+ConstStr+"'/>");
				}
        else if(OneControl[0].equals("prim_money")){//资产原值
          sb.append("<input type='text' name='"+OneControl[0]+"' _t='inputDecimal' readonly "+ConstStr+"' onchange='changeAlready_depre()'/>");
				}
				else if(OneControl[0].equals("install_money")){//安装费
          sb.append("<input onblur='changePrim()' type='text' name='"+OneControl[0]+"' _t='inputDecimal' "+ConstStr+"' />");
				}
                else if(OneControl[0].equals("is_autowrite")){//是否反写
            sb.append("<input type='text' style='display:none' name='"+OneControl[0]+"'' "+ConstStr+"' />");
                }
				else if(OneControl[0].equals("vouch_id")){//凭证号
					sb.append("<input type='text' style='display:none' name='"+OneControl[0]+"'' "+ConstStr+"' />");
				}
				else if(OneControl[0].equals("work_num_ini")){//
          sb.append("<input  onblur='changeWork()' type='text' name='"+OneControl[0]+"' _t='inputDecimal'  "+ConstStr+"' />");
				}
				else if(OneControl[0].equals("work_num")){//
          sb.append("<input  type='text' name='"+OneControl[0]+"' _t='inputDecimal'  "+ConstStr+"' />");
				}
				else if(OneControl[0].equals("work_num_sum")){//
          sb.append("<input  type='text' name='"+OneControl[0]+"' _t='inputDecimal' readonly "+ConstStr+"' />");
				}
					else if(OneControl[0].equals("tex_money")){//
          sb.append("<input  onblur='changePrim()' type='text' name='"+OneControl[0]+"' _t='inputDecimal' "+ConstStr+"' />");
				}
				else if(OneControl[0].equals("trans_money")){//运杂费
          sb.append("<input onblur='changePrim()' type='text' name='"+OneControl[0]+"' _t='inputDecimal' "+ConstStr+"'/>");
				}
        else if(OneControl[0].equals("amount")){//数量
          sb.append("<input onblur='changePrim()' type='text' name='"+OneControl[0]+"' _t='inputInteger' _required='true' "+ConstStr+" background-color:#DBFCFF'/>");
				}
		    else if(OneControl[0].equals("already_depre")){//累计工作量
          sb.append("<input  type='text' name='"+OneControl[0]+"' _t='inputDecimal'  "+ConstStr+"' "+depreRead+" onchange='changeAlready_depre()' />");
				}
				else if(OneControl[0].equals("pur_no")){//申请单号

          sb.append("<input onblur='changePurEqui()' type='text' name='"+OneControl[0]+"'  "+ConstStr+" '/>");
				}
        else if(OneControl[0].equals("fore_odd_money")){//预计残值
          sb.append("<input type='text' name='"+OneControl[0]+"' _t='inputDecimal'   "+ConstStr+" ' onchange='changeAlready_depre()' />");
				}

        else if(OneControl[0].equals("rise_date")){//启用日期
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputCalendar'/>");;
				}
	else if(OneControl[0].equals("acc_date")){//上账日期
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputCalendar'/>");;
				}
	else if(OneControl[0].equals("check_date")){//验收日期
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputCalendar'/>");;
				}
				else if(OneControl[0].equals("start_date")){//开始日期
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputCalendar'/>");;
				}
				else if(OneControl[0].equals("end_date")){//结束日期
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputCalendar'/>");;
				}
        else if(OneControl[0].equals("leave_date")){//出厂日期
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputCalendar'/>");
				}
				else if(OneControl[0].equals("out_date")){//出库日期
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputCalendar'/>");
				}
				else if(OneControl[0].equals("in_date")){//入库日期
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+" margin-left:5px' required='true' defaultValue='true'  class='inputCalendar' onchange='changePmoney();'  />");
				}
				else if(OneControl[0].equals("create_date")){//创建日期
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+" margin-left:5px' required='true' defaultValue='true'  class='inputCalendar' onchange='changePmoney()'  />");
				}
        else if(OneControl[0].equals("use_area")){//使用面积
          sb.append("<input type='text' name='"+OneControl[0]+"' _t='inputDecimal'  value='0' "+ConstStr+" '/>");
				}
        else if(OneControl[0].equals("build_area")){//建筑面积
          sb.append("<input type='text' name='"+OneControl[0]+"' _t='inputDecimal'  value='0' "+ConstStr+" '/>");
				}
        else if(OneControl[0].equals("build_stuc")){//结构
          sb.append("<input type='text' name='"+OneControl[0]+"'  "+ConstStr+"'/>");
				}
				else if(OneControl[0].equals("at_loca")){//坐落位置
          sb.append("<input type='text' name='"+OneControl[0]+"'  "+ConstStr+"'/>");
				}
				else if(OneControl[0].equals("build_floor")){//层高
          sb.append("<input type='text' name='"+OneControl[0]+"' _t='inputInteger' "+ConstStr+"'/>");
				}
				else if(OneControl[0].equals("qlty_grade")){//质量等级
          sb.append("<input type='text' name='"+OneControl[0]+"' _t='inputInteger' "+ConstStr+"'/>");
        }
        
        
        
        
        else if(OneControl[0].equals("use_years_defined")){//用户自定义使用时间
          sb.append("<input type='text' value='1' name='"+OneControl[0]+"' _t='inputInteger' "+ConstStr+"'/>");
				}
				else if(OneControl[0].equals("cur_money")){//净值
          sb.append("<input type='text' name='"+OneControl[0]+"' readonly  _t='inputDecimal' "+ConstStr+"'/>");
				}
				else if(OneControl[0].equals("card_hl")){//汇率
          sb.append("<input type='text' name='"+OneControl[0]+"' _t='inputDecimal' "+ConstStr+"'/>");
				}
        /*else if(OneControl[0].equals("use_state")){//使用状态
        	sb.append("<SELECT   name='"+OneControl[0]+"' "+ConstStr+" DISABLED style='background-color:#EDEDED;border:1 solid #838383;'><OPTION VALUE='1'>正常<OPTION VALUE='2'>待处置<OPTION VALUE='3'>处置完毕</SELECT>");
				}*/
				else if(OneControl[0].equals("use_state")) {  //使用状态
        	RelationalParameter param11=new RelationalParameter("dict_use_state",null,new String[]{""});
					RelationalResult result11=OperateHelper.executeProc(request,param11);
          sb.append("<SELECT name='"+OneControl[0]+"' "+ConstStr+"'>");
          for(int j=0;j<result11.getResultValue().size();j++) {
          	String[] UnitRow=(String[])result11.getResultValue().get(j);
          	sb.append("<OPTION VALUE='"+UnitRow[0]+"'>"+UnitRow[1]);
          }
          sb.append("</SELECT>");
				}
				else if(OneControl[0].equals("contract_price")){//合同单价
          sb.append("<input type='text' name='"+OneControl[0]+"' _t='inputDecimal' value='0' "+ConstStr+"'/>");
				}
        else if(OneControl[0].equals("equi_kind_type")){//资产模版类型

				   	sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelect'  load='equip_nature'/> ");	
				   	
				}
				 
        else if(OneControl[0].equals("equi_in_type")){//
        	sb.append("<SELECT name='"+OneControl[0]+"' onchange='changeType(this.value)' "+ConstStr+"'><OPTION VALUE='1'>部门<OPTION VALUE='0'>库房</SELECT>");
				}
				else if(OneControl[0].equals("equi_primmoney")){//设备原值
          sb.append("<input onblur='changePmoney()' type='text' name='"+OneControl[0]+"' _t='inputDecimal' _required='true' "+ConstStr+" background-color:#DBFCFF'  maxLength='13'/>");
				}
				else if(OneControl[0].equals("equi_sn")){//资产序列号
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"'/>");
				}
				else if(OneControl[0].equals("create_user")){//资产创建人
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"'/>");
				}
					else if(OneControl[0].equals("rem")){//备注
          sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"'/>");
				}
				else if(OneControl[0].equals("is_pay")){//是否付款
        	sb.append("<input type='text' name='"+OneControl[0]+"' DISABLED style='background-color:#EDEDED;border:1 solid #838383;'/>");
				}
				else if(OneControl[0].equals("pay_date")){//付款日期
          sb.append("<input type='text' name='"+OneControl[0]+"' DISABLED style='background-color:#EDEDED;border:1 solid #838383;' class='inputCalendar'/>");
				}
        else if(OneControl[0].equals("is_app")){//是否附件
        	sb.append("<SELECT name='"+OneControl[0]+"' onchange='changeAppe(this.value)' "+ConstStr+"'><OPTION VALUE='0'>否<OPTION VALUE='1'>是</SELECT>");
				}
				else if(OneControl[0].equals("is_throw")){//是否投放
        	sb.append("<SELECT  name='"+OneControl[0]+"' onchange='changeThrow()' "+ConstStr+"'><OPTION VALUE='0'>否<OPTION VALUE='1'>是</SELECT>");
				}
				
				else if(OneControl[0].equals("fund_source_code")) {  //资金来源
        	RelationalParameter param10=new RelationalParameter("dict_money_resource",null,new String[]{""});
					RelationalResult result10=OperateHelper.executeProc(request,param10);
          sb.append("<SELECT name='"+OneControl[0]+"' "+ConstStr+"'>");
          for(int j=0;j<result10.getResultValue().size();j++) {
          	String[] UnitRow=(String[])result10.getResultValue().get(j);
          	sb.append("<OPTION VALUE='"+UnitRow[0]+"'>"+UnitRow[1]);
          }
          sb.append("</SELECT>");
				}
				/*
				else if(OneControl[0].equals("equi_bar_code")){//资产条码
          sb.append(" class='inputTextA'/>");
				}
        else if(OneControl[0].equals("equi_spec")){//规格
          sb.append(" class='inputTextA'/>");
				}
				else if(OneControl[0].equals("equi_model")){//型号
          sb.append(" class='inputTextA'/>");
				}
				else if(OneControl[0].equals("equi_brand")){//品牌
          sb.append(" class='inputTextA'/>");
				}
        else if(OneControl[0].equals("equi_no_defined")){//自定义卡片号
          sb.append(" class='inputTextA'/>");
				}
        else if(OneControl[0].equals("property_cert")){//权属证书
          sb.append(" class='inputTextA' />");
				}
				else if(OneControl[0].equals("property")){//权属性质
          sb.append(" class='inputTextA' />");
				}
				else if(OneControl[0].equals("prin_emp")){//责任人
          sb.append(" class='inputTextA' /> ");
				}
			
        else if(OneControl[0].equals("use_purpose")){//使用方向
          sb.append(" class='inputTextA'/>");
				}
      	else if(OneControl[0].equals("leave_no")){//出厂号
          sb.append(" class='inputTextA' />");
				}
				
				else if(OneControl[0].equals("comp_code")){// 
          sb.append(" style='display:none' class='inputTextA'/>");
				}
      	else if(OneControl[0].equals("copy_code")){//
          sb.append(" style='display:none' class='inputTextA' />");
				}
				*/
				else if(OneControl[0].equals("copy_code")){//转正方式
					sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelect' load='sys_copy_dict'  required='true' extent='130'/>");
					 
				}else if(OneControl[0].equals("comp_code")){//转正方式
					sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"' class='inputSelect' load='sys_comp_dict' required='true' extent='130'/>");

		}
	      else{
	        sb.append("<input type='text' name='"+OneControl[0]+"' "+ConstStr+"'/>");
        }

	      sb.append("</td>");

		    if(displayNum%3==0){
		      sb.append("</tr>");
		    }
	  } else{//不显示的组件用Text框
        sb.append("<td style='display:none' align='right'>");
	      //sb.append(OneControl[4]);
	      sb.append("</td>");
	      sb.append("<td style='display:none'><input type='text' name='"+OneControl[0]+"' value='noUse'></td>");
      }
		}

	  if(displayNum%3!=0){
	    sb.append("</tr>");
	  }
	  sb.append("</table>");
	  sb.append("<input type=hidden id='hiddenCName' value='" + sbName.toString() + "'/>");
		out.print(sb.toString());
		out.flush();
	%>

  </body>


</html>
