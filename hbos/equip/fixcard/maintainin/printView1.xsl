<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/t2head/tr[1]/td)-2"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        	</xsl:call-template>
	  		</tr>		 
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="/root/t2head/tr[position()=1]">
					  <xsl:for-each select="td[position()!=1]">
					    <td nowrap='true'><xsl:value-of select="."/></td>
					  </xsl:for-each>
					</xsl:for-each>
				</tr>     
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						  <xsl:attribute name="Index"><xsl:value-of select="position()+2"/></xsl:attribute>
							<xsl:for-each select="td[position()!=1]">   
						<xsl:variable name="colNum2" select="position()+1"/>
							<xsl:choose>
							<!--�ж��Ƿ���ȫ�ǿո�-->
								<xsl:when test="substring(/root/t2head/tr[1]/td[$colNum2],string-length(/root/t2head/tr[1]/td[$colNum2] ),1)='��'">
									<td align='right'>
										<xsl:attribute name="Index"><xsl:value-of select="position()"/></xsl:attribute>
										<Data Type="String"><xsl:value-of select="format-number(.,'#,##0.00')"/></Data>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:attribute name="Index"><xsl:value-of select="position()"/></xsl:attribute>
										<Data Type="String"><xsl:value-of select="."/></Data>
									</td>
								</xsl:otherwise>
							</xsl:choose>
				</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
