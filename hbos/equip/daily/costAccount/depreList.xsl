<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th>部门名称</th>
        <th>分摊比例</th>
        <th>分摊金额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align="center">
            <xsl:value-of select="td[1]"/>
          </td>
          <td align="center">
            <xsl:value-of select="format-number(td[2],'#,##0.00')"/>
          </td>
          <td align="center">
            <xsl:value-of select="format-number(td[3],'#,##0.00')"/>
          </td>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


