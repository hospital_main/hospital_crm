<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/equip/daily/maintain/should/main.xsl,v 1.1 2012/03/12 01:48:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:48:00 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>计划编号</th>
				<th>计划名称</th>
				<th>资产卡号</th>
				<th>编制科室</th>
				<th>保养设备</th>
				<th>计划执行日期</th>
				<th>保养等级</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <td >
              <xsl:choose>
                <xsl:when test="position()=1">
    								<a href='#'>
    								<xsl:attribute name="onclick">
    									openPlanDetail("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
    								</xsl:attribute>
    								<xsl:value-of select="."/>
    								</a>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
