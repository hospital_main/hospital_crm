<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">		
		<thead>			
			<tr noWrap="true" class="mainHead">
				<th style="display:">
					<input type="checkbox"/>
				</th>
				<th>资产编码</th>
				<th>资产名称</th>
				<th>资产卡片号</th>
				<th>自定义卡号</th>
				<th>规格</th>
				<th>型号</th>
				<th>使用部门</th>
			</tr>
		</thead>
		<tbody>
			<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)"/>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" style="display:">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td[position() &lt; $colNum]">													
									<td><xsl:value-of select="."/></td>					
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
