<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/equip/daily/maintain/report/main.xsl,v 1.1 2012/03/12 01:48:00 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:48:00 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
        <th>保养单号</th>
        <th>保养单名称</th>
        <th>保养计划号</th>
        <th>保养设备</th>
        <th>执行科室</th>
				<th>保养执行日期</th>
        <th>保养费用</th>
        <th>保养工时</th>
       <!--
        <th>审核人</th>				
				<th>审核日期</th>
				-->
				<th>状态</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:if test="td[2] = '合计：'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[2] != '合计：'">
	          <td align='center'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
	    			  </input>
	          </td>
	        </xsl:if>
         
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
              </xsl:when>
              
              <xsl:when test="position()=3">
                <td><a href='#' onclick="openMaintainPlan(this) "><xsl:value-of select="."/></a></td>
              </xsl:when>
              <xsl:when test="position()=7 or position()=8 ">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>                
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
