<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th>选择</th>
        <th>资产卡片号</th>
        <th>资产编号</th>
        <th>资产名称</th>
				<th>所属科室</th>
				<th>规格</th>
				<th>型号</th>
				<th>产地厂家</th>				
				<th>计量单位</th>				
				<th>启用日期</th>	
				<th>科室编码</th>			
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='radio' name='card_sel' onclick='OptionChose(this)'>
            <xsl:attribute name="card_no"><xsl:value-of select="td[1]"/></xsl:attribute>
            <xsl:attribute name="dept_name"><xsl:value-of select="td[4]"/></xsl:attribute>
            <xsl:attribute name="at_dept"><xsl:value-of select="td[10]"/></xsl:attribute>
            <xsl:attribute name="equi_name"><xsl:value-of select="td[3]"/></xsl:attribute>
            <xsl:attribute name="spec"><xsl:value-of select="td[5]"/></xsl:attribute>
            <xsl:attribute name="model"><xsl:value-of select="td[6]"/></xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
             <td align="center">
               <xsl:value-of select="."/>
             </td>
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


