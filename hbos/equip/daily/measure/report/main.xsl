 <?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
     
	 <th style='display:none' width='25'><input type='checkbox'/></th>			 
				<th>计量记录号</th> 
				<th>资产卡号</th>               
				<th>资产名称</th>             
				<th>规格型号</th>             
				<th>供应商</th>             
				<th>使用部门</th> 
				<th>计量日期</th>        
				<th>卡片数量</th>             
				<th>原值</th>         
				<th>计量类别</th> 
				       
				<th>计量费用</th>             
				<th>计量结果</th>            
				<th>状态</th>  
				                        
				<th>计量单位</th>             
				<th>计量证书号</th>  
				<th>序列号</th>  
				<th>制单人</th>  
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:if test="td[1] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">
	          <td align='center'  noWrap='true'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
	    			  </input>
	          </td>
          </xsl:if>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
	                <xsl:if test=". = '合计'">
				        		<td noWrap='true'><xsl:value-of select="."/></td>
				        	</xsl:if>
				        	<xsl:if test=". != '合计'">
				            <td  noWrap='true' >
		                  <a tabindex='-1'><xsl:value-of select="."/></a>
				            </td>
				          </xsl:if>
                </xsl:when>
                
                <xsl:when test="position()=9 or position()=11">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
