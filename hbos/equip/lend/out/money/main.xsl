<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		 <tr noWrap='true' class='mainHead'>
          <th style='display:none'><input type='checkbox'/></th>
          <th nowrap='true'>付款单号</th>
					<th nowrap='true'>付款日期</th>
					<th nowrap='true'>供应商</th>
					<th nowrap='true'>支付方式</th>
					<th nowrap='true'>发票号码</th>					
					<th nowrap='true'>合同编码</th>					
					<th nowrap='true'>付款金额</th>
					<th nowrap='true'>付款类型</th>
					<th nowrap='true'>制单人</th>
					<th nowrap='true'>审核人</th>
					<th nowrap='true'>状态</th>
     		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
              </xsl:when>  
              <xsl:when test="position()=6">
								<td><a tabindex="-2">
											<xsl:attribute name="href">
		                  javascript:openDialog('../../out/contract/update.html?load=&lt;lend_no&gt;<xsl:value-of select="../td[position()=6]"/>&lt;/lend_no&gt;', 'dialogWidth:800px;dialogHeight:600px', result)
		                	</xsl:attribute><xsl:value-of select="."/>
		                </a>
		            </td>
              </xsl:when>  
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

