<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>分类编码</th>
				<th>设备分类</th>
				<th>分类级别</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td ><a tabindex="-2">
                    <xsl:attribute name="href" >
                      javascript:openDialog('queryDetail.html?load=&lt;root&gt;&lt;equi_kind_code&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/equi_kind_code&gt;&lt;equi_kind_name&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/equi_kind_name&gt;&lt;/root&gt;', 'dialogWidth:900px;dialogHeight:600px', result)
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                  </a></td >
                </xsl:when> 
                <xsl:otherwise>
                  <td ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>