<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=""/>
  <xsl:template match="/">
  	<root>
    	<thead>
			<xsl:variable name="colNum" select="/root/tbody/tr[1]/td[last()]"/>
			<xsl:variable name="totCol" select="count(/root/tbody/tr[1]/td)-1"/> 
			<xsl:variable name="realTotCol" select="count(/root/tbody/tr[1]/td)"/>
    	<tr noWrap='true'>
            <td style='fontsize:maintitle;colspan:11'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  			<td style='display:none'></td>
  		</tr>
  		 <tr noWrap='true' class='mainHead'>       
		        <td>使用部门</td>
                <td>归属部门</td>				
				<td>设备编码</td>		
				<td>设备名称</td>			
				<td>规格</td>
				<td>数量</td>
				<td>价格</td>
				<td>金额</td>
				<td>购入日期</td>
				<td>折旧年限</td>
				<td>设备分类</td>	
				<td>国标码</td>
			 	<td>财务分类名称</td>
        </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:if test="./td[2]!='总计'">
		<xsl:variable name="rowNum" select="position()"/>
		<xsl:if test="$rowNum =1 or /root/tbody/tr[$rowNum - 1]/td[1] != ./td[1]">
				<tr noWrap='true' align='left'>
					<td align='left' style="colspan:11;align:left;">
						<xsl:if test="$rowNum !=1">
							<xsl:attribute name="style">pagebreak:true;colspan:11</xsl:attribute>
						</xsl:if>
						【<xsl:value-of select="./td[2]"/>】-固定资产分布盘点汇总清单
					</td>						
				</tr>
		</xsl:if>
      	<tr>
        	<xsl:for-each select="td">
               <xsl:choose>
                              <xsl:when test="position()=7">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
							 </xsl:when>
                              <xsl:when test="position()=8 or position()=9">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when> 
								<xsl:when test="position()!=1">
                                 <td align='left'>
								     <xsl:value-of select="."/>
								   </td>
								</xsl:when>

								
           </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  		
  			
  			</xsl:if>
  			<xsl:if test="./td[2]='总计'">
  			<tr>
  				<td align='center' >总计</td>					
                <td></td>
				<td></td>
				<td></td>				
				<td></td>
				<td align='right'><xsl:value-of select="format-number(./td[7],'#,##0')"/></td>
				<td align='right'><xsl:value-of select="format-number(./td[8],'#,##0.00')"/></td>
				<td align='right'><xsl:value-of select="format-number(./td[9],'#,##0.00')"/></td>
				<td></td>
				<td></td>
  				<td></td>
  				<td></td>
  				<td></td>
  			</tr>
  			</xsl:if>
      </xsl:for-each>  

    </tbody>
   </root>
	</xsl:template>
	
</xsl:stylesheet>