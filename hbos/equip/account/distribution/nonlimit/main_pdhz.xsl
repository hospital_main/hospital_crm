<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
		        <th>使用部门</th>
                <th>归属部门</th>				
				<th>设备编码</th>		
				<th>设备名称</th>			
				<th>规格</th>
				<th>数量</th>
				<th>价格</th>
				<th>金额</th>
				<th>购入日期</th>
				<th>折旧年限</th>
				<th>设备分类</th>	
				<th>国标码</th>
			 	<th>财务分类名称</th>	 
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>         
          <xsl:for-each select="td">
            <xsl:choose>
               <xsl:when test="position()=8 or position()=9">
					      <td align='right'>
						       <xsl:value-of select="format-number(.,'#,##0.00')"/>
					      </td>
				       </xsl:when> 
				       <xsl:when test="position()=7">
					      <td align='right'>
						      <xsl:value-of select="format-number(.,'#,##0')"/>
					      </td>
				       </xsl:when>								
               <xsl:when test="position()=2 or position()=3 or position()=4 or position()=5 or position()=6 or position()=10 or position()=11 or position()=12 or position()=13 or position()=14">
							   <td align='left'><xsl:value-of select="."/></td>
               </xsl:when>
               
           </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			<!--<tr style="display:none">
  				<xsl:attribute name="id">annexrow<xsl:value-of select="$rowindex"/></xsl:attribute>
  				<td align='center'>
          </td>
          
          <td colspan="11" ><xsl:attribute name="id">revelation<xsl:value-of select="$rowindex"/></xsl:attribute>
          </td>
  			</tr>-->
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


