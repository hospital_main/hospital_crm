<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>

        <th>资产编号</th>
				<th>资产名称</th>		
				<th>常用名称</th>
				<th>品牌</th>				
				<th>型号规格</th>
				<th>计量单位</th>
				<th>单价</th>
				<th>数量</th>
				<th>原值</th>
				<th>所在部门</th>
			  <th>国标码</th>
			 	<th>财务分类名称</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>
        	 
          
          <xsl:for-each select="td">
            <xsl:choose>
               <xsl:when test="position()=7 or position()=9">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:when> 
								 <xsl:when test="position()=8">
								 <td align='right'>
									<xsl:value-of select="format-number(.,'#,##0')"/>
								 </td>
								</xsl:when>
								
                <xsl:otherwise>
                  <td align='left'><xsl:value-of select="."/></td>
                </xsl:otherwise>
           </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			<!--<tr style="display:none">
  				<xsl:attribute name="id">annexrow<xsl:value-of select="$rowindex"/></xsl:attribute>
  				<td align='center'>
          </td>
          
          <td colspan="12" ><xsl:attribute name="id">revelation<xsl:value-of select="$rowindex"/></xsl:attribute>
          </td>
  			</tr>-->
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


