<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
  			<th>资产卡片</th>
				<th>资产编码</th>
				<th>资产名称</th>
				<th>规格型号</th>
				<th>产地厂家</th>
				<th>入库日期</th>
				<th>启用日期</th>
				<th>原值</th>
				<th>累计计提</th>
				<th>所在部门</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td >
                  	<a tabindex="-2" href="#">
                    	<xsl:attribute name="onclick" >
                    		javascript:openDialog('equi_querydetail_last.html?load=&lt;equi_arch_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/equi_arch_no&gt;&lt;equi_code&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/equi_code&gt;&lt;equi_name&gt;<xsl:value-of select="../td[position()=3]"/>&lt;/equi_name&gt;&lt;equi_spec_model&gt;<xsl:value-of select="../td[position()=4]"/>&lt;/equi_spec_model&gt;&lt;equi_nation_factory&gt;<xsl:value-of select="../td[position()=5]"/>&lt;/equi_nation_factory&gt;&lt;in_date&gt;<xsl:value-of select="../td[position()=6]"/>&lt;/in_date&gt;&lt;rise_date&gt;<xsl:value-of select="../td[position()=7]"/>&lt;/rise_date&gt;&lt;equi_now_amount&gt;<xsl:value-of select="../td[position()=8]"/>&lt;/equi_now_amount&gt;&lt;equi_jiti&gt;<xsl:value-of select="../td[position()=9]"/>&lt;/equi_jiti&gt;', 'dialogWidth:1000px;dialogHeight:600px', result)  
                      	/*javascript:openDialog('equi_queryDetail.html?load=&lt;root&gt;&lt;depre_year&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/depre_year&gt;&lt;depre_month&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/depre_month&gt;&lt;equi_kind_code&gt;<xsl:value-of select="../td[position()=3]"/>&lt;/equi_kind_code&gt;&lt;equi_kind_name&gt;<xsl:value-of select="../td[position()=4]"/>&lt;/equi_kind_name&gt;&lt;/root&gt;', 'dialogWidth:900px;dialogHeight:600px', result)*/
                    	</xsl:attribute>
                    	<xsl:value-of select="."/>
                  	</a>
                  </td >
                </xsl:when> 
                <xsl:when test="position()=8 or position()=9">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td >
                </xsl:when> 
                <xsl:otherwise>
                  <td ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
