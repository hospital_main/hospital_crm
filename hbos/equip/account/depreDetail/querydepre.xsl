<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>资产编码</th>
				<th>资产名称</th>
				<th>规格型号</th>
				<th>计量单位</th>
				<th>原值</th>
				<th>累计计提</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td >
                  	<a tabindex="-2" href="#">
                    	<xsl:attribute name="onclick" >
                    		javascript:openDialog('equi_queryDetail.html?load=&lt;equi_code&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/equi_code&gt;&lt;equi_name&gt;<xsl:value-of select="../td[position()=2]"/>&lt;/equi_name&gt;&lt;equi_spec_model&gt;<xsl:value-of select="../td[position()=3]"/>&lt;/equi_spec_model&gt;&lt;equi_nation_factory&gt;<xsl:value-of select="../td[position()=4]"/>&lt;/equi_nation_factory&gt;&lt;equi_now_amount&gt;<xsl:value-of select="../td[position()=5]"/>&lt;/equi_now_amount&gt;&lt;equi_jiti&gt;<xsl:value-of select="../td[position()=6]"/>&lt;/equi_jiti&gt;', 'dialogWidth:900px;dialogHeight:600px', result) 
                    		/*javascript:openDialog('equi_queryDetail.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:800px;dialogHeight:600px') */
                    	</xsl:attribute>
                    	<xsl:value-of select="."/>
                  	</a>
                  </td >
                </xsl:when> 
                <xsl:when test="position()=5 or position()=6">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td >
                </xsl:when> 
                <xsl:otherwise>
                  <td ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
