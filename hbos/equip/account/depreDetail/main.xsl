<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNums" select="count(/root/tbody/tr[1]/td) - 3"/>
    <thead>
      <tr noWrap='true' class='mainHead'>      	  
      	<th nowrap='true' colspan="2">���ҷ���</th>
      	
      	<xsl:for-each select="/root/tbody/tr[td[1]='1']/td[position() &gt; 3]">
	      	<th nowrap='true'><xsl:value-of select="."/></th>
      	</xsl:for-each>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[td[1] !='1']">
      	<xsl:variable name="unit_name" select="td[2]" />
				<xsl:variable name="kpi" select="td[3]" />
				<xsl:variable name="cur_pos" select="position()" />					 
				<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[3] = $kpi and td[2]= $unit_name ])" />
				<xsl:variable name="rowspan1" select="count(/root/tbody/tr[td[2]= $unit_name ])" />
			<tr>
        	 
          <xsl:for-each select="td[position() &gt; 1]">
              <xsl:choose>
              	 <xsl:when test="position()=1"> 
				        <xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos]/td[2] ">																		
									<td rowspan="{$rowspan1}"> 
										<xsl:value-of select="." />											
									</td>
								</xsl:if>
								 
								<xsl:if test="$unit_name = ../../tr[$cur_pos]/td[2]">
									<td style="display:none"><xsl:value-of select="."/></td>
								</xsl:if>				           
				       </xsl:when>
               <xsl:when test="position()=2 "> 
				        <xsl:if test="$cur_pos = 1 or $unit_name != ../../tr[$cur_pos]/td[2] or $kpi != ../../tr[$cur_pos]/td[3]">																		
									<td rowspan="{$rowspan}"> 
										<xsl:value-of select="." />											
									</td>
								</xsl:if>
								
								<xsl:if test="$unit_name = ../../tr[$cur_pos]/td[2] and $kpi = ../../tr[$cur_pos]/td[3]">
									<td style="display:none"><xsl:value-of select="."/></td>
								</xsl:if>				           
				       </xsl:when>
				        <xsl:when test="position()>3">
				            <td align='right' >
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when> 
                <xsl:otherwise>
			            <td align="right">
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

