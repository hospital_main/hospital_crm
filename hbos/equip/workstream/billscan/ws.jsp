<%@ page contentType="text/html; charset=gb2312" language="java" import="com.viewhigh.core.web.OperateHelper,com.viewhigh.core.dao.*" errorPage="" %>
<%
String idx = request.getParameter("idx");
%>
<html xmlns:vh>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <link href="" rel="stylesheet" type="text/css">
    <script language="javascript" id="scriptID"></script>
    <script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>
    <script language='javascript'>
    var idx = <% out.write(idx); %>
    function init()
    {
    	basic_frm.document.location.href="../billbasic.jsp?type=<%=request.getParameter("wsid")%>&comp="+getCompCode()+"&zt="+getCopyCode()+"&uid="+getUserID()+"&bid="+idx+"&sb=1";
    	//document.all.basic_frm.style.display="nosne";
    }
    
    function Expand(obj)
    {
    	if(obj.title == "展开")
    	{
    		document.getElementById("basic_frm").style.display = "";
    		obj.title = "收缩";
    		obj.src="/images2/ws/top.gif"
    	}
    	else
  		{
  			document.getElementById("basic_frm").style.display = "none";
  			obj.title = "展开";
  			obj.src="/images2/ws/end.gif"
  		}
    }
    function openadata(pt,audioid)
    {
    	var p=getValuePairBySql("equip_ws_getAttachWSByAudioID","<a>"+audioid+"</a>");
    	if(p==null)
    	{
    		alert("审核人并未填写附属信息");
    		return;
    	}
    	openDialog("../billbasic.jsp?type="+pt+"&comp="+getCompCode()+"&zt="+getCopyCode()+"&uid="+getUserID()+"&bid="+p[0]+"&sb=0","dialogWidth:700px;dialogHeight:300px");
    }
    function openadata2(params)
    {
    	openDialog("../billaudio/attachData.html?load="+params,"dialogWidth:700px;dialogHeight:300px");
    }
    function openattach(params)
    {
    	openDialog("attach.html?load="+params,"dialogWidth:700px;dialogHeight:300px");
    }
    
    var isFloatingObj = null;
    var isInTDorTable = false;
    function mousein(obj)
    {
    	var t = obj.getElementsByTagName("TABLE")[0];
    	if(isFloatingObj!=null && isFloatingObj!=t){
    		isFloatingObj.style.display = "none";
    	}
    	
    	if(t.style.display == "none"){
	    	var po = getOffSetAll(obj);
	    	t.style.display = "";
	    	t.style.top = po[0] + obj.offsetHeight;
	    	t.style.left = po[1];
	    }
	    isFloatingObj = t;
	    isInTDorTable = true;
    }
    function mouseleave(obj)
    {
    	isInTDorTable = false;
    	window.setTimeout(delayOper,50);
    }
    
    function mouseinT(obj)
    {
    	isFloatingObj = obj;
    	isInTDorTable = true;
    }
    
    function mouseleaveT(obj)
    {
    	isInTDorTable = false;
    	window.setTimeout(delayOper,50);
    }
    
    function delayOper()
    {
    	if(isInTDorTable==false)
    	{
    		if(isFloatingObj!=null)
    		{
    			isFloatingObj.style.display = "none";
    			isFloatingObj = null;
    		}
    	}
    }
		function getOffSetAll(obj)
		{
			var objtop = obj.offsetTop;
			var objleft = obj.offsetLeft;
		
			var oParent = obj.offsetParent;
			if(oParent.tagName != "BODY")
			{
				objtop += oParent.offsetTop;
				objleft += oParent.offsetLeft;
				oParent = oParent.offsetParent;
			}
			return [objtop,objleft]
		}
    </script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a {
	background-color: #dffafc;
	border: 1px solid #ECE9D8;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
.jian {
	background-attachment: fixed;
	background-image: url(/images2/ws/bo2.gif);
	background-repeat: no-repeat;
	background-position: center top;
}
.jianback {
	background-attachment: fixed;
	background-image: url(/images2/ws/bo.gif);
	background-repeat: no-repeat;
	background-position: center top;
}
.b {
	background-attachment: fixed;
	background-image: url(/images2/ws/l.png);
	background-repeat: no-repeat;
	background-position: center center;
}
.s {
	background-attachment: scroll;
	background-image: url(/images2/ws/start.gif);
	background-repeat: no-repeat;
	background-position: center center;
}
.f {
	background-attachment: fixed;
	background-image: url(/images2/ws/finish.gif);
	background-repeat: no-repeat;
	background-position: center center;
}
-->
</style>
  </head>
  <body onload="init();" style="overflow:auto;scroll:auto">
    <div class="titleHeader">浏览审核状态</div>
    <div align="right"><img src="/images2/ws/end.gif" onclick="Expand(this)" title="收缩"/></div>
    <iframe name="basic_frm" id="basic_frm" style="width:100%;height:250px;display:block" src="about:blank"></iframe>
    <div id="result" align="center">
<%
RelationalParameter param = null;
RelationalResult result = null;

param = new RelationalParameter("equi_Ws_GetStartPointDataInfo",null,new String[]{idx});
result = OperateHelper.executeProc(request,param);

String[] dsInfo = (String[])result.getResultValue().get(0);
%>
	<table width="29%" height="127"  border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
	    <td height="127" align="center" class="s"><% out.write(dsInfo[0]); %><br>填单人:<% out.write(dsInfo[1]); %><br>填单时间:<% out.write(dsInfo[2]); %></td>
	  </tr>
	</table>
<%
param = new RelationalParameter("equi_Ws_GetOneDSAudioSteps",null,new String[]{idx});
result = OperateHelper.executeProc(request,param);
String isSerial = "1";

for(int i=0;i<result.getResultValue().size();i++)
{
	String[] DataInfo = (String[])result.getResultValue().get(i);
	
	param = new RelationalParameter("equi_Ws_GetJumpStationInfo",null,new String[]{DataInfo[10]});
	RelationalResult result1 = OperateHelper.executeProc(request,param);
	
	String[] CInfo = null;
	if(result1.getResultValue().size()>0)
	 CInfo = (String[])result1.getResultValue().get(0);
	
	if(isSerial.equals("1") || (isSerial.equals("0") && DataInfo[6].equals("1")))//串行 或者是并行，但是最后个并行节点
	{
		if(CInfo!=null && CInfo.length>0)
		{
%>
	<table width="50%"  border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
	    <td height="53" align="center" class="jian"><% out.write(CInfo[0]); %></td>
	  </tr>
	</table>
<%
		}
		else
		{
%>
	<table width="50%"  border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
	    <td height="53" align="center" class="<% if(DataInfo[2].equals("同意")) { %>jian<% } else { %>jianback <% } %>"></td>
	  </tr>
	</table>
<%
		}
	}

if(isSerial.equals("1") && DataInfo[6].equals("0")) //上一个串行，下一个并行
	out.write("<span noWrap='true'>");
	
if(isSerial.equals("0") && DataInfo[6].equals("1")) //上一个并行，下一个串行
	out.write("</span>");
	
isSerial = DataInfo[6];

if(DataInfo[11].equals("1"))
{
%>
		<table border="0" style="width:300px">
			<tr>
				<% if(DataInfo[2].equals("同意")){ %>
				<td class="a" align="center" onmouseenter="mousein(this);" onmouseleave="mouseleave(this);">位置:
				<% } else { %>	
				<td class="a" align="center">状态:
					<% 
					}
						if(DataInfo[2].equals("不同意"))
							out.write("回退到位置"); 
						out.write("“"+DataInfo[5]+"”结束"); 
						
						if(DataInfo[2].equals("同意"))
						{
					%>
					<table border="0" style="width:400px;display:none;position:absolute;background-color:#cccccc" onmouseenter="mouseinT(this);" onmouseleave="mouseleaveT(this)">
						<tr>
							<td style="width:33%" align="left">审核人签字:<% out.write(DataInfo[0]); %></td>
							<td align="left">审核时间:<% out.write(DataInfo[1]); %></td>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3" align="left" valign="top" style="height:50px">审核内容:<% out.write(DataInfo[3]); %></td>
						</tr>
						<tr>
							<td align="left">附属信息:</td>
							<td align="left" colspan="3">&nbsp;
							<%
							if(DataInfo[7].equals("1"))
							{
								out.write("<input type='button' onclick='openattach(\"<dsid>"+idx+"</dsid><aduioid>"+DataInfo[10]+"</aduioid>\")' value='附件'/>&nbsp;"); 
							}
								
							if(!DataInfo[8].equals("") && !DataInfo[8].equals("0") )
							{
								out.write("<input type='button' onclick='openadata(\""+DataInfo[8]+"\",\""+DataInfo[10]+"\")' value='附属信息'/>&nbsp;"); 
							}
								
							if(!DataInfo[9].equals("") && !DataInfo[9].equals("0") )
							{
								out.write("<input type='button' onclick='openadata2(\"<aduioid>"+DataInfo[10]+"</aduioid><dataid>"+DataInfo[9]+"</dataid><read>1</read><sb>0</sb>\")' value='附属信息'/>&nbsp;"); 
							}
							%>
							</td>
						</tr>
					</table>
					<%
					}
					%>
				</td>
			</tr>
		</table>
<%
}
else
{
%>
	<table width="29%" height="127"  border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
	    <td height="127" align="center" class="b">虚拟节点</td>
	  </tr>
	</table>
<%
}
}
if(dsInfo[3].equals("2"))
{
%>
	<table width="50%"  border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
	    <td height="53" align="center" class="jian"></td>
	  </tr>
	</table>
	<table width="29%" height="127"  border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
	    <td height="127" align="center" class="f">审核完成</td>
	  </tr>
	</table>
<%
}

if(isSerial.equals("0"))//最后一个节点还在循环并行状态
	out.write("</div>");
%>
    </div>
  </body>
</html>