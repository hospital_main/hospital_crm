<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>名称</th>
				<th>数据结构</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>               
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true' >
	                  <a href='javascript:void(0)'>
				              <xsl:attribute name="onclick" >
				              	changewsrole('<xsl:value-of select="../pk/pid[1]"/>')
				      			  </xsl:attribute>
	                  	<xsl:value-of select="."/>
	                  </a>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>  	
	</xsl:template>
</xsl:stylesheet>
