<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>选择</th>
				<th>合同编号</th>
				<th>合同名称</th>
				
				<th>签定日期</th>
				<th>供应商</th>
				<th>期号</th>
				<th>金额</th>
				<th>已付金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
		   		       		<td align='center'>
			    	  	  			<input type='radio' name='select' onclick='choose(this)'>
						              <xsl:attribute name="value" >
				                 	 <xsl:value-of select="../pk/id"/>
						      			  </xsl:attribute>
						      			  <xsl:attribute name="value1" >
				                 	 <xsl:value-of select="../pk/payment_id"/>
						      			  </xsl:attribute>
						      			  <xsl:attribute name="value2" >
				                 	 <xsl:value-of select="../pk/promise_id"/>
						      			  </xsl:attribute>
						      			</input>
				   					</td>
				   					<td align='center'><xsl:value-of select="."/></td>
					   				<!--td align='center'>
						   				<a href="#">
						    	    <xsl:attribute name='onclick'>
						    	      javascript:openDialog('equiContractEquip.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;', 'dialogWidth:850px;dialogHeight:650px', result)
						    	    </xsl:attribute>
						    	    <xsl:value-of select="."/>
						    	    </a>
		                </td-->
              	</xsl:when>
              		<xsl:when test="position()=7 ">
                	<td align="center"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
              	<xsl:when test="position()=8 ">
              	<td align="center"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
							
								<!--测试新增-->

								<!--xsl:when test="position()=7">
									<td><a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('contractequip_main.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/contract_no&gt;', 'dialogWidth:850px;dialogHeight:650px', result)
		                </xsl:attribute>管理</a>
		                </td>
								</xsl:when>
								<xsl:when test="position()=8">
									<td><a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('contractappe_main.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/contract_no&gt;', 'dialogWidth:850px;dialogHeight:650px', result)
		                </xsl:attribute>管理</a></td>
								</xsl:when>
								<xsl:when test="position()=9">
									<td><a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('contractpay_main.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/contract_no&gt;', 'dialogWidth:850px;dialogHeight:650px', result)

		                </xsl:attribute>管理</a></td>
								</xsl:when>
								<xsl:when test="position()=10">
									<td><a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('contractdoc_main.html?load=&lt;contract_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/contract_no&gt;', 'dialogWidth:850px;dialogHeight:650px', result)
		                </xsl:attribute>管理</a></td>
								</xsl:when-->
								<!--测试新增-->
								<xsl:otherwise>
									<td align='center'><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
