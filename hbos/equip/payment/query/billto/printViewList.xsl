<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
		  <thead>
		  	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		      <tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td noWrap="true" style="fontsize:subtitle;colspan:13;align:left"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      </tr>
		  		<tr noWrap="true" class="mainHead">
		        <td>发票单号</td>
		        <td>原始发票号</td>
						<td>开票日期</td>
						<td>供应商</td>
						<td>资产编码</td>
						<td>资产名称</td>
						<td>规格</td>
						<td>型号</td>
						<td>数量</td>
						<td>发票金额</td>
						<td>开票金额</td>
						<td>付款金额</td>
						<td>制单人</td>
						<td>审核人</td>
						<td>状态</td>
						<td>备注</td>
		  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	              <xsl:choose>
	                <xsl:when test="position()=1">
	                  <td><xsl:value-of select="."/></td>
              		</xsl:when>
									<xsl:when test="string(.)=''">
	                  <td></td>
              		</xsl:when>
	                <xsl:when test="position()=9 or position()=10 or position()=11 or position()=12 ">
	                  <td align='right'>
  	                		<xsl:value-of select="format-number(.,'#,##0.00')"/>  
  	              	</td>
	                </xsl:when>	                
	                <xsl:otherwise>	                	
	                  <td><xsl:value-of select="."/></td>
	                </xsl:otherwise>
	              </xsl:choose>
	          </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
	  	<tfoot>
	  		<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:16"/> 
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		    </tr>
	  	</tfoot>
	 </root> 	
	</xsl:template>
</xsl:stylesheet>