<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>年度</th>
				<th>月份</th>
				<th>摘要</th>
				<th>借方</th>
				<th>贷方</th>
				<th>余额</th>
				 
  		</tr>
  	</thead>
  	<tbody>
  	
			<xsl:for-each select="/root/tbody/tr">
				<tr>    
		 
         
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
	               
				        		<td noWrap='true'><xsl:value-of select="."/></td>
				        	 
				        	 
	            </xsl:when>
		    <xsl:when test="position()=5">
		              <td align="right" class="moneyCol">
	                            <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                      </td>
	            </xsl:when>
	            <xsl:when test="position()=4">
	              <td align="right" class="moneyCol">
                           <xsl:value-of select="format-number(.,'#,##0.00')"/>
                      </td>
	            </xsl:when>
	            <xsl:when test="position()=6">
		              <td align="right" class="moneyCol">
	                            <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                      </td>
	            </xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 		
	  </tbody> 	
	</xsl:template>
</xsl:stylesheet>