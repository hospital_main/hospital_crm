<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>日期</th>
				<th>单据号</th>
				<th>供应商</th>
				<th>摘要</th>
				<th>借方</th>
				<th>贷方</th>
				<th>余额</th>
				
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
			<xsl:for-each select="/root/tbody/tr">
				<tr>    
		                  <xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
				      	<td noWrap='true'><xsl:value-of select="."/></td>
				      </xsl:when>
				      <xsl:when test="position()=2">
				      	<td>
				      	  <a>
				      	    <xsl:attribute name="href">
										       javascript:openNextProgram("<xsl:value-of select="."/>|||<xsl:value-of select="../td[9]"/>|||<xsl:value-of select="../td[4]"/>")
									      </xsl:attribute><xsl:value-of select="."/>
									    </a>
		         </td>
				      </xsl:when>
							<xsl:when test="position()=5">
								 
	              <td align="right" class="moneyCol">
	              	  <xsl:if test="../td[5]!=0">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                     </xsl:if>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=6">
	              <td align="right" class="moneyCol">
                 <xsl:if test="../td[6]!=0">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                     </xsl:if>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=7">
	              <td align="right" class="moneyCol">
	              	   <xsl:if test="../td[7]!=0">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                     </xsl:if>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=8">
	              <td align="left" style="display:none">
                  <xsl:value-of select="."/>
                </td>
	            </xsl:when>
	            <xsl:when test="position()=9">
	              <td align="left" style="display:none">
                  <xsl:value-of select="."/>
                </td>
	            </xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 		
	  </tbody> 	
	</xsl:template>
</xsl:stylesheet>