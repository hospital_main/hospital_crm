<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
		  <thead>
		  	<tr noWrap="true" class="mainHead">
	      	<td noWrap="true" style="fontsize:maintitle;colspan:7"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      	<td style="display:none"/>
	      </tr>
	  		<tr noWrap="true" class="mainHead">
	        <td>日期</td>
					<td>单据号</td>
					<td>供应商</td>
					<td>摘要</td>
					<td>借方</td>
					<td>贷方</td>
					<td>余额</td>
	  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
					<tr>    
			      <xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
					      	<td noWrap='true'><xsl:value-of select="."/></td>
					      </xsl:when>
					      <xsl:when test="position()=2">
					      	<td>
										<xsl:value-of select="."/>
			         		</td>
					      </xsl:when>
								<xsl:when test="position()=5">
		              <td align="right" >
		              	<xsl:if test="../td[5]!=0">
	                  	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </xsl:if>
	                </td>
		            </xsl:when>
		            <xsl:when test="position()=6">
		              <td align="right" >
	                  <xsl:if test="../td[6]!=0">
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </xsl:if>
	                </td>
		            </xsl:when>
		            <xsl:when test="position()=7">
		              <td align="right" >
		              	<xsl:if test="../td[7]!=0">
	                  	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	                  </xsl:if>
	                </td>
		            </xsl:when>
		            <xsl:when test="position()=8">
		              <td align="left" style="display:none">
	                  <xsl:value-of select="."/>
	                </td>
		            </xsl:when>
		            <xsl:when test="position()=9">
		              <td align="left" style="display:none">
	                  <xsl:value-of select="."/>
	                </td>
		            </xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>			
				</xsl:for-each>   	
	  	</tbody>
	 </root> 	
	</xsl:template>
</xsl:stylesheet>