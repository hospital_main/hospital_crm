<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
    <root>
		  <thead>
		  	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
		  		<tr noWrap="true" class="mainHead">
						<td>资产卡号</td>
						<td>资产名称</td>
						<td>科室</td>
						<td>库房</td>
						<td>类别</td>
						<td>规格</td>
						<td>型号</td>
						<td>原值</td>
						<td>已付金额</td>
		  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position()=1">
	              <xsl:if test=". = '合计'">
				        		<td noWrap='true'><xsl:value-of select="."/></td>
				        	</xsl:if>
				        	 <xsl:if test=". != '合计'">
	              
	              <td>
									  <xsl:value-of select="."/>
                </td>
                </xsl:if>
	            </xsl:when>
                <xsl:when test="position()=8">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
	               <xsl:when test="position()=9">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>

	 </root> 	
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>