<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        
				<th>资产卡号</th>
				<th>资产名称</th>
				<th>科室</th>
				<th>库房</th>
				<th>类别</th>
				<th>规格</th>
				<th>型号</th>
				<th>原值</th>
				<th>已付金额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
         
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position()=1">
	              <xsl:if test=". = '合计'">
				        		<td noWrap='true'><xsl:value-of select="."/></td>
				        	</xsl:if>
				        	 <xsl:if test=". != '合计'">
	              
	              <td>
                  <a href="#">
									  <xsl:attribute name="onclick" >
									    javascript:openDialog('check.html?load=&lt;id&gt;<xsl:value-of select="../pk"/>&lt;/id&gt;','dialogWidth:450px;dialogHeight:350px',result)
									  </xsl:attribute>
									  <xsl:value-of select="."/>
									</a>
                </td>
                </xsl:if>
	            </xsl:when>
                <xsl:when test="position()=8">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
	               <xsl:when test="position()=9">
				            <td align='right'>
	              	    <xsl:value-of select="format-number(.,'#,##0.00')"/>
								    </td>
	              </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
		<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
