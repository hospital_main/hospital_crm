<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-7"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:4'></td>
        <td style="display:none"></td>
        <td style="display:none"></td>
        <td style="display:none"></td>
  		</tr>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:4'></td>
        <td style="display:none"></td>
        <td style="display:none"></td>
        <td style="display:none"></td>
  		</tr>
    	<tr noWrap='true'>
    		<td style='fontsize:subtitle'>付款单号</td>
        <td style='fontsize:subtitle'>供货单位</td>
        <td style='fontsize:subtitle'>发票号码</td>
        <td style='fontsize:subtitle'>金额合计</td>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
  			<tr>
  				<xsl:if test="td[4]!='合计'">  					
  				<xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test="position() = 1">
	            	<td  align="left" ><xsl:value-of select="."/></td>
	            </xsl:when>
	          	<xsl:when test="position() = 3">
	            	<td  align="left" ><xsl:value-of select="."/></td>
	            </xsl:when>
		  				<xsl:when test="position()=5">
		  					<td  align="center" ><xsl:value-of select="."/></td>
		  				</xsl:when>
		  				<xsl:when test="position()=6">
		  					<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		  				</xsl:when>
          	</xsl:choose>
          </xsl:for-each>  					
  				</xsl:if>
  				<xsl:if test="td[4]='合计'">
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test="position() = 1">
	            	<td  align="left" ><xsl:value-of select="."/></td>
	            </xsl:when>
	          	<xsl:when test="position() = 3">
	            	<td  align="left" ><xsl:value-of select="."/></td>
	            </xsl:when>
		  				<xsl:when test="position()=5">
		  					<td  align="center" ><xsl:value-of select="."/></td>
		  				</xsl:when>
		  				<xsl:when test="position()=6">
		  					<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		  				</xsl:when>
          	</xsl:choose>
          </xsl:for-each>          
          </xsl:if>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		<tfoot>
 			<tr noWrap='true'>
 				<td align="left" style='colspan:4'></td>
        <td style="display:none"></td>
        <td style="display:none" ></td>
        <td style="display:none" ></td>
  		</tr>
 		</tfoot>
 		</root>
	</xsl:template>
</xsl:stylesheet>