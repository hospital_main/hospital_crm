<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
<xsl:template match="/">
  <thead>
    <tr noWrap="true" class="mainHead">
      <th style="display:none"><input type="checkbox"/></th>
      <th>发票号</th>
      <th>供货单位</th>
      <th>开票日期</th>
      <th>采购员</th>
      <th style="display:none">序号</th>
      <th>金额</th>
    </tr>
  </thead>
  <tbody>
  <xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
  <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      	      </xsl:attribute>
    	    </input>
          </td>
        <xsl:for-each select="td">            
          <xsl:choose>
            <xsl:when test="position()=1">
              <td>
              <a tabindex='-1'><xsl:value-of select="."/></a>
              </td>
            </xsl:when>
            <xsl:when test="position()=5">
              <td style="display:none"><xsl:value-of select="."/></td>
            </xsl:when>
            <xsl:when test="position()=6">
              <td align='right'><xsl:value-of select="format-number(.,$VHMONEYFORMAT)"/></td>
            </xsl:when>
            <xsl:otherwise>
              <td><xsl:value-of select="."/></td>
            </xsl:otherwise>
          </xsl:choose>            
  	</xsl:for-each>
      </tr>
    </xsl:for-each>
  </tbody>
</xsl:template>
</xsl:stylesheet>
