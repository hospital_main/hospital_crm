<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
				<th noWrap="true">提交人</th>
				<th noWrap="true">提交日期</th>
				<th noWrap="true"  width="350">执行信息</th>
				
  		</tr>
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test=" position()=1">
          			<td height="60">
	          			   <xsl:value-of select="."/>
							   </td>
	          	</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>		                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>