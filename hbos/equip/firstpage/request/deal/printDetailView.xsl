<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<root>
    	<thead> 
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:2'></td>
	  			<td style="display:none" ></td>
	  		</tr>
	  		<tr noWrap='true'>
	        <td align='left'></td>
	  			<td align='left'></td>
	  		</tr>
	  		<tr noWrap='true'>
	        <td align='left'></td>
	  			<td align='left'></td>
	  		</tr>
	  		<tr noWrap='true'>
	        <td align='left'></td>
	  			<td align='left'></td>
	  		</tr>
	  		<tr noWrap='true'>
	        <td align='left'></td>
	  			<td align='left'></td>
	  		</tr>
	  		<tr noWrap='true'>
	        <td align='left'></td>
	  			<td align='left'></td>
	  		</tr>
	  		<tr noWrap='true'>
	        <td align='left'></td>
	  			<td align='left'></td>
	  		</tr>
	  		<tr noWrap='true'>
	        <td align='left'></td>
	  			<td align='left'></td>
	  		</tr>
	  		<tr noWrap='true'>
	        <td align='left'></td>
	  			<td align='left'></td>
	  		</tr>
	  		<tr noWrap='true'>
	        <td align='left'></td>
	  			<td align='left'></td>
	  		</tr>
	  		<tr noWrap='true'>
	        <td align='left'></td>
	  			<td align='left'></td>
	  		</tr>
	  		<tr noWrap='true' >
	        <td align='left' style="colspan:2"></td>
	  		</tr>
	  	</thead>
 		</root>
	</xsl:template>
</xsl:stylesheet>