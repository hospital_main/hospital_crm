<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
	<root>
    <thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		<xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test=" position()=1 ">
	    			<tr noWrap='true' class='mainHead'>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test=" position()!=3 and position()!=4">
		          			<td valign="middle"  >
				            	<xsl:value-of select="."/>
		          			</td>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			          	<xsl:when test=" position()=3">
		          			<td valign="middle"  >
				            	<xsl:value-of select="."/>
		          			</td>
			          	</xsl:when>
	              </xsl:choose>
		          </xsl:for-each>
		          <td>��ע</td>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
	    		<xsl:when test=" position()>1 ">
	    			<tr noWrap='true'>
		          <xsl:for-each select="td">
		          	<xsl:choose>
			            <xsl:when test=" position()=1 or position()=2">
			            	<td ><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:when test="position() >4">
			            	<td align="right" ><xsl:value-of select="."/></td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		          <xsl:for-each select="td">
		          	<xsl:choose>
		          		<xsl:when test=" position()=3 ">
			            	<td align="right">
			            		<xsl:value-of select="."/>
			            	</td>
			            </xsl:when>
		          	</xsl:choose>
		          </xsl:for-each>
		          <td/>
		  			</tr>
	    		</xsl:when>
        </xsl:choose>
   		</xsl:for-each>
  	</tbody>
 		<tfoot>
 			<tr noWrap='true'>
        <td align="left" style='align:left;fontsize:coltitle;colspan:colcount'>
        	<xsl:value-of select="/root/annex/bottomTitle_label"/>
        	<xsl:value-of select="/root/annex/bottomTitle_value"/>
        </td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
 		</tfoot>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>