<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<xsl:variable name="colNum_1" select="number(/root/tbody/tr[1]/td[1])"/>
  	<xsl:variable name="colNum_2" select="number(/root/tbody/tr[1]/td[2])"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		<tr noWrap="true" class="mainHead">
  			<td rowspan="2">类别编码</td>
				<td rowspan="2">类别名称</td>
				<td rowspan="2">期初余额</td>
				<td>
					<xsl:attribute name="colspan">
					  <xsl:value-of select="/root/tbody/tr[1]/td[1]" />
					</xsl:attribute>
					原值增加
				</td>
				<xsl:call-template name="repeat_col1">
  				<xsl:with-param name="times" select="$colNum_1"/>    
        </xsl:call-template>
				<td>
					<xsl:attribute name="colspan">
					  <xsl:value-of select="/root/tbody/tr[1]/td[2]" />
					</xsl:attribute>
					原值减少
				</td>
				<xsl:call-template name="repeat_col1">
  				<xsl:with-param name="times" select="$colNum_2"/>    
        </xsl:call-template>
				<td rowspan="2">期末余额</td>
			</tr>
			<tr noWrap="true" class="mainHead">
			  <td style="display:none"></td>
			  <td style="display:none"></td>
			  <td style="display:none"></td>
				<xsl:for-each select="/root/tbody/tr[1]/td">
					<xsl:choose>
						 <xsl:when test="position()=1 or position()=2 or position()=3 or position()=last()">
					</xsl:when>
					<xsl:otherwise>
							<td><xsl:value-of select="." /></td>
					</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
	 			<td style="display:none"></td> 
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			  <xsl:if test="position()>1">
					<tr>
						<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=1 or position()=2">
									 <td align='left'>	
										<xsl:value-of select="."/>
									 </td>
									</xsl:when>
									<xsl:otherwise>
									 <td align='right'>	
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									 </td>
									</xsl:otherwise>
								</xsl:choose>
							
						</xsl:for-each>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</tbody>
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
	<xsl:template name="repeat_col1">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 1">  
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template> 
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>