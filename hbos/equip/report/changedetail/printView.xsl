<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">  	
  	<root>
    	<thead>
			<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)"/>
			<xsl:variable name="cols" select="count(/root/tbody/tr[1]/td[text()='�ڳ����'])"/>
			<tr><td colspan='{$colNum}'></td>
    	<xsl:call-template name="repeat">
	  		<xsl:with-param name="times" select="$colNum - 1"/>
	    </xsl:call-template>
    	</tr>
			<xsl:for-each select="/root/tbody/tr[position() = 1]">
				<tr noWrap='true' class='mainHead'>
					<td rowspan='2' nowrap='true' ><xsl:value-of select="td[position()=1]"/></td>
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test="position() = 1">
	          	</xsl:when>
	          	<xsl:when test="position() mod $cols = 2">
	          		<td nowrap='true'>
	          			<xsl:attribute name="colspan"><xsl:value-of select="$cols"/></xsl:attribute>
	          			<xsl:value-of select="."/>
	          		</td>
	          	</xsl:when>
	          	<xsl:otherwise>
	          		<td style='display:none;'></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<tr noWrap='true' class='mainHead'>
   		<td style='display:none;'></td>
   		<xsl:for-each select="/root/tbody/tr[position()=2]/td[position() &gt; 1]">
          	<td nowrap='true' ><xsl:value-of select="."/></td>
   		</xsl:for-each>
   		</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
				<tr>
					<xsl:for-each select="td">					
					<xsl:choose>						
							<xsl:when test="position()=1">
								<td align='left'>	
									<a tabindex="-1">
										<xsl:value-of select="."/>
									</a>
								</td>
							</xsl:when>						
							<xsl:otherwise>
								<xsl:if test='. != ""'>
									<td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
								</xsl:if>
								<xsl:if test='. = ""'>
									<td align='right'></td>
								</xsl:if>
							</xsl:otherwise>
						
						</xsl:choose>
					</xsl:for-each>
					
				</tr>
			</xsl:for-each>
		</tbody>
		<tfoot>
	    <tr noWrap='true'>
	       <td colspan='{$colNum}'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum - 1"/>    
	        </xsl:call-template>
	  	</tr>
    </tfoot>
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>