<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="colNum" select="/root/tbody/tr[1]/td[last()]"/>
			<xsl:variable name="cols" select="count(/root/tbody/tr[1]/td[text()='�ڳ����'])"/>
			<xsl:for-each select="/root/tbody/tr[position() = 1]">
				<tr noWrap='true' class='mainHead'>
					<th rowspan='2' nowrap='true' ><xsl:value-of select="td[position()=1]"/></th>
          <xsl:for-each select="td">
          	<xsl:choose>
	          	<xsl:when test=" position() &gt; 1  and position()  mod $cols = 2">
	          		<th nowrap='true'>
	          			<xsl:attribute name="colspan"><xsl:value-of select="$cols"/></xsl:attribute>
	          			<xsl:value-of select="."/>
	          		</th>
	          	</xsl:when>
	          	<xsl:otherwise>
	          		
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<tr noWrap='true' class='mainHead'>
   		<xsl:for-each select="/root/tbody/tr[position()=2]/td[position() &gt; 1]">
          	<th nowrap='true' ><xsl:value-of select="."/></th>
   		</xsl:for-each>
   		</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
				<tr>
					<!--td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td-->
					<xsl:for-each select="td">
					
					<xsl:choose>
						
							<xsl:when test="position()=1">
								<td align='left'>	
									<a tabindex="-1">
										<xsl:value-of select="."/>
									</a>
								</td>
							</xsl:when>
						
							<xsl:otherwise>
								<td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:otherwise>
						
						</xsl:choose>
					</xsl:for-each>
					
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
