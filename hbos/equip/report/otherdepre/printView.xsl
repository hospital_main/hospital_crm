<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
				<td >资产分类</td>
				<td >临床类科室</td>
				<td >医技类科室</td>
				<td >医辅类科室</td>
				<td >医疗合计</td>
				<td >管理类科室</td>
				<td >总计</td>
	  		</tr>
	  	</thead>
	  	<tbody>
	    	<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
								  <td align='left'>	
									<xsl:value-of select="."/>
								 </td>
								</xsl:when>
								
								
								<xsl:when test="position()=3 or position()=2 or position()=4 or position()=5 or position()=6 or position()=7">
									<td align='right'>	
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</td>
										
								</xsl:when>
								
							
								<xsl:otherwise>
								 <td align='left'>	
									<xsl:value-of select="."/>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>