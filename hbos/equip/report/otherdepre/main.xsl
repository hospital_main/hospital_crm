<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<xsl:decimal-format NaN=''/>
		<thead>  
			<tr noWrap="true" class="mainHead">
				<th >资产分类</th>
				<th >临床类科室</th>
				<th >医技类科室</th>
				<th >医辅类科室</th>
				<th >医疗合计</th>
				<th >管理类科室</th>
				<th >总计</th>
			</tr>
			
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
								  <td align='left'>	
									<xsl:value-of select="."/>
								 </td>
								</xsl:when>
								
								
								<xsl:when test="position()=3 or position()=2 or position()=4 or position()=5 or position()=6 or position()=7">
									<td align='right'>	
											<xsl:value-of select="format-number(.,'#,##0.00')"/>
										</td>
										
								</xsl:when>
								
							
								<xsl:otherwise>
								 <td align='left'>	
									<xsl:value-of select="."/>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
