<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<tr noWrap='true' class='mainHead'>
        <th nowrap='true' rowspan='2'>变动类型</th>
        <th nowrap='true' rowspan='2'>业务类型</th>
		  	<th nowrap='true' rowspan='2'>资金来源</th>
		  	<xsl:variable name="colspant" select="count(/root/tbody/tr[position() = 1 ]/td[text()='合计'])"></xsl:variable>
		  	<xsl:for-each select="/root/tbody/tr[position()=1 ]/td[position()&gt;3 ]">
		  		<xsl:if test='position() mod $colspant = 1'>
						<th noWrap="true"><xsl:attribute name="colspan"><xsl:value-of select="$colspant"/></xsl:attribute><xsl:value-of select="." /></th>
					</xsl:if>
				</xsl:for-each>
      </tr>
      <tr noWrap='true' class='mainHead'>
        <xsl:for-each select="/root/tbody/tr[position()=2 ]/td[position()&gt;3 ]">
					<th noWrap="true"><xsl:value-of select="." /></th>
				</xsl:for-each>
      </tr>
    </thead>
    <tbody>
   		<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
   			<tr>
      	<xsl:variable name="postr"><xsl:value-of select="position() + 2"/></xsl:variable>
      	<xsl:variable name="td1"><xsl:value-of select="td[1]"/></xsl:variable>
      	<xsl:variable name="td2"><xsl:value-of select="td[2]"/></xsl:variable>
      	<xsl:variable name="td3"><xsl:value-of select="td[3]"/></xsl:variable>
      	<xsl:variable name="rowspan2"><xsl:value-of select="count(../tr[td[1]/text()=$td1 and (td[2]/text()=$td2 or $td2='')])"/></xsl:variable>
      	<xsl:choose>
      	<xsl:when test="../tr[position()=$postr - 1 ]/td[1]/text()=$td1">
      		<xsl:variable name="td2pre"><xsl:value-of select="../tr[position()=$postr - 1 ]/td[2]"/></xsl:variable>
          <xsl:choose>
    			<xsl:when test="$td2pre=$td2 or ($td2='' and $td2pre='')">
    				<xsl:for-each select="td">
              <xsl:choose>
	              <xsl:when test="position()=1">
	              	<td style="display:none"></td>
	              </xsl:when>
	              <xsl:when test="position()=2">
	              	<td style="display:none"></td>
	              </xsl:when>
	              <xsl:when test="position()=3">
	              	<td><xsl:value-of select="."/></td>
	              </xsl:when>
	              <xsl:otherwise>
	              	<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	              </xsl:otherwise>
          		</xsl:choose>
          	</xsl:for-each>
    			</xsl:when>
      		<xsl:otherwise>
    				<xsl:for-each select="td">
              <xsl:choose>
	              <xsl:when test="position()=1">
	              	<td style="display:none"></td>
	              </xsl:when>
	              <xsl:when test="position()=2">
	              	<td><xsl:if test="$td2!='' or ($td2='' and ($td1='期初余额' or $td1='期末余额'))"><xsl:attribute name="rowspan"><xsl:value-of select="$rowspan2"/></xsl:attribute></xsl:if>
	              		<xsl:value-of select="."/>
	              	</td>
	              </xsl:when>
	              <xsl:when test="position()=3">
	              	<td><xsl:value-of select="."/></td>
	              </xsl:when>
	              <xsl:otherwise>
	              	<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	              </xsl:otherwise>
          		</xsl:choose>
          	</xsl:for-each>
        	</xsl:otherwise>
        	</xsl:choose>
			  </xsl:when>
			  <xsl:otherwise>
			  	<xsl:variable name="rowspan1"><xsl:value-of select="count(../tr[td[1]/text()=$td1])"/></xsl:variable>
      		<xsl:for-each select="td">
            <xsl:choose>
	            <xsl:when test="position()=1">
	            	<td><xsl:attribute name="rowspan"><xsl:value-of select="$rowspan1"/></xsl:attribute><xsl:value-of select="."/></td>
	            </xsl:when>
	            <xsl:when test="position()=2">
	            	<td><xsl:attribute name="rowspan"><xsl:value-of select="$rowspan2"/></xsl:attribute>
	            		<xsl:value-of select="."/>
	            	</td>
	            </xsl:when>
	            <xsl:when test="position()=3">
	            	<td><xsl:value-of select="."/></td>
	            </xsl:when>
	            <xsl:otherwise>
	            	<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
	            </xsl:otherwise>
        		</xsl:choose>
        	</xsl:for-each>
        </xsl:otherwise>
        </xsl:choose>
        </tr>
   		</xsl:for-each>
    </tbody>
  </xsl:template>
  
</xsl:stylesheet>

