<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
       <xsl:for-each select="/root/tbody/tr[position() &lt; 3]">
       	<xsl:variable name="curRow" select="position()" />		
        <tr class='mainHead'>
        	<xsl:for-each select="td[position() &gt; 1]">
        		<xsl:variable name="curcode" select="."/>
		      	<xsl:variable name="curCol" select="position()" />	
		      	<xsl:variable name="colspan" select="count(/root/tbody/tr[$curRow]/td[.=$curcode])" />
		      	<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[$curCol + 1] = $curcode])" />
            <xsl:choose>		           
          		  <xsl:when test="$curRow=1 and .!=../td[$curCol] and position() &lt; 5">
		          		<th colspan='{$colspan}' rowspan='{$rowspan}' nowrap='true' ><xsl:value-of select="."/></th>
		          	</xsl:when>  
		          	<xsl:when test="$curRow=1 and .!=../td[$curCol] and position() &gt; 4">
		          		<th colspan='{$colspan}' nowrap='true' ><xsl:value-of select="."/></th>
		          	</xsl:when>      			
          			<xsl:when test=".!=../../tr[$curRow - 1]/td[$curCol + 1] and .!=../td[$curCol] and position() &lt; 5">
		          			<th rowspan='{$rowspan}' nowrap='true' ><xsl:value-of select="."/></th>
		          	</xsl:when> 
		          	<xsl:when test="$curRow != 1 and position() &gt; 4">
		          			<th><xsl:value-of select="."/></th>
		          	</xsl:when>         		         	
	          		<xsl:otherwise>
              	</xsl:otherwise>
            </xsl:choose> 
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
      	<xsl:variable name="curRow" select="position()" />
        <tr>
        	<xsl:for-each select="td[position() &gt; 1]">
        		<xsl:variable name="curcode" select="."/>
		      	<xsl:variable name="curCol" select="position()"/>
		      	<xsl:variable name="rowspan" select="count(/root/tbody/tr[td[$curCol + 1] = $curcode])" />
        		 <xsl:choose>	
        		 		<xsl:when test="(../td[1] = '50' or ../td[1] = '100') and position() = 2">
		          			<td nowrap='true' align='center' colspan='3'>
		          			<xsl:value-of select="."/>
		          			</td>
		          	</xsl:when>
		          	<xsl:when test="(../td[1] = '50' or ../td[1] = '100') and (position() = 3 or position() = 4)">
		          			
		          	</xsl:when>
        		 		<xsl:when test="$curRow=1 and position() &lt; 3">
		          		<td rowspan='{$rowspan}' nowrap='true' ><xsl:value-of select="."/></td>
		          	</xsl:when> 	
	           		<xsl:when test="$curRow != 1 and .!=../../tr[$curRow + 1]/td[$curCol + 1] and position() &lt; 3">
		          		<td rowspan='{$rowspan}' nowrap='true' ><xsl:value-of select="."/></td>
		          	</xsl:when>
		          	<xsl:when test="contains(.,'�ϼ�') and position() = 3">
		          			<td nowrap='true' align='center' colspan='2'>
		          			<xsl:value-of select="."/>
		          			</td>
		          	</xsl:when>
		          	<xsl:when test=".='' and position() = 4">
		          	</xsl:when>
		          	<xsl:when test="position() &gt; 2 and position() &lt; 5">
		          			<td nowrap='true' align='left'>
		          			<xsl:value-of select="."/>
		          		</td>
		          	</xsl:when>
		          	
		          	<xsl:when test="position() &gt; 4">
		          		<xsl:if test='.!=""'>
		          			<td nowrap='true' align='right'>
		          			<xsl:value-of select="format-number(.,'#,##0.00')"/>
		          			</td>
		          		</xsl:if>
		          		<xsl:if test='.=""'>
		          			<td nowrap='true' align='right'>
		          			<xsl:value-of select="format-number('0.00','#,##0.00')"/>
		          			</td>
		          		</xsl:if>
		          	</xsl:when>
		          	<xsl:otherwise>
		          		
              	</xsl:otherwise>
	            </xsl:choose>	
  			  </xsl:for-each>
  			</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>


