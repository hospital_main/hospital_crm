<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN='' />
  <xsl:template match="/">
  	<root>
    	<thead>
  		<tr noWrap="true" class="mainHead">
    			<td noWrap="true" style="fontsize:maintitle;colspan:21;align:center;border:none"/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    	</tr>
  		<tr noWrap='true' class='mainHead'>
        <!--
        <td>入库单号</td>
        -->
        <td>资产卡片号</td>
        <td>资产编号</td>
				<td>资产名称</td>
				<td>常用名</td>
				<td>资产归属</td>				
				<td>型号规格</td>
				<td>产地厂家</td>
				<td>购置日期</td>
				<td>所在部门</td>
				<td>资产状态</td>
				<td>处置日期</td>
				<td>经费来源</td>
				<td>部门类别</td>
				<td>所在地点</td>
				<td>备注</td>
				<td>原值</td>
				<td>单价</td>
				<td>数量</td>
				<td>金额</td>
				<td style ="display:none">归属</td>
      </tr>
     </thead>
  	<tbody>
  	 <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:for-each select="td">
           	<xsl:choose>
	          	<xsl:when test="position()=20 or position()=16">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:when>

	            <xsl:when test="position()=17">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=18">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=19">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
								
	              </td>
	            </xsl:when>
	            <xsl:when test="position()=20">
	              <td align='right' style ="display:none">
	              	<xsl:value-of select="."/>
								
	              </td>
	            </xsl:when>
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			
      </xsl:for-each>
    </tbody>
    <tfoot>
    
 		    <tr noWrap='true'>
		        <td style='fontsize:foot;colspan:21;align:left;border:none'></td>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
    			<td style='display:none'/>
  	  	</tr>
    		
    	</tfoot>
   </root>
	</xsl:template>
	
</xsl:stylesheet>