<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td) - 2"/>
  	<root>
    	<thead>
    	<tr><td colspan='{$colNum}'></td>
    	<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum - 1"/>    
	    </xsl:call-template>
    	</tr>
    	<tr noWrap='true' class='mainHead'>
    		<td nowrap='true' rowspan='2'>类别编码</td>
        <td nowrap='true' rowspan='2'>类别名称</td>
        <xsl:variable name="colspant" select="count(/root/tbody/tr[position() = 1 ]/td[text()='资产价值'])"></xsl:variable>
		  	<xsl:for-each select="/root/tbody/tr[position()=1 ]/td[position()&gt;4 ]">
		  		<xsl:if test='position() mod $colspant = 1'>
						<td noWrap="true">
								<xsl:attribute name="colspan"><xsl:value-of select="$colspant"/></xsl:attribute><xsl:value-of select="." />
							</td>
					</xsl:if>
					<xsl:if test='position() mod $colspant != 1'>
						<td style='display:none;'>
							
							</td>
					</xsl:if>
				</xsl:for-each>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<td style='display:none;'></td>
      	<td style='display:none;'></td>
        <xsl:for-each select="/root/tbody/tr[position()=2 ]/td[position()&gt;4 ]">
					<td noWrap="true"><xsl:value-of select="." /></td>
				</xsl:for-each>
      </tr>
    </thead>
    <tbody>
    	<xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
   			<tr>
    		<xsl:for-each select="td">
          <xsl:choose>
          	<xsl:when test="position()=3">
            </xsl:when>
            <xsl:when test="position()=1">
            	<td><xsl:value-of select="."/></td>
            </xsl:when>
            <xsl:when test="position()=4">
            </xsl:when>
            <xsl:when test="position()=2">
            	<td><xsl:value-of select="."/></td>
            </xsl:when>
            <xsl:otherwise>
            	<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
            </xsl:otherwise>
      		</xsl:choose>
      	</xsl:for-each>
        </tr>
   		</xsl:for-each>
    </tbody>
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>