<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/equip/archive/query/main.xsl,v 1.1 2012/03/12 01:47:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:47:35 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <!--th style="display:none"><input type="checkbox"/></th-->
        <th>日期</th>
        <th>单据类型</th>
		    <th>部门</th>
		    <th>数量</th>
		    <th>原值</th>
		    <th>资产卡片号</th>
		    <th>供应商</th>
		    <th>取得日期</th>
		    <!--th>已使用年限</th-->
		    <th>条码号</th>
		    <th>已折旧金额</th>
		    <th>单据编号</th>
		  </tr>  		
  	</thead>
  	<tbody>
  	<!--xsl:variable name="VHPRICEFORMAT" select="/root/annex/VHPRICEFORMAT"/>
    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/-->
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <!--td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td-->
          <xsl:for-each select="td">            
              <xsl:choose>
              <xsl:when test="position() =1">
			    			<td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position() =13">
			    			<td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position() =14">
			    			<td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		<xsl:when test="position() =15">
			    			<td style="display:none">
		              <xsl:value-of select="."/>
		            </td>
			    		</xsl:when>
			    		
			    		<xsl:when test="position()=6 or position()=11">
                <td align='right'>
			                <xsl:if test="../td[1]='A'">
			        	        <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
			      	        </xsl:if>
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </td>                  
                </xsl:when>
                <xsl:when test="position()=8">
                	<td>
                	    <xsl:if test="../td[1]='A'">
			        	        <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
			      	        </xsl:if>
	                  <a tabindex="-2">
	                    <xsl:attribute name="href" >
	                      javascript:openDialog('vendorMessage.html?load=&lt;ven_code&gt;<xsl:value-of select="../td[position()=13]"/>&lt;/ven_code&gt;', 'dialogWidth:800px;dialogHeight:500px', result)
	                    </xsl:attribute>
	                    <xsl:value-of select="."/>
	                  </a>                	
                	</td>                  
                </xsl:when> 
                <xsl:when test="position()=12">
                	<td>
                	  <xsl:if test="../td[1]='A'">
			        	        <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
			      	      </xsl:if>
			      	      <xsl:if test="'采购增加'=../td[3] or '有价调拨增加'=../td[3] or '无价调拨增加'=../td[3] or '租赁增加'=../td[3] or '其他增加'=../td[3] or '融资增加'=../td[3] or '盘盈增加'=../td[3] " >
					              <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('medicalEquip_update.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:960px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
<!--					           <xsl:if test="1=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('medicalEquip_update.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>
										 <xsl:if test="2=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('currencyAsset_update.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>
										 <xsl:if test="3=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('house_update.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>
										 <xsl:if test="4=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('soil_update.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>-->
							  	</xsl:if>
							  	<xsl:if test="'采购退货'=../td[3] or '转出退货'=../td[3]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('medicalEquip_back_update.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
<!--					           <xsl:if test="1=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('medicalEquip_back_update.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>
										 <xsl:if test="2=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('currencyAsset_back_update.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>-->
										 
							  	</xsl:if>
<!--							  	<xsl:if test="'转移增加'=../td[3] or '转移减少'=../td[3]" >
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('transfer_update.html?load=&lt;id&gt;<xsl:value-of select="../td[12]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
							  	<xsl:if test="'盘盈增加'=../td[3]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('checkSur_medicalEquip_update.html?load=&lt;detail_id&gt;<xsl:value-of select="../td[14]"/>&lt;/detail_id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
					          <xsl:if test="1=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('checkSur_medicalEquip_update.html?load=&lt;detail_id&gt;<xsl:value-of select="../td[14]"/>&lt;/detail_id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>
										 <xsl:if test="2=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('checkSur_currencyAsset_update.html?load=&lt;detail_id&gt;<xsl:value-of select="../td[14]"/>&lt;/detail_id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>
										 <xsl:if test="3=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('checkSur_house_update.html?load=&lt;detail_id&gt;<xsl:value-of select="../td[14]"/>&lt;/detail_id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>
										 <xsl:if test="4=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('checkSur_soil_update.html?load=&lt;detail_id&gt;<xsl:value-of select="../td[14]"/>&lt;/detail_id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>
							  	</xsl:if>
							  	<xsl:if test="'盘亏减少'=../td[3]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('checkLose_medicalEquip_update.html?load=&lt;detail_id&gt;<xsl:value-of select="../td[14]"/>&lt;/detail_id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
					          <xsl:if test="1=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('checkLose_medicalEquip_update.html?load=&lt;detail_id&gt;<xsl:value-of select="../td[14]"/>&lt;/detail_id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>
										 <xsl:if test="2=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('checkLose_currencyAsset_update.html?load=&lt;detail_id&gt;<xsl:value-of select="../td[14]"/>&lt;/detail_id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>
										 <xsl:if test="3=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('checkLose_house_update.html?load=&lt;detail_id&gt;<xsl:value-of select="../td[14]"/>&lt;/detail_id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>
										 <xsl:if test="4=../td[15]" >
							          <a href="#">
												  <xsl:attribute name="onclick" >
												    javascript:openDialog('checkLose_soil_update.html?load=&lt;detail_id&gt;<xsl:value-of select="../td[14]"/>&lt;/detail_id&gt;','dialogWidth:860px;dialogHeight:710px',result)
												  </xsl:attribute>
												  <xsl:value-of select="."/>
												</a>
										 </xsl:if>
							  	</xsl:if>-->
							  	<xsl:if test="'发放减少'=../td[3] or '有偿调拨减少'=../td[3] or '无价调拨减少'=../td[3] or '出让减少'=../td[3]  or '销售减少'=../td[3] or '出租减少'=../td[3] or '捐赠减少'=../td[3] or '科室退库'=../td[3] or '毁损减少'=../td[3] or '报废减少'=../td[3]">
					          <a href="#">
										  <xsl:attribute name="onclick" >
										    javascript:openDialog('waster_update.html?load=&lt;out_id&gt;<xsl:value-of select="../td[14]"/>&lt;/out_id&gt;','dialogWidth:850px;dialogHeight:650px',result)
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
							  	</xsl:if>
	                              	
                	</td>                  
                </xsl:when>
                <!--xsl:when test="position()=1">
                	<td>
	                  <a tabindex="-2">
	                    <xsl:attribute name="href" >
	                      javascript:openDialog('record.html?load=&lt;file_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/file_no&gt;', 'dialogWidth:800px;dialogHeight:600px', result)
	                    </xsl:attribute>
	                    <xsl:value-of select="."/>
	                  </a>                	
                	</td>                  
                </xsl:when>
                
                <xsl:when test="position()=8">
                	<td align="right">
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
	                </td>                  
                </xsl:when>
                <xsl:when test="position()=9">
                	<td align="right">
	                    <xsl:value-of select="."/>
	                </td>                  
                </xsl:when-->                                              
                <xsl:otherwise>
                  <td>
                  <xsl:if test="../td[1]='A'">
			        	        <xsl:attribute name="bgcolor">#E0FFFF</xsl:attribute>
			      	        </xsl:if>
			      	    <xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
