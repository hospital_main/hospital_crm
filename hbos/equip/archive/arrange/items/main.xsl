<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/equip/archive/arrange/items/main.xsl,v 1.1 2012/03/12 01:47:35 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:47:35 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style="display:none"><input type="checkbox"/></th>
				<th>项目编码</th>
				<th>项目名称</th>
				<th>是否生成条形码信息</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
            
              <xsl:choose>
                <xsl:when test="position()=1">
                  <td ><xsl:value-of select="."/></td >
                </xsl:when>
                <xsl:when test="position()=3">
                  <td align="center"><xsl:value-of select="."/></td >
                </xsl:when>
                <!--xsl:when test="position()=4">
                  <td ><a tabindex="-2">
                    <xsl:attribute name="href" >
                      javascript:openDialog('cardupdate.html?load=&lt;file_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/file_no&gt;', 'dialogWidth:560px;dialogHeight:600px', result)
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                  </a></td >
                </xsl:when>    
                <xsl:when test="position()=7">
                  <td align="right">
                    <xsl:value-of select="."/>
                  </td>
                </xsl:when> 
                <xsl:when test="position()=10">
                  <td ><a tabindex="-2">
                    <xsl:attribute name="href" >
                      javascript:openDialog('doc/main.html?load=&lt;file_no&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/file_no&gt;', 'dialogWidth:600px;dialogHeight:480px', result)
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                  </a></td >
                </xsl:when-->            
                <xsl:otherwise>
                  <td ><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>
            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
