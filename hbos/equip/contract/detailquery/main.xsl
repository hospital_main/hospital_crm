<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>合同编号</th>
				<th>合同名称</th>
				<th>原始合同编号</th>
				<th>签订日期</th>
				<th>付款期号</th>
				<th>合同付款金额</th>
				<th>合同结算方式</th>
				<th style="display:none">未付金额</th>
				<th>付款单号</th>
				<th>付款日期</th>
				<th>付款金额</th>
				<th>供应商</th>
				  
			</tr>
			 
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					 
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
									<td>
										<a tabindex="-1">
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=6 or position()=11">
									<td>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:when>
								<xsl:when test="position()=8">
									<td style="display:none">
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
