<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:10'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>		 
				<tr noWrap="true" class="mainHead">       
				<td>合同编号</td>
				<td>合同类型</td>
				<td>原始合同编号</td>
				<td>付款序号</td>
				<td>供应商名称</td>
				<td>本期应付金额</td>
				<td>已付金额</td>
				<td>付款日期</td>
				<td>合同总金额</td>
				<td>状态</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
									<td><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:when test="position()=6 or position()=7 or position()=9  ">
					                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
					             		</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			<tr>
				<td align="center">合　　计</td>
				<td></td><td></td><td></td><td></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00')"/></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00')"/></td>
				<td></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00')"/></td>
				<td></td>
			</tr>
		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>