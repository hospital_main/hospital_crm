<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>合同编号</th>
				<th>合同类型</th>
				<th>原始合同编号</th>
				<th>付款序号</th>
				<th>供应商名称</th>
				<th>本期应付金额</th>
				<th>已付金额</th>
				<th>付款日期</th>
				<th>合同总金额</th>
				<th>状态</th>
				<th>查看</th>
				<th>付款</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						
							<xsl:choose>
								<xsl:when test="position()=1">
									<td><xsl:value-of select="."/></td>
								</xsl:when>
								<xsl:when test="position()=6 or position()=7 or position()=9 ">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
					<td><a tabindex="-2">
										<xsl:attribute name="href">
		                  javascript:openDialog('../../contract/manager/contractpay_main.html?load=&lt;contract_no&gt;<xsl:value-of select="pk/contract_no"/>&lt;/contract_no&gt;', 'dialogWidth:750px;dialogHeight:550px', result)

		                </xsl:attribute>查看</a></td>
								<td><a tabindex="-2" href="#">
										<xsl:attribute name="onclick">
		                  pay("&lt;contract_no&gt;<xsl:value-of select="pk/contract_no"/>&lt;/contract_no&gt;&lt;contract_id&gt;<xsl:value-of select="pk/payment_id"/>&lt;/contract_id&gt;")
		                </xsl:attribute>付款</a></td>
				</tr>
			</xsl:for-each>
			<tr>
				<td align="center">合　　计</td>
				<td></td><td></td><td></td><td></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00')"/></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00')"/></td>
				<td></td>
				<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00')"/></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
