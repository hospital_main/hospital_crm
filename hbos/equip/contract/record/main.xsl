<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th nowrap='true'>合同编号</th>
        <th nowrap='true'>合同名称</th>
        <th nowrap='true'>签订日期</th>
        <th nowrap='true'>供应商</th>
        <th nowrap='true'>签订科室</th>
				<th nowrap='true'>合同金额</th>   
      </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">            
            <xsl:choose>
              <xsl:when test="position()=1">
                <td><a href="#">
									<xsl:attribute name="onclick" >
										 openDialog('update.html?load=&lt;ab&gt;<xsl:value-of select="."/>&lt;/ab&gt;','dialogWidth:850px;dialogHeight:500px')	
									</xsl:attribute>
									<xsl:value-of select="."/>
								</a>
								</td>                
              </xsl:when>
							<xsl:when test="position()=6">
              	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
           		</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>

