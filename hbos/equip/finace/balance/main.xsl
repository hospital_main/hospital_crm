<?xml version='1.0' encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th>资产卡片号</th>
				<th>资产名称</th>
        <th>资产编号</th>
				<th>使用状态</th>
				<th>型号规格</th>
				<th>记账日期</th>
				<th>是否折旧</th>
				<th>折旧年限</th>
				<th>月折旧率</th>
				<th>变动方式</th>
				<th>所属科室</th>
				<th>资产分类</th>
				<th>资金来源</th>
				<th>预计残值</th>
				<th>原值</th>
				<th>本月计提</th>
				<th>本年计提</th>
				<th>累计折旧</th>
				<th>资产净值</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:for-each select="td">
           	<xsl:choose>
           		<xsl:when test="position()=1">
           			<xsl:if test=".= 'ZZZZZZZZZZ'">
			            <td align="center" colspan="9">
			              合计
			            </td> 
			           </xsl:if>          		
           			<xsl:if test=".!= 'ZZZZZZZZZZ'">
			            <td align="left">
			              <xsl:value-of select="."/>
			            </td> 
			           </xsl:if>          		
			        </xsl:when>
			        
	          	<xsl:when test="position()&lt;8 ">
           			<xsl:if test="../td[1]= 'ZZZZZZZZZZ'">

			           </xsl:if>          		
           			<xsl:if test="../td[1]!= 'ZZZZZZZZZZ'">
			            <td align="left">
			              <xsl:value-of select="."/>
			            </td> 
			           </xsl:if>          		
	            </xsl:when>
	          	<xsl:when test="position()=8">
           			<xsl:if test="../td[1]= 'ZZZZZZZZZZ'">

			          </xsl:if>          		
           			<xsl:if test="../td[1]!= 'ZZZZZZZZZZ'">
			            <td align="left">
			              <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td> 
			           </xsl:if>          		
	            </xsl:when>
	            <xsl:when test="position()=9">
					<xsl:if test="../td[1]= 'ZZZZZZZZZZ'">

					</xsl:if>
					<xsl:if test="../td[1]!= 'ZZZZZZZZZZ'">
						<td align="left">
							<xsl:value-of select="format-number(.,'#,##0.0000')"/>
						</td>
					</xsl:if>
				</xsl:when>
	          	<xsl:when test="position() &gt;=14">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:when>
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr> 			
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet> 


