<?xml version='1.0' encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN='' />
  <xsl:template match="/">
	  <xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
			<tr noWrap='true'>
				<td style='fontsize:maintitle;colspan:colcount'></td>
				<xsl:call-template name="repeat">
					<xsl:with-param name="times" select="$colNum"/>
				</xsl:call-template>
			</tr>
			<tr noWrap="true" class="mainHead">
				<td>资产卡片号</td>
				<td>资产名称</td>
				<td>资产编号</td>
				<td>使用状态</td>
				<td>型号规格</td>
				<td>记账日期</td>
				<td>是否折旧</td>
				<td>折旧年限</td>
				<td>月折旧率</td>
				<td>变动方式</td>
				<td>所属科室</td>
				<td>资产分类</td>
				<td>资金来源</td>
				<td>预计残值</td>
				<td>原值</td>
				<td>本月计提</td>
				<td>本年计提</td>
				<td>累计折旧</td>
				<td>资产净值</td>
			</tr>
     </thead>
  	<tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:for-each select="td">
           	<xsl:choose>
           		<xsl:when test="position()=1">
           			<xsl:if test=".= 'ZZZZZZZZZZ'">
			            <td align="center" colspan="9">
			              合计
			            </td> 
			           </xsl:if>          		
           			<xsl:if test=".!= 'ZZZZZZZZZZ'">
			            <td align="left">
			              <xsl:value-of select="."/>
			            </td> 
			           </xsl:if>          		
			        </xsl:when>
			        
	          	<xsl:when test="position()&lt;8 ">
           			<xsl:if test="../td[1]= 'ZZZZZZZZZZ'">
										<td style='display:none'/>
			           </xsl:if>          		
           			<xsl:if test="../td[1]!= 'ZZZZZZZZZZ'">
			            <td align="left">
			              <xsl:value-of select="."/>
			            </td> 
			           </xsl:if>          		
	            </xsl:when>
	          	<xsl:when test="position()=8">
           			<xsl:if test="../td[1]= 'ZZZZZZZZZZ'">
									<td style='display:none'/>
			          </xsl:if>          		
           			<xsl:if test="../td[1]!= 'ZZZZZZZZZZ'">
			            <td align="left">
			              <xsl:value-of select="format-number(.,'#,##0.00')"/>
			            </td> 
			           </xsl:if>          		
	            </xsl:when>
				<xsl:when test="position()=9">
					<xsl:if test="../td[1]= 'ZZZZZZZZZZ'">
						<td style='display:none'/>
					</xsl:if>
					<xsl:if test="../td[1]!= 'ZZZZZZZZZZ'">
						<td align="left">
							<xsl:value-of select="format-number(.,'#,##0.0000')"/>
						</td>
					</xsl:if>
				</xsl:when>


				<xsl:when test="position() &gt;=14">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	              </td>
	            </xsl:when>
	            <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr> 			
      </xsl:for-each>
    </tbody>
 
   </root>
	</xsl:template>
	<xsl:template name="repeat">
		<xsl:param name="times" select="0"/>
		<xsl:if test="$times > 0">
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">
				<xsl:with-param  name="times" select="$times - 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>