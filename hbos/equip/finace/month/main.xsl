<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>  
			<tr noWrap="true" class="mainHead">
				<th rowspan="3">类别编码</th>
				<th rowspan="3">类别名称</th>
				<th colspan="15">原值</th>
				<th colspan="14">累计折旧</th>
				<th colspan="2">净值</th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th colspan="1">期初</th>
				<th colspan="1">原值变动</th>
				<th colspan="7">借方</th>
				<th colspan="5">贷方</th>
				<th rowspan="2">余额</th>
				<th rowspan="2">期初</th>
				<th colspan="6">借方</th>
				<th colspan="6">贷方</th>
				<th rowspan="2">余额</th>
				<th rowspan="2">期初</th>
				<th rowspan="2">期末</th>
			</tr>
			<tr noWrap="true" class="mainHead">
				<th>期初</th>
				<th>原值变动</th>
				<th>采购</th>
				<th>盘盈</th>
				<th>移库</th>
				<th>捐赠</th>
				<th>退货</th>
				<th>其他</th>
				<th>合计</th>
				<th>盘亏</th>
				<th>移库</th>
				<th>报废</th>
				<th>其他</th>
				<th>合计</th>
				
				<th>退货</th>
				<th>盘亏</th>
				<th>移库</th>
				<th>报废</th>
				<th>其他</th>
				<th>合计</th>
				<th>采购</th>
				<th>盘盈</th>
				<th>移库</th>
				<th>捐赠</th>
				<th>其他</th>
				<th>合计</th>
			</tr>
			
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1 ">
									<xsl:if test=". = '合计'">
										<td colspan="2" align="center"><xsl:value-of select="."/></td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td align='left'>
											<xsl:value-of select="."/>
										</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=2">
									<xsl:if test=". != ''">
										<td align='left'>
											<xsl:value-of select="."/>
										</td>
									</xsl:if>
								</xsl:when>

								<xsl:otherwise>
								 <td align='right'>	
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								 </td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>

			
		</tbody>
	</xsl:template>
</xsl:stylesheet>
