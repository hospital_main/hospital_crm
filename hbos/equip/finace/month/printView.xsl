<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
  		
  		<tr noWrap="true" class="mainHead">
				<td rowspan="3">类别编码</td>
				<td rowspan="3">类别名称</td>
				
				<td colspan="15">原值</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>

			
				
				<td colspan="14">累计折旧</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				
				<td colspan="2">净值</td>
				<td style="display:none"></td>
			</tr>
			<tr noWrap="true" class="mainHead">
			  <td style="display:none"></td>
			  <td style="display:none"></td>

			 	<td rowspan="2">期初</td>
				<td rowspan="2">原值变动</td>
				
				
				<td colspan="7">借方</td>
			
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
        		<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan="5">贷方</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td> 
				<td style="display:none"></td>
				<td rowspan="2">余额</td>
				
				<td rowspan="2">期初</td>
				<td colspan="6">借方</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td colspan="6">贷方</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td rowspan="2">余额</td>
				
				<td rowspan="2">期初</td>
				<td rowspan="2">期末</td>
			</tr>
			<tr noWrap="true" class="mainHead">
			  <td style="display:none"></td>
			  <td style="display:none"></td>
			  
			  <td style="display:none"></td>
				<td style="display:none"></td>
				<td>采购</td>
				<td>盘盈</td>
				<td>移库</td>
				<td>捐赠</td>
				<td>退货</td>
				<td>其他</td>
				<td>合计</td>

				<td>盘亏</td>
				<td>移库</td>
				<td>报废</td>
				<td>其他</td>
				<td>合计</td>
			  <td style="display:none"></td>
			  
			  <td style="display:none"></td>
				<td>退货</td>
				<td>盘亏</td>
				<td>移库</td>
				<td>报废</td>
				<td>其他</td>
				<td>合计</td>

				<td>采购</td>
				<td>盘盈</td>
				<td>移库</td>
				<td>捐赠</td>
				<td>其他</td>
				<td>合计</td>
			  <td style="display:none"></td>
			  
			  <td style="display:none"></td>
			  <td style="display:none"></td>				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1 ">
								<xsl:if test=". = '合计'">
									<td colspan="2" align="center"><xsl:value-of select="."/></td>
								</xsl:if>
								<xsl:if test=". != '合计'">
									<td align='left'>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:when>
							<xsl:when test="position()=2">
								<xsl:if test=". != ''">
									<td align='left'>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
								<xsl:if test=". = ''">
									<td style="display:none"></td>
								</xsl:if>
							</xsl:when>

							<xsl:otherwise>
								<td align='right'>
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>