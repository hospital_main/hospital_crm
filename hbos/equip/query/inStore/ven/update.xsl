<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr class="mainHead">
				<th noWrap="true">入库单号</th>
				<th noWrap="true">入库日期</th>
				<th noWrap="true">仓库</th>
				<th noWrap="true">业务类型</th>
				<th noWrap="true">库管员</th>
				<th noWrap="true">备注</th> 
				<th noWrap="true">金额</th>
  		</tr> 
  	</thead>
  	<tbody> 
  	  <xsl:for-each select="/root/tbody/tr">
	      <tr>
	        <xsl:for-each select="td">
	          <xsl:choose>
	          	<xsl:when test=" position()=1">
          			<td>
	          			   <a href="#">
										  <xsl:attribute name="onclick" >
											openDetailItems("<xsl:value-of select="../pk/*"/>")
										   
										  </xsl:attribute>
										  <xsl:value-of select="."/>
										</a>
			        </td>
	          	</xsl:when>
	          	<xsl:when test=" position()=7">
          			<td>
										    <xsl:value-of select="format-number(.,'#,##0.00')"/>
							      </td>
	          	</xsl:when>
			    		<xsl:otherwise>
			          <td align='left'>
	                <xsl:value-of select="."/>
	              </td>
	            </xsl:otherwise>		                        
	          </xsl:choose>
	        </xsl:for-each>
	      </tr>
	    </xsl:for-each>
   	</tbody>
	</xsl:template>
</xsl:stylesheet>