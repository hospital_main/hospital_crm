<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-5"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum - 1"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap="true" class="mainHead">
		    <td>供应商编号</td>
				<td>供应商名称</td>
				<td>单号</td>
				<td>金额</td>
				<td>资金来源</td>
				<td>备注</td>
				<td>单号明细</td>					
      </tr>
     </thead>
  	<tbody>
  	 <xsl:for-each select="/root/tbody/tr">
      	<xsl:variable name="rowindex" select="position()"/>
        <tr>
        		<xsl:for-each select="td">
           	<xsl:choose>
     				
	          	<xsl:when test="position()=1"> 
	            </xsl:when>
	          	<xsl:when test="position()=5">
	              <td align='right'>
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>	
	              </td>
	            </xsl:when>
	           <xsl:otherwise>
		            <td align="left">
		              <xsl:value-of select="."/>
		            </td>
	            </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
  			
      </xsl:for-each>
      <tr>
				<td style="border-style:none">合计：</td>
				<td style="border-style:none">　</td>
				<td style="border-style:none">　</td>
				<td style="border-style:none" align='right' ><xsl:value-of select="format-number(sum(/root/tbody/tr[td[2]!='']/td[5]),'#,##0.00')"/></td>
				<td style="border-style:none">　</td>
				<td style="border-style:none">　</td>
				<td style="border-style:none">　</td>
				<td style="border-style:none">　</td>
				<td style="border-style:none">　</td>
			</tr>
    </tbody>
    <tfoot>
    
 		    <tr noWrap='true'>
		        <td style='fontsize:foot;colspan:colcount;align:left;border:none'></td>
		  			<xsl:call-template name="repeat">
		  				<xsl:with-param name="times" select="$colNum"/>    
		        </xsl:call-template>
  	  	</tr>
    		
    	</tfoot>
   </root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>