<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    		<tr noWrap='true'>
    			<td style="fontsize:maintitle;colspan:colcount"></td>
		        <xsl:for-each select="/root/t2head/tr[1]/td[position() &gt; 2]">
				<td style="display:none">
				</td>
			</xsl:for-each>
	  	</tr>
	    	<tr noWrap="true" class="mainHead">
			<xsl:for-each select="/root/t2head/tr[1]/td[position() != 1]">
				<td nowrap="true">
					<xsl:value-of select="."/>
				</td>
			</xsl:for-each>
		</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<xsl:variable name="c" select="position()"/>

					<xsl:if test="/root/t2head/tr[2]/td[$c +1 ] != '2'">
						<td align="left">
							<xsl:value-of select="."/>
						</td>
					</xsl:if>
					<xsl:if test="/root/t2head/tr[2]/td[$c +1 ]='2'">
						<td align="right">
							<xsl:if test=". != '�ϡ�����'">
								<xsl:value-of select="format-number(.,'##,##0.00')"/>
							</xsl:if>
							<xsl:if test=". = '�ϡ�����'">
								<xsl:value-of select="."/>
							</xsl:if>
						</td>
					</xsl:if>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
		</tbody>
		    <tfoot>
 		    <tr noWrap='true'>
		        <td style='fontsize:foot;colspan:colcount;align:left;border:none'></td>
		  			<xsl:call-template name="repeat">
		  				<xsl:with-param name="times" select="$colNum"/>    
		        </xsl:call-template>
  	  	</tr>
    		
    	</tfoot>
 		</root>
	</xsl:template>
		<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>