<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:10'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
	        <td>资产卡片号</td>
	        <td>转移单号</td>
					<td>品牌</td>
					<td>资产名称</td>
					<td>型号</td>
					<td>原值</td>
					<td>转出部门</td>
					<td>转入部门</td>
					<td>转入日期</td>
					<td>卡片状态</td>
	  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	              <xsl:choose>
	                <xsl:when test="position()=1">
				            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
				            </td>
	                </xsl:when>
	                <xsl:when test="position()=6">
				            <td  noWrap='true' align="right">
		                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
				            </td>
	                </xsl:when>
				  			  <xsl:otherwise>
				            <td  noWrap='true' >
			                <xsl:value-of select="."/>
				            </td>
	                </xsl:otherwise>
	              </xsl:choose>
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>  	
	  	</tbody>
	  	<tfoot>
		    <tr noWrap='true'>
	        <td style='fontsize:foot;colspan:10;align:left;border:none'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
		  	</tr>
	  	</tfoot>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>