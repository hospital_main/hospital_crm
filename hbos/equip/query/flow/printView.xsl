<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:decimal-format NaN=""/> 
  <xsl:template match="/">
  <root>
  
    <thead>
    	<tr>
   		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">15</xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
    <tr noWrap='true' class='mainHead'>
    	 <td>卡片编码</td>
				<td>资产编码</td>
				<td>资产名称</td>
				<td>规格</td>
				<td>入库日期</td>
				<td>变动方式</td>
				<td>供应商</td>
				<td>仓库</td>
				<td>转出科室</td>
				<td>转入科室</td>
				<td>原值</td>
				<td>月折旧</td>
				<td>累计折旧</td>
				<td>变动金额</td>
				<td>维修费用</td>
				<td>资产总金额</td>
    </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>               
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=11 or position() = 12 or position() = 13 or position() = 14 or position() = 15 or position() = 16">
			            <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each> 
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>