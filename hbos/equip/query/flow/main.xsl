<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/equip/query/expire/main.xsl,v 1.1 2012/08/14 00:16:50 niuzhongyuan Exp $
 $Author: niuzhongyuan $
 $Date: 2012/08/14 00:16:50 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:decimal-format NaN=""/>  
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>卡片编码</th>
				<th>资产编码</th>
				<th>资产名称</th>
				<th>规格</th>
				<th>入库日期</th>
				<th>变动方式</th>
				<th>供应商</th>
				<th>仓库</th>
				<th>转出科室</th>
				<th>转入科室</th>
				<th>原值</th>
				<th>月折旧</th>
				<th>累计折旧</th>
				<th>变动金额</th>
				<th>维修费用</th>
				<th>资产总金额</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
          	 <xsl:choose>
          	 	
	             <xsl:when test="position()=11">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                 <xsl:when test="position()=12">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                 <xsl:when test="position()=13">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                 <xsl:when test="position()=14">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                 <xsl:when test="position()=15">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                 <xsl:when test="position()=16">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
			  		 <xsl:otherwise>
			            <td align="left">
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
