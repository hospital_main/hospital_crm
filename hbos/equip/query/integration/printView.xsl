<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<xsl:variable name="colNums" select="count(/root/t2head/tr[1]/td) - 1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNums"/>    
	        </xsl:call-template>
	  		</tr>
				<tr noWrap="true" class="mainHead">
					<xsl:for-each select="/root/t2head/tr[1]/td">
						<td>
							<xsl:value-of select="."/>
						</td>
					</xsl:for-each>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:variable name="c" select="position()"/>
							<xsl:if test="string-length($c)&gt;0">
								<xsl:if test="substring( /root/t2head/tr[1]/td[$c] ,string-length( /root/t2head/tr[1]/td[$c] ),1)=' '">
									<td align="right">
										<xsl:value-of select="format-number(.,'##,##0.00')"/>
									</td>
								</xsl:if>
								<xsl:if test="substring( /root/t2head/tr[1]/td[$c] ,string-length( /root/t2head/tr[1]/td[$c] ),1)!=' '">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:if>
							</xsl:if>
							<xsl:if test="string-length($c)=0">
								<td/>
							</xsl:if>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>