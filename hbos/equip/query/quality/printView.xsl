<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
    	<tr noWrap='true'>
        <td style='fontsize:maintitle;colspan:colcount'></td>
  			<xsl:call-template name="repeat">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
			<tr noWrap="true" class="mainHead">
				<td>设备名称</td>
				<td>是否执行质控</td>
				<td>未执行质控原因</td>
				<td>质控结果及说明</td>
				<td>质控执行人</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td align='left'>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								 <td align='left'>
									<xsl:value-of select="."/>
								 </td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
			<tr>
				<td colspan="5" rowspan="4">质控主管意见：</td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
			</tr>
			<tr>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
			</tr>
			<tr>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
			</tr>
			<tr>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
			</tr>
			<tr>
				<td colspan="5" rowspan="4">主任意见：</td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
			</tr>
			<tr>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
			</tr>
			<tr>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
			</tr>
			<tr>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
				<td style="display:none" ></td>
			</tr>
		</tbody>
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>