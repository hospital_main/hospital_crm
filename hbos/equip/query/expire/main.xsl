<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/equip/query/expire/main.xsl,v 1.1 2012/08/14 00:16:50 niuzhongyuan Exp $
 $Author: niuzhongyuan $
 $Date: 2012/08/14 00:16:50 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>资产卡号</th>
				<th>资产编码</th>
				<th>资产名称</th>
				<th>规格</th>
				<th>型号</th>
				<th>数量</th>
				<th>原值</th>
				<th>入库日期</th>
				<th>折旧年限</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <td >
                  <xsl:value-of select="."/>
            </td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
