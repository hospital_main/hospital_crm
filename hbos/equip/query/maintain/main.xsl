<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">  
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>计划保养设备名称</th>
				<th>保养单名称</th>
				<th>是否执行保养</th>
				<th>保养执行日期</th>
				<th>保养说明</th>
				<th>保养执行人</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td align='left'>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								 <td align='left'>
									<xsl:value-of select="."/>
								 </td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
