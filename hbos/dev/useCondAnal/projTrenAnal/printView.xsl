<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=' '/>
	<xsl:template match="/">
	<root>
		<colgroup>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
				<col style = 'width:100mm'/>
			</xsl:for-each>
		</colgroup>

		<thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td[position() &gt; 1])"/></xsl:attribute></td>
				<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 2]">
					<td style='display:none'/>
				</xsl:for-each>
			</tr>
			
			<tr noWrap='true' class='mainHead'>
				<xsl:for-each select="/root/tbody/tr[td[1] = '1' ]">
					<xsl:for-each select="td[position() &gt; 1 ]">
						<xsl:choose>
							<xsl:when test="position() &gt; 1">
								<td nowrap='true' valign="center">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td nowrap='true' valign="center">
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</xsl:for-each>
			</tr>
			
		</thead>

		<tbody>
			<xsl:for-each select="/root/tbody/tr[td[1] &gt; '1' ]">
				<tr>
					
					<xsl:for-each select="td[position() &gt; 1 ]">
						<xsl:choose>
							<xsl:when test="position() &gt; 2">
								<td align="right">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
	  			</xsl:for-each>
				</tr>
			</xsl:for-each>

		</tbody>
    </root>
	</xsl:template>
</xsl:stylesheet>
