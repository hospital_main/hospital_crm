<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<xsl:for-each select="/root/tbody/tr[td[1] = '1' ]">
					<xsl:for-each select="td[position() &gt; 2 ]">
						<xsl:choose>
							<xsl:when test="position() &gt; 3">
								<th nowrap='true' valign="center">
								<a href="#">
									<xsl:attribute name="onclick">
										openPieGraph(this)
									</xsl:attribute>
									<xsl:value-of select="."/>
								</a>
								</th>
							</xsl:when>
							<xsl:otherwise>
								<th nowrap='true' valign="center">
									<!--<xsl:attribute name="colspan"><xsl:value-of select="$headCol"/></xsl:attribute>-->
									<xsl:value-of select="." />
								</th>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</xsl:for-each>
			</tr>
			
		</thead>
		
		<tbody>
			<xsl:for-each select="/root/tbody/tr[td[1] &gt; '1' ]">
				<tr>
					<xsl:for-each select="td[position() &gt; 2 ]">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:when test="position()&gt;3">
								<td align="right">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
	  			</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
