<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
		<colgroup>
			<col style = 'width:200mm'/>
			<col style = 'width:200mm'/>
			<col style = 'width:200mm'/>
			<col style = 'width:200mm'/>
			<col style = 'width:200mm'/>
			<col style = 'width:200mm'/>
		</colgroup>
  
    <thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">6</xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
    <tr noWrap='true' class='mainHead'>
	  	<td nowrap='true' valign='middle'>评测名称</td>
	  	<td nowrap='true' valign='middle'>指标名称</td>
	  	<!--td nowrap='true' valign='middle'>目标值</td-->
	  	<td nowrap='true' valign='middle'>单位</td>
	  	<td nowrap='true' valign='middle'>采集部门</td>
	  	<td nowrap='true' valign='middle'>评价标准</td>
	  	<td nowrap='true' valign='middle'>计算公式</td>
    </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 3">
	                <!--td align="right">
	                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	  	          	</td-->
  	          	</xsl:when>
  	          	<xsl:when test="position() = 6">
	                <td align="left" style="width:220px;word-break;keep-all;">
	                	<xsl:value-of select="translate(., '&amp;', '&#13;&#10;')" />
	  	          	</td>
  	          	</xsl:when>
                <xsl:otherwise>                	
	                <td align="left">
	                	<xsl:value-of select="."/>
	                	<!--xsl:value-of select="substring-after(.,'|||')"/-->
	  	          	</td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>