<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
				<th><input type='checkbox'/></th>
				<th nowrap="true">基本指标代码</th>
				<th nowrap="true">基本指标名称</th>
				<th nowrap="true">说明</th>
				<th nowrap="true">性质</th>
				<th nowrap="true">状态</th>
				<th nowrap="true">对应函数名称</th>
				<th nowrap="true">计算公式</th>
      </tr>
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'>
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
								<xsl:attribute name="style">font-size:8px;</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
            <td>
              <xsl:choose>
              
                <xsl:when test="position()=1">
									<a tab="-1" href="#">
										<xsl:attribute name="onclick">
											openFactor("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
										</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
								</xsl:when>
								
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
     </tbody>
  </xsl:template>
</xsl:stylesheet>