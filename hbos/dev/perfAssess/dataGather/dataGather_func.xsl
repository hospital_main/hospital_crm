<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<!--xsl:for-each select="/root/tbody/tr[position() = 1]/td">
					<th noWrap="true"><xsl:value-of select="."/></th>
				</xsl:for-each-->
				<th>设备名称</th>
				<th>计量单位</th>
				<th>指标值</th>
				<th style="display:none">input_no</th>
			</tr>
		</thead>	
		<tbody>
			<!--xsl:for-each select="/root/tbody/tr[position() &gt; 1]">
				<tr>
					<xsl:for-each select="td">
						<td noWrap='true'>
							<xsl:value-of select="." />
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each-->
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() = 3">
								<td noWrap='true' align="right">
									<xsl:value-of select="format-number(.,'#,##0.0000')"/>
								</td>
							</xsl:when>
							<xsl:when test="position() = 4">
								<td noWrap='true' style="display:none">
									<xsl:value-of select="." />
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap='true'>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
