<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<xsl:for-each select="/root/tbody/tr[position() = 1]/td">
					<th noWrap="true"><xsl:value-of select="."/></th>
				</xsl:for-each>						
			</tr>
		</thead>	
		<tbody>
			<xsl:for-each select="/root/tbody/tr[position() &gt; 1]">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() > 2">
								<td noWrap='true' align="right">
									<xsl:value-of select="format-number(.,'#,##0.0000')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td noWrap='true'>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
