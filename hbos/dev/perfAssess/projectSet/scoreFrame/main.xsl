<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>指标名称</th>
				<th nowrap='true'>计量单位</th>
				<th nowrap='true'>极值</th>
				<th nowrap='true'>区间1</th>
				<th nowrap='true'>区间2</th>
				<th nowrap='true'>区间3</th>
				<th nowrap='true'>区间4</th>
				<th nowrap='true'>区间5</th>
				<th nowrap='true'>区间6</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
							<td>
								<xsl:value-of select="." />
							</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
