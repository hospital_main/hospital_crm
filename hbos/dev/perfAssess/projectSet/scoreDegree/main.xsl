<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>等级编码</th>
				<th nowrap='true'>等级名称</th> 
				<th nowrap='true'>分数界限</th> 
			</tr>           
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position() = 1">
							<td align="left">
								<a>
									<xsl:attribute name="href">#</xsl:attribute>
									<xsl:attribute name="onclick">
										javascript:loadData('<xsl:value-of select="."/>','<xsl:value-of select="../td[2]"/>','<xsl:value-of select="../td[3]"/>')
									</xsl:attribute>
									<xsl:value-of select="."/>
								</a>
							</td>
						</xsl:when>
						<xsl:when test="position() = 3">
							<td align="right"><xsl:value-of select="."/></td>
						</xsl:when>
						<xsl:otherwise>
							<td align="left"><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

