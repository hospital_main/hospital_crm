<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>指标名称</th>
				<th nowrap='true'>计量单位</th>
				<th nowrap='true'>极值</th>     
				<th nowrap='true'>基准值</th>
				<th nowrap='true'>目标值</th>
				<th nowrap='true'>最佳值</th>
				<th nowrap='true'>目标值上限</th>
				<th nowrap='true'>基准值上限</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:if test="position() !=1 ">
							<td align="center">
								<xsl:value-of select="." />
							</td>
						</xsl:if>
						<xsl:if test="position() =1 ">
							<td align="left">
								<xsl:value-of select="." />
							</td>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
