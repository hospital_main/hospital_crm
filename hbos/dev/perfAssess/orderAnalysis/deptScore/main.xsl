<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>名次</th>
				<th nowrap='true' width="100">科室代码</th>
				<th nowrap='true' width="150">科室名称</th>
				<th nowrap='true' width="150">科室分类</th>
				<th nowrap='true' width="130">科室属性</th>
				<th nowrap='true' width="130">考核分数</th>
				<th nowrap='true' width="130">分数等级</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
          	<xsl:choose>
          		<xsl:when test="position() = 1">
	              <td align="right">
	              	<xsl:value-of select="."/>
	  	        	</td>
  	          </xsl:when>
          		<xsl:when test="position() = 6">
	              <td align="right">
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	  	        	</td>
  	          </xsl:when>
          		<xsl:otherwise> 
          			<td>
            		  <xsl:value-of select="."/>
            		</td>
            	</xsl:otherwise>
            </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
