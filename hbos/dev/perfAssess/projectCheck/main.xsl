<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>评测名称</th>
				<th>指标名称</th>
				<!--th>目标值</th-->
				<th>单位</th>
				<th>采集部门</th>
				<th>评价标准</th>	
				<th>计算公式</th>								
			</tr>
		</thead>
  	<tbody>  	  
	    <xsl:for-each select="/root/tbody/tr">
	      <xsl:variable name="colCnt" select="position()"/>
	    	<tr>
	    		<xsl:variable name="colName" select="td[1]"/>
	    		<xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[1] = $colName])"/>
	    		<xsl:if test="$colCnt = 1">
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
								<a href="#">
									<xsl:attribute name="onclick">
										showDevWeight('<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>')
									</xsl:attribute>
			    				<xsl:value-of select="td[1]"/>
								</a>
	    			</td>
	    		</xsl:if>
	    		<xsl:if test="$colCnt &gt; 1">
		    		<xsl:if test="td[1] = ../tr[$colCnt - 1]/td[1]">
		    			<td style="display:none"/>
		    		</xsl:if>
		    		<xsl:if test="td[1] != ../tr[$colCnt - 1]/td[1]">
		    			<td noWrap="true">
		    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
								<a href="#">
									<xsl:attribute name="onclick">
										showDevWeight('<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>')
									</xsl:attribute>
			    				<xsl:value-of select="td[1]"/>
								</a>
		    			</td>
		    		</xsl:if>
	    		</xsl:if>
	    	  <xsl:for-each select="td[position() &gt; 1]">
	    	  	<xsl:choose>
	    	  		<xsl:when test="position()=5" >
								<td align="left" style="width:420px;word-break;keep-all;">
										<xsl:value-of select="." />
								</td>
							</xsl:when>													
	    	  		<xsl:when test="position()=2" >
								<!--td align="right" >
										<xsl:value-of select="." />
								</td-->
							</xsl:when>													
							<xsl:otherwise>
	    	  		<td><xsl:value-of select="."/></td>
	    	  		</xsl:otherwise>
	    	  	</xsl:choose>
	    	  </xsl:for-each>
	    	</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>
