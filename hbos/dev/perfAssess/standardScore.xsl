<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>KPI指标</th>
				<th>目标值</th>
				<th>计量单位</th>
				<th>采集部门</th>
				<th>评价标准</th>	
				<th>计算公式</th>								
			</tr>
		</thead>	
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td noWrap='true'>
									<xsl:value-of select="." />
								</td>							
							</xsl:when>	
							<xsl:when test="position()=2">
								<td noWrap='true'>
								   <xsl:if test = " ../td[15] = 1 " >
									<xsl:value-of select="." />
								   </xsl:if>
								   <xsl:if test = " ../td[15] = 0 " >
									
								   </xsl:if>
								</td>							
							</xsl:when>		
							<xsl:when test="position()=5 "  >
								<td align="left" style="width:250px;word-break;keep-all;">
								<p>
								  <xsl:if test = " ../td[15] = 0">
								  	<xsl:attribute name="style">display:none;</xsl:attribute>
								  </xsl:if>
									<xsl:if test="../td[7] ='U'">										
										<xsl:value-of select="." />
									</xsl:if>	
									<xsl:if test="../td[7] ='D'">										
										<a tabindex="-2" style="text-decoration:none" href="#" onclick="win_Open(this);">
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[9]"/></xsl:attribute>		
											<xsl:attribute name="appraise_method"><xsl:value-of select="../td[8]"/></xsl:attribute>	
											<xsl:attribute name="extremum"><xsl:value-of select="../td[10]"/></xsl:attribute>
											<xsl:attribute name="target_name"><xsl:value-of select="../td[11]"/></xsl:attribute>																																																																			
											<xsl:value-of select="." />  
										</a>
									</xsl:if>	
																			
								</p>
								</td>
							</xsl:when>	
								
							<xsl:when test="position()=6" >
								<td align="left" >										
										<a tabindex="-2" style="text-decoration:none" href="#" onclick="formula_win(this);" name="forumla_A">
											<xsl:attribute name="sequence_no"><xsl:value-of select="../td[9]"/></xsl:attribute>											
											<xsl:attribute name="gather_type"><xsl:value-of select="../td[12]"/></xsl:attribute>		
											<xsl:attribute name="expressions"><xsl:value-of select="../td[13]"/></xsl:attribute>	
											<xsl:attribute name="cal_method_type"><xsl:value-of select="../td[14]"/></xsl:attribute>	
											<xsl:attribute name="formula_chn"><xsl:value-of select="."/></xsl:attribute>																																																																																			
											<xsl:value-of select="." />  
										</a>
								</td>
							</xsl:when>								
							<xsl:when test="position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12 or position()=13 or position()=14 or position()=15" >
							</xsl:when>																				
							<xsl:otherwise>
								<td noWrap='true'>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
