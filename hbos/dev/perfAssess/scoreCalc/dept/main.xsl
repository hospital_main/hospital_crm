<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="deptList" select="/root/tbody/tr[1]/td[1]"/>
			<xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[1] = $deptList])"/>
			<tr noWrap='true' class='mainHead'>
				
				<th nowrap='true' rowspan="2" valign="center">科室代码</th>
				<th nowrap='true' rowspan="2" valign="center">科室名称</th>
				<xsl:for-each select="/root/tbody/tr[position() &lt;= $rowCnt]">
					<th nowrap='true' colspan="2"><xsl:value-of select="td[3]"/></th>
				</xsl:for-each>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<xsl:for-each select="/root/tbody/tr[position() &lt;= $rowCnt]">
					<th nowrap='true'>考核得分</th>
					<th nowrap='true'>分数等级</th>
				</xsl:for-each>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="Step" select="position() mod $rowCnt"/>

				<xsl:if test="$rowCnt = 1 or $Step = 1">
					<xsl:text disable-output-escaping="yes"><![CDATA[<tr><td noWrap="true">]]></xsl:text>
				</xsl:if>
			
				<xsl:if test="$rowCnt = 1 or $Step = 1">
					<xsl:value-of select="td[1]"/>
				</xsl:if>

				<xsl:if test="$rowCnt = 1 or $Step = 1">
					<xsl:text disable-output-escaping="yes"><![CDATA[</td>]]></xsl:text>
				</xsl:if>

				<xsl:if test="$rowCnt = 1 or $Step = 1">
					<xsl:text disable-output-escaping="yes"><![CDATA[<td noWrap="true">]]></xsl:text>
				</xsl:if>
			
				<xsl:if test="$rowCnt = 1 or $Step = 1">
					<xsl:value-of select="td[2]"/>
				</xsl:if>

				<xsl:if test="$rowCnt = 1 or $Step = 1">
					<xsl:text disable-output-escaping="yes"><![CDATA[</td>]]></xsl:text>
				</xsl:if>

				<td align="right">
					<a href="#">
						<xsl:attribute name="onclick">
							showDevScore('<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>')
						</xsl:attribute>
					 <xsl:value-of select="format-number(td[4],'#,#0.00')"/>
					</a>
        </td>
				<td>
					<xsl:value-of select="td[5]"/>
        </td>
				
				<xsl:if test="$rowCnt = 1 or $Step = 0">
					<xsl:text disable-output-escaping="yes"><![CDATA[</tr>]]></xsl:text>
				</xsl:if>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
