<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>设备编码</th>
				<th>设备名称</th>
				<th>权重</th>
				<th>考核后分数</th>
				<th>权重后得分</th>	
			</tr>
		</thead>	
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() = 3">
								<td align="right">
									<xsl:value-of select="." />
								</td>
							</xsl:when>
							<xsl:when test="position() = 4">
									<td align="right">
									<a href="#">
										<xsl:attribute name="onclick">
											showDevScore('<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>')
										</xsl:attribute>
				    				<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</a>
									</td>
							</xsl:when>
							<xsl:when test="position() = 5">
								<td align="right">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
									<td noWrap='true'>
										<xsl:value-of select="." />
									</td>
							 </xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
