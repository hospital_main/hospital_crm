<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th noWrap="true">评测名称</th>
				<th noWrap="true">分数</th>
				<th noWrap="true">指标名称</th>
				<th noWrap="true">计量单位</th>
				<!--th noWrap="true">目标值</th-->	
				<th noWrap="true">实际值</th>								
				<th noWrap="true">评价标准</th>								
				<th noWrap="true">分数</th>								
			</tr>
		</thead>	
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
	      <xsl:variable name="colCnt" select="position()"/>
	    	<tr>
	    		<xsl:variable name="colName" select="td[1]"/>
	    		<xsl:variable name="rowCnt" select="count(/root/tbody/tr[td[1] = $colName])"/>
	    		<xsl:if test="$colCnt = 1">
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[1]"/>
	    			</td>
	    			<td noWrap="true" align="right">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="format-number(td[2],'#,##0.00')"/>
	    			</td>
	    		</xsl:if>
	    		<xsl:if test="$colCnt &gt; 1">
		    		<xsl:if test="td[1] = ../tr[$colCnt - 1]/td[1]">
		    			<td style="display:none"/>
		    			<td style="display:none"/>
		    		</xsl:if>
		    		<xsl:if test="td[1] != ../tr[$colCnt - 1]/td[1]">
	    			<td noWrap="true">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="td[1]"/>
	    			</td>
	    			<td noWrap="true" align="right">
	    				<xsl:attribute name="rowspan"><xsl:value-of select="$rowCnt"/></xsl:attribute>
	    				<xsl:value-of select="format-number(td[2],'#,##0.00')"/>
	    			</td>
		    		</xsl:if>
	    		</xsl:if>
					<xsl:for-each select="td[position() &gt; 2]">
					  <xsl:choose>
					  	<xsl:when test="position() = 3">
	              <!--td align="right">
	              	<xsl:value-of select="format-number(.,'#,##0.0000')"/>
	  	        	</td-->
  	          </xsl:when>
						  <xsl:when test="position() = 5">
								<td style="width:380px;word-break;keep-all">
									<xsl:value-of select="." />
								</td>
						  </xsl:when>
						  <xsl:when test="position() = 6">
	              <td align="right">
	              	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	  	        	</td>
  	          </xsl:when>
						  <xsl:when test="position() = 4">
								<td align="right">
								<a href="#">
									<xsl:attribute name="onclick">
										showDevFactor("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
									</xsl:attribute>
			    				<xsl:value-of select="format-number(.,'#,##0.0000')"/>
								</a>
								</td>
						  </xsl:when>
						  <xsl:otherwise>
								<td noWrap='true'>
									<xsl:value-of select="." />
								</td>
						  </xsl:otherwise>
					  </xsl:choose>							
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
