<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>     
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>设备代码</th>
				<th nowrap='true'>设备名称</th>
				<th nowrap='true'>科室名称</th>
				<th nowrap='true'>考核分数</th>
				<th nowrap='true'>分数等级</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=4">
								<td align="right">
								<a href="#">
									<xsl:attribute name="onclick">
										showDevScore('<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>')
									</xsl:attribute>
			    				<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</a>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="." />
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
