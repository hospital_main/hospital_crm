<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=' '/>
	<xsl:template match="/">
	<root>
		<xsl:variable name="headRow" select="/root/tbody/tr[1]/td[2]" />
		<xsl:variable name="headCol" select="/root/tbody/tr[1]/td[3]" />
		<colgroup>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 3]">
				<col style = 'width:100mm'/>
			</xsl:for-each>
		</colgroup>

		<thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="count(/root/tbody/tr[1]/td[position() &gt; 3])"/></xsl:attribute></td>
				<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 4]">
					<td style='display:none'/>
				</xsl:for-each>
			</tr>
			
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true' valign="center">
					<xsl:attribute name="rowspan"><xsl:value-of select="$headRow"/></xsl:attribute>
					成本项目
				</td>
				<td nowrap='true' valign="center">
					<xsl:attribute name="rowspan"><xsl:value-of select="$headRow"/></xsl:attribute>
					成本类别
				</td>
				<xsl:for-each select="/root/tbody/tr[td[1] = '1' ]">
					<xsl:for-each select="td[position() &gt; 5]">
						<xsl:if test=" . != ''">
							<td nowrap='true' valign="center">
								<xsl:if test="$headRow=1">
									<xsl:attribute name="colspan"><xsl:value-of select="$headCol"/></xsl:attribute>
									<xsl:value-of select="." />
								</xsl:if>
								<xsl:if test="$headRow!=1">
									<xsl:attribute name="colspan"><xsl:value-of select="$headCol"/></xsl:attribute>
									<xsl:value-of select="." />
								</xsl:if>
							</td>
						</xsl:if>
					</xsl:for-each>
				</xsl:for-each>
			</tr>
			
			<xsl:for-each select="/root/tbody/tr[td[1] &lt;= $headRow and td[1] != '1' ]" >
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="td[position() &gt; 5]">
						<xsl:if test=" . != ''">
							<td nowrap='true' valign="center">
								<xsl:if test="$headRow=2">
									<xsl:attribute name="rowspan"><xsl:value-of select="substring-before(substring-after(.,';'),':')"/></xsl:attribute>
									<xsl:attribute name="colspan"><xsl:value-of select="substring-after(.,':')"/></xsl:attribute>
									<xsl:value-of select="substring-before(.,';')" />
								</xsl:if>

								<xsl:if test="$headRow=3">
									<xsl:variable name="tTitle" select="substring-before(.,';')" />
									<xsl:attribute name="rowspan"><xsl:value-of select="substring-before(substring-after(.,';'),':')"/></xsl:attribute>
									<xsl:attribute name="colspan"><xsl:value-of select="substring-after(.,':')"/></xsl:attribute>
									<xsl:choose>
										<xsl:when test="$tTitle='总收入' or $tTitle='金额'">
											<xsl:value-of select="substring-before(.,';')" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="substring-before(.,';')" />
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
							</td>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</thead>

		<tbody>
			<xsl:for-each select="/root/tbody/tr[td[1] &gt; 3]">
				<tr>
					<xsl:for-each select="td[position() &gt; 3 ]">
						<xsl:choose>
							<xsl:when test="position()&gt;3">
								<td align="right">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
	  			</xsl:for-each>
				</tr>
			</xsl:for-each>

		</tbody>
    </root>
	</xsl:template>
</xsl:stylesheet>
