<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<root>
		<colgroup>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
		</colgroup>

		<thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">13</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true' colspan='3'>设备信息</td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td nowrap='true' colspan='3'>收入数据</td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td nowrap='true' colspan='5'>成本数据</td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td nowrap='true' colspan='2'>保本点</td>
				<td style='display:none'/>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>设备名称</td>
				<td nowrap='true'>所属科室</td>
				<td nowrap='true'>设备类别</td>

				<td nowrap='true'>工作量</td>
				<td nowrap='true'>收入</td>
				<td nowrap='true'>单位收入</td>
				
				<td nowrap='true'>总成本</td>
				<td nowrap='true'>固定成本</td>
				<td nowrap='true'>变动成本</td>
				<td nowrap='true'>单位固定成本</td>
				<td nowrap='true'>单位变动成本</td>
				
				<td nowrap='true'>保本工作量</td>
				<td nowrap='true'>保本收入</td>
			</tr>           
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
					<xsl:choose>
						<xsl:when test="position() &gt; 3">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td align="left">
								<xsl:value-of select="."/>  
							</td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>

