<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' colspan='3'>设备信息</th>
				<th nowrap='true' colspan='3'>收入数据</th>
				<th nowrap='true' colspan='5'>成本数据</th>
				<th nowrap='true' colspan='2'>保本点</th>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>设备名称</th>
				<th nowrap='true'>所属科室</th>
				<th nowrap='true'>设备类别</th>
				<th nowrap='true'>工作量</th>
				<th nowrap='true'>收入</th>
				<th nowrap='true'>单位收入</th>
				<th nowrap='true'>总成本</th>
				<th nowrap='true'>固定成本</th>
				<th nowrap='true'>变动成本</th>
				<th nowrap='true'>单位固定成本</th>
				<th nowrap='true'>单位变动成本</th>
				<th nowrap='true'>保本工作量</th>
				<th nowrap='true'>保本收入</th>
			</tr>           
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<!--<td align='center'  style='display:none'>
					<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      		  </xsl:attribute>
    			</input>
        </td>-->
				<xsl:for-each select="td">   
					<xsl:choose>
						<xsl:when test="position() = 1">
							<td align="left">
								<!--<a href="#">
									<xsl:attribute name="onclick">
													openDialog('chart.html?load=&lt;dev&gt;<xsl:value-of select="../td[1]"/>&lt;/dev&gt;&lt;v1&gt;<xsl:value-of select="../td[12]"/>&lt;/v1&gt;&lt;v2&gt;<xsl:value-of select="../td[13]"/>&lt;/v2&gt;&lt;v3&gt;<xsl:value-of select="../td[8]"/>&lt;/v3&gt;', 'dialogWidth:650px;dialogHeight:480px', result)
								  </xsl:attribute>-->
									<xsl:value-of select="."/>
								<!--</a>-->
							</td>
						</xsl:when>
					
						<xsl:when test="position() = 4">
							<td align="right">
								<a>
									<xsl:attribute name="href">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>", "../earningsAnalyse/workNum")
									</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
								</a>
							</td>
						</xsl:when>
						<xsl:when test="position() = 5">
							<td align="right">
								<a>
									<xsl:attribute name="href">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>", "../earningsAnalyse/charge")
									</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
								</a>
							</td>
						</xsl:when>
						<xsl:when test="position() = 6">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
						</xsl:when>
						<xsl:when test="position() = 7">
							<td align="right">
								<a>
									<xsl:attribute name="href">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>", "../earningsAnalyse/cost")
									</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
								</a>
							</td>
						</xsl:when>
						<xsl:when test="position() = 8">
							<td align="right">
								<a>
									<xsl:attribute name="href">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>", "fixCost")
									</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
								</a>
							</td>
						</xsl:when>
						<xsl:when test="position() = 9">
							<td align="right">
								<a>
									<xsl:attribute name="href">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>", "changeCost")
									</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
								</a>
							</td>
						</xsl:when>
						<xsl:when test="position() > 9">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td align="left">
								<xsl:value-of select="."/>  
							</td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

