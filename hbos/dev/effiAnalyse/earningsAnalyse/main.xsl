<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' colspan='4'>设备信息</th>
				<th nowrap='true' colspan='5'>总收益</th>
				<th nowrap='true' colspan='3'>单位收益</th>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>设备名称</th>
				<th nowrap='true'>所属科室</th>
				<th nowrap='true'>设备类别</th>
				<th nowrap='true'>设备型号</th>
				<th nowrap='true'>收入</th>
				<th nowrap='true'>成本</th>
				<th nowrap='true'>收益</th>
				<th nowrap='true'>工作量</th>
				<th nowrap='true'>收益率</th>
				<th nowrap='true'>收入</th>
				<th nowrap='true'>成本</th>
				<th nowrap='true'>收益</th>
			</tr>           
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<!--<td align='center'  style='display:none'>
					<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      		  </xsl:attribute>
    			</input>
        </td>-->
				<xsl:for-each select="td">   
					<xsl:choose>
						<xsl:when test="position() = 5">
							<td align="right">
								<a>
									<xsl:attribute name="href">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>", "charge")
									</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
								</a>
							</td>
						</xsl:when>
						<xsl:when test="position() = 6">
							<td align="right">
								<a>
									<xsl:attribute name="href">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>", "cost")
									</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
								</a>
							</td>
						</xsl:when>
						<xsl:when test="position() = 7">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
						</xsl:when>
						<xsl:when test="position() = 8">
							<td align="right">
								<a>
									<xsl:attribute name="href">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>", "workNum")
									</xsl:attribute><xsl:value-of select="format-number(.,'#,##0.00')"/>
								</a>
							</td>
						</xsl:when>
						<xsl:when test="position() = 9">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00%')"/>
							</td>
						</xsl:when>
						<xsl:when test="position() > 9">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td align="left">
								<xsl:value-of select="."/>  
							</td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

