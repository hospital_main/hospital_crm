<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<root>
		<colgroup>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
				<col style = 'width:100mm'/>
		</colgroup>

		<thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">12</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true' colspan='4'>设备信息</td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td nowrap='true' colspan='5'>总收益</td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td nowrap='true' colspan='3'>单位收益</td>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>设备名称</td>
				<td nowrap='true'>所属科室</td>
				<td nowrap='true'>设备类别</td>
				<td nowrap='true'>设备型号</td>
				<td nowrap='true'>收入</td>
				<td nowrap='true'>成本</td>
				<td nowrap='true'>收益</td>
				<td nowrap='true'>工作量</td>
				<td nowrap='true'>收益率</td>
				<td nowrap='true'>收入</td>
				<td nowrap='true'>成本</td>
				<td nowrap='true'>收益</td>
			</tr>           
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
					<xsl:choose>
						<xsl:when test="position() &gt; 4">
							<td align="right">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td align="left">
								<xsl:value-of select="."/>  
							</td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</root>
	</xsl:template>
</xsl:stylesheet>

