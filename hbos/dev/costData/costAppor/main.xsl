<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
	  	<xsl:variable name="hasWork" select="/root/tbody/tr[1]/td[9]" />
      <tr noWrap='true' class='mainHead'>
				<th nowrap='true'>科室信息</th>
				<th nowrap='true'>设备类型</th>
				<th nowrap='true'>设备信息</th>
				<th nowrap='true'>设备属性</th>
				<xsl:if test="$hasWork = '1'">
					<th nowrap='true'>工作量类别</th>
				</xsl:if>
				<th nowrap='true'>成本类别</th>
				<th nowrap='true'>成本项目</th>
				<th nowrap='true'>成本金额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
				<tr>
          <xsl:for-each select="td[position() &lt; 9]">
             <xsl:choose>
               <xsl:when test="position()=8">
                 <td align="right">
                   <xsl:value-of select="format-number(.,'#,##0.00')"/>
                 </td>
               </xsl:when>
               <xsl:when test="position()=5">
								<xsl:if test="$hasWork = '1'">
	                 <td>
	                   <xsl:value-of select="."/>
	                 </td>
                 </xsl:if>
               </xsl:when>
               <xsl:otherwise>
                 <td>
                   <xsl:value-of select="."/>
	         			 </td>
              </xsl:otherwise>
			     </xsl:choose>
		  	  </xsl:for-each>
		  	</tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>
