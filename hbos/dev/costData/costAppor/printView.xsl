<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
  <root>
  	<xsl:variable name="hasWork" select="/root/annex/td[1]" />
		<colgroup>
			<col style = 'width:200mm'/>
			<xsl:if test="$hasWork = '1'">
				<col style = 'width:100mm'/>
			</xsl:if>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
		</colgroup>
  
    <thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="7 + $hasWork"/></xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<xsl:if test="$hasWork = '1'">
				<td style='display:none'/>
			</xsl:if>
		</tr>
      <tr noWrap='true' class='mainHead'>
				<td nowrap='true'>科室信息</td>
				<td nowrap='true'>设备类型</td>
				<td nowrap='true'>设备信息</td>
				<td nowrap='true'>设备属性</td>
				<xsl:if test="$hasWork = '1'">
					<td nowrap='true'>工作量类别</td>
				</xsl:if>
				<td nowrap='true'>成本类别</td>
				<td nowrap='true'>成本项目</td>
				<td nowrap='true'>成本金额</td>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td[position() &lt; 9]">
              <xsl:choose>
               <xsl:when test="position()=5">
								<xsl:if test="$hasWork = '1'">
	                 <td>
	                   <xsl:value-of select="."/>
	                 </td>
                 </xsl:if>
               </xsl:when>
              	<xsl:when test="position() = 8">
                	<td align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
  	          	</xsl:when>  	          	 	          	
                <xsl:otherwise>
                	<td align="left">
                  <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>