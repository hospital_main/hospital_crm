<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
  	<xsl:variable name="hasWork" select="/root/annex/td[1]" />
		<colgroup>
			<col style = 'width:200mm'/>
			<xsl:if test="$hasWork = '1'">
				<col style = 'width:100mm'/>
			</xsl:if>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
		</colgroup>
  
    <thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="11 + $hasWork"/></xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<xsl:if test="$hasWork = '1'">
				<td style='display:none'/>
			</xsl:if>
		</tr>
    <tr noWrap='true' class='mainHead'>
	  	<td nowrap='true' valign='middle'>录入时间</td>
	  	<td nowrap='true' valign='middle'>科室信息</td>
	  	<td nowrap='true' valign='middle'>设备信息</td>
	  	<td nowrap='true' valign='middle'>设备类别</td>
	  	<td nowrap='true' valign='middle'>设备属性</td>
	  	<xsl:if test="$hasWork = '1'">
	  		<td nowrap='true' valign='middle'>工作量信息</td>
	  	</xsl:if>
	  	<td nowrap='true' valign='middle'>成本类别</td>
	  	<td nowrap='true' valign='middle'>成本项目</td>
	  	<td nowrap='true' valign='middle'>成本金额</td>
	  	<td nowrap='true' valign='middle'>摘要说明</td>
	  	<td nowrap='true' valign='middle'>录入人</td>
	  	<td nowrap='true' valign='middle'>归集标志</td>
    </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 2 or position() = 3 or position() = 7 or position() = 8">
	                <td align="left">
	                	<xsl:value-of select="substring-after(.,'|||')"/>
	  	          	</td>
  	          	</xsl:when>
              	<xsl:when test="position() = 6">
              		<xsl:if test="$hasWork = '1'">
		                <td align="left">
		                	<xsl:value-of select="substring-after(.,'|||')"/>
		  	          	</td>
	  	          	</xsl:if>
  	          	</xsl:when> 
              	<xsl:when test="position() = 9">
                	<td align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
  	          	</xsl:when>  	          	 	          	
                <xsl:otherwise>
                	<td align="left">
                  <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>