<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
		<colgroup>
			<col style = 'width:200mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
		</colgroup>
  
    <thead>
    	<tr>
	   		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">4</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
	    <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true' valign='middle'>设备信息</td>
		  	<td nowrap='true' valign='middle'>工作编码</td>
		  	<td nowrap='true' valign='middle'>计量单位</td>
		  	<td nowrap='true' valign='middle'>数据</td>
	    </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 1 or position() = 2">
	                <td align="left">
	                	<xsl:value-of select="substring-after(.,'|||')"/>
	  	          	</td>
  	          	</xsl:when>
              	<xsl:when test="position() = 3">
	                <td align="left">
	                	<xsl:value-of select="."/>
	  	          	</td>
  	          	</xsl:when>
                <xsl:otherwise>
                	<td align="right">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>