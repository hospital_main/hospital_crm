<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
  <root>
  	<xsl:variable name="colCnt" select="count(/root/tbody/tr[1]/td)" />
		<colgroup>
			<xsl:for-each select="/root/tbody/tr[1]/td">
				<col style = 'width:200mm'/>
			</xsl:for-each>
		</colgroup>
  
    <thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="$colCnt"/></xsl:attribute></td>
				<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 1]">
					<td style='display:none'/>
				</xsl:for-each>
			</tr>
	    <tr noWrap='true' class='mainHead'>
				<xsl:for-each select="/root/tbody/tr[1]/td">
			  	<td nowrap='true' valign='middle'><xsl:value-of select="."/></td>
				</xsl:for-each>
	    </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position() &gt; 1]">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 1">
	                <td align="left">
	                	<xsl:value-of select="substring-after(.,'|||')"/>
	  	          	</td>
  	          	</xsl:when>
                <xsl:otherwise>
                	<td align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>