<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
		<colgroup>
			<col style = 'width:200mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
		</colgroup>
  
    <thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">8</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
	    <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true' valign='middle'>计量记录号</td>
		  	<td nowrap='true' valign='middle'>设备信息</td>
		  	<td nowrap='true' valign='middle'>计量日期</td>
		  	<td nowrap='true' valign='middle'>计量单位</td>
		  	<td nowrap='true' valign='middle'>计量证书</td>
		  	<td nowrap='true' valign='middle'>计量工时</td>
		  	<td nowrap='true' valign='middle'>计量费用</td>
		  	<td nowrap='true' valign='middle'>计量结果</td>
	    </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 2 or position() = 4">
	                <td align="left">
	                	<xsl:value-of select="substring-after(.,'|||')"/>
	  	          	</td>
  	          	</xsl:when>
              	<xsl:when test="position() = 6 or position() = 7">
                	<td align="right">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
  	          	</xsl:when>
                <xsl:otherwise>
                	<td align="left">
                  <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>