<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
		<colgroup>
			<col style = 'width:200mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
		</colgroup>
  
    <thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">6</xsl:attribute></td>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
			</tr>
	    <tr noWrap='true' class='mainHead'>
		  	<td nowrap='true' valign='middle'>保养记录号</td>
		  	<td nowrap='true' valign='middle'>设备信息</td>
		  	<td nowrap='true' valign='middle'>保养日期</td>
		  	<td nowrap='true' valign='middle'>保养工时</td>
		  	<td nowrap='true' valign='middle'>保养金额</td>
		  	<td nowrap='true' valign='middle'>保养说明</td>
	    </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 2 ">
	                <td align="left">
	                	<xsl:value-of select="substring-after(.,'|||')"/>
	  	          	</td>
  	          	</xsl:when>
              	<xsl:when test="position() = 4 or position() = 5">
                	<td align="right">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
  	          	</xsl:when>
                <xsl:otherwise>
                	<td align="left">
                  <xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>