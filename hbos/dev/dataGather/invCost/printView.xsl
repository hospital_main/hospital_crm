<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
  	<xsl:variable name="hasWork" select="/root/annex/td[1]" />
		<colgroup>
			<col style = 'width:200mm'/>
			<xsl:if test="$hasWork = '1'">
				<col style = 'width:100mm'/>
			</xsl:if>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
		</colgroup>
  
    <thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan"><xsl:value-of select="14 + $hasWork"/></xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<xsl:if test="$hasWork = '1'">
				<td style='display:none'/>
			</xsl:if>
		</tr>
    <tr noWrap='true' class='mainHead'>
	  	<td nowrap='true' valign='middle'>日期</td>
	  	<td nowrap='true' valign='middle'>设备信息</td>
	  	<td nowrap='true' valign='middle'>设备属性</td>
	  	<td nowrap='true' valign='middle'>材料类别</td>
	  	<td nowrap='true' valign='middle'>材料信息</td>
	  	<td nowrap='true' valign='middle'>规格型号</td>
	  	<td nowrap='true' valign='middle'>计量单位</td>
	  	<td nowrap='true' valign='middle'>是否收费</td>
	  	
	  	<xsl:if test="$hasWork = '1'">
	  		<td nowrap='true' valign='middle'>工作量信息</td>
	  	</xsl:if>
	  	<td nowrap='true' valign='middle'>价格</td>
	  	<td nowrap='true' valign='middle'>数量</td>
	  	<td nowrap='true' valign='middle'>金额</td>
	  	<td nowrap='true' valign='middle'>摘要</td>
	  	<td nowrap='true' valign='middle'>操作员</td>
	  	<td nowrap='true' valign='middle'>是否导入</td>
    </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 9">
	                
  	          	</xsl:when>
              	<xsl:when test="position() = 2 or position() = 4 or position() = 5">
	                <td align="left">
	                	<xsl:value-of select="substring-after(.,'|||')"/>
	  	          	</td>
  	          	</xsl:when>
              	<xsl:when test="position() = 12 or position() = 10 or position() = 11">
                	<td align="right">
	                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                  </td>
  	          	</xsl:when>  	          	 	          	
                <xsl:otherwise>
                	<td align="left">
                  	<xsl:value-of select="."/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>