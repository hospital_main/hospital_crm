<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
		<colgroup>
			<col style = 'width:150mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
		</colgroup>
  
    <thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">5</xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
    <tr noWrap='true' class='mainHead'>
	  	<td nowrap='true' valign='middle'>项目编码</td>
	  	<td nowrap='true' valign='middle'>项目名称</td>
	  	<td nowrap='true' valign='middle'>收入类型</td>
	  	<td nowrap='true' valign='middle'>收费类别</td>
	  	<td nowrap='true' valign='middle'>停用标志</td>
    </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 3">
	                <td align="left">
	                	<xsl:value-of select="substring-after(.,'|||')"/>
	  	          	</td>
  	          	</xsl:when>
  	          	<xsl:when test="position() = 4">
	                <td align="left">
	                	<xsl:value-of select="substring-after(.,'|||')"/>
	  	          	</td>
  	          	</xsl:when>
  	          	<xsl:when test="position() = 5">
	                <td align="left">
	                	<xsl:value-of select="substring-after(.,'|||')"/>
	  	          	</td>
  	          	</xsl:when>
                <xsl:otherwise>                	
	                <td align="left">
	                	<xsl:value-of select="."/>
	                	<!--xsl:value-of select="substring-after(.,'|||')"/-->
	  	          	</td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>