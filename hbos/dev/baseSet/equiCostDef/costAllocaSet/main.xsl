<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>项目编码</th>
				<th nowrap='true'>项目名称</th>
				<th nowrap='true'>工作量类别分摊方式</th>
				<th nowrap='true'>公共设备分摊方式</th>
				<th nowrap='true'>是否固定成本</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr noWrap='true'>
				<xsl:for-each select="td">
					
						<xsl:choose>
							<xsl:when test="position() = 3">
                <td>
                	<input type='text' class='inputSelect' load='devDecMod' required='true' extent='80'>
                		<xsl:attribute name="id">workDecType_<xsl:value-of select="../td[1]"/></xsl:attribute>
                		<xsl:attribute name="initValue"><xsl:value-of select="."/></xsl:attribute>
                	</input>
                </td>
              </xsl:when>
              <xsl:when test="position() = 4">
                <td>
                	<input type='text' class='inputSelect' load='devDecMod' required='true' extent='80'>
                		<xsl:attribute name="id">pubDecType_<xsl:value-of select="../td[1]"/></xsl:attribute>
                		<xsl:attribute name="initValue"><xsl:value-of select="."/></xsl:attribute>
                	</input>
                </td>
              </xsl:when>
              <xsl:when test="position() = 5">
                <td>
                	<input type='text' class='inputSelect' load='devTrueOrFalse' required='true' extent='60'>
                		<xsl:attribute name="id">isFix_<xsl:value-of select="../td[1]"/></xsl:attribute>
                		<xsl:attribute name="initValue"><xsl:value-of select="."/></xsl:attribute>
                	</input>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
						</xsl:choose>
					
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

