<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>设备信息</th>
				<th nowrap='true'>设备属性</th>
				<th nowrap='true'>设备类别</th>
				<th nowrap='true'>设备型号</th>
				<th nowrap='true'>折旧计算比例</th>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr noWrap='true'>
				<xsl:for-each select="td">
					
						<xsl:choose>
							
							<xsl:when test="position() = 5">
                <td>
                	<input type='text' class='inputDecimal' required='true' name="text">
                		<xsl:attribute name="id">deprRate_<xsl:value-of select="../td[1]"/></xsl:attribute>
                		<xsl:attribute name="name">text_<xsl:value-of select="../td[1]"/></xsl:attribute>
                		<xsl:attribute name="value"><xsl:value-of select="format-number(.,'#,##0.00')"/></xsl:attribute>
                	</input>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
						</xsl:choose>
					
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

