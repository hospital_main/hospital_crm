<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/dev/baseSet/equiInfo/equiInfo/detail.xsl,v 1.1 2012/03/12 01:47:14 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:47:14 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th><input type="radio"/></th>
				<th>卡片编码</th>
				<th>设备名称</th>
				<th>资产类别</th>
				<th>设备型号</th>
				<th>设备原值</th>
				<th>所在部门</th>
				<th>是否折旧</th>
				<th>是否分摊</th>
				<th>设备编码</th>
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='radio' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="name">radio_name</xsl:attribute>
      			  <xsl:attribute name="id">radio_EquiCard</xsl:attribute>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">            
              <xsl:choose>
                <xsl:when test="position()=5">
                  <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
                </xsl:when>
                <xsl:when test="position() &gt; 9">
                  <td style='display:none'><xsl:value-of select="."/></td>
                </xsl:when>
                <xsl:otherwise>
                  <td><xsl:value-of select="."/></td>
                </xsl:otherwise>
              </xsl:choose>            
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
