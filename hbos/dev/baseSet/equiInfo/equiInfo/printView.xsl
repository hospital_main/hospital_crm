<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
		<colgroup>
			<col style = 'width:150mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
			<col style = 'width:100mm'/>
		</colgroup>
  
    <thead>
    	<tr>
    		<td style="fontsize:maintitle;"><xsl:attribute name="colspan">10</xsl:attribute></td>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
			<td style='display:none'/>
		</tr>
    <tr noWrap='true' class='mainHead'>
	  	<td nowrap='true' valign='middle'>资产卡片号</td>
	  	<td nowrap='true' valign='middle'>设备编码</td>
	  	<td nowrap='true' valign='middle'>设备名称</td>
	  	<td nowrap='true' valign='middle'>设备类别</td>
	  	<td nowrap='true' valign='middle'>管理科室</td>
	  	<td nowrap='true' valign='middle'>设备属性</td>
	  	<td nowrap='true' valign='middle'>设备型号</td>
	  	<td nowrap='true' valign='middle'>设备原值</td>
	  	<td nowrap='true' valign='middle'>是否计提</td>
	  	<td nowrap='true' valign='middle'>是否分摊</td>
    </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
              	<xsl:when test="position() = 1">
	                <td align="left">
	                	<xsl:value-of select="../td[11]"/>
	  	          	</td>
  	          	</xsl:when>
              	<xsl:when test="position() = 6">
	                <td align="left">
	                	<xsl:value-of select="substring-after(.,'|||')"/>
	  	          	</td>
  	          	</xsl:when>
  	          	<xsl:when test="position() = 8">
	                <td align="right">
	                	<xsl:value-of select="format-number(.,'#,##0.00')"/>
	  	          	</td>
  	          	</xsl:when>
  	          	<xsl:when test="position() = 9">
	                <td align="left">
	                	<xsl:value-of select="substring-after(.,'|||')"/>
	  	          	</td>
  	          	</xsl:when>
  	          	<xsl:when test="position() = 10">
	                <td align="left">
	                	<xsl:value-of select="substring-after(.,'|||')"/>
	  	          	</td>
  	          	</xsl:when>
                <xsl:otherwise>                	
	                <td align="left">
	                	<xsl:value-of select="."/>
	                	<!--xsl:value-of select="substring-after(.,'|||')"/-->
	  	          	</td>
                </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
    </root>
  </xsl:template>
</xsl:stylesheet>