<?xml version='1.0' encoding="GBK" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="headRow" select="/root/tbody/tr[1]/td[2]" />
			<xsl:variable name="headCol" select="/root/tbody/tr[1]/td[3]" />
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true' valign="center">
					<xsl:attribute name="rowspan"><xsl:value-of select="$headRow"/></xsl:attribute>
					选择
				</th>
				<th nowrap='true' valign="center">
					<xsl:attribute name="rowspan"><xsl:value-of select="$headRow"/></xsl:attribute>
					设备名称
				</th>
				<th nowrap='true' valign="center">
					<xsl:attribute name="rowspan"><xsl:value-of select="$headRow"/></xsl:attribute>
					设备类别
				</th>
				<th nowrap='true' valign="center">
					<xsl:attribute name="rowspan"><xsl:value-of select="$headRow"/></xsl:attribute>
					开单科室
				</th>
				<th nowrap='true' valign="center">
					<xsl:attribute name="rowspan"><xsl:value-of select="$headRow"/></xsl:attribute>
					执行科室
				</th>
				<xsl:for-each select="/root/tbody/tr[td[1] = '1' ]">
					<xsl:for-each select="td[position() &gt; 7]">
						<xsl:if test=" . != ''">
							<th nowrap='true' valign="center">
								<xsl:if test="$headRow=1">
									<xsl:attribute name="colspan"><xsl:value-of select="$headCol"/></xsl:attribute>
									<a href="#">
										<xsl:attribute name="onclick">
											openPieGraphY(this,<xsl:value-of select="$headRow"/>,<xsl:value-of select="$headCol"/>)
										</xsl:attribute>
										<xsl:value-of select="." />
									</a>
								</xsl:if>
								<xsl:if test="$headRow!=1">
									<xsl:attribute name="colspan"><xsl:value-of select="$headCol"/></xsl:attribute>
									<xsl:value-of select="." />
								</xsl:if>
							</th>
						</xsl:if>
					</xsl:for-each>
				</xsl:for-each>
			</tr>
			
			<xsl:for-each select="/root/tbody/tr[td[1] &lt;= $headRow and td[1] != '1' ]" >
				<tr noWrap='true' class='mainHead'>
					<xsl:for-each select="td[position() &gt; 7]">
						<xsl:if test=" . != ''">
							<th nowrap='true' valign="center">
								<xsl:if test="$headRow=2">
									<xsl:attribute name="rowspan"><xsl:value-of select="substring-before(substring-after(.,';'),':')"/></xsl:attribute>
									<xsl:attribute name="colspan"><xsl:value-of select="substring-after(.,':')"/></xsl:attribute>
									<a href="#">
										<xsl:attribute name="onclick">
											openPieGraphY(this,<xsl:value-of select="$headRow"/>,<xsl:value-of select="$headCol"/>)
										</xsl:attribute>
										<xsl:value-of select="substring-before(.,';')" />
									</a>
								</xsl:if>

								<xsl:if test="$headRow=3">
									<xsl:variable name="tTitle" select="substring-before(.,';')" />
									<xsl:attribute name="rowspan"><xsl:value-of select="substring-before(substring-after(.,';'),':')"/></xsl:attribute>
									<xsl:attribute name="colspan"><xsl:value-of select="substring-after(.,':')"/></xsl:attribute>
									<xsl:choose>
										<xsl:when test="$tTitle='总收入' or $tTitle='金额'">
											<a href="#">
												<xsl:attribute name="onclick">
													openPieGraphY(this,<xsl:value-of select="$headRow"/>,<xsl:value-of select="$headCol"/>)
												</xsl:attribute>
												<xsl:value-of select="substring-before(.,';')" />
											</a>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="substring-before(.,';')" />
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
							</th>
						</xsl:if>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</thead>
		
		<tbody>
			<xsl:for-each select="/root/tbody/tr[td[1] &gt;= 4]">
				<tr>
					<td align="center">
						<input type='checkbox'/>
					</td>
					<xsl:for-each select="td[position() &gt; 3 ]">
						<xsl:choose>
							<xsl:when test="position()&gt;4">
								<td align="right">
									<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td>
									<xsl:value-of select="."/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
	  			</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
