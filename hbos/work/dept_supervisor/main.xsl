<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th width="10px" style='display:none'><input type='checkbox'/></th>
		  	<th nowrap='true'>部门</th>
		  	<th nowrap='true'>主管用户</th>
		  	<th nowrap='true'>职责描述</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	
        
        		<td align='center'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:10px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	              </xsl:attribute>
	            </input>
         		</td>
          
          <xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">

									<td align='left'>
										<a tabindex='-1'>
						                  <xsl:attribute name="href" >
						    	            javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:450px;dialogHeight:200px', result)
						  	          </xsl:attribute><xsl:value-of select="."/></a>
								 	</td>

              </xsl:when>

              <xsl:otherwise>
								<td>
		                 <xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

