<%@ page language="java" pageEncoding="GBK"%>


<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title></title>
	<STYLE> 
   .border-width{
    border-width: 0 0 0 0
    }
   
	</STYLE> 
	
  	
	<style media="print">
	.Noprint{DISPLAY:none}
	</style>
  	
  	<link rel="stylesheet" href="<%=path %>/hbos/work/form/kindeditor/plugins/code/prettify.css" />
	<script charset="GBK" src="<%=path %>/hbos/work/form/kindeditor/kindeditor.js"></script>
	<script charset="GBK" src="<%=path %>/hbos/work/form/kindeditor/lang/zh_CN.js"></script>
	<script charset="GBK" src="<%=path %>/hbos/work/form/kindeditor/plugins/code/prettify.js"></script>
	<script type="text/javascript">
		var editor;
	
		function init(){
			
		/*	var printFlag="";
			var addstr=document.URL;
			if(addstr.indexOf("<flag>")!=-1){
				printFlag=addstr.substring(addstr.indexOf("<flag>")+6,addstr.indexOf("</flag>"))
			}*/
			//打印正文
			KindEditor.ready(function(K){
					editor = K.create('div[name="printForm"]', {
							autoHeightMode: true,
							afterCreate: function() {
					            this.loadPlugin('autoheight');
					        },
							items:[]
						});
					//	prettyPrint();
					//	editor.fullscreen();//全屏
						editor.html(window.opener.document.getElementById("kindeditor").contentWindow.getHtml());
						editor.readonly(true);	
			});
			
			//处理意见
			document.getElementById("taskContent").innerHTML=window.opener.document.getElementById("content").contentWindow.document.getElementById("taskContent").innerHTML;
			document.getElementById("taskContent").innerHTML=document.getElementById("taskContent").innerHTML.replace("【增加附言】","");
			var reg=new RegExp("【回复】","g"); //创建正则RegExp对象   
			document.getElementById("taskContent").innerHTML=document.getElementById("taskContent").innerHTML.replace(reg,"");
		
			document.title=window.opener.example_title.value;
		}
		
	function changeTaskContentPrint(obj){
		//处理意见
		if(obj.checked){
			taskContent.style.display="block";
		}else{
			taskContent.style.display="none";
		}
	}
	
	function changeFormPrint(obj){
		//处理意见
		if(obj.checked){
			formTRId.style.display="block";
		}else{
			formTRId.style.display="none";
		}
	}	
	</script>
	 
  </head>
  
  <body onload="init();" >
  	<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%" align="center">
  	<tr  height="10px" valign="top"><td>
	<object id="WebBrowser" height="0" width="0" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" VIEWASTEXT ></object>
	<center class="noprint">
	  	<input type="checkbox" onclick="changeFormPrint(this);" checked/>正文
	  	<input type="checkbox" onclick="changeTaskContentPrint(this);"/>处理意见
	  	<input onclick="document.all.WebBrowser.ExecWB(6,1)" type="button" value="打印"/>
		<input onclick="document.all.WebBrowser.ExecWB(8,1)" type="button" value="页面设置"/>
		<input onclick="document.all.WebBrowser.ExecWB(7,1)" type="button" value="打印预览"/>
		<input onclick="document.all.WebBrowser.ExecWB(45,1)" type="button" value="关闭窗口"/>
		<hr/>
	</center>
	</td></tr>
	 
	<tr id="formTRId"><td valign="top">
		<!--表单-->
		<div  id="printForm" name="printForm"  style="width:100%;height:500px"></div>
	</td></tr>
	<tr><td valign="top">
		<!--处理意见-->
		<div  id="taskContent" name="taskContent" style="display:none;font-size:15px;font-weight: normal;" ></div>
	</td></tr>
	</table	
  </body>
</html>

