
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>操作时间</th>
				<th>操作人</th>
				<th>任务节点ID</th>
				<th>任务节点名称</th>
				<th>节点动作</th>
				<th>处理意见</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	 
        <tr>
          <!--td align='center'>                   
            <input type='checkbox' style='font-size:8px;' >
              <xsl:attribute name="value" >
               <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      		  </xsl:attribute>       		  
      		</input>  
          </td-->
            <xsl:for-each select="td">
            	
			    <td width="17%">
		             <xsl:value-of select="."/>
			   </td>
             
  			  </xsl:for-each>
  			</tr> 			
  		
   		</xsl:for-each>  
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
