/*
工作流发送实例方法
多个视图，用多个record元素
<record><is_main>0</is_main><view_code>mate_pay_main_wv</view_code><view_where>where pay_bill_id="+pay_bill_id+"</view_where></record>
is_main：0主表视图，1明细视图
view_code：视图名称
view_where：视图条件
*/
function workSend(workPara){
	var w=(screen.width-200); 
	var h=(screen.height-100);
	if(null=workPara || workPara==undefined || workPara==""){
		alert("请求参数为空！");
		return false;
	}
	var src=window.location.href;
	
	if(src==""){
		return false;
	}
	if(src.indexOf("hbos")!=-1){
		src=src.substring(src.indexOf("hbos")+5,src.length);
	}
	var srcWork="";
	for(var i=1;i<src.split("/").length;i++){
		srcWork+="../";
	}
	if(srcWork==""){
		return false;
	}
	var w=(screen.width-200); 
	var h=(screen.height-100);

   openDialog(srcWork+"/work/flow/example/main.html?load=<workPara>"+workPara+"</workPara>", "dialogWidth:"+w+"px;dialogHeight:"+h+"px",null,"true");
}  