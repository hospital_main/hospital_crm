
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>模板名称</th>
				<th>系统模块</th>
				<th>模板分类</th>
				<th>业务表单</th>
				<th>发布状态</th>
				<th>创建人</th>
				<th>创建时间</th>
				<th>操作</th>
				<th>模板描述</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	 
        <tr>
        <xsl:if  test="td[3]!='合 计'">
          <td align='center'>                   
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    		  </input>  
          </td>
          
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td align="left">			           
			                  <a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					           updateTemplate('<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			                </a>	                
			            </td>
                </xsl:when>
                
                <xsl:when test="position()=8">
			            <td align="left">			           
			                  <a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					           nodeQuery('<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			                </a>	                
			            </td>
                </xsl:when>
                
			  	<xsl:otherwise>
			            <td align="left">
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>        
          
          </xsl:if>
        </tr>   		
   		</xsl:for-each>  
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
