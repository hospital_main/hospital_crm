Ext.namespace("DesignConfigLoad");
var DesignConfigLoad = {
		//初始化布局
		init : function(){		
			tip = "提示";
		//全局变量
		currentBtn = "select";
		currentParam = "base";
		//--- 图形化设计 ---
		lineFlag = false;
		dragable = true;
		eventsrc = null;//eventsrc是当前触发事件的对象（节点对象）
		presrc = null; //前一个选中的对象
		xmlNode = null;//当前选中的XML节点
		x = 0,y = 0;  
		popeventsource = "";
		temp1 = 0;
		temp2 = 0;
		//--- 画线 ---
		//连线的源和目标
		srcRect=null,desRect=null;
		x0=0,x1=0,y0=0,y1=0;   //连线的头尾坐标
		fontX=0,fontY=0;	   //文字的坐标
		xml = null;//xml对象
		//各个节点所拥有的属性
		nodeParams = {
			base:["base"],//文本节点
		//	mail:["base","mailto"],//mail节点,屏蔽邮箱功能 pengjin
			process:["base","form"],//流程定义,"swimlane","sql","notice"
			start:["base"],//start、end节点,"sql","notice"
			task:["base","change","delegate"],//任务节点,"sql","notice","method"
			transition:["base","autoDelegate","case"]//transition,"sql","notice"
		}
		//虚线
		dashLine = null;
		//显示xml窗口
		xmlWin = null;
		designWin = null;
	
		//初始流程属性
		initProcess();
		
		//为流程区添加右键菜单
		var target = null;
		var contextmenu = new Ext.menu.Menu({
			id:'inceptUser',
			items:[{
				text: '节点动作',	
				iconCls: 'trans_action',
				handler: function(){
					showNodeActionWin(target);
				}
			},{
				text: '委派模式',	
				iconCls: 'trans_rotate',
				handler: function(){
					showNodeConvertWin(target);
				}
			},{
				text: '节点描述',	
				iconCls: 'messenger',
				handler: function(){
					showNodeDescriptionWin(target);
				}
			}]
		});
			
		var linemenu = new Ext.menu.Menu({
			id:'linemenuNodeWhere',
			items:[{
				text: '转换条件',	
				iconCls: 'swimlanemenu',
				handler: function(){
					showExpressionStrWin(target);
				}
			}]
		});
		
		Ext.getBody().on('mousedown',function(e){
			//判断是否是右键
			if(e.button != "2"){
				return false;
			}	
			
			target = e.target;
			//判断是否在center区域
			if(!Ext.get(target).findParent("div[id=center]",Ext.getBody()))
				return false;
			if (e.target.tagName.toLowerCase() == 'textbox') 
				target = event.srcElement.parentElement;
			if (e.target.tagName.toLowerCase() == 'b') 
				target = event.srcElement.parentElement.parentElement;			
			var tagName = target.tagName.toLowerCase();
			if (tagName!= "roundrect" && tagName!= "span"){
				return false;
			}
			//开始，结束节点屏右键
			if(event.srcElement.getElementsByTagName("B")[0]!=undefined){
				if(event.srcElement.getElementsByTagName("B")[0].firstChild.nodeValue=='Start' || event.srcElement.getElementsByTagName("B")[0].firstChild.nodeValue=='End'){
					return false;
				}
			}
		
		/*	for(var i=0;i<event.srcElement.childNodes.length;i++){
			//	alert(event.srcElement.childNodes[i].nodeValue);//获取文本值(流程开始)
			}*/
			
			//节点
			if(tagName == "roundrect"){
				contextmenu.showAt(e.getXY());
			}
			//连接线
			else if(tagName == "span"){
				ExpressionStr="";
				var nodes = xml.getElementsByTagName("transition");
				for(var i=0;i<nodes.length;i++){
					if(nodes[i].getAttribute("flowid") == target.title){
						var rootChilds = nodes[i].childNodes;
							for(var j=0;j<rootChilds.length;j++){
							ExpressionStr+=rootChilds[j].getAttribute("value")+"\r\n";
						}
						break;
					}
				}
				if(ExpressionStr!=""){		
					linemenu.showAt(e.getXY());
				}	
			}
		});
		//document.onselectstart = function(){return false;}//禁用复制事件
		document.onmousedown = downAction;  //开始移动
		document.onmouseup = upAction;  //结束移动
	},
	winShow : function(){
		designWin.show();
	}
};
function move(){
	return false;

}

//初始设置流程定义
function initProcess(){	
	presrc = null;
	if(xml == null){
		//节点计数器
		taskNum = 1;//任务
		lineNum = 1;//线
		endNum = 1;//结束
		boolNum = 1;//决策
		joinNum = 1;//合并
		forkNum = 1;//分支
		mailNum = 1;//邮箱
		//树列表计数器
		swimlaneNum = 1;//泳道
	}

	createDashLine();
//	showNodeParams(nodeParams.process,"流程定义","picon01");
	//创建xml对象
	createXml();
	//设置显示属性
//	XmlSetParams("process");

}
//根据点击的按钮设置参数
function setParams(){
	switch(currentBtn){
		case "select":
			dragable = true;
			break
		case "grid":
			break
		case "transition":
			lineFlag = true;
			break
		default:
	}
}
//左键按下时方法
function downAction(){
	//判断是否是左键被按下
	if(event.button != 1){
		return;
	}

	switch(currentBtn){
		case "select":		
			//拖动
	//		drags();
			//显示属性
	//		showParams();
			break
		case "transition":
			createLine();
			break
		default:
			createNode();
	}
}
//左键释放时方法
function upAction(){
	if(Ext.get(eventsrc)==null){
		return false;
	}
	
	if(!Ext.get(eventsrc).findParent("div[id=center]",Ext.getBody())){
		return false;
	}
	
	switch(currentBtn){
		case "select":
			dragable = false;
			break
		case "transition":
			drawLine();
			break
		default:
	}
}
//创建节点
function createNode(){

	if(!nodeOrNot()){
		return false;
	}	
	var node = document.createElement("v:roundrect");
	node.inset = '2pt,2pt,2pt,2pt';
	node.style.pixelLeft = event.x + Ext.getDom("center").parentNode.scrollLeft;
	node.style.pixelTop = event.y + Ext.getDom("center").parentNode.scrollTop;
	node.style.zIndex = 1;
	node.style.pixelWidth = 110;
	node.style.pixelHeight = 50;
	node.strokeColor = "#27548d";
	node.fillcolor= '#EEEEEE';
	Ext.fly(node).addClass("node");
	switch(currentBtn){
		case "start":
			node.id = "start";
			node.title = "流程开始";
			node.flowtype = "start";
			node.innerHTML = "<v:shadow on='T' type='single' color='#b3b3b3' offset='2px,2px' />"
			+"<v:textbox class='node_start' inset='1pt,2pt,1pt,1pt'><b>Start</b><br />流程开始</v:textbox>";
			break
		case "task":		
			node.id = "task" + taskNum;
			node.title = "任务节点" + taskNum;
			node.flowtype = "task";
			node.innerHTML = "<v:shadow on='T' type='single' color='#b3b3b3' offset='2px,2px' />"
			+"<v:textbox class='node_task' inset='1pt,2pt,1pt,1pt'><b>TaskNode</b><br />任务节点"+taskNum+"</v:textbox>";
			taskNum ++;
			break
		case "end":
			node.id = "end" + endNum;
			node.title = "流程结束" + endNum;
			node.flowtype = "end";
			node.innerHTML = "<v:shadow on='T' type='single' color='#b3b3b3' offset='2px,2px' />"
			+"<v:textbox class='node_end' inset='1pt,2pt,1pt,1pt'><b>End</b><br />流程结束"+endNum+"</v:textbox>";
			endNum ++;
			break;
	/*	case "bool":
			node.id = "bool" + boolNum;
			node.title = "决策" + boolNum;
			node.flowtype = "decision";
			node.innerHTML = "<v:shadow on='T' type='single' color='#b3b3b3' offset='2px,2px' />"
			+"<v:textbox class='node_bool' inset='1pt,2pt,1pt,1pt'><b>Decision</b><br />决策"+boolNum+"</v:textbox>";
			boolNum ++;
			break;
		case "mail":
			node.id = "mail" + mailNum;
			node.title = "邮件" + mailNum;
			node.flowtype = "mail";
			node.innerHTML = "<v:shadow on='T' type='single' color='#b3b3b3' offset='2px,2px' />"
			+"<v:textbox class='node_mail' inset='1pt,2pt,1pt,1pt'><b>Mail</b><br />邮件"+mailNum+"</v:textbox>";
			mailNum ++;
			break; 屏蔽邮箱功能 pengjin */
		case "join":
			node.style.pixelWidth = 50;
			node.id = "join" + joinNum;
			node.title = "合并" + joinNum;
			node.flowtype = "join";
			node.innerHTML = "<v:shadow on='T' type='single' color='#b3b3b3' offset='2px,2px' />"
			+"<v:textbox class='node_join' inset='1pt,2pt,1pt,1pt'><br /></v:textbox>";
			joinNum ++;
			break;
		case "fork":
			node.style.pixelWidth = 50;
			node.id = "fork" + forkNum;
			node.title = "分支" + forkNum;
			node.flowtype = "fork";
			node.innerHTML = "<v:shadow on='T' type='single' color='#b3b3b3' offset='2px,2px' />"
			+"<v:textbox class='node_fork' inset='1pt,2pt,1pt,1pt'><br /></v:textbox>";
			forkNum ++;
			break;
		default:
	}
	document.getElementById("center").appendChild(node);
	addXmlNode(node);
}
//判断是否创建节点
function nodeOrNot(){
	//点击事件事是否发生在工作区
	//防止右键菜单弹出时点击阴影出错
	if(event.srcElement == null || event.srcElement.firstChild == null) return false;
	if(event.srcElement.firstChild.id != "center" && event.srcElement.id != "center")
		return false;
	//如果是start节点判断是否已经存在
	if(currentBtn == "start" && document.getElementById("start") != null){
		return false;
	}
	return true;
}
//将当前触发事件的节点内对象转为节点对象
function selectNode(){
	eventsrc = event.srcElement;
	if(event.srcElement.tagName.toLowerCase()=="line"){//线不可以点击，为了防止串色
		return;
	}
	
	//如果事件对象是textbox，将事件对象变为它的父对象
	if (event.srcElement.tagName.toLowerCase() == 'textbox') 
		eventsrc = event.srcElement.parentElement;
	//如果事件对象是b，将事件对象变为它的父对象的父对象
	if (event.srcElement.tagName.toLowerCase() == 'b') 
		eventsrc = event.srcElement.parentElement.parentElement;
	//如果是选择并且在center区域，执行下面的选中
	if(currentBtn == "select" && !!Ext.get(eventsrc).findParent("div[id=center]",Ext.getBody())){
		//如果前次选择与当前选中一致，不执行以下语句
		//alert(presrc.id + "," + eventsrc.id);
		if(presrc == eventsrc)
			return false;	
		if(presrc != null){
			if(presrc.tagName.toLowerCase() == "span"){
				presrc.style.border = "0";
			}else{
				presrc.strokeColor = "#27548d";
				presrc.strokeWeight = "1pt";
				presrc.style.zIndex = 1;
			}
		}
		switch(eventsrc.tagName.toLowerCase()){
			case "roundrect":
				eventsrc.strokeColor = "#ff0000";
				eventsrc.strokeWeight = "2pt";
				eventsrc.style.zIndex = 2;
				break
			case "line":
				eventsrc.strokeColor = "#ff0000";
				eventsrc.strokeWeight = "2pt";
				break
			case "span":
				eventsrc.style.border = "2px solid #ff0000";
				break
		}
		//将当前节点赋值给presrc
		presrc = eventsrc;
	}else if(currentBtn == "select"){
		
		if(presrc!=null&&eventsrc.tagName.toLowerCase()=="div"&&eventsrc.firstChild!=null&&eventsrc.firstChild.id=="center"||eventsrc.id =="center"){
			if(presrc.tagName.toLowerCase() == "span"){
				presrc.style.border = "0";
			}else{
				presrc.strokeColor = "#27548d";
				presrc.strokeWeight = "1pt";
				presrc.style.zIndex = 1;
			}
			presrc = null;
		}
	}
}

function lineMove(){
	//移动时的虚线随鼠标移动
	if(lineFlag){
		//判断是否有滚动条，有的话加上滚动条的滚动长度
		dashLine.to = (event.x+Ext.getDom("center").parentNode.scrollLeft) + "," + (event.y+Ext.getDom("center").parentNode.scrollTop);
		return false;
	}
}
//显示各节点对应的属性
function showNodeParams(params,title,icon){
	Ext.getCmp("paraPanel").setTitle(title);
	Ext.getCmp("paraPanel").setIconClass(icon);
	//隐藏所有属性
	Ext.get(Ext.DomQuery.select(".key_btn")).setDisplayed("none");

}
function drags(){
	if (event.button != 1){
		return;
	}	
	
	selectNode();

	if (eventsrc.tagName.toLowerCase() == 'roundrect'){
		dragable = true;
		temp1 = eventsrc.style.pixelLeft;
		temp2 = eventsrc.style.pixelTop;
		x = event.x;
		y = event.y;
		document.onmousemove = move;
	}
}
//创建虚线
function createDashLine(){
	if(document.getElementById("dashLine") == null){
		dashLine = document.createElement("v:line"); 
		dashLine.style.display = "none";
		dashLine.style.position = "absolute";
		dashLine.id = "dashLine";
		dashLine.strokeWeight = "2pt";
		dashLine.fillcolor = "#f441ff";
		dashLine.strokeColor = "#f441ff";
		dashLine.innerHTML = "<v:stroke dashstyle='longDash'/><v:shadow on='t' type='single' color='#cccccc' offset='1px,1px'/>";
		document.getElementById("center").appendChild(dashLine);
	}
}
function createLine() {
	selectNode();
	if (eventsrc.tagName == 'roundrect' && eventsrc.flowtype != "end"){
		srcRect = eventsrc;
		//将虚线显示，并将虚线的起点和终点设为点击事件对象的中心
		var dx = srcRect.style.pixelLeft + srcRect.style.pixelWidth / 2;
		var dy = srcRect.style.pixelTop + srcRect.style.pixelHeight / 2;
		dashLine.from =  dx + "," +	dy;
		dashLine.to =  dx + "," +	dy;
		dashLine.style.pixelLeft = dx + 'px';
        dashLine.style.pixelTop = dy + 'px';
		dashLine.style.display = "block";
		document.onmousemove = lineMove;
	}else{
		srcRect = null;
	}
}
function drawLine(){
	if(srcRect == null)
		return;
	selectNode();
	if (eventsrc.tagName == 'roundrect' && srcRect != eventsrc){
		desRect = eventsrc;
		//创建线
		//判断是否画线
		if(drawOrNot()){
			var line = document.createElement("v:line");
			direction();
			line.from = x0+","+y0;
			line.to = x1+","+y1;
			line.style.pixelLeft = x0 + 'px';
			line.style.pixelTop = y0 + 'px';
			line.style.position = "absolute";
			line.style.display = "block";
			line.id = "line" + lineNum;
			line.flowtype = "transition";
			line.strokeWeight = "1pt";
			line.style.cursor = "pointer";
			line.strokeColor = "#27548d";
			line.source = srcRect.id;
			line.project = desRect.id;
			//创建箭头
			line.innerHTML = "<v:stroke endarrow='Classic' />";
			document.getElementById("center").appendChild(line);
			//在连线上生成文字
			var font = createFont();
			line.title = font.innerHTML;
			lineNum ++;
			addXmlNode(line,srcRect);
		}
	}
	//onmouseup事件后隐藏虚线和取消移动事件
	dashLine.style.display = "none";
	document.onmousemove = null;
}
function reDrawLine(){
	var lines = Ext.DomQuery.select("line[project="+eventsrc.id+"]").concat(Ext.DomQuery.select("line[source="+eventsrc.id+"]"));
	for(var i=0;i<lines.length;i++){
		if(eventsrc.id == lines[i].source){
			//将源与目的赋值为线的源与目的
			srcRect = document.getElementById(lines[i].source);
			desRect = document.getElementById(lines[i].project);
			direction();
			lines[i].to = x1 + "," + y1;
			lines[i].from = x0 + "," + y0;
			//重新设置文本位置
			fontLocation();
			var font = Ext.DomQuery.select("span[title="+lines[i].id+"]")[0];
			if(font != null){
				modifyXmlNodeAttr(lines[i],"g",fontX+","+fontY);
				font.style.pixelLeft = fontX;
				font.style.pixelTop = fontY;
			}
		}
		if(eventsrc.id == lines[i].project){
			//将源与目的赋值为线的源与目的
			srcRect = document.getElementById(lines[i].source);
			desRect = document.getElementById(lines[i].project);
			var locations = direction();
			lines[i].to = x1 + "," + y1;
			lines[i].from = x0 + "," + y0;
			//重新设置文本位置
			fontLocation();
			var font = Ext.DomQuery.select("span[title="+lines[i].id+"]")[0];
			if(font != null){
				//在修改坐标前先修改XML中的坐标，否则改完后找不到相应的节点
				modifyXmlNodeAttr(lines[i],"g",fontX+","+fontY);
				font.style.pixelLeft = fontX;
				font.style.pixelTop = fontY;
			}	
		}		
	}
}
//判断是否画线
function drawOrNot(){
	//目的地址不能是start
	if(desRect.flowtype == "start"){
		return false;
	}	
	//是否已存在
	var lines = document.getElementsByTagName('line');
	for(var i=0;i<lines.length;i++){

		if(srcRect.id==lines[i].source && desRect.id==lines[i].project){//源节点、目标节点不能重复连接
			return false;
		}	
	/*	if(srcRect.id == "start" && lines[i].source == "start"){
			return false;
		}
		if( desRect.id==lines[i].project){//目标节点已经连接，返回
			return false;
		}*/	
		if(srcRect.id==lines[i].project && desRect.id==lines[i].source){//源节点与目标节点已经连接，不能返回连接（死循环）
			return false;
		}
		if(srcRect.id == "start" && desRect.id.indexOf("end")!=-1){
			return false;
		}
		
	/*	if( desRect.id==lines[i].project && desRect.id.indexOf("task")!=-1){//通过合并节点来合并，
			return false;
		}*/
	//	alert(srcRect.id+" -- "+ desRect.id+" -- "+lines[i].source+" -- "+lines[i].projec+" -- "+lines[i].from +" -- "+lines[i].to)
	
	}
	return true;
}
//在横线上生成文字
function createFont(){
	var textNode = document.createElement("span");
	fontLocation();
	textNode.style.pixelLeft = fontX;
	textNode.style.pixelTop = fontY;
//	textNode.innerHTML = "to " + desRect.title;
	textNode.innerHTML = "";
	Ext.fly(textNode).addClass("font_node");
	textNode.title = "line" + lineNum;
	textNode.id = "text" + lineNum;
	document.getElementById("center").appendChild(textNode);
	return textNode;
}
//判断文字的位置
function fontLocation(){
	fontX = Math.round(x0+(x1-x0)/2 - 30);
	fontY = Math.round(y0+(y1-y0)/2 - 25);
}
//箭头方向判断
function direction(){

	if (srcRect.style.pixelLeft > desRect.style.pixelLeft){
		if ((srcRect.style.pixelLeft - desRect.style.pixelLeft) <= desRect.style.pixelWidth){
			x0 = srcRect.style.pixelLeft + srcRect.style.pixelWidth / 2;
			x1 = desRect.style.pixelLeft + desRect.style.pixelWidth / 2;
			if (srcRect.style.pixelTop >  desRect.style.pixelTop){
				y0 = srcRect.style.pixelTop;
				y1 = desRect.style.pixelTop  + desRect.style.pixelHeight;
			}else{
				y0 = srcRect.style.pixelTop + srcRect.style.pixelHeight;
				y1 = desRect.style.pixelTop;
			}
		}else{
			x0 = srcRect.style.pixelLeft;
			x1 = desRect.style.pixelLeft + desRect.style.pixelWidth;
			y0 = srcRect.style.pixelTop + srcRect.style.pixelHeight / 2;
			y1 = desRect.style.pixelTop + desRect.style.pixelHeight / 2;
		}
	}else{
		if ((desRect.style.pixelLeft - srcRect.style.pixelLeft) <= desRect.style.pixelWidth){
			x0 = srcRect.style.pixelLeft + srcRect.style.pixelWidth / 2;
			x1 = desRect.style.pixelLeft + desRect.style.pixelWidth / 2;
			if (srcRect.style.pixelTop >  desRect.style.pixelTop){
			   y0 = srcRect.style.pixelTop;
			   y1 = desRect.style.pixelTop  + desRect.style.pixelHeight;
			}else{
			   y0 = srcRect.style.pixelTop + srcRect.style.pixelHeight;
			   y1 = desRect.style.pixelTop;
			}
		}else{
			x0 = srcRect.style.pixelLeft + srcRect.style.pixelWidth;
			x1 = desRect.style.pixelLeft;
			y0 = srcRect.style.pixelTop + srcRect.style.pixelHeight / 2;
			y1 = desRect.style.pixelTop + desRect.style.pixelHeight / 2;
		}
	}
}

//------------	xml操作    -------------
//创建xml
function createXml(){
	if(xml == null){
		xml = new ActiveXObject("Microsoft.XMLDOM");
		var p = xml.createProcessingInstruction("xml","version=\"1.0\" encoding=\"gb2312\"");
		xml.appendChild(p);
		var root = xml.createElement("process");
		root.setAttribute("name","新建流程");
		xml.appendChild(root);
	}
}
//查看xml
function showXml(){
	var str = xml.xml.replace(/></g,'>\n\r<');
	if(xmlWin !=null){
		Ext.getCmp("xmlTextArea").setValue(str);
		xmlWin.show();
		return false;
	}
	xmlWin = new Ext.Window({
		title: '查看XML',
		width: 700,
		layout: 'fit',
		iconCls: 'picon18',
		height: 450,
		modal: true,
		closeAction: 'hide',
		maximizable:'true',
		items: new Ext.form.TextArea({
			id: 'xmlTextArea',
			value: str
		}),
		buttons:[{
			text: '保存',
			iconCls: 'picon19',
			handler: function(){
				XmlToProcess(Ext.getCmp("xmlTextArea").getValue());
				xmlWin.hide();
			}
		},{
			text: '取消',
			iconCls: 'picon09',
			handler: function(){
				xmlWin.hide();
			}
		}]
	}).show();
}
//添加xml节点
function addXmlNode(node,parentNode){
	var newNode = null;//新节点
	var attr = null;//属性
	var parent = findXmlNode(parentNode);
	if(parent == null)
		parent = xml.documentElement;//指向根节点
	switch(node.flowtype){
		case "start":
			newNode = xml.createElement("start");
			break
		case "end":
			newNode = xml.createElement("end");
			break
		case "task":
			newNode = xml.createElement("task");
			break
		case "decision":
			newNode = xml.createElement("decision");
			break
	/*	case "mail":
			newNode = xml.createElement("mail");
			break  屏蔽邮箱功能 pengjin*/
		case "join":
			newNode = xml.createElement("join");
			break
		case "fork":
			newNode = xml.createElement("fork");	
			break
		case "transition":
			newNode = xml.createElement("transition");
			//当选择节点的文本值为null，包括的节点有(join,fork)
			addXmlAttribute(newNode,"name","");
	//		addXmlAttribute(newNode,"name","to "+desRect.title);
			addXmlAttribute(newNode,"to",desRect.id);//根据名称找下一节点//pengjin update
			//设置transition节点的g属性的值为对应文本节点的位置
			var font = Ext.DomQuery.select("span[title="+node.id+"]")[0];
			addXmlAttribute(newNode,"g",font.style.pixelLeft+","+font.style.pixelTop);
			parent.appendChild(newNode);
			break
	}
	if(node.flowtype != "transition"){
		addXmlAttribute(newNode,"name",node.title);
		addXmlAttribute(newNode,"g",node.style.pixelLeft+","+node.style.pixelTop+","+node.style.pixelWidth+","+node.style.pixelHeight);
		addXmlAttribute(newNode,"nodeid",node.id);//pengjin update
		parent.appendChild(newNode);
	}
}
//添加xml属性
function addXmlAttribute(node,attr,value){
	var attribute = xml.createAttribute(attr);
	attribute.value = value;
	node.setAttributeNode(attribute);
}
//通过XML节点查找流程图节点，当前只用到通过nodeid查找roundrect节点
function findNodeXml(xmlNode){	
	var nodes = [];
	var sameNodes = [];
	//如果不是节点的话通过title查找，如果是节点的话通过flowtype查找
	if(typeof xmlNode == "object"){	
		nodes = Ext.DomQuery.select("[flowtype="+xmlNode.tagName+"]");

		for(var i=0;i<nodes.length;i++){
			if(xmlNode.getAttribute("nodeid") == nodes[i].id){//pengjin update 根据ID查找对象
				sameNodes.push(nodes[i]);
			}	
		}
		if(sameNodes.length == 1){
			return sameNodes[0];
		}else{
			for(i=0;i<sameNodes.length;i++){
				if(sameNodes[i].getAttribute("g") == xmlNode.style.pixelLeft+","+xmlNode.style.pixelTop+","+xmlNode.style.pixelWidth+","+xmlNode.style.pixelHeight)
					return sameNodes[i];
			}	
		}
	}else{
		//如果有多个id相同的节点，只能返回第一个
	//	alert(Ext.DomQuery.select("roundrect[id="+xmlNode+"]").length)
		return Ext.DomQuery.select("roundrect[id="+xmlNode+"]")[0];//pengjin update 根据ID查找对象
	//	return Ext.DomQuery.select("roundrect[title="+xmlNode+"]")[0];
	}
	return null;
}


//通过流程图节点查找XML节点
function findXmlNode(node){
	if(node == "process")
		return xml.documentElement;//返回根节点
	if(node == null || node.flowtype == null)
		return null;
	var nodes = xml.getElementsByTagName(node.flowtype);
	var sameNodes = [];
	
	for(var i=0;i<nodes.length;i++){
		//判断是连线还是节点，是节点的话用节点坐标比较，是连线的话，用连线对应的文本节点坐标比较
		if(node.flowtype == "transition"){
			var font = Ext.DomQuery.select("span[title="+node.id+"]")[0];
			for(i=0;i<nodes.length;i++){
				if(font!=null && nodes[i].getAttribute("g") == font.style.pixelLeft+","+font.style.pixelTop)
					sameNodes.push(nodes[i]);
			}
		}else{
			for(i=0;i<nodes.length;i++){
				if(nodes[i].getAttribute("g") == node.style.pixelLeft+","+node.style.pixelTop+","+node.style.pixelWidth+","+node.style.pixelHeight)
					sameNodes.push(nodes[i]);
			}	
		}
	}
	if(sameNodes.length == 1){
		return sameNodes[0];
	}else{
		for(var i=0;i<sameNodes.length;i++){
			if(sameNodes[i].getAttribute("name") == node.title){
				return sameNodes[i];
			}
		}
	}
	/*
	 *下面是先通过title查找,再通过坐标查找,这样查找的次数比较多【sameNodes的长度比较小】
	 *上面面先通过坐标查找，再通过title查找，坐标相同的几率比较小
	 *因为移动的时候文本节点的坐标改变了，是用上面的方法无法定位到对应的transition
	 *【通过修改重画reDrawLine方法，在修改坐标前先修改XML这样就能定位到相应的transition】
	 *最终使用上面的方法
	 */
	
	//先用名字进行查找，如果找到相同的，在用坐标进行匹配，如果还有相同的，返回第一个
	/*for(var i=0;i<nodes.length;i++){
		if(nodes[i].getAttribute("name") == node.title){
			sameNodes.push(nodes[i]);
		}
	}
	if(sameNodes.length == 1){
		return sameNodes[0];
	}else{
		//判断是连线还是节点，是节点的话用节点坐标比较，是连线的话，用连线对应的文本节点坐标比较
		if(node.flowtype == "transition"){
			var font = Ext.DomQuery.select("span[title="+node.id+"]")[0];
			for(i=0;i<sameNodes.length;i++){
				if(sameNodes[i].getAttribute("g") == font.style.pixelLeft+","+font.style.pixelTop)
					return sameNodes[i];
			}
		}else{
			for(i=0;i<sameNodes.length;i++){
				if(sameNodes[i].getAttribute("g") == node.style.pixelLeft+","+node.style.pixelTop+","+node.style.pixelWidth+","+node.style.pixelHeight)
					return sameNodes[i];
			}	
		}
	}
	/*
	//通过一个code进行查找，每个节点必须带code，不通用，不能适用于其他的XML
	for(var i=0;i<nodes.length;i++){
		if(nodes[i].getAttribute("code") == node.id)
			return nodes[i];
	}
	*/
	return null;
}
//删除节点
function deleteXmlNode(node){
		
	//如果是文本节点，获取对应的transition节点，清空name属性
	if(node.tagName!=undefined && node.tagName.toLowerCase() == "span"){
		node = findXmlNode(Ext.getDom(node.title));
		node.setAttribute("name","");
		return false;
	}
	//如果不是文本节点，获取父节点，通过父节点删除自己
	node = findXmlNode(node);
	if(node != null){
		node.parentNode.removeChild(node);
	}
}
//修改节点对应的属性
function modifyXmlNodeAttr(node,param,value){
	node = findXmlNode(node);
	if(node != null)
		node.setAttribute(param,value);
}
//XML的逆向转换，生成节点
var isResend=false;//是否回退到申请人
function XmltoNode(child){

	var showNode = true;//是否创建节点
	var locations = child.getAttribute("g")!=null?child.getAttribute("g").split(","):[0,0,110,50];
	var node = document.createElement("v:roundrect");
	node.inset = '2pt,2pt,2pt,2pt';
	node.style.pixelLeft = locations[0];
	node.style.pixelTop = locations[1];
	node.style.pixelWidth = locations[2];
	node.style.pixelHeight = locations[3];

	node.strokeColor = "#27548d";
	node.fillcolor= '#EEEEEE';
	Ext.fly(node).addClass("node");
	
	var textboxtTitle="处理情况：&#10;";//节点title显示人员处理情况
	var textboxtClass="node_task";//节点样式
	var textboxtSpanOver="";//节点显示执行状态
	var textboxtClassStart="node_start";//开始节点样式
	var iwaitTask=0;//待处理、未到达
	var taskCount=0;//处理数量
	var isCurTask=0;//当前处理数量
	var isNoGoTask=0;//条件不满足、未到达
	var isZhongzhi=false;

	for(var i=0;i<processTask.getElementsByTagName("tr").length;i++){
		var tds=processTask.getElementsByTagName("tr")[i];
	
		if(tds.getElementsByTagName("td")[10].firstChild!=null && tds.getElementsByTagName("td")[10].firstChild.nodeValue=='2'){//实例状态为待发送状态
			isResend=true;
			break;
		}
		
		
		//tds.getElementsByTagName("td")[9]为连接线ID
		if(tds.getElementsByTagName("td")[1].firstChild!=null && tds.getElementsByTagName("td")[0].firstChild.nodeValue==child.getAttribute("nodeid") && tds.getElementsByTagName("td")[9].firstChild==null){
			if(tds.getElementsByTagName("td")[0].firstChild.nodeValue=='start'){
				textboxtTitle+=tds.getElementsByTagName("td")[1].firstChild.nodeValue+"&#10;";
			}else{
				textboxtTitle+=tds.getElementsByTagName("td")[1].firstChild.nodeValue+"&#10;";
			}
			taskCount++;//处理人
			
			if(tds.getElementsByTagName("td")[2].firstChild.nodeValue=='1'){//正在处理
				isCurTask++;
			}
			if(tds.getElementsByTagName("td")[2].firstChild.nodeValue=='2'){//待处理、未到达
				iwaitTask++;
			}
			if(tds.getElementsByTagName("td")[4].firstChild.nodeValue=='4'){//节点动作=终止
				isZhongzhi=true;
			}
			if(tds.getElementsByTagName("td")[4].firstChild.nodeValue=='1'){//条件不满足、未到达
				isNoGoTask++;
			}
		
		}
	}

	//实例调用页面才显示执行状态
	if(parent.example_init_load!=undefined && isResend==false){
		if(isCurTask==0 && taskCount>0){
		//	textboxtSpanOver="<span title='已完成' class='overTask'><b>√</b></span>";
			textboxtClass="completeTask"
		}
		if(isCurTask>0){//正在处理
			textboxtClass="curTask";
			node.fillcolor= '#80CD47';
		}
		if(iwaitTask>0){//待处理、未到达
			textboxtClass="node_task";
		}
		if(isNoGoTask>0){//条件不满足、未到达
			textboxtClass="noGoTask";
		}
		if(isZhongzhi){//节点动作=终止
			textboxtClass="zhongzhi"
		}
	}else{
		if(isResend==true){//实例状态为已撤销状态，进入待发送
			textboxtClassStart="chexiao";
		}
		if(child.tagName=='start'){//发送页面，发起人为空
			textboxtTitle="";
		}
	}
		
	switch(child.tagName){
		case "start":
			node.id = "start";
		//	node.title =child.getAttribute("name");title='张三√&#10;李四×'
			node.flowtype = "start";
			node.innerHTML = "<v:shadow on='T' type='single' color='#b3b3b3' offset='2px,2px' />"
			+"<v:textbox class='"+textboxtClassStart+"' title='"+textboxtTitle+"' inset='1pt,2pt,1pt,1pt'><b style='display:none'>Start</b><br/>"+child.getAttribute("name")+"</v:textbox>";
			break
		case "task":	
		//	var title = "TaskNode";
			//标识泳道
		//	if(child.getAttribute("swimlane") != null&&child.getAttribute("swimlane") != "")
		//	title += "<span title='"+child.getAttribute("swimlane")+"' class='sign'>泳</span>";#F8E5E4
		//	node.id = "task" + taskNum;
			node.id=child.getAttribute("nodeid");
			node.title = child.getAttribute("name");
			node.flowtype = "task";
			node.innerHTML = "<v:shadow on='T' type='single' color='#b3b3b3' offset='2px,2px' />"
			+"<v:textbox class='"+textboxtClass+"' title='"+textboxtTitle+"' inset='1pt,2pt,1pt,1pt'><b style='display:none'>TaskNode</b>"+textboxtSpanOver+"<br/>"+child.getAttribute("name")+"</v:textbox>";
			taskNum ++;
			break
		case "end":
		//	node.id = "end" + endNum;
			node.id=child.getAttribute("nodeid");
			node.title = "";
			node.flowtype = "end";
			node.innerHTML = "<v:shadow on='T' type='single' color='#b3b3b3' offset='2px,2px' />"
			+"<v:textbox class='node_end'  inset='1pt,2pt,1pt,1pt'><b style='display:none'>End</b><br />"+child.getAttribute("name")+"</v:textbox>";
			endNum ++;
			break;
		default:
			showNode = false;
	}
	if(showNode)
		document.getElementById("center").appendChild(node);
}

//XML的逆向转换，生成连线及文本节点
function XmltoLine(child){
	lineNum=child.getAttribute("flowid").replace("line","");

	srcRect = findNodeXml(child.parentNode);
//	desRect = getNodeByAttribute(child.getAttribute("to"));
	desRect = findNodeXml(child.getAttribute("to"));

	var lineColor= "#27548d";//默认黑色
	var lineFontBeiZhu= "";//线上加备注
	/*	if(processTask!=null && isResend==false){
		for(var i=0;i<processTask.getElementsByTagName("tr").length;i++){
			var tds=processTask.getElementsByTagName("tr")[i];
			
			//tds.getElementsByTagName("td")[9]为连接线ID
			if(tds.getElementsByTagName("td")[9].firstChild!=null){
				//条件不满足，节点不执行
				if(child.getAttribute("flowid")==tds.getElementsByTagName("td")[9].firstChild.nodeValue && tds.getElementsByTagName("td")[4].firstChild.nodeValue=="1"){
				//	alert(desRect.id+"---"+tds.getElementsByTagName("td")[2].firstChild.nodeValue)
					lineColor="#FF0000";//红色
					lineFontBeiZhu="条件不满足";
				}
			}
		}
	}*/

	var locations = child.getAttribute("g")!=null?child.getAttribute("g").split(","):[0,0];
	var line = document.createElement("v:line");
	direction();
	line.title = lineFontBeiZhu;//child.getAttribute("name");
	line.from = x0+","+y0;
	line.to = x1+","+y1;
	line.style.pixelLeft = x0 + 'px';
	line.style.pixelTop = y0 + 'px';
	line.style.position = "absolute";
	line.style.display = "block";
	line.id = "line" + lineNum;
	line.flowtype = "transition";
	line.strokeWeight = "1pt";
	line.style.cursor = "pointer";
	line.strokeColor = lineColor;//条件不满足，重置颜色
	line.source = srcRect.id;
	line.project = desRect.id;
	//创建箭头
	line.innerHTML = "<v:stroke endarrow='Classic' />";
	document.getElementById("center").appendChild(line);

	//生成文本节点
	var textNode = document.createElement("span");
	textNode.style.pixelLeft = locations[0];
	textNode.style.pixelTop = locations[1];
	textNode.innerHTML =child.getAttribute("name");//条件不满足，加备注
	Ext.fly(textNode).addClass("font_node");
	textNode.title = "line" + lineNum;
	textNode.id = "text" + lineNum;
	document.getElementById("center").appendChild(textNode);

}
//XML的逆向转换，将XML转换成为流程图
function XmlToProcess(loadXml){
	//清空已有的流程图
	Ext.getDom("center").innerHTML = "";
	xml.loadXML(loadXml);
	//初始化流程信息
	initProcess();
	
	var root = findXmlNode("process");
	var rootChilds = root.childNodes;

	for(var i=0;i<rootChilds.length;i++){
		XmltoNode(rootChilds[i]);
	}	
	var lines = xml.getElementsByTagName("transition");
	for(i=0;i<lines.length;i++){
		XmltoLine(lines[i]);
	}	
	
	//重新计算计数器 start
	var taskNodeId = xml.getElementsByTagName("task");
	taskNum = 0;//任务
	for(i=0;i<taskNodeId.length;i++){
		if(taskNum<parseInt(taskNodeId[i].getAttribute("nodeid").replace("task",""))){
			taskNum=parseInt(taskNodeId[i].getAttribute("nodeid").replace("task",""));//取最大任务节点数量
		}
	}
	taskNum=taskNum+1;
	
	var endNodeId = xml.getElementsByTagName("end");
	endNum = 0;//结束
	for(i=0;i<endNodeId.length;i++){
		if(endNum<parseInt(endNodeId[i].getAttribute("nodeid").replace("end",""))){
			endNum=parseInt(endNodeId[i].getAttribute("nodeid").replace("end",""));//取最大结束节点数量
		}
	}
	endNum=endNum+1;
	
	var lineNodeId = xml.getElementsByTagName("transition");
	lineNum = 0;//连接线
	for(i=0;i<lineNodeId.length;i++){
		if(lineNum<parseInt(lineNodeId[i].getAttribute("flowid").replace("line",""))){
			lineNum=parseInt(lineNodeId[i].getAttribute("flowid").replace("line",""));//取最大连接线数量
		}
	}
	lineNum=lineNum+1;
	
/*	var joinNodeId = xml.getElementsByTagName("join");
	joinNum = 0;//合并
	for(i=0;i<joinNodeId.length;i++){
		if(joinNum<parseInt(joinNodeId[i].getAttribute("nodeid").replace("join",""))){
			joinNum=parseInt(joinNodeId[i].getAttribute("nodeid").replace("join",""));//取最大合并节点数量
		}
	}
	joinNum=joinNum+1;
	
	var forkNodeId = xml.getElementsByTagName("fork");
	forkNum = 0;//分支
	for(i=0;i<forkNodeId.length;i++){
		if(forkNum<parseInt(forkNodeId[i].getAttribute("nodeid").replace("fork",""))){
			forkNum=parseInt(forkNodeId[i].getAttribute("nodeid").replace("fork",""));//取最大分支节点数量
		}
	}
	forkNum=forkNum+1;*/
	//重新计算计数器 end
}

//根据节点名字和类型查找XML节点
function findXmlNodeByName(nodeType,nodeName){
	var nodes = xml.getElementsByTagName(nodeType);
	for(var i=0;i<nodes.length;i++)
		if(nodes[i].getAttribute("name") == nodeName)
			return nodes[i];
}
function deleteXmlNodeByType(nodeType){
	if(typeof currentTreeNode=="undefined"){
		return false;
	}
	var treeNode = Ext.getDom(currentTreeNode);
	if(treeNode == null)
		return false;
	var node = findXmlNodeByName(nodeType,treeNode.title);
	node.parentNode.removeChild(node);
	Ext.fly(treeNode).remove();
}

//选择节点动作界面
function showNodeActionWin(obj){
	var nodeActionSpan="<span style='font:14px'>节点名称："+obj.title+"</br>";
	nodeActionSpan+="</br>节点动作说明：<br/>";
	nodeActionSpan+="1、回退到上一节点：将当前节点回退到上一步节点进行处理， 如果上一节点是开始节点将回退到其待发中。<br/>";
	nodeActionSpan+="2、回退到申请人：直接回退到发起者的待发中。<br/>";
	nodeActionSpan+="3、终止：终止整个流程， 流程结束，所有节点都不能再处理。<br/>";
	nodeActionSpan+="<br/><font color='blue'>当前节点动作：";

	for(var i=0;i<processTask.getElementsByTagName("tr").length;i++){
		var tds=processTask.getElementsByTagName("tr")[i];
		//tds.getElementsByTagName("td")[9]为连接线ID
		if(tds.getElementsByTagName("td")[0].firstChild.nodeValue==obj.id && tds.getElementsByTagName("td")[9].firstChild==null){
			if(tds.getElementsByTagName("td")[7].firstChild!=null){
				nodeActionSpan+=tds.getElementsByTagName("td")[7].firstChild.nodeValue+"";
			}else{
				nodeActionSpan+="无";
			}
			break;
		}
		
	}
	nodeActionSpan+="</font></span>";
	nodeActionWin = new Ext.Window({
		title: '节点动作',
		width: 400,
		layout: 'fit',
		iconCls: 'trans_action',
		height: 300,
		modal: true,
		closeAction: 'hide',
	//	maximizable:'true',
		maximized:false,
		autoScroll:true,
	 // resizable :true,
		items:[{
            xtype:"panel",
            bodyStyle: {background:"#dfe7f4"},
            html:nodeActionSpan
        }],
		buttons:[{
			text: '取消',
			iconCls: 'picon09',
			handler: function(){
				nodeActionWin.hide();
			}
		}]
	}).show();

}


//选择委派模式界面
function showNodeConvertWin(obj){
	var convertActionSpan="<span style='font:14px'>节点名称："+obj.title+"</br>";
	convertActionSpan+="</br>委派模式说明：<br/>";
	convertActionSpan+="1、自动流转：当前节点程序自动选择，如果有条件则根据条件判断。<br/>";
	convertActionSpan+="2、手动流转：当前节点程序手动选择，如果条件不满足，也可以选择是否执行。<br/>";
	convertActionSpan+="3、不可分配：当前节点的处理人不可以分配。<br/>";
	convertActionSpan+="4、任意分配：当前节点的处理人可以重新分配，但是必须是模板授权的处理人。<br/>";
	convertActionSpan+="5、完成所有审批：当前节点必须完成所有人的审批才能进入下一节点。<br/>";
	convertActionSpan+="6、任意审批：当前节点任意一个人审批完成后，就可以进入下一节点。<br/>";
	convertActionSpan+="<br/><font color='blue'>当前节点委派模式：";
	
	for(var i=0;i<processTask.getElementsByTagName("tr").length;i++){
		var tds=processTask.getElementsByTagName("tr")[i];
		
		//tds.getElementsByTagName("td")[9]为连接线ID
		if(tds.getElementsByTagName("td")[0].firstChild.nodeValue==obj.id && tds.getElementsByTagName("td")[9].firstChild==null){
			
			if(tds.getElementsByTagName("td")[3].firstChild!=null){
				convertActionSpan+=tds.getElementsByTagName("td")[3].firstChild.nodeValue;
			}
			break;
		}
		
	}
	convertActionSpan+="</font></span>";
	convertActionWin = new Ext.Window({
		title: '委派模式',
		width: 400,
		layout: 'fit',
		iconCls: 'trans_rotate',
		height: 400,
		modal: true,
		closeAction: 'hide',
	//	maximizable:'true',
		maximized:false,
		autoScroll:true,
	 // resizable :true,	
		items:[{
            xtype:"panel",
            bodyStyle: {background:"#dfe7f4"},
            html:convertActionSpan
        }],
		buttons:[{
			text: '取消',
			iconCls: 'picon09',
			handler: function(){
				convertActionWin.hide();
			}
		}]
	}).show();

}

//选择节点描述界面
function showNodeDescriptionWin(obj){
	
	var descriptionActionSpan="<span style='font:14px'>节点名称："+obj.title+"</br>";
	descriptionActionSpan+="<br/>节点ID："+obj.id+"</br>";
	descriptionActionSpan+="<br/>节点描述：";
	
	for(var i=0;i<processTask.getElementsByTagName("tr").length;i++){
		var tds=processTask.getElementsByTagName("tr")[i];
		
		if(tds.getElementsByTagName("td")[0].firstChild.nodeValue==obj.id){
			
			if(tds.getElementsByTagName("td")[8].firstChild!=null){
				descriptionActionSpan+=tds.getElementsByTagName("td")[8].firstChild.nodeValue;
			}else{
				descriptionActionSpan+="无";
			}
			break;
		}
		
	}
	descriptionActionSpan+="</span>";
	descriptionActionWin = new Ext.Window({
		title: '节点描述',
		width: 400,
		layout: 'fit',
		iconCls: 'messenger',
		height: 300,
		modal: true,
		closeAction: 'hide',
	//	maximizable:'true',
		maximized:false,
		autoScroll:true,
	 // resizable :true,	
		items:[{
            xtype:"panel",
            bodyStyle: {background:"#dfe7f4"},
            html:descriptionActionSpan
        }],
		buttons:[{
			text: '取消',
			iconCls: 'picon09',
			handler: function(){
				descriptionActionWin.hide();
			}
		}]
	}).show();

}

//查看节点条件说明
var ExpressionStr="";
function showExpressionStrWin(obj){
		
		var	expressionWin = new Ext.Window({
			title: '转换条件',
			width: 500,
			layout: 'fit',
			iconCls: 'picon18',
			height: 300,
			modal: true,
			closeAction: 'hide',
		//	maximizable:'true',
			maximized:false,
			autoScroll:true,
		 // resizable :true,	
			items:[{
	            xtype:"textarea",
	            value:ExpressionStr,
	            readOnly:true
	        }],
			buttons:[{
				text: '取消',
				iconCls: 'picon09',
				handler: function(){
					expressionWin.hide();
				}
			}]
		}).show();

}

