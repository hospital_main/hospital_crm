
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>节点ID</th>
				<th>节点名称</th>
				<th>节点描述</th>
				<th>转换模式</th>
				<th>节点动作</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	 
        <tr>
       
          
            <xsl:for-each select="td">
              <!--xsl:choose>
                <xsl:when test="position()=1">
			            <td align="left">			           
			                  <a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					           updateTemplate('<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			                </a>	                
			            </td>
                </xsl:when>
                
                <xsl:when test="position()=8">
			            <td align="left">			           
			                  <a tabindex='-1'>
					           <xsl:attribute name="onclick" >
					           updateTemplate('<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>');
				  	          </xsl:attribute>
				  	          <xsl:attribute name="href" >#</xsl:attribute>
				  	          <xsl:value-of select="."/>
			                </a>	                
			            </td>
                </xsl:when>
                
			  	<xsl:otherwise>
			            <td align="left">
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose-->
               <td align="left">
		                	<xsl:value-of select="."/>
			    </td>
  			  </xsl:for-each>        
          
         
        </tr>   		
   		</xsl:for-each>  
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
