
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>表单编码</th>
				<th>表单名称</th>
				<th>系统模块</th>
				<th>状态</th>
				<th>操作</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	 
        <tr>
      
          <!--td align='center'>                   
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    		  </input>  
          </td-->
          
            <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td align="left">			           
				  	          <xsl:value-of select="."/>
			            </td>
                </xsl:when>
                
                
			  	<xsl:otherwise>
			            <td align="left">
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>        
          
          <td align="right">
          	 <a tabindex='-1' style="text-decoration:none;">
				<xsl:attribute name="onclick" >
				formInit('<xsl:for-each select="pk/*"><xsl:value-of select="."/></xsl:for-each>','<xsl:value-of select="td[2]"/>');
				 </xsl:attribute>
				<xsl:attribute name="href" >#</xsl:attribute>
				 【启用】
			 </a>
			 <a tabindex='-1' style="text-decoration:none;">
				<xsl:attribute name="onclick" >
				formDelete('<xsl:for-each select="pk/*"><xsl:value-of select="."/></xsl:for-each>','<xsl:value-of select="td[2]"/>');
				 </xsl:attribute>
				<xsl:attribute name="href" >#</xsl:attribute>
				 【停用】
			 </a>	                
          
          </td>
        </tr>   		
   		</xsl:for-each>  
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
