/*******************************************************************************
* 自定义插件JS 
* pengjin
* 基带版本@version 4.1.9 (2013-10-08)
*******************************************************************************/

/*主表字段 begin*/
KindEditor.lang({
	columnSelect : '主表字段'
});
		
KindEditor.plugin('columnSelect', function(K) {
	var self = this, name = 'columnSelect';
	self.clickToolbar(name, function() {
		var columnDivTable=formMainColumn();
		 html = ['<div>'+columnDivTable+'</div>'].join(''),
                        dialog = editor.createDialog({
                                name : name,
                                width : 450,
                                height:500,	
                                title : editor.lang(name),
                                body : html,
                                yesBtn : {
                                        name : editor.lang('yes'),
                                        click : function(e) {
                                             var code = $('input[name="columnRadio"]:checked').val();
	                                         if(code!=undefined){
	                                         	//没有单元格，不删除
	                                         	if(null!=self.plugin.getSelectedCell()){
											        var cell = self.plugin.getSelectedCell()[0];
											        //改单元格有子节点，删除
													if(cell.children[0]!=undefined){
														self.cmd.range.selectNodeContents(cell).collapse(true);
														self.cmd.select();
														K(cell.children[0]).remove();
														self.addBookmark();
													}
												}
                                             	editor.insertHtml("<span id=\""+code.split('|||')[0]+"\" style=\"color:blue\">["+code.split('|||')[1]+"]</span>").hideDialog();
	                                         }
                                            
                                        }
                                }
                        }),
                        textarea = K('textarea', dialog.div);
                        textarea[0];
		
	});
});
/*主表字段 end*/

/* 明细字段 begin */
KindEditor.lang({
        tableDetail : '明细字段'
});
		
KindEditor.plugin('tableDetail', function(K) {
        var self = this, name = 'tableDetail';
        self.clickToolbar(name, function() {
	//	var htmlStr=formDetailColumn();
      //html = ['<div><table id="formDetailSelect" style="width: 100%;background:#f7f7f7;" border=1 cellspacing=0><tr><th width=40px>序号</th><th>列名</th><th width=40px><input type="button" style="background:url(../../../template/design/images/icon/add.gif) no-repeat;border:none;color:#f00; width:20px;CURSOR: hand;" onclick="addRow();"/></th></tr></table></div>'].join(''),            
        var htmlStr='<div><table ><tr><td>明细视图：<select id="dept" onChange="select_employee(this.selectedIndex,0)"></select></td><td colspan="2"><input type="text" id="userSeach"/>&nbsp;&nbsp;<input type="button" onclick="userSeach();" value="查 询"/></td></tr>';
		htmlStr+='<tr><td><br><select id="user" size="25" style="width:200px;" onDblClick="move_right()"></select></td>';
		htmlStr+='<td><input type="button" value=" >> " onClick="move_right()"><br><br><input type="button" value=" << " onClick="move_left()"><br><br><input type="button" value="全选" onClick="move_right_all()"><br><br><input type="button" value="全删" onClick="move_left_all()"></td>';
		htmlStr+='<td><br><select id="userSelect" size="25" style="width:200px;" onDblClick="move_left();"></select></td></tr></table></div>';
        html=[htmlStr].join(''),           
          			 
                        dialog = editor.createDialog({
                                name : name,
                                width : 500,
                                height:500,	
                                title : editor.lang(name),
                                body : html,
                                yesBtn : {
                                        name : editor.lang('yes'),
                                        click : function(e) {
                                             var columnTableStr=getUserSelect();
	                                         if(columnTableStr!=""){
	                             
	                                         	 	//存在明细表格，同时该单元格没有子节点，删除
	                                         	 	if(null!=self.plugin.getSelectedTable() && self.plugin.getSelectedTable()[0].id==$("#dept").val()){
	                                         	 		// && self.plugin.getSelectedCell()[0].children.length==0 是否有子节点
	                                         	 		if(null!=self.plugin.getSelectedCell() ){
											          		var table = self.plugin.getSelectedTable();
															self.cmd.range.setStartBefore(table[0]).collapse(true);
															self.cmd.select();
															table.remove();
															self.addBookmark();
														}
													
										          	}
	                                         		editor.insertHtml(columnTableStr).hideDialog();
	                                         	
	                                         }
                                             
                                        }
                                }
                        }),
                        textarea = K('textarea', dialog.div);
                        textarea[0];
                        formDetailColumn();
                      
                   
                        if(null!=self.plugin.getSelectedTable() && self.plugin.getSelectedTable()[0].id!=""){
			          	//	setColumnTable(self);
			          		setUserSelect(self);
			          	}
			          	//新增加视图，默认加载第1个视图字段
			          	if(document.getElementById("dept").disabled==false && document.getElementById("dept").length>0){
				        	select_employee(0)
				        }
			          
        });
});
/* 明细字段 end */


/*页面查找 begin*/

var DOM = (document.getElementById) ? 1 : 0; 
var NS4 = (document.layers) ? 1 : 0; 
var IE4 = 0; 
if (document.all) 
{ 
     IE4 = 1; 
     DOM = 0; 
}

var win = window;    
var n    = 0;

function findIt(obj) { 
     if (obj != ""){
         findInPage(obj); 
  	}      
}


function findInPage(str) { 
var txt, i, found;

if (str == "") 
     return false;

if (DOM) 
{ 
     win.find(str, false, true); 
     return true; 
}

if (NS4) { 
     if (!win.find(str)) 
         while(win.find(str, false, true)) 
             n++; 
     else 
         n++;

     if (n == 0){ 
       //  alert("未找到指定内容."); 
       return false
  	}      
}

if (IE4) { 
     txt = win.document.body.createTextRange();

     for (i = 0; i <= n && (found = txt.findText(str)) != false; i++) { 
         txt.moveStart("character", 1); 
         txt.moveEnd("textedit"); 
     }

if (found) { 
     txt.moveStart("character", -1); 
     txt.findText(str); 
     txt.select(); 
     txt.scrollIntoView(); 
     n++; 
} 
else { 
     if (n > 0) { 
         n = 0; 
         findInPage(str); 
     } 
     else {
        // alert("未找到指定内容."); 
        return false;
    	}  
     } 
}

return false; 
} 
/*页面查找 end*/

/*动态添加表格 begin*/	
var currentStep=1;
var max_line_num=0;
function addRow(inputValue){
	if(inputValue==undefined){
		inputValue="";
	}
  max_line_num=$("#formDetailSelect tr:last-child").children("td").html();
  if(max_line_num==null) {
    max_line_num=1;
  }
  else{
    max_line_num=parseInt(max_line_num);
	max_line_num+=1;
  }
  
  $('#formDetailSelect').append("<tr id='line"+max_line_num+"'><td align=center>"+max_line_num+"</td><td><input type='text' name='columnName' value='"+inputValue+"'/></td><td align=center><input type='button' style='background:url(../../../template/design/images/icon/delete.gif) no-repeat;border:none;color:#f00; width:20px;CURSOR: hand;' onclick='removeRow("+max_line_num+");'/></td></tr>");
}
function removeRow(max_line_num){  
  $("#formDetailSelect tr").each(
    function(){
	  var seq=parseInt($(this).children("td").html());
	  if(seq==max_line_num) $(this).remove();
	 // if(seq>max_line_num) $(this).children("td").each(function(i){if(i==0)$(this).html(seq-1);});
	}
  );
  currentStep=1;
}
//获取明细表格数据
function getColumnTable(){
	var columnTableStr="";
	var inputArray=$('input[name="columnName"]');

	for(var i=0;i<inputArray.length;i++){
		if(inputArray[i].value!=""){
			columnTableStr+="<td align=center style=\"color:blue\">"+inputArray[i].value+"</td>";
		}
	}
	if(columnTableStr!=""){
		columnTableStr="<table id=\"formDetail\" border=1 bordercolor=#000000 cellspacing=0 width=100%><tr>"+columnTableStr+"</tr></table>";
	}
	return columnTableStr;
}
//设置明细表格数据
function setColumnTable(self){
	var	rows = self.plugin.getSelectedRow()[0];
	//该单元格没有子节点没有子节点，添加
	if(rows.cells[0].children.length==0){
		for(var i=0;i<rows.cells.length;i++){
			addRow(self.cmd.range.selectNodeContents(rows.cells[i]));
		}
	}
}

/*动态添加表格 end*/	

/*动态添加下拉框 begin*/
	function check_employee(ID) {
			var userSelect=document.getElementById("userSelect");
			for (var i=0; i<userSelect.options.length; i++){
				if (userSelect[i].value == ID){
					return false;
				}
			}
			return true;
		}
		
		function select_employee(v,flag){
			if(flag==0){
				move_left_all();
			}
			var user=document.getElementById("user");
			user.options.length = 0;
			for (var i=1; i<employee[v].length; i++){
				if (employee[v][i]!=undefined && check_employee(employee[v][i].split('|||')[0])){ 
					user.options.add(new Option(employee[v][i].split('|||')[0]+" "+employee[v][i].split('|||')[1], employee[v][i].split('|||')[0]));
				}
			}
		}
		
		//向右移
		function move_right() {
			var user=document.getElementById("user");
			var userSelect=document.getElementById("userSelect");
			if (user.selectedIndex >= 0) {
				userSelect.options.add(new Option(user.options[user.selectedIndex].text, user.options[user.selectedIndex].value));
				user.options[user.selectedIndex].removeNode(true);
			}
			else {
		//	 alert('请选择'); 
			}
		}
		
		//向左移
		function move_left() {
			var dept=document.getElementById("dept");
			var userSelect=document.getElementById("userSelect");
			userSelect.selectedIndex >= 0 ? userSelect.options[userSelect.selectedIndex].removeNode(true) : '';
			if (dept.selectedIndex >= 0) {
				select_employee(dept.selectedIndex);
			}
		}
		//向右全部移
		function move_right_all() {
			var user=document.getElementById("user");
			var userSelect=document.getElementById("userSelect");
			for (var i=0; i<user.options.length; i++){
				userSelect.options.add(new Option(user.options[i].text, user.options[i].value));
			}
			user.options.length = 0;
		}
		
		//向左全部移
		function move_left_all() {
			var dept=document.getElementById("dept");
			var userSelect=document.getElementById("userSelect");
			userSelect.options.length = 0;
			if (dept.selectedIndex >= 0) select_employee(dept.selectedIndex);
		}
		
		//已选字段
		function users_submit() {
			var userSelect=document.getElementById("userSelect");
			var userlist ="";
			for (var i=0; i<userSelect.options.length; i++){
			//	userlist[userlist.length] = userSelect.options[i].value;
				userlist += userSelect.options[i].value+",";
			}
			if(userlist!=""){
				userlist=userlist.substring(0,userlist.length-1)
			}
			return userlist;
		}
		
		//条件查询
		function userSeach(){
			var userSeach=document.getElementById("userSeach").value;
			var user=document.getElementById("user");
			if(userSeach!=""){
				for (var i=0; i<user.options.length; i++){
					if(user[i].text.indexOf(userSeach)!=-1){
						user.options[i].selected=true;
						break;
					}
				}
		
			}
		}
		
		//获取选中字段
		function getUserSelect(){
		
			var columnTableStr="";
			var userSelect=document.getElementById("userSelect");
			for (var i=0; i<userSelect.options.length; i++){
				if(""!=userSelect[i].value){
					if(tdStyle!=undefined && tdStyle!=""){//td样式不为空，例如修改操作，以选中单元格的样式添加列
						columnTableStr+="<td id="+userSelect[i].value+" align=center style=\"background-color:#CACACA\">"+tdStyle.replace("{td_value}",userSelect[i].text.split(" ")[1])+"</td>";
					}else{
						columnTableStr+="<td id="+userSelect[i].value+" align=center style=\"background-color:#CACACA\">"+userSelect[i].text.split(" ")[1]+"</td>";
					}
				}
			}
		
			if(columnTableStr!=""){
				if(tableWidth!=undefined && tableWidth!=""){//表格的宽度不为空，，例如修改操作，以选中表格的宽度添加表格
					columnTableStr="<table id=\""+document.getElementById("dept").value+"\" align=center border=1 bordercolor=#000000 cellspacing=0 cellpadding=0 width="+tableWidth+"><tr>"+columnTableStr+"</tr></table>";
				}else{
					columnTableStr="<table id=\""+document.getElementById("dept").value+"\" align=center border=1 bordercolor=#000000 cellspacing=0 cellpadding=0 width=100%><tr>"+columnTableStr+"</tr></table>";
				}
			}
			return columnTableStr;
		}
		
	
		//给选中字段赋值
		var tdStyle; 
		var tableWidth; 
		function setUserSelect(self){
			tdStyle=""; 
			tableWidth="";
			if(self.plugin.getSelectedRow()==null){
				return;
			}
			var	rows = self.plugin.getSelectedRow()[0];
	
			//该单元格有子节点，返回
		/*	if(rows.cells[0].children.length>0){
				return;
			}*/
			
			//单元格样式
			tdStyle=rows.cells[0].innerHTML;
			var isView=false;
			//判断视图是否存在
			var dept=document.getElementById("dept");
			for (var i=0; i<dept.options.length; i++){
				if(self.plugin.getSelectedTable()[0].id==dept[i].value){
					isView=true;//视图存在
					dept.options[i].selected=true;
			 		select_employee(i);//加载选中的视图字段
					dept.disabled="disabled";
					break;
				}
			}
			
			if(!isView){
				return;
			}
			
			var user=document.getElementById("user");
			var isInsert;
			for(var i=0;i<rows.cells.length;i++){
				isInsert=false;
				for(var j=0;j<user.options.length;j++){
					if(rows.cells[i].id==user[j].value){//左边存在字段就删除
						isInsert=true;
						user.options[j].removeNode(true);
					
					}
				}
				if(isInsert){
					//td的样式
					tdStyle=tdStyle.replace(self.cmd.range.selectNodeContents(rows.cells[i]),"{td_value}");
					//表格的宽度
					tableWidth=self.plugin.getSelectedTable()[0].width;
					$("#userSelect").append( "<option value="+rows.cells[i].id+">"+rows.cells[i].id+" "+self.cmd.range.selectNodeContents(rows.cells[i])+"</option>" );
				}
			//	userSelect.options.add(new Option(rows.cells[i].id+" "+self.cmd.range.selectNodeContents(rows.cells[i]),rows.cells[i].id));
			}
			
	}
/*动态添加下拉框 end*/
	
/* 表单模板 begin */
KindEditor.lang({
        formTemplate : '表单模板'
});

KindEditor.plugin('formTemplate', function(K) {
	var self = this, name = 'formTemplate';
	self.clickToolbar(name, function() {
		var formDivTable=formTemplate();
		if(!formDivTable){
			return;
		}
		html = ['<div>'+formDivTable+'</div>'].join(''),
                        dialog = editor.createDialog({
                                name : name,
                                width : 450,
                                height:300,	
                                title : editor.lang(name),
                                body : html,
                                yesBtn : {
                                        name : editor.lang('yes'),
                                        click : function(e) {
                                       	 
                                             var form_code = $('input[name="formRadio"]:checked').val();
	                                         if(form_code!=undefined){
	                                         	if(!window.confirm("确定要清空表单内容，导入该表单模板吗？")){
													return;
												}
	                                         	readFormTemplate(form_code);
	                                         }
                                            
                                        }
                                }
                        }),
                        textarea = K('textarea', dialog.div);
                        textarea[0];
	});
});

/* 表单模板 begin */
