<%@ page contentType="text/html; charset=gb2312" language="java" import="java.util.*,java.sql.*,java.math.BigDecimal,com.viewhigh.core.web.OperateHelper,com.viewhigh.core.dao.*" errorPage="" %>
<%@ page import="com.viewhigh.base.sql.*,com.zhuozhengsoft.pageoffice.*,com.zhuozhengsoft.pageoffice.wordwriter.*,java.awt.*,javax.servlet.*,javax.servlet.http.*,java.sql.*,java.text.SimpleDateFormat,java.util.Date"%>

<%@ taglib uri="http://java.pageoffice.cn" prefix="po"%>
<%
//******************************卓正PageOffice组件的使用*******************************
	Connection conn = ConnectionPool.get(); 
	Statement stmt = conn.createStatement();
	String contractCode = request.getParameter("contract_code");
	String pathSQL="select contract_path from  pact_file_path where contract_code='"+contractCode+"'";
	ResultSet res1 = stmt.executeQuery(pathSQL);
	String contract_path="";
	if(res1.next()){
		contract_path = res1.getString("contract_path");		
	}
	res1.close();
	//设置PageOffice服务器组件
	PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);
	poCtrl1.setServerPage(request.getContextPath()+"/poserver.zz"); //此行必须
	poCtrl1.setJsFunction_AfterDocumentOpened("AfterDocumentOpened()");
	//poCtrl1.addCustomToolButton("保存", "Save()", 1); 
	//poCtrl1.addCustomToolButton("新建批注", "InsertComment()", 3); 
	
	poCtrl1.setTitlebar(false); //隐藏标题栏
	//poCtrl1.setMenubar(false); //隐藏菜单栏
	poCtrl1.setOfficeToolbars(false);//隐藏Office工具条
	poCtrl1.setCustomToolbar(false);//隐藏自定义工具栏
	
	poCtrl1.setSaveFilePage("SaveFile.jsp");
	//打开文件
	String ppp=request.getSession().getServletContext().getRealPath("/")+contract_path;
	poCtrl1.webOpen(ppp, OpenModeType.docReadOnly, "Tom");
	//poCtrl1.webOpen(ppp, OpenModeType.docRevisionOnly, "Tom");
	
	poCtrl1.setTagId("PageOfficeCtrl1"); //此行必须	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns:vh>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<link href="" rel="stylesheet" type="text/css">
		<script language="javascript" id="scriptID"></script>
    <script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>
	<script type="text/javascript">

        function Save() {
            document.getElementById("PageOfficeCtrl1").WebSave();
        }
        function AfterDocumentOpened() {
			document.getElementById("PageOfficeCtrl1").ShowRevisions=false;
            refreshList();
			refreshList_pz();
        }
        //获取当前痕迹列表
        function refreshList() {
            var i;
            document.getElementById("ul_Comments").innerHTML = "";
            for (i = 1; i <= document.getElementById("PageOfficeCtrl1").Document.Revisions.Count; i++) {
                var str = "";
                str = str + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Author;
                var  revisionDate=document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Date;
                 //转换为标准时间
                str=str+" "+dateFormat(revisionDate,"yyyy-MM-dd HH:mm:ss");
                if (document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Type == "1") {
                    str = str + ' 插入：' + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Range.Text;
                }else if (document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Type == "2") {
                    str = str + ' 删除：' + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Range.Text;
                }else {
                    str = str + ' 调整格式或样式。';
                }
                document.getElementById("ul_Comments").innerHTML += "<li><a href='#' onclick='goToRevision(" + i + ")'>" + str + "</a></li>"
            }

        }
         //GMT时间格式转换为CST
        dateFormat = function (date, format) {
            date = new Date(date); 
            var o = {
                'M+' : date.getMonth() + 1, //month
                'd+' : date.getDate(), //day
                'H+' : date.getHours(), //hour
                'm+' : date.getMinutes(), //minute
                's+' : date.getSeconds(), //second
                'q+' : Math.floor((date.getMonth() + 3) / 3), //quarter
                'S' : date.getMilliseconds() //millisecond
            };
			if (/(y+)/.test(format))
                format = format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
 
            for (var k in o)
                if (new RegExp('(' + k + ')').test(format))
                    format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));
 
            return format;
        }

        //定位到当前痕迹
        function goToRevision(index){
	        var sMac = "Sub myfunc() " + "\r\n"+ "ActiveDocument.Revisions.Item("+index+").Range.Select " + "\r\n"+ "End Sub ";
			document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
	    }
		//刷新列表
		function refresh_click(){
	       refreshList();    
        }

		function Button2_onclick() {
            refreshList_pz();
        }

        function InsertComment() {
            document.getElementById("PageOfficeCtrl1").WordInsertComment();
            var sMac = "Sub myfunc() " + "\r\n"
                     + "On Error Resume Next " + "\r\n"
                     + "ActiveWindow.ActivePane.Close " + "\r\n"
                     + "End Sub ";
            document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
        }
		
		function refreshList_pz() {
            var sMac = "Function getComments() " + "\r\n"
                     + "Dim cmts As String " + "\r\n"
                     + "For i=1 To ActiveDocument.Comments.Count "+ "\r\n"
                     + "    cmts = cmts +ActiveDocument.Comments.Item(i).Author & \":\" & ActiveDocument.Comments.Item(i).Range.Text + \"||\" " + "\r\n"
                     + "Next" + "\r\n"
                     + "getComments = cmts" + "\r\n"
                     + "End Function ";

            var sComments = document.getElementById("PageOfficeCtrl1").RunMacro("getComments", sMac);

            var arr = sComments.split("||");

            document.getElementById("ul_Comments2").innerHTML = "";
            for (var i = 0; i < arr.length-1 ; i++) {
                document.getElementById("ul_Comments2").innerHTML += "<li><a href='#' onclick='goToComment("+(i+1)+")'>"+arr[i]+"</a></li>"
            }
        }
        
        function getComment(index){
            var sMac = "Function getCmtTxt() " + "\r\n"+ "getCmtTxt = ActiveDocument.Comments.Item(" + index + ").Range.Text " + "\r\n"+ "End Function ";
			return document.getElementById("PageOfficeCtrl1").RunMacro("getCmtTxt", sMac);
        }
        function goToComment(index){
	        var sMac = "Sub myfunc() " + "\r\n"+ "ActiveDocument.Comments.Item("+index+").Edit " + "\r\n"+ "End Sub ";
			document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
	    }
		function closePage(){
	        window.close();
	    }
		function xianshi(){
			document.getElementById("PageOfficeCtrl1").ShowRevisions = true;
		}
		function yincang(){
			document.getElementById("PageOfficeCtrl1").ShowRevisions = false;
		}
		
    </script>	
	</head>
	<body class="subBody" id="subBody" >
    <div  style=" width:100%; height:700px;">
        <div id="Div_Comments" style="float:left; width:12%; height:100%; border:solid 1px red;">
			<h3>痕迹列表</h3>
			&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="refresh" value="刷新"onclick=" return refresh_click()" style="width:100px;"/>
			<ul id="ul_Comments">
				
			</ul>
        </div>
		<div style="float:left;width:76%; height:700px;">
			<table>
				<tr>
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<button class="pageBtn" accessKey="C" onClick="xianshi()">显示痕迹</button>
						<button class="pageBtn" accessKey="C" onClick="yincang()">隐藏痕迹</button>
						<button class="pageBtn" accessKey="C" onClick="closePage();">关闭</button>
					</td>
				</tr>					
			</table>
			<div>
				<po:PageOfficeCtrl id="PageOfficeCtrl1" >
				</po:PageOfficeCtrl>
			</div>
		</div>
		<div id="Div_Comments2" style="float:left;width:10%; height:700px; border:solid 1px red;">
			<h3>批注列表</h3>
			&nbsp;&nbsp;&nbsp;&nbsp;<input id="Button2" type="button" value="刷新" onclick="return Button2_onclick()"  style="width:100px"/>
			<ul id="ul_Comments2">
            
			</ul>
        </div>
    </div>
</body>
</html>

