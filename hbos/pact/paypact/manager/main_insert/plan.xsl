<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			
			<tr noWrap="true" class="mainHead">
				<th>付款期</th>
				<th>付款期限</th>
				<th>付款期限说明</th>
				<th>本币金额</th>				
				<th>付款比例(%)</th>				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<td>
								<xsl:value-of select="."/>
							</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
