<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style=''><input type='checkbox'/></th>
				<th noWrap="true" >申请单号</th>
	      <th noWrap="true">购置年度</th>
	      <th noWrap="true">制单日期</th>
	      <th noWrap="true">计划类型</th>
				<th noWrap="true">申请科室 </th>
				<th noWrap="true">申请人</th>
				<th noWrap="true">设备名称</th>
        <th noWrap="true">设备型号</th>
				<!--th noWrap="true">附件金额</th-->
				<!--<th noWrap="true">附带文档</th>-->
				<th noWrap="true">价格</th>
				<th noWrap="true">数量</th>
				<th noWrap="true">金额</th>
        <th noWrap="true">设备类型</th>
     </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
         	<xsl:if test="td[1] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">
          <td align='center'  style=''>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          </xsl:if>
          <xsl:for-each select="td">
            <xsl:choose>

               <!--xsl:when test="position()=1">
              <xsl:if test=". = '合计'">
				        		<td noWrap='true'><xsl:value-of select="."/></td>
				       </xsl:if>
				       <xsl:if test=". != '合计'">
			            <td  noWrap='true' >
		              <a tabindex="-1">
		               <xsl:attribute name="href" >
		                  javascript:view('update.html?load=&lt;id&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/id&gt;')
		                </xsl:attribute><xsl:value-of select="."/></a>
			            </td>
              </xsl:if>
                </xsl:when-->
                <xsl:when test="position()=9 and ../td[1] != '合计'">
                 <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:when>
              <xsl:when test="position()=9 and ../td[1] = '合计'">
                 <td>
			            </td>
              </xsl:when>
                 <xsl:when test="position()=10 or position()=11">
                <td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             		 </xsl:when>

              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


