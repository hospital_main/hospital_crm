<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>
					<input type="checkbox"/>
				</th>
				<th>招标编码</th>
				<th>代理机构</th>
				<th>采购方式</th>
				<th>组织形式</th>
				<th>招标日期</th>
				<th>资产信息</th>
				<th>型号</th>
				<th>规格</th>
				<th>品牌</th>
				<th>生产厂商</th>
				<th>需求科室</th>
				<th>资金来源</th>
				<th>数量</th>
				<th>价格</th>
				<th>金额</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=13 or position()=14 or position()=15">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,###0.00')" />
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
