<%@ page contentType="text/html; charset=gb2312" language="java" import="java.util.*,com.viewhigh.core.web.OperateHelper,com.viewhigh.core.dao.*" errorPage="" %>
<%@ page import="com.viewhigh.base.sql.*,com.zhuozhengsoft.pageoffice.*,com.zhuozhengsoft.pageoffice.wordwriter.*,java.awt.*,javax.servlet.*,javax.servlet.http.*,java.sql.*,java.text.SimpleDateFormat,java.util.Date"%>
<%@ taglib uri="http://java.pageoffice.cn" prefix="po"%>
<%
	String contract_type_code = request.getParameter("contract_type_code");
	String contractCode = request.getParameter("contract_code");
	String comp_code = request.getParameter("comp_code");
	String copy_code = request.getParameter("copy_code");	
	String user_code = request.getParameter("user_code");
	
	WordDocument doc = new WordDocument();
	
	DataRegion contract_code = doc.openDataRegion("PO_合同编号");
	contract_code.setValue(contractCode);
	contract_code.setEditing(true);
	DataRegion pact_date = doc.openDataRegion("PO_签约日期_显示");
	//pact_date.getShading().setBackgroundPatternColor(Color.pink);
	//pact_date.setEditing(true);  
	DataRegion ven_code = doc.openDataRegion("PO_乙方");
	ven_code.setEditing(true);
	DataRegion ven_address = doc.openDataRegion("PO_乙方地址");
	ven_address.setEditing(true);
	DataRegion ven_youbian = doc.openDataRegion("PO_乙方邮编");
	ven_youbian.setEditing(true);
	
	DataRegion sign_date = doc.openDataRegion("PO_签约日期");
	//sign_date.getShading().setBackgroundPatternColor(Color.pink);
	sign_date.setEditing(true); 
	DataRegion pact_numery = doc.openDataRegion("PO_合同事宜");
	pact_numery.setEditing(true);
	DataRegion pact_detail = doc.openDataRegion("PO_合同明细");
	pact_detail.setEditing(true);
	DataRegion send_detail = doc.openDataRegion("PO_赠品明细");
	send_detail.setEditing(true);
	DataRegion pact_page_number = doc.openDataRegion("PO_配置清单附件页数");
	pact_page_number.setEditing(true);
	
	DataRegion jh_days = doc.openDataRegion("PO_交货天数");
	jh_days.setEditing(true); 
	DataRegion syx_months = doc.openDataRegion("PO_试运行月数");
	syx_months.setEditing(true);
	DataRegion one_number = doc.openDataRegion("PO_非预付_首款比率");
	one_number.setEditing(true);
	DataRegion other_number = doc.openDataRegion("PO_非预付_质保金比率");
	other_number.setEditing(true);
	DataRegion second_bank = doc.openDataRegion("PO_乙方开户银行");
	second_bank.setEditing(true);
	
	DataRegion second_bank_num = doc.openDataRegion("PO_乙方银行账号");
	second_bank_num.setEditing(true);
	DataRegion second_pact_num = doc.openDataRegion("PO_乙方合同份数");
	syx_months.setEditing(true);
	DataRegion jsfwtk = doc.openDataRegion("PO_技术服务条款");
	jsfwtk.setEditing(true);
	DataRegion first_pact_num = doc.openDataRegion("PO_甲方合同份数");
	first_pact_num.setEditing(true);
	DataRegion ss_address = doc.openDataRegion("PO_诉讼地点");
	ss_address.setEditing(true);
	
	DataRegion zb_days = doc.openDataRegion("PO_质保期");
	zb_days.setEditing(true);
	DataRegion zbtk = doc.openDataRegion("PO_质保条款");
	zbtk.setEditing(true);
	DataRegion pact_num = doc.openDataRegion("PO_合同份数");
	pact_num.setEditing(true);
	
	//打开数据区域中的表格，OpenTable(index)方法中的index为word文档中表格的下标，从1开始
	Table table1 = doc.openDataRegion("PO_合同明细").openTable(1);
	//设置表格边框样式
	table1.getBorder().setLineColor(Color.black);
	table1.getBorder().setLineWidth(WdLineWidth.wdLineWidth050pt);
	// 设置表头单元格文本居中
	table1.openCellRC(1, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 3).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 4).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 5).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 6).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 7).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 8).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	
	
	
	Table table2 = doc.openDataRegion("PO_赠品明细").openTable(1);
	//设置表格边框样式
	table2.getBorder().setLineColor(Color.black);
	table2.getBorder().setLineWidth(WdLineWidth.wdLineWidth050pt);
	// 设置表头单元格文本居中
	table2.openCellRC(1, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table2.openCellRC(1, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table2.openCellRC(1, 3).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table2.openCellRC(1, 4).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table2.openCellRC(1, 5).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table2.openCellRC(1, 6).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table2.openCellRC(1, 7).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	
	
	
	
	// 设置PageOffice组件服务页面
	
	PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);
	poCtrl1.setServerPage(request.getContextPath()+"/poserver.zz"); //此行必须

	// 设置界面样式
	poCtrl1.setCaption("购销合同");
	poCtrl1.setBorderStyle(BorderStyleType.BorderThin);
	// 添加自定义工具条按钮
	
	poCtrl1.addCustomToolButton("保存", "poSave();", 1);
	poCtrl1.addCustomToolButton("全屏/还原", "poSetFullScreen();", 4);
	poCtrl1.addCustomToolButton("关闭", "closePage();", 10);
	//隐藏工具栏
	poCtrl1.setOfficeToolbars(false);
	poCtrl1.setMenubar(false);

	poCtrl1.setJsFunction_OnWordDataRegionClick("OnWordDataRegionClick()");
	
	//document.getElementById("PageOfficeCtrl1").JsFunction_BeforeDocumentSaved = "Js函数名";
	// 设置保存文档的服务器页面
	
	String uulStr="SaveData.jsp?contract_type_code="+contract_type_code+"&comp_code="+comp_code+"&copy_code="+copy_code+"&user_code="+user_code;
	poCtrl1.setSaveDataPage(uulStr);
	poCtrl1.setSaveFilePage("SaveFile.jsp?contract_code="+contractCode);
	
	////获取数据对象
	poCtrl1.setWriter(doc);
	// 打开文档
	poCtrl1.webOpen("doc/addPact_yj.docx", OpenModeType.docSubmitForm, "Tom");
	poCtrl1.setTagId("PageOfficeCtrl1"); //此行必须
	//-----------  PageOffice 服务器端编程结束  -------------------//
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns:vh>
	<HEAD>
		<title>购销合同</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema"
			content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<script language="JavaScript">
	function OnWordDataRegionClick(Name, Value, Left, Bottom) {
		/**if (Name == "PO_签约日期_显示") {
			var strRet = document.getElementById("PageOfficeCtrl1").ShowHtmlModalDialog("datetimer.htm", Value, "left=" + Left + "px;top=" + Bottom + "px;width=260px;height=260px;frame=no;");
			if (strRet != "") {
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}**/
		if (Name == "PO_签约日期") {
			var strRet = document.getElementById("PageOfficeCtrl1").ShowHtmlModalDialog("datetimer2.htm", Value, "left=" + Left + "px;top=" + Bottom + "px;width=260px;height=260px;frame=no;");
			if (strRet != "") {
				var dataRegionList = document.getElementById("PageOfficeCtrl1").DataRegionList;//获取Word文件中的数据区域列表
				var dataRegionItem = dataRegionList.GetDataRegionByName("PO_签约日期_显示");//获取名称为“PO_name”的数据区域对象
				dataRegionItem.value=strRet;
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		if (Name == "PO_乙方") {
			var strRet = document.getElementById("PageOfficeCtrl1").ShowHtmlModalDialog("selectVencode.htm", Value, "left=" + Left + "px;top=" + Bottom + "px;width=260px;height=260px;frame=no;");
			if (strRet != "") {
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		if (Name == "PO_非预付_首款比率") {
			var strRet = document.getElementById("PageOfficeCtrl1").ShowHtmlModalDialog("shoukuanbili.htm", Value, "left=" + Left + "px;top=" + Bottom + "px;width=260px;height=260px;frame=no;");
			if (strRet != "") {
				var dataRegionList = document.getElementById("PageOfficeCtrl1").DataRegionList;//获取Word文件中的数据区域列表
				var dataRegionItem = dataRegionList.GetDataRegionByName("PO_非预付_质保金比率");//获取名称为“PO_name”的数据区域对象
				dataRegionItem.value=(100-strRet);
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		if (Name == "PO_非预付_质保金比率") {
			var strRet = document.getElementById("PageOfficeCtrl1").ShowHtmlModalDialog("zhibaobili.htm", Value, "left=" + Left + "px;top=" + Bottom + "px;width=260px;height=260px;frame=no;");
			if (strRet != "") {
				var dataRegionList = document.getElementById("PageOfficeCtrl1").DataRegionList;//获取Word文件中的数据区域列表
				var dataRegionItem = dataRegionList.GetDataRegionByName("PO_非预付_首款比率");//获取名称为“PO_name”的数据区域对象
				dataRegionItem.value=(100-strRet);
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		if (Name == "PO_合同明细") {
			//var strRet = document.getElementById("PageOfficeCtrl1").ShowHtmlModalDialog("pact_detail.html", Value, "left=" + Left + "px;top=" + Bottom + "px;width=260px;height=260px;frame=no;");
			//var strRet = document.getElementById("PageOfficeCtrl1").ShowHtmlModalDialog("pact_datail.html", Value, "left=" + Left + "px;top=" + top + "px;width=900px;height=700px;frame=no;");
			window.showModalDialog("pact_datail.html", window, "help:no;status:no;resizable:yes;scroll:yes;");
		}
	}
	
</script>
		<form id="Form1" method="post">

			<!-- *********************pageoffice组件的使用 **************************-->
			<script type="text/javascript">
				//保存
				function poSave() {
					document.getElementById("PageOfficeCtrl1").WebSave();
					//document.getElementById("PageOfficeCtrl1").WebSaveAsPDF();
					if (document.getElementById("PageOfficeCtrl1").CustomSaveResult == "ok") {
					   document.getElementById("PageOfficeCtrl1").Alert("文档保存成功!");
					}
				}
				//全屏
				function poSetFullScreen() {
					document.getElementById("PageOfficeCtrl1").FullScreen = !document.getElementById("PageOfficeCtrl1").FullScreen;
				}
				function closePage(){
					window.close();
				}
			</script>
			<po:PageOfficeCtrl id="PageOfficeCtrl1" />
			<!-- *********************pageoffice组件的使用 **************************-->

		</form>
	</body>
</HTML>
