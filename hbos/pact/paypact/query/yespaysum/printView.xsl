<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      	</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>年度</td>
        <td nowrap='true'>月份</td>
		  	<td nowrap='true'>摘要</td>
		  	<td nowrap='true'>借方</td>
		  	<td nowrap='true'>贷方</td>
		  	<td nowrap='true'>余额</td>
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
						
								<td align='left'><xsl:value-of select="."/></td>
							
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
