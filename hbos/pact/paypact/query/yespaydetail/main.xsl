<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>日期</th>
        <th nowrap='true'>单据编号</th>
		  	<th nowrap='true'>摘要</th>
		  	<th nowrap='true'>借方</th>
		  	<th nowrap='true'>贷方</th>
		  	<th nowrap='true'>余额</th>
      </tr>
    </thead>
    <tbody>
    	<xsl:for-each select="/root/tbody/tr[position()=1]">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=4">
								<td align='right'></td>
							</xsl:when>
							<xsl:when test="position()=5">
								<td align='right'></td>
							</xsl:when>
							<xsl:when test="position()=6">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  
   		<xsl:for-each select="/root/tbody/tr[position()>1]">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=2">
            		<xsl:choose>
            		<xsl:when test="../td[3]='合同金额'">
            			<td><a>
							    <xsl:attribute name="href" >
							      javascript:openPactPage('<xsl:value-of select="."/>','2','01',getCompCode(),getCopyCode())
							    </xsl:attribute><xsl:value-of select="."/></a></td>
								</xsl:when>
								<xsl:when test="../td[3]='变动金额'">
									<td><a>
						          <xsl:attribute name="href" >
						    	    javascript:openDialog('../../paychange/paychange.html?load=&lt;change_code&gt;<xsl:value-of select="."/>&lt;/change_code&gt;', 'dialogWidth:900px;dialogHeight:600px', result)
						  	      </xsl:attribute><xsl:value-of select="."/></a></td>
								</xsl:when>
								<xsl:when test="../td[3]='付款金额'">
									<td><a>
						          <xsl:attribute name="href" >
						    	    javascript:openDialog('../../paydocu/update.html?load=&lt;pay_id&gt;<xsl:value-of select="../pk/bills_id"/>&lt;/pay_id&gt;', 'dialogWidth:1000px;dialogHeight:570px', result)
						  	      </xsl:attribute><xsl:value-of select="."/></a></td>
								</xsl:when>
								<xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              	</xsl:otherwise>
              	</xsl:choose>
							</xsl:when>
							<xsl:when test="position()=4">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=5">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=6">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>        
    </tbody>
  </xsl:template>
</xsl:stylesheet>

