<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
		  <tr noWrap='true' class='mainHead'>
	      <th style=''><input type='checkbox'/></th>
				<th noWrap="true">退款编号</th>
				<th noWrap="true">退款日期</th>
	      <th noWrap="true">供应商</th>
				<th noWrap="true">合同编号</th>
				<th noWrap="true">合同名称</th>
				<th noWrap="true">退款金额</th>
				<th noWrap="true">摘要</th>
				<th noWrap="true">退款方式</th>
				<th noWrap="true">支票号码</th>
				<th noWrap="true">发票号码</th>
				<th noWrap="true">制单人</th>
				<th noWrap="true">审核人</th>
				<th noWrap="true">确认人</th>
	      <th noWrap="true">状态</th>
	      <th noWrap="true">凭证</th>
      </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
   				<xsl:when test="td[position()=1]/text()='合计'">
   					<tr>
		          <xsl:for-each select="td">
		            <xsl:choose>
		            	<xsl:when test="position()=1 ">
										<td></td><td><xsl:value-of select="."/></td>
									</xsl:when>
									<xsl:when test="position()=6 ">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
		              <xsl:otherwise>
		                <td><xsl:value-of select="."/></td>
		              </xsl:otherwise>
		          	</xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
   				</xsl:when>
   				<xsl:otherwise>
		        <tr>
		          <td align='center'  style=''>
		            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
		              <xsl:attribute name="value" >
		                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		      			  </xsl:attribute>
		    			  </input>
		          </td>
		          <xsl:for-each select="td">
		            <xsl:choose>
									<xsl:when test="position()=1">
									  <td  noWrap='true' >
									  <a tabindex="-1">
									    <xsl:attribute name="href" >
									      javascript:update('update.html?load=&lt;deposit_code&gt;<xsl:value-of select="."/>&lt;/deposit_code&gt;&lt;write_able&gt;true&lt;/write_able&gt;')
									    </xsl:attribute><xsl:value-of select="."/></a>
									  </td>
									</xsl:when>
									<xsl:when test="position()=4">
									  <td  noWrap='true' >
									  <a tabindex="-1">
									    <xsl:attribute name="href" >
									      javascript:viewContract('<xsl:value-of select="."/>')
									    </xsl:attribute><xsl:value-of select="."/></a>
									  </td>
									</xsl:when>
									<xsl:when test="position()=6">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=15">
										<td  noWrap='true' >
									  <a>
									    <xsl:attribute name="href" >
									       javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
									    </xsl:attribute><xsl:value-of select="."/></a>
									  </td>
									</xsl:when>
		              <xsl:otherwise>
		                <td><xsl:value-of select="."/></td>
		              </xsl:otherwise>
		          	</xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		  		</xsl:otherwise>
   			</xsl:choose>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


