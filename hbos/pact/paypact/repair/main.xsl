<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th nowrap='true'>卡片编码</th>
        <th nowrap='true'>资产名称</th>
        <th nowrap='true'>资产类别</th>
		  	<th nowrap='true'>科室信息</th>
		  	<th nowrap='true'>规格</th>
		  	<th nowrap='true'>型号</th>
		  	<th nowrap='true'>原值</th>
		  	<th nowrap='true'>供应商</th>
		  	<th nowrap='true'>合同编号</th>
		  	<th nowrap='true'>合同名称</th>
		  	<th nowrap='true'>签订日期</th>
		  	<th nowrap='true'>入库日期</th>
		  	<th nowrap='true'>保修期</th>
		  	<th nowrap='true'>保修期限</th>
		  	<th style="display:none"></th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td align='left'>
									<a href="#">
                		 <xsl:attribute name="onclick" >
                		 	    <xsl:if test="../td[15] = 1">
                		 	  	openPage('&lt;card_arch_no&gt;<xsl:value-of select="../pk/id"/>&lt;/card_arch_no&gt;')
                		      </xsl:if>
                		      <xsl:if test="../td[15] = 2">
                		      openPage1('&lt;card_arch_no&gt;<xsl:value-of select="../pk/id"/>&lt;/card_arch_no&gt;')
                		      </xsl:if>	
                		 </xsl:attribute>
                		 <xsl:value-of select="."/>
                		</a>
								 </td>
              </xsl:when>
             <xsl:when test="position()=7 ">
                	<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
             </xsl:when>	
             <xsl:when test="position()=9">
                	<td align="right">
                		<a href="#">
											 <xsl:attribute name="onclick" >
                					openPactPage('<xsl:value-of select="."/>','2','01','<xsl:value-of select="../pk/comp_code"/>','<xsl:value-of select="../pk/copy_code"/>')
												</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>		
             </xsl:when>
             <xsl:when test="position()=15 ">
                	<td style="display:none"><xsl:value-of select="."/></td>
             </xsl:when>
              <xsl:otherwise>
								<td>
		                 <xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

