<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>变动编码</th>
				<th>变动日期</th>
				<th>付款期号</th>
				<th>变动金额</th>
				<th>变动原因</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>确认人</th>
				<th>状态</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">				
						<xsl:choose>
							<xsl:when test="position()=1">			
								<td><a href="#">
											 <xsl:attribute name="onclick" >
                					openDialog('../paychange/paychange.html?load=&lt;change_code&gt;<xsl:value-of select="../td[1]"/>&lt;/change_code&gt;&lt;display&gt;none&lt;/display&gt;', 'dialogWidth:900px;dialogHeight:600px')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>