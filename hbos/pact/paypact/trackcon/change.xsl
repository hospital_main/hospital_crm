<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>变更单号</th>
				<th>变更日期</th>
				<th>签订日期</th>
				<th>变更序号</th>
				<th>变更原因</th>
				<th>操作人</th>
				<th>审核人</th>
				<th>确认人</th>
				<th>状态</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">				
						<xsl:choose>
							<xsl:when test="position()=1">			
								<td>
									<a href="#">
											 <xsl:attribute name="onclick" >
                					openDialog('../changecont/change.html?load=&lt;change_code&gt;<xsl:value-of select="."/>&lt;/change_code&gt;&lt;display&gt;none&lt;/display&gt;', 'dialogWidth:600px;dialogHeight:250px')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>