<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="contract_attr"><xsl:value-of select="/root/tbody/tr[position()=1 ]/pk/contract_attr"/></xsl:variable>
			<tr noWrap="true" class="mainHead">
				<xsl:choose>
					<xsl:when test="$contract_attr = '11' or $contract_attr = '12'">
						<th>合同名称</th>
						<th>资产编码</th>
					</xsl:when>
					<xsl:otherwise>
						<th>合同项目</th>
					</xsl:otherwise>
				</xsl:choose>
				<th>名称</th>
				<th>规格</th>
				<th>型号</th>
				<th>生产厂家</th>
				<th>制造国</th>
				<xsl:if test="count(/root/tbody/tr[position()=1]/td)&gt;13">
					<th>数量</th>
					<th>单价</th>
				</xsl:if>
				<th>金额</th>
				<th>变动金额</th>
				<th>需求科室</th>
				<th>交货日期</th>
				<th>保修期</th>
				<th>保修单位</th>
				<th>备注</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<td><xsl:value-of select="."/></td>	
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
