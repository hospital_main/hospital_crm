<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>付款单号</th>
				<th>付款日期</th>
				<th>合同期号</th>
				<th>发票号码</th>
				<th>付款金额</th>
				<th>业务员</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>确认人</th>				
				<th>状态</th>
				<th>凭证</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">				
						<xsl:choose>
							<xsl:when test="position()=1">			
								<td><a href="#">
							    <xsl:attribute name="onclick" >
							      openDialog('../paydocu/update.html?load=&lt;pay_id&gt;<xsl:value-of select="../pk/pay_id"/>&lt;/pay_id&gt;','dialogWidth:1000px;dialogHeight:570px')
							    </xsl:attribute><xsl:value-of select="."/></a></td>
							</xsl:when>
							<xsl:when test="position()=11">			
								<td><a href="#">
							    <xsl:attribute name="onclick" >
							       openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
