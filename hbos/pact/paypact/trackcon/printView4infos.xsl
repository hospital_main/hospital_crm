<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum">
  		<xsl:choose>
  			<xsl:when test="count(/root/table[position() =1 ]/tbody/tr[position() =1 ]/td)&gt;13">17</xsl:when>
  			<xsl:otherwise>15</xsl:otherwise>
  		</xsl:choose>
  	</xsl:variable>
  	<xsl:variable name="bodytitleh">23</xsl:variable>
  	<xsl:variable name="bodybodyh">45</xsl:variable>
  	<root>
  		<thead>
  			<tr>
	  			<td style="height:10;border:none;width:10"></td>
	  			<td style="border:none;colspan:colcount"></td>
	  			<xsl:call-template name="repeat_noborder">
	  				<xsl:with-param name="times" select="$colNum - 1"/>    
	        </xsl:call-template>
	  		</tr>
  			<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr>
	  			<td style="border:none;colspan:colcount"></td>
	  			<xsl:call-template name="repeat_noborder">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
	  		<tr>
	  			<td style="display:none;border:none"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2">合同号：</td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">合同名称：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">合同原始编码：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8;colspan:2"></td>
					<td style="display:none;border:none"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">签订日期：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8;colspan:2"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<xsl:if test="$colNum = 17">
						<td style="display:none;border:none"></td>
						<td style="display:none;border:none"></td>
					</xsl:if>
				</tr>
				<tr>
					<td style="display:none;border:none"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2">供应商：</td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">是否招标：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">贸易类别：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8;colspan:2"></td>
					<td style="display:none;border:none"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">组织形式：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8;colspan:2"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<xsl:if test="$colNum = 17">
						<td style="display:none;border:none"></td>
						<td style="display:none;border:none"></td>
					</xsl:if>
				</tr>
				<tr>
  				<td style="display:none;border:none"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2">采购方式：</td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">币种：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">合同金额(外币)：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8;colspan:2"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<xsl:if test="$colNum = 17">
						<td style="display:none;border:none"></td>
						<td style="display:none;border:none"></td>
					</xsl:if>
				</tr>
  			<tr>
  				<td style="display:none;border:none"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2">甲方负责人：</td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">乙方负责人：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">乙方电话：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8;colspan:2"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<xsl:if test="$colNum = 17">
						<td style="display:none;border:none"></td>
						<td style="display:none;border:none"></td>
					</xsl:if>
				</tr>
 				<tr>
					<td style="display:none;border:none"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2">签订科室：</td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">合同开始日期：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">合同截止日期：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8;colspan:2"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<xsl:if test="$colNum = 17">
						<td style="display:none;border:none"></td>
						<td style="display:none;border:none"></td>
					</xsl:if>
				</tr>
  			<tr>
					<td style="display:none;border:none"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2">项目信息：</td>
					<td style="align:32;border:none;colspan:5;fontstyle:8"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<xsl:if test="$colNum = 17">
						<td style="display:none;border:none"></td>
						<td style="display:none;border:none"></td>
					</xsl:if>
				</tr>
  			<tr>
					<td style="display:none;border:none"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2">档案位置：</td>
					<td style="align:32;border:none;colspan:5;fontstyle:8"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<xsl:if test="$colNum = 17">
						<td style="display:none;border:none"></td>
						<td style="display:none;border:none"></td>
					</xsl:if>
				</tr>
				<tr>
					<td style="display:none;border:none"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2">签订金额：</td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">变动金额：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">合同金额：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8;colspan:2"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<xsl:if test="$colNum = 17">
						<td style="display:none;border:none"></td>
						<td style="display:none;border:none"></td>
					</xsl:if>
				</tr>
				<tr>
					<td style="display:none;border:none"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2">保证金方式：</td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">保证金额：</td>
					<td style="display:none;border:none"></td>
					<td style="align:32;border:none;fontstyle:8"></td>
					<xsl:choose>
						<xsl:when test="/root/param/issecu='1'">
							<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">保证金收取日期：</td>
							<td style="display:none;border:none"></td>
							<td style="align:32;border:none;fontstyle:8;colspan:2"></td>
							<td style="display:none;border:none"></td>
							<td style="align:34;border:none;fontsize:12;fontstyle:2;colspan:2">保证金归还日期：</td>
							<td style="display:none;border:none"></td>
							<td style="align:32;border:none;fontstyle:8;colspan:2"></td>
						</xsl:when>
						<xsl:otherwise>
							<td style="display:none;border:none"></td>
							<td style="display:none;border:none"></td>
							<td style="display:none;border:none"></td>
							<td style="display:none;border:none"></td>
							<td style="display:none;border:none"></td>
							<td style="display:none;border:none"></td>
							<td style="display:none;border:none"></td>
						</xsl:otherwise>
					</xsl:choose>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<xsl:if test="$colNum = 17">
						<td style="display:none;border:none"></td>
						<td style="display:none;border:none"></td>
					</xsl:if>
				</tr>
 				<tr>
					<td style="display:none;border:none"></td>
 					<td style="align:34;border:none;fontsize:12;fontstyle:2;height:60">合同简介：</td>
					<td style="align:32;border:none;colspan:5;fontstyle:8"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<xsl:if test="$colNum = 17">
						<td style="display:none;border:none"></td>
						<td style="display:none;border:none"></td>
					</xsl:if>
				</tr>
				<tr>
					<td style="display:none;border:none"></td>
 					<td style="align:34;border:none;fontsize:12;fontstyle:2;height:60">售后服务：</td>
					<td style="align:32;border:none;colspan:5;fontstyle:8"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<td style="display:none;border:none"></td>
					<xsl:if test="$colNum = 17">
						<td style="display:none;border:none"></td>
						<td style="display:none;border:none"></td>
					</xsl:if>
				</tr>
	  	</thead>
	  	<tbody>
	  	<tr>
				<td style="border:none;colspan:colcount"></td>
  			<xsl:call-template name="repeat_noborder">
  				<xsl:with-param name="times" select="$colNum"/>    
        </xsl:call-template>
  		</tr>
	  	<xsl:for-each select="/root/table[count(tbody/tr)&gt;0]">
	  		<xsl:variable name="curCols"><xsl:value-of select="count(thead/tr[position()=2 ]/td)"/></xsl:variable>
  			<xsl:for-each select="thead/tr[position()=1]">
  				<tr>
  					<td style="display:none;border:none"></td>
		        <td style='align:32;border:none;fontsize:12;fontstyle:2;colspan:colcount'><xsl:value-of select="."/></td>
		  			<xsl:call-template name="repeat_noborder">
		  				<xsl:with-param name="times" select="$colNum - 1"/>    
		        </xsl:call-template>
		  		</tr>
  			</xsl:for-each>
  			<xsl:for-each select="thead/tr[position()=2]">
  				<tr>
  					<td><xsl:attribute name="style">border:none;height:<xsl:value-of select="$bodytitleh"/></xsl:attribute></td>
  					<xsl:for-each select="td">
  						<xsl:choose>
  							<xsl:when test="position()=2 or position()=5">
  								<td style="align:36;fontsize:10;fontstyle:2;colspan:2"><xsl:value-of select="."/></td>
  								<td style="display:none"></td>
  							</xsl:when>
  							<xsl:otherwise>
  								<td style="align:36;fontsize:10;fontstyle:2"><xsl:value-of select="."/></td>
  							</xsl:otherwise>
  						</xsl:choose>
        		</xsl:for-each>
        		<xsl:call-template name="repeat_noborder">
		  				<xsl:with-param name="times" select="$colNum - $curCols - 2"/>    
		        </xsl:call-template>
        	</tr>
  			</xsl:for-each>
		    <xsl:for-each select="tbody/tr">
	        <tr>
	        	<td style="display:none;border:none"></td>
	          <xsl:for-each select="td">
	          	<xsl:choose>
  							<xsl:when test="position()=2 or position()=5">
  								<td><xsl:attribute name="style">height:<xsl:value-of select="$bodybodyh"/>;colspan:2;fontsize:9</xsl:attribute><xsl:value-of select="."/></td>
  								<td style="display:none"></td>
  							</xsl:when>
  							<xsl:otherwise>
  								<td><xsl:attribute name="style">height:<xsl:value-of select="$bodybodyh"/>;fontsize:9</xsl:attribute><xsl:value-of select="."/></td>
  							</xsl:otherwise>
  						</xsl:choose>
	  			  </xsl:for-each>
	  			  <xsl:call-template name="repeat_noborder">
		  				<xsl:with-param name="times" select="$colNum - $curCols - 2"/>    
		        </xsl:call-template>
	  			</tr>
	   		</xsl:for-each>
	  	</xsl:for-each>
	  	</tbody>
	</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
	<xsl:template name="repeat_noborder">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none;border:none"></td>
			<xsl:call-template  name="repeat_noborder">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>