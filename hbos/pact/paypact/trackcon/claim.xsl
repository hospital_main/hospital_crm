<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>登记编号</th>
				<th>登记日期</th>
				<th>索赔金额</th>
				<th>赔偿方</th>
				<th>甲方负责人</th>
				<th>乙方负责人</th>
				<th>备注</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>确认人</th>
				<th>状态</th>
				<th>凭证</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">				
						<xsl:choose>
							<xsl:when test="position()=1">			
								<td><a href="#">
							    <xsl:attribute name="onclick" >
							      openDialog('../claimcon/update.html?load=&lt;b&gt;<xsl:value-of select="."/>&lt;/b&gt;','dialogWidth:1050px;dialogHeight:450px')
							    </xsl:attribute><xsl:value-of select="."/></a></td>
							</xsl:when>
							<xsl:when test="position()=12">			
								<td><a href="#">
							    <xsl:attribute name="onclick" >
							       openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
