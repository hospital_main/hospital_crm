<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
				<tr noWrap="true" class="mainHead">
					<td>合同编号</td>
					<td>合同名称</td>
					<td>原始合同编号</td>
					<td>合同类别</td>
					<td>签订日期</td>
					<td>签订科室</td>
					<td>供应商</td>
					<td>付款期号</td>
					<td>付款日期</td>
					<td>资金来源</td>
					<td>支付方式</td>
					<td>付款金额</td>
					<td>设置天数</td>
					<td>预警天数</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr[position() = 1]">
				<tr>
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=12">
								<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>				
						<xsl:otherwise>
									<td align='right'><xsl:value-of select="."/></td>
								</xsl:otherwise>
						</xsl:choose>
			</xsl:for-each>
				</tr>
			</xsl:for-each>	
				<xsl:for-each select="/root/tbody/tr[position() &gt; 1]">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:when test="position()=12">
								  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							  </xsl:when>
								<xsl:otherwise>
									<td align='right'><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>