<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>合同编号</th>
				<th>合同名称</th>
				<th>合同类别</th>
				<th>签订日期</th>
				<th>供应商</th>
				<th>合同金额</th>
				<th>付款计划</th>
				<th>期初执行</th>
				<th>执行金额</th>
				<th>合计执行</th>
				<th>执行进度(%)</th>
				<th>文档</th>
				<th>变更</th>				
				<th>贸易类别</th>
				<th>项目信息</th>
				<th>合同状态</th>
				<th>结转</th>
				<th>凭证</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:choose>
   				<xsl:when test="td[position()=1]/text()='合计'">
   					<tr>
							<td></td>
							<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=6">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=8">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=9">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=10">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:otherwise>
										<td><xsl:value-of select="."/></td>
									</xsl:otherwise>
								</xsl:choose>	
							</xsl:for-each>	
					  </tr>
   				</xsl:when>
   				<xsl:otherwise>
						<tr>
							<td align="center" style="display:none">
								<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
									<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
								</input>
							</td>
							<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=1">
										<td>
											<a href="#">
													 <xsl:attribute name="onclick" >
													 openDialog('update.html?load=&lt;code&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/code&gt;&lt;RW_flag&gt;2&lt;/RW_flag&gt;&lt;modCode&gt;02&lt;/modCode&gt;&lt;compCode&gt;<xsl:value-of select="../pk/comp_code"/>&lt;/compCode&gt;&lt;copyCode&gt;<xsl:value-of select="../pk/copy_code"/>&lt;/copyCode&gt;', 'dialogWidth:1050px;dialogHeight:700px', result)
													 </xsl:attribute>
													<xsl:value-of select="."/>
											</a>
										</td>
									</xsl:when>
									<xsl:when test="position()=6">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=7">
										<td>
											<a href="#">
												<xsl:attribute name="onclick" >
		                			openDialog('../manager/plan.html?load=&lt;contract_code&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/contract_code&gt;', 'dialogWidth:650px;dialogHeight:400px')
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</td>
									</xsl:when>
									<xsl:when test="position()=8">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=9">
										<td align='right'>
											<a href="#">
												<xsl:attribute name="onclick" >
			                		openDialog('../guarantee/update.html?load=&lt;b&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/b&gt;','dialogWidth:1050px;dialogHeight:450px')
												</xsl:attribute>
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
											</a>
										</td>
									</xsl:when>
									<xsl:when test="position()=10">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=11">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=12">
										<td>
											<a href="#">
												<xsl:attribute name="onclick" >
		                			openDialog('../manager/doc.html?load=&lt;contract_code&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/contract_code&gt;', 'dialogWidth:750px;dialogHeight:400px')
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</td>
									</xsl:when>
									<xsl:when test="position()=13">
										<td>
											<a href="#">
												<xsl:attribute name="onclick" >
		                			openDialog('changeList.html?load=&lt;contract_code&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/contract_code&gt;', 'dialogWidth:750px;dialogHeight:400px')
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</td>
									</xsl:when>
									<xsl:when test="position()=18">
										<td>
											<a href="#">
									    <xsl:attribute name="onclick" >
									       openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
									    </xsl:attribute><xsl:value-of select="."/></a>
										</td>
									</xsl:when>
									<xsl:otherwise>
										<td><xsl:value-of select="."/></td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>	
						</tr>
					</xsl:otherwise>
   			</xsl:choose>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
