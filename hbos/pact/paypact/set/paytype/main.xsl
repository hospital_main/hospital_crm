<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'> 
				<td noWrap="true">选择</td>
				<td nowrap='true'>类别编码</td>
				<td nowrap='true'>类别名称</td> 
				<td nowrap='true'>开始年度</td> 
				<td nowrap='true'>开始月份</td> 
				<td nowrap='true'>主管科室</td>				
				<td nowrap='true'>合同性质</td>
				<td nowrap='true'>合同前缀</td> 
				<td nowrap='true'>变更前缀</td> 
				<td nowrap='true'>金额变动前缀</td> 
				<td nowrap='true'>违约前缀</td>				
				<td nowrap='true'>索赔前缀</td>
				<td nowrap='true'>付款前缀</td> 
				<td nowrap='true'>保证金管理</td> 
				<td nowrap='true'>保证金收款前缀</td> 
				<td nowrap='true'>保证金退款前缀</td> 
				<td nowrap='true'>合同属性</td>
				<td nowrap='true'>归属</td> 
				<td nowrap='true'>保修期管理</td> 
				<td nowrap='true'>自动凭证方式</td> 
				<td nowrap='true'>合同预警天数</td>
				<td nowrap='true'>保证金预警天数</td> 
				<td nowrap='true'>付款预警天数</td>
			</tr>     
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'  style='display:none'>
	        <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	          <xsl:attribute name="value" >
	            <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	          </xsl:attribute>
	        </input>
		    </td>
				<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
									<a href='#' >
									<xsl:attribute name="onclick">
										javascript:loadData("<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>")
									</xsl:attribute><xsl:value-of select="."/>
								</a>
								 </td>
		          </xsl:when>
		         
		          <xsl:otherwise>
								<td>
				        	<xsl:value-of select="."/>
						  	</td>
							</xsl:otherwise>
		        </xsl:choose>
		    </xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

