<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
		  <tr noWrap='true' class='mainHead'>
	      <th ><input type='checkbox'/></th>
				<th>保函编号</th>
				<th>开具日期</th>
				<th>银行</th>
				<th>委托人</th>
				<th>合同编号</th>
				<th>合同名称</th>
				<th>担保金额</th>
				<th>开始日期</th>
				<th>结束日期</th>
				<th>索赔条件</th>
				<th>备注</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>确认人</th>
				<th>失效人</th>
				<th>状态</th>
      </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    	<xsl:choose>
   				<xsl:when test="td[position()=1]/text()='合计'">
   					<tr>
		        	<td></td>
		          <xsl:for-each select="td">
		            <xsl:choose>
									<xsl:when test="position()=7 ">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
		              <xsl:otherwise>
		                <td><xsl:value-of select="."/></td>
		              </xsl:otherwise>
		          	</xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
   				</xsl:when>
   				<xsl:otherwise>
		        <tr>
		          <td align='center'  style=''>
		            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
		              <xsl:attribute name="value" >
		                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
		      			  </xsl:attribute>
		    			  </input>
		          </td>
		          <xsl:for-each select="td">
		            <xsl:choose>
									<xsl:when test="position()=1">
									  <td  noWrap='true' >
									  <a tabindex="-1">
									    <xsl:attribute name="href" >
									      javascript:update('update.html?load=&lt;letter_id&gt;<xsl:value-of select="../pk/letter_id"/>&lt;/letter_id&gt;&lt;write_able&gt;true&lt;/write_able&gt;')
									    </xsl:attribute><xsl:value-of select="."/></a>
									  </td>
									</xsl:when>
									<xsl:when test="position()=5">
									  <td  noWrap='true' >
									  <a href="#">
									    <xsl:attribute name="onclick" >
									      viewContract('<xsl:value-of select="."/>')
									    </xsl:attribute><xsl:value-of select="."/></a>
									  </td>
									</xsl:when>
									<xsl:when test="position()=7">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
		              <xsl:otherwise>
		                <td><xsl:value-of select="."/></td>
		              </xsl:otherwise>
		          	</xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
		  		</xsl:otherwise>
   			</xsl:choose>
   		</xsl:for-each>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>


