<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/> 
  <xsl:template match="/">
    <thead>             
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th>卡片编码</th>
        <th>资产名称</th>
        <th>资产类别</th>
		  	<th>规格</th>
		  	<th>型号</th>
		  	<th>原值</th>
		  	<th>入库日期</th>
		  	<th>入库单号</th>
		  	<th>合同编号</th>
		  	<th>合同名称</th>
		  	<th>合同类别</th>
		  	<th>签订科室</th>
		  	<th>供应商</th>
		  	<th style='display:none'></th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td align='left'>
									<a href="#">
                		 <xsl:attribute name="onclick" >
                		 	   <xsl:if test="../td[14] = 1">
              		 	       openPage('&lt;card_arch_no&gt;<xsl:value-of select="."/>&lt;/card_arch_no&gt;');
              		 	     </xsl:if>
                		 	   <xsl:if test="../td[14] = 2">
              		 	    	 openPage3('&lt;card_arch_no&gt;<xsl:value-of select="."/>&lt;/card_arch_no&gt;');
              		 	    </xsl:if>
                		 </xsl:attribute>
                		 <xsl:value-of select="."/>
                		</a>
								 </td>
              </xsl:when>
              <xsl:when test="position()=8">
								<td align='left'>
									<a href="#">
                		 <xsl:attribute name="onclick" >
              		 	    <xsl:if test="../td[14] = 1">
              		 	    	openPage1('&lt;id&gt;<xsl:value-of select="."/>&lt;/id&gt;&lt;equi_kind_type&gt;<xsl:value-of select="../pk/equi_kind_type"/>&lt;/equi_kind_type&gt;');
              		 	    </xsl:if>
                		 	  <xsl:if test="../td[14] = 2">
              		 	    	openPage2('&lt;id&gt;<xsl:value-of select="."/>&lt;/id&gt;');
              		 	    </xsl:if>
                		 </xsl:attribute>
                		 <xsl:value-of select="."/>
                		</a>
								 </td>
              </xsl:when>
              	<xsl:when test="position()=9">
								<td align='left'>
									<a href="#">
                		 <xsl:attribute name="onclick" >
                		 			openPactPage('<xsl:value-of select="../pk/contract_code"/>','2','01','<xsl:value-of select="../pk/comp_code"/>','<xsl:value-of select="../pk/copy_code"/>')
                		 </xsl:attribute>
                		 <xsl:value-of select="."/>
                		</a>
								 </td>
              </xsl:when>
              <xsl:when test="position()=14">
              	<td style='display:none'>
              	 <xsl:value-of select="."/>
              	</td>
              </xsl:when>
              <xsl:otherwise>
								<td>
		                 <xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

