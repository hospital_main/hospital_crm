<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		<td style="display:none"/>
      		
      	</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>付款单号</td>
        <td nowrap='true'>付款日期</td>
		  	<td nowrap='true'>供应商</td>
		  	<td nowrap='true'>发票号码</td>
		  	<td nowrap='true'>付款金额</td>
		  	<td nowrap='true'>合同编号</td>
		  	<td nowrap='true'>合同名称</td>
		  	<td nowrap='true'>合同期号</td>
		  	<td nowrap='true'>摘要</td>
		  	<td nowrap='true'>计划日期</td>
		  	<td nowrap='true'>计划金额</td>
		  	<td nowrap='true'>制单人</td>
		  	<td nowrap='true'>审核人</td>
		  	<td nowrap='true'>确认人</td>
		  	<td nowrap='true'>状态</td>
				
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
						
								<td align='left'><xsl:value-of select="."/></td>
							
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
