
<?xml version="1.0" encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead> 
  		<tr noWrap="true" class="mainHead">
			<th style='display:none'><input type='checkbox'/></th>
			<th nowrap='true'>购置清单编号</th>
			<th nowrap='true'>申请时间</th>			
			<th nowrap='true'>申请科室</th>
			<th nowrap='true'>申请理由</th>
		  	<th nowrap='true'>资产名称</th>
		  	<th nowrap='true'>数量</th>
		  	<th nowrap='true'>规格型号</th>
		  	<th nowrap='true'>科室主任</th>
		  	<th nowrap='true'>资产管理部门</th>
		  	<th nowrap='true'>主管院长</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'  style='display:none'>
					<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
						<xsl:attribute name="value" >
							<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
						</xsl:attribute>
					</input>
				</td>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position()=1">
							<td align='left'>
								<a tabindex='-1'>
									<xsl:attribute name="href" >
										javascript:selectPage('purchase_no','<xsl:value-of select="../pk/id"/>');
									</xsl:attribute><xsl:value-of select="."/>
								</a>
							</td>
						</xsl:when>					
						<xsl:otherwise>
							<td>
								<xsl:value-of select="."/>
							</td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
   		</xsl:for-each>    
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
