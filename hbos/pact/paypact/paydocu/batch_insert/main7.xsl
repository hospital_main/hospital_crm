<?xml version='1.0' encoding="GBK"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		<tr noWrap='true' class='mainHead'>
			<th style='display:none'><input type='checkbox'/></th>
			<th nowrap='true'>说明书及文字材料</th>
			<th nowrap='true'>保管人员</th>
		  	<th nowrap='true'>安装工程师</th>
		  	<th nowrap='true'>验收时间</th>
		  	<th nowrap='true'>质保期</th>
		  	<th nowrap='true'>验收意见</th>
		  	<th nowrap='true'>使用科室</th>
		  	<th nowrap='true'>采供办</th>
		  	<th nowrap='true'>资产管理部门</th>
		  	<th nowrap='true'>设备责任人</th>
		  	<th nowrap='true'>责任人联系方式</th>
		</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<td align='center'  style='display:none'>
				<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
					<xsl:attribute name="value" >
						<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
					</xsl:attribute>
				</input>
			</td>
        	<xsl:for-each select="td">
				<td>
					<xsl:value-of select="."/>
				</td>
			</xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

