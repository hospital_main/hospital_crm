<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
		<tr noWrap='true' class='mainHead'>
			<th style='display:none'><input type='checkbox'/></th>
			<th nowrap='true'>付款单号</th>
			<th nowrap='true'>付款日期</th>
		  	<th nowrap='true'>供应商</th>
		  	<th nowrap='true'>发票号码</th>
		  	<th nowrap='true'>采购发票导入</th>
		  	<th nowrap='true'>付款金额</th>
		  	<th nowrap='true'>合同编号</th>
		  	<th nowrap='true'>合同名称</th>
		  	<th nowrap='true'>业务员</th>
		  	<th nowrap='true'>制单人</th>
		  	<th nowrap='true'>审核人</th>
		  	<th nowrap='true'>确认人</th>
		  	<th nowrap='true'>状态</th>
		  	<th nowrap='true'>凭证</th>  
			<th nowrap='true'>合同文档查看</th>
		</tr>
      
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:if test="td[1] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">
        		<td align='center'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	              </xsl:attribute>
	            </input>
         		</td>
        	</xsl:if>
          
			<xsl:for-each select="td">
				<xsl:choose>
					<xsl:when test="position()=1">
						<xsl:if test=". = '合计'">
							<td align='left'>
								<xsl:value-of select="."/>
							</td>
						</xsl:if>
						<xsl:if test=". != '合计'">
							<td align='left'>
								<a tabindex='-1'>
									<xsl:attribute name="href" >
										javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:1000px;dialogHeight:600px', result)
									</xsl:attribute><xsl:value-of select="."/>
								</a>
							</td>
						</xsl:if>
					</xsl:when>
					<xsl:when test="position()=6">
						<td align='right'>
							<xsl:value-of select="."/>
						</td>
					</xsl:when>
					<xsl:when test="position()=7">
						<td align='left'>
							<a tabindex='-1'>
								<xsl:attribute name="href" >
									javascript:openPactPage('<xsl:value-of select="../td[7]"/>','2','01',getCompCode(),getCopyCode())
								</xsl:attribute><xsl:value-of select="."/>
							</a>
						</td>
					</xsl:when>
					<xsl:when test="position()=14">
						<td align='left'>
							<a tabindex='-1'>
								<xsl:attribute name="href" >
									javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
								</xsl:attribute><xsl:value-of select="."/>
							</a>
						</td>
					</xsl:when>
									
					<xsl:when test="position()=15">
						<td>
							<a href="#">
								<xsl:attribute name="onclick" >
									javascript:selectPage('pactWord','<xsl:value-of select="../pk/contract_code"/>');
								</xsl:attribute>
								<xsl:value-of select="."/>
							</a>
						</td>
					</xsl:when>
					<xsl:otherwise>
						<td>
							<xsl:value-of select="."/>
						</td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

