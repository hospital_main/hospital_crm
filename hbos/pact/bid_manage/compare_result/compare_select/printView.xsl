<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=""/>
	<xsl:template match="/">
		<root>
			<thead>
				<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  			<td style="display:none"></td>
	  		</tr>
				<tr noWrap="true" class="mainHead">
					<td>招标编号</td>
					<td>招标日期</td>
					<td>比价日期</td>
					<td>项目名称</td>
					<td>资产编码</td>
					<td>资产名称</td>
					<td>需求科室</td>
					<td>供应商</td>
					<td>数量</td>
					<td>中标价格</td>
					<td>中标金额</td>
					<td>预算单价</td>
					<td>预算金额</td>
					<td>未投标金额</td>
					<td>未中标金额</td>
					<td>差异</td>
					<td>状态</td>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<td align="left">
								<xsl:value-of select="."/>
							</td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>