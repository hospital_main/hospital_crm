<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox" />
				</th>
				<th>招标编号</th>
				<th>招标日期</th>
				<th>项目名称</th>
				<th>代理机构</th>
				<th>组织形式</th>
				<th>采购方式</th>
				<th>资产信息</th>
				<th>资产编码</th>
				<th>资产名称</th>
				<th>型号</th>
				<th>规格</th>				
				<th>品牌</th>
				<th>需求科室</th>
				<th>资金来源</th>
				<th>数量</th>
				<th>预算价格</th>
				<th>预算金额</th>
				<th>状态</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openDialog('update.html?load=&lt;change_code&gt;<xsl:value-of select="../td[1]"/>&lt;/change_code&gt;', 'dialogWidth:1000px;dialogHeight:500px',result)
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
