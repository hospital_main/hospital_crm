<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>合同编号</th>
				<th>合同名称</th>
				<th>签订日期</th>
				<th>供应商</th>
				<th>签订科室</th>
				<th>合同金额</th>
				<th>付款计划</th>
				<th>文档</th>			
				<th>贸易类别</th>
				<th>项目信息</th>
				<th>合同状态</th>
				<th>审核状态</th>
				<th>结转</th>
				<th>word文档</th>				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:if test="td[1] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
        	</xsl:if>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td>
										<xsl:value-of select="."/>
									</td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
											<a href="#">
												<xsl:attribute name="onclick" >
													openUpdate('<xsl:value-of select="../pk/contract_code"/>','<xsl:value-of select="../pk/create_way"/>');
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>											
										</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=6">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,###0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=7">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:selectPactPayPlan('readPact_yj.jsp','<xsl:value-of select="../pk/contract_code"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=8">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:selectPactFile('readPact_yj.jsp','<xsl:value-of select="../pk/contract_code"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=12">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:pactCQCheckList('readPact_yj.jsp','<xsl:value-of select="../pk/contract_code"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>									
								</xsl:when>
								<xsl:when test="position()=14">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:SetLinkUrl('readPact_yj.jsp','<xsl:value-of select="../pk/contract_code"/>');
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>									
								</xsl:when>
								
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
