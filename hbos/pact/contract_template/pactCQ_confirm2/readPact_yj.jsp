<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<%@ page import="com.viewhigh.base.sql.*,com.zhuozhengsoft.pageoffice.*,com.zhuozhengsoft.pageoffice.wordwriter.*,java.awt.*,javax.servlet.*,javax.servlet.http.*,java.sql.*,java.text.SimpleDateFormat,java.util.Date"%>
<%@ taglib uri="http://java.pageoffice.cn" prefix="po"%>
<%
	
	String contractCode = request.getParameter("contract_code").trim();
	
	// 设置PageOffice组件服务页面
	
	PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);
	poCtrl1.setServerPage(request.getContextPath()+"/poserver.zz"); //此行必须
  
	// 设置界面样式
	//poCtrl1.setCaption("购销合同");
	poCtrl1.setBorderStyle(BorderStyleType.BorderThin);
	// 添加自定义工具条按钮
	
	poCtrl1.addCustomToolButton("全屏/还原", "poSetFullScreen();", 4);
	poCtrl1.addCustomToolButton("关闭", "closePage();", 10);
	//隐藏工具栏
	poCtrl1.setOfficeToolbars(false);
	poCtrl1.setMenubar(false);

	
	// 打开文档
	
	String uurl="doc/"+contractCode+".docx";
	
	poCtrl1.webOpen(uurl, OpenModeType.docSubmitForm, "Tom");
	poCtrl1.setTagId("PageOfficeCtrl1"); //此行必须
	//-----------  PageOffice 服务器端编程结束  -------------------//
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
	<HEAD>
		<title>购销合同</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema"
			content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<script language="JavaScript">
	
</script>
		<form id="Form1" method="post">

			<!-- *********************pageoffice组件的使用 **************************-->
			<script type="text/javascript">
				
				//全屏
				function poSetFullScreen() {
					document.getElementById("PageOfficeCtrl1").FullScreen = !document.getElementById("PageOfficeCtrl1").FullScreen;
				}
				function closePage(){
					window.close();
				}
			</script>
			<po:PageOfficeCtrl id="PageOfficeCtrl1" />
			<!-- *********************pageoffice组件的使用 **************************-->

		</form>
	</body>
</HTML>
