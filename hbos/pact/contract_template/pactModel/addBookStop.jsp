<%@ page language="java" import="java.util.*,com.zhuozhengsoft.pageoffice.*,com.zhuozhengsoft.pageoffice.wordwriter.*" pageEncoding="gb2312"%>
<%@ taglib uri="http://java.pageoffice.cn" prefix="po"%>
<%
//******************************卓正PageOffice组件的使用*******************************
	String file_path = request.getParameter("file_path");
	PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);
	poCtrl1.setServerPage(request.getContextPath()+"/poserver.zz"); //此行必须
	poCtrl1.setOfficeToolbars(false);//隐藏office工具栏
	poCtrl1.setMenubar(false);
	poCtrl1.setSaveFilePage("SaveFile.jsp");
	String sss=request.getSession().getServletContext().getRealPath("/")+file_path;
	poCtrl1.webOpen(sss, OpenModeType.docNormalEdit, "张三");	
	poCtrl1.setTagId("PageOfficeCtrl1"); //此行必须	

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns:vh>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<link href="" rel="stylesheet" type="text/css">
		<script language='javascript' id='scriptID'></script>
		<script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>		
	</head>
	<body>
    <form id="form1">
		<table>
			<tr>
				<td>
					<input type=text id="txtBkName" class="inputSelect" name="txtBkName" label="书签名称" load="pact_model_shuqian"  required="true" maxInput="1" >
				</td>
				<td><input type=text class="inputTextA" id="txtBkText"  name="txtBkText" label="书签文本" value="_____________" ></td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="pageBtn" id="Button1" accessKey="C" onClick="addBookMark()">插入书签</button>
					<button class="pageBtn" id="Button2" accessKey="C" onClick="delBookMark()">删除书签</button>
					<button class="pageBtn" accessKey="C" onClick="save()">保存</button>
					<button class="pageBtn" accessKey="C" onClick="closePage();">关闭</button>
				</td>
			</tr>	
		</table>
		<div style=" width:auto; height:700px;">
			<po:PageOfficeCtrl id ="PageOfficeCtrl1" >
			</po:PageOfficeCtrl>
		</div>
    </form>
   <script type="text/javascript">
       
        var bkName = "";
        var bkText = "";
		//获取word文档中选中的文字
		function  getSelectionText(){
			if (document.getElementById("PageOfficeCtrl1").Document.Application.Selection.Range.Text != "") {
				return 
			}else{
				document.getElementById("PageOfficeCtrl1").Alert("您没有选择任何文本。");
				return "__________";
			}     
		}
        function addBookMark() {
			bkName = document.getElementById("txtBkName").value;
            bkText = document.getElementById("txtBkText").value;
			
			var getStr=document.getElementById("PageOfficeCtrl1").Document.Application.Selection.Range.Text;
			if(getStr==""){
				if(bkText==""){
					getStr="__________";
				}else{
					getStr=document.getElementById("txtBkText").value;
				}
			}
			
            var mac = "Function myfunc()" + " \r\n"
                    + "Dim r As Range " + " \r\n"
                    + "Set r = Application.Selection.Range " + " \r\n"
                    + "r.Text = \"" + getStr + "\"" + " \r\n"
                    + "Application.ActiveDocument.Bookmarks.Add Name:=\"" + bkName + "\", Range:=r " + " \r\n"
                    + "Application.ActiveDocument.Bookmarks(\"" + bkName + "\").Select " + " \r\n"
                    + "End Function " + " \r\n";
            document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", mac);
        }
        function delBookMark() {
            var mac = "Function myfunc()" + " \r\n"
                    + "If  Application.ActiveDocument.Bookmarks.Exists(\"" + bkName + "\") Then " + " \r\n"
                    + "    Application.ActiveDocument.Bookmarks(\"" + bkName + "\").Select " + " \r\n"
                    + "    Application.Selection.Range.Text = \"\" " + " \r\n"
                    + "End If " + " \r\n"
                    + "End Function " + " \r\n";
            document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", mac);
        }
		
		function save() {
			document.getElementById("PageOfficeCtrl1").WebSave();
			if (document.getElementById("PageOfficeCtrl1").CustomSaveResult == "ok") {
			   document.getElementById("PageOfficeCtrl1").Alert("文档保存成功!");
			}
		}
		function closePage() {
			document.getElementById("PageOfficeCtrl1").Close();
			window.close();
		}
    </script>
</body>
</html>