<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/> 
						
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>范本编号</td>
				<td nowrap='true'>定制日期</td>
				<td nowrap='true'>范本名称</td>
				<td nowrap='true'>范本类型</td>
				<td nowrap='true'>上传 下载</td>
				<td nowrap='true'>备注</td>
				<td nowrap='true'>操作人</td>
				<td nowrap='true'>审核人</td>
				<td nowrap='true'>状态</td>					
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<td align='left'><xsl:value-of select="."/></td>
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
