<%@ page language="java" import="java.util.*,com.zhuozhengsoft.pageoffice.*,com.zhuozhengsoft.pageoffice.wordwriter.*" pageEncoding="gb2312"%>
<%@ taglib uri="http://java.pageoffice.cn" prefix="po"%>
<%
//******************************卓正PageOffice组件的使用*******************************
	String file_path = request.getParameter("file_path");
	String contract_code = request.getParameter("contract_code");
	String user = request.getParameter("user");
	PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);
	poCtrl1.setServerPage(request.getContextPath()+"/poserver.zz"); //此行必须
	//隐藏菜单栏
	poCtrl1.setMenubar(false);
	//添加自定义按钮
	poCtrl1.setSaveFilePage("SaveFile.jsp?contract_code="+contract_code);
	String sss=request.getSession().getServletContext().getRealPath("/")+file_path;
	poCtrl1.webOpen(sss, OpenModeType.docRevisionOnly, user);
	poCtrl1.setTagId("PageOfficeCtrl1"); //此行必须	

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns:vh>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<link href="" rel="stylesheet" type="text/css">
		<script language='javascript' id='scriptID'></script>
		<script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>
		
		<title>在word当前光标处插入书签</title>
	</head>
	<body>
    <form id="form1">
		<table>
			<tr>
				<td>
					<input type=text id="txtBkName" class="inputSelect" name="txtBkName" label="书签名称" load="pact_model_shuqian"  required="true" maxInput="1" >
				</td>
				<td><button class="pageBtn" accessKey="C" onClick="locateBookMark()">定位书签位置</button></td>
			</tr>
			<tr>
				<td colspan="2">
					<button class="pageBtn" accessKey="C" onClick="save()">保存</button>
					<button class="pageBtn" accessKey="C" onClick="closePage();">关闭</button>
				</td>
			</tr>	
		</table>
		<div style=" width:auto; height:700px;">
			<po:PageOfficeCtrl id ="PageOfficeCtrl1"/>			
		</div>
    </form>
   <script type="text/javascript">
		
		function save() {
			document.getElementById("PageOfficeCtrl1").WebSave();
			if (document.getElementById("PageOfficeCtrl1").CustomSaveResult == "ok") {
			   document.getElementById("PageOfficeCtrl1").Alert("文档保存成功!");
			}
		}
		function closePage() {
			document.getElementById("PageOfficeCtrl1").Close();
			window.close();
		}
		//定位到书签位置
		function locateBookMark() {
            //获取书签名称
            var bkName = document.getElementById("txtBkName").value;
            //将光标定位到书签所在的位置
            var mac = "Function myfunc()" + " \r\n"
                    + "  ActiveDocument.Bookmarks(\""+ bkName +"\").Select " + " \r\n"
                    + "End Function " + " \r\n";
            document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", mac);
        }
		
    </script>
</body>
</html>