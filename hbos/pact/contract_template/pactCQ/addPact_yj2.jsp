<%@ page contentType="text/html; charset=gb2312" language="java" import="java.util.*,java.sql.*,java.math.BigDecimal,com.viewhigh.core.web.OperateHelper,com.viewhigh.core.dao.*" errorPage="" %>
<%@ page import="com.viewhigh.base.sql.*,com.zhuozhengsoft.pageoffice.*,com.zhuozhengsoft.pageoffice.wordwriter.*,java.awt.*,javax.servlet.*,javax.servlet.http.*,java.sql.*,java.text.SimpleDateFormat,java.util.Date"%>

<%@ taglib uri="http://java.pageoffice.cn" prefix="po"%>
<%
	Connection conn = ConnectionPool.get(); 
	Statement stmt = conn.createStatement();
	
	String contractCode = request.getParameter("contract_code");
	//String contractCode = "WCHT20160014";
	
	String pathSQL="select contract_path from  pact_file_path where contract_code='"+contractCode+"'";
	ResultSet res1 = stmt.executeQuery(pathSQL);
	String contract_path="";
	if(res1.next()){
		contract_path = res1.getString("contract_path");		
	}
	res1.close();
	
	WordDocument doc = new WordDocument();
	
	
	DataRegion contract_code = doc.openDataRegion("PO_合同编号");
	contract_code.setValue(contractCode);
	contract_code.setEditing(true);
	DataRegion pact_date = doc.openDataRegion("PO_签约日期_显示");
	//pact_date.getShading().setBackgroundPatternColor(Color.pink);
	//pact_date.setEditing(true);  
	DataRegion ven_code = doc.openDataRegion("PO_乙方");
	ven_code.setEditing(true);
	DataRegion ven_address = doc.openDataRegion("PO_乙方地址");
	ven_address.setEditing(true);
	DataRegion ven_youbian = doc.openDataRegion("PO_乙方邮编");
	ven_youbian.setEditing(true);
	
	DataRegion sign_date = doc.openDataRegion("PO_签约日期");
	//sign_date.getShading().setBackgroundPatternColor(Color.pink);
	sign_date.setEditing(true); 
	DataRegion pact_numery = doc.openDataRegion("PO_合同事宜");
	pact_numery.setEditing(true);
	DataRegion pact_detail = doc.openDataRegion("PO_合同明细");
	pact_detail.setEditing(false);
	DataRegion send_detail = doc.openDataRegion("PO_赠品明细");
	send_detail.setEditing(true);
	DataRegion pact_page_number = doc.openDataRegion("PO_配置清单附件页数");
	pact_page_number.setEditing(true);
	
	DataRegion jh_days = doc.openDataRegion("PO_交货天数");
	jh_days.setEditing(true); 
	DataRegion syx_months = doc.openDataRegion("PO_试运行月数");
	syx_months.setEditing(true);
	DataRegion one_number = doc.openDataRegion("PO_非预付_首款比率");
	one_number.setEditing(true);
	DataRegion other_number = doc.openDataRegion("PO_非预付_质保金比率");
	other_number.setEditing(true);
	DataRegion second_bank = doc.openDataRegion("PO_乙方开户银行");
	second_bank.setEditing(true);
	
	DataRegion second_bank_num = doc.openDataRegion("PO_乙方银行账号");
	second_bank_num.setEditing(true);
	DataRegion second_pact_num = doc.openDataRegion("PO_乙方合同份数");
	//second_pact_num.setEditing(true);
	DataRegion jsfwtk = doc.openDataRegion("PO_技术服务条款");
	jsfwtk.setEditing(true);
	DataRegion first_pact_num = doc.openDataRegion("PO_甲方合同份数");
	//first_pact_num.setEditing(true);
	DataRegion ss_address = doc.openDataRegion("PO_诉讼地点");
	ss_address.setEditing(true);
	
	DataRegion zb_days = doc.openDataRegion("PO_质保期");
	zb_days.setEditing(true);
	DataRegion zbtk = doc.openDataRegion("PO_质保条款");
	zbtk.setEditing(true);
	DataRegion pact_num = doc.openDataRegion("PO_合同份数");
	//pact_num.setEditing(true);
	
	
	DataRegion pact_jia_person = doc.openDataRegion("PO_甲方负责人");
	pact_jia_person.setEditing(true);
	DataRegion pact_yi_person = doc.openDataRegion("PO_乙方负责人");
	pact_yi_person.setEditing(true);
	DataRegion pact_yi_phone = doc.openDataRegion("PO_乙方负责人电话");
	pact_yi_phone.setEditing(true);

	
	
	//打开数据区域中的表格，OpenTable(index)方法中的index为word文档中表格的下标，从1开始
	com.zhuozhengsoft.pageoffice.wordwriter.Table table1 = doc.openDataRegion("PO_合同明细表").openTable(1);
	//设置表格边框样式
	table1.getBorder().setLineColor(Color.black);
	table1.getBorder().setLineWidth(WdLineWidth.wdLineWidth050pt);
	// 设置表头单元格文本居中
	table1.openCellRC(1, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 3).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 4).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 5).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 6).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 7).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 8).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	
	String amountSQL="SELECT CONVERT(VARCHAR(15),CAST(CONVERT(DECIMAL(12,2),LTRIM(contract_money)) as money ),1) AS contract_money,convert(numeric(15,2),contract_money) as contract_money_temp   FROM pact_payment_main t where t.contract_code='"+contractCode+"'";
	ResultSet rs = stmt.executeQuery(amountSQL);
	
	String contract_money="" ;
	double contract_money_temp=0.00 ;
	if(rs.next()){
		contract_money = rs.getString("contract_money");
		contract_money_temp = rs.getDouble("contract_money_temp");
	}
	rs.close();
	String contract_money_Big = "";
	
	String detailSQL="select item_name,item_spec,item_model,tt.nation_name,contract_amount,' ' as unit,contract_price,contract_money from  pact_payment_detail t left join sys_nation_dict tt on tt.nation_code =t.made_nation where t.contract_code='"+contractCode+"'";
	System.out.println(detailSQL);
	ResultSet res = stmt.executeQuery(detailSQL);
	
	int idx=1;
	String item_name;
	String item_spec;
	String item_model;
	String nation_name;
	String contract_amount;
	String unit;
	String contract_price;
	String contract_money1;
	while(res.next()){
		if(idx>=2) 
			table1.insertRowAfter(table1.openCellRC(idx, 8));
		
		idx++;		
		
		table1.openCellRC(idx, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
		table1.openCellRC(idx, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
		table1.openCellRC(idx, 3).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
		table1.openCellRC(idx, 4).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
		table1.openCellRC(idx, 5).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
		table1.openCellRC(idx, 6).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
		table1.openCellRC(idx, 7).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
		table1.openCellRC(idx, 8).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
		
		table1.openCellRC(idx, 1).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
		table1.openCellRC(idx, 2).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
		table1.openCellRC(idx, 3).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
		table1.openCellRC(idx, 4).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
		table1.openCellRC(idx, 5).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
		table1.openCellRC(idx, 6).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
		table1.openCellRC(idx, 7).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
		table1.openCellRC(idx, 8).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);		
		
		item_name = res.getString(1)!=null?res.getString(1):"";
		item_spec = res.getString(2)!=null?res.getString(2):"";
		item_model = res.getString(3)!=null?res.getString(3):"";
		nation_name = res.getString(4)!=null?res.getString(4):"";
		contract_amount = res.getString(5)!=null?res.getString(5):"";
		unit = res.getString(6)!=null?res.getString(6):"";
		contract_price = res.getString(7)!=null?res.getString(7):"";
		contract_money1 = res.getString(8)!=null?res.getString(8):"";
		
		table1.openCellRC(idx, 1).setValue(res.getString(1).trim());
		table1.openCellRC(idx, 2).setValue(item_spec.trim());
		table1.openCellRC(idx, 3).setValue(item_model.trim());
		table1.openCellRC(idx, 4).setValue(nation_name.trim());
		table1.openCellRC(idx, 5).setValue(contract_amount.trim());
		table1.openCellRC(idx, 6).setValue(unit.trim());
		table1.openCellRC(idx, 7).setValue(contract_price.trim());
		table1.openCellRC(idx, 8).setValue(contract_money1.trim());
	}
	/**************金额转大写***************/
	
		// 数字表示
		char[] CHINESE_DIGIT= { '零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖' };

		// 单位无限大
		String POS[] = new String[] { "分", "角", "元", "拾", "佰", "仟", "万", "拾", "佰", "仟", "亿" };
		
		String money = contract_money_temp+"";
		
        BigDecimal bdMoney = BigDecimal.ZERO;
        try {
            // 检查数字正确性,移除首位空格
            bdMoney = new BigDecimal(money);
        } catch (Exception ex) {
            throw new IllegalArgumentException("不是正确的数字金额!");
        }
        // 检查小数位长度
        money = bdMoney.toString();
        if (money.indexOf('.') >= 0 && (money.length() - money.indexOf('.')) > 3) {

            throw new IllegalArgumentException("不是正确的数字金额!");
        }

        // 得到纯净的 '0'~'9'的字符串
        money = bdMoney.multiply(new BigDecimal("100")).abs().toString();
        money = money.indexOf('.') >= 0 ? money.substring(0, money.indexOf('.')) : money;

        char nowChar;
        String posStr;
        int posIndex;
        boolean oldCharIsZero = true;
        StringBuilder stb = new StringBuilder();

        for (int i = money.length() - 1; i >= 0; i--) {

            nowChar = money.charAt(i);
            posIndex = money.length() - 1 - i;
            // 如果长度超出了表达长度,则从"拾", "佰", "仟", "万", "拾", "佰", "仟", "亿"重新统计
            posIndex = posIndex >= POS.length ? (posIndex - 3) % (POS.length - 3) + 3 : posIndex;
            posStr = POS[posIndex];

            if (nowChar == '0') {
                if (posStr == "亿" || posStr == "万" || posStr == "元") {
                    stb.append(posStr);
                }
                // 如果旧字符不是0,则追加零,否则 当前的零就不考虑
                if (!oldCharIsZero) {
                    stb.append("零");
                }
                oldCharIsZero = true;
            } else {
                oldCharIsZero = false;
                stb.append(posStr).append(CHINESE_DIGIT[nowChar - '0']);
            }
        }

        if (bdMoney.signum() == -1) {
            stb.append("负");
        }
        stb.reverse();
        String ret = stb.toString();
        while(ret.indexOf("亿万")>=0){
            ret = ret.replace("亿万", "亿");
        }
        ret = ret.indexOf("分") < 0 ? ret + "整" : ret;
	
		contract_money_Big = ret;
	
	/*************金额转大写结束****************/
	if(idx>1){
		table1.insertRowAfter(table1.openCellRC(idx, 8));
		table1.openCellRC(idx+1,2).mergeTo(idx+1,8);
		table1.openCellRC(idx+1, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
		table1.openCellRC(idx+1, 2).getFont().setBold(true);
		table1.openCellRC(idx+1, 2).setValue("总金额：￥"+contract_money+"元　大写：人民币"+contract_money_Big);
	}
	res.close();
	stmt.close();
	conn.close();
	
	Table table2 = doc.openDataRegion("PO_赠品明细").openTable(1);
	//设置表格边框样式
	table2.getBorder().setLineColor(Color.black);
	table2.getBorder().setLineWidth(WdLineWidth.wdLineWidth050pt);
	// 设置表头单元格文本居中
	table2.openCellRC(1, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table2.openCellRC(1, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table2.openCellRC(1, 3).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table2.openCellRC(1, 4).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table2.openCellRC(1, 5).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table2.openCellRC(1, 6).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table2.openCellRC(1, 7).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	
	
	
	
	// 设置PageOffice组件服务页面
	
	PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);
	poCtrl1.setServerPage(request.getContextPath()+"/poserver.zz"); //此行必须

	// 设置界面样式
	//poCtrl1.setCaption("购销合同");
	poCtrl1.setVisible(true); 
	poCtrl1.setBorderStyle(BorderStyleType.BorderThin);
	// 添加自定义工具条按钮
	poCtrl1.setJsFunction_AfterDocumentOpened("AfterDocumentOpened()");
	poCtrl1.addCustomToolButton("保存", "poSave();", 1);
	poCtrl1.addCustomToolButton("全屏/还原", "poSetFullScreen();", 4);
	poCtrl1.addCustomToolButton("新建批注", "InsertComment()", 3); 
	poCtrl1.addCustomToolButton("关闭", "closePage();", 10);
	//隐藏工具栏
	poCtrl1.setOfficeToolbars(false);
	poCtrl1.setMenubar(false);

	poCtrl1.setJsFunction_OnWordDataRegionClick("OnWordDataRegionClick()");
	
	// 设置保存文档的服务器页面
	//String uulStr="SaveData.jsp?contract_code="+contractCode;
	poCtrl1.setSaveDataPage("SaveData.jsp?contract_code="+contractCode);
	poCtrl1.setSaveFilePage("SaveFile.jsp?contract_code="+contractCode);
	
	////获取数据对象
	poCtrl1.setWriter(doc);
	// 打开文档	
	
	if(contract_path == null || contract_path.equals("")){
		poCtrl1.webOpen("doc/addPact_yj.doc", OpenModeType.docRevisionOnly, "Tom");
		//poCtrl1.webOpen("doc/addPact_yj.doc", OpenModeType.docSubmitForm, "Tom");
	}else{
		System.out.println(contract_path);
		poCtrl1.webOpen(contract_path, OpenModeType.docRevisionOnly, "Tom");
		//poCtrl1.webOpen(contract_path, OpenModeType.docSubmitForm, "Tom");
	}
	
	
	poCtrl1.setTagId("PageOfficeCtrl1"); //此行必须
	//-----------  PageOffice 服务器端编程结束  -------------------//
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns:vh>
	<HEAD>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<link href="" rel="stylesheet" type="text/css">
		<script language='javascript' id='scriptID'></script>
		
		<script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>
		
		<title>购销合同</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
	<script language="JavaScript">
		function GetQueryString(name){ 
			var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); 
			var r = window.location.search.substr(1).match(reg); 
			if(r!=null)return  unescape(r[2]); 
			return null; 
		}
		
		function saveDocument(){ 
		    return null; 
		}
		
		function OnWordDataRegionClick(Name, Value, Left, Bottom) {
			if (Name == "PO_签约日期") {
				var strRet = document.getElementById("PageOfficeCtrl1").ShowHtmlModalDialog("datetimer2.htm", Value, "left=" + Left + "px;top=" + Bottom + "px;width=260px;height=260px;frame=no;");
				if (strRet != "") {
					var dataRegionList = document.getElementById("PageOfficeCtrl1").DataRegionList;//获取Word文件中的数据区域列表
					var dataRegionItem = dataRegionList.GetDataRegionByName("PO_签约日期_显示");//获取名称为“PO_name”的数据区域对象
					dataRegionItem.value=strRet;
					return (strRet);
				}
				else {
					if ((Value == undefined) || (Value == ""))
						return " ";
					else
						return Value;
				}
			}
			if (Name == "PO_乙方") {
				var contractCode = GetQueryString('contract_code');
				var strRet = window.showModalDialog("selectVencode.htm?contract_code="+contractCode, window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
				if (strRet != "") {
					return (strRet);
				}
				else {
					if ((Value == undefined) || (Value == ""))
						return " ";
					else
						return Value;
				}
			}
			if (Name == "PO_试运行月数") {
				var contractCode = GetQueryString('contract_code');
				var strRet = window.showModalDialog("selectTestMoths.htm?contract_code="+contractCode, window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
				if (strRet != "") {
					return (strRet);
				}
				else {
					if ((Value == undefined) || (Value == ""))
						return " ";
					else
						return Value;
				}
			}
			if (Name == "PO_质保期") {
				var strRet = window.showModalDialog("baozhiqi.htm", window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
				if (strRet != "") {					
					return (strRet);
				}
				else {
					if ((Value == undefined) || (Value == ""))
						return " ";
					else
						return Value;
				}
			}
			if (Name == "PO_合同份数") {
				var strRet = window.showModalDialog("pact_number.htm", window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
				if (strRet != "") {
					return (strRet);
				}
				else {
					if ((Value == undefined) || (Value == ""))
						return " ";
					else
						return Value;
				}
			}
			if (Name == "PO_乙方合同份数") {
				var strRet = window.showModalDialog("pact_number.htm", window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
				if (strRet != "") {
					return (strRet);
				}
				else {
					if ((Value == undefined) || (Value == ""))
						return " ";
					else
						return Value;
				}
			}
			if (Name == "PO_甲方合同份数") {
				var strRet = window.showModalDialog("pact_number.htm", window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
				if (strRet != "") {
					return (strRet);
				}
				else {
					if ((Value == undefined) || (Value == ""))
						return " ";
					else
						return Value;
				}
			}
			if (Name == "PO_非预付_首款比率") {
				var strRet = window.showModalDialog("shoukuanbili.htm", window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
				if (strRet != "") {
					var dataRegionList = document.getElementById("PageOfficeCtrl1").DataRegionList;//获取Word文件中的数据区域列表
					var dataRegionItem = dataRegionList.GetDataRegionByName("PO_非预付_质保金比率");//获取名称为“PO_name”的数据区域对象
					dataRegionItem.value=(100-strRet);
					return (strRet);
				}
				else {
					if ((Value == undefined) || (Value == ""))
						return " ";
					else
						return Value;
				}
			}
			if (Name == "PO_非预付_质保金比率") {
				var strRet = window.showModalDialog("zhibaobili.htm", window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
				if (strRet != "") {
					var dataRegionList = document.getElementById("PageOfficeCtrl1").DataRegionList;//获取Word文件中的数据区域列表
					var dataRegionItem = dataRegionList.GetDataRegionByName("PO_非预付_首款比率");//获取名称为“PO_name”的数据区域对象
					dataRegionItem.value=(100-strRet);
					return (strRet);
				}
				else {
					if ((Value == undefined) || (Value == ""))
						return " ";
					else
						return Value;
				}
			}
			
			if (Name == "PO_甲方负责人") {
				var contractCode = GetQueryString('contract_code');
				var strRet = window.showModalDialog("selectPerson.htm?contract_code="+contractCode, window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
				if (strRet != "") {
					return (strRet);
				}
				else {
					if ((Value == undefined) || (Value == ""))
						return " ";
					else
						return Value;
				}
			}
			if (Name == "PO_合同明细") {
				var contractCode = GetQueryString('contract_code');
				poSave();
				window.showModalDialog("pact_datail.html?contract_code="+contractCode, window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=1200px;dialogHeight=600px;");
				window.location.reload();
				return ;
			}
		}
	
		function poSave() {
			document.getElementById("PageOfficeCtrl1").WebSave();
			//document.getElementById("PageOfficeCtrl1").WebSaveAsPDF();
			if (document.getElementById("PageOfficeCtrl1").CustomSaveResult == "ok") {
			   document.getElementById("PageOfficeCtrl1").Alert("文档保存成功!");
			}
		}
		//全屏
		function poSetFullScreen() {
			document.getElementById("PageOfficeCtrl1").FullScreen = !document.getElementById("PageOfficeCtrl1").FullScreen;
		}
		function closePage(){
			//var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
			//parent.layer.close(index);
			document.getElementById("PageOfficeCtrl1").Close();
			//window.close();
			//closePage();
		}
		
		function AfterDocumentOpened() {
            refreshList();
			refreshList_pz();
        }
        //获取当前痕迹列表
        function refreshList() {
            var i;
            document.getElementById("ul_Comments").innerHTML = "";
            for (i = 1; i <= document.getElementById("PageOfficeCtrl1").Document.Revisions.Count; i++) {
                var str = "";
                str = str + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Author;
                var  revisionDate=document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Date;
                 //转换为标准时间
                str=str+" "+dateFormat(revisionDate,"yyyy-MM-dd HH:mm:ss");
                if (document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Type == "1") {
                    str = str + ' 插入：' + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Range.Text;
                }else if (document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Type == "2") {
                    str = str + ' 删除：' + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Range.Text;
                }else {
                    str = str + ' 调整格式或样式。';
                }
                document.getElementById("ul_Comments").innerHTML += "<li><a href='#' onclick='goToRevision(" + i + ")'>" + str + "</a></li>"
            }

        }
         //GMT时间格式转换为CST
        dateFormat = function (date, format) {
            date = new Date(date); 
            var o = {
                'M+' : date.getMonth() + 1, //month
                'd+' : date.getDate(), //day
                'H+' : date.getHours(), //hour
                'm+' : date.getMinutes(), //minute
                's+' : date.getSeconds(), //second
                'q+' : Math.floor((date.getMonth() + 3) / 3), //quarter
                'S' : date.getMilliseconds() //millisecond
            };
			if (/(y+)/.test(format))
                format = format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
 
            for (var k in o)
                if (new RegExp('(' + k + ')').test(format))
                    format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));
 
            return format;
        }

        //定位到当前痕迹
        function goToRevision(index){
	        var sMac = "Sub myfunc() " + "\r\n"+ "ActiveDocument.Revisions.Item("+index+").Range.Select " + "\r\n"+ "End Sub ";
			document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
	    }
		//刷新列表
		function refresh_click(){
	       refreshList();    
        }

		function Button2_onclick() {
            refreshList_pz();
        }

        function InsertComment() {
            document.getElementById("PageOfficeCtrl1").WordInsertComment();
            var sMac = "Sub myfunc() " + "\r\n"
                     + "On Error Resume Next " + "\r\n"
                     + "ActiveWindow.ActivePane.Close " + "\r\n"
                     + "End Sub ";
            document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
        }
		
		function refreshList_pz() {
            var sMac = "Function getComments() " + "\r\n"
                     + "Dim cmts As String " + "\r\n"
                     + "For i=1 To ActiveDocument.Comments.Count "+ "\r\n"
                     + "    cmts = cmts +ActiveDocument.Comments.Item(i).Author & \":\" & ActiveDocument.Comments.Item(i).Range.Text + \"||\" " + "\r\n"
                     + "Next" + "\r\n"
                     + "getComments = cmts" + "\r\n"
                     + "End Function ";

            var sComments = document.getElementById("PageOfficeCtrl1").RunMacro("getComments", sMac);

            var arr = sComments.split("||");

            document.getElementById("ul_Comments2").innerHTML = "";
            for (var i = 0; i < arr.length-1 ; i++) {
                document.getElementById("ul_Comments2").innerHTML += "<li><a href='#' onclick='goToComment("+(i+1)+")'>"+arr[i]+"</a></li>"
            }
        }
        
        function getComment(index){
            var sMac = "Function getCmtTxt() " + "\r\n"+ "getCmtTxt = ActiveDocument.Comments.Item(" + index + ").Range.Text " + "\r\n"+ "End Function ";
			return document.getElementById("PageOfficeCtrl1").RunMacro("getCmtTxt", sMac);
        }
        function goToComment(index){
	        var sMac = "Sub myfunc() " + "\r\n"+ "ActiveDocument.Comments.Item("+index+").Edit " + "\r\n"+ "End Sub ";
			document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
	    }
	</script>
	<div  style=" width:100%; height:700px;">
        <div id="Div_Comments" style="float:left; width:12%; height:700px; border:solid 1px red;">
			<h3>痕迹列表</h3>
			<input type="button" name="refresh" value="刷新"onclick=" return refresh_click()"/>
			<ul id="ul_Comments">
				
			</ul>
        </div>
		
		<div style="float:left;width:76%; height:700px;">
			<po:PageOfficeCtrl id="PageOfficeCtrl1" >
			</po:PageOfficeCtrl>
		</div>
		<div id="Div_Comments2" style="float:left;width:10%; height:700px; border:solid 1px red;">
			<h3>批注列表</h3>
			<input id="Button2" type="button" value="刷新" onclick="return Button2_onclick()" />
			<ul id="ul_Comments2">
            
			</ul>
        </div>
		<!--iframe src="#" width="100%" name="content"> </iframe--> 
    </div>
	
	</body>
	
</HTML>
