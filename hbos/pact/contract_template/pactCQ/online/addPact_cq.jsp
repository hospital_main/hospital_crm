<%@ page contentType="text/html; charset=gb2312" language="java" import="java.util.*,java.sql.*,java.math.BigDecimal,com.viewhigh.core.web.OperateHelper,com.viewhigh.core.dao.*" errorPage="" %>
<%@ page import="com.viewhigh.base.sql.*,com.zhuozhengsoft.pageoffice.*,com.zhuozhengsoft.pageoffice.wordwriter.*,java.awt.*,javax.servlet.*,javax.servlet.http.*,java.sql.*,java.text.SimpleDateFormat,java.util.Date"%>

<%@ taglib uri="http://java.pageoffice.cn" prefix="po"%>
<%
//******************************卓正PageOffice组件的使用*******************************
	String file_path = request.getParameter("file_path");
	String contract_code = request.getParameter("contract_code");
	String user = request.getParameter("user");
	Connection conn = ConnectionPool.get(); 
	Statement stmt = conn.createStatement();
	/***********************获取合同路径 begin*********************************/
	String pathSQL="select contract_path from  pact_file_path where contract_code='"+contract_code+"'";
	ResultSet res1 = stmt.executeQuery(pathSQL);
	String contract_path="";
	if(res1.next()){
		contract_path = res1.getString("contract_path");		
	}
	res1.close();
	stmt.close();
	conn.close();
	/************************获取合同路径 end********************************/
	
	/************************打开书签 begin********************************/
	
	
	WordDocument doc = new WordDocument();
	DataRegion contract_code_old = doc.openDataRegion("PO_合同编号");
	contract_code_old.setValue("合同编号");
	//contract_code_old.setEditing(true);
	DataRegion pact_date = doc.openDataRegion("PO_签约日期_显示");
	DataRegion singn_date = doc.openDataRegion("PO_签约日期");
	/************************打开书签 end********************************/
	
	PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);
	poCtrl1.setServerPage(request.getContextPath()+"/poserver.zz"); //此行必须
	//隐藏菜单栏
	poCtrl1.setMenubar(false);
	//添加自定义按钮
	poCtrl1.setJsFunction_AfterDocumentOpened("AfterDocumentOpened()");
	poCtrl1.setJsFunction_OnWordDataRegionClick("OnWordDataRegionClick()");
	poCtrl1.setSaveFilePage("SaveFile.jsp?contract_code="+contract_code);
	poCtrl1.setSaveDataPage("SaveData.jsp?contract_code="+contract_code);
	//打开word文档
	String sss=request.getSession().getServletContext().getRealPath("/")+file_path;
	String ppp=request.getSession().getServletContext().getRealPath("/")+contract_path;
	System.out.println(sss);
	System.out.println(ppp);
	if(contract_path == null || contract_path.equals("")){
		poCtrl1.webOpen(sss, OpenModeType.docRevisionOnly, user);
	}else{
		poCtrl1.webOpen(ppp, OpenModeType.docRevisionOnly, user);
	}
	poCtrl1.setTagId("PageOfficeCtrl1"); //此行必须	

%>
<html xmlns:vh>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="" rel="stylesheet" type="text/css">
		<script language="javascript" id="scriptID"></script>
    <script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>	
	</head>
	<body class="subBody" id="subBody">
	<div  style=" width:100%; height:700px;">
			<div id="Div_Comments" style="float:left; width:12%; height:700px; border:solid 1px red;">
				<h3>痕迹列表</h3>
				<input type="button" name="refresh" value="刷新"onclick=" return refresh_click()"/>
				<ul id="ul_Comments">
					
				</ul>
			</div>
			
			<div style="float:left;width:76%; height:700px;">
				<table>
					<tr>
						<td>
							<input type=text id="txtBkName" class="inputSelect" name="txtBkName" label="书签名称" load="pact_model_shuqian"  required="true" maxInput="1" >
						</td>
						<td>
							<button class="pageBtn" accessKey="C" onClick="locateBookMark()">定位书签位置</button>
						</td>
						<td>
							<button class="pageBtn" accessKey="C" onClick="save()">保存</button>
							<button class="pageBtn" accessKey="C" onClick="save2()">保存2</button>
							<button class="pageBtn" accessKey="C" onClick="closePage();">关闭</button>
						</td>
					</tr>
					
				</table>
				<div>
					<po:PageOfficeCtrl id="PageOfficeCtrl1" >
					</po:PageOfficeCtrl>
				</div>
			</div>
			<div id="Div_Comments2" style="float:left;width:10%; height:700px; border:solid 1px red;">
				<h3>批注列表</h3>
				<input id="Button2" type="button" value="刷新" onclick="return Button2_onclick()" />
				<ul id="ul_Comments2">
				
				</ul>
			</div>
			<iframe src="#" width="100%" name="content"></iframe> 
		</div>
    <script language="javascript">
		function init(){
			refresh_click();
			Button2_onclick();
		}
		function GetQueryString(name){ 
		     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); 
		     var r = window.location.search.substr(1).match(reg); 
		     if(r!=null)return  unescape(r[2]); 
		     return null; 
		}	
		//定位到书签位置
		function locateBookMark() {
            //获取书签名称
            var bkName = document.getElementById("txtBkName").value;
            //将光标定位到书签所在的位置
            var mac = "Function myfunc()" + " \r\n"
                    + "  ActiveDocument.Bookmarks(\""+ bkName +"\").Select " + " \r\n"
                    + "End Function " + " \r\n";
            document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", mac);
        }
		
		function save() {
			document.getElementById("PageOfficeCtrl1").WebSave();
			if (document.getElementById("PageOfficeCtrl1").CustomSaveResult == "ok") {
			   document.getElementById("PageOfficeCtrl1").Alert("文档保存成功!");
			}
		}
		function save2() {
			var doc = new WordDocument(request, response);
			var contract_code_old = doc.openDataRegion("PO_合同编号").getValue().trim();
			alert(contract_code_old)
		}
		function closePage() {
			document.getElementById("PageOfficeCtrl1").Close();
			window.close();
		}
		
		function AfterDocumentOpened() {
            refreshList();
			refreshList_pz();
        }
        //获取当前痕迹列表
        function refreshList() {
            var i;
            document.getElementById("ul_Comments").innerHTML = "";
            for (i = 1; i <= document.getElementById("PageOfficeCtrl1").Document.Revisions.Count; i++) {
                var str = "";
                str = str + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Author;
                var  revisionDate=document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Date;
                 //转换为标准时间
                str=str+" "+dateFormat(revisionDate,"yyyy-MM-dd HH:mm:ss");
                if (document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Type == "1") {
                    str = str + ' 插入：' + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Range.Text;
                }else if (document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Type == "2") {
                    str = str + ' 删除：' + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Range.Text;
                }else {
                    str = str + ' 调整格式或样式。';
                }
                document.getElementById("ul_Comments").innerHTML += "<li><a href='#' onclick='goToRevision(" + i + ")'>" + str + "</a></li>"
            }

        }
         //GMT时间格式转换为CST
        dateFormat = function (date, format) {
            date = new Date(date); 
            var o = {
                'M+' : date.getMonth() + 1, //month
                'd+' : date.getDate(), //day
                'H+' : date.getHours(), //hour
                'm+' : date.getMinutes(), //minute
                's+' : date.getSeconds(), //second
                'q+' : Math.floor((date.getMonth() + 3) / 3), //quarter
                'S' : date.getMilliseconds() //millisecond
            };
			if (/(y+)/.test(format))
                format = format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
 
            for (var k in o)
                if (new RegExp('(' + k + ')').test(format))
                    format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));
 
            return format;
        }

        //定位到当前痕迹
        function goToRevision(index){
	        var sMac = "Sub myfunc() " + "\r\n"+ "ActiveDocument.Revisions.Item("+index+").Range.Select " + "\r\n"+ "End Sub ";
			document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
	    }
		//刷新列表
		function refresh_click(){
	       refreshList();    
        }

		function Button2_onclick() {
            refreshList_pz();
        }

        function InsertComment() {
            document.getElementById("PageOfficeCtrl1").WordInsertComment();
            var sMac = "Sub myfunc() " + "\r\n"
                     + "On Error Resume Next " + "\r\n"
                     + "ActiveWindow.ActivePane.Close " + "\r\n"
                     + "End Sub ";
            document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
        }
		
		function refreshList_pz() {
            var sMac = "Function getComments() " + "\r\n"
                     + "Dim cmts As String " + "\r\n"
                     + "For i=1 To ActiveDocument.Comments.Count "+ "\r\n"
                     + "    cmts = cmts +ActiveDocument.Comments.Item(i).Author & \":\" & ActiveDocument.Comments.Item(i).Range.Text + \"||\" " + "\r\n"
                     + "Next" + "\r\n"
                     + "getComments = cmts" + "\r\n"
                     + "End Function ";

            var sComments = document.getElementById("PageOfficeCtrl1").RunMacro("getComments", sMac);

            var arr = sComments.split("||");

            document.getElementById("ul_Comments2").innerHTML = "";
            for (var i = 0; i < arr.length-1 ; i++) {
                document.getElementById("ul_Comments2").innerHTML += "<li><a href='#' onclick='goToComment("+(i+1)+")'>"+arr[i]+"</a></li>"
            }
        }
        
        function getComment(index){
            var sMac = "Function getCmtTxt() " + "\r\n"+ "getCmtTxt = ActiveDocument.Comments.Item(" + index + ").Range.Text " + "\r\n"+ "End Function ";
			return document.getElementById("PageOfficeCtrl1").RunMacro("getCmtTxt", sMac);
        }
        function goToComment(index){
	        var sMac = "Sub myfunc() " + "\r\n"+ "ActiveDocument.Comments.Item("+index+").Edit " + "\r\n"+ "End Sub ";
			document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
	    }
		
		function OnWordDataRegionClick(Name, Value, Left, Bottom) {
			if (Name == "PO_签约日期") {
				var strRet = document.getElementById("PageOfficeCtrl1").ShowHtmlModalDialog("datetimer2.htm", Value, "left=" + Left + "px;top=" + Bottom + "px;width=260px;height=260px;frame=no;");
				if (strRet != "") {
					var dataRegionList = document.getElementById("PageOfficeCtrl1").DataRegionList;//获取Word文件中的数据区域列表
					var dataRegionItem = dataRegionList.GetDataRegionByName("PO_签约日期_显示");//获取名称为“PO_name”的数据区域对象
					dataRegionItem.value=strRet;
					return (strRet);
				}
				else {
					if ((Value == undefined) || (Value == ""))
						return " ";
					else
						return Value;
				}
			}
			if (Name == "PO_乙方") {
				var contractCode = GetQueryString('contract_code');
				var strRet = window.showModalDialog("selectVencode.htm?contract_code="+contractCode, window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
				if (strRet != "") {
					return (strRet);
				}
				else {
					if ((Value == undefined) || (Value == ""))
						return " ";
					else
						return Value;
				}
			}
			if (Name == "PO_合同明细") {
				var contractCode = GetQueryString('contract_code');
				poSave();
				window.showModalDialog("pact_datail.html?contract_code="+contractCode, window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=1200px;dialogHeight=600px;");
				window.location.reload();
				return ;
			}
		}
	</script>
	</body>
</html>
