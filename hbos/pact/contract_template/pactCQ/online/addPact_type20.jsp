<%@ page contentType="text/html; charset=gb2312" language="java" import="java.util.*,java.sql.*,java.math.BigDecimal,com.viewhigh.core.web.OperateHelper,com.viewhigh.core.dao.*" errorPage="" %>
<%@ page import="com.viewhigh.base.sql.*,com.zhuozhengsoft.pageoffice.*,com.zhuozhengsoft.pageoffice.wordwriter.*,java.awt.*,javax.servlet.*,javax.servlet.http.*,java.sql.*,java.text.SimpleDateFormat,java.util.Date"%>

<%@ taglib uri="http://java.pageoffice.cn" prefix="po"%>
<%
	Connection conn = ConnectionPool.get(); 
	Statement stmt = conn.createStatement();
	
	
	String file_path = request.getParameter("file_path");
	String contract_code = request.getParameter("contract_code");
	String user = request.getParameter("user");
	//int dcount = (int)request.getParameter("dcount");
	String dcount1 =request.getParameter("dcount");
	int dcount =Integer.parseInt(dcount1);
	
	String pathSQL="select contract_path from  pact_file_path where contract_code='"+contract_code+"'";
	ResultSet res1 = stmt.executeQuery(pathSQL);
	
	String contract_path="";
	if(res1.next()){
		contract_path = res1.getString("contract_path");		
	}
	res1.close();
	
	String ven_name ="";
	String first_man ="";
	String second_man ="";
	String second_phone ="";
	int detail_count=0;	
	String pactStr="select sv.ven_name,second_man,second_phone,em.emp_name,detail_count from pact_payment_cq_main ma left join sys_vendor_dict sv on ma.ven_code=sv.ven_code left join sys_emp em on ma.first_man=em.emp_code  where contract_code='"+contract_code+"'";
	ResultSet res2 = stmt.executeQuery(pactStr);
	if(res2.next()){
		ven_name = res2.getString("ven_name");		
		second_man = res2.getString("second_man");		
		second_phone = res2.getString("second_phone");	
		first_man = res2.getString("emp_name");				
		detail_count = res2.getInt("detail_count");				
	}
	res2.close();
	
	//System.out.println("second_phone="+second_phone);
	
	
	
	WordDocument doc = new WordDocument();
	
	DataRegion contract_code_old = doc.openDataRegion("PO_合同编号");
	contract_code_old.setValue(contract_code);
	contract_code_old.setEditing(true);
	DataRegion pact_date = doc.openDataRegion("PO_签约日期_显示");
	//pact_date.getShading().setBackgroundPatternColor(Color.pink);
	//pact_date.setEditing(true);  
	//pact_date.setValue();
	DataRegion ven_code = doc.openDataRegion("PO_乙方");
	ven_code.setEditing(true);
	ven_code.setValue(ven_name);
	DataRegion ven_address = doc.openDataRegion("PO_乙方地址");
	ven_address.setEditing(true);
	DataRegion ven_youbian = doc.openDataRegion("PO_乙方邮编");
	ven_youbian.setEditing(true);
	
	DataRegion sign_date = doc.openDataRegion("PO_签约日期");
	//sign_date.getShading().setBackgroundPatternColor(Color.pink);
	sign_date.setEditing(true); 
	DataRegion pact_numery = doc.openDataRegion("PO_合同事宜");
	pact_numery.setEditing(true);
	DataRegion pact_detail = doc.openDataRegion("PO_合同明细");
	pact_detail.setEditing(false);
	DataRegion send_detail = doc.openDataRegion("PO_赠品明细");
	send_detail.setEditing(true);
	DataRegion pact_page_number = doc.openDataRegion("PO_配置清单附件页数");
	pact_page_number.setEditing(true);
	
	DataRegion jh_days = doc.openDataRegion("PO_交货天数");
	jh_days.setEditing(true); 
	DataRegion syx_months = doc.openDataRegion("PO_试运行月数");
	syx_months.setEditing(true);
	DataRegion one_number = doc.openDataRegion("PO_非预付_首款比率");
	one_number.setEditing(true);
	DataRegion other_number = doc.openDataRegion("PO_非预付_质保金比率");
	other_number.setEditing(true);
	DataRegion second_bank = doc.openDataRegion("PO_乙方开户银行");
	second_bank.setEditing(true);
	
	DataRegion second_bank_num = doc.openDataRegion("PO_乙方银行账号");
	second_bank_num.setEditing(true);
	DataRegion second_pact_num = doc.openDataRegion("PO_乙方合同份数");
	//second_pact_num.setEditing(true);
	DataRegion jsfwtk = doc.openDataRegion("PO_技术服务条款");
	jsfwtk.setEditing(true);
	DataRegion first_pact_num = doc.openDataRegion("PO_甲方合同份数");
	//first_pact_num.setEditing(true);
	DataRegion ss_address = doc.openDataRegion("PO_诉讼地点");
	ss_address.setEditing(true);
	
	DataRegion zb_days = doc.openDataRegion("PO_质保期");
	zb_days.setEditing(true);
	DataRegion zbtk = doc.openDataRegion("PO_质保条款");
	zbtk.setEditing(true);
	DataRegion pact_num = doc.openDataRegion("PO_合同份数");
	//pact_num.setEditing(true);
	
	
	DataRegion pact_jia_person = doc.openDataRegion("PO_甲方负责人");
	pact_jia_person.setEditing(true);
	pact_jia_person.setValue(first_man);
	DataRegion pact_yi_person = doc.openDataRegion("PO_乙方负责人");
	pact_yi_person.setEditing(true);
	pact_yi_person.setValue(second_man);
	DataRegion pact_yi_phone = doc.openDataRegion("PO_乙方联系电话");
	pact_yi_phone.setEditing(true);
	pact_yi_phone.setValue(second_phone);

	
	
	
	// 设置PageOffice组件服务页面
	
	PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);
	poCtrl1.setServerPage(request.getContextPath()+"/poserver.zz"); //此行必须

	// 设置界面样式
	//poCtrl1.setCaption("购销合同");
	poCtrl1.setVisible(true); 
	poCtrl1.setBorderStyle(BorderStyleType.BorderThin);
	// 添加自定义工具条按钮
	
	poCtrl1.addCustomToolButton("保存", "poSave();", 1);
	poCtrl1.addCustomToolButton("全屏/还原", "poSetFullScreen();", 4);
	poCtrl1.addCustomToolButton("关闭", "closePage();", 10);
	//隐藏工具栏
	poCtrl1.setOfficeToolbars(false);
	poCtrl1.setMenubar(false);

	poCtrl1.setJsFunction_OnWordDataRegionClick("OnWordDataRegionClick()");
	
	//document.getElementById("PageOfficeCtrl1").JsFunction_BeforeDocumentSaved = "Js函数名";
	// 设置保存文档的服务器页面
	//String uulStr="SaveData.jsp?contract_code="+contract_code;
	//poCtrl1.setSaveDataPage("SaveData.jsp?contract_code="+contract_code);
	//System.out.println("SaveFile.jsp?contract_code="+contract_code+"&pd_count="+pd_count+"&is_file="+is_file);
	poCtrl1.setSaveFilePage("SaveFile.jsp?contract_code="+contract_code+"&pd_count="+pd_count+"&is_file="+is_file);
	
	////获取数据对象
	poCtrl1.setWriter(doc);
	// 打开文档	
	String sss=request.getSession().getServletContext().getRealPath("/")+file_path;
	String ppp=request.getSession().getServletContext().getRealPath("/")+contract_path;
	//System.out.println(sss);
	//System.out.println(ppp);
	if(contract_path == null || contract_path.equals("")){
		poCtrl1.webOpen(sss, OpenModeType.docRevisionOnly, user);
	}else{
		poCtrl1.webOpen(ppp, OpenModeType.docRevisionOnly, user);
	}
	
	poCtrl1.setTagId("PageOfficeCtrl1"); //此行必须
	//-----------  PageOffice 服务器端编程结束  -------------------//
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns:vh>
	<HEAD>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<link href="" rel="stylesheet" type="text/css">
		<script language='javascript' id='scriptID'></script>
		
		<script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>
		
		<title>购销合同</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
	<script language="JavaScript">
		function GetQueryString(name){ 
		     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); 
		     var r = window.location.search.substr(1).match(reg); 
		     if(r!=null)return  unescape(r[2]); 
		     return null; 
		}
		
		function saveDocument(){ 
		    return null; 
		}

	function OnWordDataRegionClick(Name, Value, Left, Bottom) {
		if (Name == "PO_签约日期") {
			var strRet = document.getElementById("PageOfficeCtrl1").ShowHtmlModalDialog("../datetimer2.htm", Value, "left=" + Left + "px;top=" + Bottom + "px;width=260px;height=260px;frame=no;");
			if (strRet != "") {
				var DataRegionList = document.getElementById("PageOfficeCtrl1").DataRegionList;//获取Word文件中的数据区域列表
				var DataRegionItem = DataRegionList.GetDataRegionByName("PO_签约日期_显示");//获取名称为“PO_name”的数据区域对象
				DataRegionItem.value=strRet;
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		if (Name == "PO_乙方") {
			var contract_code = GetQueryString('contract_code');
			var strRet = window.showModalDialog("../selectVencode.htm?contract_code="+contract_code, window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
			if (strRet != "") {
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		if (Name == "PO_试运行月数") {
			var contract_code = GetQueryString('contract_code');
			var strRet = window.showModalDialog("../selectTestMoths.htm?contract_code="+contract_code, window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
			if (strRet != "") {
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		if (Name == "PO_质保期") {
			var strRet = window.showModalDialog("../baozhiqi.htm", window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
			if (strRet != "") {
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		if (Name == "PO_合同份数") {
			var strRet = window.showModalDialog("../pact_number.htm", window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
			if (strRet != "") {
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		if (Name == "PO_乙方合同份数") {
			var strRet = window.showModalDialog("../pact_number.htm", window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
			if (strRet != "") {
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		if (Name == "PO_甲方合同份数") {
			var strRet = window.showModalDialog("../pact_number.htm", window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
			if (strRet != "") {
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		if (Name == "PO_非预付_首款比率") {
			var strRet = document.getElementById("PageOfficeCtrl1").ShowHtmlModalDialog("../shoukuanbili.htm", Value, "left=" + Left + "px;top=" + Bottom + "px;width=260px;height=260px;frame=no;");
			if (strRet != "") {
				var DataRegionList = document.getElementById("PageOfficeCtrl1").DataRegionList;//获取Word文件中的数据区域列表
				var DataRegionItem = DataRegionList.GetDataRegionByName("PO_非预付_质保金比率");//获取名称为“PO_name”的数据区域对象
				DataRegionItem.value=(100-strRet);
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		if (Name == "PO_非预付_质保金比率") {
			var strRet = document.getElementById("PageOfficeCtrl1").ShowHtmlModalDialog("../zhibaobili.htm", Value, "left=" + Left + "px;top=" + Bottom + "px;width=260px;height=260px;frame=no;");
			if (strRet != "") {
				var DataRegionList = document.getElementById("PageOfficeCtrl1").DataRegionList;//获取Word文件中的数据区域列表
				var DataRegionItem = DataRegionList.GetDataRegionByName("PO_非预付_首款比率");//获取名称为“PO_name”的数据区域对象
				DataRegionItem.value=(100-strRet);
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		
		if (Name == "PO_甲方负责人") {
			var contract_code = GetQueryString('../contract_code');
			var strRet = window.showModalDialog("../selectPerson.htm?contract_code="+contract_code, window, "help:no;status:no;resizable:yes;scroll:yes;dialogWidth=400px;dialogHeight=150px;");
			if (strRet != "") {
				return (strRet);
			}
			else {
				if ((Value == undefined) || (Value == ""))
					return " ";
				else
					return Value;
			}
		}
		
	}
	function poSave() {
		document.getElementById("PageOfficeCtrl1").WebSave();
	}
	//全屏
	function poSetFullScreen() {
		document.getElementById("PageOfficeCtrl1").FullScreen = !document.getElementById("PageOfficeCtrl1").FullScreen;
	}
	function closePage(){
		document.getElementById("PageOfficeCtrl1").Close();
		window.close();
	}
	function AfterDocumentOpened() {
		refreshList();
		refreshList_pz();
		poSave();
	}
	
	//获取当前痕迹列表
	function refreshList() {
		var i;
		document.getElementById("ul_Comments").innerHTML = "";
		for (i = 1; i <= document.getElementById("PageOfficeCtrl1").Document.Revisions.Count; i++) {
			var str = "";
			str = str + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Author;
			var  revisionDate=document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Date;
			 //转换为标准时间
			str=str+" "+dateFormat(revisionDate,"yyyy-MM-dd HH:mm:ss");
			if (document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Type == "1") {
				str = str + ' 插入：' + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Range.Text;
			}else if (document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Type == "2") {
				str = str + ' 删除：' + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Range.Text;
			}else {
				str = str + ' 调整格式或样式。';
			}
			document.getElementById("ul_Comments").innerHTML += "<li><a href='#' onclick='goToRevision(" + i + ")'>" + str + "</a></li>"
		}

	}
	 //GMT时间格式转换为CST
	dateFormat = function (date, format) {
		date = new Date(date); 
		var o = {
			'M+' : date.getMonth() + 1, //month
			'd+' : date.getDate(), //day
			'H+' : date.getHours(), //hour
			'm+' : date.getMinutes(), //minute
			's+' : date.getSeconds(), //second
			'q+' : Math.floor((date.getMonth() + 3) / 3), //quarter
			'S' : date.getMilliseconds() //millisecond
		};
		if (/(y+)/.test(format))
			format = format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));

		for (var k in o)
			if (new RegExp('(' + k + ')').test(format))
				format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));

		return format;
	}

	//定位到当前痕迹
	function goToRevision(index){
		var sMac = "Sub myfunc() " + "\r\n"+ "ActiveDocument.Revisions.Item("+index+").Range.Select " + "\r\n"+ "End Sub ";
		document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
	}
	//刷新列表
	function refresh_click(){
	   refreshList();    
	}

	function Button2_onclick() {
		refreshList_pz();
	}

	function InsertComment() {
		document.getElementById("PageOfficeCtrl1").WordInsertComment();
		var sMac = "Sub myfunc() " + "\r\n"
				 + "On Error Resume Next " + "\r\n"
				 + "ActiveWindow.ActivePane.Close " + "\r\n"
				 + "End Sub ";
		document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
	}
	
	function refreshList_pz() {
		var sMac = "Function getComments() " + "\r\n"
				 + "Dim cmts As String " + "\r\n"
				 + "For i=1 To ActiveDocument.Comments.Count "+ "\r\n"
				 + "    cmts = cmts +ActiveDocument.Comments.Item(i).Author & \":\" & ActiveDocument.Comments.Item(i).Range.Text + \"||\" " + "\r\n"
				 + "Next" + "\r\n"
				 + "getComments = cmts" + "\r\n"
				 + "End Function ";

		var sComments = document.getElementById("PageOfficeCtrl1").RunMacro("getComments", sMac);

		var arr = sComments.split("||");

		document.getElementById("ul_Comments2").innerHTML = "";
		for (var i = 0; i < arr.length-1 ; i++) {
			document.getElementById("ul_Comments2").innerHTML += "<li><a href='#' onclick='goToComment("+(i+1)+")'>"+arr[i]+"</a></li>"
		}
	}
	
	function getComment(index){
		var sMac = "Function getCmtTxt() " + "\r\n"+ "getCmtTxt = ActiveDocument.Comments.Item(" + index + ").Range.Text " + "\r\n"+ "End Function ";
		return document.getElementById("PageOfficeCtrl1").RunMacro("getCmtTxt", sMac);
	}
	function goToComment(index){
		var sMac = "Sub myfunc() " + "\r\n"+ "ActiveDocument.Comments.Item("+index+").Edit " + "\r\n"+ "End Sub ";
		document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
	}
</script>
		<div  style=" width:100%; height:700px;">
			<div id="Div_Comments" style="float:left; width:12%; height:700px; border:solid 1px red;">
				<h3>痕迹列表</h3>
				<input type="button" name="refresh" value="刷新"onclick=" return refresh_click()"/>
				<ul id="ul_Comments">
					
				</ul>
			</div>
			<div style="float:left;width:76%; height:700px;">
				<table>
					<tr>
						<td>
							<button class="pageBtn" accessKey="C" onClick="poSave()">保存</button>
							<button class="pageBtn" accessKey="C" onClick="closePage();">关闭</button>
						</td>
					</tr>					
				</table>
				<div>
					<po:PageOfficeCtrl id="PageOfficeCtrl1" >
					</po:PageOfficeCtrl>
				</div>
			</div>
			<div id="Div_Comments2" style="float:left;width:10%; height:700px; border:solid 1px red;">
				<h3>批注列表</h3>
				<input id="Button2" type="button" value="刷新" onclick="return Button2_onclick()" />
				<ul id="ul_Comments2">
				
				</ul>
			</div>			
		</div>
		
	</body>
	
</HTML>
