<%@ page contentType="text/html; charset=gb2312" language="java" import="java.util.*,java.sql.*,java.math.BigDecimal,com.viewhigh.core.web.OperateHelper,com.viewhigh.core.dao.*" errorPage="" %>
<%@ page import="com.viewhigh.base.sql.*,com.zhuozhengsoft.pageoffice.*,com.zhuozhengsoft.pageoffice.wordwriter.*,java.awt.*,javax.servlet.*,javax.servlet.http.*,java.sql.*,java.text.SimpleDateFormat,java.util.Date"%>

<%@ taglib uri="http://java.pageoffice.cn" prefix="po"%>
<%
	Connection conn = ConnectionPool.get(); 
	Statement stmt = conn.createStatement();
	
	
	String file_path = request.getParameter("file_path");
	String contract_code = request.getParameter("contract_code");
	String user = request.getParameter("user");
	String dcount1 =request.getParameter("dcount");
	String zpcount1 =request.getParameter("zpcount");
	int dcount =Integer.parseInt(dcount1);
	int zpcount2 =Integer.parseInt(zpcount1);
	
	String pathSQL="select contract_path from  pact_file_path where contract_code='"+contract_code+"'";
	ResultSet res1 = stmt.executeQuery(pathSQL);
	
	String contract_path="";
	if(res1.next()){
		contract_path = res1.getString("contract_path");		
	}
	res1.close();
	//System.out.println("contract_path"+contract_path);
	//System.out.println("file_path"+file_path);
	
	
	String ven_name ="";
	String first_man ="";
	String second_man ="";
	String second_phone ="";
	String bank_name ="";
	String bank_code ="";
	String contractName ="";
	String ven_lperson ="";
	String ven_lphone ="";
	String ven_address2 ="";
	String ven_postcode ="";
	int detail_count=0;	
	String pactStr="select sv.ven_name,second_man,second_phone,em.emp_name,detail_count,ma.bank_name,ma.bank_code,ma.contract_name,ma.ven_lperson,ma.ven_lphone,sv.ven_adress,sv.ven_postcode from pact_payment_cq_main ma left join sys_vendor_dict sv on ma.ven_code=sv.ven_code left join sys_emp em on ma.first_man=em.emp_code  where contract_code='"+contract_code+"'";
	ResultSet res2 = stmt.executeQuery(pactStr);
	if(res2.next()){
		ven_name = res2.getString("ven_name");		
		second_man = res2.getString("second_man");		
		second_phone = res2.getString("second_phone");	
		first_man = res2.getString("emp_name");				
		detail_count = res2.getInt("detail_count");				
		bank_name = res2.getString("bank_name");				
		bank_code = res2.getString("bank_code");				
		contractName = res2.getString("contract_name");				
		ven_lperson = res2.getString("ven_lperson");				
		ven_lphone = res2.getString("ven_lphone");				
		ven_address2 = res2.getString("ven_adress");				
		ven_postcode = res2.getString("ven_postcode");				
	}
	res2.close();
	String pactStr11="select dbo.getPactPayMoneyBiLi('"+contract_code+"','01') as b1,dbo.getPactPayMoneyBiLi('"+contract_code+"','02') as b2";
	ResultSet res11 = stmt.executeQuery(pactStr11);
	String bb1="";
	String bb2="";
	
	if(res11.next()){
		bb1 = res11.getString("b1");		
		bb2 = res11.getString("b2");		
	}
	res11.close();
	bb1+="%";
	bb2+="%";
	WordDocument doc = new WordDocument();
	
	DataRegion contract_code_old = doc.openDataRegion("PO_合同编号");
	contract_code_old.setValue(contract_code);
	contract_code_old.setEditing(true);
	
	DataRegion contract_name = doc.openDataRegion("PO_标底名称");
	contract_name.setValue(contractName);
	contract_name.setEditing(true);
	
	DataRegion pact_date = doc.openDataRegion("PO_签约日期_显示");
	DataRegion ven_code = doc.openDataRegion("PO_乙方");
	ven_code.setEditing(true);
	ven_code.setValue(ven_name);
	DataRegion ven_address = doc.openDataRegion("PO_乙方地址");
	ven_address.setEditing(true);
	ven_address.setValue(ven_address2);
	DataRegion ven_youbian = doc.openDataRegion("PO_乙方邮编");
	ven_youbian.setEditing(true);   
	ven_youbian.setValue(ven_postcode);
	
	DataRegion sign_date = doc.openDataRegion("PO_签约日期");
	sign_date.setEditing(true); 
	DataRegion pact_numery = doc.openDataRegion("PO_合同事宜");
	pact_numery.setEditing(true);
	DataRegion pact_detail = doc.openDataRegion("PO_合同明细");
	pact_detail.setEditing(false);
	DataRegion send_detail = doc.openDataRegion("PO_赠品明细");
	send_detail.setEditing(true);
	DataRegion pact_page_number = doc.openDataRegion("PO_配置清单附件页数");
	pact_page_number.setEditing(true);
	
	DataRegion jh_days = doc.openDataRegion("PO_交货天数");
	jh_days.setEditing(true); 
	DataRegion syx_months = doc.openDataRegion("PO_试运行月数");
	syx_months.setEditing(true);
	DataRegion one_number = doc.openDataRegion("PO_首付款比例");
	one_number.setEditing(false);
	one_number.setValue(bb1);
	DataRegion other_number = doc.openDataRegion("PO_尾款比例");
	other_number.setEditing(false);
	other_number.setValue(bb2);
	DataRegion second_bank = doc.openDataRegion("PO_乙方开户银行");
	second_bank.setEditing(true);
	second_bank.setValue(bank_name);
	DataRegion second_bank_num = doc.openDataRegion("PO_乙方银行账号");
	second_bank_num.setEditing(true);
	second_bank_num.setValue(bank_code);
	DataRegion second_pact_num = doc.openDataRegion("PO_乙方合同份数");
	DataRegion jsfwtk = doc.openDataRegion("PO_技术服务条款");
	jsfwtk.setEditing(true);
	DataRegion first_pact_num = doc.openDataRegion("PO_甲方合同份数");
	DataRegion ss_address = doc.openDataRegion("PO_诉讼地点");
	ss_address.setEditing(true);
	
	DataRegion zb_days = doc.openDataRegion("PO_质保期");
	zb_days.setEditing(true);
	DataRegion zbtk = doc.openDataRegion("PO_质保条款");
	zbtk.setEditing(true);
	DataRegion pact_num = doc.openDataRegion("PO_合同份数");
	
	DataRegion pact_jia_person = doc.openDataRegion("PO_甲方负责人");
	pact_jia_person.setEditing(true);
	pact_jia_person.setValue(first_man);
	DataRegion pact_yi_person = doc.openDataRegion("PO_乙方负责人");
	pact_yi_person.setEditing(true);
	pact_yi_person.setValue(second_man);
	DataRegion pact_yi_phone = doc.openDataRegion("PO_乙方联系电话");
	pact_yi_phone.setEditing(true);
	pact_yi_phone.setValue(second_phone);
	
	DataRegion ven_lperson2 = doc.openDataRegion("PO_乙方法人");
	ven_lperson2.setEditing(true);
	ven_lperson2.setValue(ven_lperson);
	DataRegion ven_lperson_phone = doc.openDataRegion("PO_乙方法人电话");
	ven_lperson_phone.setEditing(true);
	ven_lperson_phone.setValue(ven_lphone);
	
	
	//打开数据区域中的表格，OpenTable(index)方法中的index为word文档中表格的下标，从1开始
	com.zhuozhengsoft.pageoffice.wordwriter.Table table1 = doc.openDataRegion("PO_合同明细表").openTable(1);
	//设置表格边框样式
	table1.getBorder().setLineColor(Color.black);
	table1.getBorder().setLineWidth(WdLineWidth.wdLineWidth050pt);
	// 设置表头单元格文本居中
	table1.openCellRC(1, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 3).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 4).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 5).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 6).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 7).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	table1.openCellRC(1, 8).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
	
	String amountSQL="SELECT CONVERT(VARCHAR(50),CAST(CONVERT(DECIMAL(18,2),LTRIM(contract_money)) as money ),1) AS contract_money,dbo.fn_ChnMoney(contract_money) as contract_money_temp ,is_file,zp_count  FROM pact_payment_cq_main t where t.contract_code='"+contract_code+"'";
	ResultSet rs = stmt.executeQuery(amountSQL);
	String contract_money="" ;
	String contract_money_temp="";
	String is_file="";
	int zp_count=0;
	if(rs.next()){
		contract_money = rs.getString("contract_money");
		contract_money_temp = rs.getString("contract_money_temp");
		is_file = rs.getString("is_file");
		zp_count=rs.getInt("zp_count");	
	}
	rs.close();
	String contract_money_Big = "";
	String detailSQL="select case isnull(item_name,'') when '' then brief else dbo.getPactDetailName('01010100001 wwww') end item_name,t.pinpai,item_model,t.made_nation,contract_amount,t.unit,contract_price,contract_money from  pact_payment_cq_detail t  where t.contract_code='"+contract_code+"'";
	ResultSet res = stmt.executeQuery(detailSQL);
	detail_count+=2; 
	int n=dcount+2;
	////System.out.println("条数加2="+n);
	int pd_count=0;
	int idx=1;
	String item_name;
	String item_spec;
	String item_model;
	String nation_name;
	String contract_amount;
	String unit;
	String contract_price;
	String contract_money1;
	System.out.println("detail_count:"+detail_count);	
	System.out.println("n:"+n);
	if(n<detail_count){
		System.out.println("111111:");	
		for(int i=detail_count;i>=n;i--){
			Cell  cell=table1.openCellRC(i,1);
			table1.removeRowAt(cell);
		}
		while(res.next()){
			idx++;		
			pd_count++;
			table1.openCellRC(idx, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
			table1.openCellRC(idx, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
			table1.openCellRC(idx, 3).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
			table1.openCellRC(idx, 4).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
			table1.openCellRC(idx, 5).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table1.openCellRC(idx, 6).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table1.openCellRC(idx, 7).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table1.openCellRC(idx, 8).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			
			table1.openCellRC(idx, 1).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 2).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 3).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 4).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 5).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 6).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 7).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 8).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);		
			
			item_name = res.getString(1)!=null?res.getString(1):"";
			item_spec = res.getString(2)!=null?res.getString(2):"";
			item_model = res.getString(3)!=null?res.getString(3):"";
			nation_name = res.getString(4)!=null?res.getString(4):"";
			contract_amount = res.getString(5)!=null?res.getString(5):"";
			unit = res.getString(6)!=null?res.getString(6):"";
			contract_price = res.getString(7)!=null?res.getString(7):"";
			contract_money1 = res.getString(8)!=null?res.getString(8):"";
			
			table1.openCellRC(idx, 1).setValue(res.getString(1).trim());
			table1.openCellRC(idx, 2).setValue(item_spec.trim());
			table1.openCellRC(idx, 3).setValue(item_model.trim());
			table1.openCellRC(idx, 4).setValue(nation_name.trim());
			table1.openCellRC(idx, 5).setValue(contract_amount.trim());
			table1.openCellRC(idx, 6).setValue(unit.trim());
			table1.openCellRC(idx, 7).setValue(contract_price.trim());
			table1.openCellRC(idx, 8).setValue(contract_money1.trim());
		}	
	}else{
		System.out.println("22222:");	
		while(res.next()){
			if(idx>=detail_count-1) 
				table1.insertRowAfter(table1.openCellRC(idx, 8));
			
			idx++;		
			pd_count++;
			table1.openCellRC(idx, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
			table1.openCellRC(idx, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
			table1.openCellRC(idx, 3).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
			table1.openCellRC(idx, 4).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
			table1.openCellRC(idx, 5).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table1.openCellRC(idx, 6).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table1.openCellRC(idx, 7).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table1.openCellRC(idx, 8).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			
			table1.openCellRC(idx, 1).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 2).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 3).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 4).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 5).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 6).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 7).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table1.openCellRC(idx, 8).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);		
			
			item_name = res.getString(1)!=null?res.getString(1):"";
			item_spec = res.getString(2)!=null?res.getString(2):"";
			item_model = res.getString(3)!=null?res.getString(3):"";
			nation_name = res.getString(4)!=null?res.getString(4):"";
			contract_amount = res.getString(5)!=null?res.getString(5):"";
			unit = res.getString(6)!=null?res.getString(6):"";
			contract_price = res.getString(7)!=null?res.getString(7):"";
			contract_money1 = res.getString(8)!=null?res.getString(8):"";
			
			table1.openCellRC(idx, 1).setValue(res.getString(1).trim());
			table1.openCellRC(idx, 2).setValue(item_spec.trim());
			table1.openCellRC(idx, 3).setValue(item_model.trim());
			table1.openCellRC(idx, 4).setValue(nation_name.trim());
			table1.openCellRC(idx, 5).setValue(contract_amount.trim());
			table1.openCellRC(idx, 6).setValue(unit.trim());
			table1.openCellRC(idx, 7).setValue(contract_price.trim());
			table1.openCellRC(idx, 8).setValue(contract_money1.trim());
		}		
	}
	
	if(idx>1){
		table1.insertRowAfter(table1.openCellRC(idx, 8));
		table1.openCellRC(idx+1,2).mergeTo(idx+1,8);
		table1.openCellRC(idx+1, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
		table1.openCellRC(idx+1, 2).getFont().setBold(true);
		table1.openCellRC(idx+1, 2).setValue("总金额：￥"+contract_money+"元　大写：人民币"+contract_money_temp);
	}
	Cell  cell=table1.openCellRC(idx+2,1);
	table1.removeRowAt(cell);
	res.close();
	
	
	String zpSQL="select equi_name,pinpai,equi_space,chandi,amount,unit,price,amount_money from pact_zengpin_cq_detail t  where t.contract_code='"+contract_code+"' order by t.id";
	ResultSet res_zp = stmt.executeQuery(zpSQL);
	
	Table table2 = doc.openDataRegion("PO_赠品明细表").openTable(1);
	//设置表格边框样式
	table2.getBorder().setLineColor(Color.black);
	table2.getBorder().setLineWidth(WdLineWidth.wdLineWidth050pt);
	// 设置表头单元格文本居中
	int idx2=1;
	zp_count+=2; 
	int n2=zpcount2+2;
	int zpCount=0;
	System.out.println("zp_count:"+zp_count);	
	System.out.println("n2:"+n2);	
	
	if(n2<zp_count){
		System.out.println("33333:");		
		for(int i=zp_count;i>n2;i--){
			Cell  cell2=table2.openCellRC(i,1);
			table2.removeRowAt(cell2);
		}
		while(res_zp.next()){
			idx2++;
			zpCount++;			
			/*table2.openCellRC(idx2, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 3).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 4).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 5).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 6).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 7).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 8).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			*/
			table2.openCellRC(idx2, 1).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 2).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 3).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 4).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 5).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 6).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 7).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 8).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);		
			
			item_name = res_zp.getString(1)!=null?res_zp.getString(1):"";
			item_spec = res_zp.getString(2)!=null?res_zp.getString(2):"";
			item_model = res_zp.getString(3)!=null?res_zp.getString(3):"";
			nation_name = res_zp.getString(4)!=null?res_zp.getString(4):"";
			contract_amount = res_zp.getString(5)!=null?res_zp.getString(5):"";
			unit = res_zp.getString(6)!=null?res_zp.getString(6):"";
			contract_price = res_zp.getString(7)!=null?res_zp.getString(7):"";
			contract_money1 = res_zp.getString(8)!=null?res_zp.getString(8):"";
			
			table2.openCellRC(idx2, 1).setValue(item_name.trim());
			table2.openCellRC(idx2, 2).setValue(item_spec.trim());
			table2.openCellRC(idx2, 3).setValue(item_model.trim());
			table2.openCellRC(idx2, 4).setValue(nation_name.trim());
			table2.openCellRC(idx2, 5).setValue(contract_amount.trim());
			table2.openCellRC(idx2, 6).setValue(unit.trim());
			table2.openCellRC(idx2, 7).setValue(contract_price.trim());
			table2.openCellRC(idx2, 8).setValue(contract_money1.trim());
		}
		
	}else{
		System.out.println("44444:");	
		while(res_zp.next()){
			if(idx2>=zp_count-1) 
				table2.insertRowAfter(table2.openCellRC(idx2, 8));
				//table1.insertRowAfter(table1.openCellRC(idx, 8));
			idx2++;	
			zpCount++;			
			table2.openCellRC(idx2, 1).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 3).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 4).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 5).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 6).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 7).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			table2.openCellRC(idx2, 8).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphCenter);
			
			table2.openCellRC(idx2, 1).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 2).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 3).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 4).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 5).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 6).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 7).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);
			table2.openCellRC(idx2, 8).setVerticalAlignment(WdCellVerticalAlignment.wdCellAlignVerticalCenter);		
			
			item_name = res_zp.getString(1)!=null?res_zp.getString(1):"";
			item_spec = res_zp.getString(2)!=null?res_zp.getString(2):"";
			item_model = res_zp.getString(3)!=null?res_zp.getString(3):"";
			nation_name = res_zp.getString(4)!=null?res_zp.getString(4):"";
			contract_amount = res_zp.getString(5)!=null?res_zp.getString(5):"";
			unit = res_zp.getString(6)!=null?res_zp.getString(6):"";
			contract_price = res_zp.getString(7)!=null?res_zp.getString(7):"";
			contract_money1 = res_zp.getString(8)!=null?res_zp.getString(8):"";
			
			table2.openCellRC(idx2, 1).setValue(item_name.trim());
			table2.openCellRC(idx2, 2).setValue(item_spec.trim());
			table2.openCellRC(idx2, 3).setValue(item_model.trim());
			table2.openCellRC(idx2, 4).setValue(nation_name.trim());
			table2.openCellRC(idx2, 5).setValue(contract_amount.trim());
			table2.openCellRC(idx2, 6).setValue(unit.trim());
			table2.openCellRC(idx2, 7).setValue(contract_price.trim());
			table2.openCellRC(idx2, 8).setValue(contract_money1.trim());
		}
	}
	
	res_zp.close();
	
	String zpSQL_money="select SUM(t.amount_money) zp_money,dbo.fn_ChnMoney(SUM(t.amount_money)) zp_money_big  from pact_zengpin_cq_detail t  where t.contract_code='"+contract_code+"'";
	ResultSet res_zp_money = stmt.executeQuery(zpSQL_money);
	String zp_money="";
	String zp_money_big="";
	while(res_zp_money.next()){
		zp_money = res_zp_money.getString("zp_money");
		zp_money_big = res_zp_money.getString("zp_money_big");
	}
	
	
	if(idx2>1){
		System.out.println("idx2:"+idx2);	
		//table2.insertRowAfter(table2.openCellRC(idx2, 8));
		table2.openCellRC(idx2+1,2).mergeTo(idx2+1,8);
		table2.openCellRC(idx2+1, 2).getParagraphFormat().setAlignment(WdParagraphAlignment.wdAlignParagraphLeft);
		table2.openCellRC(idx2+1, 2).getFont().setBold(true);
		table2.openCellRC(idx2+1, 1).setValue("");
		table2.openCellRC(idx2+1, 2).setValue("总金额：￥"+zp_money+"元　大写：人民币"+zp_money_big);
	}
	res_zp_money.close();
	stmt.close();
	conn.close();
	
	
	// 设置PageOffice组件服务页面
	
	PageOfficeCtrl poCtrl1 = new PageOfficeCtrl(request);
	poCtrl1.setServerPage(request.getContextPath()+"/poserver.zz"); //此行必须

	// 设置界面样式
	//poCtrl1.setCaption("购销合同");
	poCtrl1.setVisible(true); 
	poCtrl1.setBorderStyle(BorderStyleType.BorderThin);
	// 添加自定义工具条按钮
	
	//poCtrl1.addCustomToolButton("保存", "poSave();", 1);
	//poCtrl1.addCustomToolButton("全屏/还原", "poSetFullScreen();", 4);
	//poCtrl1.addCustomToolButton("关闭", "closePage();", 10);
	//隐藏工具栏
	
	//poCtrl1.setTitlebar(false); //隐藏标题栏
	//poCtrl1.setMenubar(false); //隐藏菜单栏
	//poCtrl1.setOfficeToolbars(false);//隐藏Office工具条
	poCtrl1.setCustomToolbar(false);//隐藏自定义工具栏
	
	
	//poCtrl1.setJsFunction_OnWordDataRegionClick("OnWordDataRegionClick()");
	
	////获取数据对象
	poCtrl1.setWriter(doc);
	// 打开文档	
	String sss="";
	if(contract_path == null || contract_path.equals("")){
		sss=request.getSession().getServletContext().getRealPath("/")+file_path;
		String[] strs=file_path.split("\\.");
		String hz_str=strs[1].toString();
		if(strs.length==2){
			contract_path="pactWord/"+contract_code+"."+hz_str;			
		}else{
			contract_path="pactWord/"+contract_code+".docx";
		}		
	}else{
		sss=request.getSession().getServletContext().getRealPath("/")+contract_path;
	}
	// 设置保存文档的服务器页面
	poCtrl1.setSaveFilePage("SaveFile.jsp?contract_code="+contract_code+"&pd_count="+pd_count+"&is_file="+is_file+"&contract_path="+contract_path+"&zpCount="+zpCount);
	poCtrl1.webOpen(sss, OpenModeType.docRevisionOnly, user);
	
	
	poCtrl1.setTagId("PageOfficeCtrl1"); //此行必须
	//-----------  PageOffice 服务器端编程结束  -------------------//
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns:vh>
	<HEAD>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<link href="" rel="stylesheet" type="text/css">
		<script language='javascript' id='scriptID'></script>
		
		<script language='javascript'>typeof(window.dialogArguments)=="undefined"?parent.pageInit(window):window.dialogArguments.pageInit(window)</script>
		
		<title>购销合同</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
	<script language="JavaScript">
		function GetQueryString(name){ 
		     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); 
		     var r = window.location.search.substr(1).match(reg); 
		     if(r!=null)return  unescape(r[2]); 
		     return null; 
		}
		
		function saveDocument(){ 
		    return null; 
		}

	
	function poSave() {
		document.getElementById("PageOfficeCtrl1").WebSave();
	}
	//全屏
	function poSetFullScreen() {
		document.getElementById("PageOfficeCtrl1").FullScreen = !document.getElementById("PageOfficeCtrl1").FullScreen;
	}
	function closePage(){
		document.getElementById("PageOfficeCtrl1").Close();
		window.close();
	}
	function AfterDocumentOpened() {
		document.getElementById("PageOfficeCtrl1").ShowRevisions=false;
		refreshList();
		refreshList_pz();
		poSave();
	}
	
	//获取当前痕迹列表
	function refreshList() {
		var i;
		document.getElementById("ul_Comments").innerHTML = "";
		for (i = 1; i <= document.getElementById("PageOfficeCtrl1").Document.Revisions.Count; i++) {
			var str = "";
			str = str + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Author;
			var  revisionDate=document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Date;
			 //转换为标准时间
			str=str+" "+dateFormat(revisionDate,"yyyy-MM-dd HH:mm:ss");
			if (document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Type == "1") {
				str = str + ' 插入：' + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Range.Text;
			}else if (document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Type == "2") {
				str = str + ' 删除：' + document.getElementById("PageOfficeCtrl1").Document.Revisions.Item(i).Range.Text;
			}else {
				str = str + ' 调整格式或样式。';
			}
			document.getElementById("ul_Comments").innerHTML += "<li><a href='#' onclick='goToRevision(" + i + ")'>" + str + "</a></li>"
		}

	}
	 //GMT时间格式转换为CST
	dateFormat = function (date, format) {
		date = new Date(date); 
		var o = {
			'M+' : date.getMonth() + 1, //month
			'd+' : date.getDate(), //day
			'H+' : date.getHours(), //hour
			'm+' : date.getMinutes(), //minute
			's+' : date.getSeconds(), //second
			'q+' : Math.floor((date.getMonth() + 3) / 3), //quarter
			'S' : date.getMilliseconds() //millisecond
		};
		if (/(y+)/.test(format))
			format = format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));

		for (var k in o)
			if (new RegExp('(' + k + ')').test(format))
				format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));

		return format;
	}

	//定位到当前痕迹
	function goToRevision(index){
		var sMac = "Sub myfunc() " + "\r\n"+ "ActiveDocument.Revisions.Item("+index+").Range.Select " + "\r\n"+ "End Sub ";
		document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
	}
	//刷新列表
	function refresh_click(){
	   refreshList();    
	}

	function Button2_onclick() {
		refreshList_pz();
	}

	function InsertComment() {
		document.getElementById("PageOfficeCtrl1").WordInsertComment();
		var sMac = "Sub myfunc() " + "\r\n"
				 + "On Error Resume Next " + "\r\n"
				 + "ActiveWindow.ActivePane.Close " + "\r\n"
				 + "End Sub ";
		document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
	}
	
	function refreshList_pz() {
		var sMac = "Function getComments() " + "\r\n"
				 + "Dim cmts As String " + "\r\n"
				 + "For i=1 To ActiveDocument.Comments.Count "+ "\r\n"
				 + "    cmts = cmts +ActiveDocument.Comments.Item(i).Author & \":\" & ActiveDocument.Comments.Item(i).Range.Text + \"||\" " + "\r\n"
				 + "Next" + "\r\n"
				 + "getComments = cmts" + "\r\n"
				 + "End Function ";

		var sComments = document.getElementById("PageOfficeCtrl1").RunMacro("getComments", sMac);

		var arr = sComments.split("||");

		document.getElementById("ul_Comments2").innerHTML = "";
		for (var i = 0; i < arr.length-1 ; i++) {
			document.getElementById("ul_Comments2").innerHTML += "<li><a href='#' onclick='goToComment("+(i+1)+")'>"+arr[i]+"</a></li>"
		}
	}
	
	function getComment(index){
		var sMac = "Function getCmtTxt() " + "\r\n"+ "getCmtTxt = ActiveDocument.Comments.Item(" + index + ").Range.Text " + "\r\n"+ "End Function ";
		return document.getElementById("PageOfficeCtrl1").RunMacro("getCmtTxt", sMac);
	}
	function goToComment(index){
		var sMac = "Sub myfunc() " + "\r\n"+ "ActiveDocument.Comments.Item("+index+").Edit " + "\r\n"+ "End Sub ";
		document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", sMac);
	}
	function xianshi(){
		document.getElementById("PageOfficeCtrl1").ShowRevisions = true;
	}
	function yincang(){
		document.getElementById("PageOfficeCtrl1").ShowRevisions = false;
	}
</script>
		<div  style=" width:100%; height:700px;">
			<div id="Div_Comments" style="float:left; width:12%; height:700px; border:solid 1px red;">
				<h3>痕迹列表</h3>
				<input type="button" name="refresh" value="刷新"onclick=" return refresh_click()"/>
				<ul id="ul_Comments">
					
				</ul>
			</div>
			<div style="float:left;width:76%; height:700px;">
				<table>
					<tr>
						<td>
							<button class="pageBtn" accessKey="C" onClick="xianshi()">显示痕迹</button>
							<button class="pageBtn" accessKey="C" onClick="yincang()">隐藏痕迹</button>
							<button class="pageBtn" accessKey="C" onClick="poSave()">保存</button>
							<button class="pageBtn" accessKey="C" onClick="closePage();">关闭</button>
						</td>
					</tr>					
				</table>
				<div>
					<po:PageOfficeCtrl id="PageOfficeCtrl1" >
					</po:PageOfficeCtrl>
				</div>
			</div>
			<div id="Div_Comments2" style="float:left;width:10%; height:700px; border:solid 1px red;">
				<h3>批注列表</h3>
				<input id="Button2" type="button" value="刷新" onclick="return Button2_onclick()" />
				<ul id="ul_Comments2">
				
				</ul>
			</div>			
		</div>
		
	</body>
	
</HTML>
