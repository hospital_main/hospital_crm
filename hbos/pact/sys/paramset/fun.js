function submitData(obj){
	
	var res=checkInputs();
	if(res==null||res==""||res.indexOf("<record></record>")!=-1)
		return false;
		window.xmlhttp.post(obj.name, res,"");
		var str = window.xmlhttp._object.responseText
		if (!window.doMsg(str,"")) {
		  return false;
		}
		return true;
	//sysDictsUnitinfoCopyParas_select.click();
}
function checkInputs(){

	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	var res="";
	for(var i=0;i<trs.length;i++){
		if(trs[i].getAttribute("_editParaType")==null)
			continue;
		
		
	 if(trs[i]._editDataValue==trs[i]._editInputValue)
		 continue;

		res+="<record>";
	  if(checkValue(trs[i],trs[i]._editDataType,trs[i]._editInputValue)){
		  res+=trs[i]._editPk+"<value>"+trs[i]._editInputValue+"</value>";
		}
    	
	  
		res+="</record>";
	}
	
	return res;
}
function checkValue(tr,type,value){
	var msg=tr.cells[0].innerText+" "+tr.cells[1].innerText+" :要求是" ;
	var res=true;
	if(type=="1"&&value!=""){
		msg+=" 文本 ";
		res=true;
	}else if(type=="2"&&IsDate(value)){
		msg+=" 日期 ";
		var d= CDate(value);
		var year=d.getFullYear();
		var month=d.getMonth()+1;
		var day=d.getDate();
		year='0000'+year;year=year.substring(year.length-4)
	  	month='00'+month;month=month.substring(month.length-2)
	  	day='00'+day; day=day.substring(day.length-2)
	 	tr._editInputValue=year+'-'+month+'-'+day;
		res=true;
	}else  if(type=="3"){
		msg+=" 编码规则 ";
		res=isRuleCode(value);
	}else if(type=="4"){
		msg+=" 正整数 ";
		var v=parseInt(value,10);
		if(isNaN(v)||v<0)
			res=false;
	}else if(type=="5"){
		var v=parseInt(value,10);
		if(isNaN(v))
			res=false;
	}
	if(res==false)
		alert(msg);
	return res;
}
function initInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	for(var i=0;i<trs.length;i++){
		initTrInputs(trs[i]);
	}
}
function initTrInputs(tr){
	var pt=tr.getAttribute("_editParaType");
	var dt=tr.getAttribute("_editDataType");
	var dv=tr.getAttribute("_editDataValue");
	var pc=tr.getAttribute("_paraCode");
	var pv=tr.getAttribute("_paraValue");
	
	if(pt==null||dt==null)
		return ;
	var inp="";
	var id=getJsGuid();
	var tag="";
	var chk="";
	tr._editInputId=id;
	if(pt=="1"){
		var tab = '';
		if(pc=='2501'){
			tab = 'pact_payment_item';
		}else if(pc=='2502'){
			tab = 'pact_receive_item';
		}
		if(tab != ''){
			var cou = getValuePairBySql("pactSysParamset_checkItem","<table>"+tab + "</table>");
			if(cou[0] > 0){
				inp +="<input type='text' readonly=true style='background-color:#EEEEEE;border:1px solid #808080;width:110px' id='"+id+"' name='"+id+"' value='"+dv+"'/>";
				
			}else{
				inp +="<input type='text' id='"+id+"' name='"+id+"' value='"+dv+"'/>";
			}			
		}else{			
				inp +="<input type='text' id='"+id+"' name='"+id+"' value='"+dv+"'/>";
		}		
		tag="input";
		
	}else if(pt=="3"){
		var ops=tr.cells[3].innerText.split("\/");
		inp="<select id='"+id+"' name='"+id+"'>";
	  
	  
		tag="select"
		for(var i=0;i<ops.length;i++){
			if(dv==ops[i])
				chk=" selected ";
			else
				chk="";

   		inp+="<option "+chk+" value='"+ops[i]+"'>"+ops[i]+"</option>";
		}
		inp+="</select>";
	}else{
		return ;
	}
	tr.cells[2].innerHTML=inp;
	var inpus=tr.cells[2].getElementsByTagName("input");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	inpus=tr.cells[2].getElementsByTagName("select");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	
}
function setInputChange(tr,inp){
	inp.onblur=
	inp.onclick=
	inp.onkeyup=function(){tr._editInputValue=this.value};
}