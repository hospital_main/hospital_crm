<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
  	<thead>
  		<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
		  <tr noWrap='true' class='mainHead'>
        <td nowrap='true'>协议编号</td>
        <td nowrap='true'>协议名称</td>
		  	<td nowrap='true'>签订日期</td>
		  	<td nowrap='true'>供应商</td>
		  	<td nowrap='true'>仓库</td>
		  	<td nowrap='true'>材料编码</td>
		  	<td nowrap='true'>材料名称</td>
		  	<td nowrap='true'>入库日期</td>
		  	<td nowrap='true'>入库单号</td>
		  	<td nowrap='true'>物资类别</td>
		  	<td nowrap='true'>规格</td>
		  	<td nowrap='true'>到货数量</td>		  	
		  	<td nowrap='true'>单价</td>  
		  	<td nowrap='true'>入库金额</td>
		  	<td nowrap='true'>优惠金额</td>		  			
		  	<td nowrap='true'>开票金额</td>
		  	<td nowrap='true'>未开票金额</td>
		  	<td nowrap='true'>付款金额</td>
		  	<td nowrap='true'>未付金额</td>
      </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							 <xsl:when test="position()=12 or position()=13 or position()=14 or position()=15 or position()=16 or position()=17 or position()=18 or position()=19">
								<td align='right'>
								<xsl:value-of select="format-number(.,'###,###0.00')" />
								 </td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		 
 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>