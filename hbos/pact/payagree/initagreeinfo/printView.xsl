<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
				<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/> 				
			</tr>
			<tr noWrap='true' class='mainHead'>
				<td nowrap='true'>协议编号</td>
				<td nowrap='true'>协议名称</td>
				<td nowrap='true'>签订日期</td>
				<td nowrap='true'>供应商</td>
				<td nowrap='true'>组织形式</td>
				<td nowrap='true'>采购方式</td>
				<td nowrap='true'>是否招标</td>
				<td nowrap='true'>招标信息</td>
				<td nowrap='true'>签订科室</td>
				<td nowrap='true'>开始日期</td>
				<td nowrap='true'>截止日期</td>
				<td nowrap='true'>文档</td>
				<td nowrap='true'>状态</td>
				<td nowrap='true'>结转</td>				
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<td align='left'><xsl:value-of select="."/></td>
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
