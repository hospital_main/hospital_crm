<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th nowrap='true'>协议编号</th>
        <th nowrap='true'>协议名称</th>
		  	<th nowrap='true'>签订日期</th>
		  	<th nowrap='true'>供应商</th>
		  	<th nowrap='true'>签订科室</th>
		  	<th nowrap='true'>到货数量</th>
		  	<th nowrap='true'>到货金额</th>
		  	<th nowrap='true'>优惠金额</th>
		  	<th nowrap='true'>开票金额</th>
		  	<th nowrap='true'>未开票金额</th>
		  	<th nowrap='true'>付款金额</th>
		  	<th nowrap='true'>未付金额</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td align='left'>
									<a tabindex='-1'>
						                  <xsl:attribute name="href" >
						    	            javascript:openDialog('../agreeinfo/update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:1000px;dialogHeight:570px', result)
						  	          </xsl:attribute><xsl:value-of select="."/></a>
								 </td>
              </xsl:when>
              <xsl:when test="position()=6 or position()=7 or position()=8 or position()=9 or position()=10 or position()=11 or position()=12">
								<td align='right'>
								<xsl:value-of select="format-number(.,'###,###0.00')" />
								 </td>
              </xsl:when>
              <xsl:otherwise>
								<td>
		                 <xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

