<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
    <thead>
    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="5"/>    
	        </xsl:call-template>
	  		</tr>
      <tr noWrap='true' class='mainHead'>
        <td nowrap='true'>日期</td>
        <td nowrap='true'>单据编号</td>
		  	<td nowrap='true'>摘要</td>
		  	<td nowrap='true'>借方</td>
		  	<td nowrap='true'>贷方</td>
		  	<td nowrap='true'>余额</td>
		  	<td style="display:none"></td>
      </tr>
    </thead>
    <tbody>
    	<xsl:for-each select="/root/tbody/tr">
        <xsl:if test="td[3]!='本月合计' and  td[3]!='本年累计'">
        	<xsl:if test="td[3]='期初余额'">
        		<tr>
			  			<xsl:for-each select="td">
			   			<xsl:choose>
			   				<xsl:when test="position()=3">
					       	<td>
					          <xsl:value-of select="."/>
					       	</td>
			      		</xsl:when>	      		
				      	<xsl:when test="position()=6">
					        <td align='right'>
					          <xsl:value-of select="format-number(.,'#,##0.00')"/>
					        </td>
				      	</xsl:when>		    
				      	<xsl:when test="position()=7">
					        <td style='display:none'>
					          <xsl:value-of select="."/>
					        </td>
				      	</xsl:when>  
				      	<xsl:otherwise>
				        	<td align='left'>
				        	</td>
				      	</xsl:otherwise>			                        
			    		</xsl:choose>
			  			</xsl:for-each>
						</tr>
						</xsl:if>	
						<xsl:if test="td[3]!='期初余额'">
	      	<tr>
		  			<xsl:for-each select="td">
		   			<xsl:choose>
			      	<xsl:when test="position()=4 or position()=5 or position()=6">
				        <td align='right'>
				          <xsl:value-of select="format-number(.,'#,##0.00')"/>
				        </td>
			      	</xsl:when>
			      	<xsl:when test="position()=7">
				        <td style='display:none'>
				          <xsl:value-of select="."/>
				        </td>
			      	</xsl:when>  
			      	<xsl:otherwise>
			        	<td align='left'>
			          <xsl:value-of select="."/>
			        	</td>
			      	</xsl:otherwise>			                        
		    		</xsl:choose>
		  			</xsl:for-each>
					</tr>
					</xsl:if>	
					</xsl:if>
        <xsl:if test="td[3]='本月合计' or  td[3]='本年累计'">
      	<tr>
	  			<xsl:for-each select="td">
	   			<xsl:choose>
	   				<xsl:when test="position()=1">
			       	<td>
			          
			       	</td>
	      		</xsl:when>
	      		
		      	<xsl:when test="position()=4 or position()=5 or position()=6">
			        <td align='right'>
			          <xsl:value-of select="format-number(.,'#,##0.00')"/>
			        </td>
		      	</xsl:when>		    
		      	<xsl:when test="position()=7">
			        <td style='display:none'>
			          <xsl:value-of select="."/>
			        </td>
		      	</xsl:when>  
		      	<xsl:otherwise>
		        	<td align='left'>
		          <xsl:value-of select="."/>
		        	</td>
		      	</xsl:otherwise>			                        
	    		</xsl:choose>
	  			</xsl:for-each>
				</tr>
				</xsl:if>
      		
			</xsl:for-each>
   	       
    </tbody>
    </root>
  </xsl:template>
  <xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>

