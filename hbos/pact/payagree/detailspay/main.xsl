<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>日期</th>
        <th nowrap='true'>单据编号</th>
		  	<th nowrap='true'>摘要</th>
		  	<th nowrap='true'>借方</th>
		  	<th nowrap='true'>贷方</th>
		  	<th nowrap='true'>余额</th>
		  	<th style='display:none'></th>
      </tr>
    </thead>
    <tbody>
    	<xsl:for-each select="/root/tbody/tr">
        <xsl:if test="td[3]!='本月合计' and  td[3]!='本年累计'">
      	<tr>
	  			<xsl:for-each select="td">
	   			<xsl:choose>
	      		<xsl:when test="position()=2">
	    			<td>
	          	<a href="#">
	          	<xsl:attribute name="onclick" >
	          		<xsl:if test="../td[3]='采购发票'">
									javascript:openDialog('../../../mate/pay/invoice/update_detail.html?load=&lt;id&gt;<xsl:value-of select="../td[last()]"/>&lt;/id&gt;','dialogWidth:850px;dialogHeight:650px',result)
								</xsl:if>
								<xsl:if test="../td[3]='付款单'">
									javascript:openDialog('../../../mate/pay/pay/pay/update_detail.html?load=&lt;pay_bill_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/pay_bill_id&gt;','dialogWidth:850px;dialogHeight:650px',result)
								</xsl:if>
	          	 	</xsl:attribute>
	          	<xsl:value-of select="."/>
	          	</a>
	        		</td>
	      		</xsl:when>
		      	<xsl:when test="position()=4 or position()=5 or position()=6">
			        <td align='right'>
			          <xsl:value-of select="format-number(.,'#,##0.00')"/>
			        </td>
		      	</xsl:when>
		      	<xsl:when test="position()=7">
			        <td style='display:none'>
			          <xsl:value-of select="."/>
			        </td>
		      	</xsl:when>  
		      	<xsl:otherwise>
		        	<td align='left'>
		          <xsl:value-of select="."/>
		        	</td>
		      	</xsl:otherwise>			                        
	    		</xsl:choose>
	  			</xsl:for-each>
				</tr>
				</xsl:if>
        <xsl:if test="td[3]='本月合计' or  td[3]='本年累计'">
      	<tr>
	  			<xsl:for-each select="td">
	   			<xsl:choose>
	   				<xsl:when test="position()=1">
			       	<td>
			          
			       	</td>
	      		</xsl:when>
	      		<xsl:when test="position()=2">
	    			<td>
	          	<a href="#">
	          	<xsl:attribute name="onclick" >
	          	 javascript:openDialog('../pay/pay/update_detail.html?load=&lt;pay_bill_id&gt;<xsl:value-of select="../td[last()]"/>&lt;/pay_bill_id&gt;','dialogWidth:850px;dialogHeight:650px',result)
							</xsl:attribute>
	          	<xsl:value-of select="."/>
	          	</a>
	        		</td>
	      		</xsl:when>
		      	<xsl:when test="position()=4 or position()=5 or position()=6">
			        <td align='right'>
			          <xsl:value-of select="format-number(.,'#,##0.00')"/>
			        </td>
		      	</xsl:when>		    
		      	<xsl:when test="position()=7">
			        <td style='display:none'>
			          <xsl:value-of select="."/>
			        </td>
		      	</xsl:when>  
		      	<xsl:otherwise>
		        	<td align='left'>
		          <xsl:value-of select="."/>
		        	</td>
		      	</xsl:otherwise>			                        
	    		</xsl:choose>
	  			</xsl:for-each>
				</tr>
				</xsl:if>
			</xsl:for-each>
   	       
    </tbody>
  </xsl:template>
</xsl:stylesheet>

