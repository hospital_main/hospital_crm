<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/pact/firstpage/alerm/information.xsl,v 1.3 2013-05-23 07:17:58 yuchengying Exp $
 $Author: yuchengying $
 $Date: 2013-05-23 07:17:58 $
 $Revision: 1.3 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead> 
  		<tr noWrap="true" class="mainHead">
				<th>提醒条目</th>
				<th>内容</th>
				<th>提醒条目</th>
				<th>内容</th>
				<th>提醒条目</th>
				<th>内容</th>
				<th>提醒条目</th>
				<th>内容</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>   
            <xsl:choose>
							<xsl:when test="position()=1 or position()=3">
							 <xsl:for-each select="td">
							 	 <xsl:choose>
							 	 	<xsl:when test="position()=2 or position()=4 or position()=6 or position()=8">
							 	 	  <td align="right"><xsl:value-of select="format-number(.,'#,##0')"/></td>
							 	 	</xsl:when>
							 	  <xsl:otherwise>
							 	    <td><xsl:value-of select="."/></td>
							 	  </xsl:otherwise>
							 	 </xsl:choose>
							 </xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
					  	<xsl:for-each select="td">
					  		 <xsl:choose>
					  		 	<xsl:when test="position()=2 or position()=4 or position()=6 or position()=8">
							 	 	  <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							 	 	</xsl:when>
							 	 	 <xsl:otherwise>
							 	    <td><xsl:value-of select="."/></td>
							 	  </xsl:otherwise>
					  		 </xsl:choose>
							 </xsl:for-each>  
              </xsl:otherwise>
					  </xsl:choose>
  			</tr>
   		</xsl:for-each>    
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
