<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
    	<tr noWrap='true' class='mainHead'>
        <th nowrap='true' rowspan='2'>合同编号</th>
        <th nowrap='true' rowspan='2'>合同名称</th>
		  	<th nowrap='true' rowspan='2'>客户</th>
		  	<th nowrap='true' rowspan='2'>签订科室</th>
		  	<th nowrap='true' rowspan='2'>签订日期</th>
		  	<th nowrap='true' rowspan='2'>结转</th>
		  	<th nowrap='true' rowspan='2'>应收凭证</th>
		  	<th nowrap='true' colspan='9'>计划收款</th>
		  	<th nowrap='true' colspan='6'>实际收款</th>
      </tr>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>期号</th>
        <th nowrap='true'>收款日期</th>
		  	<th nowrap='true'>资金来源</th>
		  	<th nowrap='true'>收款方式</th>
		  	<th nowrap='true'>计划金额</th>
		  	<th nowrap='true'>变动金额</th>
		  	<th nowrap='true'>期初收款</th>
		  	<th nowrap='true'>实际收款</th>
		  	<th nowrap='true'>未收金额</th>
		
		  	<th nowrap='true'>收款单号</th>
		  	<th nowrap='true'>收款日期</th>
		  	<th nowrap='true'>资金来源</th>
		  	<th nowrap='true'>收款方式</th>
		  	<th nowrap='true'>收款金额</th>
		  	<th nowrap='true'>收款凭证</th>
      </tr>
    </thead>
    <tbody> 
   		<xsl:for-each select="/root/tbody/tr">
   			<xsl:choose>
   				<xsl:when test="td[position()=2]/text()='合计'">
   					<tr>
		          <xsl:for-each select="td">
		            <xsl:choose>
		            	<xsl:when test="position()=1">
										<td>合计</td>
									</xsl:when>
									<xsl:when test="position()=12">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=13">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=14">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=15">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=16">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=21">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=23">
									</xsl:when>
		              <xsl:otherwise>
		                <td></td>
		              </xsl:otherwise>
		          	</xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
   				</xsl:when>
   				<xsl:otherwise>
		        <tr>
		        	<xsl:variable name="postr"><xsl:value-of select="position()-1"/></xsl:variable>
		        	<xsl:variable name="td1"><xsl:value-of select="td[1]"/></xsl:variable>
		        	<xsl:variable name="td8"><xsl:value-of select="td[8]"/></xsl:variable>
		        	<xsl:variable name="countp"><xsl:value-of select="count(../tr[td[1]/text()=$td1 and td[8]/text()=$td8])"/></xsl:variable>
		        	<xsl:choose>
		        	<xsl:when test="../tr[position()=$postr]/td[1]/text()=$td1">
		        		<xsl:variable name="pretrtd8"><xsl:value-of select="../tr[position()=$postr]/td[8]/text()"/></xsl:variable>
			          <xsl:for-each select="td">
			            <xsl:choose>
			            	<xsl:when test="position()&lt;8">
		              	<td style="display:none"></td>
		              	</xsl:when>
		              	<xsl:when test="position()&lt;17">
		              		<xsl:choose>
		            			<xsl:when test="$pretrtd8=$td8">
		            				<td style="display:none"></td>
		            			</xsl:when>
		              		<xsl:otherwise>
			                	<td><xsl:attribute name="rowspan"><xsl:value-of select="$countp"/></xsl:attribute>
			                		<xsl:choose>
				              		<xsl:when test="position()&gt;11">
				              			<xsl:attribute name="align">right</xsl:attribute>
														<xsl:value-of select="format-number(.,'#,##0.00')"/>
													</xsl:when>
													<xsl:otherwise>
						                <xsl:value-of select="."/>
						              </xsl:otherwise>
													</xsl:choose>
			                	</td>
			              	</xsl:otherwise>
			              	</xsl:choose>
			            	</xsl:when>
			            	<xsl:when test="position()=17">
		              	<td>
									  <a tabindex='-1'>
								          <xsl:attribute name="href" >
								    	    javascript:openDialog('../../paydocu/update.html?load=&lt;contract_code&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/contract_code&gt;&lt;receive_id&gt;<xsl:value-of select="../pk/receive_id"/>&lt;/receive_id&gt;', 'dialogWidth:1000px;dialogHeight:570px', result)
								  	      </xsl:attribute><xsl:value-of select="."/></a>
									  </td>
		              	</xsl:when>
										<xsl:when test="position()=21">
											<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
										</xsl:when>
										<xsl:when test="position()=22">
											<td>
												<a>
										    <xsl:attribute name="href" >
										       javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/s_vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
										    </xsl:attribute><xsl:value-of select="."/></a>
											</td>
										</xsl:when>
										<xsl:when test="position()=23">
										</xsl:when>
			              <xsl:otherwise>
			                <td><xsl:value-of select="."/></td>
			              </xsl:otherwise>
			          	</xsl:choose>
			  			  </xsl:for-each>
		  			  </xsl:when>
		  			  <xsl:otherwise>
		  			  	<xsl:variable name="countc"><xsl:value-of select="count(../tr[td[1]/text()=$td1])"/></xsl:variable>
		        		<xsl:for-each select="td">
		              <xsl:choose>
		              <xsl:when test="position()=1">
		              	<td><xsl:attribute name="rowspan"><xsl:value-of select="$countc"/></xsl:attribute>
		              		<a>
									    <xsl:attribute name="href" >
									      javascript:viewContract('<xsl:value-of select="../pk/contract_code"/>')
									    </xsl:attribute><xsl:value-of select="."/></a>
		              	</td>
		              </xsl:when>
		              <xsl:when test="position()=7">
		              	<td><xsl:attribute name="rowspan"><xsl:value-of select="$countc"/></xsl:attribute>
		              	<a>
									    <xsl:attribute name="href" >
									       javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/c_vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
									    </xsl:attribute><xsl:value-of select="."/></a>
		              	</td>
		              </xsl:when>
		              <xsl:when test="position()&lt;8">
		              	<td><xsl:attribute name="rowspan"><xsl:value-of select="$countc"/></xsl:attribute><xsl:value-of select="."/></td>
		              </xsl:when>
		              <xsl:when test="position()&lt;17">
		              	<td><xsl:attribute name="rowspan"><xsl:value-of select="$countp"/></xsl:attribute>
		              		<xsl:choose>
		              		<xsl:when test="position()&gt;11">
		              			<xsl:attribute name="align">right</xsl:attribute>
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
											</xsl:when>
											<xsl:otherwise>
				                <xsl:value-of select="."/>
				              </xsl:otherwise>
											</xsl:choose>
		              	</td>
		              </xsl:when>
		              <xsl:when test="position()=17">
		              	<td>
									  <a tabindex='-1'>
								          <xsl:attribute name="href" >
								    	    javascript:openDialog('../../paydocu/update.html?load=&lt;contract_code&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/contract_code&gt;&lt;receive_id&gt;<xsl:value-of select="../pk/receive_id"/>&lt;/receive_id&gt;', 'dialogWidth:1000px;dialogHeight:570px', result)
								  	      </xsl:attribute><xsl:value-of select="."/></a>
									  </td>
		              </xsl:when>
									<xsl:when test="position()=21">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=22">
											<td>
												<a>
										    <xsl:attribute name="href" >
										       javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/s_vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
										    </xsl:attribute><xsl:value-of select="."/></a>
											</td>
									</xsl:when>
									<xsl:when test="position()=23">
									</xsl:when>
		              <xsl:otherwise>
		                <td><xsl:value-of select="."/></td>
		              </xsl:otherwise>
		          		</xsl:choose>
		          	</xsl:for-each>
		          </xsl:otherwise>
		          </xsl:choose>
		  			</tr>
  				</xsl:otherwise>
   			</xsl:choose>
   		</xsl:for-each>        
    </tbody>
  </xsl:template>
</xsl:stylesheet>

