<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
  		<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="20"/>    
	        </xsl:call-template>
	  	</tr>
		  <tr noWrap='true' class='mainHead'>
        <td nowrap='true' rowspan='2'>合同编号</td>
        <td nowrap='true' rowspan='2'>合同名称</td>
		  	<td nowrap='true' rowspan='2'>客户</td>
		  	<td nowrap='true' rowspan='2'>签订科室</td>
		  	<td nowrap='true' rowspan='2'>签订日期</td>
		  	<td nowrap='true' rowspan='2'>结转</td>
		  	<td nowrap='true' rowspan='2'>应收凭证</td>
		  	<td nowrap='true' colspan='9'>计划收款</td>
		  	<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
		  	<td nowrap='true' colspan='6'>实际收款</td>
		  	<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
      </tr>
      <tr noWrap='true' class='mainHead'>
      	<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
				<td style='display:none'/>
        <td nowrap='true'>期号</td>
        <td nowrap='true'>收款日期</td>
		  	<td nowrap='true'>资金来源</td>
		  	<td nowrap='true'>收款方式</td>
		  	<td nowrap='true'>收款金额</td>
		  	<td nowrap='true'>变动金额</td>
		  	<td nowrap='true'>期初收款</td>
		  	<td nowrap='true'>实际收款</td>
		  	<td nowrap='true'>未收金额</td>
		  	<td nowrap='true'>收款单号</td>
		  	<td nowrap='true'>收款日期</td>
		  	<td nowrap='true'>资金来源</td>
		  	<td nowrap='true'>收款方式</td>
		  	<td nowrap='true'>收款金额</td>
		  	<td nowrap='true'>收款凭证</td>
      </tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr[position()=1]">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=1">
								<td>合计</td>
							</xsl:when>
							<xsl:when test="position()=12">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=13">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=14">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=15">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=16">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=21">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=23">
							</xsl:when>
              <xsl:otherwise>
                <td></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  
   		<xsl:for-each select="/root/tbody/tr">
   			<xsl:if test="position()!=1">
        <tr>
        	<xsl:variable name="postr"><xsl:value-of select="position()-1"/></xsl:variable>
        	<xsl:variable name="td1"><xsl:value-of select="td[1]"/></xsl:variable>
        	<xsl:variable name="td8"><xsl:value-of select="td[8]"/></xsl:variable>
        	<xsl:variable name="countp"><xsl:value-of select="count(../tr[td[1]/text()=$td1 and td[8]/text()=$td8])"/></xsl:variable>
        	<xsl:choose>
        	<xsl:when test="../tr[position()=$postr]/td[1]/text()=$td1">
        		<xsl:variable name="pretrtd8"><xsl:value-of select="../tr[position()=$postr]/td[8]/text()"/></xsl:variable>
	          <xsl:for-each select="td">
	            <xsl:choose>
	            	<xsl:when test="position()&lt;8">
              	<td style="display:none"></td>
              	</xsl:when>
              	<xsl:when test="position()&lt;17">
              		<xsl:choose>
            			<xsl:when test="$pretrtd8=$td8">
            				<td style="display:none"></td>
            			</xsl:when>
              		<xsl:otherwise>
	                	<td><xsl:if test="$countp!=0"><xsl:attribute name="rowspan"><xsl:value-of select="$countp"/></xsl:attribute></xsl:if>
	                		<xsl:choose>
		              		<xsl:when test="position()&gt;11">
		              			<xsl:attribute name="align">right</xsl:attribute>
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
											</xsl:when>
											<xsl:otherwise>
				                <xsl:value-of select="."/>
				              </xsl:otherwise>
											</xsl:choose>
	                	</td>
	              	</xsl:otherwise>
	              	</xsl:choose>
	            	</xsl:when>
								<xsl:when test="position()=21">
									<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
								</xsl:when>
								<xsl:when test="position()=23">
								</xsl:when>
	              <xsl:otherwise>
	                <td><xsl:value-of select="."/></td>
	              </xsl:otherwise>
	          	</xsl:choose>
	  			  </xsl:for-each>
  			  </xsl:when>
  			  <xsl:otherwise>
  			  	<xsl:variable name="countc"><xsl:value-of select="count(../tr[td[1]/text()=$td1])"/></xsl:variable>
        		<xsl:for-each select="td">
              <xsl:choose>
              <xsl:when test="position()&lt;8">
              	<td><xsl:attribute name="rowspan"><xsl:value-of select="$countc"/></xsl:attribute><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:when test="position()&lt;17">
              	<td><xsl:attribute name="rowspan"><xsl:value-of select="$countp"/></xsl:attribute>
              		<xsl:choose>
              		<xsl:when test="position()&gt;11">
              			<xsl:attribute name="align">right</xsl:attribute>
										<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</xsl:when>
									<xsl:otherwise>
		                <xsl:value-of select="."/>
		              </xsl:otherwise>
									</xsl:choose>
              	</td>
              </xsl:when>
							<xsl:when test="position()=21">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=23">
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          		</xsl:choose>
          	</xsl:for-each>
          </xsl:otherwise>
          </xsl:choose>
  			</tr>
  			</xsl:if>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>