<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				
				<th>变更单号</th>
				<th>变更日期</th>
				<th>合同类别</th>	
				<th>合同编号</th>
				<th>合同名称</th>			
				<th>签订日期</th>		
				<th>客户</th>
				<th>变更序号</th>
				<th>变更原因</th>
				<th>文档</th>
				<th>操作人</th>
				<th>审核人</th>
				<th>确认人</th>
				<th>状态</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:if test="td[8] = '0'">
        		<td></td>
        	</xsl:if>
					<xsl:if test="td[8] != '0'">
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
        	</xsl:if>
        	
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<td>
										
										<a href="#">
											 <xsl:attribute name="onclick" >											 	  
                					openDialog('change.html?load=&lt;change_code&gt;<xsl:value-of select="../pk/change_code"/>&lt;/change_code&gt;&lt;contract_no&gt;<xsl:value-of select="../pk/contract_no"/>&lt;/contract_no&gt;', 'dialogWidth:600px;dialogHeight:250px',result)
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=4">
									<td>
										<a href="#">
										 <xsl:attribute name="onclick" >
              					openPactPage('<xsl:value-of select="../pk/contract_no"/>','2','02','<xsl:value-of select="../pk/comp_code"/>','<xsl:value-of select="../pk/copy_code"/>')
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
							
								<xsl:when test="position()=10">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openDialog('../collcon/doc.html?load=&lt;contract_code&gt;<xsl:value-of select="../pk/contract_no"/>&lt;/contract_code&gt;', 'dialogWidth:750px;dialogHeight:400px')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
