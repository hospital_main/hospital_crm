<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>摘要</th>
				<th>资金来源</th>
				<th>收款方式</th>
				<th>收款期限</th>
				<th>收款条件</th>
				<th>收款金额</th>
				<th>变动金额</th>
				<th>期初收款</th>
				<th>实际收款</th>
				<th>收款比例(%)</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<td><xsl:value-of select="."/></td>	
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
