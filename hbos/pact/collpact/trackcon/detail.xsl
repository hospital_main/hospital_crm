<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>合同项目</th>
				<th>名称</th>
				<th>规格</th>
				<th>型号</th>
				<th>数量</th>
				<th>单价</th>
				<th>金额</th>
				<th>变动金额</th>
				<th>备注</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<td><xsl:value-of select="."/></td>	
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
