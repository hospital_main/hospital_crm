<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>保函编号</th>
				<th>开具日期</th>
				<th>银行</th>
				<th>客户</th>
				<th>担保金额</th>
				<th>开始日期</th>
				<th>结束日期</th>
				<th>索赔条件</th>
				<th>备注</th>
				<th>制单人</th>
				<th>审核人</th>
				<th>确认人</th>
				<th>状态</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">				
						<xsl:choose>
							<xsl:when test="position()=1">			
								<td><a href="#">
							    <xsl:attribute name="onclick" >
							      openDialog('../banksecu/update.html?load=&lt;letter_id&gt;<xsl:value-of select="../pk/letter_id"/>&lt;/letter_id&gt;','dialogWidth:600px;dialogHeight:550px')
							    </xsl:attribute><xsl:value-of select="."/></a></td>
							</xsl:when>
							<xsl:when test="position()=5">			
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>