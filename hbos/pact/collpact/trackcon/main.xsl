<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>合同编号</th>
				<th>合同名称</th>
				<th>合同类别</th>
				<th>签订日期</th>
				<th>签订科室</th>
				<th>客户</th>
				<th>项目信息</th>
				<th>合同金额</th>
				<th>保证金</th>
				<th>期初执行</th>
				<th>执行金额</th>
				<th>合计执行</th>
				<th>执行进度(%)</th>
				<th>文档</th>
				<th>合同状态</th>
				<th>结转</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:choose>
   				<xsl:when test="td[position()=1]/text()='合计'">
   					<tr>
							<td></td>
							<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=8">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=10">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=11">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=12">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:otherwise>
										<td><xsl:value-of select="."/></td>
									</xsl:otherwise>
								</xsl:choose>	
							</xsl:for-each>	
					  </tr>
   				</xsl:when>
   				<xsl:otherwise>
						<tr>
							<td align="center" style="display:none">
								<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
									<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
								</input>
							</td>
							<xsl:for-each select="td">
								<xsl:choose>
									<xsl:when test="position()=1">
										<td>
											<a href="#">
													<xsl:attribute name="onclick" >
			                			openPactDetailPage('<xsl:value-of select="../pk/contract_code"/>','<xsl:value-of select="../pk/comp_code"/>','<xsl:value-of select="../pk/copy_code"/>')
													</xsl:attribute>
													<xsl:value-of select="."/>
											</a>
										</td>
									</xsl:when>
									<xsl:when test="position()=8">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=9">
										<td  noWrap='true' >
											<a href="#">
												<xsl:attribute name="onclick" >
			                		window.showModalDialog('secu.html?load=&lt;code&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/code&gt;',window, 'scrollbars:no;resizable:no;help:no;status:no;dialogWidth:700px;dialogHeight:500px')
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</td>
									</xsl:when>
									<xsl:when test="position()=10">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=11">
										<td align='right'>
											<a href="#">
												<xsl:attribute name="onclick" >
			                		openDialog('../guarantee/update.html?load=&lt;b&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/b&gt;','dialogWidth:1050px;dialogHeight:450px')
												</xsl:attribute>
												<xsl:value-of select="format-number(.,'#,##0.00')"/>
											</a>
										</td>
									</xsl:when>
									<xsl:when test="position()=12">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=13">
										<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
									</xsl:when>
									<xsl:when test="position()=14">
										<td>
											<a href="#">
												<xsl:attribute name="onclick" >
		                			openDialog('../collcon/doc.html?load=&lt;contract_code&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/contract_code&gt;', 'dialogWidth:750px;dialogHeight:400px')
												</xsl:attribute>
												<xsl:value-of select="."/>
											</a>
										</td>
									</xsl:when>
									<xsl:otherwise>
										<td><xsl:value-of select="."/></td>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>	
						</tr>
					</xsl:otherwise>
   			</xsl:choose>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
