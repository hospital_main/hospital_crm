<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th nowrap='true'>收款单号</th>
				<th nowrap='true'>收款日期</th>
				<th nowrap='true'>发票号码</th>
				<th nowrap='true'>收款金额</th>
				<th nowrap='true'>业务员</th>
				<th nowrap='true'>制单人</th>
				<th nowrap='true'>审核人</th>
				<th nowrap='true'>确认人</th>
				<th nowrap='true'>状态</th>
				<th nowrap='true'>凭证</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=1">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openDialog('../paydocu/update.html?load=&lt;receive_id&gt;<xsl:value-of select="../pk/receive_id"/>&lt;/receive_id&gt;', 'dialogWidth:1000px;dialogHeight:570px',result)
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=10">
								<td>
									<a href="#">
										<xsl:attribute name="onclick" >
											javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
							   		</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
								</td>
							</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>    
		</tbody>
	</xsl:template>
</xsl:stylesheet>
