<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>合同编号</th>
				<th>合同名称</th>
				<th>合同类别</th>
				<th>签订日期</th>				
				<th>签订科室</th>
				<th>客户</th>
				<th>项目信息</th>
				<th>合同金额</th>
				<th>收款计划</th>
				<th>期初执行</th>
				<th>执行金额</th>
				<th>合计执行</th>
				<th>执行进度(%)</th>
				<th>文档</th>		
				<th>变更</th>
				<th>合同状态</th>
				<th>结转</th>
				<th>凭证</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:variable name="receive_id" select="td[1]"/>
				<tr>
					<xsl:if test="td[1] = '合计'">
        		<td></td>
        	</xsl:if>
        	<xsl:if test="td[1] != '合计'">
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
        	</xsl:if>
        	
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:if test=". = '合计'">
										<td>
										<xsl:value-of select="."/>
									</td>
									</xsl:if>
									<xsl:if test=". != '合计'">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openUpdatepage('<xsl:value-of select="../pk/contract_code"/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=8 or position()=10 or position()=12 or position()=13">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,###0.00')" />
									</td>
								</xsl:when>
								<xsl:when test="position()=9">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openDialog('../collcon/plan.html?load=&lt;contract_code&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/contract_code&gt;', 'dialogWidth:750px;dialogHeight:400px')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=11">
									<td  align='right'>
										<xsl:if test="$receive_id !='合计'">
											<a href="#">
										 		<xsl:attribute name="onclick" >
              					openDialog('paydocu.html?load=&lt;contract_code&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/contract_code&gt;', 'dialogWidth:750px;dialogHeight:400px')
												</xsl:attribute>
												<xsl:value-of select="format-number(.,'###,###0.00')" />
											</a>
										</xsl:if>
										<xsl:if test="$receive_id ='合计'">
											<xsl:value-of select="format-number(.,'###,###0.00')" />
		              	</xsl:if>
									</td>
								</xsl:when>
								<xsl:when test="position()=14">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openDialog('../collcon/doc.html?load=&lt;contract_code&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/contract_code&gt;', 'dialogWidth:750px;dialogHeight:400px')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=15">
										<td>
										<a href="#">
											 <xsl:attribute name="onclick" >
                					openDialog('changeList.html?load=&lt;contract_code&gt;<xsl:value-of select="../pk/contract_code"/>&lt;/contract_code&gt;', 'dialogWidth:750px;dialogHeight:400px')
											 </xsl:attribute>
										<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:when test="position()=18">
									<td>
										<a href="#">
											<xsl:attribute name="onclick" >
												javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
												</xsl:attribute>
											<xsl:value-of select="."/>
										</a>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
