<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>摘要</th>
				<th>资金来源</th>
				<th>收款方式</th>
				<th>收款期限</th>
				<th>收款条件</th>
				<th>收款金额</th>
				<xsl:if test="/root/tbody/tr/td[10] != 1">
					<th style="display:none">期初收款</th>
				</xsl:if>
				<xsl:if test="/root/tbody/tr/td[10] = 1">
					<th>期初收款</th>
				</xsl:if>
				
				<th>变更金额</th>
				<th>收款比例(%)</th>
				<th style="display:none">是否期初</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">						
							<xsl:choose>
								<xsl:when test="position()= 7 ">
									<xsl:if test="../td[10] != 1">
										<td style="display:none">
											<xsl:value-of select="."/>
										</td>
									</xsl:if>
									<xsl:if test="../td[10] = 1">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,###0.00')"/>
										</td>
									</xsl:if>
								</xsl:when>
								<xsl:when test="position()=6 or position()=8 or position=9">
										<td align='right'>
											<xsl:value-of select="format-number(.,'###,###0.00')"/>
									</td>
								</xsl:when>
								<xsl:when test="position()=10">
										<td style="display:none">
											<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
