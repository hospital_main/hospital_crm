<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
  	<thead>
  		<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>
		  <tr noWrap='true' class='mainHead'>
        <td nowrap='true'>收款单号</td>
        <td nowrap='true'>收款日期</td>
		  	<td nowrap='true'>客户</td>
		  	<td nowrap='true'>发票号码</td>
		  	<td nowrap='true'>收款金额</td>
		  	<td nowrap='true'>合同编号</td>
		  	<td nowrap='true'>合同名称</td>
		  	<td nowrap='true'>合同期号</td>
		  	<td nowrap='true'>摘要</td>
		  	<td nowrap='true'>计划日期</td>
		  	<td nowrap='true'>计划金额</td>
		  	<td nowrap='true'>制单人</td>
		  	<td nowrap='true'>审核人</td>
		  	<td nowrap='true'>确认人</td>
		  	<td nowrap='true'>状态</td>
		  	<td nowrap='true'>凭证</td>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position()=1]">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=1 ">
								<td><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:when test="position()=5 ">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=11">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<xsl:for-each select="/root/tbody/tr[position()!=1]">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
							<xsl:when test="position()=1">
							  <td  noWrap='true' >
							  <a href="#">
						          <xsl:attribute name="onclick" >
						    	    openUpdate('../paydocu/update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;write_able&gt;true&lt;/write_able&gt;')
						  	      </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:when test="position()=5">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=6">
							  <td  noWrap='true' >
							  <a tabindex="-1">
							    <xsl:attribute name="href" >
							      javascript:viewContract('<xsl:value-of select="."/>')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:when test="position()=11">
								<td align='right'><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:when test="position()=16">
								<td  noWrap='true' >
							  <a>
							    <xsl:attribute name="href" >
							       javascript:openVouchDlg('&lt;vouch_id&gt;<xsl:value-of select="../pk/vouch_id"/>&lt;/vouch_id&gt;&lt;edit_mask&gt;readOnly&lt;/edit_mask&gt;')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          	</xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>