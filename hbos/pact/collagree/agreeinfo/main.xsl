<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
        <th nowrap='true'>协议编号</th>
        <th nowrap='true'>协议名称</th>
		  	<th nowrap='true'>签订日期</th>
		  	<th nowrap='true'>客户</th>
		  	<th nowrap='true'>签订科室</th>
		  	<th nowrap='true'>开始日期</th>
		  	<th nowrap='true'>截止日期</th>
		  	<th nowrap='true'>文档</th>
		  	<th nowrap='true'>状态</th>
		  	<th nowrap='true'>结转</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td align='left'>
									<a tabindex='-1' href="#">
						                  <xsl:attribute name="onclick" >
						    	            openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;RW_flag&gt;1&lt;/RW_flag&gt;', 'dialogWidth:1000px;dialogHeight:570px', result)
						  	          </xsl:attribute><xsl:value-of select="."/></a>
								 </td>
              </xsl:when>
              <xsl:when test="position()=8">
								<td align='left'>
									<a tabindex='-1' href="#">
						                  <xsl:attribute name="onclick" >
						    	            openDialog('main_doc.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:850px;dialogHeight:523px', result)
						  	          </xsl:attribute><xsl:value-of select="."/></a>
								 </td>
              </xsl:when>
              <xsl:otherwise>
								<td>
		                 <xsl:value-of select="."/>
				  			</td>
				  		</xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

