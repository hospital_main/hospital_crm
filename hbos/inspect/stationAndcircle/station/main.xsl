<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th >
					<input type="checkbox"/>
				</th>
				<th>站点编码</th> 
				<th>站点名称</th> 
				<th>站点安全级别</th>
				<th>站点位置信息</th> 
				<th>位置半径</th> 
				<th>是否启用</th> 
				<th>备注</th> 
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">						
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
								<a href="#">
			                <xsl:attribute name="onclick">
			                	openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:350px;dialogHeight:300px');
			                </xsl:attribute>
			                <xsl:value-of select="."/>
								</a>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
