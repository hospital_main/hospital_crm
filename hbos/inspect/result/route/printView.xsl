<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
    <thead>
    	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		  </tr>
      <tr noWrap="true" class="mainHead">
      	<td>记录编号</td>
        <td>记录日期</td>
        <td>巡检计划</td>
        <td>巡检线路</td>
        <td>巡检人</td>
        <td>巡检类型</td>
        <td>卡片编码</td>
        <td>设备名称</td>
        <td>设备规格</td>
        <td>项目序号</td>
        <td>项目名称</td>
        <td>检查数据</td>
        <td>数据单位</td>
        <td>问题描述</td>
        <td>状态</td>
      </tr>                
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>    
					<xsl:for-each select="td">
						<td><xsl:value-of select="."/></td> 	
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 		
	  </tbody> 	
	  </root>
  </xsl:template>
</xsl:stylesheet>
