<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
        <th noWrap="true">记录编号</th>
        <th noWrap="true">记录日期</th>
        <th noWrap="true">计划编号</th>
        <th noWrap="true">巡检路线</th>
        <th noWrap="true">巡检人</th>
        <th noWrap="true">巡检类型</th>
        <th noWrap="true">编制科室</th>
        <th noWrap="true">设备科室</th>
        <th noWrap="true">卡片编号</th>
        <th noWrap="true">设备名称</th>
        <th noWrap="true">责任人</th>
        <th noWrap="true">设备规格</th>
        <th noWrap="true">故障描述</th>
        <th noWrap="true">故障原因</th>
        <th noWrap="true">整改措施</th>
        <th noWrap="true">科室意见</th>
      </tr>                
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>    
					<xsl:for-each select="td">
						<xsl:choose>
							 <xsl:when test="position()=1">
                <td>
	                   	<a>
	                  	<xsl:attribute name="href" >
	              				javascript:openDialog('recordInfo.html?load=&lt;rec_code&gt;<xsl:value-of select="."/>&lt;/rec_code&gt;', 'dialogWidth:1000px;dialogHeight:850px;')
	      	    				</xsl:attribute>
	                  	<xsl:value-of select="."/>
	                 		</a>
                 	</td>
               </xsl:when>   
               <xsl:when test="position()=3">
                <td>
	              <a href="#">
									 <xsl:attribute name="onclick" >
            					openUpdatepage('<xsl:value-of select="."/>')
										</xsl:attribute>
										<xsl:value-of select="."/>
								</a>
                 	</td>
               </xsl:when>   
							 <xsl:when test="position()=9">
                <td>
	                   	<a>
	                  	<xsl:attribute name="href" >
	              				javascript:openDialog('../../../repaire/fixcard/maintain/cardInfo1.html?load=&lt;card_arch_no&gt;<xsl:value-of select="."/>&lt;/card_arch_no&gt;', 'dialogWidth:1000px;dialogHeight:800px;')
	      	    				</xsl:attribute>
	                  	<xsl:value-of select="."/>
	                 		</a>
                 	</td>
                </xsl:when>   
              <xsl:otherwise>
								<td><xsl:value-of select="."/></td>
						  </xsl:otherwise>
						 	</xsl:choose> 
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 		
	  </tbody> 	
  </xsl:template>
</xsl:stylesheet>
