<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true"  class='mainHead'>
				 
				<th>卡片编码</th> 
				<th>资产名称</th>
				<th>资产规格</th> 
				<th>资产净值</th> 
				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
        <tr>
        	  
          <xsl:for-each select="td">
						
	          <xsl:choose>
	          	<xsl:when test="position()=4">
	          		<td align="right">
									<xsl:value-of select="."/>
								</td>
	          	</xsl:when>
	          	<xsl:otherwise>
	          		<td>
		              <xsl:value-of select="."/>
		            </td>
	          	</xsl:otherwise>
	          </xsl:choose>
						
          </xsl:for-each>
        </tr>
      </xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
