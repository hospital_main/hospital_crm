<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap="true" class="mainHead">
        <th noWrap="true">卡片编码</th>
        <th noWrap="true">设备名称</th>
        <th noWrap="true">设备规格</th>
        <th noWrap="true">供应商</th>
        <th noWrap="true">生产厂商</th>
        <th noWrap="true">品牌</th>
        <th noWrap="true">序列号</th>
        <th noWrap="true">入库日期</th>
        <th noWrap="true">科室信息</th>
        <th noWrap="true">责任人</th>
        <th noWrap="true">原值</th>
        <th noWrap="true">折旧年限</th>
        <th noWrap="true">使用状态</th>
      </tr>                
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr> 
					<xsl:for-each select="td">
						<xsl:choose>
							 <xsl:when test="position()=1">
                  <td>
	                   	<a>
	                  	<xsl:attribute name="href" >
	              				javascript:openDialog('update.html?load=&lt;equi_arch_no&gt;<xsl:value-of select="."/>&lt;/equi_arch_no&gt;&lt;equi_code&gt;<xsl:value-of select="../td[14]"/>&lt;/equi_code&gt;', 'dialogWidth:1000px;dialogHeight:850px;')
	      	    				</xsl:attribute>
	                  	<xsl:value-of select="."/>
	                 		</a>
                 	</td>
                </xsl:when>      
                <xsl:when test="position()=14">
                  
                </xsl:when> 
              <xsl:otherwise>
								<td><xsl:value-of select="."/></td>
						  </xsl:otherwise>
						 	</xsl:choose> 
					</xsl:for-each>
				</tr>				
			</xsl:for-each> 		
	  </tbody> 	
  </xsl:template>
</xsl:stylesheet>
