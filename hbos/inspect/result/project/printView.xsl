<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
    <thead>
    	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		  </tr>
      <tr noWrap="true" class="mainHead">
        <td>卡片编码</td>
        <td>设备名称</td>
        <td>设备规格</td>
        <td>供应商</td>
        <td>生产厂商</td>
        <td>品牌</td>
        <td>序列号</td>
        <td>入库日期</td>
        <td>科室信息</td>
        <td>责任人</td>
        <td>原值</td>
        <td>折旧年限</td>
        <td>使用状态</td>
      </tr>                
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>    
					<xsl:for-each select="td">
						<td><xsl:value-of select="."/></td> 	
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 		
	  </tbody> 	
	  </root>
  </xsl:template>
</xsl:stylesheet>
