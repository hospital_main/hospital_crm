<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th>卡片编码</th><th>资产名称</th><th>资产规格</th><th>资产原值</th><th>巡检结果</th><th>异常描述</th><th>电子签名</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>	
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
									<a tabindex='-1' href='#'>
										<xsl:attribute name='onclick'>
											openDialog('item.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:380px;dialogHeight:260px', '');
											</xsl:attribute>
										<xsl:value-of select="."/>
									</a>
								</td>
							</xsl:when>
							<xsl:when test="position() = last()">
								<td>
									<span>
									<a tabindex='-1' href='#'>
										<xsl:attribute name='onclick'>
											if('<xsl:value-of select="."/>' == '') return;
											openD1('<xsl:value-of select="."/>')
										</xsl:attribute>
										查看
									</a>
									</span>
									<span>  
									<a tabindex='-1' href='#' style="margin-left:10px;">
										<xsl:attribute name='onclick'>
											if(state.value == 1) {alert("已审核，不能上传！"); return}; 										
											openDialog('uploadPic.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>','dialogWidth:400px;dialogHeight:150px;resizable:no;', '')
											loadData();
										</xsl:attribute>
										上传
									</a>
									</span>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
