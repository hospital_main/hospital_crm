<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/> 
      		<td style="display:none"/>
      		<td style="display:none"/> 
      		<td style="display:none"/> 
      		<td style="display:none"/>
      		<td style="display:none"/> 
      		<td style="display:none"/> 
      	</tr>
			<tr noWrap='true' class='mainHead'>
				
				<td>记录编码</td> 
				<td>计划编码</td> 
				<td>记录日期</td> 
				<td>巡检人员</td> 
				<td>巡检线路</td> 
				<td>编制科室</td> 
				<td>记录说明</td> 
				<td>审核人</td> 
				<td>状态</td> 
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					<td>
          	<xsl:value-of select="."/>
	  			</td>
							
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
