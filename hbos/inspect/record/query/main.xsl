<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th colspan="2">记录</th>
				<th colspan="3">巡检</th>
				<th rowspan="2">计划编号</th>
				<th rowspan="2">卡片编号</th>
				<th rowspan="2">编制科室</th>
				<th rowspan="2">责任人</th>
				<th rowspan="2">供应商</th>
				<th colspan="4">设备</th>
			</tr>
			<tr noWrap='true' class='mainHead'>	
				<th>编号</th>
				<th>日期</th> 
				<th>路线</th> 
				<th>人员</th>
				<th>结果</th>
				<th>科室</th>
				<th>名称</th>
				<th>规格</th>
				<th>原值</th>
			</tr>
		</thead>
  	<tbody>
		<xsl:for-each select="/root/tbody/tr">
				<tr >    
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
							<td>
				                <a tabindex='-1' href='#'>
								<xsl:attribute name='onclick'>
									getRecordDetail('<xsl:value-of select="../pk/rec_code"/>','<xsl:value-of select="../pk/plan_code"/>')
								</xsl:attribute>
								<xsl:value-of select="."/>
								</a></td>
							</xsl:when>
							<xsl:when test="position()=6">
								<td>
								<a href="#">
									 <xsl:attribute name="onclick" >
            					getPlanDetail('<xsl:value-of select="../pk/plan_code"/>')
									 </xsl:attribute>
									<xsl:value-of select="."/>
								</a>
								</td>
							</xsl:when>
							<xsl:when test="position()=7"><td>
				                   	<a tabindex='-1' href='#'>
											<xsl:attribute name='onclick'>
												getCardDetail('<xsl:value-of select="../pk/equi_arch_no"/>')
											</xsl:attribute>
											<xsl:value-of select="."/>
										</a></td>
							</xsl:when>
							<xsl:when test="position()=13 or position()=14"><td align='right'>
								<xsl:value-of select="."/></td>
							</xsl:when>
              				<xsl:otherwise>
              					<td>
								<xsl:value-of select="."/></td>
						 	</xsl:otherwise>
              		</xsl:choose>
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 	
	  </tbody> 	
  </xsl:template>
</xsl:stylesheet>
