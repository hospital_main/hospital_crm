<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap='true' class='mainHead'>
	  			<td noWrap="true" colspan="2">记录</td>
	  			<td style='display:none'/>
				<td noWrap="true" colspan="3">巡检</td>
				<td style='display:none'/>
	  			<td style='display:none'/>
				<td rowspan="2">计划编号</td>
				<td rowspan="2">卡片编号</td>
				<td rowspan="2">编制科室</td>
				<td rowspan="2">责任人</td>
				<td rowspan="2">供应商</td>
				<td noWrap="true" colspan="4">设备</td>
				<td style='display:none'/>
	  			<td style='display:none'/>
	  			<td style='display:none'/>
			</tr>
			<tr noWrap='true' class='mainHead'>	
				<td>编号</td>
				<td>日期</td> 

				<td>路线</td> 
				<td>人员</td>
				<td>结果</td>

				<td style='display:none'/>
		        <td style='display:none'/>
		        <td style='display:none'/>
		        <td style='display:none'/>
		        <td style='display:none'/>
				<td>科室</td>
				<td>名称</td>
				<td>规格</td>
				<td>原值</td>
			</tr>
			
	  	</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr align="center">    
					<xsl:for-each select="td">
						
						<xsl:choose>
							<xsl:when test="position()=13 or position()=14"><td align='right'>
										<xsl:value-of select="."/></td>
							</xsl:when>
          				<xsl:otherwise><td>
							<xsl:value-of select="."/></td>
					 	</xsl:otherwise>
              		</xsl:choose>
					</xsl:for-each>
				</tr>	
			</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>