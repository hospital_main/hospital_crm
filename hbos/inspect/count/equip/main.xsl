<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>卡片编码</th><th>设备名称</th><th>设备规格</th>
				<th>供应商</th><th>入库日期</th>
				<th>科室信息</th><th>计划次数</th><th>巡检次数</th>
				<th>正常次数</th>
				<th>异常次数</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">		
							<xsl:choose>
								<xsl:when test="position() &gt; 6">
									<td align='right'>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<td><xsl:value-of select="."/></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
