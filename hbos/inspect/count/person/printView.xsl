<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<xsl:variable name="colNum" select="count(/root/tbody/tr[1]/td)-1"/>
  	<root>
    	<thead>
	    	<tr noWrap='true'>
	        <td style='fontsize:maintitle;colspan:colcount'></td>
	  			<xsl:call-template name="repeat">
	  				<xsl:with-param name="times" select="$colNum"/>    
	        </xsl:call-template>
	  		</tr>		 
	  		<tr noWrap="true" class="mainHead">
				<td>职工编码</td><td>职工姓名</td><td>科室名称</td><td>计划线路数量</td><td>巡检线路数量</td><td>计划设备数量</td><td>巡检设备数量</td><td>正常设备数量</td><td>异常设备数量</td>
				</tr>
	  	</thead>
			<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				<xsl:for-each select="td">
					<xsl:choose>
						<xsl:when test="position() &gt; 3">
									<td align='right'>
										<xsl:value-of select="."/>
									</td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				</tr>
			</xsl:for-each>
			</tbody>
 		</root>
	</xsl:template>
	<xsl:template name="repeat">  
		<xsl:param name="times" select="0"/>  
		<xsl:if test="$times > 0">  
			<td style="display:none"></td>
			<xsl:call-template  name="repeat">  
				<xsl:with-param  name="times" select="$times - 1"/>  
			</xsl:call-template>  
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>