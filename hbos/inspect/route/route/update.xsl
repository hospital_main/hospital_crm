<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true"  class='nodrop nodrag mainHead'  id="id_0">
				<th align="center">
					<input type='checkbox' value=''/>
				</th>
				<th>���ұ���</th> 
				<th>��������</th>
				<th>�������</th> 
				
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
        <tr>
        	<xsl:attribute name="id">id_<xsl:value-of select="position()"/></xsl:attribute>
          <td align='center'  style='cursor:auto;'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
						<td>
				          <xsl:choose>
				          	<xsl:when test="position()=1">
										<a href="#">
											 <xsl:attribute name="onclick" >
		            					loadCard('<xsl:value-of select="."/>')
												</xsl:attribute>
												<xsl:value-of select="."/>
										</a>
				          	</xsl:when>
				          	<xsl:otherwise>
					              <xsl:value-of select="."/>
				          	</xsl:otherwise>
				          </xsl:choose>
						</td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
