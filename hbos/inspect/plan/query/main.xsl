<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
			
				<th>计划编码</th> 
				<th>计划日期</th> 
				<th>巡检类型</th>
				<th>巡检人员</th> 
				<th>巡检路线</th> 
				<th>部门序号</th> 
				<th>科室名称</th> 
				<th>卡片编码</th> 
				<th>资产名称</th> 
				<th>资产规格</th> 
				<th>供应商</th> 
				<th>资产原值</th> 
				<th>入库日期</th> 
				<th>状态</th> 
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
				
					<xsl:for-each select="td">						
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
								<a href="#">
									 <xsl:attribute name="onclick" >
            					openUpdatepage('<xsl:value-of select="../pk/plan_code"/>')
										</xsl:attribute>
										<xsl:value-of select="."/>
								</a>
								</td>
							</xsl:when>
							<xsl:when test="position()=12">
								<td align="right">
									<xsl:value-of select="."/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
