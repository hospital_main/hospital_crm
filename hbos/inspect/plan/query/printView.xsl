<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<root>
		<thead>
			<tr noWrap="true" class="mainHead">
      		<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
      		<td style="display:none"/>
      		<td style="display:none"/> 
      		<td style="display:none"/>
      		<td style="display:none"/> 
      		<td style="display:none"/>
      		<td style="display:none"/> 
      		<td style="display:none"/>
      		<td style="display:none"/> 
      		<td style="display:none"/> 
      		<td style="display:none"/>
      		<td style="display:none"/> 
      		<td style="display:none"/>
      		<td style="display:none"/> 
      		
      	</tr>
			<tr noWrap='true' class='mainHead'>
				
				<td>计划编码</td> 
				<td>计划日期</td> 
				<td>巡检类型</td>
				<td>巡检人员</td> 
				<td>巡检路线</td> 
				<td>部门序号</td> 
				<td>科室名称</td> 
				<td>卡片编码</td> 
				<td>资产名称</td> 
				<td>资产规格</td> 
				<td>供应商</td> 
				<td>资产原值</td> 
				<td>入库日期</td> 
				<td>状态</td> 
			</tr>     
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">
					
					<xsl:choose>
						 
						<xsl:when test="position()=12">
							<td align="right">
								<xsl:value-of select="."/>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="."/></td>
						</xsl:otherwise>
					</xsl:choose>
					
							
				</xsl:for-each>
			</tr>
			</xsl:for-each>
		</tbody>
	</root>
</xsl:template>

</xsl:stylesheet>
