<?xml version="1.0" encoding="GB2312"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap="true" class="mainHead">
				<th style="display:none">
					<input type="checkbox"/>
				</th>
				<th>任务编码</th> 
				<th>任务日期</th> 
				<th style="display:none">巡检类型</th>
				<th>巡检人员</th> 
				<th >巡检线路</th> 
				<th>编制科室</th> 
				<th>制单人</th> 
				<th>审核人</th> 
				<th>状态</th> 
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align="center" style="display:none">
						<input type="checkbox" TABINDEX="-1" style="font-size:8px;">
							<xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">						
						<xsl:choose>
							<xsl:when test="position()=1">
								<td>
								<a href="#">
									 <xsl:attribute name="onclick" >
            					openUpdatepage('<xsl:value-of select="../pk/plan_code"/>')
										</xsl:attribute>
										<xsl:value-of select="."/>
								</a>
								</td>
							</xsl:when>
							 
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
						
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
