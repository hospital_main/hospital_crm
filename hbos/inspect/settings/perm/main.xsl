<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style='display:none'>选择</th>
				<th noWrap="true">职工编码</th>
	      <th noWrap="true">职工名称</th>
	      <th noWrap="true">用户编码</th>
	      <th noWrap="true">用户名称</th>
	      <th noWrap="true">科室</th>
				<!--th noWrap="true">巡检</th-->
				<th noWrap="true">工程师</th>
				<th noWrap="true">组长</th>
				<th noWrap="true">调度中心</th>
     </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
          <td align='center' style='display:none'>
            <div><input type='checkbox'>
	            <xsl:attribute name="id">id_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
        	    </xsl:attribute>
     			  </input></div>
     			</td>

          <xsl:for-each select="td">
            <xsl:choose>
              <!--xsl:when test="position()=1">
              </xsl:when-->
              <!--xsl:when test="position() = 4">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">check_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">readClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when-->
              <xsl:when test="position() = 6 ">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">engineer_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">writeClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 7 ">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">head_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">writeClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:when test="position() = 8">
 		          	<td align='center'><div>
 		          		  <input type='checkbox'>
					            <xsl:attribute name="id">manage_<xsl:value-of select='$CurTrPos'/></xsl:attribute>
					            <xsl:attribute name="onclick">writeClick("<xsl:value-of select='$CurTrPos'/>");</xsl:attribute>
				              <xsl:if test="text() = '1'">
				      			    <xsl:attribute name="checked">1</xsl:attribute>
				      			  </xsl:if>
 		          		  </input>
 		          	</div></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</xsl:template>
</xsl:stylesheet>


