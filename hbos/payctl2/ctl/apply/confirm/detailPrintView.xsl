<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
	  	<thead>
		  	<tr>
		   		<td style="fontsize:maintitle;colspan:13;"></td>
					<td style='display:none;'/>
					<td style='display:none;'/>
					<td style='display:none;'/>
					<td style='display:none;'/>
					<td style='display:none;'/>
					<td style='display:none;'/>
					<td style='display:none;'/>
					<td style='display:none;'/>
					<td style='display:none;'/>
					<td style='display:none;'/>
					<td style='display:none;'/>
					<td style='display:none;'/>
				</tr>
		  	<tr>
		   		<td style="fontsize:subtitle;colspan:colcount;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
		  	<tr>
		   		<td align="left" colspan="2">部   门：</td>
					<td style='display:none'/>
					<td align="left" colspan="4"/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td align="left">申请人：</td>
					<td align="left" colspan="3"/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td align="left" colspan="3"/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
		  	<tr>
		   		<td align="left" style="colspan:2;">说   明：</td>
					<td style='display:none'/>
		   		<td align="left" style="colspan:11;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
		  	<tr>
		   		<td style="colspan:13;fontsize:30;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
	  		<tr noWrap="true" class="mainHead">
					<td align="center" style="fontsize:coltitle;colspan:4">申请情况</td>
					<td align="center" style="fontsize:coltitle;display:none"></td>
					<td align="center" style="fontsize:coltitle;display:none"></td>
					<td align="center" style="fontsize:coltitle;display:none"></td>
					<td align="center" style="fontsize:coltitle;"></td>
					<td align="center" style="fontsize:coltitle;colspan:4">预算情况</td>
					<td align="center" style="fontsize:coltitle;display:none"></td>
					<td align="center" style="fontsize:coltitle;display:none"></td>
					<td align="center" style="fontsize:coltitle;display:none"></td>
					<td align="center" style="fontsize:coltitle;colspan:3">在途情况</td>
					<td align="center" style="fontsize:coltitle;display:none"></td>
					<td align="center" style="fontsize:coltitle;display:none"></td>
					<td align="center" style="fontsize:coltitle;rowspan:2">可用预算</td>
	  		</tr>  	
	  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;width:40;">支出项目</td>
					<td style="fontsize:coltitle;width:40;">申请金额</td>
					<td style="fontsize:coltitle;width:40;">可用额度</td>
					<td style="fontsize:coltitle;width:40;">是否超额度</td>
					<td align="center" style="fontsize:coltitle;width:40;"></td>
					<td style="fontsize:coltitle;width:40;">年初预算</td>
					<td style="fontsize:coltitle;width:40;">当前预算</td>
					<td style="fontsize:coltitle;width:40;">执行额</td>
					<td style="fontsize:coltitle;width:40;">预算结余</td>
					<td style="fontsize:coltitle;width:40;">已申请未审核</td>
					<td style="fontsize:coltitle;width:40;">已申请未报销</td>
					<td style="fontsize:coltitle;width:40;">已制单未报销</td>
					<td align="center" style="fontsize:coltitle;display:none;width:40;"></td>
	  		</tr>  	
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
		      <tr>
		          <xsl:for-each select="td">
		            <xsl:choose>
		              <xsl:when test="position()=1 or position()=4 or position()=5">
  			            <td><xsl:value-of select="."/></td>
			            </xsl:when>
			            <xsl:otherwise>
  			            <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
			            </xsl:otherwise>
			          </xsl:choose>
		  			  </xsl:for-each>
		  			</tr>
	   		</xsl:for-each>
	 		</tbody>
	 		<tfoot>
		  	<tr>
		   		<td align="left" >合计金额(小写)</td>
					<td style="colspan:2;" align="right" valign="center"/>
					<td style='display:none'/>
		   		<td align="left" style="colspan:3;">合计金额(大写)</td>
					<td style='display:none'/>
					<td style='display:none'/>
		   		<td align="left" style="colspan:7;" valign="center"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
				<tr>
				  <td align="left" style="fontsize:foot;colspan:2"></td>
				  <td style="fontsize:foot;display:none"/>
				  <td align="left" style="fontsize:foot;colspan:2;"></td>
				  <td style="fontsize:foot;display:none"/>
				  <td align="left" style="fontsize:foot;colspan:3"></td>
				  <td style="fontsize:foot;display:none"/>
				  <td style="fontsize:foot;display:none"/>
				  <td align="left" style="fontsize:foot;colspan:2"></td>
				  <td style="fontsize:foot;display:none"/>	
				  <td align="left" style="fontsize:foot;colspan:2"></td>
				  <td style="fontsize:foot;display:none"/>
				  <td align="left" style="fontsize:foot;colspan:2"></td>
				  <td style="fontsize:foot;display:none"/>	
				  </tr>			  
	 		</tfoot>
 		</root>
	</xsl:template>
</xsl:stylesheet>
