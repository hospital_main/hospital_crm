<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
	  	<thead>
		  	<tr>
		   		<td style="fontsize:maintitle;colspan:colcount;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
	  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;">科目编码</td>
					<td style="fontsize:coltitle;">科目名称</td>
					<td style="fontsize:coltitle;">申请金额</td>
					<td style="fontsize:coltitle;">摘要</td>
					<td style="fontsize:coltitle;">单据编码</td>
					<td style="fontsize:coltitle;">单据说明</td>
					<td style="fontsize:coltitle;">制单日期</td>
					<td style="fontsize:coltitle;">审核状态</td>
	  		</tr>  	
	  	</thead>
	  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=3">
                <td align='right' noWrap='true'>
		  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
              </xsl:when> 
              <xsl:otherwise>
                <td align="left"><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<tr>
   			<td align="center" colspan="2">合计</td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),'#,##0.00')"/></td>
   			<td></td>
   			<td></td>
   			<td></td>
   			<td></td>
   			<td></td>
   		</tr>
	 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>