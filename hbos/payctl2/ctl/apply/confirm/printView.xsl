<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
	  	<thead>
		  	<tr>
		   		<td style="fontsize:maintitle;colspan:colcount;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
	  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;" width="180">单据编号</td>
					<td style="fontsize:coltitle;" width="280">申请人</td>
					<td style="fontsize:coltitle;" width="330">科室</td>
					<td style="fontsize:coltitle;" width="330">项目名称</td>
					<td style="fontsize:coltitle;" width="330">说明</td>
					<td style="fontsize:coltitle;" width="330">金额</td>
					<td style="fontsize:coltitle;" width="330">制单日期</td>
					<td style="fontsize:coltitle;" width="330">审核状态</td>
					<td style="fontsize:coltitle;" width="330">审核人</td>
					<td style="fontsize:coltitle;" width="330">审核日期</td>
	  		</tr>  	
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
		            <xsl:choose>
		              <xsl:when test=" position() = 7 ">
		                <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
		              </xsl:when>
		              <xsl:when test=" position() = 2 ">
		               
		              </xsl:when>
		              <xsl:otherwise>
		              	<td><xsl:value-of select="."/></td>
		              </xsl:otherwise>
		          </xsl:choose>          
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>