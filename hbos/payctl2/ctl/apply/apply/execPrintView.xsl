<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
	  	<thead>
		  	<tr>
		   		<td style="fontsize:maintitle;colspan:colcount;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
	  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;" colspan="5">预算情况</td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style="fontsize:coltitle;" colspan="3">在途单据情况</td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style="fontsize:coltitle;" rowspan="2">可用额度</td>
	  		</tr>
	  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;">预算科目</td>
					<td style="fontsize:coltitle;">年初预算</td>
					<td style="fontsize:coltitle;">当前预算</td>
					<td style="fontsize:coltitle;">执行额</td>
					<td style="fontsize:coltitle;">预算结余</td>
					<td style="fontsize:coltitle;">已申请未确认</td>
					<td style="fontsize:coltitle;">已申请未报销</td>
					<td style="fontsize:coltitle;">已制单未报销</td>
					<td style='display:none'/>
	  		</tr>
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
		            <xsl:choose>
		            	<xsl:when test=" position() = 1 ">
		            	</xsl:when>
		              <xsl:when test=" position() = 3 or position() = 2 ">
		                <td align="right"><xsl:value-of select="."/></td>
		              </xsl:when>
		              <xsl:otherwise>
		              	<td><xsl:value-of select="."/></td>
		              </xsl:otherwise>
		          </xsl:choose>          
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>