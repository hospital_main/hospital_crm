<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style="display:none"><input type="checkbox"/></th>
      	<th nowrap='true' valign='middle'>单据编号</th>
      	<th nowrap='true' valign='middle'>申请人</th>
      	<th nowrap='true' valign='middle'>科室</th>
      	<th nowrap='true' valign='middle'>项目名称</th>
      	<th nowrap='true' valign='middle'>说明</th>
      	<th nowrap='true' valign='middle'>金额</th>
      	<th nowrap='true' valign='middle'>制单日期</th>
      	<th nowrap='true' valign='middle'>审核状态</th>
      	<th nowrap='true' valign='middle'>审核人</th>
      	<th nowrap='true' valign='middle'>审核日期</th>
      	<th nowrap='true' valign='middle'>是否有效</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<td align='center'  style='display:none'>
	          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	            <xsl:attribute name="value" >
	              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      	    </xsl:attribute>
	    	  	</input>
          </td>
          <xsl:for-each select="td">
	          <xsl:choose>
              <xsl:when test="position()=1">
                <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
              </xsl:when>
              
               <xsl:when test="position()=2">
               	<td style="display:none"><xsl:value-of select="."/></td>
				</xsl:when>
			
							<xsl:when test=" position() = 7 ">
								<td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
	          </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

