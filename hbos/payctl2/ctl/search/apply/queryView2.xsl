<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/payctl2/ctl/search/apply/queryView2.xsl,v 1.1 2012/03/12 01:55:15 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:55:15 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<table>
		<thead>
			<tr>
				<td style="colspan:10;fontsize:maintitle">职工信息查询</td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
				<td style="display:none"></td>
			</tr>
			<tr>
				<td style='fontsize:coltitle'>单据编号</td>
				<td nowrap='true' style='fontsize:coltitle'>申请人</td>
				<td nowrap='true' style='fontsize:coltitle'>科室</td>
				<td nowrap='true' style='fontsize:coltitle'>摘要</td>
				<td nowrap='true' style='fontsize:coltitle'>金额</td>
				<td nowrap='true' style='fontsize:coltitle'>制单日期</td>
				<td nowrap='true' style='fontsize:coltitle'>支付状态</td>
				<td nowrap='true' style='fontsize:coltitle'>支付审核人</td>
				<td nowrap='true' style='fontsize:coltitle'>支付日期</td>
				<td nowrap='true' style='fontsize:coltitle'>报销单序号</td>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td"> 
						<xsl:choose>
							<xsl:when test="position()=3 or position()=4">
								<td ><xsl:value-of select="."/></td>
							</xsl:when>
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</tbody>
		</table>
	</xsl:template>
</xsl:stylesheet>
