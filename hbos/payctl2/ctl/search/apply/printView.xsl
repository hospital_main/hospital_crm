<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
  	<thead>
	  	<tr>
	   		<td style="fontsize:maintitle;colspan:colcount;"></td>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
				<td style="display:none"/>
			</tr>
			<tr>
					<td>单据编号</td>
					<td> 申请人</td>
					<td> 科室</td>
					<td> 项目名称</td>
					<td> 说明</td>
					<td> 申请金额</td>
					<td> 实际报销额</td>
					<td> 超额</td>
					<td> 会计期间</td>
					<td> 制单人</td>
					<td> 制单日期</td>
					<td> 审核状态 </td>
					<td>审核人</td>
					<td> 审核日期 </td>
			</tr>
			
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
	            
	                <td><xsl:value-of select="."/></td>
	                
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>