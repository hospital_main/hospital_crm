<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="month_Cols" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1] and td[2]=/root/tbody/tr[1]/td[2]])"/>
		<tr>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" rowspan="2">选择</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="2">科目编码</td>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="2">科目名称</td>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $month_Cols]">
			  <td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" colspan="3">
			  	<xsl:if test="td[3]='00'">
						年度预算
					</xsl:if>
			  	<xsl:if test="td[3]!='00'">
						<xsl:value-of select="td[3]"/>月
					</xsl:if>
				</td>
			</xsl:for-each>
		</tr>
		<tr>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $month_Cols]">
				<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >预算金额</td>
				<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >实际执行</td>
				<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" >执行进度%</td>
			</xsl:for-each>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:if test="( position() mod $month_Cols )=1 or $month_Cols = 1">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;&lt;td&gt;&lt;input type="radio" name="lineCharRadio" onclick="selectLine(this)"/&gt;&lt;/td&gt;
				</xsl:text>
				<td>
					<xsl:value-of select="td[1]"/>
				</td>
				<td>
					<xsl:value-of select="td[2]"/>
				</td>
			</xsl:if>
			
			<xsl:if test="(position() mod $month_Cols)!=0">
				<td align="right">
					<xsl:value-of select="format-number(td[4],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[5],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[6],'#,##0.00')"/>
				</td>
			</xsl:if>
			
			<xsl:if test="(position() mod $month_Cols)=0 or $month_Cols = 1">
				<td align="right">
					<xsl:value-of select="format-number(td[4],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[5],'#,##0.00')"/>
				</td>
				<td align="right">
					<xsl:value-of select="format-number(td[6],'#,##0.00')"/>
				</td>
				<xsl:text disable-output-escaping="yes">
					&lt;/tr&gt;
				</xsl:text>
			</xsl:if>
			
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>