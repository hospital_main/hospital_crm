<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<thead>
		<xsl:variable name="cols" select="string-length(/root/tbody/tr[1]/td[1])"/>
		<tr>
			<td style="background-color:#FCB724;font-weight: bold;font-family:'宋体'" align="center" rowspan="2">选择</td>
			<td class="mainHead" align="center" rowspan="2">科目编码</td>
			<td class="mainHead" align="center" rowspan="2">科目名称</td>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 3]">
			  <td class="mainHead" align="center">
			  	<xsl:if test=".!='' and .='年度预算'">
					<xsl:attribute name="colspan">3</xsl:attribute>
				</xsl:if>
			  	<xsl:if test=".!='' and .!='年度预算'">
					<xsl:attribute name="colspan"><xsl:value-of select="$cols"/></xsl:attribute>
				</xsl:if>
			  	<xsl:if test=".=''">
					<xsl:attribute name="style">display:none</xsl:attribute>
				</xsl:if>
					<xsl:value-of select="."/>
				</td>
			</xsl:for-each>
		</tr>
		<tr>
			<td style="display:none"/>
			<td style="display:none"/>
			<td style="display:none"/>
			<xsl:for-each select="/root/tbody/tr[2]/td[position() &gt; 3]">
			  <td class="mainHead" align="center">
				<xsl:value-of select="."/>
				</td>
			</xsl:for-each>
		</tr>
	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr[ position() &gt; 2]">
			<tr>
			<td><input type="radio" name="clesda" onclick="selectLine(this)"><xsl:attribute name="showcol"><xsl:value-of select="$cols"/></xsl:attribute></input></td>
			<xsl:for-each select="td[position()!=1]">
				<xsl:choose>
					<xsl:when test="position()=1 or position()=2">
						<td><xsl:value-of select="."/></td>
					</xsl:when>
					<xsl:otherwise>
						<td align="right"><xsl:value-of select="format-number(.,'##,##0.00')"/></td>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>