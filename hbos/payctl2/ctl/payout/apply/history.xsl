<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th>科目编码</th>
        <th>科目名称</th>
        <th>报销金额</th>
        <th>单据编码</th>
        <th>单据说明</th>
        <th>制单日期</th>
        <th>支付状态</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=3">
                <td align='right' noWrap='true'>
		  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
              </xsl:when> 
              <xsl:otherwise>
                <td align="left"><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<tr>
   			<td align="center" colspan="2">合计</td>
   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),'#,##0.00')"/></td>
   			<td></td>
   			<td></td>
   			<td></td>
   			<td></td>
   		</tr>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>