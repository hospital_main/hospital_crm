//报表注册,初始化，计算
function cellConfMapObj(re,acctY,isOld,mod_type){
	this.confMap=new Object();
	this.inputBoxConf=new Object();
	this.rep_code=re;
	this.accY=acctY;
	this.isOld=isOld;
	this.mod_flag=mod_type;
	this.proposer="";
	this.dept_code="";
	this.dept_name=""
	this.date_col;//日期单元格所在的列
	this.date_row;//日期单元格所在的行
	this.dept_row;//科室单元格所在的行
	this.singleAcctBudgCode_row;//单科目中会计科目所在的行
	this.confBudgCodeMap=new Object();//需要废弃的，多余的对象
	this.singleRowSub;//单科目中报销金额所在的行
	this.singleRowMap=new Object();
	this.rowMap2=new Object();
	
	this.cell_init=function(){
		this.confMap_init();
		this.cellConf_init();
		this.acctBudgRefresh();
		
	}
	
		
	this.selComBox=function(flag,row,col,c,v){//c:code ,v:value,flag：标志下拉框类型,'02'表示申请人，需要对科室进行联动;'21'表示预算科目
		
		CellWeb1.SetCellNote(col,row,0,c);
		CellWeb1.SetCellString(col,row,0,v);
		if(flag=="02"){
			this.proposer=c;
			if(this.confMap["05____"+this.dept_row]!=null){
				var ro=this.confMap["05____"+this.dept_row][0];
				var co=this.confMap["05____"+this.dept_row][1];
				CellWeb1.SetCellNote(co,ro,0,this.getDeptCode(c))
				CellWeb1.SetCellString(co,ro,0,this.getDeptName(c));
				this.dept_code=CellWeb1.GetCellNote(co,ro,0);
				this.dept_name=CellWeb1.GetCellString2(co,ro,0);
				if(this.dept_code==""){
						alert("该职工所在科室无预算，编制预算后才能报销!");
				}
				for(var key in this.confBudgCodeMap){
						var row=key.split("_")[1];
						var col=key.split("_")[2];
						cc=this;
						tt=row;
						ct=col;
						var v=c="";
						this.confBudgCodeMap[key]=new CellComboBoxCtl(CellWeb1,"payctl2_subj_code_py","<acct_year>"+getAcctYear()+"</acct_year><dept_code>"+this.dept_code+"</dept_code>",false,function(co,va){cc.selComBox('21',tt,ct,co,va)},function(){cc.endselComBox()},"|||");
						this.inputBoxConf[key]=this.confBudgCodeMap[key];
						CellWeb1.SetCellNote(col,row,0,"")
						CellWeb1.SetCellString(col,row,0,"");
				}
			}
		}
		/*
		else{
			if(this.mod_flag!='1'){
				this.comfMap["22__"+c+"__"+row]=this.comfMap["22____"+row]
			}
		}
		*/
		return true;
	}
	
	this.endselComBox=function(){
		//隐藏之后回调
	
	}
	
	this.get_cellConf=function(){
		window.xmlhttp.post("payctl2_getCtrlDefDetail","<report_code>"+this.rep_code+"</report_code>","");
		var doc = window.xmlhttp._object.responseText;
		var dom=new ActiveXObject("Microsoft.XMLDOM");
		dom.loadXML(doc);
		return dom;
	}
	
	this.confMap_init=function(){
		var d=this.get_cellConf();
		var tbody=d.getElementsByTagName("tbody")[0];
			var trs=tbody.getElementsByTagName("tr");
			for(var i=0;i<trs.length;i++){
				var tds=trs[i].getElementsByTagName("td");
				//tkey:type,科目编码,行,列,数据类型
				var tkey=tds[0].text+'__'+tds[4].text+'__'+tds[1].text;
				this.confMap[tkey]=new Array();
				if(this.mod_flag=="1"){
					this.singleRowMap[tds[0].text+'__'+tds[4].text]=tds[1].text;					
				}
				for(var j=1;j<tds.length;j++){
					this.confMap[tkey][j-1]=tds[j].text;
				}
				if(tds[0].text=="22"){
					this.mapInit(tds);
					this.confMap_bakConf(tds);
					if(this.mod_flag=="1"){
						this.singleRowSub=tds[1].text;
					}
				}
				
				if(!this.isOld){
					CellWeb1.SetCellNote(parseInt(tds[2].text),parseInt(tds[1].text),0,"");
				}
			}
	}
	
	this.mapInit=function(tdObj){
		this.rowMap2[tdObj[4].text]=tdObj[1].text;
	}
	
	this.confMap_bakConf=function(tdObj){
		this.confMap["24__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"]=new Array();
		this.confMap["24__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][0]=tdObj[1].text;
		this.confMap["24__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][1]=tdObj[2].text;
		this.confMap["24__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][2]=tdObj[3].text;
		this.confMap["24__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][3]=tdObj[4].text;
		this.confMap["25__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"]=new Array();
		this.confMap["25__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][0]=tdObj[1].text;
		this.confMap["25__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][1]=tdObj[2].text;
		this.confMap["25__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][2]=tdObj[3].text;
		this.confMap["25__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][3]=tdObj[4].text;
		this.confMap["27__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"]=new Array();
		this.confMap["27__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][0]=tdObj[1].text;
		this.confMap["27__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][1]=tdObj[2].text;
		this.confMap["27__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][2]=tdObj[3].text;
		this.confMap["27__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][3]=tdObj[4].text;
		this.confMap["30__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"]=new Array();
		this.confMap["30__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][0]=tdObj[1].text;
		this.confMap["30__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][1]=tdObj[2].text;
		this.confMap["30__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][2]=tdObj[3].text;
		this.confMap["30__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][3]=tdObj[4].text;
		this.confMap["31__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"]=new Array();
		this.confMap["31__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][0]=tdObj[1].text;
		this.confMap["31__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][1]=tdObj[2].text;
		this.confMap["31__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][2]=tdObj[3].text;
		this.confMap["31__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][3]=tdObj[4].text;
		this.confMap["32__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"]=new Array();
		this.confMap["32__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][0]=tdObj[1].text;
		this.confMap["32__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][1]=tdObj[2].text;
		this.confMap["32__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][2]=tdObj[3].text;
		this.confMap["32__"+tdObj[4].text+"__"+tdObj[1].text+"__sub"][3]=tdObj[4].text;
	}
	
	this.acctBudgRefresh=function(){
		for(var key in this.inputBoxConf){
			var keyFun=key.split("_")[0];
			var row=key.split("_")[1];
			var col=key.split("_")[2];
			if(keyFun=="21"){
				cc=this;
				tt=row;
				ct=col;
				var c=v="";
				this.inputBoxConf[key]=new CellComboBoxCtl(CellWeb1,"payctl2_subj_code_py","<acct_year>"+getAcctYear()+"</acct_year><dept_code>"+this.dept_code+"</dept_code>",false,function(co,va){cc.selComBox('21',tt,ct,co,va)},function(){cc.endselComBox()},c+"|||"+v);
				this.confBudgCodeMap[key]=this.inputBoxConf[key];
			}
		}
	}
	
	this.cellConf_init=function(){
		for(var keyFun in this.confMap){
			var key=keyFun.split("__")[0];
			var row=parseInt(this.confMap[keyFun][0]);
			var col=parseInt(this.confMap[keyFun][1]);
			var data_type=parseInt(this.confMap[keyFun][2]);
			var acct_budg_code=this.confMap[keyFun][3];
			if(key=="01"){
				//单据编号
				CellWeb1.SetCellInput(col,row,0,5);//单元格设置为只读
				CellWeb1.SetCellNumType(col,row,0,data_type);//设置单元格格式	
				var v="";			
				if(!this.isOld){
					v=this.getDeptCodeAlll()[2];//this.getBillNum()
					CellWeb1.SetCellString(col,row,0,v);
				}else{
					v=CellWeb1.GetCellString2(col,row,0);
				}
				this.setReportCode(v);
			}
			if(key=="02"){
				//申请人
				CellWeb1.SetCellNumType(col,row,0,data_type);//设置单元格格式			
				cs=this;
				rr=row;
				cr=col;
				var v,c;
				//添加页面设置成当前登录人			
				if(!this.isOld){
					v=this.getDeptCodeAlll()[4];//this.getOperName();
					c=this.getDeptCodeAlll()[3];//this.getOperCode();
					this.proposer=c;
					CellWeb1.SetCellNote(col,row,0,c);
					CellWeb1.SetCellString(col,row,0,v);					
				}else{
					v=CellWeb1.GetCellString2(col,row,0);
					c=CellWeb1.GetCellNote(col,row,0);
					this.proposer=c;
				}
				this.inputBoxConf[key+"_"+row+"_"+col]=new CellComboBoxCtl(CellWeb1,"payctl2_ctlSearchApply_comp_py","",false,function(co,va){cs.selComBox('02',rr,cr,co,va)},function(){cs.endselComBox()},c+"|||"+v);
			}
			if(key=="03" || key=="04"){
				//制单人/审核人
				CellWeb1.SetCellInput(col,row,0,5);//单元格设置为只读
				CellWeb1.SetCellNumType(col,row,0,data_type);//设置单元格格式		
				if(!this.isOld){
					var v=this.getDeptCodeAlll()[4];//this.getOperName();
					var c=this.getDeptCodeAlll()[3];//this.getOperCode();
					CellWeb1.SetCellNote(col,row,0,c);
					CellWeb1.SetCellString(col,row,0,v);
				}
			}
			if(key=="05"){
				//科室
				this.dept_row=row;
				CellWeb1.SetCellInput(col,row,0,5);//单元格设置为只读
				CellWeb1.SetCellNumType(col,row,0,data_type);//设置单元格格式
				if(!this.isOld){
					//添加页面:获取当前登录人所在的科室名称和科室编码
					var v=this.getDeptCodeAlll()[1];//this.getDeptName(this.getOperCode());					
					var c=this.getDeptCodeAlll()[0];//this.getDeptCode(this.getOperCode());
					if(v==""){
						alert("该职工所在科室无预算，编制预算后才能报销!");
					}
					this.dept_code=c;
					this.dept_name=v;
					CellWeb1.SetCellNote(col,row,0,c)
					CellWeb1.SetCellString(col,row,0,v);
				}else{
					this.dept_code=CellWeb1.GetCellNote(col,row,0);
					this.dept_name=CellWeb1.GetCellString2(col,row,0);
				}
			}
			if(key=="07"){
				//制单日期
				CellWeb1.SetCellNumType(col,row,0,data_type);//设置单元格格式
				this.date_col=col;
				this.date_row=row;
				if(!this.isOld){
					var v=this.getCurrentDate();
					CellWeb1.SetCellString(col,row,0,v);
				}
			}
			if(key=="08"){
				//支付日期
				CellWeb1.SetCellInput(col,row,0,5);//单元格设置为只读
				CellWeb1.SetCellNumType(col,row,0,data_type);//设置单元格格式
				if(!this.isOld){
					var v=this.getCurrentDate();
					CellWeb1.SetCellString(col,row,0,v);
				}
			}
			if(key=="09"){
				//报销说明
				CellWeb1.SetCellNumType(col,row,0,data_type);//设置单元格格式
			}
			if(key=="21"){
				//预算科目
				this.singleAcctBudgCode_row=row;
				CellWeb1.SetCellNumType(col,row,0,data_type);//设置单元格格式
				cc=this;
				tt=row;
				ct=col;
				var v=c="";
				if(this.isOld){
					v=CellWeb1.GetCellString2(col,row,0);
					c=CellWeb1.GetCellNote(col,row,0);
				}
				this.inputBoxConf[key+"_"+row+"_"+col]="";//new CellComboBoxCtl(CellWeb1,"payctl2_subj_code_py","<acct_year>"+getAcctYear()+"</acct_year><dept_code></dept_code>",false,function(co,va){cc.selComBox('21',tt,ct,co,va)},function(){cc.endselComBox()},c+"|||"+v);
				this.confBudgCodeMap[key+"_"+row+"_"+col]=this.inputBoxConf[key+"_"+row+"_"+col];
			}
		}
	}
	this.getDeptCodeAlllArray=null;//deptCode,deptName,BilNum,operCode,operName
	this.getDeptCodeAlll=function(){
		var dept_code="";
		if(this.getDeptCodeAlllArray==null){
			this.getDeptCodeAlllArray=[];
			window.xmlhttp.post("payctl2_getDeptCodeAll","<report_code>"+this.rep_code+"</report_code>","?load=_load");
			var tds=window.xmlhttp._object.responseXML.getElementsByTagName("td");
			for(var i=0;i<tds.length;i++){
				this.getDeptCodeAlllArray.push(tds[i].text);
			}
		}
		return this.getDeptCodeAlllArray;
	}
	this.getDeptCode=function(emp_code){
		var dept_code="";
		window.xmlhttp.post("payctl2_getDeptCode","<emp_code>"+emp_code+"</emp_code>","");
		var doc = window.xmlhttp._object.responseText;
		if(doc.search(/<td>/)!=-1){
			var deptCode=doc.substring(doc.search(/<td>/)+"<td>".length,doc.search(/<\/td>/));
			if(deptCode!=""){
				dept_code=deptCode;
			}
		}
		
		return dept_code;
	}
	
	this.getDeptName=function(emp_code){
		var dept_name="";
		window.xmlhttp.post("payctl2_getDeptName","<emp_code>"+emp_code+"</emp_code>","");
		var doc = window.xmlhttp._object.responseText;
		if(doc.search(/<td>/)!=-1){
			var deptName=doc.substring(doc.search(/<td>/)+"<td>".length,doc.search(/<\/td>/));
			if(deptName!=""){
				dept_name=deptName;
			}
		}
		return dept_name;
	}
	
	this.getBillNum=function(){
		var num="000001";
		window.xmlhttp.post("payctl2_getBillNum","<report_code>"+this.rep_code+"</report_code>","");
		var doc = window.xmlhttp._object.responseText;
		var previousNumStr=doc.substring(doc.search(/<td>/)+"<td>".length,doc.search(/<\/td>/));
		if(previousNumStr!=""){
			var previousNum=parseInt(previousNumStr.substring(4,previousNumStr.length).replace(/0{1,}/,""))+1;
			num=""+previousNum;
			
		}
		num="0000000"+num;
		return this.accY+num.substring(num.length-6);
	}
	
	this.getOperCode=function(){
		var oper="";
		window.xmlhttp.post("payctl2_getOperCode","","");
		var doc = window.xmlhttp._object.responseText;
		var operCode=doc.substring(doc.search(/<td>/)+"<td>".length,doc.search(/<\/td>/));
		if(operCode!=""){
			oper=operCode;
		}
		return oper;
	}
	
	this.getOperName=function(){
		var oper="";
		window.xmlhttp.post("payctl2_getOperName","","");
		var doc = window.xmlhttp._object.responseText;
		var operName=doc.substring(doc.search(/<td>/)+"<td>".length,doc.search(/<\/td>/));
		if(operName!=""){
			oper=operName;
		}
		return oper;
	}
	
	this.getCurrentDate=function(){
		var myDate=new Date();
		var year=myDate.getFullYear();  //定义year属性，年份，默认值为当前系统年份。
		var month=myDate.getMonth()+1;  //定义month属性，月份，默认值为当前系统月份。
		var day=myDate.getDate();  //定义date属性，日，默认值为当前系统的日。
		year='0000'+year;year=year.substring(year.length-4)
	  month='00'+month;month=month.substring(month.length-2)
	  day='00'+day; day=day.substring(day.length-2)
	  //return year+'-'+month+'-'+day+' '+myDate.toLocaleTimeString();
	  return year+'-'+month+'-'+day;
	}
	
	this.doCalc=function(){
		var ymd="";		
		if(this.confMap["07____"+this.date_row]!=null){
			ymd=CellWeb1.GetCellString2(parseInt(this.confMap["07____"+this.date_row][1]),parseInt(this.confMap["07____"+this.date_row][0]),0);
		}else{
			ymd=this.getCurrentDate();
		}
		subBody.load="payctl2CtlPayoutApply_getBudgFlag";
		subBody.para="<d>"+ymd+"</d>";
		subBody.post();
		
		var results=subBody.getOneDim();
		var result = results[0];
		if(result!="0"){
			alert(results[1]+"月份已结帐,不能报销");
			return ;
		}
		var mm=results[1];
		this.calcValue_init(mm);
		this.cellValue_calc();
		this.cellValue2_calc();
		return true;
	}
	
	this.cellValue2_calc=function(){
		
		for(var key in  this.confMap){
			var keyFun=key.split("__")[0];
			var row=parseInt(this.confMap[key][0]);
			var col=parseInt(this.confMap[key][1]);
			var data_type=parseInt(this.confMap[key][2]);
			var acct_budg_code="";
			var acct_budg_code_sub="";
			if(this.mod_flag=="2"){
				acct_budg_code_sub=acct_budg_code=this.confMap[key][3];
			}
			if(this.mod_flag=="1"){
				if(this.confMap["21____"+this.singleAcctBudgCode_row]!=null){
					acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+this.singleAcctBudgCode_row][1]),parseInt(this.confMap["21____"+this.singleAcctBudgCode_row][0]),0);
				}
				if(acct_budg_code==""){
					continue;
				}
			}
			
			if(keyFun=="31"){//可用额度
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
				var v=0;
				var flag=this.confMap[key][4];
				var flag1=this.confMap[key][6];
				//alert(key+" : "+flag+" : "+flag1);
				if(this.mod_flag=="1"){
					//alert(this.confMap["24__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4]+" : "+this.confMap["25__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4]+" : "+this.confMap["27__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4]+" : "+this.confMap["30__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4]);
						if(flag==0){
							if(this.confMap["24__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null && this.confMap["25__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null && this.confMap["27__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null && this.confMap["30__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null){
								v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])-parseInt(this.confMap["27__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])+parseInt(this.confMap["30__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4]);
							}
						}else{
							if(flag1!=1){
								if(this.confMap["24__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null && this.confMap["25__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null && this.confMap["27__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null && this.confMap["30__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null){
									v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])-parseInt(this.confMap["27__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])+parseInt(this.confMap["30__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4]);
								}
							}else{
								if(this.confMap["30__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null){
									v=parseInt(this.confMap["30__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4]);
								}
							}
						}
				}else{				
					if(this.mod_flag=="2"){
						if(flag==0){
							v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])-parseInt(this.confMap["27__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])+parseInt(this.confMap["30__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4]);
						}else{
							if(flag1!=1){
								v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])-parseInt(this.confMap["27__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])+parseInt(this.confMap["30__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4]);
							}else{
								v=parseInt(this.confMap["30__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4]);
							}
						}
						}else{
						if(flag==0){
							v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+row+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+row+"__sub"][4])-parseInt(this.confMap["27__"+acct_budg_code_sub+"__"+row+"__sub"][4])+parseInt(this.confMap["30__"+acct_budg_code_sub+"__"+row+"__sub"][4]);
						}else{
							if(flag1!=1){
								if(this.confMap["24__"+acct_budg_code_sub+"__"+row+"__sub"]!=null && this.confMap["25__"+acct_budg_code_sub+"__"+row+"__sub"]!=null && this.confMap["27__"+acct_budg_code_sub+"__"+row+"__sub"]!=null && this.confMap["30__"+acct_budg_code_sub+"__"+row+"__sub"]!=null){
									v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+row+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+row+"__sub"][4])-parseInt(this.confMap["27__"+acct_budg_code_sub+"__"+row+"__sub"][4])+parseInt(this.confMap["30__"+acct_budg_code_sub+"__"+row+"__sub"][4]);
								}
							}else{
								if(this.confMap["30__"+acct_budg_code_sub+"__"+row+"__sub"]!=null){
									v=parseInt(this.confMap["30__"+acct_budg_code_sub+"__"+row+"__sub"][4]);
								}
							}
						}
						}
				}
				this.confMap[key][5]=v;
				if(key.split("__")[3]!="sub"){
					CellWeb1.SetCellString(col,row,0,v);
				}
			}
			
			if(keyFun=="32"){//是否超
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
						//alert(key+" : "+acct_budg_code);
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
				var v=0;
				var v1=0;
				var flag=0;
				if(this.mod_flag=="1"){
					if(this.confMap["31__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null){
						flag=this.confMap["31__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4];
					}
					if(flag=="1"){
						if(this.confMap["31__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null){
							v=parseInt(this.confMap["31__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][5]);
						}
					}else{
						if(this.confMap["24__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null && this.confMap["25__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null && this.confMap["27__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null && this.confMap["30__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null){
							v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])-parseInt(this.confMap["27__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])+parseInt(this.confMap["30__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4]);
						}
					}
					if(this.confMap["22__"+acct_budg_code_sub+"__"+this.singleRowSub]!=null){
						v1=parseInt(CellWeb1.GetCellString2(parseInt(this.confMap["22__"+acct_budg_code_sub+"__"+this.singleRowSub][1]),parseInt(this.confMap["22__"+acct_budg_code_sub+"__"+this.singleRowSub][0]),0));
					}
				}else{				
					if(this.mod_flag=="2"){
						if(this.confMap["31__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"]!=null){
							flag=this.confMap["31__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4];
						}
						if(flag=="1"){
							if(this.confMap["31__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"]!=null){
								v=parseInt(this.confMap["31__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][5]);
							}
						}else{
							if(this.confMap["24__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"]!=null && this.confMap["25__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"]!=null && this.confMap["27__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"]!=null && this.confMap["30__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"]!=null){
								v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])-parseInt(this.confMap["27__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])+parseInt(this.confMap["30__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4]);
							}
						}
						if(this.confMap["22__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]]!=null && this.confMap["22__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]]!=null){
							v1=parseInt(CellWeb1.GetCellString2(parseInt(this.confMap["22__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]][1]),parseInt(this.confMap["22__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]][0]),0));
						}
					}else{
						if(this.confMap["31__"+acct_budg_code_sub+"__"+row+"__sub"]!=null){
							flag=this.confMap["31__"+acct_budg_code_sub+"__"+row+"__sub"][4];
						}
						if(flag=="1"){
							if(this.confMap["31__"+acct_budg_code_sub+"__"+row+"__sub"]!=null){
								v=parseInt(this.confMap["31__"+acct_budg_code_sub+"__"+row+"__sub"][5]);
							}
						}else{
							if(this.confMap["24__"+acct_budg_code_sub+"__"+row+"__sub"]!=null && this.confMap["25__"+acct_budg_code_sub+"__"+row+"__sub"]!=null && this.confMap["27__"+acct_budg_code_sub+"__"+row+"__sub"]!=null && this.confMap["30__"+acct_budg_code_sub+"__"+row+"__sub"]!=null){
								v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+row+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+row+"__sub"][4])-parseInt(this.confMap["27__"+acct_budg_code_sub+"__"+row+"__sub"][4])+parseInt(this.confMap["30__"+acct_budg_code_sub+"__"+row+"__sub"][4]);
							}
						}
						if(this.confMap["22__"+acct_budg_code_sub+"__"+row]!=null && this.confMap["22__"+acct_budg_code_sub+"__"+row]!=null){
							v1=parseInt(CellWeb1.GetCellString2(parseInt(this.confMap["22__"+acct_budg_code_sub+"__"+row][1]),parseInt(this.confMap["22__"+acct_budg_code_sub+"__"+row][0]),0));
						}
					}
				}
				if(isNaN(v1)){
					v1=0;
				}
				//alert(key+" : "+v+" : "+v1);
				if(v>=v1){
					if(key.split("__")[3]!="sub"){
						CellWeb1.SetCellString(col,row,0,"未超");
					}
					this.confMap[key][4]="0";
				}else{
					if(key.split("__")[3]!="sub"){
						CellWeb1.SetCellString(col,row,0,"超额度");
					}
					this.confMap[key][4]="1";
				}
			}
						
		}
	}
	
	this.cellValue_calc=function(){
		for(var key in  this.confMap){
			var keyFun=key.split("__")[0];
			var row=parseInt(this.confMap[key][0]);
			var col=parseInt(this.confMap[key][1]);
			var data_type=parseInt(this.confMap[key][2]);
			var acct_budg_code="";
			var acct_budg_code_sub="";
			if(this.mod_flag=="2"){
				acct_budg_code_sub=acct_budg_code=this.confMap[key][3];
			}
			if(this.mod_flag=="1"){
				if(this.confMap["21____"+this.singleAcctBudgCode_row]!=null){
					acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+this.singleAcctBudgCode_row][1]),parseInt(this.confMap["21____"+this.singleAcctBudgCode_row][0]),0);
				}
				if(acct_budg_code==""){
					continue;
				}
			}
			
						
			if(keyFun=="26"){//预算结余
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
				
				var v=0;
				if(this.mod_flag=="1"){
					if(this.confMap["24__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null && this.confMap["25__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null){
						v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])
						
					}else{
						continue;
					}
				}else{//alert(acct_budg_code_sub+" : "+this.confMap["24__"+acct_budg_code_sub+"__"+row+"__sub"]+" : "+this.confMap["25__"+acct_budg_code_sub+"__"+row+"__sub"]);
					if(this.mod_flag=="2"){
						v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])
					}else{
						if(this.confMap["24__"+acct_budg_code_sub+"__"+row+"__sub"]!=null && this.confMap["25__"+acct_budg_code_sub+"__"+row+"__sub"]!=null){
							v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+row+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+row+"__sub"][4])
						}else{
							continue;
						}
					}
				}
				CellWeb1.SetCellString(col,row,0,v);
			}
			
			if(keyFun=="27"){//预算可用额度
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
				var v=0;
				
				if(this.mod_flag=="1"){
						if(this.confMap["24__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null && this.confMap["25__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"]!=null && this.confMap[key]!=null){
							v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+this.singleRowSub+"__sub"][4])-parseInt(this.confMap[key][4]);
						}else{
							continue;
						}
				}else{
				if(this.mod_flag=="2"){
						v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+this.rowMap2[acct_budg_code_sub]+"__sub"][4])-parseInt(this.confMap[key][4]);
					}else{
						if(this.confMap["24__"+acct_budg_code_sub+"__"+row+"__sub"]!=null && this.confMap["25__"+acct_budg_code_sub+"__"+row+"__sub"]!=null && this.confMap[key]!=null){
							v=parseInt(this.confMap["24__"+acct_budg_code_sub+"__"+row+"__sub"][4])-parseInt(this.confMap["25__"+acct_budg_code_sub+"__"+row+"__sub"][4])-parseInt(this.confMap[key][4]);
						}else{
							continue;
						}
					}
				}
				if(key.split("__")[3]!="sub"){
					CellWeb1.SetCellString(col,row,0,v);
				}
				//CellWeb1.SetCellString(col,row,0,v);
			}
									
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	this.calcValue_init_xml=null;////0 payctl2_getBudgValue,1 payctl2_getBudgValue1,2 payctl2_getBudgExecValue,3 payctl2_getBudgRemainValue,4 payctl2_getBudgApplyValue,5 payctl2_getBudgApplyValue1,5 payctl2_getBudgApplyValue2,7 payctl2_getRemainFlag ,8 payctl2_getApplyType
	this.getBudgAllValue=function(deptCode,acct_budg_code,ymd){
		if(this.calcValue_init_xml[deptCode+"-"+acct_budg_code])
			return this.calcValue_init_xml[deptCode+"-"+acct_budg_code];
		var v=0;
		window.xmlhttp.post("payctl2_getBudgAllValue","<dept_code>"+deptCode+"</dept_code><acct_budg_code>"+acct_budg_code+"</acct_budg_code><ymd>"+ymd+"</ymd><appIds>"+appIds.value+"</appIds><pay_id>"+pay_id.value+"</pay_id>","");
		var tds=window.xmlhttp._object.responseXML.getElementsByTagName("td");
		this.calcValue_init_xml[deptCode+"-"+acct_budg_code]=["","","","","","","","","",""];
		for(var i=0;i<tds.length;i++){
			
			this.calcValue_init_xml[deptCode+"-"+acct_budg_code][i]=tds[i].text;
		}
		return this.calcValue_init_xml[deptCode+"-"+acct_budg_code];
	}
	this.calcValue_init=function(ymd){
		this.calcValue_init_xml={};
		for(var key in this.confMap){
			var keyFun=key.split("__")[0];
			var row=parseInt(this.confMap[key][0]);
			var col=parseInt(this.confMap[key][1]);
			var data_type=parseInt(this.confMap[key][2]);
			var acct_budg_code="";
			var acct_budg_code_sub="";
			if(this.mod_flag=="2"){
				acct_budg_code_sub=acct_budg_code=this.confMap[key][3]
			}
			if(this.mod_flag=="1"){
				if(this.confMap["21____"+this.singleAcctBudgCode_row]!=null){
					acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+this.singleAcctBudgCode_row][1]),parseInt(this.confMap["21____"+this.singleAcctBudgCode_row][0]),0);
				}
				this.confMap[key][3]=acct_budg_code;
				if(acct_budg_code==""){
					continue;
				}
			}
			
			var deptCode="";
			if(this.confMap["05____"+this.dept_row]!=null){
				deptCode=CellWeb1.GetCellNote(parseInt(this.confMap["05____"+this.dept_row][1]),parseInt(this.confMap["05____"+this.dept_row][0]),0);
			}
			
			if(keyFun=="22"){//报销金额 科目赋值
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
			}
			
			if(keyFun=="23"){//年初预算
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
				var v = this.getBudgAllValue(deptCode,acct_budg_code,ymd)[1];//this.getBudgValue(deptCode,acct_budg_code,ymd,'1');
				this.confMap[key][4]=v;
				CellWeb1.SetCellString(col,row,0,v);
			}
			
			if(keyFun=="24"){//预算总额
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
				var v = this.getBudgAllValue(deptCode,acct_budg_code,ymd)[0];//this.getBudgValue(deptCode,acct_budg_code,ymd,'');
				this.confMap[key][4]=v;
				if(key.split("__")[3]!="sub"){
					CellWeb1.SetCellString(col,row,0,v);
				}
			}
			
			if(keyFun=="25"){//已执行额
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
				var v =this.getBudgAllValue(deptCode,acct_budg_code,ymd)[2];// this.getExecValue(deptCode,acct_budg_code,ymd);
				this.confMap[key][4]=v;
				if(key.split("__")[3]!="sub"){
					CellWeb1.SetCellString(col,row,0,v);
				}
			} 
			
			if(keyFun=="27"){//预算可用额度,返回计算所用值
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
				var v = this.getBudgAllValue(deptCode,acct_budg_code,ymd)[3];//this.getBudgRemainValue(deptCode,acct_budg_code,ymd);
				this.confMap[key][4]=v;
				/*
				if(key.split("__")[3]!="sub"){
					CellWeb1.SetCellString(col,row,0,v);
				}
				*/
				//CellWeb1.SetCellString(col,row,0,v);
			}
			
			if(keyFun=="28"){//申请金额
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
				var v = this.getBudgAllValue(deptCode,acct_budg_code,ymd)[4];//this.getBudgApplyValue(deptCode,acct_budg_code,ymd,"0");
				this.confMap[key][4]=v;
				CellWeb1.SetCellString(col,row,0,v);
			}
			
			if(keyFun=="29"){//申请单已报销额
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
				var v = this.getBudgAllValue(deptCode,acct_budg_code,ymd)[5];//this.getBudgApplyValue(deptCode,acct_budg_code,ymd,"1");
				this.confMap[key][4]=v;
				CellWeb1.SetCellString(col,row,0,v);
			}
			
			if(keyFun=="30"){//申请可用额度
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
				var v = this.getBudgAllValue(deptCode,acct_budg_code,ymd)[6];//this.getBudgApplyValue(deptCode,acct_budg_code,ymd,"2");
				this.confMap[key][4]=v;
				if(key.split("__")[3]!="sub"){
					CellWeb1.SetCellString(col,row,0,v);
				}
			}
			
			if(keyFun=="31"){//可用额度,返回标志
				if(this.mod_flag=="3"){
					acct_budg_code="";
					if(this.confMap["21____"+row]!=null){
						acct_budg_code=CellWeb1.GetCellNote(parseInt(this.confMap["21____"+row][1]),parseInt(this.confMap["21____"+row][0]),0)
					}
					this.confMap[key][3]=acct_budg_code;
					if(acct_budg_code==""){
						continue;
					}
				}
				var v =this.getBudgAllValue(deptCode,acct_budg_code,ymd)[7];// this.getRemainFlag(deptCode,acct_budg_code);
				var v1=this.getBudgAllValue(deptCode,acct_budg_code,ymd)[8];//this.getApplyType(deptCode,acct_budg_code);
				this.confMap[key][4]=v;
				this.confMap[key][6]=v1;
				//CellWeb1.SetCellString(col,row,0,v);
			}
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	this.getBudgValue=function(dept_code,acctBudgCode,ymd,flag){
		var v=0;
		window.xmlhttp.post("payctl2_getBudgValue","<dept_code>"+dept_code+"</dept_code><acct_budg_code>"+acctBudgCode+"</acct_budg_code><ymd>"+ymd+"</ymd><flag>"+flag+"</flag>","");
		var doc = window.xmlhttp._object.responseText;
		var budgValue=parseInt(doc.substring(doc.search(/<td>/)+"<td>".length,doc.search(/<\/td>/)));
		if(!isNaN(budgValue)){
			v=budgValue;
		}
		return v;		
	}
		
	this.getExecValue=function(dept_code,acctBudgCode,ymd){
		var v=0;
		window.xmlhttp.post("payctl2_getBudgExecValue","<dept_code>"+dept_code+"</dept_code><acct_budg_code>"+acctBudgCode+"</acct_budg_code><ymd>"+ymd+"</ymd>","");
		var doc = window.xmlhttp._object.responseText;
		var budgValue=parseInt(doc.substring(doc.search(/<td>/)+"<td>".length,doc.search(/<\/td>/)));
		if(!isNaN(budgValue)){
			v=budgValue;
		}
		return v;		
	}
		
	this.getBudgRemainValue=function(dept_code,acctBudgCode,ymd){
		var v=0;
		window.xmlhttp.post("payctl2_getBudgRemainValue","<pay_id>"+pay_id.value+"</pay_id><dept_code>"+dept_code+"</dept_code><acct_budg_code>"+acctBudgCode+"</acct_budg_code><ymd>"+ymd+"</ymd>","");
		var doc = window.xmlhttp._object.responseText;
		var budgValue=parseInt(doc.substring(doc.search(/<td>/)+"<td>".length,doc.search(/<\/td>/)));
		if(!isNaN(budgValue)){
			v=budgValue;
		}
		return v;		
	}
		
	this.getBudgApplyValue=function(dept_code,acctBudgCode,ymd,flag){
		var v=0;
		window.xmlhttp.post("payctl2_getBudgApplyValue","<dept_code>"+dept_code+"</dept_code><acct_budg_code>"+acctBudgCode+"</acct_budg_code><ymd>"+ymd+"</ymd><appIds>"+appIds.value+"</appIds><flag>"+flag+"</flag>","");
		var doc = window.xmlhttp._object.responseText;
		var budgValue=parseInt(doc.substring(doc.search(/<td>/)+"<td>".length,doc.search(/<\/td>/)));
		if(!isNaN(budgValue)){
			v=budgValue;
		}
		return v;		
	}
	
	this.getRemainFlag=function(dept_code,acctBudgCode){
		var flag='0';
		window.xmlhttp.post("payctl2_getRemainFlag","<dept_code>"+dept_code+"</dept_code><acct_budg_code>"+acctBudgCode+"</acct_budg_code><appIds>"+appIds.value+"</appIds>","");
		var doc = window.xmlhttp._object.responseText;
		var tempFlag=doc.substring(doc.search(/<td>/)+"<td>".length,doc.search(/<\/td>/));
		if(tempFlag!=''){
			flag=tempFlag;
		}
		return flag;		
	}
	
	this.getApplyType=function(dept_code,acctBudgCode){
		var flag='0';
		window.xmlhttp.post("payctl2_getApplyType","<dept_code>"+dept_code+"</dept_code><acct_budg_code>"+acctBudgCode+"</acct_budg_code><appIds>"+appIds.value+"</appIds>","");
		var doc = window.xmlhttp._object.responseText;
		var tempFlag=doc.substring(doc.search(/<td>/)+"<td>".length,doc.search(/<\/td>/));
		if(tempFlag!=''){
			flag=tempFlag;
		}
		return flag;		
	}
	
	this.setReportCode=function(code){
		if(typeof(pay_billcode)!="undefined" && typeof(pay_billcode)=="object"){
			pay_billcode.value=code;
		}
	}
	
	this.getProposer=function(){
		return this.proposer;
	}
	
	this.getDeptCodeRes=function(){
		return this.dept_code;
	}
	
	this.getDeptCodeName=function(){
		return this.dept_name;
	}
	
	this.getOperDate=function(){
		return CellWeb1.GetCellString2(this.date_col,this.date_row,0);
	}
	
	this.cell_init();
	
	this.getResultConfArrObj=function(){
		var arr=new Object();
		for(var key in this.confMap){
			var code=this.confMap[key][3];
			var row=this.confMap[key][0];
			var col=this.confMap[key][1];
			var keyType=key.split("__")[0];
			if(code!=""){
				if(arr[code]==null){
					arr[code]=new Array();
					if(keyType=="22"){//报销金额
						//arr[code][0]=this.confMap[key][4];
						arr[code][0]=CellWeb1.GetCellString2(parseInt(this.confMap[key][1]),parseInt(this.confMap[key][0]),0);
					}
					if(keyType=="31" && key.split("__").length==4){//可用额度
						var codeSub=code;
						if(this.mod_flag!="2"){
							codeSub="";
						}
						arr[code][1]=this.confMap["31__"+codeSub+"__"+row+"__sub"][5];
					}
					if(keyType=="32" && key.split("__").length==4){//是否超
						var codeSub=code;
						if(this.mod_flag!="2"){
							codeSub="";
						}
						arr[code][2]=this.confMap["32__"+codeSub+"__"+row+"__sub"][4];
					}
				}else{
					if(keyType=="22"){//报销金额
						//arr[code][0]=this.confMap[key][4];
						var codeSub=code;
						if(this.mod_flag!="2"){
							codeSub="";
						}
						arr[code][0]=CellWeb1.GetCellString2(parseInt(this.confMap["22__"+codeSub+"__"+row][1]),parseInt(this.confMap["22__"+codeSub+"__"+row][0]),0);
					}
					if(keyType=="31" && key.split("__").length==4){//可用额度
						var codeSub=code;
						if(this.mod_flag!="2"){
							codeSub="";
						}
						arr[code][1]=this.confMap[key][5];
					}
					if(keyType=="32" && key.split("__").length==4){//是否超
						var codeSub=code;
						if(this.mod_flag!="2"){
							codeSub="";
						}
						arr[code][2]=this.confMap[key][4];
					}
				}
			}
		}
		return arr;
		//return this.assemble(arr);
	}
	
	this.assemble=function(obj){
		var str="";
		for(var key in obj){
			str+="<recordSub>";
			str+="<acct_budg_code>"+key+"</acct_budg_code>";
			str+="<pay>"+obj[key][0]+"</pay>";
			str+="<ctrl_money>"+obj[key][1]+"</ctrl_money>";
			str+="<is_exce>"+obj[key][2]+"</is_exce>";
			str+="</recordSub>";
		}
		return str;
	}
}