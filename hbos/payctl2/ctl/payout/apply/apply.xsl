<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  			<th><input type='checkbox' onclick="checkInit(this)"/></th>
        <th>是否核销</th>
        <th style="display:none"></th>
        <th>单据编号</th>
        <th>申请人</th>
        <th>科室</th>
        <th>预算项目</th>
        <th>说明</th>
        <th>金额</th>
        <th>可用额度</th>
        <th>制单日期</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
        	<td align='center'>
	          <input type='checkbox' TABINDEX='-1' style='font-size:8px;' onclick="checkSimple(this)">
	            <xsl:attribute name="value" >
	              <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      	    </xsl:attribute>
	    	  	</input>
          </td>
          <td><input type="checkbox" DISABLED="true" /></td>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test=" position()=1 ">
            		<td><a href="#" onclick="ShowDetail(this)"><xsl:value-of select="."/></a></td>
            	</xsl:when> 
            	<xsl:when test=" position()=2 ">
            		<td style="display:none"><xsl:value-of select="."/></td>
            	</xsl:when> 
              <xsl:when test="position()=6 or position()=7">
                <td align='right' noWrap='true'>
		  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
              </xsl:when> 
              <xsl:otherwise>
                <td align="left"><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<xsl:if test="count(/root/tbody/tr) &gt; 1">
	   		<tr>
	   			<td align="center" colspan="6">金额合计</td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/></td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/></td>
	   			<td></td>
	   			<td></td>
	   		</tr>
   		</xsl:if>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>