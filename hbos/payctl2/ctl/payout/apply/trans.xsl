<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <root>
    <xsl:for-each select="//Table/Row">
      <xsl:for-each select="Cell">
        <record row="{../@Index}" col="{@Index}" data="{Data/@code}|||{Data}" type="{Data/@Type}"/>
      </xsl:for-each>
    </xsl:for-each>
  </root>
	</xsl:template>
</xsl:stylesheet>



