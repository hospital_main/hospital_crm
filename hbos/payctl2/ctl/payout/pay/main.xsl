<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th width='25'><input type='checkbox' onclick="checkInit(this)"/></th>
				<th>单据编号</th>
				<th>申请人</th>
				<th>科室</th>
				<th>摘要</th>
				<th>金额</th>
				<th>制单日期</th>
				<th>支付状态</th>
				<th>支付审核人</th>
				<th>支付日期</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  noWrap='true'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true'>
	                  <a tabindex="-2">
			                <xsl:attribute name="href" >
			                  javascript:openDialog('update.html?load=&lt;pay_billcode&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/pay_billcode&gt;&lt;pay_id&gt;<xsl:value-of select="../td[position()=10]"/>&lt;/pay_id&gt;&lt;appIds&gt;<xsl:value-of select="../td[position()=11]"/>&lt;/appIds&gt;&lt;audit&gt;<xsl:value-of select="../td[position()=13]"/>&lt;/audit&gt;&lt;dept_code&gt;<xsl:value-of select="../td[position()=14]"/>&lt;/dept_code&gt;&lt;dept_name&gt;<xsl:value-of select="../td[position()=3]"/>&lt;/dept_name&gt;', 'dialogWidth:1024px;dialogHeight:700px')
			                </xsl:attribute>
	                  	<xsl:value-of select="."/>
	                  </a>
			            </td>
                </xsl:when>
                <xsl:when test="position()=10 or position()=11 or position()=12 or position()=13 or position()=14">
			            <td  noWrap='true' style="display:none">
	                  <xsl:value-of select="."/>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
