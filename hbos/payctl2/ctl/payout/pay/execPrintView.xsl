<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
	  	<thead>
		  	<tr>
		   		<td style="fontsize:maintitle;colspan:colcount;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
	  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;" colspan="2">本次报销情况</td>
					<td style='display:none'/>
					<td style="fontsize:coltitle;" width="180">年度预算</td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style="fontsize:coltitle;" width="180">月度预算</td>
					<td style='display:none'/>
					<td style='display:none'/>
	  		</tr>  	
	  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;" width="180">支出项目</td>
					<td style="fontsize:coltitle;" width="180">报销金额</td>
					<td style="fontsize:coltitle;" width="180">预算总额</td>
					<td style="fontsize:coltitle;" width="330">实际执行</td>
					<td style="fontsize:coltitle;" width="330">预算结余</td>
					<td style="fontsize:coltitle;" width="180">预算总额</td>
					<td style="fontsize:coltitle;" width="180">实际执行</td>
					<td style="fontsize:coltitle;" width="180">预算结余</td>
	  		</tr>  	
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
	            <xsl:choose>
	            	<xsl:when test=" position()=1 ">
	            		<td align='left'><xsl:value-of select="."/></td>
	            	</xsl:when> 
	              <xsl:otherwise>
	                <td align='right' noWrap='true'>
			  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
									</td>
	              </xsl:otherwise>
	            </xsl:choose>   
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	   		<xsl:if test="count(/root/tbody/tr) &gt; 0">
		   		<tr>
		   			<td align="center">合计</td>
		   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[2]),'#,##0.00')"/></td>
		   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),'#,##0.00')"/></td>
		   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[4]),'#,##0.00')"/></td>
		   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00')"/></td>
		   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/></td>
		   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/></td>
		   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/></td>
		   		</tr>
	   		</xsl:if>
	 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>