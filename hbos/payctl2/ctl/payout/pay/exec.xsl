<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th colspan="2">本次报销情况</th>
        <th colspan="3">年度预算</th>
        <th colspan="3">月度预算</th>
  		</tr>
  		<tr noWrap='true' class='mainHead'>
        <th>支出项目</th>
        <th>报销金额</th>
        <th>预算总额</th>
        <th>实际执行</th>
        <th>预算结余</th>
        <th>预算总额</th>
        <th>实际执行</th>
        <th>预算结余</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test=" position()=1 ">
            		<td align="left"><xsl:value-of select="."/></td>
            	</xsl:when> 
              <xsl:otherwise>
                <td align="right"><xsl:value-of select="format-number(.,'#,##0.00')"/></td>
              </xsl:otherwise>
            </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<xsl:if test="count(/root/tbody/tr) &gt; 0">
	   		<tr>
	   			<td align="center">合计</td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[2]),'#,##0.00')"/></td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[3]),'#,##0.00')"/></td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[4]),'#,##0.00')"/></td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[5]),'#,##0.00')"/></td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[6]),'#,##0.00')"/></td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[7]),'#,##0.00')"/></td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/></td>
	   		</tr>
   		</xsl:if>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>