<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th>是否核销</th>
        <th>当前核销状态</th>
        <th>单据编号</th>
        <th>申请人</th>
        <th>科室</th>
        <th>预算项目</th>
        <th>说明</th>
        <th>金额</th>
        <th>制单人</th>
        <th>制单日期</th>
        <th>审核状态</th>
        <th>审核人</th>
        <th>审核日期</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test=" position()=3 ">
            		<td><a tabindex='-1'>
                  <xsl:attribute name="href" >
    	            	javascript:openDialog('../../apply/confirm/update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:724px;dialogHeight:768px', result)
  	          		</xsl:attribute>
  	          		<xsl:value-of select="."/>
  	          	</a></td>
            	</xsl:when> 
              <xsl:when test="position()=8">
                <td align='right' noWrap='true'>
		  						<xsl:value-of select="format-number(.,'#,##0.00')"/>
								</td>
              </xsl:when>  
              <xsl:when test="position()=11">
                <td style="display:none">
		  						<xsl:value-of select="."/>
								</td>
              </xsl:when> 
              <xsl:otherwise>
                <td align="left"><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>          
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
   		<xsl:if test="count(/root/tbody/tr) &gt; 0">
	   		<tr>
	   			<td align="center" colspan="7">金额合计</td>
	   			<td align="right"><xsl:value-of select="format-number(sum(/root/tbody/tr/td[8]),'#,##0.00')"/></td>
	   			<td></td>
	   			<td></td>
	   			<td></td>
	   			<td></td>
	   			<td></td>
	   		</tr>
   		</xsl:if>
 		</tbody>
	</xsl:template>
</xsl:stylesheet>