<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
	  	<thead>
		  	<tr>
		   		<td style="fontsize:maintitle;colspan:colcount;"></td>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
					<td style='display:none'/>
				</tr>
	  		<tr noWrap="true" class="mainHead">
					<td style="fontsize:coltitle;" width="180">科目编码</td>
					<td style="fontsize:coltitle;" width="280">科目名称</td>
					<td style="fontsize:coltitle;" width="330">报销金额</td>
					<td style="fontsize:coltitle;" width="330">单据编码</td>
					<td style="fontsize:coltitle;" width="330">单据说明</td>
					<td style="fontsize:coltitle;" width="330">制单日期</td>
					<td style="fontsize:coltitle;" width="330">支付状态</td>
	  		</tr>  	
	  	</thead>
	  	<tbody>
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>
	          <xsl:for-each select="td">
		            <xsl:choose>
		              <xsl:when test=" position() = 3 ">
		                <td align="right"><xsl:value-of select="."/></td>
		              </xsl:when>
		              <xsl:otherwise>
		              	<td><xsl:value-of select="."/></td>
		              </xsl:otherwise>
		          </xsl:choose>          
	  			  </xsl:for-each>
	  			</tr>
	   		</xsl:for-each>
	 		</tbody>
 		</root>
	</xsl:template>
</xsl:stylesheet>