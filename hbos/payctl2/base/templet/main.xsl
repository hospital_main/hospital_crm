<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th width='25'><input type='checkbox' onclick="checkInit(this)"/></th>
				<th>单据模板编码</th>
				<th>单据模板名称</th>
				<th>模板说明</th>
				<th>单据类型</th>
				<th>是否备份</th>
				<th>是否停用</th>
				<th>模版设置</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  noWrap='true'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=1">
			            <td  noWrap='true'>
	                  <a tabindex="-2">
	                  	<xsl:value-of select="."/>
	                  </a>
			            </td>
                </xsl:when>
                <xsl:when test="position()=5 or position()=6">
                	<td noWrap="true" align="center">
                		<xsl:value-of select="."/>
                	</td>
                </xsl:when>
                <xsl:when test="position()=7 or position()=8 or position()=9">
                	<td noWrap="true" style="display:none">
                		<xsl:value-of select="."/>
                	</td>
                </xsl:when>
                <xsl:when test="position()=10">
			            <td  noWrap='true' align="center">
	                  <a tabindex="-2">
			                <xsl:attribute name="href" >
			                  javascript:openDialog('modConf.html?load=&lt;report_code&gt;<xsl:value-of select="../td[position()=1]"/>&lt;/report_code&gt;&lt;count_mod&gt;<xsl:value-of select="../td[position()=7]"/>&lt;/count_mod&gt;&lt;is_backup&gt;<xsl:value-of select="../td[position()=8]"/>&lt;/is_backup&gt;&lt;is_stop&gt;<xsl:value-of select="../td[position()=9]"/>&lt;/is_stop&gt;', 'dialogWidth:1024px;dialogHeight:700px')
			                </xsl:attribute>
	                  	<xsl:value-of select="."/>
	                  </a>
			            </td>
                </xsl:when>
			  			  <xsl:otherwise>
			            <td  noWrap='true' >
		                  <xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
