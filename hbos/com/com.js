var equiCommon = {

    // 二维码通用
    QcodeModel: function () {
        function i() {

        }
        // 打印二维码
        function qcode(arg) {
            if (arg == null || arg.length == 0) {
                alert('请传入卡片数组');
            }
            else {
                // 组合参数
                var ds = new Object();
                ds.begin = function (error) {

                    var arr = function a() {
                        var xmlText = "";
                        for (var i = 0; i < arg.length; i++) {
                            xmlText += '<record><equi_arch_no>' + arg[i] + '</equi_arch_no></record>';
                        }
                        xmlText = "<sqls><![CDATA[<root>" + xmlText + "</root>]]></sqls>";
                        window.xmlhttp.post('equiBarCodeCreate_data1', xmlText, "?isCheck=false");
                        var xml = xmlhttp._object.responseText.replace(/tr/g, 'b').replace(/td/g, 'a');

                        var t = [];
                        $(xml, 'b').find('a').each(function () {
                            t.push($(this).text().split("|||"));
                        });
                        return t;
                    } ();

                    // 是否需要重新加载
                    var needrefresh = false;
                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i].join('').indexOf('.jpg') == -1) {
                            // 如果没有二维码的话，则再生成一次，然后重新执行
                            saveQcode(arg[i]);
                            needrefresh = true;
                        }
                    }
                    if (needrefresh) {
                        arr = a();
                    }
                    this.temp = arr;
                };

                ds.end = function () {
                    this.temp = null;
                };

                ds.getNextHeadData = function () {
                    if (this.temp == null || this.temp.length == 0)
                        return null;
                    return this.temp.pop();
                };

                ds.getNextRowData = function () {
                    return null;
                };
                //ds.hide=true;
                //ds.autoBegin=true;
                printCellByTemplate("equiArchiveArrangeBarCode_select1", ds);

            }
        }

        // 生成二维码
        function saveQcode(arg) {
            // 1.找到生成项
            window.xmlhttp.post('equipArchiveArrangeCard_item', "<equi_arch_no>" + arg + "</equi_arch_no>", '?isCheck=false');
            var xml = xmlhttp._object.responseText.replace(/tr/g, 'b').replace(/td/g, 'a');
            var qcode_value = '';
            if ($($('<xml>' + xml + '</xml>').find('error').text() == '')) {
                qcode_value = $(xml).text();
            }

            if (qcode_value == null || qcode_value == '') {
                alert('未能加载条码生成项目，请联系系统管理员！');
                return;
            }

            // 2.传入参数生成
            window.xmlhttp.post('equipArchiveArrangeCard_update1', '<item_no>' + arg + '</item_no>'
                + '<barCode>' + qcode_value + '</barCode>'
                + '<barType>QR_CODE</barType>', '?isCheck=false');
            var xml = xmlhttp._object.responseText.replace(/tr/g, 'b').replace(/td/g, 'a');
            if ($($('<xml>' + xml + '</xml>').find('error').text() == '')) {
                return true;
            }
            return false;
        }

        i.prototype.printqcode = qcode;
        i.prototype.saveQcode = saveQcode;
        return new i;
    },

    // 获取选择的checked
    SelectModel: function () {
        function i() {

        }

        function getCheckId(params, index) {
            var arr = [];
            $('#' + params + ' tbody tr').each(function () {
                var input = $(this).find('input:first');
                if ($(input).attr('checked')) {
                    if (index == null || index == 0) {
                        arr.push($(input).val());
                    } else {
                        var tds = $(this).find('td');
                        arr.push($(tds[index]).text());
                    }
                }
            });
            return arr;
        }

        i.prototype.getCheckId = getCheckId,
		i.prototype.G = getCheckId,
         i.prototype.GetCheckId = getCheckId;
        return new i;
    },

    // Post
   XMLHttp: function () {
        this.main = [],this.tdetail=[], this.error = '';
        function i() {

        }

        function post(sqlid, params, right) {
            window.xmlhttp.post(sqlid, params, "?isCheck=" + (right || 'false'))
                , xml = xmlhttp._object.responseText.replace(/tr/g, 'b').replace(/td/g, 'a')
                , tdetail = []
                    , tmain = [];
            $('<xml>' + xml + '</xml>').find('b').each(function () {
                $(this).find('a').each(function () {
                    tdetail.push($(this).text());
                });
                $(this).find('pk').children().each(function () {
                    tdetail.push($(this).text());
                });
                tmain.push(tdetail),
                    tdetail = [];
            });
            return error = $($('<xml>' + xml + '</xml>').find('error')).text(), {
                Succ: error == '',
                Error: error,
                Result: tmain
            };
        }

 return i.prototype.Post = post,
		i.prototype.P=post,
             new i;

    }
	,
	SMS:function(col,pk){
		function i(){
			$.ajaxSetup({ 
				cache: true 
			});			
			window.dest=col;
			window.phonekey=pk;
			var aug="dialogWidth:700px;dialogHeight:500px;overflow:auto;";
			openDialog(window.location.protocol+'//'+window.location.host+'/hbos/com/sms/sendMsgBatch.html',aug,null,'true');
		}
		return new i;
	}
	,
	IMG:function(btnOpen,txtPath,callback){
		$.ajaxSetup({ 
			cache: true 
		});	
		function i(){
			$("[name="+btnOpen.name+"]").click(function(){
				var path= txtPath.value.split(';');
				imgsList=[];//图片数组
				for(var i=0;i<path.length;i++){
					imgsList.push({filename:path[i],filetype:'JPG'});	  
				}
				var aug="dialogWidth:[MAXW];dialogHeight:[MAXH];overflow:auto;";
				openDialog(window.location.protocol+'//'+window.location.host+'/hbos/com/img/img.html',aug,null,'true');
				
				if(callback!=null){					
					callback();
				}
			});
		}
			
		// 上传
		window.uploadpageMult=function(){
			var uploadfiles = openDialog(window.location.protocol+'//'+window.location.host+'/hbos/equip/plan/party/apply/upload.html?load=<btn_insert></btn_insert>', 'dialogWidth:300px;dialogHeight:150px;');
			
			if(uploadfiles==null || uploadfiles.length==0)
				return;
			
			// 全部添加到里面
			imgsList=[];
			$(txtPath.value.split(';')).each(function(i,e){
				imgsList.push({filename:e});
			});
			
			$(uploadfiles.split(';')).each(function(i,e){
				imgsList.push({filename:e});
			});		
			txtPath.value+=';'+uploadfiles;
		},
		window.deleteFile=function(arg){		
			var files=txtPath.value.split(';');
			var temp=[];
			for(var i=0;i<arg.length;i++){			
				temp[arg[i]]=true;
			}
			
			var tempOut=[];
			var delFile=[];
			for(var i=0;i<files.length;i++){
				if(!temp[files[i]]){
					tempOut.push(files[i]);
				}else {
					delFile.push(files[i]);
				}
			}
			
			// 物理删除
			if(vhFile!=null) 
				vhFile.del2(delFile.join(';'));
			
			imgsList=[];
			//if(vhFile!=null) vhFile.del2(delete_path);
			
			txtPath.value=tempOut.join(';');
			if(tempOut && tempOut.length>0){
				$(tempOut).each(function(i,e){
					e && imgsList.push({filename:e});
				});
			}		
		}
		return new i;		
	}
	
},
 Common = equiCommon;