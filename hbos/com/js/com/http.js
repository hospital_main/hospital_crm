define(function(){
	
	/*
	sqlid;
	params;
	right：权限验证
	*/
	var post=function(sqlid, params, right) {
		window.xmlhttp.post(sqlid, params, "?isCheck=" + (right || 'false'))
			, xml = xmlhttp._object.responseText.replace(/tr/g, 'b').replace(/td/g, 'a')
			, tdetail = []
				, tmain = [];
		$('<xml>' + xml + '</xml>').find('b').each(function () {
			$(this).find('a').each(function () {
				tdetail.push($(this).text());
			});
			$(this).find('pk').children().each(function () {
				tdetail.push($(this).text());
			});
			tmain.push(tdetail),
				tdetail = [];
		});
		return error = $($('<xml>' + xml + '</xml>').find('error')).text(), {
			succ: error == '',
			error: error,
			result: tmain
		};
	};
	
	return {
		post:post
	};
});
    
