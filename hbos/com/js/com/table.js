define(function(){
		/*
			params:tablename
			index:
		*/
        var getCheckId=function getCheckId(params, index) {
            var arr = [];
            $('#' + params + ' tbody tr').each(function () {
                var input = $(this).find('input:first');
                if ($(input).attr('checked')) {
                    if (index == null || index == 0) {
                        arr.push($(input).val());
                    } else {
                        var tds = $(this).find('td');
                        arr.push($(tds[index]).text());
                    }
                }
            });
            return arr;
        }
		
		return {
			getcheck:	getCheckId		
		}
    });
    
