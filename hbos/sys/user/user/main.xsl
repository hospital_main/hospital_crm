<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
  	<th nowrap='true'>用户编码</th>
  	<th nowrap='true'>用户名称</th>
  	<th nowrap='true'>用户描述</th>
  	
  	<th nowrap='true'>类别</th>
  	<th nowrap='true'>是否停用</th>
	
	<th nowrap='true'>分配角色</th>	<th nowrap='true'>复制用户权限</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td">
          
              <xsl:choose>
                <xsl:when test="position()=1">
		
		<td>
			<a tabindex='-1'>
                  <xsl:attribute name="href" >
    	            javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:350px;dialogHeight:400px', result)
  	          </xsl:attribute><xsl:value-of select="."/></a>
		  </td>
                </xsl:when>
		
		<xsl:when test="position()=6">
	          <td align="center">		
		        <a href="#">
				<xsl:attribute name="onclick" >
					javascript:openDialog('js_list.html?load=&lt;id&gt;<xsl:value-of select="../pk/*"/>&lt;/id&gt;&lt;user_name&gt;<xsl:value-of select="../td[2]"/>&lt;/user_name&gt;','dialogWidth:700px;dialogHeight:490px',result)
				</xsl:attribute>
					<xsl:value-of select="."/>	
			</a>
								
			</td>						
								
	        </xsl:when>			
		<!-- XTPT17 系统信息-权限管理-用户管理 王羽 2017-03-23 -->					<xsl:when test="position()=7">				<td align="center">							<a href="#">						<xsl:attribute name="onclick" >							javascript:openDialog('copy_perm.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:350px;dialogHeight:200px', result)						</xsl:attribute>												<xsl:value-of select="."/>						</a>								</td>										        </xsl:when>		
			
                <xsl:otherwise> 
		<td>
                  <xsl:value-of select="."/>
		  </td>
                </xsl:otherwise>
              </xsl:choose>
            
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

