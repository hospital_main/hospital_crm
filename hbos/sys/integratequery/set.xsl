<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>��������</th> 
			</tr>     
		</thead>  
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td[2]">   
					<td>
						<a href="#">
							<xsl:attribute name="onclick">loadData("<xsl:value-of select="../td[1]"/>")</xsl:attribute><xsl:value-of select="."/>
						</a>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

