<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
  <xsl:variable name="colNum" select="7"/>
  <xsl:variable name="comName" select="/root/@comName"/>
  <root>
    <colgroup>
 	    <col style='width:26mm'/>
 	    <col style='width:40mm'/>
 	    <col style='width:26mm'/>
 	    <col style='width:26mm'/>
 	    <col style='width:26mm'/>
 	    <col style='width:26mm'/>
 	    <col style='width:26mm'/>
    </colgroup>

    <tr height='35'><td border='none' colspan='{$colNum}' style='text-align:center;'>科室信息字典</td></tr>

    <tr height='22'>
      <td border='none' colspan='{ceiling($colNum div 2)}'>编制单位：<xsl:value-of select='$comName'/></td>
    </tr>

    <tr height='25'>
      <th>科室编码</th>
      <th>科室名称</th>
      <th>上级科室</th>
      <th>科室属性</th>
      <th>科室类别</th>
      <th>会计类别</th>
      <th>停用标志</th>
    </tr>

    <xsl:for-each select='//tbody/tr'>
      <tr height='22'>
        <xsl:for-each select='td'>
          <xsl:if test='position() &lt;= 7'>
            <td colspan='{@colspan}' rowspan='{@rowspan}'>
              <xsl:value-of select="."/>
            </td>
          </xsl:if>
        </xsl:for-each>
      </tr>
    </xsl:for-each>
  </root>
  </xsl:template>
</xsl:stylesheet>