<!--
 $Header: /cvsroot/H-ERP3.1.2ALL/H-ERP3.1.2_ALL/ROOT/hbos/sys/info/sysinfodata/main.xsl,v 1.1 2012/03/12 01:56:21 zhoulidong Exp $
 $Author: zhoulidong $
 $Date: 2012/03/12 01:56:21 $
 $Revision: 1.1 $
-->
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th>信息编码</th>
				<th>信息名称</th>
				<th>信息数值</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  style='display:none'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
          </td>
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
                <xsl:when test="position()=3">
                  <xsl:attribute name="value" >&lt;<xsl:value-of select="name()"/><xsl:value-of select="position()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/><xsl:value-of select="position()"/>&gt;</xsl:attribute>
                  <xsl:attribute name="align">right</xsl:attribute>
                    <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
