<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format NaN=' '/>
  <xsl:template match="/">
  <xsl:variable name="colNum" select="9"/>
  <xsl:variable name="comName" select="/root/@comName"/>
  <root>
    <colgroup>
 	    <col style='width:26mm'/>
 	    <col style='width:26mm'/>
 	    <col style='width:26mm'/>
 	    <col style='width:26mm'/>
 	    <col style='width:26mm'/>
 	    <col style='width:26mm'/>
 	    <col style='width:26mm'/>
 	    <col style='width:26mm'/>
 	    <col style='width:26mm'/>
    </colgroup>

    <tr height='35'><td border='none' colspan='{$colNum}' style='text-align:center;'>人员信息字典</td></tr>

    <tr height='22'>
      <td border='none' colspan='{ceiling($colNum div 2)}'>编制单位：<xsl:value-of select='$comName'/></td>
    </tr>

    <tr height='25'>
      <th>职工编码</th>
      <th>职工名称</th>
      <th>性别</th>
      <th>科室</th>
      <th>职称</th>
      <th>职务</th>
      <th>学历</th>
      <th>人员类别</th>
      <th>停用标志</th>
    </tr>

    <xsl:for-each select='//tbody/tr'>
      <tr height='22'>
        <xsl:for-each select='td'>
          <xsl:if test='position() &lt;= 9'>
            <td colspan='{@colspan}' rowspan='{@rowspan}'>
              <xsl:value-of select="."/>
            </td>
          </xsl:if>
        </xsl:for-each>
      </tr>
    </xsl:for-each>
  </root>
  </xsl:template>
</xsl:stylesheet>