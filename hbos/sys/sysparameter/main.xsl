<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        
				<th noWrap="true" >编号</th>
	      <th noWrap="true">名称</th>
	      <th noWrap="true">参数值</th>
				<th noWrap="true">选项</th>
				<th noWrap="true">说明</th>
     </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>     			
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
              	<td nowrap="true">
              		<xsl:attribute name="value" >
                     <xsl:for-each select="../*">
                      <xsl:choose>
                        <xsl:when test="position()=1 ">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:when>
                      </xsl:choose>
                    </xsl:for-each>
            		  </xsl:attribute>
              		<xsl:value-of select="."/>
              	</td>
								
              </xsl:when>

              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</xsl:template>
</xsl:stylesheet>


