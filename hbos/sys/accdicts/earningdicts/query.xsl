<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th nowrap='true'>单位编码</th>
				<th nowrap='true'>单位名称</th>
				<th nowrap='true'>地址</th>
				<th nowrap='true'>Email</th>
				<th nowrap='true'>联系电话</th>
				<th nowrap='true'>联系人</th>
				<th nowrap='true'>税务证号</th>
				<th nowrap='true'>单位领导</th>
				<th nowrap='true'>主管会计</th>
			</tr>              
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 1">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

