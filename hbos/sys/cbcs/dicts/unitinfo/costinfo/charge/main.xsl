<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
			<th width='25'><input type='checkbox'/></th>
			<th noWrap="true">收费类别代码 </th>
			<th noWrap="true">收费类别名称 </th>
			<th noWrap="true">收入项目 </th>
			<th noWrap="true">收入类型 </th>
			<th noWrap="true">拼音码 </th>
			</tr>
		</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					
					<tr>
						 <td align='center'  style=''>
            	<input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              	<xsl:attribute name="value" >
              		<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              	</xsl:attribute>
    			  	</input>  
          	</td>
					<xsl:for-each select="./td">
							<xsl:choose>
							<xsl:when test="position()=1">
								<td  noWrap='true' >
							  	<a tabindex="-1">
							    <xsl:attribute name="href" >
							      javascript:view('<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()" />&gt;<xsl:value-of select="." />&lt;/<xsl:value-of select="name()" />&gt;</xsl:for-each>')
							    </xsl:attribute><xsl:value-of select="."/></a>
							  </td>
							</xsl:when>
							<xsl:otherwise><td><xsl:value-of select="."></xsl:value-of></td></xsl:otherwise>
						</xsl:choose>
					</xsl:for-each></tr>
				</xsl:for-each></tbody>
		</xsl:template>
	
	</xsl:stylesheet>