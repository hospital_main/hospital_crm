<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
      <thead>
	        <tr noWrap='true' class='mainHead'>
			<th style='display:none'>����</th>
			<th noWrap="true">����</th>
      			<th noWrap="true">����</th>
			<th noWrap="true">Ȩ��</th>
	        </tr>
      </thead>
      <tbody>
          <xsl:for-each select="/root/tbody/tr">
	        <tr>
	            <xsl:for-each select="td">
		            <xsl:choose>
		             <xsl:when test="position()=1">
		             		<td style='display:none'><xsl:value-of select="."/></td>
              		     </xsl:when>
		              <xsl:when test="position() = 4">
			 		  <td align='center'>
			 		        <input type='checkbox'>
						      <xsl:if test="text() = '1'">
							  <xsl:attribute name="checked">1</xsl:attribute>
						      </xsl:if>
						      
			 		        </input>
			 		   </td>
		              </xsl:when>         
		              <xsl:otherwise>
		                   	<td><xsl:value-of select="."/></td>
		              </xsl:otherwise>
		            </xsl:choose>
	  	     </xsl:for-each>
	        </tr>
          </xsl:for-each>
       </tbody>
  </xsl:template>
</xsl:stylesheet>


