<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
				<th noWrap="true">用户</th>
	      <th noWrap="true">单位</th>
	      <th noWrap="true">账套</th>
				<th noWrap="true">系统</th>
				<th noWrap="true">模块</th>
				<th noWrap="true">权限来源</th>
     </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <xsl:for-each select="td">
            <td><xsl:value-of select="."/></td>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</xsl:template>
</xsl:stylesheet>


