<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th >权限</th>
				<th >单位代码</th>
				<th >单位名称</th>
				<th noWrap="true">账套代码</th>
				<th noWrap="true">账套名称</th>
				<th noWrap="true">会计年度</th>
				<th noWrap="true">名称</th>
				<th noWrap="true">读权限</th>
				<th noWrap="true">写权限</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<xsl:for-each select="td">
						<td><xsl:value-of select="."/></td>
					</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
	</xsl:template>
</xsl:stylesheet>


