<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
	<xsl:template match="/">
		<root>
			<colgroup>		       
				<col style = 'width:200mm'/>
				<col style = 'width:100mm'/>	 
			</colgroup>
			<thead>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan:2;fontsize:maintitle'>模块权限</td>
			  	<td style="display:none"></td> 
				</tr>
				<tr noWrap='true' class='mainHead'>
					<td style='colspan2;fontsize:subtitle'></td>
			  	<td style="display:none"></td> 
				</tr>
				<tr noWrap='true' class='mainHead'>
			  	<td nowrap='true'>模块编码</td>
			  	<td nowrap='true'>模块名称</td> 
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="/root/tbody/tr">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position()=2">
								<td align="right">
										<xsl:value-of select="."/>
								</td>
								</xsl:when>
								<xsl:when test="position()=3">
								<td align="right">
										<xsl:value-of select="."/>
								</td>
								</xsl:when>
								<xsl:otherwise> 
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</tbody>
		</root>
	</xsl:template>
</xsl:stylesheet>
