<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style=''><input type='checkbox' class='vhcheckBox'/></th>
				<th noWrap="true" >单位编码</th>
	      <th noWrap="true">单位名称</th>
	      <th noWrap="true">单位领导</th>
				<th noWrap="true">财务主管 </th>
     </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  >
            <input type='checkbox' TABINDEX='-1'  class='vhcheckBox'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
        	    </xsl:attribute>
              <xsl:if test="td[1]!=''">
      			    <xsl:attribute name="checked" >
      			      <xsl:value-of select="1"/>
      			    </xsl:attribute>
      			  </xsl:if>
     			  </input>          
     			</td>
     			
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">

              </xsl:when>

              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</xsl:template>
</xsl:stylesheet>


