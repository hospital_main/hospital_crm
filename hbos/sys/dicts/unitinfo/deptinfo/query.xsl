<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th nowrap='true'>部门编码</th>
				<th nowrap='true'>部门名称</th>
				<th nowrap='true'>部门类别</th>
				<th nowrap='true'>部门类型</th>
				<th nowrap='true'>部门性质</th>
				<th nowrap='true'>支出性质</th>
				<th nowrap='true'>是否采购</th>
				<th nowrap='true'>是否停用</th>
			</tr>                               
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 1">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

