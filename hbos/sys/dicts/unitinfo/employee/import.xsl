<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th nowrap='true'>所属部门</th>  
				<th nowrap='true'>职工编码</th>
				<th nowrap='true'>职工姓名</th>
				<th nowrap='true'>性别</th>
				<th nowrap='true'>出生年月</th>
				<th nowrap='true'>国籍</th>
				<th nowrap='true'>学历</th>
				<th nowrap='true'>身份证号</th>  
				<th nowrap='true'>进单位时间</th>
				<th nowrap='true'>职称编码</th>
				<th nowrap='true'>职位编码</th>
				<th nowrap='true'>职工类别</th>
				<th nowrap='true'>工作电话</th>
				<th nowrap='true'>手机</th>
				<th nowrap='true'>EMAIL</th>
				<th nowrap='true'>职工工号</th> 
				<th nowrap='true'>是否停用</th> 
				<th nowrap='true'>职工属性</th> 
				<th nowrap='true'>是否采购人员</th>
				<th nowrap='true'>政治面貌</th> 
				<th nowrap='true'>考勤类别</th>  
			</tr>                          
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 1">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

