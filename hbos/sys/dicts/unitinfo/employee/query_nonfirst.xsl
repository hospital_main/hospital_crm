<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<xsl:for-each select="/root/tbody/tr">
  			<xsl:if test="position()=1">
		  		<tr noWrap="true" class="mainHead">
		       <xsl:for-each select="td">
							<th><xsl:value-of select="."/></th>
						</xsl:for-each>
		  		</tr>
	  		</xsl:if>
  		</xsl:for-each>  	
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <xsl:if test="position() &gt;= 1">
        	<tr>             
	          <xsl:for-each select="td">
	                <td>
									<xsl:choose>
										<xsl:when test="position() &gt; 1">
											<xsl:value-of select="."/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="."/>  
										</xsl:otherwise>
									</xsl:choose>
								</td>
	  			  </xsl:for-each>
  				</tr>
  			</xsl:if>
   		</xsl:for-each>  	
  	</tbody>  	
	</xsl:template>
</xsl:stylesheet>
