<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th>科目编码</th>
				<th>科目名称</th>
				<th></th>
			</tr>           
		</thead>  	
		<tbody> 
			<xsl:for-each select="/root/tbody/tr">
				<tr>
					<td align='center'  style='display:none'>
						
						<input type='checkbox' TABINDEX='-1'>
							<xsl:attribute name="value" >
								<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
							</xsl:attribute>
							<xsl:attribute name="acct_subj">
								<xsl:value-of select="pk/acct_subj_code"/>
							</xsl:attribute>
						</input>
					</td>
					<xsl:for-each select="td">
						<td >
							<xsl:choose>
								<xsl:when test="position()=1">
									<xsl:value-of select="."/>
								</xsl:when>
								
								<xsl:otherwise>
									<xsl:value-of select="."/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
					<td>
						<a href="#"><xsl:attribute name="onclick">openDialog("detail.html?load=<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>",'dialogWidth:850px;dialogHeight:650px',result)</xsl:attribute>查看明细</a>
					</td>
				</tr>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>



