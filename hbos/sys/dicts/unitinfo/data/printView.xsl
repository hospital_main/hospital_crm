<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  <table>
  	<thead>
  		<tr noWrap="true" class="mainHead" >
  			<td style="colspan:3;fontsize:maintitle"></td>
			<td style="display:none"></td>
			<td style="display:none"></td>
  		</tr>
  	
  		<tr noWrap='true' class='mainHead'>
        
		  <td noWrap="true" >信息编码</td>
	      <td noWrap="true">信息名称</td>
	      <td noWrap="true">数据</td>
     </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>     			
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
              	<td nowrap="true">
              		<xsl:attribute name="value" >
                     <xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
            		  </xsl:attribute>
              		<xsl:value-of select="."/>
              	</td>
								
              </xsl:when>

              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</table>	
 	</xsl:template>
</xsl:stylesheet>


