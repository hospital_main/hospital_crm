<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<xsl:variable name="cash_flag" select="/root/tbody/tr[1]/td[5]"/>
			<xsl:variable name="fix_flag" select="/root/tbody/tr[1]/td[6]"/>
			<xsl:variable name="mat_flag" select="/root/tbody/tr[1]/td[7]"/>
			<xsl:variable name="med_flag" select="/root/tbody/tr[1]/td[8]"/>
			<xsl:variable name="wage_flag" select="/root/tbody/tr[1]/td[9]"/>
			<xsl:variable name="acc_flag" select="/root/tbody/tr[1]/td[10]"/>
			<xsl:variable name="budg_flag" select="/root/tbody/tr[1]/td[11]"/>
			<xsl:variable name="perf_flag" select="/root/tbody/tr[1]/td[12]"/>
			<xsl:variable name="cost_flag" select="/root/tbody/tr[1]/td[13]"/>

			<xsl:variable name="imma_flag" select="/root/tbody/tr[1]/td[14]"/>
			<xsl:variable name="hr_flag" select="/root/tbody/tr[1]/td[15]"/>

			<tr noWrap="true" class="mainHead">
				<th nowrap="true">年度</th>
				<th nowrap="true">月份</th>
				<th nowrap="true">开始日期</th>
				<th nowrap="true">结束日期</th>
				<th nowrap="true">
					<xsl:if test="$cash_flag != 1">
						<xsl:attribute name="style">display:none</xsl:attribute>
					</xsl:if>
		  		现金银行结账</th>
				<th nowrap="true">
					<xsl:if test="$fix_flag != 1">
						<xsl:attribute name="style">display:none</xsl:attribute>
					</xsl:if>
		  		固定资产结账</th>
				<th nowrap="true">
					<xsl:if test="$mat_flag != 1">
						<xsl:attribute name="style">display:none</xsl:attribute>
					</xsl:if>
		  		物资结账</th>
				<th nowrap="true">
					<xsl:if test="$med_flag != 1">
						<xsl:attribute name="style">display:none</xsl:attribute>
					</xsl:if>
		  		药库结账</th>
				<th nowrap="true">
					<xsl:if test="$wage_flag != 1">
						<xsl:attribute name="style">display:none</xsl:attribute>
					</xsl:if>
		  		工资结账</th>
				<th nowrap="true">
					<xsl:if test="$acc_flag != 1">
						<xsl:attribute name="style">display:none</xsl:attribute>
					</xsl:if>
		  		账务结帐</th>
				<th nowrap="true">
					<xsl:if test="$budg_flag != 1">
						<xsl:attribute name="style">display:none</xsl:attribute>
					</xsl:if>
		  		预算结账</th>
				<th nowrap="true">
					<xsl:if test="$perf_flag != 1">
						<xsl:attribute name="style">display:none</xsl:attribute>
					</xsl:if>
		  		绩效结账</th>
				<th nowrap="true">
					<xsl:if test="$cost_flag != 1">
						<xsl:attribute name="style">display:none</xsl:attribute>
					</xsl:if>
		  		成本结账</th>


				<th nowrap="true">
					<xsl:if test="$imma_flag != 1">
						<xsl:attribute name="style">display:none</xsl:attribute>
					</xsl:if>
		  		无形资产结账</th>
				<th nowrap="true">
					<xsl:if test="$hr_flag != 1">
						<xsl:attribute name="style">display:none</xsl:attribute>
					</xsl:if>
		  		人力资源结账</th>
			</tr>
		</thead>
		<tbody>
			<xsl:for-each select="/root/tbody/tr">
				<xsl:if test="position() &gt; 1">
					<tr>
						<xsl:for-each select="td">
							<xsl:choose>
								<xsl:when test="position() &lt;5">
									<td>
										<xsl:value-of select="."/>
									</td>
								</xsl:when>
								<xsl:otherwise>
									<xsl:variable name="pos" select="position()"/>
									<xsl:variable name="data" select="."/>
									<xsl:variable name="display" select="/root/tbody/tr[1]/td[$pos]"/>
									<xsl:choose>
										<xsl:when test="$display != 1">
											<td style="display:none"><xsl:value-of select="$display"/></td>
										</xsl:when>
										<xsl:otherwise>
											<td>
												<xsl:value-of select="$data"/>
											</td>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>
