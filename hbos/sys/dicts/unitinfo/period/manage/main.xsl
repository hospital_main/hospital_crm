<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <!--<th >选择</th>-->
		  	<th nowrap='true'>月份</th>
		  	<th nowrap='true'>开始日期</th>
		  	<th nowrap='true'>结束日期</th>
      </tr>
    </thead>
    <tbody>
	    <xsl:variable name='TotalTrCount' select='count(/root/tbody/tr)'/>
      <xsl:for-each select="/root/tbody/tr">
	    <xsl:variable name='CurTrPos' select='position()'/>
        <tr>
		
			<input type='checkbox' TABINDEX='-1'  class='vhcheckBox' style="display:none" >
				 <xsl:attribute name="value"><xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each></xsl:attribute>
				 <xsl:attribute name="id">period_<xsl:value-of select="$CurTrPos"/></xsl:attribute>
				<!--<xsl:attribute name="style">display:none</xsl:attribute>-->
				
     			</input>          
     		
        
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position() = 2 and $CurTrPos != 1">
 		          	<td>
				            <xsl:attribute name="id">start_date_<xsl:value-of select="$CurTrPos"/></xsl:attribute>
				            <xsl:value-of select="."/>
 		          	</td>
              </xsl:when>
	       <xsl:when test="position() = 2 and $CurTrPos = 1">
 		          	<td>
				
					    <input type='text' class='inputTextA' maxInput='10' required='true' >
					            <xsl:attribute name="name">start_date_<xsl:value-of select="$CurTrPos"/></xsl:attribute>
						    <xsl:attribute name="id">start_date_<xsl:value-of select="$CurTrPos"/></xsl:attribute>
					            <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
							<xsl:if test="(../td[4] != 0) ">
				      			    <xsl:attribute name="readOnly">true</xsl:attribute>
				      			</xsl:if>
					    </input>
 		          	</td>
              </xsl:when>
	      
              <xsl:when test="position() = 3">
 		          	<td>
										<input type='text' class='inputTextA' maxInput='10' required='true' onChange='check1({$CurTrPos}, {$TotalTrCount});'>
					            <xsl:attribute name="name">end_date_<xsl:value-of select="$CurTrPos"/></xsl:attribute>
					            <xsl:attribute name="value"><xsl:value-of select="."/></xsl:attribute>
				              <xsl:if test="(../td[4] != 0) ">
				      			    <xsl:attribute name="readOnly">true</xsl:attribute>
				      			  </xsl:if>
										</input>
 		          	</td>
              </xsl:when>
              <xsl:when test="position() = 4">
              </xsl:when>
              <xsl:otherwise>
                <td align="center"><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

