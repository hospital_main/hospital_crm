<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<tbody>
		<xsl:variable name="c" select="count(/root/tbody/tr[td[1]=/root/tbody/tr[1]/td[1]])"/>
		<tr class="mainHead">
			<td></td>
			<xsl:for-each select="/root/tbody/tr[position() &lt;= $c]">
				<td>
					<xsl:value-of select="td[2]"/>
				</td>
			</xsl:for-each>
		</tr>
		<xsl:for-each select="/root/tbody/tr">
			<xsl:if test="( position() mod $c )=1">
				<xsl:text disable-output-escaping="yes">
					&lt;tr&gt;
				</xsl:text>
				<td>
					<xsl:value-of select="td[1]"/>
				</td>
			</xsl:if>
			<td align="center">
				<xsl:if test="td[3]&gt;td[4]">
          <xsl:if test="td[3]='27' and td[4]='10'">
            <xsl:if test="td[6]='1'">
						  <input type="checkbox">
							  <xsl:attribute name="mod_code1">
								  <xsl:value-of select="td[3]"/>
							  </xsl:attribute>
							  <xsl:attribute name="mod_code2">
								  <xsl:value-of select="td[4]"/>
							  </xsl:attribute>
							  <xsl:attribute name="mod_value">
								  <xsl:value-of select="td[5]"/>
							  </xsl:attribute>
							  <xsl:attribute name="checked">true
							  </xsl:attribute>
                <xsl:attribute name="disabled">true
							  </xsl:attribute>
                <xsl:attribute name="title">全面预算与资金支出控制默认联用
							  </xsl:attribute>
						  </input>
            </xsl:if>
          	<xsl:if test="td[6]='0'">
						  <input type="checkbox" disabled="true">
                <xsl:attribute name="title">全面预算与资金支出控制未同时购买
							  </xsl:attribute>
						  </input>
					  </xsl:if>
          </xsl:if>
          <xsl:if test="not(td[3]='27' and td[4]='10')">
					  <xsl:if test="td[6]='1'">
						  <input type="checkbox">
							  <xsl:attribute name="mod_code1">
								  <xsl:value-of select="td[3]"/>
							  </xsl:attribute>
							  <xsl:attribute name="mod_code2">
								  <xsl:value-of select="td[4]"/>
							  </xsl:attribute>
							  <xsl:attribute name="mod_value">
								  <xsl:value-of select="td[5]"/>
							  </xsl:attribute>
							  <xsl:if test="td[5]='1'">
								  <xsl:attribute name="checked">true<xsl:value-of select="td[5]"/>
								  </xsl:attribute>
							  </xsl:if>
						  </input>
					  </xsl:if>
					  <xsl:if test="td[6]='0'">
						  <input type="checkbox" disabled="true"/>
					  </xsl:if>
				  </xsl:if>
				</xsl:if>
				<xsl:if test="td[3]&lt;=td[4]">
					-
				</xsl:if>
			</td>
			<xsl:if test="(position() mod $c)=0">
				<xsl:text disable-output-escaping="yes">
					&lt;/tr&gt;
				</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

