var res2 = true; //定义全局变量,判断输入框合法性
function submitData(obj){
	var res=checkInputs();
	if(res==null||res=="")
		return false;
	if(res2){ //只有验证合法才可以发送请求
		window.xmlhttp.post(obj.name, res,"")
		var str = window.xmlhttp._object.responseText
		if (!window.doMsg(str,"")) {
		  return false;
		}
	} else {
		res2 = true;
		return false;
	}
	return true;
	//sysDictsUnitinfoCopyParas_select.click();
}
function checkInputs(){
  var para0311Value = "";
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	var res="";
	for(var i=0;i<trs.length;i++){
		if(trs[i].getAttribute("_editParaType")==null)
			continue;

		 {
			  	if(trs[i]._editDataValue==trs[i]._editInputValue)
			  		continue;
			  	res+="<record>";
			  	if (trs[i].getAttribute("_paraCode")=='0506') {
			  		var checkV = /^[0-9]+$/
						if(!checkV.test(trs[i]._editInputValue)){
							alert("科室分摊开始年月设置必须数字输入！")
							return "";
						}
					}
			  	//trs[i]._editDataType  ()
			  	//alert("boolean:"+checkValue(trs[i],trs[i].getAttribute("_editDataType"),trs[i]._editInputValue));
			  	if(checkValue(trs[i],trs[i].getAttribute("_editDataType"),trs[i]._editInputValue)){
			  		res+=trs[i].getAttribute("_editPk") +"<value>"+ trs[i]._editInputValue +"</value><subj></subj>";
			  	}
    	}

		res+="</record>";
	}
	
	return res;
}


function checkValue(tr,type,value){
	var msg=tr.cells[0].innerText+" "+tr.cells[1].innerText+" :要求是" ;
	var res=true;
	if(type=="1"&&value!=""){
		msg+=" 文本 ";
		//验证输入是否含有非法字符
		if(value.trim().indexOf('<')>=0
		  || value.trim().indexOf('>')>=0
		  || value.trim().indexOf('"')>=0
		  || value.trim().indexOf('&')>=0
		  || value.trim().indexOf('\'')>=0
		  || value.trim().indexOf('\\')>=0
		  || value.trim().indexOf('/')>=0)
		{
			msg = "缺省值中含有非法字符,请勿包含：<,>,\",&,\',\\,/";
			res=false;
			res2 = false;
		} else {
		  res=true;
	  }
	}else if(type=="2"&&IsDate(value)){
		msg+=" 日期 ";
		var d= CDate(value);
		var year=d.getFullYear();
		var month=d.getMonth()+1;
		var day=d.getDate();
		year='0000'+year;year=year.substring(year.length-4)
	  	month='00'+month;month=month.substring(month.length-2)
	  	day='00'+day; day=day.substring(day.length-2)
	 	tr._editInputValue=year+'-'+month+'-'+day;
		res=true;
	}else  if(type=="3"){
		msg+=" 编码规则 ";
		res=isRuleCode(value);
	}else if(type=="4"){
		// alert("type input :"+type);
		msg+=" 正整数 ";
		//var v=parseInt(value,10);
		//if(isNaN(v)||v<0)
		//	res=false;
		var checkV = /^[0-9]+$/
		if(!checkV.test(value)){
			res=false
			res2 = false //判断是否发送请求的全局变量
		}else{
			res2 = true
		}
	}else if(type=="5"){
		var v=parseInt(value,10);
		if(isNaN(v))
			res=false;
	}
	if(res==false){
		res2 = false;
		alert(msg);
	}
		
	return res;
}

function initInputs(){
	var table=document.getElementById("_mainDataTable");
	var trs=table.getElementsByTagName("tr");
	for(var i=0;i<trs.length;i++)
		initTrInputs(trs[i]);
}
function initTrInputs(tr){
	var pt=tr.getAttribute("_editParaType");
	var dt=tr.getAttribute("_editDataType");
	var dv=tr.getAttribute("_editDataValue");
	var pc=tr.getAttribute("_paraCode");
	var pv=tr.getAttribute("_paraValue");
	
	if(pt==null||dt==null)
		return ;
	var inp="";
	var id=getJsGuid();
	var tag="";
	var chk="";
	tr._editInputId=id;
	if(pt=="1"){
		inp="<input type='text' id='"+id+"' name='"+id+"' value='"+dv+"'/>";
		tag="input";
	}else if(pt=="2"){
		var ops=tr.cells[3].innerText.split("\/");
		if(pc=="0311"){

	  	inp="<select id='"+id+"' name='"+id+"' onChange='changeValue(this,\"" + pc + "\")'>";
	  	g_0311_value = dv ;
	  	g_seleSubj = pv ;
	  	
			if(dv=="是" && pc=="0311"){
			  seleAcct.innerHTML = "<a href=# onclick=openDialog('setBudgSubj.html','dialogWidth:680px;dialogHeight:650px;')>科目设置</a>";
			}else{
			  seleAcct.innerHTML = "";
			}
  	}else if(pc=="0321"){
	  	inp="<select id='"+id+"' name='"+id+"' onChange='changeValue(this,\"" + pc + "\")'>";
	  	g_0311_value = dv ;
	  	g_seleSubj = pv ;
  	}else if(pc=="0331"){
  		inp="<select id='"+id+"' disabled name='"+id+"'>";
  	}else{
		  inp="<select id='"+id+"' name='"+id+"'>";
	  }
	  
		tag="select"
		for(var i=0;i<ops.length;i++){
			if(dv==ops[i])
				chk=" selected ";
			else
				chk="";

   		inp+="<option "+chk+" value='"+ops[i]+"'>"+ops[i]+"</option>";
		}
		inp+="</select>";
	}else if(pt=="3"){
		var ops=tr.cells[3].innerText.split("\/");
		inp="";
		tag="input"
		for(var i=0;i<ops.length;i++){
			if(dv==ops[i])
				chk=" checked ";
			else
				chk="";
			if(pc=="0333"){
				if(dt ==9){
					inp+="<input type='radio' "+chk+" name='"+id+"' value='"+ops[i]+"'  onChange='changeValue(this,\"" + pc + "\")' disabled= 'true' />"+ops[i];
				}else{
					inp+="<input type='radio' "+chk+" name='"+id+"' value='"+ops[i]+"'  onChange='changeValue(this,\"" + pc + "\")' />"+ops[i];
				}
			}else{
				if(dt ==9){
					inp+="<input type='radio' "+chk+" name='"+id+"' value='"+ops[i]+"' disabled= 'true' />"+ops[i];
				}else{
					inp+="<input type='radio' "+chk+" name='"+id+"' value='"+ops[i]+"' />"+ops[i];
				}
			}
			if(i+1!=ops.length)
				inp+="<br/>";
		}
	}else
		return ;
	tr.cells[2].innerHTML=inp;
	//alert(inp);   
	var inpus=tr.cells[2].getElementsByTagName("input");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	inpus=tr.cells[2].getElementsByTagName("select");
	for(var i=0;i<inpus.length;i++){
		setInputChange(tr,inpus[i])
	}
	
}
function setInputChange(tr,inp){
	inp.onblur=function(){tr._editInputValue=this.value;};//getAttribute("_editInputValue")
	inp.onclick="";
	inp.onkeyup="";
}

