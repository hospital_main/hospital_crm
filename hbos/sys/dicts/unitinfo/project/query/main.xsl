
<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th>项目编码</th>
				<th>项目名称</th>
				<th>填报部门</th>
				<th>填报日期</th>
				<th>项目级别</th>
				<th>项目类型</th>
				<th>预算总额</th>				
				<th>项目状态</th>
				<th>填报人</th>
				<th>项目负责人</th>
				<th>财务负责人</th>
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
	    	 
        <tr>
          <xsl:for-each select="td">
              <xsl:choose>
                <xsl:when test="position()=7">
			            <td align="right" >               
	                    <xsl:value-of select="format-number(.,'#,##0.00')"/>	                	                
	               		 </td>
                </xsl:when>
                
			  	 			<xsl:otherwise>
			            <td >
		                	<xsl:value-of select="."/>
			            </td>
                </xsl:otherwise>
              </xsl:choose>
  			  </xsl:for-each>
  		  
  			</tr> 			
  		
   		</xsl:for-each>  
   	 
  	</tbody>
	</xsl:template>
</xsl:stylesheet>
