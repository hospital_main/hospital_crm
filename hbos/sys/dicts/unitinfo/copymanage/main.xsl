<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th style='display:none'><input type='checkbox'/></th>
		  	<th nowrap='true'>账套编码</th>
		  	<th nowrap='true'>账套名称</th>
		  	<th nowrap='true'>开始年度</th>
		  	<th nowrap='true'>开始月份</th>
		  	<th nowrap='true'>科目体系</th>
		  	<th nowrap='true'>当前年度</th>
		  	<th nowrap='true'>当前月份</th>
		  	<th nowrap='true'>年度账</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <!--<td align='center'  style='display:none'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
              </xsl:attribute>
            </input>
          </td>-->
          <xsl:for-each select="td">
            <td>
              <xsl:choose>
                <xsl:when test="position()=1">
                	<a tabindex='-1'><xsl:attribute name="href">javascript:openDialog('update.html?load=<xsl:for-each select="../pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>', 'dialogWidth:600px;dialogHeight:350px', result);</xsl:attribute><xsl:value-of select="."/></a>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="."/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
          <td>
          	<a href="#">
							<xsl:attribute name="onclick">
								javascript:openDialog('yearMain.html?load=<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>&lt;copy_name&gt;<xsl:value-of select="td[2]"/>&lt;/copy_name&gt;', 'dialogWidth:800px;dialogHeight:600px', null);result.refresh();
							</xsl:attribute>设置
						</a>
          </td>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

