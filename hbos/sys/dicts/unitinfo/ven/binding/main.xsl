<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th nowrap="true">供应商编码</th>
				<th nowrap="true">供应商名称</th>
				<th nowrap="true">绑定供应商名称</th>
				<th nowrap="true">创建时间</th>
				<th nowrap="true">状态</th>
				<th nowrap="true">审批人</th>
				<th nowrap="true">审批时间</th>
				<th nowrap="true">审批意见</th>
				<th nowrap="true" style="display:none;">是否同步</th>
  		</tr>
  	</thead>
  	<tbody>
	    <xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
  	  <xsl:for-each select="/root/tbody/tr">
  	      <tr>
	      	  <td align='center'  style='display:none'>
	            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
	              <xsl:attribute name="value" >
	                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
	      			  </xsl:attribute>
	    			  </input>
	          </td>
  	        <xsl:for-each select="td">
  	        	<xsl:if test="position() != 9">
  	          		<td align='left'>
  	                	<xsl:value-of select="."/>
  	              	</td>
  	        	</xsl:if>
  	        </xsl:for-each>
  	      </tr>
	    </xsl:for-each>
		</tbody>
	</xsl:template>
</xsl:stylesheet>