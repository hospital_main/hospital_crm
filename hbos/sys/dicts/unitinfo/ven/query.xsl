<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th nowrap='true'>供应商编码</th>
				<th nowrap='true'>供应商名称</th>
				<th nowrap='true'>省</th>
				<th nowrap='true'>市</th>
				<th nowrap='true'>地址</th>
				<th nowrap='true'>邮政编码</th>
				<!--<th nowrap='true'>地区</th>-->
				<th nowrap='true'>法人</th>
				<th nowrap='true'>联系人</th>
				<th nowrap='true'>电话</th>
				<th nowrap='true'>主要产品</th>
				<th nowrap='true'>信用额度</th>
				<th nowrap='true'>营业执照</th>
			</tr>                        
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 1">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:when test="position() = 11">
								<xsl:value-of select="format-number(.,'#,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

