<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  	<tr noWrap='true' class='mainHead'>
  		<th noWrap="true"><input type="checkbox"/></th>
		<th noWrap="true">供应商编码</th>
	        <th noWrap="true">供应商名称</th>
	        <th noWrap="true">用户</th>
		<th noWrap="true">操作时间</th>
		<th noWrap="true">操作类型</th>
		<th noWrap="true">单位</th>
		<th noWrap="true">账套</th>
		<th noWrap="true" style="display:none">PK</th>
     </tr>
  	</thead>
  	<tbody>
	<xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
          <xsl:for-each select="td">
          	 <td><xsl:value-of select="."/></td>
  	  </xsl:for-each>
  	</tr>
   	</xsl:for-each>
 	</tbody>
 	</xsl:template>
</xsl:stylesheet>


