<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead' >
      	<th nowrap='true' style='display:none'>单位编码</th>
	      <th nowrap='true'>供应商编码</th>
			  <th nowrap='true'>供应商名称</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr> 
          <xsl:for-each select="td">
            <xsl:choose>
            	<xsl:when test="position()=1">
            		<td style='display:none'><xsl:value-of select="."/></td>
            	</xsl:when>
              <xsl:when test="position()=2">
              	<td>
                	<a tabindex='-1' href='#'>
	                  <xsl:attribute name="onclick" >
	                  	<!--javascript:queryDetail('&lt;root&gt;&lt;comp_code&gt;<xsl:value-of select="../td[1]"/>&lt;/comp_code&gt;&lt;ven_code&gt;<xsl:value-of select="../td[2]"/>&lt;/ven_code&gt;&lt;/root&gt;');
	    	            	-->
	    	            	javascript:queryDetail('&lt;comp_code&gt;<xsl:value-of select="../td[1]"/>&lt;/comp_code&gt;&lt;ven_code&gt;<xsl:value-of select="../td[2]"/>&lt;/ven_code&gt;');
	    	            	
	  	          		</xsl:attribute>
	  	          		<xsl:value-of select="."/>
  	          		</a>
	          		</td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>