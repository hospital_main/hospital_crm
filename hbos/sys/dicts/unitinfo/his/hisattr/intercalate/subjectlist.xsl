<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
  		<th noWrap="true" width="5%"></th>
			<th noWrap="true" width="30%">科目编号</th>
			<th noWrap="true" width="65%">科目名称</th>
		</tr>
  	</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<td align='center'>
							<input type='radio' name="selRadio" onclick="selValue(this)">
								<xsl:attribute name="acct_subj_code" ><xsl:value-of select="td[1]"/></xsl:attribute>
								<xsl:attribute name="acct_subj_name" ><xsl:value-of select="td[2]"/></xsl:attribute>
							</input>
				</td>
			  <xsl:attribute name="acct_subj">
			    	<xsl:value-of select="td[3]"/>
				</xsl:attribute>
				<xsl:for-each select="td[position() &lt; 3]">  
				<td>
					<xsl:choose>
							<xsl:when test="position() = 1">
							  <xsl:attribute name="ondblclick">
									javascript:dbsub();
							  </xsl:attribute>
							  <xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
					</xsl:choose>
				</td>    
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>