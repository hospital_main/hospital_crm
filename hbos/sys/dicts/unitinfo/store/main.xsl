<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
				<th nowrap="true">库房编码</th>
				<th nowrap="true">库房名称</th>
				<th nowrap="true">所属系统</th>
				<th nowrap="true">负责人</th>
				<th nowrap="true">保管</th>
				<th nowrap="true">会计</th>
				<th nowrap="true">采购</th>	
			</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	  
        <tr>
           <xsl:for-each select="td">
        
				   <td>
					  <xsl:value-of select="."/>  
					</td>
          </xsl:for-each>
           
  			</tr>
   		</xsl:for-each>  	
  	</tbody>
	</xsl:template>
</xsl:stylesheet>