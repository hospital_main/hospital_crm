<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
  	<root>
  		<table>
    <thead>

		 <tr noWrap="true" class="mainHead">
				<td nowrap="true" colspan="7"></td>
				<td nowrap="true" style="display:none"></td>
				<td nowrap="true" style="display:none"></td>
				<td nowrap="true" style="display:none"></td>
				<td nowrap="true" style="display:none"></td>
				<td nowrap="true" style="display:none"></td>
				<td nowrap="true" style="display:none"></td>	
			</tr>

  		<tr noWrap="true" class="mainHead">
				<td nowrap="true">库房编码</td>
				<td nowrap="true">库房名称</td>
				<td nowrap="true">所属系统</td>
				<td nowrap="true">负责人</td>
				<td nowrap="true">保管</td>
				<td nowrap="true">会计</td>
				<td nowrap="true">采购</td>	
			</tr>
  	</thead>
  	<tbody>
	   <xsl:for-each select="/root/tbody/tr">
	  
        <tr>
           <xsl:for-each select="td">
        
				   <td>
					  <xsl:value-of select="."/>  
					</td>
          </xsl:for-each>
           
  			</tr>
   		</xsl:for-each>  	
 		</tbody>
 		<tfoot>
 		    <tr noWrap="true">
				<td nowrap="true" colspan="7"></td>
				<td nowrap="true" style="display:none"></td>
				<td nowrap="true" style="display:none"></td>
				<td nowrap="true" style="display:none"></td>
				<td nowrap="true" style="display:none"></td>
				<td nowrap="true" style="display:none"></td>
				<td nowrap="true" style="display:none"></td>	
			</tr>
 		</tfoot>
 		</table>
 		</root>
	</xsl:template>
</xsl:stylesheet>