<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=''/>
  <xsl:template match="/">
    <root>
      <thead>
      
  	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:maintitle;colspan:3;align:chenter"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	
  	  </tr>
	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	
  	  </tr>
	  <tr noWrap="true" class="mainHead">
  	  	<td noWrap="true" style="fontsize:subtitle;colspan:3;align:left"/>
  	  	<td style="display:none"/>
  	  	<td style="display:none"/>
  	  	
  	  </tr>

        <tr noWrap='true' class='mainHead'>  	  
          <td>指标编码</td>
          <td>指标名称</td>
          <td>指标值</td>
	 
	</tr> 		      
      </thead>	      
      <tbody>
        <xsl:for-each select="/root/tbody/tr">
	  <tr>
            <xsl:for-each select="td">          
	      <xsl:choose>            
		<xsl:when test="position()=3">
      	          <td align="left">
		    <xsl:value-of select="."/>
		  </td>
      	        </xsl:when>
		<xsl:otherwise>
		  <td align="left">
		    <xsl:value-of select="."/>
		  </td>
		</xsl:otherwise>
	      </xsl:choose>
            </xsl:for-each>
	  </tr>
        </xsl:for-each>
      </tbody>
     
    </root>   
  </xsl:template>
</xsl:stylesheet>