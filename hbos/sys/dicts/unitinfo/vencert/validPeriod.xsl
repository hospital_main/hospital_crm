<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<thead>
			<tr noWrap='true' class='mainHead'>
				<th style='display:none'><input type='checkbox'/></th>
				<th noWrap="true">供应商</th>
	      <th noWrap="true">许可证</th>
				<th noWrap="true">开始日期 </th>
				<th noWrap="true">结束日期</th>
				<th noWrap="true">经营范围</th>
			</tr>                        
		</thead>
	<tbody>
		<xsl:for-each select="/root/tbody/tr">
			<tr>
				<xsl:for-each select="td">   
					<td>
						<xsl:choose>
							<xsl:when test="position() = 1">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:for-each>
			</tr>
		</xsl:for-each>
	</tbody>
	</xsl:template>
</xsl:stylesheet>

