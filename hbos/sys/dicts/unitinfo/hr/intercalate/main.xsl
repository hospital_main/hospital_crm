<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
			<th noWrap="true" width="20%">人事科室编码</th>
			<th noWrap="true" width="20%">人事科室名称</th>
			<th noWrap="true" width="60%">HERP科室</th>
		</tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
	        <tr> 
	         <xsl:for-each select="td[position()>1]">
	          <td>
	           <xsl:choose>
							<xsl:when test="position() = 3">
								<table width="100%">
									<tr>
										<td width="60%">
											<xsl:attribute name="ondblclick">
												clearSubj();
											</xsl:attribute>
											<xsl:value-of select="."/>
										</td>
										<td>
										 <xsl:if test="../td[1]='0'">
										  [<a >
											<xsl:attribute name="onclick">
												setting();
											</xsl:attribute>
											<xsl:attribute name="href">
												#
											</xsl:attribute>
											设置
											</a>]
											</xsl:if>
										</td>
									</tr>
								</table>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="."/>  
							</xsl:otherwise>
					</xsl:choose>    
	       </td>
		   </xsl:for-each>
		   
  		</tr>
   	    </xsl:for-each>
 	</tbody>
  </xsl:template>
</xsl:stylesheet>