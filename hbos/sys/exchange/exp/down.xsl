<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap='true' class='mainHead'>
        <th style=''><input type='checkbox' class='vhcheckBox'/></th>
				<th noWrap="true">文件描述</th>
				<th noWrap="true">创建时间</th>
     </tr>
  	</thead>
  	<tbody>
	    <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'  >
            <input type='checkbox' TABINDEX='-1'  class='vhcheckBox'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
        	    </xsl:attribute>
     			  </input>          
     			</td>
     			
          <xsl:for-each select="td">
            <xsl:choose>
              <xsl:when test="position()=1">
                <td>
                	 <a target="_new">
		                  <xsl:attribute name="href">downloadExFile.cmd?file=<xsl:value-of select="../td[3]"/></xsl:attribute>
		                  <xsl:value-of select="."/>
	                  </a>
                </td>
              </xsl:when>
              <xsl:when test="position()=3">
                <td style="display:none"><xsl:value-of select="."/></td>
              </xsl:when>
              <xsl:otherwise>
                <td><xsl:value-of select="."/></td>
              </xsl:otherwise>
          </xsl:choose>
  			  </xsl:for-each>
  			</tr>
   		</xsl:for-each>
 		</tbody>
 	</xsl:template>
</xsl:stylesheet>


