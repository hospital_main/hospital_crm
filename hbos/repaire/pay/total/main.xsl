<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>供应商编号</th>
				<th>供应商名称</th>
				<th>维修金额</th>
				<th>开票金额</th>	
				<th>未开票金额</th>
				<th>付款金额</th>	
				<th>未付款金额</th>		
				<th>开票未付款金额</th>
  		</tr>
  	</thead>
  	<tbody>
  		 
			<xsl:for-each select="/root/tbody/tr">
				<tr>    			
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test=" position() > 2">
	              <td align="right" class="moneyCol">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when> 
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
							
						</xsl:choose>
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 		
	  </tbody> 	
	</xsl:template>
</xsl:stylesheet>