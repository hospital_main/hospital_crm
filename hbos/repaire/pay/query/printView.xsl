<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
		  <thead>
		  	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/> 
		     	</tr>
		  		<tr noWrap="true" class="mainHead">
		        <td>记录日期</td>
						<td>申请部门</td>
						<td>设备名称</td>
						<td>规格</td>				
						<td>供应商</td>	
						<td>维修级别</td>				
						<td>材料金额</td>
						<td>维修金额</td>
						<td>合计金额</td>	
						<td>发票金额</td>	
						<td>未开票金额</td>
						<td>付款金额</td>	
						<td>未付款金额</td>		
						<td>开票未付款金额</td>
		  		</tr>
	  	</thead>
	  	<tbody>
	  	 
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>    			
					  
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position() > 6">
	              <td align="right" class="moneyCol">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>
	             
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
							
						</xsl:choose>
					</xsl:for-each>
				</tr>		
	   		</xsl:for-each>  	
	  	</tbody>
	  	 
	 </root> 	
	</xsl:template>
</xsl:stylesheet>