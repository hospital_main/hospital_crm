<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
		  <thead>
		  	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		     	</tr>
		  		<tr noWrap="true" class="mainHead">
		        <td>付款编码</td>
						<td>付款日期</td>
						<td>职能部门</td>
						<td>供应商</td>				
						<td>经办人</td>	
						<td>金额</td>				
						<td>状态</td>
						<td>制单人</td>
						<td>审核人</td>
						<td>确认人</td>		
		  		</tr>
	  	</thead>
	  	<tbody>
	  	 
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>    			
					  
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=6">
	              <td align="right" class="moneyCol">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when>
	            
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
							
						</xsl:choose>
					</xsl:for-each>
				</tr>		
	   		</xsl:for-each>  	
	  	</tbody>
	  	 
	 </root> 	
	</xsl:template>
</xsl:stylesheet>