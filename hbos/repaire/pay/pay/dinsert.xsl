<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
<xsl:template match="/">
  <thead>
  	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
    <tr noWrap="true" class="mainHead">
      <th style="display:none"><input type="checkbox"/></th>
      <th>发票编号</th>
      <th>发票号</th>
      <th>开票日期</th>
      <th>申请科室</th>
      <th>设备名称</th>
      <th>维修部门</th>
      <th>维修级别</th>
      <th>开票金额</th>
      <th>已付款金额</th>
      <th>未付款金额</th>
    </tr>
  </thead>
  <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        <td align='center'  style='display:none'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
               <xsl:value-of select="td[1]"/>;;<xsl:value-of select="td[2]"/>;;<xsl:value-of select="td[3]"/>;;<xsl:value-of select="pk/repair_rec_no"/>;;<xsl:value-of select="td[4]"/>;;<xsl:value-of select="td[5]"/>;;<xsl:value-of select="td[8]"/>;;<xsl:value-of select="td[10]"/>;;<xsl:value-of select="pk/bill_detail_id"/>
    	      </xsl:attribute>
  	    	</input>
        </td>
        <xsl:for-each select="td">            
          <xsl:choose>
             

			  <!--

				<xsl:if test="'21'!=../td[12]  ">
					<a href="#">
							<xsl:attribute name="onclick" >
								javascript:openDialog('ddinsert.html?load=&lt;iow_id&gt;<xsl:value-of select="../td[13]"/>&lt;/iow_id&gt;','dialogWidth:860px;dialogHeight:600px',result)
							</xsl:attribute>
							<xsl:value-of select="."/>
					</a>  
				</xsl:if>
				-->
 
             
            <xsl:when test="position()=8 or position()=9 or position()=10">
              <td  align="right">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
              </td>
            </xsl:when>

            <xsl:otherwise>
              <td><xsl:value-of select="."/></td>
            </xsl:otherwise>
          </xsl:choose>            
  	</xsl:for-each>
      </tr>
    </xsl:for-each>
  </tbody>
</xsl:template>
</xsl:stylesheet>
