<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th style='display:none' width='25'><input type='checkbox'/></th>
				<th>付款编号</th>
				<th>付款日期</th>
				<th>职能部门</th>
				<th>供应商</th>				
				<th>经办人</th>	
				<th>金额</th>				
				<th>状态</th>
				<th>制单人</th>
				<th>审核人</th>	
				<th>确认人</th>		
  		</tr>
  	</thead>
  	<tbody>
  		 
			<xsl:for-each select="/root/tbody/tr">
				<tr>    			
					<xsl:if test="td[1]!='合 计'">
					  <td align='center'  >
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
            </td>
          </xsl:if> 
					<xsl:if test="td[1]='合 计'">
							<td>
							</td>
						</xsl:if>
						
						
					<xsl:for-each select="td">
						<xsl:choose>
							<xsl:when test="position()=1">
	              <td>
	              	<xsl:if test=".='合 计'">
										 <xsl:value-of select="."/>
									</xsl:if>
									<xsl:if test=".!='合 计'">
                  <a TABINDEX='-1' >
									  
									  <xsl:value-of select="."/>
									</a>
									</xsl:if>
                </td>
	            </xsl:when>
	            
							<xsl:when test="position()=6">
	              <td align="right" class="moneyCol">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
                </td>
	            </xsl:when> 
							<xsl:otherwise>
								<td><xsl:value-of select="."/></td>
							</xsl:otherwise>
							
						</xsl:choose>
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 		
	  </tbody> 	
	</xsl:template>
</xsl:stylesheet>