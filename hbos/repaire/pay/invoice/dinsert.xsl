<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:decimal-format NaN=""/>  
<xsl:template match="/">
  <thead>
  	<xsl:variable name="VHMONEYFORMAT" select="/root/annex/VHMONEYFORMAT"/>
    <tr noWrap="true" class="mainHead">
      <th style="display:none"><input type="checkbox"/></th>
      <th>维修记录号</th>
      <th>制单日期</th>
      <th>申请科室</th>
      <th>卡片编号</th>
      <th>设备名称</th>
      <th>规格</th>
      <th>维修部门</th>
      <th>工程师</th>
      <th>维修级别</th>
      <th>合计金额</th>
      <th>已开票金额</th>
      <th>未开票金额</th>
    </tr>
  </thead>
  <tbody>
    <xsl:for-each select="/root/tbody/tr">
      <tr>
        
        <td align='center'  style='display:none'>
          <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
            <xsl:attribute name="value" >
              <xsl:value-of select="pk/repair_rec_no"/>;;<xsl:value-of select="td[1]"/>;;<xsl:value-of select="td[2]"/>;;<xsl:value-of select="td[4]"/>;;<xsl:value-of select="td[9]"/>;;<xsl:value-of select="td[11]"/>
    	      </xsl:attribute>
  	    	</input>
        </td>
        
        <xsl:for-each select="td">            
          <xsl:choose>
 
				<xsl:when test="position()=1">
					<td>
						<a href="#">
							<xsl:attribute name="onclick" >
								javascript:openDialog('../../repaire/record/update.html?load=&lt;no&gt;<xsl:value-of select="."/>&lt;/no&gt;','dialogWidth:860px;dialogHeight:600px',result)
							</xsl:attribute>
							<xsl:value-of select="."/>
						</a>  
					</td>
				</xsl:when> 
 
             
            <xsl:when test="position()=10 or position()=11 or position()=12">
              <td  align="right">
                  <xsl:value-of select="format-number(.,'#,##0.00')"/>
              </td>
            </xsl:when>

            <xsl:otherwise>
              <td><xsl:value-of select="."/></td>
            </xsl:otherwise>
          </xsl:choose>            
  	</xsl:for-each>
      </tr>
    </xsl:for-each>
  </tbody>
</xsl:template>
</xsl:stylesheet>
