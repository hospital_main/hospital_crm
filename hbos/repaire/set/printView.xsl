<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<root>
    <thead>
    	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		  </tr>
      <tr noWrap="true" class="mainHead">
        <td>人员编码</td>
        <td>人员姓名</td>
        <td>供应商</td>
      </tr>                
    </thead>
    <tbody>
			<xsl:for-each select="/root/tbody/tr">
				<tr>    
					<xsl:for-each select="td">
						<td><xsl:value-of select="."/></td> 	
					</xsl:for-each>
				</tr>			
			</xsl:for-each> 		
	  </tbody> 	
	  </root>
  </xsl:template>
</xsl:stylesheet>
