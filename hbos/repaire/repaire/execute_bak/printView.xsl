<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <root>
		  <thead>
		  	<tr noWrap="true" class="mainHead">
		      	<td noWrap="true" style="fontsize:maintitle;colspan:colcount"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		      	<td style="display:none"/>
		     	</tr>
		  		<tr noWrap="true" class="mainHead">
		        <td>执行单号</td>
						<td>记录日期</td>
						<td>派工单号</td>
						<td>维修部门</td>				
						<td>卡片编号</td>	
						<td>设备名称</td>				
						<td>规格</td>
						<td>型号</td>
						<td>是否内部</td>
						<td>维修工程师</td>
						<td>供应商</td>
						<td>维修内容</td>
						<td>状态</td>
						<td>制单人</td>
						<td>审核人</td>
		  		</tr>
	  	</thead>
	  	<tbody>
	  	 
		    <xsl:for-each select="/root/tbody/tr">
	        <tr>    			
					  
					<xsl:for-each select="td">
						<td><xsl:value-of select="."/></td>
						
					</xsl:for-each>
				</tr>		
	   		</xsl:for-each>  	
	  	</tbody>
	  	 
	 </root> 	
	</xsl:template>
</xsl:stylesheet>