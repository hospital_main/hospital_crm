<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>选择</th>
      	<th nowrap='true'>派工单号</th>
      	<th nowrap='true'>派工日期</th>
      	<th nowrap='true'>申请部门</th>
      	<th nowrap='true'>申请人</th>
      	<th nowrap='true'>卡片编号</th>
      	<th nowrap='true'>设备名称</th>
      	<th nowrap='true'>是否内部</th>
      	<th nowrap='true'>是否合同</th>
      	<th nowrap='true'>维修部门</th>
      	<th nowrap='true'>维修工程师</th>
      	<th nowrap='true'>供应商</th>
      	<th nowrap='true'>状态</th>
      	<th nowrap='true'>制单人</th>
      	<th nowrap='true'>审核人</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='radio' style='font-size:8px;' name="a" onclick="selAccount(this)">
              <xsl:attribute name="detail_id" ><xsl:value-of select="td[1]"/></xsl:attribute>
            	<xsl:attribute name="repair_dept_id" ><xsl:value-of select="td[10]"/></xsl:attribute>
            	<xsl:attribute name="card_no" ><xsl:value-of select="td[6]"/></xsl:attribute>
            	<xsl:attribute name="is_inner_id" ><xsl:value-of select="td[8]"/></xsl:attribute>
            	<xsl:attribute name="repair_emp_id" ><xsl:value-of select="td[11]"/></xsl:attribute>
            	<xsl:attribute name="ven_code_id" ><xsl:value-of select="td[12]"/></xsl:attribute>
            </input>
          </td>
          <xsl:for-each select="td[position()>1]">
            <xsl:choose>
          	<xsl:when test="position()=1">
                 <td>
	                   	<a>
	                  	<xsl:attribute name="href" >
	              				javascript:openDialog('work.html?load=&lt;dispatch_no&gt;<xsl:value-of select="."/>&lt;/dispatch_no&gt;', 'dialogWidth:960px;dialogHeight:380px;')
	      	    				</xsl:attribute>
	                  	<xsl:value-of select="."/>
	                 		</a>
                 	</td>
               </xsl:when>
          	<xsl:otherwise>
            <td>
              <xsl:value-of select="."/>
            </td>
             </xsl:otherwise>
             </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

