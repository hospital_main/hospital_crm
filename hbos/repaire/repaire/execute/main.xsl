<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
  	<thead>
  		<tr noWrap="true" class="mainHead">
        <th><input type="checkbox"/></th>
				<th>派工单号</th>
				<th>派工日期</th>
				<th>申请部门</th>
				<th>申请人</th>				
				<th>卡片编号</th>	
				<th>设备名称</th>				
				<th>规格</th>
				<th>型号</th>
				<th>是否内部</th>
				 <th>维修部门</th>	 
				<th>维修工程师</th>
				<th style="display:none;">供应商</th>	
				<th style="display:none;">是否合同</th>	
				<th style="display:none;">维保合同</th>
				<th>紧急程度</th>
				<th>状态</th>	
				<th>制单人</th>	
				<th>审核人</th>	
				<th>申请单据</th>	
  		</tr>
  	</thead>
  	<tbody>
  		<xsl:for-each select="/root/tbody/tr">
				<tr>    
					<td align='center'>
            <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
              <xsl:attribute name="value" >
                <xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
      			  </xsl:attribute>
    			  </input>
          </td>
					<xsl:for-each select="td">
						<xsl:choose>
							 <xsl:when test="position()=1">
                  <td><a tabindex='-1'><xsl:value-of select="."/></a></td>
                </xsl:when>
                <xsl:when test="position()=5">
                <td>
	                   	<a>
	                  	<xsl:attribute name="href" >
	              				javascript:openDialog('../../fixcard/maintain/cardInfo.html?load=&lt;card_arch_no&gt;<xsl:value-of select="."/>&lt;/card_arch_no&gt;', 'dialogWidth:1000px;dialogHeight:800px;')
	      	    				</xsl:attribute>
	                  	<xsl:value-of select="."/>
	                 		</a>
                 	</td>
                </xsl:when>
                
                <xsl:when test="position()=16">
                 <td>
	                   	<a>
	                  	<xsl:attribute name="href" >
	              				javascript:openDialog('apply.html?load=&lt;apply_no&gt;<xsl:value-of select="."/>&lt;/apply_no&gt;', 'dialogWidth:800px;dialogHeight:350px;')
	      	    				</xsl:attribute>
	                  	<xsl:value-of select="."/>
	                 		</a>
                 	</td>
                </xsl:when>
              <xsl:otherwise>
								<td><xsl:value-of select="."/></td>
						  </xsl:otherwise>
						 	</xsl:choose> 
					</xsl:for-each>
				</tr>			
			</xsl:for-each>  		
	  </tbody> 	
	</xsl:template>
</xsl:stylesheet>