<?xml version='1.0' encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      <tr noWrap='true' class='mainHead'>
        <th nowrap='true'>选择</th>
      	<th nowrap='true'>申请单据号</th>
      	<th nowrap='true'>资产卡片号</th>
      	<th nowrap='true'>资产编码</th>
      	<th nowrap='true'>资产名称</th>
      	<th nowrap='true'>申请日期</th>
      	<th nowrap='true'>故障说明</th>
      	<th nowrap='true' style="display:none;">工单编码</th>
      	<th nowrap='true' style="display:none;">计划时间</th>
        <th nowrap='true' style="display:none;">维修开始时间</th>
      	<th nowrap='true' style="display:none;">维修工程师</th>
        <th nowrap='true' style="display:none;">维修部门</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr">
        <tr>
          <td align='center'>
            <input type='radio' style='font-size:8px;' name="a" onclick="selAccount(this)">
              <xsl:attribute name="detail_id" ><xsl:value-of select="td[1]"/></xsl:attribute>
              <xsl:attribute name="card_no" ><xsl:value-of select="td[3]"/></xsl:attribute>
              <xsl:attribute name="wo_h_id" ><xsl:value-of select="td[8]"/></xsl:attribute>
              <xsl:attribute name="planHours" ><xsl:value-of select="td[9]"/></xsl:attribute>
              <xsl:attribute name="planBeginTime" ><xsl:value-of select="td[10]"/></xsl:attribute>
              <xsl:attribute name="empName" ><xsl:value-of select="td[11]"/></xsl:attribute>
              <xsl:attribute name="deptName" ><xsl:value-of select="td[12]"/></xsl:attribute>
           </input>
          </td>
          <xsl:for-each select="td[position()>1]">
          	<xsl:choose>
          	<xsl:when test="position()=7">
						 <td style="display:none;">
						 	<xsl:value-of select="."/>
						 	</td>
						</xsl:when>
						<xsl:when test="position()=8">
						 <td style="display:none;">
						 	<xsl:value-of select="."/>
						 	</td>
						</xsl:when>
						<xsl:when test="position()=9">
						 <td style="display:none;">
						 	<xsl:value-of select="."/>
						 	</td>
						</xsl:when>
						<xsl:when test="position()=10">
						 <td style="display:none;">
						 	<xsl:value-of select="."/>
						 	</td>
						</xsl:when>
						<xsl:when test="position()=11">
						 <td style="display:none;">
						 	<xsl:value-of select="."/>
						 	</td>
						</xsl:when>
							<xsl:otherwise>
            <td>
              <xsl:value-of select="."/>
            </td>
             </xsl:otherwise>
              </xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

