	  var showstop=0,showlower=0,texpands=1;
      //全局变量 职工基本信息empidvalue
      var empidvalue="";
      //条件说明cond_desc 条件列表cond_list 条件内容cond
      var cond_desc = "";
      var cond_list = "";
      var cond = "";
      //条件名称
      var cond_title = "";

      function init(){//初始化左侧树
        var pk=getPk();
        unitTree.expand=1;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        unitTree.init();

        //初始化考勤开始结束时间为当前结账时间
      	window.xmlhttp.post('hr_att_wss_wageBatch_startEndDate',"","?isCheck=false");
		var resText = window.xmlhttp._object.responseXml;
		queryDate.value = resText.getElementsByTagName("td")[0].firstChild.nodeValue;
		queryDate1.value= resText.getElementsByTagName("td")[1].firstChild.nodeValue;
        window.setTimeout(changeType,300);
      }
      
      function loadData(isLeaf,pk,parameter,label){//点击数节点时触发，用于加载右侧的列表数据
        //document.getElementById("firstNode").style.display='';
        //alert(isLeaf+":"+pk+":"+parameter+":"+label);
        /*
        temp_dept_code=pk.substr(pk.indexOf("<dcode>")+"<dcode>".length);
 		temp_dept_code=temp_dept_code.substr(0,temp_dept_code.indexOf("</dcode>"));
 		dept_code.value=temp_dept_code;
        
        //dutyid.value = parameter;
 		var temp_sign;
 		temp_sign=pk.substr(pk.indexOf("<sign>")+"<sign>".length);
 		temp_sign=temp_sign.substr(0,temp_sign.indexOf("</sign>"));
 		
        if(temp_sign == "dept"){
          dutyid.value = 0;
          document.getElementById("insert").style.display='';
          document.getElementById("uninsert").style.display='none';
        }else{
          dutyid.value = parameter;
          document.getElementById("insert").style.display='none';
          document.getElementById("uninsert").style.display='';
        }
        */
        var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
		xmlDoc.async="false";  
		xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + pk + "</root>");
		//从PK中取得dcode sign
		dept_code.value = xmlDoc.getElementsByTagName("dcode")[0].firstChild.nodeValue;
		sign.value = xmlDoc.getElementsByTagName("sign")[0].firstChild.nodeValue;
		inlower.value = xmlDoc.getElementsByTagName("inlower")[0].firstChild.nodeValue;
		userid.value = getUserID();
		wsswsswss.value =  wsswss.value;
		//SalSort1.value = window.parentPage.document.body.all['SalSort'].value;
		//alert(SalSort1.value);
		//SalYear1.value = window.parentPage.document.body.all['SalYear'].value;
		//SalMonth1.value = window.parentPage.document.body.all['SalMonth'].value;
	//	var salyear = window.parentPage.document.body.all['SalYear'].value;
    //var salmonth = window.parentPage.document.body.all['SalMonth'].value;
        hr_att_wss_wageCycleBatch_select_PerAdjust.click();
        //editTable.submit(hr_att_wss_wageCycleBatch_select_PerAdjust,result);
      }
      
      function show_isstop(t){
        showstop=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
      }
      
      function include_lower(t){
        showlower=t.checked? '1':'0';
        var pk=getPk();
        unitTree.expand=texpands;
        unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
      }
      
      function show_all(t){
        var pk=getPk();
        if(t.checked){
          texpands=10;
          unitTree.expand=10;
          unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        }else{
          texpands=1;
          unitTree.expand=1;
          unitTree.setSqlData("hr_dtyHrJobSystemJobSetMain_treeselect",pk);
        }
      }
      
      function getPk(){
        return "<comp_code>"+getCompCode()+"</comp_code>"+"<user_id>0</user_id>"
               +"<show_duty>1</show_duty>"+"<show_stop>"+showstop+"</show_stop>"
               +"<include_lower>"+showlower+"</include_lower>"+"<is_super>0</is_super>"
               +"<show_count>0</show_count>";
      }
      
      //按钮权限查询
      function getPurview(){
      	window.xmlhttp.post('hr_empDocumentBasicInfoMain_buttonPurview', "<tableid>sys_emp</tableid><userid>"+getUserID()+"</userid>","?isCheck=false");
		var resText = window.xmlhttp._object.responseXml;
      	
      	var addPerm = resText.getElementsByTagName("td")[0].firstChild.nodeValue;
      	var editPerm = resText.getElementsByTagName("td")[1].firstChild.nodeValue;
      	var deletePerm = resText.getElementsByTagName("td")[2].firstChild.nodeValue;
      	var photoPerm = resText.getElementsByTagName("td")[3].firstChild.nodeValue;
      	
      	if(addPerm!='0'){
      		addPermButton.style.display="";
      	}else{
      		addPermButton.style.display="none";
      	}
      	if(editPerm!='0' && result.getRows()>0){
      		updatePermButton.style.display="";
      	}else{
      		updatePermButton.style.display="none";
      	}
      	if(deletePerm!='0' && result.getRows()>0){
      		deletePermButton.style.display="none";
      	}else{
      		deletePermButton.style.display="none";
      	}
      	if(photoPerm!='0' && result.getRows()>0){
      		photoPermButton.style.display="";
      	}else{
      		photoPermButton.style.display="none";
      	}
      }
      
      //判断result复选框 互斥
      var checkFlag=-1;    //记录第一次选择的复选框的行号
	  function judgeCheck(){
	  	if(result.getRows()>0){
		  	var count = 0;
	      	var data = result.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(checkFlag==-1){
			        		checkFlag=i;
			        	}else{
			        		if(checkFlag!=i){
						    	result.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[checkFlag].getElementsByTagName("input")[0].checked=false;
			        			checkFlag=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0) checkFlag=-1;  //恢复复选框行号记录初始值
	    }
	  }
	  
	  
      //添加页面需要的参数
      function insertMess(id){
			this.pk={id:'empid',value:id+"##"+"sys_emp"};
			this.button={text:'保存',name:'hr_empDocumentBasicInfoMain_insert',onclick:'editTable.submit(this);window.parentPage.refreshParent();',accessKey:'S'};
	  }
	  //添加页面需要的参数
	  function insertCallbacks(){
				setInsertPage('hr_empDocumentBasicInfoMain_showFields','<userid>'+getUserID()+'</userid>','',"<empid>"+empidvalue+"</empid><userid>"+getUserID()+"</userid>",new insertMess(empidvalue));
	  }
      
      //修改
      function update(obj){
      	var count = 0;
      	var data = result.getElementsByTagName('tbody')[1];
    	var trs = data.getElementsByTagName("tr"); 
    	for (var i=0; i<trs.length; i++){
    		var inputs =  trs[i].getElementsByTagName("input");
    		for (var j=0; j<inputs.length; j++) {	        
		        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
		        	//得到要修改的记录的empid
		        	var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
					xmlDoc.async="false";  
					xmlDoc.loadXML("<?xml version=\"1.0\" encoding=\"gb2312\"?><root>" + inputs[j].value + "</root>"); 
					empidvalue = xmlDoc.getElementsByTagName("empid")[0].firstChild.nodeValue;
		        	count++;
		        }
	    	}
    	}
    	if (count == 0) {
	      alert('请选择要修改的记录!');
	      return false;
	    }
	    if (count > 1) {
	      alert('只能选择一条记录进行修改!');
	      return false;
	    }
	    
      	openDialog('updateadd.html','dialogWidth:520px;dialogHeight:700px')
      }
      //修改页面需要的参数
      function updateMess(id){
			this.pk={id:'empid',value:id};
			this.button={text:'保存',name:'hr_empDocumentBasicInfoMain_update',onclick:'editTable.submit(this);window.parentPage.refreshParent();',accessKey:'S'};
	  }
	  //修改页面需要的参数
	  function updateCallbacks(){
				setUpdatePage('hr_empDocumentBasicInfoMain_update_list','<userid>'+getUserID()+'</userid>','hr_empDocumentBasicInfoMain_update',"<empid>"+empidvalue+"</empid><userid>"+getUserID()+"</userid>",new updateMess(empidvalue));
	  }
      
      //删除
      function deleteItems(obj){
      	var count = 0;
      	var data = result.getElementsByTagName('tbody')[1];
    	var trs = data.getElementsByTagName("tr"); 
    	for (var i=0; i<trs.length; i++){
    		var inputs =  trs[i].getElementsByTagName("input");
    		for (var j=0; j<inputs.length; j++) {	        
		        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
		        	count++;
		        }
	    	}
    	}
    	if (count == 0) {
	      alert('请选择要删除的记录!');
	      return false;
	    }
	    
	    //删除职工提示信息
	    var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
		xmlDoc.async="false";  
		xmlDoc.loadXML(result.getSelectXml());
	    window.xmlhttp.post('hr_empDocumentBasicInfoMain_deleteSuggestiveInfo', "<userid>"+getUserID()+"</userid><empid>"+xmlDoc.getElementsByTagName('empid')[0].firstChild.nodeValue+"</empid>");
		var resText = window.xmlhttp._object.responseXml;
	    if(resText.getElementsByTagName("td")[0].firstChild.nodeValue=="false"){
	    	alert(resText.getElementsByTagName("td")[1].firstChild.nodeValue);
	    }else{
	    	if(confirm("如果要删除用户，那么所有与该用户相关的 "
	    				+resText.getElementsByTagName("td")[1].firstChild.nodeValue
	    				+" 表中的记录都将被删除，确认要删除用户及其相关信息吗？")){
	    		result.submit_choose(obj.name,"<empid>"+xmlDoc.getElementsByTagName('empid')[0].firstChild.nodeValue+"</empid><userid>"+getUserID()+"</userid>");
				result.refresh();
	    	}
	    }
      }
      
      //刷新结果集
      function refreshParent(){
      	if(unitTree.style.display==''){
      		//alert("科室职位目录树");
      		window.document.body.all['hr_att_wss_wageCycleBatch_select_PerAdjust'].click();
      	}else{
      		//alert("条件控件");
      		judgeCommonCondListCheck();
      	}
	  }
	  
	  //关闭
	  function photoHandle(){
	  	window.close();
	  }
	  
	  //科室职位目录按钮
	  function deptDutyList(){
	  	unitTree.style.display='';
	  	tree_param.style.display="";
	  	condTable.style.display='none';
	  	commonCondDispaly.value='';
	  	//清空职工列表
        result.innerHTML = "";
        //判断职工功能按钮的权限
	  //	getPurview(); 
	  }
	  
	  //条件查询按钮 
	  function condSelect(){
	  	unitTree.style.display='none';
	  	tree_param.style.display="none";
	  	condTable.style.display='';
	  	commonCondSelectByUserid.value = getUserID();
        //清空职工列表
        result.innerHTML = "";
        //判断职工功能按钮的权限
       // getPurview();  
	  	hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  }
	  
	  //判断commonCondList复选框 互斥 给条件显示框赋值具体条件
      var commonCondListCheckFlag=-1;    //记录第一次选择的复选框的行号
	  function judgeCommonCondListCheck(){
	  	if(commonCondList.getRows()>0){
		  	var count = 0;
	      	var data = commonCondList.getElementsByTagName('tbody')[1];
	    	var trs = data.getElementsByTagName("tr");
	    	for (var i=0; i<trs.length; i++){
	    		var inputs =  trs[i].getElementsByTagName("input");
	    		for (var j=0; j<inputs.length; j++) {	        
			        if (inputs[j].type == 'checkbox' && inputs[j].checked) {
			        	count++;
			        	if(commonCondListCheckFlag==-1){
			        		commonCondListCheckFlag=i;
			        	}else{
			        		if(commonCondListCheckFlag!=i){
						    	commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
			        			commonCondListCheckFlag=i;
			        		}
			        	}
			        }
		    	}
	    	}
	    	if(count==0){ 
	    		commonCondListCheckFlag=-1;  //恢复复选框行号记录初始值
	    		commonCondDispaly.value = "";  //清空条件显示框
				//清空职工列表
				result.innerHTML = "";
	    	}else{
	    		//条件说明cond_desc 条件列表cond_list 条件内容cond
		  		cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
		  		cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
		  		cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
		  		
	    		//给条件显示框赋值具体条件
				commonCondDispaly.value = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
				//按常用条件查询职工
				var userid_cond = getUserID();
				var common_cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
				var common_cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
				var wss = wsswss.value;
					//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  
					common_cond = common_cond.replace(new RegExp("<","gm"),"&lt;");
					common_cond = common_cond.replace(new RegExp(">","gm"),"&gt;");
					
				//alert(common_cond);
				window.xmlhttp.post('hr_att_wss_wageCycleBatch_select_PerAdjust_select', "<userid_cond>"+userid_cond+"</userid_cond><common_cond>"+common_cond+"</common_cond><common_cond_list>"+common_cond_list+"</common_cond_list><wss>"+wss+"</wss>");
				var resText = window.xmlhttp._object.responseXml;
				result.setDataXml(resText.xml);
	    	}
	    //getPurview	();  //判断职工功能按钮的权限
	    }
	  }
	  
	  //新条件 在页面顶定义三个全局变量 条件说明cond_desc 条件列表cond_list 条件内容cond
	  function newCond(){
	  		//把全局变量清空
	  		cond_desc = "";
	  		cond_list = "";
	  		cond = "";
	  		//清空条件显示框
			commonCondDispaly.value = "";
			//清空职工列表
			result.innerHTML = "";
			//判断职工功能按钮的权限
        //	getPurview();  
	  		if(commonCondListCheckFlag!=-1){  //判断是否选择了常用条件
		  		//把列表commonCondList的复选框选择去掉
		  		commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
		  		//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1;  
		  		
			}
  			openIFDialog(window,'condadjust.html?load=<cond_type>new</cond_type>','dialogWidth:520px;dialogHeight:500px');
	  }
	  
	  //修改条件
	  function updateCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请选择条件！！！");
	  		return false;
	  	}else{/*
	  		if(commonCondListCheckFlag==-1){
	  			alert("请选择一个常用条件！！！");
	  			return false;
	  		}else{*/
		  		//条件说明cond_desc 条件列表cond_list 条件内容cond
		  	//	cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;
		  	//	cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
		  	//	cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;
		  		cond = cond.replace(/</g,"小于");
				cond = cond.replace(/>/g,"大于");
		  		//把列表commonCondList的复选框选择去掉
		  	//	commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("input")[0].checked=false;
		  		//恢复复选框行号记录初始值
		  	//	commonCondListCheckFlag=-1;  
		  		//清空条件显示框
			//	commonCondDispaly.value = "";
				//清空职工列表
			//	result.innerHTML = "";
				//判断职工功能按钮的权限
        		//getPurview();  
        

        		//处理特殊字符 "&lt;"、"&gt;" 变成 "小于"、"大于"  
				cond = cond.replace(/&lt;/g,"小于");
				cond = cond.replace(/&gt;/g,"大于");

		  		openIFDialog(window,'condadjust.html?load=<cond_type>update</cond_type><cond_desc>'+cond_desc+'</cond_desc><cond_list>'+cond_list+'</cond_list><cond>'+cond+'</cond>','dialogWidth:520px;dialogHeight:500px');
		  	//}
	  	}
	  }
	  
	  //添加新条件后判断按钮的显示
	  function getPurviewAfterInsertCond(){
	  		//getPurview(); 
	  }
	  //保存条件 输入条件名称
	  function saveCond(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
	  		openIFDialog(window,'../../../../hr/condName.html','dialogWidth:400px;dialogHeight:200px');
	  	}
	  }

	  //保存条件
	  function saveCond1(){
	  	if(commonCondDispaly.value.length==0){
	  		alert("条件为空，请新建条件！！！");
	  		return false;
	  	}else{
  			//var cond_title = window.prompt("请输入输入常用条件名称！！！","");
  			if(cond_title==null || cond_title.length==0){
  				alert("条件名称为空！！！");
  				return false;
  			}else{
  			
	  			/*if (cond_desc==null || cond_desc==""){
				cond_desc = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[4].firstChild.nodeValue;			
				}
				if(cond_list==null || cond_list==""){
					cond_list = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[3].firstChild.nodeValue;
				}
				if(cond==null || cond==""){
					cond = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[5].firstChild.nodeValue;		
					//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  		
					cond = cond.replace(new RegExp("<","gm"),"&lt;");
					cond = cond.replace(new RegExp(">","gm"),"&gt;");
				} */		
				
						
				//处理特殊字符 "<"、">" 变成 "&lt;"、"&gt;"  
				cond = cond.replace(/</g,"&lt;");
				cond = cond.replace(/>/g,"&gt;");

  			
	  			window.xmlhttp.post('hr_empDocumentBasicInfoMain_insertCond', "<user_id>"+getUserID()+"</user_id><cond_title>"+cond_title+"</cond_title><cond_desc>"+cond_desc+"</cond_desc><cond_list>"+cond_list+"</cond_list><cond>"+cond+"</cond>","?isCheck=false");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
		  		commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空职工列表
				result.innerHTML = "";
				//判断职工功能按钮的权限
	        	//getPurview();  
	  			//刷新常用条件
	  			hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  		}
	  	}
	  }
	  
	  //删除条件
	  function deleteCond(){
	  	if(commonCondListCheckFlag==-1){
	  		alert("请选择一个常用条件！！！");
	  		return false;
	  	}else{
	  		if(confirm('确定删除该常用条件吗？')){
		  		var cond_id = commonCondList.getElementsByTagName("tbody")[1].getElementsByTagName("tr")[commonCondListCheckFlag].getElementsByTagName("td")[2].firstChild.nodeValue;
		  		window.xmlhttp.post('hr_empDocumentBasicInfoMain_deleteCond', "<cond_id>"+cond_id+"</cond_id>","?isCheck=false");
	  			alert(window.xmlhttp._object.responseXml.getElementsByTagName("td")[0].firstChild.nodeValue);
	  			//恢复复选框行号记录初始值
			  	commonCondListCheckFlag=-1; 
	  			//清空条件显示框
				commonCondDispaly.value = "";
				//清空职工列表
				result.innerHTML = "";
				//判断职工功能按钮的权限
	        	//getPurview();  
	  			//刷新常用条件
	  			hr_empDocumentBasicInfoMain_commonCondSelect.click();
	  		}else{
	  			return false;
	  		}
	  	}
	  }
 function update(){
      	if(confirm('您确定要删除吗？')){
      	result.submit_choose("hr_slrSalPersonnelPersonnelSal_PerAdjust_delete")
      	
			//	result.refresh();
   }
      }	  
function changeType(){//开始日期不能大于结束日期
	var d=getValuePairBySql('wage_month_begin_end_date_new',"");//返回当前年月的开始日期和结束日期

	if(queryDate.value > d[1] || queryDate.value < d[0])
	{
		queryDate.value = d[0];
		alert('日期必须在当前期间范围内!');
		}
       if(queryDate.value > queryDate1.value)
       {
       	queryDate.value = queryDate1.value;
       	alert('开始日期不能大于结束日期');
       	
       	}
      }   
      
function changeType1(){//开始日期不能大于结束日期
	var d=getValuePairBySql('wage_month_begin_end_date_new',"");//返回当前年月的开始日期和结束日期

	if(queryDate1.value > d[1] || queryDate1.value < d[0])
	{
		queryDate1.value = d[1];
		alert('日期必须在当前期间范围内!');
		}
       if(queryDate.value > queryDate1.value)
       {
       	queryDate1.value = queryDate.value;
       	alert('开始日期不能大于结束日期');
       	
       	}
      }              
//添加
 function insert(){
	  	if(KQshu.value >1){
	  		alert('考勤数量不能大于1!');
	  		return;
	  	}
	  	var strHtml='';
	  	//  var strHtml = "<multiData>"; 
	    
	  	var inputs = result.getElementsByTagName("INPUT");
	  	var is_check=false;
	  	var is_dept=false
		var dept_code="";
		
    	for(var i=0;i<inputs.length;i++){
    		if(inputs[i].type == 'checkbox' && inputs[i].checked && inputs[i].value != 'on'){    		
    			strHtml+= '<record>'+"<dispType>"+dispType.value+"</dispType><dateRank>"+dateRank.value+"</dateRank><queryDate>"+queryDate.value+"</queryDate><queryDate1>"+queryDate1.value+"</queryDate1><cycledept_code>"+cycledept_code.value+"</cycledept_code><amount>"+amount.value+"</amount>"+inputs[i].value+'</record>';
    			dept_code=inputs[i].value.replace("<dept_code>",",");
    			dept_code=dept_code.replace("</dept_code>",",");
    			if(dept_code.split(",")[1]!=cycledept_code.value){//检查所选部门与考勤部门是否相同
    				is_dept=true;
    			}	
    			is_check=true;
    			//strHtml += inputs[i].value.replace(/<[/]?pid>/g,"") + ";";
    		}
    	}
    	if(!is_check){
    		alert("请选择！");
    		return;
    	}
    	if(!is_dept){
    		alert("所选职工的“部门名称”与“轮转考勤科室”不能相同，请重新选择！");
    		return;
    	}
    //	strHtml+="</multiData>";    
    
		window.xmlhttp.post("hr_att_wss_wageCycleBatch_batchSave",strHtml,"?isCheck=false");
	   	var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");   
		xmlDoc.async="false"; 
		xmlDoc.load(xmlhttp._object.responseXML);	
		var error=xmlDoc.selectSingleNode("root/error");
		var msg=xmlDoc.selectSingleNode("root/msg");
		var tbody=xmlDoc.selectSingleNode("root/tbody");	

		if(error.text){
			alert(error.text);	
			return;	
		}
	  	else if(msg.text!="" && tbody.text==""){
	  		alert(msg.text);
	  		
	  	}else{//显示未更新的数据
	  	//	openIFDialog(window, 'baseInfoMainError.html?<dispType>'+dispType.value+'</dispType><dateRank>'+dateRank.value+'</dateRank><queryDate>'+queryDate.value+'</queryDate><queryDate1>'+queryDate1.value+'</queryDate1><amount>'+amount.value+'</amount>', 'dialogWidth:600px;dialogHeight:600px')
	  		openDialog('baseInfoMainError.html', 'dialogWidth:800px;dialogHeight:600px')
	  	}
     //	result.submit_choose("hr_att_wss_wageCycleBatch_batchSave","<dispType>"+dispType.value+"</dispType><dateRank>"+dateRank.value+"</dateRank><queryDate>"+queryDate.value+"</queryDate><queryDate1>"+queryDate1.value+"</queryDate1><cycledept_code>"+cycledept_code.value+"</cycledept_code><amount>"+amount.value+"</amount>")
      	
				//result.refresh();
   
      }      