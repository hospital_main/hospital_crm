<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      	<tr noWrap='true' class='mainHead'>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 0]">
				 <xsl:choose>
					<xsl:when test="position() &lt; 5">
						<th nowrap='true' rowspan="2" width='80'><xsl:value-of select="."/></th>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="string-length(.) = 3 ">
							<xsl:if test="string(.) = '星期六' or string(.) = '星期日'">
								<th nowrap='true' background="#99CDFF"  bgcolor="#99CDFF" width='80'>
									<xsl:value-of select="."/>
								</th>
							</xsl:if>
							<xsl:if test="string(.) != '星期六' and string(.) != '星期日'">
								<th nowrap='true' width='80'>
									<xsl:value-of select="."/>
								</th>
							</xsl:if>
						</xsl:if>
						<xsl:if test="string-length(.) != 3">
							<th nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF" width='80'>		
							<xsl:value-of select="substring(.,2,string-length(.))"/>
							</th>
						</xsl:if>
						
					 </xsl:otherwise>
				 </xsl:choose>
			</xsl:for-each>
      	</tr>
      	<tr noWrap='true' class='mainHead'>
			<xsl:for-each select="/root/tbody/tr[2]/td[position() &gt; 4]">
				<xsl:if test="string-length(.) = 1 or string-length(.) = 2 ">
					<xsl:if test="string-length(.) = 1">
						<th nowrap='true'><xsl:value-of select="."/></th>
					</xsl:if>
					<xsl:if test="string-length(.) = 2">
						<xsl:if test="starts-with(.,'_') = true() ">
						<th nowrap='true' background="#99CDFF"  bgcolor="#99CDFF"><xsl:value-of select="substring-after(.,'_')"/></th>
						</xsl:if>
						<xsl:if test="starts-with(.,'_') = false() ">
						<th nowrap='true'><xsl:value-of select="."/></th>
						</xsl:if>	
					</xsl:if>
				</xsl:if>
				<xsl:if test="string-length(.) &gt; 2 ">
					<xsl:if test="starts-with(.,'_') = true() ">
						<th nowrap='true' background="#99CDFF"  bgcolor="#99CDFF"><xsl:value-of select="substring-after(.,'_')"/></th>
					</xsl:if>
					<xsl:if test="starts-with(.,'_') = false() ">
						<th nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF"><xsl:value-of select="."/></th>
					</xsl:if>	
				</xsl:if>
			</xsl:for-each>
      	</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
      	<xsl:variable name="EMP_CODE" select="td[1]"/>
        <tr>
          <xsl:for-each select="td[position() &gt; 0]">
          	<xsl:variable name="NUM" select="position()-4"></xsl:variable>
            <td>
            	<xsl:attribute name="onDblClick">
					selectTdData(this,"<xsl:value-of select="$EMP_CODE"/>","<xsl:value-of select="$NUM"/>")
                 </xsl:attribute>
          <xsl:attribute name="id">tdid_<xsl:value-of select="../pk/check_id"/>_<xsl:value-of select="$NUM"/>_</xsl:attribute>
          <xsl:attribute name="pk" ><xsl:value-of select="../pk/check_id"/></xsl:attribute>
                <xsl:choose>
							  <xsl:when test="position() &lt; 5">
								 <xsl:value-of select="."/>
							  </xsl:when>
							  <xsl:otherwise>
								<xsl:if test="string-length(.) &lt;= 5 ">
									<xsl:attribute name="width">
									 <xsl:value-of select="80"/>
									 </xsl:attribute>
									<xsl:value-of select="."/>
								</xsl:if>
								<xsl:if test="string-length(.) &gt; 5">
									<xsl:value-of select="substring(.,0,6)"/>...
								</xsl:if>
							  </xsl:otherwise>
							</xsl:choose>
				           
						 </td>
              
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>

