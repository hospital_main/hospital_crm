<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <thead>
      	<tr noWrap='true' class='mainHead'>
		 <th style='' width='25' rowspan="2" ><input type='checkbox'/></th>
			<xsl:for-each select="/root/tbody/tr[1]/td[position() &gt; 0]">
				<xsl:variable name="TR1_AMOUNT" select="/root/tbody/tr[1]/td[last()]"/>
				 <xsl:choose>
					<xsl:when test="position() &lt; 5 ">
						<th nowrap='true' rowspan="2" width='80'><xsl:value-of select="."/></th>
					</xsl:when>
					<xsl:when test="position() &gt; $TR1_AMOUNT + 4 and position() &lt; last()">
						<th nowrap='true' rowspan="2" width='80'><xsl:value-of select="."/></th>
					</xsl:when>
					<xsl:when test="position() = last()">
						<th nowrap='true' style="display:none"></th>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="string-length(.) = 3 ">
							<xsl:if test="string(.) = '星期六' or string(.) = '星期日'">
								<th nowrap='true' background="#99CDFF"  bgcolor="#99CDFF" width='80'>
									<xsl:value-of select="."/>
								</th>
							</xsl:if>
							<xsl:if test="string(.) != '星期六' and string(.) != '星期日'">
								<th nowrap='true' width='80'>
									<xsl:value-of select="."/>
								</th>
							</xsl:if>
						</xsl:if>
						<xsl:if test="string-length(.) != 3">
							<th nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF" width='80'>		
							<xsl:value-of select="substring(.,2,string-length(.))"/>
							</th>
						</xsl:if>
					 </xsl:otherwise>
				 </xsl:choose>
			</xsl:for-each>
      	</tr>
      	<tr noWrap='true' class='mainHead'>
      		<th nowrap='true' style="display:none"></th>
      		<th nowrap='true' style="display:none"></th>
      		<th nowrap='true' style="display:none"></th>
      		<th nowrap='true' style="display:none"></th>
      		<th nowrap='true' style="display:none"></th>
			<xsl:for-each select="/root/tbody/tr[2]/td[position() &gt; 4]">
			<xsl:variable name="TR2_AMOUNT" select="/root/tbody/tr[2]/td[last()]"/>
				<xsl:choose>
					<xsl:when test="position() &gt; $TR2_AMOUNT">
						<th nowrap='true' style="display:none"></th>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="string-length(.) = 1 or string-length(.) = 2 ">
							<xsl:if test="string-length(.) = 1">
								<th nowrap='true'><xsl:value-of select="."/></th>
							</xsl:if>
							<xsl:if test="string-length(.) = 2">
								<xsl:if test="starts-with(.,'_') = true() ">
								<th nowrap='true' background="#99CDFF"  bgcolor="#99CDFF"><xsl:value-of select="substring-after(.,'_')"/></th>
								</xsl:if>
								<xsl:if test="starts-with(.,'_') = false() ">
								<th nowrap='true'><xsl:value-of select="."/></th>
								</xsl:if>	
							</xsl:if>
						</xsl:if>
						<xsl:if test="string-length(.) &gt; 2 ">
							<xsl:if test="starts-with(.,'_') = true() ">
								<th nowrap='true' background="#99CDFF"  bgcolor="#99CDFF"><xsl:value-of select="substring-after(.,'_')"/></th>
							</xsl:if>
							<xsl:if test="starts-with(.,'_') = false() ">
								<th nowrap='true' background="#FFCCFF"  bgcolor="#FFCCFF"><xsl:value-of select="."/></th>
							</xsl:if>	
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
      	</tr>
    </thead>
    <tbody>
      <xsl:for-each select="/root/tbody/tr[position() &gt; 2]">
			<xsl:variable name="TR2_AMOUNT" select="/root/tbody/tr[2]/td[last()]"/>
			<xsl:variable name="SUB_STATE" select="td[$TR2_AMOUNT+5]"/>
			<xsl:variable name="IS_TURN" select="td[$TR2_AMOUNT+6]"/>
			<xsl:variable name="EMP_CODE" select="td[1]"/>
			<xsl:variable name="CHECK_ID" select="./pk/check_id"/>
        <tr>
           <td align="right">
			   <input type='checkbox' TABINDEX='-1' style='font-size:8px;'>
				    <xsl:attribute name="value" >
						<xsl:for-each select="pk/*">&lt;<xsl:value-of select="name()"/>&gt;<xsl:value-of select="."/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:for-each>
						</xsl:attribute>
						<xsl:if test="$SUB_STATE = '是' or $IS_TURN = '是'">
							<xsl:attribute name="disabled" >disabled</xsl:attribute>
						</xsl:if>
				</input>
           </td>
          <xsl:for-each select="td[position() &gt; 0]">
          <xsl:variable name="NUM" select="position()- 4 "></xsl:variable>
          <xsl:choose>
					<xsl:when test="position() = last()">
						<td nowrap='true' style="display:none"></td>
					</xsl:when>
					<xsl:when test="position() &gt; 4 and position() &lt; $TR2_AMOUNT + 5">
 	<!-- colorChange(this);getTdData(this,"<xsl:value-of select="$CHECK_ID"/>","<xsl:value-of select="$NUM"/>")-->
				  <td>
				     <xsl:attribute name="onDblClick">
				     			selectTdData(this,"<xsl:value-of select="$CHECK_ID"/>","<xsl:value-of select="$NUM"/>","<xsl:value-of select="$SUB_STATE"/>","<xsl:value-of select="$IS_TURN"/>")
						 </xsl:attribute>
						 <xsl:attribute name="id">tdid_<xsl:value-of select="../pk/check_id"/>_<xsl:value-of select="$NUM"/>_</xsl:attribute>
						<xsl:if test="string-length(.) &lt;= 6 ">
							<xsl:attribute name="width">
								 <xsl:value-of select="80"/>
								 </xsl:attribute>
							 <xsl:value-of select="."/>
						</xsl:if>
					<xsl:if test="string-length(.) &gt; 5">
						<xsl:value-of select="substring(.,0,6)"/>...
					</xsl:if>
					</td>
				  </xsl:when>
				  <xsl:otherwise>
									  <td>
						<xsl:value-of select="."/>
				  </td>
				  </xsl:otherwise>
				</xsl:choose>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </tbody>
  </xsl:template>
</xsl:stylesheet>